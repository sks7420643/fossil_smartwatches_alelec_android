package com.portfolio.platform.uirenew.home.profile.opt;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.Gq6;
import com.fossil.Hq6;
import com.fossil.Hr7;
import com.fossil.I14;
import com.fossil.R95;
import com.fossil.S37;
import com.fossil.Um5;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileOptInFragment extends BaseFragment implements Hq6 {
    @DexIgnore
    public static /* final */ Ai j; // = new Ai(null);
    @DexIgnore
    public G37<R95> g;
    @DexIgnore
    public Gq6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final ProfileOptInFragment a() {
            return new ProfileOptInFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInFragment b;

        @DexIgnore
        public Bi(ProfileOptInFragment profileOptInFragment) {
            this.b = profileOptInFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInFragment b;
        @DexIgnore
        public /* final */ /* synthetic */ R95 c;

        @DexIgnore
        public Ci(ProfileOptInFragment profileOptInFragment, R95 r95) {
            this.b = profileOptInFragment;
            this.c = r95;
        }

        @DexIgnore
        public final void onClick(View view) {
            ProfileOptInFragment profileOptInFragment = this.b;
            FlexibleSwitchCompat flexibleSwitchCompat = this.c.q;
            Wg6.b(flexibleSwitchCompat, "binding.anonymousSwitch");
            profileOptInFragment.N6("Usage_Data", flexibleSwitchCompat.isChecked());
            AnalyticsHelper B6 = this.b.B6();
            FlexibleSwitchCompat flexibleSwitchCompat2 = this.c.q;
            Wg6.b(flexibleSwitchCompat2, "binding.anonymousSwitch");
            B6.v(flexibleSwitchCompat2.isChecked());
            Gq6 L6 = ProfileOptInFragment.L6(this.b);
            FlexibleSwitchCompat flexibleSwitchCompat3 = this.c.q;
            Wg6.b(flexibleSwitchCompat3, "binding.anonymousSwitch");
            L6.n(flexibleSwitchCompat3.isChecked());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInFragment b;
        @DexIgnore
        public /* final */ /* synthetic */ R95 c;

        @DexIgnore
        public Di(ProfileOptInFragment profileOptInFragment, R95 r95) {
            this.b = profileOptInFragment;
            this.c = r95;
        }

        @DexIgnore
        public final void onClick(View view) {
            ProfileOptInFragment profileOptInFragment = this.b;
            FlexibleSwitchCompat flexibleSwitchCompat = this.c.y;
            Wg6.b(flexibleSwitchCompat, "binding.scSubcribeEmail");
            profileOptInFragment.N6("Emails", flexibleSwitchCompat.isChecked());
            AnalyticsHelper B6 = this.b.B6();
            FlexibleSwitchCompat flexibleSwitchCompat2 = this.c.y;
            Wg6.b(flexibleSwitchCompat2, "binding.scSubcribeEmail");
            B6.u(flexibleSwitchCompat2.isChecked());
            Gq6 L6 = ProfileOptInFragment.L6(this.b);
            FlexibleSwitchCompat flexibleSwitchCompat3 = this.c.y;
            Wg6.b(flexibleSwitchCompat3, "binding.scSubcribeEmail");
            L6.o(flexibleSwitchCompat3.isChecked());
        }
    }

    /*
    static {
        Wg6.b(ProfileOptInFragment.class.getSimpleName(), "ProfileOptInFragment::class.java.simpleName");
    }
    */

    @DexIgnore
    public static final /* synthetic */ Gq6 L6(ProfileOptInFragment profileOptInFragment) {
        Gq6 gq6 = profileOptInFragment.h;
        if (gq6 != null) {
            return gq6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Hq6
    public void C(int i2, String str) {
        Wg6.c(str, "message");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.Hq6
    public void F4(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        G37<R95> g37 = this.g;
        if (g37 != null) {
            R95 a2 = g37.a();
            if (a2 != null && (flexibleSwitchCompat = a2.q) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Hq6
    public void J5() {
        a();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Gq6 gq6) {
        O6(gq6);
    }

    @DexIgnore
    public final void N6(String str, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("Item", str);
        hashMap.put("Optin", z ? "Yes" : "No");
        w6("profile_optin", hashMap);
    }

    @DexIgnore
    public void O6(Gq6 gq6) {
        Wg6.c(gq6, "presenter");
        I14.l(gq6);
        Wg6.b(gq6, "checkNotNull(presenter)");
        this.h = gq6;
    }

    @DexIgnore
    @Override // com.fossil.Hq6
    public void P0() {
        String string = getString(2131886911);
        Wg6.b(string, "getString(R.string.Onboa\u2026ggingIn_Text__PleaseWait)");
        H6(string);
    }

    @DexIgnore
    @Override // com.fossil.Hq6
    public void Y4(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        G37<R95> g37 = this.g;
        if (g37 != null) {
            R95 a2 = g37.a();
            if (a2 != null && (flexibleSwitchCompat = a2.y) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        R95 r95 = (R95) Aq0.e(LayoutInflater.from(getContext()), 2131558599, null, false);
        r95.w.setOnClickListener(new Bi(this));
        FlexibleTextView flexibleTextView = r95.u;
        Wg6.b(flexibleTextView, "binding.ftvDescriptionSubcribeEmail");
        Hr7 hr7 = Hr7.a;
        String c = Um5.c(PortfolioApp.get.instance(), 2131887216);
        Wg6.b(c, "LanguageHelper.getString\u2026_GetTipsAboutBrandsTools)");
        String format = String.format(c, Arrays.copyOf(new Object[]{PortfolioApp.get.instance().Q()}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        flexibleTextView.setText(format);
        r95.q.setOnClickListener(new Ci(this, r95));
        r95.y.setOnClickListener(new Di(this, r95));
        this.g = new G37<>(this, r95);
        Wg6.b(r95, "binding");
        return r95.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Gq6 gq6 = this.h;
        if (gq6 != null) {
            gq6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Gq6 gq6 = this.h;
        if (gq6 != null) {
            gq6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
