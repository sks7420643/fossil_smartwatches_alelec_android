package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings;

import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Ko7;
import com.fossil.Pm7;
import com.fossil.Ts0;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppSettingsViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public Gson a; // = new Gson();
    @DexIgnore
    public CommuteTimeWatchAppSetting b;
    @DexIgnore
    public MFUser c;
    @DexIgnore
    public MutableLiveData<Ai> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ UserRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public List<AddressWrapper> a;

        @DexIgnore
        public Ai(List<AddressWrapper> list) {
            this.a = list;
        }

        @DexIgnore
        public final List<AddressWrapper> a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || ((obj instanceof Ai) && Wg6.a(this.a, ((Ai) obj).a));
        }

        @DexIgnore
        public int hashCode() {
            List<AddressWrapper> list = this.a;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(addresses=" + this.a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel$start$1", f = "CommuteTimeWatchAppSettingsViewModel.kt", l = {63}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeWatchAppSettingsViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel$start$1$1", f = "CommuteTimeWatchAppSettingsViewModel.kt", l = {63}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.e;
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = commuteTimeWatchAppSettingsViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel2 = this.this$0;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.L$1 = commuteTimeWatchAppSettingsViewModel2;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
                commuteTimeWatchAppSettingsViewModel = commuteTimeWatchAppSettingsViewModel2;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                commuteTimeWatchAppSettingsViewModel = (CommuteTimeWatchAppSettingsViewModel) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            commuteTimeWatchAppSettingsViewModel.c = (MFUser) g;
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel3 = this.this$0;
            CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = commuteTimeWatchAppSettingsViewModel3.b;
            commuteTimeWatchAppSettingsViewModel3.e(commuteTimeWatchAppSetting != null ? commuteTimeWatchAppSetting.getAddresses() : null);
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = CommuteTimeWatchAppSettingsViewModel.class.getSimpleName();
        Wg6.b(simpleName, "CommuteTimeWatchAppSetti\u2026el::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public CommuteTimeWatchAppSettingsViewModel(An4 an4, UserRepository userRepository) {
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(userRepository, "mUserRepository");
        this.e = userRepository;
    }

    @DexIgnore
    public final void e(List<AddressWrapper> list) {
        this.d.l(new Ai(list));
    }

    @DexIgnore
    public final CommuteTimeWatchAppSetting f() {
        return this.b;
    }

    @DexIgnore
    public final CommuteTimeWatchAppSetting g() {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting(new ArrayList());
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.b;
        if (commuteTimeWatchAppSetting2 != null) {
            List<AddressWrapper> addresses = commuteTimeWatchAppSetting2.getAddresses();
            ArrayList arrayList = new ArrayList();
            for (T t : addresses) {
                if (!TextUtils.isEmpty(t.getAddress())) {
                    arrayList.add(t);
                }
            }
            commuteTimeWatchAppSetting.setAddresses(Pm7.j0(arrayList));
        }
        return commuteTimeWatchAppSetting;
    }

    @DexIgnore
    public final MutableLiveData<Ai> h() {
        return this.d;
    }

    @DexIgnore
    public final void i(String str) {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = f;
        local.d(str2, "init with setting " + str);
        try {
            commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) this.a.k(str, CommuteTimeWatchAppSetting.class);
            List<AddressWrapper> addresses = commuteTimeWatchAppSetting.getAddresses();
            ArrayList arrayList = new ArrayList();
            for (T t : addresses) {
                if (t.getType() == AddressWrapper.AddressType.HOME) {
                    arrayList.add(t);
                }
            }
            List<AddressWrapper> addresses2 = commuteTimeWatchAppSetting.getAddresses();
            ArrayList arrayList2 = new ArrayList();
            for (T t2 : addresses2) {
                if (t2.getType() == AddressWrapper.AddressType.WORK) {
                    arrayList2.add(t2);
                }
            }
            if (arrayList.isEmpty()) {
                commuteTimeWatchAppSetting.getAddresses().add(0, new AddressWrapper(AddressWrapper.AddressType.HOME.getValue(), AddressWrapper.AddressType.HOME));
            }
            if (arrayList2.isEmpty()) {
                commuteTimeWatchAppSetting.getAddresses().add(1, new AddressWrapper(AddressWrapper.AddressType.WORK.getValue(), AddressWrapper.AddressType.WORK));
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = f;
            local2.d(str3, "exception when parse commute time setting " + e2);
            commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting(null, 1, null);
        }
        this.b = commuteTimeWatchAppSetting;
        if (commuteTimeWatchAppSetting == null) {
            this.b = new CommuteTimeWatchAppSetting(null, 1, null);
        }
    }

    @DexIgnore
    public final boolean j() {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.b;
        return commuteTimeWatchAppSetting != null && commuteTimeWatchAppSetting.getAddresses().size() < 10;
    }

    @DexIgnore
    public final void k(AddressWrapper addressWrapper) {
        FLogger.INSTANCE.getLocal().d(f, "onUserChooseAddress " + addressWrapper);
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.b;
        if (!(commuteTimeWatchAppSetting == null || addressWrapper == null)) {
            if (commuteTimeWatchAppSetting != null) {
                int i = 0;
                int i2 = -1;
                for (T t : commuteTimeWatchAppSetting.getAddresses()) {
                    if (i >= 0) {
                        int i3 = Wg6.a(addressWrapper.getId(), t.getId()) ? i : i2;
                        i++;
                        i2 = i3;
                    } else {
                        Hm7.l();
                        throw null;
                    }
                }
                if (i2 != -1) {
                    CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.b;
                    if (commuteTimeWatchAppSetting2 != null) {
                        commuteTimeWatchAppSetting2.getAddresses().remove(i2);
                        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting3 = this.b;
                        if (commuteTimeWatchAppSetting3 != null) {
                            commuteTimeWatchAppSetting3.getAddresses().add(i2, addressWrapper);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    CommuteTimeWatchAppSetting commuteTimeWatchAppSetting4 = this.b;
                    if (commuteTimeWatchAppSetting4 != null) {
                        commuteTimeWatchAppSetting4.getAddresses().add(addressWrapper);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting5 = this.b;
                e(commuteTimeWatchAppSetting5 != null ? commuteTimeWatchAppSetting5.getAddresses() : null);
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void l(AddressWrapper addressWrapper) {
        List<AddressWrapper> list = null;
        if (addressWrapper != null) {
            if (addressWrapper.getType() == AddressWrapper.AddressType.OTHER) {
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.b;
                if (commuteTimeWatchAppSetting != null) {
                    commuteTimeWatchAppSetting.getAddresses().remove(addressWrapper);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.b;
                if (commuteTimeWatchAppSetting2 != null) {
                    commuteTimeWatchAppSetting2.getAddresses();
                    if (Wg6.a(addressWrapper.getId(), addressWrapper.getId())) {
                        addressWrapper.setAddress("");
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            CommuteTimeWatchAppSetting commuteTimeWatchAppSetting3 = this.b;
            if (commuteTimeWatchAppSetting3 != null) {
                list = commuteTimeWatchAppSetting3.getAddresses();
            }
            e(list);
        }
    }

    @DexIgnore
    public final void m() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Bi(this, null), 3, null);
    }
}
