package com.portfolio.platform.uirenew.home.details.heartrate;

import android.os.Bundle;
import android.util.Pair;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Ai5;
import com.fossil.Ao7;
import com.fossil.At7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gl7;
import com.fossil.Gu7;
import com.fossil.H47;
import com.fossil.Hr7;
import com.fossil.Hs0;
import com.fossil.Im6;
import com.fossil.Jm6;
import com.fossil.Kl5;
import com.fossil.Km6;
import com.fossil.Ko7;
import com.fossil.Lm7;
import com.fossil.Ls0;
import com.fossil.Mn7;
import com.fossil.Or0;
import com.fossil.Pm7;
import com.fossil.Qq7;
import com.fossil.Ss0;
import com.fossil.Ts7;
import com.fossil.W57;
import com.fossil.Xh5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Cf;
import com.mapped.Coroutine;
import com.mapped.Fd;
import com.mapped.Hg6;
import com.mapped.Hh6;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.PagingRequestHelper;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.U04;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.workout.WorkoutTetherScreenShotManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateDetailPresenter extends Im6 implements PagingRequestHelper.Ai {
    @DexIgnore
    public Date e;
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public MutableLiveData<Lc6<Date, Date>> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ WorkoutTetherScreenShotManager h;
    @DexIgnore
    public List<DailyHeartRateSummary> i;
    @DexIgnore
    public List<HeartRateSample> j;
    @DexIgnore
    public DailyHeartRateSummary k;
    @DexIgnore
    public List<HeartRateSample> l;
    @DexIgnore
    public Ai5 m;
    @DexIgnore
    public LiveData<H47<List<DailyHeartRateSummary>>> n;
    @DexIgnore
    public LiveData<H47<List<HeartRateSample>>> o;
    @DexIgnore
    public Listing<WorkoutSession> p;
    @DexIgnore
    public /* final */ Jm6 q;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository r;
    @DexIgnore
    public /* final */ HeartRateSampleRepository s;
    @DexIgnore
    public /* final */ UserRepository t;
    @DexIgnore
    public /* final */ WorkoutSessionRepository u;
    @DexIgnore
    public /* final */ U04 v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Hg6<HeartRateSample, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Date date) {
            super(1);
            this.$date = date;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Boolean invoke(HeartRateSample heartRateSample) {
            return Boolean.valueOf(invoke(heartRateSample));
        }

        @DexIgnore
        public final boolean invoke(HeartRateSample heartRateSample) {
            Wg6.c(heartRateSample, "it");
            return TimeUtils.m0(heartRateSample.getDate(), this.$date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$observeWorkoutSessionData$1", f = "HeartRateDetailPresenter.kt", l = {93}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii<T> implements Ls0<Cf<WorkoutSession>> {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$observeWorkoutSessionData$1$1$1", f = "HeartRateDetailPresenter.kt", l = {104}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Cf $pageList;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Xe6 xe6, Aiii aiii) {
                        super(2, xe6);
                        this.this$0 = aiii;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        Aiiii aiiii = new Aiiii(xe6, this.this$0);
                        aiiii.p$ = (Il6) obj;
                        throw null;
                        //return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                        throw null;
                        //return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    /* JADX WARNING: Removed duplicated region for block: B:25:0x009c  */
                    /* JADX WARNING: Removed duplicated region for block: B:41:0x0014 A[SYNTHETIC] */
                    @Override // com.fossil.Zn7
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                        /*
                        // Method dump skipped, instructions count: 319
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter.Bi.Aii.Aiii.Aiiii.invokeSuspend(java.lang.Object):java.lang.Object");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Cf cf, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                    this.$pageList = cf;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$pageList, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        FragmentActivity activity = ((Km6) this.this$0.a.this$0.q).getActivity();
                        if (activity != null) {
                            WorkoutTetherScreenShotManager workoutTetherScreenShotManager = this.this$0.a.this$0.h;
                            Wg6.b(activity, "it");
                            workoutTetherScreenShotManager.w(activity);
                            Dv7 a2 = Bw7.a();
                            Aiiii aiiii = new Aiiii(null, this);
                            this.L$0 = il6;
                            this.L$1 = activity;
                            this.label = 1;
                            if (Eu7.g(a2, aiiii, this) == d) {
                                return d;
                            }
                        }
                    } else if (i == 1) {
                        FragmentActivity fragmentActivity = (FragmentActivity) this.L$1;
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    Jm6 jm6 = this.this$0.a.this$0.q;
                    Ai5 ai5 = this.this$0.a.this$0.m;
                    Cf<WorkoutSession> cf = this.$pageList;
                    Wg6.b(cf, "pageList");
                    jm6.s(true, ai5, cf);
                    return Cd6.a;
                }
            }

            @DexIgnore
            public Aii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            public final void a(Cf<WorkoutSession> cf) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HeartRateDetailPresenter", "getWorkoutSessionsPaging observed size = " + cf.size());
                if (DeviceHelper.o.y(PortfolioApp.get.instance().J())) {
                    Wg6.b(cf, "pageList");
                    if (Pm7.j0(cf).isEmpty()) {
                        this.a.this$0.q.s(false, this.a.this$0.m, cf);
                        return;
                    }
                }
                Rm6 unused = Gu7.d(this.a.this$0.k(), null, null, new Aiii(this, cf, null), 3, null);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(Cf<WorkoutSession> cf) {
                a(cf);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(HeartRateDetailPresenter heartRateDetailPresenter, Date date, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = heartRateDetailPresenter;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$date, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object workoutSessionsPaging;
            HeartRateDetailPresenter heartRateDetailPresenter;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                HeartRateDetailPresenter heartRateDetailPresenter2 = this.this$0;
                WorkoutSessionRepository workoutSessionRepository = heartRateDetailPresenter2.u;
                Date date = this.$date;
                WorkoutSessionRepository workoutSessionRepository2 = this.this$0.u;
                U04 u04 = this.this$0.v;
                HeartRateDetailPresenter heartRateDetailPresenter3 = this.this$0;
                this.L$0 = il6;
                this.L$1 = heartRateDetailPresenter2;
                this.label = 1;
                workoutSessionsPaging = workoutSessionRepository.getWorkoutSessionsPaging(date, workoutSessionRepository2, u04, heartRateDetailPresenter3, this);
                if (workoutSessionsPaging == d) {
                    return d;
                }
                heartRateDetailPresenter = heartRateDetailPresenter2;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                heartRateDetailPresenter = (HeartRateDetailPresenter) this.L$1;
                workoutSessionsPaging = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            heartRateDetailPresenter.p = (Listing) workoutSessionsPaging;
            Listing listing = this.this$0.p;
            if (listing != null) {
                LiveData pagedList = listing.getPagedList();
                Jm6 jm6 = this.this$0.q;
                if (jm6 != null) {
                    pagedList.h((Km6) jm6, new Aii(this));
                    return Cd6.a;
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$sampleTransformations$1$1", f = "HeartRateDetailPresenter.kt", l = {72, 72}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<HeartRateSample>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Date date, Date date2, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$first, this.$second, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<HeartRateSample>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r8)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                    com.fossil.El7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.El7.b(r8)
                    com.fossil.Hs0 r0 = r7.p$
                    com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$Ci r1 = r7.this$0
                    com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter r1 = r1.a
                    com.portfolio.platform.data.source.HeartRateSampleRepository r1 = com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter.B(r1)
                    java.util.Date r2 = r7.$first
                    java.util.Date r3 = r7.$second
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getHeartRateSamples(r2, r3, r5, r7)
                    if (r2 != r4) goto L_0x005b
                    r0 = r4
                    goto L_0x0017
                L_0x005b:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter.Ci.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Ci(HeartRateDetailPresenter heartRateDetailPresenter) {
            this.a = heartRateDetailPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<HeartRateSample>>> a(Lc6<? extends Date, ? extends Date> lc6) {
            return Or0.c(null, 0, new Aii(this, (Date) lc6.component1(), (Date) lc6.component2(), null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Lc6) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1", f = "HeartRateDetailPresenter.kt", l = {176}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1$1", f = "HeartRateDetailPresenter.kt", l = {176}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Date>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Date> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    PortfolioApp instance = PortfolioApp.get.instance();
                    this.L$0 = il6;
                    this.label = 1;
                    Object n0 = instance.n0(this);
                    return n0 == d ? d : n0;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(HeartRateDetailPresenter heartRateDetailPresenter, Date date, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = heartRateDetailPresenter;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$date, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            HeartRateDetailPresenter heartRateDetailPresenter;
            Pair<Date, Date> L;
            Lc6 lc6;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                if (this.this$0.e == null) {
                    HeartRateDetailPresenter heartRateDetailPresenter2 = this.this$0;
                    Dv7 h = heartRateDetailPresenter2.h();
                    Aii aii = new Aii(null);
                    this.L$0 = il6;
                    this.L$1 = heartRateDetailPresenter2;
                    this.label = 1;
                    g = Eu7.g(h, aii, this);
                    if (g == d) {
                        return d;
                    }
                    heartRateDetailPresenter = heartRateDetailPresenter2;
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HeartRateDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.e);
                this.this$0.f = this.$date;
                boolean k0 = TimeUtils.k0(this.this$0.e, this.$date);
                boolean k02 = TimeUtils.k0(new Date(), this.$date);
                Boolean p0 = TimeUtils.p0(this.$date);
                Jm6 jm6 = this.this$0.q;
                Date date = this.$date;
                Wg6.b(p0, "isToday");
                jm6.j(date, k0, p0.booleanValue(), !k02);
                L = TimeUtils.L(this.$date, this.this$0.e);
                Wg6.b(L, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
                lc6 = (Lc6) this.this$0.g.e();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("HeartRateDetailPresenter", "setDate - rangeDateValue=" + lc6 + ", newRange=" + new Lc6(L.first, L.second));
                if (lc6 != null || !TimeUtils.m0((Date) lc6.getFirst(), (Date) L.first) || !TimeUtils.m0((Date) lc6.getSecond(), (Date) L.second)) {
                    this.this$0.g.l(new Lc6(L.first, L.second));
                } else {
                    this.this$0.j0();
                    this.this$0.k0();
                    HeartRateDetailPresenter heartRateDetailPresenter3 = this.this$0;
                    heartRateDetailPresenter3.h0(heartRateDetailPresenter3.f);
                }
                return Cd6.a;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
                heartRateDetailPresenter = (HeartRateDetailPresenter) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            heartRateDetailPresenter.e = (Date) g;
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("HeartRateDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.e);
            this.this$0.f = this.$date;
            boolean k03 = TimeUtils.k0(this.this$0.e, this.$date);
            boolean k022 = TimeUtils.k0(new Date(), this.$date);
            Boolean p02 = TimeUtils.p0(this.$date);
            Jm6 jm62 = this.this$0.q;
            Date date2 = this.$date;
            Wg6.b(p02, "isToday");
            jm62.j(date2, k03, p02.booleanValue(), !k022);
            L = TimeUtils.L(this.$date, this.this$0.e);
            Wg6.b(L, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            lc6 = (Lc6) this.this$0.g.e();
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            local22.d("HeartRateDetailPresenter", "setDate - rangeDateValue=" + lc6 + ", newRange=" + new Lc6(L.first, L.second));
            if (lc6 != null) {
            }
            this.this$0.g.l(new Lc6(L.first, L.second));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1", f = "HeartRateDetailPresenter.kt", l = {225}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1$summary$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super DailyHeartRateSummary>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super DailyHeartRateSummary> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    HeartRateDetailPresenter heartRateDetailPresenter = this.this$0.this$0;
                    return heartRateDetailPresenter.e0(heartRateDetailPresenter.f, this.this$0.this$0.i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(HeartRateDetailPresenter heartRateDetailPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = heartRateDetailPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Integer e;
            Resting resting;
            Integer e2;
            int i = 0;
            Object d = Yn7.d();
            int i2 = this.label;
            if (i2 == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 h = this.this$0.h();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(h, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i2 == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) g;
            if (this.this$0.k == null || (!Wg6.a(this.this$0.k, dailyHeartRateSummary))) {
                this.this$0.k = dailyHeartRateSummary;
                DailyHeartRateSummary dailyHeartRateSummary2 = this.this$0.k;
                int intValue = (dailyHeartRateSummary2 == null || (resting = dailyHeartRateSummary2.getResting()) == null || (e2 = Ao7.e(resting.getValue())) == null) ? 0 : e2.intValue();
                DailyHeartRateSummary dailyHeartRateSummary3 = this.this$0.k;
                if (!(dailyHeartRateSummary3 == null || (e = Ao7.e(dailyHeartRateSummary3.getMax())) == null)) {
                    i = e.intValue();
                }
                this.this$0.q.A5(intValue, i);
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1", f = "HeartRateDetailPresenter.kt", l = {238, 242, 251, 258}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii<T> implements Comparator<T> {
                @DexIgnore
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    return Mn7.c(Long.valueOf(t.getStartTimeId().getMillis()), Long.valueOf(t2.getStartTimeId().getMillis()));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    List list = this.this$0.this$0.l;
                    if (list == null) {
                        return null;
                    }
                    if (list.size() > 1) {
                        Lm7.r(list, new Aiii());
                    }
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$2", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $listTimeZoneChange;
            @DexIgnore
            public /* final */ /* synthetic */ List $listTodayHeartRateModel;
            @DexIgnore
            public /* final */ /* synthetic */ int $maxHR;
            @DexIgnore
            public /* final */ /* synthetic */ Hh6 $previousTimeZoneOffset;
            @DexIgnore
            public /* final */ /* synthetic */ long $startOfDay;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Fi fi, long j, Hh6 hh6, List list, List list2, int i, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
                this.$startOfDay = j;
                this.$previousTimeZoneOffset = hh6;
                this.$listTimeZoneChange = list;
                this.$listTodayHeartRateModel = list2;
                this.$maxHR = i;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, this.$startOfDay, this.$previousTimeZoneOffset, this.$listTimeZoneChange, this.$listTodayHeartRateModel, this.$maxHR, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                StringBuilder sb;
                char c;
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    List<HeartRateSample> list = this.this$0.this$0.l;
                    if (list == null) {
                        return null;
                    }
                    for (HeartRateSample heartRateSample : list) {
                        long j = (long) 60000;
                        long millis = (heartRateSample.getStartTimeId().getMillis() - this.$startOfDay) / j;
                        long millis2 = (heartRateSample.getEndTime().getMillis() - this.$startOfDay) / j;
                        long j2 = (millis2 + millis) / ((long) 2);
                        if (heartRateSample.getTimezoneOffsetInSecond() != this.$previousTimeZoneOffset.element) {
                            int hourOfDay = heartRateSample.getStartTimeId().getHourOfDay();
                            String c2 = Kl5.c(hourOfDay);
                            Hr7 hr7 = Hr7.a;
                            String format = String.format("%02d:%02d", Arrays.copyOf(new Object[]{Ao7.e(Math.abs(heartRateSample.getTimezoneOffsetInSecond() / 3600)), Ao7.e(Math.abs((heartRateSample.getTimezoneOffsetInSecond() / 60) % 60))}, 2));
                            Wg6.b(format, "java.lang.String.format(format, *args)");
                            List list2 = this.$listTimeZoneChange;
                            Integer e = Ao7.e((int) j2);
                            Lc6 lc6 = new Lc6(Ao7.e(hourOfDay), Ao7.d(((float) heartRateSample.getTimezoneOffsetInSecond()) / 3600.0f));
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append(c2);
                            if (heartRateSample.getTimezoneOffsetInSecond() >= 0) {
                                sb = new StringBuilder();
                                c = '+';
                            } else {
                                sb = new StringBuilder();
                                c = '-';
                            }
                            sb.append(c);
                            sb.append(format);
                            sb2.append(sb.toString());
                            list2.add(new Gl7(e, lc6, sb2.toString()));
                            this.$previousTimeZoneOffset.element = heartRateSample.getTimezoneOffsetInSecond();
                        }
                        if (!this.$listTodayHeartRateModel.isEmpty()) {
                            W57 w57 = (W57) Pm7.P(this.$listTodayHeartRateModel);
                            if (millis - ((long) w57.a()) > 1) {
                                this.$listTodayHeartRateModel.add(new W57(0, 0, 0, w57.a(), (int) millis, (int) j2));
                            }
                        }
                        int average = (int) heartRateSample.getAverage();
                        if (heartRateSample.getMax() == this.$maxHR) {
                            average = heartRateSample.getMax();
                        }
                        this.$listTodayHeartRateModel.add(new W57(average, heartRateSample.getMin(), heartRateSample.getMax(), (int) millis, (int) millis2, (int) j2));
                    }
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$maxHR$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super Integer>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Integer> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                int i;
                Object obj2;
                Integer e;
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    List list = this.this$0.this$0.l;
                    if (list != null) {
                        Iterator it = list.iterator();
                        if (!it.hasNext()) {
                            obj2 = null;
                        } else {
                            Object next = it.next();
                            if (it.hasNext()) {
                                Integer e2 = Ao7.e(((HeartRateSample) next).getMax());
                                while (true) {
                                    next = it.next();
                                    e2 = Ao7.e(((HeartRateSample) next).getMax());
                                    if (e2.compareTo(e2) >= 0) {
                                        e2 = e2;
                                        next = next;
                                    }
                                    if (!it.hasNext()) {
                                        break;
                                    }
                                }
                            }
                            obj2 = next;
                        }
                        HeartRateSample heartRateSample = (HeartRateSample) obj2;
                        if (!(heartRateSample == null || (e = Ao7.e(heartRateSample.getMax())) == null)) {
                            i = e.intValue();
                            return Ao7.e(i);
                        }
                    }
                    i = 0;
                    return Ao7.e(i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1$summary$1", f = "HeartRateDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Dii extends Ko7 implements Coroutine<Il6, Xe6<? super List<HeartRateSample>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Dii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Dii dii = new Dii(this.this$0, xe6);
                dii.p$ = (Il6) obj;
                throw null;
                //return dii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<HeartRateSample>> xe6) {
                throw null;
                //return ((Dii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    HeartRateDetailPresenter heartRateDetailPresenter = this.this$0.this$0;
                    return heartRateDetailPresenter.d0(heartRateDetailPresenter.f, this.this$0.this$0.j);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(HeartRateDetailPresenter heartRateDetailPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = heartRateDetailPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v144, types: [java.util.List] */
        /* JADX WARN: Type inference failed for: r0v147, types: [java.util.List] */
        /* JADX WARN: Type inference failed for: r1v56, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:109:0x03e1  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0137  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x0152  */
        /* JADX WARNING: Removed duplicated region for block: B:51:0x019f  */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x01e1  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x022f  */
        /* JADX WARNING: Unknown variable types count: 3 */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 1000
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter.Fi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1", f = "HeartRateDetailPresenter.kt", l = {143}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii<T> implements Ls0<H47<? extends List<DailyHeartRateSummary>>> {
            @DexIgnore
            public /* final */ /* synthetic */ Gi a;

            @DexIgnore
            public Aii(Gi gi) {
                this.a = gi;
            }

            @DexIgnore
            public final void a(H47<? extends List<DailyHeartRateSummary>> h47) {
                Xh5 xh5 = null;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("start - summaryTransformations -- status=");
                sb.append(h47 != null ? h47.d() : null);
                sb.append(", resource=");
                sb.append(h47 != null ? (List) h47.c() : null);
                sb.append(", status=");
                if (h47 != null) {
                    xh5 = h47.d();
                }
                sb.append(xh5);
                local.d("HeartRateDetailPresenter", sb.toString());
                if (h47 != null && h47.d() != Xh5.DATABASE_LOADING) {
                    this.a.this$0.i = (List) h47.c();
                    this.a.this$0.j0();
                }
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<DailyHeartRateSummary>> h47) {
                a(h47);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii<T> implements Ls0<H47<? extends List<HeartRateSample>>> {
            @DexIgnore
            public /* final */ /* synthetic */ Gi a;

            @DexIgnore
            public Bii(Gi gi) {
                this.a = gi;
            }

            @DexIgnore
            public final void a(H47<? extends List<HeartRateSample>> h47) {
                Xh5 xh5 = null;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("start - sampleTransformations -- status=");
                sb.append(h47 != null ? h47.d() : null);
                sb.append(", resource=");
                sb.append(h47 != null ? (List) h47.c() : null);
                sb.append(", status=");
                if (h47 != null) {
                    xh5 = h47.d();
                }
                sb.append(xh5);
                local.d("HeartRateDetailPresenter", sb.toString());
                if (h47 != null && h47.d() != Xh5.DATABASE_LOADING) {
                    this.a.this$0.j = (List) h47.c();
                    this.a.this$0.k0();
                }
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<HeartRateSample>> h47) {
                a(h47);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(HeartRateDetailPresenter heartRateDetailPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = heartRateDetailPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.this$0, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object currentUser;
            HeartRateDetailPresenter heartRateDetailPresenter;
            String value;
            MFUser.UnitGroup unitGroup;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                HeartRateDetailPresenter heartRateDetailPresenter2 = this.this$0;
                UserRepository userRepository = heartRateDetailPresenter2.t;
                this.L$0 = il6;
                this.L$1 = heartRateDetailPresenter2;
                this.label = 1;
                currentUser = userRepository.getCurrentUser(this);
                if (currentUser == d) {
                    return d;
                }
                heartRateDetailPresenter = heartRateDetailPresenter2;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                heartRateDetailPresenter = (HeartRateDetailPresenter) this.L$1;
                currentUser = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) currentUser;
            if (mFUser == null || (unitGroup = mFUser.getUnitGroup()) == null || (value = unitGroup.getDistance()) == null) {
                value = Ai5.METRIC.getValue();
            }
            Ai5 fromString = Ai5.fromString(value);
            Wg6.b(fromString, "Unit.fromString(mUserRep\u2026    ?: Unit.METRIC.value)");
            heartRateDetailPresenter.m = fromString;
            LiveData liveData = this.this$0.n;
            Jm6 jm6 = this.this$0.q;
            if (jm6 != null) {
                liveData.h((Km6) jm6, new Aii(this));
                this.this$0.o.h((LifecycleOwner) this.this$0.q, new Bii(this));
                return Cd6.a;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$summaryTransformations$1$1", f = "HeartRateDetailPresenter.kt", l = {68, 68}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<DailyHeartRateSummary>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Hi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Hi hi, Date date, Date date2, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = hi;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$first, this.$second, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<DailyHeartRateSummary>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r8)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                    com.fossil.El7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.El7.b(r8)
                    com.fossil.Hs0 r0 = r7.p$
                    com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$Hi r1 = r7.this$0
                    com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter r1 = r1.a
                    com.portfolio.platform.data.source.HeartRateSummaryRepository r1 = com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter.G(r1)
                    java.util.Date r2 = r7.$first
                    java.util.Date r3 = r7.$second
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getHeartRateSummaries(r2, r3, r5, r7)
                    if (r2 != r4) goto L_0x005b
                    r0 = r4
                    goto L_0x0017
                L_0x005b:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter.Hi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Hi(HeartRateDetailPresenter heartRateDetailPresenter) {
            this.a = heartRateDetailPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<DailyHeartRateSummary>>> a(Lc6<? extends Date, ? extends Date> lc6) {
            return Or0.c(null, 0, new Aii(this, (Date) lc6.component1(), (Date) lc6.component2(), null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Lc6) obj);
        }
    }

    @DexIgnore
    public HeartRateDetailPresenter(Jm6 jm6, HeartRateSummaryRepository heartRateSummaryRepository, HeartRateSampleRepository heartRateSampleRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FileRepository fileRepository, U04 u04) {
        Wg6.c(jm6, "mView");
        Wg6.c(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        Wg6.c(heartRateSampleRepository, "mHeartRateSampleRepository");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(workoutSessionRepository, "mWorkoutSessionRepository");
        Wg6.c(fileRepository, "mFileRepository");
        Wg6.c(u04, "appExecutors");
        this.q = jm6;
        this.r = heartRateSummaryRepository;
        this.s = heartRateSampleRepository;
        this.t = userRepository;
        this.u = workoutSessionRepository;
        this.v = u04;
        this.h = new WorkoutTetherScreenShotManager(this.u, fileRepository);
        this.i = new ArrayList();
        this.j = new ArrayList();
        this.m = Ai5.METRIC;
        LiveData<H47<List<DailyHeartRateSummary>>> c = Ss0.c(this.g, new Hi(this));
        Wg6.b(c, "Transformations.switchMa\u2026t, second, true)) }\n    }");
        this.n = c;
        LiveData<H47<List<HeartRateSample>>> c2 = Ss0.c(this.g, new Ci(this));
        Wg6.b(c2, "Transformations.switchMa\u2026st, second, true))}\n    }");
        this.o = c2;
    }

    @DexIgnore
    public final List<HeartRateSample> d0(Date date, List<HeartRateSample> list) {
        Ts7 z;
        Ts7 h2;
        if (list == null || (z = Pm7.z(list)) == null || (h2 = At7.h(z, new Ai(date))) == null) {
            return null;
        }
        return At7.u(h2);
    }

    @DexIgnore
    @Override // com.mapped.PagingRequestHelper.Ai
    public void e(PagingRequestHelper.Gi gi) {
        Wg6.c(gi, "report");
    }

    @DexIgnore
    public final DailyHeartRateSummary e0(Date date, List<DailyHeartRateSummary> list) {
        T t2;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (TimeUtils.m0(next.getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return t2;
    }

    @DexIgnore
    public WorkoutTetherScreenShotManager f0() {
        return this.h;
    }

    @DexIgnore
    public final String g0(Float f2) {
        if (f2 == null) {
            return "";
        }
        Hr7 hr7 = Hr7.a;
        String format = String.format("%02d:%02d", Arrays.copyOf(new Object[]{Integer.valueOf((int) Math.abs(f2.floatValue())), Integer.valueOf(((int) Math.abs(f2.floatValue() - (f2.floatValue() % ((float) 10)))) * 60)}, 2));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        if (f2.floatValue() >= ((float) 0)) {
            return '+' + format;
        }
        return '-' + format;
    }

    @DexIgnore
    public final void h0(Date date) {
        q();
        Rm6 unused = Gu7.d(k(), null, null, new Bi(this, date, null), 3, null);
    }

    @DexIgnore
    public void i0() {
        this.q.M5(this);
    }

    @DexIgnore
    public final Rm6 j0() {
        return Gu7.d(k(), null, null, new Ei(this, null), 3, null);
    }

    @DexIgnore
    public final Rm6 k0() {
        return Gu7.d(k(), null, null, new Fi(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Rm6 unused = Gu7.d(k(), null, null, new Gi(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", "stop");
        LiveData<H47<List<HeartRateSample>>> liveData = this.o;
        Jm6 jm6 = this.q;
        if (jm6 != null) {
            liveData.n((Km6) jm6);
            this.n.n((LifecycleOwner) this.q);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
    }

    @DexIgnore
    @Override // com.fossil.Im6
    public Ai5 n() {
        return this.m;
    }

    @DexIgnore
    @Override // com.fossil.Im6
    public /* bridge */ /* synthetic */ Fd o() {
        return f0();
    }

    @DexIgnore
    @Override // com.fossil.Im6
    public void p(Date date) {
        Wg6.c(date, "date");
        h0(date);
    }

    @DexIgnore
    @Override // com.fossil.Im6
    public void q() {
        LiveData<Cf<WorkoutSession>> pagedList;
        try {
            Listing<WorkoutSession> listing = this.p;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                Jm6 jm6 = this.q;
                if (jm6 != null) {
                    pagedList.n((Km6) jm6);
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
                }
            }
            this.u.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(Cd6.a);
            local.e("HeartRateDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.Im6
    public void r(Bundle bundle) {
        Wg6.c(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.f.getTime());
    }

    @DexIgnore
    @Override // com.fossil.Im6
    public void s(Date date) {
        Wg6.c(date, "date");
        Rm6 unused = Gu7.d(k(), null, null, new Di(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Im6
    public void t() {
        Date O = TimeUtils.O(this.f);
        Wg6.b(O, "DateHelper.getNextDate(mDate)");
        s(O);
    }

    @DexIgnore
    @Override // com.fossil.Im6
    public void u() {
        Date P = TimeUtils.P(this.f);
        Wg6.b(P, "DateHelper.getPrevDate(mDate)");
        s(P);
    }
}
