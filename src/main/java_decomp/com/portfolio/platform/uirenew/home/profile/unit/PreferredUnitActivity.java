package com.portfolio.platform.uirenew.home.profile.unit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.Wt6;
import com.fossil.Yt6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PreferredUnitActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public PreferredUnitPresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            Wg6.c(context, "context");
            Intent intent = new Intent(context, PreferredUnitActivity.class);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        Wt6 wt6 = (Wt6) getSupportFragmentManager().Y(2131362158);
        if (wt6 == null) {
            wt6 = Wt6.x.b();
            i(wt6, 2131362158);
        }
        PortfolioApp.get.instance().getIface().x1(new Yt6(wt6)).a(this);
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onResume() {
        super.onResume();
        l(false);
    }
}
