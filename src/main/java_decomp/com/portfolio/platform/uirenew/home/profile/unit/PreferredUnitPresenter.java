package com.portfolio.platform.uirenew.home.profile.unit;

import com.fossil.Ai5;
import com.fossil.Bw7;
import com.fossil.Du6;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu6;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ij5;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PreferredUnitPresenter extends Du6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public MFUser e;
    @DexIgnore
    public /* final */ Eu6 f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ UpdateUser h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ei<UpdateUser.Ci, UpdateUser.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ PreferredUnitPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ Ai5 b;

        @DexIgnore
        public Ai(PreferredUnitPresenter preferredUnitPresenter, Ai5 ai5) {
            this.a = preferredUnitPresenter;
            this.b = ai5;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(UpdateUser.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(UpdateUser.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.f.a();
            this.a.f.o(bi.a(), "");
        }

        @DexIgnore
        public void c(UpdateUser.Ci ci) {
            UserDisplayUnit a2;
            Wg6.c(ci, "responseValue");
            this.a.f.a();
            this.a.f.F3(this.b);
            MFUser mFUser = this.a.e;
            if (mFUser != null && (a2 = Ij5.a(mFUser)) != null) {
                PortfolioApp.get.instance().y1(a2, PortfolioApp.get.instance().J());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(UpdateUser.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ei<UpdateUser.Ci, UpdateUser.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ PreferredUnitPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ Ai5 b;

        @DexIgnore
        public Bi(PreferredUnitPresenter preferredUnitPresenter, Ai5 ai5) {
            this.a = preferredUnitPresenter;
            this.b = ai5;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(UpdateUser.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(UpdateUser.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.f.a();
            this.a.f.o(bi.a(), "");
        }

        @DexIgnore
        public void c(UpdateUser.Ci ci) {
            Wg6.c(ci, "responseValue");
            this.a.f.a();
            this.a.f.A2(this.b);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(UpdateUser.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Ei<UpdateUser.Ci, UpdateUser.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ PreferredUnitPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ Ai5 b;

        @DexIgnore
        public Ci(PreferredUnitPresenter preferredUnitPresenter, Ai5 ai5) {
            this.a = preferredUnitPresenter;
            this.b = ai5;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(UpdateUser.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(UpdateUser.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.f.a();
            this.a.f.o(bi.a(), "");
        }

        @DexIgnore
        public void c(UpdateUser.Ci ci) {
            UserDisplayUnit a2;
            Wg6.c(ci, "responseValue");
            this.a.f.a();
            this.a.f.m1(this.b);
            MFUser mFUser = this.a.e;
            if (mFUser != null && (a2 = Ij5.a(mFUser)) != null) {
                PortfolioApp.get.instance().y1(a2, PortfolioApp.get.instance().J());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(UpdateUser.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Ei<UpdateUser.Ci, UpdateUser.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ PreferredUnitPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ Ai5 b;

        @DexIgnore
        public Di(PreferredUnitPresenter preferredUnitPresenter, Ai5 ai5) {
            this.a = preferredUnitPresenter;
            this.b = ai5;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(UpdateUser.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(UpdateUser.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.f.a();
            this.a.f.o(bi.a(), "");
        }

        @DexIgnore
        public void c(UpdateUser.Ci ci) {
            Wg6.c(ci, "responseValue");
            this.a.f.a();
            this.a.f.R2(this.b);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(UpdateUser.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitPresenter$start$1", f = "PreferredUnitPresenter.kt", l = {30}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PreferredUnitPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitPresenter$start$1$1", f = "PreferredUnitPresenter.kt", l = {30}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.g;
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(PreferredUnitPresenter preferredUnitPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = preferredUnitPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            PreferredUnitPresenter preferredUnitPresenter;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                PreferredUnitPresenter preferredUnitPresenter2 = this.this$0;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.L$1 = preferredUnitPresenter2;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
                preferredUnitPresenter = preferredUnitPresenter2;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                preferredUnitPresenter = (PreferredUnitPresenter) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            preferredUnitPresenter.e = (MFUser) g;
            Eu6 eu6 = this.this$0.f;
            MFUser mFUser = this.this$0.e;
            if (mFUser != null) {
                MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
                if (unitGroup != null) {
                    String height = unitGroup.getHeight();
                    if (height == null) {
                        height = Ai5.METRIC.getValue();
                    }
                    eu6.A2(Ai5.fromString(height));
                    Eu6 eu62 = this.this$0.f;
                    MFUser mFUser2 = this.this$0.e;
                    if (mFUser2 != null) {
                        MFUser.UnitGroup unitGroup2 = mFUser2.getUnitGroup();
                        if (unitGroup2 != null) {
                            String weight = unitGroup2.getWeight();
                            if (weight == null) {
                                weight = Ai5.METRIC.getValue();
                            }
                            eu62.R2(Ai5.fromString(weight));
                            Eu6 eu63 = this.this$0.f;
                            MFUser mFUser3 = this.this$0.e;
                            if (mFUser3 != null) {
                                MFUser.UnitGroup unitGroup3 = mFUser3.getUnitGroup();
                                if (unitGroup3 != null) {
                                    String distance = unitGroup3.getDistance();
                                    if (distance == null) {
                                        distance = Ai5.METRIC.getValue();
                                    }
                                    eu63.F3(Ai5.fromString(distance));
                                    Eu6 eu64 = this.this$0.f;
                                    MFUser mFUser4 = this.this$0.e;
                                    if (mFUser4 != null) {
                                        MFUser.UnitGroup unitGroup4 = mFUser4.getUnitGroup();
                                        if (unitGroup4 != null) {
                                            String temperature = unitGroup4.getTemperature();
                                            if (temperature == null) {
                                                temperature = Ai5.METRIC.getValue();
                                            }
                                            eu64.m1(Ai5.fromString(temperature));
                                            return Cd6.a;
                                        }
                                        Wg6.i();
                                        throw null;
                                    }
                                    Wg6.i();
                                    throw null;
                                }
                                Wg6.i();
                                throw null;
                            }
                            Wg6.i();
                            throw null;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    /*
    static {
        String simpleName = PreferredUnitPresenter.class.getSimpleName();
        Wg6.b(simpleName, "PreferredUnitPresenter::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public PreferredUnitPresenter(Eu6 eu6, UserRepository userRepository, UpdateUser updateUser) {
        Wg6.c(eu6, "mView");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(updateUser, "mUpdateUser");
        this.f = eu6;
        this.g = userRepository;
        this.h = updateUser;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Rm6 unused = Gu7.d(k(), null, null, new Ei(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(i, "presenter stop");
    }

    @DexIgnore
    @Override // com.fossil.Du6
    public void n(Ai5 ai5) {
        Wg6.c(ai5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setDistanceUnit() called with: unit = [" + ai5 + ']');
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local2.d(str2, "setDistanceUnit: unit = " + ai5);
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save distance unit with null user");
        } else if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ai5.getValue();
                Wg6.b(value, "unit.value");
                unitGroup.setDistance(value);
                this.f.b();
                UpdateUser updateUser = this.h;
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    updateUser.e(new UpdateUser.Ai(mFUser2), new Ai(this, ai5));
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Du6
    public void o(Ai5 ai5) {
        Wg6.c(ai5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setHeightUnit() called with: unit = [" + ai5 + ']');
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save height with null user");
        } else if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ai5.getValue();
                Wg6.b(value, "unit.value");
                unitGroup.setHeight(value);
                this.f.b();
                UpdateUser updateUser = this.h;
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    updateUser.e(new UpdateUser.Ai(mFUser2), new Bi(this, ai5));
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Du6
    public void p(Ai5 ai5) {
        Wg6.c(ai5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setTemperatureUnit() called with: unit = [" + ai5 + ']');
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local2.d(str2, "setTemperatureUnit: unit = " + ai5);
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save temperature unit with null user");
        } else if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ai5.getValue();
                Wg6.b(value, "unit.value");
                unitGroup.setTemperature(value);
                this.f.b();
                UpdateUser updateUser = this.h;
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    updateUser.e(new UpdateUser.Ai(mFUser2), new Ci(this, ai5));
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Du6
    public void q(Ai5 ai5) {
        Wg6.c(ai5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setWeightUnit() called with: unit = [" + ai5 + ']');
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save weight with null user");
        } else if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ai5.getValue();
                Wg6.b(value, "unit.value");
                unitGroup.setWeight(value);
                this.f.b();
                UpdateUser updateUser = this.h;
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    updateUser.e(new UpdateUser.Ai(mFUser2), new Di(this, ai5));
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public void v() {
        this.f.M5(this);
    }
}
