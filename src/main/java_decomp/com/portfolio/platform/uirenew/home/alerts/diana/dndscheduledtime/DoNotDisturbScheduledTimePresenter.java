package com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime;

import com.facebook.share.internal.VideoUploader;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Q36;
import com.fossil.R36;
import com.fossil.Um5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DoNotDisturbScheduledTimePresenter extends Q36 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public DNDScheduledTimeModel g;
    @DexIgnore
    public DNDScheduledTimeModel h;
    @DexIgnore
    public /* final */ R36 i;
    @DexIgnore
    public /* final */ DNDSettingsDatabase j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$save$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {98}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DoNotDisturbScheduledTimePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$save$1$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ DNDScheduledTimeModel b;
                @DexIgnore
                public /* final */ /* synthetic */ Aii c;

                @DexIgnore
                public Aiii(DNDScheduledTimeModel dNDScheduledTimeModel, Aii aii) {
                    this.b = dNDScheduledTimeModel;
                    this.c = aii;
                }

                @DexIgnore
                public final void run() {
                    this.c.this$0.this$0.j.getDNDScheduledTimeDao().insertDNDScheduledTime(this.b);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Biii implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ DNDScheduledTimeModel b;
                @DexIgnore
                public /* final */ /* synthetic */ Aii c;

                @DexIgnore
                public Biii(DNDScheduledTimeModel dNDScheduledTimeModel, Aii aii) {
                    this.b = dNDScheduledTimeModel;
                    this.c = aii;
                }

                @DexIgnore
                public final void run() {
                    this.c.this$0.this$0.j.getDNDScheduledTimeDao().insertDNDScheduledTime(this.b);
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    if (this.this$0.this$0.f == 0) {
                        DNDScheduledTimeModel dNDScheduledTimeModel = this.this$0.this$0.g;
                        if (dNDScheduledTimeModel == null) {
                            return null;
                        }
                        dNDScheduledTimeModel.setMinutes(this.this$0.this$0.e);
                        this.this$0.this$0.j.runInTransaction(new Aiii(dNDScheduledTimeModel, this));
                        return Cd6.a;
                    }
                    DNDScheduledTimeModel dNDScheduledTimeModel2 = this.this$0.this$0.h;
                    if (dNDScheduledTimeModel2 == null) {
                        return null;
                    }
                    dNDScheduledTimeModel2.setMinutes(this.this$0.this$0.e);
                    this.this$0.this$0.j.runInTransaction(new Biii(dNDScheduledTimeModel2, this));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = doNotDisturbScheduledTimePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(i2, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.i.close();
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$start$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {35}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DoNotDisturbScheduledTimePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$start$1$listDNDScheduledTimeModel$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends DNDScheduledTimeModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends DNDScheduledTimeModel>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.j.getDNDScheduledTimeDao().getListDNDScheduledTimeModel();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = doNotDisturbScheduledTimePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(i2, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            for (DNDScheduledTimeModel dNDScheduledTimeModel : (List) g) {
                if (dNDScheduledTimeModel.getScheduledTimeType() == this.this$0.f) {
                    R36 r36 = this.this$0.i;
                    DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter = this.this$0;
                    r36.t(doNotDisturbScheduledTimePresenter.A(doNotDisturbScheduledTimePresenter.f));
                    this.this$0.i.E(dNDScheduledTimeModel.getMinutes());
                }
                if (dNDScheduledTimeModel.getScheduledTimeType() == 0) {
                    this.this$0.g = dNDScheduledTimeModel;
                } else {
                    this.this$0.h = dNDScheduledTimeModel;
                }
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = DoNotDisturbScheduledTimePresenter.class.getSimpleName();
        Wg6.b(simpleName, "DoNotDisturbScheduledTim\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public DoNotDisturbScheduledTimePresenter(R36 r36, DNDSettingsDatabase dNDSettingsDatabase) {
        Wg6.c(r36, "mView");
        Wg6.c(dNDSettingsDatabase, "mDNDSettingsDatabase");
        this.i = r36;
        this.j = dNDSettingsDatabase;
    }

    @DexIgnore
    public final String A(int i2) {
        if (i2 == 0) {
            String c = Um5.c(PortfolioApp.get.instance(), 2131886122);
            Wg6.b(c, "LanguageHelper.getString\u2026eAlerts_Main_Text__Start)");
            return c;
        }
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886120);
        Wg6.b(c2, "LanguageHelper.getString\u2026oveAlerts_Main_Text__End)");
        return c2;
    }

    @DexIgnore
    public void B() {
        this.i.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(k, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Rm6 unused = Gu7.d(k(), null, null, new Bi(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    @Override // com.fossil.Q36
    public void n() {
        Rm6 unused = Gu7.d(k(), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Q36
    public void o(int i2) {
        this.f = i2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x005f  */
    @Override // com.fossil.Q36
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void p(java.lang.String r9, java.lang.String r10, boolean r11) {
        /*
            r8 = this;
            r1 = 0
            r3 = 12
            java.lang.String r0 = "hourValue"
            com.mapped.Wg6.c(r9, r0)
            java.lang.String r0 = "minuteValue"
            com.mapped.Wg6.c(r10, r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter.k
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "updateTime: hourValue = "
            r4.append(r5)
            r4.append(r9)
            java.lang.String r5 = ", minuteValue = "
            r4.append(r5)
            r4.append(r10)
            java.lang.String r5 = ", isPM = "
            r4.append(r5)
            r4.append(r11)
            java.lang.String r4 = r4.toString()
            r0.d(r2, r4)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x006a }
            java.lang.String r2 = "Integer.valueOf(hourValue)"
            com.mapped.Wg6.b(r0, r2)     // Catch:{ Exception -> 0x006a }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x006a }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x00a4 }
            java.lang.String r4 = "Integer.valueOf(minuteValue)"
            com.mapped.Wg6.b(r2, r4)     // Catch:{ Exception -> 0x00a4 }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x00a4 }
        L_0x0053:
            com.portfolio.platform.PortfolioApp$inner r4 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r4 = r4.instance()
            boolean r4 = android.text.format.DateFormat.is24HourFormat(r4)
            if (r4 == 0) goto L_0x0091
            if (r11 == 0) goto L_0x008d
            if (r0 != r3) goto L_0x008a
            r1 = r3
        L_0x0064:
            int r0 = r1 * 60
            int r0 = r0 + r2
            r8.e = r0
        L_0x0069:
            return
        L_0x006a:
            r2 = move-exception
            r0 = r1
        L_0x006c:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter.k
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Exception when parse time e="
            r6.append(r7)
            r6.append(r2)
            java.lang.String r2 = r6.toString()
            r4.e(r5, r2)
            r2 = r1
            goto L_0x0053
        L_0x008a:
            int r1 = r0 + 12
            goto L_0x0064
        L_0x008d:
            if (r0 == r3) goto L_0x0064
            r1 = r0
            goto L_0x0064
        L_0x0091:
            if (r11 != 0) goto L_0x009d
            if (r0 != r3) goto L_0x009b
        L_0x0095:
            int r0 = r1 * 60
            int r0 = r0 + r2
            r8.e = r0
            goto L_0x0069
        L_0x009b:
            r1 = r0
            goto L_0x0095
        L_0x009d:
            if (r0 != r3) goto L_0x00a1
            r1 = r3
            goto L_0x0095
        L_0x00a1:
            int r1 = r0 + 12
            goto L_0x0095
        L_0x00a4:
            r2 = move-exception
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter.p(java.lang.String, java.lang.String, boolean):void");
    }
}
