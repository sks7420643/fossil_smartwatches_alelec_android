package com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search;

import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Lm7;
import com.fossil.T86;
import com.fossil.U86;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.helper.AppHelper;
import java.util.ArrayList;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchSecondTimezonePresenter extends T86 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public SecondTimezoneSetting e;
    @DexIgnore
    public ArrayList<SecondTimezoneSetting> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Gson g; // = new Gson();
    @DexIgnore
    public /* final */ U86 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1", f = "SearchSecondTimezonePresenter.kt", l = {37, 38}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SearchSecondTimezonePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$rawDataList$1", f = "SearchSecondTimezonePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super ArrayList<SecondTimezoneSetting>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super ArrayList<SecondTimezoneSetting>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return AppHelper.g.i();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$sortedDataList$1", f = "SearchSecondTimezonePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super ArrayList<SecondTimezoneSetting>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $rawDataList;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii<T> implements Comparator<SecondTimezoneSetting> {
                @DexIgnore
                public static /* final */ Aiii b; // = new Aiii();

                @DexIgnore
                public final int a(SecondTimezoneSetting secondTimezoneSetting, SecondTimezoneSetting secondTimezoneSetting2) {
                    return secondTimezoneSetting.component1().compareTo(secondTimezoneSetting2.component1());
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // java.util.Comparator
                public /* bridge */ /* synthetic */ int compare(SecondTimezoneSetting secondTimezoneSetting, SecondTimezoneSetting secondTimezoneSetting2) {
                    return a(secondTimezoneSetting, secondTimezoneSetting2);
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(ArrayList arrayList, Xe6 xe6) {
                super(2, xe6);
                this.$rawDataList = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.$rawDataList, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super ArrayList<SecondTimezoneSetting>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    Lm7.r(this.$rawDataList, Aiii.b);
                    return this.$rawDataList;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(SearchSecondTimezonePresenter searchSecondTimezonePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = searchSecondTimezonePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0063  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 0
                r5 = 2
                r4 = 1
                java.lang.Object r3 = com.fossil.Yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0065
                if (r0 == r4) goto L_0x0040
                if (r0 != r5) goto L_0x0038
                java.lang.Object r0 = r7.L$1
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
                r0 = r8
            L_0x001b:
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter r1 = r7.this$0
                java.util.ArrayList r1 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter.o(r1)
                r1.addAll(r0)
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter r0 = r7.this$0
                com.fossil.U86 r0 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter.p(r0)
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter r1 = r7.this$0
                java.util.ArrayList r1 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter.o(r1)
                r0.G2(r1)
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0037:
                return r0
            L_0x0038:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0040:
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
                r2 = r0
                r1 = r8
            L_0x0049:
                r0 = r1
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter r1 = r7.this$0
                com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter.n(r1)
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$Ai$Bii r4 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$Ai$Bii
                r4.<init>(r0, r6)
                r7.L$0 = r2
                r7.L$1 = r0
                r7.label = r5
                java.lang.Object r0 = com.fossil.Eu7.g(r1, r4, r7)
                if (r0 != r3) goto L_0x001b
                r0 = r3
                goto L_0x0037
            L_0x0065:
                com.fossil.El7.b(r8)
                com.mapped.Il6 r0 = r7.p$
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter r1 = r7.this$0
                com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter.n(r1)
                com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$Ai$Aii r2 = new com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$Ai$Aii
                r2.<init>(r6)
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r7)
                if (r1 != r3) goto L_0x0081
                r0 = r3
                goto L_0x0037
            L_0x0081:
                r2 = r0
                goto L_0x0049
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = SearchSecondTimezonePresenter.class.getSimpleName();
        Wg6.b(simpleName, "SearchSecondTimezonePres\u2026er::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public SearchSecondTimezonePresenter(U86 u86) {
        Wg6.c(u86, "mView");
        this.h = u86;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        if (this.f.isEmpty()) {
            Rm6 unused = Gu7.d(k(), null, null, new Ai(this, null), 3, null);
        } else {
            this.h.G2(this.f);
        }
        SecondTimezoneSetting secondTimezoneSetting = this.e;
        if (secondTimezoneSetting != null) {
            this.h.v5(secondTimezoneSetting.getTimeZoneName());
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
    }

    @DexIgnore
    public void q(String str) {
        Wg6.c(str, MicroAppSetting.SETTING);
        try {
            this.e = (SecondTimezoneSetting) this.g.k(str, SecondTimezoneSetting.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local.d(str2, "exception when parse second timezone setting " + e2);
        }
    }

    @DexIgnore
    public void r() {
        this.h.M5(this);
    }
}
