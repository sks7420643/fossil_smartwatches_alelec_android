package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.B36;
import com.fossil.Z26;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationWatchRemindersActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public NotificationWatchRemindersPresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment) {
            Wg6.c(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationWatchRemindersActivity.class);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }
    }

    /*
    static {
        Wg6.b(NotificationWatchRemindersActivity.class.getSimpleName(), "NotificationWatchReminde\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        Z26 z26 = (Z26) getSupportFragmentManager().Y(2131362158);
        if (z26 == null) {
            z26 = Z26.t.b();
            k(z26, Z26.t.a(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (z26 != null) {
            iface.p0(new B36(z26)).a(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersContract.View");
    }
}
