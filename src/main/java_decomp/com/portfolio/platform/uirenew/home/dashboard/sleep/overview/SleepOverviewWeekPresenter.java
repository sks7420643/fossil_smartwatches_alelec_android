package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.H47;
import com.fossil.Hm7;
import com.fossil.Hs0;
import com.fossil.Ik6;
import com.fossil.Jk6;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Or0;
import com.fossil.Ss0;
import com.fossil.Xh5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.SleepOverviewWeekFragment;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.FitnessHelper;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepOverviewWeekPresenter extends Ik6 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE g;
    @DexIgnore
    public /* final */ LiveData<H47<List<MFSleepDay>>> h;
    @DexIgnore
    public BarChart.c i;
    @DexIgnore
    public /* final */ Jk6 j;
    @DexIgnore
    public /* final */ UserRepository k;
    @DexIgnore
    public /* final */ SleepSummariesRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter", f = "SleepOverviewWeekPresenter.kt", l = {155}, m = "calculateStartAndEndDate")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewWeekPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(SleepOverviewWeekPresenter sleepOverviewWeekPresenter, Xe6 xe6) {
            super(xe6);
            this.this$0 = sleepOverviewWeekPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.x(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewWeekPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$mSleepSummaries$1$1", f = "SleepOverviewWeekPresenter.kt", l = {44, 48, 48}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<MFSleepDay>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$mSleepSummaries$1$1$startAndEnd$1", f = "SleepOverviewWeekPresenter.kt", l = {44}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends Date, ? extends Date>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends Date, ? extends Date>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Aii aii = this.this$0;
                        SleepOverviewWeekPresenter sleepOverviewWeekPresenter = aii.this$0.a;
                        Date date = aii.$it;
                        Wg6.b(date, "it");
                        this.L$0 = il6;
                        this.label = 1;
                        Object x = sleepOverviewWeekPresenter.x(date, this);
                        return x == d ? d : x;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Date date, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$it, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<MFSleepDay>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x005e  */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x00b7  */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00da  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r13) {
                /*
                // Method dump skipped, instructions count: 227
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter.Bi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Bi(SleepOverviewWeekPresenter sleepOverviewWeekPresenter) {
            this.a = sleepOverviewWeekPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<MFSleepDay>>> a(Date date) {
            return Or0.c(null, 0, new Aii(this, date, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<H47<? extends List<MFSleepDay>>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewWeekPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$start$1$1", f = "SleepOverviewWeekPresenter.kt", l = {71}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$start$1$1$1", f = "SleepOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super BarChart.c>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super BarChart.c> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        SleepOverviewWeekPresenter sleepOverviewWeekPresenter = this.this$0.this$0.a;
                        Date date = sleepOverviewWeekPresenter.e;
                        if (date != null) {
                            H47 h47 = (H47) this.this$0.this$0.a.h.e();
                            return sleepOverviewWeekPresenter.A(date, h47 != null ? (List) h47.c() : null);
                        }
                        Wg6.i();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                SleepOverviewWeekPresenter sleepOverviewWeekPresenter;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    SleepOverviewWeekPresenter sleepOverviewWeekPresenter2 = this.this$0.a;
                    Dv7 h = sleepOverviewWeekPresenter2.h();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.L$1 = sleepOverviewWeekPresenter2;
                    this.label = 1;
                    g = Eu7.g(h, aiii, this);
                    if (g == d) {
                        return d;
                    }
                    sleepOverviewWeekPresenter = sleepOverviewWeekPresenter2;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    sleepOverviewWeekPresenter = (SleepOverviewWeekPresenter) this.L$1;
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                sleepOverviewWeekPresenter.i = (BarChart.c) g;
                this.this$0.a.j.p(this.this$0.a.i);
                return Cd6.a;
            }
        }

        @DexIgnore
        public Ci(SleepOverviewWeekPresenter sleepOverviewWeekPresenter) {
            this.a = sleepOverviewWeekPresenter;
        }

        @DexIgnore
        public final void a(H47<? extends List<MFSleepDay>> h47) {
            Xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mSleepSummaries -- sleepSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("SleepOverviewWeekPresenter", sb.toString());
            if (a2 != Xh5.DATABASE_LOADING) {
                Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<MFSleepDay>> h47) {
            a(h47);
        }
    }

    @DexIgnore
    public SleepOverviewWeekPresenter(Jk6 jk6, UserRepository userRepository, SleepSummariesRepository sleepSummariesRepository, PortfolioApp portfolioApp) {
        Wg6.c(jk6, "mView");
        Wg6.c(userRepository, "userRepository");
        Wg6.c(sleepSummariesRepository, "mSummariesRepository");
        Wg6.c(portfolioApp, "mApp");
        this.j = jk6;
        this.k = userRepository;
        this.l = sleepSummariesRepository;
        this.g = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.J());
        LiveData<H47<List<MFSleepDay>>> c = Ss0.c(this.f, new Bi(this));
        Wg6.b(c, "Transformations.switchMa\u2026, false))\n        }\n    }");
        this.h = c;
        this.i = new BarChart.c(0, 0, null, 7, null);
    }

    @DexIgnore
    public final BarChart.c A(Date date, List<MFSleepDay> list) {
        T t;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("SleepOverviewWeekPresenter", sb.toString());
        BarChart.c cVar = new BarChart.c(0, 0, null, 7, null);
        Calendar instance = Calendar.getInstance(Locale.US);
        Wg6.b(instance, "calendar");
        instance.setTime(date);
        instance.add(5, -6);
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i4 <= 6) {
            Date time = instance.getTime();
            Wg6.b(time, "calendar.time");
            long time2 = time.getTime();
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (TimeUtils.m0(next.getDate(), instance.getTime())) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    i3 = FitnessHelper.c.e(t2);
                    int sleepMinutes = t2.getSleepMinutes();
                    i2 = Math.max(Math.max(i3, sleepMinutes), i2);
                    cVar.b().add(new BarChart.a(i3, Hm7.c(Hm7.c(new BarChart.b(0, null, 0, sleepMinutes, null, 23, null))), time2, i4 == 6));
                } else {
                    cVar.b().add(new BarChart.a(i3, Hm7.c(Hm7.c(new BarChart.b(0, null, 0, 0, null, 23, null))), time2, i4 == 6));
                }
            } else {
                cVar.b().add(new BarChart.a(i3, Hm7.c(Hm7.c(new BarChart.b(0, null, 0, 0, null, 23, null))), time2, i4 == 6));
            }
            instance.add(5, 1);
            i3 = i3;
            i2 = i2;
            i4++;
        }
        if (i2 > 0) {
            i3 = i2;
        } else if (i3 <= 0) {
            i3 = 480;
        }
        cVar.f(i3);
        return cVar;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        y();
        Date date = this.e;
        if (date == null || !TimeUtils.p0(date).booleanValue()) {
            Date date2 = new Date();
            this.e = date2;
            this.f.l(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepOverviewWeekPresenter", "start with date " + this.e);
        LiveData<H47<List<MFSleepDay>>> liveData = this.h;
        Jk6 jk6 = this.j;
        if (jk6 != null) {
            liveData.h((SleepOverviewWeekFragment) jk6, new Ci(this));
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekFragment");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekPresenter", "stop");
        try {
            LiveData<H47<List<MFSleepDay>>> liveData = this.h;
            Jk6 jk6 = this.j;
            if (jk6 != null) {
                liveData.n((SleepOverviewWeekFragment) jk6);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewWeekPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ik6
    public FossilDeviceSerialPatternUtil.DEVICE n() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.g;
        Wg6.b(device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object x(java.util.Date r6, com.mapped.Xe6<? super com.mapped.Lc6<? extends java.util.Date, ? extends java.util.Date>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter.Ai
            if (r0 == 0) goto L_0x0057
            r0 = r7
            com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$Ai r0 = (com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0057
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0066
            if (r0 != r4) goto L_0x005e
            java.lang.Object r0 = r2.L$2
            com.mapped.Jh6 r0 = (com.mapped.Jh6) r0
            java.lang.Object r1 = r2.L$1
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter r2 = (com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter) r2
            com.fossil.El7.b(r3)
            r2 = r3
            r4 = r0
        L_0x0031:
            r0 = r2
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x004c
            java.lang.String r0 = r0.getCreatedAt()
            if (r0 == 0) goto L_0x008a
            java.util.Date r2 = com.mapped.TimeUtils.q0(r0)
            T r0 = r4.element
            java.util.Date r0 = (java.util.Date) r0
            boolean r0 = com.mapped.TimeUtils.j0(r0, r2)
            if (r0 != 0) goto L_0x004c
            r4.element = r2
        L_0x004c:
            com.mapped.Lc6 r2 = new com.mapped.Lc6
            T r0 = r4.element
            java.util.Date r0 = (java.util.Date) r0
            r2.<init>(r0, r1)
            r0 = r2
        L_0x0056:
            return r0
        L_0x0057:
            com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$Ai r0 = new com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter$Ai
            r0.<init>(r5, r7)
            r2 = r0
            goto L_0x0014
        L_0x005e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0066:
            com.fossil.El7.b(r3)
            com.mapped.Jh6 r0 = new com.mapped.Jh6
            r0.<init>()
            r3 = 6
            java.util.Date r3 = com.mapped.TimeUtils.Q(r6, r3)
            r0.element = r3
            com.portfolio.platform.data.source.UserRepository r3 = r5.k
            r2.L$0 = r5
            r2.L$1 = r6
            r2.L$2 = r0
            r2.label = r4
            java.lang.Object r2 = r3.getCurrentUser(r2)
            if (r2 != r1) goto L_0x0087
            r0 = r1
            goto L_0x0056
        L_0x0087:
            r1 = r6
            r4 = r0
            goto L_0x0031
        L_0x008a:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewWeekPresenter.x(java.util.Date, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public void y() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekPresenter", "loadData");
    }

    @DexIgnore
    public void z() {
        this.j.M5(this);
    }
}
