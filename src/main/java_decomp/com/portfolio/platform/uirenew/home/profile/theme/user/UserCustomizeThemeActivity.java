package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.mapped.Qg6;
import com.mapped.UserCustomizeThemeFragment;
import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserCustomizeThemeActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str, boolean z) {
            Wg6.c(context, "context");
            Wg6.c(str, "id");
            Intent intent = new Intent(context, UserCustomizeThemeActivity.class);
            intent.setFlags(536870912);
            Bundle bundle = new Bundle();
            bundle.putString("THEME_ID", str);
            bundle.putBoolean("THEME_MODE_EDIT", z);
            intent.addFlags(32768);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        UserCustomizeThemeFragment userCustomizeThemeFragment = (UserCustomizeThemeFragment) getSupportFragmentManager().Y(2131362158);
        if (userCustomizeThemeFragment == null) {
            userCustomizeThemeFragment = UserCustomizeThemeFragment.m.c();
            i(userCustomizeThemeFragment, 2131362158);
        }
        Intent intent = getIntent();
        Wg6.b(intent, "intent");
        Bundle extras = intent.getExtras();
        if (extras != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putString("THEME_ID", extras.getString("THEME_ID"));
            bundle2.putBoolean("THEME_MODE_EDIT", extras.getBoolean("THEME_MODE_EDIT"));
            userCustomizeThemeFragment.setArguments(bundle2);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onResume() {
        super.onResume();
        l(false);
    }
}
