package com.portfolio.platform.uirenew.home.details.goaltracking;

import android.os.Bundle;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Ao7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.H47;
import com.fossil.Hm7;
import com.fossil.Hs0;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Or0;
import com.fossil.Ss0;
import com.fossil.U08;
import com.fossil.W08;
import com.fossil.Xh5;
import com.fossil.Yl6;
import com.fossil.Yn7;
import com.fossil.Zl6;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.mapped.Cd6;
import com.mapped.Cf;
import com.mapped.Coroutine;
import com.mapped.Gg6;
import com.mapped.GoalTrackingDetailFragment;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.PagingRequestHelper;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.SupportedFunction;
import com.mapped.TimeUtils;
import com.mapped.U04;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDetailPresenter extends Yl6 implements PagingRequestHelper.Ai {
    @DexIgnore
    public Date e;
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public MutableLiveData<Lc6<Date, Date>> g; // = new MutableLiveData<>();
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ U08 j; // = W08.b(false, 1, null);
    @DexIgnore
    public List<GoalTrackingSummary> k; // = new ArrayList();
    @DexIgnore
    public GoalTrackingSummary l;
    @DexIgnore
    public List<GoalTrackingData> m;
    @DexIgnore
    public LiveData<H47<List<GoalTrackingSummary>>> n;
    @DexIgnore
    public Listing<GoalTrackingData> o;
    @DexIgnore
    public /* final */ Zl6 p;
    @DexIgnore
    public /* final */ GoalTrackingRepository q;
    @DexIgnore
    public /* final */ U04 r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$addRecord$1", f = "GoalTrackingDetailPresenter.kt", l = {Action.Selfie.TAKE_BURST}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$addRecord$1$1", f = "GoalTrackingDetailPresenter.kt", l = {203}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ GoalTrackingData $goalTrackingData;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, GoalTrackingData goalTrackingData, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
                this.$goalTrackingData = goalTrackingData;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$goalTrackingData, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    GoalTrackingRepository goalTrackingRepository = this.this$0.this$0.q;
                    List<GoalTrackingData> i2 = Hm7.i(this.$goalTrackingData);
                    this.L$0 = il6;
                    this.label = 1;
                    if (goalTrackingRepository.insertFromDevice(i2, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(GoalTrackingDetailPresenter goalTrackingDetailPresenter, Date date, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingDetailPresenter;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, this.$date, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                String uuid = UUID.randomUUID().toString();
                Wg6.b(uuid, "UUID.randomUUID().toString()");
                Date date = this.$date;
                TimeZone timeZone = TimeZone.getDefault();
                Wg6.b(timeZone, "TimeZone.getDefault()");
                DateTime b = TimeUtils.b(date, timeZone.getRawOffset() / 1000);
                Wg6.b(b, "DateHelper.createDateTim\u2026fault().rawOffset / 1000)");
                TimeZone timeZone2 = TimeZone.getDefault();
                Wg6.b(timeZone2, "TimeZone.getDefault()");
                GoalTrackingData goalTrackingData = new GoalTrackingData(uuid, b, timeZone2.getRawOffset() / 1000, this.$date, new Date().getTime(), new Date().getTime());
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, goalTrackingData, null);
                this.L$0 = il6;
                this.L$1 = goalTrackingData;
                this.label = 1;
                if (Eu7.g(i2, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                GoalTrackingData goalTrackingData2 = (GoalTrackingData) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1", f = "GoalTrackingDetailPresenter.kt", l = {211}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingData $raw;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1$1", f = "GoalTrackingDetailPresenter.kt", l = {211}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    GoalTrackingRepository goalTrackingRepository = this.this$0.this$0.q;
                    GoalTrackingData goalTrackingData = this.this$0.$raw;
                    this.L$0 = il6;
                    this.label = 1;
                    if (goalTrackingRepository.deleteGoalTracking(goalTrackingData, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(GoalTrackingDetailPresenter goalTrackingDetailPresenter, GoalTrackingData goalTrackingData, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingDetailPresenter;
            this.$raw = goalTrackingData;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$raw, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(i2, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1", f = "GoalTrackingDetailPresenter.kt", l = {85}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii<T> implements Ls0<Cf<GoalTrackingData>> {
            @DexIgnore
            public /* final */ /* synthetic */ Ci a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Cf $dataList;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public Object L$1;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Xe6 xe6, Aiii aiii) {
                        super(2, xe6);
                        this.this$0 = aiii;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        Aiiii aiiii = new Aiiii(xe6, this.this$0);
                        aiiii.p$ = (Il6) obj;
                        throw null;
                        //return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                        throw null;
                        //return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Object invokeSuspend(Object obj) {
                        Object goalTrackingDataInDate;
                        GoalTrackingDetailPresenter goalTrackingDetailPresenter;
                        Object d = Yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            El7.b(obj);
                            Il6 il6 = this.p$;
                            GoalTrackingDetailPresenter goalTrackingDetailPresenter2 = this.this$0.this$0.a.this$0;
                            GoalTrackingRepository goalTrackingRepository = goalTrackingDetailPresenter2.q;
                            Date date = this.this$0.this$0.a.$date;
                            this.L$0 = il6;
                            this.L$1 = goalTrackingDetailPresenter2;
                            this.label = 1;
                            goalTrackingDataInDate = goalTrackingRepository.getGoalTrackingDataInDate(date, this);
                            if (goalTrackingDataInDate == d) {
                                return d;
                            }
                            goalTrackingDetailPresenter = goalTrackingDetailPresenter2;
                        } else if (i == 1) {
                            Il6 il62 = (Il6) this.L$0;
                            El7.b(obj);
                            goalTrackingDetailPresenter = (GoalTrackingDetailPresenter) this.L$1;
                            goalTrackingDataInDate = obj;
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        goalTrackingDetailPresenter.m = (List) goalTrackingDataInDate;
                        return Cd6.a;
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Cf cf, Xe6 xe6, Aii aii) {
                    super(2, xe6);
                    this.$dataList = cf;
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.$dataList, xe6, this.this$0);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:20:0x007c  */
                @Override // com.fossil.Zn7
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                    /*
                        r7 = this;
                        r1 = 2
                        r3 = 1
                        r6 = 0
                        java.lang.Object r2 = com.fossil.Yn7.d()
                        int r0 = r7.label
                        if (r0 == 0) goto L_0x007e
                        if (r0 == r3) goto L_0x005a
                        if (r0 != r1) goto L_0x0052
                        java.lang.Object r0 = r7.L$1
                        com.fossil.U08 r0 = (com.fossil.U08) r0
                        java.lang.Object r1 = r7.L$0
                        com.mapped.Il6 r1 = (com.mapped.Il6) r1
                        com.fossil.El7.b(r8)     // Catch:{ all -> 0x00a0 }
                    L_0x001a:
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$Ci$Aii r1 = r7.this$0     // Catch:{ all -> 0x00a0 }
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$Ci r1 = r1.a     // Catch:{ all -> 0x00a0 }
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter r1 = r1.this$0     // Catch:{ all -> 0x00a0 }
                        com.fossil.Zl6 r1 = com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter.L(r1)     // Catch:{ all -> 0x00a0 }
                        com.mapped.Cf r2 = r7.$dataList     // Catch:{ all -> 0x00a0 }
                        r1.n5(r2)     // Catch:{ all -> 0x00a0 }
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$Ci$Aii r1 = r7.this$0     // Catch:{ all -> 0x00a0 }
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$Ci r1 = r1.a     // Catch:{ all -> 0x00a0 }
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter r1 = r1.this$0     // Catch:{ all -> 0x00a0 }
                        boolean r1 = com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter.J(r1)     // Catch:{ all -> 0x00a0 }
                        if (r1 == 0) goto L_0x004a
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$Ci$Aii r1 = r7.this$0     // Catch:{ all -> 0x00a0 }
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$Ci r1 = r1.a     // Catch:{ all -> 0x00a0 }
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter r1 = r1.this$0     // Catch:{ all -> 0x00a0 }
                        boolean r1 = com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter.I(r1)     // Catch:{ all -> 0x00a0 }
                        if (r1 == 0) goto L_0x004a
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$Ci$Aii r1 = r7.this$0     // Catch:{ all -> 0x00a0 }
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$Ci r1 = r1.a     // Catch:{ all -> 0x00a0 }
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter r1 = r1.this$0     // Catch:{ all -> 0x00a0 }
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter.W(r1)     // Catch:{ all -> 0x00a0 }
                    L_0x004a:
                        com.mapped.Cd6 r1 = com.mapped.Cd6.a     // Catch:{ all -> 0x00a0 }
                        r0.b(r6)
                        com.mapped.Cd6 r0 = com.mapped.Cd6.a
                    L_0x0051:
                        return r0
                    L_0x0052:
                        java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                        java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                        r0.<init>(r1)
                        throw r0
                    L_0x005a:
                        java.lang.Object r0 = r7.L$1
                        com.fossil.U08 r0 = (com.fossil.U08) r0
                        java.lang.Object r1 = r7.L$0
                        com.mapped.Il6 r1 = (com.mapped.Il6) r1
                        com.fossil.El7.b(r8)
                    L_0x0065:
                        com.fossil.Dv7 r3 = com.fossil.Bw7.b()     // Catch:{ all -> 0x009b }
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$Ci$Aii$Aiii$Aiiii r4 = new com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$Ci$Aii$Aiii$Aiiii     // Catch:{ all -> 0x009b }
                        r5 = 0
                        r4.<init>(r5, r7)     // Catch:{ all -> 0x009b }
                        r7.L$0 = r1     // Catch:{ all -> 0x009b }
                        r7.L$1 = r0     // Catch:{ all -> 0x009b }
                        r1 = 2
                        r7.label = r1     // Catch:{ all -> 0x009b }
                        java.lang.Object r1 = com.fossil.Eu7.g(r3, r4, r7)     // Catch:{ all -> 0x009b }
                        if (r1 != r2) goto L_0x001a
                        r0 = r2
                        goto L_0x0051
                    L_0x007e:
                        com.fossil.El7.b(r8)
                        com.mapped.Il6 r1 = r7.p$
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$Ci$Aii r0 = r7.this$0
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$Ci r0 = r0.a
                        com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter r0 = r0.this$0
                        com.fossil.U08 r0 = com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter.G(r0)
                        r7.L$0 = r1
                        r7.L$1 = r0
                        r7.label = r3
                        java.lang.Object r3 = r0.a(r6, r7)
                        if (r3 != r2) goto L_0x0065
                        r0 = r2
                        goto L_0x0051
                    L_0x009b:
                        r1 = move-exception
                    L_0x009c:
                        r0.b(r6)
                        throw r1
                    L_0x00a0:
                        r1 = move-exception
                        goto L_0x009c
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter.Ci.Aii.Aiii.invokeSuspend(java.lang.Object):java.lang.Object");
                }
            }

            @DexIgnore
            public Aii(Ci ci) {
                this.a = ci;
            }

            @DexIgnore
            public final void a(Cf<GoalTrackingData> cf) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getGoalTrackingDataPaging observer size=");
                sb.append(cf != null ? Integer.valueOf(cf.size()) : null);
                local.d("GoalTrackingDetailPresenter", sb.toString());
                if (cf != null) {
                    this.a.this$0.i = true;
                    Rm6 unused = Gu7.d(this.a.this$0.k(), null, null, new Aiii(cf, null, this), 3, null);
                }
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(Cf<GoalTrackingData> cf) {
                a(cf);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(GoalTrackingDetailPresenter goalTrackingDetailPresenter, Date date, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingDetailPresenter;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$date, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object goalTrackingDataPaging;
            GoalTrackingDetailPresenter goalTrackingDetailPresenter;
            LiveData pagedList;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                GoalTrackingDetailPresenter goalTrackingDetailPresenter2 = this.this$0;
                GoalTrackingRepository goalTrackingRepository = goalTrackingDetailPresenter2.q;
                Date date = this.$date;
                U04 u04 = this.this$0.r;
                GoalTrackingDetailPresenter goalTrackingDetailPresenter3 = this.this$0;
                this.L$0 = il6;
                this.L$1 = goalTrackingDetailPresenter2;
                this.label = 1;
                goalTrackingDataPaging = goalTrackingRepository.getGoalTrackingDataPaging(date, u04, goalTrackingDetailPresenter3, this);
                if (goalTrackingDataPaging == d) {
                    return d;
                }
                goalTrackingDetailPresenter = goalTrackingDetailPresenter2;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                goalTrackingDetailPresenter = (GoalTrackingDetailPresenter) this.L$1;
                goalTrackingDataPaging = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            goalTrackingDetailPresenter.o = (Listing) goalTrackingDataPaging;
            Listing listing = this.this$0.o;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                Zl6 zl6 = this.this$0.p;
                if (zl6 != null) {
                    pagedList.h((GoalTrackingDetailFragment) zl6, new Aii(this));
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
                }
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1", f = "GoalTrackingDetailPresenter.kt", l = {142, 260, 167}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1$1", f = "GoalTrackingDetailPresenter.kt", l = {142}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Date>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Date> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    PortfolioApp instance = PortfolioApp.get.instance();
                    this.L$0 = il6;
                    this.label = 1;
                    Object n0 = instance.n0(this);
                    return n0 == d ? d : n0;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super GoalTrackingSummary>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Xe6 xe6, Di di) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(xe6, this.this$0);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super GoalTrackingSummary> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    GoalTrackingDetailPresenter goalTrackingDetailPresenter = this.this$0.this$0;
                    return goalTrackingDetailPresenter.X(goalTrackingDetailPresenter.f, this.this$0.this$0.k);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(GoalTrackingDetailPresenter goalTrackingDetailPresenter, Date date, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingDetailPresenter;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$date, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00bf  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r16) {
            /*
            // Method dump skipped, instructions count: 548
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter.Di.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1", f = "GoalTrackingDetailPresenter.kt", l = {260, 227, 230}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1$1$maxValue$1", f = "GoalTrackingDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Integer>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $data;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(ArrayList arrayList, Xe6 xe6) {
                super(2, xe6);
                this.$data = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$data, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Integer> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                ArrayList<ArrayList<BarChart.b>> d;
                ArrayList<BarChart.b> arrayList;
                int i = 0;
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    Iterator it = this.$data.iterator();
                    if (!it.hasNext()) {
                        obj2 = null;
                    } else {
                        Object next = it.next();
                        if (!it.hasNext()) {
                            obj2 = next;
                        } else {
                            ArrayList<BarChart.b> arrayList2 = ((BarChart.a) next).d().get(0);
                            Wg6.b(arrayList2, "it.mListOfBarPoints[0]");
                            Iterator<T> it2 = arrayList2.iterator();
                            int i2 = 0;
                            while (it2.hasNext()) {
                                i2 = Ao7.e(it2.next().e()).intValue() + i2;
                            }
                            Integer e = Ao7.e(i2);
                            while (true) {
                                next = it.next();
                                ArrayList<BarChart.b> arrayList3 = ((BarChart.a) next).d().get(0);
                                Wg6.b(arrayList3, "it.mListOfBarPoints[0]");
                                Iterator<T> it3 = arrayList3.iterator();
                                int i3 = 0;
                                while (it3.hasNext()) {
                                    i3 = Ao7.e(it3.next().e()).intValue() + i3;
                                }
                                e = Ao7.e(i3);
                                if (e.compareTo(e) >= 0) {
                                    e = e;
                                    next = next;
                                }
                                if (!it.hasNext()) {
                                    break;
                                }
                            }
                            obj2 = next;
                        }
                    }
                    BarChart.a aVar = (BarChart.a) obj2;
                    if (aVar == null || (d = aVar.d()) == null || (arrayList = d.get(0)) == null) {
                        return null;
                    }
                    Iterator<T> it4 = arrayList.iterator();
                    while (it4.hasNext()) {
                        i += Ao7.e(it4.next().e()).intValue();
                    }
                    return Ao7.e(i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Xe6 xe6, Ei ei) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(xe6, this.this$0);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return SupportedFunction.a.c(this.this$0.this$0.f, this.this$0.this$0.m);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(GoalTrackingDetailPresenter goalTrackingDetailPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingDetailPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00d3  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00f9  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0116  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x011a  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
            // Method dump skipped, instructions count: 305
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter.Ei.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<H47<? extends List<GoalTrackingSummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$1$1", f = "GoalTrackingDetailPresenter.kt", l = {118}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$1$1$summary$1", f = "GoalTrackingDetailPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super GoalTrackingSummary>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super GoalTrackingSummary> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        GoalTrackingDetailPresenter goalTrackingDetailPresenter = this.this$0.this$0.a;
                        return goalTrackingDetailPresenter.X(goalTrackingDetailPresenter.f, this.this$0.this$0.a.k);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Dv7 h = this.this$0.a.h();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    g = Eu7.g(h, aiii, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) g;
                if (this.this$0.a.l == null || (!Wg6.a(this.this$0.a.l, goalTrackingSummary))) {
                    this.this$0.a.l = goalTrackingSummary;
                    this.this$0.a.p.X0(this.this$0.a.l);
                    if (this.this$0.a.h && this.this$0.a.i) {
                        this.this$0.a.a0();
                    }
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Fi(GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
            this.a = goalTrackingDetailPresenter;
        }

        @DexIgnore
        public final void a(H47<? extends List<GoalTrackingSummary>> h47) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingDetailPresenter", "start - mGoalTrackingSummary -- goalTrackingSummary=" + h47);
            if ((h47 != null ? h47.d() : null) != Xh5.DATABASE_LOADING) {
                this.a.k = h47 != null ? (List) h47.c() : null;
                this.a.h = true;
                Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<GoalTrackingSummary>> h47) {
            a(h47);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDetailPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$summaryTransformations$1$1", f = "GoalTrackingDetailPresenter.kt", l = {60, 60}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<GoalTrackingSummary>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Gi gi, Date date, Date date2, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = gi;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$first, this.$second, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<GoalTrackingSummary>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r8)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                    com.fossil.El7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.El7.b(r8)
                    com.fossil.Hs0 r0 = r7.p$
                    com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$Gi r1 = r7.this$0
                    com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter r1 = r1.a
                    com.portfolio.platform.data.source.GoalTrackingRepository r1 = com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter.C(r1)
                    java.util.Date r2 = r7.$first
                    java.util.Date r3 = r7.$second
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getSummaries(r2, r3, r5, r7)
                    if (r2 != r4) goto L_0x005b
                    r0 = r4
                    goto L_0x0017
                L_0x005b:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter.Gi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Gi(GoalTrackingDetailPresenter goalTrackingDetailPresenter) {
            this.a = goalTrackingDetailPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<GoalTrackingSummary>>> a(Lc6<? extends Date, ? extends Date> lc6) {
            return Or0.c(null, 0, new Aii(this, (Date) lc6.component1(), (Date) lc6.component2(), null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Lc6) obj);
        }
    }

    @DexIgnore
    public GoalTrackingDetailPresenter(Zl6 zl6, GoalTrackingRepository goalTrackingRepository, U04 u04) {
        Wg6.c(zl6, "mView");
        Wg6.c(goalTrackingRepository, "mGoalTrackingRepository");
        Wg6.c(u04, "mAppExecutors");
        this.p = zl6;
        this.q = goalTrackingRepository;
        this.r = u04;
        LiveData<H47<List<GoalTrackingSummary>>> c = Ss0.c(this.g, new Gi(this));
        Wg6.b(c, "Transformations.switchMa\u2026d, true))\n        }\n    }");
        this.n = c;
    }

    @DexIgnore
    public final GoalTrackingSummary X(Date date, List<GoalTrackingSummary> list) {
        T t;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (TimeUtils.m0(next.getDate(), date)) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final void Y(Date date) {
        q();
        Rm6 unused = Gu7.d(k(), null, null, new Ci(this, date, null), 3, null);
    }

    @DexIgnore
    public void Z() {
        this.p.M5(this);
    }

    @DexIgnore
    public final Rm6 a0() {
        return Gu7.d(k(), null, null, new Ei(this, null), 3, null);
    }

    @DexIgnore
    public final void b0() {
        LiveData<H47<List<GoalTrackingSummary>>> liveData = this.n;
        Zl6 zl6 = this.p;
        if (zl6 != null) {
            liveData.h((GoalTrackingDetailFragment) zl6, new Fi(this));
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
    }

    @DexIgnore
    @Override // com.mapped.PagingRequestHelper.Ai
    public void e(PagingRequestHelper.Gi gi) {
        Wg6.c(gi, "report");
        if (gi.b()) {
            this.p.d();
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        b0();
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", "stop");
        LiveData<H47<List<GoalTrackingSummary>>> liveData = this.n;
        Zl6 zl6 = this.p;
        if (zl6 != null) {
            liveData.n((GoalTrackingDetailFragment) zl6);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
    }

    @DexIgnore
    @Override // com.fossil.Yl6
    public void n(Date date) {
        Wg6.c(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailPresenter", "addRecord date=" + date);
        Rm6 unused = Gu7.d(k(), null, null, new Ai(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Yl6
    public void o(GoalTrackingData goalTrackingData) {
        Wg6.c(goalTrackingData, OrmLiteConfigUtil.RAW_DIR_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailPresenter", "deleteRecord GoalTrackingData=" + goalTrackingData);
        Rm6 unused = Gu7.d(k(), null, null, new Bi(this, goalTrackingData, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Yl6
    public void p(Date date) {
        Wg6.c(date, "date");
        Y(date);
    }

    @DexIgnore
    @Override // com.fossil.Yl6
    public void q() {
        LiveData<Cf<GoalTrackingData>> pagedList;
        try {
            Listing<GoalTrackingData> listing = this.o;
            if (listing != null && (pagedList = listing.getPagedList()) != null) {
                Zl6 zl6 = this.p;
                if (zl6 != null) {
                    pagedList.n((GoalTrackingDetailFragment) zl6);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(Cd6.a);
            local.e("GoalTrackingDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.Yl6
    public void r() {
        Gg6<Cd6> retry;
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", "retry all failed request");
        Listing<GoalTrackingData> listing = this.o;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }

    @DexIgnore
    @Override // com.fossil.Yl6
    public void s(Bundle bundle) {
        Wg6.c(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.f.getTime());
    }

    @DexIgnore
    @Override // com.fossil.Yl6
    public void t(Date date) {
        Wg6.c(date, "date");
        Rm6 unused = Gu7.d(k(), null, null, new Di(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Yl6
    public void u() {
        Date O = TimeUtils.O(this.f);
        Wg6.b(O, "DateHelper.getNextDate(mDate)");
        t(O);
    }

    @DexIgnore
    @Override // com.fossil.Yl6
    public void v() {
        Date P = TimeUtils.P(this.f);
        Wg6.b(P, "DateHelper.getPrevDate(mDate)");
        t(P);
    }
}
