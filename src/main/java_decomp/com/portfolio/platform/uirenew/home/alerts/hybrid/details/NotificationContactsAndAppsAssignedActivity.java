package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import com.fossil.I46;
import com.fossil.J46;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationContactsAndAppsAssignedActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public NotificationContactsAndAppsAssignedPresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, int i) {
            Wg6.c(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationContactsAndAppsAssignedActivity.class);
            intent.putExtra("EXTRA_HAND_NUMBER", i);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        I46 i46 = (I46) getSupportFragmentManager().Y(2131362158);
        if (i46 == null) {
            i46 = I46.s.b();
            k(i46, I46.s.a(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (i46 != null) {
            int intExtra = getIntent().getIntExtra("EXTRA_HAND_NUMBER", 0);
            LoaderManager supportLoaderManager = getSupportLoaderManager();
            Wg6.b(supportLoaderManager, "supportLoaderManager");
            iface.m(new J46(i46, intExtra, supportLoaderManager)).a(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedContract.View");
    }
}
