package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.graphics.Color;
import androidx.lifecycle.MutableLiveData;
import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Ts0;
import com.fossil.Ws6;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeRingChartViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e; // = "#bdbdbd";
    @DexIgnore
    public MutableLiveData<Ai> a; // = new MutableLiveData<>();
    @DexIgnore
    public Ai b; // = new Ai(null, null, null, null, null, null, null, null, 255, null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public Integer e;
        @DexIgnore
        public Integer f;
        @DexIgnore
        public Integer g;
        @DexIgnore
        public Integer h;

        @DexIgnore
        public Ai() {
            this(null, null, null, null, null, null, null, null, 255, null);
        }

        @DexIgnore
        public Ai(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8) {
            this.a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
            this.e = num5;
            this.f = num6;
            this.g = num7;
            this.h = num8;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8, int i, Qg6 qg6) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2, (i & 4) != 0 ? null : num3, (i & 8) != 0 ? null : num4, (i & 16) != 0 ? null : num5, (i & 32) != 0 ? null : num6, (i & 64) != 0 ? null : num7, (i & 128) == 0 ? num8 : null);
        }

        @DexIgnore
        public static /* synthetic */ void j(Ai ai, Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8, int i, Object obj) {
            ai.i((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2, (i & 4) != 0 ? null : num3, (i & 8) != 0 ? null : num4, (i & 16) != 0 ? null : num5, (i & 32) != 0 ? null : num6, (i & 64) != 0 ? null : num7, (i & 128) != 0 ? null : num8);
        }

        @DexIgnore
        public final Integer a() {
            return this.d;
        }

        @DexIgnore
        public final Integer b() {
            return this.c;
        }

        @DexIgnore
        public final Integer c() {
            return this.b;
        }

        @DexIgnore
        public final Integer d() {
            return this.a;
        }

        @DexIgnore
        public final Integer e() {
            return this.f;
        }

        @DexIgnore
        public final Integer f() {
            return this.e;
        }

        @DexIgnore
        public final Integer g() {
            return this.h;
        }

        @DexIgnore
        public final Integer h() {
            return this.g;
        }

        @DexIgnore
        public final void i(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8) {
            this.a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
            this.e = num5;
            this.f = num6;
            this.g = num7;
            this.h = num8;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeRingChartViewModel$saveColor$1", f = "CustomizeRingChartViewModel.kt", l = {76, 77, 120}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $bigColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $biggestColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $mediumColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $smallColor;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeRingChartViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(CustomizeRingChartViewModel customizeRingChartViewModel, String str, String str2, String str3, String str4, String str5, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = customizeRingChartViewModel;
            this.$id = str;
            this.$biggestColor = str2;
            this.$bigColor = str3;
            this.$mediumColor = str4;
            this.$smallColor = str5;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$id, this.$biggestColor, this.$bigColor, this.$mediumColor, this.$smallColor, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0066  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0097  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00fa  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0135  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x015f  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x01ab  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x01f9  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x02a1  */
        /* JADX WARNING: Removed duplicated region for block: B:69:0x02a4  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r19) {
            /*
            // Method dump skipped, instructions count: 681
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeRingChartViewModel.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = CustomizeRingChartViewModel.class.getSimpleName();
        Wg6.b(simpleName, "CustomizeRingChartViewModel::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public CustomizeRingChartViewModel(ThemeRepository themeRepository) {
        Wg6.c(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void d() {
        this.a.l(this.b);
    }

    @DexIgnore
    public final MutableLiveData<Ai> e() {
        return this.a;
    }

    @DexIgnore
    public final void f(String str, String str2, String str3, String str4, String str5) {
        Wg6.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str6 = d;
        local.d(str6, "saveColor biggestColor=" + str2 + " bigColor=" + str3 + " mediumColor=" + str4 + " smallColor=" + str5);
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Bi(this, str, str2, str3, str4, str5, null), 3, null);
    }

    @DexIgnore
    public final void g() {
        String b2 = Ws6.u.b();
        int parseColor = b2 != null ? Color.parseColor(b2) : Color.parseColor(e);
        String a2 = Ws6.u.a();
        int parseColor2 = a2 != null ? Color.parseColor(a2) : Color.parseColor(e);
        String c2 = Ws6.u.c();
        int parseColor3 = c2 != null ? Color.parseColor(c2) : Color.parseColor(e);
        String d2 = Ws6.u.d();
        int parseColor4 = d2 != null ? Color.parseColor(d2) : Color.parseColor(e);
        this.b.i(Integer.valueOf(parseColor), Integer.valueOf(parseColor), Integer.valueOf(parseColor2), Integer.valueOf(parseColor2), Integer.valueOf(parseColor3), Integer.valueOf(parseColor3), Integer.valueOf(parseColor4), Integer.valueOf(parseColor4));
        d();
    }

    @DexIgnore
    public final void h(int i, int i2) {
        switch (i) {
            case 401:
                Ai.j(this.b, Integer.valueOf(i2), Integer.valueOf(i2), null, null, null, null, null, null, 252, null);
                d();
                return;
            case Action.ActivityTracker.TAG_ACTIVITY /* 402 */:
                Ai.j(this.b, null, null, Integer.valueOf(i2), Integer.valueOf(i2), null, null, null, null, 243, null);
                d();
                return;
            case MFNetworkReturnCode.WRONG_PASSWORD /* 403 */:
                Ai.j(this.b, null, null, null, null, Integer.valueOf(i2), Integer.valueOf(i2), null, null, 207, null);
                d();
                return;
            case 404:
                Ai.j(this.b, null, null, null, null, null, null, Integer.valueOf(i2), Integer.valueOf(i2), 63, null);
                d();
                return;
            default:
                return;
        }
    }
}
