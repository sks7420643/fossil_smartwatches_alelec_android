package com.portfolio.platform.uirenew.home.profile.opt;

import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gq6;
import com.fossil.Gu7;
import com.fossil.Hq6;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileOptInPresenter extends Gq6 {
    @DexIgnore
    public /* final */ String e; // = "ProfileOptInPresenter";
    @DexIgnore
    public MFUser f;
    @DexIgnore
    public /* final */ Hq6 g;
    @DexIgnore
    public /* final */ UpdateUser h;
    @DexIgnore
    public /* final */ UserRepository i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ei<UpdateUser.Ci, UpdateUser.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public Ai(ProfileOptInPresenter profileOptInPresenter, boolean z) {
            this.a = profileOptInPresenter;
            this.b = z;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(UpdateUser.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(UpdateUser.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.t().F4(!this.b);
            this.a.t().J5();
            Hq6 t = this.a.t();
            int a2 = bi.a();
            String b2 = bi.b();
            if (b2 != null) {
                t.C(a2, b2);
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public void c(UpdateUser.Ci ci) {
            Wg6.c(ci, "responseValue");
            this.a.t().J5();
            AnalyticsHelper.f.g().s(this.b);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(UpdateUser.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ei<UpdateUser.Ci, UpdateUser.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public Bi(ProfileOptInPresenter profileOptInPresenter, boolean z) {
            this.a = profileOptInPresenter;
            this.b = z;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(UpdateUser.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(UpdateUser.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.t().Y4(!this.b);
            this.a.t().J5();
            Hq6 t = this.a.t();
            int a2 = bi.a();
            String b2 = bi.b();
            if (b2 != null) {
                t.C(a2, b2);
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public void c(UpdateUser.Ci ci) {
            Wg6.c(ci, "responseValue");
            this.a.t().J5();
            AnalyticsHelper.f.g().s(this.b);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(UpdateUser.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1", f = "ProfileOptInPresenter.kt", l = {31}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileOptInPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInPresenter$start$1$1", f = "ProfileOptInPresenter.kt", l = {31}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository s = this.this$0.this$0.s();
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = s.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(ProfileOptInPresenter profileOptInPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileOptInPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            ProfileOptInPresenter profileOptInPresenter;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ProfileOptInPresenter profileOptInPresenter2 = this.this$0;
                Dv7 i2 = profileOptInPresenter2.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.L$1 = profileOptInPresenter2;
                this.label = 1;
                g = Eu7.g(i2, aii, this);
                if (g == d) {
                    return d;
                }
                profileOptInPresenter = profileOptInPresenter2;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                profileOptInPresenter = (ProfileOptInPresenter) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            profileOptInPresenter.f = (MFUser) g;
            MFUser mFUser = this.this$0.f;
            if (mFUser != null) {
                this.this$0.t().F4(mFUser.getDiagnosticEnabled());
                this.this$0.t().Y4(mFUser.getEmailOptIn());
            }
            return Cd6.a;
        }
    }

    @DexIgnore
    public ProfileOptInPresenter(Hq6 hq6, UpdateUser updateUser, UserRepository userRepository) {
        Wg6.c(hq6, "mView");
        Wg6.c(updateUser, "mUpdateUser");
        Wg6.c(userRepository, "mUserRepository");
        this.g = hq6;
        this.h = updateUser;
        this.i = userRepository;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(this.e, "presenter starts: Get user information");
        Rm6 unused = Gu7.d(k(), null, null, new Ci(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(this.e, "presenter stop");
    }

    @DexIgnore
    @Override // com.fossil.Gq6
    public void n(boolean z) {
        MFUser mFUser = this.f;
        if (mFUser != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.e;
            local.d(str, "setAnonymouslySendUsageData() called with: checked = " + z);
            mFUser.setDiagnosticEnabled(z);
            this.g.P0();
            if (this.h.e(new UpdateUser.Ai(mFUser), new Ai(this, z)) != null) {
                return;
            }
        }
        FLogger.INSTANCE.getLocal().e(this.e, "mMfUser is null");
    }

    @DexIgnore
    @Override // com.fossil.Gq6
    public void o(boolean z) {
        MFUser mFUser = this.f;
        if (mFUser != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.e;
            local.d(str, "setSubcribeEmailData() called with: checked = " + z);
            mFUser.setEmailOptIn(z);
            this.g.P0();
            if (this.h.e(new UpdateUser.Ai(mFUser), new Bi(this, z)) != null) {
                return;
            }
        }
        FLogger.INSTANCE.getLocal().e(this.e, "mMfUser is null");
    }

    @DexIgnore
    public final UserRepository s() {
        return this.i;
    }

    @DexIgnore
    public final Hq6 t() {
        return this.g;
    }

    @DexIgnore
    public void u() {
        this.g.M5(this);
    }
}
