package com.portfolio.platform.uirenew.home.dashboard.activetime;

import androidx.lifecycle.LiveData;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Pd6;
import com.fossil.Qd6;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Cf;
import com.mapped.Coroutine;
import com.mapped.DashboardActiveTimeFragment;
import com.mapped.Gg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.PagingRequestHelper;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardActiveTimePresenter extends Pd6 {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<ActivitySummary> f;
    @DexIgnore
    public /* final */ Qd6 g;
    @DexIgnore
    public /* final */ SummariesRepository h;
    @DexIgnore
    public /* final */ FitnessDataRepository i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ U04 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1", f = "DashboardActiveTimePresenter.kt", l = {58, 66}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DashboardActiveTimePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii<T> implements Ls0<Cf<ActivitySummary>> {
            @DexIgnore
            public /* final */ /* synthetic */ Qd6 a;

            @DexIgnore
            public Aii(Qd6 qd6) {
                this.a = qd6;
            }

            @DexIgnore
            public final void a(Cf<ActivitySummary> cf) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getSummariesPaging observer size=");
                sb.append(cf != null ? Integer.valueOf(cf.size()) : null);
                local.d("DashboardActiveTimePresenter", sb.toString());
                if (cf != null) {
                    this.a.q(cf);
                }
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(Cf<ActivitySummary> cf) {
                a(cf);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements PagingRequestHelper.Ai {
            @DexIgnore
            public /* final */ /* synthetic */ Ai a;

            @DexIgnore
            public Bii(Ai ai) {
                this.a = ai;
            }

            @DexIgnore
            @Override // com.mapped.PagingRequestHelper.Ai
            public final void e(PagingRequestHelper.Gi gi) {
                Wg6.c(gi, "report");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DashboardActiveTimePresenter", "onStatusChange status=" + gi);
                if (gi.b()) {
                    this.a.this$0.g.d();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1$user$1", f = "DashboardActiveTimePresenter.kt", l = {58}, m = "invokeSuspend")
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.j;
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(DashboardActiveTimePresenter dashboardActiveTimePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dashboardActiveTimePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0067  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
                r10 = this;
                r9 = 2
                r4 = 1
                java.lang.Object r8 = com.fossil.Yn7.d()
                int r0 = r10.label
                if (r0 == 0) goto L_0x00a0
                if (r0 == r4) goto L_0x0059
                if (r0 != r9) goto L_0x0051
                java.lang.Object r0 = r10.L$4
                com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter r0 = (com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter) r0
                java.lang.Object r1 = r10.L$3
                java.util.Date r1 = (java.util.Date) r1
                java.lang.Object r1 = r10.L$2
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r10.L$1
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r10.L$0
                com.mapped.Il6 r1 = (com.mapped.Il6) r1
                com.fossil.El7.b(r11)
                r1 = r11
                r2 = r0
            L_0x0027:
                r0 = r1
                com.portfolio.platform.data.Listing r0 = (com.portfolio.platform.data.Listing) r0
                com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter.x(r2, r0)
                com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter r0 = r10.this$0
                com.fossil.Qd6 r1 = com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter.w(r0)
                com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter r0 = r10.this$0
                com.portfolio.platform.data.Listing r0 = com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter.r(r0)
                if (r0 == 0) goto L_0x004e
                androidx.lifecycle.LiveData r2 = r0.getPagedList()
                if (r2 == 0) goto L_0x004e
                if (r1 == 0) goto L_0x00c0
                r0 = r1
                com.mapped.DashboardActiveTimeFragment r0 = (com.mapped.DashboardActiveTimeFragment) r0
                com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$Ai$Aii r3 = new com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$Ai$Aii
                r3.<init>(r1)
                r2.h(r0, r3)
            L_0x004e:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0050:
                return r0
            L_0x0051:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0059:
                java.lang.Object r0 = r10.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r11)
                r6 = r0
                r1 = r11
            L_0x0062:
                r5 = r1
                com.portfolio.platform.data.model.MFUser r5 = (com.portfolio.platform.data.model.MFUser) r5
                if (r5 == 0) goto L_0x004e
                java.lang.String r0 = r5.getCreatedAt()
                java.util.Date r2 = com.mapped.TimeUtils.q0(r0)
                com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter r7 = r10.this$0
                com.portfolio.platform.data.source.SummariesRepository r0 = com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter.u(r7)
                com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter r1 = r10.this$0
                com.portfolio.platform.data.source.FitnessDataRepository r1 = com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter.t(r1)
                java.lang.String r3 = "createdDate"
                com.mapped.Wg6.b(r2, r3)
                com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter r3 = r10.this$0
                com.mapped.U04 r3 = com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter.s(r3)
                com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$Ai$Bii r4 = new com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$Ai$Bii
                r4.<init>(r10)
                r10.L$0 = r6
                r10.L$1 = r5
                r10.L$2 = r5
                r10.L$3 = r2
                r10.L$4 = r7
                r10.label = r9
                r5 = r10
                java.lang.Object r1 = r0.getSummariesPaging(r1, r2, r3, r4, r5)
                if (r1 != r8) goto L_0x00bd
                r0 = r8
                goto L_0x0050
            L_0x00a0:
                com.fossil.El7.b(r11)
                com.mapped.Il6 r0 = r10.p$
                com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter r1 = r10.this$0
                com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter.q(r1)
                com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$Ai$Cii r2 = new com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$Ai$Cii
                r3 = 0
                r2.<init>(r10, r3)
                r10.L$0 = r0
                r10.label = r4
                java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r10)
                if (r1 != r8) goto L_0x00c8
                r0 = r8
                goto L_0x0050
            L_0x00bd:
                r2 = r7
                goto L_0x0027
            L_0x00c0:
                com.mapped.Rc6 r0 = new com.mapped.Rc6
                java.lang.String r1 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment"
                r0.<init>(r1)
                throw r0
            L_0x00c8:
                r6 = r0
                goto L_0x0062
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public DashboardActiveTimePresenter(Qd6 qd6, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, U04 u04) {
        Wg6.c(qd6, "mView");
        Wg6.c(summariesRepository, "mSummariesRepository");
        Wg6.c(fitnessDataRepository, "mFitnessDataRepository");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(u04, "mAppExecutors");
        this.g = qd6;
        this.h = summariesRepository;
        this.i = fitnessDataRepository;
        this.j = userRepository;
        this.k = u04;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.get.instance().J());
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Boolean p0 = TimeUtils.p0(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActiveTimePresenter", "start isDateTodayDate " + p0 + " listingPage " + this.f);
        if (!p0.booleanValue()) {
            this.e = new Date();
            Listing<ActivitySummary> listing = this.f;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimePresenter", "stop");
    }

    @DexIgnore
    @Override // com.fossil.Pd6
    public void n() {
        Rm6 unused = Gu7.d(k(), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Pd6
    public void o() {
        LiveData<Cf<ActivitySummary>> pagedList;
        try {
            Qd6 qd6 = this.g;
            Listing<ActivitySummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                if (qd6 != null) {
                    pagedList.n((DashboardActiveTimeFragment) qd6);
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment");
                }
            }
            this.h.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(Cd6.a);
            local.e("DashboardActiveTimePresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.Pd6
    public void p() {
        Gg6<Cd6> retry;
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimePresenter", "retry all failed request");
        Listing<ActivitySummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }

    @DexIgnore
    public void y() {
        this.g.M5(this);
    }
}
