package com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.L86;
import com.fossil.M86;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchRingPhoneActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public SearchRingPhonePresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            Wg6.c(fragment, "context");
            Wg6.c(str, "selectedRingtone");
            Intent intent = new Intent(fragment.getContext(), SearchRingPhoneActivity.class);
            intent.putExtra("KEY_SELECTED_RINGPHONE", str);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 104);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        Ringtone ringtone;
        super.onCreate(bundle);
        setContentView(2131558439);
        L86 l86 = (L86) getSupportFragmentManager().Y(2131362158);
        if (l86 == null) {
            l86 = L86.l.b();
            k(l86, L86.l.a(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (l86 != null) {
            iface.k0(new M86(l86)).a(this);
            Intent intent = getIntent();
            if (intent != null) {
                SearchRingPhonePresenter searchRingPhonePresenter = this.A;
                if (searchRingPhonePresenter != null) {
                    String stringExtra = intent.getStringExtra("KEY_SELECTED_RINGPHONE");
                    Wg6.b(stringExtra, "it.getStringExtra(KEY_SELECTED_RINGPHONE)");
                    searchRingPhonePresenter.w(stringExtra);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
            if (bundle != null && (ringtone = (Ringtone) bundle.getParcelable("KEY_SELECTED_RINGPHONE")) != null) {
                SearchRingPhonePresenter searchRingPhonePresenter2 = this.A;
                if (searchRingPhonePresenter2 != null) {
                    searchRingPhonePresenter2.v(ringtone);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhoneContract.View");
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.c(bundle, "outState");
        SearchRingPhonePresenter searchRingPhonePresenter = this.A;
        if (searchRingPhonePresenter != null) {
            bundle.putParcelable("KEY_SELECTED_RINGPHONE", searchRingPhonePresenter.n());
            super.onSaveInstanceState(bundle);
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }
}
