package com.portfolio.platform.uirenew.home.customize.hybrid.search;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Pm7;
import com.fossil.Qc6;
import com.fossil.Rc6;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchMicroAppPresenter extends Qc6 {
    @DexIgnore
    public String e; // = "empty";
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h;
    @DexIgnore
    public /* final */ ArrayList<MicroApp> i; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Rc6 k;
    @DexIgnore
    public /* final */ MicroAppRepository l;
    @DexIgnore
    public /* final */ An4 m;
    @DexIgnore
    public /* final */ PortfolioApp n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$search$1", f = "SearchMicroAppPresenter.kt", l = {79}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $query;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SearchMicroAppPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$search$1$1", f = "SearchMicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<MicroApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<MicroApp>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    MicroAppRepository microAppRepository = this.this$0.this$0.l;
                    Ai ai = this.this$0;
                    return Pm7.j0(microAppRepository.queryMicroAppByName(ai.$query, ai.this$0.n.J()));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(SearchMicroAppPresenter searchMicroAppPresenter, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = searchMicroAppPresenter;
            this.$query = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, this.$query, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            List arrayList;
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                arrayList = new ArrayList();
                if (this.$query.length() > 0) {
                    Dv7 i2 = this.this$0.i();
                    Aii aii = new Aii(this, null);
                    this.L$0 = il6;
                    this.L$1 = arrayList;
                    this.label = 1;
                    g = Eu7.g(i2, aii, this);
                    if (g == d) {
                        return d;
                    }
                }
                this.this$0.k.B(this.this$0.D(arrayList));
                this.this$0.h = this.$query;
                return Cd6.a;
            } else if (i == 1) {
                List list = (List) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            arrayList = (List) g;
            this.this$0.k.B(this.this$0.D(arrayList));
            this.this$0.h = this.$query;
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$start$1", f = "SearchMicroAppPresenter.kt", l = {40, 44}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SearchMicroAppPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$start$1$allMicroApps$1", f = "SearchMicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends MicroApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends MicroApp>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.l.getAllMicroApp(PortfolioApp.get.instance().J());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$start$1$allSearchedMicroApps$1", f = "SearchMicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends MicroApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends MicroApp>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    MicroAppRepository microAppRepository = this.this$0.this$0.l;
                    List<String> G = this.this$0.this$0.m.G();
                    Wg6.b(G, "sharedPreferencesManager.microAppSearchedIdsRecent");
                    return microAppRepository.getMicroAppByIds(G, this.this$0.this$0.n.J());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(SearchMicroAppPresenter searchMicroAppPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = searchMicroAppPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0074  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 0
                r5 = 2
                r4 = 1
                java.lang.Object r3 = com.fossil.Yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0076
                if (r0 == r4) goto L_0x003f
                if (r0 != r5) goto L_0x0037
                java.lang.Object r0 = r7.L$1
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
                r0 = r8
            L_0x001b:
                java.util.List r0 = (java.util.List) r0
                com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter r1 = r7.this$0
                java.util.ArrayList r1 = com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter.s(r1)
                r1.clear()
                com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter r1 = r7.this$0
                java.util.ArrayList r1 = com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter.s(r1)
                r1.addAll(r0)
                com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter r0 = r7.this$0
                com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter.x(r0)
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0036:
                return r0
            L_0x0037:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x003f:
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
                r2 = r0
                r1 = r8
            L_0x0048:
                r0 = r1
                java.util.List r0 = (java.util.List) r0
                com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter r1 = r7.this$0
                java.util.ArrayList r1 = com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter.r(r1)
                r1.clear()
                com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter r1 = r7.this$0
                java.util.ArrayList r1 = com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter.r(r1)
                r1.addAll(r0)
                com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter r1 = r7.this$0
                com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter.q(r1)
                com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$Bi$Bii r4 = new com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$Bi$Bii
                r4.<init>(r7, r6)
                r7.L$0 = r2
                r7.L$1 = r0
                r7.label = r5
                java.lang.Object r0 = com.fossil.Eu7.g(r1, r4, r7)
                if (r0 != r3) goto L_0x001b
                r0 = r3
                goto L_0x0036
            L_0x0076:
                com.fossil.El7.b(r8)
                com.mapped.Il6 r0 = r7.p$
                com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter r1 = r7.this$0
                com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter.q(r1)
                com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$Bi$Aii r2 = new com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter$Bi$Aii
                r2.<init>(r7, r6)
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r7)
                if (r1 != r3) goto L_0x0092
                r0 = r3
                goto L_0x0036
            L_0x0092:
                r2 = r0
                goto L_0x0048
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.hybrid.search.SearchMicroAppPresenter.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public SearchMicroAppPresenter(Rc6 rc6, MicroAppRepository microAppRepository, An4 an4, PortfolioApp portfolioApp) {
        Wg6.c(rc6, "mView");
        Wg6.c(microAppRepository, "mMicroAppRepository");
        Wg6.c(an4, "sharedPreferencesManager");
        Wg6.c(portfolioApp, "mApp");
        this.k = rc6;
        this.l = microAppRepository;
        this.m = an4;
        this.n = portfolioApp;
    }

    @DexIgnore
    public final void A() {
        if (this.j.isEmpty()) {
            this.k.B(D(Pm7.j0(this.i)));
        } else {
            this.k.K(D(Pm7.j0(this.j)));
        }
        if (!TextUtils.isEmpty(this.h)) {
            Rc6 rc6 = this.k;
            String str = this.h;
            if (str != null) {
                rc6.z(str);
                String str2 = this.h;
                if (str2 != null) {
                    p(str2);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public Bundle B(Bundle bundle) {
        if (bundle != null) {
            bundle.putString(ViewHierarchy.DIMENSION_TOP_KEY, this.e);
            bundle.putString("middle", this.f);
            bundle.putString("bottom", this.g);
        }
        return bundle;
    }

    @DexIgnore
    public void C() {
        this.k.M5(this);
    }

    @DexIgnore
    public final List<Lc6<MicroApp, String>> D(List<MicroApp> list) {
        ArrayList arrayList = new ArrayList();
        for (MicroApp microApp : list) {
            String id = microApp.getId();
            if (Wg6.a(id, this.e)) {
                arrayList.add(new Lc6(microApp, ViewHierarchy.DIMENSION_TOP_KEY));
            } else if (Wg6.a(id, this.f)) {
                arrayList.add(new Lc6(microApp, "middle"));
            } else if (Wg6.a(id, this.g)) {
                arrayList.add(new Lc6(microApp, "bottom"));
            } else {
                arrayList.add(new Lc6(microApp, ""));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final void E(String str, String str2, String str3) {
        Wg6.c(str, "watchAppTop");
        Wg6.c(str2, "watchAppMiddle");
        Wg6.c(str3, "watchAppBottom");
        this.e = str;
        this.g = str3;
        this.f = str2;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Rm6 unused = Gu7.d(k(), null, null, new Bi(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.Qc6
    public void n() {
        this.h = "";
        this.k.D();
        A();
    }

    @DexIgnore
    @Override // com.fossil.Qc6
    public void o(MicroApp microApp) {
        Wg6.c(microApp, "selectedMicroApp");
        List<String> G = this.m.G();
        Wg6.b(G, "sharedPreferencesManager.microAppSearchedIdsRecent");
        if (!G.contains(microApp.getId())) {
            G.add(0, microApp.getId());
            if (G.size() > 5) {
                G = G.subList(0, 5);
            }
            this.m.F1(G);
        }
        this.k.v1(microApp);
    }

    @DexIgnore
    @Override // com.fossil.Qc6
    public void p(String str) {
        Wg6.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        Rm6 unused = Gu7.d(k(), null, null, new Ai(this, str, null), 3, null);
    }
}
