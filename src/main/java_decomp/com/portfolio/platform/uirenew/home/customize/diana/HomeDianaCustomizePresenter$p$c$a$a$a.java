package com.portfolio.platform.uirenew.home.customize.diana;

import com.fossil.Ko7;
import com.fossil.Mn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeDianaCustomizePresenter$p$c$a$a$a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $it;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter.Pi.Cii.Aiii.Aiiii this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return Mn7.c(Boolean.valueOf(t2.m()), Boolean.valueOf(t.m()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Comparator b;

        @DexIgnore
        public b(Comparator comparator) {
            this.b = comparator;
        }

        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            int compare = this.b.compare(t, t2);
            return compare != 0 ? compare : Mn7.c(t.c(), t2.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements Comparator<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Comparator b;

        @DexIgnore
        public c(Comparator comparator) {
            this.b = comparator;
        }

        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            int compare = this.b.compare(t, t2);
            return compare != 0 ? compare : Mn7.c(t.e(), t2.e());
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HomeDianaCustomizePresenter$p$c$a$a$a(List list, Xe6 xe6, HomeDianaCustomizePresenter.Pi.Cii.Aiii.Aiiii aiiii) {
        super(2, xe6);
        this.$it = list;
        this.this$0 = aiiii;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        HomeDianaCustomizePresenter$p$c$a$a$a homeDianaCustomizePresenter$p$c$a$a$a = new HomeDianaCustomizePresenter$p$c$a$a$a(this.$it, xe6, this.this$0);
        homeDianaCustomizePresenter$p$c$a$a$a.p$ = (Il6) obj;
        throw null;
        //return homeDianaCustomizePresenter$p$c$a$a$a;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((HomeDianaCustomizePresenter$p$c$a$a$a) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0081 A[Catch:{ all -> 0x0185 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x013d A[Catch:{ all -> 0x0185 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x017b  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r11) {
        /*
        // Method dump skipped, instructions count: 394
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$p$c$a$a$a.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
