package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Bn6;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.H47;
import com.fossil.Hm7;
import com.fossil.Hs0;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Or0;
import com.fossil.Rj6;
import com.fossil.Sj6;
import com.fossil.Ss0;
import com.fossil.Xh5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.SleepOverviewDayFragment;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.helper.FitnessHelper;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepOverviewDayPresenter extends Rj6 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public Date f;
    @DexIgnore
    public /* final */ MutableLiveData<Date> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ LiveData<H47<List<MFSleepDay>>> j;
    @DexIgnore
    public /* final */ LiveData<H47<List<MFSleepSession>>> k;
    @DexIgnore
    public /* final */ Sj6 l;
    @DexIgnore
    public /* final */ SleepSummariesRepository m;
    @DexIgnore
    public /* final */ SleepSessionsRepository n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewDayPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$mSleepSessions$1$1", f = "SleepOverviewDayPresenter.kt", l = {52, 52}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<MFSleepSession>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Date date, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$it, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<MFSleepSession>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r6) goto L_0x0020
                    if (r0 != r7) goto L_0x0018
                    java.lang.Object r0 = r8.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r9)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r8.L$1
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                    com.fossil.El7.b(r9)
                    r2 = r9
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r1
                    r8.label = r7
                    java.lang.Object r0 = r3.a(r0, r8)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.El7.b(r9)
                    com.fossil.Hs0 r0 = r8.p$
                    com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$Ai r1 = r8.this$0
                    com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter r1 = r1.a
                    com.portfolio.platform.data.source.SleepSessionsRepository r1 = com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter.q(r1)
                    java.util.Date r2 = r8.$it
                    java.lang.String r3 = "it"
                    com.mapped.Wg6.b(r2, r3)
                    java.util.Date r3 = r8.$it
                    java.lang.String r5 = "it"
                    com.mapped.Wg6.b(r3, r5)
                    r8.L$0 = r0
                    r8.L$1 = r0
                    r8.label = r6
                    r5 = 0
                    java.lang.Object r2 = r1.getSleepSessionList(r2, r3, r5, r8)
                    if (r2 != r4) goto L_0x0066
                    r0 = r4
                    goto L_0x0017
                L_0x0066:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter.Ai.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Ai(SleepOverviewDayPresenter sleepOverviewDayPresenter) {
            this.a = sleepOverviewDayPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<MFSleepSession>>> a(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayPresenter", "mSleepSessions onDateChange " + date);
            return Or0.c(null, 0, new Aii(this, date, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewDayPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$mSleepSummaries$1$1", f = "SleepOverviewDayPresenter.kt", l = {46, 46}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<MFSleepDay>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Date date, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$it, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<MFSleepDay>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r6) goto L_0x0020
                    if (r0 != r7) goto L_0x0018
                    java.lang.Object r0 = r8.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r9)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r8.L$1
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r8.L$0
                    com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                    com.fossil.El7.b(r9)
                    r2 = r9
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r1
                    r8.label = r7
                    java.lang.Object r0 = r3.a(r0, r8)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.El7.b(r9)
                    com.fossil.Hs0 r0 = r8.p$
                    com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$Bi r1 = r8.this$0
                    com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter r1 = r1.a
                    com.portfolio.platform.data.source.SleepSummariesRepository r1 = com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter.s(r1)
                    java.util.Date r2 = r8.$it
                    java.lang.String r3 = "it"
                    com.mapped.Wg6.b(r2, r3)
                    java.util.Date r3 = r8.$it
                    java.lang.String r5 = "it"
                    com.mapped.Wg6.b(r3, r5)
                    r8.L$0 = r0
                    r8.L$1 = r0
                    r8.label = r6
                    r5 = 0
                    java.lang.Object r2 = r1.getSleepSummaries(r2, r3, r5, r8)
                    if (r2 != r4) goto L_0x0066
                    r0 = r4
                    goto L_0x0017
                L_0x0066:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter.Bi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Bi(SleepOverviewDayPresenter sleepOverviewDayPresenter) {
            this.a = sleepOverviewDayPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<MFSleepDay>>> a(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayPresenter", "mSleepSummaries onDateChange " + date);
            return Or0.c(null, 0, new Aii(this, date, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$showDetailChart$1", f = "SleepOverviewDayPresenter.kt", l = {172}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewDayPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Bn6.Bi>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $it;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(List list, Xe6 xe6, Ci ci) {
                super(2, xe6);
                this.$it = list;
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$it, xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends Bn6.Bi>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    SleepOverviewDayPresenter sleepOverviewDayPresenter = this.this$0.this$0;
                    Date date = sleepOverviewDayPresenter.f;
                    if (date != null) {
                        return sleepOverviewDayPresenter.C(date, this.$it);
                    }
                    Wg6.i();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(SleepOverviewDayPresenter sleepOverviewDayPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = sleepOverviewDayPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            List list;
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                H47 h47 = (H47) this.this$0.k.e();
                if (h47 == null || (list = (List) h47.c()) == null) {
                    this.this$0.l.S4(new ArrayList());
                    return Cd6.a;
                }
                Dv7 h = this.this$0.h();
                Aii aii = new Aii(list, null, this);
                this.L$0 = il6;
                this.L$1 = list;
                this.label = 1;
                g = Eu7.g(h, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                List list2 = (List) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.l.S4((List) g);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<H47<? extends List<MFSleepDay>>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewDayPresenter a;

        @DexIgnore
        public Di(SleepOverviewDayPresenter sleepOverviewDayPresenter) {
            this.a = sleepOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(H47<? extends List<MFSleepDay>> h47) {
            Xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mSleepSummaries -- sleepSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("SleepOverviewDayPresenter", sb.toString());
            if (a2 != Xh5.DATABASE_LOADING) {
                this.a.h = true;
                if (this.a.h && this.a.i) {
                    this.a.B();
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<MFSleepDay>> h47) {
            a(h47);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<H47<? extends List<MFSleepSession>>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewDayPresenter a;

        @DexIgnore
        public Ei(SleepOverviewDayPresenter sleepOverviewDayPresenter) {
            this.a = sleepOverviewDayPresenter;
        }

        @DexIgnore
        public final void a(H47<? extends List<MFSleepSession>> h47) {
            Xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mSleepSessions -- sleepSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            sb.append('\"');
            local.d("SleepOverviewDayPresenter", sb.toString());
            if (a2 != Xh5.DATABASE_LOADING) {
                this.a.i = true;
                if (this.a.h && this.a.i) {
                    this.a.B();
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<MFSleepSession>> h47) {
            a(h47);
        }
    }

    @DexIgnore
    public SleepOverviewDayPresenter(Sj6 sj6, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, PortfolioApp portfolioApp) {
        Wg6.c(sj6, "mView");
        Wg6.c(sleepSummariesRepository, "mSummariesRepository");
        Wg6.c(sleepSessionsRepository, "mSessionsRepository");
        Wg6.c(portfolioApp, "mApp");
        this.l = sj6;
        this.m = sleepSummariesRepository;
        this.n = sleepSessionsRepository;
        this.e = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.J());
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.g = mutableLiveData;
        LiveData<H47<List<MFSleepDay>>> c = Ss0.c(mutableLiveData, new Bi(this));
        Wg6.b(c, "Transformations.switchMa\u2026 false) )\n        }\n    }");
        this.j = c;
        LiveData<H47<List<MFSleepSession>>> c2 = Ss0.c(this.g, new Ai(this));
        Wg6.b(c2, "Transformations.switchMa\u2026false)  )\n        }\n    }");
        this.k = c2;
    }

    @DexIgnore
    public void A() {
        this.l.M5(this);
    }

    @DexIgnore
    public final Rm6 B() {
        return Gu7.d(k(), null, null, new Ci(this, null), 3, null);
    }

    @DexIgnore
    public final List<Bn6.Bi> C(Date date, List<MFSleepSession> list) {
        T t;
        List<MFSleepDay> c;
        T t2;
        ArrayList<MFSleepSession> arrayList = new ArrayList();
        for (T t3 : list) {
            if (TimeUtils.l0(Long.valueOf(date.getTime()), Long.valueOf(t3.getDate()))) {
                arrayList.add(t3);
            }
        }
        H47<List<MFSleepDay>> e2 = this.j.e();
        if (e2 == null || (c = e2.c()) == null) {
            t = null;
        } else {
            Iterator<T> it = c.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                T next = it.next();
                if (TimeUtils.m0(date, next.getDate())) {
                    t2 = next;
                    break;
                }
            }
            t = t2;
        }
        int e3 = FitnessHelper.c.e(t);
        ArrayList arrayList2 = new ArrayList();
        for (MFSleepSession mFSleepSession : arrayList) {
            BarChart.c cVar = new BarChart.c(0, 0, null, 7, null);
            ArrayList arrayList3 = new ArrayList();
            SleepDistribution realSleepStateDistInMinute = mFSleepSession.getRealSleepStateDistInMinute();
            List<WrapperSleepStateChange> sleepStateChange = mFSleepSession.getSleepStateChange();
            ArrayList arrayList4 = new ArrayList();
            int realStartTime = mFSleepSession.getRealStartTime();
            int totalMinuteBySleepDistribution = realSleepStateDistInMinute.getTotalMinuteBySleepDistribution();
            if (sleepStateChange != null) {
                for (T t4 : sleepStateChange) {
                    BarChart.b bVar = new BarChart.b(0, null, 0, 0, null, 31, null);
                    bVar.g((int) t4.index);
                    bVar.j(realStartTime);
                    bVar.i(totalMinuteBySleepDistribution);
                    int i2 = t4.state;
                    if (i2 == 0) {
                        bVar.h(BarChart.e.LOWEST);
                    } else if (i2 == 1) {
                        bVar.h(BarChart.e.DEFAULT);
                    } else if (i2 == 2) {
                        bVar.h(BarChart.e.HIGHEST);
                    }
                    arrayList4.add(bVar);
                }
            }
            arrayList3.add(arrayList4);
            cVar.b().add(new BarChart.a(e3, arrayList3.size() != 0 ? arrayList3 : Hm7.c(Hm7.c(new BarChart.b(0, null, 0, 0, null, 23, null))), 0, false, 12, null));
            cVar.f(e3);
            cVar.e(e3);
            int awake = realSleepStateDistInMinute.getAwake();
            int light = realSleepStateDistInMinute.getLight();
            int deep = realSleepStateDistInMinute.getDeep();
            if (totalMinuteBySleepDistribution > 0) {
                float f2 = (float) totalMinuteBySleepDistribution;
                float f3 = ((float) awake) / f2;
                float f4 = ((float) light) / f2;
                arrayList2.add(new Bn6.Bi(cVar, f3, f4, ((float) 1) - (f3 + f4), awake, light, deep, mFSleepSession.getTimezoneOffset()));
            }
        }
        return arrayList2;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date date = this.f;
        if (date == null || !TimeUtils.p0(date).booleanValue()) {
            this.h = false;
            this.i = false;
            Date date2 = new Date();
            this.f = date2;
            this.g.l(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepOverviewDayPresenter", "loadData - mDate=" + this.f);
        LiveData<H47<List<MFSleepDay>>> liveData = this.j;
        Sj6 sj6 = this.l;
        if (sj6 != null) {
            liveData.h((SleepOverviewDayFragment) sj6, new Di(this));
            this.k.h((LifecycleOwner) this.l, new Ei(this));
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayFragment");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayPresenter", "stop");
        try {
            LiveData<H47<List<MFSleepDay>>> liveData = this.j;
            Sj6 sj6 = this.l;
            if (sj6 != null) {
                liveData.n((SleepOverviewDayFragment) sj6);
                this.k.n((LifecycleOwner) this.l);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE z() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        Wg6.b(device, "mCurrentDeviceType");
        return device;
    }
}
