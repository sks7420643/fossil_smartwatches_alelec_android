package com.portfolio.platform.uirenew.home.customize.domain.usecase;

import android.content.Intent;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.CustomizeSettingExtension;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetHybridPresetToWatchUseCase extends CoroutineUseCase<Bi, Ci, Ai> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public Bi e;
    @DexIgnore
    public /* final */ Di f; // = new Di();
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ HybridPresetRepository h;
    @DexIgnore
    public /* final */ MicroAppRepository i;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public Ai(int i, ArrayList<Integer> arrayList) {
            Wg6.c(arrayList, "mBLEErrorCodes");
            this.a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ HybridPreset a;

        @DexIgnore
        public Bi(HybridPreset hybridPreset) {
            Wg6.c(hybridPreset, "mPreset");
            this.a = hybridPreset;
        }

        @DexIgnore
        public final HybridPreset a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di implements BleCommandResultManager.Bi {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            if (SetHybridPresetToWatchUseCase.this.q()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetHybridPresetToWatchUseCase", "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
                if (communicateMode == CommunicateMode.SET_LINK_MAPPING) {
                    SetHybridPresetToWatchUseCase.this.u(false);
                    if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                        FLogger.INSTANCE.getLocal().d("SetHybridPresetToWatchUseCase", "onReceive - success");
                        SetHybridPresetToWatchUseCase.this.v();
                        return;
                    }
                    FLogger.INSTANCE.getLocal().d("SetHybridPresetToWatchUseCase", "onReceive - failed");
                    int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>(intExtra2);
                    }
                    SetHybridPresetToWatchUseCase.this.i(new Ai(intExtra2, integerArrayListExtra));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$setPresetToDb$1", f = "SetHybridPresetToWatchUseCase.kt", l = {80}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SetHybridPresetToWatchUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = setHybridPresetToWatchUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            HybridPreset a2;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                a2 = SetHybridPresetToWatchUseCase.o(this.this$0).a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetHybridPresetToWatchUseCase", "set to watch success, set preset " + a2 + " to db");
                a2.setActive(true);
                HybridPresetRepository hybridPresetRepository = this.this$0.h;
                this.L$0 = il6;
                this.L$1 = a2;
                this.label = 1;
                if (hybridPresetRepository.upsertHybridPreset(a2, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                a2 = (HybridPreset) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "Calendar.getInstance()");
            String w0 = TimeUtils.w0(instance.getTime());
            Iterator<HybridPresetAppSetting> it = a2.getButtons().iterator();
            while (it.hasNext()) {
                HybridPresetAppSetting next = it.next();
                if (!CustomizeSettingExtension.d(next.getSettings())) {
                    SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase = this.this$0;
                    String appId = next.getAppId();
                    String settings = next.getSettings();
                    if (settings != null) {
                        setHybridPresetToWatchUseCase.t(appId, settings);
                        MicroAppLastSettingRepository microAppLastSettingRepository = this.this$0.j;
                        String appId2 = next.getAppId();
                        Wg6.b(w0, "updatedAt");
                        String settings2 = next.getSettings();
                        if (settings2 != null) {
                            microAppLastSettingRepository.upsertMicroAppLastSetting(new MicroAppLastSetting(appId2, w0, settings2));
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
            this.this$0.j(new Ci());
            return Cd6.a;
        }
    }

    @DexIgnore
    public SetHybridPresetToWatchUseCase(DeviceRepository deviceRepository, HybridPresetRepository hybridPresetRepository, MicroAppRepository microAppRepository, MicroAppLastSettingRepository microAppLastSettingRepository) {
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(hybridPresetRepository, "mHybridPresetRepository");
        Wg6.c(microAppRepository, "mMicroAppRepository");
        Wg6.c(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        this.g = deviceRepository;
        this.h = hybridPresetRepository;
        this.i = microAppRepository;
        this.j = microAppLastSettingRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Bi o(SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase) {
        Bi bi = setHybridPresetToWatchUseCase.e;
        if (bi != null) {
            return bi;
        }
        Wg6.n("mRequestValues");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "SetHybridPresetToWatchUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return s(bi, xe6);
    }

    @DexIgnore
    public final boolean q() {
        return this.d;
    }

    @DexIgnore
    public final void r() {
        BleCommandResultManager.d.e(this.f, CommunicateMode.SET_LINK_MAPPING);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x007d, code lost:
        if (com.fossil.Ao7.f(com.portfolio.platform.PortfolioApp.get.instance().D1(r0, r1)) != null) goto L_0x007f;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object s(com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase.Bi r6, com.mapped.Xe6<java.lang.Object> r7) {
        /*
            r5 = this;
            r4 = 0
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = "SetHybridPresetToWatchUseCase"
            java.lang.String r2 = "executeUseCase"
            r0.d(r1, r2)
            if (r6 == 0) goto L_0x0091
            r5.e = r6
            r0 = 1
            r5.d = r0
            com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r0 = r0.instance()
            java.lang.String r0 = r0.J()
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "execute with preset "
            r2.append(r3)
            com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$Bi r3 = r5.e
            if (r3 == 0) goto L_0x008b
            com.portfolio.platform.data.model.room.microapp.HybridPreset r3 = r3.a()
            r2.append(r3)
            java.lang.String r3 = "SetHybridPresetToWatchUseCase"
            java.lang.String r2 = r2.toString()
            r1.d(r3, r2)
            com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$Bi r1 = r5.e
            if (r1 == 0) goto L_0x0085
            com.portfolio.platform.data.model.room.microapp.HybridPreset r1 = r1.a()
            com.portfolio.platform.data.source.DeviceRepository r2 = r5.g
            com.portfolio.platform.data.source.MicroAppRepository r3 = r5.i
            java.util.List r1 = com.mapped.CustomizeConfigurationExt.k(r1, r0, r2, r3)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "set preset to watch with bleMappings "
            r3.append(r4)
            r3.append(r1)
            java.lang.String r4 = "SetHybridPresetToWatchUseCase"
            java.lang.String r3 = r3.toString()
            r2.d(r4, r3)
            com.portfolio.platform.PortfolioApp$inner r2 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r2 = r2.instance()
            long r0 = r2.D1(r0, r1)
            java.lang.Long r0 = com.fossil.Ao7.f(r0)
            if (r0 == 0) goto L_0x0091
        L_0x007f:
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            return r0
        L_0x0085:
            java.lang.String r0 = "mRequestValues"
            com.mapped.Wg6.n(r0)
            throw r4
        L_0x008b:
            java.lang.String r0 = "mRequestValues"
            com.mapped.Wg6.n(r0)
            throw r4
        L_0x0091:
            com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$Ai r0 = new com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$Ai
            r1 = -1
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r0.<init>(r1, r2)
            r5.i(r0)
            goto L_0x007f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase.s(com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$Bi, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void t(String str, String str2) {
        try {
            if (Wg6.a(str, MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                PortfolioApp.get.instance().t1(((SecondTimezoneSetting) new Gson().k(str2, SecondTimezoneSetting.class)).getTimeZoneId());
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("SetHybridPresetToWatchUseCase", "setAutoSettingToWatch exception " + e2);
        }
    }

    @DexIgnore
    public final void u(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final Rm6 v() {
        return Gu7.d(g(), null, null, new Ei(this, null), 3, null);
    }

    @DexIgnore
    public final void w() {
        BleCommandResultManager.d.j(this.f, CommunicateMode.SET_LINK_MAPPING);
    }
}
