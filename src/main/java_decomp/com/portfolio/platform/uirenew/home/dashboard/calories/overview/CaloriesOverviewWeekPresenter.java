package com.portfolio.platform.uirenew.home.dashboard.calories.overview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.H47;
import com.fossil.Hm7;
import com.fossil.Hs0;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Or0;
import com.fossil.Rh5;
import com.fossil.Ss0;
import com.fossil.Wg6;
import com.fossil.Xg6;
import com.fossil.Xh5;
import com.fossil.Yn7;
import com.mapped.CaloriesOverviewWeekFragment;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.FitnessHelper;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CaloriesOverviewWeekPresenter extends Wg6 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public Date f;
    @DexIgnore
    public /* final */ MutableLiveData<Date> g;
    @DexIgnore
    public /* final */ LiveData<H47<List<ActivitySummary>>> h;
    @DexIgnore
    public BarChart.c i;
    @DexIgnore
    public /* final */ Xg6 j;
    @DexIgnore
    public /* final */ UserRepository k;
    @DexIgnore
    public /* final */ SummariesRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter", f = "CaloriesOverviewWeekPresenter.kt", l = {156}, m = "calculateStartAndEndDate")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewWeekPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter, Xe6 xe6) {
            super(xe6);
            this.this$0 = caloriesOverviewWeekPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.w(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewWeekPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$mActivitySummaries$1$1", f = "CaloriesOverviewWeekPresenter.kt", l = {42, 47, 45}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<ActivitySummary>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$mActivitySummaries$1$1$startAndEnd$1", f = "CaloriesOverviewWeekPresenter.kt", l = {42}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends Date, ? extends Date>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    com.mapped.Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends Date, ? extends Date>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Aii aii = this.this$0;
                        CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter = aii.this$0.a;
                        Date date = aii.$it;
                        com.mapped.Wg6.b(date, "it");
                        this.L$0 = il6;
                        this.label = 1;
                        Object w = caloriesOverviewWeekPresenter.w(date, this);
                        return w == d ? d : w;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Date date, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                com.mapped.Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$it, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<ActivitySummary>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0047  */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x00a8  */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00cb  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 3
                    r5 = 2
                    r4 = 1
                    java.lang.Object r6 = com.fossil.Yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x00ab
                    if (r0 == r4) goto L_0x0049
                    if (r0 == r5) goto L_0x0027
                    if (r0 != r7) goto L_0x001f
                    java.lang.Object r0 = r8.L$1
                    com.mapped.Lc6 r0 = (com.mapped.Lc6) r0
                    java.lang.Object r0 = r8.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r9)
                L_0x001c:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x001e:
                    return r0
                L_0x001f:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0027:
                    java.lang.Object r0 = r8.L$2
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r8.L$1
                    com.mapped.Lc6 r1 = (com.mapped.Lc6) r1
                    java.lang.Object r2 = r8.L$0
                    com.fossil.Hs0 r2 = (com.fossil.Hs0) r2
                    com.fossil.El7.b(r9)
                    r5 = r0
                    r3 = r9
                L_0x0038:
                    r0 = r3
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r2
                    r8.L$1 = r1
                    r8.label = r7
                    java.lang.Object r0 = r5.a(r0, r8)
                    if (r0 != r6) goto L_0x001c
                    r0 = r6
                    goto L_0x001e
                L_0x0049:
                    java.lang.Object r0 = r8.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r9)
                    r4 = r0
                    r1 = r9
                L_0x0052:
                    r0 = r1
                    com.mapped.Lc6 r0 = (com.mapped.Lc6) r0
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r1.getLocal()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r1 = "mActivitySummaries onDateChanged - startDate="
                    r3.append(r1)
                    java.lang.Object r1 = r0.getFirst()
                    java.util.Date r1 = (java.util.Date) r1
                    r3.append(r1)
                    java.lang.String r1 = ", endDate="
                    r3.append(r1)
                    java.lang.Object r1 = r0.getSecond()
                    java.util.Date r1 = (java.util.Date) r1
                    r3.append(r1)
                    java.lang.String r1 = "CaloriesOverviewWeekPresenter"
                    java.lang.String r3 = r3.toString()
                    r2.d(r1, r3)
                    com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$Bi r1 = r8.this$0
                    com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter r1 = r1.a
                    com.portfolio.platform.data.source.SummariesRepository r3 = com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter.r(r1)
                    java.lang.Object r1 = r0.getFirst()
                    java.util.Date r1 = (java.util.Date) r1
                    java.lang.Object r2 = r0.getSecond()
                    java.util.Date r2 = (java.util.Date) r2
                    r8.L$0 = r4
                    r8.L$1 = r0
                    r8.L$2 = r4
                    r8.label = r5
                    r5 = 0
                    java.lang.Object r3 = r3.getSummaries(r1, r2, r5, r8)
                    if (r3 != r6) goto L_0x00cb
                    r0 = r6
                    goto L_0x001e
                L_0x00ab:
                    com.fossil.El7.b(r9)
                    com.fossil.Hs0 r0 = r8.p$
                    com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$Bi r1 = r8.this$0
                    com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter r1 = r1.a
                    com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter.o(r1)
                    com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$Bi$Aii$Aiii r2 = new com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$Bi$Aii$Aiii
                    r3 = 0
                    r2.<init>(r8, r3)
                    r8.L$0 = r0
                    r8.label = r4
                    java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r8)
                    if (r1 != r6) goto L_0x00d0
                    r0 = r6
                    goto L_0x001e
                L_0x00cb:
                    r2 = r4
                    r1 = r0
                    r5 = r4
                    goto L_0x0038
                L_0x00d0:
                    r4 = r0
                    goto L_0x0052
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter.Bi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Bi(CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter) {
            this.a = caloriesOverviewWeekPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<ActivitySummary>>> a(Date date) {
            return Or0.c(null, 0, new Aii(this, date, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<H47<? extends List<ActivitySummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ CaloriesOverviewWeekPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$start$1$1", f = "CaloriesOverviewWeekPresenter.kt", l = {72}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$start$1$1$1", f = "CaloriesOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super BarChart.c>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    com.mapped.Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super BarChart.c> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter = this.this$0.this$0.a;
                        Date date = caloriesOverviewWeekPresenter.f;
                        if (date != null) {
                            return caloriesOverviewWeekPresenter.z(date, this.this$0.$data);
                        }
                        com.mapped.Wg6.i();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$data = list;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                com.mapped.Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$data, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter2 = this.this$0.a;
                    Dv7 h = caloriesOverviewWeekPresenter2.h();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.L$1 = caloriesOverviewWeekPresenter2;
                    this.label = 1;
                    g = Eu7.g(h, aiii, this);
                    if (g == d) {
                        return d;
                    }
                    caloriesOverviewWeekPresenter = caloriesOverviewWeekPresenter2;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    caloriesOverviewWeekPresenter = (CaloriesOverviewWeekPresenter) this.L$1;
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                caloriesOverviewWeekPresenter.i = (BarChart.c) g;
                Xg6 xg6 = this.this$0.a.j;
                BarChart.c cVar = this.this$0.a.i;
                if (cVar == null) {
                    cVar = new BarChart.c(0, 0, null, 7, null);
                }
                xg6.p(cVar);
                return Cd6.a;
            }
        }

        @DexIgnore
        public Ci(CaloriesOverviewWeekPresenter caloriesOverviewWeekPresenter) {
            this.a = caloriesOverviewWeekPresenter;
        }

        @DexIgnore
        public final void a(H47<? extends List<ActivitySummary>> h47) {
            Xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mActivitySummaries -- activitySummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("CaloriesOverviewWeekPresenter", sb.toString());
            if (a2 != Xh5.DATABASE_LOADING) {
                Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, list, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<ActivitySummary>> h47) {
            a(h47);
        }
    }

    @DexIgnore
    public CaloriesOverviewWeekPresenter(Xg6 xg6, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        com.mapped.Wg6.c(xg6, "mView");
        com.mapped.Wg6.c(userRepository, "userRepository");
        com.mapped.Wg6.c(summariesRepository, "mSummariesRepository");
        com.mapped.Wg6.c(portfolioApp, "mApp");
        this.j = xg6;
        this.k = userRepository;
        this.l = summariesRepository;
        this.e = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.J());
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.g = mutableLiveData;
        LiveData<H47<List<ActivitySummary>>> c = Ss0.c(mutableLiveData, new Bi(this));
        com.mapped.Wg6.b(c, "Transformations.switchMa\u2026, false))\n        }\n    }");
        this.h = c;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        x();
        Date date = this.f;
        if (date == null || !TimeUtils.p0(date).booleanValue()) {
            Date date2 = new Date();
            this.f = date2;
            this.g.l(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesOverviewWeekPresenter", "start with date " + this.f);
        LiveData<H47<List<ActivitySummary>>> liveData = this.h;
        Xg6 xg6 = this.j;
        if (xg6 != null) {
            liveData.h((CaloriesOverviewWeekFragment) xg6, new Ci(this));
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("CaloriesOverviewWeekPresenter", "start - mDate=" + this.f);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekFragment");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekPresenter", "stop");
        try {
            LiveData<H47<List<ActivitySummary>>> liveData = this.h;
            Xg6 xg6 = this.j;
            if (xg6 != null) {
                liveData.n((CaloriesOverviewWeekFragment) xg6);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CaloriesOverviewWeekPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Wg6
    public FossilDeviceSerialPatternUtil.DEVICE n() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        com.mapped.Wg6.b(device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object w(java.util.Date r6, com.mapped.Xe6<? super com.mapped.Lc6<? extends java.util.Date, ? extends java.util.Date>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter.Ai
            if (r0 == 0) goto L_0x0057
            r0 = r7
            com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$Ai r0 = (com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0057
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0066
            if (r0 != r4) goto L_0x005e
            java.lang.Object r0 = r2.L$2
            com.mapped.Jh6 r0 = (com.mapped.Jh6) r0
            java.lang.Object r1 = r2.L$1
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter r2 = (com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter) r2
            com.fossil.El7.b(r3)
            r2 = r3
            r4 = r0
        L_0x0031:
            r0 = r2
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x004c
            java.lang.String r0 = r0.getCreatedAt()
            if (r0 == 0) goto L_0x008a
            java.util.Date r2 = com.mapped.TimeUtils.q0(r0)
            T r0 = r4.element
            java.util.Date r0 = (java.util.Date) r0
            boolean r0 = com.mapped.TimeUtils.j0(r0, r2)
            if (r0 != 0) goto L_0x004c
            r4.element = r2
        L_0x004c:
            com.mapped.Lc6 r2 = new com.mapped.Lc6
            T r0 = r4.element
            java.util.Date r0 = (java.util.Date) r0
            r2.<init>(r0, r1)
            r0 = r2
        L_0x0056:
            return r0
        L_0x0057:
            com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$Ai r0 = new com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter$Ai
            r0.<init>(r5, r7)
            r2 = r0
            goto L_0x0014
        L_0x005e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0066:
            com.fossil.El7.b(r3)
            com.mapped.Jh6 r0 = new com.mapped.Jh6
            r0.<init>()
            r3 = 6
            java.util.Date r3 = com.mapped.TimeUtils.Q(r6, r3)
            r0.element = r3
            com.portfolio.platform.data.source.UserRepository r3 = r5.k
            r2.L$0 = r5
            r2.L$1 = r6
            r2.L$2 = r0
            r2.label = r4
            java.lang.Object r2 = r3.getCurrentUser(r2)
            if (r2 != r1) goto L_0x0087
            r0 = r1
            goto L_0x0056
        L_0x0087:
            r4 = r0
            r1 = r6
            goto L_0x0031
        L_0x008a:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewWeekPresenter.w(java.util.Date, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public void x() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewWeekPresenter", "loadData");
    }

    @DexIgnore
    public void y() {
        this.j.M5(this);
    }

    @DexIgnore
    public final BarChart.c z(Date date, List<ActivitySummary> list) {
        T t;
        boolean z;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("CaloriesOverviewWeekPresenter", sb.toString());
        BarChart.c cVar = new BarChart.c(0, 0, null, 7, null);
        Calendar instance = Calendar.getInstance(Locale.US);
        com.mapped.Wg6.b(instance, "calendar");
        instance.setTime(date);
        instance.add(5, -6);
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i4 <= 6) {
            Date time = instance.getTime();
            com.mapped.Wg6.b(time, "calendar.time");
            long time2 = time.getTime();
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    T t2 = next;
                    if (t2.getDay() == instance.get(5) && t2.getMonth() == instance.get(2) + 1 && t2.getYear() == instance.get(1)) {
                        z = true;
                        continue;
                    } else {
                        z = false;
                        continue;
                    }
                    if (z) {
                        t = next;
                        break;
                    }
                }
                T t3 = t;
                if (t3 != null) {
                    i3 = FitnessHelper.c.d(t3, Rh5.CALORIES);
                    int calories = (int) t3.getCalories();
                    i2 = Math.max(Math.max(i3, calories), i2);
                    cVar.b().add(new BarChart.a(i3, Hm7.c(Hm7.c(new BarChart.b(0, null, 0, calories, null, 23, null))), time2, i4 == 6));
                } else {
                    cVar.b().add(new BarChart.a(i3, Hm7.c(Hm7.c(new BarChart.b(0, null, 0, 0, null, 23, null))), time2, i4 == 6));
                }
            } else {
                cVar.b().add(new BarChart.a(i3, Hm7.c(Hm7.c(new BarChart.b(0, null, 0, 0, null, 23, null))), time2, i4 == 6));
            }
            instance.add(5, 1);
            i3 = i3;
            i2 = i2;
            i4++;
        }
        if (i2 > 0) {
            i3 = i2;
        }
        cVar.f(i3);
        return cVar;
    }
}
