package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.M76;
import com.fossil.X76;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsDefaultAddressActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public CommuteTimeSettingsDefaultAddressPresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, Bundle bundle) {
            Wg6.c(fragment, "fragment");
            Wg6.c(bundle, "bundle");
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsDefaultAddressActivity.class);
            intent.putExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS", bundle);
            fragment.startActivityForResult(intent, 111);
        }

        @DexIgnore
        public final void b(Fragment fragment, Bundle bundle) {
            Wg6.c(fragment, "fragment");
            Wg6.c(bundle, "bundle");
            Intent intent = new Intent(fragment.getContext(), CommuteTimeSettingsDefaultAddressActivity.class);
            intent.putExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS", bundle);
            fragment.startActivityForResult(intent, 112);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        M76 m76 = (M76) getSupportFragmentManager().Y(2131362158);
        if (m76 == null) {
            m76 = M76.s.a();
            k(m76, M76.s.b(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (m76 != null) {
            iface.m0(new X76(m76)).a(this);
            Bundle bundleExtra = getIntent().getBundleExtra("KEY_BUNDLE_SETTING_DEFAULT_ADDRESS");
            if (bundleExtra != null) {
                CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter = this.A;
                if (commuteTimeSettingsDefaultAddressPresenter != null) {
                    String string = bundleExtra.getString("AddressType");
                    if (string == null) {
                        string = "Home";
                    }
                    String string2 = bundleExtra.getString("KEY_DEFAULT_PLACE");
                    if (string2 == null) {
                        string2 = "";
                    }
                    commuteTimeSettingsDefaultAddressPresenter.A(string, string2);
                    return;
                }
                Wg6.n("mCommuteTimeSettingsDefaultAddressPresenter");
                throw null;
            }
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressContract.View");
    }
}
