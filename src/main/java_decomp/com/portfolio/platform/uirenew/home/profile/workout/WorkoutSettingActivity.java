package com.portfolio.platform.uirenew.home.profile.workout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.WorkoutSettingFragment;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            Wg6.c(context, "context");
            FLogger.INSTANCE.getLocal().d("WorkoutSettingActivity", "start()");
            context.startActivity(new Intent(context, WorkoutSettingActivity.class));
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        if (((WorkoutSettingFragment) getSupportFragmentManager().Y(2131362158)) == null) {
            i(WorkoutSettingFragment.m.a(), 2131362158);
        }
    }
}
