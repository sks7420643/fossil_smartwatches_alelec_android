package com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone;

import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.J86;
import com.fossil.K86;
import com.fossil.Ko7;
import com.fossil.Pn5;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.helper.AppHelper;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SearchRingPhonePresenter extends J86 {
    @DexIgnore
    public /* final */ ArrayList<Ringtone> e; // = new ArrayList<>();
    @DexIgnore
    public Ringtone f;
    @DexIgnore
    public /* final */ Gson g; // = new Gson();
    @DexIgnore
    public /* final */ K86 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter$start$1", f = "SearchRingPhonePresenter.kt", l = {29}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SearchRingPhonePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.ringphone.SearchRingPhonePresenter$start$1$1", f = "SearchRingPhonePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Ringtone>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends Ringtone>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return AppHelper.g.e();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(SearchRingPhonePresenter searchRingPhonePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = searchRingPhonePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            ArrayList arrayList;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                if (this.this$0.e.isEmpty()) {
                    ArrayList arrayList2 = this.this$0.e;
                    Dv7 h = this.this$0.h();
                    Aii aii = new Aii(null);
                    this.L$0 = il6;
                    this.L$1 = arrayList2;
                    this.label = 1;
                    g = Eu7.g(h, aii, this);
                    if (g == d) {
                        return d;
                    }
                    arrayList = arrayList2;
                }
                this.this$0.u().J3(this.this$0.e);
                return Cd6.a;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
                arrayList = (ArrayList) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            arrayList.addAll((Collection) g);
            if (this.this$0.f == null) {
                SearchRingPhonePresenter searchRingPhonePresenter = this.this$0;
                searchRingPhonePresenter.f = (Ringtone) searchRingPhonePresenter.e.get(0);
            }
            this.this$0.u().J3(this.this$0.e);
            return Cd6.a;
        }
    }

    @DexIgnore
    public SearchRingPhonePresenter(K86 k86) {
        Wg6.c(k86, "mView");
        this.h = k86;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Rm6 unused = Gu7.d(k(), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.J86
    public Ringtone n() {
        Ringtone ringtone = this.f;
        if (ringtone != null) {
            return ringtone;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.J86
    public void o(Ringtone ringtone) {
        Wg6.c(ringtone, Constants.RINGTONE);
        Pn5.o.a().j(ringtone);
        this.f = ringtone;
    }

    @DexIgnore
    @Override // com.fossil.J86
    public void p() {
        Pn5.o.a().p();
    }

    @DexIgnore
    public final K86 u() {
        return this.h;
    }

    @DexIgnore
    public void v(Ringtone ringtone) {
        Wg6.c(ringtone, Constants.RINGTONE);
        this.f = ringtone;
    }

    @DexIgnore
    public void w(String str) {
        Wg6.c(str, MicroAppSetting.SETTING);
        try {
            this.f = (Ringtone) this.g.k(str, Ringtone.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SearchRingPhonePresenter", "exception when parse ringtone setting " + e2);
        }
    }

    @DexIgnore
    public void x() {
        this.h.M5(this);
    }
}
