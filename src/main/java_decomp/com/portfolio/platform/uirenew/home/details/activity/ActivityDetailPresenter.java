package com.portfolio.platform.uirenew.home.details.activity;

import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Ai5;
import com.fossil.Ao7;
import com.fossil.At7;
import com.fossil.Bl6;
import com.fossil.Bw7;
import com.fossil.Cl6;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.H47;
import com.fossil.Hs0;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Or0;
import com.fossil.Pm7;
import com.fossil.Qq7;
import com.fossil.Ss0;
import com.fossil.Ts7;
import com.fossil.Xh5;
import com.fossil.Yn7;
import com.mapped.ActivityDetailFragment;
import com.mapped.Cd6;
import com.mapped.Cf;
import com.mapped.Coroutine;
import com.mapped.Fd;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.PagingRequestHelper;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.SupportedFunction;
import com.mapped.TimeUtils;
import com.mapped.U04;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.workout.WorkoutTetherScreenShotManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityDetailPresenter extends Bl6 implements PagingRequestHelper.Ai {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public MutableLiveData<Lc6<Date, Date>> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ WorkoutTetherScreenShotManager i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public List<ActivitySummary> l;
    @DexIgnore
    public List<ActivitySample> m;
    @DexIgnore
    public ActivitySummary n;
    @DexIgnore
    public List<ActivitySample> o;
    @DexIgnore
    public Ai5 p;
    @DexIgnore
    public LiveData<H47<List<ActivitySummary>>> q;
    @DexIgnore
    public LiveData<H47<List<ActivitySample>>> r;
    @DexIgnore
    public Listing<WorkoutSession> s;
    @DexIgnore
    public /* final */ Cl6 t;
    @DexIgnore
    public /* final */ SummariesRepository u;
    @DexIgnore
    public /* final */ ActivitiesRepository v;
    @DexIgnore
    public /* final */ UserRepository w;
    @DexIgnore
    public /* final */ WorkoutSessionRepository x;
    @DexIgnore
    public /* final */ U04 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Hg6<ActivitySample, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Date date) {
            super(1);
            this.$date = date;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Boolean invoke(ActivitySample activitySample) {
            return Boolean.valueOf(invoke(activitySample));
        }

        @DexIgnore
        public final boolean invoke(ActivitySample activitySample) {
            Wg6.c(activitySample, "it");
            return TimeUtils.m0(activitySample.getDate(), this.$date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$observeWorkoutSessionData$1", f = "ActivityDetailPresenter.kt", l = {100}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii<T> implements Ls0<Cf<WorkoutSession>> {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$observeWorkoutSessionData$1$1$1", f = "ActivityDetailPresenter.kt", l = {111}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Cf $pageList;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Xe6 xe6, Aiii aiii) {
                        super(2, xe6);
                        this.this$0 = aiii;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        Aiiii aiiii = new Aiiii(xe6, this.this$0);
                        aiiii.p$ = (Il6) obj;
                        throw null;
                        //return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                        throw null;
                        //return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    /* JADX WARNING: Removed duplicated region for block: B:25:0x009c  */
                    /* JADX WARNING: Removed duplicated region for block: B:41:0x0014 A[SYNTHETIC] */
                    @Override // com.fossil.Zn7
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                        /*
                        // Method dump skipped, instructions count: 319
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter.Bi.Aii.Aiii.Aiiii.invokeSuspend(java.lang.Object):java.lang.Object");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Cf cf, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                    this.$pageList = cf;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$pageList, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        FragmentActivity activity = ((ActivityDetailFragment) this.this$0.a.this$0.t).getActivity();
                        if (activity != null) {
                            WorkoutTetherScreenShotManager workoutTetherScreenShotManager = this.this$0.a.this$0.i;
                            Wg6.b(activity, "it");
                            workoutTetherScreenShotManager.w(activity);
                            Dv7 a2 = Bw7.a();
                            Aiiii aiiii = new Aiiii(null, this);
                            this.L$0 = il6;
                            this.L$1 = activity;
                            this.label = 1;
                            if (Eu7.g(a2, aiiii, this) == d) {
                                return d;
                            }
                        }
                    } else if (i == 1) {
                        FragmentActivity fragmentActivity = (FragmentActivity) this.L$1;
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    Cl6 cl6 = this.this$0.a.this$0.t;
                    Ai5 ai5 = this.this$0.a.this$0.p;
                    Cf<WorkoutSession> cf = this.$pageList;
                    Wg6.b(cf, "pageList");
                    cl6.s(true, ai5, cf);
                    return Cd6.a;
                }
            }

            @DexIgnore
            public Aii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            public final void a(Cf<WorkoutSession> cf) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ActivityDetailPresenter", "on workout sessions changed, size " + cf.size());
                if (DeviceHelper.o.y(PortfolioApp.get.instance().J())) {
                    Wg6.b(cf, "pageList");
                    if (Pm7.j0(cf).isEmpty()) {
                        this.a.this$0.t.s(false, this.a.this$0.p, cf);
                        return;
                    }
                }
                Rm6 unused = Gu7.d(this.a.this$0.k(), null, null, new Aiii(this, cf, null), 3, null);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(Cf<WorkoutSession> cf) {
                a(cf);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(ActivityDetailPresenter activityDetailPresenter, Date date, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = activityDetailPresenter;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$date, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object workoutSessionsPaging;
            ActivityDetailPresenter activityDetailPresenter;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ActivityDetailPresenter activityDetailPresenter2 = this.this$0;
                WorkoutSessionRepository workoutSessionRepository = activityDetailPresenter2.x;
                Date date = this.$date;
                WorkoutSessionRepository workoutSessionRepository2 = this.this$0.x;
                U04 u04 = this.this$0.y;
                ActivityDetailPresenter activityDetailPresenter3 = this.this$0;
                this.L$0 = il6;
                this.L$1 = activityDetailPresenter2;
                this.label = 1;
                workoutSessionsPaging = workoutSessionRepository.getWorkoutSessionsPaging(date, workoutSessionRepository2, u04, activityDetailPresenter3, this);
                if (workoutSessionsPaging == d) {
                    return d;
                }
                activityDetailPresenter = activityDetailPresenter2;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                activityDetailPresenter = (ActivityDetailPresenter) this.L$1;
                workoutSessionsPaging = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            activityDetailPresenter.s = (Listing) workoutSessionsPaging;
            Listing listing = this.this$0.s;
            if (listing != null) {
                LiveData pagedList = listing.getPagedList();
                Cl6 cl6 = this.this$0.t;
                if (cl6 != null) {
                    pagedList.h((ActivityDetailFragment) cl6, new Aii(this));
                    return Cd6.a;
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$sampleTransformations$1$1", f = "ActivityDetailPresenter.kt", l = {79, 79}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<ActivitySample>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Date date, Date date2, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$first, this.$second, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<ActivitySample>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r8)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                    com.fossil.El7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.El7.b(r8)
                    com.fossil.Hs0 r0 = r7.p$
                    com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$Ci r1 = r7.this$0
                    com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter r1 = r1.a
                    com.portfolio.platform.data.source.ActivitiesRepository r1 = com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter.B(r1)
                    java.util.Date r2 = r7.$first
                    java.util.Date r3 = r7.$second
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getActivityList(r2, r3, r5, r7)
                    if (r2 != r4) goto L_0x005b
                    r0 = r4
                    goto L_0x0017
                L_0x005b:
                    r3 = r0
                    r1 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter.Ci.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Ci(ActivityDetailPresenter activityDetailPresenter) {
            this.a = activityDetailPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<ActivitySample>>> a(Lc6<? extends Date, ? extends Date> lc6) {
            return Or0.c(Bw7.c(), 0, new Aii(this, (Date) lc6.component1(), (Date) lc6.component2(), null), 2, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Lc6) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$setDate$1", f = "ActivityDetailPresenter.kt", l = {211, 235, 236}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$setDate$1$1", f = "ActivityDetailPresenter.kt", l = {211}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Date>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Date> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    PortfolioApp instance = PortfolioApp.get.instance();
                    this.L$0 = il6;
                    this.label = 1;
                    Object n0 = instance.n0(this);
                    return n0 == d ? d : n0;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$setDate$1$samples$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<ActivitySample>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<ActivitySample>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    ActivityDetailPresenter activityDetailPresenter = this.this$0.this$0;
                    return activityDetailPresenter.h0(activityDetailPresenter.g, this.this$0.this$0.m);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$setDate$1$summary$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super ActivitySummary>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super ActivitySummary> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    ActivityDetailPresenter activityDetailPresenter = this.this$0.this$0;
                    return activityDetailPresenter.i0(activityDetailPresenter.g, this.this$0.this$0.l);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(ActivityDetailPresenter activityDetailPresenter, Date date, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = activityDetailPresenter;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$date, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00cc  */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x022d  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 563
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter.Di.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$showDetailChart$1", f = "ActivityDetailPresenter.kt", l = {278, 280}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$showDetailChart$1$maxValue$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Integer>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $data;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(ArrayList arrayList, Xe6 xe6) {
                super(2, xe6);
                this.$data = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$data, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Integer> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                ArrayList<ArrayList<BarChart.b>> d;
                ArrayList<BarChart.b> arrayList;
                int i = 0;
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    Iterator it = this.$data.iterator();
                    if (!it.hasNext()) {
                        obj2 = null;
                    } else {
                        Object next = it.next();
                        if (!it.hasNext()) {
                            obj2 = next;
                        } else {
                            ArrayList<BarChart.b> arrayList2 = ((BarChart.a) next).d().get(0);
                            Wg6.b(arrayList2, "it.mListOfBarPoints[0]");
                            Iterator<T> it2 = arrayList2.iterator();
                            int i2 = 0;
                            while (it2.hasNext()) {
                                i2 = Ao7.e(it2.next().e()).intValue() + i2;
                            }
                            Integer e = Ao7.e(i2);
                            while (true) {
                                next = it.next();
                                ArrayList<BarChart.b> arrayList3 = ((BarChart.a) next).d().get(0);
                                Wg6.b(arrayList3, "it.mListOfBarPoints[0]");
                                Iterator<T> it3 = arrayList3.iterator();
                                int i3 = 0;
                                while (it3.hasNext()) {
                                    i3 = Ao7.e(it3.next().e()).intValue() + i3;
                                }
                                e = Ao7.e(i3);
                                if (e.compareTo(e) >= 0) {
                                    e = e;
                                    next = next;
                                }
                                if (!it.hasNext()) {
                                    break;
                                }
                            }
                            obj2 = next;
                        }
                    }
                    BarChart.a aVar = (BarChart.a) obj2;
                    if (aVar == null || (d = aVar.d()) == null || (arrayList = d.get(0)) == null) {
                        return null;
                    }
                    Iterator<T> it4 = arrayList.iterator();
                    while (it4.hasNext()) {
                        i += Ao7.e(it4.next().e()).intValue();
                    }
                    return Ao7.e(i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$showDetailChart$1$pair$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return SupportedFunction.a.b(this.this$0.this$0.g, this.this$0.this$0.o, 0);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(ActivityDetailPresenter activityDetailPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = activityDetailPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0088  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x00a6  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                r7 = 0
                r6 = 2
                r4 = 1
                java.lang.Object r3 = com.fossil.Yn7.d()
                int r0 = r8.label
                if (r0 == 0) goto L_0x008a
                if (r0 == r4) goto L_0x005d
                if (r0 != r6) goto L_0x0055
                java.lang.Object r0 = r8.L$2
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                java.lang.Object r1 = r8.L$1
                com.mapped.Lc6 r1 = (com.mapped.Lc6) r1
                java.lang.Object r2 = r8.L$0
                com.mapped.Il6 r2 = (com.mapped.Il6) r2
                com.fossil.El7.b(r9)
                r3 = r1
                r4 = r0
                r2 = r9
            L_0x0021:
                r0 = r2
                java.lang.Integer r0 = (java.lang.Integer) r0
                com.portfolio.platform.helper.FitnessHelper$Ai r1 = com.portfolio.platform.helper.FitnessHelper.c
                com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter r2 = r8.this$0
                com.portfolio.platform.data.model.room.fitness.ActivitySummary r2 = com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter.F(r2)
                com.fossil.Rh5 r5 = com.fossil.Rh5.TOTAL_STEPS
                int r1 = r1.d(r2, r5)
                com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter r2 = r8.this$0
                com.fossil.Cl6 r2 = com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter.P(r2)
                if (r0 == 0) goto L_0x00aa
                int r0 = r0.intValue()
            L_0x003e:
                com.portfolio.platform.ui.view.chart.base.BarChart$c r5 = new com.portfolio.platform.ui.view.chart.base.BarChart$c
                int r6 = r1 / 16
                int r0 = java.lang.Math.max(r0, r6)
                r5.<init>(r0, r1, r4)
                java.lang.Object r0 = r3.getSecond()
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                r2.n(r5, r0)
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0054:
                return r0
            L_0x0055:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x005d:
                java.lang.Object r0 = r8.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r9)
                r2 = r0
                r1 = r9
            L_0x0066:
                r0 = r1
                com.mapped.Lc6 r0 = (com.mapped.Lc6) r0
                java.lang.Object r1 = r0.getFirst()
                java.util.ArrayList r1 = (java.util.ArrayList) r1
                com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter r4 = r8.this$0
                com.fossil.Dv7 r4 = com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter.z(r4)
                com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$Ei$Aii r5 = new com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$Ei$Aii
                r5.<init>(r1, r7)
                r8.L$0 = r2
                r8.L$1 = r0
                r8.L$2 = r1
                r8.label = r6
                java.lang.Object r2 = com.fossil.Eu7.g(r4, r5, r8)
                if (r2 != r3) goto L_0x00a6
                r0 = r3
                goto L_0x0054
            L_0x008a:
                com.fossil.El7.b(r9)
                com.mapped.Il6 r0 = r8.p$
                com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter r1 = r8.this$0
                com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter.z(r1)
                com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$Ei$Bii r2 = new com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$Ei$Bii
                r2.<init>(r8, r7)
                r8.L$0 = r0
                r8.label = r4
                java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r8)
                if (r1 != r3) goto L_0x00ac
                r0 = r3
                goto L_0x0054
            L_0x00a6:
                r3 = r0
                r4 = r1
                goto L_0x0021
            L_0x00aa:
                r0 = 0
                goto L_0x003e
            L_0x00ac:
                r2 = r0
                goto L_0x0066
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter.Ei.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1", f = "ActivityDetailPresenter.kt", l = {149}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$1", f = "ActivityDetailPresenter.kt", l = {150}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Ai5>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ai5> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                String value;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.w;
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    if (currentUser == d) {
                        return d;
                    }
                    obj2 = currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    obj2 = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (obj2 != null) {
                    MFUser.UnitGroup unitGroup = ((MFUser) obj2).getUnitGroup();
                    if (unitGroup == null || (value = unitGroup.getDistance()) == null) {
                        value = Ai5.METRIC.getValue();
                    }
                    return Ai5.fromString(value);
                }
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii<T> implements Ls0<H47<? extends List<ActivitySummary>>> {
            @DexIgnore
            public /* final */ /* synthetic */ Fi a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$2$1", f = "ActivityDetailPresenter.kt", l = {163}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Bii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$2$1$summary$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
                public static final class Aiiii extends Ko7 implements Coroutine<Il6, Xe6<? super ActivitySummary>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Aiii aiii, Xe6 xe6) {
                        super(2, xe6);
                        this.this$0 = aiii;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        Aiiii aiiii = new Aiiii(this.this$0, xe6);
                        aiiii.p$ = (Il6) obj;
                        throw null;
                        //return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super ActivitySummary> xe6) {
                        throw null;
                        //return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Object invokeSuspend(Object obj) {
                        Yn7.d();
                        if (this.label == 0) {
                            El7.b(obj);
                            ActivityDetailPresenter activityDetailPresenter = this.this$0.this$0.a.this$0;
                            return activityDetailPresenter.i0(activityDetailPresenter.g, this.this$0.this$0.a.this$0.l);
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Bii bii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = bii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object g;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Dv7 h = this.this$0.a.this$0.h();
                        Aiiii aiiii = new Aiiii(this, null);
                        this.L$0 = il6;
                        this.label = 1;
                        g = Eu7.g(h, aiiii, this);
                        if (g == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        g = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    ActivitySummary activitySummary = (ActivitySummary) g;
                    if (this.this$0.a.this$0.n == null || (!Wg6.a(this.this$0.a.this$0.n, activitySummary))) {
                        this.this$0.a.this$0.n = activitySummary;
                        this.this$0.a.this$0.t.J(this.this$0.a.this$0.p, this.this$0.a.this$0.n);
                        if (this.this$0.a.this$0.j && this.this$0.a.this$0.k) {
                            this.this$0.a.this$0.m0();
                        }
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            public Bii(Fi fi) {
                this.a = fi;
            }

            @DexIgnore
            public final void a(H47<? extends List<ActivitySummary>> h47) {
                Xh5 a2 = h47.a();
                List list = (List) h47.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("start - summaryTransformations -- activitySummaries=");
                sb.append(list != null ? Integer.valueOf(list.size()) : null);
                sb.append(", status=");
                sb.append(a2);
                local.d("ActivityDetailPresenter", sb.toString());
                if (a2 == Xh5.NETWORK_LOADING || a2 == Xh5.SUCCESS) {
                    this.a.this$0.l = list;
                    this.a.this$0.j = true;
                    Rm6 unused = Gu7.d(this.a.this$0.k(), null, null, new Aiii(this, null), 3, null);
                }
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<ActivitySummary>> h47) {
                a(h47);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii<T> implements Ls0<H47<? extends List<ActivitySample>>> {
            @DexIgnore
            public /* final */ /* synthetic */ Fi a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$3$1", f = "ActivityDetailPresenter.kt", l = {186}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Cii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$start$1$3$1$samples$1", f = "ActivityDetailPresenter.kt", l = {}, m = "invokeSuspend")
                public static final class Aiiii extends Ko7 implements Coroutine<Il6, Xe6<? super List<ActivitySample>>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Aiii aiii, Xe6 xe6) {
                        super(2, xe6);
                        this.this$0 = aiii;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        Aiiii aiiii = new Aiiii(this.this$0, xe6);
                        aiiii.p$ = (Il6) obj;
                        throw null;
                        //return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super List<ActivitySample>> xe6) {
                        throw null;
                        //return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Object invokeSuspend(Object obj) {
                        Yn7.d();
                        if (this.label == 0) {
                            El7.b(obj);
                            ActivityDetailPresenter activityDetailPresenter = this.this$0.this$0.a.this$0;
                            return activityDetailPresenter.h0(activityDetailPresenter.g, this.this$0.this$0.a.this$0.m);
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Cii cii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = cii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object g;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Dv7 h = this.this$0.a.this$0.h();
                        Aiiii aiiii = new Aiiii(this, null);
                        this.L$0 = il6;
                        this.label = 1;
                        g = Eu7.g(h, aiiii, this);
                        if (g == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        g = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    List list = (List) g;
                    if (this.this$0.a.this$0.o == null || (!Wg6.a(this.this$0.a.this$0.o, list))) {
                        this.this$0.a.this$0.o = list;
                        if (this.this$0.a.this$0.j && this.this$0.a.this$0.k) {
                            this.this$0.a.this$0.m0();
                        }
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            public Cii(Fi fi) {
                this.a = fi;
            }

            @DexIgnore
            public final void a(H47<? extends List<ActivitySample>> h47) {
                Xh5 a2 = h47.a();
                List list = (List) h47.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("start - sampleTransformations -- activitySamples=");
                sb.append(list != null ? Integer.valueOf(list.size()) : null);
                sb.append(", status=");
                sb.append(a2);
                local.d("ActivityDetailPresenter", sb.toString());
                if (a2 == Xh5.NETWORK_LOADING || a2 == Xh5.SUCCESS) {
                    this.a.this$0.m = list;
                    this.a.this$0.k = true;
                    Rm6 unused = Gu7.d(this.a.this$0.k(), null, null, new Aiii(this, null), 3, null);
                }
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<ActivitySample>> h47) {
                a(h47);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(ActivityDetailPresenter activityDetailPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = activityDetailPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            ActivityDetailPresenter activityDetailPresenter;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ActivityDetailPresenter activityDetailPresenter2 = this.this$0;
                Dv7 i2 = activityDetailPresenter2.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.L$1 = activityDetailPresenter2;
                this.label = 1;
                g = Eu7.g(i2, aii, this);
                if (g == d) {
                    return d;
                }
                activityDetailPresenter = activityDetailPresenter2;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                activityDetailPresenter = (ActivityDetailPresenter) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Wg6.b(g, "withContext(IO) {\n      \u2026TRIC.value)\n            }");
            activityDetailPresenter.p = (Ai5) g;
            LiveData liveData = this.this$0.q;
            Cl6 cl6 = this.this$0.t;
            if (cl6 != null) {
                liveData.h((ActivityDetailFragment) cl6, new Bii(this));
                this.this$0.r.h((LifecycleOwner) this.this$0.t, new Cii(this));
                return Cd6.a;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivityDetailPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$summaryTransformations$1$1", f = "ActivityDetailPresenter.kt", l = {75, 75}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<ActivitySummary>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Gi gi, Date date, Date date2, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = gi;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$first, this.$second, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<ActivitySummary>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r5 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x003c
                    if (r0 == r5) goto L_0x0020
                    if (r0 != r6) goto L_0x0018
                    java.lang.Object r0 = r7.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r8)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r7.L$1
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                    com.fossil.El7.b(r8)
                    r2 = r8
                    r3 = r0
                L_0x002d:
                    r0 = r2
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r7.L$0 = r1
                    r7.label = r6
                    java.lang.Object r0 = r3.a(r0, r7)
                    if (r0 != r4) goto L_0x0015
                    r0 = r4
                    goto L_0x0017
                L_0x003c:
                    com.fossil.El7.b(r8)
                    com.fossil.Hs0 r0 = r7.p$
                    com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter$Gi r1 = r7.this$0
                    com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter r1 = r1.a
                    com.portfolio.platform.data.source.SummariesRepository r1 = com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter.L(r1)
                    java.util.Date r2 = r7.$first
                    java.util.Date r3 = r7.$second
                    r7.L$0 = r0
                    r7.L$1 = r0
                    r7.label = r5
                    java.lang.Object r2 = r1.getSummaries(r2, r3, r5, r7)
                    if (r2 != r4) goto L_0x005b
                    r0 = r4
                    goto L_0x0017
                L_0x005b:
                    r1 = r0
                    r3 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.activity.ActivityDetailPresenter.Gi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Gi(ActivityDetailPresenter activityDetailPresenter) {
            this.a = activityDetailPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<ActivitySummary>>> a(Lc6<? extends Date, ? extends Date> lc6) {
            return Or0.c(Bw7.c(), 0, new Aii(this, (Date) lc6.component1(), (Date) lc6.component2(), null), 2, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Lc6) obj);
        }
    }

    @DexIgnore
    public ActivityDetailPresenter(Cl6 cl6, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FileRepository fileRepository, U04 u04, PortfolioApp portfolioApp) {
        Wg6.c(cl6, "mView");
        Wg6.c(summariesRepository, "mSummariesRepository");
        Wg6.c(activitiesRepository, "mActivitiesRepository");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(workoutSessionRepository, "mWorkoutSessionRepository");
        Wg6.c(fileRepository, "mFileRepository");
        Wg6.c(u04, "appExecutors");
        Wg6.c(portfolioApp, "mApp");
        this.t = cl6;
        this.u = summariesRepository;
        this.v = activitiesRepository;
        this.w = userRepository;
        this.x = workoutSessionRepository;
        this.y = u04;
        this.e = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.J());
        this.i = new WorkoutTetherScreenShotManager(this.x, fileRepository);
        this.l = new ArrayList();
        this.m = new ArrayList();
        this.p = Ai5.METRIC;
        LiveData<H47<List<ActivitySummary>>> c = Ss0.c(this.h, new Gi(this));
        Wg6.b(c, "Transformations.switchMa\u2026t, second, true)) }\n    }");
        this.q = c;
        LiveData<H47<List<ActivitySample>>> c2 = Ss0.c(this.h, new Ci(this));
        Wg6.b(c2, "Transformations.switchMa\u2026t, second, true)) }\n    }");
        this.r = c2;
    }

    @DexIgnore
    @Override // com.mapped.PagingRequestHelper.Ai
    public void e(PagingRequestHelper.Gi gi) {
        Wg6.c(gi, "report");
    }

    @DexIgnore
    public final List<ActivitySample> h0(Date date, List<ActivitySample> list) {
        Ts7 z;
        Ts7 h2;
        if (list == null || (z = Pm7.z(list)) == null || (h2 = At7.h(z, new Ai(date))) == null) {
            return null;
        }
        return At7.u(h2);
    }

    @DexIgnore
    public final ActivitySummary i0(Date date, List<ActivitySummary> list) {
        T t2;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (TimeUtils.m0(next.getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return t2;
    }

    @DexIgnore
    public WorkoutTetherScreenShotManager j0() {
        return this.i;
    }

    @DexIgnore
    public final void k0(Date date) {
        LiveData<Cf<WorkoutSession>> pagedList;
        r();
        Listing<WorkoutSession> listing = this.s;
        if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
            Cl6 cl6 = this.t;
            if (cl6 != null) {
                pagedList.n((ActivityDetailFragment) cl6);
            } else {
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
            }
        }
        Rm6 unused = Gu7.d(k(), null, null, new Bi(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("ActivityDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Rm6 unused = Gu7.d(k(), null, null, new Fi(this, null), 3, null);
    }

    @DexIgnore
    public void l0() {
        this.t.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("ActivityDetailPresenter", "stop");
        LiveData<H47<List<ActivitySummary>>> liveData = this.q;
        Cl6 cl6 = this.t;
        if (cl6 != null) {
            liveData.n((ActivityDetailFragment) cl6);
            this.r.n((LifecycleOwner) this.t);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
    }

    @DexIgnore
    public final Rm6 m0() {
        return Gu7.d(k(), null, null, new Ei(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Bl6
    public FossilDeviceSerialPatternUtil.DEVICE n() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        Wg6.b(device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    @Override // com.fossil.Bl6
    public Ai5 o() {
        return this.p;
    }

    @DexIgnore
    @Override // com.fossil.Bl6
    public /* bridge */ /* synthetic */ Fd p() {
        return j0();
    }

    @DexIgnore
    @Override // com.fossil.Bl6
    public void q(Date date) {
        Wg6.c(date, "date");
        k0(date);
    }

    @DexIgnore
    @Override // com.fossil.Bl6
    public void r() {
        LiveData<Cf<WorkoutSession>> pagedList;
        try {
            this.x.removePagingListener();
            Listing<WorkoutSession> listing = this.s;
            if (listing != null && (pagedList = listing.getPagedList()) != null) {
                Cl6 cl6 = this.t;
                if (cl6 != null) {
                    pagedList.n((ActivityDetailFragment) cl6);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.activity.ActivityDetailFragment");
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(Cd6.a);
            local.e("ActivityDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.Bl6
    public void s(Bundle bundle) {
        Wg6.c(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.g.getTime());
    }

    @DexIgnore
    @Override // com.fossil.Bl6
    public void t(Date date) {
        Wg6.c(date, "date");
        Rm6 unused = Gu7.d(k(), null, null, new Di(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Bl6
    public void u() {
        Date O = TimeUtils.O(this.g);
        Wg6.b(O, "DateHelper.getNextDate(mDate)");
        t(O);
    }

    @DexIgnore
    @Override // com.fossil.Bl6
    public void v() {
        Date P = TimeUtils.P(this.g);
        Wg6.b(P, "DateHelper.getPrevDate(mDate)");
        t(P);
    }
}
