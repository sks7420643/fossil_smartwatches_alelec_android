package com.portfolio.platform.uirenew.home.customize.diana;

import android.content.Intent;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dp5;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Hq5;
import com.fossil.Jn5;
import com.fossil.Jo5;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Lo5;
import com.fossil.Ls0;
import com.fossil.Mn7;
import com.fossil.Mo5;
import com.fossil.S87;
import com.fossil.U08;
import com.fossil.Uh5;
import com.fossil.Um5;
import com.fossil.W08;
import com.fossil.X66;
import com.fossil.Y66;
import com.fossil.Ym5;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.HomeDianaCustomizeFragment;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.manager.CustomizeRealDataManager;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.usecase.SetPresetUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeDianaCustomizePresenter extends X66 {
    @DexIgnore
    public /* final */ CustomizeRealDataRepository A;
    @DexIgnore
    public /* final */ UserRepository B;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository C;
    @DexIgnore
    public /* final */ DianaPresetRepository D;
    @DexIgnore
    public /* final */ WFAssetRepository E;
    @DexIgnore
    public /* final */ SetPresetUseCase F;
    @DexIgnore
    public LiveData<List<Mo5>> e; // = new MutableLiveData();
    @DexIgnore
    public LiveData<List<CustomizeRealData>> f; // = this.A.getAllRealDataAsLiveData();
    @DexIgnore
    public /* final */ ArrayList<Complication> g; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<WatchApp> h; // = new ArrayList<>();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<Mo5> i; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public Mo5 j;
    @DexIgnore
    public MutableLiveData<String> k;
    @DexIgnore
    public int l; // = -1;
    @DexIgnore
    public CopyOnWriteArrayList<CustomizeRealData> m; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public int n; // = 2;
    @DexIgnore
    public MFUser o;
    @DexIgnore
    public U08 p; // = W08.b(false, 1, null);
    @DexIgnore
    public boolean q;
    @DexIgnore
    public String r; // = "";
    @DexIgnore
    public boolean s;
    @DexIgnore
    public /* final */ Y66 t;
    @DexIgnore
    public /* final */ WatchAppRepository u;
    @DexIgnore
    public /* final */ ComplicationRepository v;
    @DexIgnore
    public /* final */ SetWatchAppUseCase w;
    @DexIgnore
    public /* final */ UserRepository x;
    @DexIgnore
    public /* final */ CustomizeRealDataManager y;
    @DexIgnore
    public /* final */ FileRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$buildConfigsPerPreset$2", f = "HomeDianaCustomizePresenter.kt", l = {227}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Map<String, List<? extends S87>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $presets;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(HomeDianaCustomizePresenter homeDianaCustomizePresenter, List list, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDianaCustomizePresenter;
            this.$presets = list;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, this.$presets, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Map<String, List<? extends S87>>> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v19, types: [java.util.Map] */
        /* JADX WARN: Type inference failed for: r3v7, types: [java.lang.Iterable] */
        /* JADX WARN: Type inference failed for: r4v7, types: [java.util.Map] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0094  */
        /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x003e  */
        /* JADX WARNING: Unknown variable types count: 3 */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r16) {
            /*
            // Method dump skipped, instructions count: 218
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter", f = "HomeDianaCustomizePresenter.kt", l = {Action.Music.MUSIC_END_ACTION, 207}, m = "buildUiPresets")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(HomeDianaCustomizePresenter homeDianaCustomizePresenter, Xe6 xe6) {
            super(xe6);
            this.this$0 = homeDianaCustomizePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$buildUiPresets$configPerPreset$1", f = "HomeDianaCustomizePresenter.kt", l = {208}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Map<String, List<? extends S87>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $uiPresets;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(HomeDianaCustomizePresenter homeDianaCustomizePresenter, List list, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDianaCustomizePresenter;
            this.$uiPresets = list;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$uiPresets, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Map<String, List<? extends S87>>> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.this$0;
                List<Dp5> list = this.$uiPresets;
                this.L$0 = il6;
                this.label = 1;
                Object e0 = homeDianaCustomizePresenter.e0(list, this);
                return e0 == d ? d : e0;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$buildUiPresets$uiPresets$1", f = "HomeDianaCustomizePresenter.kt", l = {203}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Dp5>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(HomeDianaCustomizePresenter homeDianaCustomizePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDianaCustomizePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super List<? extends Dp5>> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v12, types: [java.util.Collection] */
        /* JADX WARN: Type inference failed for: r2v4, types: [java.util.Collection] */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0040  */
        /* JADX WARNING: Unknown variable types count: 2 */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 251
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Di.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements CoroutineUseCase.Ei<SetPresetUseCase.Di, SetPresetUseCase.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ Mo5 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        DianaPresetRepository dianaPresetRepository = this.this$0.this$0.c.D;
                        String str = this.this$0.this$0.b;
                        this.L$0 = il6;
                        this.label = 1;
                        if (dianaPresetRepository.e(str, this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("set new preset to watch success, delete current active ");
                    Mo5 mo5 = this.this$0.a;
                    sb.append(mo5 != null ? mo5.f() : null);
                    local.d("HomeDianaCustomizePresenter", sb.toString());
                    Dv7 i2 = this.this$0.c.i();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (Eu7.g(i2, aiii, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.c.t.w();
                this.this$0.c.t.O(this.this$0.c.j0());
                return Cd6.a;
            }
        }

        @DexIgnore
        public Ei(Mo5 mo5, String str, HomeDianaCustomizePresenter homeDianaCustomizePresenter, String str2) {
            this.a = mo5;
            this.b = str;
            this.c = homeDianaCustomizePresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(SetPresetUseCase.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(SetPresetUseCase.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.c.t.w();
            int b2 = bi.b();
            if (b2 == 1101 || b2 == 1112 || b2 == 1113) {
                List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(bi.a());
                Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                Y66 y66 = this.c.t;
                Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                if (array != null) {
                    Uh5[] uh5Arr = (Uh5[]) array;
                    y66.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.c.t.u();
        }

        @DexIgnore
        public void c(SetPresetUseCase.Di di) {
            Wg6.c(di, "responseValue");
            Rm6 unused = Gu7.d(this.c.k(), null, null, new Aii(this, null), 3, null);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(SetPresetUseCase.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $nextActivePresetId$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Mo5 $preset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    DianaPresetRepository dianaPresetRepository = this.this$0.this$0.D;
                    String e = this.this$0.$preset.e();
                    this.L$0 = il6;
                    this.label = 1;
                    if (dianaPresetRepository.e(e, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(Mo5 mo5, Xe6 xe6, HomeDianaCustomizePresenter homeDianaCustomizePresenter, String str) {
            super(2, xe6);
            this.$preset = mo5;
            this.this$0 = homeDianaCustomizePresenter;
            this.$nextActivePresetId$inlined = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.$preset, xe6, this.this$0, this.$nextActivePresetId$inlined);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(i2, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.t.O(this.this$0.j0() - 1);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter", f = "HomeDianaCustomizePresenter.kt", l = {MFNetworkReturnCode.ITEM_NAME_IN_USED}, m = "generateSharingFaceFromPreset")
    public static final class Gi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(HomeDianaCustomizePresenter homeDianaCustomizePresenter, Xe6 xe6) {
            super(xe6);
            this.this$0 = homeDianaCustomizePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$generateSharingFaceFromPreset$response$1", f = "HomeDianaCustomizePresenter.kt", l = {MFNetworkReturnCode.ITEM_NAME_IN_USED}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<Ku3>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(HomeDianaCustomizePresenter homeDianaCustomizePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDianaCustomizePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Ap4<Ku3>> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                DianaPresetRepository dianaPresetRepository = this.this$0.D;
                Mo5 mo5 = this.this$0.j;
                if (mo5 != null) {
                    String e = mo5.e();
                    this.L$0 = il6;
                    this.label = 1;
                    Object h = dianaPresetRepository.h(e, this);
                    return h == d ? d : h;
                }
                Wg6.i();
                throw null;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$onHomeTabChange$1", f = "HomeDianaCustomizePresenter.kt", l = {616, 494, 503}, m = "invokeSuspend")
    public static final class Ii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $tab;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii<T> implements Comparator<T> {
            @DexIgnore
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return Mn7.c(Boolean.valueOf(t2.m()), Boolean.valueOf(t.m()));
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii<T> implements Comparator<T> {
            @DexIgnore
            public /* final */ /* synthetic */ Comparator b;

            @DexIgnore
            public Bii(Comparator comparator) {
                this.b = comparator;
            }

            @DexIgnore
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                int compare = this.b.compare(t, t2);
                return compare != 0 ? compare : Mn7.c(t.c(), t2.c());
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii<T> implements Comparator<T> {
            @DexIgnore
            public /* final */ /* synthetic */ Comparator b;

            @DexIgnore
            public Cii(Comparator comparator) {
                this.b = comparator;
            }

            @DexIgnore
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                int compare = this.b.compare(t, t2);
                return compare != 0 ? compare : Mn7.c(t.e(), t2.e());
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Dii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Mo5>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ii this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Dii(Xe6 xe6, Ii ii) {
                super(2, xe6);
                this.this$0 = ii;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Dii dii = new Dii(xe6, this.this$0);
                dii.p$ = (Il6) obj;
                throw null;
                //return dii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends Mo5>> xe6) {
                throw null;
                //return ((Dii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    DianaPresetRepository dianaPresetRepository = this.this$0.this$0.D;
                    String J = PortfolioApp.get.instance().J();
                    this.L$0 = il6;
                    this.label = 1;
                    Object o = dianaPresetRepository.o(J, this);
                    return o == d ? d : o;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(HomeDianaCustomizePresenter homeDianaCustomizePresenter, int i, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDianaCustomizePresenter;
            this.$tab = i;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ii ii = new Ii(this.this$0, this.$tab, xe6);
            ii.p$ = (Il6) obj;
            throw null;
            //return ii;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ii) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0070 A[Catch:{ all -> 0x0139 }] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00bd  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 321
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Ii.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter", f = "HomeDianaCustomizePresenter.kt", l = {467, 468, 469, 470, 471, 472}, m = "presetToUIPreset")
    public static final class Ji extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(HomeDianaCustomizePresenter homeDianaCustomizePresenter, Xe6 xe6) {
            super(xe6);
            this.this$0 = homeDianaCustomizePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o0(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$reloadPresetWatchFace$1", f = "HomeDianaCustomizePresenter.kt", l = {581, 583, 585, 592, 597}, m = "invokeSuspend")
    public static final class Ki extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $downloadUrl;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$reloadPresetWatchFace$1$1", f = "HomeDianaCustomizePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $config;
            @DexIgnore
            public /* final */ /* synthetic */ Dp5 $uiDianaPreset;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ki this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ki ki, Dp5 dp5, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ki;
                this.$uiDianaPreset = dp5;
                this.$config = list;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$uiDianaPreset, this.$config, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.t.W4(this.$uiDianaPreset, this.$config);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.e("HomeDianaCustomizePresenter", "update: " + this.$uiDianaPreset + " - config: " + this.$config);
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(HomeDianaCustomizePresenter homeDianaCustomizePresenter, String str, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDianaCustomizePresenter;
            this.$downloadUrl = str;
            this.$id = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ki ki = new Ki(this.this$0, this.$downloadUrl, this.$id, xe6);
            ki.p$ = (Il6) obj;
            throw null;
            //return ki;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ki) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x006e  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x008a  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00cc  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x00f2  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0132  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 323
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Ki.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$renameCurrentPreset$1", f = "HomeDianaCustomizePresenter.kt", l = {275}, m = "invokeSuspend")
    public static final class Li extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $name;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<Mo5>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Mo5 $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Li this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Mo5 mo5, Xe6 xe6, Li li) {
                super(2, xe6);
                this.$it = mo5;
                this.this$0 = li;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$it, xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Kz4<Mo5>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Il6 il6;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    il6 = this.p$;
                    DianaPresetRepository dianaPresetRepository = this.this$0.this$0.D;
                    Mo5 mo5 = this.$it;
                    this.L$0 = il6;
                    this.label = 1;
                    if (dianaPresetRepository.k(mo5, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    il6 = (Il6) this.L$0;
                    El7.b(obj);
                } else if (i == 2) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                DianaPresetRepository dianaPresetRepository2 = this.this$0.this$0.D;
                Jo5 c = Lo5.c(this.$it, this.this$0.this$0.z);
                this.L$0 = il6;
                this.label = 2;
                Object q = dianaPresetRepository2.q(c, this);
                return q == d ? d : q;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Li(HomeDianaCustomizePresenter homeDianaCustomizePresenter, String str, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDianaCustomizePresenter;
            this.$presetId = str;
            this.$name = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Li li = new Li(this.this$0, this.$presetId, this.$name, xe6);
            li.p$ = (Il6) obj;
            throw null;
            //return li;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Li) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Iterator it = this.this$0.i.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    Object next = it.next();
                    if (Ao7.a(Wg6.a(((Mo5) next).e(), this.$presetId)).booleanValue()) {
                        obj2 = next;
                        break;
                    }
                }
                Mo5 mo5 = (Mo5) obj2;
                Mo5 a2 = mo5 != null ? Lo5.a(mo5) : null;
                if (a2 != null) {
                    a2.r(this.$name);
                    a2.s(2);
                    Dv7 b = Bw7.b();
                    Aii aii = new Aii(a2, null, this);
                    this.L$0 = il6;
                    this.L$1 = a2;
                    this.L$2 = a2;
                    this.label = 1;
                    g = Eu7.g(b, aii, this);
                    if (g == d) {
                        return d;
                    }
                }
                return Cd6.a;
            } else if (i == 1) {
                Mo5 mo52 = (Mo5) this.L$2;
                Mo5 mo53 = (Mo5) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Kz4 kz4 = (Kz4) g;
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Mo5 $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Jo5>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Mi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Mi mi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = mi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Jo5> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    Mi mi = this.this$0;
                    return Lo5.c(mi.$it, mi.this$0.z);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<DianaWatchFaceUser>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Jo5 $preset;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Mi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Mi mi, Jo5 jo5, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = mi;
                this.$preset = jo5;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, this.$preset, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ap4<DianaWatchFaceUser>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    DianaWatchFaceRepository dianaWatchFaceRepository = this.this$0.this$0.C;
                    String e = Ym5.e(24);
                    Wg6.b(e, "StringHelper.randomUUID(24)");
                    String c = this.$preset.c();
                    String f = this.this$0.$it.f();
                    this.L$0 = il6;
                    this.label = 1;
                    Object createWatchFace$default = DianaWatchFaceRepository.createWatchFace$default(dianaWatchFaceRepository, e, c, f, null, this, 8, null);
                    return createWatchFace$default == d ? d : createWatchFace$default;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Ap4 $result;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Mi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ DianaWatchFaceUser $localWatchFace;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Cii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Cii cii, DianaWatchFaceUser dianaWatchFaceUser, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = cii;
                    this.$localWatchFace = dianaWatchFaceUser;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$localWatchFace, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        this.this$0.this$0.this$0.t.w();
                        this.this$0.this$0.this$0.t.C0(this.$localWatchFace.getId());
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Biii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Cii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Cii cii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = cii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Biii biii = new Biii(this.this$0, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
                    //return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    String str;
                    Integer code;
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        this.this$0.this$0.this$0.t.w();
                        Y66 y66 = this.this$0.this$0.this$0.t;
                        ServerError c = ((Hq5) this.this$0.$result).c();
                        int intValue = (c == null || (code = c.getCode()) == null) ? -1 : code.intValue();
                        ServerError c2 = ((Hq5) this.this$0.$result).c();
                        if (c2 == null || (str = c2.getMessage()) == null) {
                            str = "";
                        }
                        y66.E5(intValue, str);
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Mi mi, Ap4 ap4, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = mi;
                this.$result = ap4;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, this.$result, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:13:0x006a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r15) {
                /*
                // Method dump skipped, instructions count: 291
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Mi.Cii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Mi(Mo5 mo5, Xe6 xe6, HomeDianaCustomizePresenter homeDianaCustomizePresenter) {
            super(2, xe6);
            this.$it = mo5;
            this.this$0 = homeDianaCustomizePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Mi mi = new Mi(this.$it, xe6, this.this$0);
            mi.p$ = (Il6) obj;
            throw null;
            //return mi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Mi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x006e  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 1
                r4 = 2
                r2 = 0
                java.lang.Object r5 = com.fossil.Yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0070
                if (r0 == r6) goto L_0x004d
                if (r0 != r4) goto L_0x0045
                java.lang.Object r0 = r7.L$1
                com.fossil.Jo5 r0 = (com.fossil.Jo5) r0
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
                r0 = r8
            L_0x001b:
                r5 = r0
                com.mapped.Ap4 r5 = (com.mapped.Ap4) r5
                boolean r0 = r5 instanceof com.fossil.Kq5
                if (r0 == 0) goto L_0x0099
                com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r0 = r7.this$0
                com.fossil.Y66 r0 = com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.V(r0)
                r0.w()
                com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r0 = r7.this$0
                com.fossil.Y66 r1 = com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.V(r0)
                com.fossil.Kq5 r5 = (com.fossil.Kq5) r5
                java.lang.Object r0 = r5.a()
                if (r0 == 0) goto L_0x0095
                com.portfolio.platform.data.model.watchface.DianaWatchFaceUser r0 = (com.portfolio.platform.data.model.watchface.DianaWatchFaceUser) r0
                java.lang.String r0 = r0.getId()
                r1.C0(r0)
            L_0x0042:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0044:
                return r0
            L_0x0045:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x004d:
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
                r3 = r0
                r1 = r8
            L_0x0056:
                r0 = r1
                com.fossil.Jo5 r0 = (com.fossil.Jo5) r0
                com.fossil.Dv7 r1 = com.fossil.Bw7.b()
                com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Mi$Bii r6 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Mi$Bii
                r6.<init>(r7, r0, r2)
                r7.L$0 = r3
                r7.L$1 = r0
                r7.label = r4
                java.lang.Object r0 = com.fossil.Eu7.g(r1, r6, r7)
                if (r0 != r5) goto L_0x001b
                r0 = r5
                goto L_0x0044
            L_0x0070:
                com.fossil.El7.b(r8)
                com.mapped.Il6 r0 = r7.p$
                com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r1 = r7.this$0
                com.fossil.Y66 r1 = com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.V(r1)
                java.lang.String r3 = ""
                r1.W3(r3)
                com.fossil.Dv7 r1 = com.fossil.Bw7.b()
                com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Mi$Aii r3 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Mi$Aii
                r3.<init>(r7, r2)
                r7.L$0 = r0
                r7.label = r6
                java.lang.Object r1 = com.fossil.Eu7.g(r1, r3, r7)
                if (r1 != r5) goto L_0x00b1
                r0 = r5
                goto L_0x0044
            L_0x0095:
                com.mapped.Wg6.i()
                throw r2
            L_0x0099:
                boolean r0 = r5 instanceof com.fossil.Hq5
                if (r0 == 0) goto L_0x0042
                com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r0 = r7.this$0
                com.mapped.Il6 r0 = com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.X(r0)
                com.fossil.Dv7 r1 = com.fossil.Bw7.b()
                com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Mi$Cii r3 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Mi$Cii
                r3.<init>(r7, r5, r2)
                r5 = r2
                com.fossil.Eu7.d(r0, r1, r2, r3, r4, r5)
                goto L_0x0042
            L_0x00b1:
                r3 = r0
                goto L_0x0056
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Mi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements CoroutineUseCase.Ei<SetPresetUseCase.Di, SetPresetUseCase.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter a;

        @DexIgnore
        public Ni(HomeDianaCustomizePresenter homeDianaCustomizePresenter) {
            this.a = homeDianaCustomizePresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(SetPresetUseCase.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(SetPresetUseCase.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.t.w();
            int b = bi.b();
            if (b == 1101 || b == 1112 || b == 1113) {
                List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(bi.a());
                Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                Y66 y66 = this.a.t;
                Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                if (array != null) {
                    Uh5[] uh5Arr = (Uh5[]) array;
                    y66.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            } else if (b == 1941) {
                this.a.t.E5(601, "");
            } else if (b != 1942) {
                this.a.t.u();
            } else {
                this.a.t.E5(500, "");
            }
        }

        @DexIgnore
        public void c(SetPresetUseCase.Di di) {
            Wg6.c(di, "responseValue");
            this.a.t.w();
            this.a.t.m0(0);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(SetPresetUseCase.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$shareCurrentWF$1", f = "HomeDianaCustomizePresenter.kt", l = {397, MFNetworkReturnCode.BAD_REQUEST}, m = "invokeSuspend")
    public static final class Oi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Oi(HomeDianaCustomizePresenter homeDianaCustomizePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDianaCustomizePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Oi oi = new Oi(this.this$0, xe6);
            oi.p$ = (Il6) obj;
            throw null;
            //return oi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Oi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.t.W3("");
                Mo5 mo5 = this.this$0.j;
                if (mo5 == null) {
                    Wg6.i();
                    throw null;
                } else if (mo5.h() == 0) {
                    HomeDianaCustomizePresenter homeDianaCustomizePresenter = this.this$0;
                    this.L$0 = il6;
                    this.label = 1;
                    if (homeDianaCustomizePresenter.i0(this) == d) {
                        return d;
                    }
                } else {
                    FLogger.INSTANCE.getLocal().e("HomeDianaCustomizePresenter", "shareCurrentWF - try to sync");
                    HomeDianaCustomizePresenter homeDianaCustomizePresenter2 = this.this$0;
                    this.L$0 = il6;
                    this.label = 2;
                    if (homeDianaCustomizePresenter2.q0(this) == d) {
                        return d;
                    }
                }
            } else if (i == 1 || i == 2) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1", f = "HomeDianaCustomizePresenter.kt", l = {116}, m = "invokeSuspend")
    public static final class Pi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$1", f = "HomeDianaCustomizePresenter.kt", l = {118, 120}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Pi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Pi pi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = pi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x008b  */
            /* JADX WARNING: Removed duplicated region for block: B:15:0x00bb  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 2
                    r3 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r6.label
                    if (r0 == 0) goto L_0x008d
                    if (r0 == r3) goto L_0x0051
                    if (r0 != r5) goto L_0x0049
                    java.lang.Object r0 = r6.L$1
                    java.util.ArrayList r0 = (java.util.ArrayList) r0
                    java.lang.Object r1 = r6.L$0
                    com.mapped.Il6 r1 = (com.mapped.Il6) r1
                    com.fossil.El7.b(r7)
                    r1 = r7
                    r2 = r0
                L_0x001b:
                    r0 = r1
                    java.util.Collection r0 = (java.util.Collection) r0
                    r2.addAll(r0)
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Pi r0 = r6.this$0
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r0 = r0.this$0
                    java.util.concurrent.CopyOnWriteArrayList r0 = com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.J(r0)
                    r0.clear()
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Pi r0 = r6.this$0
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r0 = r0.this$0
                    java.util.concurrent.CopyOnWriteArrayList r0 = com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.J(r0)
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Pi r1 = r6.this$0
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r1 = r1.this$0
                    com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository r1 = com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.L(r1)
                    java.util.List r1 = r1.getAllRealDataRaw()
                    boolean r0 = r0.addAll(r1)
                    java.lang.Boolean r0 = com.fossil.Ao7.a(r0)
                L_0x0048:
                    return r0
                L_0x0049:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0051:
                    java.lang.Object r0 = r6.L$1
                    java.util.ArrayList r0 = (java.util.ArrayList) r0
                    java.lang.Object r1 = r6.L$0
                    com.mapped.Il6 r1 = (com.mapped.Il6) r1
                    com.fossil.El7.b(r7)
                    r3 = r0
                    r2 = r7
                L_0x005e:
                    r0 = r2
                    java.util.Collection r0 = (java.util.Collection) r0
                    r3.addAll(r0)
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Pi r0 = r6.this$0
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r0 = r0.this$0
                    java.util.ArrayList r0 = com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.A(r0)
                    r0.clear()
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Pi r0 = r6.this$0
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r0 = r0.this$0
                    java.util.ArrayList r0 = com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.A(r0)
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Pi r2 = r6.this$0
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r2 = r2.this$0
                    com.portfolio.platform.data.source.WatchAppRepository r2 = com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.W(r2)
                    r6.L$0 = r1
                    r6.L$1 = r0
                    r6.label = r5
                    java.lang.Object r1 = r2.getAllWatchAppRaw(r6)
                    if (r1 != r4) goto L_0x00bb
                    r0 = r4
                    goto L_0x0048
                L_0x008d:
                    com.fossil.El7.b(r7)
                    com.mapped.Il6 r1 = r6.p$
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Pi r0 = r6.this$0
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r0 = r0.this$0
                    java.util.ArrayList r0 = com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.z(r0)
                    r0.clear()
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Pi r0 = r6.this$0
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r0 = r0.this$0
                    java.util.ArrayList r0 = com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.z(r0)
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Pi r2 = r6.this$0
                    com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r2 = r2.this$0
                    com.portfolio.platform.data.source.ComplicationRepository r2 = com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.G(r2)
                    r6.L$0 = r1
                    r6.L$1 = r0
                    r6.label = r3
                    java.lang.Object r2 = r2.getAllComplicationRaw(r6)
                    if (r2 != r4) goto L_0x00be
                    r0 = r4
                    goto L_0x0048
                L_0x00bb:
                    r2 = r0
                    goto L_0x001b
                L_0x00be:
                    r3 = r0
                    goto L_0x005e
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Pi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii<T> implements Ls0<List<? extends CustomizeRealData>> {
            @DexIgnore
            public /* final */ /* synthetic */ Pi a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $it;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Bii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Xe6 xe6, Aiii aiii) {
                        super(2, xe6);
                        this.this$0 = aiii;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        Aiiii aiiii = new Aiiii(xe6, this.this$0);
                        aiiii.p$ = (Il6) obj;
                        throw null;
                        //return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                        throw null;
                        //return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = Yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            El7.b(obj);
                            Il6 il6 = this.p$;
                            UserRepository userRepository = this.this$0.this$0.a.this$0.B;
                            this.L$0 = il6;
                            this.label = 1;
                            Object currentUser = userRepository.getCurrentUser(this);
                            return currentUser == d ? d : currentUser;
                        } else if (i == 1) {
                            Il6 il62 = (Il6) this.L$0;
                            El7.b(obj);
                            return obj;
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(List list, Xe6 xe6, Bii bii) {
                    super(2, xe6);
                    this.$it = list;
                    this.this$0 = bii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.$it, xe6, this.this$0);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:24:0x00bd A[Catch:{ all -> 0x0123 }] */
                /* JADX WARNING: Removed duplicated region for block: B:30:0x00f7  */
                /* JADX WARNING: Removed duplicated region for block: B:34:0x0118  */
                @Override // com.fossil.Zn7
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                    /*
                    // Method dump skipped, instructions count: 296
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Pi.Bii.Aiii.invokeSuspend(java.lang.Object):java.lang.Object");
                }
            }

            @DexIgnore
            public Bii(Pi pi) {
                this.a = pi;
            }

            @DexIgnore
            public final void a(List<CustomizeRealData> list) {
                if (list != null) {
                    Rm6 unused = Gu7.d(this.a.this$0.k(), null, null, new Aiii(list, null, this), 3, null);
                }
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(List<? extends CustomizeRealData> list) {
                a(list);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii<T> implements Ls0<String> {
            @DexIgnore
            public /* final */ /* synthetic */ Pi a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$3$1", f = "HomeDianaCustomizePresenter.kt", l = {616, 152, 153}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $it;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Cii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii<T> implements Ls0<List<? extends Mo5>> {
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii a;

                    @DexIgnore
                    public Aiiii(Aiii aiii) {
                        this.a = aiii;
                    }

                    @DexIgnore
                    public final void a(List<Mo5> list) {
                        if (list != null) {
                            Rm6 unused = Gu7.d(this.a.this$0.a.this$0.k(), null, null, new HomeDianaCustomizePresenter$p$c$a$a$a(list, null, this), 3, null);
                        }
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // com.fossil.Ls0
                    public /* bridge */ /* synthetic */ void onChanged(List<? extends Mo5> list) {
                        a(list);
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Cii cii, String str, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = cii;
                    this.$it = str;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$it, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                /* JADX WARNING: Removed duplicated region for block: B:27:0x00b6  */
                /* JADX WARNING: Removed duplicated region for block: B:32:0x00e9  */
                /* JADX WARNING: Removed duplicated region for block: B:36:0x0109  */
                @Override // com.fossil.Zn7
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                    /*
                    // Method dump skipped, instructions count: 283
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Pi.Cii.Aiii.invokeSuspend(java.lang.Object):java.lang.Object");
                }
            }

            @DexIgnore
            public Cii(Pi pi) {
                this.a = pi;
            }

            @DexIgnore
            public final void a(String str) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("HomeDianaCustomizePresenter", "on active serial change " + str + " mCurrentHomeTab " + this.a.this$0.n);
                if (TextUtils.isEmpty(str) || !FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
                    this.a.this$0.t.r(true);
                } else {
                    Rm6 unused = Gu7.d(this.a.this$0.k(), null, null, new Aiii(this, str, null), 3, null);
                }
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(String str) {
                a(str);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Pi(HomeDianaCustomizePresenter homeDianaCustomizePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDianaCustomizePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Pi pi = new Pi(this.this$0, xe6);
            pi.p$ = (Il6) obj;
            throw null;
            //return pi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Pi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                if (this.this$0.g.isEmpty() || this.this$0.h.isEmpty()) {
                    FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "init comps, apps");
                    Dv7 h = this.this$0.h();
                    Aii aii = new Aii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (Eu7.g(h, aii, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            LiveData liveData = this.this$0.f;
            Y66 y66 = this.this$0.t;
            if (y66 != null) {
                liveData.h((HomeDianaCustomizeFragment) y66, new Bii(this));
                MutableLiveData mutableLiveData = this.this$0.k;
                Y66 y662 = this.this$0.t;
                if (y662 != null) {
                    mutableLiveData.h((HomeDianaCustomizeFragment) y662, new Cii(this));
                    this.this$0.w.o();
                    BleCommandResultManager.d.g(CommunicateMode.SET_PRESET_APPS_DATA);
                    return Cd6.a;
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter", f = "HomeDianaCustomizePresenter.kt", l = {433}, m = "syncPresetToServer")
    public static final class Qi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Qi(HomeDianaCustomizePresenter homeDianaCustomizePresenter, Xe6 xe6) {
            super(xe6);
            this.this$0 = homeDianaCustomizePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.q0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$syncPresetToServer$2", f = "HomeDianaCustomizePresenter.kt", l = {436, 443, 445}, m = "invokeSuspend")
    public static final class Ri extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeDianaCustomizePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$syncPresetToServer$2$1", f = "HomeDianaCustomizePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ri this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ri ri, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ri;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.t.w();
                    this.this$0.this$0.t.g2(null, 404);
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ri(HomeDianaCustomizePresenter homeDianaCustomizePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeDianaCustomizePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ri ri = new Ri(this.this$0, xe6);
            ri.p$ = (Il6) obj;
            throw null;
            //return ri;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ri) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:42:0x0180  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 399
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Ri.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public HomeDianaCustomizePresenter(Y66 y66, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, SetWatchAppUseCase setWatchAppUseCase, UserRepository userRepository, CustomizeRealDataManager customizeRealDataManager, FileRepository fileRepository, CustomizeRealDataRepository customizeRealDataRepository, UserRepository userRepository2, DianaWatchFaceRepository dianaWatchFaceRepository, PortfolioApp portfolioApp, DianaPresetRepository dianaPresetRepository, WFAssetRepository wFAssetRepository, SetPresetUseCase setPresetUseCase) {
        Wg6.c(y66, "mView");
        Wg6.c(watchAppRepository, "mWatchAppRepository");
        Wg6.c(complicationRepository, "mComplicationRepository");
        Wg6.c(setWatchAppUseCase, "mSetWatchAppUseCase");
        Wg6.c(userRepository, "userRepository");
        Wg6.c(customizeRealDataManager, "customizeRealDataManager");
        Wg6.c(fileRepository, "mFileRepository");
        Wg6.c(customizeRealDataRepository, "mCustomizeRealDataRepository");
        Wg6.c(userRepository2, "mUserRepository");
        Wg6.c(dianaWatchFaceRepository, "mDianaWatchFaceRepository");
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(dianaPresetRepository, "dianaPresetRepository");
        Wg6.c(wFAssetRepository, "wfAssetRepository");
        Wg6.c(setPresetUseCase, "mSetDianaPresetUseCase");
        this.t = y66;
        this.u = watchAppRepository;
        this.v = complicationRepository;
        this.w = setWatchAppUseCase;
        this.x = userRepository;
        this.y = customizeRealDataManager;
        this.z = fileRepository;
        this.A = customizeRealDataRepository;
        this.B = userRepository2;
        this.C = dianaWatchFaceRepository;
        this.D = dianaPresetRepository;
        this.E = wFAssetRepository;
        this.F = setPresetUseCase;
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "init comps, apps first time");
        this.k = portfolioApp.K();
    }

    @DexIgnore
    public final /* synthetic */ Object e0(List<Dp5> list, Xe6<? super Map<String, List<S87>>> xe6) {
        return Eu7.g(Bw7.a(), new Ai(this, list, null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object f0(com.mapped.Xe6<? super com.mapped.Cd6> r9) {
        /*
            r8 = this;
            r7 = 0
            r6 = 2
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Bi
            if (r0 == 0) goto L_0x003c
            r0 = r9
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Bi r0 = (com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Bi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003c
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0016:
            java.lang.Object r3 = r2.result
            java.lang.Object r4 = com.fossil.Yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0076
            if (r0 == r5) goto L_0x004b
            if (r0 != r6) goto L_0x0043
            java.lang.Object r0 = r2.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r1 = r2.L$0
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r1 = (com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter) r1
            com.fossil.El7.b(r3)
            r2 = r3
            r4 = r0
        L_0x0031:
            r0 = r2
            java.util.Map r0 = (java.util.Map) r0
            com.fossil.Y66 r1 = r1.t
            r1.F5(r4, r0)
        L_0x0039:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x003b:
            return r0
        L_0x003c:
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Bi r0 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Bi
            r0.<init>(r8, r9)
            r2 = r0
            goto L_0x0016
        L_0x0043:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004b:
            java.lang.Object r0 = r2.L$0
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r0 = (com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter) r0
            com.fossil.El7.b(r3)
            r1 = r3
            r8 = r0
        L_0x0054:
            r0 = r1
            java.util.List r0 = (java.util.List) r0
            boolean r1 = r0.isEmpty()
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x0039
            com.fossil.Dv7 r1 = com.fossil.Bw7.a()
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Ci r3 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Ci
            r3.<init>(r8, r0, r7)
            r2.L$0 = r8
            r2.L$1 = r0
            r2.label = r6
            java.lang.Object r2 = com.fossil.Eu7.g(r1, r3, r2)
            if (r2 != r4) goto L_0x00ac
            r0 = r4
            goto L_0x003b
        L_0x0076:
            com.fossil.El7.b(r3)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "buildUiPresets, preset = "
            r1.append(r3)
            java.util.concurrent.CopyOnWriteArrayList<com.fossil.Mo5> r3 = r8.i
            r1.append(r3)
            java.lang.String r3 = "HomeDianaCustomizePresenter"
            java.lang.String r1 = r1.toString()
            r0.d(r3, r1)
            com.fossil.Dv7 r0 = com.fossil.Bw7.b()
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Di r1 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Di
            r1.<init>(r8, r7)
            r2.L$0 = r8
            r2.label = r5
            java.lang.Object r1 = com.fossil.Eu7.g(r0, r1, r2)
            if (r1 != r4) goto L_0x0054
            r0 = r4
            goto L_0x003b
        L_0x00ac:
            r4 = r0
            r1 = r8
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.f0(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public void g0() {
        this.t.K1();
    }

    @DexIgnore
    public final void h0(String str) {
        T t2;
        T t3;
        String str2;
        List<Mo5> e2 = this.e.e();
        if (e2 != null) {
            e2.isEmpty();
        }
        Iterator<T> it = this.i.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (Wg6.a(next.e(), str)) {
                t2 = next;
                break;
            }
        }
        T t4 = t2;
        if (t4 != null) {
            Iterator<T> it2 = this.i.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t3 = null;
                    break;
                }
                T next2 = it2.next();
                if (next2.m()) {
                    t3 = next2;
                    break;
                }
            }
            T t5 = t3;
            if (t5 == null || (str2 = t5.e()) == null) {
                str2 = "";
            }
            this.t.W3("");
            this.F.e(new SetPresetUseCase.Ci(t4.e()), new Ei(t5, str2, this, str));
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object i0(com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r6 = this;
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 0
            boolean r0 = r7 instanceof com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Gi
            if (r0 == 0) goto L_0x0072
            r0 = r7
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Gi r0 = (com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Gi) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0072
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0080
            if (r4 != r5) goto L_0x0078
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r0 = (com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter) r0
            com.fossil.El7.b(r1)
            r6 = r0
        L_0x0028:
            r0 = r1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x00af
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "shareCurrentWF - success: "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r4 = "HomeDianaCustomizePresenter"
            java.lang.String r3 = r3.toString()
            r1.d(r4, r3)
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            com.mapped.Ku3 r0 = (com.mapped.Ku3) r0
            if (r0 == 0) goto L_0x0098
            java.lang.String r1 = "shareableLink"
            com.google.gson.JsonElement r0 = r0.p(r1)
            if (r0 == 0) goto L_0x0098
            java.lang.String r0 = r0.f()
        L_0x0061:
            com.fossil.Y66 r1 = r6.t
            r1.w()
            if (r0 == 0) goto L_0x009a
            com.fossil.Y66 r1 = r6.t
            r2 = 200(0xc8, float:2.8E-43)
            r1.g2(r0, r2)
        L_0x006f:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0071:
            return r0
        L_0x0072:
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Gi r0 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Gi
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0078:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0080:
            com.fossil.El7.b(r1)
            com.fossil.Dv7 r1 = com.fossil.Bw7.b()
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Hi r4 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Hi
            r4.<init>(r6, r2)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r1 = com.fossil.Eu7.g(r1, r4, r0)
            if (r1 != r3) goto L_0x0028
            r0 = r3
            goto L_0x0071
        L_0x0098:
            r0 = r2
            goto L_0x0061
        L_0x009a:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = "HomeDianaCustomizePresenter"
            java.lang.String r3 = "shareCurrentWF - sharableLink is null"
            r0.e(r1, r3)
            com.fossil.Y66 r0 = r6.t
            r1 = 404(0x194, float:5.66E-43)
            r0.g2(r2, r1)
            goto L_0x006f
        L_0x00af:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x006f
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "shareCurrentWF - failure: "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r4 = "HomeDianaCustomizePresenter"
            java.lang.String r3 = r3.toString()
            r1.e(r4, r3)
            com.fossil.Y66 r1 = r6.t
            r1.w()
            com.fossil.Y66 r1 = r6.t
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            int r0 = r0.a()
            r1.g2(r2, r0)
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.i0(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final int j0() {
        return this.l;
    }

    @DexIgnore
    public boolean k0() {
        return this.q;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Rm6 unused = Gu7.d(k(), null, null, new Pi(this, null), 3, null);
    }

    @DexIgnore
    public void l0(int i2, int i3, Intent intent) {
        if (i2 == 100 && i3 == -1) {
            this.t.c0(0);
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        try {
            MutableLiveData<String> mutableLiveData = this.k;
            Y66 y66 = this.t;
            if (y66 != null) {
                mutableLiveData.n((HomeDianaCustomizeFragment) y66);
                this.e.n((LifecycleOwner) this.t);
                this.f.n((LifecycleOwner) this.t);
                this.w.r();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizePresenter", "stop fail due to " + e2);
        }
    }

    @DexIgnore
    public void m0(int i2) {
        this.n = i2;
        Rm6 unused = Gu7.d(k(), null, null, new Ii(this, i2, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.X66
    public void n(int i2) {
        if (this.i.size() > i2) {
            this.l = i2;
            this.j = this.i.get(i2);
        }
    }

    @DexIgnore
    public void n0() {
        FLogger.INSTANCE.getLocal().e("HomeDianaCustomizePresenter", "onUpdateFwComplete ");
        this.t.M4();
    }

    @DexIgnore
    @Override // com.fossil.X66
    public void o() {
        if (this.i.size() < 2 || this.j == null) {
            this.t.h1();
            return;
        }
        CopyOnWriteArrayList<Mo5> copyOnWriteArrayList = this.i;
        ArrayList arrayList = new ArrayList();
        for (T t2 : copyOnWriteArrayList) {
            String e2 = t2.e();
            Mo5 mo5 = this.j;
            if (mo5 == null) {
                Wg6.i();
                throw null;
            } else if (!Wg6.a(e2, mo5.e())) {
                arrayList.add(t2);
            }
        }
        Mo5 mo52 = (Mo5) arrayList.get(0);
        Y66 y66 = this.t;
        Mo5 mo53 = this.j;
        if (mo53 != null) {
            boolean m2 = mo53.m();
            Mo5 mo54 = this.j;
            if (mo54 != null) {
                y66.r0(m2, mo54.f(), mo52.f(), mo52.e());
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0150  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01a2  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01d7  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01db  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0218  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0222  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x02b4  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x02b8  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x02bf  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x02c6  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object o0(com.fossil.Mo5 r20, com.mapped.Xe6<? super com.fossil.Dp5> r21) {
        /*
        // Method dump skipped, instructions count: 758
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.o0(com.fossil.Mo5, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.X66
    public void p(String str) {
        Wg6.c(str, "nextActivePresetId");
        Mo5 mo5 = this.j;
        if (mo5 == null) {
            return;
        }
        if (mo5.m()) {
            h0(str);
        } else {
            Rm6 unused = Gu7.d(k(), null, null, new Fi(mo5, null, this, str), 3, null);
        }
    }

    @DexIgnore
    public void p0() {
        this.t.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.X66
    public String q() {
        String e2;
        Mo5 mo5 = this.j;
        return (mo5 == null || (e2 = mo5.e()) == null) ? "" : e2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object q0(com.mapped.Xe6<? super com.mapped.Cd6> r8) {
        /*
            r7 = this;
            r6 = 0
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Qi
            if (r0 == 0) goto L_0x002a
            r0 = r8
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Qi r0 = (com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.Qi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002a
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0038
            if (r3 != r5) goto L_0x0030
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter r0 = (com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter) r0
            com.fossil.El7.b(r2)
        L_0x0027:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0029:
            return r0
        L_0x002a:
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Qi r0 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Qi
            r0.<init>(r7, r8)
            goto L_0x0014
        L_0x0030:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0038:
            com.fossil.El7.b(r2)
            com.fossil.Mo5 r2 = r7.j
            if (r2 == 0) goto L_0x0072
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "syncPresetToServer - current preset: "
            r3.append(r4)
            com.fossil.Mo5 r4 = r7.j
            r3.append(r4)
            java.lang.String r4 = "HomeDianaCustomizePresenter"
            java.lang.String r3 = r3.toString()
            r2.d(r4, r3)
            com.fossil.Dv7 r2 = com.fossil.Bw7.b()
            com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Ri r3 = new com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$Ri
            r3.<init>(r7, r6)
            r0.L$0 = r7
            r0.label = r5
            java.lang.Object r0 = com.fossil.Eu7.g(r2, r3, r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0029
        L_0x0072:
            com.fossil.Y66 r0 = r7.t
            r0.w()
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = "HomeDianaCustomizePresenter"
            java.lang.String r2 = "syncPresetToServer - current preset is null"
            r0.e(r1, r2)
            com.fossil.Y66 r0 = r7.t
            r1 = 503(0x1f7, float:7.05E-43)
            r0.g2(r6, r1)
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter.q0(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.X66
    public void r(String str) {
        Wg6.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizePresenter", "navigate to " + str);
        this.r = str;
    }

    @DexIgnore
    @Override // com.fossil.X66
    public void s(String str, String str2) {
        Wg6.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("HomeDianaCustomizePresenter", "reloadPresetWatchFace - id: " + str + " - downloadUrl: " + str2);
        if (str2 != null) {
            Rm6 unused = Gu7.d(k(), Bw7.b(), null, new Ki(this, str2, str, null), 2, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.X66
    public void t(String str, String str2) {
        Wg6.c(str, "name");
        Wg6.c(str2, "presetId");
        Rm6 unused = Gu7.d(k(), null, null, new Li(this, str2, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.X66
    public String u() {
        return this.r;
    }

    @DexIgnore
    @Override // com.fossil.X66
    public void v() {
        Mo5 mo5 = this.j;
        if (mo5 != null) {
            Rm6 unused = Gu7.d(k(), null, null, new Mi(mo5, null, this), 3, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.X66
    public void w() {
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizePresenter", "setPresetToWatch check permission BLE");
        Jn5 jn5 = Jn5.b;
        Y66 y66 = this.t;
        if (y66 == null) {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        } else if (Jn5.c(jn5, ((HomeDianaCustomizeFragment) y66).getContext(), Jn5.Ai.SET_BLE_COMMAND, false, false, false, null, 60, null)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizePresenter", "setPresetToWatch currentPreset " + this.j);
            Mo5 mo5 = this.j;
            if (mo5 != null) {
                Y66 y662 = this.t;
                String c = Um5.c(PortfolioApp.get.instance(), 2131886814);
                Wg6.b(c, "LanguageHelper.getString\u2026on_Text__ApplyingToWatch)");
                y662.W3(c);
                this.F.e(new SetPresetUseCase.Ci(mo5.e()), new Ni(this));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.X66
    public void x() {
        if (this.j != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("shareCurrentWF - id: ");
            Mo5 mo5 = this.j;
            if (mo5 != null) {
                sb.append(mo5.e());
                local.d("HomeDianaCustomizePresenter", sb.toString());
                Rm6 unused = Gu7.d(k(), null, null, new Oi(this, null), 3, null);
                return;
            }
            Wg6.i();
            throw null;
        }
        this.t.g2(null, 503);
    }

    @DexIgnore
    @Override // com.fossil.X66
    public void y(boolean z2) {
        this.q = z2;
    }
}
