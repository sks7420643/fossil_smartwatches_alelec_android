package com.portfolio.platform.uirenew.home.dashboard.sleep;

import androidx.lifecycle.LiveData;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Kj6;
import com.fossil.Ko7;
import com.fossil.Lj6;
import com.fossil.Ls0;
import com.fossil.Mj6;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Cf;
import com.mapped.Coroutine;
import com.mapped.Gg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.PagingRequestHelper;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardSleepPresenter extends Kj6 {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<SleepSummary> f;
    @DexIgnore
    public /* final */ Lj6 g;
    @DexIgnore
    public /* final */ SleepSummariesRepository h;
    @DexIgnore
    public /* final */ SleepSessionsRepository i;
    @DexIgnore
    public /* final */ FitnessDataRepository j;
    @DexIgnore
    public /* final */ UserRepository k;
    @DexIgnore
    public /* final */ U04 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$1", f = "DashboardSleepPresenter.kt", l = {65, 73}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DashboardSleepPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements PagingRequestHelper.Ai {
            @DexIgnore
            public /* final */ /* synthetic */ Ai a;

            @DexIgnore
            public Aii(Ai ai) {
                this.a = ai;
            }

            @DexIgnore
            @Override // com.mapped.PagingRequestHelper.Ai
            public final void e(PagingRequestHelper.Gi gi) {
                Wg6.c(gi, "report");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DashboardSleepPresenter", "onStatusChange status=" + gi);
                if (gi.b()) {
                    this.a.this$0.y().d();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii<T> implements Ls0<Cf<SleepSummary>> {
            @DexIgnore
            public /* final */ /* synthetic */ Ai a;

            @DexIgnore
            public Bii(Ai ai) {
                this.a = ai;
            }

            @DexIgnore
            public final void a(Cf<SleepSummary> cf) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getSummariesPaging observer size=");
                sb.append(cf != null ? Integer.valueOf(cf.size()) : null);
                local.d("DashboardSleepPresenter", sb.toString());
                if (cf != null) {
                    this.a.this$0.y().q(cf);
                }
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(Cf<SleepSummary> cf) {
                a(cf);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$initDataSource$1$user$1", f = "DashboardSleepPresenter.kt", l = {65}, m = "invokeSuspend")
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.k;
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(DashboardSleepPresenter dashboardSleepPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dashboardSleepPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0066  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                r11 = this;
                r10 = 2
                r4 = 1
                java.lang.Object r9 = com.fossil.Yn7.d()
                int r0 = r11.label
                if (r0 == 0) goto L_0x00a5
                if (r0 == r4) goto L_0x0058
                if (r0 != r10) goto L_0x0050
                java.lang.Object r0 = r11.L$4
                com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter r0 = (com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter) r0
                java.lang.Object r1 = r11.L$3
                java.util.Date r1 = (java.util.Date) r1
                java.lang.Object r1 = r11.L$2
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r11.L$1
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r11.L$0
                com.mapped.Il6 r1 = (com.mapped.Il6) r1
                com.fossil.El7.b(r12)
                r1 = r12
                r2 = r0
            L_0x0027:
                r0 = r1
                com.portfolio.platform.data.Listing r0 = (com.portfolio.platform.data.Listing) r0
                com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter.x(r2, r0)
                com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter r0 = r11.this$0
                com.fossil.Lj6 r0 = r0.y()
                com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter r1 = r11.this$0
                com.portfolio.platform.data.Listing r1 = com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter.u(r1)
                if (r1 == 0) goto L_0x004d
                androidx.lifecycle.LiveData r1 = r1.getPagedList()
                if (r1 == 0) goto L_0x004d
                if (r0 == 0) goto L_0x00c5
                com.fossil.Mj6 r0 = (com.fossil.Mj6) r0
                com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$Ai$Bii r2 = new com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$Ai$Bii
                r2.<init>(r11)
                r1.h(r0, r2)
            L_0x004d:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x004f:
                return r0
            L_0x0050:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0058:
                java.lang.Object r0 = r11.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r12)
                r7 = r0
                r1 = r12
            L_0x0061:
                r6 = r1
                com.portfolio.platform.data.model.MFUser r6 = (com.portfolio.platform.data.model.MFUser) r6
                if (r6 == 0) goto L_0x004d
                java.lang.String r0 = r6.getCreatedAt()
                java.util.Date r3 = com.mapped.TimeUtils.q0(r0)
                com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter r8 = r11.this$0
                com.portfolio.platform.data.source.SleepSummariesRepository r0 = com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter.v(r8)
                com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter r1 = r11.this$0
                com.portfolio.platform.data.source.SleepSessionsRepository r1 = com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter.t(r1)
                com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter r2 = r11.this$0
                com.portfolio.platform.data.source.FitnessDataRepository r2 = com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter.s(r2)
                java.lang.String r4 = "createdDate"
                com.mapped.Wg6.b(r3, r4)
                com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter r4 = r11.this$0
                com.mapped.U04 r4 = com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter.r(r4)
                com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$Ai$Aii r5 = new com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$Ai$Aii
                r5.<init>(r11)
                r11.L$0 = r7
                r11.L$1 = r6
                r11.L$2 = r6
                r11.L$3 = r3
                r11.L$4 = r8
                r11.label = r10
                r6 = r11
                java.lang.Object r1 = r0.getSummariesPaging(r1, r2, r3, r4, r5, r6)
                if (r1 != r9) goto L_0x00c2
                r0 = r9
                goto L_0x004f
            L_0x00a5:
                com.fossil.El7.b(r12)
                com.mapped.Il6 r0 = r11.p$
                com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter r1 = r11.this$0
                com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter.q(r1)
                com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$Ai$Cii r2 = new com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter$Ai$Cii
                r3 = 0
                r2.<init>(r11, r3)
                r11.L$0 = r0
                r11.label = r4
                java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r11)
                if (r1 != r9) goto L_0x00cd
                r0 = r9
                goto L_0x004f
            L_0x00c2:
                r2 = r8
                goto L_0x0027
            L_0x00c5:
                com.mapped.Rc6 r0 = new com.mapped.Rc6
                java.lang.String r1 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment"
                r0.<init>(r1)
                throw r0
            L_0x00cd:
                r7 = r0
                goto L_0x0061
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepPresenter.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public DashboardSleepPresenter(Lj6 lj6, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, U04 u04) {
        Wg6.c(lj6, "mView");
        Wg6.c(sleepSummariesRepository, "mSleepSummariesRepository");
        Wg6.c(sleepSessionsRepository, "mSleepSessionsRepository");
        Wg6.c(fitnessDataRepository, "mFitnessDataRepository");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(u04, "mAppExecutors");
        this.g = lj6;
        this.h = sleepSummariesRepository;
        this.i = sleepSessionsRepository;
        this.j = fitnessDataRepository;
        this.k = userRepository;
        this.l = u04;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.get.instance().J());
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Boolean p0 = TimeUtils.p0(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardSleepPresenter", "start isDateTodayDate " + p0 + " listingPage " + this.f);
        if (!p0.booleanValue()) {
            this.e = new Date();
            Listing<SleepSummary> listing = this.f;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", "stop");
    }

    @DexIgnore
    @Override // com.fossil.Kj6
    public void n() {
        Rm6 unused = Gu7.d(k(), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Kj6
    public void o() {
        LiveData<Cf<SleepSummary>> pagedList;
        try {
            Lj6 lj6 = this.g;
            Listing<SleepSummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                if (lj6 != null) {
                    pagedList.n((Mj6) lj6);
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment");
                }
            }
            this.h.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(Cd6.a);
            local.e("DashboardSleepPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.Kj6
    public void p() {
        Gg6<Cd6> retry;
        FLogger.INSTANCE.getLocal().d("DashboardSleepPresenter", "retry all failed request");
        Listing<SleepSummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }

    @DexIgnore
    public final Lj6 y() {
        return this.g;
    }

    @DexIgnore
    public void z() {
        this.g.M5(this);
    }
}
