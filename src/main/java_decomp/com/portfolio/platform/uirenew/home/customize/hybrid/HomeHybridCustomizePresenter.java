package com.portfolio.platform.uirenew.home.customize.hybrid;

import android.content.Intent;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.Ao7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Hl5;
import com.fossil.Hl7;
import com.fossil.Im7;
import com.fossil.Jn5;
import com.fossil.Kb6;
import com.fossil.Ko7;
import com.fossil.Lb6;
import com.fossil.Ls0;
import com.fossil.M66;
import com.fossil.Mn7;
import com.fossil.N66;
import com.fossil.Pm7;
import com.fossil.Uh5;
import com.fossil.Um5;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.HomeHybridCustomizeFragment;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeHybridCustomizePresenter extends Kb6 {
    @DexIgnore
    public LiveData<List<HybridPreset>> e; // = new MutableLiveData();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<HybridPreset> g; // = new ArrayList<>();
    @DexIgnore
    public HybridPreset h;
    @DexIgnore
    public MutableLiveData<String> i; // = this.n.K();
    @DexIgnore
    public int j; // = -1;
    @DexIgnore
    public int k; // = 2;
    @DexIgnore
    public Lc6<Boolean, ? extends List<HybridPreset>> l; // = Hl7.a(Boolean.FALSE, null);
    @DexIgnore
    public Boolean m;
    @DexIgnore
    public /* final */ PortfolioApp n;
    @DexIgnore
    public /* final */ Lb6 o;
    @DexIgnore
    public /* final */ MicroAppRepository p;
    @DexIgnore
    public /* final */ HybridPresetRepository q;
    @DexIgnore
    public /* final */ SetHybridPresetToWatchUseCase r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$createNewPreset$1", f = "HomeHybridCustomizePresenter.kt", l = {291}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ HybridPreset $newPreset;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(HybridPreset hybridPreset, Xe6 xe6, Ai ai) {
                super(2, xe6);
                this.$newPreset = hybridPreset;
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$newPreset, xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.this$0.this$0;
                    homeHybridCustomizePresenter.S(homeHybridCustomizePresenter.O() + 1);
                    HybridPresetRepository hybridPresetRepository = this.this$0.this$0.q;
                    HybridPreset hybridPreset = this.$newPreset;
                    this.L$0 = il6;
                    this.label = 1;
                    if (hybridPresetRepository.upsertHybridPreset(hybridPreset, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(HomeHybridCustomizePresenter homeHybridCustomizePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeHybridCustomizePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Iterator it = this.this$0.g.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    Object next = it.next();
                    if (Ao7.a(((HybridPreset) next).isActive()).booleanValue()) {
                        obj2 = next;
                        break;
                    }
                }
                HybridPreset hybridPreset = (HybridPreset) obj2;
                if (hybridPreset != null) {
                    HybridPreset cloneFrom = HybridPreset.Companion.cloneFrom(hybridPreset);
                    Dv7 i2 = this.this$0.i();
                    Aii aii = new Aii(cloneFrom, null, this);
                    this.L$0 = il6;
                    this.L$1 = hybridPreset;
                    this.L$2 = hybridPreset;
                    this.L$3 = cloneFrom;
                    this.label = 1;
                    if (Eu7.g(i2, aii, this) == d) {
                        return d;
                    }
                }
                return Cd6.a;
            } else if (i == 1) {
                HybridPreset hybridPreset2 = (HybridPreset) this.L$3;
                HybridPreset hybridPreset3 = (HybridPreset) this.L$2;
                HybridPreset hybridPreset4 = (HybridPreset) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.o.N3();
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ei<SetHybridPresetToWatchUseCase.Ci, SetHybridPresetToWatchUseCase.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset a;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset b;
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        HybridPresetRepository hybridPresetRepository = this.this$0.this$0.c.q;
                        HybridPreset hybridPreset = this.this$0.this$0.a;
                        if (hybridPreset != null) {
                            String id = hybridPreset.getId();
                            this.L$0 = il6;
                            this.label = 1;
                            if (hybridPresetRepository.deletePresetById(id, this) == d) {
                                return d;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Dv7 i2 = this.this$0.c.i();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (Eu7.g(i2, aiii, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.c.o.w();
                this.this$0.c.o.O(this.this$0.c.O());
                Bi bi = this.this$0;
                bi.c.P(bi.b);
                return Cd6.a;
            }
        }

        @DexIgnore
        public Bi(HybridPreset hybridPreset, HybridPreset hybridPreset2, HomeHybridCustomizePresenter homeHybridCustomizePresenter, String str) {
            this.a = hybridPreset;
            this.b = hybridPreset2;
            this.c = homeHybridCustomizePresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(SetHybridPresetToWatchUseCase.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(SetHybridPresetToWatchUseCase.Ai ai) {
            Wg6.c(ai, "errorValue");
            this.c.o.w();
            int b2 = ai.b();
            if (b2 == 1101 || b2 == 1112 || b2 == 1113) {
                List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(ai.a());
                Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                Lb6 lb6 = this.c.o;
                Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                if (array != null) {
                    Uh5[] uh5Arr = (Uh5[]) array;
                    lb6.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.c.o.u();
        }

        @DexIgnore
        public void c(SetHybridPresetToWatchUseCase.Ci ci) {
            Wg6.c(ci, "responseValue");
            Rm6 unused = Gu7.d(this.c.k(), null, null, new Aii(this, null), 3, null);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(SetHybridPresetToWatchUseCase.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $nextActivePresetId$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $preset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    HybridPresetRepository hybridPresetRepository = this.this$0.this$0.q;
                    String id = this.this$0.$preset.getId();
                    this.L$0 = il6;
                    this.label = 1;
                    if (hybridPresetRepository.deletePresetById(id, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(HybridPreset hybridPreset, Xe6 xe6, HomeHybridCustomizePresenter homeHybridCustomizePresenter, String str) {
            super(2, xe6);
            this.$preset = hybridPreset;
            this.this$0 = homeHybridCustomizePresenter;
            this.$nextActivePresetId$inlined = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.$preset, xe6, this.this$0, this.$nextActivePresetId$inlined);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(i2, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("mView.showDeleteSuccessfully ");
            sb.append(this.this$0.O() - 1);
            local.d("HomeHybridCustomizeFragment", sb.toString());
            this.this$0.o.O(this.this$0.O() - 1);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$renameCurrentPreset$1", f = "HomeHybridCustomizePresenter.kt", l = {157}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $name;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ HybridPreset $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(HybridPreset hybridPreset, Xe6 xe6, Di di) {
                super(2, xe6);
                this.$it = hybridPreset;
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$it, xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    HybridPresetRepository hybridPresetRepository = this.this$0.this$0.q;
                    HybridPreset hybridPreset = this.$it;
                    this.L$0 = il6;
                    this.label = 1;
                    if (hybridPresetRepository.upsertHybridPreset(hybridPreset, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(HomeHybridCustomizePresenter homeHybridCustomizePresenter, String str, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeHybridCustomizePresenter;
            this.$presetId = str;
            this.$name = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$presetId, this.$name, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Iterator it = this.this$0.g.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    Object next = it.next();
                    if (Ao7.a(Wg6.a(((HybridPreset) next).getId(), this.$presetId)).booleanValue()) {
                        obj2 = next;
                        break;
                    }
                }
                HybridPreset hybridPreset = (HybridPreset) obj2;
                HybridPreset clone = hybridPreset != null ? hybridPreset.clone() : null;
                if (clone != null) {
                    clone.setName(this.$name);
                    Dv7 i2 = this.this$0.i();
                    Aii aii = new Aii(clone, null, this);
                    this.L$0 = il6;
                    this.L$1 = clone;
                    this.L$2 = clone;
                    this.label = 1;
                    if (Eu7.g(i2, aii, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                HybridPreset hybridPreset2 = (HybridPreset) this.L$2;
                HybridPreset hybridPreset3 = (HybridPreset) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements CoroutineUseCase.Ei<SetHybridPresetToWatchUseCase.Ci, SetHybridPresetToWatchUseCase.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter a;

        @DexIgnore
        public Ei(HomeHybridCustomizePresenter homeHybridCustomizePresenter) {
            this.a = homeHybridCustomizePresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(SetHybridPresetToWatchUseCase.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(SetHybridPresetToWatchUseCase.Ai ai) {
            Wg6.c(ai, "errorValue");
            this.a.o.w();
            int b = ai.b();
            if (b == 1101 || b == 1112 || b == 1113) {
                List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(ai.a());
                Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                Lb6 lb6 = this.a.o;
                Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                if (array != null) {
                    Uh5[] uh5Arr = (Uh5[]) array;
                    lb6.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            this.a.o.u();
        }

        @DexIgnore
        public void c(SetHybridPresetToWatchUseCase.Ci ci) {
            Wg6.c(ci, "responseValue");
            this.a.o.w();
            this.a.o.m0(0);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(SetHybridPresetToWatchUseCase.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1", f = "HomeHybridCustomizePresenter.kt", l = {63}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1$1", f = "HomeHybridCustomizePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends MicroApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends MicroApp>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.p.getAllMicroApp(this.this$0.this$0.n.J());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii<T> implements Ls0<String> {
            @DexIgnore
            public /* final */ /* synthetic */ Fi a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii<T> implements Ls0<List<? extends HybridPreset>> {
                @DexIgnore
                public /* final */ /* synthetic */ Bii a;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii<T> implements Comparator<T> {
                    @DexIgnore
                    @Override // java.util.Comparator
                    public final int compare(T t, T t2) {
                        return Mn7.c(Boolean.valueOf(t2.isActive()), Boolean.valueOf(t.isActive()));
                    }
                }

                @DexIgnore
                public Aiii(Bii bii) {
                    this.a = bii;
                }

                @DexIgnore
                public final void a(List<HybridPreset> list) {
                    if (list != null) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("HomeHybridCustomizePresenter", "onObserve on preset list change " + list);
                        List b0 = Pm7.b0(list, new Aiiii());
                        boolean a2 = Wg6.a(b0, this.a.a.this$0.g) ^ true;
                        int itemCount = this.a.a.this$0.o.getItemCount();
                        if (a2 || b0.size() != itemCount - 1) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            local2.d("HomeHybridCustomizePresenter", "process change - " + a2 + " - itemCount: " + itemCount + " - sortedPresetSize: " + b0.size());
                            if (this.a.a.this$0.k == 2) {
                                this.a.a.this$0.N(b0);
                                return;
                            }
                            this.a.a.this$0.l = Hl7.a(Boolean.TRUE, b0);
                            return;
                        }
                        ArrayList<T> arrayList = this.a.a.this$0.g;
                        ArrayList arrayList2 = new ArrayList(Im7.m(arrayList, 10));
                        for (T t : arrayList) {
                            arrayList2.add(this.a.a.this$0.L(t));
                        }
                        if (!arrayList2.isEmpty()) {
                            boolean g = Jn5.g(Jn5.b, ((HomeHybridCustomizeFragment) this.a.a.this$0.o).getContext(), ((M66) arrayList2.get(0)).b(), false, false, false, null, 56, null);
                            if (!Wg6.a(Boolean.valueOf(g), this.a.a.this$0.m)) {
                                this.a.a.this$0.m = Boolean.valueOf(g);
                                this.a.a.this$0.o.J2(arrayList2);
                            }
                        }
                    }
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // com.fossil.Ls0
                public /* bridge */ /* synthetic */ void onChanged(List<? extends HybridPreset> list) {
                    a(list);
                }
            }

            @DexIgnore
            public Bii(Fi fi) {
                this.a = fi;
            }

            @DexIgnore
            public final void a(String str) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeHybridCustomizePresenter", "on active serial change " + str + " mCurrentHomeTab " + this.a.this$0.k);
                if (TextUtils.isEmpty(str) || FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
                    this.a.this$0.o.r(true);
                    return;
                }
                HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.a.this$0;
                HybridPresetRepository hybridPresetRepository = homeHybridCustomizePresenter.q;
                if (str != null) {
                    homeHybridCustomizePresenter.e = hybridPresetRepository.getPresetListAsLiveData(str);
                    this.a.this$0.e.h((LifecycleOwner) this.a.this$0.o, new Aiii(this));
                    this.a.this$0.o.r(false);
                    return;
                }
                Wg6.i();
                throw null;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(String str) {
                a(str);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(HomeHybridCustomizePresenter homeHybridCustomizePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homeHybridCustomizePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0087  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x002c  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 1
                java.lang.Object r2 = com.fossil.Yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0056
                if (r0 != r6) goto L_0x004e
                java.lang.Object r0 = r7.L$1
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                java.lang.Object r1 = r7.L$0
                com.mapped.Il6 r1 = (com.mapped.Il6) r1
                com.fossil.El7.b(r8)
                r2 = r0
                r1 = r8
            L_0x0018:
                r0 = r1
                java.util.Collection r0 = (java.util.Collection) r0
                r2.addAll(r0)
            L_0x001e:
                com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter r0 = r7.this$0
                androidx.lifecycle.MutableLiveData r1 = com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter.D(r0)
                com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter r0 = r7.this$0
                com.fossil.Lb6 r0 = com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter.F(r0)
                if (r0 == 0) goto L_0x0087
                com.mapped.HomeHybridCustomizeFragment r0 = (com.mapped.HomeHybridCustomizeFragment) r0
                com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$Fi$Bii r2 = new com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$Fi$Bii
                r2.<init>(r7)
                r1.h(r0, r2)
                com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter r0 = r7.this$0
                com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase r0 = com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter.E(r0)
                r0.r()
                com.portfolio.platform.service.BleCommandResultManager r0 = com.portfolio.platform.service.BleCommandResultManager.d
                com.misfit.frameworks.buttonservice.communite.CommunicateMode[] r1 = new com.misfit.frameworks.buttonservice.communite.CommunicateMode[r6]
                r2 = 0
                com.misfit.frameworks.buttonservice.communite.CommunicateMode r3 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SET_LINK_MAPPING
                r1[r2] = r3
                r0.g(r1)
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x004d:
                return r0
            L_0x004e:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0056:
                com.fossil.El7.b(r8)
                com.mapped.Il6 r1 = r7.p$
                com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter r0 = r7.this$0
                java.util.ArrayList r0 = com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter.u(r0)
                boolean r0 = r0.isEmpty()
                if (r0 == 0) goto L_0x001e
                com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter r0 = r7.this$0
                java.util.ArrayList r0 = com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter.u(r0)
                com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter r3 = r7.this$0
                com.fossil.Dv7 r3 = com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter.v(r3)
                com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$Fi$Aii r4 = new com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$Fi$Aii
                r5 = 0
                r4.<init>(r7, r5)
                r7.L$0 = r1
                r7.L$1 = r0
                r7.label = r6
                java.lang.Object r1 = com.fossil.Eu7.g(r3, r4, r7)
                if (r1 != r2) goto L_0x008f
                r0 = r2
                goto L_0x004d
            L_0x0087:
                com.mapped.Rc6 r0 = new com.mapped.Rc6
                java.lang.String r1 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment"
                r0.<init>(r1)
                throw r0
            L_0x008f:
                r2 = r0
                goto L_0x0018
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter.Fi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public HomeHybridCustomizePresenter(PortfolioApp portfolioApp, Lb6 lb6, MicroAppRepository microAppRepository, HybridPresetRepository hybridPresetRepository, SetHybridPresetToWatchUseCase setHybridPresetToWatchUseCase, An4 an4) {
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(lb6, "mView");
        Wg6.c(microAppRepository, "mMicroAppRepository");
        Wg6.c(hybridPresetRepository, "mHybridPresetRepository");
        Wg6.c(setHybridPresetToWatchUseCase, "mSetHybridPresetToWatchUseCase");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.n = portfolioApp;
        this.o = lb6;
        this.p = microAppRepository;
        this.q = hybridPresetRepository;
        this.r = setHybridPresetToWatchUseCase;
    }

    @DexIgnore
    public final M66 L(HybridPreset hybridPreset) {
        T t;
        ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
        ArrayList arrayList = new ArrayList();
        List<Jn5.Ai> c = Hl5.a.c(hybridPreset);
        Iterator<HybridPresetAppSetting> it = buttons.iterator();
        while (it.hasNext()) {
            HybridPresetAppSetting next = it.next();
            String component1 = next.component1();
            String component2 = next.component2();
            Iterator<T> it2 = this.f.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t = null;
                    break;
                }
                T next2 = it2.next();
                if (Wg6.a(next2.getId(), component2)) {
                    t = next2;
                    break;
                }
            }
            T t2 = t;
            if (t2 != null) {
                String id = t2.getId();
                String icon = t2.getIcon();
                if (icon == null) {
                    icon = "";
                }
                arrayList.add(new N66(id, icon, Um5.d(PortfolioApp.get.instance(), t2.getNameKey(), t2.getName()), component1, null, 16, null));
            }
        }
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "convertPresetToHybridPresetConfigWrapper");
        String id2 = hybridPreset.getId();
        String name = hybridPreset.getName();
        if (name == null) {
            name = "";
        }
        return new M66(id2, name, arrayList, c, hybridPreset.isActive());
    }

    @DexIgnore
    public final void M(String str) {
        T t;
        T t2;
        Jn5 jn5 = Jn5.b;
        Lb6 lb6 = this.o;
        if (lb6 == null) {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } else if (Jn5.c(jn5, ((HomeHybridCustomizeFragment) lb6).getContext(), Jn5.Ai.SET_BLE_COMMAND, false, false, false, null, 60, null)) {
            List<HybridPreset> e2 = this.e.e();
            if (e2 != null) {
                e2.isEmpty();
            }
            Iterator<T> it = this.g.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (Wg6.a(next.getId(), str)) {
                    t = next;
                    break;
                }
            }
            T t3 = t;
            if (t3 != null) {
                Iterator<T> it2 = this.g.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next2 = it2.next();
                    if (next2.isActive()) {
                        t2 = next2;
                        break;
                    }
                }
                T t4 = t2;
                this.o.y();
                FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "delete activePreset " + ((Object) t4) + " set preset " + ((Object) t3) + " as active first");
                this.r.e(new SetHybridPresetToWatchUseCase.Bi(t3), new Bi(t4, t3, this, str));
            }
        }
    }

    @DexIgnore
    public final void N(List<HybridPreset> list) {
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "doShowingPreset");
        this.g.clear();
        this.g.addAll(list);
        int size = this.g.size();
        int i2 = this.j;
        if (size > i2 && i2 > 0) {
            this.h = this.g.get(i2);
        }
        U();
    }

    @DexIgnore
    public final int O() {
        return this.j;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean P(com.portfolio.platform.data.model.room.microapp.HybridPreset r7) {
        /*
            r6 = this;
            r2 = 1
            if (r7 != 0) goto L_0x0005
            r0 = r2
        L_0x0004:
            return r0
        L_0x0005:
            java.util.ArrayList r0 = r7.getButtons()
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.util.Iterator r4 = r0.iterator()
        L_0x0012:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x002f
            java.lang.Object r1 = r4.next()
            r0 = r1
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r0 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r0
            com.fossil.Bl5 r5 = com.fossil.Bl5.c
            java.lang.String r0 = r0.getAppId()
            boolean r0 = r5.c(r0)
            if (r0 == 0) goto L_0x0012
            r3.add(r1)
            goto L_0x0012
        L_0x002f:
            r1 = 0
            boolean r0 = r3.isEmpty()
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x007f
            java.util.Iterator r3 = r3.iterator()
        L_0x003c:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x007f
            java.lang.Object r0 = r3.next()
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r0 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r0
            com.fossil.Bl5 r4 = com.fossil.Bl5.c
            java.lang.String r5 = r0.getAppId()
            boolean r4 = r4.e(r5)
            if (r4 != 0) goto L_0x003c
        L_0x0054:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "setPresetToWatch missingPermissionComp "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r4 = "HomeHybridCustomizePresenter"
            java.lang.String r3 = r3.toString()
            r1.d(r4, r3)
            if (r0 == 0) goto L_0x007d
            com.fossil.Lb6 r1 = r6.o
            java.lang.String r0 = r0.getAppId()
            r1.C5(r0)
            r0 = 0
            goto L_0x0004
        L_0x007d:
            r0 = r2
            goto L_0x0004
        L_0x007f:
            r0 = r1
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter.P(com.portfolio.platform.data.model.room.microapp.HybridPreset):boolean");
    }

    @DexIgnore
    public void Q(int i2, int i3, Intent intent) {
        if (i2 == 100 && i3 == -1) {
            this.o.c0(0);
        }
    }

    @DexIgnore
    public void R(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "onHomeTabChange - tab: " + i2);
        this.k = i2;
        if (i2 == 2 && this.l.getFirst().booleanValue()) {
            List<HybridPreset> list = (List) this.l.getSecond();
            if (list != null) {
                N(list);
            }
            this.l = Hl7.a(Boolean.FALSE, null);
        }
    }

    @DexIgnore
    public final void S(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public void T() {
        this.o.M5(this);
    }

    @DexIgnore
    public final void U() {
        ArrayList<HybridPreset> arrayList = this.g;
        ArrayList arrayList2 = new ArrayList(Im7.m(arrayList, 10));
        Iterator<T> it = arrayList.iterator();
        while (it.hasNext()) {
            arrayList2.add(L(it.next()));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "showPresets - size=" + this.g.size() + " uiData " + arrayList2);
        if (this.m == null && !this.g.isEmpty() && !arrayList2.isEmpty()) {
            Jn5 jn5 = Jn5.b;
            Lb6 lb6 = this.o;
            if (lb6 != null) {
                this.m = Boolean.valueOf(Jn5.g(jn5, ((HomeHybridCustomizeFragment) lb6).getContext(), ((M66) arrayList2.get(0)).b(), false, false, false, null, 56, null));
            } else {
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
        }
        this.o.J2(arrayList2);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Rm6 unused = Gu7.d(k(), null, null, new Fi(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        try {
            LiveData<List<HybridPreset>> liveData = this.e;
            Lb6 lb6 = this.o;
            if (lb6 != null) {
                liveData.n((HomeHybridCustomizeFragment) lb6);
                this.i.n((LifecycleOwner) this.o);
                this.r.w();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d("HomeHybridCustomizePresenter", "Exception when remove observer.");
        }
    }

    @DexIgnore
    @Override // com.fossil.Kb6
    public void n(int i2) {
        if (this.g.size() > i2) {
            this.j = i2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "changePresetPosition " + this.j);
            HybridPreset hybridPreset = this.g.get(this.j);
            this.h = hybridPreset;
            if (hybridPreset != null) {
                this.o.j2(hybridPreset.isActive());
                return;
            }
            return;
        }
        this.o.j2(false);
    }

    @DexIgnore
    @Override // com.fossil.Kb6
    public void o() {
        Rm6 unused = Gu7.d(k(), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Kb6
    public void p(String str) {
        Wg6.c(str, "nextActivePresetId");
        HybridPreset hybridPreset = this.h;
        if (hybridPreset == null) {
            return;
        }
        if (hybridPreset.isActive()) {
            M(str);
        } else {
            Rm6 unused = Gu7.d(k(), null, null, new Ci(hybridPreset, null, this, str), 3, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Kb6
    public void q(String str, String str2) {
        Wg6.c(str, "name");
        Wg6.c(str2, "presetId");
        Rm6 unused = Gu7.d(k(), null, null, new Di(this, str2, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Kb6
    public void r() {
        HybridPreset hybridPreset;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "setPresetToWatch mCurrentPreset=" + this.h);
        Jn5 jn5 = Jn5.b;
        Lb6 lb6 = this.o;
        if (lb6 == null) {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
        } else if (Jn5.c(jn5, ((HomeHybridCustomizeFragment) lb6).getContext(), Jn5.Ai.SET_BLE_COMMAND, false, false, false, null, 60, null) && (hybridPreset = this.h) != null) {
            this.o.y();
            this.r.e(new SetHybridPresetToWatchUseCase.Bi(hybridPreset), new Ei(this));
        }
    }
}
