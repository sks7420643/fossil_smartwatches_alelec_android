package com.portfolio.platform.uirenew.home.customize.diana;

import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.D76;
import com.fossil.Dv7;
import com.fossil.E76;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Hl5;
import com.fossil.Hs0;
import com.fossil.Jn5;
import com.fossil.Ko7;
import com.fossil.L66;
import com.fossil.Ls0;
import com.fossil.Mo5;
import com.fossil.N66;
import com.fossil.Oo5;
import com.fossil.Or0;
import com.fossil.Sl5;
import com.fossil.Ss0;
import com.fossil.Uh5;
import com.fossil.Ul5;
import com.fossil.Um5;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.CustomizeConfigurationExt;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.V3;
import com.mapped.WatchAppEditFragment;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetWatchAppUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppEditPresenter extends D76 {
    @DexIgnore
    public WatchAppEditViewModel e;
    @DexIgnore
    public MutableLiveData<List<Oo5>> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<L66> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<L66> h;
    @DexIgnore
    public /* final */ E76 i;
    @DexIgnore
    public /* final */ SetWatchAppUseCase j;
    @DexIgnore
    public /* final */ An4 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $it;
        @DexIgnore
        public /* final */ /* synthetic */ List $presetButtons$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $shouldShowSkippedPermissions$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppEditPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements CoroutineUseCase.Ei<SetWatchAppUseCase.Ci, SetWatchAppUseCase.Ai> {
            @DexIgnore
            public /* final */ /* synthetic */ Ai a;
            @DexIgnore
            public /* final */ /* synthetic */ Ul5 b;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Aiii aiii, Xe6 xe6) {
                        super(2, xe6);
                        this.this$0 = aiii;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        Aiiii aiiii = new Aiiii(this.this$0, xe6);
                        aiiii.p$ = (Il6) obj;
                        throw null;
                        //return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                        throw null;
                        //return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = Yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            El7.b(obj);
                            Il6 il6 = this.p$;
                            WatchAppEditViewModel x = WatchAppEditPresenter.x(this.this$0.this$0.a.this$0);
                            this.L$0 = il6;
                            this.label = 1;
                            if (x.E(this) == d) {
                                return d;
                            }
                        } else if (i == 1) {
                            Il6 il62 = (Il6) this.L$0;
                            El7.b(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        return Cd6.a;
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Dv7 b = Bw7.b();
                        Aiiii aiiii = new Aiiii(this, null);
                        this.L$0 = il6;
                        this.label = 1;
                        if (Eu7.g(b, aiiii, this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    this.this$0.a.this$0.i.w();
                    this.this$0.a.this$0.i.t0(true);
                    return Cd6.a;
                }
            }

            @DexIgnore
            public Aii(Ai ai, Ul5 ul5) {
                this.a = ai;
                this.b = ul5;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void a(SetWatchAppUseCase.Ai ai) {
                b(ai);
            }

            @DexIgnore
            public void b(SetWatchAppUseCase.Ai ai) {
                Wg6.c(ai, "errorValue");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppEditPresenter", "setToWatch onError, errorCode = " + ai);
                this.a.this$0.i.w();
                int b2 = ai.b();
                if (b2 != 1101) {
                    if (b2 == 8888) {
                        this.a.this$0.i.c();
                    } else if (!(b2 == 1112 || b2 == 1113)) {
                        this.a.this$0.i.P();
                    }
                    String arrayList = ai.a().toString();
                    Ul5 ul5 = this.b;
                    Wg6.b(arrayList, "errorCode");
                    ul5.c(arrayList);
                    return;
                }
                List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(ai.a());
                Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                E76 e76 = this.a.this$0.i;
                Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                if (array != null) {
                    Uh5[] uh5Arr = (Uh5[]) array;
                    e76.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    String arrayList2 = ai.a().toString();
                    Ul5 ul52 = this.b;
                    Wg6.b(arrayList2, "errorCode");
                    ul52.c(arrayList2);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }

            @DexIgnore
            public void c(SetWatchAppUseCase.Ci ci) {
                Wg6.c(ci, "responseValue");
                FLogger.INSTANCE.getLocal().d("WatchAppEditPresenter", "setToWatch success");
                this.b.c("");
                Rm6 unused = Gu7.d(this.a.this$0.k(), null, null, new Aiii(this, null), 3, null);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.CoroutineUseCase.Ei
            public /* bridge */ /* synthetic */ void onSuccess(SetWatchAppUseCase.Ci ci) {
                c(ci);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(List list, Xe6 xe6, WatchAppEditPresenter watchAppEditPresenter, boolean z, List list2) {
            super(2, xe6);
            this.$it = list;
            this.this$0 = watchAppEditPresenter;
            this.$shouldShowSkippedPermissions$inlined = z;
            this.$presetButtons$inlined = list2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.$it, xe6, this.this$0, this.$shouldShowSkippedPermissions$inlined, this.$presetButtons$inlined);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r3v13, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0100  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0191  */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 409
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditPresenter.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<List<? extends Oo5>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppEditPresenter a;

        @DexIgnore
        public Bi(WatchAppEditPresenter watchAppEditPresenter) {
            this.a = watchAppEditPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: androidx.lifecycle.MutableLiveData */
        /* JADX WARN: Multi-variable type inference failed */
        public final void a(List<Oo5> list) {
            MutableLiveData mutableLiveData = this.a.f;
            if (list != null) {
                mutableLiveData.l(CustomizeConfigurationExt.a(list));
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends Oo5> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppEditPresenter a;

        @DexIgnore
        public Ci(WatchAppEditPresenter watchAppEditPresenter) {
            this.a = watchAppEditPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppEditPresenter", "start - observe selected watchApp value=" + str);
            E76 e76 = this.a.i;
            if (str != null) {
                e76.W5(str);
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppEditPresenter a;

        @DexIgnore
        public Di(WatchAppEditPresenter watchAppEditPresenter) {
            this.a = watchAppEditPresenter;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppEditPresenter", "update preset status isChanged=" + bool);
            E76 e76 = this.a.i;
            if (bool != null) {
                e76.s0(bool.booleanValue());
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<L66> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppEditPresenter a;

        @DexIgnore
        public Ei(WatchAppEditPresenter watchAppEditPresenter) {
            this.a = watchAppEditPresenter;
        }

        @DexIgnore
        public final void a(L66 l66) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppEditPresenter", "show preset button" + l66);
            E76 e76 = this.a.i;
            Wg6.b(l66, "it");
            e76.a2(l66);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(L66 l66) {
            a(l66);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppEditPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditPresenter$wrapperButtonPrestTransformations$1$1", f = "WatchAppEditPresenter.kt", l = {75}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<L66>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $buttons;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
                this.$buttons = list;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$buttons, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<L66> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Hs0 hs0 = this.p$;
                    Hl5 hl5 = Hl5.a;
                    List<Oo5> list = this.$buttons;
                    Wg6.b(list, "buttons");
                    List<Jn5.Ai> a2 = hl5.a(list);
                    Mo5 p = WatchAppEditPresenter.x(this.this$0.a).p();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("WatchAppEditPresenter", "receive buttons " + this.$buttons + " currentPreset " + p);
                    ArrayList arrayList = new ArrayList();
                    List<Oo5> list2 = this.$buttons;
                    Wg6.b(list2, "buttons");
                    for (Oo5 oo5 : list2) {
                        WatchApp o = WatchAppEditPresenter.x(this.this$0.a).o(oo5.a());
                        if (o != null) {
                            String watchappId = o.getWatchappId();
                            String icon = o.getIcon();
                            if (icon == null) {
                                icon = "";
                            }
                            Ao7.a(arrayList.add(new N66(watchappId, icon, Um5.d(PortfolioApp.get.instance(), o.getNameKey(), o.getName()), oo5.b(), null, 16, null)));
                        }
                    }
                    L66 l66 = (L66) this.this$0.a.g.e();
                    if (l66 != null) {
                        this.this$0.a.B(l66.b(), arrayList);
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("WatchAppEditPresenter", "wrapperPresetTransformations presetChanged watchAppsDetail=" + arrayList);
                    if (p != null) {
                        this.this$0.a.g.l(new L66(p.e(), p.f(), arrayList, a2, p.m()));
                    }
                    MutableLiveData mutableLiveData = this.this$0.a.g;
                    this.L$0 = hs0;
                    this.L$1 = a2;
                    this.L$2 = p;
                    this.L$3 = arrayList;
                    this.label = 1;
                    if (hs0.a(mutableLiveData, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    ArrayList arrayList2 = (ArrayList) this.L$3;
                    Mo5 mo5 = (Mo5) this.L$2;
                    List list3 = (List) this.L$1;
                    Hs0 hs02 = (Hs0) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Fi(WatchAppEditPresenter watchAppEditPresenter) {
            this.a = watchAppEditPresenter;
        }

        @DexIgnore
        public final LiveData<L66> a(List<Oo5> list) {
            return Or0.c(null, 0, new Aii(this, list, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((List) obj);
        }
    }

    @DexIgnore
    public WatchAppEditPresenter(E76 e76, SetWatchAppUseCase setWatchAppUseCase, An4 an4) {
        Wg6.c(e76, "mView");
        Wg6.c(setWatchAppUseCase, "mSetWatchAppUseCase");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.i = e76;
        this.j = setWatchAppUseCase;
        this.k = an4;
        new Gson();
        LiveData<L66> c = Ss0.c(this.f, new Fi(this));
        Wg6.b(c, "Transformations.switchMa\u2026erPreset)\n        }\n    }");
        this.h = c;
    }

    @DexIgnore
    public static final /* synthetic */ WatchAppEditViewModel x(WatchAppEditPresenter watchAppEditPresenter) {
        WatchAppEditViewModel watchAppEditViewModel = watchAppEditPresenter.e;
        if (watchAppEditViewModel != null) {
            return watchAppEditViewModel;
        }
        Wg6.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    public final void A(String str, String str2, String str3) {
        Sl5 b = AnalyticsHelper.f.b("set_watch_apps_manually");
        b.a("button", str);
        b.a("old_app", str2);
        b.a("new_app", str3);
        b.b();
    }

    @DexIgnore
    public final void B(List<N66> list, List<N66> list2) {
        boolean z;
        if (list.size() == list2.size()) {
            int size = list2.size();
            int i2 = 0;
            while (true) {
                if (i2 >= size) {
                    z = false;
                    break;
                } else if (!Wg6.a(list.get(i2).a(), list2.get(i2).a())) {
                    z = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (!z) {
                FLogger.INSTANCE.getLocal().d("WatchAppEditPresenter", "Process watch app list, old and new list are not different, no need to send logs");
                return;
            }
            int size2 = list2.size();
            for (int i3 = 0; i3 < size2; i3++) {
                N66 n66 = list.get(i3);
                N66 n662 = list2.get(i3);
                FLogger.INSTANCE.getLocal().d("WatchAppEditPresenter", "Process watch app list, item at " + i3 + " position: " + n66.c() + ", oldId: " + n66.a() + ", newId: " + n662.a());
                String c = n662.c();
                if (c != null) {
                    int hashCode = c.hashCode();
                    if (hashCode != -1074341483) {
                        if (hashCode == 115029 && c.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                            A(ViewHierarchy.DIMENSION_TOP_KEY, n66.a(), n662.a());
                        }
                    } else if (c.equals("middle")) {
                        A("middle", n66.a(), n662.a());
                    }
                }
                A("bottom", n66.a(), n662.a());
            }
            return;
        }
        FLogger.INSTANCE.getLocal().d("WatchAppEditPresenter", "Process watch app list, old and new list sizes are not the same, logs won't be sent");
    }

    @DexIgnore
    public Bundle C(Bundle bundle) {
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            List<Oo5> e2 = watchAppEditViewModel.q().e();
            if (!(e2 == null || bundle == null)) {
                bundle.putString("KEY_CURRENT_PRESET", new Gson().t(e2));
            }
            WatchAppEditViewModel watchAppEditViewModel2 = this.e;
            if (watchAppEditViewModel2 != null) {
                List<Oo5> s = watchAppEditViewModel2.s();
                if (!(s == null || bundle == null)) {
                    bundle.putString("KEY_ORIGINAL_PRESET", new Gson().t(s));
                }
                WatchAppEditViewModel watchAppEditViewModel3 = this.e;
                if (watchAppEditViewModel3 != null) {
                    String e3 = watchAppEditViewModel3.v().e();
                    if (!(e3 == null || bundle == null)) {
                        bundle.putString("KEY_PRESET_WATCH_APP_POS_SELECTED", e3);
                    }
                    return bundle;
                }
                Wg6.n("mWatchAppEditViewModel");
                throw null;
            }
            Wg6.n("mWatchAppEditViewModel");
            throw null;
        }
        Wg6.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    public void D() {
        this.i.M5(this);
    }

    @DexIgnore
    public final void E(List<Oo5> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppEditPresenter", "buttons=" + list);
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            watchAppEditViewModel.G(list);
        } else {
            Wg6.n("mWatchAppEditViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        this.j.o();
        BleCommandResultManager.d.g(CommunicateMode.SET_WATCH_APPS);
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            MutableLiveData<List<Oo5>> q = watchAppEditViewModel.q();
            E76 e76 = this.i;
            if (e76 != null) {
                q.h((WatchAppEditFragment) e76, new Bi(this));
                WatchAppEditViewModel watchAppEditViewModel2 = this.e;
                if (watchAppEditViewModel2 != null) {
                    watchAppEditViewModel2.v().h((LifecycleOwner) this.i, new Ci(this));
                    WatchAppEditViewModel watchAppEditViewModel3 = this.e;
                    if (watchAppEditViewModel3 != null) {
                        watchAppEditViewModel3.r().h((LifecycleOwner) this.i, new Di(this));
                        this.h.h((LifecycleOwner) this.i, new Ei(this));
                        return;
                    }
                    Wg6.n("mWatchAppEditViewModel");
                    throw null;
                }
                Wg6.n("mWatchAppEditViewModel");
                throw null;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditFragment");
        }
        Wg6.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        this.j.r();
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            MutableLiveData<String> v = watchAppEditViewModel.v();
            E76 e76 = this.i;
            if (e76 != null) {
                v.n((WatchAppEditFragment) e76);
                this.g.n((LifecycleOwner) this.i);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditFragment");
        }
        Wg6.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.D76
    public void n(String str, String str2) {
        T t;
        Wg6.c(str, "watchAppId");
        Wg6.c(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("WatchAppEditPresenter", "dropWatchApp - watchAppId=" + str + ", toPosition=" + str2);
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            if (!watchAppEditViewModel.D(str)) {
                WatchAppEditViewModel watchAppEditViewModel2 = this.e;
                if (watchAppEditViewModel2 != null) {
                    List<Oo5> e2 = watchAppEditViewModel2.q().e();
                    if (e2 != null) {
                        Wg6.b(e2, "presetButtons");
                        Iterator<T> it = e2.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t = null;
                                break;
                            }
                            T next = it.next();
                            if (Wg6.a(next.b(), str2)) {
                                t = next;
                                break;
                            }
                        }
                        T t2 = t;
                        if (t2 != null) {
                            t2.d(str);
                        }
                        E(e2);
                    }
                } else {
                    Wg6.n("mWatchAppEditViewModel");
                    throw null;
                }
            }
            if (str.hashCode() == -829740640 && str.equals("commute-time") && !this.k.t()) {
                this.k.s1(true);
                this.i.i0("commute-time");
                return;
            }
            return;
        }
        Wg6.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.D76
    public void o() {
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            boolean C = watchAppEditViewModel.C();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppEditPresenter", "isPresetChanged " + C);
            if (C) {
                this.i.L();
            } else {
                this.i.t0(false);
            }
        } else {
            Wg6.n("mWatchAppEditViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.D76
    public void p(boolean z) {
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            List<Oo5> e2 = watchAppEditViewModel.q().e();
            if (e2 != null) {
                Rm6 unused = Gu7.d(k(), null, null, new Ai(e2, null, this, z, e2), 3, null);
                return;
            }
            return;
        }
        Wg6.n("mWatchAppEditViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.D76
    public void q(WatchAppEditViewModel watchAppEditViewModel) {
        Wg6.c(watchAppEditViewModel, "viewModel");
        this.e = watchAppEditViewModel;
    }

    @DexIgnore
    @Override // com.fossil.D76
    public void r(String str) {
        Wg6.c(str, "watchAppPos");
        WatchAppEditViewModel watchAppEditViewModel = this.e;
        if (watchAppEditViewModel != null) {
            watchAppEditViewModel.F(str);
        } else {
            Wg6.n("mWatchAppEditViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.D76
    public void s(String str, String str2) {
        T t;
        T t2;
        Wg6.c(str, "fromPosition");
        Wg6.c(str2, "toPosition");
        FLogger.INSTANCE.getLocal().d("WatchAppEditPresenter", "swapWatchApp - fromPosition=" + str + ", toPosition=" + str2);
        if (!Wg6.a(str, str2)) {
            WatchAppEditViewModel watchAppEditViewModel = this.e;
            if (watchAppEditViewModel != null) {
                List<Oo5> e2 = watchAppEditViewModel.q().e();
                if (e2 != null) {
                    Wg6.b(e2, "buttons");
                    Iterator<T> it = e2.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        if (Wg6.a(next.b(), str)) {
                            t = next;
                            break;
                        }
                    }
                    T t3 = t;
                    Iterator<T> it2 = e2.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            t2 = null;
                            break;
                        }
                        T next2 = it2.next();
                        if (Wg6.a(next2.b(), str2)) {
                            t2 = next2;
                            break;
                        }
                    }
                    T t4 = t2;
                    if (t3 != null) {
                        t3.e(str2);
                    }
                    if (t4 != null) {
                        t4.e(str);
                    }
                    FLogger.INSTANCE.getLocal().d("WatchAppEditPresenter", "swapWatchApp - update preset");
                    E(e2);
                    return;
                }
                return;
            }
            Wg6.n("mWatchAppEditViewModel");
            throw null;
        }
    }
}
