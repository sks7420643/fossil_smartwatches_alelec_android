package com.portfolio.platform.uirenew.home.profile.workout;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hq4;
import com.fossil.Hs0;
import com.fossil.Jn5;
import com.fossil.Ko7;
import com.fossil.Or0;
import com.fossil.Uh5;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingViewModel extends Hq4 {
    @DexIgnore
    public /* final */ LiveData<List<WorkoutSetting>> h; // = Or0.c(null, 0, new Ci(this, null), 3, null);
    @DexIgnore
    public /* final */ MutableLiveData<Ai> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ SetWorkoutSettingUseCase j;
    @DexIgnore
    public /* final */ WorkoutSettingRepository k;

    @DexIgnore
    public enum Ai {
        WAITING_FOR_CONFIRMATION,
        SKIP,
        SUCCESS,
        FAIL_DEVICE_DISCONNECT,
        FAIL_UNKNOWN
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingViewModel$checkDataChanged$1", f = "WorkoutSettingViewModel.kt", l = {40}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $newList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSettingViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(WorkoutSettingViewModel workoutSettingViewModel, List list, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = workoutSettingViewModel;
            this.$newList = list;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$newList, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object workoutSettingList;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                WorkoutSettingRepository workoutSettingRepository = this.this$0.k;
                this.L$0 = il6;
                this.label = 1;
                workoutSettingList = workoutSettingRepository.getWorkoutSettingList(this);
                if (workoutSettingList == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                workoutSettingList = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.t((List) workoutSettingList, this.$newList)) {
                FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "data is changed, ask user to set data to watch");
                Hq4.d(this.this$0, false, true, null, 5, null);
                this.this$0.r().l(Ai.WAITING_FOR_CONFIRMATION);
            } else {
                FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "data is not changed, just skip");
                Hq4.d(this.this$0, false, true, null, 5, null);
                this.this$0.r().l(Ai.SKIP);
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingViewModel$mWorkoutSettingsLiveData$1", f = "WorkoutSettingViewModel.kt", l = {20, 20}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Hs0<List<? extends WorkoutSetting>>, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Hs0 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSettingViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(WorkoutSettingViewModel workoutSettingViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = workoutSettingViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Hs0) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Hs0<List<? extends WorkoutSetting>> hs0, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(hs0, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r6) {
            /*
                r5 = this;
                r4 = 2
                r2 = 1
                java.lang.Object r3 = com.fossil.Yn7.d()
                int r0 = r5.label
                if (r0 == 0) goto L_0x0038
                if (r0 == r2) goto L_0x0020
                if (r0 != r4) goto L_0x0018
                java.lang.Object r0 = r5.L$0
                com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                com.fossil.El7.b(r6)
            L_0x0015:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r5.L$1
                com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                java.lang.Object r1 = r5.L$0
                com.fossil.Hs0 r1 = (com.fossil.Hs0) r1
                com.fossil.El7.b(r6)
                r2 = r0
            L_0x002c:
                r5.L$0 = r1
                r5.label = r4
                java.lang.Object r0 = r2.b(r6, r5)
                if (r0 != r3) goto L_0x0015
                r0 = r3
                goto L_0x0017
            L_0x0038:
                com.fossil.El7.b(r6)
                com.fossil.Hs0 r0 = r5.p$
                com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingViewModel r1 = r5.this$0
                com.portfolio.platform.data.source.WorkoutSettingRepository r1 = com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingViewModel.o(r1)
                r5.L$0 = r0
                r5.L$1 = r0
                r5.label = r2
                java.lang.Object r6 = r1.getWorkoutSettingList(r5)
                if (r6 != r3) goto L_0x0051
                r0 = r3
                goto L_0x0017
            L_0x0051:
                r1 = r0
                r2 = r0
                goto L_0x002c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingViewModel.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Ei<SetWorkoutSettingUseCase.Ci, SetWorkoutSettingUseCase.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSettingViewModel a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di(WorkoutSettingViewModel workoutSettingViewModel) {
            this.a = workoutSettingViewModel;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(SetWorkoutSettingUseCase.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(SetWorkoutSettingUseCase.Bi bi) {
            Wg6.c(bi, "errorValue");
            Hq4.d(this.a, false, true, null, 5, null);
            if (!Jn5.b.m(PortfolioApp.get.instance().getApplicationContext(), Jn5.Ci.BLUETOOTH_CONNECTION)) {
                FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "setWorkoutSettingToWatch fail, missed permissions");
                this.a.e(Uh5.BLUETOOTH_OFF);
                return;
            }
            int b = bi.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutSettingViewModel", "set WorkoutSetting to Watch - onError - lastErrorCode = " + b);
            if (b == 1101) {
                List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(bi.a());
                Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026de(errorValue.errorCodes)");
                WorkoutSettingViewModel workoutSettingViewModel = this.a;
                Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
                if (array != null) {
                    Uh5[] uh5Arr = (Uh5[]) array;
                    workoutSettingViewModel.e((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            } else if (b != 8888) {
                this.a.r().l(Ai.FAIL_UNKNOWN);
            } else {
                this.a.r().l(Ai.FAIL_DEVICE_DISCONNECT);
            }
        }

        @DexIgnore
        public void c(SetWorkoutSettingUseCase.Ci ci) {
            Wg6.c(ci, "responseValue");
            FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "setWorkoutSettingToWatch success");
            Hq4.d(this.a, false, true, null, 5, null);
            this.a.r().l(Ai.SUCCESS);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(SetWorkoutSettingUseCase.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore
    public WorkoutSettingViewModel(SetWorkoutSettingUseCase setWorkoutSettingUseCase, WorkoutSettingRepository workoutSettingRepository) {
        Wg6.c(setWorkoutSettingUseCase, "mSetWorkoutSetting");
        Wg6.c(workoutSettingRepository, "mWorkoutSettingRepository");
        this.j = setWorkoutSettingUseCase;
        this.k = workoutSettingRepository;
        this.j.p();
    }

    @DexIgnore
    @Override // com.fossil.Ts0
    public void onCleared() {
        this.j.r();
        super.onCleared();
    }

    @DexIgnore
    public final void q(List<WorkoutSetting> list) {
        Wg6.c(list, "newList");
        FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "checkDataChanged()");
        Hq4.d(this, true, false, null, 6, null);
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Bi(this, list, null), 3, null);
    }

    @DexIgnore
    public final MutableLiveData<Ai> r() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<List<WorkoutSetting>> s() {
        return this.h;
    }

    @DexIgnore
    public final boolean t(List<WorkoutSetting> list, List<WorkoutSetting> list2) {
        loop0:
        for (WorkoutSetting workoutSetting : list2) {
            Iterator<WorkoutSetting> it = list.iterator();
            while (true) {
                if (it.hasNext()) {
                    WorkoutSetting next = it.next();
                    if (workoutSetting.getType() != next.getType() || (workoutSetting.getAskMeFirst() == next.getAskMeFirst() && workoutSetting.getEnable() == next.getEnable())) {
                    }
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void u(List<WorkoutSetting> list) {
        Wg6.c(list, "workoutSettingList");
        FLogger.INSTANCE.getLocal().d("WorkoutSettingViewModel", "setWorkoutSettingToWatch()");
        Hq4.d(this, true, false, null, 6, null);
        this.j.e(new SetWorkoutSettingUseCase.Ai(list), new Di(this));
    }
}
