package com.portfolio.platform.uirenew.home.customize.diana;

import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jj5;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Mo5;
import com.fossil.Ol5;
import com.fossil.Oo5;
import com.fossil.Ss0;
import com.fossil.Ts0;
import com.fossil.Vt7;
import com.fossil.Ym5;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.google.gson.Gson;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.CustomizeConfigurationExt;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppEditViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ Ai u; // = new Ai(null);
    @DexIgnore
    public Mo5 a;
    @DexIgnore
    public List<Oo5> b;
    @DexIgnore
    public List<DianaAppSetting> c; // = new ArrayList();
    @DexIgnore
    public List<DianaAppSetting> d; // = new ArrayList();
    @DexIgnore
    public MutableLiveData<List<Oo5>> e; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<WatchApp> g; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<WatchApp> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Gson k; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<String> l;
    @DexIgnore
    public /* final */ LiveData<WatchApp> m;
    @DexIgnore
    public /* final */ LiveData<Boolean> n;
    @DexIgnore
    public /* final */ Ls0<List<Oo5>> o;
    @DexIgnore
    public /* final */ DianaPresetRepository p;
    @DexIgnore
    public /* final */ DianaAppSettingRepository q;
    @DexIgnore
    public /* final */ WatchAppRepository r;
    @DexIgnore
    public /* final */ An4 s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return WatchAppEditViewModel.t;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<List<? extends Oo5>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppEditViewModel a;

        @DexIgnore
        public Bi(WatchAppEditViewModel watchAppEditViewModel) {
            this.a = watchAppEditViewModel;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v6, resolved type: androidx.lifecycle.MutableLiveData */
        /* JADX WARN: Multi-variable type inference failed */
        public final void a(List<Oo5> list) {
            T t;
            boolean z;
            if (list != null) {
                String str = (String) this.a.h.e();
                String str2 = (String) this.a.i.e();
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    T t2 = next;
                    if (!Wg6.a(t2.b(), str) || Vt7.j(t2.a(), str2, true)) {
                        z = false;
                        continue;
                    } else {
                        z = true;
                        continue;
                    }
                    if (z) {
                        t = next;
                        break;
                    }
                }
                T t3 = t;
                if (t3 != null) {
                    FLogger.INSTANCE.getLocal().d(WatchAppEditViewModel.u.a(), "Update new watchapp id=" + t3.a() + " at position=" + str);
                    this.a.i.l(t3.a());
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends Oo5> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$init$1", f = "WatchAppEditViewModel.kt", l = {123, 127}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public /* final */ /* synthetic */ String $watchAppPos;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppEditViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$init$1$1", f = "WatchAppEditViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.e.i(this.this$0.this$0.o);
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(WatchAppEditViewModel watchAppEditViewModel, String str, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchAppEditViewModel;
            this.$presetId = str;
            this.$watchAppPos = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$presetId, this.$watchAppPos, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0046  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 2
                r4 = 1
                java.lang.Object r1 = com.fossil.Yn7.d()
                int r0 = r6.label
                if (r0 == 0) goto L_0x0048
                if (r0 == r4) goto L_0x0020
                if (r0 != r5) goto L_0x0018
                java.lang.Object r0 = r6.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r7)
            L_0x0015:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r6.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r7)
            L_0x0027:
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel r2 = r6.this$0
                androidx.lifecycle.MutableLiveData r2 = com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.i(r2)
                java.lang.String r3 = r6.$watchAppPos
                r2.l(r3)
                com.fossil.Jx7 r2 = com.fossil.Bw7.c()
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$Ci$Aii r3 = new com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$Ci$Aii
                r4 = 0
                r3.<init>(r6, r4)
                r6.L$0 = r0
                r6.label = r5
                java.lang.Object r0 = com.fossil.Eu7.g(r2, r3, r6)
                if (r0 != r1) goto L_0x0015
                r0 = r1
                goto L_0x0017
            L_0x0048:
                com.fossil.El7.b(r7)
                com.mapped.Il6 r0 = r6.p$
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel r2 = r6.this$0
                java.util.List r2 = com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.f(r2)
                if (r2 != 0) goto L_0x0027
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel r2 = r6.this$0
                java.lang.String r3 = r6.$presetId
                r6.L$0 = r0
                r6.label = r4
                java.lang.Object r2 = r2.B(r3, r6)
                if (r2 != r1) goto L_0x0027
                r0 = r1
                goto L_0x0017
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$initFromSaveInstanceState$1", f = "WatchAppEditViewModel.kt", l = {135, 138, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 147}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public /* final */ /* synthetic */ List $savedCurrentPresetButtons;
        @DexIgnore
        public /* final */ /* synthetic */ List $savedOriginalPresetButton;
        @DexIgnore
        public /* final */ /* synthetic */ String $watchAppPos;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppEditViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$initFromSaveInstanceState$1$1", f = "WatchAppEditViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.e.i(this.this$0.this$0.o);
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(WatchAppEditViewModel watchAppEditViewModel, String str, List list, String str2, List list2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchAppEditViewModel;
            this.$presetId = str;
            this.$savedOriginalPresetButton = list;
            this.$watchAppPos = str2;
            this.$savedCurrentPresetButtons = list2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$presetId, this.$savedOriginalPresetButton, this.$watchAppPos, this.$savedCurrentPresetButtons, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0063  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0084  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00b2  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                r8 = 4
                r7 = 3
                r6 = 2
                r5 = 1
                java.lang.Object r4 = com.fossil.Yn7.d()
                int r0 = r9.label
                if (r0 == 0) goto L_0x0094
                if (r0 == r5) goto L_0x006d
                if (r0 == r6) goto L_0x0065
                if (r0 == r7) goto L_0x0036
                if (r0 != r8) goto L_0x002e
                java.lang.Object r0 = r9.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r10)
            L_0x001b:
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel r0 = r9.this$0
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.a(r0)
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel r0 = r9.this$0
                androidx.lifecycle.MutableLiveData r0 = com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.a(r0)
                java.util.List r1 = r9.$savedCurrentPresetButtons
                r0.l(r1)
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x002d:
                return r0
            L_0x002e:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0036:
                java.lang.Object r0 = r9.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r10)
            L_0x003d:
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel r1 = r9.this$0
                java.util.List r2 = r9.$savedOriginalPresetButton
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.m(r1, r2)
            L_0x0044:
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel r1 = r9.this$0
                androidx.lifecycle.MutableLiveData r1 = com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.i(r1)
                java.lang.String r2 = r9.$watchAppPos
                r1.l(r2)
                com.fossil.Jx7 r1 = com.fossil.Bw7.c()
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$Di$Aii r2 = new com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$Di$Aii
                r3 = 0
                r2.<init>(r9, r3)
                r9.L$0 = r0
                r9.label = r8
                java.lang.Object r0 = com.fossil.Eu7.g(r1, r2, r9)
                if (r0 != r4) goto L_0x001b
                r0 = r4
                goto L_0x002d
            L_0x0065:
                java.lang.Object r0 = r9.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r10)
                goto L_0x0044
            L_0x006d:
                java.lang.Object r0 = r9.L$1
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel r0 = (com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel) r0
                java.lang.Object r1 = r9.L$0
                com.mapped.Il6 r1 = (com.mapped.Il6) r1
                com.fossil.El7.b(r10)
                r3 = r0
                r2 = r10
            L_0x007a:
                r0 = r2
                com.fossil.Mo5 r0 = (com.fossil.Mo5) r0
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.l(r3, r0)
                java.util.List r0 = r9.$savedOriginalPresetButton
                if (r0 != 0) goto L_0x00b2
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel r0 = r9.this$0
                java.lang.String r2 = r9.$presetId
                r9.L$0 = r1
                r9.label = r6
                java.lang.Object r0 = r0.B(r2, r9)
                if (r0 != r4) goto L_0x00b0
                r0 = r4
                goto L_0x002d
            L_0x0094:
                com.fossil.El7.b(r10)
                com.mapped.Il6 r1 = r9.p$
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel r0 = r9.this$0
                com.portfolio.platform.preset.data.source.DianaPresetRepository r2 = com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.c(r0)
                java.lang.String r3 = r9.$presetId
                r9.L$0 = r1
                r9.L$1 = r0
                r9.label = r5
                java.lang.Object r2 = r2.n(r3, r9)
                if (r2 != r4) goto L_0x00c4
                r0 = r4
                goto L_0x002d
            L_0x00b0:
                r0 = r1
                goto L_0x0044
            L_0x00b2:
                com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel r0 = r9.this$0
                r9.L$0 = r1
                r9.label = r7
                java.lang.Object r0 = r0.A(r9)
                if (r0 != r4) goto L_0x00c1
                r0 = r4
                goto L_0x002d
            L_0x00c1:
                r0 = r1
                goto L_0x003d
            L_0x00c4:
                r3 = r0
                goto L_0x007a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.Di.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel", f = "WatchAppEditViewModel.kt", l = {Action.Presenter.NEXT, 312, 314}, m = "initWatchAppAndSetting")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(WatchAppEditViewModel watchAppEditViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = watchAppEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.A(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel", f = "WatchAppEditViewModel.kt", l = {290, 291}, m = "initializePreset")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(WatchAppEditViewModel watchAppEditViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = watchAppEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.B(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppEditViewModel a;

        @DexIgnore
        public Gi(WatchAppEditViewModel watchAppEditViewModel) {
            this.a = watchAppEditViewModel;
        }

        @DexIgnore
        public final MutableLiveData<WatchApp> a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchAppEditViewModel.u.a();
            local.d(a2, "transformWatchAppIdToModel watchAppId=" + str);
            WatchAppEditViewModel watchAppEditViewModel = this.a;
            Wg6.b(str, "id");
            WatchApp o = watchAppEditViewModel.o(str);
            if (o == null) {
                o = Ol5.c.a();
            }
            this.a.j.l(o);
            return this.a.j;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((String) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppEditViewModel a;

        @DexIgnore
        public Hi(WatchAppEditViewModel watchAppEditViewModel) {
            this.a = watchAppEditViewModel;
        }

        @DexIgnore
        public final MutableLiveData<String> a(String str) {
            Object obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = WatchAppEditViewModel.u.a();
            local.d(a2, "transformWatchAppPosToId pos=" + str);
            List list = (List) this.a.e.e();
            if (list != null) {
                Iterator it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj = null;
                        break;
                    }
                    Object next = it.next();
                    if (Wg6.a(((Oo5) next).b(), str)) {
                        obj = next;
                        break;
                    }
                }
                Oo5 oo5 = (Oo5) obj;
                if (oo5 != null) {
                    this.a.i.o(oo5.a());
                    return this.a.i;
                }
            }
            this.a.i.o("empty_watch_app_id");
            return this.a.i;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((String) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppEditViewModel a;

        @DexIgnore
        public Ii(WatchAppEditViewModel watchAppEditViewModel) {
            this.a = watchAppEditViewModel;
        }

        @DexIgnore
        public final MutableLiveData<Boolean> a(List<Oo5> list) {
            Object obj;
            Object obj2;
            Object obj3;
            boolean z = false;
            Wg6.b(list, "buttonList");
            boolean z2 = false;
            boolean z3 = false;
            for (T t : list) {
                List list2 = this.a.b;
                if (list2 != null) {
                    Iterator it = list2.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            obj = null;
                            break;
                        }
                        Object next = it.next();
                        if (Wg6.a(((Oo5) next).b(), t.b())) {
                            obj = next;
                            break;
                        }
                    }
                    Oo5 oo5 = (Oo5) obj;
                    String a2 = oo5 != null ? oo5.a() : null;
                    if (a2 == null) {
                        Wg6.i();
                        throw null;
                    } else if (!Wg6.a(a2, t.a())) {
                        z3 = true;
                    } else {
                        Iterator it2 = this.a.c.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                obj2 = null;
                                break;
                            }
                            Object next2 = it2.next();
                            if (Wg6.a(t.a(), ((DianaAppSetting) next2).getAppId())) {
                                obj2 = next2;
                                break;
                            }
                        }
                        DianaAppSetting dianaAppSetting = (DianaAppSetting) obj2;
                        Iterator it3 = this.a.d.iterator();
                        while (true) {
                            if (!it3.hasNext()) {
                                obj3 = null;
                                break;
                            }
                            Object next3 = it3.next();
                            if (Wg6.a(t.a(), ((DianaAppSetting) next3).getAppId())) {
                                obj3 = next3;
                                break;
                            }
                        }
                        DianaAppSetting dianaAppSetting2 = (DianaAppSetting) obj3;
                        FLogger.INSTANCE.getLocal().d(WatchAppEditViewModel.u.a(), "verify setting of " + t.a() + " originalSetting " + dianaAppSetting + " currentSetting " + dianaAppSetting2);
                        if (!Wg6.a(dianaAppSetting, dianaAppSetting2)) {
                            z2 = true;
                        }
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            FLogger.INSTANCE.getLocal().d(WatchAppEditViewModel.u.a(), "check preset isChanged isConfigChanged " + z3 + " isSettingChanged " + z2);
            MutableLiveData mutableLiveData = this.a.f;
            if (z3 || z2) {
                z = true;
            }
            mutableLiveData.l(Boolean.valueOf(z));
            return this.a.f;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((List) obj);
        }
    }

    /*
    static {
        String simpleName = WatchAppEditViewModel.class.getSimpleName();
        Wg6.b(simpleName, "WatchAppEditViewModel::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public WatchAppEditViewModel(DianaPresetRepository dianaPresetRepository, DianaAppSettingRepository dianaAppSettingRepository, WatchAppRepository watchAppRepository, An4 an4) {
        Wg6.c(dianaPresetRepository, "mDianaPresetRepository");
        Wg6.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        Wg6.c(watchAppRepository, "mWatchAppRepository");
        Wg6.c(an4, "sharedPreferencesManager");
        this.p = dianaPresetRepository;
        this.q = dianaAppSettingRepository;
        this.r = watchAppRepository;
        this.s = an4;
        LiveData<String> c2 = Ss0.c(this.h, new Hi(this));
        Wg6.b(c2, "Transformations.switchMa\u2026dWatchAppIdLiveData\n    }");
        this.l = c2;
        LiveData<WatchApp> c3 = Ss0.c(c2, new Gi(this));
        Wg6.b(c3, "Transformations.switchMa\u2026tedWatchAppLiveData\n    }");
        this.m = c3;
        LiveData<Boolean> c4 = Ss0.c(this.e, new Ii(this));
        Wg6.b(c4, "Transformations.switchMa\u2026resetButtonsChanged\n    }");
        this.n = c4;
        this.o = new Bi(this);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a6  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x013b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object A(com.mapped.Xe6<? super com.mapped.Cd6> r12) {
        /*
        // Method dump skipped, instructions count: 318
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.A(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object B(java.lang.String r9, com.mapped.Xe6<? super com.mapped.Cd6> r10) {
        /*
            r8 = this;
            r7 = 2
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r10 instanceof com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.Fi
            if (r0 == 0) goto L_0x0046
            r0 = r10
            com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$Fi r0 = (com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.Fi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0046
            int r1 = r1 + r3
            r0.label = r1
            r4 = r0
        L_0x0015:
            java.lang.Object r3 = r4.result
            java.lang.Object r6 = com.fossil.Yn7.d()
            int r0 = r4.label
            if (r0 == 0) goto L_0x0078
            if (r0 == r5) goto L_0x0055
            if (r0 != r7) goto L_0x004d
            java.lang.Object r0 = r4.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r4.L$0
            com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel r0 = (com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel) r0
            com.fossil.El7.b(r3)
        L_0x002e:
            com.fossil.Mo5 r1 = r0.a
            if (r1 == 0) goto L_0x0043
            java.util.List r1 = r1.a()
            if (r1 == 0) goto L_0x0043
            r0.b = r1
            androidx.lifecycle.MutableLiveData<java.util.List<com.fossil.Oo5>> r0 = r0.e
            java.util.ArrayList r1 = com.mapped.CustomizeConfigurationExt.a(r1)
            r0.l(r1)
        L_0x0043:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0045:
            return r0
        L_0x0046:
            com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$Fi r0 = new com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel$Fi
            r0.<init>(r8, r10)
            r4 = r0
            goto L_0x0015
        L_0x004d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0055:
            java.lang.Object r0 = r4.L$2
            com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel r0 = (com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel) r0
            java.lang.Object r1 = r4.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r4.L$0
            com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel r2 = (com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel) r2
            com.fossil.El7.b(r3)
            r5 = r0
        L_0x0065:
            r0 = r3
            com.fossil.Mo5 r0 = (com.fossil.Mo5) r0
            r5.a = r0
            r4.L$0 = r2
            r4.L$1 = r1
            r4.label = r7
            java.lang.Object r0 = r2.A(r4)
            if (r0 != r6) goto L_0x00ad
            r0 = r6
            goto L_0x0045
        L_0x0078:
            com.fossil.El7.b(r3)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.t
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "initializePreset presetId="
            r2.append(r3)
            r2.append(r9)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            com.portfolio.platform.preset.data.source.DianaPresetRepository r0 = r8.p
            r4.L$0 = r8
            r4.L$1 = r9
            r4.L$2 = r8
            r4.label = r5
            java.lang.Object r3 = r0.n(r9, r4)
            if (r3 != r6) goto L_0x00a9
            r0 = r6
            goto L_0x0045
        L_0x00a9:
            r5 = r8
            r2 = r8
            r1 = r9
            goto L_0x0065
        L_0x00ad:
            r0 = r2
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.B(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final boolean C() {
        Boolean e2 = this.f.e();
        if (e2 != null) {
            return e2.booleanValue();
        }
        return false;
    }

    @DexIgnore
    public final boolean D(String str) {
        T t2;
        T t3;
        Wg6.c(str, "watchAppId");
        if (Wg6.a(str, "empty")) {
            return false;
        }
        List<Oo5> e2 = this.e.e();
        if (e2 != null) {
            Iterator<T> it = e2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t3 = null;
                    break;
                }
                T next = it.next();
                if (Wg6.a(next.a(), str)) {
                    t3 = next;
                    break;
                }
            }
            t2 = t3;
        } else {
            t2 = null;
        }
        return t2 != null;
    }

    @DexIgnore
    public final Object E(Xe6<? super Cd6> xe6) {
        Object upsertDianaAppSetting = this.q.upsertDianaAppSetting(this.d, xe6);
        return upsertDianaAppSetting == Yn7.d() ? upsertDianaAppSetting : Cd6.a;
    }

    @DexIgnore
    public final void F(String str) {
        Wg6.c(str, "watchAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = t;
        local.d(str2, "setSelectedWatchApp watchAppPos=" + str);
        this.h.l(str);
    }

    @DexIgnore
    public final void G(List<Oo5> list) {
        Wg6.c(list, "presetButton");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "savePreset presetButton=" + list);
        this.e.l(CustomizeConfigurationExt.a(list));
    }

    @DexIgnore
    public final void H(String str, Parcelable parcelable) {
        T t2;
        String str2 = null;
        Wg6.c(str, "appId");
        Wg6.c(parcelable, "settingParcelable");
        Iterator<T> it = this.d.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (Wg6.a(next.getAppId(), str)) {
                t2 = next;
                break;
            }
        }
        T t3 = t2;
        FLogger.INSTANCE.getLocal().d(t, "update setting of " + str + " setting " + parcelable + " currentAppSetting " + ((Object) t3));
        int hashCode = str.hashCode();
        if (hashCode != -829740640) {
            if (hashCode == 1223440372 && str.equals("weather")) {
                str2 = Jj5.a((WeatherWatchAppSetting) parcelable);
            }
        } else if (str.equals("commute-time")) {
            str2 = Jj5.a((CommuteTimeWatchAppSetting) parcelable);
        }
        FLogger.INSTANCE.getLocal().d(t, "json setting of " + str + " setting " + str2);
        if (str2 != null) {
            if (t3 != null) {
                t3.setSetting(str2);
            } else {
                String e2 = Ym5.e(24);
                Wg6.b(e2, "StringHelper.randomUUID(24)");
                this.d.add(new DianaAppSetting(e2, str, "watch_app", str2));
            }
            MutableLiveData<List<Oo5>> mutableLiveData = this.e;
            mutableLiveData.l(mutableLiveData.e());
        }
    }

    @DexIgnore
    public final List<WatchApp> n(String str) {
        Wg6.c(str, "category");
        CopyOnWriteArrayList<WatchApp> copyOnWriteArrayList = this.g;
        ArrayList arrayList = new ArrayList();
        for (T t2 : copyOnWriteArrayList) {
            if (t2.getCategories().contains(str)) {
                arrayList.add(t2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final WatchApp o(String str) {
        T t2;
        Wg6.c(str, "watchAppId");
        Iterator<T> it = this.g.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (Wg6.a(str, next.getWatchappId())) {
                t2 = next;
                break;
            }
        }
        return t2;
    }

    @DexIgnore
    @Override // com.fossil.Ts0
    public void onCleared() {
        this.e.m(this.o);
        super.onCleared();
    }

    @DexIgnore
    public final Mo5 p() {
        return this.a;
    }

    @DexIgnore
    public final MutableLiveData<List<Oo5>> q() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Boolean> r() {
        return this.n;
    }

    @DexIgnore
    public final List<Oo5> s() {
        return this.b;
    }

    @DexIgnore
    public final List<DianaAppSetting> t() {
        List<DianaAppSetting> list = this.d;
        ArrayList arrayList = new ArrayList();
        for (T t2 : list) {
            T t3 = t2;
            if (Wg6.a(t3.getAppId(), "commute-time") || Wg6.a(t3.getAppId(), "weather")) {
                arrayList.add(t2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final LiveData<WatchApp> u() {
        return this.m;
    }

    @DexIgnore
    public final MutableLiveData<String> v() {
        return this.h;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v15, resolved type: com.portfolio.platform.data.model.setting.WeatherWatchAppSetting */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0095, code lost:
        if (r1 != null) goto L_0x0097;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object w(java.lang.String r9, com.mapped.Xe6<? super android.os.Parcelable> r10) {
        /*
        // Method dump skipped, instructions count: 326
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.WatchAppEditViewModel.w(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final List<DianaAppSetting> x() {
        return this.d;
    }

    @DexIgnore
    public final Rm6 y(String str, String str2) {
        Wg6.c(str, "presetId");
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Ci(this, str, str2, null), 3, null);
    }

    @DexIgnore
    public final Rm6 z(String str, List<Oo5> list, List<Oo5> list2, String str2) {
        Wg6.c(str, "presetId");
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Di(this, str, list, str2, list2, null), 3, null);
    }
}
