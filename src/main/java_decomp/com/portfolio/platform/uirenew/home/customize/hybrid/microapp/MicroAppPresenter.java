package com.portfolio.platform.uirenew.home.customize.hybrid.microapp;

import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.Ao7;
import com.fossil.Bl5;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gl7;
import com.fossil.Gu7;
import com.fossil.Ic6;
import com.fossil.Jc6;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Ss0;
import com.fossil.Ub6;
import com.fossil.Um5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppPresenter extends Ic6 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public HybridCustomizeViewModel e;
    @DexIgnore
    public ArrayList<Category> f; // = new ArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<MicroApp> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<MicroApp>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Gl7<String, Boolean, Parcelable>> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<String> k;
    @DexIgnore
    public /* final */ Ls0<String> l;
    @DexIgnore
    public /* final */ LiveData<List<MicroApp>> m;
    @DexIgnore
    public /* final */ Ls0<List<MicroApp>> n;
    @DexIgnore
    public /* final */ Ls0<Gl7<String, Boolean, Parcelable>> o;
    @DexIgnore
    public /* final */ Jc6 p;
    @DexIgnore
    public /* final */ CategoryRepository q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$checkSettingOfSelectedMicroApp$1", f = "MicroAppPresenter.kt", l = {229}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$checkSettingOfSelectedMicroApp$1$1", f = "MicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Parcelable>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Parcelable> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return MicroAppPresenter.z(this.this$0.this$0).q(this.this$0.$id);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(MicroAppPresenter microAppPresenter, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = microAppPresenter;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, this.$id, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            boolean d;
            Object g;
            Parcelable parcelable = null;
            Object d2 = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                d = Bl5.c.d(this.$id);
                if (d) {
                    Dv7 h = this.this$0.h();
                    Aii aii = new Aii(this, null);
                    this.L$0 = il6;
                    this.L$1 = null;
                    this.Z$0 = d;
                    this.label = 1;
                    g = Eu7.g(h, aii, this);
                    if (g == d2) {
                        return d2;
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = MicroAppPresenter.r;
                local.d(str, "checkSettingOfSelectedMicroApp id=" + this.$id + " settings=" + parcelable);
                this.this$0.j.l(new Gl7(this.$id, Ao7.a(d), parcelable));
                return Cd6.a;
            } else if (i == 1) {
                d = this.Z$0;
                Parcelable parcelable2 = (Parcelable) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            parcelable = (Parcelable) g;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = MicroAppPresenter.r;
            local2.d(str2, "checkSettingOfSelectedMicroApp id=" + this.$id + " settings=" + parcelable);
            this.this$0.j.l(new Gl7(this.$id, Ao7.a(d), parcelable));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public Bi(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        public final void a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = MicroAppPresenter.r;
            local.d(str2, "onLiveDataChanged category=" + str);
            if (str != null) {
                this.a.p.u0(str);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mCategoryOfSelectedMicroAppTransformation$1$2$1", f = "MicroAppPresenter.kt", l = {75}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MicroAppPresenter $this_run;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$mCategoryOfSelectedMicroAppTransformation$1$2$1$allCategory$1", f = "MicroAppPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Category>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super List<? extends Category>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        return this.this$0.$this_run.q.getAllCategories();
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(MicroAppPresenter microAppPresenter, Xe6 xe6) {
                super(2, xe6);
                this.$this_run = microAppPresenter;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$this_run, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Dv7 b = Bw7.b();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    g = Eu7.g(b, aiii, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) g;
                if (!list.isEmpty()) {
                    this.$this_run.h.l(((Category) list.get(0)).getId());
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Ci(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        public final MutableLiveData<String> a(MicroApp microApp) {
            this.a.G(microApp.getId());
            if (microApp != null) {
                String str = (String) this.a.h.e();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = MicroAppPresenter.r;
                local.d(str2, "transform from selected microApp to category currentCategory=" + str + " compsCategories=" + microApp.getCategories());
                ArrayList<String> categories = microApp.getCategories();
                if (str == null || !categories.contains(str)) {
                    this.a.h.l(categories.get(0));
                } else {
                    this.a.h.l(str);
                }
            } else {
                MicroAppPresenter microAppPresenter = this.a;
                Rm6 unused = Gu7.d(microAppPresenter.k(), null, null, new Aii(microAppPresenter, null), 3, null);
            }
            return this.a.h;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((MicroApp) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<List<? extends MicroApp>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public Di(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        public final void a(List<MicroApp> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = MicroAppPresenter.r;
            StringBuilder sb = new StringBuilder();
            sb.append("onLiveDataChanged microApps by category value=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d(str, sb.toString());
            if (list != null) {
                this.a.p.U5(list);
                MicroApp e = MicroAppPresenter.z(this.a).s().e();
                if (e != null) {
                    this.a.p.A1(e);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends MicroApp> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public Ei(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        public final MutableLiveData<List<MicroApp>> a(String str) {
            T t;
            boolean z;
            FLogger.INSTANCE.getLocal().d(MicroAppPresenter.r, "transform from category to list microApp with category=" + str);
            HybridCustomizeViewModel z2 = MicroAppPresenter.z(this.a);
            Wg6.b(str, "category");
            List<MicroApp> m = z2.m(str);
            ArrayList arrayList = new ArrayList();
            HybridPreset e = MicroAppPresenter.z(this.a).o().e();
            MicroApp e2 = MicroAppPresenter.z(this.a).s().e();
            String id = e2 != null ? e2.getId() : null;
            if (e != null) {
                for (MicroApp microApp : m) {
                    Iterator<T> it = e.getButtons().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        T t2 = next;
                        if (!Wg6.a(t2.getAppId(), microApp.getId()) || !(!Wg6.a(t2.getAppId(), id))) {
                            z = false;
                            continue;
                        } else {
                            z = true;
                            continue;
                        }
                        if (z) {
                            t = next;
                            break;
                        }
                    }
                    if (t == null || Wg6.a(microApp.getId(), "empty")) {
                        arrayList.add(microApp);
                    }
                }
            }
            this.a.i.l(arrayList);
            return this.a.i;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((String) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<Gl7<? extends String, ? extends Boolean, ? extends Parcelable>> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public Fi(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        public final void a(Gl7<String, Boolean, ? extends Parcelable> gl7) {
            String str;
            String str2;
            if (gl7 != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = MicroAppPresenter.r;
                local.d(str3, "onLiveDataChanged setting of " + gl7.getFirst() + " isSettingRequired " + gl7.getSecond().booleanValue() + " setting " + ((Parcelable) gl7.getThird()));
                boolean booleanValue = gl7.getSecond().booleanValue();
                MicroApp e = MicroAppPresenter.z(this.a).s().e();
                if (!Wg6.a(e != null ? e.getId() : null, gl7.getFirst())) {
                    return;
                }
                if (booleanValue) {
                    Parcelable parcelable = (Parcelable) gl7.getThird();
                    if (parcelable == null) {
                        str = Bl5.c.a(gl7.getFirst());
                        str2 = "";
                    } else {
                        try {
                            String first = gl7.getFirst();
                            if (Wg6.a(first, MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                                str = "";
                                str2 = ((CommuteTimeSetting) parcelable).getAddress();
                            } else if (Wg6.a(first, MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                                str = "";
                                str2 = ((SecondTimezoneSetting) parcelable).getTimeZoneName();
                            } else {
                                if (Wg6.a(first, MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
                                    str = "";
                                    str2 = ((Ringtone) parcelable).getRingtoneName();
                                }
                                str = "";
                                str2 = "";
                            }
                        } catch (Exception e2) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str4 = MicroAppPresenter.r;
                            local2.d(str4, "exception when parse micro app setting " + e2);
                        }
                    }
                    this.a.p.g0(true, gl7.getFirst(), str, str2);
                    return;
                }
                this.a.p.g0(false, gl7.getFirst(), "", null);
                if (e != null) {
                    Jc6 jc6 = this.a.p;
                    String d = Um5.d(PortfolioApp.get.instance(), e.getDescriptionKey(), e.getDescription());
                    Wg6.b(d, "LanguageHelper.getString\u2026ptionKey, it.description)");
                    jc6.g3(d);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Gl7<? extends String, ? extends Boolean, ? extends Parcelable> gl7) {
            a(gl7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$start$1", f = "MicroAppPresenter.kt", l = {187}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter$start$1$allCategory$1", f = "MicroAppPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Category>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Gi gi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = gi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends Category>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.q.getAllCategories();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(MicroAppPresenter microAppPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = microAppPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.this$0, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(i2, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ArrayList arrayList = new ArrayList();
            for (Category category : (List) g) {
                if (!MicroAppPresenter.z(this.this$0).m(category.getId()).isEmpty()) {
                    arrayList.add(category);
                }
            }
            this.this$0.f.clear();
            this.this$0.f.addAll(arrayList);
            this.this$0.p.S(this.this$0.f);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> implements Ls0<MicroApp> {
        @DexIgnore
        public /* final */ /* synthetic */ MicroAppPresenter a;

        @DexIgnore
        public Hi(MicroAppPresenter microAppPresenter) {
            this.a = microAppPresenter;
        }

        @DexIgnore
        public final void a(MicroApp microApp) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = MicroAppPresenter.r;
            local.d(str, "onLiveDataChanged selectedMicroApp value=" + microApp);
            this.a.g.l(microApp);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(MicroApp microApp) {
            a(microApp);
        }
    }

    /*
    static {
        String simpleName = MicroAppPresenter.class.getSimpleName();
        Wg6.b(simpleName, "MicroAppPresenter::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public MicroAppPresenter(Jc6 jc6, CategoryRepository categoryRepository) {
        Wg6.c(jc6, "mView");
        Wg6.c(categoryRepository, "mCategoryRepository");
        this.p = jc6;
        this.q = categoryRepository;
        LiveData<String> c = Ss0.c(this.g, new Ci(this));
        Wg6.b(c, "Transformations.switchMa\u2026tedMicroAppLiveData\n    }");
        this.k = c;
        this.l = new Bi(this);
        LiveData<List<MicroApp>> c2 = Ss0.c(this.k, new Ei(this));
        Wg6.b(c2, "Transformations.switchMa\u2026tedCategoryLiveData\n    }");
        this.m = c2;
        this.n = new Di(this);
        this.o = new Fi(this);
    }

    @DexIgnore
    public static final /* synthetic */ HybridCustomizeViewModel z(MicroAppPresenter microAppPresenter) {
        HybridCustomizeViewModel hybridCustomizeViewModel = microAppPresenter.e;
        if (hybridCustomizeViewModel != null) {
            return hybridCustomizeViewModel;
        }
        Wg6.n("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    public final Rm6 G(String str) {
        return Gu7.d(k(), null, null, new Ai(this, str, null), 3, null);
    }

    @DexIgnore
    public void H() {
        this.p.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(r, "onStart");
        this.k.i(this.l);
        this.m.i(this.n);
        this.j.i(this.o);
        if (this.f.isEmpty()) {
            Rm6 unused = Gu7.d(k(), null, null, new Gi(this, null), 3, null);
        } else {
            this.p.S(this.f);
        }
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            LiveData<MicroApp> s = hybridCustomizeViewModel.s();
            Jc6 jc6 = this.p;
            if (jc6 != null) {
                s.h((Ub6) jc6, new Hi(this));
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.MicroAppFragment");
        }
        Wg6.n("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            LiveData<MicroApp> s = hybridCustomizeViewModel.s();
            Jc6 jc6 = this.p;
            if (jc6 != null) {
                s.n((Ub6) jc6);
                this.i.m(this.n);
                this.h.m(this.l);
                this.j.m(this.o);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.hybrid.MicroAppFragment");
        }
        Wg6.n("mHybridCustomizeViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ic6
    public void n() {
        T t;
        T t2;
        T t3;
        String str;
        String appId;
        String appId2;
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            HybridPreset e2 = hybridCustomizeViewModel.o().e();
            if (e2 != null) {
                Iterator<T> it = e2.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (Wg6.a(next.getPosition(), ViewHierarchy.DIMENSION_TOP_KEY)) {
                        t = next;
                        break;
                    }
                }
                T t4 = t;
                String str2 = (t4 == null || (appId2 = t4.getAppId()) == null) ? "empty" : appId2;
                Iterator<T> it2 = e2.getButtons().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next2 = it2.next();
                    if (Wg6.a(next2.getPosition(), "middle")) {
                        t2 = next2;
                        break;
                    }
                }
                T t5 = t2;
                String str3 = (t5 == null || (appId = t5.getAppId()) == null) ? "empty" : appId;
                Iterator<T> it3 = e2.getButtons().iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        t3 = null;
                        break;
                    }
                    T next3 = it3.next();
                    if (Wg6.a(next3.getPosition(), "bottom")) {
                        t3 = next3;
                        break;
                    }
                }
                T t6 = t3;
                if (t6 == null || (str = t6.getAppId()) == null) {
                    str = "empty";
                }
                this.p.a0(str2, str3, str);
                return;
            }
            this.p.a0("empty", "empty", "empty");
            return;
        }
        Wg6.n("mHybridCustomizeViewModel");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004f, code lost:
        if (r1 != null) goto L_0x0051;
     */
    @DexIgnore
    @Override // com.fossil.Ic6
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void o() {
        /*
            r6 = this;
            r3 = 0
            com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel r0 = r6.e
            if (r0 == 0) goto L_0x0094
            androidx.lifecycle.LiveData r0 = r0.s()
            java.lang.Object r0 = r0.e()
            com.portfolio.platform.data.model.room.microapp.MicroApp r0 = (com.portfolio.platform.data.model.room.microapp.MicroApp) r0
            if (r0 == 0) goto L_0x0066
            com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel r1 = r6.e
            if (r1 == 0) goto L_0x008e
            androidx.lifecycle.MutableLiveData r1 = r1.o()
            java.lang.Object r1 = r1.e()
            com.portfolio.platform.data.model.room.microapp.HybridPreset r1 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r1
            if (r1 == 0) goto L_0x0067
            java.util.ArrayList r1 = r1.getButtons()
            if (r1 == 0) goto L_0x0067
            java.util.Iterator r4 = r1.iterator()
        L_0x002b:
            boolean r1 = r4.hasNext()
            if (r1 == 0) goto L_0x009a
            java.lang.Object r2 = r4.next()
            r1 = r2
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r1 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r1
            java.lang.String r5 = r0.getId()
            java.lang.String r1 = r1.getAppId()
            boolean r1 = com.mapped.Wg6.a(r5, r1)
            if (r1 == 0) goto L_0x002b
            r1 = r2
        L_0x0047:
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r1 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r1
            if (r1 == 0) goto L_0x0067
            java.lang.String r1 = r1.getSettings()
            if (r1 == 0) goto L_0x0067
        L_0x0051:
            java.lang.String r0 = r0.getId()
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r2 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME
            java.lang.String r2 = r2.getValue()
            boolean r2 = com.mapped.Wg6.a(r0, r2)
            if (r2 == 0) goto L_0x006a
            com.fossil.Jc6 r0 = r6.p
            r0.f0(r1)
        L_0x0066:
            return
        L_0x0067:
            java.lang.String r1 = ""
            goto L_0x0051
        L_0x006a:
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r2 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TIME2_ID
            java.lang.String r2 = r2.getValue()
            boolean r2 = com.mapped.Wg6.a(r0, r2)
            if (r2 == 0) goto L_0x007c
            com.fossil.Jc6 r0 = r6.p
            r0.d1(r1)
            goto L_0x0066
        L_0x007c:
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r2 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_RING_PHONE
            java.lang.String r2 = r2.getValue()
            boolean r0 = com.mapped.Wg6.a(r0, r2)
            if (r0 == 0) goto L_0x0066
            com.fossil.Jc6 r0 = r6.p
            r0.a5(r1)
            goto L_0x0066
        L_0x008e:
            java.lang.String r0 = "mHybridCustomizeViewModel"
            com.mapped.Wg6.n(r0)
            throw r3
        L_0x0094:
            java.lang.String r0 = "mHybridCustomizeViewModel"
            com.mapped.Wg6.n(r0)
            throw r3
        L_0x009a:
            r1 = r3
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter.o():void");
    }

    @DexIgnore
    @Override // com.fossil.Ic6
    public void p(Category category) {
        Wg6.c(category, "category");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "category change " + category);
        this.h.l(category.getId());
    }

    @DexIgnore
    @Override // com.fossil.Ic6
    public void q(String str) {
        T t;
        Wg6.c(str, "newMicroAppId");
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            MicroApp n2 = hybridCustomizeViewModel.n(str);
            FLogger.INSTANCE.getLocal().d(r, "onUserChooseMicroApp " + n2);
            if (n2 != null) {
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.e;
                if (hybridCustomizeViewModel2 != null) {
                    HybridPreset e2 = hybridCustomizeViewModel2.o().e();
                    if (e2 != null) {
                        HybridPreset clone = e2.clone();
                        ArrayList arrayList = new ArrayList();
                        HybridCustomizeViewModel hybridCustomizeViewModel3 = this.e;
                        if (hybridCustomizeViewModel3 != null) {
                            String e3 = hybridCustomizeViewModel3.t().e();
                            if (e3 != null) {
                                Wg6.b(e3, "mHybridCustomizeViewMode\u2026ctedMicroAppPos().value!!");
                                String str2 = e3;
                                HybridCustomizeViewModel hybridCustomizeViewModel4 = this.e;
                                if (hybridCustomizeViewModel4 == null) {
                                    Wg6.n("mHybridCustomizeViewModel");
                                    throw null;
                                } else if (!hybridCustomizeViewModel4.y(str)) {
                                    Iterator<HybridPresetAppSetting> it = clone.getButtons().iterator();
                                    while (it.hasNext()) {
                                        HybridPresetAppSetting next = it.next();
                                        if (Wg6.a(next.getPosition(), str2)) {
                                            arrayList.add(new HybridPresetAppSetting(str2, str, next.getLocalUpdateAt()));
                                        } else {
                                            arrayList.add(next);
                                        }
                                    }
                                    clone.getButtons().clear();
                                    clone.getButtons().addAll(arrayList);
                                    FLogger.INSTANCE.getLocal().d(r, "Update current preset=" + clone);
                                    HybridCustomizeViewModel hybridCustomizeViewModel5 = this.e;
                                    if (hybridCustomizeViewModel5 != null) {
                                        hybridCustomizeViewModel5.B(clone);
                                    } else {
                                        Wg6.n("mHybridCustomizeViewModel");
                                        throw null;
                                    }
                                } else {
                                    Iterator<T> it2 = e2.getButtons().iterator();
                                    while (true) {
                                        if (!it2.hasNext()) {
                                            t = null;
                                            break;
                                        }
                                        T next2 = it2.next();
                                        if (Wg6.a(next2.getAppId(), str)) {
                                            t = next2;
                                            break;
                                        }
                                    }
                                    T t2 = t;
                                    if (t2 != null) {
                                        HybridCustomizeViewModel hybridCustomizeViewModel6 = this.e;
                                        if (hybridCustomizeViewModel6 != null) {
                                            hybridCustomizeViewModel6.A(t2.getPosition());
                                        } else {
                                            Wg6.n("mHybridCustomizeViewModel");
                                            throw null;
                                        }
                                    }
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.n("mHybridCustomizeViewModel");
                            throw null;
                        }
                    }
                } else {
                    Wg6.n("mHybridCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            Wg6.n("mHybridCustomizeViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ic6
    public void r(HybridCustomizeViewModel hybridCustomizeViewModel) {
        Wg6.c(hybridCustomizeViewModel, "viewModel");
        this.e = hybridCustomizeViewModel;
    }

    @DexIgnore
    @Override // com.fossil.Ic6
    public void s(String str, String str2) {
        T t;
        Wg6.c(str, "microAppId");
        Wg6.c(str2, MicroAppSetting.SETTING);
        HybridCustomizeViewModel hybridCustomizeViewModel = this.e;
        if (hybridCustomizeViewModel != null) {
            HybridPreset e2 = hybridCustomizeViewModel.o().e();
            if (e2 != null) {
                HybridPreset clone = e2.clone();
                Iterator<T> it = clone.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (Wg6.a(next.getAppId(), str)) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    t2.setSettings(str2);
                }
                FLogger.INSTANCE.getLocal().d(r, "update new setting " + str2 + " of " + str);
                HybridCustomizeViewModel hybridCustomizeViewModel2 = this.e;
                if (hybridCustomizeViewModel2 != null) {
                    hybridCustomizeViewModel2.B(clone);
                } else {
                    Wg6.n("mHybridCustomizeViewModel");
                    throw null;
                }
            }
        } else {
            Wg6.n("mHybridCustomizeViewModel");
            throw null;
        }
    }
}
