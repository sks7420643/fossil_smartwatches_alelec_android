package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.Ia6;
import com.fossil.Ta6;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherSettingActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public WeatherSettingPresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            Wg6.c(fragment, "fragment");
            Wg6.c(str, MicroAppSetting.SETTING);
            Intent intent = new Intent(fragment.getContext(), WeatherSettingActivity.class);
            intent.putExtra(Constants.USER_SETTING, str);
            fragment.startActivityForResult(intent, 105);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        String str;
        FLogger.INSTANCE.getLocal().d(r(), "onCreate");
        super.onCreate(bundle);
        setContentView(2131558428);
        Ia6 ia6 = (Ia6) getSupportFragmentManager().Y(2131362158);
        if (ia6 == null) {
            ia6 = Ia6.m.a();
            k(ia6, Ia6.m.b(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (ia6 != null) {
            iface.X(new Ta6(ia6)).a(this);
            Intent intent = getIntent();
            if (intent == null || (str = intent.getStringExtra(Constants.USER_SETTING)) == null) {
                str = "";
            }
            WeatherSettingPresenter weatherSettingPresenter = this.A;
            if (weatherSettingPresenter != null) {
                weatherSettingPresenter.w(str);
            } else {
                Wg6.n("mWeatherSettingPresenter");
                throw null;
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingContract.View");
        }
    }
}
