package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.P06;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationAppsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public NotificationAppsPresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment) {
            Wg6.c(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationAppsActivity.class);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        NotificationAppsFragment notificationAppsFragment = (NotificationAppsFragment) getSupportFragmentManager().Y(2131362158);
        if (notificationAppsFragment == null) {
            notificationAppsFragment = NotificationAppsFragment.s.b();
            k(notificationAppsFragment, NotificationAppsFragment.s.a(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (notificationAppsFragment != null) {
            iface.y0(new P06(notificationAppsFragment)).a(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsContract.View");
    }
}
