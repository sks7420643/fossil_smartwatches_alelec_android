package com.portfolio.platform.uirenew.home.details.workout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.Fn6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutEditActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str) {
            Wg6.c(context, "context");
            Wg6.c(str, "workoutSessionId");
            Intent intent = new Intent(context, WorkoutEditActivity.class);
            intent.setFlags(536870912);
            intent.putExtra("EXTRA_WORKOUT_SESSION_ID", str);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        if (((Fn6) getSupportFragmentManager().Y(2131362158)) == null) {
            Fn6.Ai ai = Fn6.s;
            String stringExtra = getIntent().getStringExtra("EXTRA_WORKOUT_SESSION_ID");
            Wg6.b(stringExtra, "intent.getStringExtra(EXTRA_WORKOUT_SESSION_ID)");
            i(ai.a(stringExtra), 2131362158);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onResume() {
        super.onResume();
        l(false);
    }
}
