package com.portfolio.platform.uirenew.home.profile.theme;

import androidx.lifecycle.MutableLiveData;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Ts0;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.source.ThemeRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemesViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public ArrayList<Theme> a; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<Style> b; // = new ArrayList<>();
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public MutableLiveData<Ai> e; // = new MutableLiveData<>();
    @DexIgnore
    public Ai f; // = new Ai(null, null, null, null, false, 31, null);
    @DexIgnore
    public /* final */ ThemeRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public ArrayList<Theme> a;
        @DexIgnore
        public List<Style> b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public boolean e;

        @DexIgnore
        public Ai() {
            this(null, null, null, null, false, 31, null);
        }

        @DexIgnore
        public Ai(ArrayList<Theme> arrayList, List<Style> list, String str, String str2, boolean z) {
            this.a = arrayList;
            this.b = list;
            this.c = str;
            this.d = str2;
            this.e = z;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(ArrayList arrayList, List list, String str, String str2, boolean z, int i, Qg6 qg6) {
            this((i & 1) != 0 ? null : arrayList, (i & 2) != 0 ? null : list, (i & 4) != 0 ? null : str, (i & 8) == 0 ? str2 : null, (i & 16) != 0 ? false : z);
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final boolean b() {
            return this.e;
        }

        @DexIgnore
        public final ArrayList<Theme> c() {
            return this.a;
        }

        @DexIgnore
        public final List<Style> d() {
            return this.b;
        }

        @DexIgnore
        public final String e() {
            return this.d;
        }

        @DexIgnore
        public final void f(boolean z) {
            this.e = z;
        }

        @DexIgnore
        public final void g(ArrayList<Theme> arrayList, String str, String str2, List<Style> list) {
            this.a = arrayList;
            this.d = str2;
            this.c = str;
            this.e = false;
            this.b = list;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$applyTheme$1", f = "ThemesViewModel.kt", l = {66}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThemesViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$applyTheme$1$1", f = "ThemesViewModel.kt", l = {68, 69}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x0049  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r1 = com.fossil.Yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x004b
                    if (r0 == r6) goto L_0x0032
                    if (r0 != r7) goto L_0x002a
                    java.lang.Object r0 = r8.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r9)
                L_0x0015:
                    com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$Bi r0 = r8.this$0
                    com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel r0 = r0.this$0
                    com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$Ai r0 = com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel.f(r0)
                    r0.f(r6)
                    com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$Bi r0 = r8.this$0
                    com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel r0 = r0.this$0
                    com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel.a(r0)
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0029:
                    return r0
                L_0x002a:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0032:
                    java.lang.Object r0 = r8.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r9)
                L_0x0039:
                    com.portfolio.platform.manager.ThemeManager$Ai r2 = com.portfolio.platform.manager.ThemeManager.l
                    com.portfolio.platform.manager.ThemeManager r2 = r2.a()
                    r8.L$0 = r0
                    r8.label = r7
                    java.lang.Object r0 = r2.i(r8)
                    if (r0 != r1) goto L_0x0015
                    r0 = r1
                    goto L_0x0029
                L_0x004b:
                    com.fossil.El7.b(r9)
                    com.mapped.Il6 r0 = r8.p$
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    java.lang.String r3 = com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel.h()
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "apply theme "
                    r4.append(r5)
                    com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$Bi r5 = r8.this$0
                    com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel r5 = r5.this$0
                    java.lang.String r5 = com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel.g(r5)
                    r4.append(r5)
                    java.lang.String r4 = r4.toString()
                    r2.d(r3, r4)
                    com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$Bi r2 = r8.this$0
                    com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel r2 = r2.this$0
                    com.portfolio.platform.data.source.ThemeRepository r2 = com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel.e(r2)
                    com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$Bi r3 = r8.this$0
                    com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel r3 = r3.this$0
                    java.lang.String r3 = com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel.g(r3)
                    r8.L$0 = r0
                    r8.label = r6
                    java.lang.Object r2 = r2.setCurrentThemeId(r3, r8)
                    if (r2 != r1) goto L_0x0039
                    r0 = r1
                    goto L_0x0029
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel.Bi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(ThemesViewModel themesViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = themesViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$deleteTheme$1", f = "ThemesViewModel.kt", l = {79, 80, 82, 83, 85}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThemesViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(ThemesViewModel themesViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = themesViewModel;
            this.$id = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$id, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x00d3  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00e9  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0124  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x013b  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0164  */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x016b  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x016f  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x0179  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0181  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 394
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$onThemeChange$1", f = "ThemesViewModel.kt", l = {50}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $themeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThemesViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$onThemeChange$1$1", f = "ThemesViewModel.kt", l = {51}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super ArrayList<Style>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super ArrayList<Style>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object listStyleById;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    ThemeRepository themeRepository = this.this$0.this$0.g;
                    String str = this.this$0.$themeId;
                    this.L$0 = il6;
                    this.label = 1;
                    listStyleById = themeRepository.getListStyleById(str, this);
                    if (listStyleById == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    listStyleById = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ArrayList arrayList = (ArrayList) listStyleById;
                return arrayList != null ? arrayList : new ArrayList();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(ThemesViewModel themesViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = themesViewModel;
            this.$themeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$themeId, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            ThemesViewModel themesViewModel;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.d = this.$themeId;
                ThemesViewModel themesViewModel2 = this.this$0;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.L$1 = themesViewModel2;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
                themesViewModel = themesViewModel2;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                themesViewModel = (ThemesViewModel) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            themesViewModel.b = (ArrayList) g;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = ThemesViewModel.h;
            local.d(str, "userSelectedId " + this.this$0.d + " currentThemeId " + this.this$0.c);
            this.this$0.f.g(this.this$0.a, this.this$0.c, this.this$0.d, this.this$0.b);
            this.this$0.o();
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$start$1", f = "ThemesViewModel.kt", l = {27}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThemesViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$start$1$1", f = "ThemesViewModel.kt", l = {28, 29, 31}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:16:0x006d  */
            /* JADX WARNING: Removed duplicated region for block: B:19:0x007e  */
            /* JADX WARNING: Removed duplicated region for block: B:25:0x00b6  */
            /* JADX WARNING: Removed duplicated region for block: B:26:0x00b9  */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x00c3  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                // Method dump skipped, instructions count: 208
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel.Ei.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(ThemesViewModel themesViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = themesViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = ThemesViewModel.h;
            local.d(str, "mCurrentThemeId=" + this.this$0.c);
            this.this$0.f.g(this.this$0.a, this.this$0.c, this.this$0.d, this.this$0.b);
            this.this$0.o();
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = ThemesViewModel.class.getSimpleName();
        Wg6.b(simpleName, "ThemesViewModel::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public ThemesViewModel(ThemeRepository themeRepository) {
        Wg6.c(themeRepository, "mThemesRepository");
        this.g = themeRepository;
    }

    @DexIgnore
    public final void m() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Bi(this, null), 3, null);
    }

    @DexIgnore
    public final void n(String str) {
        Wg6.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = h;
        local.d(str2, "deleteTheme id=" + str);
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Ci(this, str, null), 3, null);
    }

    @DexIgnore
    public final void o() {
        this.e.l(this.f);
    }

    @DexIgnore
    public final MutableLiveData<Ai> p() {
        return this.e;
    }

    @DexIgnore
    public final void q(String str) {
        Wg6.c(str, "themeId");
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Di(this, str, null), 3, null);
    }

    @DexIgnore
    public final void r() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ei(this, null), 3, null);
    }
}
