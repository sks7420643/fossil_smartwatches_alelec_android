package com.portfolio.platform.uirenew.signup.verification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import com.fossil.A07;
import com.fossil.B07;
import com.fossil.F07;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EmailOtpVerificationActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public B07 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, SignUpEmailAuth signUpEmailAuth) {
            Wg6.c(context, "context");
            Wg6.c(signUpEmailAuth, "emailAuth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("EmailOtpVerificationActivity", "start verify Otp with Email Auth =" + signUpEmailAuth);
            Intent intent = new Intent(context, EmailOtpVerificationActivity.class);
            intent.putExtra("EMAIL_AUTH", signUpEmailAuth);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        A07 a07 = (A07) getSupportFragmentManager().Y(2131362158);
        if (a07 == null) {
            a07 = A07.w.a();
            k(a07, r(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (a07 != null) {
            iface.H1(new F07(this, a07)).a(this);
            Intent intent = getIntent();
            if (intent != null && intent.hasExtra("EMAIL_AUTH")) {
                FLogger.INSTANCE.getLocal().d(r(), "Retrieve from intent emailAddress");
                B07 b07 = this.A;
                if (b07 != null) {
                    Parcelable parcelableExtra = intent.getParcelableExtra("EMAIL_AUTH");
                    Wg6.b(parcelableExtra, "it.getParcelableExtra(co\u2026ums.Constants.EMAIL_AUTH)");
                    b07.u((SignUpEmailAuth) parcelableExtra);
                    return;
                }
                Wg6.n("mPresenter");
                throw null;
            }
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationContract.View");
    }
}
