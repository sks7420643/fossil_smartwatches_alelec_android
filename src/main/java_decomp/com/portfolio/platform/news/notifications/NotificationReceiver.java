package com.portfolio.platform.news.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Ux7;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationReceiver extends BroadcastReceiver {
    @DexIgnore
    public An4 a;
    @DexIgnore
    public Cj4 b;
    @DexIgnore
    public UserRepository c;
    @DexIgnore
    public /* final */ Il6 d; // = Jv7.a(Ux7.b(null, 1, null).plus(Bw7.b()));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.news.notifications.NotificationReceiver$onReceive$1", f = "NotificationReceiver.kt", l = {52}, m = "invokeSuspend")
    public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Intent $intent;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(NotificationReceiver notificationReceiver, Intent intent, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationReceiver;
            this.$intent = intent;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            a aVar = new a(this.this$0, this.$intent, xe6);
            aVar.p$ = (Il6) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object currentUser;
            Integer e;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                UserRepository b = this.this$0.b();
                this.L$0 = il6;
                this.label = 1;
                currentUser = b.getCurrentUser(this);
                if (currentUser == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                currentUser = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((MFUser) currentUser) == null) {
                FLogger.INSTANCE.getLocal().d("NotificationReceiver", "onReceive - user is NULL!!!");
                return Cd6.a;
            }
            Intent intent = this.$intent;
            int intValue = (intent == null || (e = Ao7.e(intent.getIntExtra("ACTION_EVENT", 0))) == null) ? 0 : e.intValue();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("NotificationReceiver", "onReceive - actionEvent=" + intValue);
            if (intValue == 1) {
                PortfolioApp.get.instance().S1(this.this$0.a(), false, 10);
            }
            return Cd6.a;
        }
    }

    @DexIgnore
    public NotificationReceiver() {
        PortfolioApp.get.instance().getIface().O(this);
    }

    @DexIgnore
    public final Cj4 a() {
        Cj4 cj4 = this.b;
        if (cj4 != null) {
            return cj4;
        }
        Wg6.n("mDeviceSettingFactory");
        throw null;
    }

    @DexIgnore
    public final UserRepository b() {
        UserRepository userRepository = this.c;
        if (userRepository != null) {
            return userRepository;
        }
        Wg6.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        FLogger.INSTANCE.getLocal().d("NotificationReceiver", "onReceive");
        Rm6 unused = Gu7.d(this.d, null, null, new a(this, intent, null), 3, null);
    }
}
