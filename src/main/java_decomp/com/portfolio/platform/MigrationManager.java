package com.portfolio.platform;

import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Jq4;
import com.fossil.Ko7;
import com.fossil.Mn5;
import com.fossil.Ry6;
import com.fossil.Ty6;
import com.fossil.Vt7;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.data.legacy.threedotzero.ActivePreset;
import com.portfolio.platform.data.legacy.threedotzero.DeviceModel;
import com.portfolio.platform.data.legacy.threedotzero.LegacyGoalTrackingSettings;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.model.room.microapp.ButtonMapping;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import com.portfolio.platform.util.DeviceUtils;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MigrationManager {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ Ai v; // = new Ai(null);
    @DexIgnore
    public /* final */ List<DianaAppSetting> a; // = new ArrayList();
    @DexIgnore
    public /* final */ An4 b;
    @DexIgnore
    public /* final */ UserRepository c;
    @DexIgnore
    public /* final */ NotificationsRepository d;
    @DexIgnore
    public /* final */ GetHybridDeviceSettingUseCase e;
    @DexIgnore
    public /* final */ PortfolioApp f;
    @DexIgnore
    public /* final */ GoalTrackingRepository g;
    @DexIgnore
    public /* final */ DeviceDao h;
    @DexIgnore
    public /* final */ HybridCustomizeDatabase i;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository j;
    @DexIgnore
    public /* final */ FlagRepository k;
    @DexIgnore
    public /* final */ DianaPresetRepository l;
    @DexIgnore
    public /* final */ WatchFaceRepository m;
    @DexIgnore
    public /* final */ FileRepository n;
    @DexIgnore
    public /* final */ WFBackgroundPhotoRepository o;
    @DexIgnore
    public /* final */ com.portfolio.platform.preset.data.source.DianaPresetRepository p;
    @DexIgnore
    public /* final */ WFAssetRepository q;
    @DexIgnore
    public /* final */ DeviceRepository r;
    @DexIgnore
    public /* final */ DianaAppSettingRepository s;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return MigrationManager.u;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.MigrationManager", f = "MigrationManager.kt", l = {567, 568, 595}, m = "migrateComplicationWatchAppSetting46")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ MigrationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(MigrationManager migrationManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = migrationManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.MigrationManager", f = "MigrationManager.kt", l = {626, 672, 677, 727}, m = "migrateFor2Dot0")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ MigrationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(MigrationManager migrationManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = migrationManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.p(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.MigrationManager$migrateFor2Dot0$4", f = "MigrationManager.kt", l = {763}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MigrationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(MigrationManager migrationManager, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = migrationManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase = this.this$0.e;
                Ty6 ty6 = new Ty6(this.this$0.f.J(), Ry6.MIGRATION);
                this.L$0 = il6;
                this.label = 1;
                if (Jq4.a(getHybridDeviceSettingUseCase, ty6, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ LegacyGoalTrackingSettings $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MigrationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(LegacyGoalTrackingSettings legacyGoalTrackingSettings, Xe6 xe6, MigrationManager migrationManager) {
            super(2, xe6);
            this.$it = legacyGoalTrackingSettings;
            this.this$0 = migrationManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.$it, xe6, this.this$0);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0050 A[LOOP:0: B:12:0x004e->B:13:0x0050, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x010b  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 273
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.MigrationManager.Ei.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.MigrationManager$migrateFor4Dot0$8", f = "MigrationManager.kt", l = {286}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MigrationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(MigrationManager migrationManager, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = migrationManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase = this.this$0.e;
                Ty6 ty6 = new Ty6(this.this$0.f.J(), Ry6.MIGRATION);
                this.L$0 = il6;
                this.label = 1;
                if (Jq4.a(getHybridDeviceSettingUseCase, ty6, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MigrationManager b;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList c;
        @DexIgnore
        public /* final */ /* synthetic */ List d;

        @DexIgnore
        public Gi(MigrationManager migrationManager, ArrayList arrayList, List list) {
            this.b = migrationManager;
            this.c = arrayList;
            this.d = list;
        }

        @DexIgnore
        public final void run() {
            this.b.i.microAppDao().upsertListMicroApp(this.c);
            this.b.i.presetDao().upsertPresetList(this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.MigrationManager", f = "MigrationManager.kt", l = {606, 621}, m = "migratePhotoBackgrounds46")
    public static final class Hi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ MigrationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(MigrationManager migrationManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = migrationManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.r(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.MigrationManager", f = "MigrationManager.kt", l = {351, 361, 362, 363, 364, 365}, m = "migratePresetToWatchFace")
    public static final class Ii extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ MigrationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(MigrationManager migrationManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = migrationManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.MigrationManager", f = "MigrationManager.kt", l = {384, 401, 534, 557}, m = "migratePresets46")
    public static final class Ji extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$13;
        @DexIgnore
        public Object L$14;
        @DexIgnore
        public Object L$15;
        @DexIgnore
        public Object L$16;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ MigrationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(MigrationManager migrationManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = migrationManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.t(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.MigrationManager", f = "MigrationManager.kt", l = {147, 150, 154}, m = "processMigration")
    public static final class Ki extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ MigrationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(MigrationManager migrationManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = migrationManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.MigrationManager$upgrade$2", f = "MigrationManager.kt", l = {105, 110, 114, 122, 124, 125}, m = "invokeSuspend")
    public static final class Li extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MigrationManager this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.MigrationManager$upgrade$2$1", f = "MigrationManager.kt", l = {136}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    DeviceUtils a2 = DeviceUtils.h.a();
                    this.L$0 = il6;
                    this.label = 1;
                    if (a2.d(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Li(MigrationManager migrationManager, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = migrationManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Li li = new Li(this.this$0, xe6);
            li.p$ = (Il6) obj;
            throw null;
            //return li;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
            throw null;
            //return ((Li) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x00e5  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x00fe  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x0155  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x01f4  */
        /* JADX WARNING: Removed duplicated region for block: B:60:0x027e  */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x02c2  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x02c5  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r16) {
            /*
            // Method dump skipped, instructions count: 736
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.MigrationManager.Li.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = MigrationManager.class.getSimpleName();
        Wg6.b(simpleName, "MigrationManager::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public MigrationManager(An4 an4, UserRepository userRepository, NotificationsRepository notificationsRepository, GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, PortfolioApp portfolioApp, GoalTrackingRepository goalTrackingRepository, DeviceDao deviceDao, HybridCustomizeDatabase hybridCustomizeDatabase, MicroAppLastSettingRepository microAppLastSettingRepository, Cj4 cj4, FlagRepository flagRepository, DianaPresetRepository dianaPresetRepository, WatchFaceRepository watchFaceRepository, FileRepository fileRepository, WFBackgroundPhotoRepository wFBackgroundPhotoRepository, com.portfolio.platform.preset.data.source.DianaPresetRepository dianaPresetRepository2, WFAssetRepository wFAssetRepository, DeviceRepository deviceRepository, DianaAppSettingRepository dianaAppSettingRepository, DianaWatchFaceRepository dianaWatchFaceRepository) {
        Wg6.c(an4, "mSharedPrefs");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(notificationsRepository, "mNotificationRepository");
        Wg6.c(getHybridDeviceSettingUseCase, "mGetHybridDeviceSettingUseCase");
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(goalTrackingRepository, "mGoalTrackingRepository");
        Wg6.c(deviceDao, "mDeviceDao");
        Wg6.c(hybridCustomizeDatabase, "mHybridCustomizeDatabase");
        Wg6.c(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        Wg6.c(cj4, "mDeviceSettingFactory");
        Wg6.c(flagRepository, "flagRepository");
        Wg6.c(dianaPresetRepository, "oldPresetRepository");
        Wg6.c(watchFaceRepository, "oldWatchFaceRepository");
        Wg6.c(fileRepository, "fileRepository");
        Wg6.c(wFBackgroundPhotoRepository, "newPhotoWFBackgroundPhotoRepository");
        Wg6.c(dianaPresetRepository2, "newPresetRepository");
        Wg6.c(wFAssetRepository, "wfAssetRepository");
        Wg6.c(deviceRepository, "deviceRepository");
        Wg6.c(dianaAppSettingRepository, "dianaAppSettingRepository");
        Wg6.c(dianaWatchFaceRepository, "dianaWatchFaceRepository");
        this.b = an4;
        this.c = userRepository;
        this.d = notificationsRepository;
        this.e = getHybridDeviceSettingUseCase;
        this.f = portfolioApp;
        this.g = goalTrackingRepository;
        this.h = deviceDao;
        this.i = hybridCustomizeDatabase;
        this.j = microAppLastSettingRepository;
        this.k = flagRepository;
        this.l = dianaPresetRepository;
        this.m = watchFaceRepository;
        this.n = fileRepository;
        this.o = wFBackgroundPhotoRepository;
        this.p = dianaPresetRepository2;
        this.q = wFAssetRepository;
        this.r = deviceRepository;
        this.s = dianaAppSettingRepository;
        this.t = dianaWatchFaceRepository;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0135, code lost:
        if (r0 == null) goto L_0x0137;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x014a, code lost:
        if (r0 != null) goto L_0x00ce;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.portfolio.platform.data.model.room.microapp.HybridPreset j(java.lang.String r13, java.lang.String r14, com.portfolio.platform.data.legacy.threedotzero.MappingSet r15, com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting r16) {
        /*
        // Method dump skipped, instructions count: 408
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.MigrationManager.j(java.lang.String, java.lang.String, com.portfolio.platform.data.legacy.threedotzero.MappingSet, com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting):com.portfolio.platform.data.model.room.microapp.HybridPreset");
    }

    @DexIgnore
    public final HybridPreset k(List<? extends MicroApp> list, String str, String str2, ActivePreset activePreset, HashMap<String, String> hashMap) {
        T t2;
        FLogger.INSTANCE.getLocal().d(u, "convert " + activePreset);
        ArrayList arrayList = new ArrayList();
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "Calendar.getInstance()");
        String w0 = TimeUtils.w0(instance.getTime());
        for (ButtonMapping buttonMapping : activePreset.getButtonMappingList()) {
            String component1 = buttonMapping.component1();
            String component2 = buttonMapping.component2();
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                T next = it.next();
                if (Wg6.a(next.getAppId(), component2)) {
                    t2 = next;
                    break;
                }
            }
            if (t2 == null) {
                FLogger.INSTANCE.getLocal().d(u, "this preset is not support for " + str2 + " due to " + component2);
                return null;
            }
            String str3 = hashMap.get(component2);
            Wg6.b(w0, "localUpdatedAt");
            HybridPresetAppSetting hybridPresetAppSetting = new HybridPresetAppSetting(component1, component2, w0);
            if (str3 != null) {
                hybridPresetAppSetting.setSettings(str3);
            }
            arrayList.add(hybridPresetAppSetting);
        }
        HybridPreset hybridPreset = new HybridPreset(str + ':' + str2, "Active Preset", str2, arrayList, true);
        FLogger.INSTANCE.getLocal().d(u, "legacy active createdAt " + activePreset.getCreateAt() + " updatedAt " + activePreset.getUpdateAt());
        hybridPreset.setPinType(activePreset.getPinType());
        long j2 = (long) 1000;
        hybridPreset.setCreatedAt(TimeUtils.w0(new Date(activePreset.getCreateAt() * j2)));
        hybridPreset.setUpdatedAt(TimeUtils.w0(new Date(activePreset.getUpdateAt() * j2)));
        FLogger.INSTANCE.getLocal().d(u, "active preset createdAt " + hybridPreset.getCreatedAt() + " updatedAt " + hybridPreset.getUpdatedAt());
        return hybridPreset;
    }

    @DexIgnore
    public final HybridPreset l(String str, RecommendedPreset recommendedPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = u;
        StringBuilder sb = new StringBuilder();
        sb.append("convertRecommendPresetToHybridPreset ");
        sb.append(recommendedPreset != null ? recommendedPreset.getName() : null);
        local.d(str2, sb.toString());
        if (recommendedPreset == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "Calendar.getInstance()");
        String w0 = TimeUtils.w0(instance.getTime());
        List<ButtonMapping> buttonMappingList = recommendedPreset.getButtonMappingList();
        Wg6.b(buttonMappingList, "recommendedPreset.buttonMappingList");
        for (T t2 : buttonMappingList) {
            String component1 = t2.component1();
            String component2 = t2.component2();
            Wg6.b(w0, "localUpdatedAt");
            arrayList.add(new HybridPresetAppSetting(component1, component2, w0));
        }
        HybridPreset hybridPreset = new HybridPreset(str + ':' + recommendedPreset.getId(), recommendedPreset.getName(), str, new ArrayList(), false);
        hybridPreset.setPinType(0);
        long j2 = (long) 1000;
        hybridPreset.setCreatedAt(TimeUtils.w0(new Date(recommendedPreset.getCreateAt() * j2)));
        hybridPreset.setUpdatedAt(TimeUtils.w0(new Date(recommendedPreset.getUpdateAt() * j2)));
        return hybridPreset;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x015c, code lost:
        if (r0 == null) goto L_0x015e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0192, code lost:
        if (r0 != null) goto L_0x00d5;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.portfolio.platform.data.model.room.microapp.HybridPreset m(java.lang.String r13, com.portfolio.platform.data.legacy.threedotzero.MappingSet r14, com.portfolio.platform.data.legacy.threedotzero.DeviceProvider r15, com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting r16) {
        /*
        // Method dump skipped, instructions count: 487
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.MigrationManager.m(java.lang.String, com.portfolio.platform.data.legacy.threedotzero.MappingSet, com.portfolio.platform.data.legacy.threedotzero.DeviceProvider, com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting):com.portfolio.platform.data.model.room.microapp.HybridPreset");
    }

    @DexIgnore
    public final HybridPreset n(List<? extends MicroApp> list, String str, SavedPreset savedPreset, HashMap<String, String> hashMap) {
        T t2;
        FLogger.INSTANCE.getLocal().d(u, "convert " + savedPreset + " microAppList " + list.size());
        ArrayList arrayList = new ArrayList();
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "Calendar.getInstance()");
        String w0 = TimeUtils.w0(instance.getTime());
        for (ButtonMapping buttonMapping : savedPreset.getButtonMappingList()) {
            String component1 = buttonMapping.component1();
            String component2 = buttonMapping.component2();
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                T next = it.next();
                if (Wg6.a(next.getAppId(), component2)) {
                    t2 = next;
                    break;
                }
            }
            if (t2 == null) {
                FLogger.INSTANCE.getLocal().d(u, "this preset is not support for " + str + " due to " + component2);
                return null;
            }
            String str2 = hashMap.get(component2);
            Wg6.b(w0, "localUpdatedAt");
            HybridPresetAppSetting hybridPresetAppSetting = new HybridPresetAppSetting(component1, component2, w0);
            if (str2 != null) {
                hybridPresetAppSetting.setSettings(str2);
            }
            arrayList.add(hybridPresetAppSetting);
        }
        HybridPreset hybridPreset = new HybridPreset(str + ':' + savedPreset.getId(), savedPreset.getName(), str, arrayList, false);
        FLogger.INSTANCE.getLocal().d(u, "legacy createdAt " + savedPreset.getCreateAt() + " updatedAt " + savedPreset.getUpdateAt());
        long j2 = (long) 1000;
        hybridPreset.setCreatedAt(TimeUtils.w0(new Date(savedPreset.getCreateAt() * j2)));
        hybridPreset.setUpdatedAt(TimeUtils.w0(new Date(savedPreset.getUpdateAt() * j2)));
        FLogger.INSTANCE.getLocal().d(u, "preset createdAt " + hybridPreset.getCreatedAt() + " updatedAt " + hybridPreset.getUpdatedAt());
        hybridPreset.setPinType(savedPreset.getPinType());
        return hybridPreset;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x016a  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01c6  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01d7  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01f0  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x01fe  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object o(com.mapped.Xe6<? super com.mapped.Cd6> r14) {
        /*
        // Method dump skipped, instructions count: 513
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.MigrationManager.o(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:128:0x05d4  */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x072b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x07be  */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x081e  */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x0928  */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x092d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01d2  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0219  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x038b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object p(com.mapped.Xe6<? super com.mapped.Cd6> r23) {
        /*
        // Method dump skipped, instructions count: 2360
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.MigrationManager.p(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x022c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object q(java.lang.String r23, com.mapped.Xe6<? super com.mapped.Cd6> r24) {
        /*
        // Method dump skipped, instructions count: 1422
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.MigrationManager.q(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object r(java.lang.String r19, com.mapped.Xe6<? super com.mapped.Cd6> r20) {
        /*
        // Method dump skipped, instructions count: 357
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.MigrationManager.r(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0110  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0131  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0141  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0148  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01fa  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object s(com.mapped.Xe6<? super java.lang.Integer> r16) {
        /*
        // Method dump skipped, instructions count: 528
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.MigrationManager.s(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v161, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r6v19, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r7v14, types: [java.util.List] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x037e  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x043f  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x044a  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x0461  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x047e  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x04a5  */
    /* JADX WARNING: Removed duplicated region for block: B:145:0x04b0  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0510  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x051e  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x0528  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x0534  */
    /* JADX WARNING: Removed duplicated region for block: B:170:0x0540  */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x054b  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x0556  */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x0561  */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x057c  */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x0597  */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x05b2  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x05cd  */
    /* JADX WARNING: Removed duplicated region for block: B:228:0x0707  */
    /* JADX WARNING: Removed duplicated region for block: B:236:0x05d1 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x033b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:259:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0250  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x02bb  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0302  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x030f  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0318  */
    /* JADX WARNING: Unknown variable types count: 3 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object t(java.lang.String r34, com.mapped.Xe6<? super com.mapped.Cd6> r35) {
        /*
        // Method dump skipped, instructions count: 1868
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.MigrationManager.t(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object u(java.lang.String r11, com.mapped.Xe6<? super com.mapped.Cd6> r12) {
        /*
        // Method dump skipped, instructions count: 312
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.MigrationManager.u(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void v() {
        this.b.w1(System.currentTimeMillis());
        this.b.U0(0);
        this.b.H1(true);
    }

    @DexIgnore
    public void w() {
        List<DeviceModel> allDevice = Mn5.p.a().e().getAllDevice();
        String J = this.f.J();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "migrationDeviceData - allDevices=" + allDevice + ", activeSerial=" + J);
        try {
            boolean z = !TextUtils.isEmpty(J) && !DeviceHelper.o.v(J);
            if (z) {
                this.b.E0("");
                this.b.p1(true);
            }
            if (!(allDevice == null || allDevice.isEmpty())) {
                for (DeviceModel deviceModel : allDevice) {
                    if (deviceModel != null && !TextUtils.isEmpty(deviceModel.getDeviceId())) {
                        String deviceId = deviceModel.getDeviceId();
                        String macAddress = deviceModel.getMacAddress();
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = u;
                        local2.d(str2, "migrationDeviceData - step1: deviceId=" + deviceId + ", deviceAddress=" + macAddress);
                        DeviceHelper.Ai ai = DeviceHelper.o;
                        String deviceId2 = deviceModel.getDeviceId();
                        if (deviceId2 == null) {
                            Wg6.i();
                            throw null;
                        } else if (!ai.v(deviceId2)) {
                            Mn5.p.a().e().removeDevice(deviceId);
                        }
                    }
                }
                if (z) {
                    try {
                        IButtonConnectivity b2 = PortfolioApp.get.b();
                        if (b2 != null) {
                            b2.removeActiveSerial(J);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } catch (Exception e2) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str3 = u;
                        local3.d(str3, "Exception when remove legacy device in button service ,keep going e=" + e2);
                    }
                }
                for (DeviceModel deviceModel2 : allDevice) {
                    if (deviceModel2 != null && !TextUtils.isEmpty(deviceModel2.getDeviceId())) {
                        String deviceId3 = deviceModel2.getDeviceId();
                        String macAddress2 = deviceModel2.getMacAddress();
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String str4 = u;
                        local4.d(str4, "migrationDeviceData - step2: - deviceId=" + deviceId3 + ", deviceAddress=" + macAddress2);
                        DeviceHelper.Ai ai2 = DeviceHelper.o;
                        String deviceId4 = deviceModel2.getDeviceId();
                        if (deviceId4 == null) {
                            Wg6.i();
                            throw null;
                        } else if (ai2.v(deviceId4)) {
                            if (Vt7.j(deviceModel2.getDeviceId(), J, true)) {
                                PortfolioApp portfolioApp = this.f;
                                String deviceId5 = deviceModel2.getDeviceId();
                                if (deviceId5 != null) {
                                    portfolioApp.n1(deviceId5, deviceModel2.getMacAddress());
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            }
                            IButtonConnectivity b3 = PortfolioApp.get.b();
                            if (b3 != null) {
                                b3.setPairedSerial(deviceModel2.getDeviceId(), deviceModel2.getMacAddress());
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            IButtonConnectivity b4 = PortfolioApp.get.b();
                            if (b4 != null) {
                                b4.removePairedSerial(deviceModel2.getDeviceId());
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                    }
                }
                this.b.q1(false);
            }
        } catch (Exception e3) {
            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            String str5 = u;
            local5.e(str5, "migrationDeviceData - Exception when upgrade to 1.14.0 version e=" + e3);
            e3.printStackTrace();
            this.b.q1(true);
        }
    }

    @DexIgnore
    public Object x(Xe6<? super Boolean> xe6) {
        return Eu7.g(Bw7.b(), new Li(this, null), xe6);
    }
}
