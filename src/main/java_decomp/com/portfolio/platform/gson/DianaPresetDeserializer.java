package com.portfolio.platform.gson;

import com.fossil.Jj5;
import com.google.gson.JsonElement;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.Ku3;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetDeserializer implements Hu3<DianaPreset> {
    @DexIgnore
    public DianaPreset a(JsonElement jsonElement, Type type, Gu3 gu3) {
        boolean z;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String w0;
        String str6;
        String str7;
        String str8;
        String str9;
        if (jsonElement != null) {
            Ku3 d = jsonElement.d();
            String str10 = "id";
            JsonElement p = d.p("id");
            Wg6.b(p, "jsonObject.get(Constants.JSON_KEY_ID)");
            String f = p.f();
            JsonElement p2 = d.p("name");
            Wg6.b(p2, "jsonObject.get(Constants.JSON_KEY_NAME)");
            String f2 = p2.f();
            JsonElement p3 = d.p("updatedAt");
            Wg6.b(p3, "jsonObject.get(Constants.JSON_KEY_UPDATED_AT)");
            String f3 = p3.f();
            JsonElement p4 = d.p("createdAt");
            Wg6.b(p4, "jsonObject.get(Constants.JSON_KEY_CREATED_AT)");
            String f4 = p4.f();
            JsonElement p5 = d.p("isActive");
            Wg6.b(p5, "jsonObject.get(Constants\u2026SON_KEY_IS_PRESET_ACTIVE)");
            boolean a2 = p5.a();
            String str11 = "serialNumber";
            JsonElement p6 = d.p("serialNumber");
            Wg6.b(p6, "jsonObject.get(Constants.JSON_KEY_SERIAL_NUMBER)");
            String f5 = p6.f();
            String str12 = "watchFaceId";
            JsonElement p7 = d.p("watchFaceId");
            Wg6.b(p7, "jsonObject.get(Constants.JSON_KEY_WATCH_FACE_ID)");
            String f6 = p7.f();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (gu3 != null) {
                Iterator<JsonElement> it = d.q("buttons").iterator();
                while (true) {
                    String str13 = "";
                    if (!it.hasNext()) {
                        break;
                    }
                    JsonElement next = it.next();
                    Wg6.b(next, "item");
                    Ku3 d2 = next.d();
                    if (d2.s("buttonPosition")) {
                        JsonElement p8 = d2.p("buttonPosition");
                        Wg6.b(p8, "itemJsonObject.get(Constants.JSON_KEY_BUTTON_POS)");
                        str7 = p8.f();
                    } else {
                        str7 = "";
                    }
                    if (d2.s("appId")) {
                        JsonElement p9 = d2.p("appId");
                        Wg6.b(p9, "itemJsonObject.get(Constants.JSON_KEY_APP_ID)");
                        str8 = p9.f();
                    } else {
                        str8 = "";
                    }
                    if (d2.s("localUpdatedAt")) {
                        JsonElement p10 = d2.p("localUpdatedAt");
                        Wg6.b(p10, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        str9 = p10.f();
                    } else {
                        str9 = "";
                    }
                    if (d2.s(Constants.USER_SETTING)) {
                        try {
                            JsonElement p11 = d2.p(Constants.USER_SETTING);
                            Wg6.b(p11, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str13 = Jj5.a(p11.d());
                        } catch (Exception e) {
                            FLogger.INSTANCE.getLocal().d("DianaPresetDeserializer", "Exception when parse json string");
                        }
                    }
                    Wg6.b(str7, "position");
                    Wg6.b(str8, "appId");
                    Wg6.b(str9, "localUpdatedAt");
                    arrayList.add(new DianaPresetWatchAppSetting(str7, str8, str9, str13));
                }
                Iterator<JsonElement> it2 = d.q("complications").iterator();
                while (it2.hasNext()) {
                    JsonElement next2 = it2.next();
                    Wg6.b(next2, "item");
                    Ku3 d3 = next2.d();
                    if (d3.s("complicationPosition")) {
                        JsonElement p12 = d3.p("complicationPosition");
                        Wg6.b(p12, "itemJsonObject.get(Const\u2026SON_KEY_COMPLICATION_POS)");
                        str4 = p12.f();
                    } else {
                        str4 = "";
                    }
                    if (d3.s("appId")) {
                        JsonElement p13 = d3.p("appId");
                        Wg6.b(p13, "itemJsonObject.get(Constants.JSON_KEY_APP_ID)");
                        str5 = p13.f();
                    } else {
                        str5 = "";
                    }
                    if (d3.s("localUpdatedAt")) {
                        JsonElement p14 = d3.p("localUpdatedAt");
                        Wg6.b(p14, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        w0 = p14.f();
                    } else {
                        Calendar instance = Calendar.getInstance();
                        Wg6.b(instance, "Calendar.getInstance()");
                        w0 = TimeUtils.w0(instance.getTime());
                    }
                    if (d3.s(Constants.USER_SETTING)) {
                        try {
                            JsonElement p15 = d3.p(Constants.USER_SETTING);
                            Wg6.b(p15, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str6 = Jj5.a(p15.d());
                        } catch (Exception e2) {
                            FLogger.INSTANCE.getLocal().d("DianaPresetDeserializer", "Exception when parse json string");
                        }
                        Wg6.b(str4, "position");
                        Wg6.b(str5, "appId");
                        Wg6.b(w0, "localUpdatedAt");
                        arrayList2.add(new DianaPresetComplicationSetting(str4, str5, w0, str6));
                    }
                    str6 = "";
                    Wg6.b(str4, "position");
                    Wg6.b(str5, "appId");
                    Wg6.b(w0, "localUpdatedAt");
                    arrayList2.add(new DianaPresetComplicationSetting(str4, str5, w0, str6));
                }
                str = f2;
                str3 = "name";
                z = a2;
                str2 = f5;
            } else {
                str10 = "id";
                str3 = "name";
                str11 = "serialNumber";
                str = f2;
                str12 = "watchFaceId";
                z = a2;
                str2 = f5;
            }
            Wg6.b(f, str10);
            Wg6.b(str2, str11);
            Wg6.b(str, str3);
            Wg6.b(f6, str12);
            DianaPreset dianaPreset = new DianaPreset(f, str2, str, z, arrayList2, arrayList, f6);
            dianaPreset.setCreatedAt(f4);
            dianaPreset.setUpdatedAt(f3);
            return dianaPreset;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hu3
    public /* bridge */ /* synthetic */ DianaPreset deserialize(JsonElement jsonElement, Type type, Gu3 gu3) {
        return a(jsonElement, type, gu3);
    }
}
