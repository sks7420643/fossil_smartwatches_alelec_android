package com.portfolio.platform.gson;

import android.text.TextUtils;
import com.fossil.Fj4;
import com.fossil.Ij4;
import com.fossil.Kj4;
import com.google.gson.JsonElement;
import com.mapped.Fu3;
import com.mapped.Ku3;
import com.mapped.Pu3;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import java.lang.reflect.Type;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetWatchAppSettingSerializer implements Pu3<List<? extends DianaPresetWatchAppSetting>> {
    @DexIgnore
    public JsonElement a(List<DianaPresetWatchAppSetting> list, Type type, Kj4 kj4) {
        Wg6.c(list, "src");
        Fu3 fu3 = new Fu3();
        Ij4 ij4 = new Ij4();
        for (DianaPresetWatchAppSetting dianaPresetWatchAppSetting : list) {
            String component1 = dianaPresetWatchAppSetting.component1();
            String component2 = dianaPresetWatchAppSetting.component2();
            String component3 = dianaPresetWatchAppSetting.component3();
            String component4 = dianaPresetWatchAppSetting.component4();
            Ku3 ku3 = new Ku3();
            ku3.n("buttonPosition", component1);
            ku3.n("appId", component2);
            ku3.n("localUpdatedAt", component3);
            if (!TextUtils.isEmpty(component4)) {
                try {
                    JsonElement c = ij4.c(component4);
                    if (c != null) {
                        ku3.k(Constants.USER_SETTING, (Ku3) c);
                    } else {
                        throw new Rc6("null cannot be cast to non-null type com.google.gson.JsonObject");
                    }
                } catch (Exception e) {
                    ku3.k(Constants.USER_SETTING, Fj4.a);
                }
            } else {
                ku3.k(Constants.USER_SETTING, Fj4.a);
            }
            fu3.k(ku3);
        }
        return fu3;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.reflect.Type, com.fossil.Kj4] */
    @Override // com.mapped.Pu3
    public /* bridge */ /* synthetic */ JsonElement serialize(List<? extends DianaPresetWatchAppSetting> list, Type type, Kj4 kj4) {
        return a(list, type, kj4);
    }
}
