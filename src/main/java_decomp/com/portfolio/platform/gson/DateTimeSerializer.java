package com.portfolio.platform.gson;

import com.fossil.Hj4;
import com.fossil.Jj4;
import com.fossil.Kj4;
import com.google.gson.JsonElement;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.Pu3;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import java.lang.reflect.Type;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DateTimeSerializer implements Hu3<DateTime>, Pu3<DateTime> {
    @DexIgnore
    public DateTime a(JsonElement jsonElement, Type type, Gu3 gu3) throws Hj4 {
        Wg6.c(jsonElement, "je");
        String f = jsonElement.f();
        Wg6.b(f, "je.asString");
        if (f.length() == 0) {
            return null;
        }
        return TimeUtils.S(jsonElement.f());
    }

    @DexIgnore
    public JsonElement b(DateTime dateTime, Type type, Kj4 kj4) {
        String str;
        if (dateTime == null || (str = TimeUtils.x0(dateTime)) == null) {
            str = "";
        }
        return new Jj4(str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hu3
    public /* bridge */ /* synthetic */ DateTime deserialize(JsonElement jsonElement, Type type, Gu3 gu3) {
        return a(jsonElement, type, gu3);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.reflect.Type, com.fossil.Kj4] */
    @Override // com.mapped.Pu3
    public /* bridge */ /* synthetic */ JsonElement serialize(DateTime dateTime, Type type, Kj4 kj4) {
        return b(dateTime, type, kj4);
    }
}
