package com.portfolio.platform.gson;

import com.google.gson.JsonElement;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.Ku3;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeDeserializer implements Hu3<Theme> {
    @DexIgnore
    public Theme a(JsonElement jsonElement, Type type, Gu3 gu3) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        Ku3 d = jsonElement != null ? jsonElement.d() : null;
        if (d == null) {
            return null;
        }
        if (d.s("id")) {
            JsonElement p = d.p("id");
            Wg6.b(p, "jsonObject.get(Constants.JSON_KEY_ID)");
            str = p.f();
        } else {
            str = "";
        }
        if (d.s("name")) {
            JsonElement p2 = d.p("name");
            Wg6.b(p2, "jsonObject.get(Constants.JSON_KEY_NAME)");
            str2 = p2.f();
        } else {
            str2 = "";
        }
        ArrayList arrayList = new ArrayList();
        if (gu3 != null && d.s("styles")) {
            Iterator<JsonElement> it = d.q("styles").iterator();
            while (it.hasNext()) {
                JsonElement next = it.next();
                Wg6.b(next, "item");
                Ku3 d2 = next.d();
                if (d2.s("key")) {
                    JsonElement p3 = d2.p("key");
                    Wg6.b(p3, "itemJsonObject.get(Constants.JSON_KEY_KEY)");
                    str3 = p3.f();
                } else {
                    str3 = "";
                }
                if (d2.s("type")) {
                    JsonElement p4 = d2.p("type");
                    Wg6.b(p4, "itemJsonObject.get(Constants.JSON_KEY_TYPE)");
                    str4 = p4.f();
                } else {
                    str4 = "";
                }
                if (d2.s("value")) {
                    JsonElement p5 = d2.p("value");
                    if (p5 instanceof Ku3) {
                        Ku3 ku3 = (Ku3) p5;
                        if (ku3.s("fileName")) {
                            JsonElement p6 = ku3.p("fileName");
                            Wg6.b(p6, "valueJsonElement.get(Constants.JSON_KEY_FILE_NAME)");
                            str5 = p6.f();
                            Wg6.b(str5, "valueJsonElement.get(Con\u2026N_KEY_FILE_NAME).asString");
                        }
                    } else {
                        Wg6.b(p5, "valueJsonElement");
                        str5 = p5.f();
                        Wg6.b(str5, "valueJsonElement.asString");
                    }
                    Wg6.b(str3, "key");
                    Wg6.b(str4, "type");
                    arrayList.add(new Style(str3, str4, str5));
                }
                str5 = "";
                Wg6.b(str3, "key");
                Wg6.b(str4, "type");
                arrayList.add(new Style(str3, str4, str5));
            }
        }
        Wg6.b(str, "id");
        Wg6.b(str2, "name");
        return new Theme(str, str2, arrayList);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hu3
    public /* bridge */ /* synthetic */ Theme deserialize(JsonElement jsonElement, Type type, Gu3 gu3) {
        return a(jsonElement, type, gu3);
    }
}
