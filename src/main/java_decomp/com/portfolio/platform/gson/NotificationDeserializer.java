package com.portfolio.platform.gson;

import com.fossil.Dt4;
import com.fossil.Lt4;
import com.fossil.Ss4;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.Ku3;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.lang.reflect.Type;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationDeserializer implements Hu3<Dt4> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<Ss4> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<Lt4> {
    }

    @DexIgnore
    public Dt4 a(JsonElement jsonElement, Type type, Gu3 gu3) {
        Ss4 ss4;
        Lt4 lt4;
        boolean z;
        int i;
        int i2;
        boolean z2;
        if (jsonElement == null) {
            return null;
        }
        Ku3 d = jsonElement.d();
        JsonElement p = d.p("id");
        Wg6.b(p, "jsonObject.get(Constants.JSON_KEY_ID)");
        String f = p.f();
        JsonElement p2 = d.p("createdAt");
        String f2 = p2 != null ? p2.f() : null;
        Date date = new Date();
        if (f2 != null) {
            date = TimeUtils.q0(f2);
            Wg6.b(date, "DateHelper.parseJodaTime(dateStr)");
        }
        JsonElement p3 = d.p("message");
        Wg6.b(p3, "jsonObject.get(BCConst.NOTIFICATION_MESSAGE_KEY)");
        Ku3 d2 = p3.d();
        JsonElement p4 = d2.p("titleLocKey");
        Wg6.b(p4, "message.get(BCConst.NOTI\u2026ON_TITLE_LOC_MESSAGE_KEY)");
        String f3 = p4.f();
        Wg6.b(f3, "message.get(BCConst.NOTI\u2026LOC_MESSAGE_KEY).asString");
        JsonElement p5 = d2.p("bodyLocKey");
        Wg6.b(p5, "message.get(BCConst.NOTI\u2026ION_BODY_LOC_MESSAGE_KEY)");
        String f4 = p5.f();
        Wg6.b(f4, "message.get(BCConst.NOTI\u2026LOC_MESSAGE_KEY).asString");
        JsonElement p6 = d2.p("payload");
        Wg6.b(p6, "message.get(BCConst.NOTIFICATION_PAYLOAD_KEY)");
        Ku3 d3 = p6.d();
        JsonElement p7 = d3.p("challenge");
        String f5 = p7 != null ? p7.f() : null;
        try {
            ss4 = (Ss4) new Gson().l(f5, new a().getType());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String simpleName = NotificationDeserializer.class.getSimpleName();
            Wg6.b(simpleName, "this::class.java.simpleName");
            local.e(simpleName, "exception when parse challengeData: " + f5);
            e.printStackTrace();
            ss4 = null;
        }
        JsonElement p8 = d3.p("profile");
        String f6 = p8 != null ? p8.f() : null;
        try {
            lt4 = (Lt4) new Gson().l(f6, new b().getType());
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String simpleName2 = NotificationDeserializer.class.getSimpleName();
            Wg6.b(simpleName2, "this::class.java.simpleName");
            local2.e(simpleName2, "exception when parse profileData: " + f6);
            e2.printStackTrace();
            lt4 = null;
        }
        JsonElement p9 = d3.p("confirm");
        String f7 = p9 != null ? p9.f() : null;
        if (f7 != null) {
            try {
                z2 = Boolean.parseBoolean(f7);
            } catch (Exception e3) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String simpleName3 = NotificationDeserializer.class.getSimpleName();
                Wg6.b(simpleName3, "this::class.java.simpleName");
                local3.e(simpleName3, "exception when parse confirmChallenge: " + f7);
                z2 = false;
            }
            z = z2;
        } else {
            z = false;
        }
        JsonElement p10 = d3.p("rank");
        String f8 = p10 != null ? p10.f() : null;
        if (f8 != null) {
            try {
                i2 = Integer.parseInt(f8);
            } catch (Exception e4) {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String simpleName4 = NotificationDeserializer.class.getSimpleName();
                Wg6.b(simpleName4, "this::class.java.simpleName");
                local4.e(simpleName4, "exception when parse rank: " + f8);
                i2 = 0;
            }
            i = i2;
        } else {
            i = 0;
        }
        JsonElement p11 = d2.p("reachGoalAt");
        String f9 = p11 != null ? p11.f() : null;
        Date q0 = f9 != null ? TimeUtils.q0(f9) : null;
        Wg6.b(f, "id");
        return new Dt4(f, f3, f4, date, ss4, lt4, z, i, q0);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hu3
    public /* bridge */ /* synthetic */ Dt4 deserialize(JsonElement jsonElement, Type type, Gu3 gu3) {
        return a(jsonElement, type, gu3);
    }
}
