package com.portfolio.platform.gson;

import com.fossil.Jj5;
import com.google.gson.JsonElement;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.Ku3;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaRecommendPresetDeserializer implements Hu3<DianaRecommendPreset> {
    @DexIgnore
    public DianaRecommendPreset a(JsonElement jsonElement, Type type, Gu3 gu3) {
        boolean z;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String w0;
        String str6;
        String str7;
        String str8;
        String str9;
        if (jsonElement != null) {
            Ku3 d = jsonElement.d();
            String str10 = "name";
            JsonElement p = d.p("name");
            Wg6.b(p, "jsonObject.get(Constants.JSON_KEY_NAME)");
            String f = p.f();
            String str11 = "id";
            JsonElement p2 = d.p("id");
            Wg6.b(p2, "jsonObject.get(\"id\")");
            String f2 = p2.f();
            JsonElement p3 = d.p("isDefault");
            Wg6.b(p3, "jsonObject.get(Constants.JSON_KEY_IS_DEFAULT)");
            boolean a2 = p3.a();
            String str12 = "updatedAt";
            JsonElement p4 = d.p("updatedAt");
            Wg6.b(p4, "jsonObject.get(Constants.JSON_KEY_UPDATED_AT)");
            String f3 = p4.f();
            JsonElement p5 = d.p("createdAt");
            Wg6.b(p5, "jsonObject.get(Constants.JSON_KEY_CREATED_AT)");
            String f4 = p5.f();
            JsonElement p6 = d.p("watchFaceId");
            Wg6.b(p6, "jsonObject.get(Constants.JSON_KEY_WATCH_FACE_ID)");
            String f5 = p6.f();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (gu3 != null) {
                Iterator<JsonElement> it = d.q("buttons").iterator();
                while (true) {
                    String str13 = "";
                    if (!it.hasNext()) {
                        break;
                    }
                    JsonElement next = it.next();
                    Wg6.b(next, "item");
                    Ku3 d2 = next.d();
                    if (d2.s("buttonPosition")) {
                        JsonElement p7 = d2.p("buttonPosition");
                        Wg6.b(p7, "itemJsonObject.get(Constants.JSON_KEY_BUTTON_POS)");
                        str7 = p7.f();
                    } else {
                        str7 = "";
                    }
                    if (d2.s("appId")) {
                        JsonElement p8 = d2.p("appId");
                        Wg6.b(p8, "itemJsonObject.get(Constants.JSON_KEY_APP_ID)");
                        str8 = p8.f();
                    } else {
                        str8 = "";
                    }
                    if (d2.s("localUpdatedAt")) {
                        JsonElement p9 = d2.p("localUpdatedAt");
                        Wg6.b(p9, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        str9 = p9.f();
                    } else {
                        str9 = "";
                    }
                    if (d2.s(Constants.USER_SETTING)) {
                        try {
                            JsonElement p10 = d2.p(Constants.USER_SETTING);
                            Wg6.b(p10, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str13 = Jj5.a(p10.d());
                        } catch (Exception e) {
                            FLogger.INSTANCE.getLocal().d("DianaPresetDeserializer", "Exception when parse json string");
                        }
                    }
                    Wg6.b(str7, "position");
                    Wg6.b(str8, "appId");
                    Wg6.b(str9, "localUpdatedAt");
                    arrayList.add(new DianaPresetWatchAppSetting(str7, str8, str9, str13));
                }
                Iterator<JsonElement> it2 = d.q("complications").iterator();
                while (it2.hasNext()) {
                    JsonElement next2 = it2.next();
                    Wg6.b(next2, "item");
                    Ku3 d3 = next2.d();
                    if (d3.s("complicationPosition")) {
                        JsonElement p11 = d3.p("complicationPosition");
                        Wg6.b(p11, "itemJsonObject.get(Const\u2026SON_KEY_COMPLICATION_POS)");
                        str4 = p11.f();
                    } else {
                        str4 = "";
                    }
                    if (d3.s("appId")) {
                        JsonElement p12 = d3.p("appId");
                        Wg6.b(p12, "itemJsonObject.get(Constants.JSON_KEY_APP_ID)");
                        str5 = p12.f();
                    } else {
                        str5 = "";
                    }
                    if (d3.s("localUpdatedAt")) {
                        JsonElement p13 = d3.p("localUpdatedAt");
                        Wg6.b(p13, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        w0 = p13.f();
                    } else {
                        Calendar instance = Calendar.getInstance();
                        Wg6.b(instance, "Calendar.getInstance()");
                        w0 = TimeUtils.w0(instance.getTime());
                    }
                    if (d3.s(Constants.USER_SETTING)) {
                        try {
                            JsonElement p14 = d3.p(Constants.USER_SETTING);
                            Wg6.b(p14, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str6 = Jj5.a(p14.d());
                        } catch (Exception e2) {
                            FLogger.INSTANCE.getLocal().d("DianaPresetDeserializer", "Exception when parse json string");
                        }
                        Wg6.b(str4, "position");
                        Wg6.b(str5, "appId");
                        Wg6.b(w0, "localUpdatedAt");
                        arrayList2.add(new DianaPresetComplicationSetting(str4, str5, w0, str6));
                    }
                    str6 = "";
                    Wg6.b(str4, "position");
                    Wg6.b(str5, "appId");
                    Wg6.b(w0, "localUpdatedAt");
                    arrayList2.add(new DianaPresetComplicationSetting(str4, str5, w0, str6));
                }
                str3 = "watchFaceId";
                str2 = "createdAt";
                str = f;
                z = a2;
            } else {
                str10 = "name";
                str2 = "createdAt";
                str3 = "watchFaceId";
                str11 = "id";
                str12 = "updatedAt";
                str = f;
                z = a2;
            }
            Wg6.b(f2, str11);
            Wg6.b(str, str10);
            Wg6.b(f5, str3);
            Wg6.b(f4, str2);
            Wg6.b(f3, str12);
            return new DianaRecommendPreset("", f2, str, z, arrayList2, arrayList, f5, f4, f3);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hu3
    public /* bridge */ /* synthetic */ DianaRecommendPreset deserialize(JsonElement jsonElement, Type type, Gu3 gu3) {
        return a(jsonElement, type, gu3);
    }
}
