package com.portfolio.platform.gson;

import com.fossil.Jj5;
import com.google.gson.JsonElement;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.Ku3;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaAppSettingDeserializer implements Hu3<DianaAppSetting> {
    @DexIgnore
    public DianaAppSetting a(JsonElement jsonElement, Type type, Gu3 gu3) {
        String str;
        if (jsonElement != null) {
            Ku3 d = jsonElement.d();
            JsonElement p = d.p("id");
            Wg6.b(p, "jsonObject.get(\"id\")");
            String f = p.f();
            JsonElement p2 = d.p("category");
            Wg6.b(p2, "jsonObject.get(\"category\")");
            String f2 = p2.f();
            JsonElement p3 = d.p("appId");
            Wg6.b(p3, "jsonObject.get(\"appId\")");
            String f3 = p3.f();
            if (d.p(Constants.USER_SETTING) != null) {
                JsonElement p4 = d.p(Constants.USER_SETTING);
                Wg6.b(p4, "jsonObject.get(\"settings\")");
                str = Jj5.a(p4.d());
            } else {
                str = "";
            }
            Wg6.b(f, "id");
            Wg6.b(f3, "appId");
            Wg6.b(f2, "category");
            DianaAppSetting dianaAppSetting = new DianaAppSetting(f, f3, f2, str);
            dianaAppSetting.getPinType();
            return dianaAppSetting;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hu3
    public /* bridge */ /* synthetic */ DianaAppSetting deserialize(JsonElement jsonElement, Type type, Gu3 gu3) {
        return a(jsonElement, type, gu3);
    }
}
