package com.portfolio.platform;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.text.TextUtils;
import android.util.Log;
import android.view.accessibility.AccessibilityManager;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Jn5;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Pm7;
import com.fossil.So4;
import com.fossil.Tt4;
import com.fossil.Ux7;
import com.fossil.Wt7;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Fd;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Md;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.buddy_challenge.domain.ProfileRepository;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.preset.data.source.DianaRecommendedPresetRepository;
import com.portfolio.platform.service.buddychallenge.BuddyChallengeManager;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import java.util.List;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ApplicationEventListener implements Fd {
    @DexIgnore
    public static /* final */ String I;
    @DexIgnore
    public static /* final */ a J; // = new a(null);
    @DexIgnore
    public /* final */ WorkoutSettingRepository A;
    @DexIgnore
    public /* final */ FlagRepository B;
    @DexIgnore
    public /* final */ BuddyChallengeManager C;
    @DexIgnore
    public /* final */ ThemeRepository D;
    @DexIgnore
    public /* final */ WFAssetRepository E;
    @DexIgnore
    public /* final */ DianaPresetRepository F;
    @DexIgnore
    public /* final */ WFBackgroundPhotoRepository G;
    @DexIgnore
    public /* final */ DianaAppSettingRepository H;
    @DexIgnore
    public /* final */ Il6 b; // = Jv7.a(Bw7.b().plus(Ux7.b(null, 1, null)));
    @DexIgnore
    public /* final */ PortfolioApp c;
    @DexIgnore
    public /* final */ An4 d;
    @DexIgnore
    public /* final */ HybridPresetRepository e;
    @DexIgnore
    public /* final */ CategoryRepository f;
    @DexIgnore
    public /* final */ WatchAppRepository g;
    @DexIgnore
    public /* final */ ComplicationRepository h;
    @DexIgnore
    public /* final */ MicroAppRepository i;
    @DexIgnore
    public /* final */ RingStyleRepository j;
    @DexIgnore
    public /* final */ DeviceRepository k;
    @DexIgnore
    public /* final */ UserRepository l;
    @DexIgnore
    public /* final */ Cj4 m;
    @DexIgnore
    public /* final */ AlarmsRepository s;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository t;
    @DexIgnore
    public /* final */ WatchLocalizationRepository u;
    @DexIgnore
    public /* final */ ProfileRepository v;
    @DexIgnore
    public /* final */ FriendRepository w;
    @DexIgnore
    public /* final */ Tt4 x;
    @DexIgnore
    public /* final */ FileRepository y;
    @DexIgnore
    public /* final */ FCMRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ApplicationEventListener.I;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ApplicationEventListener", f = "ApplicationEventListener.kt", l = {128, 129, 130, 131, 134, 135, 136, 139, ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL, 141, 142, 143, 144, 150, 157, 162, 163, DateTimeConstants.HOURS_PER_WEEK, 169, 170, 171, 172, 173, 174, 175, 176, 179, 180, 181}, m = "downloadResources")
    public static final class b extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ApplicationEventListener this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ApplicationEventListener applicationEventListener, Xe6 xe6) {
            super(xe6);
            this.this$0 = applicationEventListener;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.j(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$1", f = "ApplicationEventListener.kt", l = {92, 93}, m = "invokeSuspend")
    public static final class c extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ApplicationEventListener this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ApplicationEventListener applicationEventListener, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = applicationEventListener;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (Il6) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((c) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r6) {
            /*
                r5 = this;
                r4 = 2
                r3 = 1
                java.lang.Object r1 = com.fossil.Yn7.d()
                int r0 = r5.label
                if (r0 == 0) goto L_0x0039
                if (r0 == r3) goto L_0x0020
                if (r0 != r4) goto L_0x0018
                java.lang.Object r0 = r5.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r6)
            L_0x0015:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r5.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r6)
            L_0x0027:
                com.portfolio.platform.ApplicationEventListener r2 = r5.this$0
                com.portfolio.platform.PortfolioApp r2 = com.portfolio.platform.ApplicationEventListener.c(r2)
                r5.L$0 = r0
                r5.label = r4
                java.lang.Object r0 = r2.d1(r5)
                if (r0 != r1) goto L_0x0015
                r0 = r1
                goto L_0x0017
            L_0x0039:
                com.fossil.El7.b(r6)
                com.mapped.Il6 r0 = r5.p$
                com.portfolio.platform.ApplicationEventListener r2 = r5.this$0
                com.portfolio.platform.data.source.ThemeRepository r2 = com.portfolio.platform.ApplicationEventListener.e(r2)
                r5.L$0 = r0
                r5.label = r3
                java.lang.Object r2 = r2.initializeLocalTheme(r5)
                if (r2 != r1) goto L_0x0027
                r0 = r1
                goto L_0x0017
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ApplicationEventListener.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.ApplicationEventListener$onAppEnterForeground$2", f = "ApplicationEventListener.kt", l = {101, 102, 103, 108, 119}, m = "invokeSuspend")
    public static final class d extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isMigrationComplete;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ApplicationEventListener this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ApplicationEventListener applicationEventListener, boolean z, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = applicationEventListener;
            this.$isMigrationComplete = z;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            d dVar = new d(this.this$0, this.$isMigrationComplete, xe6);
            dVar.p$ = (Il6) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((d) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x004f  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00af  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00c0  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x010a  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0157  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x015f  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 360
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ApplicationEventListener.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = So4.INSTANCE.getClass().getSimpleName();
        Wg6.b(simpleName, "ApplicationEventListener\u2026lass.javaClass.simpleName");
        I = simpleName;
    }
    */

    @DexIgnore
    public ApplicationEventListener(PortfolioApp portfolioApp, An4 an4, HybridPresetRepository hybridPresetRepository, CategoryRepository categoryRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, MicroAppRepository microAppRepository, com.portfolio.platform.data.source.DianaPresetRepository dianaPresetRepository, RingStyleRepository ringStyleRepository, DeviceRepository deviceRepository, UserRepository userRepository, Cj4 cj4, AlarmsRepository alarmsRepository, DianaWatchFaceRepository dianaWatchFaceRepository, WatchLocalizationRepository watchLocalizationRepository, ProfileRepository profileRepository, FriendRepository friendRepository, Tt4 tt4, FileRepository fileRepository, FCMRepository fCMRepository, WorkoutSettingRepository workoutSettingRepository, FlagRepository flagRepository, BuddyChallengeManager buddyChallengeManager, ThemeRepository themeRepository, WFAssetRepository wFAssetRepository, DianaPresetRepository dianaPresetRepository2, DianaRecommendedPresetRepository dianaRecommendedPresetRepository, WFBackgroundPhotoRepository wFBackgroundPhotoRepository, DianaAppSettingRepository dianaAppSettingRepository) {
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(an4, "mSharedPrefs");
        Wg6.c(hybridPresetRepository, "mHybridPresetRepository");
        Wg6.c(categoryRepository, "mCategoryRepository");
        Wg6.c(watchAppRepository, "mWatchAppRepository");
        Wg6.c(complicationRepository, "mComplicationRepository");
        Wg6.c(microAppRepository, "mMicroAppRepository");
        Wg6.c(dianaPresetRepository, "mDianaPresetRepository");
        Wg6.c(ringStyleRepository, "mRingStyleRepository");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(cj4, "mDeviceSettingFactory");
        Wg6.c(alarmsRepository, "mAlarmRepository");
        Wg6.c(dianaWatchFaceRepository, "mWatchFaceRepository");
        Wg6.c(watchLocalizationRepository, "watchLocalization");
        Wg6.c(profileRepository, "profileRepository");
        Wg6.c(friendRepository, "friendRepository");
        Wg6.c(tt4, "challengeRepository");
        Wg6.c(fileRepository, "mFileRepository");
        Wg6.c(fCMRepository, "fcmRepository");
        Wg6.c(workoutSettingRepository, "mWorkoutSettingRepository");
        Wg6.c(flagRepository, "flagRepository");
        Wg6.c(buddyChallengeManager, "buddyChallengeManager");
        Wg6.c(themeRepository, "mThemeRepository");
        Wg6.c(wFAssetRepository, "assetRepository");
        Wg6.c(dianaPresetRepository2, "dianaPresetRepository");
        Wg6.c(dianaRecommendedPresetRepository, "dianaRecommendedPresetRepository");
        Wg6.c(wFBackgroundPhotoRepository, "wfBackgroundPhotoRepository");
        Wg6.c(dianaAppSettingRepository, "dianaAppSettingRepository");
        this.c = portfolioApp;
        this.d = an4;
        this.e = hybridPresetRepository;
        this.f = categoryRepository;
        this.g = watchAppRepository;
        this.h = complicationRepository;
        this.i = microAppRepository;
        this.j = ringStyleRepository;
        this.k = deviceRepository;
        this.l = userRepository;
        this.m = cj4;
        this.s = alarmsRepository;
        this.t = dianaWatchFaceRepository;
        this.u = watchLocalizationRepository;
        this.v = profileRepository;
        this.w = friendRepository;
        this.x = tt4;
        this.y = fileRepository;
        this.z = fCMRepository;
        this.A = workoutSettingRepository;
        this.B = flagRepository;
        this.C = buddyChallengeManager;
        this.D = themeRepository;
        this.E = wFAssetRepository;
        this.F = dianaPresetRepository2;
        this.G = wFBackgroundPhotoRepository;
        this.H = dianaAppSettingRepository;
    }

    @DexIgnore
    public final /* synthetic */ Object i(Xe6<? super Cd6> xe6) {
        FLogger.INSTANCE.getLocal().d(I, "Inside .autoSync");
        String J2 = this.c.J();
        if (TextUtils.isEmpty(J2)) {
            FLogger.INSTANCE.getLocal().d(I, "User has no active device, skip auto sync");
            return Cd6.a;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = I;
        local.d(str, "Inside .autoSync lastSyncSuccess=" + this.d.B(this.c.J()));
        long currentTimeMillis = System.currentTimeMillis() - this.d.C(this.c.J());
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.OTHER;
        String str2 = I;
        remote.i(component, session, J2, str2, "[App Open] Last sync OK interval " + currentTimeMillis);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str3 = I;
        local2.d(str3, "Inside .autoSync, last sync interval " + currentTimeMillis);
        if (currentTimeMillis >= ((long) CommuteTimeService.A) || currentTimeMillis < 0) {
            PortfolioApp portfolioApp = this.c;
            if (portfolioApp.R(portfolioApp.J()) == CommunicateMode.OTA.getValue()) {
                FLogger.INSTANCE.getLocal().d(I, "Inside .autoSync, device is ota, wait for the next time");
            } else if (this.c.z0() || this.d.X()) {
                FLogger.INSTANCE.getLocal().d(I, "Inside .autoSync, start auto-sync.");
                boolean c2 = Jn5.c(Jn5.b, this.c, Jn5.Ai.SET_BLE_COMMAND, false, false, false, null, 56, null);
                IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                FLogger.Component component2 = FLogger.Component.APP;
                FLogger.Session session2 = FLogger.Session.OTHER;
                String str4 = I;
                remote2.i(component2, session2, J2, str4, "[App Open] [Sync Start] AUTO SYNC isBlueToothEnabled " + BluetoothUtils.isBluetoothEnable() + ' ');
                if (c2) {
                    this.c.S1(this.m, false, 10);
                    return Cd6.a;
                }
                FLogger.INSTANCE.getLocal().d(I, "autoSync fail due to lack of permission");
            } else {
                FLogger.INSTANCE.getLocal().d(I, "Inside .autoSync, doesn't start auto-sync.");
            }
        }
        return Cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:101:0x0351  */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0354  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x0373  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0376  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0385  */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x039a  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x03b3  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x03c9  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x03ea  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x03ed  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0431  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0434  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0452  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0455  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x0473  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0476  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0495  */
    /* JADX WARNING: Removed duplicated region for block: B:137:0x0498  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x0509  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x052e  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x0531  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x0535  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x053a  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x053f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x0548  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x054c  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x054f  */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0555  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x0559  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ce  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x013f  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x015e  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0161  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0180  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0183  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01a3  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01a6  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01cb  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01ce  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01ed  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01f0  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x020d  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0222  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0261  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0295  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x02ad  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02bd  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x02dd  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x02e0  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x030d  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0310  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x032f  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0332  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object j(java.lang.String r13, boolean r14, com.mapped.Xe6<? super com.mapped.Cd6> r15) {
        /*
        // Method dump skipped, instructions count: 1438
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.ApplicationEventListener.j(java.lang.String, boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void k() {
        Object systemService = this.c.getSystemService("accessibility");
        if (systemService != null) {
            List<AccessibilityServiceInfo> enabledAccessibilityServiceList = ((AccessibilityManager) systemService).getEnabledAccessibilityServiceList(-1);
            StringBuilder sb = new StringBuilder();
            for (AccessibilityServiceInfo accessibilityServiceInfo : enabledAccessibilityServiceList) {
                Wg6.b(accessibilityServiceInfo, "accessibility");
                String id = accessibilityServiceInfo.getId();
                Wg6.b(id, "accessibility.id");
                sb.append((String) Pm7.P(Wt7.Y(id, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0, 6, null)));
                sb.append(" ");
            }
            String sb2 = sb.toString();
            Wg6.b(sb2, "enabledAccessibilityStringBuilder.toString()");
            if (sb2 != null) {
                String obj = Wt7.u0(sb2).toString();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = I;
                local.d(str, "Enabled Accessibility: " + obj);
                AnalyticsHelper.f.g().j("accessibility_config_on_launch", obj);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
        throw new Rc6("null cannot be cast to non-null type android.view.accessibility.AccessibilityManager");
    }

    @DexIgnore
    @Md(Lifecycle.a.ON_STOP)
    public final void onAppEnterBackground() {
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.c.J(), I, "[App Close] User put app in background");
    }

    @DexIgnore
    @Md(Lifecycle.a.ON_START)
    public final void onAppEnterForeground() {
        Log.d(I, "ApplicationEventListener onAppEnterForeground");
        Rm6 unused = Gu7.d(this.b, null, null, new c(this, null), 3, null);
        boolean k0 = this.d.k0(this.c.P());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = I;
        local.d(str, "onAppEnterForeground isMigrationComplete " + k0);
        if (k0) {
            Rm6 unused2 = Gu7.d(this.b, null, null, new d(this, k0, null), 3, null);
        }
    }
}
