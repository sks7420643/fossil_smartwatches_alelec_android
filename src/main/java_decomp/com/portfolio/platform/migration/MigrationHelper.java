package com.portfolio.platform.migration;

import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MigrationHelper {
    @DexIgnore
    public /* final */ An4 a;
    @DexIgnore
    public /* final */ DeviceRepository b;
    @DexIgnore
    public /* final */ DianaPresetRepository c;
    @DexIgnore
    public /* final */ MigrationManager d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.migration.MigrationHelper$isNeedToBlockingMigratePresetToWatchFace$2", f = "MigrationHelper.kt", l = {37}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MigrationHelper this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(MigrationHelper migrationHelper, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = migrationHelper;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            T t;
            Object allPresets;
            Device device;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Boolean j0 = this.this$0.a.j0();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("MigrationHelper", "isNeedToBlockingMigratePresetToWatchFace isMigrated " + j0);
                if (!j0.booleanValue()) {
                    Iterator<T> it = this.this$0.b.getAllDevice().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        T next = it.next();
                        if (Ao7.a(DeviceHelper.o.x(next.getDeviceId())).booleanValue()) {
                            t = next;
                            break;
                        }
                    }
                    T t2 = t;
                    DianaPresetRepository dianaPresetRepository = this.this$0.c;
                    this.L$0 = il6;
                    this.L$1 = j0;
                    this.L$2 = t2;
                    this.label = 1;
                    allPresets = dianaPresetRepository.getAllPresets(this);
                    if (allPresets == d) {
                        return d;
                    }
                    device = t2;
                }
                this.this$0.a.G1(Ao7.a(true));
                return Ao7.a(false);
            } else if (i == 1) {
                Boolean bool = (Boolean) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                device = (Device) this.L$2;
                allPresets = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list = (List) allPresets;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("MigrationHelper", "isNeedToBlockingMigratePresetToWatchFace dianaDevice " + device + " oldPresetSize " + list.size());
            if (device != null && (!list.isEmpty())) {
                return Ao7.a(true);
            }
            this.this$0.a.G1(Ao7.a(true));
            return Ao7.a(false);
        }
    }

    @DexIgnore
    public MigrationHelper(An4 an4, DeviceRepository deviceRepository, DianaPresetRepository dianaPresetRepository, MigrationManager migrationManager) {
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(dianaPresetRepository, "oldPresetRepository");
        Wg6.c(migrationManager, "mMigrationManager");
        this.a = an4;
        this.b = deviceRepository;
        this.c = dianaPresetRepository;
        this.d = migrationManager;
    }

    @DexIgnore
    public final boolean d(String str) {
        Wg6.c(str, "version");
        return this.a.k0(str);
    }

    @DexIgnore
    public final Object e(Xe6<? super Boolean> xe6) {
        return Eu7.g(Bw7.b(), new Ai(this, null), xe6);
    }

    @DexIgnore
    public final Object f(String str, Xe6<? super Boolean> xe6) {
        String x = this.a.x();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MigrationHelper", "start migration for " + str + " lastVersion " + x);
        return this.d.x(xe6);
    }

    @DexIgnore
    public final Object g(Xe6<? super Integer> xe6) {
        return this.d.s(xe6);
    }
}
