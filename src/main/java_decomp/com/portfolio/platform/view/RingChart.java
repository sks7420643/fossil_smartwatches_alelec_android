package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingChart extends View {
    @DexIgnore
    public /* final */ ArrayList<b> b; // = new ArrayList<>();
    @DexIgnore
    public float c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public float f;
    @DexIgnore
    public Paint g; // = new Paint(1);
    @DexIgnore
    public Paint h;
    @DexIgnore
    public Paint i;
    @DexIgnore
    public GestureDetector j;
    @DexIgnore
    public View.OnClickListener k;
    @DexIgnore
    public RectF l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a() {
        }

        @DexIgnore
        public boolean onDown(MotionEvent motionEvent) {
            Wg6.c(motionEvent, "e");
            return RingChart.this.getMListener$app_fossilRelease() != null;
        }

        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            Wg6.c(motionEvent, "e");
            if (RingChart.this.getMListener$app_fossilRelease() != null) {
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                float measuredWidth = (float) (RingChart.this.getMeasuredWidth() / 2);
                float f = x - measuredWidth;
                float f2 = y - measuredWidth;
                if ((f * f) + (f2 * f2) < (RingChart.this.getMThickness$app_fossilRelease() + measuredWidth) * (measuredWidth + RingChart.this.getMThickness$app_fossilRelease())) {
                    View.OnClickListener mListener$app_fossilRelease = RingChart.this.getMListener$app_fossilRelease();
                    if (mListener$app_fossilRelease != null) {
                        mListener$app_fossilRelease.onClick(RingChart.this);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
            return super.onSingleTapUp(motionEvent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b {
        @DexIgnore
        public float a;
        @DexIgnore
        public int b;
        @DexIgnore
        public float c;
        @DexIgnore
        public float d;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final float b() {
            return this.d;
        }

        @DexIgnore
        public final float c() {
            return this.c;
        }

        @DexIgnore
        public final float d() {
            return this.a;
        }

        @DexIgnore
        public final void e(int i) {
            this.b = i;
        }

        @DexIgnore
        public final void f(float f) {
            this.d = f;
        }

        @DexIgnore
        public final void g(float f) {
            this.c = f;
        }

        @DexIgnore
        public final void h(float f) {
            this.a = f;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingChart(Context context) {
        super(context);
        Wg6.c(context, "context");
        b();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, X24.RingChart, 0, 0);
        try {
            this.d = obtainStyledAttributes.getInteger(1, 0);
            this.e = obtainStyledAttributes.getColor(0, 0);
            Resources resources = context.getResources();
            Wg6.b(resources, "context.resources");
            this.f = obtainStyledAttributes.getDimension(2, resources.getDisplayMetrics().density * ((float) 8));
            obtainStyledAttributes.recycle();
            b();
        } catch (Throwable th) {
            obtainStyledAttributes.recycle();
            throw th;
        }
    }

    @DexIgnore
    public final void a(float f2, int i2) {
        b bVar = new b();
        bVar.e(i2);
        bVar.h(f2);
        this.c += f2;
        this.b.add(bVar);
    }

    @DexIgnore
    public final void b() {
        setLayerType(1, null);
        this.g.setStyle(Paint.Style.FILL);
        Paint paint = new Paint(this.g);
        this.h = paint;
        if (paint != null) {
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            this.j = new GestureDetector(getContext(), new a());
            Paint paint2 = new Paint(this.g);
            this.i = paint2;
            if (paint2 != null) {
                paint2.setAlpha(50);
                if (isInEditMode()) {
                    Random random = new Random();
                    float f2 = (float) 100;
                    float nextFloat = random.nextFloat() * f2;
                    float f3 = f2 - nextFloat;
                    float nextFloat2 = random.nextFloat() * f3;
                    float nextFloat3 = random.nextFloat();
                    a(nextFloat, -65536);
                    a(nextFloat2, -16711936);
                    a(nextFloat3 * (f3 - nextFloat2), -16776961);
                    if (this.d == 0) {
                        this.d = 100;
                        return;
                    }
                    return;
                }
                return;
            }
            Wg6.n("mBackgroundPaint");
            throw null;
        }
        Wg6.n("mClearPaint");
        throw null;
    }

    @DexIgnore
    public final void c() {
        float max = Math.max(this.c, (float) this.d);
        Iterator<b> it = this.b.iterator();
        float f2 = -90.0f;
        while (true) {
            float f3 = f2;
            if (it.hasNext()) {
                b next = it.next();
                next.g(f3);
                if (max != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    f3 += (next.d() / max) * 360.0f;
                }
                next.f(f3);
                if (next.b() > ((float) 270)) {
                    next.f(270.0f);
                }
                f2 = next.b();
            } else {
                invalidate();
                return;
            }
        }
    }

    @DexIgnore
    public final View.OnClickListener getMListener$app_fossilRelease() {
        return this.k;
    }

    @DexIgnore
    public final float getMThickness$app_fossilRelease() {
        return this.f;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        float f2;
        Wg6.c(canvas, "canvas");
        super.onDraw(canvas);
        RectF rectF = this.l;
        if (rectF != null) {
            float centerX = rectF.centerX();
            RectF rectF2 = this.l;
            if (rectF2 != null) {
                float centerY = rectF2.centerY();
                RectF rectF3 = this.l;
                if (rectF3 != null) {
                    float f3 = (float) 2;
                    float width = rectF3.width() / f3;
                    Paint paint = this.i;
                    if (paint != null) {
                        canvas.drawCircle(centerX, centerY, width, paint);
                        Iterator<b> it = this.b.iterator();
                        while (it.hasNext()) {
                            b next = it.next();
                            this.g.setColor(next.a());
                            if (next.b() > next.c()) {
                                RectF rectF4 = this.l;
                                if (rectF4 != null) {
                                    canvas.drawArc(rectF4, next.c(), next.b() - next.c(), true, this.g);
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            }
                        }
                        float f4 = this.c;
                        if (f4 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f4 < ((float) this.d)) {
                            this.g.setColor(this.e);
                            if (this.b.isEmpty() || this.c == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                f2 = -90.0f;
                            } else {
                                ArrayList<b> arrayList = this.b;
                                f2 = arrayList.get(arrayList.size() - 1).b();
                            }
                            if (270.0f > f2) {
                                RectF rectF5 = this.l;
                                if (rectF5 != null) {
                                    canvas.drawArc(rectF5, f2, 270.0f - f2, true, this.g);
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            }
                        }
                        RectF rectF6 = this.l;
                        if (rectF6 != null) {
                            float centerX2 = rectF6.centerX();
                            RectF rectF7 = this.l;
                            if (rectF7 != null) {
                                float centerY2 = rectF7.centerY();
                                RectF rectF8 = this.l;
                                if (rectF8 != null) {
                                    float width2 = rectF8.width() / f3;
                                    float f5 = this.f;
                                    Paint paint2 = this.h;
                                    if (paint2 != null) {
                                        canvas.drawCircle(centerX2, centerY2, width2 - f5, paint2);
                                    } else {
                                        Wg6.n("mClearPaint");
                                        throw null;
                                    }
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.n("mBackgroundPaint");
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        RectF rectF = this.l;
        if (rectF == null) {
            this.l = new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) i2, (float) i3);
        } else if (rectF != null) {
            rectF.right = (float) i2;
            if (rectF != null) {
                rectF.bottom = (float) i3;
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
        c();
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        Wg6.c(motionEvent, Constants.EVENT);
        GestureDetector gestureDetector = this.j;
        if (gestureDetector != null) {
            return gestureDetector.onTouchEvent(motionEvent);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void setGoal(int i2) {
        this.d = i2;
    }

    @DexIgnore
    public final void setMListener$app_fossilRelease(View.OnClickListener onClickListener) {
        this.k = onClickListener;
    }

    @DexIgnore
    public final void setMThickness$app_fossilRelease(float f2) {
        this.f = f2;
    }
}
