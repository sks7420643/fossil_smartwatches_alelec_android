package com.portfolio.platform.view.indicator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Il5;
import com.mapped.W6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.R;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CirclePageIndicator extends View implements ViewPager.i {
    @DexIgnore
    public float b;
    @DexIgnore
    public /* final */ Paint c;
    @DexIgnore
    public /* final */ Paint d;
    @DexIgnore
    public /* final */ Paint e;
    @DexIgnore
    public RecyclerView f;
    @DexIgnore
    public ViewPager g;
    @DexIgnore
    public ViewPager.i h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public float k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public float u;
    @DexIgnore
    public int v;
    @DexIgnore
    public float w;
    @DexIgnore
    public int x;
    @DexIgnore
    public boolean y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends View.BaseSavedState {
        @DexIgnore
        public static /* final */ Parcelable.Creator<a> CREATOR; // = new a();
        @DexIgnore
        public int b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Parcelable.Creator<a> {
            @DexIgnore
            public a a(Parcel parcel) {
                return new a(parcel);
            }

            @DexIgnore
            public a[] b(int i) {
                return new a[i];
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // android.os.Parcelable.Creator
            public /* bridge */ /* synthetic */ a createFromParcel(Parcel parcel) {
                return a(parcel);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object[]' to match base method */
            @Override // android.os.Parcelable.Creator
            public /* bridge */ /* synthetic */ a[] newArray(int i) {
                return b(i);
            }
        }

        @DexIgnore
        public a(Parcel parcel) {
            super(parcel);
            this.b = parcel.readInt();
        }

        @DexIgnore
        public a(Parcelable parcelable) {
            super(parcelable);
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.b);
        }
    }

    @DexIgnore
    public CirclePageIndicator(Context context) {
        this(context, null);
    }

    @DexIgnore
    public CirclePageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 2130969880);
    }

    @DexIgnore
    public CirclePageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.c = new Paint(1);
        this.d = new Paint(1);
        this.e = new Paint(1);
        this.w = -1.0f;
        this.x = -1;
        if (!isInEditMode()) {
            Resources resources = getResources();
            int d2 = W6.d(context, 2131099842);
            int d3 = W6.d(context, 2131099829);
            int d4 = W6.d(context, R.color.transparent);
            float dimension = resources.getDimension(2131165426);
            float dimension2 = resources.getDimension(2131165440);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.CirclePageIndicator, i2, 0);
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, X24.LinePageIndicator, i2, 0);
            this.s = obtainStyledAttributes.getBoolean(4, true);
            this.m = obtainStyledAttributes.getInt(0, 0);
            this.c.setStyle(Paint.Style.FILL);
            this.c.setColor(obtainStyledAttributes.getColor(5, d2));
            this.d.setStyle(Paint.Style.STROKE);
            this.d.setColor(obtainStyledAttributes.getColor(9, d4));
            this.d.setStrokeWidth(obtainStyledAttributes.getDimension(10, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            this.e.setStyle(Paint.Style.FILL);
            this.e.setColor(obtainStyledAttributes.getColor(3, d3));
            this.b = obtainStyledAttributes.getDimension(6, dimension);
            this.t = obtainStyledAttributes.getBoolean(8, false);
            this.u = obtainStyledAttributes2.getDimension(1, dimension2);
            Drawable drawable = obtainStyledAttributes.getDrawable(1);
            if (drawable != null) {
                setBackground(drawable);
            }
            obtainStyledAttributes.recycle();
            obtainStyledAttributes2.recycle();
            this.v = ViewConfiguration.get(context).getScaledPagingTouchSlop();
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void a(int i2, float f2, int i3) {
        this.i = i2;
        this.k = f2;
        invalidate();
        ViewPager.i iVar = this.h;
        if (iVar != null) {
            iVar.a(i2, f2, i3);
        }
    }

    @DexIgnore
    public final int b(int i2) {
        ViewPager viewPager;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824 || (viewPager = this.g) == null) {
            return size;
        }
        int e2 = viewPager.getAdapter().e();
        float f2 = this.b;
        int paddingLeft = (int) ((((float) (e2 - 1)) * f2) + ((float) (getPaddingLeft() + getPaddingRight())) + (((float) (e2 * 2)) * f2) + 1.0f);
        return mode == Integer.MIN_VALUE ? Math.min(paddingLeft, size) : paddingLeft;
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void c(int i2) {
        this.l = i2;
        ViewPager.i iVar = this.h;
        if (iVar != null) {
            iVar.c(i2);
        }
    }

    @DexIgnore
    @Override // androidx.viewpager.widget.ViewPager.i
    public void d(int i2) {
        if (this.t || this.l == 0) {
            this.i = Il5.a(getContext()) ? (this.g.getAdapter().e() - i2) - 1 : i2;
            this.j = i2;
            invalidate();
        }
        ViewPager.i iVar = this.h;
        if (iVar != null) {
            iVar.d(i2);
        }
    }

    @DexIgnore
    public final int e(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            return size;
        }
        int paddingTop = (int) ((this.b * 2.0f) + ((float) getPaddingTop()) + ((float) getPaddingBottom()) + 1.0f);
        return mode == Integer.MIN_VALUE ? Math.min(paddingTop, size) : paddingTop;
    }

    @DexIgnore
    public void f(ViewPager viewPager, int i2) {
        setViewPager(viewPager);
        setCurrentItem(i2);
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        int itemCount;
        int height;
        int paddingTop;
        int paddingBottom;
        int paddingLeft;
        float f2;
        float f3;
        float f4;
        float f5;
        super.onDraw(canvas);
        ViewPager viewPager = this.g;
        if (viewPager == null || viewPager.getAdapter() == null) {
            RecyclerView recyclerView = this.f;
            itemCount = (recyclerView == null || recyclerView.getAdapter() == null) ? 0 : this.f.getAdapter().getItemCount();
        } else {
            itemCount = this.g.getAdapter().e();
        }
        if (itemCount != 0) {
            if (this.i >= itemCount) {
                setCurrentItem(itemCount - 1);
                return;
            }
            if (this.m == 0) {
                height = getWidth();
                paddingTop = getPaddingLeft();
                paddingBottom = getPaddingRight();
                paddingLeft = getPaddingTop();
            } else {
                height = getHeight();
                paddingTop = getPaddingTop();
                paddingBottom = getPaddingBottom();
                paddingLeft = getPaddingLeft();
            }
            float f6 = this.b;
            float f7 = this.u + (f6 * 2.0f);
            float f8 = ((float) paddingLeft) + f6;
            float f9 = ((float) paddingTop) + f6;
            if (this.s) {
                f9 += (((float) ((height - paddingTop) - paddingBottom)) / 2.0f) - ((((float) itemCount) * f7) / 2.0f);
            }
            float f10 = this.b;
            float strokeWidth = this.d.getStrokeWidth() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? f10 - (this.d.getStrokeWidth() / 2.0f) : f10;
            for (int i2 = 0; i2 < itemCount; i2++) {
                float f11 = (((float) i2) * f7) + f9;
                if (this.m == 0) {
                    f4 = f8;
                    f5 = f11;
                } else {
                    f4 = f11;
                    f5 = f8;
                }
                if (this.c.getAlpha() > 0) {
                    canvas.drawCircle(f5, f4, strokeWidth, this.c);
                }
                float f12 = this.b;
                if (strokeWidth != f12) {
                    canvas.drawCircle(f5, f4, f12, this.d);
                }
            }
            float f13 = ((float) (this.t ? this.j : this.i)) * f7;
            if (!this.t) {
                f13 += this.k * f7;
            }
            if (this.m == 0) {
                f3 = f9 + f13;
                f2 = f8;
            } else {
                f2 = f13 + f9;
                f3 = f8;
            }
            canvas.drawCircle(f3, f2, this.b, this.e);
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (this.m == 0) {
            setMeasuredDimension(b(i2), e(i3));
        } else {
            setMeasuredDimension(e(i2), b(i3));
        }
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        a aVar = (a) parcelable;
        super.onRestoreInstanceState(aVar.getSuperState());
        int i2 = aVar.b;
        this.i = i2;
        this.j = i2;
        requestLayout();
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        a aVar = new a(super.onSaveInstanceState());
        aVar.b = this.i;
        return aVar;
    }

    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int i2 = 0;
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        ViewPager viewPager = this.g;
        if (viewPager == null || viewPager.getAdapter() == null || this.g.getAdapter().e() == 0) {
            return false;
        }
        int action = motionEvent.getAction() & 255;
        if (action != 0) {
            if (action != 1) {
                if (action == 2) {
                    float x2 = motionEvent.getX(motionEvent.findPointerIndex(this.x));
                    float f2 = x2 - this.w;
                    if (!this.y && Math.abs(f2) > ((float) this.v)) {
                        this.y = true;
                    }
                    if (!this.y) {
                        return true;
                    }
                    this.w = x2;
                    if (!this.g.A() && !this.g.e()) {
                        return true;
                    }
                    this.g.s(f2);
                    return true;
                } else if (action != 3) {
                    if (action == 5) {
                        int actionIndex = motionEvent.getActionIndex();
                        this.w = motionEvent.getX(actionIndex);
                        this.x = motionEvent.getPointerId(actionIndex);
                        return true;
                    } else if (action != 6) {
                        return true;
                    } else {
                        int actionIndex2 = motionEvent.getActionIndex();
                        if (motionEvent.getPointerId(actionIndex2) == this.x) {
                            if (actionIndex2 == 0) {
                                i2 = 1;
                            }
                            this.x = motionEvent.getPointerId(i2);
                        }
                        this.w = motionEvent.getX(motionEvent.findPointerIndex(this.x));
                        return true;
                    }
                }
            }
            if (!this.y) {
                int e2 = this.g.getAdapter().e();
                float width = (float) getWidth();
                float f3 = width / 2.0f;
                float f4 = width / 6.0f;
                if (this.i <= 0 || motionEvent.getX() >= f3 - f4) {
                    if (this.i < e2 - 1 && motionEvent.getX() > f4 + f3) {
                        if (action == 3) {
                            return true;
                        }
                        this.g.setCurrentItem(this.i + 1);
                        return true;
                    }
                } else if (action == 3) {
                    return true;
                } else {
                    this.g.setCurrentItem(this.i - 1);
                    return true;
                }
            }
            this.y = false;
            this.x = -1;
            if (!this.g.A()) {
                return true;
            }
            this.g.q();
            return true;
        }
        this.x = motionEvent.getPointerId(0);
        this.w = motionEvent.getX();
        return true;
    }

    @DexIgnore
    public void setCurrentItem(int i2) {
        if (this.g == null && this.f == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        ViewPager viewPager = this.g;
        if (viewPager != null) {
            viewPager.setCurrentItem(i2);
            if (Il5.a(getContext())) {
                i2 = (this.g.getAdapter().e() - i2) - 1;
            }
            this.i = i2;
        } else {
            if (Il5.a(getContext())) {
                i2 = (this.f.getAdapter().getItemCount() - i2) - 1;
            }
            this.i = i2;
        }
        invalidate();
    }

    @DexIgnore
    public void setOnPageChangeListener(ViewPager.i iVar) {
        this.h = iVar;
    }

    @DexIgnore
    public void setViewPager(ViewPager viewPager) {
        if (!Objects.equals(this.g, viewPager)) {
            ViewPager viewPager2 = this.g;
            if (viewPager2 != null) {
                viewPager2.N(this);
            }
            if (viewPager.getAdapter() != null) {
                this.g = viewPager;
                viewPager.c(this);
                invalidate();
                return;
            }
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
    }
}
