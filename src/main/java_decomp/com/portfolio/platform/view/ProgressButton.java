package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.widget.FrameLayout;
import androidx.appcompat.app.AppCompatActivity;
import com.fossil.G57;
import com.fossil.Mo0;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.X24;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProgressButton extends FlexibleButton {
    @DexIgnore
    public int A;
    @DexIgnore
    public long B;
    @DexIgnore
    public Drawable[] C;
    @DexIgnore
    public G57 D;
    @DexIgnore
    public ViewGroup E;
    @DexIgnore
    public FrameLayout F;
    @DexIgnore
    public String G; // = "";
    @DexIgnore
    public String H; // = "";
    @DexIgnore
    public String I; // = "";
    @DexIgnore
    public String J; // = "";
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y; // = true;
    @DexIgnore
    public Drawable z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProgressButton(Context context) {
        super(context);
        Wg6.c(context, "context");
        h(context, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProgressButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        h(context, attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProgressButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        h(context, attributeSet);
    }

    @DexIgnore
    public final void e() {
        CharSequence a2;
        G57 g57 = this.D;
        if (g57 != null) {
            if (!this.x) {
                if (g57 != null) {
                    a2 = g57.b(isSelected());
                } else {
                    Wg6.i();
                    throw null;
                }
            } else if (g57 != null) {
                a2 = g57.a();
            } else {
                Wg6.i();
                throw null;
            }
            setText(a2);
        }
    }

    @DexIgnore
    public final void f() {
        G57 g57 = this.D;
        if (g57 != null) {
            setText(g57.b(isSelected()));
            Drawable[] drawableArr = this.C;
            if (drawableArr != null) {
                if (drawableArr != null) {
                    Drawable drawable = drawableArr[0];
                    if (drawableArr != null) {
                        Drawable drawable2 = drawableArr[1];
                        if (drawableArr != null) {
                            Drawable drawable3 = drawableArr[2];
                            if (drawableArr != null) {
                                setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawableArr[3]);
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            if (this.w) {
                setClickable(true);
            }
            this.x = false;
            this.A = 0;
            ViewGroup viewGroup = this.E;
            if (viewGroup != null && this.y) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.F);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void g() {
        G57 g57 = this.D;
        if (g57 != null) {
            setText(g57.a());
            this.C = (Drawable[]) Arrays.copyOf(getCompoundDrawables(), 4);
            setCompoundDrawables(null, null, null, null);
            if (this.w) {
                setClickable(false);
            }
            this.x = true;
            this.A = 0;
            if (this.E != null && this.y) {
                FrameLayout frameLayout = this.F;
                if (frameLayout != null) {
                    ViewParent parent = frameLayout.getParent();
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(this.F);
                    }
                    ViewGroup viewGroup = this.E;
                    if (viewGroup != null) {
                        viewGroup.addView(this.F);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void h(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.ProgressButton);
        if (obtainStyledAttributes.hasValue(5)) {
            this.z = obtainStyledAttributes.getDrawable(5);
        }
        G57 g57 = new G57();
        g57.c("");
        g57.d(getText());
        g57.e(getText());
        if (obtainStyledAttributes.hasValue(7)) {
            g57.d(obtainStyledAttributes.getString(7));
        }
        if (obtainStyledAttributes.hasValue(10)) {
            g57.e(obtainStyledAttributes.getString(10));
        }
        if (obtainStyledAttributes.hasValue(6)) {
            g57.c(obtainStyledAttributes.getString(6));
        }
        if (obtainStyledAttributes.hasValue(4)) {
            this.x = obtainStyledAttributes.getBoolean(4, false);
        }
        if (obtainStyledAttributes.hasValue(0)) {
            this.w = obtainStyledAttributes.getBoolean(0, true);
        }
        if (obtainStyledAttributes.hasValue(3)) {
            this.y = obtainStyledAttributes.getBoolean(3, true);
        }
        if (obtainStyledAttributes.hasValue(8)) {
            String string = obtainStyledAttributes.getString(8);
            if (string == null) {
                string = "";
            }
            this.G = string;
        }
        if (obtainStyledAttributes.hasValue(9)) {
            String string2 = obtainStyledAttributes.getString(9);
            if (string2 == null) {
                string2 = "";
            }
            this.H = string2;
        }
        if (obtainStyledAttributes.hasValue(1)) {
            String string3 = obtainStyledAttributes.getString(1);
            if (string3 == null) {
                string3 = "";
            }
            this.I = string3;
        }
        if (obtainStyledAttributes.hasValue(2)) {
            String string4 = obtainStyledAttributes.getString(2);
            if (string4 == null) {
                string4 = "";
            }
            this.J = string4;
        }
        this.D = g57;
        obtainStyledAttributes.recycle();
        if (context != null) {
            try {
                Window window = ((AppCompatActivity) context).getWindow();
                Wg6.b(window, "(context as AppCompatActivity).window");
                this.E = (ViewGroup) window.getDecorView().findViewById(16908290);
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
            this.F = new FrameLayout(getContext());
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
            FrameLayout frameLayout = this.F;
            if (frameLayout != null) {
                frameLayout.setBackgroundColor(0);
                FrameLayout frameLayout2 = this.F;
                if (frameLayout2 != null) {
                    frameLayout2.setClickable(true);
                    FrameLayout frameLayout3 = this.F;
                    if (frameLayout3 != null) {
                        frameLayout3.setLayoutParams(layoutParams);
                        Drawable drawable = this.z;
                        if (drawable != null) {
                            if (drawable == null) {
                                Wg6.i();
                                throw null;
                            } else if (drawable != null) {
                                int intrinsicWidth = drawable.getIntrinsicWidth();
                                Drawable drawable2 = this.z;
                                if (drawable2 != null) {
                                    drawable.setBounds(0, 0, intrinsicWidth, drawable2.getIntrinsicHeight());
                                    if (this.x) {
                                        g();
                                    } else {
                                        f();
                                    }
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                        if (TextUtils.isEmpty(this.G)) {
                            this.G = "primaryText";
                        }
                        if (TextUtils.isEmpty(this.H)) {
                            this.H = "nonBrandTextStyle2";
                        }
                        c(this.G, this.H, this.I, this.J);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
        throw new Rc6("null cannot be cast to non-null type androidx.appcompat.app.AppCompatActivity");
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        super.onDraw(canvas);
        if (this.z != null && this.x) {
            canvas.save();
            int width = getWidth();
            Drawable drawable = this.z;
            if (drawable != null) {
                int minimumWidth = (width - drawable.getMinimumWidth()) / 2;
                int height = getHeight();
                Drawable drawable2 = this.z;
                if (drawable2 != null) {
                    canvas.translate((float) minimumWidth, (float) ((height - drawable2.getMinimumHeight()) / 2));
                    long drawingTime = getDrawingTime();
                    if (drawingTime - this.B > 100) {
                        this.B = drawingTime;
                        int i = this.A + 1;
                        this.A = i;
                        if (((long) i) >= 12) {
                            this.A = 0;
                        }
                    }
                    int i2 = (int) (((float) (this.A * 10000)) / ((float) 12));
                    Drawable drawable3 = this.z;
                    if (drawable3 != null) {
                        drawable3.setLevel(i2);
                        Drawable drawable4 = this.z;
                        if (drawable4 != null) {
                            drawable4.draw(canvas);
                            canvas.restore();
                            Mo0.b0(this);
                            return;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public void setSelected(boolean z2) {
        super.setSelected(z2);
        e();
    }
}
