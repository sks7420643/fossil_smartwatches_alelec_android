package com.portfolio.platform.view.ruler;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Ai5;
import com.fossil.Hr7;
import com.fossil.K67;
import com.mapped.Qg6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.mapped.Xz5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RulerValuePicker extends FrameLayout implements Xz5.Bi {
    @DexIgnore
    public View b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public View d;
    @DexIgnore
    public RulerView e;
    @DexIgnore
    public Xz5 f;
    @DexIgnore
    public K67 g;
    @DexIgnore
    public Paint h;
    @DexIgnore
    public Path i;
    @DexIgnore
    public a j;
    @DexIgnore
    public TextView k;
    @DexIgnore
    public float l; // = 0.6f;
    @DexIgnore
    public int m; // = -1;
    @DexIgnore
    public Ai5 s; // = Ai5.METRIC;

    @DexIgnore
    public interface a extends Serializable {
        @DexIgnore
        String format(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends View.BaseSavedState {
        @DexIgnore
        public static /* final */ a CREATOR; // = new a(null);
        @DexIgnore
        public int b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Parcelable.Creator<b> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(Qg6 qg6) {
                this();
            }

            @DexIgnore
            public b a(Parcel parcel) {
                Wg6.c(parcel, "in");
                return new b(parcel);
            }

            @DexIgnore
            public b[] b(int i) {
                return new b[i];
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // android.os.Parcelable.Creator
            public /* bridge */ /* synthetic */ b createFromParcel(Parcel parcel) {
                return a(parcel);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object[]' to match base method */
            @Override // android.os.Parcelable.Creator
            public /* bridge */ /* synthetic */ b[] newArray(int i) {
                return b(i);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Parcel parcel) {
            super(parcel);
            Wg6.c(parcel, "in");
            this.b = parcel.readInt();
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Parcelable parcelable) {
            super(parcelable);
            Wg6.c(parcelable, "superState");
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final void b(int i) {
            this.b = i;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            Wg6.c(parcel, "out");
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ RulerValuePicker a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(RulerValuePicker rulerValuePicker) {
            this.a = rulerValuePicker;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            Wg6.c(animator, "animation");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            Wg6.c(animator, "animation");
            this.a.setMIsChangingValue$app_fossilRelease(false);
            if (this.a.getMListener$app_fossilRelease() != null) {
                K67 mListener$app_fossilRelease = this.a.getMListener$app_fossilRelease();
                if (mListener$app_fossilRelease != null) {
                    mListener$app_fossilRelease.c(false);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            Wg6.c(animator, "animation");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            Wg6.c(animator, "animation");
            if (this.a.getMListener$app_fossilRelease() != null) {
                K67 mListener$app_fossilRelease = this.a.getMListener$app_fossilRelease();
                if (mListener$app_fossilRelease != null) {
                    mListener$app_fossilRelease.c(true);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RulerValuePicker(Context context) {
        super(context);
        Wg6.c(context, "context");
        f(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RulerValuePicker(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        f(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RulerValuePicker(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Wg6.c(context, "context");
        f(attributeSet);
    }

    @DexIgnore
    @Override // com.mapped.Xz5.Bi
    public void a() {
        FLogger.INSTANCE.getLocal().d("RulerValuePicker", "onScrollStopped");
        RulerView rulerView = this.e;
        g(rulerView != null ? rulerView.getIndicatorIntervalWidth() : 0);
        int currentValue = getCurrentValue();
        TextView textView = this.k;
        if (textView != null) {
            textView.setText(d(currentValue));
            K67 k67 = this.g;
            if (k67 == null) {
                return;
            }
            if (k67 != null) {
                k67.a(currentValue);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void b() {
        Xz5 xz5 = new Xz5(getContext(), this);
        this.f = xz5;
        if (xz5 != null) {
            xz5.setHorizontalScrollBarEnabled(false);
            LinearLayout linearLayout = new LinearLayout(getContext());
            View view = new View(getContext());
            this.b = view;
            linearLayout.addView(view);
            Context context = getContext();
            Wg6.b(context, "context");
            RulerView rulerView = new RulerView(context);
            this.e = rulerView;
            linearLayout.addView(rulerView);
            View view2 = new View(getContext());
            this.d = view2;
            linearLayout.addView(view2);
            Xz5 xz52 = this.f;
            if (xz52 != null) {
                xz52.removeAllViews();
                Xz5 xz53 = this.f;
                if (xz53 != null) {
                    xz53.addView(linearLayout);
                    removeAllViews();
                    addView(this.f);
                    addView(this.k);
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void c() {
        Path path = this.i;
        if (path != null) {
            path.reset();
            float height = (float) getHeight();
            path.moveTo((float) (getWidth() / 2), height);
            path.lineTo((float) (getWidth() / 2), height * (((float) 1) - this.l));
        }
    }

    @DexIgnore
    public final String d(int i2) {
        a aVar = this.j;
        if (aVar == null) {
            return e(i2);
        }
        if (aVar != null) {
            return aVar.format(i2);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final String e(int i2) {
        Hr7 hr7 = Hr7.a;
        Locale locale = Locale.getDefault();
        Wg6.b(locale, "Locale.getDefault()");
        String format = String.format(locale, "%d", Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
        Wg6.b(format, "java.lang.String.format(locale, format, *args)");
        return format;
    }

    @DexIgnore
    public final void f(AttributeSet attributeSet) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        this.k = new TextView(getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        layoutParams.gravity = 49;
        TextView textView = this.k;
        if (textView != null) {
            textView.setLayoutParams(layoutParams);
            TextView textView2 = this.k;
            if (textView2 != null) {
                textView2.setGravity(17);
                TextView textView3 = this.k;
                if (textView3 != null) {
                    textView3.setBackground(getBackground());
                    b();
                    if (attributeSet != null) {
                        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, X24.RulerValuePicker);
                        try {
                            if (obtainStyledAttributes.hasValue(7)) {
                                str = obtainStyledAttributes.getString(7);
                                if (str == null) {
                                    str = "primaryText";
                                }
                                TextView textView4 = this.k;
                                if (textView4 != null) {
                                    textView4.setTextColor(this.m);
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            } else {
                                str = "";
                            }
                            if (obtainStyledAttributes.hasValue(10)) {
                                String string = obtainStyledAttributes.getString(10);
                                if (string == null) {
                                    string = "primaryText";
                                }
                                str2 = string;
                            } else {
                                str2 = "";
                            }
                            if (obtainStyledAttributes.hasValue(0)) {
                                String string2 = obtainStyledAttributes.getString(0);
                                if (string2 == null) {
                                    string2 = Explore.COLUMN_BACKGROUND;
                                }
                                str3 = string2;
                            } else {
                                str3 = "";
                            }
                            if (obtainStyledAttributes.hasValue(13)) {
                                String string3 = obtainStyledAttributes.getString(13);
                                if (string3 == null) {
                                    string3 = Explore.COLUMN_BACKGROUND;
                                }
                                str4 = string3;
                            } else {
                                str4 = "";
                            }
                            if (obtainStyledAttributes.hasValue(11)) {
                                setTextSize(obtainStyledAttributes.getDimensionPixelSize(11, 14));
                            }
                            if (obtainStyledAttributes.hasValue(9)) {
                                setSelectedTextSize((int) obtainStyledAttributes.getDimension(9, 14.0f));
                            }
                            if (obtainStyledAttributes.hasValue(12)) {
                                setTextStyle(obtainStyledAttributes.getInt(12, 0));
                            }
                            if (obtainStyledAttributes.hasValue(1)) {
                                String string4 = obtainStyledAttributes.getString(1);
                                if (string4 == null) {
                                    string4 = "secondaryText";
                                }
                                str5 = string4;
                            } else {
                                str5 = "";
                            }
                            if (obtainStyledAttributes.hasValue(3)) {
                                setIndicatorWidth(obtainStyledAttributes.getDimensionPixelSize(3, 4));
                            }
                            if (obtainStyledAttributes.hasValue(2)) {
                                setIndicatorIntervalDistance(obtainStyledAttributes.getDimensionPixelSize(2, 4));
                            }
                            if (obtainStyledAttributes.hasValue(4) || obtainStyledAttributes.hasValue(14)) {
                                float fraction = obtainStyledAttributes.getFraction(4, 1, 1, 0.6f);
                                this.l = fraction;
                                j(fraction, obtainStyledAttributes.getFraction(14, 1, 1, 0.4f));
                            }
                            if (obtainStyledAttributes.hasValue(6) || obtainStyledAttributes.hasValue(5)) {
                                k(obtainStyledAttributes.getInteger(6, 0), obtainStyledAttributes.getInteger(5, 100));
                            }
                            if (obtainStyledAttributes.hasValue(8)) {
                                str6 = obtainStyledAttributes.getString(8);
                                if (str6 == null) {
                                    str6 = "goalNumber";
                                }
                            } else {
                                str6 = "";
                            }
                            m(str2, str6, str, str5, str3, str4);
                        } catch (Exception e2) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            local.d("RulerValuePicker", "init - e=" + e2);
                        } catch (Throwable th) {
                            obtainStyledAttributes.recycle();
                            throw th;
                        }
                        obtainStyledAttributes.recycle();
                    }
                    this.h = new Paint(1);
                    h();
                    this.i = new Path();
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void g(int i2) {
        Xz5 xz5 = this.f;
        if (xz5 == null) {
            return;
        }
        if (xz5 != null) {
            int scrollX = xz5.getScrollX() % i2;
            if (scrollX < i2 / 2) {
                Xz5 xz52 = this.f;
                if (xz52 != null) {
                    xz52.scrollBy(-scrollX, 0);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Xz5 xz53 = this.f;
                if (xz53 != null) {
                    xz53.scrollBy(i2 - scrollX, 0);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final int getCurrentValue() {
        Xz5 xz5 = this.f;
        if (xz5 == null || this.e == null) {
            return 0;
        }
        if (xz5 != null) {
            int scrollX = xz5.getScrollX();
            RulerView rulerView = this.e;
            if (rulerView != null) {
                int indicatorIntervalWidth = scrollX / rulerView.getIndicatorIntervalWidth();
                RulerView rulerView2 = this.e;
                if (rulerView2 != null) {
                    int minValue = indicatorIntervalWidth + rulerView2.getMinValue();
                    RulerView rulerView3 = this.e;
                    if (rulerView3 == null) {
                        Wg6.i();
                        throw null;
                    } else if (minValue > rulerView3.getMaxValue()) {
                        RulerView rulerView4 = this.e;
                        if (rulerView4 != null) {
                            return rulerView4.getMaxValue();
                        }
                        Wg6.i();
                        throw null;
                    } else {
                        RulerView rulerView5 = this.e;
                        if (rulerView5 == null) {
                            Wg6.i();
                            throw null;
                        } else if (minValue >= rulerView5.getMinValue()) {
                            return minValue;
                        } else {
                            RulerView rulerView6 = this.e;
                            if (rulerView6 != null) {
                                return rulerView6.getMinValue();
                            }
                            Wg6.i();
                            throw null;
                        }
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final int getIndicatorColor() {
        RulerView rulerView = this.e;
        if (rulerView != null) {
            return rulerView.getIndicatorColor();
        }
        return 0;
    }

    @DexIgnore
    public final int getIndicatorIntervalWidth() {
        RulerView rulerView = this.e;
        if (rulerView != null) {
            return rulerView.getIndicatorIntervalWidth();
        }
        return 0;
    }

    @DexIgnore
    public final float getIndicatorWidth() {
        RulerView rulerView = this.e;
        return rulerView != null ? rulerView.getIndicatorWidth() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final float getLongIndicatorHeightRatio() {
        RulerView rulerView = this.e;
        return rulerView != null ? rulerView.getLongIndicatorHeightRatio() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final boolean getMIsChangingValue$app_fossilRelease() {
        return this.c;
    }

    @DexIgnore
    public final K67 getMListener$app_fossilRelease() {
        return this.g;
    }

    @DexIgnore
    public final int getMaxValue() {
        RulerView rulerView = this.e;
        if (rulerView != null) {
            return rulerView.getMaxValue();
        }
        return 0;
    }

    @DexIgnore
    public final int getMinValue() {
        RulerView rulerView = this.e;
        if (rulerView != null) {
            return rulerView.getMinValue();
        }
        return 0;
    }

    @DexIgnore
    public final int getNotchColor() {
        return this.m;
    }

    @DexIgnore
    public final float getShortIndicatorHeightRatio() {
        RulerView rulerView = this.e;
        return rulerView != null ? rulerView.getShortIndicatorHeightRatio() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final int getTextColor() {
        RulerView rulerView = this.e;
        if (rulerView != null) {
            return rulerView.getTextColor();
        }
        return 0;
    }

    @DexIgnore
    public final float getTextSize() {
        RulerView rulerView = this.e;
        return rulerView != null ? rulerView.getTextSize() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final Ai5 getUnit() {
        return this.s;
    }

    @DexIgnore
    public final void h() {
        Paint paint = this.h;
        if (paint != null) {
            paint.setColor(this.m);
            paint.setStrokeWidth(5.0f);
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
        }
    }

    @DexIgnore
    public final void i(int i2) {
        RulerView rulerView = this.e;
        int minValue = rulerView != null ? rulerView.getMinValue() : 0;
        RulerView rulerView2 = this.e;
        int maxValue = rulerView2 != null ? rulerView2.getMaxValue() : 0;
        int i3 = maxValue - minValue;
        int i4 = i2 < minValue ? 0 : i2 > maxValue ? i3 : i2 - minValue;
        Xz5 xz5 = this.f;
        int i5 = i3 / 2;
        RulerView rulerView3 = this.e;
        if (rulerView3 != null) {
            ObjectAnimator ofInt = ObjectAnimator.ofInt(xz5, "scrollX", i5, i4 * rulerView3.getIndicatorIntervalWidth());
            Wg6.b(ofInt, "animator");
            ofInt.setDuration(200L);
            ofInt.addListener(new c(this));
            ofInt.start();
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void j(float f2, float f3) {
        RulerView rulerView = this.e;
        if (rulerView != null) {
            rulerView.h(f2, f3);
        }
    }

    @DexIgnore
    public final void k(int i2, int i3) {
        RulerView rulerView = this.e;
        if (rulerView != null) {
            rulerView.i(i2, i3);
        }
        i(i2);
    }

    @DexIgnore
    public final void l(int i2, int i3, int i4) {
        if (!this.c) {
            this.c = true;
            RulerView rulerView = this.e;
            if (rulerView != null) {
                rulerView.i(i2, i3);
            }
            i(i4);
        }
    }

    @DexIgnore
    public final void m(String str, String str2, String str3, String str4, String str5, String str6) {
        RulerView rulerView;
        TextView textView;
        String d2 = ThemeManager.l.a().d(str);
        Typeface f2 = ThemeManager.l.a().f(str2);
        String d3 = ThemeManager.l.a().d(str4);
        String d4 = ThemeManager.l.a().d(str3);
        String d5 = ThemeManager.l.a().d(str5);
        String d6 = ThemeManager.l.a().d(str6);
        if (!(d5 == null || (textView = this.k) == null)) {
            textView.setBackgroundColor(Color.parseColor(d5));
        }
        if (!(d6 == null || (rulerView = this.e) == null)) {
            rulerView.setBackgroundColor(Color.parseColor(d6));
        }
        if (d2 != null) {
            setTextColor(Color.parseColor(d2));
        }
        if (f2 != null) {
            setTextTypeface(f2);
        }
        if (d3 != null) {
            setIndicatorColor(Color.parseColor(d3));
        }
        if (d4 != null) {
            setNotchColor(Color.parseColor(d4));
            TextView textView2 = this.k;
            if (textView2 != null) {
                textView2.setTextColor(this.m);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public void onDrawForeground(Canvas canvas) {
        Path path;
        Wg6.c(canvas, "canvas");
        super.onDrawForeground(canvas);
        Paint paint = this.h;
        if (paint != null && (path = this.i) != null) {
            if (path == null) {
                Wg6.i();
                throw null;
            } else if (paint != null) {
                canvas.drawPath(path, paint);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        ViewGroup.LayoutParams layoutParams = null;
        super.onLayout(z, i2, i3, i4, i5);
        if (z) {
            int width = getWidth();
            View view = this.b;
            ViewGroup.LayoutParams layoutParams2 = view != null ? view.getLayoutParams() : null;
            if (layoutParams2 != null) {
                layoutParams2.width = width / 2;
            }
            View view2 = this.b;
            if (view2 != null) {
                view2.setLayoutParams(layoutParams2);
            }
            View view3 = this.d;
            if (view3 != null) {
                layoutParams = view3.getLayoutParams();
            }
            if (layoutParams != null) {
                layoutParams.width = width / 2;
            }
            View view4 = this.d;
            if (view4 != null) {
                view4.setLayoutParams(layoutParams);
            }
            c();
            invalidate();
        }
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        Wg6.c(parcelable, "state");
        b bVar = (b) parcelable;
        super.onRestoreInstanceState(bVar.getSuperState());
        i(bVar.a());
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (onSaveInstanceState == null) {
            return null;
        }
        b bVar = new b(onSaveInstanceState);
        bVar.b(getCurrentValue());
        return bVar;
    }

    @DexIgnore
    @Override // com.mapped.Xz5.Bi
    public void onScrollChanged() {
        FLogger.INSTANCE.getLocal().d("RulerValuePicker", "onScrollChanged");
        int currentValue = getCurrentValue();
        TextView textView = this.k;
        if (textView != null) {
            textView.setText(d(currentValue));
            K67 k67 = this.g;
            if (k67 == null) {
                return;
            }
            if (k67 != null) {
                k67.b(currentValue);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void setFormatter(a aVar) {
        Wg6.c(aVar, "formatter");
        if (!Wg6.a(aVar, this.j)) {
            this.j = aVar;
            RulerView rulerView = this.e;
            if (rulerView == null) {
                return;
            }
            if (aVar != null) {
                rulerView.setFormatter(aVar);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void setIndicatorColor(int i2) {
        RulerView rulerView = this.e;
        if (rulerView != null) {
            rulerView.setIndicatorColor(i2);
        }
    }

    @DexIgnore
    public final void setIndicatorColorRes(int i2) {
        setIndicatorColor(W6.d(getContext(), i2));
    }

    @DexIgnore
    public final void setIndicatorIntervalDistance(int i2) {
        RulerView rulerView = this.e;
        if (rulerView != null) {
            rulerView.setIndicatorIntervalDistance(i2);
        }
    }

    @DexIgnore
    public final void setIndicatorWidth(int i2) {
        RulerView rulerView = this.e;
        if (rulerView != null) {
            rulerView.setIndicatorWidth(i2);
        }
    }

    @DexIgnore
    public final void setIndicatorWidthRes(int i2) {
        Context context = getContext();
        Wg6.b(context, "context");
        setIndicatorWidth(context.getResources().getDimensionPixelSize(i2));
    }

    @DexIgnore
    public final void setMIsChangingValue$app_fossilRelease(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public final void setMListener$app_fossilRelease(K67 k67) {
        this.g = k67;
    }

    @DexIgnore
    public final void setNotchColor(int i2) {
        this.m = i2;
        TextView textView = this.k;
        if (textView != null) {
            textView.setTextColor(i2);
            h();
            invalidate();
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void setNotchColorRes(int i2) {
        setNotchColor(W6.d(getContext(), i2));
    }

    @DexIgnore
    public final void setSelectedTextSize(int i2) {
        TextView textView = this.k;
        if (textView != null) {
            textView.setTextSize(0, (float) i2);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void setTextColor(int i2) {
        RulerView rulerView = this.e;
        if (rulerView != null) {
            rulerView.setTextColor(i2);
        }
    }

    @DexIgnore
    public final void setTextColorRes(int i2) {
        setTextColor(W6.d(getContext(), i2));
    }

    @DexIgnore
    public final void setTextSize(int i2) {
        RulerView rulerView = this.e;
        if (rulerView != null) {
            rulerView.setTextSize(i2);
        }
    }

    @DexIgnore
    public final void setTextStyle(int i2) {
        RulerView rulerView = this.e;
        if (rulerView != null) {
            rulerView.setTextStyle(i2);
        }
        TextView textView = this.k;
        if (textView == null) {
            Wg6.i();
            throw null;
        } else if (textView != null) {
            textView.setTypeface(Typeface.create(textView.getTypeface(), i2));
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void setTextTypeface(Typeface typeface) {
        if (typeface != null) {
            RulerView rulerView = this.e;
            if (rulerView != null) {
                rulerView.setTextTypeface(typeface);
            }
            TextView textView = this.k;
            if (textView != null) {
                textView.setTypeface(typeface);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void setUnit(Ai5 ai5) {
        Wg6.c(ai5, "<set-?>");
        this.s = ai5;
    }

    @DexIgnore
    public final void setValuePickerListener(K67 k67) {
        this.g = k67;
    }
}
