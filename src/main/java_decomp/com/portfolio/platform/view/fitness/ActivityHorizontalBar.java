package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Nl0;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityHorizontalBar extends BaseFitnessChart {
    @DexIgnore
    public float A;
    @DexIgnore
    public float B;
    @DexIgnore
    public float C;
    @DexIgnore
    public float D;
    @DexIgnore
    public /* final */ Paint E;
    @DexIgnore
    public /* final */ Paint F;
    @DexIgnore
    public /* final */ Paint G;
    @DexIgnore
    public /* final */ Paint H;
    @DexIgnore
    public /* final */ Paint I;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public int h;
    @DexIgnore
    public String i;
    @DexIgnore
    public int j;
    @DexIgnore
    public float k;
    @DexIgnore
    public float l;
    @DexIgnore
    public float m;
    @DexIgnore
    public float s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public Typeface y;
    @DexIgnore
    public RectF z;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityHorizontalBar(Context context) {
        this(context, null, 0);
        Wg6.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityHorizontalBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        Wg6.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityHorizontalBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        TypedArray obtainStyledAttributes;
        Wg6.c(context, "context");
        this.i = "";
        this.k = 5.0f;
        this.l = 8.0f;
        this.m = 8.0f;
        this.s = 1.0f;
        this.t = 8;
        this.u = -1;
        this.v = 10;
        this.x = true;
        this.z = new RectF();
        this.E = new Paint(1);
        this.F = new Paint(1);
        this.G = new Paint(1);
        this.H = new Paint(1);
        this.I = new Paint(1);
        if (!(attributeSet == null || (obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, X24.ActivityHorizontalBar, 0, 0)) == null)) {
            this.h = obtainStyledAttributes.getColor(0, 0);
            this.j = obtainStyledAttributes.getColor(2, 0);
            this.k = (float) obtainStyledAttributes.getDimensionPixelSize(4, 5);
            this.l = (float) obtainStyledAttributes.getDimensionPixelSize(5, 8);
            this.m = (float) obtainStyledAttributes.getDimensionPixelSize(3, 8);
            this.s = obtainStyledAttributes.getFloat(6, 1.0f);
            this.u = obtainStyledAttributes.getResourceId(7, -1);
            this.t = obtainStyledAttributes.getDimensionPixelSize(8, 8);
            String string = obtainStyledAttributes.getString(9);
            this.i = string == null ? "" : string;
            this.w = obtainStyledAttributes.getColor(10, 0);
            this.v = obtainStyledAttributes.getDimensionPixelSize(11, 10);
            try {
                this.y = Nl0.b(getContext(), obtainStyledAttributes.getResourceId(1, -1));
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ActivityHorizontalBar", "init - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        d();
    }

    @DexIgnore
    public final RectF c(RectF rectF, float f2) {
        float f3 = rectF.left + f2;
        float f4 = rectF.top + f2;
        float f5 = rectF.right - f2;
        float f6 = rectF.bottom - f2;
        float f7 = (float) 0;
        if (f5 <= f7 || f5 < f3) {
            f5 = f3 + f2;
        }
        if (f6 <= f7 || f6 < f4) {
            f6 = f4 + f2;
        }
        return new RectF(f3, f4, f5, f6);
    }

    @DexIgnore
    public final void d() {
        this.I.setAntiAlias(true);
        this.I.setStyle(Paint.Style.STROKE);
        this.E.setColor(this.j);
        this.E.setAntiAlias(true);
        this.E.setStyle(Paint.Style.FILL);
        this.G.setAlpha((int) (this.s * ((float) 255)));
        this.G.setColorFilter(new PorterDuffColorFilter(this.j, PorterDuff.Mode.SRC_IN));
        this.G.setAntiAlias(true);
        this.H.setColor(-1);
        this.H.setAntiAlias(true);
        this.H.setStyle(Paint.Style.FILL);
        this.F.setColor(this.w);
        this.F.setAntiAlias(true);
        this.F.setStyle(Paint.Style.FILL);
        this.F.setTextSize((float) this.v);
        Typeface typeface = this.y;
        if (typeface != null) {
            this.F.setTypeface(typeface);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return this.u;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.t;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        super.onDraw(canvas);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityHorizontalBar", "onDraw - mValue=" + this.f + ", mGoal=" + this.g + ", mMax=" + this.e);
        this.A = ((float) getWidth()) - this.m;
        if (canvas != null) {
            canvas.drawColor(this.h);
            RectF rectF = this.z;
            float f3 = this.e;
            float f4 = (float) 0;
            float f5 = f3 > f4 ? (this.A * this.f) / f3 : 0.0f;
            float f6 = this.l;
            float f7 = (float) 2;
            rectF.set(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f5 + (f6 * f7), f6 * ((float) 3));
            float f8 = this.f;
            if (f8 > f4) {
                if (f8 >= this.g) {
                    this.E.setAlpha((int) 15.299999999999999d);
                    RectF rectF2 = this.z;
                    float f9 = (float) 4;
                    float f10 = this.k;
                    canvas.drawRoundRect(rectF2, f9 * f10, f9 * f10, this.E);
                    this.E.setAlpha((int) 40.800000000000004d);
                    RectF c = c(this.z, this.l / f7);
                    float f11 = this.k;
                    canvas.drawRoundRect(c, f11 * 2.0f, f11 * 2.0f, this.E);
                }
                this.E.setAlpha(255);
                RectF c2 = c(this.z, this.l);
                float f12 = this.k;
                canvas.drawRoundRect(c2, f12, f12, this.E);
            }
            float height = ((float) getHeight()) / 2.0f;
            float f13 = this.e;
            if (f13 > f4) {
                f2 = (this.A * this.g) / f13;
            }
            float f14 = this.l;
            float f15 = f2 + f14;
            this.B = f15;
            if (this.f <= this.g) {
                canvas.drawBitmap(getStartBitmap(), this.B - ((float) getStartBitmap().getWidth()), height - ((float) (getStartBitmap().getHeight() / 2)), this.G);
            } else {
                canvas.drawCircle(f15 - (((float) this.t) / 2.0f), height, f14 / 4.0f, this.H);
            }
            if (this.x) {
                canvas.drawText(this.i, ((float) getWidth()) - this.C, height + (this.D / f7), this.F);
            }
        }
    }

    @DexIgnore
    public void onGlobalLayout() {
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode != Integer.MIN_VALUE) {
            if (mode == 0) {
                size = (int) (this.l * ((float) 3));
            } else if (mode != 1073741824) {
                size = 0;
            }
        }
        setMeasuredDimension(getWidth(), size);
    }
}
