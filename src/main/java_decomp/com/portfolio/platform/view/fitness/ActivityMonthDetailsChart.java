package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.K37;
import com.fossil.Nl0;
import com.fossil.Qq7;
import com.fossil.Wt7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityMonthDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String E;
    @DexIgnore
    public int A;
    @DexIgnore
    public /* final */ ArrayList<Lc6<Float, Float>> B;
    @DexIgnore
    public float C;
    @DexIgnore
    public float D;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ Paint l;
    @DexIgnore
    public /* final */ Paint m;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Qq7 implements Hg6<LinkedList<Integer>, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Canvas $canvas$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ int $chartHeight;
        @DexIgnore
        public /* final */ /* synthetic */ int $chartWidth;
        @DexIgnore
        public /* final */ /* synthetic */ ActivityMonthDetailsChart this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(int i, int i2, ActivityMonthDetailsChart activityMonthDetailsChart, Canvas canvas) {
            super(1);
            this.$chartWidth = i;
            this.$chartHeight = i2;
            this.this$0 = activityMonthDetailsChart;
            this.$canvas$inlined = canvas;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(LinkedList<Integer> linkedList) {
            invoke(linkedList);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(LinkedList<Integer> linkedList) {
            Wg6.c(linkedList, "barCenterXList");
            this.this$0.f(this.$canvas$inlined, linkedList);
            ActivityMonthDetailsChart activityMonthDetailsChart = this.this$0;
            Canvas canvas = this.$canvas$inlined;
            int i = this.$chartWidth;
            int i2 = this.$chartHeight;
            activityMonthDetailsChart.h(canvas, linkedList, i, i2, i2 - 4);
        }
    }

    /*
    static {
        String simpleName = ActivityMonthDetailsChart.class.getSimpleName();
        Wg6.b(simpleName, "ActivityMonthDetailsChart::class.java.simpleName");
        E = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityMonthDetailsChart(Context context) {
        this(context, null, 0);
        Wg6.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityMonthDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        Wg6.c(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityMonthDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Wg6.c(context, "context");
        this.l = new Paint(1);
        this.m = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = context.getResources().getDimensionPixelSize(2131165385);
        this.w = context.getResources().getDimensionPixelSize(2131165372);
        this.x = context.getResources().getDimensionPixelSize(2131165420);
        this.y = context.getResources().getDimensionPixelSize(2131165407);
        this.z = context.getResources().getDimensionPixelSize(2131165372);
        this.A = 4;
        this.B = new ArrayList<>();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, X24.ActivityMonthDetailsChart, 0, 0));
        }
        int d = W6.d(context, 2131099831);
        TypedArray mTypedArray = getMTypedArray();
        this.e = mTypedArray != null ? mTypedArray.getColor(3, d) : d;
        TypedArray mTypedArray2 = getMTypedArray();
        this.f = mTypedArray2 != null ? mTypedArray2.getColor(2, d) : d;
        TypedArray mTypedArray3 = getMTypedArray();
        this.g = mTypedArray3 != null ? mTypedArray3.getColor(1, d) : d;
        TypedArray mTypedArray4 = getMTypedArray();
        this.h = mTypedArray4 != null ? mTypedArray4.getColor(0, d) : d;
        TypedArray mTypedArray5 = getMTypedArray();
        this.i = mTypedArray5 != null ? mTypedArray5.getColor(4, d) : d;
        TypedArray mTypedArray6 = getMTypedArray();
        this.k = mTypedArray6 != null ? mTypedArray6.getDimensionPixelSize(6, 40) : 40;
        TypedArray mTypedArray7 = getMTypedArray();
        this.j = mTypedArray7 != null ? mTypedArray7.getResourceId(5, 2131296261) : 2131296261;
        TypedArray mTypedArray8 = getMTypedArray();
        if (mTypedArray8 != null) {
            mTypedArray8.recycle();
        }
    }

    @DexIgnore
    private final float getMChartMax() {
        return Math.max(getMMaxGoal(), getMMaxValues());
    }

    @DexIgnore
    private final float getMMaxGoal() {
        T t2;
        Float f2;
        Iterator<T> it = this.B.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (it.hasNext()) {
                float floatValue = ((Number) next.getSecond()).floatValue();
                while (true) {
                    next = it.next();
                    floatValue = ((Number) next.getSecond()).floatValue();
                    if (Float.compare(floatValue, floatValue) >= 0) {
                        floatValue = floatValue;
                        next = next;
                    }
                    if (!it.hasNext()) {
                        break;
                    }
                }
            }
            t2 = next;
        }
        T t3 = t2;
        return (t3 == null || (f2 = (Float) t3.getSecond()) == null) ? this.C : f2.floatValue();
    }

    @DexIgnore
    private final float getMMaxValues() {
        T t2;
        Float f2;
        Iterator<T> it = this.B.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            T next = it.next();
            if (it.hasNext()) {
                float floatValue = ((Number) next.getFirst()).floatValue();
                while (true) {
                    next = it.next();
                    floatValue = ((Number) next.getFirst()).floatValue();
                    if (Float.compare(floatValue, floatValue) >= 0) {
                        floatValue = floatValue;
                        next = next;
                    }
                    if (!it.hasNext()) {
                        break;
                    }
                }
            }
            t2 = next;
        }
        T t3 = t2;
        return (t3 == null || (f2 = (Float) t3.getFirst()) == null) ? this.D : f2.floatValue();
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            Rect rect = new Rect();
            this.s.getTextBounds("1", 0, Wt7.A("1") > 0 ? Wt7.A("1") : 1, rect);
            int height = (int) (((((float) getHeight()) - ((float) rect.height())) - ((float) this.x)) - ((float) this.y));
            int width = getWidth();
            int width2 = getStartBitmap().getWidth();
            int i2 = this.x;
            int i3 = this.z;
            int i4 = ((width - width2) - (i2 * 2)) - i3;
            e(canvas, i4, height, i3, new a(i4, height, this, canvas));
            g(canvas, 0, height);
        }
    }

    @DexIgnore
    public final void e(Canvas canvas, int i2, int i3, int i4, Hg6<? super LinkedList<Integer>, Cd6> hg6) {
        if (!this.B.isEmpty()) {
            float mChartMax = getMChartMax();
            int size = i2 / this.B.size();
            int i5 = this.w;
            int size2 = size < i5 ? i2 / this.B.size() : i5;
            int i6 = size2 / 2;
            int i7 = i6 / 3;
            LinkedList linkedList = new LinkedList();
            Iterator<Lc6<Float, Float>> it = this.B.iterator();
            while (it.hasNext()) {
                float f2 = (float) i3;
                RectF rectF = new RectF((float) i4, Math.max(f2 - ((it.next().component1().floatValue() / mChartMax) * f2), (float) this.A), (float) (i4 + i6), f2);
                linkedList.add(Integer.valueOf((int) (rectF.left + (rectF.width() / ((float) 2)))));
                i4 += size2;
                K37.d(canvas, rectF, this.t, (float) i7);
            }
            hg6.invoke(linkedList);
        }
    }

    @DexIgnore
    public final void f(Canvas canvas, List<Integer> list) {
        Integer[] numArr;
        Exception e2;
        int i2 = 0;
        if (!list.isEmpty()) {
            int size = list.size();
            switch (size) {
                case 28:
                    numArr = new Integer[]{0, 7, 14, 21, 27};
                    break;
                case 29:
                    numArr = new Integer[]{0, 7, 14, 21, 28};
                    break;
                case 30:
                    numArr = new Integer[]{0, 7, 14, 21, 29};
                    break;
                default:
                    numArr = new Integer[]{0, 7, 14, 21, 30};
                    break;
            }
            try {
                int i3 = 0;
                for (Integer num : numArr) {
                    try {
                        i3 = num.intValue();
                        i(canvas, String.valueOf(i3 + 1), list.get(i3).intValue());
                    } catch (Exception e3) {
                        e2 = e3;
                        i2 = i3;
                        FLogger.INSTANCE.getLocal().d(E, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e2);
                    }
                }
            } catch (Exception e4) {
                e2 = e4;
                FLogger.INSTANCE.getLocal().d(E, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e2);
            }
        }
    }

    @DexIgnore
    public final void g(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.l);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.l);
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.fitness.BaseFitnessChart
    public int getStarSizeInPx() {
        return this.v;
    }

    @DexIgnore
    public final void h(Canvas canvas, List<Integer> list, int i2, int i3, int i4) {
        if ((!this.B.isEmpty()) && this.B.size() > 1) {
            float mChartMax = getMChartMax();
            if (mChartMax > ((float) 0)) {
                Path path = new Path();
                float height = (float) getStartBitmap().getHeight();
                int size = list.size();
                for (int i5 = 1; i5 < size; i5++) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = E;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Previous goal: ");
                    int i6 = i5 - 1;
                    sb.append(this.B.get(i6).getSecond().floatValue());
                    sb.append(", current goal: ");
                    sb.append(this.B.get(i5).getSecond().floatValue());
                    sb.append(", chart max: ");
                    sb.append(mChartMax);
                    local.d(str, sb.toString());
                    float intValue = (float) list.get(i5).intValue();
                    float f2 = (float) i3;
                    float f3 = (float) i4;
                    height = Math.max(Math.min((1.0f - (this.B.get(i5).getSecond().floatValue() / mChartMax)) * f2, f3), (float) this.A);
                    float intValue2 = (float) list.get(i6).intValue();
                    float max = Math.max(Math.min((1.0f - (this.B.get(i6).getSecond().floatValue() / mChartMax)) * f2, f3), (float) this.A);
                    if (height == max) {
                        path.moveTo(intValue2, max);
                        if (i5 == list.size() - 1) {
                            path.lineTo((float) (this.x + i2), height);
                        } else {
                            path.lineTo(intValue, height);
                        }
                        canvas.drawPath(path, this.m);
                    } else {
                        path.moveTo(intValue2, max);
                        path.lineTo(intValue, max);
                        canvas.drawPath(path, this.m);
                        path.moveTo(intValue, max);
                        path.lineTo(intValue, height);
                        canvas.drawPath(path, this.m);
                        if (i5 == list.size() - 1) {
                            path.moveTo(intValue, height);
                            path.lineTo((float) (this.x + i2), height);
                            canvas.drawPath(path, this.m);
                        }
                    }
                }
                canvas.drawBitmap(getStartBitmap(), (float) (this.x + i2), height - ((float) (getStartBitmap().getHeight() / 2)), this.s);
            }
        }
    }

    @DexIgnore
    public final void i(Canvas canvas, String str, int i2) {
        canvas.drawText(str, ((float) i2) - (this.s.measureText(str, 0, str.length()) / ((float) 2)), (float) ((getHeight() - (new Rect().height() / 2)) - this.y), this.s);
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.l.setColor(this.e);
        float f2 = (float) 4;
        this.l.setStrokeWidth(f2);
        this.s.setColor(this.i);
        this.s.setStyle(Paint.Style.FILL);
        this.s.setTextSize((float) this.k);
        this.s.setTypeface(Nl0.b(getContext(), this.j));
        this.m.setColor(this.f);
        this.m.setStyle(Paint.Style.STROKE);
        this.m.setStrokeWidth(f2);
        this.m.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        this.t.setColor(this.g);
        this.t.setStrokeWidth((float) this.w);
        this.t.setStyle(Paint.Style.FILL);
        this.u.setColor(this.h);
        this.u.setStrokeWidth((float) this.w);
        this.u.setStyle(Paint.Style.FILL);
        this.A = getStartBitmap().getHeight() / 2;
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }
}
