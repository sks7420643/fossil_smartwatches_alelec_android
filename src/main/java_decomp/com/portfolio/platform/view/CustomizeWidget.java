package com.portfolio.platform.view;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.A57;
import com.fossil.Ac7;
import com.fossil.Cb7;
import com.fossil.O87;
import com.fossil.Tj5;
import com.fossil.Un0;
import com.fossil.Vj5;
import com.fossil.W67;
import com.fossil.Zb7;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.mapped.Cd6;
import com.mapped.Gh6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.manager.ThemeManager;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeWidget extends ConstraintLayout implements Zb7 {
    @DexIgnore
    public TextView A;
    @DexIgnore
    public float A0;
    @DexIgnore
    public TextView B;
    @DexIgnore
    public boolean B0;
    @DexIgnore
    public ProgressBar C;
    @DexIgnore
    public boolean C0;
    @DexIgnore
    public /* final */ int D;
    @DexIgnore
    public int D0;
    @DexIgnore
    public /* final */ int E;
    @DexIgnore
    public int E0;
    @DexIgnore
    public Integer F;
    @DexIgnore
    public boolean F0;
    @DexIgnore
    public Integer G;
    @DexIgnore
    public Integer H;
    @DexIgnore
    public int I;
    @DexIgnore
    public int J;
    @DexIgnore
    public int K;
    @DexIgnore
    public Drawable L;
    @DexIgnore
    public Drawable M;
    @DexIgnore
    public Drawable N;
    @DexIgnore
    public String O;
    @DexIgnore
    public String P;
    @DexIgnore
    public String Q;
    @DexIgnore
    public int R;
    @DexIgnore
    public int S;
    @DexIgnore
    public float T;
    @DexIgnore
    public float U;
    @DexIgnore
    public String V;
    @DexIgnore
    public int W;
    @DexIgnore
    public int a0;
    @DexIgnore
    public float b0;
    @DexIgnore
    public float c0;
    @DexIgnore
    public String d0;
    @DexIgnore
    public int e0;
    @DexIgnore
    public String f0;
    @DexIgnore
    public int g0;
    @DexIgnore
    public String h0;
    @DexIgnore
    public int i0;
    @DexIgnore
    public float j0;
    @DexIgnore
    public float k0;
    @DexIgnore
    public boolean l0;
    @DexIgnore
    public boolean m0;
    @DexIgnore
    public Drawable n0;
    @DexIgnore
    public /* final */ ColorDrawable o0;
    @DexIgnore
    public Ac7 p0;
    @DexIgnore
    public String q0;
    @DexIgnore
    public boolean r0;
    @DexIgnore
    public boolean s0;
    @DexIgnore
    public Cb7 t0;
    @DexIgnore
    public String u0;
    @DexIgnore
    public O87 v0;
    @DexIgnore
    public View w;
    @DexIgnore
    public float w0;
    @DexIgnore
    public View x;
    @DexIgnore
    public Rect x0;
    @DexIgnore
    public ImageView y;
    @DexIgnore
    public float y0;
    @DexIgnore
    public ImageView z;
    @DexIgnore
    public float z0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements View.OnTouchListener {
        @DexIgnore
        public float b;
        @DexIgnore
        public float c;
        @DexIgnore
        public float d;
        @DexIgnore
        public float e;
        @DexIgnore
        public long f;
        @DexIgnore
        public /* final */ int g; // = 10;
        @DexIgnore
        public /* final */ int h; // = 200;
        @DexIgnore
        public boolean i;

        @DexIgnore
        public a() {
            CustomizeWidget.this = r2;
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            Ac7 ac7;
            boolean z = false;
            if (!(view == null || motionEvent == null)) {
                int action = motionEvent.getAction();
                if (action == 0) {
                    CustomizeWidget.this.bringToFront();
                    CustomizeWidget.this.setElevation(10.0f);
                    Ac7 ac72 = CustomizeWidget.this.p0;
                    if (ac72 != null) {
                        z = ac72.p(CustomizeWidget.this);
                    }
                    this.i = z;
                    if (!z) {
                        this.b = CustomizeWidget.this.getX() - motionEvent.getRawX();
                        this.c = CustomizeWidget.this.getY() - motionEvent.getRawY();
                        this.d = CustomizeWidget.this.getX();
                        this.e = CustomizeWidget.this.getY();
                        this.f = System.currentTimeMillis();
                    }
                } else if (action == 1) {
                    CustomizeWidget.this.setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    if (this.i) {
                        return false;
                    }
                    Ac7 ac73 = CustomizeWidget.this.p0;
                    if (ac73 != null) {
                        ac73.o(CustomizeWidget.this, this.d, this.e);
                    }
                    boolean z2 = Math.abs(CustomizeWidget.this.getX() - this.d) + Math.abs(CustomizeWidget.this.getY() - this.e) < ((float) this.g);
                    if (System.currentTimeMillis() - this.f < ((long) this.h)) {
                        z = true;
                    }
                    if (z2 && z && CustomizeWidget.this.R()) {
                        CustomizeWidget.this.a0();
                        Ac7 ac74 = CustomizeWidget.this.p0;
                        if (ac74 != null) {
                            CustomizeWidget customizeWidget = CustomizeWidget.this;
                            ac74.h(customizeWidget, customizeWidget.getContentTheme());
                        }
                    }
                } else if (action != 2 || this.i) {
                    return false;
                } else {
                    CustomizeWidget.this.animate().x(motionEvent.getRawX() + this.b).y(motionEvent.getRawY() + this.c).setDuration(0).start();
                    if (Math.abs(CustomizeWidget.this.getX() - this.d) + Math.abs(CustomizeWidget.this.getY() - this.e) > ((float) this.g) && (ac7 = CustomizeWidget.this.p0) != null) {
                        ac7.i(CustomizeWidget.this);
                    }
                    if (System.currentTimeMillis() - this.f < ((long) (this.h + 100))) {
                        return true;
                    }
                }
            }
            return true;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(CustomizeWidget customizeWidget);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Un0.Ci {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeWidget a;
        @DexIgnore
        public /* final */ /* synthetic */ b b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends View.DragShadowBuilder {
            @DexIgnore
            public /* final */ /* synthetic */ c a;
            @DexIgnore
            public /* final */ /* synthetic */ Gh6 b;
            @DexIgnore
            public /* final */ /* synthetic */ Intent c;
            @DexIgnore
            public /* final */ /* synthetic */ Un0 d;
            @DexIgnore
            public /* final */ /* synthetic */ View e;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Gh6 gh6, Intent intent, Un0 un0, View view, View view2) {
                super(view2);
                this.a = cVar;
                this.b = gh6;
                this.c = intent;
                this.d = un0;
                this.e = view;
            }

            @DexIgnore
            public void onDrawShadow(Canvas canvas) {
                Wg6.c(canvas, "canvas");
                View view = getView();
                if (view != null) {
                    view.draw(canvas);
                }
                this.a.a.setDragMode(true);
            }

            @DexIgnore
            public void onProvideShadowMetrics(Point point, Point point2) {
                Wg6.c(point, "shadowSize");
                Wg6.c(point2, "shadowTouchPoint");
                super.onProvideShadowMetrics(point, point2);
                Rect watchFaceRect = this.a.a.getWatchFaceRect();
                if (watchFaceRect != null) {
                    float width = ((float) watchFaceRect.width()) * 0.31666666f;
                    this.b.element = width / (((float) this.a.a.getWidth()) - CustomizeWidget.G(this.a.a).getX());
                    this.c.putExtra("complication_radius", (int) (width / ((float) 2)));
                    Point point3 = new Point();
                    this.d.b(point3);
                    int i = point3.x;
                    View view = getView();
                    Wg6.b(view, "view");
                    float f = this.b.element;
                    float width2 = (float) ((int) ((width - (((float) this.a.a.getWidth()) - CustomizeWidget.G(this.a.a).getX())) / 2.0f));
                    int i2 = point3.y;
                    View view2 = getView();
                    Wg6.b(view2, "view");
                    float f2 = this.b.element;
                    point3.x = (int) ((((float) (i - (view.getWidth() / 2))) * f) - width2);
                    point3.y = (int) ((((float) (i2 - (view2.getWidth() / 2))) * f2) - width2);
                    this.c.putExtra("complication_point", point3);
                }
                Un0 un0 = this.d;
                if (un0 != null) {
                    un0.b(point2);
                }
            }
        }

        @DexIgnore
        public c(CustomizeWidget customizeWidget, b bVar, String str) {
            this.a = customizeWidget;
            this.b = bVar;
            this.c = str;
        }

        @DexIgnore
        @Override // com.fossil.Un0.Ci
        public final boolean a(View view, Un0 un0) {
            Gh6 gh6 = new Gh6();
            gh6.element = 1.0f;
            b bVar = this.b;
            if (bVar != null) {
                bVar.a(this.a);
            }
            Intent intent = new Intent();
            intent.putExtra("complication_id", this.a.getComplicationId());
            a aVar = new a(this, gh6, intent, un0, view, view);
            ClipData clipData = new ClipData(new ClipDescription(this.c, new String[]{"text/plain"}), new ClipData.Item(intent));
            return Build.VERSION.SDK_INT >= 24 ? this.a.startDragAndDrop(clipData, aVar, null, 257) : this.a.startDrag(clipData, aVar, null, 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Un0.Ci {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeWidget a;
        @DexIgnore
        public /* final */ /* synthetic */ b b;
        @DexIgnore
        public /* final */ /* synthetic */ float c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ Intent e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends View.DragShadowBuilder {
            @DexIgnore
            public /* final */ /* synthetic */ d a;
            @DexIgnore
            public /* final */ /* synthetic */ Un0 b;
            @DexIgnore
            public /* final */ /* synthetic */ View c;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, Un0 un0, View view, View view2) {
                super(view2);
                this.a = dVar;
                this.b = un0;
                this.c = view;
            }

            @DexIgnore
            public void onDrawShadow(Canvas canvas) {
                Wg6.c(canvas, "canvas");
                float f = this.a.c;
                canvas.scale(f, f);
                View view = getView();
                if (view != null) {
                    view.draw(canvas);
                }
                this.a.a.setDragMode(true);
            }

            @DexIgnore
            public void onProvideShadowMetrics(Point point, Point point2) {
                Wg6.c(point, "shadowSize");
                Wg6.c(point2, "shadowTouchPoint");
                super.onProvideShadowMetrics(point, point2);
                View view = getView();
                Wg6.b(view, "view");
                int width = (int) (((float) view.getWidth()) * this.a.c);
                View view2 = getView();
                Wg6.b(view2, "view");
                int height = (int) (((float) view2.getHeight()) * this.a.c);
                point.set(width, height);
                point2.set(width / 2, height / 2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WidgetControl", "onDragStart - shadowTouchPoint=" + point2);
                Un0 un0 = this.b;
                if (un0 != null) {
                    un0.b(point2);
                }
            }
        }

        @DexIgnore
        public d(CustomizeWidget customizeWidget, b bVar, float f, String str, Intent intent) {
            this.a = customizeWidget;
            this.b = bVar;
            this.c = f;
            this.d = str;
            this.e = intent;
        }

        @DexIgnore
        @Override // com.fossil.Un0.Ci
        public final boolean a(View view, Un0 un0) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onDragStart - v=");
            sb.append(view != null ? Integer.valueOf(view.getId()) : null);
            sb.append(", helper=");
            sb.append(un0);
            local.d("WidgetControl", sb.toString());
            b bVar = this.b;
            if (bVar != null) {
                bVar.a(this.a);
            }
            a aVar = new a(this, un0, view, view);
            ClipData clipData = new ClipData(new ClipDescription(this.d, new String[]{"text/plain"}), new ClipData.Item(this.e));
            FLogger.INSTANCE.getLocal().d("WidgetControl", "onDragStart - created ClipDescription");
            return Build.VERSION.SDK_INT >= 24 ? this.a.startDragAndDrop(clipData, aVar, null, 257) : this.a.startDrag(clipData, aVar, null, 257);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomizeWidget(Context context, Rect rect, float f, float f2, float f3, AttributeSet attributeSet) {
        super(context, attributeSet);
        View view = null;
        this.D = Color.parseColor("#00000000");
        int parseColor = Color.parseColor("#FFFFFF");
        this.E = parseColor;
        this.I = parseColor;
        this.J = parseColor;
        this.K = parseColor;
        this.R = parseColor;
        this.S = parseColor;
        this.T = -1.0f;
        this.U = -1.0f;
        this.W = parseColor;
        this.a0 = parseColor;
        this.b0 = -1.0f;
        this.c0 = -1.0f;
        this.e0 = parseColor;
        this.f0 = "";
        this.g0 = parseColor;
        this.h0 = "";
        this.i0 = parseColor;
        this.j0 = -1.0f;
        this.k0 = -1.0f;
        this.o0 = new ColorDrawable(W6.d(getContext(), R.color.transparent));
        this.u0 = "empty";
        this.v0 = O87.WHITE;
        this.w0 = 1.0f;
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        view = layoutInflater != null ? layoutInflater.inflate(2131558847, (ViewGroup) this, true) : view;
        if (view != null) {
            this.x0 = rect;
            this.y0 = f;
            this.z0 = f2;
            this.A0 = f3;
            P(view, attributeSet);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CustomizeWidget(Context context, Rect rect, float f, float f2, float f3, AttributeSet attributeSet, int i, Qg6 qg6) {
        this(context, (i & 2) != 0 ? null : rect, f, f2, f3, (i & 32) == 0 ? attributeSet : null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomizeWidget(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        View view = null;
        this.D = Color.parseColor("#00000000");
        int parseColor = Color.parseColor("#FFFFFF");
        this.E = parseColor;
        this.I = parseColor;
        this.J = parseColor;
        this.K = parseColor;
        this.R = parseColor;
        this.S = parseColor;
        this.T = -1.0f;
        this.U = -1.0f;
        this.W = parseColor;
        this.a0 = parseColor;
        this.b0 = -1.0f;
        this.c0 = -1.0f;
        this.e0 = parseColor;
        this.f0 = "";
        this.g0 = parseColor;
        this.h0 = "";
        this.i0 = parseColor;
        this.j0 = -1.0f;
        this.k0 = -1.0f;
        this.o0 = new ColorDrawable(W6.d(getContext(), R.color.transparent));
        this.u0 = "empty";
        this.v0 = O87.WHITE;
        this.w0 = 1.0f;
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        view = layoutInflater != null ? layoutInflater.inflate(2131558846, (ViewGroup) this, true) : view;
        if (view != null) {
            P(view, attributeSet);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    public static final /* synthetic */ View G(CustomizeWidget customizeWidget) {
        View view = customizeWidget.w;
        if (view != null) {
            return view;
        }
        Wg6.n("clRoot");
        throw null;
    }

    @DexIgnore
    public static /* synthetic */ Un0 X(CustomizeWidget customizeWidget, String str, Intent intent, View.OnDragListener onDragListener, b bVar, int i, Object obj) {
        if ((i & 4) != 0) {
            onDragListener = null;
        }
        if ((i & 8) != 0) {
            bVar = null;
        }
        return customizeWidget.W(str, intent, onDragListener, bVar);
    }

    @DexIgnore
    private final void setTopIconSrc(Drawable drawable) {
        this.M = drawable;
        if (drawable != null) {
            this.M = drawable;
            ImageView imageView = this.z;
            if (imageView != null) {
                imageView.setImageDrawable(drawable);
                if (!this.l0) {
                    ImageView imageView2 = this.z;
                    if (imageView2 != null) {
                        imageView2.setImageDrawable(this.M);
                        ImageView imageView3 = this.z;
                        if (imageView3 != null) {
                            imageView3.setImageTintList(ColorStateList.valueOf(this.e0));
                            this.i0 = this.e0;
                            return;
                        }
                        Wg6.n("ivTop");
                        throw null;
                    }
                    Wg6.n("ivTop");
                    throw null;
                }
                return;
            }
            Wg6.n("ivTop");
            throw null;
        }
    }

    @DexIgnore
    public final void I() {
        this.F0 = true;
        ProgressBar progressBar = this.C;
        if (progressBar != null) {
            progressBar.setProgress(100);
        } else {
            Wg6.n("pbProgress");
            throw null;
        }
    }

    @DexIgnore
    public final void J() {
        if (this.F0) {
            this.F0 = false;
            ProgressBar progressBar = this.C;
            if (progressBar != null) {
                progressBar.setProgress(0);
            } else {
                Wg6.n("pbProgress");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void K() {
        if (!TextUtils.isEmpty(this.Q)) {
            TextView textView = this.A;
            if (textView != null) {
                textView.setText(this.Q);
            } else {
                Wg6.n("tvTop");
                throw null;
            }
        } else {
            TextView textView2 = this.A;
            if (textView2 != null) {
                textView2.setVisibility(4);
            } else {
                Wg6.n("tvTop");
                throw null;
            }
        }
        if (this.m0) {
            ImageView imageView = this.z;
            if (imageView != null) {
                imageView.setVisibility(0);
                ImageView imageView2 = this.z;
                if (imageView2 != null) {
                    Drawable drawable = this.L;
                    if (drawable == null) {
                        drawable = this.o0;
                    }
                    imageView2.setImageDrawable(drawable);
                    TextView textView3 = this.A;
                    if (textView3 != null) {
                        textView3.setVisibility(4);
                        if (!TextUtils.isEmpty(this.d0)) {
                            TextView textView4 = this.B;
                            if (textView4 != null) {
                                textView4.setText(this.d0);
                                TextView textView5 = this.B;
                                if (textView5 != null) {
                                    textView5.setVisibility(0);
                                } else {
                                    Wg6.n("tvBottom");
                                    throw null;
                                }
                            } else {
                                Wg6.n("tvBottom");
                                throw null;
                            }
                        } else {
                            TextView textView6 = this.B;
                            if (textView6 != null) {
                                textView6.setVisibility(8);
                            } else {
                                Wg6.n("tvBottom");
                                throw null;
                            }
                        }
                    } else {
                        Wg6.n("tvTop");
                        throw null;
                    }
                } else {
                    Wg6.n("ivTop");
                    throw null;
                }
            } else {
                Wg6.n("ivTop");
                throw null;
            }
        } else if (this.l0) {
            String str = this.P;
            if (str == null) {
                str = this.O;
            }
            if (!TextUtils.isEmpty(str)) {
                ImageView imageView3 = this.z;
                if (imageView3 != null) {
                    Vj5<Drawable> U0 = Tj5.b(imageView3).J(str).U0();
                    ImageView imageView4 = this.z;
                    if (imageView4 != null) {
                        Wg6.b(U0.F0(imageView4), "GlideApp.with(ivTop)\n   \u2026             .into(ivTop)");
                    } else {
                        Wg6.n("ivTop");
                        throw null;
                    }
                } else {
                    Wg6.n("ivTop");
                    throw null;
                }
            } else {
                Drawable drawable2 = this.N;
                if (drawable2 == null) {
                    drawable2 = this.M;
                }
                if (drawable2 != null) {
                    ImageView imageView5 = this.z;
                    if (imageView5 != null) {
                        imageView5.setImageDrawable(drawable2);
                        ImageView imageView6 = this.z;
                        if (imageView6 != null) {
                            imageView6.setVisibility(0);
                            TextView textView7 = this.A;
                            if (textView7 != null) {
                                textView7.setVisibility(4);
                            } else {
                                Wg6.n("tvTop");
                                throw null;
                            }
                        } else {
                            Wg6.n("ivTop");
                            throw null;
                        }
                    } else {
                        Wg6.n("ivTop");
                        throw null;
                    }
                } else {
                    ImageView imageView7 = this.z;
                    if (imageView7 != null) {
                        imageView7.setImageDrawable(this.o0);
                        ImageView imageView8 = this.z;
                        if (imageView8 != null) {
                            imageView8.setVisibility(4);
                            TextView textView8 = this.A;
                            if (textView8 != null) {
                                textView8.setVisibility(0);
                            } else {
                                Wg6.n("tvTop");
                                throw null;
                            }
                        } else {
                            Wg6.n("ivTop");
                            throw null;
                        }
                    } else {
                        Wg6.n("ivTop");
                        throw null;
                    }
                }
            }
            if (!TextUtils.isEmpty(this.V)) {
                TextView textView9 = this.B;
                if (textView9 != null) {
                    textView9.setText(this.V);
                    TextView textView10 = this.B;
                    if (textView10 != null) {
                        textView10.setVisibility(0);
                    } else {
                        Wg6.n("tvBottom");
                        throw null;
                    }
                } else {
                    Wg6.n("tvBottom");
                    throw null;
                }
            } else {
                TextView textView11 = this.B;
                if (textView11 != null) {
                    textView11.setVisibility(8);
                } else {
                    Wg6.n("tvBottom");
                    throw null;
                }
            }
        } else {
            if (!TextUtils.isEmpty(this.O)) {
                ImageView imageView9 = this.z;
                if (imageView9 != null) {
                    Vj5<Drawable> U02 = Tj5.b(imageView9).J(this.O).U0();
                    ImageView imageView10 = this.z;
                    if (imageView10 != null) {
                        Wg6.b(U02.F0(imageView10), "GlideApp.with(ivTop)\n   \u2026             .into(ivTop)");
                    } else {
                        Wg6.n("ivTop");
                        throw null;
                    }
                } else {
                    Wg6.n("ivTop");
                    throw null;
                }
            } else {
                Drawable drawable3 = this.M;
                if (drawable3 != null) {
                    ImageView imageView11 = this.z;
                    if (imageView11 != null) {
                        imageView11.setImageDrawable(drawable3);
                        ImageView imageView12 = this.z;
                        if (imageView12 != null) {
                            imageView12.setVisibility(0);
                            TextView textView12 = this.A;
                            if (textView12 != null) {
                                textView12.setVisibility(4);
                            } else {
                                Wg6.n("tvTop");
                                throw null;
                            }
                        } else {
                            Wg6.n("ivTop");
                            throw null;
                        }
                    } else {
                        Wg6.n("ivTop");
                        throw null;
                    }
                } else {
                    ImageView imageView13 = this.z;
                    if (imageView13 != null) {
                        imageView13.setImageDrawable(this.o0);
                        ImageView imageView14 = this.z;
                        if (imageView14 != null) {
                            imageView14.setVisibility(4);
                            TextView textView13 = this.A;
                            if (textView13 != null) {
                                textView13.setVisibility(0);
                            } else {
                                Wg6.n("tvTop");
                                throw null;
                            }
                        } else {
                            Wg6.n("ivTop");
                            throw null;
                        }
                    } else {
                        Wg6.n("ivTop");
                        throw null;
                    }
                }
            }
            if (!TextUtils.isEmpty(this.V)) {
                TextView textView14 = this.B;
                if (textView14 != null) {
                    textView14.setText(this.V);
                    TextView textView15 = this.B;
                    if (textView15 != null) {
                        textView15.setVisibility(0);
                    } else {
                        Wg6.n("tvBottom");
                        throw null;
                    }
                } else {
                    Wg6.n("tvBottom");
                    throw null;
                }
            } else {
                TextView textView16 = this.B;
                if (textView16 != null) {
                    textView16.setVisibility(8);
                } else {
                    Wg6.n("tvBottom");
                    throw null;
                }
            }
        }
        ImageView imageView15 = this.z;
        if (imageView15 != null) {
            imageView15.setVisibility(0);
            if (this.l0) {
                this.H = this.G;
                this.K = this.J;
                View view = this.w;
                if (view != null) {
                    view.getBackground().clearColorFilter();
                    if (this.J != this.E) {
                        View view2 = this.w;
                        if (view2 != null) {
                            view2.getBackground().mutate().setColorFilter(this.J, PorterDuff.Mode.SRC_IN);
                        } else {
                            Wg6.n("clRoot");
                            throw null;
                        }
                    } else {
                        Drawable drawable4 = this.n0;
                        if (drawable4 != null) {
                            View view3 = this.w;
                            if (view3 != null) {
                                view3.setBackground(drawable4);
                                Cd6 cd6 = Cd6.a;
                            } else {
                                Wg6.n("clRoot");
                                throw null;
                            }
                        } else {
                            View view4 = this.w;
                            if (view4 != null) {
                                Context context = getContext();
                                Integer num = this.H;
                                if (num != null) {
                                    view4.setBackground(W6.f(context, num.intValue()));
                                    Cd6 cd62 = Cd6.a;
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            } else {
                                Wg6.n("clRoot");
                                throw null;
                            }
                        }
                    }
                    ImageView imageView16 = this.z;
                    if (imageView16 != null) {
                        imageView16.setImageTintList(ColorStateList.valueOf(this.g0));
                        this.i0 = this.g0;
                        TextView textView17 = this.A;
                        if (textView17 != null) {
                            textView17.setTextColor(this.S);
                            TextView textView18 = this.B;
                            if (textView18 != null) {
                                textView18.setTextColor(this.a0);
                                float f = this.U;
                                if (f != -1.0f) {
                                    TextView textView19 = this.A;
                                    if (textView19 != null) {
                                        textView19.setTextSize(f);
                                    } else {
                                        Wg6.n("tvTop");
                                        throw null;
                                    }
                                } else {
                                    float f2 = this.T;
                                    if (f2 != -1.0f) {
                                        TextView textView20 = this.A;
                                        if (textView20 != null) {
                                            textView20.setTextSize(f2);
                                        } else {
                                            Wg6.n("tvTop");
                                            throw null;
                                        }
                                    } else {
                                        float f3 = this.k0;
                                        if (f3 != -1.0f) {
                                            TextView textView21 = this.A;
                                            if (textView21 != null) {
                                                textView21.setTextSize(f3);
                                            } else {
                                                Wg6.n("tvTop");
                                                throw null;
                                            }
                                        } else {
                                            float f4 = this.j0;
                                            if (f4 != -1.0f) {
                                                TextView textView22 = this.A;
                                                if (textView22 != null) {
                                                    textView22.setTextSize(f4);
                                                } else {
                                                    Wg6.n("tvTop");
                                                    throw null;
                                                }
                                            }
                                        }
                                    }
                                }
                                float f5 = this.c0;
                                if (f5 != -1.0f) {
                                    TextView textView23 = this.B;
                                    if (textView23 != null) {
                                        textView23.setTextSize(f5);
                                    } else {
                                        Wg6.n("tvBottom");
                                        throw null;
                                    }
                                } else {
                                    float f6 = this.b0;
                                    if (f6 != -1.0f) {
                                        TextView textView24 = this.B;
                                        if (textView24 != null) {
                                            textView24.setTextSize(f6);
                                        } else {
                                            Wg6.n("tvBottom");
                                            throw null;
                                        }
                                    } else {
                                        float f7 = this.k0;
                                        if (f7 != -1.0f) {
                                            TextView textView25 = this.B;
                                            if (textView25 != null) {
                                                textView25.setTextSize(f7);
                                            } else {
                                                Wg6.n("tvBottom");
                                                throw null;
                                            }
                                        } else {
                                            float f8 = this.j0;
                                            if (f8 != -1.0f) {
                                                TextView textView26 = this.B;
                                                if (textView26 != null) {
                                                    textView26.setTextSize(f8);
                                                } else {
                                                    Wg6.n("tvBottom");
                                                    throw null;
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                Wg6.n("tvBottom");
                                throw null;
                            }
                        } else {
                            Wg6.n("tvTop");
                            throw null;
                        }
                    } else {
                        Wg6.n("ivTop");
                        throw null;
                    }
                } else {
                    Wg6.n("clRoot");
                    throw null;
                }
            } else {
                this.H = this.F;
                this.K = this.I;
                View view5 = this.w;
                if (view5 != null) {
                    view5.getBackground().clearColorFilter();
                    if (this.I != this.E) {
                        View view6 = this.w;
                        if (view6 != null) {
                            view6.getBackground().mutate().setColorFilter(this.I, PorterDuff.Mode.SRC_IN);
                        } else {
                            Wg6.n("clRoot");
                            throw null;
                        }
                    } else {
                        Drawable drawable5 = this.n0;
                        if (drawable5 != null) {
                            View view7 = this.w;
                            if (view7 != null) {
                                view7.setBackground(drawable5);
                                Cd6 cd63 = Cd6.a;
                            } else {
                                Wg6.n("clRoot");
                                throw null;
                            }
                        } else {
                            View view8 = this.w;
                            if (view8 != null) {
                                Context context2 = getContext();
                                Integer num2 = this.H;
                                if (num2 != null) {
                                    view8.setBackground(W6.f(context2, num2.intValue()));
                                    Cd6 cd64 = Cd6.a;
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            } else {
                                Wg6.n("clRoot");
                                throw null;
                            }
                        }
                    }
                    ImageView imageView17 = this.z;
                    if (imageView17 != null) {
                        imageView17.setImageTintList(ColorStateList.valueOf(this.e0));
                        this.i0 = this.e0;
                        TextView textView27 = this.A;
                        if (textView27 != null) {
                            textView27.setTextColor(this.R);
                            TextView textView28 = this.B;
                            if (textView28 != null) {
                                textView28.setTextColor(this.W);
                                float f9 = this.T;
                                if (f9 != -1.0f) {
                                    TextView textView29 = this.A;
                                    if (textView29 != null) {
                                        textView29.setTextSize(f9);
                                    } else {
                                        Wg6.n("tvTop");
                                        throw null;
                                    }
                                } else {
                                    float f10 = this.j0;
                                    if (f10 != -1.0f) {
                                        TextView textView30 = this.A;
                                        if (textView30 != null) {
                                            textView30.setTextSize(f10);
                                        } else {
                                            Wg6.n("tvTop");
                                            throw null;
                                        }
                                    }
                                }
                                float f11 = this.b0;
                                if (f11 != -1.0f) {
                                    TextView textView31 = this.B;
                                    if (textView31 != null) {
                                        textView31.setTextSize(f11);
                                    } else {
                                        Wg6.n("tvBottom");
                                        throw null;
                                    }
                                } else {
                                    float f12 = this.j0;
                                    if (f12 != -1.0f) {
                                        TextView textView32 = this.B;
                                        if (textView32 != null) {
                                            textView32.setTextSize(f12);
                                        } else {
                                            Wg6.n("tvBottom");
                                            throw null;
                                        }
                                    }
                                }
                            } else {
                                Wg6.n("tvBottom");
                                throw null;
                            }
                        } else {
                            Wg6.n("tvTop");
                            throw null;
                        }
                    } else {
                        Wg6.n("ivTop");
                        throw null;
                    }
                } else {
                    Wg6.n("clRoot");
                    throw null;
                }
            }
        } else {
            Wg6.n("ivTop");
            throw null;
        }
    }

    @DexIgnore
    public final void L(int i) {
        this.e0 = i;
        this.R = i;
        this.W = i;
        ImageView imageView = this.z;
        if (imageView != null) {
            imageView.setImageTintList(ColorStateList.valueOf(i));
            TextView textView = this.A;
            if (textView != null) {
                textView.setTextColor(this.R);
                TextView textView2 = this.B;
                if (textView2 != null) {
                    textView2.setTextColor(this.W);
                } else {
                    Wg6.n("tvBottom");
                    throw null;
                }
            } else {
                Wg6.n("tvTop");
                throw null;
            }
        } else {
            Wg6.n("ivTop");
            throw null;
        }
    }

    @DexIgnore
    public final void M(String str, b bVar) {
        Wg6.c(str, "label");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WidgetControl", "attachDragEvent - label = " + str);
        new Un0(this, new c(this, bVar, str)).a();
    }

    @DexIgnore
    public final void N() {
        this.I = this.D;
        View view = this.w;
        if (view != null) {
            view.getBackground().mutate().setColorFilter(this.I, PorterDuff.Mode.SRC_IN);
        } else {
            Wg6.n("clRoot");
            throw null;
        }
    }

    @DexIgnore
    public final void O(String str) {
        Wg6.c(str, "complicationId");
        this.u0 = str;
        switch (str.hashCode()) {
            case -829740640:
                if (str.equals("commute-time")) {
                    setTopIconSrc(W6.f(getContext(), 2131231133));
                    return;
                }
                break;
            case -331239923:
                if (str.equals(Constants.BATTERY)) {
                    setTopIconSrc(W6.f(getContext(), 2131231129));
                    return;
                }
                break;
            case -168965370:
                if (str.equals("calories")) {
                    setTopIconSrc(W6.f(getContext(), 2131231131));
                    return;
                }
                break;
            case -85386984:
                if (str.equals("active-minutes")) {
                    setTopIconSrc(W6.f(getContext(), 2131231144));
                    return;
                }
                break;
            case -48173007:
                if (str.equals("chance-of-rain")) {
                    setTopIconSrc(W6.f(getContext(), 2131231138));
                    return;
                }
                break;
            case 3076014:
                if (str.equals("date")) {
                    setTopIconSrc(W6.f(getContext(), 2131231130));
                    return;
                }
                break;
            case 96634189:
                if (str.equals("empty")) {
                    setTopIconSrc(W6.f(getContext(), 2131231132));
                    return;
                }
                break;
            case 109761319:
                if (str.equals("steps")) {
                    setTopIconSrc(W6.f(getContext(), 2131231139));
                    return;
                }
                break;
            case 134170930:
                if (str.equals("second-timezone")) {
                    setTopIconSrc(W6.f(getContext(), 2131231141));
                    return;
                }
                break;
            case 1223440372:
                if (str.equals("weather")) {
                    setTopIconSrc(W6.f(getContext(), 2131231142));
                    return;
                }
                break;
            case 1884273159:
                if (str.equals("heart-rate")) {
                    setTopIconSrc(W6.f(getContext(), 2131231134));
                    return;
                }
                break;
        }
        setTopIconSrc(W6.f(getContext(), 2131231132));
    }

    @DexIgnore
    public final void P(View view, AttributeSet attributeSet) {
        String d2;
        String d3;
        View findViewById = findViewById(2131363009);
        Wg6.b(findViewById, "findViewById(R.id.root)");
        this.w = findViewById;
        if (findViewById != null) {
            findViewById.setElevation(getElevation());
            View findViewById2 = findViewById(2131362583);
            Wg6.b(findViewById2, "findViewById(R.id.holder)");
            this.x = findViewById2;
            View findViewById3 = view.findViewById(2131362760);
            Wg6.b(findViewById3, "view.findViewById(R.id.iv_top)");
            this.z = (ImageView) findViewById3;
            View findViewById4 = view.findViewById(2131363413);
            Wg6.b(findViewById4, "view.findViewById(R.id.tv_top)");
            this.A = (TextView) findViewById4;
            View findViewById5 = view.findViewById(2131363286);
            Wg6.b(findViewById5, "view.findViewById(R.id.tv_bottom)");
            this.B = (TextView) findViewById5;
            View findViewById6 = view.findViewById(2131362927);
            Wg6.b(findViewById6, "view.findViewById(R.id.pb_progress)");
            this.C = (ProgressBar) findViewById6;
            View findViewById7 = view.findViewById(2131362746);
            Wg6.b(findViewById7, "view.findViewById(R.id.iv_ring)");
            this.y = (ImageView) findViewById7;
            Context context = getContext();
            TypedArray obtainStyledAttributes = context != null ? context.obtainStyledAttributes(attributeSet, X24.WidgetControl) : null;
            if (obtainStyledAttributes != null) {
                this.F = Integer.valueOf(obtainStyledAttributes.getResourceId(0, 2131230874));
                this.G = Integer.valueOf(obtainStyledAttributes.getResourceId(2, 2131230876));
                obtainStyledAttributes.getResourceId(2, 2131230876);
                this.H = this.F;
                this.L = obtainStyledAttributes.getDrawable(23);
                this.d0 = obtainStyledAttributes.getString(7);
                this.M = obtainStyledAttributes.getDrawable(22);
                this.N = obtainStyledAttributes.getDrawable(24);
                this.Q = obtainStyledAttributes.getString(17);
                this.R = obtainStyledAttributes.getColor(18, this.E);
                this.S = obtainStyledAttributes.getColor(19, this.E);
                this.T = obtainStyledAttributes.getDimension(20, -1.0f);
                this.U = obtainStyledAttributes.getDimension(21, -1.0f);
                this.V = obtainStyledAttributes.getString(4);
                this.W = obtainStyledAttributes.getColor(5, this.E);
                this.a0 = obtainStyledAttributes.getColor(6, this.E);
                this.b0 = obtainStyledAttributes.getDimension(8, -1.0f);
                this.c0 = obtainStyledAttributes.getDimension(9, -1.0f);
                String string = obtainStyledAttributes.getString(10);
                if (string == null) {
                    string = "";
                }
                this.f0 = string;
                String string2 = obtainStyledAttributes.getString(11);
                if (string2 == null) {
                    string2 = "";
                }
                this.h0 = string2;
                this.b0 = obtainStyledAttributes.getDimension(12, -1.0f);
                this.k0 = obtainStyledAttributes.getDimension(13, -1.0f);
                this.l0 = obtainStyledAttributes.getBoolean(16, false);
                this.m0 = obtainStyledAttributes.getBoolean(15, false);
                this.C0 = obtainStyledAttributes.getBoolean(14, false);
                setRemoveMode(this.m0);
                obtainStyledAttributes.recycle();
            }
            if (!TextUtils.isEmpty(this.f0) && (d3 = ThemeManager.l.a().d(this.f0)) != null) {
                int parseColor = Color.parseColor(d3);
                this.i0 = parseColor;
                this.e0 = parseColor;
            }
            if (!TextUtils.isEmpty(this.h0) && (d2 = ThemeManager.l.a().d(this.h0)) != null) {
                this.g0 = Color.parseColor(d2);
            }
            if (!this.l0) {
                ImageView imageView = this.z;
                if (imageView != null) {
                    imageView.setImageTintList(ColorStateList.valueOf(this.e0));
                } else {
                    Wg6.n("ivTop");
                    throw null;
                }
            }
        } else {
            Wg6.n("clRoot");
            throw null;
        }
    }

    @DexIgnore
    public final boolean Q() {
        return this.s0;
    }

    @DexIgnore
    public final boolean R() {
        return this.r0;
    }

    @DexIgnore
    public final void S(String str) {
        Wg6.c(str, "microAppId");
        switch (A57.a[MicroAppInstruction.MicroAppID.Companion.getMicroAppId(str).ordinal()]) {
            case 1:
                setTopIconSrc(W6.f(getContext(), 2131231104));
                return;
            case 2:
                setTopIconSrc(W6.f(getContext(), 2131231171));
                return;
            case 3:
                setTopIconSrc(W6.f(getContext(), 2131231163));
                return;
            case 4:
                setTopIconSrc(W6.f(getContext(), 2131231164));
                return;
            case 5:
                setTopIconSrc(W6.f(getContext(), 2131231169));
                return;
            case 6:
                setTopIconSrc(W6.f(getContext(), 2131231167));
                return;
            case 7:
                setTopIconSrc(W6.f(getContext(), 2131231166));
                return;
            case 8:
                setTopIconSrc(W6.f(getContext(), 2131231084));
                return;
            case 9:
                setTopIconSrc(W6.f(getContext(), 2131231170));
                return;
            case 10:
                setTopIconSrc(W6.f(getContext(), 2131231174));
                return;
            case 11:
                setTopIconSrc(W6.f(getContext(), 2131231175));
                return;
            case 12:
                setTopIconSrc(W6.f(getContext(), 2131231162));
                return;
            case 13:
                setTopIconSrc(W6.f(getContext(), 2131231176));
                return;
            case 14:
                setTopIconSrc(W6.f(getContext(), 2131231172));
                return;
            case 15:
                setTopIconSrc(W6.f(getContext(), 2131231173));
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void T() {
        K();
    }

    @DexIgnore
    public final void U() {
        this.p0 = null;
    }

    @DexIgnore
    public final void V(Integer num, Integer num2, Integer num3, Integer num4) {
        if (num3 != null) {
            num3.intValue();
            this.e0 = num3.intValue();
            this.R = num3.intValue();
            this.W = num3.intValue();
        }
        if (num != null) {
            num.intValue();
            this.g0 = num.intValue();
            this.S = num.intValue();
            this.a0 = num.intValue();
        }
        if (num2 != null) {
            num2.intValue();
            this.J = num2.intValue();
        }
        if (num4 != null) {
            num4.intValue();
            this.I = num4.intValue();
        }
        K();
    }

    @DexIgnore
    public final Un0 W(String str, Intent intent, View.OnDragListener onDragListener, b bVar) {
        Wg6.c(str, "label");
        Wg6.c(intent, "intentItem");
        setTag(getTag());
        Un0 un0 = new Un0(this, new d(this, bVar, 1.0f, str, intent));
        un0.a();
        if (onDragListener != null) {
            setOnDragListener(onDragListener);
        }
        return un0;
    }

    @DexIgnore
    public final void Y(boolean z2) {
        this.s0 = z2;
        if (z2) {
            N();
            this.t0 = null;
            ImageView imageView = this.y;
            if (imageView != null) {
                imageView.setImageDrawable(null);
                ProgressBar progressBar = this.C;
                if (progressBar != null) {
                    progressBar.setVisibility(0);
                } else {
                    Wg6.n("pbProgress");
                    throw null;
                }
            } else {
                Wg6.n("ivRing");
                throw null;
            }
        } else {
            ProgressBar progressBar2 = this.C;
            if (progressBar2 != null) {
                progressBar2.setVisibility(8);
            } else {
                Wg6.n("pbProgress");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void Z(Cb7 cb7, Bitmap bitmap) {
        this.t0 = cb7;
        ProgressBar progressBar = this.C;
        if (progressBar != null) {
            progressBar.setVisibility(8);
            String c2 = cb7 != null ? cb7.c() : null;
            if (c2 == null || c2.length() == 0) {
                ImageView imageView = this.y;
                if (imageView != null) {
                    imageView.setImageBitmap(bitmap);
                } else {
                    Wg6.n("ivRing");
                    throw null;
                }
            } else {
                Vj5<Drawable> n1 = Tj5.a(getContext()).J(cb7 != null ? cb7.c() : null).n1(this.o0);
                ImageView imageView2 = this.y;
                if (imageView2 != null) {
                    Wg6.b(n1.F0(imageView2), "GlideApp.with(context).l\u2026entDrawable).into(ivRing)");
                } else {
                    Wg6.n("ivRing");
                    throw null;
                }
            }
        } else {
            Wg6.n("pbProgress");
            throw null;
        }
    }

    @DexIgnore
    public final void a0() {
        O87 o87 = this.v0;
        O87 o872 = O87.WHITE;
        if (o87 == o872) {
            o872 = O87.BLACK;
        }
        this.v0 = o872;
        L(o872.getValue());
    }

    @DexIgnore
    public final void b0(String str) {
        Wg6.c(str, "watchAppId");
        switch (str.hashCode()) {
            case -829740640:
                if (str.equals("commute-time")) {
                    setTopIconSrc(W6.f(getContext(), 2131231043));
                    return;
                }
                return;
            case -740386388:
                if (str.equals("diagnostics")) {
                    setTopIconSrc(W6.f(getContext(), 2131231167));
                    return;
                }
                return;
            case -420342747:
                if (str.equals("wellness")) {
                    setTopIconSrc(W6.f(getContext(), 2131231143));
                    return;
                }
                return;
            case 96634189:
                if (str.equals("empty")) {
                    setTopIconSrc(null);
                    return;
                }
                return;
            case 104263205:
                if (str.equals(Constants.MUSIC)) {
                    setTopIconSrc(W6.f(getContext(), 2131231135));
                    return;
                }
                return;
            case 110364485:
                if (str.equals("timer")) {
                    setTopIconSrc(W6.f(getContext(), 2131231195));
                    return;
                }
                return;
            case 1223440372:
                if (str.equals("weather")) {
                    setTopIconSrc(W6.f(getContext(), 2131231142));
                    return;
                }
                return;
            case 1374620322:
                if (str.equals("notification-panel")) {
                    setTopIconSrc(W6.f(getContext(), 2131231169));
                    return;
                }
                return;
            case 1525170845:
                if (str.equals("workout")) {
                    setTopIconSrc(W6.f(getContext(), 2131231144));
                    return;
                }
                return;
            case 1860261700:
                if (str.equals("stop-watch")) {
                    setTopIconSrc(W6.f(getContext(), 2131231173));
                    return;
                }
                return;
            case 1904923164:
                if (str.equals("buddy-challenge")) {
                    setTopIconSrc(W6.f(getContext(), 2131231165));
                    return;
                }
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void c0(int i) {
        setTopIconSrc(W6.f(getContext(), i));
    }

    @DexIgnore
    public final int getBackgroundDrawableColor() {
        int i = this.K;
        if (i != this.E) {
            return i;
        }
        Integer num = this.H;
        if (num != null && num.intValue() == 2131230874) {
            return 2131099840;
        }
        return (num != null && num.intValue() == 2131230873) ? 2131099968 : 2131100360;
    }

    @DexIgnore
    public final int getBottomTextColor() {
        TextView textView = this.B;
        if (textView != null) {
            return textView.getCurrentTextColor();
        }
        Wg6.n("tvBottom");
        throw null;
    }

    @DexIgnore
    public final String getComplicationId() {
        return this.u0;
    }

    @DexIgnore
    public String getComponentId() {
        return this.u0;
    }

    @DexIgnore
    public final O87 getContentTheme() {
        return this.v0;
    }

    @DexIgnore
    public final int getIconTintColor() {
        return this.i0;
    }

    @DexIgnore
    public int getOrderIndex() {
        return this.D0;
    }

    @DexIgnore
    public int getPreviousIndex() {
        return this.E0;
    }

    @DexIgnore
    public final Cb7 getRing() {
        return this.t0;
    }

    @DexIgnore
    public final BitmapDrawable getRingDrawable() {
        ImageView imageView = this.y;
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable != null ? drawable instanceof BitmapDrawable : true) {
                return (BitmapDrawable) drawable;
            }
            return null;
        }
        Wg6.n("ivRing");
        throw null;
    }

    @DexIgnore
    public final float getScaleFactor() {
        return this.w0;
    }

    @DexIgnore
    public final String getSetting() {
        return this.q0;
    }

    @DexIgnore
    @Override // com.fossil.Zb7
    public W67.Ci getType() {
        return W67.Ci.COMPLICATION;
    }

    @DexIgnore
    @Override // com.fossil.Zb7
    public CustomizeWidget getView() {
        return this;
    }

    @DexIgnore
    public final Rect getWatchFaceRect() {
        return this.x0;
    }

    @DexIgnore
    @Override // androidx.constraintlayout.widget.ConstraintLayout
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
        if (!this.B0) {
            this.B0 = true;
            Rect rect = this.x0;
            if (rect != null) {
                float width = ((float) rect.width()) * 0.31666666f;
                float f = (float) (i3 - i);
                float f2 = width / f;
                this.w0 = f2;
                setScaleX(f2);
                setScaleY(this.w0);
                float f3 = (width - f) / 2.0f;
                setTranslationX((this.y0 * ((float) rect.width())) + this.A0 + f3);
                setTranslationY((((float) rect.height()) * this.z0) + this.A0 + f3);
            }
        }
        super.onLayout(z2, i, i2, i3, i4);
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.C0) {
            Integer valueOf = motionEvent != null ? Integer.valueOf(motionEvent.getAction()) : null;
            if (valueOf != null && valueOf.intValue() == 1) {
                FLogger.INSTANCE.getLocal().d("WidgetControl", "onTouchEvent up");
                setSelectedWc(false);
            } else if (valueOf != null && valueOf.intValue() == 0) {
                FLogger.INSTANCE.getLocal().d("WidgetControl", "onTouchEvent down");
                setSelectedWc(true);
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    public final void setBackgroundDrawableCus(Drawable drawable) {
        Wg6.c(drawable, ResourceManager.DRAWABLE);
        this.n0 = drawable;
        K();
    }

    @DexIgnore
    public final void setBackgroundRes(Integer num) {
        this.n0 = null;
        this.F = num;
        K();
    }

    @DexIgnore
    public final void setBottomContent(String str) {
        this.V = str;
        if (this.m0) {
            if (!TextUtils.isEmpty(str)) {
                TextView textView = this.B;
                if (textView != null) {
                    textView.setText(str);
                } else {
                    Wg6.n("tvBottom");
                    throw null;
                }
            }
        } else if (!TextUtils.isEmpty(str)) {
            TextView textView2 = this.B;
            if (textView2 != null) {
                textView2.setVisibility(0);
                TextView textView3 = this.B;
                if (textView3 != null) {
                    textView3.setText(str);
                } else {
                    Wg6.n("tvBottom");
                    throw null;
                }
            } else {
                Wg6.n("tvBottom");
                throw null;
            }
        } else {
            TextView textView4 = this.B;
            if (textView4 != null) {
                textView4.setVisibility(8);
            } else {
                Wg6.n("tvBottom");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void setContentTheme(O87 o87) {
        Wg6.c(o87, BaseFeatureModel.COLUMN_COLOR);
        this.v0 = o87;
        L(o87.getValue());
    }

    @DexIgnore
    public final void setDefaultColorRes(Integer num) {
        if (num != null) {
            num.intValue();
            this.e0 = num.intValue();
            K();
        }
    }

    @DexIgnore
    public final void setDragMode(boolean z2) {
        if (z2) {
            View view = this.w;
            if (view != null) {
                view.setVisibility(4);
                View view2 = this.x;
                if (view2 != null) {
                    view2.setVisibility(0);
                    setAlpha(1.0f);
                    return;
                }
                Wg6.n("vHolder");
                throw null;
            }
            Wg6.n("clRoot");
            throw null;
        }
        View view3 = this.w;
        if (view3 != null) {
            view3.setVisibility(0);
            View view4 = this.x;
            if (view4 != null) {
                view4.setVisibility(4);
                setAlpha(1.0f);
                return;
            }
            Wg6.n("vHolder");
            throw null;
        }
        Wg6.n("clRoot");
        throw null;
    }

    @DexIgnore
    public final void setEventEditorListener(Ac7 ac7) {
        setOnTouchListener(new a());
        this.p0 = ac7;
    }

    @DexIgnore
    public final void setInFocused(boolean z2) {
        this.r0 = z2;
    }

    @DexIgnore
    public void setOrderIndex(int i) {
        this.D0 = i;
    }

    @DexIgnore
    public void setPreviousIndex(int i) {
        this.E0 = i;
    }

    @DexIgnore
    public final void setRemoveMode(boolean z2) {
        this.m0 = z2;
        K();
    }

    @DexIgnore
    public final void setSelectedWc(boolean z2) {
        this.l0 = z2;
        K();
    }

    @DexIgnore
    public final void setSetting(String str) {
        this.q0 = str;
    }

    @DexIgnore
    public final void setWatchFaceRect(Rect rect) {
        this.x0 = rect;
    }
}
