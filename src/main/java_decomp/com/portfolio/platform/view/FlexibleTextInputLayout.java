package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.textfield.TextInputLayout;
import com.mapped.Wg6;
import com.mapped.X24;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleTextInputLayout extends TextInputLayout {
    @DexIgnore
    public String A0; // = "";
    @DexIgnore
    public String B0; // = "";
    @DexIgnore
    public String C0; // = "";
    @DexIgnore
    public String y0; // = "";
    @DexIgnore
    public String z0; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputLayout(Context context) {
        super(context);
        Wg6.c(context, "context");
        h0(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        h0(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        h0(attributeSet);
    }

    @DexIgnore
    public final void h0(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, X24.FlexibleTextInputLayout);
            String string = obtainStyledAttributes.getString(3);
            if (string == null) {
                string = "";
            }
            this.y0 = string;
            String string2 = obtainStyledAttributes.getString(4);
            if (string2 == null) {
                string2 = "";
            }
            this.z0 = string2;
            String string3 = obtainStyledAttributes.getString(2);
            if (string3 == null) {
                string3 = "";
            }
            this.A0 = string3;
            String string4 = obtainStyledAttributes.getString(0);
            if (string4 == null) {
                string4 = "";
            }
            this.B0 = string4;
            String string5 = obtainStyledAttributes.getString(1);
            if (string5 == null) {
                string5 = "";
            }
            this.C0 = string5;
            obtainStyledAttributes.recycle();
        }
        if (TextUtils.isEmpty(this.y0)) {
            this.y0 = "primaryText";
        }
        if (TextUtils.isEmpty(this.z0)) {
            this.z0 = "nonBrandTextStyle2";
        }
        i0(this.y0, this.z0, this.C0);
        setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void i0(String str, String str2, String str3) {
        String d = ThemeManager.l.a().d(str);
        Typeface f = ThemeManager.l.a().f(str2);
        String d2 = ThemeManager.l.a().d(this.A0);
        String d3 = ThemeManager.l.a().d(this.B0);
        String d4 = ThemeManager.l.a().d(str3);
        if (d != null) {
            setDefaultHintTextColor(ColorStateList.valueOf(Color.parseColor(d)));
        }
        if (d2 != null) {
            setPasswordVisibilityToggleTintList(ColorStateList.valueOf(Color.parseColor(d2)));
        }
        if (f != null) {
            setTypeface(f);
        }
        if (d3 != null) {
            setBoxStrokeColor(Color.parseColor(d3));
        }
        if (d4 != null) {
            setErrorTextColor(ColorStateList.valueOf(Color.parseColor(d4)));
        }
    }
}
