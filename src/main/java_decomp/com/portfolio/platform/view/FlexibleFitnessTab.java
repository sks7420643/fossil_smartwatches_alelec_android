package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.Am0;
import com.fossil.Gf0;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleFitnessTab extends ConstraintLayout {
    @DexIgnore
    public String A; // = "";
    @DexIgnore
    public int B;
    @DexIgnore
    public String C; // = "";
    @DexIgnore
    public View w;
    @DexIgnore
    public ImageView x;
    @DexIgnore
    public FlexibleTextView y;
    @DexIgnore
    public FlexibleTextView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleFitnessTab(Context context) {
        super(context);
        Wg6.c(context, "context");
        x(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleFitnessTab(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        x(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleFitnessTab(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        x(attributeSet);
    }

    @DexIgnore
    private final void setTabBackgroundColor(int i) {
        Context context = getContext();
        if (context != null) {
            Drawable d = Gf0.d(context, 2131231177);
            if (d != null) {
                Am0.n(Am0.r(d), i);
                View view = this.w;
                if (view != null) {
                    Drawable background = view.getBackground();
                    if (background instanceof ShapeDrawable) {
                        Paint paint = ((ShapeDrawable) background).getPaint();
                        Wg6.b(paint, "drawable.paint");
                        paint.setColor(i);
                    } else if (background instanceof GradientDrawable) {
                        ((GradientDrawable) background).setColor(i);
                    } else if (background instanceof ColorDrawable) {
                        ((ColorDrawable) background).setColor(i);
                    } else if (background instanceof StateListDrawable) {
                        background.setColorFilter(i, PorterDuff.Mode.SRC_ATOP);
                    } else if (background instanceof BitmapDrawable) {
                        background.mutate().setColorFilter(i, PorterDuff.Mode.SRC_ATOP);
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.constraintlayout.widget.ConstraintLayout
    private final void x(AttributeSet attributeSet) {
        Context context = getContext();
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        View inflate = layoutInflater != null ? layoutInflater.inflate(2131558844, (ViewGroup) this, true) : null;
        if (inflate != null) {
            this.w = inflate.findViewById(2131362103);
            this.x = (ImageView) inflate.findViewById(2131362756);
            this.y = (FlexibleTextView) inflate.findViewById(2131362537);
            this.z = (FlexibleTextView) inflate.findViewById(2131362536);
            if (attributeSet != null) {
                Context context2 = getContext();
                TypedArray obtainStyledAttributes = context2 != null ? context2.obtainStyledAttributes(attributeSet, X24.FlexibleFitnessTab) : null;
                if (obtainStyledAttributes != null) {
                    String string = obtainStyledAttributes.getString(0);
                    if (string == null) {
                        string = "hybridInactiveTab";
                    }
                    Wg6.b(string, "styledAttrs.getString(R.\u2026d) ?: \"hybridInactiveTab\"");
                    String d = ThemeManager.l.a().d(string);
                    if (d == null) {
                        d = "#ffffff";
                    }
                    this.A = d;
                    this.B = obtainStyledAttributes.getResourceId(1, 2131231069);
                    String string2 = obtainStyledAttributes.getString(2);
                    if (string2 == null) {
                        string2 = "";
                    }
                    this.C = string2;
                    ImageView imageView = this.x;
                    if (imageView != null) {
                        imageView.setImageResource(this.B);
                        FlexibleTextView flexibleTextView = this.z;
                        if (flexibleTextView != null) {
                            flexibleTextView.setText(this.C);
                            obtainStyledAttributes.recycle();
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
            View view = this.w;
            if (view != null) {
                view.setBackgroundResource(2131231177);
                G();
                return;
            }
            Wg6.i();
            throw null;
        }
        throw new Rc6("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    public final void G() {
        if (!TextUtils.isEmpty(this.A)) {
            setTabBackgroundColor(Color.parseColor(this.A));
        }
    }

    @DexIgnore
    public final void H(int i) {
        ImageView imageView = this.x;
        if (imageView != null) {
            imageView.setColorFilter(i);
        }
        FlexibleTextView flexibleTextView = this.z;
        if (flexibleTextView != null) {
            flexibleTextView.setTextColor(i);
        }
        FlexibleTextView flexibleTextView2 = this.y;
        if (flexibleTextView2 != null) {
            flexibleTextView2.setTextColor(i);
        }
    }

    @DexIgnore
    public final void I(int i) {
        setTabBackgroundColor(i);
    }

    @DexIgnore
    public final void J(String str) {
        Wg6.c(str, "value");
        FlexibleTextView flexibleTextView = this.z;
        if (flexibleTextView != null) {
            flexibleTextView.setText(str);
        }
    }

    @DexIgnore
    public final void K(String str) {
        Wg6.c(str, "value");
        FlexibleTextView flexibleTextView = this.y;
        if (flexibleTextView != null) {
            flexibleTextView.setText(str);
        }
    }
}
