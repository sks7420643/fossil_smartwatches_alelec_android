package com.portfolio.platform.view.blur;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import androidx.renderscript.RenderScript;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Aw0;
import com.fossil.Dw0;
import com.fossil.Tv0;
import com.fossil.Vt7;
import com.fossil.Vv0;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.X24;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BlurView extends View {
    @DexIgnore
    public static /* final */ a A; // = new a(null);
    @DexIgnore
    public static int x;
    @DexIgnore
    public static /* final */ b y; // = new b();
    @DexIgnore
    public static Boolean z; // = null;
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ float d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public Bitmap f;
    @DexIgnore
    public Bitmap g;
    @DexIgnore
    public Canvas h;
    @DexIgnore
    public RenderScript i;
    @DexIgnore
    public Dw0 j;
    @DexIgnore
    public Tv0 k;
    @DexIgnore
    public Tv0 l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public /* final */ Rect s; // = new Rect();
    @DexIgnore
    public /* final */ Rect t; // = new Rect();
    @DexIgnore
    public View u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public /* final */ ViewTreeObserver.OnPreDrawListener w; // = new c(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Boolean a() {
            return BlurView.z;
        }

        @DexIgnore
        public final boolean b(Context context) {
            if (a() == null && context != null) {
                c(Boolean.valueOf((context.getApplicationInfo().flags & 2) != 0));
            }
            return a() == Boolean.TRUE;
        }

        @DexIgnore
        public final void c(Boolean bool) {
            BlurView.z = bool;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RuntimeException {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ViewTreeObserver.OnPreDrawListener {
        @DexIgnore
        public /* final */ /* synthetic */ BlurView b;

        @DexIgnore
        public c(BlurView blurView) {
            this.b = blurView;
        }

        @DexIgnore
        public final boolean onPreDraw() {
            Canvas canvas;
            int[] iArr = new int[2];
            Bitmap bitmap = this.b.g;
            View view = this.b.u;
            if (view != null && this.b.isShown() && this.b.p()) {
                boolean a2 = Wg6.a(this.b.g, bitmap);
                view.getLocationOnScreen(iArr);
                int i = -iArr[0];
                int i2 = -iArr[1];
                this.b.getLocationOnScreen(iArr);
                int i3 = iArr[0];
                int i4 = iArr[1];
                Bitmap bitmap2 = this.b.f;
                if (bitmap2 != null) {
                    bitmap2.eraseColor(this.b.c & 16777215);
                    Canvas canvas2 = this.b.h;
                    if (canvas2 != null) {
                        int save = canvas2.save();
                        this.b.m = true;
                        BlurView.x++;
                        try {
                            Canvas canvas3 = this.b.h;
                            if (canvas3 != null) {
                                Bitmap bitmap3 = this.b.f;
                                if (bitmap3 != null) {
                                    float width = (((float) bitmap3.getWidth()) * 1.0f) / ((float) this.b.getWidth());
                                    Bitmap bitmap4 = this.b.f;
                                    if (bitmap4 != null) {
                                        canvas3.scale(width, (((float) bitmap4.getHeight()) * 1.0f) / ((float) this.b.getHeight()));
                                        Canvas canvas4 = this.b.h;
                                        if (canvas4 != null) {
                                            canvas4.translate((float) (-(i + i3)), (float) (-(i4 + i2)));
                                            if (view.getBackground() != null) {
                                                Drawable background = view.getBackground();
                                                Canvas canvas5 = this.b.h;
                                                if (canvas5 != null) {
                                                    background.draw(canvas5);
                                                } else {
                                                    Wg6.i();
                                                    throw null;
                                                }
                                            }
                                            view.draw(this.b.h);
                                            this.b.m = false;
                                            BlurView.x--;
                                            canvas = this.b.h;
                                            if (canvas == null) {
                                                Wg6.i();
                                                throw null;
                                            }
                                            canvas.restoreToCount(save);
                                            BlurView blurView = this.b;
                                            Bitmap bitmap5 = blurView.f;
                                            if (bitmap5 != null) {
                                                Bitmap bitmap6 = this.b.g;
                                                if (bitmap6 != null) {
                                                    blurView.n(bitmap5, bitmap6);
                                                    if ((!a2) || this.b.v) {
                                                        this.b.invalidate();
                                                    }
                                                } else {
                                                    Wg6.i();
                                                    throw null;
                                                }
                                            } else {
                                                Wg6.i();
                                                throw null;
                                            }
                                        } else {
                                            Wg6.i();
                                            throw null;
                                        }
                                    } else {
                                        Wg6.i();
                                        throw null;
                                    }
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } catch (b e) {
                            this.b.m = false;
                            BlurView.x--;
                            canvas = this.b.h;
                            if (canvas == null) {
                                Wg6.i();
                                throw null;
                            }
                        } catch (Throwable th) {
                            this.b.m = false;
                            BlurView.x--;
                            Canvas canvas6 = this.b.h;
                            if (canvas6 == null) {
                                Wg6.i();
                                throw null;
                            }
                            canvas6.restoreToCount(save);
                            throw th;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            return true;
        }
    }

    /*
    static {
        try {
            ClassLoader classLoader = BlurView.class.getClassLoader();
            if (classLoader != null) {
                classLoader.loadClass("android.support.v8.renderscript.RenderScript");
                return;
            }
            Wg6.i();
            throw null;
        } catch (ClassNotFoundException e2) {
            throw new RuntimeException("RenderScript support not enabled. Add \"android { defaultConfig { renderscriptSupportModeEnabled true }}\" in your build.gradle");
        }
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BlurView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.BlurView);
        Resources resources = context.getResources();
        Wg6.b(resources, "context.resources");
        this.d = obtainStyledAttributes.getDimension(2, TypedValue.applyDimension(1, 10.0f, resources.getDisplayMetrics()));
        this.b = obtainStyledAttributes.getFloat(0, 4.0f);
        this.c = obtainStyledAttributes.getColor(1, 0);
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    private final View getActivityDecorView() {
        Context context = getContext();
        for (int i2 = 0; i2 < 4 && context != null && !(context instanceof Activity) && (context instanceof ContextWrapper); i2++) {
            context = ((ContextWrapper) context).getBaseContext();
        }
        if (!(context instanceof Activity)) {
            return null;
        }
        Window window = ((Activity) context).getWindow();
        Wg6.b(window, "ctx.window");
        return window.getDecorView();
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        if (this.m) {
            throw y;
        } else if (x <= 0) {
            super.draw(canvas);
        }
    }

    @DexIgnore
    public final void n(Bitmap bitmap, Bitmap bitmap2) {
        Tv0 tv0 = this.k;
        if (tv0 != null) {
            tv0.f(bitmap);
            Dw0 dw0 = this.j;
            if (dw0 != null) {
                dw0.m(this.k);
                Dw0 dw02 = this.j;
                if (dw02 != null) {
                    dw02.l(this.l);
                    Tv0 tv02 = this.l;
                    if (tv02 != null) {
                        tv02.g(bitmap2);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void o(Canvas canvas, Bitmap bitmap, int i2) {
        if (bitmap != null) {
            this.s.right = bitmap.getWidth();
            this.s.bottom = bitmap.getHeight();
            this.t.right = getWidth();
            this.t.bottom = getHeight();
            canvas.drawBitmap(bitmap, this.s, this.t, (Paint) null);
        }
        canvas.drawColor(i2);
    }

    @DexIgnore
    public void onAttachedToWindow() {
        boolean z2 = false;
        super.onAttachedToWindow();
        View activityDecorView = getActivityDecorView();
        this.u = activityDecorView;
        if (activityDecorView == null) {
            this.v = false;
        } else if (activityDecorView != null) {
            activityDecorView.getViewTreeObserver().addOnPreDrawListener(this.w);
            View view = this.u;
            if (view != null) {
                if (view.getRootView() != getRootView()) {
                    z2 = true;
                }
                this.v = z2;
                if (z2) {
                    View view2 = this.u;
                    if (view2 != null) {
                        view2.postInvalidate();
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        View view = this.u;
        if (view != null) {
            if (view != null) {
                view.getViewTreeObserver().removeOnPreDrawListener(this.w);
            } else {
                Wg6.i();
                throw null;
            }
        }
        q();
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        super.onDraw(canvas);
        o(canvas, this.g, this.c);
    }

    @DexIgnore
    public final boolean p() {
        Bitmap bitmap;
        float f2 = 25.0f;
        if (this.d == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            q();
            return false;
        }
        float f3 = this.b;
        if (this.e || this.i == null) {
            if (this.i == null) {
                try {
                    RenderScript a2 = RenderScript.a(getContext());
                    this.i = a2;
                    if (a2 == null) {
                        Wg6.i();
                        throw null;
                    } else if (a2 != null) {
                        this.j = Dw0.k(a2, Vv0.k(a2));
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } catch (Aw0 e2) {
                    if (A.b(getContext())) {
                        if (e2.getMessage() != null) {
                            String message = e2.getMessage();
                            if (message == null) {
                                Wg6.i();
                                throw null;
                            } else if (Vt7.s(message, "Error networkLoading RS jni library: java.lang.UnsatisfiedLinkError:", false, 2, null)) {
                                throw new RuntimeException("Error networkLoading RS jni library, Upgrade buildToolsVersion=\"24.0.2\" or higher may solve this issue");
                            }
                        }
                        throw e2;
                    }
                    s();
                    return false;
                }
            }
            this.e = false;
            float f4 = this.d / f3;
            if (f4 > 25.0f) {
                f3 = (f4 * f3) / 25.0f;
            } else {
                f2 = f4;
            }
            Dw0 dw0 = this.j;
            if (dw0 != null) {
                dw0.n(f2);
            } else {
                Wg6.i();
                throw null;
            }
        }
        int width = getWidth();
        int height = getHeight();
        int max = Math.max(1, (int) (((float) width) / f3));
        int max2 = Math.max(1, (int) (((float) height) / f3));
        if (!(this.h == null || (bitmap = this.g) == null)) {
            if (bitmap == null) {
                Wg6.i();
                throw null;
            } else if (bitmap.getWidth() == max) {
                Bitmap bitmap2 = this.g;
                if (bitmap2 == null) {
                    Wg6.i();
                    throw null;
                } else if (bitmap2.getHeight() == max2) {
                    return true;
                }
            }
        }
        r();
        try {
            Bitmap createBitmap = Bitmap.createBitmap(max, max2, Bitmap.Config.ARGB_8888);
            this.f = createBitmap;
            if (createBitmap == null) {
                r();
                return false;
            }
            Bitmap bitmap3 = this.f;
            if (bitmap3 != null) {
                this.h = new Canvas(bitmap3);
                RenderScript renderScript = this.i;
                if (renderScript != null) {
                    Bitmap bitmap4 = this.f;
                    if (bitmap4 != null) {
                        Tv0 h2 = Tv0.h(renderScript, bitmap4, Tv0.Bi.MIPMAP_NONE, 1);
                        this.k = h2;
                        RenderScript renderScript2 = this.i;
                        if (renderScript2 == null) {
                            Wg6.i();
                            throw null;
                        } else if (h2 != null) {
                            this.l = Tv0.i(renderScript2, h2.l());
                            Bitmap createBitmap2 = Bitmap.createBitmap(max, max2, Bitmap.Config.ARGB_8888);
                            this.g = createBitmap2;
                            if (createBitmap2 != null) {
                                return true;
                            }
                            r();
                            return false;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } catch (OutOfMemoryError e3) {
            r();
            return false;
        } catch (Throwable th) {
            r();
            return false;
        }
    }

    @DexIgnore
    public final void q() {
        r();
        s();
    }

    @DexIgnore
    public final void r() {
        Tv0 tv0 = this.k;
        if (tv0 != null) {
            if (tv0 != null) {
                tv0.b();
                this.k = null;
            } else {
                Wg6.i();
                throw null;
            }
        }
        Tv0 tv02 = this.l;
        if (tv02 != null) {
            if (tv02 != null) {
                tv02.b();
                this.l = null;
            } else {
                Wg6.i();
                throw null;
            }
        }
        Bitmap bitmap = this.f;
        if (bitmap != null) {
            if (bitmap != null) {
                bitmap.recycle();
                this.f = null;
            } else {
                Wg6.i();
                throw null;
            }
        }
        Bitmap bitmap2 = this.g;
        if (bitmap2 == null) {
            return;
        }
        if (bitmap2 != null) {
            bitmap2.recycle();
            this.g = null;
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void s() {
        RenderScript renderScript = this.i;
        if (renderScript != null) {
            if (renderScript != null) {
                renderScript.e();
                this.i = null;
            } else {
                Wg6.i();
                throw null;
            }
        }
        Dw0 dw0 = this.j;
        if (dw0 == null) {
            return;
        }
        if (dw0 != null) {
            dw0.b();
            this.j = null;
            return;
        }
        Wg6.i();
        throw null;
    }
}
