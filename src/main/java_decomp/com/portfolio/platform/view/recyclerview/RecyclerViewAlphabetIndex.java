package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Em7;
import com.fossil.P47;
import com.mapped.Mj6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecyclerViewAlphabetIndex extends RecyclerView {
    @DexIgnore
    public String[] b; // = {"#", "A", "B", "C", "D", "E", DeviceIdentityUtils.FLASH_SERIAL_NUMBER_PREFIX, "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX, "T", "U", "V", "W", "X", "Y", "Z"};
    @DexIgnore
    public float c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public Typeface f;
    @DexIgnore
    public a g;
    @DexIgnore
    public LinearLayoutManager h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.g<a> {
        @DexIgnore
        public /* final */ Typeface a;
        @DexIgnore
        public /* final */ Typeface b;
        @DexIgnore
        public String[] c;
        @DexIgnore
        public int d;
        @DexIgnore
        public b e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class a extends RecyclerView.ViewHolder {
            @DexIgnore
            public /* final */ FlexibleTextView a;
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a implements View.OnClickListener {
                @DexIgnore
                public /* final */ /* synthetic */ a b;

                @DexIgnore
                public a(a aVar) {
                    this.b = aVar;
                }

                @DexIgnore
                public final void onClick(View view) {
                    int adapterPosition = this.b.getAdapterPosition();
                    if (adapterPosition != -1) {
                        String str = "" + this.b.a().getText().toString();
                        if (RecyclerViewAlphabetIndex.this.e()) {
                            RecyclerViewAlphabetIndex.this.setLetterToBold(str);
                        }
                        b g = this.b.b.g();
                        if (g != null) {
                            Wg6.b(view, "it");
                            g.a(view, adapterPosition, str);
                        }
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(a aVar, View view) {
                super(view);
                Wg6.c(view, "itemView");
                this.b = aVar;
                View findViewById = view.findViewById(2131363249);
                if (findViewById != null) {
                    this.a = (FlexibleTextView) findViewById;
                    view.setOnClickListener(new a(this));
                    return;
                }
                Wg6.i();
                throw null;
            }

            @DexIgnore
            public final FlexibleTextView a() {
                return this.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a() {
            Typeface defaultFromStyle = Typeface.defaultFromStyle(0);
            if (defaultFromStyle != null) {
                this.a = defaultFromStyle;
                this.b = Typeface.defaultFromStyle(1);
                return;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        public final b g() {
            return this.e;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public int getItemCount() {
            String[] strArr = this.c;
            if (strArr != null) {
                return strArr.length;
            }
            return 0;
        }

        @DexIgnore
        public void h(a aVar, int i) {
            Wg6.c(aVar, "holder");
            String[] strArr = this.c;
            if (strArr != null) {
                aVar.a().setText(strArr[i]);
                if (RecyclerViewAlphabetIndex.this.e()) {
                    aVar.a().setTypeface(i == this.d ? this.b : this.a);
                    aVar.a().setTextSize(2, RecyclerViewAlphabetIndex.this.getMFontSize$app_fossilRelease());
                    if (RecyclerViewAlphabetIndex.this.getMItemColor$app_fossilRelease() != 0) {
                        aVar.a().setTextColor(RecyclerViewAlphabetIndex.this.getMItemColor$app_fossilRelease());
                    }
                    if (RecyclerViewAlphabetIndex.this.getTypeFaceBaseTheme$app_fossilRelease() != null) {
                        aVar.a().setTypeface(RecyclerViewAlphabetIndex.this.getTypeFaceBaseTheme$app_fossilRelease());
                        return;
                    }
                    return;
                }
                return;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        public a i(ViewGroup viewGroup, int i) {
            Wg6.c(viewGroup, "parent");
            View inflate = LayoutInflater.from(RecyclerViewAlphabetIndex.this.getContext()).inflate(2131558702, viewGroup, false);
            Wg6.b(inflate, "view");
            return new a(this, inflate);
        }

        @DexIgnore
        public final void j(b bVar) {
            Wg6.c(bVar, "sectionIndexClickListener");
            this.e = bVar;
        }

        @DexIgnore
        public final void k(String[] strArr) {
            Wg6.c(strArr, "alphabet");
            this.c = strArr;
            notifyDataSetChanged();
        }

        @DexIgnore
        public final void l(int i) {
            this.d = i;
            notifyDataSetChanged();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public /* bridge */ /* synthetic */ void onBindViewHolder(a aVar, int i) {
            h(aVar, i);
        }

        @DexIgnore
        /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public /* bridge */ /* synthetic */ a onCreateViewHolder(ViewGroup viewGroup, int i) {
            return i(viewGroup, i);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(View view, int i, String str);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewAlphabetIndex(Context context) {
        super(context);
        Wg6.c(context, "context");
        setOverScrollMode(2);
        d();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewAlphabetIndex(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        setOverScrollMode(2);
        c(context, attributeSet);
        d();
    }

    @DexIgnore
    private final void setAlphabet(String[] strArr) {
        Arrays.sort(strArr);
        a aVar = this.g;
        if (aVar != null) {
            aVar.k(strArr);
        }
    }

    @DexIgnore
    public final void a() {
        setAlphabet(this.b);
    }

    @DexIgnore
    public final void c(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.RecyclerViewAlphabetIndex);
        obtainStyledAttributes.getDimensionPixelSize(4, 15);
        this.c = obtainStyledAttributes.getDimension(1, (float) P47.m(3.0f));
        this.d = obtainStyledAttributes.getColor(2, W6.d(context, 2131099967));
        this.e = obtainStyledAttributes.getBoolean(0, false);
        obtainStyledAttributes.recycle();
        String d2 = ThemeManager.l.a().d("primaryText");
        this.f = ThemeManager.l.a().f("descriptionText2");
        if (!TextUtils.isEmpty(d2)) {
            this.d = Color.parseColor(d2);
        }
    }

    @DexIgnore
    public final void d() {
        this.g = new a();
        this.h = new LinearLayoutManager(getContext(), 1, false);
        setHasFixedSize(true);
        setAdapter(this.g);
        setLayoutManager(this.h);
    }

    @DexIgnore
    public final boolean e() {
        return this.e;
    }

    @DexIgnore
    public final float getMFontSize$app_fossilRelease() {
        return this.c;
    }

    @DexIgnore
    public final int getMItemColor$app_fossilRelease() {
        return this.d;
    }

    @DexIgnore
    public final Typeface getTypeFaceBaseTheme$app_fossilRelease() {
        return this.f;
    }

    @DexIgnore
    public final void setCustomizable$app_fossilRelease(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public final void setLetterToBold(String str) {
        Wg6.c(str, "letter");
        int Q = Em7.Q(this.b, str);
        if (new Mj6("[0-9]+").matches(str)) {
            Q = this.b.length - 1;
        }
        LinearLayoutManager linearLayoutManager = this.h;
        if (linearLayoutManager != null) {
            linearLayoutManager.D2(Q, 0);
        }
        a aVar = this.g;
        if (aVar != null) {
            aVar.l(Q);
        }
    }

    @DexIgnore
    public final void setMFontSize$app_fossilRelease(float f2) {
        this.c = f2;
    }

    @DexIgnore
    public final void setMItemColor$app_fossilRelease(int i) {
        this.d = i;
    }

    @DexIgnore
    public final void setOnSectionIndexClickListener(b bVar) {
        Wg6.c(bVar, "sectionIndexClickListener");
        a aVar = this.g;
        if (aVar != null) {
            aVar.j(bVar);
        }
    }

    @DexIgnore
    public final void setTypeFaceBaseTheme$app_fossilRelease(Typeface typeface) {
        this.f = typeface;
    }
}
