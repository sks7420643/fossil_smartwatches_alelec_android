package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.textfield.TextInputEditText;
import com.mapped.Wg6;
import com.mapped.X24;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleTextInputEditText extends TextInputEditText {
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public String f; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputEditText(Context context) {
        super(context);
        Wg6.c(context, "context");
        a(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, X24.FlexibleTextInputEditText);
            String string = obtainStyledAttributes.getString(0);
            if (string == null) {
                string = "";
            }
            this.e = string;
            String string2 = obtainStyledAttributes.getString(1);
            if (string2 == null) {
                string2 = "";
            }
            this.f = string2;
            obtainStyledAttributes.recycle();
        }
        if (TextUtils.isEmpty(this.e)) {
            this.e = "primaryText";
        }
        if (TextUtils.isEmpty(this.f)) {
            this.f = "nonBrandTextStyle2";
        }
        b(this.e, this.f);
        setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public final void b(String str, String str2) {
        String d = ThemeManager.l.a().d(str);
        Typeface f2 = ThemeManager.l.a().f(str2);
        if (d != null) {
            setTextColor(Color.parseColor(d));
            setHintTextColor(Color.parseColor(d));
            setLinkTextColor(Color.parseColor(d));
        }
        if (f2 != null) {
            setTypeface(f2);
        }
    }
}
