package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import com.mapped.Wg6;
import com.mapped.X24;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleProgressBar extends ProgressBar {
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleProgressBar(Context context) {
        super(context);
        Wg6.c(context, "context");
        a(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, X24.FlexibleProgressBar);
            String string = obtainStyledAttributes.getString(0);
            if (string == null) {
                string = "secondaryText";
            }
            Wg6.b(string, "styledAttrs.getString(R.\u2026       ?: \"secondaryText\"");
            String string2 = obtainStyledAttributes.getString(1);
            if (string2 == null) {
                string2 = "primaryText";
            }
            Wg6.b(string2, "styledAttrs.getString(R.\u2026         ?: \"primaryText\"");
            this.b = ThemeManager.l.a().d(string);
            this.c = ThemeManager.l.a().d(string2);
            b();
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public final void b() {
        String str = this.b;
        if (str != null) {
            setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(str)));
        }
        String str2 = this.c;
        if (str2 == null) {
            return;
        }
        if (Build.VERSION.SDK_INT > 21) {
            setProgressTintList(ColorStateList.valueOf(Color.parseColor(str2)));
        } else if (getProgressDrawable() != null) {
            Drawable mutate = getProgressDrawable().mutate();
            Wg6.b(mutate, "this.getProgressDrawable().mutate()");
            mutate.setColorFilter(Color.parseColor(str2), PorterDuff.Mode.SRC_IN);
            setProgressDrawable(mutate);
        }
    }
}
