package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatCheckBox;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.mapped.Wg6;
import com.mapped.X24;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleCheckBox extends AppCompatCheckBox {
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public String f; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleCheckBox(Context context) {
        super(context);
        Wg6.c(context, "context");
        b(null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleCheckBox(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        b(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleCheckBox(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        b(attributeSet);
    }

    @DexIgnore
    public final void b(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, X24.FlexibleCheckBox);
            String string = obtainStyledAttributes.getString(0);
            if (string == null) {
                string = "";
            }
            this.e = string;
            String string2 = obtainStyledAttributes.getString(1);
            if (string2 == null) {
                string2 = "";
            }
            this.f = string2;
            obtainStyledAttributes.recycle();
        }
        c();
    }

    @DexIgnore
    public final void c() {
        if (!TextUtils.isEmpty(this.e) && !TextUtils.isEmpty(this.f)) {
            String d = ThemeManager.l.a().d(this.f);
            String d2 = ThemeManager.l.a().d(this.e);
            if (!TextUtils.isEmpty(d) && !TextUtils.isEmpty(d2)) {
                setButtonTintList(new ColorStateList(new int[][]{new int[]{-16842912}, new int[0]}, new int[]{Color.parseColor(d2), Color.parseColor(d)}));
            }
        }
    }

    @DexIgnore
    public final void setEnableColor(String str) {
        Wg6.c(str, BaseFeatureModel.COLUMN_COLOR);
        this.f = str;
        c();
    }
}
