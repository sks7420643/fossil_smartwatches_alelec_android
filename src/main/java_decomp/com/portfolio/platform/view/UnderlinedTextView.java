package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.mapped.X24;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class UnderlinedTextView extends FlexibleTextView {
    @DexIgnore
    public Rect v;
    @DexIgnore
    public Paint w;
    @DexIgnore
    public float x;
    @DexIgnore
    public float y;
    @DexIgnore
    public /* final */ Rect z;

    @DexIgnore
    public UnderlinedTextView(Context context) {
        this(context, null, 0);
    }

    @DexIgnore
    public UnderlinedTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public UnderlinedTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.z = new Rect();
        o(context, attributeSet, i);
    }

    @DexIgnore
    public final void o(Context context, AttributeSet attributeSet, int i) {
        float f = context.getResources().getDisplayMetrics().density;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.UnderlinedTextView, i, 0);
        int color = obtainStyledAttributes.getColor(3, -65536);
        this.x = obtainStyledAttributes.getDimension(2, -1.0f);
        float dimension = obtainStyledAttributes.getDimension(1, f * 1.0f);
        this.y = obtainStyledAttributes.getDimension(0, 4.0f * dimension);
        obtainStyledAttributes.recycle();
        this.v = new Rect();
        Paint paint = new Paint();
        this.w = paint;
        paint.setStyle(Paint.Style.STROKE);
        this.w.setColor(color);
        this.w.setStrokeWidth(dimension);
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        int lineCount = getLineCount();
        CharSequence text = getText();
        getPaint().getTextBounds(text.toString(), 0, text.length(), this.z);
        if (this.x == -1.0f) {
            for (int i = 0; i < lineCount; i++) {
                int lineBounds = getLineBounds(i, this.v);
                Rect rect = this.v;
                float f = (float) lineBounds;
                float f2 = this.y;
                canvas.drawLine((float) rect.left, f + f2, (float) rect.right, f2 + f, this.w);
            }
        } else {
            int lineBounds2 = getLineBounds(getLineCount() - 1, this.v);
            float width = (((float) this.v.width()) - this.x) / 2.0f;
            Rect rect2 = this.v;
            float f3 = (float) lineBounds2;
            float f4 = this.y;
            canvas.drawLine(((float) rect2.left) + width, f3 + f4, ((float) rect2.right) - width, f3 + f4, this.w);
        }
        super.onDraw(canvas);
    }
}
