package com.portfolio.platform.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.widget.FrameLayout;
import androidx.viewpager2.widget.ViewPager2;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NestedScrollableHost extends FrameLayout {
    @DexIgnore
    public int b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NestedScrollableHost(Context context) {
        super(context);
        Wg6.c(context, "context");
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        Wg6.b(viewConfiguration, "ViewConfiguration.get(context)");
        this.b = viewConfiguration.getScaledTouchSlop();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NestedScrollableHost(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        ViewConfiguration viewConfiguration = ViewConfiguration.get(getContext());
        Wg6.b(viewConfiguration, "ViewConfiguration.get(context)");
        this.b = viewConfiguration.getScaledTouchSlop();
    }

    @DexIgnore
    private final View getChild() {
        if (getChildCount() > 0) {
            return getChildAt(0);
        }
        return null;
    }

    @DexIgnore
    private final ViewPager2 getParentViewPager() {
        ViewParent parent = getParent();
        if (!(parent instanceof View)) {
            parent = null;
        }
        View view = (View) parent;
        while (view != null && !(view instanceof ViewPager2)) {
            ViewParent parent2 = view.getParent();
            if (!(parent2 instanceof View)) {
                parent2 = null;
            }
            view = (View) parent2;
        }
        if (!(view instanceof ViewPager2)) {
            view = null;
        }
        return (ViewPager2) view;
    }

    @DexIgnore
    public final boolean a(int i, float f) {
        int i2 = -((int) Math.signum(f));
        if (i == 0) {
            View child = getChild();
            if (child != null) {
                return child.canScrollHorizontally(i2);
            }
            return false;
        } else if (i == 1) {
            View child2 = getChild();
            if (child2 != null) {
                return child2.canScrollVertically(i2);
            }
            return false;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @DexIgnore
    public final void b(MotionEvent motionEvent) {
        float f = 1.0f;
        ViewPager2 parentViewPager = getParentViewPager();
        if (parentViewPager != null) {
            int orientation = parentViewPager.getOrientation();
            if (!a(orientation, -1.0f) && !a(orientation, 1.0f)) {
                return;
            }
            if (motionEvent.getAction() == 0) {
                this.c = motionEvent.getX();
                this.d = motionEvent.getY();
                getParent().requestDisallowInterceptTouchEvent(true);
            } else if (motionEvent.getAction() == 2) {
                float x = motionEvent.getX() - this.c;
                float y = motionEvent.getY() - this.d;
                boolean z = orientation == 0;
                float abs = (z ? 0.5f : 1.0f) * Math.abs(x);
                float abs2 = Math.abs(y);
                if (!z) {
                    f = 0.5f;
                }
                float f2 = abs2 * f;
                int i = this.b;
                if (abs > ((float) i) || f2 > ((float) i)) {
                    if (z == (f2 > abs)) {
                        getParent().requestDisallowInterceptTouchEvent(false);
                        return;
                    }
                    if (a(orientation, z ? x : y)) {
                        getParent().requestDisallowInterceptTouchEvent(true);
                    } else {
                        getParent().requestDisallowInterceptTouchEvent(false);
                    }
                }
            }
        }
    }

    @DexIgnore
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        Wg6.c(motionEvent, "e");
        b(motionEvent);
        return super.onInterceptTouchEvent(motionEvent);
    }
}
