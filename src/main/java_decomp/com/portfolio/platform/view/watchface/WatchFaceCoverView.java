package com.portfolio.platform.view.watchface;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Qq7;
import com.fossil.Yk7;
import com.fossil.Zk7;
import com.mapped.Gg6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceCoverView extends AppCompatImageView {
    @DexIgnore
    public float d;
    @DexIgnore
    public /* final */ Paint e;
    @DexIgnore
    public /* final */ Yk7 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Qq7 implements Gg6<Bitmap> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceCoverView this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(WatchFaceCoverView watchFaceCoverView) {
            super(0);
            this.this$0 = watchFaceCoverView;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final Bitmap invoke() {
            String d = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
            int parseColor = d != null ? Color.parseColor(d) : -1;
            Bitmap createBitmap = Bitmap.createBitmap(this.this$0.getWidth(), this.this$0.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint(1);
            canvas.drawARGB(0, 0, 0, 0);
            canvas.drawColor(parseColor);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
            canvas.drawCircle(((float) this.this$0.getWidth()) / 2.0f, ((float) this.this$0.getHeight()) / 2.0f, (float) ((int) (((float) this.this$0.getHeight()) * this.this$0.d)), paint);
            return createBitmap;
        }
    }

    @DexIgnore
    public WatchFaceCoverView(Context context) {
        this(context, null, 0, 6, null);
    }

    @DexIgnore
    public WatchFaceCoverView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchFaceCoverView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Wg6.c(context, "context");
        int[] iArr = X24.WatchFaceCoverView;
        Wg6.b(iArr, "R.styleable.WatchFaceCoverView");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, iArr, 0, 0);
        this.d = obtainStyledAttributes.getFloat(0, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        obtainStyledAttributes.recycle();
        this.e = new Paint(1);
        this.f = Zk7.a(new a(this));
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WatchFaceCoverView(Context context, AttributeSet attributeSet, int i, int i2, Qg6 qg6) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    @DexIgnore
    private final Bitmap getBackgroundBitmap() {
        return (Bitmap) this.f.getValue();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (canvas != null) {
            canvas.drawBitmap(getBackgroundBitmap(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.e);
        }
        super.onDraw(canvas);
    }
}
