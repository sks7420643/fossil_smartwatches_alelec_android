package com.portfolio.platform.view.watchface;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Ab7;
import com.fossil.Ac7;
import com.fossil.At7;
import com.fossil.Bb7;
import com.fossil.Cb7;
import com.fossil.Db7;
import com.fossil.Eb7;
import com.fossil.Hl7;
import com.fossil.Ik5;
import com.fossil.Im7;
import com.fossil.M67;
import com.fossil.N67;
import com.fossil.O87;
import com.fossil.P67;
import com.fossil.Pm7;
import com.fossil.Po0;
import com.fossil.Qq7;
import com.fossil.R67;
import com.fossil.S67;
import com.fossil.S87;
import com.fossil.T67;
import com.fossil.Ty4;
import com.fossil.V67;
import com.fossil.Vn0;
import com.fossil.W67;
import com.fossil.W87;
import com.fossil.Za7;
import com.fossil.Zb7;
import com.fossil.Zn0;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.mapped.Hg6;
import com.mapped.Kc6;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.joda.time.chrono.BasicChronology;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"ClickableViewAccessibility"})
public final class WatchFaceEditorView extends ConstraintLayout implements Ac7, W67.Ai {
    @DexIgnore
    public View A;
    @DexIgnore
    public Rect B;
    @DexIgnore
    public float C;
    @DexIgnore
    public float D;
    @DexIgnore
    public CustomizeWidget E;
    @DexIgnore
    public CustomizeWidget F;
    @DexIgnore
    public int G;
    @DexIgnore
    public boolean H;
    @DexIgnore
    public b I;
    @DexIgnore
    public W67 J;
    @DexIgnore
    public /* final */ i K;
    @DexIgnore
    public /* final */ p L;
    @DexIgnore
    public /* final */ Vn0 M;
    @DexIgnore
    public /* final */ ScaleGestureDetector N;
    @DexIgnore
    public /* final */ Db7 w;
    @DexIgnore
    public ImageView x;
    @DexIgnore
    public Rect y;
    @DexIgnore
    public N67 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ float a;
        @DexIgnore
        public /* final */ float b;

        @DexIgnore
        public a(float f, float f2) {
            this.a = f;
            this.b = f2;
        }

        @DexIgnore
        public final float a() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!(Float.compare(this.a, aVar.a) == 0 && Float.compare(this.b, aVar.b) == 0)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            return (Float.floatToIntBits(this.a) * 31) + Float.floatToIntBits(this.b);
        }

        @DexIgnore
        public String toString() {
            return "EditorConfig(offset=" + this.a + ", editorSize=" + this.b + ")";
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Object b();  // void declaration

        @DexIgnore
        boolean c(Zb7 zb7);

        @DexIgnore
        void d(Zb7 zb7);

        @DexIgnore
        void e(W67.Bi bi, S87 s87);

        @DexIgnore
        void f(S87.Ci ci);

        @DexIgnore
        void g(Eb7 eb7);

        @DexIgnore
        void h(S87 s87);

        @DexIgnore
        void i(Rect rect);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditorView b;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeWidget c;

        @DexIgnore
        public c(WatchFaceEditorView watchFaceEditorView, CustomizeWidget customizeWidget) {
            this.b = watchFaceEditorView;
            this.c = customizeWidget;
        }

        @DexIgnore
        public final void run() {
            S87.Ai a0 = WatchFaceEditorView.a0(this.b, this.c, false, 2, null);
            b bVar = this.b.I;
            if (bVar != null) {
                bVar.e(W67.Bi.ADDED, a0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends Qq7 implements Hg6<View, Boolean> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Boolean invoke(View view) {
            return Boolean.valueOf(invoke(view));
        }

        @DexIgnore
        public final boolean invoke(View view) {
            Wg6.c(view, "it");
            return view instanceof CustomizeWidget;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditorView b;
        @DexIgnore
        public /* final */ /* synthetic */ S67 c;

        @DexIgnore
        public e(WatchFaceEditorView watchFaceEditorView, S67 s67) {
            this.b = watchFaceEditorView;
            this.c = s67;
        }

        @DexIgnore
        public final void run() {
            Lc6 d0 = this.b.d0(this.c.getWidth(), this.c.getHeight());
            this.c.s(((Number) d0.component1()).floatValue(), ((Number) d0.component2()).floatValue());
            this.c.g();
            this.b.setCurrentElement(this.c);
            S87 z0 = this.b.z0(W67.d(this.c, false, 1, null));
            b bVar = this.b.I;
            if (bVar != null) {
                bVar.e(W67.Bi.ADDED, z0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends Qq7 implements Hg6<View, Boolean> {
        @DexIgnore
        public static /* final */ f INSTANCE; // = new f();

        @DexIgnore
        public f() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Boolean invoke(View view) {
            return Boolean.valueOf(invoke(view));
        }

        @DexIgnore
        public final boolean invoke(View view) {
            Wg6.c(view, "it");
            return view instanceof S67;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditorView b;
        @DexIgnore
        public /* final */ /* synthetic */ T67 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ g b;

            @DexIgnore
            public a(g gVar) {
                this.b = gVar;
            }

            @DexIgnore
            public final void run() {
                W87 metric = this.b.c.getMetric();
                Lc6 d0 = this.b.b.d0((int) metric.c(), (int) metric.a());
                this.b.c.s(((Number) d0.component1()).floatValue(), ((Number) d0.component2()).floatValue());
                g gVar = this.b;
                S87 z0 = gVar.b.z0(W67.d(gVar.c, false, 1, null));
                b bVar = this.b.b.I;
                if (bVar != null) {
                    bVar.e(W67.Bi.ADDED, z0);
                }
            }
        }

        @DexIgnore
        public g(WatchFaceEditorView watchFaceEditorView, T67 t67) {
            this.b = watchFaceEditorView;
            this.c = t67;
        }

        @DexIgnore
        public final void run() {
            W87 metric = this.c.getMetric();
            if (this.b.d(metric.c(), metric.a(), metric.b())) {
                T67.z(this.c, null, 1, null);
                this.c.post(new a(this));
            } else {
                W87 metric2 = this.c.getMetric();
                Lc6 d0 = this.b.d0((int) metric2.c(), (int) metric2.a());
                this.c.s(((Number) d0.component1()).floatValue(), ((Number) d0.component2()).floatValue());
                S87 z0 = this.b.z0(W67.d(this.c, false, 1, null));
                b bVar = this.b.I;
                if (bVar != null) {
                    bVar.e(W67.Bi.ADDED, z0);
                }
            }
            this.c.g();
            this.b.setCurrentElement(this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends Qq7 implements Hg6<View, Boolean> {
        @DexIgnore
        public static /* final */ h INSTANCE; // = new h();

        @DexIgnore
        public h() {
            super(1);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Boolean invoke(View view) {
            return Boolean.valueOf(invoke(view));
        }

        @DexIgnore
        public final boolean invoke(View view) {
            Wg6.c(view, "it");
            return view instanceof T67;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends GestureDetector.SimpleOnGestureListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditorView b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(WatchFaceEditorView watchFaceEditorView) {
            this.b = watchFaceEditorView;
        }

        @DexIgnore
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            this.b.setCurrentElement(null);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends M67 {
        @DexIgnore
        public /* final */ /* synthetic */ R67 a;

        @DexIgnore
        public j(R67 r67) {
            this.a = r67;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.a.setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.a.e();
            this.a.m();
            this.a.setAnimating$app_fossilRelease(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ S87.Bi b;
        @DexIgnore
        public /* final */ /* synthetic */ S67 c;

        @DexIgnore
        public k(S87.Bi bi, S67 s67) {
            this.b = bi;
            this.c = s67;
        }

        @DexIgnore
        public final void run() {
            W87 b2 = this.b.b();
            if (b2 != null) {
                Lc6 a2 = Hl7.a(Float.valueOf(b2.d()), Float.valueOf(b2.e()));
                this.c.s(((Number) a2.component1()).floatValue(), ((Number) a2.component2()).floatValue());
                this.c.g();
                this.c.setOrderIndex(this.b.a());
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ S87.Ci b;
        @DexIgnore
        public /* final */ /* synthetic */ T67 c;

        @DexIgnore
        public l(S87.Ci ci, T67 t67) {
            this.b = ci;
            this.c = t67;
        }

        @DexIgnore
        public final void run() {
            W87 b2 = this.b.b();
            if (b2 != null) {
                Lc6 a2 = Hl7.a(Float.valueOf(b2.d()), Float.valueOf(b2.e()));
                this.c.s(((Number) a2.component1()).floatValue(), ((Number) a2.component2()).floatValue());
                this.c.g();
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditorView c;

        @DexIgnore
        public m(View view, WatchFaceEditorView watchFaceEditorView) {
            this.b = view;
            this.c = watchFaceEditorView;
        }

        @DexIgnore
        public final void run() {
            this.c.B = P67.b(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ImageView b;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditorView c;
        @DexIgnore
        public /* final */ /* synthetic */ b d;

        @DexIgnore
        public n(ImageView imageView, WatchFaceEditorView watchFaceEditorView, b bVar) {
            this.b = imageView;
            this.c = watchFaceEditorView;
            this.d = bVar;
        }

        @DexIgnore
        public final void run() {
            ImageView imageView = this.b;
            Wg6.b(imageView, "this");
            int[] c2 = P67.c(imageView);
            this.c.C = (float) c2[0];
            WatchFaceEditorView watchFaceEditorView = this.c;
            ImageView imageView2 = this.b;
            Wg6.b(imageView2, "this");
            watchFaceEditorView.y = P67.b(imageView2);
            WatchFaceEditorView watchFaceEditorView2 = this.c;
            ImageView imageView3 = this.b;
            Wg6.b(imageView3, "this");
            watchFaceEditorView2.D = (float) imageView3.getWidth();
            float f = this.c.D / ((float) 2);
            float f2 = (float) c2[0];
            Rect rect = this.c.y;
            if (rect != null) {
                float width = ((float) rect.width()) / 2.0f;
                float f3 = (float) c2[1];
                Rect rect2 = this.c.y;
                if (rect2 != null) {
                    this.c.z = new N67(f2 + width, f3 + (((float) rect2.height()) / 2.0f), f);
                    this.d.i(new Rect(this.c.y));
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnTouchListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditorView b;

        @DexIgnore
        public o(WatchFaceEditorView watchFaceEditorView) {
            this.b = watchFaceEditorView;
        }

        @DexIgnore
        public final boolean onTouch(View view, MotionEvent motionEvent) {
            this.b.M.a(motionEvent);
            this.b.N.onTouchEvent(motionEvent);
            if (!this.b.H) {
                WatchFaceEditorView watchFaceEditorView = this.b;
                watchFaceEditorView.H = watchFaceEditorView.N.isInProgress();
            }
            if (!this.b.H) {
                return true;
            }
            W67 currentFocused = this.b.getCurrentFocused();
            if (!(currentFocused instanceof R67)) {
                currentFocused = null;
            }
            R67 r67 = (R67) currentFocused;
            if (r67 == null) {
                return true;
            }
            WatchFaceEditorView watchFaceEditorView2 = this.b;
            Wg6.b(view, "handlerView");
            Wg6.b(motionEvent, Constants.EVENT);
            watchFaceEditorView2.h0(view, r67, motionEvent);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditorView a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public p(WatchFaceEditorView watchFaceEditorView) {
            this.a = watchFaceEditorView;
        }

        @DexIgnore
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            W67 currentFocused = this.a.getCurrentFocused();
            if (!(currentFocused instanceof R67)) {
                currentFocused = null;
            }
            R67 r67 = (R67) currentFocused;
            if (r67 != null) {
                r67.t(scaleGestureDetector != null ? scaleGestureDetector.getScaleFactor() : 1.0f);
            }
            return super.onScale(scaleGestureDetector);
        }

        @DexIgnore
        public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
            W67 currentFocused = this.a.getCurrentFocused();
            if (!(currentFocused instanceof R67)) {
                currentFocused = null;
            }
            R67 r67 = (R67) currentFocused;
            if (r67 != null) {
                r67.u(scaleGestureDetector != null ? scaleGestureDetector.getScaleFactor() : 1.0f);
            }
            return super.onScaleBegin(scaleGestureDetector);
        }

        @DexIgnore
        public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {
            W67 currentFocused = this.a.getCurrentFocused();
            if (!(currentFocused instanceof R67)) {
                currentFocused = null;
            }
            R67 r67 = (R67) currentFocused;
            if (r67 != null) {
                r67.v(scaleGestureDetector != null ? scaleGestureDetector.getScaleFactor() : 1.0f);
            }
        }
    }

    @DexIgnore
    public WatchFaceEditorView(Context context) {
        this(context, null, 0, 6, null);
    }

    @DexIgnore
    public WatchFaceEditorView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchFaceEditorView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Wg6.c(context, "context");
        this.w = new Db7(null, null, null, false, 15, null);
        this.G = -1;
        LayoutInflater.from(context).inflate(2131558852, (ViewGroup) this, true);
        this.K = new i(this);
        this.L = new p(this);
        this.M = new Vn0(context, this.K);
        this.N = new ScaleGestureDetector(context, this.L);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WatchFaceEditorView(Context context, AttributeSet attributeSet, int i2, int i3, Qg6 qg6) {
        this(context, (i3 & 2) != 0 ? null : attributeSet, (i3 & 4) != 0 ? 0 : i2);
    }

    @DexIgnore
    public static /* synthetic */ S87.Ai a0(WatchFaceEditorView watchFaceEditorView, CustomizeWidget customizeWidget, boolean z2, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        return watchFaceEditorView.Z(customizeWidget, z2);
    }

    @DexIgnore
    public static /* synthetic */ W87 c0(WatchFaceEditorView watchFaceEditorView, W87 w87, float f2, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            f2 = 1.0f;
        }
        return watchFaceEditorView.b0(w87, f2);
    }

    @DexIgnore
    public static /* synthetic */ W87 f0(WatchFaceEditorView watchFaceEditorView, W87 w87, float f2, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        return watchFaceEditorView.e0(w87, f2);
    }

    @DexIgnore
    private final void setCurrentComplication(CustomizeWidget customizeWidget) {
        this.F = customizeWidget;
        this.J = null;
    }

    @DexIgnore
    private final void setCurrentElement(W67 w67) {
        S87 s87 = null;
        if (!Wg6.a(this.J, w67)) {
            this.J = w67;
            if (w67 != null) {
                w67.n();
            }
            this.F = null;
            if (w67 instanceof T67) {
                s87 = W67.d(w67, false, 1, null);
            }
            b bVar = this.I;
            if (bVar != null) {
                bVar.h(s87);
            }
        }
    }

    @DexIgnore
    public static /* synthetic */ void u0(WatchFaceEditorView watchFaceEditorView, String str, Typeface typeface, O87 o87, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = null;
        }
        if ((i2 & 2) != 0) {
            typeface = null;
        }
        if ((i2 & 4) != 0) {
            o87 = null;
        }
        watchFaceEditorView.t0(str, typeface, o87);
    }

    @DexIgnore
    public final boolean W(Bb7 bb7) {
        float f2;
        float f3;
        Wg6.c(bb7, "complication");
        if (At7.g(At7.h(Po0.a(this), d.INSTANCE)) == 4) {
            return false;
        }
        int c2 = c();
        Za7 c3 = bb7.c();
        if (c3 != null) {
            f3 = (c3.a() - ((float) c3.c())) / this.D;
            f2 = (c3.b() - ((float) c3.c())) / this.D;
        } else {
            f2 = 0.0f;
            f3 = 0.0f;
        }
        CustomizeWidget customizeWidget = new CustomizeWidget(getContext(), new Rect(this.y), f3, f2, this.C, null, 32, null);
        customizeWidget.O(bb7.a());
        customizeWidget.setBottomContent(bb7.b());
        customizeWidget.setRemoveMode(false);
        customizeWidget.N();
        customizeWidget.T();
        customizeWidget.setInFocused(true);
        customizeWidget.setSetting(bb7.d());
        customizeWidget.setEventEditorListener(this);
        customizeWidget.setOrderIndex(c2);
        customizeWidget.setPreviousIndex(c2);
        addView(customizeWidget);
        this.E = customizeWidget;
        setCurrentComplication(customizeWidget);
        Db7 db7 = this.w;
        db7.b().add(bb7.a());
        db7.e(bb7.a());
        db7.g("");
        Eb7 eb7 = new Eb7(Ab7.ADDED, this.w);
        b bVar = this.I;
        if (bVar != null) {
            bVar.g(eb7);
        }
        customizeWidget.post(new c(this, customizeWidget));
        return true;
    }

    @DexIgnore
    public final boolean X(S87.Bi bi) {
        Wg6.c(bi, "stickerConfig");
        if (At7.g(At7.h(Po0.a(this), f.INSTANCE)) == 5) {
            return false;
        }
        Lc6<S67, ViewGroup.LayoutParams> a2 = S67.s.a(this, bi, c());
        S67 component1 = a2.component1();
        addView(component1, a2.component2());
        component1.post(new e(this, component1));
        return true;
    }

    @DexIgnore
    public final boolean Y(S87.Ci ci) {
        Wg6.c(ci, "textConfig");
        if (At7.g(At7.h(Po0.a(this), h.INSTANCE)) == 5) {
            return false;
        }
        Lc6<T67, ViewGroup.LayoutParams> a2 = T67.s.a(this, ci, c());
        T67 component1 = a2.component1();
        addView(component1, a2.component2());
        component1.post(new g(this, component1));
        return true;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0056 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0092  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.S87.Ai Z(com.portfolio.platform.view.CustomizeWidget r15, boolean r16) {
        /*
            r14 = this;
            android.graphics.Rect r0 = r14.y
            r2 = 0
            if (r0 == 0) goto L_0x00c4
            int r0 = r0.width()
        L_0x0009:
            com.fossil.Cb7 r1 = r15.getRing()
            if (r1 == 0) goto L_0x00c7
            java.lang.String r1 = r1.b()
        L_0x0013:
            com.fossil.Cb7 r3 = r15.getRing()
            if (r3 == 0) goto L_0x00d0
            java.lang.String r3 = r3.c()
            if (r3 == 0) goto L_0x0025
            boolean r3 = com.fossil.Vt7.l(r3)
            if (r3 == 0) goto L_0x00ca
        L_0x0025:
            r3 = 1
        L_0x0026:
            if (r3 != 0) goto L_0x00d0
            if (r1 == 0) goto L_0x0030
            boolean r3 = com.fossil.Vt7.l(r1)
            if (r3 == 0) goto L_0x00cd
        L_0x0030:
            r3 = 1
        L_0x0031:
            if (r3 != 0) goto L_0x00d0
            com.fossil.A97 r3 = com.fossil.A97.b
            android.graphics.Bitmap r3 = r3.c(r1)
            if (r3 == 0) goto L_0x00d0
            float r0 = (float) r0
            r4 = 1117257728(0x42980000, float:76.0)
            float r0 = r0 * r4
            r4 = 1131413504(0x43700000, float:240.0)
            float r0 = r0 / r4
            int r4 = r15.getWidth()
            float r4 = (float) r4
            float r0 = r0 / r4
            int r4 = r15.getWidth()
            float r4 = (float) r4
            float r0 = r0 * r4
            int r0 = (int) r0
            r4 = 0
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createScaledBitmap(r3, r0, r0, r4)
        L_0x0054:
            if (r0 != 0) goto L_0x00d2
            if (r1 == 0) goto L_0x005e
            boolean r3 = com.fossil.Vt7.l(r1)
            if (r3 == 0) goto L_0x005f
        L_0x005e:
            r2 = 1
        L_0x005f:
            if (r2 != 0) goto L_0x00d2
            com.fossil.A97 r0 = com.fossil.A97.b
            android.graphics.Bitmap r0 = r0.c(r1)
            r13 = r0
        L_0x0068:
            r0 = 2
            int[] r0 = new int[r0]
            r15.getLocationOnScreen(r0)
            com.fossil.W87 r0 = new com.fossil.W87
            float r1 = r15.getX()
            float r2 = r15.getY()
            int r3 = r15.getWidth()
            float r3 = (float) r3
            int r4 = r15.getHeight()
            float r4 = (float) r4
            float r5 = r15.getScaleFactor()
            r0.<init>(r1, r2, r3, r4, r5)
            r1 = 0
            r2 = 2
            r3 = 0
            com.fossil.W87 r8 = f0(r14, r0, r1, r2, r3)
            if (r16 == 0) goto L_0x00a0
            int r0 = r14.c()
            int r1 = r15.getOrderIndex()
            r15.setPreviousIndex(r1)
            r15.setOrderIndex(r0)
        L_0x00a0:
            com.fossil.S87$Ai r0 = new com.fossil.S87$Ai
            java.lang.String r1 = r15.getComplicationId()
            com.fossil.Cb7 r2 = r15.getRing()
            boolean r4 = r15.Q()
            r5 = 0
            r6 = 0
            com.fossil.O87 r7 = r15.getContentTheme()
            int r9 = r15.getOrderIndex()
            int r10 = r15.getPreviousIndex()
            r11 = 48
            r12 = 0
            r3 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            return r0
        L_0x00c4:
            r0 = 0
            goto L_0x0009
        L_0x00c7:
            r1 = 0
            goto L_0x0013
        L_0x00ca:
            r3 = 0
            goto L_0x0026
        L_0x00cd:
            r3 = 0
            goto L_0x0031
        L_0x00d0:
            r0 = 0
            goto L_0x0054
        L_0x00d2:
            r13 = r0
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.watchface.WatchFaceEditorView.Z(com.portfolio.platform.view.CustomizeWidget, boolean):com.fossil.S87$Ai");
    }

    @DexIgnore
    @Override // com.fossil.W67.Ai
    public void a() {
        b bVar = this.I;
        if (bVar != null) {
            bVar.a();
        }
    }

    @DexIgnore
    @Override // com.fossil.W67.Ai
    public void b() {
        b bVar = this.I;
        if (bVar != null) {
            bVar.b();
        }
    }

    @DexIgnore
    public final W87 b0(W87 w87, float f2) {
        if (f2 == 1.0f) {
            return new W87(((float) Math.ceil((double) (w87.d() * this.D))) + this.C, ((float) Math.ceil((double) (w87.e() * this.D))) + this.C, (float) Math.ceil((double) (w87.c() * this.D)), (float) Math.ceil((double) (w87.a() * this.D)), f2);
        }
        float ceil = (float) Math.ceil((double) (w87.c() * this.D));
        float ceil2 = (float) Math.ceil((double) (w87.a() * this.D));
        float f3 = ceil / f2;
        float f4 = ceil2 / f2;
        return new W87(((float) Math.ceil((double) (w87.d() * this.D))) + this.C + ((ceil - f3) / 2.0f), ((float) Math.ceil((double) (w87.e() * this.D))) + this.C + ((ceil2 - f4) / 2.0f), f3, f4, f2);
    }

    @DexIgnore
    @Override // com.fossil.W67.Ai
    public int c() {
        int i2 = this.G + 1;
        this.G = i2;
        return i2;
    }

    @DexIgnore
    @Override // com.fossil.W67.Ai
    public boolean d(float f2, float f3, float f4) {
        ImageView imageView = this.x;
        if (imageView != null) {
            return f2 * f4 > ((float) imageView.getWidth()) * 0.95f || f3 * f4 > ((float) imageView.getHeight()) * 0.95f;
        }
        return false;
    }

    @DexIgnore
    public final Lc6<Float, Float> d0(int i2, int i3) {
        float f2 = this.D;
        float f3 = (float) 2;
        float f4 = this.C;
        return new Lc6<>(Float.valueOf(((f2 - ((float) i2)) / f3) + f4), Float.valueOf(((f2 - ((float) i3)) / f3) + f4));
    }

    @DexIgnore
    @Override // com.fossil.W67.Ai
    public boolean e(W67 w67) {
        Wg6.c(w67, "element");
        return Wg6.a(this.J, w67);
    }

    @DexIgnore
    public final W87 e0(W87 w87, float f2) {
        float c2 = w87.c() * w87.b();
        float a2 = w87.a() * w87.b();
        float c3 = (c2 - w87.c()) / 2.0f;
        float d2 = w87.d();
        float f3 = this.C;
        float e2 = w87.e();
        float f4 = this.C;
        float f5 = this.D;
        return new W87(((d2 - c3) - f3) / f5, ((e2 - ((a2 - w87.a()) / 2.0f)) - f4) / f5, (c2 / f5) + f2, (a2 / f5) + f2, w87.b());
    }

    @DexIgnore
    @Override // com.fossil.W67.Ai
    public boolean f(float f2, float f3, float f4) {
        ImageView imageView = this.x;
        if (imageView != null) {
            return f2 * f4 < ((float) imageView.getWidth()) * 0.05f || f3 * f4 < ((float) imageView.getHeight()) * 0.05f;
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.W67.Ai
    public a g() {
        return new a(this.C, this.D);
    }

    @DexIgnore
    public final List<S87> g0() {
        ArrayList arrayList = new ArrayList();
        for (View view : Po0.a(this)) {
            if (view instanceof W67) {
                S87 d2 = W67.d((W67) view, false, 1, null);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchFaceEditorView", "exportElements - actual " + d2);
                S87 z0 = z0(d2);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("WatchFaceEditorView", "exportElements - scaled " + z0);
                arrayList.add(z0);
            } else if (view instanceof CustomizeWidget) {
                S87.Ai a0 = a0(this, (CustomizeWidget) view, false, 2, null);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("WatchFaceEditorView", "exportElements - complication " + a0);
                arrayList.add(a0);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final CustomizeWidget getCurrentComplication() {
        return this.F;
    }

    @DexIgnore
    public final W67 getCurrentFocused() {
        return this.J;
    }

    @DexIgnore
    public final float getOffset() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.Ac7
    public void h(Zb7 zb7, O87 o87) {
        Wg6.c(zb7, "component");
        Wg6.c(o87, BaseFeatureModel.COLUMN_COLOR);
        if (zb7 instanceof CustomizeWidget) {
            S87.Ai Z = Z(((CustomizeWidget) zb7).getView(), true);
            b bVar = this.I;
            if (bVar != null) {
                bVar.e(W67.Bi.UPDATED, Z);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00bd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void h0(android.view.View r10, com.fossil.R67 r11, android.view.MotionEvent r12) {
        /*
            r9 = this;
            r5 = 0
            r4 = 4
            r8 = 0
            r7 = 1
            r3 = 0
            int r0 = r10.getId()
            r1 = 2131363078(0x7f0a0506, float:1.8345955E38)
            if (r0 != r1) goto L_0x0011
            r11.q()
        L_0x0011:
            int r0 = r12.getAction()
            if (r0 == 0) goto L_0x00de
            if (r0 == r7) goto L_0x001d
            r1 = 2
            if (r0 == r1) goto L_0x00de
        L_0x001c:
            return
        L_0x001d:
            int r0 = r11.getWidth()
            float r0 = (float) r0
            float r1 = r11.getScaleX()
            int r2 = r11.getHeight()
            float r2 = (float) r2
            float r6 = r11.getScaleY()
            float r2 = r2 * r6
            float r1 = r1 * r0
            r0 = r9
            boolean r0 = com.fossil.W67.Ai.Aii.a(r0, r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x0083
            float r0 = r9.D
            r1 = 1064514355(0x3f733333, float:0.95)
            float r0 = r0 * r1
            int r1 = r11.getWidth()
            int r2 = r11.getHeight()
            int r1 = java.lang.Math.max(r1, r2)
        L_0x004a:
            float r1 = (float) r1
            float r0 = r0 / r1
        L_0x004c:
            boolean r1 = r11.a()
            com.portfolio.platform.view.watchface.WatchFaceEditorView$j r2 = new com.portfolio.platform.view.watchface.WatchFaceEditorView$j
            r2.<init>(r11)
            if (r1 == 0) goto L_0x00a0
            int r4 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r4 == 0) goto L_0x00a0
            r11.setAnimating$app_fossilRelease(r7)
            android.view.ViewPropertyAnimator r1 = r11.animate()
            float r3 = r11.getMPrevTranslateX()
            android.view.ViewPropertyAnimator r1 = r1.translationX(r3)
            float r3 = r11.getMPrevTranslateY()
            android.view.ViewPropertyAnimator r1 = r1.translationY(r3)
            android.view.ViewPropertyAnimator r1 = r1.scaleX(r0)
            android.view.ViewPropertyAnimator r0 = r1.scaleY(r0)
            r0.setListener(r2)
        L_0x007d:
            r11.setEditing$app_fossilRelease(r8)
            r9.H = r8
            goto L_0x001c
        L_0x0083:
            r0 = r9
            r1 = r2
            boolean r0 = com.fossil.W67.Ai.Aii.b(r0, r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x009e
            float r0 = r9.D
            r1 = 1028443341(0x3d4ccccd, float:0.05)
            float r0 = r0 * r1
            int r1 = r11.getWidth()
            int r2 = r11.getHeight()
            int r1 = java.lang.Math.min(r1, r2)
            goto L_0x004a
        L_0x009e:
            r0 = r3
            goto L_0x004c
        L_0x00a0:
            if (r1 == 0) goto L_0x00bd
            r11.setAnimating$app_fossilRelease(r7)
            android.view.ViewPropertyAnimator r0 = r11.animate()
            float r1 = r11.getMPrevTranslateX()
            android.view.ViewPropertyAnimator r0 = r0.translationX(r1)
            float r1 = r11.getMPrevTranslateY()
            android.view.ViewPropertyAnimator r0 = r0.translationY(r1)
            r0.setListener(r2)
            goto L_0x007d
        L_0x00bd:
            int r1 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r1 == 0) goto L_0x00d4
            r11.setAnimating$app_fossilRelease(r7)
            android.view.ViewPropertyAnimator r1 = r11.animate()
            android.view.ViewPropertyAnimator r1 = r1.scaleX(r0)
            android.view.ViewPropertyAnimator r0 = r1.scaleY(r0)
            r0.setListener(r2)
            goto L_0x007d
        L_0x00d4:
            r11.setElevation(r3)
            r11.e()
            r11.m()
            goto L_0x007d
        L_0x00de:
            r11.setEditing$app_fossilRelease(r7)
            com.fossil.O67$Bi r0 = com.fossil.O67.s
            float r0 = r0.c()
            r11.setElevation(r0)
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.watchface.WatchFaceEditorView.h0(android.view.View, com.fossil.R67, android.view.MotionEvent):void");
    }

    @DexIgnore
    @Override // com.fossil.Ac7
    public void i(Zb7 zb7) {
        Wg6.c(zb7, "component");
        b bVar = this.I;
        if (bVar != null) {
            bVar.d(zb7);
        }
    }

    @DexIgnore
    public final void i0(S87.Ai ai) {
        W87 b2 = ai.b();
        if (b2 != null) {
            float d2 = b2.d();
            W87 b3 = ai.b();
            if (b3 != null) {
                CustomizeWidget customizeWidget = new CustomizeWidget(getContext(), new Rect(this.y), d2, b3.e(), this.C, null, 32, null);
                customizeWidget.setSetting(ai.l());
                customizeWidget.O(ai.i());
                customizeWidget.setContentTheme(ai.h());
                customizeWidget.setBottomContent(ai.j());
                customizeWidget.Z(ai.k(), ai.g());
                customizeWidget.Y(ai.m());
                customizeWidget.setRemoveMode(false);
                customizeWidget.N();
                customizeWidget.T();
                customizeWidget.setEventEditorListener(this);
                int c2 = c();
                customizeWidget.setOrderIndex(c2);
                customizeWidget.setPreviousIndex(c2);
                addView(customizeWidget);
                return;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.W67.Ai
    public boolean j(MotionEvent motionEvent) {
        Wg6.c(motionEvent, Constants.EVENT);
        Rect rect = this.B;
        if (rect != null) {
            return P67.a(rect, motionEvent);
        }
        return false;
    }

    @DexIgnore
    public final void j0(List<? extends S87> list) {
        Wg6.c(list, "elementList");
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceEditorView", "importElements - scaled " + ((Object) it.next()));
        }
        ArrayList<S87> arrayList = new ArrayList(Im7.m(list, 10));
        Iterator<T> it2 = list.iterator();
        while (it2.hasNext()) {
            arrayList.add(y0(it2.next()));
        }
        for (S87 s87 : arrayList) {
            if (s87 instanceof S87.Ci) {
                l0((S87.Ci) s87);
            } else if (s87 instanceof S87.Bi) {
                k0((S87.Bi) s87);
            } else if (s87 instanceof S87.Ai) {
                S87.Ai ai = (S87.Ai) s87;
                i0(ai);
                this.w.b().add(ai.i());
            }
        }
        if (!list.isEmpty()) {
            this.G = ((S87) Pm7.P(list)).a();
        }
        Eb7 eb7 = new Eb7(Ab7.ADDED, this.w);
        b bVar = this.I;
        if (bVar != null) {
            bVar.g(eb7);
        }
    }

    @DexIgnore
    @Override // com.fossil.W67.Ai
    public void k(S87 s87) {
        Wg6.c(s87, "elementConfig");
        b bVar = this.I;
        if (bVar != null) {
            bVar.e(W67.Bi.UPDATED, z0(s87));
        }
    }

    @DexIgnore
    public final void k0(S87.Bi bi) {
        Lc6<S67, ViewGroup.LayoutParams> a2 = S67.s.a(this, bi, c());
        S67 component1 = a2.component1();
        addView(component1, a2.component2());
        component1.post(new k(bi, component1));
    }

    @DexIgnore
    @Override // com.fossil.W67.Ai
    public void l(W67 w67) {
        Wg6.c(w67, "element");
        S87 z0 = z0(W67.d(w67, false, 1, null));
        b bVar = this.I;
        if (bVar != null) {
            bVar.e(W67.Bi.REMOVED, z0);
        }
        removeView(w67);
        setCurrentElement(null);
    }

    @DexIgnore
    public final void l0(S87.Ci ci) {
        Lc6<T67, ViewGroup.LayoutParams> a2 = T67.s.a(this, ci, c());
        T67 component1 = a2.component1();
        addView(component1, a2.component2());
        component1.post(new l(ci, component1));
    }

    @DexIgnore
    @Override // com.fossil.W67.Ai
    public void m(T67 t67) {
        Wg6.c(t67, "textElement");
        S87 d2 = W67.d(t67, false, 1, null);
        if (d2 != null) {
            S87.Ci ci = (S87.Ci) d2;
            b bVar = this.I;
            if (bVar != null) {
                bVar.f(ci);
                return;
            }
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.watchface.edit.model.ElementConfig.TextConfig");
    }

    @DexIgnore
    public final void m0(View view, b bVar) {
        Wg6.c(view, "deleteView");
        Wg6.c(bVar, "editorEventListener");
        this.I = bVar;
        view.post(new m(view, this));
        this.A = view;
        findViewById(2131363078).setOnTouchListener(new o(this));
        ImageView imageView = (ImageView) findViewById(2131361908);
        imageView.post(new n(imageView, this, bVar));
        this.x = imageView;
    }

    @DexIgnore
    @Override // com.fossil.W67.Ai
    public boolean n(W67 w67) {
        Wg6.c(w67, "element");
        Rect b2 = P67.b(w67);
        N67 n67 = this.z;
        if (n67 != null) {
            return n67.a(b2);
        }
        return false;
    }

    @DexIgnore
    public final void n0(String str, Bitmap bitmap) {
        BitmapDrawable bitmapDrawable;
        if (str != null) {
            if (bitmap != null) {
                Resources resources = getResources();
                Wg6.b(resources, "resources");
                bitmapDrawable = new BitmapDrawable(resources, bitmap);
            } else {
                bitmapDrawable = null;
            }
            s0(str, bitmapDrawable);
        } else if (bitmap != null) {
            setBackground(bitmap);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ac7
    public void o(Zb7 zb7, float f2, float f3) {
        String str;
        Wg6.c(zb7, "component");
        b bVar = this.I;
        String str2 = null;
        if (bVar != null ? bVar.c(zb7) : false) {
            if (zb7.getType() == W67.Ci.COMPLICATION) {
                S87.Ai a0 = a0(this, (CustomizeWidget) zb7, false, 2, null);
                b bVar2 = this.I;
                if (bVar2 != null) {
                    bVar2.e(W67.Bi.REMOVED, a0);
                }
            }
        } else if (V67.a[zb7.getType().ordinal()] == 1) {
            CustomizeWidget customizeWidget = (CustomizeWidget) (!(zb7 instanceof CustomizeWidget) ? null : zb7);
            this.E = this.F;
            setCurrentComplication(customizeWidget);
            CustomizeWidget customizeWidget2 = this.F;
            if (customizeWidget2 != null) {
                CustomizeWidget customizeWidget3 = this.E;
                String complicationId = customizeWidget3 != null ? customizeWidget3.getComplicationId() : null;
                CustomizeWidget customizeWidget4 = this.F;
                if (customizeWidget4 != null) {
                    str2 = customizeWidget4.getComplicationId();
                }
                customizeWidget2.setInFocused(Wg6.a(complicationId, str2));
            }
            CustomizeWidget customizeWidget5 = this.F;
            if (customizeWidget5 != null) {
                float x2 = customizeWidget5.getX();
                float width = (float) (customizeWidget5.getWidth() / 2);
                ImageView imageView = this.x;
                float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                float x3 = imageView != null ? imageView.getX() : 0.0f;
                float y2 = customizeWidget5.getY();
                float height = (float) (customizeWidget5.getHeight() / 2);
                ImageView imageView2 = this.x;
                if (imageView2 != null) {
                    f4 = imageView2.getY();
                }
                int width2 = customizeWidget5.getWidth() / 2;
                ViewGroup.LayoutParams layoutParams = customizeWidget5.getLayoutParams();
                if (!p0(new Za7((x2 + width) - x3, (y2 + height) - f4, (int) (((float) ((width2 - (layoutParams instanceof ViewGroup.MarginLayoutParams ? Zn0.b((ViewGroup.MarginLayoutParams) layoutParams) : 0)) - customizeWidget5.getPaddingStart())) * customizeWidget5.getScaleFactor())))) {
                    customizeWidget5.animate().x(f2).y(f3).setDuration(0).start();
                }
            }
            if (customizeWidget != null) {
                String complicationId2 = customizeWidget.getComplicationId();
                Cb7 ring = customizeWidget.getRing();
                if (ring == null || (str = ring.b()) == null) {
                    str = "";
                }
                boolean Q = customizeWidget.Q();
                Db7 db7 = this.w;
                db7.e(complicationId2);
                db7.g(str);
                db7.f(Q);
                Eb7 eb7 = new Eb7(Ab7.SELECTED, this.w);
                b bVar3 = this.I;
                if (bVar3 != null) {
                    bVar3.g(eb7);
                }
                S87.Ai Z = Z(customizeWidget, true);
                b bVar4 = this.I;
                if (bVar4 != null) {
                    bVar4.e(W67.Bi.UPDATED, Z);
                    return;
                }
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.view.CustomizeWidget");
        }
    }

    @DexIgnore
    public final boolean o0(String str) {
        Wg6.c(str, "complicationId");
        boolean contains = this.w.b().contains(str);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceEditorView", str + " isExisted = " + contains);
        return contains;
    }

    @DexIgnore
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        Wg6.c(motionEvent, Constants.EVENT);
        W67 w67 = this.J;
        return (w67 instanceof R67) && w67 != null && w67.j() && motionEvent.getPointerCount() > 1;
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        Wg6.c(motionEvent, Constants.EVENT);
        this.N.onTouchEvent(motionEvent);
        W67 w67 = this.J;
        if (!(w67 instanceof R67)) {
            w67 = null;
        }
        R67 r67 = (R67) w67;
        if (r67 != null) {
            h0(this, r67, motionEvent);
        }
        return super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    @Override // com.fossil.Ac7
    public boolean p(Zb7 zb7) {
        Wg6.c(zb7, "component");
        return false;
    }

    @DexIgnore
    public final boolean p0(Za7 za7) {
        Wg6.c(za7, ViewHierarchy.DIMENSION_KEY);
        ImageView imageView = this.x;
        if (imageView == null) {
            return false;
        }
        int width = imageView.getWidth() / 2;
        double d2 = (double) 2;
        return ((float) Math.pow((double) (((float) Math.pow((double) (((float) (imageView.getHeight() / 2)) - za7.b()), d2)) + ((float) Math.pow((double) (((float) width) - za7.a()), d2))), (double) 0.5f)) < ((float) (width + za7.c()));
    }

    @DexIgnore
    @Override // com.fossil.W67.Ai
    public boolean q(W67 w67) {
        Wg6.c(w67, "element");
        W67 w672 = this.J;
        if (w672 == null) {
            setCurrentElement(w67);
            return true;
        } else if (w672 == null || w672.j()) {
            return false;
        } else {
            setCurrentElement(w67);
            return true;
        }
    }

    @DexIgnore
    public final void q0() {
        for (View view : Po0.a(this)) {
            if (view instanceof S67) {
                ((S67) view).y();
            }
        }
    }

    @DexIgnore
    public final void r0(Zb7 zb7) {
        Wg6.c(zb7, "component");
        this.E = null;
        setCurrentComplication(null);
        if (zb7 instanceof CustomizeWidget) {
            CustomizeWidget customizeWidget = (CustomizeWidget) zb7;
            customizeWidget.U();
            Db7 db7 = this.w;
            db7.b().remove(customizeWidget.getComplicationId());
            db7.e(null);
            db7.g("");
            db7.f(false);
            Eb7 eb7 = new Eb7(Ab7.REMOVED, this.w);
            b bVar = this.I;
            if (bVar != null) {
                bVar.g(eb7);
            }
        }
        removeView(zb7.getView());
    }

    @DexIgnore
    public final void s0(String str, Drawable drawable) {
        ImageView imageView = this.x;
        if (imageView != null) {
            Ty4.a(imageView, str, drawable);
        }
    }

    @DexIgnore
    public final void setBackground(Bitmap bitmap) {
        Wg6.c(bitmap, "bitmap");
        ImageView imageView = this.x;
        if (imageView != null) {
            imageView.setImageBitmap(bitmap);
        }
    }

    @DexIgnore
    public final void setWatchFaceBackground(Drawable drawable) {
        ImageView imageView = this.x;
        if (imageView != null) {
            imageView.setImageDrawable(drawable);
        }
    }

    @DexIgnore
    public final void t0(String str, Typeface typeface, O87 o87) {
        W67 w67 = this.J;
        if (!(w67 instanceof T67)) {
            w67 = null;
        }
        T67 t67 = (T67) w67;
        if (t67 != null) {
            t67.B(str, typeface, o87);
        }
    }

    @DexIgnore
    public final void v0(boolean z2) {
        CustomizeWidget customizeWidget = this.F;
        if (customizeWidget != null) {
            customizeWidget.Y(z2);
            S87.Ai Z = Z(customizeWidget, true);
            b bVar = this.I;
            if (bVar != null) {
                bVar.e(W67.Bi.UPDATED, Z);
            }
        }
    }

    @DexIgnore
    public final void w0(Cb7 cb7) {
        CustomizeWidget customizeWidget = this.F;
        if (customizeWidget != null) {
            customizeWidget.Z(cb7, null);
            S87.Ai Z = Z(customizeWidget, true);
            b bVar = this.I;
            if (bVar != null) {
                bVar.e(W67.Bi.UPDATED, Z);
            }
        }
    }

    @DexIgnore
    public final void x0(Bb7 bb7) {
        String str;
        boolean z2 = false;
        Wg6.c(bb7, "complication");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceEditorView", "updateSelectedComplication, complication = " + bb7);
        if (this.F != null) {
            boolean e2 = Ik5.d.e(bb7.a());
            CustomizeWidget customizeWidget = this.F;
            if (customizeWidget != null) {
                boolean z3 = customizeWidget.Q() && !e2;
                HashSet<String> b2 = this.w.b();
                CustomizeWidget customizeWidget2 = this.F;
                if (customizeWidget2 != null) {
                    b2.remove(customizeWidget2.getComplicationId());
                    CustomizeWidget customizeWidget3 = this.E;
                    if (customizeWidget3 != null) {
                        customizeWidget3.setInFocused(false);
                    }
                    CustomizeWidget customizeWidget4 = this.F;
                    this.E = customizeWidget4;
                    boolean Q = customizeWidget4 != null ? customizeWidget4.Q() : false;
                    CustomizeWidget customizeWidget5 = this.E;
                    Cb7 ring = customizeWidget5 != null ? customizeWidget5.getRing() : null;
                    CustomizeWidget customizeWidget6 = this.F;
                    if (customizeWidget6 != null) {
                        customizeWidget6.setInFocused(true);
                        customizeWidget6.setSetting(bb7.d());
                        customizeWidget6.O(bb7.a());
                        customizeWidget6.setBottomContent(bb7.b());
                        if (z3) {
                            customizeWidget6.Z(null, null);
                            customizeWidget6.Y(false);
                        } else {
                            customizeWidget6.Z(ring, null);
                            customizeWidget6.Y(Q);
                        }
                        customizeWidget6.setRemoveMode(false);
                        customizeWidget6.T();
                    }
                    Db7 db7 = this.w;
                    db7.b().add(bb7.a());
                    db7.e(bb7.a());
                    if (ring == null || (str = ring.b()) == null) {
                        str = "";
                    }
                    db7.g(str);
                    if (!z3) {
                        z2 = Q;
                    }
                    db7.f(z2);
                    Eb7 eb7 = new Eb7(Ab7.SELECTED, this.w);
                    b bVar = this.I;
                    if (bVar != null) {
                        bVar.g(eb7);
                    }
                    CustomizeWidget customizeWidget7 = this.F;
                    if (customizeWidget7 != null) {
                        S87.Ai Z = Z(customizeWidget7, true);
                        b bVar2 = this.I;
                        if (bVar2 != null) {
                            bVar2.e(W67.Bi.UPDATED, Z);
                            return;
                        }
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final S87 y0(S87 s87) {
        if (s87 instanceof S87.Ci) {
            S87.Ci ci = (S87.Ci) s87;
            float floor = ((float) Math.floor((double) (ci.h() * this.D))) / 60.0f;
            W87 b2 = s87.b();
            if (b2 != null) {
                return S87.Ci.f(ci, null, null, 60.0f, null, b0(b2, floor), 0, 0, 107, null);
            }
            Wg6.i();
            throw null;
        } else if (s87 instanceof S87.Bi) {
            W87 b3 = s87.b();
            if (b3 != null) {
                return S87.Bi.g((S87.Bi) s87, null, null, null, c0(this, b3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 2, null), 0, 0, 55, null);
            }
            Wg6.i();
            throw null;
        } else if (s87 instanceof S87.Ai) {
            return S87.Ai.f((S87.Ai) s87, null, null, null, false, null, null, null, null, 0, 0, BasicChronology.CACHE_MASK, null);
        } else {
            throw new Kc6();
        }
    }

    @DexIgnore
    public final S87 z0(S87 s87) {
        if (s87 instanceof S87.Ci) {
            W87 b2 = s87.b();
            if (b2 != null) {
                float ceil = ((float) Math.ceil((double) b2.b())) * 0.02f;
                if (ceil > 0.06f) {
                    ceil = 0.06f;
                }
                W87 b3 = s87.b();
                if (b3 != null) {
                    W87 e0 = e0(b3, ceil);
                    S87.Ci ci = (S87.Ci) s87;
                    float h2 = ci.h();
                    W87 b4 = s87.b();
                    if (b4 != null) {
                        return S87.Ci.f(ci, null, null, (h2 * b4.b()) / this.D, null, e0, 0, 0, 107, null);
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        } else if (s87 instanceof S87.Bi) {
            W87 b5 = s87.b();
            if (b5 != null) {
                return S87.Bi.g((S87.Bi) s87, null, null, null, f0(this, b5, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 2, null), 0, 0, 55, null);
            }
            Wg6.i();
            throw null;
        } else if (s87 instanceof S87.Ai) {
            W87 b6 = s87.b();
            if (b6 != null) {
                return S87.Ai.f((S87.Ai) s87, null, null, null, false, null, null, null, f0(this, b6, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 2, null), 0, 0, 895, null);
            }
            Wg6.i();
            throw null;
        } else {
            throw new Kc6();
        }
    }
}
