package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Py5;
import com.mapped.X24;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ColorPickerView extends View {
    @DexIgnore
    public float A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public String C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public Rect G;
    @DexIgnore
    public Rect H;
    @DexIgnore
    public Rect I;
    @DexIgnore
    public Rect J;
    @DexIgnore
    public Point K;
    @DexIgnore
    public c L;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public Paint h;
    @DexIgnore
    public Paint i;
    @DexIgnore
    public Paint j;
    @DexIgnore
    public Paint k;
    @DexIgnore
    public Paint l;
    @DexIgnore
    public Paint m;
    @DexIgnore
    public Shader s;
    @DexIgnore
    public Shader t;
    @DexIgnore
    public Shader u;
    @DexIgnore
    public b v;
    @DexIgnore
    public b w;
    @DexIgnore
    public int x;
    @DexIgnore
    public float y;
    @DexIgnore
    public float z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public Canvas a;
        @DexIgnore
        public Bitmap b;
        @DexIgnore
        public float c;

        @DexIgnore
        public b() {
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void K5(int i);
    }

    @DexIgnore
    public ColorPickerView(Context context) {
        this(context, null);
    }

    @DexIgnore
    public ColorPickerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public ColorPickerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.x = 255;
        this.y = 360.0f;
        this.z = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.A = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.B = false;
        this.C = null;
        this.D = -4342339;
        this.E = -9539986;
        this.K = null;
        g(context, attributeSet);
    }

    @DexIgnore
    private int getPreferredHeight() {
        int a2 = Py5.a(getContext(), 200.0f);
        return this.B ? a2 + this.d + this.c : a2;
    }

    @DexIgnore
    private int getPreferredWidth() {
        return Py5.a(getContext(), 200.0f) + this.b + this.d;
    }

    @DexIgnore
    public final Point a(int i2) {
        Rect rect = this.J;
        float width = (float) rect.width();
        Point point = new Point();
        point.x = (int) ((width - ((((float) i2) * width) / 255.0f)) + ((float) rect.left));
        point.y = rect.top;
        return point;
    }

    @DexIgnore
    public final void b(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new TypedValue().data, new int[]{16842808});
        if (this.E == -9539986) {
            this.E = obtainStyledAttributes.getColor(0, -9539986);
        }
        if (this.D == -4342339) {
            this.D = obtainStyledAttributes.getColor(0, -4342339);
        }
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        Rect rect;
        if (this.B && (rect = this.J) != null) {
            this.m.setColor(this.E);
            canvas.drawRect((float) (rect.left - 1), (float) (rect.top - 1), (float) (rect.right + 1), (float) (rect.bottom + 1), this.m);
            float[] fArr = {this.y, this.z, this.A};
            int HSVToColor = Color.HSVToColor(fArr);
            int HSVToColor2 = Color.HSVToColor(0, fArr);
            int i2 = rect.top;
            LinearGradient linearGradient = new LinearGradient((float) rect.left, (float) i2, (float) rect.right, (float) i2, HSVToColor, HSVToColor2, Shader.TileMode.CLAMP);
            this.u = linearGradient;
            this.j.setShader(linearGradient);
            canvas.drawRect(rect, this.j);
            String str = this.C;
            if (str != null && !str.equals("")) {
                canvas.drawText(this.C, (float) rect.centerX(), (float) (rect.centerY() + Py5.a(getContext(), 4.0f)), this.k);
            }
            Point a2 = a(this.x);
            RectF rectF = new RectF();
            int i3 = a2.x;
            int i4 = this.g;
            rectF.left = (float) (i3 - (i4 / 2));
            rectF.right = (float) (i3 + (i4 / 2));
            int i5 = rect.top;
            int i6 = this.f;
            rectF.top = (float) (i5 - i6);
            rectF.bottom = (float) (rect.bottom + i6);
            canvas.drawRoundRect(rectF, 2.0f, 2.0f, this.l);
        }
    }

    @DexIgnore
    public final void d(Canvas canvas) {
        Rect rect = this.I;
        this.m.setColor(this.E);
        canvas.drawRect((float) (rect.left - 1), (float) (rect.top - 1), (float) (rect.right + 1), (float) (rect.bottom + 1), this.m);
        if (this.w == null) {
            b bVar = new b();
            this.w = bVar;
            bVar.b = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            this.w.a = new Canvas(this.w.b);
            int height = (int) (((float) rect.height()) + 0.5f);
            int[] iArr = new int[height];
            float f2 = 360.0f;
            for (int i2 = 0; i2 < height; i2++) {
                iArr[i2] = Color.HSVToColor(new float[]{f2, 1.0f, 1.0f});
                f2 -= 360.0f / ((float) height);
            }
            Paint paint = new Paint();
            paint.setStrokeWidth(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            for (int i3 = 0; i3 < height; i3++) {
                paint.setColor(iArr[i3]);
                b bVar2 = this.w;
                float f3 = (float) i3;
                bVar2.a.drawLine(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3, (float) bVar2.b.getWidth(), f3, paint);
            }
        }
        canvas.drawBitmap(this.w.b, (Rect) null, rect, (Paint) null);
        Point f4 = f(this.y);
        RectF rectF = new RectF();
        int i4 = rect.left;
        int i5 = this.f;
        rectF.left = (float) (i4 - i5);
        rectF.right = (float) (rect.right + i5);
        int i6 = f4.y;
        int i7 = this.g;
        rectF.top = (float) (i6 - (i7 / 2));
        rectF.bottom = (float) (i6 + (i7 / 2));
        canvas.drawRoundRect(rectF, 2.0f, 2.0f, this.l);
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        Rect rect = this.H;
        this.m.setColor(this.E);
        Rect rect2 = this.G;
        canvas.drawRect((float) rect2.left, (float) rect2.top, (float) (rect.right + 1), (float) (rect.bottom + 1), this.m);
        if (this.s == null) {
            int i2 = rect.left;
            this.s = new LinearGradient((float) i2, (float) rect.top, (float) i2, (float) rect.bottom, -1, -16777216, Shader.TileMode.CLAMP);
        }
        b bVar = this.v;
        if (bVar == null || bVar.c != this.y) {
            if (this.v == null) {
                this.v = new b();
            }
            b bVar2 = this.v;
            if (bVar2.b == null) {
                bVar2.b = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            }
            b bVar3 = this.v;
            if (bVar3.a == null) {
                bVar3.a = new Canvas(this.v.b);
            }
            int HSVToColor = Color.HSVToColor(new float[]{this.y, 1.0f, 1.0f});
            int i3 = rect.top;
            this.t = new LinearGradient((float) rect.left, (float) i3, (float) rect.right, (float) i3, -1, HSVToColor, Shader.TileMode.CLAMP);
            this.h.setShader(new ComposeShader(this.s, this.t, PorterDuff.Mode.MULTIPLY));
            b bVar4 = this.v;
            bVar4.a.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) bVar4.b.getWidth(), (float) this.v.b.getHeight(), this.h);
            this.v.c = this.y;
        }
        canvas.drawBitmap(this.v.b, (Rect) null, rect, (Paint) null);
        Point m2 = m(this.z, this.A);
        this.i.setColor(-16777216);
        canvas.drawCircle((float) m2.x, (float) m2.y, (float) (this.e - Py5.a(getContext(), 1.0f)), this.i);
        this.i.setColor(-2236963);
        canvas.drawCircle((float) m2.x, (float) m2.y, (float) this.e, this.i);
    }

    @DexIgnore
    public final Point f(float f2) {
        Rect rect = this.I;
        float height = (float) rect.height();
        Point point = new Point();
        point.y = (int) ((height - ((f2 * height) / 360.0f)) + ((float) rect.top));
        point.x = rect.left;
        return point;
    }

    @DexIgnore
    public final void g(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, X24.ColorPickerView);
        this.B = obtainStyledAttributes.getBoolean(1, false);
        this.D = obtainStyledAttributes.getColor(3, -4342339);
        this.E = obtainStyledAttributes.getColor(2, -9539986);
        obtainStyledAttributes.recycle();
        b(context);
        this.b = Py5.a(getContext(), 30.0f);
        this.c = Py5.a(getContext(), 20.0f);
        this.d = Py5.a(getContext(), 10.0f);
        this.e = Py5.a(getContext(), 5.0f);
        this.g = Py5.a(getContext(), 4.0f);
        this.f = Py5.a(getContext(), 2.0f);
        this.F = getResources().getDimensionPixelSize(2131165316);
        h();
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    @DexIgnore
    public String getAlphaSliderText() {
        return this.C;
    }

    @DexIgnore
    public int getBorderColor() {
        return this.E;
    }

    @DexIgnore
    public int getColor() {
        return Color.HSVToColor(this.x, new float[]{this.y, this.z, this.A});
    }

    @DexIgnore
    public int getPaddingBottom() {
        return Math.max(super.getPaddingBottom(), this.F);
    }

    @DexIgnore
    public int getPaddingLeft() {
        return Math.max(super.getPaddingLeft(), this.F);
    }

    @DexIgnore
    public int getPaddingRight() {
        return Math.max(super.getPaddingRight(), this.F);
    }

    @DexIgnore
    public int getPaddingTop() {
        return Math.max(super.getPaddingTop(), this.F);
    }

    @DexIgnore
    public int getSliderTrackerColor() {
        return this.D;
    }

    @DexIgnore
    public final void h() {
        this.h = new Paint();
        this.i = new Paint();
        this.l = new Paint();
        this.j = new Paint();
        this.k = new Paint();
        this.m = new Paint();
        this.i.setStyle(Paint.Style.STROKE);
        this.i.setStrokeWidth((float) Py5.a(getContext(), 2.0f));
        this.i.setAntiAlias(true);
        this.l.setColor(this.D);
        this.l.setStyle(Paint.Style.STROKE);
        this.l.setStrokeWidth((float) Py5.a(getContext(), 2.0f));
        this.l.setAntiAlias(true);
        this.k.setColor(-14935012);
        this.k.setTextSize((float) Py5.a(getContext(), 14.0f));
        this.k.setAntiAlias(true);
        this.k.setTextAlign(Paint.Align.CENTER);
        this.k.setFakeBoldText(true);
    }

    @DexIgnore
    public final boolean i(MotionEvent motionEvent) {
        Point point = this.K;
        if (point == null) {
            return false;
        }
        int i2 = point.x;
        int i3 = point.y;
        if (this.I.contains(i2, i3)) {
            this.y = k(motionEvent.getY());
        } else if (this.H.contains(i2, i3)) {
            float[] l2 = l(motionEvent.getX(), motionEvent.getY());
            this.z = l2[0];
            this.A = l2[1];
        } else {
            Rect rect = this.J;
            if (rect == null || !rect.contains(i2, i3)) {
                return false;
            }
            this.x = j((int) motionEvent.getX());
        }
        return true;
    }

    @DexIgnore
    public final int j(int i2) {
        Rect rect = this.J;
        int width = rect.width();
        int i3 = rect.left;
        return 255 - (((i2 < i3 ? 0 : i2 > rect.right ? width : i2 - i3) * 255) / width);
    }

    @DexIgnore
    public final float k(float f2) {
        Rect rect = this.I;
        float height = (float) rect.height();
        int i2 = rect.top;
        return 360.0f - (((f2 < ((float) i2) ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : f2 > ((float) rect.bottom) ? height : f2 - ((float) i2)) * 360.0f) / height);
    }

    @DexIgnore
    public final float[] l(float f2, float f3) {
        float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        Rect rect = this.H;
        float width = (float) rect.width();
        float height = (float) rect.height();
        int i2 = rect.left;
        float f5 = f2 < ((float) i2) ? 0.0f : f2 > ((float) rect.right) ? width : f2 - ((float) i2);
        int i3 = rect.top;
        if (f3 >= ((float) i3)) {
            f4 = f3 > ((float) rect.bottom) ? height : f3 - ((float) i3);
        }
        return new float[]{f5 * (1.0f / width), 1.0f - (f4 * (1.0f / height))};
    }

    @DexIgnore
    public final Point m(float f2, float f3) {
        Rect rect = this.H;
        Point point = new Point();
        point.x = (int) ((((float) rect.width()) * f2) + ((float) rect.left));
        point.y = (int) (((float) rect.top) + (((float) rect.height()) * (1.0f - f3)));
        return point;
    }

    @DexIgnore
    public void n(int i2, boolean z2) {
        c cVar;
        int alpha = Color.alpha(i2);
        float[] fArr = new float[3];
        Color.RGBToHSV(Color.red(i2), Color.green(i2), Color.blue(i2), fArr);
        this.x = alpha;
        float f2 = fArr[0];
        this.y = f2;
        float f3 = fArr[1];
        this.z = f3;
        float f4 = fArr[2];
        this.A = f4;
        if (z2 && (cVar = this.L) != null) {
            cVar.K5(Color.HSVToColor(alpha, new float[]{f2, f3, f4}));
        }
        invalidate();
    }

    @DexIgnore
    public final void o() {
        if (this.B) {
            Rect rect = this.G;
            int i2 = rect.left;
            int i3 = rect.bottom;
            this.J = new Rect(i2 + 1, (i3 - this.c) + 1, rect.right - 1, i3 - 1);
        }
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (this.G.width() > 0 && this.G.height() > 0) {
            e(canvas);
            d(canvas);
            c(canvas);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0088, code lost:
        if (r4 != false) goto L_0x007a;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r10, int r11) {
        /*
            r9 = this;
            r4 = 1
            r5 = 0
            r7 = 1073741824(0x40000000, float:2.0)
            int r0 = android.view.View.MeasureSpec.getMode(r10)
            int r2 = android.view.View.MeasureSpec.getMode(r11)
            int r1 = android.view.View.MeasureSpec.getSize(r10)
            int r3 = r9.getPaddingLeft()
            int r1 = r1 - r3
            int r3 = r9.getPaddingRight()
            int r1 = r1 - r3
            int r3 = android.view.View.MeasureSpec.getSize(r11)
            int r6 = r9.getPaddingBottom()
            int r3 = r3 - r6
            int r6 = r9.getPaddingTop()
            int r3 = r3 - r6
            if (r0 == r7) goto L_0x002c
            if (r2 != r7) goto L_0x005b
        L_0x002c:
            if (r0 != r7) goto L_0x008f
            if (r2 == r7) goto L_0x008f
            int r0 = r9.d
            int r2 = r1 - r0
            int r4 = r9.b
            int r2 = r2 - r4
            boolean r4 = r9.B
            if (r4 == 0) goto L_0x003f
            int r4 = r9.c
            int r0 = r0 + r4
            int r2 = r2 + r0
        L_0x003f:
            if (r2 <= r3) goto L_0x008d
            r0 = r1
            r2 = r3
        L_0x0043:
            int r1 = r9.getPaddingLeft()
            int r0 = r0 + r1
            int r1 = r9.getPaddingRight()
            int r0 = r0 + r1
            int r1 = r9.getPaddingTop()
            int r1 = r1 + r2
            int r2 = r9.getPaddingBottom()
            int r1 = r1 + r2
            r9.setMeasuredDimension(r0, r1)
            return
        L_0x005b:
            int r6 = r9.d
            int r2 = r9.b
            int r0 = r3 + r6
            int r0 = r0 + r2
            int r7 = r1 - r6
            int r2 = r7 - r2
            boolean r7 = r9.B
            if (r7 == 0) goto L_0x0071
            int r7 = r9.c
            int r8 = r6 + r7
            int r0 = r0 - r8
            int r6 = r6 + r7
            int r2 = r2 + r6
        L_0x0071:
            if (r0 > r1) goto L_0x007c
            r6 = r4
        L_0x0074:
            if (r2 > r3) goto L_0x007e
        L_0x0076:
            if (r6 == 0) goto L_0x0080
            if (r4 == 0) goto L_0x0080
        L_0x007a:
            r0 = r1
            goto L_0x0043
        L_0x007c:
            r6 = r5
            goto L_0x0074
        L_0x007e:
            r4 = r5
            goto L_0x0076
        L_0x0080:
            if (r4 != 0) goto L_0x0086
            if (r6 == 0) goto L_0x0086
        L_0x0084:
            r2 = r3
            goto L_0x0043
        L_0x0086:
            if (r6 != 0) goto L_0x008a
            if (r4 != 0) goto L_0x007a
        L_0x008a:
            r0 = r1
            r2 = r3
            goto L_0x0043
        L_0x008d:
            r0 = r1
            goto L_0x0043
        L_0x008f:
            if (r2 != r7) goto L_0x008a
            if (r0 == r7) goto L_0x008a
            int r2 = r9.d
            int r0 = r3 + r2
            int r4 = r9.b
            int r0 = r0 + r4
            boolean r4 = r9.B
            if (r4 == 0) goto L_0x00a2
            int r4 = r9.c
            int r2 = r2 + r4
            int r0 = r0 - r2
        L_0x00a2:
            if (r0 <= r1) goto L_0x0084
            r0 = r1
            r2 = r3
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.view.ColorPickerView.onMeasure(int, int):void");
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.x = bundle.getInt("alpha");
            this.y = bundle.getFloat("hue");
            this.z = bundle.getFloat("sat");
            this.A = bundle.getFloat("val");
            this.B = bundle.getBoolean("show_alpha");
            this.C = bundle.getString("alpha_text");
            parcelable = bundle.getParcelable("instanceState");
        }
        super.onRestoreInstanceState(parcelable);
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putInt("alpha", this.x);
        bundle.putFloat("hue", this.y);
        bundle.putFloat("sat", this.z);
        bundle.putFloat("val", this.A);
        bundle.putBoolean("show_alpha", this.B);
        bundle.putString("alpha_text", this.C);
        return bundle;
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        Rect rect = new Rect();
        this.G = rect;
        rect.left = getPaddingLeft();
        this.G.right = i2 - getPaddingRight();
        this.G.top = getPaddingTop();
        this.G.bottom = i3 - getPaddingBottom();
        this.s = null;
        this.t = null;
        this.u = null;
        this.v = null;
        this.w = null;
        q();
        p();
        o();
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean i2;
        int action = motionEvent.getAction();
        if (action == 0) {
            this.K = new Point((int) motionEvent.getX(), (int) motionEvent.getY());
            i2 = i(motionEvent);
        } else if (action != 1) {
            i2 = action != 2 ? false : i(motionEvent);
        } else {
            this.K = null;
            i2 = i(motionEvent);
        }
        if (!i2) {
            return super.onTouchEvent(motionEvent);
        }
        c cVar = this.L;
        if (cVar != null) {
            cVar.K5(Color.HSVToColor(this.x, new float[]{this.y, this.z, this.A}));
        }
        invalidate();
        return true;
    }

    @DexIgnore
    public final void p() {
        Rect rect = this.G;
        this.I = new Rect((rect.right - this.b) + 1, rect.top + 1, rect.right - 1, (rect.bottom - 1) - (this.B ? this.d + this.c : 0));
    }

    @DexIgnore
    public final void q() {
        Rect rect = this.G;
        int i2 = rect.left;
        int i3 = rect.top;
        int i4 = rect.bottom - 1;
        int i5 = rect.right;
        int i6 = this.d;
        int i7 = this.b;
        if (this.B) {
            i4 -= this.c + i6;
        }
        this.H = new Rect(i2 + 1, i3 + 1, ((i5 - 1) - i6) - i7, i4);
    }

    @DexIgnore
    public void setAlphaSliderText(int i2) {
        setAlphaSliderText(getContext().getString(i2));
    }

    @DexIgnore
    public void setAlphaSliderText(String str) {
        this.C = str;
        invalidate();
    }

    @DexIgnore
    public void setAlphaSliderVisible(boolean z2) {
        if (this.B != z2) {
            this.B = z2;
            this.s = null;
            this.t = null;
            this.u = null;
            this.w = null;
            this.v = null;
            requestLayout();
        }
    }

    @DexIgnore
    public void setBorderColor(int i2) {
        this.E = i2;
        invalidate();
    }

    @DexIgnore
    public void setColor(int i2) {
        n(i2, false);
    }

    @DexIgnore
    public void setOnColorChangedListener(c cVar) {
        this.L = cVar;
    }

    @DexIgnore
    public void setSliderTrackerColor(int i2) {
        this.D = i2;
        this.l.setColor(i2);
        invalidate();
    }
}
