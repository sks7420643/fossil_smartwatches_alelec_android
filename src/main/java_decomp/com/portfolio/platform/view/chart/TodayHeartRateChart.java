package com.portfolio.platform.view.chart;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Gl7;
import com.fossil.Hm7;
import com.fossil.Im7;
import com.fossil.P47;
import com.fossil.Pm7;
import com.fossil.Um5;
import com.fossil.W57;
import com.mapped.Lc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BaseChart;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TodayHeartRateChart extends BaseChart {
    @DexIgnore
    public String A; // = "";
    @DexIgnore
    public float B;
    @DexIgnore
    public String C; // = "";
    @DexIgnore
    public float D;
    @DexIgnore
    public float E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public float H;
    @DexIgnore
    public float I;
    @DexIgnore
    public float J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public float M;
    @DexIgnore
    public float N;
    @DexIgnore
    public short O;
    @DexIgnore
    public short P;
    @DexIgnore
    public Paint Q;
    @DexIgnore
    public Paint R;
    @DexIgnore
    public Paint S;
    @DexIgnore
    public Paint T;
    @DexIgnore
    public List<W57> U;
    @DexIgnore
    public float V;
    @DexIgnore
    public String W; // = "";
    @DexIgnore
    public List<Gl7<Integer, Lc6<Integer, Float>, String>> a0; // = new ArrayList();
    @DexIgnore
    public int b0;
    @DexIgnore
    public String w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public String z; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TodayHeartRateChart(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Typeface f;
        Typeface f2;
        String d;
        String d2;
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attrs");
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.TodayHeartRateChart);
        String string = obtainStyledAttributes.getString(1);
        this.W = string == null ? "#ffffff" : string;
        String string2 = obtainStyledAttributes.getString(0);
        this.w = string2 == null ? "" : string2;
        this.x = obtainStyledAttributes.getColor(5, W6.d(context, R.color.steps));
        this.y = obtainStyledAttributes.getColor(4, W6.d(context, 2131099834));
        String string3 = obtainStyledAttributes.getString(6);
        this.z = string3 == null ? "" : string3;
        String string4 = obtainStyledAttributes.getString(3);
        this.A = string4 == null ? "" : string4;
        this.B = obtainStyledAttributes.getDimension(2, P47.n(13.0f));
        String string5 = obtainStyledAttributes.getString(8);
        this.C = string5 == null ? "" : string5;
        this.D = obtainStyledAttributes.getDimension(7, P47.n(13.0f));
        obtainStyledAttributes.recycle();
        this.Q = new Paint();
        if (!TextUtils.isEmpty(this.w) && (d2 = ThemeManager.l.a().d(this.w)) != null) {
            this.Q.setColor(Color.parseColor(d2));
        }
        this.Q.setStrokeWidth(2.0f);
        this.Q.setStyle(Paint.Style.STROKE);
        this.R = new Paint(1);
        if (!TextUtils.isEmpty(this.z) && (d = ThemeManager.l.a().d(this.z)) != null) {
            this.R.setColor(Color.parseColor(d));
        }
        this.R.setStyle(Paint.Style.FILL);
        this.R.setTextSize(this.B);
        if (!TextUtils.isEmpty(this.A) && (f2 = ThemeManager.l.a().f(this.A)) != null) {
            this.R.setTypeface(f2);
        }
        Paint paint = new Paint(1);
        this.S = paint;
        paint.setColor(this.R.getColor());
        this.S.setStyle(Paint.Style.FILL);
        this.S.setTextSize(this.D);
        if (!TextUtils.isEmpty(this.C) && (f = ThemeManager.l.a().f(this.C)) != null) {
            this.S.setTypeface(f);
        }
        Paint paint2 = new Paint(1);
        this.T = paint2;
        paint2.setStrokeWidth(P47.b(2.0f));
        this.T.setStyle(Paint.Style.STROKE);
        Rect rect = new Rect();
        this.R.getTextBounds("222", 0, 3, rect);
        this.L = (float) rect.width();
        this.M = (float) rect.height();
        Rect rect2 = new Rect();
        String c = Um5.c(PortfolioApp.get.instance(), 2131887231);
        this.S.getTextBounds(c, 0, c.length(), rect2);
        this.N = (float) rect2.height();
        this.U = new ArrayList();
        n();
        a();
        u();
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void f(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        super.f(canvas);
        this.E = ((float) canvas.getHeight()) - (this.N * ((float) 2));
        this.F = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.G = (float) canvas.getWidth();
        this.H = this.D;
        float b = P47.b(5.0f) + this.F;
        this.I = b;
        this.K = this.E - (this.D * ((float) 6));
        float f = this.G - (this.L * 2.0f);
        this.J = f;
        int i = this.b0;
        if (i == 0) {
            i = DateTimeConstants.MINUTES_PER_DAY;
        }
        this.V = (f - b) / ((float) i);
        q(canvas);
        u();
    }

    @DexIgnore
    public final int getDayInMinuteWithTimeZone() {
        return this.b0;
    }

    @DexIgnore
    public final List<Gl7<Integer, Lc6<Integer, Float>, String>> getListTimeZoneChange() {
        return this.a0;
    }

    @DexIgnore
    public final String getMBackgroundColor() {
        return this.W;
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void k(Canvas canvas) {
        Wg6.c(canvas, "canvas");
        super.k(canvas);
        if (this.a0.isEmpty()) {
            List<String> i = Hm7.i(Um5.c(PortfolioApp.get.instance(), 2131886713), Um5.c(PortfolioApp.get.instance(), 2131886715), Um5.c(PortfolioApp.get.instance(), 2131886714), Um5.c(PortfolioApp.get.instance(), 2131886716), Um5.c(PortfolioApp.get.instance(), 2131886713));
            ArrayList arrayList = new ArrayList();
            int size = i.size();
            float f = (this.J - this.I) / ((float) (size - 1));
            for (int i2 = 0; i2 < size; i2++) {
                arrayList.add(Float.valueOf(this.I + (((float) i2) * f)));
            }
            s(canvas, i, arrayList, this.E + (((((float) canvas.getHeight()) - this.E) + this.N) / ((float) 2)));
            return;
        }
        List<Gl7<Integer, Lc6<Integer, Float>, String>> list = this.a0;
        ArrayList arrayList2 = new ArrayList(Im7.m(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList2.add((String) it.next().getThird());
        }
        List<String> j0 = Pm7.j0(arrayList2);
        List<Gl7<Integer, Lc6<Integer, Float>, String>> list2 = this.a0;
        ArrayList arrayList3 = new ArrayList(Im7.m(list2, 10));
        Iterator<T> it2 = list2.iterator();
        while (it2.hasNext()) {
            arrayList3.add(Integer.valueOf(((Number) it2.next().getFirst()).intValue()));
        }
        List<Number> j02 = Pm7.j0(arrayList3);
        ArrayList arrayList4 = new ArrayList();
        for (Number number : j02) {
            int intValue = number.intValue();
            arrayList4.add(Float.valueOf((((float) intValue) * this.V) + this.I));
        }
        s(canvas, j0, arrayList4, this.E + (((((float) canvas.getHeight()) - this.E) + this.N) / ((float) 2)));
    }

    @DexIgnore
    public final void m(List<W57> list) {
        Wg6.c(list, "heartRatePointList");
        this.U.clear();
        this.U.addAll(list);
        n();
        getMGraph().invalidate();
    }

    @DexIgnore
    public final void n() {
        Object obj;
        Object obj2;
        List<W57> list = this.U;
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            if (t.d() > 0) {
                arrayList.add(t);
            }
        }
        Iterator it = arrayList.iterator();
        if (!it.hasNext()) {
            obj = null;
        } else {
            Object next = it.next();
            if (!it.hasNext()) {
                obj = next;
            } else {
                int d = ((W57) next).d();
                while (true) {
                    next = it.next();
                    d = ((W57) next).d();
                    if (d <= d) {
                        d = d;
                        next = next;
                    }
                    if (!it.hasNext()) {
                        break;
                    }
                }
                obj = next;
            }
        }
        W57 w57 = (W57) obj;
        this.O = (short) (w57 != null ? (short) w57.d() : 0);
        List<W57> list2 = this.U;
        ArrayList arrayList2 = new ArrayList();
        for (T t2 : list2) {
            if (t2.b() > 0) {
                arrayList2.add(t2);
            }
        }
        Iterator it2 = arrayList2.iterator();
        if (!it2.hasNext()) {
            obj2 = null;
        } else {
            Object next2 = it2.next();
            if (!it2.hasNext()) {
                obj2 = next2;
            } else {
                int b = ((W57) next2).b();
                while (true) {
                    next2 = it2.next();
                    int b2 = ((W57) next2).b();
                    if (b >= b2) {
                        b2 = b;
                        next2 = next2;
                    }
                    if (!it2.hasNext()) {
                        break;
                    }
                    b = b2;
                }
                obj2 = next2;
            }
        }
        W57 w572 = (W57) obj2;
        short b3 = w572 != null ? (short) w572.b() : 0;
        this.P = (short) b3;
        if (b3 == ((short) 0)) {
            this.P = (short) 100;
        }
    }

    @DexIgnore
    public final void o(String str, String str2) {
        Wg6.c(str, "maxHeartRate");
        Wg6.c(str2, "lowestHeartRate");
        String d = ThemeManager.l.a().d(str);
        String d2 = ThemeManager.l.a().d(str2);
        if (d != null) {
            this.y = Color.parseColor(d);
        }
        if (d2 != null) {
            this.x = Color.parseColor(d2);
        }
    }

    @DexIgnore
    public void onLayout(boolean z2, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.ui.view.chart.base.BaseChart
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        getMLegend().layout(getMLeftPadding(), getMTopPadding(), i - getMRightPadding(), (i2 - getMLegendHeight()) - getMBottomPadding());
    }

    @DexIgnore
    public final void p(Canvas canvas) {
        canvas.drawLine(this.F, this.K, (float) canvas.getWidth(), this.K, this.Q);
        canvas.drawLine(this.F, this.H, (float) canvas.getWidth(), this.H, this.Q);
        float f = this.J;
        float f2 = this.J;
        float measureText = this.R.measureText(String.valueOf((int) this.O));
        float f3 = (float) 2;
        canvas.drawText(String.valueOf((int) this.O), f + (((((float) canvas.getWidth()) - f2) - measureText) / f3) + P47.b(2.0f), (this.M * 1.5f) + this.K, this.R);
        float f4 = this.J;
        float width = ((((float) canvas.getWidth()) - this.J) - this.R.measureText(String.valueOf((int) this.P))) / f3;
        canvas.drawText(String.valueOf((int) this.P), f4 + width + P47.b(2.0f), (this.M * 1.5f) + this.H, this.R);
    }

    @DexIgnore
    public final void q(Canvas canvas) {
        p(canvas);
        r(canvas);
    }

    @DexIgnore
    public final void r(Canvas canvas) {
        int i;
        Path path;
        ArrayList<Path> arrayList = new ArrayList();
        while (true) {
            Path path2 = null;
            do {
                i = 0;
                boolean z2 = true;
                for (T t : this.U) {
                    float c = this.I + (((float) t.c()) * this.V);
                    float f = this.H;
                    float f2 = this.K;
                    short s = this.P;
                    float d = f + (((float) (s - t.d())) * ((f2 - f) / ((float) (s - this.O))));
                    if (d <= this.K) {
                        if (z2) {
                            path = new Path();
                            path.moveTo(c, d);
                            z2 = false;
                        } else if (path2 != null) {
                            path2.lineTo(c, d);
                            path = path2;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                        i++;
                        path2 = path;
                    }
                }
                if (path2 != null) {
                    arrayList.add(path2);
                }
                Paint paint = this.T;
                float f3 = this.I;
                paint.setShader(new LinearGradient(f3, this.H, f3, this.K, this.y, this.x, Shader.TileMode.MIRROR));
                for (Path path3 : arrayList) {
                    canvas.drawPath(path3, this.T);
                }
                return;
            } while (path2 == null);
            if (i > 1) {
                arrayList.add(path2);
            }
        }
    }

    @DexIgnore
    public final void s(Canvas canvas, List<String> list, List<Float> list2, float f) {
        int i = 0;
        for (T t : list) {
            if (i >= 0) {
                canvas.drawText(t, list2.get(i).floatValue(), f, this.S);
                i++;
            } else {
                Hm7.l();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void setDayInMinuteWithTimeZone(int i) {
        this.b0 = i;
    }

    @DexIgnore
    public final void setListTimeZoneChange(List<Gl7<Integer, Lc6<Integer, Float>, String>> list) {
        Wg6.c(list, "value");
        this.a0 = list;
        getMLegend().invalidate();
    }

    @DexIgnore
    public final void setMBackgroundColor(String str) {
        Wg6.c(str, "<set-?>");
        this.W = str;
    }

    @DexIgnore
    public void t(String str, int i) {
        Wg6.c(str, "keyColor");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setGraphPreviewColor keyColor=" + str + " valueColor=" + i);
        int hashCode = str.hashCode();
        if (hashCode != -1222874814) {
            if (hashCode == -685682508 && str.equals("lowestHeartRate")) {
                this.x = i;
            }
        } else if (str.equals("maxHeartRate")) {
            this.y = i;
        }
        getMGraph().invalidate();
    }

    @DexIgnore
    public final void u() {
        if (!TextUtils.isEmpty(this.W)) {
            String d = ThemeManager.l.a().d(this.W);
            if (!TextUtils.isEmpty(d)) {
                setBackgroundColor(Color.parseColor(d));
            }
        }
    }
}
