package com.portfolio.platform.buddy_challenge.util;

import android.util.SparseArray;
import androidx.lifecycle.Lifecycle;
import com.fossil.Bz4;
import com.mapped.Fd;
import com.mapped.Md;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TimerViewObserver implements Fd {
    @DexIgnore
    public SparseArray<Bz4> b; // = new SparseArray<>();

    @DexIgnore
    public final void a() {
        this.b.clear();
    }

    @DexIgnore
    public final void c(Bz4 bz4, int i) {
        Wg6.c(bz4, "aware");
        this.b.put(i, bz4);
    }

    @DexIgnore
    public final void d(int i) {
        this.b.remove(i);
    }

    @DexIgnore
    @Md(Lifecycle.a.ON_PAUSE)
    public final void onPause() {
        if (this.b.size() != 0) {
            SparseArray<Bz4> sparseArray = this.b;
            int size = sparseArray.size();
            for (int i = 0; i < size; i++) {
                sparseArray.keyAt(i);
                sparseArray.valueAt(i).onPause();
            }
        }
    }

    @DexIgnore
    @Md(Lifecycle.a.ON_RESUME)
    public final void onResume() {
        if (this.b.size() != 0) {
            SparseArray<Bz4> sparseArray = this.b;
            int size = sparseArray.size();
            for (int i = 0; i < size; i++) {
                sparseArray.keyAt(i);
                sparseArray.valueAt(i).onResume();
            }
        }
    }
}
