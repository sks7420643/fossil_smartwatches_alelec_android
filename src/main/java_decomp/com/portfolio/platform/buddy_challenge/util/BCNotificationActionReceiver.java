package com.portfolio.platform.buddy_challenge.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Cl5;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Pt4;
import com.fossil.Tt4;
import com.fossil.Ux7;
import com.fossil.Xr4;
import com.fossil.Xs4;
import com.fossil.Xy4;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCNotificationActionReceiver extends BroadcastReceiver {
    @DexIgnore
    public FriendRepository a;
    @DexIgnore
    public Tt4 b;
    @DexIgnore
    public An4 c;
    @DexIgnore
    public /* final */ Il6 d; // = Jv7.a(Ux7.b(null, 1, null).plus(Bw7.b()));
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver$onReceive$1", f = "BCNotificationActionReceiver.kt", l = {47}, m = "invokeSuspend")
    public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xs4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCNotificationActionReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(BCNotificationActionReceiver bCNotificationActionReceiver, Xs4 xs4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCNotificationActionReceiver;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            a aVar = new a(this.this$0, this.$friend, xe6);
            aVar.p$ = (Il6) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object p;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                FriendRepository c = this.this$0.c();
                Xs4 xs4 = this.$friend;
                this.L$0 = il6;
                this.label = 1;
                p = c.p(true, xs4, this);
                if (p == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                p = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((Kz4) p).c() != null) {
                Xr4.a.b("bc_accept_friend_request", PortfolioApp.get.instance().l0(), this.$friend.d(), PortfolioApp.get.instance());
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver$onReceive$2", f = "BCNotificationActionReceiver.kt", l = {59}, m = "invokeSuspend")
    public static final class b extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xs4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCNotificationActionReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(BCNotificationActionReceiver bCNotificationActionReceiver, Xs4 xs4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCNotificationActionReceiver;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            b bVar = new b(this.this$0, this.$friend, xe6);
            bVar.p$ = (Il6) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((b) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object p;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                FriendRepository c = this.this$0.c();
                Xs4 xs4 = this.$friend;
                this.L$0 = il6;
                this.label = 1;
                p = c.p(false, xs4, this);
                if (p == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                p = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((Kz4) p).c() != null) {
                Xr4.a.b("bc_reject_friend_request", PortfolioApp.get.instance().l0(), this.$friend.d(), PortfolioApp.get.instance());
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver$onReceive$3", f = "BCNotificationActionReceiver.kt", l = {76}, m = "invokeSuspend")
    public static final class c extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCNotificationActionReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(BCNotificationActionReceiver bCNotificationActionReceiver, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCNotificationActionReceiver;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (Il6) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((c) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object v;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Long d2 = this.this$0.d().d();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.this$0.e;
                local.e(str, "ACTION_SET_SYNC_DATA - endTimeOfChallenge: " + d2 + " - exact: " + Xy4.a.b());
                if (d2.longValue() > Xy4.a.b()) {
                    Tt4 b = this.this$0.b();
                    this.L$0 = il6;
                    this.L$1 = d2;
                    this.label = 1;
                    v = b.v(this);
                    if (v == d) {
                        return d;
                    }
                } else {
                    this.this$0.d().e(Ao7.f(0));
                    this.this$0.d().m0(Ao7.a(false));
                    return Cd6.a;
                }
            } else if (i == 1) {
                Long l = (Long) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                v = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Pt4 pt4 = (Pt4) ((Kz4) v).c();
            if (pt4 != null) {
                PortfolioApp.get.instance().m1(pt4.a());
            } else {
                Cl5.c.j();
            }
            return Cd6.a;
        }
    }

    @DexIgnore
    public BCNotificationActionReceiver() {
        String simpleName = BCNotificationActionReceiver.class.getSimpleName();
        Wg6.b(simpleName, "BCNotificationActionRece\u2026er::class.java.simpleName");
        this.e = simpleName;
        PortfolioApp.get.instance().getIface().r1(this);
    }

    @DexIgnore
    public final Tt4 b() {
        Tt4 tt4 = this.b;
        if (tt4 != null) {
            return tt4;
        }
        Wg6.n("challengeRepository");
        throw null;
    }

    @DexIgnore
    public final FriendRepository c() {
        FriendRepository friendRepository = this.a;
        if (friendRepository != null) {
            return friendRepository;
        }
        Wg6.n("friendRepository");
        throw null;
    }

    @DexIgnore
    public final An4 d() {
        An4 an4 = this.c;
        if (an4 != null) {
            return an4;
        }
        Wg6.n("shared");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        Xs4 xs4 = intent != null ? (Xs4) intent.getParcelableExtra("friend_extra") : null;
        String action = intent != null ? intent.getAction() : null;
        if (action != null) {
            int hashCode = action.hashCode();
            if (hashCode != -1294772761) {
                if (hashCode != -1088442688) {
                    if (hashCode == 1236721143 && action.equals("com.buddy_challenge.friend.decline") && xs4 != null) {
                        Cl5.c.d(PortfolioApp.get.instance(), xs4.d().hashCode());
                        Rm6 unused = Gu7.d(this.d, null, null, new b(this, xs4, null), 3, null);
                    }
                } else if (action.equals("com.buddy_challenge.set_sync_data")) {
                    FLogger.INSTANCE.getLocal().e(this.e, "ACTION_SET_SYNC_DATA");
                    Cl5.c.e();
                    Rm6 unused2 = Gu7.d(this.d, null, null, new c(this, null), 3, null);
                }
            } else if (action.equals("com.buddy_challenge.friend.accept") && xs4 != null) {
                Cl5.c.d(PortfolioApp.get.instance(), xs4.d().hashCode());
                Rm6 unused3 = Gu7.d(this.d, null, null, new a(this, xs4, null), 3, null);
            }
        }
    }
}
