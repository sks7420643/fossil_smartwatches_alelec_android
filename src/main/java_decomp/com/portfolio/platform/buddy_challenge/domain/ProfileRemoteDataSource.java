package com.portfolio.platform.buddy_challenge.domain;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.El7;
import com.fossil.It4;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Jf6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileRemoteDataSource {
    @DexIgnore
    public /* final */ ApiServiceV2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource", f = "ProfileRemoteDataSource.kt", l = {31}, m = "changeSocialId")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(ProfileRemoteDataSource profileRemoteDataSource, Xe6 xe6) {
            super(xe6);
            this.this$0 = profileRemoteDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$changeSocialId$response$1", f = "ProfileRemoteDataSource.kt", l = {31}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Hg6<Xe6<? super Q88<It4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(ProfileRemoteDataSource profileRemoteDataSource, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = profileRemoteDataSource;
            this.$jsonObject = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Bi(this.this$0, this.$jsonObject, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<It4>> xe6) {
            throw null;
            //return ((Bi) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                Ku3 ku3 = this.$jsonObject;
                this.label = 1;
                Object socialId = apiServiceV2.socialId(ku3, this);
                return socialId == d ? d : socialId;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource", f = "ProfileRemoteDataSource.kt", l = {16}, m = "fetchSocialProfile")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(ProfileRemoteDataSource profileRemoteDataSource, Xe6 xe6) {
            super(xe6);
            this.this$0 = profileRemoteDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$fetchSocialProfile$response$1", f = "ProfileRemoteDataSource.kt", l = {16}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(ProfileRemoteDataSource profileRemoteDataSource, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = profileRemoteDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Di(this.this$0, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Di) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                this.label = 1;
                Object mySocialProfile = apiServiceV2.getMySocialProfile(this);
                return mySocialProfile == d ? d : mySocialProfile;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public ProfileRemoteDataSource(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        this.a = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(java.lang.String r9, com.mapped.Xe6<? super com.mapped.Ap4<com.fossil.It4>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource.Ai
            if (r0 == 0) goto L_0x0046
            r0 = r10
            com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$Ai r0 = (com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0046
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0055
            if (r0 != r5) goto L_0x004d
            java.lang.Object r0 = r1.L$2
            com.mapped.Ku3 r0 = (com.mapped.Ku3) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource r0 = (com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x0031:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0077
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            r2 = 0
            r3 = 2
            r1.<init>(r0, r2, r3, r4)
            r0 = r1
        L_0x0045:
            return r0
        L_0x0046:
            com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$Ai r0 = new com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$Ai
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x004d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0055:
            com.fossil.El7.b(r2)
            com.mapped.Ku3 r0 = new com.mapped.Ku3
            r0.<init>()
            java.lang.String r2 = "socialId"
            r0.n(r2, r9)
            com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$Bi r2 = new com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$Bi
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r0
            r1.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r2, r1)
            if (r0 != r3) goto L_0x0031
            r0 = r3
            goto L_0x0045
        L_0x0077:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x0094
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0045
        L_0x0094:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource.b(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.mapped.Xe6<? super com.mapped.Ap4<com.fossil.It4>> r9) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource.Ci
            if (r0 == 0) goto L_0x0045
            r0 = r9
            com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$Ci r0 = (com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource.Ci) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0045
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0053
            if (r3 != r5) goto L_0x004b
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource r0 = (com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0028:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0078
            com.fossil.Hz4 r1 = com.fossil.Hz4.a
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r1 = r0.a()
            com.mapped.Ku3 r1 = (com.mapped.Ku3) r1
            if (r1 != 0) goto L_0x0067
        L_0x003a:
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            boolean r0 = r0.b()
            r1.<init>(r4, r0)
            r0 = r1
        L_0x0044:
            return r0
        L_0x0045:
            com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$Ci r0 = new com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$Ci
            r0.<init>(r8, r9)
            goto L_0x0014
        L_0x004b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0053:
            com.fossil.El7.b(r1)
            com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$Di r1 = new com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$Di
            r1.<init>(r8, r4)
            r0.L$0 = r8
            r0.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r1, r0)
            if (r0 != r2) goto L_0x0028
            r0 = r2
            goto L_0x0044
        L_0x0067:
            com.google.gson.Gson r2 = new com.google.gson.Gson     // Catch:{ Mj4 -> 0x0073 }
            r2.<init>()     // Catch:{ Mj4 -> 0x0073 }
            java.lang.Class<com.fossil.It4> r3 = com.fossil.It4.class
            java.lang.Object r4 = r2.g(r1, r3)     // Catch:{ Mj4 -> 0x0073 }
            goto L_0x003a
        L_0x0073:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003a
        L_0x0078:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x0095
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0044
        L_0x0095:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource.c(com.mapped.Xe6):java.lang.Object");
    }
}
