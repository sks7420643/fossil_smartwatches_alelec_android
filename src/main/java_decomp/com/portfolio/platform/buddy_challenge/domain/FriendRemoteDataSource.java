package com.portfolio.platform.buddy_challenge.domain;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Xs4;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Jf6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FriendRemoteDataSource {
    @DexIgnore
    public /* final */ ApiServiceV2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {114}, m = "block")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(FriendRemoteDataSource friendRemoteDataSource, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRemoteDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$block$response$1", f = "FriendRemoteDataSource.kt", l = {114}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(FriendRemoteDataSource friendRemoteDataSource, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = friendRemoteDataSource;
            this.$jsonObject = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Bi(this.this$0, this.$jsonObject, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Bi) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                Ku3 ku3 = this.$jsonObject;
                this.label = 1;
                Object block = apiServiceV2.block(ku3, this);
                return block == d ? d : block;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {99}, m = "cancelFriendRequest")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(FriendRemoteDataSource friendRemoteDataSource, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRemoteDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$cancelFriendRequest$response$1", f = "FriendRemoteDataSource.kt", l = {99}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(FriendRemoteDataSource friendRemoteDataSource, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = friendRemoteDataSource;
            this.$jsonObject = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Di(this.this$0, this.$jsonObject, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Di) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                Ku3 ku3 = this.$jsonObject;
                this.label = 1;
                Object cancelFriendRequest = apiServiceV2.cancelFriendRequest(ku3, this);
                return cancelFriendRequest == d ? d : cancelFriendRequest;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {53}, m = "fetchFriends")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(FriendRemoteDataSource friendRemoteDataSource, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRemoteDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$fetchFriends$response$1", f = "FriendRemoteDataSource.kt", l = {53}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<Xs4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $status;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(FriendRemoteDataSource friendRemoteDataSource, String str, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = friendRemoteDataSource;
            this.$status = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Fi(this.this$0, this.$status, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<ApiResponse<Xs4>>> xe6) {
            throw null;
            //return ((Fi) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                String str = this.$status;
                this.label = 1;
                Object friends = apiServiceV2.getFriends(str, this);
                return friends == d ? d : friends;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {17}, m = Constants.FIND)
    public static final class Gi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(FriendRemoteDataSource friendRemoteDataSource, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRemoteDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$find$response$1", f = "FriendRemoteDataSource.kt", l = {17}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<Xs4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $key;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(FriendRemoteDataSource friendRemoteDataSource, String str, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = friendRemoteDataSource;
            this.$key = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Hi(this.this$0, this.$key, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<ApiResponse<Xs4>>> xe6) {
            throw null;
            //return ((Hi) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                String str = this.$key;
                this.label = 1;
                Object findFriend = apiServiceV2.findFriend(str, this);
                return findFriend == d ? d : findFriend;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {145}, m = "respondFriendRequest")
    public static final class Ii extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(FriendRemoteDataSource friendRemoteDataSource, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRemoteDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f(false, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$respondFriendRequest$response$1", f = "FriendRemoteDataSource.kt", l = {145}, m = "invokeSuspend")
    public static final class Ji extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(FriendRemoteDataSource friendRemoteDataSource, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = friendRemoteDataSource;
            this.$jsonObject = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Ji(this.this$0, this.$jsonObject, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Ji) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                Ku3 ku3 = this.$jsonObject;
                this.label = 1;
                Object respondFriendRequest = apiServiceV2.respondFriendRequest(ku3, this);
                return respondFriendRequest == d ? d : respondFriendRequest;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {69}, m = "sendRequest")
    public static final class Ki extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(FriendRemoteDataSource friendRemoteDataSource, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRemoteDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g(null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$sendRequest$response$1", f = "FriendRemoteDataSource.kt", l = {69}, m = "invokeSuspend")
    public static final class Li extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Li(FriendRemoteDataSource friendRemoteDataSource, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = friendRemoteDataSource;
            this.$jsonObject = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Li(this.this$0, this.$jsonObject, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Li) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                Ku3 ku3 = this.$jsonObject;
                this.label = 1;
                Object sendRequest = apiServiceV2.sendRequest(ku3, this);
                return sendRequest == d ? d : sendRequest;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {84}, m = "unFriend")
    public static final class Mi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Mi(FriendRemoteDataSource friendRemoteDataSource, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRemoteDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$unFriend$response$1", f = "FriendRemoteDataSource.kt", l = {84}, m = "invokeSuspend")
    public static final class Ni extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ni(FriendRemoteDataSource friendRemoteDataSource, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = friendRemoteDataSource;
            this.$jsonObject = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Ni(this.this$0, this.$jsonObject, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Ni) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                Ku3 ku3 = this.$jsonObject;
                this.label = 1;
                Object unFriend = apiServiceV2.unFriend(ku3, this);
                return unFriend == d ? d : unFriend;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource", f = "FriendRemoteDataSource.kt", l = {129}, m = "unblock")
    public static final class Oi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Oi(FriendRemoteDataSource friendRemoteDataSource, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRemoteDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$unblock$response$1", f = "FriendRemoteDataSource.kt", l = {129}, m = "invokeSuspend")
    public static final class Pi extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Pi(FriendRemoteDataSource friendRemoteDataSource, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = friendRemoteDataSource;
            this.$jsonObject = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Pi(this.this$0, this.$jsonObject, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Pi) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                Ku3 ku3 = this.$jsonObject;
                this.label = 1;
                Object unblock = apiServiceV2.unblock(ku3, this);
                return unblock == d ? d : unblock;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public FriendRemoteDataSource(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        this.a = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(java.lang.String r9, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Void>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.Ai
            if (r0 == 0) goto L_0x003f
            r0 = r10
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Ai r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003f
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x004e
            if (r0 != r5) goto L_0x0046
            java.lang.Object r0 = r1.L$2
            com.mapped.Ku3 r0 = (com.mapped.Ku3) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x0031:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0070
            com.fossil.Kq5 r0 = new com.fossil.Kq5
            r1 = 0
            r2 = 2
            r0.<init>(r4, r1, r2, r4)
        L_0x003e:
            return r0
        L_0x003f:
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Ai r0 = new com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Ai
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0046:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004e:
            com.fossil.El7.b(r2)
            com.mapped.Ku3 r0 = new com.mapped.Ku3
            r0.<init>()
            java.lang.String r2 = "profileID"
            r0.n(r2, r9)
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Bi r2 = new com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Bi
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r0
            r1.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r2, r1)
            if (r0 != r3) goto L_0x0031
            r0 = r3
            goto L_0x003e
        L_0x0070:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x009e
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.portfolio.platform.data.model.ServerError r0 = r3.c()
            if (r0 == 0) goto L_0x0099
            java.lang.Integer r0 = r0.getCode()
            if (r0 == 0) goto L_0x0099
            int r1 = r0.intValue()
        L_0x0087:
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x003e
        L_0x0099:
            int r1 = r3.a()
            goto L_0x0087
        L_0x009e:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.b(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(java.lang.String r9, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Void>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.Ci
            if (r0 == 0) goto L_0x003f
            r0 = r10
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Ci r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.Ci) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003f
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x004e
            if (r0 != r5) goto L_0x0046
            java.lang.Object r0 = r1.L$2
            com.mapped.Ku3 r0 = (com.mapped.Ku3) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x0031:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0070
            com.fossil.Kq5 r0 = new com.fossil.Kq5
            r1 = 0
            r2 = 2
            r0.<init>(r4, r1, r2, r4)
        L_0x003e:
            return r0
        L_0x003f:
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Ci r0 = new com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Ci
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0046:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004e:
            com.fossil.El7.b(r2)
            com.mapped.Ku3 r0 = new com.mapped.Ku3
            r0.<init>()
            java.lang.String r2 = "friendId"
            r0.n(r2, r9)
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Di r2 = new com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Di
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r0
            r1.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r2, r1)
            if (r0 != r3) goto L_0x0031
            r0 = r3
            goto L_0x003e
        L_0x0070:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x008d
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x003e
        L_0x008d:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.c(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(java.lang.String r9, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.Xs4>>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.Ei
            if (r0 == 0) goto L_0x0044
            r0 = r10
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Ei r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.Ei) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0044
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0053
            if (r0 != r5) goto L_0x004b
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x002d:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0069
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x0043:
            return r0
        L_0x0044:
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Ei r0 = new com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Ei
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x004b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0053:
            com.fossil.El7.b(r2)
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Fi r0 = new com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Fi
            r0.<init>(r8, r9, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x0043
        L_0x0069:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x0086
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0043
        L_0x0086:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.d(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(java.lang.String r9, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.Xs4>>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.Gi
            if (r0 == 0) goto L_0x0044
            r0 = r10
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Gi r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.Gi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0044
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0053
            if (r0 != r5) goto L_0x004b
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x002d:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0069
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x0043:
            return r0
        L_0x0044:
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Gi r0 = new com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Gi
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x004b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0053:
            com.fossil.El7.b(r2)
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Hi r0 = new com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Hi
            r0.<init>(r8, r9, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x0043
        L_0x0069:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x0086
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0043
        L_0x0086:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.e(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object f(boolean r9, java.lang.String r10, com.mapped.Xe6<? super com.mapped.Ap4<com.fossil.Xs4>> r11) {
        /*
        // Method dump skipped, instructions count: 219
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.f(boolean, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g(java.lang.String r9, java.lang.String r10, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Void>> r11) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r11 instanceof com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.Ki
            if (r0 == 0) goto L_0x0043
            r0 = r11
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Ki r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.Ki) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0043
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0052
            if (r0 != r5) goto L_0x004a
            java.lang.Object r0 = r1.L$3
            com.mapped.Ku3 r0 = (com.mapped.Ku3) r0
            java.lang.Object r0 = r1.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x0035:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x007b
            com.fossil.Kq5 r0 = new com.fossil.Kq5
            r1 = 0
            r2 = 2
            r0.<init>(r4, r1, r2, r4)
        L_0x0042:
            return r0
        L_0x0043:
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Ki r0 = new com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Ki
            r0.<init>(r8, r11)
            r1 = r0
            goto L_0x0015
        L_0x004a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0052:
            com.fossil.El7.b(r2)
            com.mapped.Ku3 r0 = new com.mapped.Ku3
            r0.<init>()
            java.lang.String r2 = "friendId"
            r0.n(r2, r9)
            java.lang.String r2 = "socialId"
            r0.n(r2, r10)
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Li r2 = new com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Li
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r10
            r1.L$3 = r0
            r1.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r2, r1)
            if (r0 != r3) goto L_0x0035
            r0 = r3
            goto L_0x0042
        L_0x007b:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00a9
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.portfolio.platform.data.model.ServerError r0 = r3.c()
            if (r0 == 0) goto L_0x00a4
            java.lang.Integer r0 = r0.getCode()
            if (r0 == 0) goto L_0x00a4
            int r1 = r0.intValue()
        L_0x0092:
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0042
        L_0x00a4:
            int r1 = r3.a()
            goto L_0x0092
        L_0x00a9:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.g(java.lang.String, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object h(java.lang.String r9, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Void>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.Mi
            if (r0 == 0) goto L_0x003f
            r0 = r10
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Mi r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.Mi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003f
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x004e
            if (r0 != r5) goto L_0x0046
            java.lang.Object r0 = r1.L$2
            com.mapped.Ku3 r0 = (com.mapped.Ku3) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x0031:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0070
            com.fossil.Kq5 r0 = new com.fossil.Kq5
            r1 = 0
            r2 = 2
            r0.<init>(r4, r1, r2, r4)
        L_0x003e:
            return r0
        L_0x003f:
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Mi r0 = new com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Mi
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0046:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004e:
            com.fossil.El7.b(r2)
            com.mapped.Ku3 r0 = new com.mapped.Ku3
            r0.<init>()
            java.lang.String r2 = "friendId"
            r0.n(r2, r9)
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Ni r2 = new com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Ni
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r0
            r1.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r2, r1)
            if (r0 != r3) goto L_0x0031
            r0 = r3
            goto L_0x003e
        L_0x0070:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x008d
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x003e
        L_0x008d:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.h(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object i(java.lang.String r9, com.mapped.Xe6<? super com.mapped.Ap4<com.fossil.Os4>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.Oi
            if (r0 == 0) goto L_0x004e
            r0 = r10
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Oi r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.Oi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x004e
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x005d
            if (r0 != r5) goto L_0x0055
            java.lang.Object r0 = r1.L$2
            com.mapped.Ku3 r0 = (com.mapped.Ku3) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x0031:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0090
            com.fossil.Hz4 r1 = com.fossil.Hz4.a
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r1 = r0.a()
            com.mapped.Ku3 r1 = (com.mapped.Ku3) r1
            if (r1 != 0) goto L_0x007f
        L_0x0043:
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            boolean r0 = r0.b()
            r1.<init>(r4, r0)
            r0 = r1
        L_0x004d:
            return r0
        L_0x004e:
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Oi r0 = new com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Oi
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0055:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005d:
            com.fossil.El7.b(r2)
            com.mapped.Ku3 r0 = new com.mapped.Ku3
            r0.<init>()
            java.lang.String r2 = "profileID"
            r0.n(r2, r9)
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Pi r2 = new com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource$Pi
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r0
            r1.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r2, r1)
            if (r0 != r3) goto L_0x0031
            r0 = r3
            goto L_0x004d
        L_0x007f:
            com.google.gson.Gson r2 = new com.google.gson.Gson     // Catch:{ Mj4 -> 0x008b }
            r2.<init>()     // Catch:{ Mj4 -> 0x008b }
            java.lang.Class<com.fossil.Os4> r3 = com.fossil.Os4.class
            java.lang.Object r4 = r2.g(r1, r3)     // Catch:{ Mj4 -> 0x008b }
            goto L_0x0043
        L_0x008b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0043
        L_0x0090:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00be
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.portfolio.platform.data.model.ServerError r0 = r3.c()
            if (r0 == 0) goto L_0x00b9
            java.lang.Integer r0 = r0.getCode()
            if (r0 == 0) goto L_0x00b9
            int r1 = r0.intValue()
        L_0x00a7:
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x004d
        L_0x00b9:
            int r1 = r3.a()
            goto L_0x00a7
        L_0x00be:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource.i(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }
}
