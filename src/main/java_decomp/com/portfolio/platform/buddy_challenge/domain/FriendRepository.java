package com.portfolio.platform.buddy_challenge.domain;

import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Im7;
import com.fossil.Mm7;
import com.fossil.Pm7;
import com.fossil.Xs4;
import com.fossil.Xt4;
import com.mapped.An4;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FriendRepository {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Xt4 b;
    @DexIgnore
    public /* final */ FriendRemoteDataSource c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {315}, m = "block")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(FriendRepository friendRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {290}, m = "cancelFriendRequest")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(FriendRepository friendRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {393, 395}, m = "completePendingConfirmedFriends")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(FriendRepository friendRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {229}, m = "fetchBlockedFriends")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(FriendRepository friendRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.f(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {207}, m = "fetchFriends")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(FriendRepository friendRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {177}, m = "fetchReceivedRequestFriends")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(FriendRepository friendRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {130, 132}, m = "fetchSentRequestFriends")
    public static final class Gi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(FriendRepository friendRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {66}, m = "respondRequest")
    public static final class Hi extends Jf6 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(FriendRepository friendRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.p(false, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {26}, m = "searchProfile")
    public static final class Ii extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(FriendRepository friendRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.q(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {46}, m = "sendRequest")
    public static final class Ji extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(FriendRepository friendRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.r(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {259}, m = "unFriend")
    public static final class Ki extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(FriendRepository friendRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {350}, m = "unblock")
    public static final class Li extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FriendRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Li(FriendRepository friendRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = friendRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.t(null, this);
        }
    }

    @DexIgnore
    public FriendRepository(Xt4 xt4, FriendRemoteDataSource friendRemoteDataSource, An4 an4) {
        Wg6.c(xt4, "local");
        Wg6.c(friendRemoteDataSource, "remote");
        Wg6.c(an4, "shared");
        this.b = xt4;
        this.c = friendRemoteDataSource;
        String simpleName = FriendRepository.class.getSimpleName();
        Wg6.b(simpleName, "FriendRepository::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.Xs4 r10, com.mapped.Xe6<? super com.fossil.Kz4<java.lang.String>> r11) {
        /*
        // Method dump skipped, instructions count: 294
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRepository.a(com.fossil.Xs4, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.fossil.Xs4 r11, com.mapped.Xe6<? super com.fossil.Kz4<java.lang.String>> r12) {
        /*
        // Method dump skipped, instructions count: 244
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRepository.b(com.fossil.Xs4, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void c() {
        this.b.a();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0162  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.mapped.Xe6<? super com.mapped.Cd6> r14) {
        /*
        // Method dump skipped, instructions count: 380
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRepository.d(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final int e() {
        return this.b.b();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object f(com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.Xs4>>> r8) {
        /*
        // Method dump skipped, instructions count: 266
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRepository.f(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object g(com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.Xs4>>> r7) {
        /*
            r6 = this;
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 0
            boolean r0 = r7 instanceof com.portfolio.platform.buddy_challenge.domain.FriendRepository.Ei
            if (r0 == 0) goto L_0x0073
            r0 = r7
            com.portfolio.platform.buddy_challenge.domain.FriendRepository$Ei r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRepository.Ei) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0073
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0081
            if (r4 != r5) goto L_0x0079
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.buddy_challenge.domain.FriendRepository r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRepository) r0
            com.fossil.El7.b(r1)
            r6 = r0
        L_0x0028:
            r0 = r1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x009b
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r0 = (com.portfolio.platform.data.source.remote.ApiResponse) r0
            if (r0 == 0) goto L_0x0094
            java.util.List r0 = r0.get_items()
        L_0x003d:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = r6.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "fetchFriends - data: "
            r4.append(r5)
            r4.append(r0)
            java.lang.String r4 = r4.toString()
            r1.e(r3, r4)
            if (r0 == 0) goto L_0x0069
            com.fossil.Xt4 r1 = r6.b
            r1.q(r0)
            com.fossil.Xt4 r1 = r6.b
            java.util.List r1 = r1.j()
            r6.o(r1, r0)
        L_0x0069:
            if (r0 == 0) goto L_0x0096
        L_0x006b:
            com.fossil.Kz4 r1 = new com.fossil.Kz4
            r3 = 2
            r1.<init>(r0, r2, r3, r2)
            r0 = r1
        L_0x0072:
            return r0
        L_0x0073:
            com.portfolio.platform.buddy_challenge.domain.FriendRepository$Ei r0 = new com.portfolio.platform.buddy_challenge.domain.FriendRepository$Ei
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0079:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0081:
            com.fossil.El7.b(r1)
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource r1 = r6.c
            r0.L$0 = r6
            r0.label = r5
            java.lang.String r4 = "friend"
            java.lang.Object r1 = r1.d(r4, r0)
            if (r1 != r3) goto L_0x0028
            r0 = r3
            goto L_0x0072
        L_0x0094:
            r0 = r2
            goto L_0x003d
        L_0x0096:
            java.util.List r0 = com.fossil.Hm7.e()
            goto L_0x006b
        L_0x009b:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00da
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = r6.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "fetchFriends - "
            r4.append(r5)
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            int r5 = r0.a()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.e(r3, r4)
            int r1 = r0.a()
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00cf
            java.lang.String r2 = r0.getMessage()
        L_0x00cf:
            com.fossil.Kz4 r0 = new com.fossil.Kz4
            com.portfolio.platform.data.model.ServerError r3 = new com.portfolio.platform.data.model.ServerError
            r3.<init>(r1, r2)
            r0.<init>(r3)
            goto L_0x0072
        L_0x00da:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRepository.g(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object h(com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.Xs4>>> r7) {
        /*
        // Method dump skipped, instructions count: 248
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRepository.h(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0170  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object i(com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.Xs4>>> r14) {
        /*
        // Method dump skipped, instructions count: 438
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRepository.i(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Xs4 j(String str) {
        Wg6.c(str, "id");
        return this.b.h(str);
    }

    @DexIgnore
    public final List<Xs4> k() {
        return this.b.k();
    }

    @DexIgnore
    public final List<Xs4> l() {
        return this.b.j();
    }

    @DexIgnore
    public final LiveData<List<Xs4>> m() {
        return this.b.l();
    }

    @DexIgnore
    public final long n(Xs4 xs4) {
        Wg6.c(xs4, "friend");
        return this.b.p(xs4);
    }

    @DexIgnore
    public final void o(List<Xs4> list, List<Xs4> list2) {
        List V = Pm7.V(list, list2);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : V) {
            String d = ((Xs4) obj).d();
            Object obj2 = linkedHashMap.get(d);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(d, obj2);
            }
            ((List) obj2).add(obj);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            if (((List) entry.getValue()).size() == 1) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList<Xs4> arrayList = new ArrayList();
        for (Map.Entry entry2 : linkedHashMap2.entrySet()) {
            Mm7.s(arrayList, (List) entry2.getValue());
        }
        ArrayList arrayList2 = new ArrayList(Im7.m(arrayList, 10));
        for (Xs4 xs4 : arrayList) {
            arrayList2.add(xs4.d());
        }
        Xt4 xt4 = this.b;
        Object[] array = arrayList2.toArray(new String[0]);
        if (array != null) {
            xt4.g((String[]) array);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object p(boolean r13, com.fossil.Xs4 r14, com.mapped.Xe6<? super com.fossil.Kz4<com.fossil.Xs4>> r15) {
        /*
        // Method dump skipped, instructions count: 569
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRepository.p(boolean, com.fossil.Xs4, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object q(java.lang.String r7, com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.fossil.Xs4>>> r8) {
        /*
            r6 = this;
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            boolean r0 = r8 instanceof com.portfolio.platform.buddy_challenge.domain.FriendRepository.Ii
            if (r0 == 0) goto L_0x0054
            r0 = r8
            com.portfolio.platform.buddy_challenge.domain.FriendRepository$Ii r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRepository.Ii) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x0054
            int r1 = r1 + r4
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r4 = r1.label
            if (r4 == 0) goto L_0x0063
            if (r4 != r5) goto L_0x005b
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.buddy_challenge.domain.FriendRepository r1 = (com.portfolio.platform.buddy_challenge.domain.FriendRepository) r1
            com.fossil.El7.b(r2)
            r7 = r0
        L_0x002d:
            r0 = r2
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r2 = r0 instanceof com.fossil.Kq5
            if (r2 == 0) goto L_0x007f
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r0 = (com.portfolio.platform.data.source.remote.ApiResponse) r0
            if (r0 == 0) goto L_0x0076
            java.util.List r0 = r0.get_items()
            r1 = r0
        L_0x0043:
            if (r1 != 0) goto L_0x0078
            com.fossil.Kz4 r0 = new com.fossil.Kz4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            r2 = 600(0x258, float:8.41E-43)
            java.lang.String r3 = "code: 200 but null response"
            r1.<init>(r2, r3)
            r0.<init>(r1)
        L_0x0053:
            return r0
        L_0x0054:
            com.portfolio.platform.buddy_challenge.domain.FriendRepository$Ii r0 = new com.portfolio.platform.buddy_challenge.domain.FriendRepository$Ii
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0015
        L_0x005b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0063:
            com.fossil.El7.b(r2)
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource r2 = r6.c
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r5
            java.lang.Object r2 = r2.e(r7, r1)
            if (r2 == r0) goto L_0x0053
            r1 = r6
            goto L_0x002d
        L_0x0076:
            r1 = r3
            goto L_0x0043
        L_0x0078:
            com.fossil.Kz4 r0 = new com.fossil.Kz4
            r2 = 2
            r0.<init>(r1, r3, r2, r3)
            goto L_0x0053
        L_0x007f:
            boolean r2 = r0 instanceof com.fossil.Hq5
            if (r2 == 0) goto L_0x00c7
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r1 = r1.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "searchProfile - "
            r4.append(r5)
            r4.append(r7)
            java.lang.String r5 = " - "
            r4.append(r5)
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            int r5 = r0.a()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.e(r1, r4)
            int r2 = r0.a()
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00cd
            java.lang.String r0 = r0.getMessage()
        L_0x00bb:
            com.fossil.Kz4 r1 = new com.fossil.Kz4
            com.portfolio.platform.data.model.ServerError r3 = new com.portfolio.platform.data.model.ServerError
            r3.<init>(r2, r0)
            r1.<init>(r3)
            r0 = r1
            goto L_0x0053
        L_0x00c7:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        L_0x00cd:
            r0 = r3
            goto L_0x00bb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRepository.q(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object r(com.fossil.Xs4 r8, com.mapped.Xe6<? super com.fossil.Kz4<java.lang.Boolean>> r9) {
        /*
            r7 = this;
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            r6 = 1
            boolean r0 = r9 instanceof com.portfolio.platform.buddy_challenge.domain.FriendRepository.Ji
            if (r0 == 0) goto L_0x0043
            r0 = r9
            com.portfolio.platform.buddy_challenge.domain.FriendRepository$Ji r0 = (com.portfolio.platform.buddy_challenge.domain.FriendRepository.Ji) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x0043
            int r1 = r1 + r4
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r4 = r1.label
            if (r4 == 0) goto L_0x0052
            if (r4 != r6) goto L_0x004a
            java.lang.Object r0 = r1.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$1
            com.fossil.Xs4 r0 = (com.fossil.Xs4) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.buddy_challenge.domain.FriendRepository r1 = (com.portfolio.platform.buddy_challenge.domain.FriendRepository) r1
            com.fossil.El7.b(r2)
            r8 = r0
        L_0x0031:
            r0 = r2
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r2 = r0 instanceof com.fossil.Kq5
            if (r2 == 0) goto L_0x006f
            com.fossil.Kz4 r0 = new com.fossil.Kz4
            java.lang.Boolean r1 = com.fossil.Ao7.a(r6)
            r2 = 2
            r0.<init>(r1, r3, r2, r3)
        L_0x0042:
            return r0
        L_0x0043:
            com.portfolio.platform.buddy_challenge.domain.FriendRepository$Ji r0 = new com.portfolio.platform.buddy_challenge.domain.FriendRepository$Ji
            r0.<init>(r7, r9)
            r1 = r0
            goto L_0x0015
        L_0x004a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0052:
            com.fossil.El7.b(r2)
            java.lang.String r2 = r8.d()
            com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource r4 = r7.c
            java.lang.String r5 = r8.i()
            r1.L$0 = r7
            r1.L$1 = r8
            r1.L$2 = r2
            r1.label = r6
            java.lang.Object r2 = r4.g(r2, r5, r1)
            if (r2 == r0) goto L_0x0042
            r1 = r7
            goto L_0x0031
        L_0x006f:
            boolean r2 = r0 instanceof com.fossil.Hq5
            if (r2 == 0) goto L_0x00cd
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r1 = r1.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "sendRequest - "
            r4.append(r5)
            java.lang.String r5 = r8.d()
            r4.append(r5)
            java.lang.String r5 = " - "
            r4.append(r5)
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            int r5 = r0.a()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.e(r1, r4)
            com.portfolio.platform.data.model.ServerError r1 = r0.c()
            if (r1 == 0) goto L_0x00c8
            java.lang.Integer r1 = r1.getCode()
            if (r1 == 0) goto L_0x00c8
            int r1 = r1.intValue()
        L_0x00b1:
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00d3
            java.lang.String r0 = r0.getMessage()
        L_0x00bb:
            com.fossil.Kz4 r2 = new com.fossil.Kz4
            com.portfolio.platform.data.model.ServerError r3 = new com.portfolio.platform.data.model.ServerError
            r3.<init>(r1, r0)
            r2.<init>(r3)
            r0 = r2
            goto L_0x0042
        L_0x00c8:
            int r1 = r0.a()
            goto L_0x00b1
        L_0x00cd:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        L_0x00d3:
            r0 = r3
            goto L_0x00bb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRepository.r(com.fossil.Xs4, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object s(com.fossil.Xs4 r11, com.mapped.Xe6<? super com.fossil.Kz4<java.lang.String>> r12) {
        /*
        // Method dump skipped, instructions count: 256
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRepository.s(com.fossil.Xs4, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object t(com.fossil.Xs4 r10, com.mapped.Xe6<? super com.fossil.Kz4<java.lang.String>> r11) {
        /*
        // Method dump skipped, instructions count: 280
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FriendRepository.t(com.fossil.Xs4, com.mapped.Xe6):java.lang.Object");
    }
}
