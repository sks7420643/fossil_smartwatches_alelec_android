package com.portfolio.platform.buddy_challenge.domain;

import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Fu4;
import com.fossil.It4;
import com.mapped.An4;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileRepository {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Fu4 b;
    @DexIgnore
    public /* final */ ProfileRemoteDataSource c;
    @DexIgnore
    public /* final */ An4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRepository", f = "ProfileRepository.kt", l = {49}, m = "changeSocialId")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(ProfileRepository profileRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = profileRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRepository", f = "ProfileRepository.kt", l = {22}, m = "fetchSocialProfile")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(ProfileRepository profileRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = profileRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    @DexIgnore
    public ProfileRepository(Fu4 fu4, ProfileRemoteDataSource profileRemoteDataSource, An4 an4) {
        Wg6.c(fu4, "local");
        Wg6.c(profileRemoteDataSource, "remote");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.b = fu4;
        this.c = profileRemoteDataSource;
        this.d = an4;
        String name = ProfileRepository.class.getName();
        Wg6.b(name, "ProfileRepository::class.java.name");
        this.a = name;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r7, com.mapped.Xe6<? super com.fossil.Kz4<com.fossil.It4>> r8) {
        /*
            r6 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 0
            boolean r0 = r8 instanceof com.portfolio.platform.buddy_challenge.domain.ProfileRepository.Ai
            if (r0 == 0) goto L_0x0068
            r0 = r8
            com.portfolio.platform.buddy_challenge.domain.ProfileRepository$Ai r0 = (com.portfolio.platform.buddy_challenge.domain.ProfileRepository.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0068
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0077
            if (r3 != r4) goto L_0x006f
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.buddy_challenge.domain.ProfileRepository r0 = (com.portfolio.platform.buddy_challenge.domain.ProfileRepository) r0
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002e:
            r0 = r1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x009f
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            com.fossil.It4 r0 = (com.fossil.It4) r0
            if (r0 != 0) goto L_0x0089
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = r6.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "generateSocialProfile - Success - "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.e(r2, r0)
            com.fossil.Kz4 r0 = new com.fossil.Kz4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            r2 = 600(0x258, float:8.41E-43)
            r1.<init>(r2, r5)
            r0.<init>(r1)
        L_0x0067:
            return r0
        L_0x0068:
            com.portfolio.platform.buddy_challenge.domain.ProfileRepository$Ai r0 = new com.portfolio.platform.buddy_challenge.domain.ProfileRepository$Ai
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0015
        L_0x006f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0077:
            com.fossil.El7.b(r2)
            com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource r2 = r6.c
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r4
            java.lang.Object r1 = r2.b(r7, r1)
            if (r1 != r0) goto L_0x002e
            goto L_0x0067
        L_0x0089:
            com.mapped.An4 r1 = r6.d
            java.lang.String r2 = r0.c()
            r1.G0(r2)
            com.fossil.Fu4 r1 = r6.b
            r1.b(r0)
            com.fossil.Kz4 r1 = new com.fossil.Kz4
            r2 = 2
            r1.<init>(r0, r5, r2, r5)
            r0 = r1
            goto L_0x0067
        L_0x009f:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00d5
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = r6.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "changeSocialId - Failure - "
            r3.append(r4)
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            int r4 = r0.a()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.e(r2, r3)
            com.fossil.Kz4 r1 = new com.fossil.Kz4
            com.portfolio.platform.data.model.ServerError r2 = new com.portfolio.platform.data.model.ServerError
            int r0 = r0.a()
            r2.<init>(r0, r5)
            r1.<init>(r2)
            r0 = r1
            goto L_0x0067
        L_0x00d5:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.ProfileRepository.a(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void b() {
        this.b.a();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.mapped.Xe6<? super com.fossil.Kz4<com.fossil.It4>> r7) {
        /*
            r6 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 0
            boolean r0 = r7 instanceof com.portfolio.platform.buddy_challenge.domain.ProfileRepository.Bi
            if (r0 == 0) goto L_0x0046
            r0 = r7
            com.portfolio.platform.buddy_challenge.domain.ProfileRepository$Bi r0 = (com.portfolio.platform.buddy_challenge.domain.ProfileRepository.Bi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0046
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0054
            if (r3 != r4) goto L_0x004c
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.buddy_challenge.domain.ProfileRepository r0 = (com.portfolio.platform.buddy_challenge.domain.ProfileRepository) r0
            com.fossil.El7.b(r1)
            r6 = r0
        L_0x0028:
            r0 = r1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x007b
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            com.fossil.It4 r0 = (com.fossil.It4) r0
            if (r0 != 0) goto L_0x0065
            com.fossil.Kz4 r0 = new com.fossil.Kz4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            r2 = 600(0x258, float:8.41E-43)
            r1.<init>(r2, r5)
            r0.<init>(r1)
        L_0x0045:
            return r0
        L_0x0046:
            com.portfolio.platform.buddy_challenge.domain.ProfileRepository$Bi r0 = new com.portfolio.platform.buddy_challenge.domain.ProfileRepository$Bi
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x004c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0054:
            com.fossil.El7.b(r1)
            com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource r1 = r6.c
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r1 = r1.c(r0)
            if (r1 != r2) goto L_0x0028
            r0 = r2
            goto L_0x0045
        L_0x0065:
            com.mapped.An4 r1 = r6.d
            java.lang.String r2 = r0.c()
            r1.G0(r2)
            com.fossil.Fu4 r1 = r6.b
            r1.b(r0)
            com.fossil.Kz4 r1 = new com.fossil.Kz4
            r2 = 2
            r1.<init>(r0, r5, r2, r5)
            r0 = r1
            goto L_0x0045
        L_0x007b:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00b1
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = r6.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "downloadProfile - Failure - "
            r3.append(r4)
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            int r4 = r0.a()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.e(r2, r3)
            com.fossil.Kz4 r1 = new com.fossil.Kz4
            com.portfolio.platform.data.model.ServerError r2 = new com.portfolio.platform.data.model.ServerError
            int r0 = r0.a()
            r2.<init>(r0, r5)
            r1.<init>(r2)
            r0 = r1
            goto L_0x0045
        L_0x00b1:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.ProfileRepository.c(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final It4 d() {
        return this.b.c();
    }

    @DexIgnore
    public final LiveData<It4> e() {
        return this.b.d();
    }
}
