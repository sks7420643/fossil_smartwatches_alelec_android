package com.portfolio.platform.buddy_challenge.domain;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Jf6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FCMRepository {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b; // = "deviceToken";
    @DexIgnore
    public /* final */ An4 c;
    @DexIgnore
    public /* final */ ApiServiceV2 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository", f = "FCMRepository.kt", l = {30}, m = "registerFCM")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FCMRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(FCMRepository fCMRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = fCMRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository$registerFCM$response$1", f = "FCMRepository.kt", l = {30}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $body;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ FCMRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(FCMRepository fCMRepository, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = fCMRepository;
            this.$body = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Bi(this.this$0, this.$body, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Bi) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.d;
                Ku3 ku3 = this.$body;
                this.label = 1;
                Object pushFCMToken = apiServiceV2.pushFCMToken(ku3, this);
                return pushFCMToken == d ? d : pushFCMToken;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository", f = "FCMRepository.kt", l = {52}, m = "retry")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FCMRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(FCMRepository fCMRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = fCMRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository", f = "FCMRepository.kt", l = {72}, m = "unregisterFCM")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FCMRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(FCMRepository fCMRepository, Xe6 xe6) {
            super(xe6);
            this.this$0 = fCMRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.domain.FCMRepository$unregisterFCM$response$1", f = "FCMRepository.kt", l = {72}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $body;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ FCMRepository this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(FCMRepository fCMRepository, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = fCMRepository;
            this.$body = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Ei(this.this$0, this.$body, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Ei) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.d;
                Ku3 ku3 = this.$body;
                this.label = 1;
                Object removeFCMToken = apiServiceV2.removeFCMToken(ku3, this);
                return removeFCMToken == d ? d : removeFCMToken;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public FCMRepository(An4 an4, ApiServiceV2 apiServiceV2) {
        Wg6.c(an4, "shared");
        Wg6.c(apiServiceV2, "api");
        this.c = an4;
        this.d = apiServiceV2;
        String simpleName = FCMRepository.class.getSimpleName();
        Wg6.b(simpleName, "FCMRepository::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public final Ku3 b(String str) {
        Ku3 ku3 = new Ku3();
        ku3.n(this.b, str);
        return ku3;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(java.lang.String r10, com.mapped.Xe6<? super com.fossil.Kz4<java.lang.Boolean>> r11) {
        /*
        // Method dump skipped, instructions count: 277
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FCMRepository.c(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.mapped.Xe6<? super com.fossil.Kz4<java.lang.Boolean>> r12) {
        /*
            r11 = this;
            r10 = 0
            r4 = 0
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 1
            boolean r0 = r12 instanceof com.portfolio.platform.buddy_challenge.domain.FCMRepository.Ci
            if (r0 == 0) goto L_0x0035
            r0 = r12
            com.portfolio.platform.buddy_challenge.domain.FCMRepository$Ci r0 = (com.portfolio.platform.buddy_challenge.domain.FCMRepository.Ci) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0035
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0016:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0044
            if (r0 != r5) goto L_0x003c
            java.lang.Object r0 = r1.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.buddy_challenge.domain.FCMRepository r0 = (com.portfolio.platform.buddy_challenge.domain.FCMRepository) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x0032:
            com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
        L_0x0034:
            return r0
        L_0x0035:
            com.portfolio.platform.buddy_challenge.domain.FCMRepository$Ci r0 = new com.portfolio.platform.buddy_challenge.domain.FCMRepository$Ci
            r0.<init>(r11, r12)
            r1 = r0
            goto L_0x0016
        L_0x003c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0044:
            com.fossil.El7.b(r2)
            com.mapped.An4 r0 = r11.c
            java.lang.String r0 = r0.p()
            com.mapped.An4 r2 = r11.c
            java.lang.String r2 = r2.i()
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = r11.a
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "retry - consider - deviceToken: "
            r8.append(r9)
            r8.append(r0)
            java.lang.String r9 = " - authorisedToken: "
            r8.append(r9)
            r8.append(r2)
            java.lang.String r8 = r8.toString()
            r6.e(r7, r8)
            boolean r6 = com.mapped.Wg6.a(r0, r2)
            r6 = r6 ^ 1
            if (r6 == 0) goto L_0x009c
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r6 = r11.a
            java.lang.String r7 = "do retry"
            r4.e(r6, r7)
            r1.L$0 = r11
            r1.L$1 = r0
            r1.L$2 = r2
            r1.label = r5
            java.lang.Object r0 = r11.c(r0, r1)
            if (r0 != r3) goto L_0x0032
            r0 = r3
            goto L_0x0034
        L_0x009c:
            if (r0 == 0) goto L_0x00a4
            int r0 = r0.length()
            if (r0 != 0) goto L_0x00b4
        L_0x00a4:
            r0 = r5
        L_0x00a5:
            if (r0 == 0) goto L_0x00b6
            com.fossil.Kz4 r0 = new com.fossil.Kz4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            java.lang.String r2 = "deviceToken is empty"
            r1.<init>(r4, r2)
            r0.<init>(r1)
            goto L_0x0034
        L_0x00b4:
            r0 = r4
            goto L_0x00a5
        L_0x00b6:
            com.fossil.Kz4 r0 = new com.fossil.Kz4
            java.lang.Boolean r1 = com.fossil.Ao7.a(r5)
            r2 = 2
            r0.<init>(r1, r10, r2, r10)
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FCMRepository.d(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.mapped.Xe6<? super com.fossil.Kz4<java.lang.Boolean>> r11) {
        /*
        // Method dump skipped, instructions count: 298
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.FCMRepository.e(com.mapped.Xe6):java.lang.Object");
    }
}
