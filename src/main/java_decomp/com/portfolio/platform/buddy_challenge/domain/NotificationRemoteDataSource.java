package com.portfolio.platform.buddy_challenge.domain;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.Dt4;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Jf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationRemoteDataSource {
    @DexIgnore
    public /* final */ ApiServiceV2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Jf6 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(NotificationRemoteDataSource notificationRemoteDataSource, Xe6 xe6) {
            super(xe6);
            this.this$0 = notificationRemoteDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<Dt4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationRemoteDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(NotificationRemoteDataSource notificationRemoteDataSource, int i, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = notificationRemoteDataSource;
            this.$limit = i;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Bi(this.this$0, this.$limit, xe6);
        }

        @DexIgnore
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<ApiResponse<Dt4>>> xe6) {
            throw null;
            //return ((Bi) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                Integer e = Ao7.e(this.$limit);
                this.label = 1;
                Object notifications = apiServiceV2.notifications(e, this);
                return notifications == d ? d : notifications;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public NotificationRemoteDataSource(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        this.a = apiServiceV2;
    }

    @DexIgnore
    public static /* synthetic */ Object c(NotificationRemoteDataSource notificationRemoteDataSource, int i, Xe6 xe6, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 100;
        }
        return notificationRemoteDataSource.b(i, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(int r9, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.Dt4>>> r10) {
        /*
            r8 = this;
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource.Ai
            if (r0 == 0) goto L_0x0041
            r0 = r10
            com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource$Ai r0 = (com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource.Ai) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x0041
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x004f
            if (r4 != r5) goto L_0x0047
            int r2 = r0.I$0
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource r0 = (com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x002a:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0065
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            java.lang.Object r2 = r0.a()
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x0040:
            return r0
        L_0x0041:
            com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource$Ai r0 = new com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource$Ai
            r0.<init>(r8, r10)
            goto L_0x0014
        L_0x0047:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004f:
            com.fossil.El7.b(r1)
            com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource$Bi r1 = new com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource$Bi
            r1.<init>(r8, r9, r3)
            r0.L$0 = r8
            r0.I$0 = r9
            r0.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r1, r0)
            if (r0 != r2) goto L_0x002a
            r0 = r2
            goto L_0x0040
        L_0x0065:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x007f
            r2 = r0
            com.fossil.Hq5 r2 = (com.fossil.Hq5) r2
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r2.a()
            com.portfolio.platform.data.model.ServerError r2 = r2.c()
            r6 = 28
            r4 = r3
            r5 = r3
            r7 = r3
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0040
        L_0x007f:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource.b(int, com.mapped.Xe6):java.lang.Object");
    }
}
