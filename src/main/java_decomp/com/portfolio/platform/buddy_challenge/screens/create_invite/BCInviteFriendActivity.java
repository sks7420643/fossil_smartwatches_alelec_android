package com.portfolio.platform.buddy_challenge.screens.create_invite;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.Ts4;
import com.fossil.Vu4;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCInviteFriendActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, Ts4 ts4) {
            Wg6.c(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), BCInviteFriendActivity.class);
            intent.putExtra("challenge_draft_extra", ts4);
            fragment.startActivity(intent);
        }

        @DexIgnore
        public final void b(Fragment fragment, Ts4 ts4) {
            Wg6.c(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), BCInviteFriendActivity.class);
            intent.putExtra("challenge_draft_extra", ts4);
            fragment.startActivityForResult(intent, 11);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((Vu4) getSupportFragmentManager().Y(2131362158)) == null) {
            k(Vu4.u.b((Ts4) getIntent().getParcelableExtra("challenge_draft_extra")), Vu4.u.a(), 2131362158);
        }
    }
}
