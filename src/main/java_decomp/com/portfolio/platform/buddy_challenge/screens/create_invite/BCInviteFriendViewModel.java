package com.portfolio.platform.buddy_challenge.screens.create_invite;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gl7;
import com.fossil.Gs4;
import com.fossil.Gu7;
import com.fossil.Hl7;
import com.fossil.Im7;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Ms4;
import com.fossil.Ot4;
import com.fossil.Ps4;
import com.fossil.Py4;
import com.fossil.Ts0;
import com.fossil.Ts4;
import com.fossil.Tt4;
import com.fossil.Us0;
import com.fossil.Xs4;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.BCGenerator;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rl6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.data.model.ServerError;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCInviteFriendViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, Boolean>> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, ServerError>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<List<Ot4>, ServerError>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Gs4>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Date>> f; // = new MutableLiveData<>();
    @DexIgnore
    public Ts4 g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public List<String> i; // = new ArrayList();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<List<Ot4>, String>> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Gl7<Ps4, Long, String>> k; // = new MutableLiveData<>();
    @DexIgnore
    public List<Ot4> l;
    @DexIgnore
    public String m; // = "now";
    @DexIgnore
    public /* final */ FriendRepository n;
    @DexIgnore
    public /* final */ Tt4 o;
    @DexIgnore
    public /* final */ An4 p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchFriends$1", f = "BCInviteFriendViewModel.kt", l = {287, 296}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCInviteFriendViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchFriends$1$friendsWrapper$1", f = "BCInviteFriendViewModel.kt", l = {287}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Xs4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Xs4>>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    FriendRepository friendRepository = this.this$0.this$0.n;
                    this.L$0 = il6;
                    this.label = 1;
                    Object g = friendRepository.g(this);
                    return g == d ? d : g;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchFriends$1$localFriends$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<Xs4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<Xs4>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.n.l();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(BCInviteFriendViewModel bCInviteFriendViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCInviteFriendViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0089  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x00c7  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 250
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchUnInvitedFilterFriends$1", f = "BCInviteFriendViewModel.kt", l = {310, 311, 312, 313, 319, 325, 328}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCInviteFriendViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchUnInvitedFilterFriends$1$friends$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<Xs4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<Xs4>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.n.l();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$fetchUnInvitedFilterFriends$1$suggestedFriends$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Ot4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $combinationFriends;
            @DexIgnore
            public /* final */ /* synthetic */ List $friends;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(List list, List list2, Xe6 xe6) {
                super(2, xe6);
                this.$friends = list;
                this.$combinationFriends = list2;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.$friends, this.$combinationFriends, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends Ot4>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return Py4.m(this.$friends, this.$combinationFriends);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(BCInviteFriendViewModel bCInviteFriendViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCInviteFriendViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r5v19, types: [java.util.List] */
        /* JADX WARN: Type inference failed for: r5v23, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x00e7  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0148  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0180  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x01cf  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x020a  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x022b  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0298  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x029c  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x02f3  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0344  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x0359  */
        /* JADX WARNING: Unknown variable types count: 2 */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r21) {
            /*
            // Method dump skipped, instructions count: 886
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel", f = "BCInviteFriendViewModel.kt", l = {335, 335}, m = "loadInvitedPlayers")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCInviteFriendViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(BCInviteFriendViewModel bCInviteFriendViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCInviteFriendViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.A(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$loadInvitedPlayers$2", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Rl6<? extends Lc6<? extends Rl6<? extends Kz4<List<? extends Ms4>>>, ? extends Rl6<? extends Kz4<List<? extends Ms4>>>>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCInviteFriendViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$loadInvitedPlayers$2$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends Rl6<? extends Kz4<List<? extends Ms4>>>, ? extends Rl6<? extends Kz4<List<? extends Ms4>>>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$loadInvitedPlayers$2$1$joined$1", f = "BCInviteFriendViewModel.kt", l = {338}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Ms4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Ms4>>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Tt4 p = this.this$0.this$0.this$0.p();
                        String str = this.this$0.this$0.$challengeId;
                        if (str != null) {
                            this.L$0 = il6;
                            this.label = 1;
                            Object t = p.t(str, new String[]{"waiting"}, this);
                            return t == d ? d : t;
                        }
                        Wg6.i();
                        throw null;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$loadInvitedPlayers$2$1$pending$1", f = "BCInviteFriendViewModel.kt", l = {337}, m = "invokeSuspend")
            public static final class Biii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Ms4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Biii biii = new Biii(this.this$0, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
                    //return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Ms4>>> xe6) {
                    throw null;
                    //return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Tt4 p = this.this$0.this$0.this$0.p();
                        String str = this.this$0.this$0.$challengeId;
                        if (str != null) {
                            this.L$0 = il6;
                            this.label = 1;
                            Object t = p.t(str, new String[]{"invited"}, this);
                            return t == d ? d : t;
                        }
                        Wg6.i();
                        throw null;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends Rl6<? extends Kz4<List<? extends Ms4>>>, ? extends Rl6<? extends Kz4<List<? extends Ms4>>>>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    return Hl7.a(Gu7.b(il6, null, null, new Biii(this, null), 3, null), Gu7.b(il6, null, null, new Aiii(this, null), 3, null));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(BCInviteFriendViewModel bCInviteFriendViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCInviteFriendViewModel;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$challengeId, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Rl6<? extends Lc6<? extends Rl6<? extends Kz4<List<? extends Ms4>>>, ? extends Rl6<? extends Kz4<List<? extends Ms4>>>>>> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return Gu7.b(this.p$, Bw7.b(), null, new Aii(this, null), 2, null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCInviteFriendViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<Ps4>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Kz4<Ps4>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    if (this.this$0.this$0.h) {
                        Lc6 lc6 = (Lc6) this.this$0.this$0.d.e();
                        List<Ot4> list = lc6 != null ? (List) lc6.getFirst() : null;
                        if (!(list == null || list.isEmpty())) {
                            Ts4 a2 = BCInviteFriendViewModel.a(this.this$0.this$0);
                            ArrayList arrayList = new ArrayList(Im7.m(list, 10));
                            for (Ot4 ot4 : list) {
                                arrayList.add(ot4.b());
                            }
                            Object[] array = arrayList.toArray(new String[0]);
                            if (array != null) {
                                a2.q((String[]) array);
                            } else {
                                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                            }
                        }
                    }
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = BCInviteFriendViewModel.q;
                    local.e(str, "onInviteMoreFriends - launch - ids: " + BCInviteFriendViewModel.a(this.this$0.this$0).d());
                    Tt4 p = this.this$0.this$0.p();
                    Ei ei = this.this$0;
                    String str2 = ei.$it;
                    String[] d2 = BCInviteFriendViewModel.a(ei.this$0).d();
                    this.L$0 = il6;
                    this.label = 1;
                    Object I = p.I(str2, d2, this);
                    return I == d ? d : I;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(String str, Xe6 xe6, BCInviteFriendViewModel bCInviteFriendViewModel) {
            super(2, xe6);
            this.$it = str;
            this.this$0 = bCInviteFriendViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.$it, xe6, this.this$0);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.a.l(Hl7.a(null, Ao7.a(true)));
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Kz4 kz4 = (Kz4) g;
            this.this$0.a.l(Hl7.a(null, Ao7.a(false)));
            if (kz4.c() != null) {
                this.this$0.c.l(Hl7.a(Ao7.a(true), null));
            } else {
                this.this$0.c.l(Hl7.a(Ao7.a(false), kz4.a()));
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$onSearching$1", f = "BCInviteFriendViewModel.kt", l = {232}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $key;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCInviteFriendViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$onSearching$1$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $searchingFriends;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
                this.$searchingFriends = list;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$searchingFriends, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.j.l(Hl7.a(this.$searchingFriends, this.this$0.$key));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(BCInviteFriendViewModel bCInviteFriendViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCInviteFriendViewModel;
            this.$key = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, this.$key, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:31:0x008c  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x004a A[SYNTHETIC] */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                r11 = 0
                r4 = 0
                r5 = 1
                java.lang.Object r6 = com.fossil.Yn7.d()
                int r0 = r12.label
                if (r0 == 0) goto L_0x0023
                if (r0 != r5) goto L_0x001b
                java.lang.Object r0 = r12.L$1
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r12.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r13)
            L_0x0018:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x001a:
                return r0
            L_0x001b:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0023:
                com.fossil.El7.b(r13)
                com.mapped.Il6 r7 = r12.p$
                com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel r0 = r12.this$0
                java.util.List r0 = com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel.c(r0)
                if (r0 == 0) goto L_0x0036
                boolean r0 = r0.isEmpty()
                if (r0 == 0) goto L_0x0090
            L_0x0036:
                r0 = r5
            L_0x0037:
                if (r0 != 0) goto L_0x0018
                com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel r0 = r12.this$0
                java.util.List r0 = com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel.c(r0)
                if (r0 == 0) goto L_0x00af
                java.util.ArrayList r8 = new java.util.ArrayList
                r8.<init>()
                java.util.Iterator r9 = r0.iterator()
            L_0x004a:
                boolean r0 = r9.hasNext()
                if (r0 == 0) goto L_0x0097
                java.lang.Object r1 = r9.next()
                r0 = r1
                com.fossil.Ot4 r0 = (com.fossil.Ot4) r0
                java.lang.String r2 = r0.a()
                java.lang.String r3 = ""
                if (r2 == 0) goto L_0x0092
            L_0x005f:
                java.lang.String r10 = r12.$key
                boolean r2 = com.fossil.Wt7.u(r2, r10, r5)
                if (r2 != 0) goto L_0x0081
                java.lang.String r2 = r0.c()
                if (r2 == 0) goto L_0x00b3
            L_0x006d:
                java.lang.String r3 = r12.$key
                boolean r2 = com.fossil.Wt7.u(r2, r3, r5)
                if (r2 != 0) goto L_0x0081
                java.lang.String r0 = r0.e()
                java.lang.String r2 = r12.$key
                boolean r0 = com.fossil.Wt7.u(r0, r2, r5)
                if (r0 == 0) goto L_0x0095
            L_0x0081:
                r0 = r5
            L_0x0082:
                java.lang.Boolean r0 = com.fossil.Ao7.a(r0)
                boolean r0 = r0.booleanValue()
                if (r0 == 0) goto L_0x004a
                r8.add(r1)
                goto L_0x004a
            L_0x0090:
                r0 = r4
                goto L_0x0037
            L_0x0092:
                java.lang.String r2 = ""
                goto L_0x005f
            L_0x0095:
                r0 = r4
                goto L_0x0082
            L_0x0097:
                com.fossil.Jx7 r0 = com.fossil.Bw7.c()
                com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$Fi$Aii r1 = new com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$Fi$Aii
                r1.<init>(r12, r8, r11)
                r12.L$0 = r7
                r12.L$1 = r8
                r12.label = r5
                java.lang.Object r0 = com.fossil.Eu7.g(r0, r1, r12)
                if (r0 != r6) goto L_0x0018
                r0 = r6
                goto L_0x001a
            L_0x00af:
                com.mapped.Wg6.i()
                throw r11
            L_0x00b3:
                r2 = r3
                goto L_0x006d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel.Fi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$onStartLater$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCInviteFriendViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(BCInviteFriendViewModel bCInviteFriendViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCInviteFriendViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.this$0, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.f.l(BCGenerator.a.b());
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$onStartNow$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCInviteFriendViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(BCInviteFriendViewModel bCInviteFriendViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCInviteFriendViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.e.l(BCGenerator.a.h());
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$startChallenge$1", f = "BCInviteFriendViewModel.kt", l = {261, 272}, m = "invokeSuspend")
    public static final class Ii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCInviteFriendViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$startChallenge$1$1", f = "BCInviteFriendViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Object>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Ps4 $challenge;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ii this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ii ii, Ps4 ps4, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ii;
                this.$challenge = ps4;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$challenge, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Object> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Long f;
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.p().e();
                    String p = this.$challenge.p();
                    if (p != null && (f = Ao7.f(PortfolioApp.get.instance().m1(p))) != null) {
                        return f;
                    }
                    this.this$0.this$0.v().m0(Ao7.a(true));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$startChallenge$1$result$1", f = "BCInviteFriendViewModel.kt", l = {261}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<Ps4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $activeSerialNumber;
            @DexIgnore
            public /* final */ /* synthetic */ int $encryptedMethod;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ii this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ii ii, int i, String str, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ii;
                this.$encryptedMethod = i;
                this.$activeSerialNumber = str;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, this.$encryptedMethod, this.$activeSerialNumber, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Kz4<Ps4>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Tt4 p = this.this$0.this$0.p();
                    Ts4 a2 = BCInviteFriendViewModel.a(this.this$0.this$0);
                    int i2 = this.$encryptedMethod;
                    String str = this.$activeSerialNumber;
                    this.L$0 = il6;
                    this.label = 1;
                    Object f = p.f(a2, i2, str, this);
                    return f == d ? d : f;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(BCInviteFriendViewModel bCInviteFriendViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCInviteFriendViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ii ii = new Ii(this.this$0, xe6);
            ii.p$ = (Il6) obj;
            throw null;
            //return ii;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ii) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x006d  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x0131  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
            // Method dump skipped, instructions count: 333
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel.Ii.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = BCInviteFriendViewModel.class.getSimpleName();
        Wg6.b(simpleName, "BCInviteFriendViewModel::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public BCInviteFriendViewModel(FriendRepository friendRepository, Tt4 tt4, An4 an4) {
        Wg6.c(friendRepository, "friendRepository");
        Wg6.c(tt4, "challengeRepository");
        Wg6.c(an4, "shared");
        this.n = friendRepository;
        this.o = tt4;
        this.p = an4;
    }

    @DexIgnore
    public static final /* synthetic */ Ts4 a(BCInviteFriendViewModel bCInviteFriendViewModel) {
        Ts4 ts4 = bCInviteFriendViewModel.g;
        if (ts4 != null) {
            return ts4;
        }
        Wg6.n("cachedDraft");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object A(java.lang.String r8, com.mapped.Xe6<? super com.mapped.Lc6<? extends com.mapped.Rl6<com.fossil.Kz4<java.util.List<com.fossil.Ms4>>>, ? extends com.mapped.Rl6<com.fossil.Kz4<java.util.List<com.fossil.Ms4>>>>> r9) {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel.Ci
            if (r0 == 0) goto L_0x002f
            r0 = r9
            com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$Ci r0 = (com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel.Ci) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002f
            int r1 = r1 + r3
            r0.label = r1
            r3 = r0
        L_0x0015:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.Yn7.d()
            int r0 = r3.label
            if (r0 == 0) goto L_0x005b
            if (r0 == r5) goto L_0x003e
            if (r0 != r6) goto L_0x0036
            java.lang.Object r0 = r3.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r3.L$0
            com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel r0 = (com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel) r0
            com.fossil.El7.b(r2)
        L_0x002e:
            return r2
        L_0x002f:
            com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$Ci r0 = new com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$Ci
            r0.<init>(r7, r9)
            r3 = r0
            goto L_0x0015
        L_0x0036:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003e:
            java.lang.Object r0 = r3.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r3.L$0
            com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel r1 = (com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel) r1
            com.fossil.El7.b(r2)
            r8 = r0
        L_0x004a:
            r0 = r2
            com.mapped.Rl6 r0 = (com.mapped.Rl6) r0
            r3.L$0 = r1
            r3.L$1 = r8
            r3.label = r6
            java.lang.Object r2 = r0.l(r3)
            if (r2 != r4) goto L_0x002e
            r2 = r4
            goto L_0x002e
        L_0x005b:
            com.fossil.El7.b(r2)
            com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$Di r0 = new com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel$Di
            r1 = 0
            r0.<init>(r7, r8, r1)
            r3.L$0 = r7
            r3.L$1 = r8
            r3.label = r5
            java.lang.Object r2 = com.fossil.Ux7.c(r0, r3)
            if (r2 != r4) goto L_0x0072
            r2 = r4
            goto L_0x002e
        L_0x0072:
            r1 = r7
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel.A(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void B() {
        List<Ot4> list = this.l;
        if (list != null) {
            this.d.l(Hl7.a(list, null));
        }
        this.l = null;
    }

    @DexIgnore
    public final void C() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        StringBuilder sb = new StringBuilder();
        sb.append("onInviteMoreFriends - ids: ");
        Ts4 ts4 = this.g;
        if (ts4 != null) {
            sb.append(ts4.d());
            sb.append(" - cachedInvitedAll: ");
            sb.append(this.h);
            local.e(str, sb.toString());
            Ts4 ts42 = this.g;
            if (ts42 != null) {
                if (!(ts42.d().length == 0) || this.h) {
                    Ts4 ts43 = this.g;
                    if (ts43 != null) {
                        String c2 = ts43.c();
                        if (c2 != null) {
                            Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ei(c2, null, this), 3, null);
                            return;
                        }
                        return;
                    }
                    Wg6.n("cachedDraft");
                    throw null;
                }
                this.c.l(Hl7.a(Boolean.TRUE, null));
                return;
            }
            Wg6.n("cachedDraft");
            throw null;
        }
        Wg6.n("cachedDraft");
        throw null;
    }

    @DexIgnore
    public final void D(boolean z) {
        if (this.h != z) {
            Lc6<List<Ot4>, ServerError> e2 = this.d.e();
            List<Ot4> first = e2 != null ? e2.getFirst() : null;
            this.h = z;
            if (z) {
                if (first != null) {
                    Iterator<T> it = first.iterator();
                    while (it.hasNext()) {
                        it.next().g(true);
                    }
                }
                Ts4 ts4 = this.g;
                if (ts4 != null) {
                    ts4.r(true);
                } else {
                    Wg6.n("cachedDraft");
                    throw null;
                }
            } else {
                if (this.i.isEmpty()) {
                    if (first != null) {
                        Iterator<T> it2 = first.iterator();
                        while (it2.hasNext()) {
                            it2.next().g(false);
                        }
                    }
                } else if (first != null) {
                    for (T t : first) {
                        t.g(this.i.contains(t.b()));
                    }
                }
                Ts4 ts42 = this.g;
                if (ts42 != null) {
                    ts42.r(false);
                } else {
                    Wg6.n("cachedDraft");
                    throw null;
                }
            }
            if (first != null) {
                this.d.l(Hl7.a(first, null));
            }
        }
    }

    @DexIgnore
    public final void E(Ot4 ot4) {
        int i2 = 0;
        Wg6.c(ot4, "friend");
        if (!ot4.f() && this.h) {
            this.h = false;
            Ts4 ts4 = this.g;
            if (ts4 != null) {
                ts4.r(false);
                this.b.l(Boolean.FALSE);
                Lc6<List<Ot4>, ServerError> e2 = this.d.e();
                List<Ot4> first = e2 != null ? e2.getFirst() : null;
                if (first != null) {
                    this.i.clear();
                    Iterator<T> it = first.iterator();
                    while (it.hasNext()) {
                        this.i.add(it.next().b());
                    }
                }
            } else {
                Wg6.n("cachedDraft");
                throw null;
            }
        }
        if (ot4.f()) {
            if (!this.i.contains(ot4.b())) {
                this.i.add(ot4.b());
            }
        } else {
            this.i.remove(ot4.b());
        }
        Ts4 ts42 = this.g;
        if (ts42 != null) {
            Object[] array = this.i.toArray(new String[0]);
            if (array != null) {
                ts42.q((String[]) array);
                List<Ot4> list = this.l;
                if (list != null) {
                    Iterator<Ot4> it2 = list.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            i2 = -1;
                            break;
                        } else if (Wg6.a(it2.next().b(), ot4.b())) {
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (i2 != -1) {
                        list.get(i2).g(ot4.f());
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = q;
                StringBuilder sb = new StringBuilder();
                sb.append("cachedDraft: ");
                Ts4 ts43 = this.g;
                if (ts43 != null) {
                    sb.append(ts43);
                    local.e(str, sb.toString());
                    return;
                }
                Wg6.n("cachedDraft");
                throw null;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
        Wg6.n("cachedDraft");
        throw null;
    }

    @DexIgnore
    public final void F() {
        Ts4 ts4 = this.g;
        if (ts4 != null) {
            if (ts4.c() != null) {
                o();
            } else {
                n();
            }
        } else {
            Wg6.n("cachedDraft");
            throw null;
        }
    }

    @DexIgnore
    public final void G(String str) {
        Wg6.c(str, "key");
        Lc6<List<Ot4>, ServerError> e2 = this.d.e();
        this.l = e2 != null ? e2.getFirst() : null;
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.a(), null, new Fi(this, str, null), 2, null);
    }

    @DexIgnore
    public final void H() {
        this.m = "later";
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.a(), null, new Gi(this, null), 2, null);
    }

    @DexIgnore
    public final void I() {
        this.m = "now";
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.a(), null, new Hi(this, null), 2, null);
    }

    @DexIgnore
    public final void J(Date date) {
        Wg6.c(date, "date");
        SimpleDateFormat simpleDateFormat = TimeUtils.p.get();
        String format = simpleDateFormat != null ? simpleDateFormat.format(date) : null;
        if (format != null) {
            K(format);
        }
    }

    @DexIgnore
    public final void K(String str) {
        Ts4 ts4 = this.g;
        if (ts4 != null) {
            ts4.u(str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = q;
            StringBuilder sb = new StringBuilder();
            sb.append("startChallenge - draft: ");
            Ts4 ts42 = this.g;
            if (ts42 != null) {
                sb.append(ts42);
                local.e(str2, sb.toString());
                this.a.l(Hl7.a(null, Boolean.TRUE));
                Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ii(this, null), 3, null);
                return;
            }
            Wg6.n("cachedDraft");
            throw null;
        }
        Wg6.n("cachedDraft");
        throw null;
    }

    @DexIgnore
    public final void n() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    public final void o() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Bi(this, null), 3, null);
    }

    @DexIgnore
    public final Tt4 p() {
        return this.o;
    }

    @DexIgnore
    public final LiveData<Gl7<Ps4, Long, String>> q() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<Boolean> r() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<List<Date>> s() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, Boolean>> t() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<Lc6<List<Ot4>, String>> u() {
        return this.j;
    }

    @DexIgnore
    public final An4 v() {
        return this.p;
    }

    @DexIgnore
    public final LiveData<List<Gs4>> w() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, ServerError>> x() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Lc6<List<Ot4>, ServerError>> y() {
        return this.d;
    }

    @DexIgnore
    public final void z(Ts4 ts4) {
        boolean z = false;
        String str = null;
        this.g = ts4 != null ? ts4 : new Ts4(null, null, null, null, null, 0, 0, null, null, false, null, 2047, null);
        if (ts4 != null) {
            str = ts4.c();
        }
        if (str != null) {
            z = true;
        }
        if (z) {
            o();
        } else {
            n();
        }
    }
}
