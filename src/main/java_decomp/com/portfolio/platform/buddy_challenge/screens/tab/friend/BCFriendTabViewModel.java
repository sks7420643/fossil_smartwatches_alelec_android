package com.portfolio.platform.buddy_challenge.screens.tab.friend;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.NativeProtocol;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Cz4;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hj5;
import com.fossil.Hl7;
import com.fossil.Hm7;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Ss0;
import com.fossil.Ts0;
import com.fossil.U08;
import com.fossil.Us0;
import com.fossil.W08;
import com.fossil.Xs4;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hx5;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rl6;
import com.mapped.Rm6;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCFriendTabViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, Boolean>> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<String, String>> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<ServerError> g; // = new MutableLiveData<>();
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ U08 j; // = W08.b(false, 1, null);
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public /* final */ LiveData<List<Object>> l;
    @DexIgnore
    public /* final */ FriendRepository m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Rl6<Kz4<List<Xs4>>> a;
        @DexIgnore
        public Rl6<Kz4<List<Xs4>>> b;
        @DexIgnore
        public Rl6<Kz4<List<Xs4>>> c;
        @DexIgnore
        public Rl6<Kz4<List<Xs4>>> d;

        @DexIgnore
        public Ai(Rl6<Kz4<List<Xs4>>> rl6, Rl6<Kz4<List<Xs4>>> rl62, Rl6<Kz4<List<Xs4>>> rl63, Rl6<Kz4<List<Xs4>>> rl64) {
            Wg6.c(rl6, "sentRequestFriend");
            Wg6.c(rl62, NativeProtocol.AUDIENCE_FRIENDS);
            Wg6.c(rl63, "receivedRequestFriends");
            Wg6.c(rl64, "blockedFriends");
            this.a = rl6;
            this.b = rl62;
            this.c = rl63;
            this.d = rl64;
        }

        @DexIgnore
        public final Rl6<Kz4<List<Xs4>>> a() {
            return this.d;
        }

        @DexIgnore
        public final Rl6<Kz4<List<Xs4>>> b() {
            return this.b;
        }

        @DexIgnore
        public final Rl6<Kz4<List<Xs4>>> c() {
            return this.c;
        }

        @DexIgnore
        public final Rl6<Kz4<List<Xs4>>> d() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!Wg6.a(this.a, ai.a) || !Wg6.a(this.b, ai.b) || !Wg6.a(this.c, ai.c) || !Wg6.a(this.d, ai.d)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            Rl6<Kz4<List<Xs4>>> rl6 = this.a;
            int hashCode = rl6 != null ? rl6.hashCode() : 0;
            Rl6<Kz4<List<Xs4>>> rl62 = this.b;
            int hashCode2 = rl62 != null ? rl62.hashCode() : 0;
            Rl6<Kz4<List<Xs4>>> rl63 = this.c;
            int hashCode3 = rl63 != null ? rl63.hashCode() : 0;
            Rl6<Kz4<List<Xs4>>> rl64 = this.d;
            if (rl64 != null) {
                i = rl64.hashCode();
            }
            return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "AllFriendRequest(sentRequestFriend=" + this.a + ", friends=" + this.b + ", receivedRequestFriends=" + this.c + ", blockedFriends=" + this.d + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ BCFriendTabViewModel a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$allFriendsLive$1$1", f = "BCFriendTabViewModel.kt", l = {243}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MutableLiveData $allFriends;
            @DexIgnore
            public /* final */ /* synthetic */ List $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, MutableLiveData mutableLiveData, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$allFriends = mutableLiveData;
                this.$it = list;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$allFriends, this.$it, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX INFO: finally extract failed */
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                U08 u08;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    u08 = this.this$0.a.j;
                    this.L$0 = il6;
                    this.L$1 = u08;
                    this.label = 1;
                    if (u08.a(null, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    u08 = (U08) this.L$1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                try {
                    MutableLiveData mutableLiveData = this.$allFriends;
                    BCFriendTabViewModel bCFriendTabViewModel = this.this$0.a;
                    List list = this.$it;
                    Wg6.b(list, "it");
                    mutableLiveData.l(bCFriendTabViewModel.v(list));
                    Cd6 cd6 = Cd6.a;
                    u08.b(null);
                    return Cd6.a;
                } catch (Throwable th) {
                    u08.b(null);
                    throw th;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$allFriendsLive$1$2", f = "BCFriendTabViewModel.kt", l = {86, 86}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x004f  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 0
                    r5 = 2
                    r3 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x0051
                    if (r0 == r3) goto L_0x0035
                    if (r0 != r5) goto L_0x002d
                    java.lang.Object r0 = r7.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r8)
                L_0x0016:
                    com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$Bi r0 = r7.this$0
                    com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel r0 = r0.a
                    androidx.lifecycle.MutableLiveData r0 = com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel.d(r0)
                    r1 = 0
                    java.lang.Boolean r1 = com.fossil.Ao7.a(r1)
                    com.mapped.Lc6 r1 = com.fossil.Hl7.a(r1, r6)
                    r0.l(r1)
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x002c:
                    return r0
                L_0x002d:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0035:
                    java.lang.Object r0 = r7.L$1
                    com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel r0 = (com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel) r0
                    java.lang.Object r1 = r7.L$0
                    com.mapped.Il6 r1 = (com.mapped.Il6) r1
                    com.fossil.El7.b(r8)
                    r3 = r0
                    r2 = r8
                L_0x0042:
                    r0 = r2
                    com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$Ai r0 = (com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel.Ai) r0
                    r7.L$0 = r1
                    r7.label = r5
                    java.lang.Object r0 = r3.u(r0, r7)
                    if (r0 != r4) goto L_0x0016
                    r0 = r4
                    goto L_0x002c
                L_0x0051:
                    com.fossil.El7.b(r8)
                    com.mapped.Il6 r1 = r7.p$
                    com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$Bi r0 = r7.this$0
                    com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel r0 = r0.a
                    androidx.lifecycle.MutableLiveData r0 = com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel.d(r0)
                    java.lang.Boolean r2 = com.fossil.Ao7.a(r3)
                    com.mapped.Lc6 r2 = com.fossil.Hl7.a(r2, r6)
                    r0.l(r2)
                    com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$Bi r0 = r7.this$0
                    com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel r0 = r0.a
                    r7.L$0 = r1
                    r7.L$1 = r0
                    r7.label = r3
                    java.lang.Object r2 = r0.l(r7)
                    if (r2 != r4) goto L_0x007b
                    r0 = r4
                    goto L_0x002c
                L_0x007b:
                    r3 = r0
                    goto L_0x0042
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel.Bi.Bii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Bi(BCFriendTabViewModel bCFriendTabViewModel) {
            this.a = bCFriendTabViewModel;
        }

        @DexIgnore
        public final MutableLiveData<List<Object>> a(List<Xs4> list) {
            MutableLiveData<List<Object>> mutableLiveData = new MutableLiveData<>();
            if (list.isEmpty()) {
                if (!this.a.k) {
                    this.a.a.l(Boolean.TRUE);
                }
                mutableLiveData.l(Hm7.e());
            } else {
                this.a.a.l(Boolean.FALSE);
                this.a.b.l(Hl7.a(Boolean.FALSE, null));
                Rm6 unused = Gu7.d(Us0.a(this.a), Bw7.a(), null, new Aii(this, mutableLiveData, list, null), 2, null);
            }
            if (this.a.k) {
                this.a.k = false;
                Rm6 unused2 = Gu7.d(Us0.a(this.a), null, null, new Bii(this, null), 3, null);
            }
            return mutableLiveData;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((List) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$block$1", f = "BCFriendTabViewModel.kt", l = {181}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xs4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCFriendTabViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(BCFriendTabViewModel bCFriendTabViewModel, Xs4 xs4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCFriendTabViewModel;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$friend, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object a2;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.b.l(Hl7.a(null, Ao7.a(true)));
                FriendRepository friendRepository = this.this$0.m;
                Xs4 xs4 = this.$friend;
                this.L$0 = il6;
                this.label = 1;
                a2 = friendRepository.a(xs4, this);
                if (a2 == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                a2 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Kz4 kz4 = (Kz4) a2;
            if (kz4.a() != null) {
                this.this$0.g.l(kz4.a());
            }
            this.this$0.b.l(Hl7.a(null, Ao7.a(false)));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$cancelRequest$1", f = "BCFriendTabViewModel.kt", l = {205}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xs4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCFriendTabViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(BCFriendTabViewModel bCFriendTabViewModel, Xs4 xs4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCFriendTabViewModel;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$friend, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object b;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.b.l(Hl7.a(null, Ao7.a(true)));
                FriendRepository friendRepository = this.this$0.m;
                Xs4 xs4 = this.$friend;
                this.L$0 = il6;
                this.label = 1;
                b = friendRepository.b(xs4, this);
                if (b == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                b = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Kz4 kz4 = (Kz4) b;
            if (kz4.a() != null) {
                this.this$0.g.l(kz4.a());
            }
            this.this$0.b.l(Hl7.a(null, Ao7.a(false)));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel", f = "BCFriendTabViewModel.kt", l = {145, 145}, m = "fetchAll")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCFriendTabViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(BCFriendTabViewModel bCFriendTabViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCFriendTabViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.l(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2", f = "BCFriendTabViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Rl6<? extends Ai>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCFriendTabViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1", f = "BCFriendTabViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Ai>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1$blockedFriends$1", f = "BCFriendTabViewModel.kt", l = {160}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Xs4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Xs4>>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        FriendRepository friendRepository = this.this$0.this$0.this$0.m;
                        this.L$0 = il6;
                        this.label = 1;
                        Object f = friendRepository.f(this);
                        return f == d ? d : f;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1$friends$1", f = "BCFriendTabViewModel.kt", l = {152}, m = "invokeSuspend")
            public static final class Biii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Xs4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Biii biii = new Biii(this.this$0, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
                    //return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Xs4>>> xe6) {
                    throw null;
                    //return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        FriendRepository friendRepository = this.this$0.this$0.this$0.m;
                        this.L$0 = il6;
                        this.label = 1;
                        Object g = friendRepository.g(this);
                        return g == d ? d : g;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1$receivedRequestFriends$1", f = "BCFriendTabViewModel.kt", l = {156}, m = "invokeSuspend")
            public static final class Ciii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Xs4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Ciii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Ciii ciii = new Ciii(this.this$0, xe6);
                    ciii.p$ = (Il6) obj;
                    throw null;
                    //return ciii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Xs4>>> xe6) {
                    throw null;
                    //return ((Ciii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        FriendRepository friendRepository = this.this$0.this$0.this$0.m;
                        this.L$0 = il6;
                        this.label = 1;
                        Object h = friendRepository.h(this);
                        return h == d ? d : h;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$fetchAll$2$1$sentRequestFriends$1", f = "BCFriendTabViewModel.kt", l = {148}, m = "invokeSuspend")
            public static final class Diii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Xs4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Diii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Diii diii = new Diii(this.this$0, xe6);
                    diii.p$ = (Il6) obj;
                    throw null;
                    //return diii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Xs4>>> xe6) {
                    throw null;
                    //return ((Diii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        FriendRepository friendRepository = this.this$0.this$0.this$0.m;
                        this.L$0 = il6;
                        this.label = 1;
                        Object i2 = friendRepository.i(this);
                        return i2 == d ? d : i2;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ai> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    return new Ai(Gu7.b(il6, null, null, new Diii(this, null), 3, null), Gu7.b(il6, null, null, new Biii(this, null), 3, null), Gu7.b(il6, null, null, new Ciii(this, null), 3, null), Gu7.b(il6, null, null, new Aiii(this, null), 3, null));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(BCFriendTabViewModel bCFriendTabViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCFriendTabViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Rl6<? extends Ai>> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return Gu7.b(this.p$, Bw7.b(), null, new Aii(this, null), 2, null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel", f = "BCFriendTabViewModel.kt", l = {107, 108, 109, 110}, m = "handleFriendResult")
    public static final class Gi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public boolean Z$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCFriendTabViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(BCFriendTabViewModel bCFriendTabViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCFriendTabViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$refresh$1", f = "BCFriendTabViewModel.kt", l = {96, 96}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCFriendTabViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(BCFriendTabViewModel bCFriendTabViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCFriendTabViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x004d  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 2
                r2 = 1
                java.lang.Object r4 = com.fossil.Yn7.d()
                int r0 = r6.label
                if (r0 == 0) goto L_0x004f
                if (r0 == r2) goto L_0x0033
                if (r0 != r5) goto L_0x002b
                java.lang.Object r0 = r6.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r7)
            L_0x0015:
                com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel r0 = r6.this$0
                androidx.lifecycle.MutableLiveData r0 = com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel.d(r0)
                r1 = 0
                java.lang.Boolean r1 = com.fossil.Ao7.a(r1)
                r2 = 0
                com.mapped.Lc6 r1 = com.fossil.Hl7.a(r1, r2)
                r0.l(r1)
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x002a:
                return r0
            L_0x002b:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0033:
                java.lang.Object r0 = r6.L$1
                com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel r0 = (com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel) r0
                java.lang.Object r1 = r6.L$0
                com.mapped.Il6 r1 = (com.mapped.Il6) r1
                com.fossil.El7.b(r7)
                r3 = r0
                r2 = r7
            L_0x0040:
                r0 = r2
                com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$Ai r0 = (com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel.Ai) r0
                r6.L$0 = r1
                r6.label = r5
                java.lang.Object r0 = r3.u(r0, r6)
                if (r0 != r4) goto L_0x0015
                r0 = r4
                goto L_0x002a
            L_0x004f:
                com.fossil.El7.b(r7)
                com.mapped.Il6 r1 = r6.p$
                com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel r0 = r6.this$0
                r6.L$0 = r1
                r6.L$1 = r0
                r6.label = r2
                java.lang.Object r2 = r0.l(r6)
                if (r2 != r4) goto L_0x0064
                r0 = r4
                goto L_0x002a
            L_0x0064:
                r3 = r0
                goto L_0x0040
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel.Hi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$responseToFriendRequest$1", f = "BCFriendTabViewModel.kt", l = {219}, m = "invokeSuspend")
    public static final class Ii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $confirmation;
        @DexIgnore
        public /* final */ /* synthetic */ Xs4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCFriendTabViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(BCFriendTabViewModel bCFriendTabViewModel, boolean z, Xs4 xs4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCFriendTabViewModel;
            this.$confirmation = z;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ii ii = new Ii(this.this$0, this.$confirmation, this.$friend, xe6);
            ii.p$ = (Il6) obj;
            throw null;
            //return ii;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ii) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object p;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                FriendRepository friendRepository = this.this$0.m;
                boolean z = this.$confirmation;
                Xs4 xs4 = this.$friend;
                this.L$0 = il6;
                this.label = 1;
                p = friendRepository.p(z, xs4, this);
                if (p == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                p = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Kz4 kz4 = (Kz4) p;
            if (kz4.c() != null) {
                this.this$0.f.l(Hl7.a(this.$confirmation ? "bc_accept_friend_request" : "bc_reject_friend_request", ((Xs4) kz4.c()).d()));
            } else {
                this.this$0.g.l(kz4.a());
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$unblock$1", f = "BCFriendTabViewModel.kt", l = {193}, m = "invokeSuspend")
    public static final class Ji extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xs4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCFriendTabViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(BCFriendTabViewModel bCFriendTabViewModel, Xs4 xs4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCFriendTabViewModel;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ji ji = new Ji(this.this$0, this.$friend, xe6);
            ji.p$ = (Il6) obj;
            throw null;
            //return ji;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ji) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object t;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.b.l(Hl7.a(null, Ao7.a(true)));
                FriendRepository friendRepository = this.this$0.m;
                Xs4 xs4 = this.$friend;
                this.L$0 = il6;
                this.label = 1;
                t = friendRepository.t(xs4, this);
                if (t == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                t = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Kz4 kz4 = (Kz4) t;
            if (kz4.a() != null) {
                this.this$0.g.l(kz4.a());
            }
            this.this$0.b.l(Hl7.a(null, Ao7.a(false)));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$unfriend$1", f = "BCFriendTabViewModel.kt", l = {170}, m = "invokeSuspend")
    public static final class Ki extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xs4 $friend;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCFriendTabViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(BCFriendTabViewModel bCFriendTabViewModel, Xs4 xs4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCFriendTabViewModel;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ki ki = new Ki(this.this$0, this.$friend, xe6);
            ki.p$ = (Il6) obj;
            throw null;
            //return ki;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ki) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object s;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.b.l(Hl7.a(null, Ao7.a(true)));
                FriendRepository friendRepository = this.this$0.m;
                Xs4 xs4 = this.$friend;
                this.L$0 = il6;
                this.label = 1;
                s = friendRepository.s(xs4, this);
                if (s == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                s = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Kz4 kz4 = (Kz4) s;
            if (kz4.a() != null) {
                this.this$0.g.l(kz4.a());
            }
            this.this$0.b.l(Hl7.a(null, Ao7.a(false)));
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = BCFriendTabViewModel.class.getSimpleName();
        Wg6.b(simpleName, "BCFriendTabViewModel::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public BCFriendTabViewModel(An4 an4, FriendRepository friendRepository) {
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(friendRepository, "friendRepository");
        this.m = friendRepository;
        LiveData<List<Object>> c2 = Ss0.c(Cz4.a(this.m.m()), new Bi(this));
        Wg6.b(c2, "Transformations.switchMa\u2026\n        allFriends\n    }");
        this.l = c2;
    }

    @DexIgnore
    public final void j(Xs4 xs4) {
        Wg6.c(xs4, "friend");
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Ci(this, xs4, null), 2, null);
    }

    @DexIgnore
    public final void k(Xs4 xs4) {
        Wg6.c(xs4, "friend");
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Di(this, xs4, null), 2, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object l(com.mapped.Xe6<? super com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel.Ai> r7) {
        /*
            r6 = this;
            r5 = 2
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel.Ei
            if (r0 == 0) goto L_0x002b
            r0 = r7
            com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$Ei r0 = (com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel.Ei) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002b
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0015:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0051
            if (r0 == r4) goto L_0x003a
            if (r0 != r5) goto L_0x0032
            java.lang.Object r0 = r2.L$0
            com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel r0 = (com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel) r0
            com.fossil.El7.b(r1)
        L_0x002a:
            return r1
        L_0x002b:
            com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$Ei r0 = new com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$Ei
            r0.<init>(r6, r7)
            r2 = r0
            goto L_0x0015
        L_0x0032:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003a:
            java.lang.Object r0 = r2.L$0
            com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel r0 = (com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel) r0
            com.fossil.El7.b(r1)
            r6 = r0
        L_0x0042:
            r0 = r1
            com.mapped.Rl6 r0 = (com.mapped.Rl6) r0
            r2.L$0 = r6
            r2.label = r5
            java.lang.Object r1 = r0.l(r2)
            if (r1 != r3) goto L_0x002a
            r1 = r3
            goto L_0x002a
        L_0x0051:
            com.fossil.El7.b(r1)
            com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$Fi r0 = new com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$Fi
            r1 = 0
            r0.<init>(r6, r1)
            r2.L$0 = r6
            r2.label = r4
            java.lang.Object r1 = com.fossil.Jv7.e(r0, r2)
            if (r1 != r3) goto L_0x0042
            r1 = r3
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel.l(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final LiveData<List<Object>> m() {
        return this.l;
    }

    @DexIgnore
    public final LiveData<String> n() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Boolean> o() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<Boolean> p() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, Boolean>> q() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<Boolean> r() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Lc6<String, String>> s() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<ServerError> t() {
        return this.g;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0067, code lost:
        if (r1 != null) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0191, code lost:
        if (r1 != null) goto L_0x0193;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x01ed, code lost:
        if (r1 != null) goto L_0x01ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0245, code lost:
        if (r1 != null) goto L_0x0247;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0075  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ed  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0186  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01b6  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01e2  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0210  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x023a  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0266  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0269  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x02b5  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x02c0  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x02cc  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02e2 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object u(com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel.Ai r16, com.mapped.Xe6<? super com.mapped.Cd6> r17) {
        /*
        // Method dump skipped, instructions count: 763
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel.u(com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel$Ai, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final List<Object> v(List<Xs4> list) {
        return list.isEmpty() ? Hm7.e() : Hj5.c(list);
    }

    @DexIgnore
    public final void w() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Hi(this, null), 3, null);
    }

    @DexIgnore
    public final void x(Xs4 xs4, boolean z) {
        Wg6.c(xs4, "friend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = n;
        local.e(str, "responseToFriendRequest - friend: " + xs4 + " - confirmation: " + z);
        this.e.l(xs4.d());
        if (Hx5.b(PortfolioApp.get.instance())) {
            Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Ii(this, z, xs4, null), 2, null);
        } else {
            this.d.l(Boolean.TRUE);
        }
    }

    @DexIgnore
    public final void y(Xs4 xs4) {
        Wg6.c(xs4, "friend");
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Ji(this, xs4, null), 2, null);
    }

    @DexIgnore
    public final void z(Xs4 xs4) {
        Wg6.c(xs4, "friend");
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Ki(this, xs4, null), 2, null);
    }
}
