package com.portfolio.platform.buddy_challenge.screens.pending;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.At4;
import com.fossil.Bw7;
import com.fossil.Cz4;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gl7;
import com.fossil.Gs4;
import com.fossil.Gu7;
import com.fossil.Hl7;
import com.fossil.Ht4;
import com.fossil.Iz4;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Ms4;
import com.fossil.Ps4;
import com.fossil.Pw4;
import com.fossil.Py4;
import com.fossil.Ts0;
import com.fossil.Tt4;
import com.fossil.Us0;
import com.fossil.Vy4;
import com.fossil.Xs4;
import com.fossil.Xy4;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.BCGenerator;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Jh6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel;
import com.portfolio.platform.data.model.ServerError;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCWaitingChallengeDetailViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ Ai r; // = new Ai(null);
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, ServerError>> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, Boolean>> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Ps4> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Gl7<List<At4>, ServerError, Integer>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Gs4>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Vy4, Ps4>> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Iz4> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, ServerError>> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Gl7<String, String, String>> i; // = new MutableLiveData<>();
    @DexIgnore
    public LiveData<Ps4> j;
    @DexIgnore
    public String k;
    @DexIgnore
    public Ps4 l;
    @DexIgnore
    public Timer m;
    @DexIgnore
    public /* final */ Tt4 n;
    @DexIgnore
    public /* final */ FriendRepository o;
    @DexIgnore
    public /* final */ An4 p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return BCWaitingChallengeDetailViewModel.q;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$acceptOrJoin$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {121, 136, 155}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isAccepted;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCWaitingChallengeDetailViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$acceptOrJoin$1$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Object>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Ps4 $challenge;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Ps4 ps4, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$challenge = ps4;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$challenge, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Object> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Long f;
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.n.e();
                    String p = this.$challenge.p();
                    if (p != null && (f = Ao7.f(PortfolioApp.get.instance().m1(p))) != null) {
                        return f;
                    }
                    this.this$0.this$0.p.m0(Ao7.a(true));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$acceptOrJoin$1$currentChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Ps4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ps4> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.n.g(new String[]{"running", "waiting"}, Xy4.a.a());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$acceptOrJoin$1$result$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {137}, m = "invokeSuspend")
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<Ps4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $activeSerialNumber;
            @DexIgnore
            public /* final */ /* synthetic */ int $encryptedMethod;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Bi bi, int i, String str, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$encryptedMethod = i;
                this.$activeSerialNumber = str;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, this.$encryptedMethod, this.$activeSerialNumber, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Kz4<Ps4>> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Tt4 tt4 = this.this$0.this$0.n;
                    String b = BCWaitingChallengeDetailViewModel.b(this.this$0.this$0);
                    int i2 = this.$encryptedMethod;
                    String str = this.$activeSerialNumber;
                    this.L$0 = il6;
                    this.label = 1;
                    Object D = tt4.D(b, i2, str, this);
                    return D == d ? d : D;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel, boolean z, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCWaitingChallengeDetailViewModel;
            this.$isAccepted = z;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$isAccepted, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x007b  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00b4  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0129  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x01f9  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 540
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {331, 332, 336, 342}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCWaitingChallengeDetailViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<Xs4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<Xs4>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.o.l();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1$friendsIn$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends At4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Jh6 $friends;
            @DexIgnore
            public /* final */ /* synthetic */ List $players;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Jh6 jh6, List list, Xe6 xe6) {
                super(2, xe6);
                this.$friends = jh6;
                this.$players = list;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.$friends, this.$players, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends At4>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return Py4.i(this.$friends.element, this.$players);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1$friendsWrapper$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {331}, m = "invokeSuspend")
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Xs4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Xs4>>> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    FriendRepository friendRepository = this.this$0.this$0.o;
                    this.L$0 = il6;
                    this.label = 1;
                    Object g = friendRepository.g(this);
                    return g == d ? d : g;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1$playersWrapper$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {332}, m = "invokeSuspend")
        public static final class Dii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Ms4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Dii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Dii dii = new Dii(this.this$0, xe6);
                dii.p$ = (Il6) obj;
                throw null;
                //return dii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Ms4>>> xe6) {
                throw null;
                //return ((Dii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Tt4 tt4 = this.this$0.this$0.n;
                    String str = this.this$0.$challengeId;
                    this.L$0 = il6;
                    this.label = 1;
                    Object t = tt4.t(str, new String[]{"waiting", "running", "completed", "left_after_start"}, this);
                    return t == d ? d : t;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCWaitingChallengeDetailViewModel;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$challengeId, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x00a6  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00f8  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x0139  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0157  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0160  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0174  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 378
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$leaveChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {198}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCWaitingChallengeDetailViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$leaveChallenge$1$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {Action.Music.MUSIC_END_ACTION, 210}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$leaveChallenge$1$1$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Long>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $synDataStr;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(String str, Xe6 xe6) {
                    super(2, xe6);
                    this.$synDataStr = str;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.$synDataStr, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Long> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        return Ao7.f(PortfolioApp.get.instance().m1(this.$synDataStr));
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x006e  */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x0132  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r15) {
                /*
                // Method dump skipped, instructions count: 335
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel.Di.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCWaitingChallengeDetailViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.b.l(Hl7.a(null, Ao7.a(true)));
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$loadChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCWaitingChallengeDetailViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCWaitingChallengeDetailViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel = this.this$0;
                this.L$0 = il6;
                this.label = 1;
                if (bCWaitingChallengeDetailViewModel.B(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.l(Hl7.a(Ao7.a(false), null));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel", f = "BCWaitingChallengeDetailViewModel.kt", l = {306}, m = "loadChallengeFromServer")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCWaitingChallengeDetailViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCWaitingChallengeDetailViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.B(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$loadChallengeFromServer$result$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {306}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<Ps4>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCWaitingChallengeDetailViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCWaitingChallengeDetailViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.this$0, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Kz4<Ps4>> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Tt4 tt4 = this.this$0.n;
                String b = BCWaitingChallengeDetailViewModel.b(this.this$0);
                this.L$0 = il6;
                this.label = 1;
                Object n = Tt4.n(tt4, b, null, this, 2, null);
                return n == d ? d : n;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$loadForVisitChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {105}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCWaitingChallengeDetailViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCWaitingChallengeDetailViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.b.l(Hl7.a(null, Ao7.a(true)));
                BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel = this.this$0;
                this.L$0 = il6;
                this.label = 1;
                if (bCWaitingChallengeDetailViewModel.B(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.l(Hl7.a(null, Ao7.a(false)));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii extends TimerTask {
        @DexIgnore
        public /* final */ /* synthetic */ BCWaitingChallengeDetailViewModel b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;
        @DexIgnore
        public /* final */ /* synthetic */ Ps4 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ii this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Object>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Object> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        Ii ii = this.this$0.this$0;
                        if (ii.c) {
                            ii.d.v("running");
                        } else {
                            ii.d.v("completed");
                        }
                        int c = this.this$0.this$0.b.n.c(this.this$0.this$0.d.f());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = BCWaitingChallengeDetailViewModel.r.a();
                        local.e(a2, "starAndStopChallenge - count: " + c);
                        if (c > 0) {
                            return Ao7.f(this.this$0.this$0.b.n.B(this.this$0.this$0.d));
                        }
                        this.this$0.this$0.b.c.l(this.this$0.this$0.d);
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Xe6 xe6, Ii ii) {
                super(2, xe6);
                this.this$0 = ii;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Dv7 b = Bw7.b();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (Eu7.g(b, aiii, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Ii(BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel, boolean z, Ps4 ps4) {
            this.b = bCWaitingChallengeDetailViewModel;
            this.c = z;
            this.d = ps4;
        }

        @DexIgnore
        public void run() {
            Rm6 unused = Gu7.d(Us0.a(this.b), null, null, new Aii(null, this), 3, null);
        }
    }

    /*
    static {
        String simpleName = BCWaitingChallengeDetailViewModel.class.getSimpleName();
        Wg6.b(simpleName, "BCWaitingChallengeDetail\u2026el::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public BCWaitingChallengeDetailViewModel(Tt4 tt4, FriendRepository friendRepository, An4 an4) {
        Wg6.c(tt4, "challengeRepository");
        Wg6.c(friendRepository, "friendRepository");
        Wg6.c(an4, "shared");
        this.n = tt4;
        this.o = friendRepository;
        this.p = an4;
    }

    @DexIgnore
    public static final /* synthetic */ String b(BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel) {
        String str = bCWaitingChallengeDetailViewModel.k;
        if (str != null) {
            return str;
        }
        Wg6.n("challengeId");
        throw null;
    }

    @DexIgnore
    public final void A() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ei(this, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object B(com.mapped.Xe6<? super com.mapped.Cd6> r8) {
        /*
            r7 = this;
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = 1
            boolean r0 = r8 instanceof com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel.Fi
            if (r0 == 0) goto L_0x005f
            r0 = r8
            com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$Fi r0 = (com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel.Fi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x005f
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x006d
            if (r3 != r6) goto L_0x0065
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel r0 = (com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel) r0
            com.fossil.El7.b(r1)
            r7 = r0
        L_0x0027:
            r0 = r1
            com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
            java.lang.Object r1 = r0.c()
            com.fossil.Ps4 r1 = (com.fossil.Ps4) r1
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel.q
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "loadChallengeFromServer - challenge: "
            r4.append(r5)
            r4.append(r1)
            java.lang.String r4 = r4.toString()
            r2.e(r3, r4)
            if (r1 == 0) goto L_0x0086
            r7.l = r1
            androidx.lifecycle.LiveData<com.fossil.Ps4> r0 = r7.j
            if (r0 != 0) goto L_0x0059
            androidx.lifecycle.MutableLiveData<com.fossil.Ps4> r0 = r7.c
            r0.l(r1)
        L_0x0059:
            r7.I(r1)
        L_0x005c:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x005e:
            return r0
        L_0x005f:
            com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$Fi r0 = new com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$Fi
            r0.<init>(r7, r8)
            goto L_0x0013
        L_0x0065:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x006d:
            com.fossil.El7.b(r1)
            com.fossil.Dv7 r1 = com.fossil.Bw7.b()
            com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$Gi r3 = new com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$Gi
            r4 = 0
            r3.<init>(r7, r4)
            r0.L$0 = r7
            r0.label = r6
            java.lang.Object r1 = com.fossil.Eu7.g(r1, r3, r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
            goto L_0x005e
        L_0x0086:
            com.portfolio.platform.data.model.ServerError r0 = r0.a()
            androidx.lifecycle.LiveData<com.fossil.Ps4> r1 = r7.j
            if (r1 != 0) goto L_0x009c
            androidx.lifecycle.MutableLiveData<com.mapped.Lc6<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r1 = r7.h
            java.lang.Boolean r2 = com.fossil.Ao7.a(r6)
            com.mapped.Lc6 r0 = com.fossil.Hl7.a(r2, r0)
            r1.l(r0)
            goto L_0x005c
        L_0x009c:
            androidx.lifecycle.MutableLiveData<com.mapped.Lc6<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r1 = r7.h
            r2 = 0
            java.lang.Boolean r2 = com.fossil.Ao7.a(r2)
            com.mapped.Lc6 r0 = com.fossil.Hl7.a(r2, r0)
            r1.l(r0)
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel.B(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void C(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = q;
        local.e(str2, "loadForVisitChallenge - visitId: " + str);
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Hi(this, null), 3, null);
    }

    @DexIgnore
    public final void D() {
        LiveData<Ps4> liveData = this.j;
        Ps4 e2 = liveData != null ? liveData.e() : null;
        if (e2 != null) {
            String l0 = PortfolioApp.get.instance().l0();
            Ht4 i2 = e2.i();
            if (Wg6.a(l0, i2 != null ? i2.b() : null) && Wg6.a("waiting", e2.n())) {
                long b2 = Xy4.a.b();
                Date m2 = e2.m();
                if (b2 < (m2 != null ? m2.getTime() : 0)) {
                    this.e.l(BCGenerator.a.f());
                    return;
                }
            }
            this.e.l(BCGenerator.a.e());
        }
    }

    @DexIgnore
    public final void E(Vy4 vy4) {
        Wg6.c(vy4, "option");
        LiveData<Ps4> liveData = this.j;
        Ps4 e2 = liveData != null ? liveData.e() : null;
        if (e2 != null) {
            int i2 = Pw4.a[vy4.ordinal()];
            if (i2 == 1) {
                m(Vy4.EDIT, e2);
            } else if (i2 == 2) {
                m(Vy4.ADD_FRIENDS, e2);
            } else if (i2 == 3) {
                m(Vy4.LEAVE, e2);
            }
        }
    }

    @DexIgnore
    public final void F() {
        Timer timer = this.m;
        if (timer != null) {
            timer.cancel();
        }
    }

    @DexIgnore
    public final void G() {
        A();
        String str = this.k;
        if (str != null) {
            n(str);
        } else {
            Wg6.n("challengeId");
            throw null;
        }
    }

    @DexIgnore
    public final void H() {
        Ps4 e2 = this.c.e();
        if (e2 == null) {
            LiveData<Ps4> liveData = this.j;
            e2 = liveData != null ? liveData.e() : null;
        }
        I(e2 != null ? Py4.a(e2) : null);
    }

    @DexIgnore
    public final void I(Ps4 ps4) {
        long b2;
        long j2 = 0;
        FLogger.INSTANCE.getLocal().e(q, "starAndStopChallenge - challenge: " + ps4);
        if (ps4 != null) {
            boolean z = true;
            if (!Wg6.a("completed", ps4.n())) {
                Date m2 = ps4.m();
                long time = m2 != null ? m2.getTime() : 0;
                Date e2 = ps4.e();
                long time2 = e2 != null ? e2.getTime() : 0;
                if (!Wg6.a("waiting", ps4.n())) {
                    b2 = time2 - Xy4.a.b();
                    z = false;
                } else if (Xy4.a.b() < time2) {
                    b2 = time - Xy4.a.b();
                } else {
                    b2 = 0;
                    z = false;
                }
                FLogger.INSTANCE.getLocal().e(BCCurrentChallengeSubTabViewModel.n.a(), "starAndStopChallenge - startTime: " + time + " - endTime: " + time2 + " - exactTime: " + Xy4.a.b() + "- timeLeft: " + b2 + " - isStart: " + z);
                if (b2 >= 0) {
                    j2 = b2;
                }
                Timer timer = this.m;
                if (timer != null) {
                    timer.cancel();
                }
                Timer timer2 = new Timer();
                this.m = timer2;
                if (timer2 != null) {
                    timer2.schedule(new Ii(this, z, ps4), j2);
                }
            }
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.e(str, "acceptOrJoin - isAccepted: " + z);
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Bi(this, z, null), 3, null);
    }

    @DexIgnore
    public final void m(Vy4 vy4, Ps4 ps4) {
        this.f.l(new Lc6<>(vy4, ps4));
    }

    @DexIgnore
    public final void n(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = q;
        local.e(str2, "friendsInChallenge - challengeId: " + str);
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ci(this, str, null), 3, null);
    }

    @DexIgnore
    public final LiveData<Gl7<String, String, String>> o() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<Ps4> p() {
        return Cz4.a(this.c);
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, ServerError>> q() {
        return this.h;
    }

    @DexIgnore
    public final LiveData<Gl7<List<At4>, ServerError, Integer>> r() {
        return Cz4.a(this.d);
    }

    @DexIgnore
    public final LiveData<Ps4> s() {
        return this.j;
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, Boolean>> t() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, ServerError>> u() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<Lc6<Vy4, Ps4>> v() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<List<Gs4>> w() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Iz4> x() {
        return this.g;
    }

    @DexIgnore
    public final void y(Ps4 ps4, String str) {
        String str2;
        Date m2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = q;
        local.e(str3, "init - challenge: " + ps4 + " - visitId: " + str);
        if (str == null) {
            this.l = ps4;
            if (ps4 == null || (str2 = ps4.f()) == null) {
                str2 = "";
            }
            this.k = str2;
            Tt4 tt4 = this.n;
            if (str2 != null) {
                this.j = Cz4.a(tt4.b(str2));
                I(ps4);
                if (Xy4.a.b() < ((ps4 == null || (m2 = ps4.m()) == null) ? 0 : m2.getTime())) {
                    A();
                }
            } else {
                Wg6.n("challengeId");
                throw null;
            }
        } else {
            this.k = str;
            C(str);
        }
        Tt4 tt42 = this.n;
        String str4 = this.k;
        if (str4 != null) {
            tt42.J(str4, false);
            String str5 = this.k;
            if (str5 != null) {
                n(str5);
            } else {
                Wg6.n("challengeId");
                throw null;
            }
        } else {
            Wg6.n("challengeId");
            throw null;
        }
    }

    @DexIgnore
    public final void z() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Di(this, null), 3, null);
    }
}
