package com.portfolio.platform.buddy_challenge.screens.leaderboard;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.Gv4;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCLeaderBoardActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, long j, int i, String str2) {
            Wg6.c(fragment, "fragment");
            Wg6.c(str, "challengeId");
            Intent intent = new Intent(fragment.getContext(), BCLeaderBoardActivity.class);
            intent.putExtra("challenge_id_extra", str);
            intent.putExtra("challenge_start_time_extra", j);
            intent.putExtra("challenge_target_extra", i);
            intent.putExtra("challenge_status_extra", str2);
            fragment.startActivityForResult(intent, 15);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((Gv4) getSupportFragmentManager().Y(2131362158)) == null) {
            k(Gv4.v.b(getIntent().getStringExtra("challenge_id_extra"), getIntent().getLongExtra("challenge_start_time_extra", 0), getIntent().getIntExtra("challenge_target_extra", -1), getIntent().getStringExtra("challenge_status_extra")), Gv4.v.a(), 2131362158);
        }
    }
}
