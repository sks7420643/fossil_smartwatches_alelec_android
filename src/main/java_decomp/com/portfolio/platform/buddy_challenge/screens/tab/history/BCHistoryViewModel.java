package com.portfolio.platform.buddy_challenge.screens.tab.history;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.Ao7;
import com.fossil.Bt4;
import com.fossil.Bw7;
import com.fossil.Cz4;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Py4;
import com.fossil.Qt4;
import com.fossil.Ss0;
import com.fossil.Ts0;
import com.fossil.Tt4;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCHistoryViewModel extends Ts0 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public boolean b; // = true;
    @DexIgnore
    public Range c;
    @DexIgnore
    public /* final */ LiveData<List<Qt4>> d;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> e;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> f;
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, ServerError>> g;
    @DexIgnore
    public /* final */ Tt4 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$historyChallengeFromServer$2", f = "BCHistoryViewModel.kt", l = {124}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Bt4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public /* final */ /* synthetic */ int $offset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCHistoryViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(BCHistoryViewModel bCHistoryViewModel, int i, int i2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCHistoryViewModel;
            this.$limit = i;
            this.$offset = i2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, this.$limit, this.$offset, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super List<? extends Bt4>> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:17:0x006b, code lost:
            if (r0 == r1) goto L_0x004b;
         */
        @DexIgnore
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
            // Method dump skipped, instructions count: 278
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ BCHistoryViewModel a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$historyChallengesLive$1$1", f = "BCHistoryViewModel.kt", l = {41}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $histories;
            @DexIgnore
            public /* final */ /* synthetic */ MutableLiveData $result;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$historyChallengesLive$1$1$uiHistories$1", f = "BCHistoryViewModel.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Qt4>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super List<? extends Qt4>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        List list = this.this$0.$histories;
                        Wg6.b(list, "histories");
                        return Py4.l(list);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(List list, MutableLiveData mutableLiveData, Xe6 xe6) {
                super(2, xe6);
                this.$histories = list;
                this.$result = mutableLiveData;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$histories, this.$result, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Dv7 a2 = Bw7.a();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    g = Eu7.g(a2, aiii, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.$result.l((List) g);
                return Cd6.a;
            }
        }

        @DexIgnore
        public Bi(BCHistoryViewModel bCHistoryViewModel) {
            this.a = bCHistoryViewModel;
        }

        @DexIgnore
        public final MutableLiveData<List<Qt4>> a(List<Bt4> list) {
            MutableLiveData<List<Qt4>> mutableLiveData = new MutableLiveData<>();
            if (!list.isEmpty()) {
                this.a.f.l(Boolean.FALSE);
                this.a.e.l(Boolean.FALSE);
            } else if (!this.a.b) {
                this.a.f.l(Boolean.TRUE);
            }
            if (this.a.b) {
                this.a.b = false;
                this.a.p();
            }
            Rm6 unused = Gu7.d(Us0.a(this.a), null, null, new Aii(list, mutableLiveData, null), 3, null);
            return mutableLiveData;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((List) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$loadHistoryChallenge$1", f = "BCHistoryViewModel.kt", l = {65}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCHistoryViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(BCHistoryViewModel bCHistoryViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCHistoryViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                BCHistoryViewModel bCHistoryViewModel = this.this$0;
                this.L$0 = il6;
                this.label = 1;
                if (bCHistoryViewModel.o(5, 0, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.e.l(Ao7.a(false));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$loadMore$1", f = "BCHistoryViewModel.kt", l = {112, 115}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $position;
        @DexIgnore
        public /* final */ /* synthetic */ int $total;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCHistoryViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(BCHistoryViewModel bCHistoryViewModel, int i, int i2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCHistoryViewModel;
            this.$position = i;
            this.$total = i2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$position, this.$total, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Integer e;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.this$0.a;
                StringBuilder sb = new StringBuilder();
                sb.append("loadMore: hasNext: ");
                Range range = this.this$0.c;
                sb.append(range != null ? Ao7.a(range.isHasNext()) : null);
                sb.append(" - offset: ");
                Range range2 = this.this$0.c;
                sb.append(range2 != null ? Ao7.e(range2.getOffset()) : null);
                sb.append(" - position: ");
                sb.append(this.$position);
                sb.append(" - total: ");
                sb.append(this.$total);
                local.e(str, sb.toString());
                int i2 = this.$position + 1;
                if (i2 % 5 == 0 && i2 == this.$total) {
                    List<Qt4> e2 = this.this$0.m().e();
                    if (!(e2 == null || e2.isEmpty()) && e2.size() < 15) {
                        if (this.this$0.c == null) {
                            BCHistoryViewModel bCHistoryViewModel = this.this$0;
                            this.L$0 = il6;
                            this.I$0 = i2;
                            this.L$1 = e2;
                            this.label = 1;
                            if (bCHistoryViewModel.o(5, i2, this) == d) {
                                return d;
                            }
                        } else if (this.this$0.c != null) {
                            Range range3 = this.this$0.c;
                            if (range3 == null) {
                                Wg6.i();
                                throw null;
                            } else if (range3.isHasNext()) {
                                Range range4 = this.this$0.c;
                                if (i2 > ((range4 == null || (e = Ao7.e(range4.getOffset())) == null) ? 0 : e.intValue())) {
                                    BCHistoryViewModel bCHistoryViewModel2 = this.this$0;
                                    this.L$0 = il6;
                                    this.I$0 = i2;
                                    this.L$1 = e2;
                                    this.label = 2;
                                    if (bCHistoryViewModel2.o(5, i2, this) == d) {
                                        return d;
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (i == 1 || i == 2) {
                List list = (List) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel$refresh$1", f = "BCHistoryViewModel.kt", l = {75, 79, 82, 84, 89}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCHistoryViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(BCHistoryViewModel bCHistoryViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCHistoryViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                List<Qt4> e = this.this$0.m().e();
                if (e == null || e.isEmpty()) {
                    BCHistoryViewModel bCHistoryViewModel = this.this$0;
                    this.L$0 = il6;
                    this.L$1 = e;
                    this.label = 5;
                    if (bCHistoryViewModel.o(5, 0, this) == d) {
                        return d;
                    }
                } else if (e.size() == 15) {
                    BCHistoryViewModel bCHistoryViewModel2 = this.this$0;
                    this.L$0 = il6;
                    this.L$1 = e;
                    this.label = 1;
                    if (bCHistoryViewModel2.o(5, 0, this) == d) {
                        return d;
                    }
                } else {
                    int size = (e.size() / 5) * 5;
                    if (this.this$0.c == null) {
                        BCHistoryViewModel bCHistoryViewModel3 = this.this$0;
                        this.L$0 = il6;
                        this.L$1 = e;
                        this.I$0 = size;
                        this.label = 2;
                        if (bCHistoryViewModel3.o(5, size, this) == d) {
                            return d;
                        }
                    } else {
                        Range range = this.this$0.c;
                        if (range == null) {
                            Wg6.i();
                            throw null;
                        } else if (range.isHasNext()) {
                            BCHistoryViewModel bCHistoryViewModel4 = this.this$0;
                            this.L$0 = il6;
                            this.L$1 = e;
                            this.I$0 = size;
                            this.label = 3;
                            if (bCHistoryViewModel4.o(5, size, this) == d) {
                                return d;
                            }
                        } else {
                            BCHistoryViewModel bCHistoryViewModel5 = this.this$0;
                            this.L$0 = il6;
                            this.L$1 = e;
                            this.I$0 = size;
                            this.label = 4;
                            if (bCHistoryViewModel5.o(5, 0, this) == d) {
                                return d;
                            }
                        }
                    }
                }
            } else if (i == 1 || i == 2 || i == 3 || i == 4 || i == 5) {
                List list = (List) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.e.l(Ao7.a(false));
            return Cd6.a;
        }
    }

    @DexIgnore
    public BCHistoryViewModel(Tt4 tt4) {
        Wg6.c(tt4, "challengeRepository");
        this.h = tt4;
        String simpleName = BCHistoryViewModel.class.getSimpleName();
        Wg6.b(simpleName, "BCHistoryViewModel::class.java.simpleName");
        this.a = simpleName;
        LiveData<List<Qt4>> c2 = Ss0.c(Cz4.a(this.h.A()), new Bi(this));
        Wg6.b(c2, "Transformations.switchMa\u2026  }\n\n        result\n    }");
        this.d = c2;
        this.e = new MutableLiveData<>();
        this.f = new MutableLiveData<>();
        this.g = new MutableLiveData<>();
    }

    @DexIgnore
    public final LiveData<Boolean> k() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, ServerError>> l() {
        return this.g;
    }

    @DexIgnore
    public final LiveData<List<Qt4>> m() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Boolean> n() {
        return this.e;
    }

    @DexIgnore
    public final /* synthetic */ Object o(int i, int i2, Xe6<? super List<Bt4>> xe6) {
        return Eu7.g(Bw7.b(), new Ai(this, i, i2, null), xe6);
    }

    @DexIgnore
    public final void p() {
        FLogger.INSTANCE.getLocal().e(this.a, "loadHistoryChallenge");
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ci(this, null), 3, null);
    }

    @DexIgnore
    public final void q(int i, int i2) {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Di(this, i, i2, null), 3, null);
    }

    @DexIgnore
    public final void r(int i, int i2) {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ei(this, null), 3, null);
    }
}
