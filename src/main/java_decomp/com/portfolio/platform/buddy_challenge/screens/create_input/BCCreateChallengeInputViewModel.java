package com.portfolio.platform.buddy_challenge.screens.create_input;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gl7;
import com.fossil.Gs4;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Ps4;
import com.fossil.Py4;
import com.fossil.Ts0;
import com.fossil.Ts4;
import com.fossil.Tt4;
import com.fossil.Us0;
import com.fossil.Us4;
import com.fossil.Vs4;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.BCGenerator;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.buddy_challenge.screens.tab.friend.BCFriendTabViewModel;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCCreateChallengeInputViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public /* final */ MutableLiveData<Ts4> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Gs4>> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Bi> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Ai> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Integer> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Integer> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Integer> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> l; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Gl7<Boolean, ServerError, Ts4>> m; // = new MutableLiveData<>();
    @DexIgnore
    public Ts4 n;
    @DexIgnore
    public Ts4 o;
    @DexIgnore
    public List<Gs4> p; // = BCGenerator.a.g();
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public /* final */ Tt4 s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ boolean a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Ai(boolean z, int i) {
            this.a = z;
            this.b = i;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final boolean b() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!(this.a == ai.a && this.b == ai.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            boolean z = this.a;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return (i * 31) + this.b;
        }

        @DexIgnore
        public String toString() {
            return "NameRule(isValid=" + this.a + ", length=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ Ts4 a;

        @DexIgnore
        public Bi(Ts4 ts4) {
            Wg6.c(ts4, "challengeDraft");
            this.a = ts4;
        }

        @DexIgnore
        public final Ts4 a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || ((obj instanceof Bi) && Wg6.a(this.a, ((Bi) obj).a));
        }

        @DexIgnore
        public int hashCode() {
            Ts4 ts4 = this.a;
            if (ts4 != null) {
                return ts4.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "NextState(challengeDraft=" + this.a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputViewModel$onDone$1", f = "BCCreateChallengeInputViewModel.kt", l = {259}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCCreateChallengeInputViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputViewModel$onDone$1$result$1", f = "BCCreateChallengeInputViewModel.kt", l = {261, 263}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<Ps4>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Kz4<Ps4>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object l;
                Object l2;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    if (Wg6.a(BCCreateChallengeInputViewModel.b(this.this$0.this$0).k(), "activity_best_result")) {
                        Tt4 t = this.this$0.this$0.t();
                        Ts4 ts4 = this.this$0.this$0.n;
                        String c = ts4 != null ? ts4.c() : null;
                        if (c != null) {
                            Ts4 ts42 = this.this$0.this$0.n;
                            String e = ts42 != null ? ts42.e() : null;
                            Ts4 ts43 = this.this$0.this$0.n;
                            String a2 = ts43 != null ? ts43.a() : null;
                            this.L$0 = il6;
                            this.label = 1;
                            l2 = Tt4.l(t, c, e, a2, null, this, 8, null);
                            if (l2 == d) {
                                return d;
                            }
                            return (Kz4) l2;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Tt4 t2 = this.this$0.this$0.t();
                    Ts4 ts44 = this.this$0.this$0.n;
                    String c2 = ts44 != null ? ts44.c() : null;
                    if (c2 != null) {
                        Ts4 ts45 = this.this$0.this$0.n;
                        String e2 = ts45 != null ? ts45.e() : null;
                        Ts4 ts46 = this.this$0.this$0.n;
                        String a3 = ts46 != null ? ts46.a() : null;
                        this.L$0 = il6;
                        this.label = 2;
                        l = Tt4.l(t2, c2, e2, a3, null, this, 8, null);
                        if (l == d) {
                            return d;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    l2 = obj;
                    return (Kz4) l2;
                } else if (i == 2) {
                    Il6 il63 = (Il6) this.L$0;
                    El7.b(obj);
                    l = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return (Kz4) l;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCCreateChallengeInputViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.l.l(Ao7.a(true));
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Kz4 kz4 = (Kz4) g;
            this.this$0.l.l(Ao7.a(false));
            if (kz4.c() != null) {
                this.this$0.m.l(new Gl7(Ao7.a(true), null, this.this$0.n));
            } else {
                this.this$0.m.l(new Gl7(Ao7.a(false), kz4.a(), null));
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = BCFriendTabViewModel.class.getSimpleName();
        Wg6.b(simpleName, "BCFriendTabViewModel::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public BCCreateChallengeInputViewModel(An4 an4, Tt4 tt4) {
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(tt4, "mChallengeRepository");
        this.s = tt4;
    }

    @DexIgnore
    public static final /* synthetic */ Ts4 b(BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel) {
        Ts4 ts4 = bCCreateChallengeInputViewModel.o;
        if (ts4 != null) {
            return ts4;
        }
        Wg6.n("originalDraft");
        throw null;
    }

    @DexIgnore
    public final LiveData<Boolean> A() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<Boolean> B() {
        return this.e;
    }

    @DexIgnore
    public final void C(Vs4 vs4) {
        if (vs4 != null) {
            String d2 = vs4.d();
            this.n = new Ts4(null, null, null, vs4.g(), d2, vs4.e(), vs4.b(), null, null, false, null, FailureCode.FAILED_TO_CLEAR_DATA, null);
            e();
        }
        k();
    }

    @DexIgnore
    public final void D(Ts4 ts4) {
        Wg6.c(ts4, "draft");
        this.n = ts4;
        this.o = new Ts4(null, ts4.e(), ts4.a(), null, null, ts4.i(), ts4.b(), null, null, false, null, 1945, null);
        e();
    }

    @DexIgnore
    public final void E(String str) {
        Wg6.c(str, "newDes");
        Ts4 ts4 = this.n;
        if (ts4 != null) {
            ts4.n(str);
        }
        if (str.length() > 0) {
            if (str.length() > 500) {
                this.h.l(Boolean.FALSE);
            } else {
                this.h.l(Boolean.TRUE);
            }
        }
        j();
    }

    @DexIgnore
    public final void F() {
        Ts4 ts4 = this.o;
        if (ts4 != null) {
            Ts4 ts42 = this.n;
            if (ts42 == null) {
                Wg6.i();
                throw null;
            } else if (Py4.c(ts4, ts42)) {
                this.m.l(new Gl7<>(Boolean.TRUE, null, null));
            } else {
                Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ci(this, null), 3, null);
            }
        } else {
            Wg6.n("originalDraft");
            throw null;
        }
    }

    @DexIgnore
    public final void G(CharSequence charSequence) {
        if (charSequence != null) {
            if (!(charSequence.length() > 0)) {
                this.q = 0;
                f(0);
                Ts4 ts4 = this.n;
                if (ts4 != null) {
                    ts4.p((this.q * 60 * 60) + (this.r * 60));
                }
                j();
            } else if (charSequence.length() > 3) {
                f(this.q);
                return;
            } else {
                int parseInt = Integer.parseInt(charSequence.toString());
                if (this.q != parseInt) {
                    this.q = parseInt;
                    Ts4 ts42 = this.n;
                    if (ts42 != null) {
                        ts42.p((parseInt * 60 * 60) + (this.r * 60));
                    }
                    if (parseInt <= 9 && charSequence.length() > 1) {
                        f(this.q);
                    }
                    j();
                } else if (parseInt == 0 && charSequence.length() > 1) {
                    f(0);
                }
            }
            n();
        }
    }

    @DexIgnore
    public final void H(CharSequence charSequence) {
        if (charSequence != null) {
            if (charSequence.length() > 0) {
                int parseInt = Integer.parseInt(charSequence.toString());
                if (this.r != parseInt) {
                    this.r = parseInt;
                    if (parseInt <= 9 && charSequence.length() > 1) {
                        g(parseInt);
                    } else if (parseInt > 59) {
                        this.r = 59;
                        g(59);
                    }
                    Ts4 ts4 = this.n;
                    if (ts4 != null) {
                        ts4.p((this.q * 60 * 60) + (this.r * 60));
                    }
                    j();
                    n();
                } else if (parseInt == 0 && charSequence.length() > 1) {
                    g(parseInt);
                }
            } else {
                this.r = 0;
                g(0);
                Ts4 ts42 = this.n;
                if (ts42 != null) {
                    ts42.p((this.q * 60 * 60) + (this.r * 60));
                }
                j();
                n();
            }
        }
    }

    @DexIgnore
    public final void I(String str) {
        Wg6.c(str, "newName");
        M(str.length());
        Ts4 ts4 = this.n;
        if (ts4 != null) {
            ts4.s(str);
            j();
        }
    }

    @DexIgnore
    public final void J() {
        Ts4 ts4 = this.n;
        if (ts4 != null) {
            i(ts4);
        }
    }

    @DexIgnore
    public final void K(Gs4 gs4) {
        Wg6.c(gs4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
        Ts4 ts4 = this.n;
        if (ts4 != null) {
            Object a2 = gs4.a();
            if (!(a2 instanceof String)) {
                a2 = null;
            }
            String str = (String) a2;
            if (str == null) {
                str = "public_with_friend";
            }
            ts4.t(str);
        }
        k();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = t;
        local.e(str2, "onPrivacyChanged - privacyModels: " + this.p);
    }

    @DexIgnore
    public final void L(CharSequence charSequence) {
        int i2 = 0;
        if (charSequence != null) {
            if (!(charSequence.length() > 0)) {
                Ts4 ts4 = this.n;
                if (ts4 != null) {
                    ts4.v(0);
                }
                m(0);
                j();
                l(0);
            } else if (charSequence.length() > 7) {
                Ts4 ts42 = this.n;
                if (ts42 != null) {
                    i2 = ts42.i();
                }
                m(i2);
            } else {
                int parseInt = Integer.parseInt(charSequence.toString());
                Ts4 ts43 = this.n;
                if (ts43 == null || parseInt != ts43.i()) {
                    Ts4 ts44 = this.n;
                    if (ts44 != null) {
                        ts44.v(parseInt);
                    }
                    if (parseInt <= 9 && charSequence.length() > 1) {
                        m(parseInt);
                    }
                    l(parseInt);
                    j();
                } else if (parseInt == 0 && charSequence.length() > 1) {
                    m(0);
                }
            }
        }
    }

    @DexIgnore
    public final void M(int i2) {
        if (1 <= i2 && 32 >= i2) {
            Ai e2 = this.g.e();
            if (e2 == null || !e2.b()) {
                h(true, i2);
                return;
            }
            return;
        }
        Ai e3 = this.g.e();
        if (e3 == null || e3.b()) {
            h(false, i2);
        }
    }

    @DexIgnore
    public final void e() {
        this.a.l(this.n);
    }

    @DexIgnore
    public final void f(int i2) {
        this.i.l(Integer.valueOf(i2));
    }

    @DexIgnore
    public final void g(int i2) {
        this.j.l(Integer.valueOf(i2));
    }

    @DexIgnore
    public final void h(boolean z, int i2) {
        this.g.l(new Ai(z, i2));
    }

    @DexIgnore
    public final void i(Ts4 ts4) {
        this.c.l(new Bi(ts4));
    }

    @DexIgnore
    public final void j() {
        String str;
        Ts4 ts4 = this.n;
        if (ts4 == null || (str = ts4.a()) == null) {
            str = "";
        }
        if (str.length() > 0) {
            MutableLiveData<Boolean> mutableLiveData = this.d;
            Ts4 ts42 = this.n;
            mutableLiveData.l(Boolean.valueOf((ts42 != null ? Us4.a(ts42) : false) && str.length() <= 500));
            return;
        }
        MutableLiveData<Boolean> mutableLiveData2 = this.d;
        Ts4 ts43 = this.n;
        mutableLiveData2.l(ts43 != null ? Boolean.valueOf(Us4.a(ts43)) : null);
    }

    @DexIgnore
    public final void k() {
        this.b.l(this.p);
    }

    @DexIgnore
    public final void l(int i2) {
        if (1000 <= i2 && 300000 >= i2) {
            if (!Wg6.a(this.f.e(), Boolean.TRUE)) {
                this.f.l(Boolean.TRUE);
            }
        } else if (!Wg6.a(this.f.e(), Boolean.FALSE)) {
            this.f.l(Boolean.FALSE);
        }
    }

    @DexIgnore
    public final void m(int i2) {
        this.k.l(Integer.valueOf(i2));
    }

    @DexIgnore
    public final void n() {
        int i2 = (this.q * 60 * 60) + (this.r * 60);
        if (3600 <= i2 && 259200 >= i2) {
            if (!Wg6.a(this.e.e(), Boolean.TRUE)) {
                this.e.l(Boolean.TRUE);
            }
        } else if (!Wg6.a(this.e.e(), Boolean.FALSE)) {
            this.e.l(Boolean.FALSE);
        }
    }

    @DexIgnore
    public final LiveData<Boolean> o() {
        return this.h;
    }

    @DexIgnore
    public final LiveData<Ts4> p() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<Gl7<Boolean, ServerError, Ts4>> q() {
        return this.m;
    }

    @DexIgnore
    public final LiveData<Integer> r() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<Boolean> s() {
        return this.l;
    }

    @DexIgnore
    public final Tt4 t() {
        return this.s;
    }

    @DexIgnore
    public final LiveData<Integer> u() {
        return this.j;
    }

    @DexIgnore
    public final LiveData<Ai> v() {
        return this.g;
    }

    @DexIgnore
    public final LiveData<Bi> w() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Boolean> x() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<List<Gs4>> y() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<Integer> z() {
        return this.k;
    }
}
