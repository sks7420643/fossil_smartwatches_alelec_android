package com.portfolio.platform.buddy_challenge.screens.overview_leaderboard;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.Bt4;
import com.fossil.Bw7;
import com.fossil.Cz4;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gl7;
import com.fossil.Gs4;
import com.fossil.Gu7;
import com.fossil.Hl7;
import com.fossil.Hm7;
import com.fossil.Hw4;
import com.fossil.Ko7;
import com.fossil.Ks4;
import com.fossil.Kz4;
import com.fossil.Ms4;
import com.fossil.Ps4;
import com.fossil.Ss0;
import com.fossil.Ts0;
import com.fossil.Tt4;
import com.fossil.Us0;
import com.fossil.Vy4;
import com.fossil.Xy4;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.BCGenerator;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCOverviewLeaderBoardViewModel extends Ts0 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ MutableLiveData<Ps4> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Object>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, Boolean>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Integer, Ms4>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, ServerError>> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Gs4>> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Gl7<Boolean, Boolean, Boolean>> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, ServerError>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Gl7<String, String, Integer>> j; // = new MutableLiveData<>();
    @DexIgnore
    public LiveData<Object> k;
    @DexIgnore
    public String l;
    @DexIgnore
    public /* final */ Tt4 m;
    @DexIgnore
    public /* final */ An4 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel", f = "BCOverviewLeaderBoardViewModel.kt", l = {267, 273}, m = "calculateTopNear")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCOverviewLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCOverviewLeaderBoardViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, null, 0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$calculateTopNear$2", f = "BCOverviewLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ms4 $current;
        @DexIgnore
        public /* final */ /* synthetic */ int $numberOfPlayers;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCOverviewLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel, int i, Ms4 ms4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCOverviewLeaderBoardViewModel;
            this.$numberOfPlayers = i;
            this.$current = ms4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$numberOfPlayers, this.$current, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.e.l(Hl7.a(Ao7.e(this.$numberOfPlayers), this.$current));
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$calculateTopNear$3", f = "BCOverviewLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $allPlayers;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCOverviewLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel, List list, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCOverviewLeaderBoardViewModel;
            this.$allPlayers = list;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$allPlayers, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.c.l(this.$allPlayers);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel", f = "BCOverviewLeaderBoardViewModel.kt", l = {230}, m = "fetchChallengeWithId")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCOverviewLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCOverviewLeaderBoardViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.n(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchChallengeWithId$wrapper$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {230}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<Ps4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCOverviewLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCOverviewLeaderBoardViewModel;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$challengeId, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Kz4<Ps4>> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Tt4 tt4 = this.this$0.m;
                String str = this.$challengeId;
                this.L$0 = il6;
                this.label = 1;
                Object n = Tt4.n(tt4, str, null, this, 2, null);
                return n == d ? d : n;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ BCOverviewLeaderBoardViewModel a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchFocusedPlayersHistory$1$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {217, 219}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Bt4 $historyChallenge;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Bt4 bt4, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
                this.$historyChallenge = bt4;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$historyChallenge, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i != 0) {
                    if (i == 1) {
                        Lc6 lc6 = (Lc6) this.L$2;
                        List list = (List) this.L$1;
                    } else if (i != 2) {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    Il6 il6 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    El7.b(obj);
                    Il6 il62 = this.p$;
                    Bt4 bt4 = this.$historyChallenge;
                    if (bt4 != null) {
                        List<Ms4> k = bt4.k();
                        Lc6 H = this.this$0.a.H(k, 3, 3);
                        int size = k.size();
                        this.L$0 = il62;
                        this.L$1 = k;
                        this.L$2 = H;
                        this.label = 1;
                        if (this.this$0.a.m((List) H.getFirst(), (List) H.getSecond(), size, this) == d) {
                            return d;
                        }
                    } else {
                        Fi fi = this.this$0;
                        BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel = fi.a;
                        String str = fi.b;
                        this.L$0 = il62;
                        this.label = 2;
                        if (bCOverviewLeaderBoardViewModel.p(str, this) == d) {
                            return d;
                        }
                    }
                }
                this.this$0.a.d.l(Hl7.a(Ao7.a(false), null));
                return Cd6.a;
            }
        }

        @DexIgnore
        public Fi(BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel, String str) {
            this.a = bCOverviewLeaderBoardViewModel;
            this.b = str;
        }

        @DexIgnore
        public final MutableLiveData<Object> a(Bt4 bt4) {
            Rm6 unused = Gu7.d(Us0.a(this.a), Bw7.b(), null, new Aii(this, bt4, null), 2, null);
            return new MutableLiveData<>();
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Bt4) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel", f = "BCOverviewLeaderBoardViewModel.kt", l = {242, 257}, m = "fetchFocusedPlayersOnServer")
    public static final class Gi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCOverviewLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCOverviewLeaderBoardViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.p(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchFocusedPlayersOnServer$playersWrapper$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {243}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Ks4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCOverviewLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCOverviewLeaderBoardViewModel;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, this.$challengeId, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Ks4>>> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Tt4 tt4 = this.this$0.m;
                ArrayList c = Hm7.c(this.$challengeId);
                this.L$0 = il6;
                this.label = 1;
                Object q = Tt4.q(tt4, c, 3, 3, false, this, 8, null);
                return q == d ? d : q;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchPlayersForHistory$2", f = "BCOverviewLeaderBoardViewModel.kt", l = {104, 108, 112}, m = "invokeSuspend")
    public static final class Ii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $historyId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCOverviewLeaderBoardViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$fetchPlayersForHistory$2$historyChallenge$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Bt4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ii this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ii ii, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ii;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Bt4> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.m.z(this.this$0.$historyId);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCOverviewLeaderBoardViewModel;
            this.$historyId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ii ii = new Ii(this.this$0, this.$historyId, xe6);
            ii.p$ = (Il6) obj;
            throw null;
            //return ii;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ii) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x008b  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00aa  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0107  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 268
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.Ii.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$init$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {73, 74, 76}, m = "invokeSuspend")
    public static final class Ji extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ps4 $challenge;
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ String $historyId;
        @DexIgnore
        public /* final */ /* synthetic */ int $numberOfPlayers;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCOverviewLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel, String str, String str2, int i, Ps4 ps4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCOverviewLeaderBoardViewModel;
            this.$historyId = str;
            this.$challengeId = str2;
            this.$numberOfPlayers = i;
            this.$challenge = ps4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ji ji = new Ji(this.this$0, this.$historyId, this.$challengeId, this.$numberOfPlayers, this.$challenge, xe6);
            ji.p$ = (Il6) obj;
            throw null;
            //return ji;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ji) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0053  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                r7 = 3
                r6 = 2
                r5 = 1
                java.lang.Object r1 = com.fossil.Yn7.d()
                int r0 = r8.label
                if (r0 == 0) goto L_0x0055
                if (r0 == r5) goto L_0x003e
                if (r0 == r6) goto L_0x0023
                if (r0 != r7) goto L_0x001b
                java.lang.Object r0 = r8.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r9)
            L_0x0018:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x001a:
                return r0
            L_0x001b:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0023:
                java.lang.Object r0 = r8.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r9)
            L_0x002a:
                com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel r2 = r8.this$0
                com.fossil.Ps4 r3 = r8.$challenge
                java.lang.String r3 = r3.f()
                r8.L$0 = r0
                r8.label = r7
                java.lang.Object r0 = r2.n(r3, r8)
                if (r0 != r1) goto L_0x0018
                r0 = r1
                goto L_0x001a
            L_0x003e:
                java.lang.Object r0 = r8.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r9)
            L_0x0045:
                com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel r2 = r8.this$0
                java.lang.String r3 = r8.$challengeId
                r8.L$0 = r0
                r8.label = r6
                java.lang.Object r2 = r2.p(r3, r8)
                if (r2 != r1) goto L_0x002a
                r0 = r1
                goto L_0x001a
            L_0x0055:
                com.fossil.El7.b(r9)
                com.mapped.Il6 r0 = r8.p$
                java.lang.String r2 = r8.$historyId
                if (r2 != 0) goto L_0x0070
                com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel r2 = r8.this$0
                java.lang.String r3 = r8.$challengeId
                int r4 = r8.$numberOfPlayers
                r8.L$0 = r0
                r8.label = r5
                java.lang.Object r2 = r2.C(r3, r4, r8)
                if (r2 != r1) goto L_0x0045
                r0 = r1
                goto L_0x001a
            L_0x0070:
                com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel r0 = r8.this$0
                com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.a(r0, r2)
                goto L_0x0018
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.Ji.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$initDisplayPlayer$2", f = "BCOverviewLeaderBoardViewModel.kt", l = {195}, m = "invokeSuspend")
    public static final class Ki extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ int $numberOfPlayers;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCOverviewLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel, String str, int i, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCOverviewLeaderBoardViewModel;
            this.$challengeId = str;
            this.$numberOfPlayers = i;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ki ki = new Ki(this.this$0, this.$challengeId, this.$numberOfPlayers, xe6);
            ki.p$ = (Il6) obj;
            throw null;
            //return ki;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ki) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Ks4 j = this.this$0.m.j(this.$challengeId);
                if (j != null) {
                    List<Ms4> d2 = j.d();
                    List<Ms4> b = j.b();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = this.this$0.a;
                    local.e(str, "initDisplayPlayer - top: " + d2 + " - near: " + b);
                    BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel = this.this$0;
                    int i2 = this.$numberOfPlayers;
                    this.L$0 = il6;
                    this.L$1 = j;
                    this.L$2 = d2;
                    this.L$3 = b;
                    this.label = 1;
                    if (bCOverviewLeaderBoardViewModel.m(d2, b, i2, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                List list = (List) this.L$3;
                List list2 = (List) this.L$2;
                Ks4 ks4 = (Ks4) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$leaveChallenge$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {126, 137}, m = "invokeSuspend")
    public static final class Li extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCOverviewLeaderBoardViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$leaveChallenge$1$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Long>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $encryptedDataStr;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(String str, Xe6 xe6) {
                super(2, xe6);
                this.$encryptedDataStr = str;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$encryptedDataStr, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Long> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return Ao7.f(PortfolioApp.get.instance().m1(this.$encryptedDataStr));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$leaveChallenge$1$wrapper$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {126}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<Ps4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Ps4 $challenge;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Li this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Li li, Ps4 ps4, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = li;
                this.$challenge = ps4;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, this.$challenge, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Kz4<Ps4>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Tt4 tt4 = this.this$0.this$0.m;
                    String f = this.$challenge.f();
                    this.L$0 = il6;
                    this.label = 1;
                    Object E = tt4.E(f, this);
                    return E == d ? d : E;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Li(BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCOverviewLeaderBoardViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Li li = new Li(this.this$0, xe6);
            li.p$ = (Il6) obj;
            throw null;
            //return li;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Li) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x006e  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0176  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
            // Method dump skipped, instructions count: 422
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.Li.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$refresh$1", f = "BCOverviewLeaderBoardViewModel.kt", l = {91, 92, 95}, m = "invokeSuspend")
    public static final class Mi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCOverviewLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Mi(BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCOverviewLeaderBoardViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Mi mi = new Mi(this.this$0, xe6);
            mi.p$ = (Il6) obj;
            throw null;
            //return mi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Mi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0057  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                r11 = this;
                r10 = 0
                r9 = 3
                r8 = 2
                r7 = 1
                java.lang.Object r2 = com.fossil.Yn7.d()
                int r0 = r11.label
                if (r0 == 0) goto L_0x0059
                if (r0 == r7) goto L_0x003a
                if (r0 == r8) goto L_0x0012
                if (r0 != r9) goto L_0x0032
            L_0x0012:
                java.lang.Object r0 = r11.L$1
                com.fossil.Ps4 r0 = (com.fossil.Ps4) r0
                java.lang.Object r0 = r11.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r12)
            L_0x001d:
                com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel r0 = r11.this$0
                androidx.lifecycle.MutableLiveData r0 = com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.k(r0)
                r1 = 0
                java.lang.Boolean r1 = com.fossil.Ao7.a(r1)
                com.mapped.Lc6 r1 = com.fossil.Hl7.a(r1, r10)
                r0.l(r1)
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0031:
                return r0
            L_0x0032:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x003a:
                java.lang.Object r0 = r11.L$1
                com.fossil.Ps4 r0 = (com.fossil.Ps4) r0
                java.lang.Object r1 = r11.L$0
                com.mapped.Il6 r1 = (com.mapped.Il6) r1
                com.fossil.El7.b(r12)
            L_0x0045:
                com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel r3 = r11.this$0
                java.lang.String r4 = r0.f()
                r11.L$0 = r1
                r11.L$1 = r0
                r11.label = r8
                java.lang.Object r0 = r3.n(r4, r11)
                if (r0 != r2) goto L_0x001d
                r0 = r2
                goto L_0x0031
            L_0x0059:
                com.fossil.El7.b(r12)
                com.mapped.Il6 r1 = r11.p$
                com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel r0 = r11.this$0
                androidx.lifecycle.MutableLiveData r0 = com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.f(r0)
                java.lang.Object r0 = r0.e()
                com.fossil.Ps4 r0 = (com.fossil.Ps4) r0
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel r4 = r11.this$0
                java.lang.String r4 = com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.e(r4)
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "refresh - challenge: "
                r5.append(r6)
                r5.append(r0)
                java.lang.String r5 = r5.toString()
                r3.e(r4, r5)
                com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel r3 = r11.this$0
                java.lang.String r3 = com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.c(r3)
                if (r3 != 0) goto L_0x00a8
                if (r0 == 0) goto L_0x001d
                com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel r3 = r11.this$0
                java.lang.String r4 = r0.f()
                r11.L$0 = r1
                r11.L$1 = r0
                r11.label = r7
                java.lang.Object r3 = r3.p(r4, r11)
                if (r3 != r2) goto L_0x0045
                r0 = r2
                goto L_0x0031
            L_0x00a8:
                com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel r3 = r11.this$0
                java.lang.String r4 = com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.c(r3)
                if (r4 == 0) goto L_0x00bf
                r11.L$0 = r1
                r11.L$1 = r0
                r11.label = r9
                java.lang.Object r0 = r3.q(r4, r11)
                if (r0 != r2) goto L_0x001d
                r0 = r2
                goto L_0x0031
            L_0x00bf:
                com.mapped.Wg6.i()
                throw r10
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.Mi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public BCOverviewLeaderBoardViewModel(Tt4 tt4, An4 an4) {
        Wg6.c(tt4, "challengeRepository");
        Wg6.c(an4, "shared");
        this.m = tt4;
        this.n = an4;
        String simpleName = BCOverviewLeaderBoardViewModel.class.getSimpleName();
        Wg6.b(simpleName, "BCOverviewLeaderBoardVie\u2026el::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public final LiveData<Gl7<Boolean, Boolean, Boolean>> A() {
        return this.h;
    }

    @DexIgnore
    public final void B(Ps4 ps4, String str) {
        this.l = str;
        if (ps4 != null) {
            Integer h2 = ps4.h();
            int intValue = h2 != null ? h2.intValue() : 0;
            String f2 = ps4.f();
            this.b.l(ps4);
            Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ji(this, str, f2, intValue, ps4, null), 3, null);
        }
    }

    @DexIgnore
    public final /* synthetic */ Object C(String str, int i2, Xe6<? super Cd6> xe6) {
        Object g2 = Eu7.g(Bw7.b(), new Ki(this, str, i2, null), xe6);
        return g2 == Yn7.d() ? g2 : Cd6.a;
    }

    @DexIgnore
    public final void D() {
        FLogger.INSTANCE.getLocal().e(this.a, "leave challenge");
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Li(this, null), 3, null);
    }

    @DexIgnore
    public final void E() {
        String n2;
        Ps4 e2 = this.b.e();
        if (e2 != null && (n2 = e2.n()) != null) {
            int hashCode = n2.hashCode();
            if (hashCode != -1402931637) {
                if (hashCode == 1550783935 && n2.equals("running")) {
                    if (this.l == null) {
                        long b2 = Xy4.a.b();
                        Date e3 = e2.e();
                        if (b2 < (e3 != null ? e3.getTime() : 0)) {
                            this.g.l(BCGenerator.a.d());
                            return;
                        }
                    }
                    this.g.l(BCGenerator.a.c());
                }
            } else if (n2.equals("completed")) {
                this.g.l(BCGenerator.a.c());
            }
        }
    }

    @DexIgnore
    public final void F(Vy4 vy4) {
        Wg6.c(vy4, "option");
        int i2 = Hw4.a[vy4.ordinal()];
        if (i2 == 1) {
            MutableLiveData<Gl7<Boolean, Boolean, Boolean>> mutableLiveData = this.h;
            Boolean bool = Boolean.TRUE;
            Boolean bool2 = Boolean.FALSE;
            mutableLiveData.l(new Gl7<>(bool, bool2, bool2));
        } else if (i2 == 2) {
            this.h.l(new Gl7<>(Boolean.FALSE, Boolean.TRUE, Boolean.FALSE));
        } else if (i2 == 3) {
            MutableLiveData<Gl7<Boolean, Boolean, Boolean>> mutableLiveData2 = this.h;
            Boolean bool3 = Boolean.FALSE;
            mutableLiveData2.l(new Gl7<>(bool3, bool3, Boolean.TRUE));
        }
    }

    @DexIgnore
    public final void G() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Mi(this, null), 3, null);
    }

    @DexIgnore
    public final Lc6<List<Ms4>, List<Ms4>> H(List<Ms4> list, int i2, int i3) {
        FLogger.INSTANCE.getLocal().e(this.a, "topAndNearFromPlayers - players: " + list);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        try {
            int size = list.size();
            int i4 = 0;
            T t = null;
            for (T t2 : list) {
                if (i4 >= 0) {
                    T t3 = t2;
                    if (i4 < i2) {
                        arrayList.add(t3);
                        if (Wg6.a(t3.d(), PortfolioApp.get.instance().l0())) {
                            i4++;
                            t = t3;
                        }
                    }
                    t3 = t;
                    i4++;
                    t = t3;
                } else {
                    Hm7.l();
                    throw null;
                }
            }
            if (t != null) {
                for (int i5 = i2; i5 < i3 + i2; i5++) {
                    if (i5 < size) {
                        arrayList2.add(list.get(i5));
                    }
                }
            } else if (size > i2) {
                for (int i6 = i2; i6 < size; i6++) {
                    if (Wg6.a(list.get(i6).d(), PortfolioApp.get.instance().l0())) {
                        if (i6 == size - 1) {
                            int i7 = i6 - i3;
                            while (true) {
                                i7++;
                                if (i7 >= size) {
                                    break;
                                } else if (i7 > i2 - 1) {
                                    arrayList2.add(list.get(i7));
                                }
                            }
                        } else if (i6 == i2) {
                            for (int i8 = i6; i8 < i6 + i3; i8++) {
                                if (i8 < size) {
                                    arrayList2.add(list.get(i8));
                                }
                            }
                        } else {
                            for (int i9 = i6 - (i3 / 2); i9 < (i6 + i3) - 1; i9++) {
                                arrayList2.add(list.get(i9));
                            }
                        }
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return Hl7.a(arrayList, arrayList2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00bf, code lost:
        if (r0 != null) goto L_0x00c1;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x014d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object m(java.util.List<com.fossil.Ms4> r12, java.util.List<com.fossil.Ms4> r13, int r14, com.mapped.Xe6<? super com.mapped.Cd6> r15) {
        /*
        // Method dump skipped, instructions count: 336
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.m(java.util.List, java.util.List, int, com.mapped.Xe6):java.lang.Object");
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v11, resolved type: androidx.lifecycle.MutableLiveData<com.fossil.Ps4> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0090, code lost:
        if (r1 == r0) goto L_0x0069;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object n(java.lang.String r7, com.mapped.Xe6<? super com.fossil.Ps4> r8) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.Di
            if (r0 == 0) goto L_0x006a
            r0 = r8
            com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$Di r0 = (com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.Di) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x006a
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0079
            if (r3 != r5) goto L_0x0071
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel r0 = (com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel) r0
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
            java.lang.Object r0 = r0.c()
            com.fossil.Ps4 r0 = (com.fossil.Ps4) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r1.getLocal()
            java.lang.String r3 = r6.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r1 = "fetchChallengeWithId - challenge: "
            r4.append(r1)
            r4.append(r0)
            java.lang.String r1 = " - _challengeLive: "
            r4.append(r1)
            androidx.lifecycle.MutableLiveData<com.fossil.Ps4> r1 = r6.b
            java.lang.Object r1 = r1.e()
            com.fossil.Ps4 r1 = (com.fossil.Ps4) r1
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            r2.e(r3, r1)
            if (r0 == 0) goto L_0x0069
            androidx.lifecycle.MutableLiveData<com.fossil.Ps4> r1 = r6.b
            r1.l(r0)
        L_0x0069:
            return r0
        L_0x006a:
            com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$Di r0 = new com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$Di
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0014
        L_0x0071:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0079:
            com.fossil.El7.b(r2)
            com.fossil.Dv7 r2 = com.fossil.Bw7.b()
            com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$Ei r3 = new com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel$Ei
            r4 = 0
            r3.<init>(r6, r7, r4)
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r5
            java.lang.Object r1 = com.fossil.Eu7.g(r2, r3, r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.n(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void o(String str) {
        this.k = Ss0.c(this.m.y(str), new Fi(this, str));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object p(java.lang.String r11, com.mapped.Xe6<? super com.mapped.Cd6> r12) {
        /*
        // Method dump skipped, instructions count: 247
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel.p(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object q(String str, Xe6<? super Cd6> xe6) {
        Object g2 = Eu7.g(Bw7.b(), new Ii(this, str, null), xe6);
        return g2 == Yn7.d() ? g2 : Cd6.a;
    }

    @DexIgnore
    public final LiveData<Ps4> r() {
        return Cz4.a(this.b);
    }

    @DexIgnore
    public final LiveData<Lc6<Integer, Ms4>> s() {
        return Cz4.a(this.e);
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, ServerError>> t() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<List<Object>> u() {
        return Cz4.a(this.c);
    }

    @DexIgnore
    public final LiveData<Object> v() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<Gl7<String, String, Integer>> w() {
        return this.j;
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, ServerError>> x() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, Boolean>> y() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<List<Gs4>> z() {
        return this.g;
    }
}
