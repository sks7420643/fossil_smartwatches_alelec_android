package com.portfolio.platform.buddy_challenge.screens.notification;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.Cz4;
import com.fossil.Dt4;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Ss0;
import com.fossil.Ts0;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.buddy_challenge.domain.NotificationRepository;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCNotificationViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, ServerError>> c; // = new MutableLiveData<>();
    @DexIgnore
    public boolean d; // = true;
    @DexIgnore
    public /* final */ LiveData<List<Dt4>> e;
    @DexIgnore
    public /* final */ NotificationRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel$fetchNotification$1", f = "BCNotificationViewModel.kt", l = {57}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCNotificationViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(BCNotificationViewModel bCNotificationViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCNotificationViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.a.l(Ao7.a(true));
                BCNotificationViewModel bCNotificationViewModel = this.this$0;
                this.L$0 = il6;
                this.label = 1;
                if (bCNotificationViewModel.h(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel", f = "BCNotificationViewModel.kt", l = {68}, m = "fetchNotificationServer")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCNotificationViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(BCNotificationViewModel bCNotificationViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCNotificationViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel$fetchNotificationServer$result$1", f = "BCNotificationViewModel.kt", l = {68}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<Dt4>>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCNotificationViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(BCNotificationViewModel bCNotificationViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCNotificationViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Kz4<List<Dt4>>> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                NotificationRepository notificationRepository = this.this$0.f;
                this.L$0 = il6;
                this.label = 1;
                Object b = notificationRepository.b(this);
                return b == d ? d : b;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ BCNotificationViewModel a;

        @DexIgnore
        public Di(BCNotificationViewModel bCNotificationViewModel) {
            this.a = bCNotificationViewModel;
        }

        @DexIgnore
        public final MutableLiveData<List<Dt4>> a(List<Dt4> list) {
            MutableLiveData<List<Dt4>> mutableLiveData = new MutableLiveData<>();
            mutableLiveData.o(list);
            if (!list.isEmpty()) {
                this.a.b.l(Boolean.FALSE);
                this.a.a.l(Boolean.FALSE);
            } else if (!this.a.d) {
                this.a.b.l(Boolean.TRUE);
            }
            if (this.a.d) {
                this.a.d = false;
                this.a.g();
            }
            return mutableLiveData;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((List) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel$refresh$1", f = "BCNotificationViewModel.kt", l = {63}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCNotificationViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(BCNotificationViewModel bCNotificationViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCNotificationViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                BCNotificationViewModel bCNotificationViewModel = this.this$0;
                this.L$0 = il6;
                this.label = 1;
                if (bCNotificationViewModel.h(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = BCNotificationViewModel.class.getSimpleName();
        Wg6.b(simpleName, "BCNotificationViewModel::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public BCNotificationViewModel(NotificationRepository notificationRepository) {
        Wg6.c(notificationRepository, "notificationRepository");
        this.f = notificationRepository;
        LiveData<List<Dt4>> c2 = Ss0.c(Cz4.a(this.f.c()), new Di(this));
        Wg6.b(c2, "Transformations.switchMa\u2026    }\n\n        live\n    }");
        this.e = c2;
    }

    @DexIgnore
    public final void g() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object h(com.mapped.Xe6<? super com.mapped.Cd6> r9) {
        /*
            r8 = this;
            r7 = 0
            r6 = 0
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 1
            boolean r0 = r9 instanceof com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel.Bi
            if (r0 == 0) goto L_0x006b
            r0 = r9
            com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel$Bi r0 = (com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel.Bi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x006b
            int r1 = r1 + r3
            r0.label = r1
        L_0x0015:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0079
            if (r3 != r5) goto L_0x0071
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel r0 = (com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel) r0
            com.fossil.El7.b(r1)
            r8 = r0
        L_0x0029:
            r0 = r1
            com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel.g
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "result: "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.e(r2, r3)
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r1 = r8.a
            java.lang.Boolean r2 = com.fossil.Ao7.a(r6)
            r1.l(r2)
            java.lang.Object r1 = r0.c()
            java.util.List r1 = (java.util.List) r1
            if (r1 == 0) goto L_0x0091
            boolean r0 = r1.isEmpty()
            if (r0 == 0) goto L_0x0068
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r0 = r8.b
            java.lang.Boolean r1 = com.fossil.Ao7.a(r5)
            r0.l(r1)
        L_0x0068:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x006a:
            return r0
        L_0x006b:
            com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel$Bi r0 = new com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel$Bi
            r0.<init>(r8, r9)
            goto L_0x0015
        L_0x0071:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0079:
            com.fossil.El7.b(r1)
            com.fossil.Dv7 r1 = com.fossil.Bw7.b()
            com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel$Ci r3 = new com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel$Ci
            r3.<init>(r8, r7)
            r0.L$0 = r8
            r0.label = r5
            java.lang.Object r1 = com.fossil.Eu7.g(r1, r3, r0)
            if (r1 != r2) goto L_0x0029
            r0 = r2
            goto L_0x006a
        L_0x0091:
            androidx.lifecycle.LiveData<java.util.List<com.fossil.Dt4>> r1 = r8.e
            java.lang.Object r1 = r1.e()
            if (r1 == 0) goto L_0x00a9
            androidx.lifecycle.LiveData<java.util.List<com.fossil.Dt4>> r1 = r8.e
            java.lang.Object r1 = r1.e()
            if (r1 == 0) goto L_0x00cd
            java.util.List r1 = (java.util.List) r1
            boolean r1 = r1.isEmpty()
            if (r1 == 0) goto L_0x00bb
        L_0x00a9:
            androidx.lifecycle.MutableLiveData<com.mapped.Lc6<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r1 = r8.c
            java.lang.Boolean r2 = com.fossil.Ao7.a(r5)
            com.portfolio.platform.data.model.ServerError r0 = r0.a()
            com.mapped.Lc6 r0 = com.fossil.Hl7.a(r2, r0)
            r1.l(r0)
            goto L_0x0068
        L_0x00bb:
            androidx.lifecycle.MutableLiveData<com.mapped.Lc6<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r1 = r8.c
            java.lang.Boolean r2 = com.fossil.Ao7.a(r6)
            com.portfolio.platform.data.model.ServerError r0 = r0.a()
            com.mapped.Lc6 r0 = com.fossil.Hl7.a(r2, r0)
            r1.l(r0)
            goto L_0x0068
        L_0x00cd:
            com.mapped.Wg6.i()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel.h(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final LiveData<Boolean> i() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, ServerError>> j() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Boolean> k() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<List<Dt4>> l() {
        return this.e;
    }

    @DexIgnore
    public final void m() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ei(this, null), 3, null);
    }
}
