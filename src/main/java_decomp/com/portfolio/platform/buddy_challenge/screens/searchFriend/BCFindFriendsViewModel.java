package com.portfolio.platform.buddy_challenge.screens.searchFriend;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.Ao7;
import com.fossil.Bt4;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Hl7;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Ms4;
import com.fossil.Ts0;
import com.fossil.Tt4;
import com.fossil.Us0;
import com.fossil.Xs4;
import com.fossil.Yn7;
import com.fossil.Yy4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCFindFriendsViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Yy4> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Kz4<List<Xs4>>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Kz4<Boolean>, Integer>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<String, String>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Stack<String> f; // = new Stack<>();
    @DexIgnore
    public /* final */ FriendRepository g;
    @DexIgnore
    public /* final */ Tt4 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$addFriendRequestedToLocal$1", f = "BCFindFriendsViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xs4 $friend;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCFindFriendsViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(BCFindFriendsViewModel bCFindFriendsViewModel, Xs4 xs4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCFindFriendsViewModel;
            this.$friend = xs4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, this.$friend, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                if (this.this$0.g.j(this.$friend.d()) == null) {
                    this.$friend.k(1);
                } else {
                    this.$friend.k(0);
                }
                this.this$0.g.n(this.$friend);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$loadSuggestedFriendFromHistoryChallenge$1", f = "BCFindFriendsViewModel.kt", l = {85}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $historyId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCFindFriendsViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$loadSuggestedFriendFromHistoryChallenge$1$1", f = "BCFindFriendsViewModel.kt", l = {91}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$loadSuggestedFriendFromHistoryChallenge$1$1$1", f = "BCFindFriendsViewModel.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $friends;
                @DexIgnore
                public /* final */ /* synthetic */ List $players;
                @DexIgnore
                public /* final */ /* synthetic */ List $suggestedFriends;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, List list, List list2, List list3, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                    this.$players = list;
                    this.$friends = list2;
                    this.$suggestedFriends = list3;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$players, this.$friends, this.$suggestedFriends, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object obj2;
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        for (Ms4 ms4 : this.$players) {
                            if ((!Wg6.a(ms4.d(), PortfolioApp.get.instance().l0())) && (!Wg6.a(ms4.p(), Ao7.a(true))) && (!Wg6.a(ms4.k(), "left_after_start"))) {
                                Iterator it = this.$friends.iterator();
                                while (true) {
                                    if (!it.hasNext()) {
                                        obj2 = null;
                                        break;
                                    }
                                    Object next = it.next();
                                    if (Ao7.a(Wg6.a(ms4.d(), ((Xs4) next).d())).booleanValue()) {
                                        obj2 = next;
                                        break;
                                    }
                                }
                                if (((Xs4) obj2) == null) {
                                    this.$suggestedFriends.add(new Xs4(ms4.d(), ms4.i(), ms4.c(), ms4.e(), null, ms4.g(), true, 0, 1));
                                }
                            }
                        }
                        this.this$0.this$0.this$0.c.l(new Kz4(this.$suggestedFriends, null, 2, null));
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                List<Ms4> x;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Bt4 z = this.this$0.this$0.h.z(this.this$0.$historyId);
                    List<Xs4> k = this.this$0.this$0.g.k();
                    ArrayList arrayList = new ArrayList();
                    if (z == null || (x = z.k()) == null) {
                        x = this.this$0.this$0.h.x();
                    }
                    Dv7 a2 = Bw7.a();
                    Aiii aiii = new Aiii(this, x, k, arrayList, null);
                    this.L$0 = il6;
                    this.L$1 = z;
                    this.L$2 = k;
                    this.L$3 = arrayList;
                    this.L$4 = x;
                    this.label = 1;
                    if (Eu7.g(a2, aiii, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    List list = (List) this.L$4;
                    List list2 = (List) this.L$3;
                    List list3 = (List) this.L$2;
                    Bt4 bt4 = (Bt4) this.L$1;
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(BCFindFriendsViewModel bCFindFriendsViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCFindFriendsViewModel;
            this.$historyId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$historyId, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.a.l(Ao7.a(true));
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.a.l(Ao7.a(false));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$searchPeople$1", f = "BCFindFriendsViewModel.kt", l = {58}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $keyword;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCFindFriendsViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$searchPeople$1$result$1", f = "BCFindFriendsViewModel.kt", l = {60}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<Xs4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Kz4<List<Xs4>>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    FriendRepository friendRepository = this.this$0.this$0.g;
                    String str = this.this$0.$keyword;
                    this.L$0 = il6;
                    this.label = 1;
                    Object q = friendRepository.q(str, this);
                    return q == d ? d : q;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(BCFindFriendsViewModel bCFindFriendsViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCFindFriendsViewModel;
            this.$keyword = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$keyword, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = BCFindFriendsViewModel.i;
                local.e(str, "searchPeople " + this.$keyword);
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Kz4 kz4 = (Kz4) g;
            if (this.this$0.f.contains(this.$keyword)) {
                this.this$0.c.l(kz4);
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$sendFriendRequest$1", f = "BCFindFriendsViewModel.kt", l = {121}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Xs4 $friend;
        @DexIgnore
        public /* final */ /* synthetic */ int $position;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCFindFriendsViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsViewModel$sendFriendRequest$1$result$1", f = "BCFindFriendsViewModel.kt", l = {122}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<Boolean>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Kz4<Boolean>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    FriendRepository friendRepository = this.this$0.this$0.g;
                    Xs4 xs4 = this.this$0.$friend;
                    this.L$0 = il6;
                    this.label = 1;
                    Object r = friendRepository.r(xs4, this);
                    return r == d ? d : r;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(BCFindFriendsViewModel bCFindFriendsViewModel, Xs4 xs4, int i, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCFindFriendsViewModel;
            this.$friend = xs4;
            this.$position = i;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$friend, this.$position, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                FLogger.INSTANCE.getLocal().d(BCFindFriendsViewModel.i, "sendFriendRequest");
                this.this$0.a.l(Ao7.a(true));
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Kz4 kz4 = (Kz4) g;
            this.this$0.a.l(Ao7.a(false));
            if (kz4.c() != null && ((Boolean) kz4.c()).booleanValue()) {
                this.this$0.e.l(Hl7.a(PortfolioApp.get.instance().l0(), this.$friend.d()));
                this.this$0.j(this.$friend);
            }
            this.this$0.d.l(Hl7.a(kz4, Ao7.e(this.$position)));
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = BCFindFriendsViewModel.class.getSimpleName();
        Wg6.b(simpleName, "BCFindFriendsViewModel::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public BCFindFriendsViewModel(FriendRepository friendRepository, Tt4 tt4) {
        Wg6.c(friendRepository, "mSocialFriendRepository");
        Wg6.c(tt4, "challengeRepository");
        this.g = friendRepository;
        this.h = tt4;
    }

    @DexIgnore
    public final void j(Xs4 xs4) {
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Ai(this, xs4, null), 2, null);
    }

    @DexIgnore
    public final void k() {
        if (!this.f.isEmpty()) {
            this.f.pop();
        }
    }

    @DexIgnore
    public final LiveData<Lc6<String, String>> l() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Boolean> m() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<Kz4<List<Xs4>>> n() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Yy4> o() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<Lc6<Kz4<Boolean>, Integer>> p() {
        return this.d;
    }

    @DexIgnore
    public final void q(String str) {
        Wg6.c(str, "historyId");
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Bi(this, str, null), 3, null);
    }

    @DexIgnore
    public final void r() {
        k();
        this.b.l(Yy4.CANCEL);
    }

    @DexIgnore
    public final void s() {
        this.b.l(Yy4.SEARCH);
    }

    @DexIgnore
    public final void t() {
        this.b.l(Yy4.TYPING);
    }

    @DexIgnore
    public final void u(String str) {
        Wg6.c(str, "keyword");
        k();
        this.f.push(str);
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ci(this, str, null), 3, null);
    }

    @DexIgnore
    public final void v(Xs4 xs4, int i2) {
        Wg6.c(xs4, "friend");
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Di(this, xs4, i2, null), 3, null);
    }
}
