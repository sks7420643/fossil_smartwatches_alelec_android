package com.portfolio.platform.buddy_challenge.screens.searchFriend;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.Sw4;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCFindFriendsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str) {
            Wg6.c(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), BCFindFriendsActivity.class);
            intent.putExtra("challenge_history_id_extra", str);
            fragment.startActivityForResult(intent, 16);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((Sw4) getSupportFragmentManager().Y(2131362158)) == null) {
            k(Sw4.t.b(getIntent().getStringExtra("challenge_history_id_extra")), Sw4.t.a(), 2131362158);
        }
    }
}
