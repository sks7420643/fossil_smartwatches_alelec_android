package com.portfolio.platform.buddy_challenge.screens.overview_leaderboard;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.Fw4;
import com.fossil.Ps4;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCOverviewLeaderBoardActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, Ps4 ps4, String str) {
            Wg6.c(fragment, "fragment");
            Wg6.c(ps4, "challenge");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("BCOverviewLeaderBoardActivity", "startActivity - challenge: " + ps4 + " - historyId: " + str);
            Intent intent = new Intent(fragment.getContext(), BCOverviewLeaderBoardActivity.class);
            intent.putExtra("challenge_extra", ps4);
            intent.putExtra("challenge_history_id_extra", str);
            fragment.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        Fragment Y = getSupportFragmentManager().Y(2131362158);
        if (!(Y instanceof Fw4)) {
            Y = null;
        }
        if (((Fw4) Y) == null) {
            Ps4 ps4 = (Ps4) getIntent().getParcelableExtra("challenge_extra");
            String stringExtra = getIntent().getStringExtra("challenge_history_id_extra");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String r = r();
            local.e(r, "onCreate - challenge: " + ps4 + " - historyId: " + stringExtra);
            j(Fw4.t.b(ps4, stringExtra), Fw4.t.a());
        }
    }
}
