package com.portfolio.platform.buddy_challenge.screens.tab.challenge.current;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.Bt4;
import com.fossil.Bw7;
import com.fossil.Cz4;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gl7;
import com.fossil.Gu7;
import com.fossil.Hl7;
import com.fossil.Hm7;
import com.fossil.Ko7;
import com.fossil.Ks4;
import com.fossil.Kz4;
import com.fossil.Ps4;
import com.fossil.Py4;
import com.fossil.Ss0;
import com.fossil.Ts0;
import com.fossil.Tt4;
import com.fossil.Us0;
import com.fossil.Uv7;
import com.fossil.Xy4;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Timer;
import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCCurrentChallengeSubTabViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai n; // = new Ai(null);
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Gl7<Ks4, String, Integer>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, Ps4>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, ServerError>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> f; // = new MutableLiveData<>();
    @DexIgnore
    public Timer g;
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public LiveData<Ps4> i;
    @DexIgnore
    public Timer j;
    @DexIgnore
    public /* final */ Tt4 k;
    @DexIgnore
    public /* final */ An4 l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return BCCurrentChallengeSubTabViewModel.m;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ BCCurrentChallengeSubTabViewModel a;

        @DexIgnore
        public Bi(BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel) {
            this.a = bCCurrentChallengeSubTabViewModel;
        }

        @DexIgnore
        public final MutableLiveData<Ps4> a(Ps4 ps4) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = BCCurrentChallengeSubTabViewModel.n.a();
            local.e(a2, "switchMap - challenge: " + ps4);
            MutableLiveData<Ps4> mutableLiveData = new MutableLiveData<>();
            mutableLiveData.l(ps4);
            if (ps4 == null) {
                Timer timer = this.a.g;
                if (timer != null) {
                    timer.cancel();
                }
                if (!this.a.h) {
                    this.a.b.l(Boolean.TRUE);
                    Timer timer2 = this.a.g;
                    if (timer2 != null) {
                        timer2.cancel();
                    }
                    Timer timer3 = this.a.j;
                    if (timer3 != null) {
                        timer3.cancel();
                    }
                    this.a.l.e(0L);
                    this.a.f.l(Boolean.TRUE);
                }
            } else {
                this.a.F(Py4.a(ps4));
                this.a.I();
                this.a.A(ps4);
                this.a.E(ps4);
            }
            if (this.a.h) {
                this.a.h = false;
                this.a.G();
            }
            return mutableLiveData;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Ps4) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel", f = "BCCurrentChallengeSubTabViewModel.kt", l = {277, 297}, m = "challengesFromServer")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCCurrentChallengeSubTabViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCCurrentChallengeSubTabViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.q(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$challengesFromServer$localChallenge$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Ps4>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCCurrentChallengeSubTabViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCCurrentChallengeSubTabViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Ps4> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return this.this$0.k.g(new String[]{"running", "waiting"}, Xy4.a.a());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$completedChallenge$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {196}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ps4 $currentChallenge;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCCurrentChallengeSubTabViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$completedChallenge$1$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {197, 200}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Bt4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Bt4>>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Il6 il6;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il62 = this.p$;
                    this.L$0 = il62;
                    this.label = 1;
                    if (Uv7.a(1000, this) == d) {
                        return d;
                    }
                    il6 = il62;
                } else if (i == 1) {
                    El7.b(obj);
                    il6 = (Il6) this.L$0;
                } else if (i == 2) {
                    Il6 il63 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.this$0.k.i(this.this$0.$currentChallenge.f());
                Tt4 tt4 = this.this$0.this$0.k;
                this.L$0 = il6;
                this.label = 2;
                Object s = Tt4.s(tt4, 5, 0, this, 2, null);
                return s == d ? d : s;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel, Ps4 ps4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCCurrentChallengeSubTabViewModel;
            this.$currentChallenge = ps4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$currentChallenge, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$initDisplayPlayer$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ int $target;
        @DexIgnore
        public /* final */ /* synthetic */ String $type;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCCurrentChallengeSubTabViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel, String str, String str2, int i, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCCurrentChallengeSubTabViewModel;
            this.$challengeId = str;
            this.$type = str2;
            this.$target = i;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, this.$challengeId, this.$type, this.$target, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                Ks4 j = this.this$0.k.j(this.$challengeId);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = BCCurrentChallengeSubTabViewModel.n.a();
                local.e(a2, "initDisplayPlayer - bcDisplayPlayer: " + j);
                if (j != null) {
                    this.this$0.c.l(new Gl7(j, this.$type, Ao7.e(this.$target)));
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$refresh$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {177, 181}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCCurrentChallengeSubTabViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCCurrentChallengeSubTabViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.this$0, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0061  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                r5 = 2
                r4 = 1
                java.lang.Object r1 = com.fossil.Yn7.d()
                int r0 = r6.label
                if (r0 == 0) goto L_0x0073
                if (r0 == r4) goto L_0x0038
                if (r0 != r5) goto L_0x0030
                java.lang.Object r0 = r6.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r7)
                r0 = r7
            L_0x0016:
                com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
                java.lang.Object r0 = r0.c()
                com.fossil.Pt4 r0 = (com.fossil.Pt4) r0
                if (r0 == 0) goto L_0x002d
                com.portfolio.platform.PortfolioApp$inner r1 = com.portfolio.platform.PortfolioApp.get
                com.portfolio.platform.PortfolioApp r1 = r1.instance()
                java.lang.String r0 = r0.a()
                r1.m1(r0)
            L_0x002d:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x002f:
                return r0
            L_0x0030:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0038:
                java.lang.Object r0 = r6.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r7)
            L_0x003f:
                com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel r2 = r6.this$0
                androidx.lifecycle.MutableLiveData r2 = com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel.h(r2)
                r3 = 0
                java.lang.Boolean r3 = com.fossil.Ao7.a(r3)
                r2.l(r3)
                com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel r2 = r6.this$0
                com.mapped.An4 r2 = com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel.b(r2)
                java.lang.Boolean r2 = r2.l0()
                java.lang.Boolean r3 = com.fossil.Ao7.a(r4)
                boolean r2 = com.mapped.Wg6.a(r2, r3)
                if (r2 == 0) goto L_0x002d
                com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel r2 = r6.this$0
                com.fossil.Tt4 r2 = com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel.a(r2)
                r6.L$0 = r0
                r6.label = r5
                java.lang.Object r0 = r2.v(r6)
                if (r0 != r1) goto L_0x0016
                r0 = r1
                goto L_0x002f
            L_0x0073:
                com.fossil.El7.b(r7)
                com.mapped.Il6 r0 = r6.p$
                com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel r2 = r6.this$0
                r6.L$0 = r0
                r6.label = r4
                java.lang.Object r2 = r2.q(r6)
                if (r2 != r1) goto L_0x003f
                r0 = r1
                goto L_0x002f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel.Gi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi extends TimerTask {
        @DexIgnore
        public /* final */ /* synthetic */ BCCurrentChallengeSubTabViewModel b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Hi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Xe6 xe6, Hi hi) {
                super(2, xe6);
                this.this$0 = hi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object q;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Tt4 tt4 = this.this$0.b.k;
                    List i2 = Hm7.i(this.this$0.c);
                    this.L$0 = il6;
                    this.label = 1;
                    q = Tt4.q(tt4, i2, 3, 3, false, this, 8, null);
                    if (q == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    q = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List<Ks4> list = (List) ((Kz4) q).c();
                if (list != null && (!list.isEmpty())) {
                    for (Ks4 ks4 : list) {
                        if (Ao7.a(Wg6.a(ks4.a(), this.this$0.c)).booleanValue()) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String a2 = BCCurrentChallengeSubTabViewModel.n.a();
                            local.e(a2, " setupFetchDisplayPlayersTimer - displayPlayer: " + ks4);
                            MutableLiveData mutableLiveData = this.this$0.b.c;
                            Hi hi = this.this$0;
                            mutableLiveData.l(new Gl7(ks4, hi.d, Ao7.e(hi.e)));
                        }
                    }
                    throw new NoSuchElementException("Collection contains no element matching the predicate.");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Hi(BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel, String str, String str2, int i) {
            this.b = bCCurrentChallengeSubTabViewModel;
            this.c = str;
            this.d = str2;
            this.e = i;
        }

        @DexIgnore
        public void run() {
            Rm6 unused = Gu7.d(Us0.a(this.b), Bw7.b(), null, new Aii(null, this), 2, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii extends TimerTask {
        @DexIgnore
        public /* final */ /* synthetic */ BCCurrentChallengeSubTabViewModel b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;
        @DexIgnore
        public /* final */ /* synthetic */ Ps4 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ii this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        FLogger.INSTANCE.getLocal().e(BCCurrentChallengeSubTabViewModel.n.a(), "starAndStopChallenge - startChallenge");
                        Ps4 a2 = this.this$0.this$0.b.k.a(this.this$0.this$0.d.f());
                        if (a2 != null) {
                            a2.v("running");
                            this.this$0.this$0.b.k.B(a2);
                        }
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Biii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Bt4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Biii biii = new Biii(this.this$0, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
                    //return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Bt4>>> xe6) {
                    throw null;
                    //return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Il6 il6;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il62 = this.p$;
                        FLogger.INSTANCE.getLocal().e(BCCurrentChallengeSubTabViewModel.n.a(), "starAndStopChallenge - stopChallenge");
                        this.L$0 = il62;
                        this.label = 1;
                        if (Uv7.a(1000, this) == d) {
                            return d;
                        }
                        il6 = il62;
                    } else if (i == 1) {
                        El7.b(obj);
                        il6 = (Il6) this.L$0;
                    } else if (i == 2) {
                        Il6 il63 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    this.this$0.this$0.b.k.i(this.this$0.this$0.d.f());
                    Tt4 tt4 = this.this$0.this$0.b.k;
                    this.L$0 = il6;
                    this.label = 2;
                    Object s = Tt4.s(tt4, 5, 0, this, 2, null);
                    return s == d ? d : s;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Xe6 xe6, Ii ii) {
                super(2, xe6);
                this.this$0 = ii;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    if (this.this$0.c) {
                        Dv7 b = Bw7.b();
                        Aiii aiii = new Aiii(this, null);
                        this.L$0 = il6;
                        this.label = 1;
                        if (Eu7.g(b, aiii, this) == d) {
                            return d;
                        }
                    } else {
                        Dv7 b2 = Bw7.b();
                        Biii biii = new Biii(this, null);
                        this.L$0 = il6;
                        this.label = 2;
                        if (Eu7.g(b2, biii, this) == d) {
                            return d;
                        }
                    }
                } else if (i == 1 || i == 2) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Ii(BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel, boolean z, Ps4 ps4) {
            this.b = bCCurrentChallengeSubTabViewModel;
            this.c = z;
            this.d = ps4;
        }

        @DexIgnore
        public void run() {
            Rm6 unused = Gu7.d(Us0.a(this.b), null, null, new Aii(null, this), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$start$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {94}, m = "invokeSuspend")
    public static final class Ji extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCCurrentChallengeSubTabViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel$start$1$1", f = "BCCurrentChallengeSubTabViewModel.kt", l = {94}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ji this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ji ji, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ji;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel = this.this$0.this$0;
                    this.L$0 = il6;
                    this.label = 1;
                    if (bCCurrentChallengeSubTabViewModel.q(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(BCCurrentChallengeSubTabViewModel bCCurrentChallengeSubTabViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCCurrentChallengeSubTabViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ji ji = new Ji(this.this$0, xe6);
            ji.p$ = (Il6) obj;
            throw null;
            //return ji;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ji) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = BCCurrentChallengeSubTabViewModel.class.getSimpleName();
        Wg6.b(simpleName, "BCCurrentChallengeSubTab\u2026el::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public BCCurrentChallengeSubTabViewModel(Tt4 tt4, An4 an4) {
        Wg6.c(tt4, "challengeRepository");
        Wg6.c(an4, "shared");
        this.k = tt4;
        this.l = an4;
        String[] strArr = {"running", "waiting"};
        LiveData<Ps4> c2 = Ss0.c(Cz4.a(this.k.h(strArr, Xy4.a.a())), new Bi(this));
        Wg6.b(c2, "Transformations.switchMa\u2026    }\n\n        live\n    }");
        this.i = c2;
    }

    @DexIgnore
    public final void A(Ps4 ps4) {
        String f2 = ps4.f();
        String r = ps4.r();
        if (r == null) {
            r = "activity_reach_goal";
        }
        Integer q = ps4.q();
        int intValue = q != null ? q.intValue() : -1;
        String n2 = ps4.n();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "initDisplayPlayer - id: " + f2 + " - type: " + r + " - target: " + intValue + " -status: " + n2);
        if (Wg6.a("running", n2)) {
            Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Fi(this, f2, r, intValue, null), 2, null);
        }
    }

    @DexIgnore
    public final void B() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "pause - playerTimer: " + this.g);
        Timer timer = this.g;
        if (timer != null) {
            timer.cancel();
        }
        Timer timer2 = this.j;
        if (timer2 != null) {
            timer2.cancel();
        }
    }

    @DexIgnore
    public final void C() {
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Gi(this, null), 2, null);
    }

    @DexIgnore
    public final void D() {
        Ps4 e2 = this.i.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "resume - challenge: " + e2);
        if (e2 != null) {
            F(Py4.a(e2));
            A(e2);
            E(e2);
        }
    }

    @DexIgnore
    public final void E(Ps4 ps4) {
        String f2 = ps4.f();
        String r = ps4.r();
        String str = r != null ? r : "activity_reach_goal";
        Integer q = ps4.q();
        int intValue = q != null ? q.intValue() : -1;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.e(str2, "scheduleFetchDisplayPlayers - challenge: " + ps4);
        if (Wg6.a("running", ps4.n())) {
            Timer timer = this.g;
            if (timer != null) {
                timer.cancel();
            }
            Timer timer2 = new Timer();
            this.g = timer2;
            if (timer2 != null) {
                timer2.schedule(new Hi(this, f2, str, intValue), 0, 15000);
            }
        }
    }

    @DexIgnore
    public final void F(Ps4 ps4) {
        long b2;
        long j2 = 0;
        Date m2 = ps4.m();
        long time = m2 != null ? m2.getTime() : 0;
        Date e2 = ps4.e();
        long time2 = e2 != null ? e2.getTime() : 0;
        boolean z = false;
        if (!Wg6.a("waiting", ps4.n())) {
            b2 = time2 - Xy4.a.b();
        } else if (Xy4.a.b() < time2) {
            b2 = time - Xy4.a.b();
            z = true;
        } else {
            b2 = 0;
        }
        FLogger.INSTANCE.getLocal().e(m, "starAndStopChallenge - startTime: " + time + " - endTime: " + time2 + " - timeLeft: " + b2 + " - isStart: " + z);
        if (b2 >= 0) {
            j2 = b2;
        }
        Timer timer = this.j;
        if (timer != null) {
            timer.cancel();
        }
        Timer timer2 = new Timer();
        this.j = timer2;
        if (timer2 != null) {
            timer2.schedule(new Ii(this, z, ps4), j2);
        }
    }

    @DexIgnore
    public final void G() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ji(this, null), 3, null);
    }

    @DexIgnore
    public final void H() {
        String n2;
        Ps4 e2 = this.i.e();
        if (e2 != null && (n2 = e2.n()) != null) {
            if (Wg6.a("waiting", n2)) {
                this.d.l(Hl7.a(Boolean.FALSE, e2));
            } else {
                this.d.l(Hl7.a(Boolean.TRUE, e2));
            }
        }
    }

    @DexIgnore
    public final void I() {
        this.a.l(Boolean.FALSE);
        this.b.l(Boolean.FALSE);
    }

    @DexIgnore
    public final void J(boolean z) {
        this.f.l(Boolean.valueOf(z));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object q(com.mapped.Xe6<? super com.mapped.Cd6> r15) {
        /*
        // Method dump skipped, instructions count: 370
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.tab.challenge.current.BCCurrentChallengeSubTabViewModel.q(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void r() {
        if (this.l.d().longValue() <= Xy4.a.b() || !Wg6.a(this.l.l0(), Boolean.TRUE)) {
            this.f.l(Boolean.TRUE);
        } else {
            this.f.l(Boolean.FALSE);
        }
    }

    @DexIgnore
    public final void s() {
        Ps4 e2 = this.i.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.e(str, "completedChallenge - current: " + e2);
        if (e2 != null) {
            Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ei(this, e2, null), 3, null);
            Timer timer = this.g;
            if (timer != null) {
                timer.cancel();
            }
            this.l.e(0L);
            this.f.l(Boolean.TRUE);
            this.b.l(Boolean.TRUE);
        }
    }

    @DexIgnore
    public final LiveData<Ps4> t() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<Gl7<Ks4, String, Integer>> u() {
        return Cz4.a(this.c);
    }

    @DexIgnore
    public final LiveData<Boolean> v() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, ServerError>> w() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Boolean> x() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<Boolean> y() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, Ps4>> z() {
        return this.d;
    }
}
