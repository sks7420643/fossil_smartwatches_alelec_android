package com.portfolio.platform.buddy_challenge.screens.tab.challenge.create;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.Ao7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Iz4;
import com.fossil.Ko7;
import com.fossil.Ps4;
import com.fossil.Ts0;
import com.fossil.Tt4;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCCreateSubTabViewModel extends Ts0 {
    @DexIgnore
    public /* final */ MutableLiveData<Object> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Iz4> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ An4 c;
    @DexIgnore
    public /* final */ Tt4 d;
    @DexIgnore
    public /* final */ FriendRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel$onCreateClick$1", f = "BCCreateSubTabViewModel.kt", l = {33, 38}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCCreateSubTabViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel$onCreateClick$1$challenge$1", f = "BCCreateSubTabViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Ps4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ps4> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.d.g(new String[]{"running", "waiting"}, new Date());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel$onCreateClick$1$countFriend$1", f = "BCCreateSubTabViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Integer>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Integer> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return Ao7.e(this.this$0.this$0.e.e());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(BCCreateSubTabViewModel bCCreateSubTabViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCCreateSubTabViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0055  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00c4  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                r8 = 0
                r7 = 2
                r6 = 1
                java.lang.Object r3 = com.fossil.Yn7.d()
                int r0 = r9.label
                if (r0 == 0) goto L_0x007a
                if (r0 == r6) goto L_0x0047
                if (r0 != r7) goto L_0x003f
                java.lang.Object r0 = r9.L$1
                com.fossil.Ps4 r0 = (com.fossil.Ps4) r0
                java.lang.Object r0 = r9.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r10)
                r0 = r10
            L_0x001b:
                java.lang.Number r0 = (java.lang.Number) r0
                int r0 = r0.intValue()
                if (r0 >= r6) goto L_0x00a9
                com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel r0 = r9.this$0
                com.mapped.An4 r0 = com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel.c(r0)
                boolean r0 = r0.V()
                if (r0 == 0) goto L_0x0094
                com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel r0 = r9.this$0
                androidx.lifecycle.MutableLiveData r0 = com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel.d(r0)
                java.lang.Boolean r1 = com.fossil.Ao7.a(r6)
                r0.l(r1)
            L_0x003c:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x003e:
                return r0
            L_0x003f:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0047:
                java.lang.Object r0 = r9.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r10)
                r2 = r0
                r1 = r10
            L_0x0050:
                r0 = r1
                com.fossil.Ps4 r0 = (com.fossil.Ps4) r0
                if (r0 != 0) goto L_0x00c4
                com.portfolio.platform.PortfolioApp$inner r1 = com.portfolio.platform.PortfolioApp.get
                com.portfolio.platform.PortfolioApp r1 = r1.instance()
                boolean r1 = r1.L()
                if (r1 == 0) goto L_0x00b7
                com.fossil.Dv7 r4 = com.fossil.Bw7.b()
                com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel$Ai$Bii r5 = new com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel$Ai$Bii
                r5.<init>(r9, r8)
                r9.L$0 = r2
                r9.L$1 = r0
                r9.Z$0 = r1
                r9.label = r7
                java.lang.Object r0 = com.fossil.Eu7.g(r4, r5, r9)
                if (r0 != r3) goto L_0x001b
                r0 = r3
                goto L_0x003e
            L_0x007a:
                com.fossil.El7.b(r10)
                com.mapped.Il6 r0 = r9.p$
                com.fossil.Dv7 r1 = com.fossil.Bw7.b()
                com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel$Ai$Aii r2 = new com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel$Ai$Aii
                r2.<init>(r9, r8)
                r9.L$0 = r0
                r9.label = r6
                java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r9)
                if (r1 != r3) goto L_0x00d1
                r0 = r3
                goto L_0x003e
            L_0x0094:
                com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel r0 = r9.this$0
                com.mapped.An4 r0 = com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel.c(r0)
                r0.j1()
                com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel r0 = r9.this$0
                androidx.lifecycle.MutableLiveData r0 = com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel.e(r0)
                com.fossil.Iz4 r1 = com.fossil.Iz4.SUGGEST_FIND_FRIEND
                r0.l(r1)
                goto L_0x003c
            L_0x00a9:
                com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel r0 = r9.this$0
                androidx.lifecycle.MutableLiveData r0 = com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel.d(r0)
                java.lang.Boolean r1 = com.fossil.Ao7.a(r6)
                r0.l(r1)
                goto L_0x003c
            L_0x00b7:
                com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel r0 = r9.this$0
                androidx.lifecycle.MutableLiveData r0 = com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel.e(r0)
                com.fossil.Iz4 r1 = com.fossil.Iz4.NEED_ACTIVE_DEVICE
                r0.l(r1)
                goto L_0x003c
            L_0x00c4:
                com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel r0 = r9.this$0
                androidx.lifecycle.MutableLiveData r0 = com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel.e(r0)
                com.fossil.Iz4 r1 = com.fossil.Iz4.JOIN_TOO_MUCH
                r0.l(r1)
                goto L_0x003c
            L_0x00d1:
                r2 = r0
                goto L_0x0050
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public BCCreateSubTabViewModel(An4 an4, Tt4 tt4, FriendRepository friendRepository) {
        Wg6.c(an4, "shared");
        Wg6.c(tt4, "challengeRepository");
        Wg6.c(friendRepository, "friendRepository");
        this.c = an4;
        this.d = tt4;
        this.e = friendRepository;
    }

    @DexIgnore
    public final LiveData<Object> f() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<Iz4> g() {
        return this.b;
    }

    @DexIgnore
    public final void h() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ai(this, null), 3, null);
    }
}
