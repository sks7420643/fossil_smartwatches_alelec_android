package com.portfolio.platform.buddy_challenge.screens.leaderboard;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.Bt4;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Ms4;
import com.fossil.Ps4;
import com.fossil.Py4;
import com.fossil.Ts0;
import com.fossil.Tt4;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCLeaderBoardViewModel extends Ts0 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ MutableLiveData<List<Object>> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, ServerError>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public Timer f;
    @DexIgnore
    public /* final */ Tt4 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel", f = "BCLeaderBoardViewModel.kt", l = {94}, m = "initPlayers")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(BCLeaderBoardViewModel bCLeaderBoardViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCLeaderBoardViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.j(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$initPlayers$historyChallenge$1", f = "BCLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Bt4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(BCLeaderBoardViewModel bCLeaderBoardViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCLeaderBoardViewModel;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$challengeId, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Bt4> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return this.this$0.g.z(this.$challengeId);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$loadPlayers$1", f = "BCLeaderBoardViewModel.kt", l = {51, 56, 58}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ String $status;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(BCLeaderBoardViewModel bCLeaderBoardViewModel, String str, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCLeaderBoardViewModel;
            this.$challengeId = str;
            this.$status = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$challengeId, this.$status, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0046  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0059  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0072  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 3
                r5 = 2
                r4 = 1
                java.lang.Object r1 = com.fossil.Yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0074
                if (r0 == r4) goto L_0x0048
                if (r0 == r5) goto L_0x0023
                if (r0 != r6) goto L_0x001b
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
            L_0x0018:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x001a:
                return r0
            L_0x001b:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0023:
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
            L_0x002a:
                com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel r2 = r7.this$0
                androidx.lifecycle.MutableLiveData r2 = com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel.d(r2)
                r3 = 0
                java.lang.Boolean r3 = com.fossil.Ao7.a(r3)
                r2.l(r3)
                com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel r2 = r7.this$0
                java.lang.String r3 = r7.$challengeId
                r7.L$0 = r0
                r7.label = r6
                java.lang.Object r0 = r2.o(r3, r7)
                if (r0 != r1) goto L_0x0018
                r0 = r1
                goto L_0x001a
            L_0x0048:
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
            L_0x004f:
                java.lang.String r2 = r7.$status
                java.lang.String r3 = "completed"
                boolean r2 = com.mapped.Wg6.a(r2, r3)
                if (r2 == 0) goto L_0x0064
                com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel r2 = r7.this$0
                androidx.lifecycle.MutableLiveData r2 = com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel.c(r2)
                java.lang.String r3 = "completed"
                r2.l(r3)
            L_0x0064:
                com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel r2 = r7.this$0
                java.lang.String r3 = r7.$challengeId
                r7.L$0 = r0
                r7.label = r5
                java.lang.Object r2 = r2.l(r3, r7)
                if (r2 != r1) goto L_0x002a
                r0 = r1
                goto L_0x001a
            L_0x0074:
                com.fossil.El7.b(r8)
                com.mapped.Il6 r0 = r7.p$
                com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel r2 = r7.this$0
                androidx.lifecycle.MutableLiveData r2 = com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel.d(r2)
                java.lang.Boolean r3 = com.fossil.Ao7.a(r4)
                r2.l(r3)
                com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel r2 = r7.this$0
                java.lang.String r3 = r7.$challengeId
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r2 = r2.j(r3, r7)
                if (r2 != r1) goto L_0x004f
                r0 = r1
                goto L_0x001a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel", f = "BCLeaderBoardViewModel.kt", l = {103, 116, 122}, m = "loadPlayersFromServer")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(BCLeaderBoardViewModel bCLeaderBoardViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCLeaderBoardViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.l(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$loadPlayersFromServer$2", f = "BCLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $players;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(BCLeaderBoardViewModel bCLeaderBoardViewModel, List list, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCLeaderBoardViewModel;
            this.$players = list;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$players, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.g.C(this.$players);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$loadPlayersFromServer$result$1", f = "BCLeaderBoardViewModel.kt", l = {104}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Ms4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(BCLeaderBoardViewModel bCLeaderBoardViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCLeaderBoardViewModel;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, this.$challengeId, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Ms4>>> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Tt4 tt4 = this.this$0.g;
                String str = this.$challengeId;
                this.L$0 = il6;
                this.label = 1;
                Object t = tt4.t(str, new String[]{"running", "completed", "left_after_start"}, this);
                return t == d ? d : t;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$refresh$1", f = "BCLeaderBoardViewModel.kt", l = {65}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(BCLeaderBoardViewModel bCLeaderBoardViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCLeaderBoardViewModel;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.this$0, this.$challengeId, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                BCLeaderBoardViewModel bCLeaderBoardViewModel = this.this$0;
                String str = this.$challengeId;
                this.L$0 = il6;
                this.label = 1;
                if (bCLeaderBoardViewModel.l(str, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.c.l(Ao7.a(false));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$updateHistoryChallenge$2", f = "BCLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ List $players;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(BCLeaderBoardViewModel bCLeaderBoardViewModel, String str, List list, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCLeaderBoardViewModel;
            this.$challengeId = str;
            this.$players = list;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, this.$challengeId, this.$players, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                Bt4 z = this.this$0.g.z(this.$challengeId);
                if (z != null) {
                    List list = this.$players;
                    if (!(list == null || list.isEmpty())) {
                        this.this$0.g.K(Py4.b(z, this.$players));
                    }
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii extends TimerTask {
        @DexIgnore
        public /* final */ /* synthetic */ BCLeaderBoardViewModel b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ii this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Xe6 xe6, Ii ii) {
                super(2, xe6);
                this.this$0 = ii;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    FLogger.INSTANCE.getLocal().e(this.this$0.b.a, "waitingChallengeEnd - end challenge");
                    Object e = this.this$0.b.b.e();
                    if (!(e instanceof List)) {
                        e = null;
                    }
                    List<Ms4> list = (List) e;
                    if (list != null) {
                        this.this$0.b.g.C(list);
                    }
                    this.this$0.b.e.l("completed");
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public Ii(BCLeaderBoardViewModel bCLeaderBoardViewModel) {
            this.b = bCLeaderBoardViewModel;
        }

        @DexIgnore
        public void run() {
            Rm6 unused = Gu7.d(Us0.a(this.b), Bw7.b(), null, new Aii(null, this), 2, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel", f = "BCLeaderBoardViewModel.kt", l = {72}, m = "waitingChallengeEnd")
    public static final class Ji extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(BCLeaderBoardViewModel bCLeaderBoardViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCLeaderBoardViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$waitingChallengeEnd$challenge$1", f = "BCLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Ki extends Ko7 implements Coroutine<Il6, Xe6<? super Ps4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCLeaderBoardViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(BCLeaderBoardViewModel bCLeaderBoardViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCLeaderBoardViewModel;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ki ki = new Ki(this.this$0, this.$challengeId, xe6);
            ki.p$ = (Il6) obj;
            throw null;
            //return ki;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Ps4> xe6) {
            throw null;
            //return ((Ki) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return this.this$0.g.a(this.$challengeId);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public BCLeaderBoardViewModel(Tt4 tt4) {
        Wg6.c(tt4, "challengeRepository");
        this.g = tt4;
        String simpleName = BCLeaderBoardViewModel.class.getSimpleName();
        Wg6.b(simpleName, "BCLeaderBoardViewModel::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public final LiveData<String> f() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, ServerError>> g() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Boolean> h() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<List<Object>> i() {
        return this.b;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object j(java.lang.String r7, com.mapped.Xe6<? super com.mapped.Cd6> r8) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel.Ai
            if (r0 == 0) goto L_0x005a
            r0 = r8
            com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$Ai r0 = (com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x005a
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0069
            if (r3 != r5) goto L_0x0061
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel r0 = (com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel) r0
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.fossil.Bt4 r0 = (com.fossil.Bt4) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = r6.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "initPlayers - historyChallenge: "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.e(r2, r3)
            if (r0 == 0) goto L_0x0057
            androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Object>> r1 = r6.b
            java.util.List r0 = r0.k()
            r1.l(r0)
        L_0x0057:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0059:
            return r0
        L_0x005a:
            com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$Ai r0 = new com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$Ai
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0014
        L_0x0061:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0069:
            com.fossil.El7.b(r2)
            com.fossil.Dv7 r2 = com.fossil.Bw7.b()
            com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$Bi r3 = new com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$Bi
            r4 = 0
            r3.<init>(r6, r7, r4)
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r5
            java.lang.Object r1 = com.fossil.Eu7.g(r2, r3, r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel.j(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void k(String str, String str2) {
        if (str != null) {
            Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ci(this, str, str2, null), 3, null);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0141  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0165  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object l(java.lang.String r13, com.mapped.Xe6<? super com.mapped.Cd6> r14) {
        /*
        // Method dump skipped, instructions count: 404
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel.l(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void m(String str) {
        Wg6.c(str, "challengeId");
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Gi(this, str, null), 3, null);
    }

    @DexIgnore
    public final /* synthetic */ Object n(String str, List<Ms4> list, Xe6<? super Cd6> xe6) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local.e(str2, "updateHistoryChallenge - challengeId: " + str);
        Object g2 = Eu7.g(Bw7.b(), new Hi(this, str, list, null), xe6);
        return g2 == Yn7.d() ? g2 : Cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object o(java.lang.String r9, com.mapped.Xe6<? super com.mapped.Cd6> r10) {
        /*
            r8 = this;
            r7 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel.Ji
            if (r0 == 0) goto L_0x0061
            r0 = r10
            com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$Ji r0 = (com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel.Ji) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0061
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0016:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0070
            if (r3 != r7) goto L_0x0068
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel r0 = (com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel) r0
            com.fossil.El7.b(r2)
            r1 = r2
            r8 = r0
        L_0x002f:
            r0 = r1
            com.fossil.Ps4 r0 = (com.fossil.Ps4) r0
            if (r0 == 0) goto L_0x005e
            java.util.Date r0 = r0.e()
            if (r0 == 0) goto L_0x008a
            long r0 = r0.getTime()
            java.lang.Long r0 = com.fossil.Ao7.f(r0)
            if (r0 == 0) goto L_0x008a
            long r0 = r0.longValue()
        L_0x0048:
            int r2 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r2 >= 0) goto L_0x004d
            r0 = r4
        L_0x004d:
            java.util.Timer r2 = new java.util.Timer
            r2.<init>()
            r8.f = r2
            if (r2 == 0) goto L_0x005e
            com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$Ii r3 = new com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$Ii
            r3.<init>(r8)
            r2.schedule(r3, r0)
        L_0x005e:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0060:
            return r0
        L_0x0061:
            com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$Ji r0 = new com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$Ji
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0016
        L_0x0068:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0070:
            com.fossil.El7.b(r2)
            com.fossil.Dv7 r2 = com.fossil.Bw7.b()
            com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$Ki r3 = new com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$Ki
            r6 = 0
            r3.<init>(r8, r9, r6)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.label = r7
            java.lang.Object r1 = com.fossil.Eu7.g(r2, r3, r1)
            if (r1 != r0) goto L_0x002f
            goto L_0x0060
        L_0x008a:
            com.fossil.Xy4 r0 = com.fossil.Xy4.a
            long r0 = r0.b()
            long r0 = r4 - r0
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel.o(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }
}
