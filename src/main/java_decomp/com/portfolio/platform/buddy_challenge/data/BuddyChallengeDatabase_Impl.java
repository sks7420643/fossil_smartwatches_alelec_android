package com.portfolio.platform.buddy_challenge.data;

import com.facebook.share.internal.ShareConstants;
import com.fossil.Ex0;
import com.fossil.Ft4;
import com.fossil.Gt4;
import com.fossil.Hw0;
import com.fossil.Ix0;
import com.fossil.Jt4;
import com.fossil.Kt4;
import com.fossil.Lx0;
import com.fossil.Nw0;
import com.fossil.Qs4;
import com.fossil.Rs4;
import com.fossil.Ys4;
import com.fossil.Zs4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.mapped.Ji;
import com.mapped.Oh;
import com.mapped.Qh;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BuddyChallengeDatabase_Impl extends BuddyChallengeDatabase {
    @DexIgnore
    public volatile Ys4 a;
    @DexIgnore
    public volatile Jt4 b;
    @DexIgnore
    public volatile Qs4 c;
    @DexIgnore
    public volatile Ft4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends Qh.Ai {
        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void createAllTables(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `profile` (`id` TEXT NOT NULL, `socialId` TEXT NOT NULL, `firstName` TEXT, `lastName` TEXT, `points` INTEGER, `profilePicture` TEXT, `createdAt` TEXT, `updatedAt` TEXT, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `friend` (`id` TEXT NOT NULL, `socialId` TEXT NOT NULL, `firstName` TEXT, `lastName` TEXT, `points` INTEGER, `profilePicture` TEXT, `confirmation` INTEGER NOT NULL, `pin` INTEGER NOT NULL, `friendType` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `challenge` (`id` TEXT NOT NULL, `type` TEXT, `name` TEXT, `des` TEXT, `owner` TEXT, `numberOfPlayers` INTEGER, `startTime` TEXT, `endTime` TEXT, `target` INTEGER, `duration` INTEGER, `privacy` TEXT, `version` TEXT, `status` TEXT, `syncData` TEXT, `createdAt` TEXT, `updatedAt` TEXT, `category` TEXT, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `notification` (`id` TEXT NOT NULL, `titleKey` TEXT NOT NULL, `bodyKey` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `challengeData` TEXT, `profileData` TEXT, `confirmChallenge` INTEGER NOT NULL, `rank` INTEGER NOT NULL, `reachGoalAt` TEXT, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `fitness` (`pinType` INTEGER NOT NULL, `id` INTEGER NOT NULL, `encryptMethod` INTEGER NOT NULL, `encryptedData` TEXT NOT NULL, `keyType` INTEGER NOT NULL, `sequence` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `display_player` (`challengeId` TEXT NOT NULL, `numberOfPlayers` INTEGER, `topPlayers` TEXT, `nearPlayers` TEXT, PRIMARY KEY(`challengeId`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `challenge_player` (`challengeId` TEXT NOT NULL, `challengeName` TEXT NOT NULL, `players` TEXT NOT NULL, PRIMARY KEY(`challengeId`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `history_challenge` (`id` TEXT NOT NULL, `type` TEXT, `name` TEXT, `des` TEXT, `owner` TEXT, `numberOfPlayers` INTEGER, `startTime` TEXT, `endTime` TEXT, `target` INTEGER, `duration` INTEGER, `privacy` TEXT, `version` TEXT, `status` TEXT, `players` TEXT NOT NULL, `topPlayer` TEXT, `currentPlayer` TEXT, `createdAt` TEXT, `updatedAt` TEXT, `completedAt` TEXT, `isFirst` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `players` (`id` TEXT NOT NULL, `socialId` TEXT NOT NULL, `firstName` TEXT, `lastName` TEXT, `isDeleted` INTEGER, `ranking` INTEGER, `status` TEXT, `stepsBase` INTEGER, `stepsOffset` INTEGER, `caloriesBase` INTEGER, `caloriesOffset` INTEGER, `profilePicture` TEXT, `lastSync` TEXT, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'b238af0aae08cbacb61a8a87455c3eee')");
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void dropAllTables(Lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `profile`");
            lx0.execSQL("DROP TABLE IF EXISTS `friend`");
            lx0.execSQL("DROP TABLE IF EXISTS `challenge`");
            lx0.execSQL("DROP TABLE IF EXISTS `notification`");
            lx0.execSQL("DROP TABLE IF EXISTS `fitness`");
            lx0.execSQL("DROP TABLE IF EXISTS `display_player`");
            lx0.execSQL("DROP TABLE IF EXISTS `challenge_player`");
            lx0.execSQL("DROP TABLE IF EXISTS `history_challenge`");
            lx0.execSQL("DROP TABLE IF EXISTS `players`");
            if (BuddyChallengeDatabase_Impl.this.mCallbacks != null) {
                int size = BuddyChallengeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) BuddyChallengeDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onCreate(Lx0 lx0) {
            if (BuddyChallengeDatabase_Impl.this.mCallbacks != null) {
                int size = BuddyChallengeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) BuddyChallengeDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onOpen(Lx0 lx0) {
            BuddyChallengeDatabase_Impl.this.mDatabase = lx0;
            BuddyChallengeDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (BuddyChallengeDatabase_Impl.this.mCallbacks != null) {
                int size = BuddyChallengeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) BuddyChallengeDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPostMigrate(Lx0 lx0) {
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPreMigrate(Lx0 lx0) {
            Ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public Qh.Bi onValidateSchema(Lx0 lx0) {
            HashMap hashMap = new HashMap(8);
            hashMap.put("id", new Ix0.Ai("id", "TEXT", true, 1, null, 1));
            hashMap.put("socialId", new Ix0.Ai("socialId", "TEXT", true, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_FIRST_NAME, new Ix0.Ai(Constants.PROFILE_KEY_FIRST_NAME, "TEXT", false, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_LAST_NAME, new Ix0.Ai(Constants.PROFILE_KEY_LAST_NAME, "TEXT", false, 0, null, 1));
            hashMap.put("points", new Ix0.Ai("points", "INTEGER", false, 0, null, 1));
            hashMap.put(Constants.PROFILE_KEY_PROFILE_PIC, new Ix0.Ai(Constants.PROFILE_KEY_PROFILE_PIC, "TEXT", false, 0, null, 1));
            hashMap.put("createdAt", new Ix0.Ai("createdAt", "TEXT", false, 0, null, 1));
            hashMap.put("updatedAt", new Ix0.Ai("updatedAt", "TEXT", false, 0, null, 1));
            Ix0 ix0 = new Ix0("profile", hashMap, new HashSet(0), new HashSet(0));
            Ix0 a2 = Ix0.a(lx0, "profile");
            if (!ix0.equals(a2)) {
                return new Qh.Bi(false, "profile(com.portfolio.platform.buddy_challenge.data.Profile).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(9);
            hashMap2.put("id", new Ix0.Ai("id", "TEXT", true, 1, null, 1));
            hashMap2.put("socialId", new Ix0.Ai("socialId", "TEXT", true, 0, null, 1));
            hashMap2.put(Constants.PROFILE_KEY_FIRST_NAME, new Ix0.Ai(Constants.PROFILE_KEY_FIRST_NAME, "TEXT", false, 0, null, 1));
            hashMap2.put(Constants.PROFILE_KEY_LAST_NAME, new Ix0.Ai(Constants.PROFILE_KEY_LAST_NAME, "TEXT", false, 0, null, 1));
            hashMap2.put("points", new Ix0.Ai("points", "INTEGER", false, 0, null, 1));
            hashMap2.put(Constants.PROFILE_KEY_PROFILE_PIC, new Ix0.Ai(Constants.PROFILE_KEY_PROFILE_PIC, "TEXT", false, 0, null, 1));
            hashMap2.put("confirmation", new Ix0.Ai("confirmation", "INTEGER", true, 0, null, 1));
            hashMap2.put("pin", new Ix0.Ai("pin", "INTEGER", true, 0, null, 1));
            hashMap2.put("friendType", new Ix0.Ai("friendType", "INTEGER", true, 0, null, 1));
            Ix0 ix02 = new Ix0("friend", hashMap2, new HashSet(0), new HashSet(0));
            Ix0 a3 = Ix0.a(lx0, "friend");
            if (!ix02.equals(a3)) {
                return new Qh.Bi(false, "friend(com.portfolio.platform.buddy_challenge.data.Friend).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
            }
            HashMap hashMap3 = new HashMap(17);
            hashMap3.put("id", new Ix0.Ai("id", "TEXT", true, 1, null, 1));
            hashMap3.put("type", new Ix0.Ai("type", "TEXT", false, 0, null, 1));
            hashMap3.put("name", new Ix0.Ai("name", "TEXT", false, 0, null, 1));
            hashMap3.put("des", new Ix0.Ai("des", "TEXT", false, 0, null, 1));
            hashMap3.put("owner", new Ix0.Ai("owner", "TEXT", false, 0, null, 1));
            hashMap3.put("numberOfPlayers", new Ix0.Ai("numberOfPlayers", "INTEGER", false, 0, null, 1));
            hashMap3.put(SampleRaw.COLUMN_START_TIME, new Ix0.Ai(SampleRaw.COLUMN_START_TIME, "TEXT", false, 0, null, 1));
            hashMap3.put(SampleRaw.COLUMN_END_TIME, new Ix0.Ai(SampleRaw.COLUMN_END_TIME, "TEXT", false, 0, null, 1));
            hashMap3.put("target", new Ix0.Ai("target", "INTEGER", false, 0, null, 1));
            hashMap3.put("duration", new Ix0.Ai("duration", "INTEGER", false, 0, null, 1));
            hashMap3.put(ShareConstants.WEB_DIALOG_PARAM_PRIVACY, new Ix0.Ai(ShareConstants.WEB_DIALOG_PARAM_PRIVACY, "TEXT", false, 0, null, 1));
            hashMap3.put("version", new Ix0.Ai("version", "TEXT", false, 0, null, 1));
            hashMap3.put("status", new Ix0.Ai("status", "TEXT", false, 0, null, 1));
            hashMap3.put("syncData", new Ix0.Ai("syncData", "TEXT", false, 0, null, 1));
            hashMap3.put("createdAt", new Ix0.Ai("createdAt", "TEXT", false, 0, null, 1));
            hashMap3.put("updatedAt", new Ix0.Ai("updatedAt", "TEXT", false, 0, null, 1));
            hashMap3.put("category", new Ix0.Ai("category", "TEXT", false, 0, null, 1));
            Ix0 ix03 = new Ix0("challenge", hashMap3, new HashSet(0), new HashSet(0));
            Ix0 a4 = Ix0.a(lx0, "challenge");
            if (!ix03.equals(a4)) {
                return new Qh.Bi(false, "challenge(com.portfolio.platform.buddy_challenge.data.Challenge).\n Expected:\n" + ix03 + "\n Found:\n" + a4);
            }
            HashMap hashMap4 = new HashMap(9);
            hashMap4.put("id", new Ix0.Ai("id", "TEXT", true, 1, null, 1));
            hashMap4.put("titleKey", new Ix0.Ai("titleKey", "TEXT", true, 0, null, 1));
            hashMap4.put("bodyKey", new Ix0.Ai("bodyKey", "TEXT", true, 0, null, 1));
            hashMap4.put("createdAt", new Ix0.Ai("createdAt", "TEXT", true, 0, null, 1));
            hashMap4.put("challengeData", new Ix0.Ai("challengeData", "TEXT", false, 0, null, 1));
            hashMap4.put("profileData", new Ix0.Ai("profileData", "TEXT", false, 0, null, 1));
            hashMap4.put("confirmChallenge", new Ix0.Ai("confirmChallenge", "INTEGER", true, 0, null, 1));
            hashMap4.put("rank", new Ix0.Ai("rank", "INTEGER", true, 0, null, 1));
            hashMap4.put("reachGoalAt", new Ix0.Ai("reachGoalAt", "TEXT", false, 0, null, 1));
            Ix0 ix04 = new Ix0("notification", hashMap4, new HashSet(0), new HashSet(0));
            Ix0 a5 = Ix0.a(lx0, "notification");
            if (!ix04.equals(a5)) {
                return new Qh.Bi(false, "notification(com.portfolio.platform.buddy_challenge.data.Notification).\n Expected:\n" + ix04 + "\n Found:\n" + a5);
            }
            HashMap hashMap5 = new HashMap(6);
            hashMap5.put("pinType", new Ix0.Ai("pinType", "INTEGER", true, 0, null, 1));
            hashMap5.put("id", new Ix0.Ai("id", "INTEGER", true, 1, null, 1));
            hashMap5.put("encryptMethod", new Ix0.Ai("encryptMethod", "INTEGER", true, 0, null, 1));
            hashMap5.put("encryptedData", new Ix0.Ai("encryptedData", "TEXT", true, 0, null, 1));
            hashMap5.put("keyType", new Ix0.Ai("keyType", "INTEGER", true, 0, null, 1));
            hashMap5.put("sequence", new Ix0.Ai("sequence", "TEXT", true, 0, null, 1));
            Ix0 ix05 = new Ix0("fitness", hashMap5, new HashSet(0), new HashSet(0));
            Ix0 a6 = Ix0.a(lx0, "fitness");
            if (!ix05.equals(a6)) {
                return new Qh.Bi(false, "fitness(com.portfolio.platform.buddy_challenge.data.BCFitnessData).\n Expected:\n" + ix05 + "\n Found:\n" + a6);
            }
            HashMap hashMap6 = new HashMap(4);
            hashMap6.put("challengeId", new Ix0.Ai("challengeId", "TEXT", true, 1, null, 1));
            hashMap6.put("numberOfPlayers", new Ix0.Ai("numberOfPlayers", "INTEGER", false, 0, null, 1));
            hashMap6.put("topPlayers", new Ix0.Ai("topPlayers", "TEXT", false, 0, null, 1));
            hashMap6.put("nearPlayers", new Ix0.Ai("nearPlayers", "TEXT", false, 0, null, 1));
            Ix0 ix06 = new Ix0("display_player", hashMap6, new HashSet(0), new HashSet(0));
            Ix0 a7 = Ix0.a(lx0, "display_player");
            if (!ix06.equals(a7)) {
                return new Qh.Bi(false, "display_player(com.portfolio.platform.buddy_challenge.data.BCDisplayPlayer).\n Expected:\n" + ix06 + "\n Found:\n" + a7);
            }
            HashMap hashMap7 = new HashMap(3);
            hashMap7.put("challengeId", new Ix0.Ai("challengeId", "TEXT", true, 1, null, 1));
            hashMap7.put("challengeName", new Ix0.Ai("challengeName", "TEXT", true, 0, null, 1));
            hashMap7.put("players", new Ix0.Ai("players", "TEXT", true, 0, null, 1));
            Ix0 ix07 = new Ix0("challenge_player", hashMap7, new HashSet(0), new HashSet(0));
            Ix0 a8 = Ix0.a(lx0, "challenge_player");
            if (!ix07.equals(a8)) {
                return new Qh.Bi(false, "challenge_player(com.portfolio.platform.buddy_challenge.data.BCChallengePlayer).\n Expected:\n" + ix07 + "\n Found:\n" + a8);
            }
            HashMap hashMap8 = new HashMap(20);
            hashMap8.put("id", new Ix0.Ai("id", "TEXT", true, 1, null, 1));
            hashMap8.put("type", new Ix0.Ai("type", "TEXT", false, 0, null, 1));
            hashMap8.put("name", new Ix0.Ai("name", "TEXT", false, 0, null, 1));
            hashMap8.put("des", new Ix0.Ai("des", "TEXT", false, 0, null, 1));
            hashMap8.put("owner", new Ix0.Ai("owner", "TEXT", false, 0, null, 1));
            hashMap8.put("numberOfPlayers", new Ix0.Ai("numberOfPlayers", "INTEGER", false, 0, null, 1));
            hashMap8.put(SampleRaw.COLUMN_START_TIME, new Ix0.Ai(SampleRaw.COLUMN_START_TIME, "TEXT", false, 0, null, 1));
            hashMap8.put(SampleRaw.COLUMN_END_TIME, new Ix0.Ai(SampleRaw.COLUMN_END_TIME, "TEXT", false, 0, null, 1));
            hashMap8.put("target", new Ix0.Ai("target", "INTEGER", false, 0, null, 1));
            hashMap8.put("duration", new Ix0.Ai("duration", "INTEGER", false, 0, null, 1));
            hashMap8.put(ShareConstants.WEB_DIALOG_PARAM_PRIVACY, new Ix0.Ai(ShareConstants.WEB_DIALOG_PARAM_PRIVACY, "TEXT", false, 0, null, 1));
            hashMap8.put("version", new Ix0.Ai("version", "TEXT", false, 0, null, 1));
            hashMap8.put("status", new Ix0.Ai("status", "TEXT", false, 0, null, 1));
            hashMap8.put("players", new Ix0.Ai("players", "TEXT", true, 0, null, 1));
            hashMap8.put("topPlayer", new Ix0.Ai("topPlayer", "TEXT", false, 0, null, 1));
            hashMap8.put("currentPlayer", new Ix0.Ai("currentPlayer", "TEXT", false, 0, null, 1));
            hashMap8.put("createdAt", new Ix0.Ai("createdAt", "TEXT", false, 0, null, 1));
            hashMap8.put("updatedAt", new Ix0.Ai("updatedAt", "TEXT", false, 0, null, 1));
            hashMap8.put("completedAt", new Ix0.Ai("completedAt", "TEXT", false, 0, null, 1));
            hashMap8.put("isFirst", new Ix0.Ai("isFirst", "INTEGER", true, 0, null, 1));
            Ix0 ix08 = new Ix0("history_challenge", hashMap8, new HashSet(0), new HashSet(0));
            Ix0 a9 = Ix0.a(lx0, "history_challenge");
            if (!ix08.equals(a9)) {
                return new Qh.Bi(false, "history_challenge(com.portfolio.platform.buddy_challenge.data.HistoryChallenge).\n Expected:\n" + ix08 + "\n Found:\n" + a9);
            }
            HashMap hashMap9 = new HashMap(13);
            hashMap9.put("id", new Ix0.Ai("id", "TEXT", true, 1, null, 1));
            hashMap9.put("socialId", new Ix0.Ai("socialId", "TEXT", true, 0, null, 1));
            hashMap9.put(Constants.PROFILE_KEY_FIRST_NAME, new Ix0.Ai(Constants.PROFILE_KEY_FIRST_NAME, "TEXT", false, 0, null, 1));
            hashMap9.put(Constants.PROFILE_KEY_LAST_NAME, new Ix0.Ai(Constants.PROFILE_KEY_LAST_NAME, "TEXT", false, 0, null, 1));
            hashMap9.put("isDeleted", new Ix0.Ai("isDeleted", "INTEGER", false, 0, null, 1));
            hashMap9.put("ranking", new Ix0.Ai("ranking", "INTEGER", false, 0, null, 1));
            hashMap9.put("status", new Ix0.Ai("status", "TEXT", false, 0, null, 1));
            hashMap9.put("stepsBase", new Ix0.Ai("stepsBase", "INTEGER", false, 0, null, 1));
            hashMap9.put("stepsOffset", new Ix0.Ai("stepsOffset", "INTEGER", false, 0, null, 1));
            hashMap9.put("caloriesBase", new Ix0.Ai("caloriesBase", "INTEGER", false, 0, null, 1));
            hashMap9.put("caloriesOffset", new Ix0.Ai("caloriesOffset", "INTEGER", false, 0, null, 1));
            hashMap9.put(Constants.PROFILE_KEY_PROFILE_PIC, new Ix0.Ai(Constants.PROFILE_KEY_PROFILE_PIC, "TEXT", false, 0, null, 1));
            hashMap9.put("lastSync", new Ix0.Ai("lastSync", "TEXT", false, 0, null, 1));
            Ix0 ix09 = new Ix0("players", hashMap9, new HashSet(0), new HashSet(0));
            Ix0 a10 = Ix0.a(lx0, "players");
            if (ix09.equals(a10)) {
                return new Qh.Bi(true, null);
            }
            return new Qh.Bi(false, "players(com.portfolio.platform.buddy_challenge.data.BCPlayer).\n Expected:\n" + ix09 + "\n Found:\n" + a10);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase
    public Qs4 a() {
        Qs4 qs4;
        if (this.c != null) {
            return this.c;
        }
        synchronized (this) {
            if (this.c == null) {
                this.c = new Rs4(this);
            }
            qs4 = this.c;
        }
        return qs4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase
    public Ys4 b() {
        Ys4 ys4;
        if (this.a != null) {
            return this.a;
        }
        synchronized (this) {
            if (this.a == null) {
                this.a = new Zs4(this);
            }
            ys4 = this.a;
        }
        return ys4;
    }

    @DexIgnore
    @Override // com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase
    public Ft4 c() {
        Ft4 ft4;
        if (this.d != null) {
            return this.d;
        }
        synchronized (this) {
            if (this.d == null) {
                this.d = new Gt4(this);
            }
            ft4 = this.d;
        }
        return ft4;
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public void clearAllTables() {
        super.assertNotMainThread();
        Lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `profile`");
            writableDatabase.execSQL("DELETE FROM `friend`");
            writableDatabase.execSQL("DELETE FROM `challenge`");
            writableDatabase.execSQL("DELETE FROM `notification`");
            writableDatabase.execSQL("DELETE FROM `fitness`");
            writableDatabase.execSQL("DELETE FROM `display_player`");
            writableDatabase.execSQL("DELETE FROM `challenge_player`");
            writableDatabase.execSQL("DELETE FROM `history_challenge`");
            writableDatabase.execSQL("DELETE FROM `players`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Nw0 createInvalidationTracker() {
        return new Nw0(this, new HashMap(0), new HashMap(0), "profile", "friend", "challenge", "notification", "fitness", "display_player", "challenge_player", "history_challenge", "players");
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Ji createOpenHelper(Hw0 hw0) {
        Qh qh = new Qh(hw0, new a(1), "b238af0aae08cbacb61a8a87455c3eee", "5290fb2a20a9967b5a4b27ffa5f1bb99");
        Ji.Bi.Aii a2 = Ji.Bi.a(hw0.b);
        a2.c(hw0.c);
        a2.b(qh);
        return hw0.a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase
    public Jt4 d() {
        Jt4 jt4;
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b == null) {
                this.b = new Kt4(this);
            }
            jt4 = this.b;
        }
        return jt4;
    }
}
