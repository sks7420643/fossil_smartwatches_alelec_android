package com.portfolio.platform.buddy_challenge.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.CountDownTimer;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Bs4;
import com.fossil.Cs4;
import com.fossil.Ds4;
import com.fossil.Hs4;
import com.fossil.Is4;
import com.fossil.Xy4;
import com.mapped.W6;
import com.mapped.Wg6;
import com.mapped.X24;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CircleProgressView extends View implements Hs4, Is4 {
    @DexIgnore
    public float A;
    @DexIgnore
    public float B;
    @DexIgnore
    public Bs4 C;
    @DexIgnore
    public float D;
    @DexIgnore
    public int E;
    @DexIgnore
    public TimerViewObserver F;
    @DexIgnore
    public CountDownTimer G;
    @DexIgnore
    public int H;
    @DexIgnore
    public long I;
    @DexIgnore
    public long J;
    @DexIgnore
    public long K;
    @DexIgnore
    public String L;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Path c; // = new Path();
    @DexIgnore
    public /* final */ Paint d; // = new Paint(1);
    @DexIgnore
    public /* final */ Path e; // = new Path();
    @DexIgnore
    public /* final */ Paint f; // = new Paint(1);
    @DexIgnore
    public /* final */ Path g; // = new Path();
    @DexIgnore
    public /* final */ Paint h; // = new Paint(1);
    @DexIgnore
    public float i; // = 0.5f;
    @DexIgnore
    public float j; // = 60.0f;
    @DexIgnore
    public float k; // = 2.0f;
    @DexIgnore
    public float l; // = 270.0f;
    @DexIgnore
    public float m; // = 40.0f;
    @DexIgnore
    public /* final */ float s; // = 360.0f;
    @DexIgnore
    public /* final */ RectF t; // = new RectF();
    @DexIgnore
    public boolean u; // = true;
    @DexIgnore
    public int v; // = W6.d(getContext(), 2131099701);
    @DexIgnore
    public int w; // = W6.d(getContext(), 2131099702);
    @DexIgnore
    public int x; // = W6.d(getContext(), 2131099689);
    @DexIgnore
    public Ds4 y; // = Ds4.IDLE;
    @DexIgnore
    public float z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleProgressView(Context context) {
        super(context);
        Wg6.c(context, "context");
        String simpleName = CircleProgressView.class.getSimpleName();
        Wg6.b(simpleName, "CircleProgressView::class.java.simpleName");
        this.b = simpleName;
        Context context2 = getContext();
        Wg6.b(context2, "context");
        this.C = new Bs4(context2, this);
        this.D = 800.0f;
        this.E = 10;
        this.H = System.identityHashCode(this);
        this.L = this.b;
        i(context, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleProgressView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attributeSet");
        String simpleName = CircleProgressView.class.getSimpleName();
        Wg6.b(simpleName, "CircleProgressView::class.java.simpleName");
        this.b = simpleName;
        Context context2 = getContext();
        Wg6.b(context2, "context");
        this.C = new Bs4(context2, this);
        this.D = 800.0f;
        this.E = 10;
        this.H = System.identityHashCode(this);
        this.L = this.b;
        i(context, attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleProgressView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Wg6.c(context, "context");
        Wg6.c(attributeSet, "attributeSet");
        String simpleName = CircleProgressView.class.getSimpleName();
        Wg6.b(simpleName, "CircleProgressView::class.java.simpleName");
        this.b = simpleName;
        Context context2 = getContext();
        Wg6.b(context2, "context");
        this.C = new Bs4(context2, this);
        this.D = 800.0f;
        this.E = 10;
        this.H = System.identityHashCode(this);
        this.L = this.b;
        i(context, attributeSet);
    }

    @DexIgnore
    @Override // com.fossil.Hs4
    public void a() {
        invalidate();
    }

    @DexIgnore
    @Override // com.fossil.Is4
    public void b(long j2, long j3) {
        l(j2, j3);
    }

    @DexIgnore
    public final float c(long j2, long j3) {
        return (((float) j3) * this.s) / ((float) j2);
    }

    @DexIgnore
    @Override // com.fossil.Is4
    public void d(long j2, long j3) {
        Is4.Ai.d(this, j2, j3);
    }

    @DexIgnore
    public void e() {
        FLogger.INSTANCE.getLocal().e(this.b, "clean");
        CountDownTimer timer = getTimer();
        if (timer != null) {
            timer.cancel();
        }
        TimerViewObserver timerViewObserver = getTimerViewObserver();
        if (timerViewObserver != null) {
            timerViewObserver.d(getIdentity());
        }
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        if (this.u) {
            this.c.reset();
            float f2 = this.l;
            while (f2 < this.l + this.s) {
                this.c.addArc(this.t, f2, this.i);
                f2 += this.k;
            }
            if (canvas != null) {
                canvas.drawPath(this.c, this.d);
            }
        }
    }

    @DexIgnore
    public final void g(Canvas canvas) {
        this.e.reset();
        RectF rectF = new RectF();
        RectF rectF2 = this.t;
        float f2 = rectF2.left;
        float f3 = this.j;
        float f4 = (float) 2;
        rectF.set(f2 + (f3 / f4), rectF2.top + (f3 / f4), rectF2.right - (f3 / f4), rectF2.bottom - (f3 / f4));
        if (canvas != null) {
            canvas.drawArc(rectF, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.s, true, this.f);
        }
    }

    @DexIgnore
    @Override // com.fossil.Hs4
    public float getAnimationDuration() {
        return this.D;
    }

    @DexIgnore
    @Override // com.fossil.Hs4
    public Ds4 getAnimationState() {
        return this.y;
    }

    @DexIgnore
    @Override // com.fossil.Hs4
    public float getCurrentValue() {
        return this.B;
    }

    @DexIgnore
    @Override // com.fossil.Is4
    public long getEndTime() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.Hs4
    public int getFrameDelay() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.Hs4
    public float getFrom() {
        return this.z;
    }

    @DexIgnore
    public Bs4 getHandler() {
        return this.C;
    }

    @DexIgnore
    public int getIdentity() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.Is4
    public long getInterval() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.Is4
    public String getTag() {
        return this.L;
    }

    @DexIgnore
    @Override // com.fossil.Is4
    public CountDownTimer getTimer() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.Is4
    public TimerViewObserver getTimerViewObserver() {
        return this.F;
    }

    @DexIgnore
    @Override // com.fossil.Hs4
    public float getTo() {
        return this.A;
    }

    @DexIgnore
    @Override // com.fossil.Is4
    public long getTotalMillisecond() {
        return this.I;
    }

    @DexIgnore
    public final void h(Canvas canvas) {
        this.g.reset();
        float f2 = this.l;
        while (f2 < this.l + getCurrentValue()) {
            this.g.addArc(this.t, f2, this.i);
            f2 += this.k;
        }
        if (canvas != null) {
            canvas.drawPath(this.g, this.h);
        }
    }

    @DexIgnore
    public final void i(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, X24.CircleProgressView);
        this.i = obtainStyledAttributes.getFloat(3, 0.5f);
        this.j = obtainStyledAttributes.getDimension(2, 60.0f);
        this.u = obtainStyledAttributes.getBoolean(7, true);
        this.m = obtainStyledAttributes.getDimension(8, 40.0f);
        this.v = obtainStyledAttributes.getColor(0, W6.d(context, 2131099701));
        this.x = obtainStyledAttributes.getColor(9, W6.d(context, 2131099689));
        this.w = obtainStyledAttributes.getColor(0, W6.d(context, 2131099702));
        setCurrentValue(obtainStyledAttributes.getFloat(4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        obtainStyledAttributes.recycle();
        this.d.setColor(this.v);
        this.d.setStyle(Paint.Style.STROKE);
        this.d.setStrokeWidth(this.j);
        this.f.setColor(this.w);
        this.f.setStyle(Paint.Style.FILL);
        this.h.setColor(this.x);
        this.h.setStyle(Paint.Style.STROKE);
        this.h.setStrokeWidth(this.j);
    }

    @DexIgnore
    public final void j(long j2, long j3) {
        setTotalMillisecond(j2);
        setEndTime(j3);
        setInterval(j2 <= 3600000 ? ButtonService.CONNECT_TIMEOUT : 30000);
        d(j2, getEndTime() - Xy4.a.b());
    }

    @DexIgnore
    public void k(TimerViewObserver timerViewObserver, int i2) {
        Is4.Ai.c(this, timerViewObserver, i2);
    }

    @DexIgnore
    public final void l(long j2, long j3) {
        float c2 = c(j3, j2);
        if (Math.abs(getCurrentValue() - c2) > 2.0f) {
            Message message = new Message();
            message.what = Cs4.SET_VALUE_ANIMATED.ordinal();
            message.obj = new float[]{getCurrentValue(), c2};
            getHandler().sendMessage(message);
            return;
        }
        setCurrentValue(c2);
        invalidate();
    }

    @DexIgnore
    public final void m(long j2, long j3) {
        setCurrentValue(c(j3, j2));
        invalidate();
    }

    @DexIgnore
    @Override // com.fossil.Is4
    public void onDone() {
        m(0, 1);
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        f(canvas);
        g(canvas);
        h(canvas);
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        int min = Math.min(View.getDefaultSize(getSuggestedMinimumWidth(), i2), View.getDefaultSize(getSuggestedMinimumHeight(), i3));
        RectF rectF = this.t;
        float f2 = this.m;
        float f3 = (float) min;
        rectF.set(f2, f2, f3 - f2, f3 - f2);
    }

    @DexIgnore
    @Override // com.fossil.Bz4
    public void onPause() {
        Is4.Ai.a(this);
    }

    @DexIgnore
    @Override // com.fossil.Bz4
    public void onResume() {
        Is4.Ai.b(this);
    }

    @DexIgnore
    public void setAnimationDuration(float f2) {
        this.D = f2;
    }

    @DexIgnore
    @Override // com.fossil.Hs4
    public void setAnimationState(Ds4 ds4) {
        Wg6.c(ds4, "<set-?>");
        this.y = ds4;
    }

    @DexIgnore
    @Override // com.fossil.Hs4
    public void setCurrentValue(float f2) {
        this.B = f2;
    }

    @DexIgnore
    public void setEndTime(long j2) {
        this.J = j2;
    }

    @DexIgnore
    public void setFrameDelay(int i2) {
        this.E = i2;
    }

    @DexIgnore
    @Override // com.fossil.Hs4
    public void setFrom(float f2) {
        this.z = f2;
    }

    @DexIgnore
    public void setHandler(Bs4 bs4) {
        Wg6.c(bs4, "<set-?>");
        this.C = bs4;
    }

    @DexIgnore
    public void setIdentity(int i2) {
        this.H = i2;
    }

    @DexIgnore
    public void setInterval(long j2) {
        this.K = j2;
    }

    @DexIgnore
    public final void setProgressColour(int i2) {
        this.x = i2;
        this.h.setColor(i2);
        invalidate();
    }

    @DexIgnore
    public void setTag(String str) {
        Wg6.c(str, "<set-?>");
        this.L = str;
    }

    @DexIgnore
    @Override // com.fossil.Is4
    public void setTimer(CountDownTimer countDownTimer) {
        this.G = countDownTimer;
    }

    @DexIgnore
    @Override // com.fossil.Is4
    public void setTimerViewObserver(TimerViewObserver timerViewObserver) {
        this.F = timerViewObserver;
    }

    @DexIgnore
    @Override // com.fossil.Hs4
    public void setTo(float f2) {
        this.A = f2;
    }

    @DexIgnore
    public void setTotalMillisecond(long j2) {
        this.I = j2;
    }
}
