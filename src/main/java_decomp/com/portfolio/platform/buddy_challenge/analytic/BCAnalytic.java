package com.portfolio.platform.buddy_challenge.analytic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.share.internal.ShareConstants;
import com.fossil.Bw7;
import com.fossil.Ct0;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Ht4;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Ms4;
import com.fossil.Ps4;
import com.fossil.Tt4;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AnalyticsHelper;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCAnalytic {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Ai b; // = new Ai(this);
    @DexIgnore
    public /* final */ AnalyticsHelper c;
    @DexIgnore
    public /* final */ Tt4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ BCAnalytic a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(BCAnalytic bCAnalytic) {
            this.a = bCAnalytic;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            String str;
            BCAnalytic bCAnalytic = this.a;
            if (intent == null || (str = intent.getAction()) == null) {
                str = "";
            }
            bCAnalytic.l(str, intent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.analytic.BCAnalytic$logChallengeFriend$1", f = "BCAnalytic.kt", l = {252}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCAnalytic this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(BCAnalytic bCAnalytic, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCAnalytic;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$challengeId, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object t;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Tt4 tt4 = this.this$0.d;
                String str = this.$challengeId;
                this.L$0 = il6;
                this.label = 1;
                t = tt4.t(str, new String[]{"invited"}, this);
                if (t == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                t = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Kz4 kz4 = (Kz4) t;
            if (kz4.c() != null) {
                for (Ms4 ms4 : (Iterable) kz4.c()) {
                    this.this$0.t(this.$challengeId, PortfolioApp.get.instance().l0(), ms4.d());
                }
            }
            return Cd6.a;
        }
    }

    @DexIgnore
    public BCAnalytic(AnalyticsHelper analyticsHelper, Tt4 tt4) {
        Wg6.c(analyticsHelper, "firebaseAnalytics");
        Wg6.c(tt4, "challengeRepository");
        this.c = analyticsHelper;
        this.d = tt4;
        String simpleName = BCAnalytic.class.getSimpleName();
        Wg6.b(simpleName, "BCAnalytic::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public final HashMap<String, Object> d() {
        HashMap<String, Object> hashMap = new HashMap<>();
        String g = this.c.g();
        if (g == null) {
            g = "";
        }
        hashMap.put("user_id", g);
        hashMap.put(Constants.SERIAL_NUMBER, PortfolioApp.get.instance().J());
        return hashMap;
    }

    @DexIgnore
    public final IntentFilter e() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("bc_create_challenge");
        intentFilter.addAction("bc_challenge_a_friend");
        intentFilter.addAction("bc_accept_challenge");
        intentFilter.addAction("bc_join_challenge");
        intentFilter.addAction("bc_left_challenge_after_start");
        intentFilter.addAction("bc_left_challenge_before_start");
        intentFilter.addAction("bc_complete_challenge");
        intentFilter.addAction("bc_create_social_profile");
        intentFilter.addAction("bc_look_for_friend");
        intentFilter.addAction("bc_send_friend_request");
        intentFilter.addAction("bc_reject_friend_request");
        intentFilter.addAction("bc_accept_friend_request");
        intentFilter.addAction("bc_look_for_friend_challenge");
        return intentFilter;
    }

    @DexIgnore
    public final void f(Intent intent) {
        String str;
        String stringExtra;
        String str2 = (intent == null || (stringExtra = intent.getStringExtra("challenge_id_extra")) == null) ? "" : stringExtra;
        if (intent == null || (str = intent.getStringExtra("current_user_id_extra")) == null) {
            str = "";
        }
        HashMap<String, Object> d2 = d();
        d2.put("challenge_id", str2);
        d2.put("profile_id", str);
        this.c.l("bc_accept_challenge", d2);
    }

    @DexIgnore
    public final void g(Intent intent) {
        String str;
        String stringExtra;
        String str2 = (intent == null || (stringExtra = intent.getStringExtra("current_user_id_extra")) == null) ? "" : stringExtra;
        if (intent == null || (str = intent.getStringExtra("friend_id_extra")) == null) {
            str = "";
        }
        HashMap<String, Object> d2 = d();
        d2.put("profile_id", str2);
        d2.put("to_profile_id", str);
        this.c.l("bc_accept_friend_request", d2);
    }

    @DexIgnore
    public final void h(String str) {
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Bi(this, str, null), 3, null);
    }

    @DexIgnore
    public final void i(Intent intent) {
        String stringExtra;
        String stringExtra2;
        boolean z = false;
        String str = (intent == null || (stringExtra2 = intent.getStringExtra("challenge_id_extra")) == null) ? "" : stringExtra2;
        String str2 = (intent == null || (stringExtra = intent.getStringExtra("current_user_id_extra")) == null) ? "" : stringExtra;
        int intExtra = intent != null ? intent.getIntExtra("rank_extra", 0) : 0;
        int intExtra2 = intent != null ? intent.getIntExtra("current_steps_extra", 0) : 0;
        if (intent != null) {
            z = intent.getBooleanExtra("reach_goal_or_top", false);
        }
        HashMap<String, Object> d2 = d();
        d2.put("challenge_id", str);
        d2.put("profile_id", str2);
        d2.put("rank", Integer.valueOf(intExtra));
        d2.put("number_of_steps", Integer.valueOf(intExtra2));
        d2.put("is_reach_goal_or_get_best_result", Boolean.valueOf(z));
        this.c.l("bc_complete_challenge", d2);
    }

    @DexIgnore
    public final void j(Intent intent) {
        HashMap<String, Object> hashMap;
        String str;
        String str2;
        String stringExtra;
        long j = 0L;
        long longExtra = intent != null ? intent.getLongExtra("start_tracking_time_extra", 0) : 0;
        String str3 = (intent == null || (stringExtra = intent.getStringExtra("start_type_extra")) == null) ? "" : stringExtra;
        Ps4 ps4 = intent != null ? (Ps4) intent.getParcelableExtra("challenge_extra") : null;
        if (ps4 != null) {
            HashMap<String, Object> d2 = d();
            Ht4 i = ps4.i();
            if (i == null || (str2 = i.b()) == null) {
                str2 = "";
            }
            d2.put("profile_id", str2);
            d2.put("challenge_id", ps4.f());
            d2.put("start_tracking_at", Long.valueOf(longExtra));
            d2.put("end_tracking_at", Long.valueOf(System.currentTimeMillis()));
            String g = ps4.g();
            if (g == null) {
                g = "";
            }
            d2.put("challenge_name", g);
            d2.put("challenge_mode", Wg6.a(ps4.r(), "activity_best_result") ? "ABR" : "ARG");
            Date b2 = ps4.b();
            d2.put("challenge_created_at", b2 != null ? Long.valueOf(b2.getTime()) : 0L);
            d2.put("challenge_start_type", str3);
            Date m = ps4.m();
            d2.put("challenge_start_at", m != null ? Long.valueOf(m.getTime()) : 0L);
            Date e = ps4.e();
            if (e != null) {
                j = Long.valueOf(e.getTime());
            }
            d2.put("challenge_end_at", j);
            Integer d3 = ps4.d();
            d2.put("duration", Float.valueOf((d3 != null ? (float) d3.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) 3600)));
            String k = ps4.k();
            if (k == null) {
                k = "";
            }
            d2.put(ShareConstants.WEB_DIALOG_PARAM_PRIVACY, k);
            Integer q = ps4.q();
            if (q != null) {
                d2.put("goal", Integer.valueOf(q.intValue()));
                hashMap = d2;
            } else {
                hashMap = d2;
            }
        } else {
            hashMap = null;
        }
        this.c.l("bc_create_challenge", hashMap);
        if (ps4 == null || (str = ps4.f()) == null) {
            str = "";
        }
        h(str);
    }

    @DexIgnore
    public final void k(Intent intent) {
        String str;
        if (intent == null || (str = intent.getStringExtra("current_user_id_extra")) == null) {
            str = "";
        }
        HashMap<String, Object> d2 = d();
        d2.put("profile_id", str);
        this.c.l("bc_create_social_profile", d2);
    }

    @DexIgnore
    public final void l(String str, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local.e(str2, "logEvent - action: " + str);
        switch (str.hashCode()) {
            case -2105630900:
                if (str.equals("bc_join_challenge")) {
                    o(intent);
                    return;
                }
                return;
            case -2059382039:
                if (str.equals("bc_left_challenge_after_start")) {
                    p(intent);
                    return;
                }
                return;
            case -1618588386:
                if (str.equals("bc_create_challenge")) {
                    j(intent);
                    return;
                }
                return;
            case -1436459621:
                if (str.equals("bc_complete_challenge")) {
                    i(intent);
                    return;
                }
                return;
            case 411008118:
                if (str.equals("bc_look_for_friend")) {
                    m(intent);
                    return;
                }
                return;
            case 528344632:
                if (str.equals("bc_left_challenge_before_start")) {
                    q(intent);
                    return;
                }
                return;
            case 567473916:
                if (str.equals("bc_create_social_profile")) {
                    k(intent);
                    return;
                }
                return;
            case 681203674:
                if (str.equals("bc_look_for_friend_challenge")) {
                    n(intent);
                    return;
                }
                return;
            case 691102855:
                if (str.equals("bc_accept_friend_request")) {
                    g(intent);
                    return;
                }
                return;
            case 695714346:
                if (str.equals("bc_accept_challenge")) {
                    f(intent);
                    return;
                }
                return;
            case 769307271:
                if (str.equals("bc_send_friend_request")) {
                    s(intent);
                    return;
                }
                return;
            case 1436927248:
                if (str.equals("bc_reject_friend_request")) {
                    r(intent);
                    return;
                }
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void m(Intent intent) {
        String str;
        if (intent == null || (str = intent.getStringExtra("current_user_id_extra")) == null) {
            str = "";
        }
        HashMap<String, Object> d2 = d();
        d2.put("profile_id", str);
        this.c.l("bc_look_for_friend", d2);
    }

    @DexIgnore
    public final void n(Intent intent) {
        String str;
        if (intent == null || (str = intent.getStringExtra("current_user_id_extra")) == null) {
            str = "";
        }
        HashMap<String, Object> d2 = d();
        d2.put("profile_id", str);
        this.c.l("bc_look_for_friend_challenge", d2);
    }

    @DexIgnore
    public final void o(Intent intent) {
        String str;
        String stringExtra;
        String str2 = (intent == null || (stringExtra = intent.getStringExtra("challenge_id_extra")) == null) ? "" : stringExtra;
        if (intent == null || (str = intent.getStringExtra("current_user_id_extra")) == null) {
            str = "";
        }
        HashMap<String, Object> d2 = d();
        d2.put("challenge_id", str2);
        d2.put("profile_id", str);
        this.c.l("bc_join_challenge", d2);
    }

    @DexIgnore
    public final void p(Intent intent) {
        String str;
        String stringExtra;
        int i = 0;
        String str2 = (intent == null || (stringExtra = intent.getStringExtra("challenge_id_extra")) == null) ? "" : stringExtra;
        if (intent == null || (str = intent.getStringExtra("current_user_id_extra")) == null) {
            str = "";
        }
        if (intent != null) {
            i = intent.getIntExtra("current_steps_extra", 0);
        }
        HashMap<String, Object> d2 = d();
        d2.put("challenge_id", str2);
        d2.put("profile_id", str);
        d2.put("number_of_steps", Integer.valueOf(i));
        this.c.l("bc_left_challenge_after_start", d2);
    }

    @DexIgnore
    public final void q(Intent intent) {
        String str;
        String stringExtra;
        String str2 = (intent == null || (stringExtra = intent.getStringExtra("challenge_id_extra")) == null) ? "" : stringExtra;
        if (intent == null || (str = intent.getStringExtra("current_user_id_extra")) == null) {
            str = "";
        }
        HashMap hashMap = new HashMap();
        hashMap.put("challenge_id", str2);
        hashMap.put("profile_id", str);
        this.c.l("bc_left_challenge_before_start", hashMap);
    }

    @DexIgnore
    public final void r(Intent intent) {
        String str;
        String stringExtra;
        String str2 = (intent == null || (stringExtra = intent.getStringExtra("current_user_id_extra")) == null) ? "" : stringExtra;
        if (intent == null || (str = intent.getStringExtra("friend_id_extra")) == null) {
            str = "";
        }
        HashMap<String, Object> d2 = d();
        d2.put("profile_id", str2);
        d2.put("to_profile_id", str);
        this.c.l("bc_reject_friend_request", d2);
    }

    @DexIgnore
    public final void s(Intent intent) {
        String str;
        String stringExtra;
        String str2 = (intent == null || (stringExtra = intent.getStringExtra("current_user_id_extra")) == null) ? "" : stringExtra;
        if (intent == null || (str = intent.getStringExtra("friend_id_extra")) == null) {
            str = "";
        }
        HashMap<String, Object> d2 = d();
        d2.put("profile_id", str2);
        d2.put("to_profile_id", str);
        this.c.l("bc_send_friend_request", d2);
    }

    @DexIgnore
    public final void t(String str, String str2, String str3) {
        HashMap<String, Object> d2 = d();
        d2.put("challenge_id", str);
        d2.put("profile_id", str2);
        d2.put("to_profile_id", str3);
        this.c.l("bc_challenge_a_friend", d2);
    }

    @DexIgnore
    public final void u(Context context) {
        Wg6.c(context, "context");
        Ct0.b(context).c(this.b, e());
    }
}
