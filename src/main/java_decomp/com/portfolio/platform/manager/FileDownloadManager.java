package com.portfolio.platform.manager;

import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.U08;
import com.fossil.Uu7;
import com.fossil.Ux7;
import com.fossil.W08;
import com.fossil.W18;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.LocalFile;
import com.portfolio.platform.data.source.remote.DownloadServiceApi;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDownloadManager {
    @DexIgnore
    public static /* final */ Dv7 i; // = Bw7.b();
    @DexIgnore
    public int a;
    @DexIgnore
    public U08 b; // = W08.b(false, 1, null);
    @DexIgnore
    public DownloadServiceApi c;
    @DexIgnore
    public /* final */ LinkedBlockingDeque<Ai> d; // = new LinkedBlockingDeque<>();
    @DexIgnore
    public /* final */ List<String> e; // = new ArrayList();
    @DexIgnore
    public /* final */ Uu7 f;
    @DexIgnore
    public /* final */ Il6 g;
    @DexIgnore
    public /* final */ Il6 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public int a;
        @DexIgnore
        public /* final */ LocalFile b;
        @DexIgnore
        public /* final */ Bi c;

        @DexIgnore
        public Ai(LocalFile localFile, Bi bi) {
            Wg6.c(localFile, "localFile");
            this.b = localFile;
            this.c = bi;
        }

        @DexIgnore
        public final Bi a() {
            return this.c;
        }

        @DexIgnore
        public final LocalFile b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.a;
        }

        @DexIgnore
        public final void d(int i) {
            this.a = i;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof Ai)) {
                return false;
            }
            return Wg6.a(this.b.getRemoteUrl(), ((Ai) obj).b.getRemoteUrl());
        }

        @DexIgnore
        public int hashCode() {
            int hashCode = this.b.hashCode();
            Bi bi = this.c;
            return (bi != null ? bi.hashCode() : 0) + (hashCode * 31);
        }

        @DexIgnore
        public String toString() {
            return "DownloadFileJobInfo(localFile=" + this.b + ", callback=" + this.c + ")";
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void onComplete(boolean z, LocalFile localFile);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.FileDownloadManager$downloadFile$1", f = "FileDownloadManager.kt", l = {100}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ai $job;
        @DexIgnore
        public /* final */ /* synthetic */ LocalFile $localFile;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FileDownloadManager this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.manager.FileDownloadManager$downloadFile$1$repoResponse$1", f = "FileDownloadManager.kt", l = {100}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Hg6<Xe6<? super Q88<W18>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(1, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                return new Aii(this.this$0, xe6);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Hg6
            public final Object invoke(Xe6<? super Q88<W18>> xe6) {
                throw null;
                //return ((Aii) create(xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    DownloadServiceApi j = this.this$0.this$0.j();
                    String remoteUrl = this.this$0.$localFile.getRemoteUrl();
                    this.label = 1;
                    Object downloadFile = j.downloadFile(remoteUrl, this);
                    return downloadFile == d ? d : downloadFile;
                } else if (i == 1) {
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(FileDownloadManager fileDownloadManager, LocalFile localFile, Ai ai, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = fileDownloadManager;
            this.$localFile = localFile;
            this.$job = ai;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$localFile, this.$job, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00c9  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0188  */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x0326  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 990
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.FileDownloadManager.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.FileDownloadManager$enqueue$1", f = "FileDownloadManager.kt", l = {213}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FileDownloadManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(FileDownloadManager fileDownloadManager, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = fileDownloadManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            U08 u08;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                u08 = this.this$0.b;
                this.L$0 = il6;
                this.L$1 = u08;
                this.label = 1;
                if (u08.a(null, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                u08 = (U08) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            try {
                this.this$0.g();
                Cd6 cd6 = Cd6.a;
                u08.b(null);
                return Cd6.a;
            } catch (Throwable th) {
                u08.b(null);
                throw th;
            }
        }
    }

    @DexIgnore
    public FileDownloadManager() {
        Uu7 b2 = Ux7.b(null, 1, null);
        this.f = b2;
        this.g = Jv7.a(b2.plus(i));
        this.h = Jv7.a(this.f.plus(Bw7.a()));
        PortfolioApp.get.instance().getIface().i1(this);
    }

    @DexIgnore
    public final void g() {
        synchronized (this) {
            if (this.d.size() > 0) {
                Ai pop = this.d.pop();
                Wg6.b(pop, "jobToRun");
                h(pop);
            }
        }
    }

    @DexIgnore
    public final void h(Ai ai) {
        LocalFile b2 = ai.b();
        this.a++;
        this.e.add(b2.getRemoteUrl());
        Rm6 unused = Gu7.d(this.g, null, null, new Ci(this, b2, ai, null), 3, null);
    }

    @DexIgnore
    public final void i(Ai ai) {
        synchronized (this) {
            Wg6.c(ai, "job");
            if (this.e.contains(ai.b().getRemoteUrl()) || this.d.contains(ai)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FileDownloadManager", ai.b().getRemoteUrl() + " is already added");
                return;
            }
            this.d.add(ai);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("FileDownloadManager", ai.b().getRemoteUrl() + " added to mQueue at times: " + ai.c() + ", having " + this.a + " download running");
            Rm6 unused = Gu7.d(this.h, null, null, new Di(this, null), 3, null);
        }
    }

    @DexIgnore
    public final DownloadServiceApi j() {
        DownloadServiceApi downloadServiceApi = this.c;
        if (downloadServiceApi != null) {
            return downloadServiceApi;
        }
        Wg6.n("mApiService");
        throw null;
    }
}
