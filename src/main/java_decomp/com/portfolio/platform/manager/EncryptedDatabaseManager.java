package com.portfolio.platform.manager;

import android.util.Log;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bw7;
import com.fossil.Eu7;
import com.fossil.Ko7;
import com.fossil.Nn5;
import com.fossil.Pw0;
import com.fossil.U08;
import com.fossil.W08;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Oh;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.UserDao;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase;
import java.io.File;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SupportFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EncryptedDatabaseManager {
    @DexIgnore
    public static U08 a; // = W08.b(false, 1, null);
    @DexIgnore
    public static volatile UserDatabase b;
    @DexIgnore
    public static volatile DianaCustomizeDatabase c;
    @DexIgnore
    public static volatile FitnessDatabase d;
    @DexIgnore
    public static volatile SleepDatabase e;
    @DexIgnore
    public static volatile GoalTrackingDatabase f;
    @DexIgnore
    public static volatile ThirdPartyDatabase g;
    @DexIgnore
    public static volatile AlarmDatabase h;
    @DexIgnore
    public static volatile WorkoutSettingDatabase i;
    @DexIgnore
    public static /* final */ EncryptedDatabaseManager j; // = new EncryptedDatabaseManager();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {340}, m = "getAlarmDatabase")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ EncryptedDatabaseManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(EncryptedDatabaseManager encryptedDatabaseManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = encryptedDatabaseManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {362}, m = "getDianaCustomizeDatabase")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ EncryptedDatabaseManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(EncryptedDatabaseManager encryptedDatabaseManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = encryptedDatabaseManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.v(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {329}, m = "getFitnessDatabase")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ EncryptedDatabaseManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(EncryptedDatabaseManager encryptedDatabaseManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = encryptedDatabaseManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.y(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {307}, m = "getGoalTrackingDatabase")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ EncryptedDatabaseManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(EncryptedDatabaseManager encryptedDatabaseManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = encryptedDatabaseManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.A(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {318}, m = "getSleepDatabase")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ EncryptedDatabaseManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(EncryptedDatabaseManager encryptedDatabaseManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = encryptedDatabaseManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.D(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {351}, m = "getThirdPartyDatabase")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ EncryptedDatabaseManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(EncryptedDatabaseManager encryptedDatabaseManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = encryptedDatabaseManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.F(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {296}, m = "getUserDatabase")
    public static final class Gi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ EncryptedDatabaseManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(EncryptedDatabaseManager encryptedDatabaseManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = encryptedDatabaseManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.H(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {380}, m = "getWorkoutSettingDatabase")
    public static final class Hi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ EncryptedDatabaseManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(EncryptedDatabaseManager encryptedDatabaseManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = encryptedDatabaseManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.J(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.EncryptedDatabaseManager$provideEncryptedDatabase$2", f = "EncryptionDatabaseManager.kt", l = {390, 56}, m = "invokeSuspend")
    public static final class Ii extends Ko7 implements Coroutine<Il6, Xe6<? super Integer>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        public Ii(Xe6 xe6) {
            super(2, xe6);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ii ii = new Ii(xe6);
            ii.p$ = (Il6) obj;
            throw null;
            //return ii;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Integer> xe6) {
            throw null;
            //return ((Ii) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00dd A[Catch:{ all -> 0x0129 }] */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0120  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
            // Method dump skipped, instructions count: 316
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.EncryptedDatabaseManager.Ii.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A(com.mapped.Xe6<? super com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.manager.EncryptedDatabaseManager.Di
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.portfolio.platform.manager.EncryptedDatabaseManager$Di r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager.Di) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.manager.EncryptedDatabaseManager r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager) r0
            com.fossil.El7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getGoalTrackingDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.f
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.manager.EncryptedDatabaseManager$Di r0 = new com.portfolio.platform.manager.EncryptedDatabaseManager$Di
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r2)
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.f
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getGoalTrackingDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.mapped.Wg6.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.f
            if (r0 != 0) goto L_0x0032
            com.mapped.Wg6.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.EncryptedDatabaseManager.A(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final UserDatabase B() {
        return b;
    }

    @DexIgnore
    public final SleepDatabase C(char[] cArr) {
        String str;
        UserDao userDao;
        MFUser currentUser;
        UserDatabase userDatabase = b;
        if (userDatabase == null || (userDao = userDatabase.userDao()) == null || (currentUser = userDao.getCurrentUser()) == null || (str = currentUser.getUserId()) == null) {
            str = "Anonymous";
        }
        String str2 = str + "_sleep.db";
        Nn5.Ai c2 = Nn5.a.c(PortfolioApp.get.instance(), str2);
        Log.d("EncryptionDM", "getSleepDatabase isEncrypted " + c2 + " dbName " + str2);
        if (c2 == Nn5.Ai.UNENCRYPTED) {
            Log.d("EncryptionDM", "getSleepDatabase encrypt fitness db with passphrase " + ((Object) cArr));
            Nn5 nn5 = Nn5.a;
            PortfolioApp instance = PortfolioApp.get.instance();
            File databasePath = PortfolioApp.get.instance().getDatabasePath(str2);
            Wg6.b(databasePath, "PortfolioApp.instance.getDatabasePath(dbName)");
            nn5.b(instance, databasePath, cArr != null ? (char[]) cArr.clone() : null);
        }
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            if (!Nn5.a.e(PortfolioApp.get.instance(), str2, bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.get.instance().getDatabasePath(str2).delete();
            }
            Oh.Ai a2 = Pw0.a(PortfolioApp.get.instance(), SleepDatabase.class, str2);
            a2.g(supportFactory);
            a2.b(SleepDatabase.Companion.getMIGRATION_FROM_3_TO_9());
            a2.f();
            a2.e();
            Oh d2 = a2.d();
            Wg6.b(d2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (SleepDatabase) d2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object D(com.mapped.Xe6<? super com.portfolio.platform.data.source.local.sleep.SleepDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.manager.EncryptedDatabaseManager.Ei
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.portfolio.platform.manager.EncryptedDatabaseManager$Ei r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager.Ei) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.manager.EncryptedDatabaseManager r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager) r0
            com.fossil.El7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getSleepDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.e
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.manager.EncryptedDatabaseManager$Ei r0 = new com.portfolio.platform.manager.EncryptedDatabaseManager$Ei
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r2)
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.e
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getSleepDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.mapped.Wg6.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.e
            if (r0 != 0) goto L_0x0032
            com.mapped.Wg6.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.EncryptedDatabaseManager.D(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final ThirdPartyDatabase E(char[] cArr) {
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            Nn5.Ai c2 = Nn5.a.c(PortfolioApp.get.instance(), "thirdParty.db");
            Log.d("EncryptionDM", "getThirdPartyDatabase isEncrypted " + c2 + " dbName thirdParty.db");
            if (c2 == Nn5.Ai.UNENCRYPTED) {
                Log.d("EncryptionDM", "getThirdPartyDatabase encrypt fitness db with passphrase " + ((Object) cArr));
                Nn5 nn5 = Nn5.a;
                PortfolioApp instance = PortfolioApp.get.instance();
                File databasePath = PortfolioApp.get.instance().getDatabasePath("thirdParty.db");
                Wg6.b(databasePath, "PortfolioApp.instance.ge\u2026HIRD_PARTY_DATABASE_NAME)");
                nn5.b(instance, databasePath, (char[]) cArr.clone());
            }
            if (!Nn5.a.e(PortfolioApp.get.instance(), "thirdParty.db", bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.get.instance().getDatabasePath("thirdParty.db").delete();
            }
            Oh.Ai a2 = Pw0.a(PortfolioApp.get.instance(), ThirdPartyDatabase.class, "thirdParty.db");
            a2.g(supportFactory);
            a2.f();
            Oh d2 = a2.d();
            Wg6.b(d2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (ThirdPartyDatabase) d2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object F(com.mapped.Xe6<? super com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.manager.EncryptedDatabaseManager.Fi
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.portfolio.platform.manager.EncryptedDatabaseManager$Fi r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager.Fi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.manager.EncryptedDatabaseManager r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager) r0
            com.fossil.El7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getThirdPartyDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.g
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.manager.EncryptedDatabaseManager$Fi r0 = new com.portfolio.platform.manager.EncryptedDatabaseManager$Fi
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r2)
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.g
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getThirdPartyDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.mapped.Wg6.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.g
            if (r0 != 0) goto L_0x0032
            com.mapped.Wg6.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.EncryptedDatabaseManager.F(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final UserDatabase G(char[] cArr) {
        if (Nn5.a.c(PortfolioApp.get.instance(), "user.db") == Nn5.Ai.UNENCRYPTED) {
            Nn5 nn5 = Nn5.a;
            PortfolioApp instance = PortfolioApp.get.instance();
            File databasePath = PortfolioApp.get.instance().getDatabasePath("user.db");
            Wg6.b(databasePath, "PortfolioApp.instance.ge\u2026tants.USER_DATABASE_NAME)");
            nn5.b(instance, databasePath, cArr != null ? (char[]) cArr.clone() : null);
        }
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            if (!Nn5.a.e(PortfolioApp.get.instance(), "user.db", bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.get.instance().getDatabasePath("user.db").delete();
            }
            Oh.Ai a2 = Pw0.a(PortfolioApp.get.instance(), UserDatabase.class, "user.db");
            a2.b(UserDatabase.Companion.getMIGRATION_FROM_3_TO_4(), UserDatabase.Companion.getMIGRATION_FROM_4_TO_5());
            a2.f();
            a2.e();
            a2.g(supportFactory);
            Oh d2 = a2.d();
            Wg6.b(d2, "Room\n                .da\u2026\n                .build()");
            return (UserDatabase) d2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object H(com.mapped.Xe6<? super com.portfolio.platform.data.model.room.UserDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.manager.EncryptedDatabaseManager.Gi
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.portfolio.platform.manager.EncryptedDatabaseManager$Gi r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager.Gi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.manager.EncryptedDatabaseManager r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager) r0
            com.fossil.El7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getUserDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.model.room.UserDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.b
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.manager.EncryptedDatabaseManager$Gi r0 = new com.portfolio.platform.manager.EncryptedDatabaseManager$Gi
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r2)
            com.portfolio.platform.data.model.room.UserDatabase r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.b
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getUserDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.mapped.Wg6.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.model.room.UserDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.b
            if (r0 != 0) goto L_0x0032
            com.mapped.Wg6.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.EncryptedDatabaseManager.H(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final WorkoutSettingDatabase I(char[] cArr) {
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            Nn5.Ai c2 = Nn5.a.c(PortfolioApp.get.instance(), "workoutSetting.db");
            Log.d("EncryptionDM", "getWorkoutSetting isEncrypted " + c2 + " dbName workoutSetting.db");
            if (c2 == Nn5.Ai.UNENCRYPTED) {
                Log.d("EncryptionDM", "getWorkoutSetting encrypt fitness db with passphrase " + ((Object) cArr));
                Nn5 nn5 = Nn5.a;
                PortfolioApp instance = PortfolioApp.get.instance();
                File databasePath = PortfolioApp.get.instance().getDatabasePath("workoutSetting.db");
                Wg6.b(databasePath, "PortfolioApp.instance.ge\u2026UT_SETTING_DATABASE_NAME)");
                nn5.b(instance, databasePath, (char[]) cArr.clone());
            }
            if (!Nn5.a.e(PortfolioApp.get.instance(), "workoutSetting.db", bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.get.instance().getDatabasePath("workoutSetting.db").delete();
            }
            Oh.Ai a2 = Pw0.a(PortfolioApp.get.instance(), WorkoutSettingDatabase.class, "workoutSetting.db");
            a2.g(supportFactory);
            a2.b(WorkoutSettingDatabase.Companion.getMIGRATION_FROM_1_TO_2());
            a2.f();
            Oh d2 = a2.d();
            Wg6.b(d2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (WorkoutSettingDatabase) d2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object J(com.mapped.Xe6<? super com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.manager.EncryptedDatabaseManager.Hi
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.portfolio.platform.manager.EncryptedDatabaseManager$Hi r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager.Hi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.manager.EncryptedDatabaseManager r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager) r0
            com.fossil.El7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getWorkoutSettingDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.i
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.manager.EncryptedDatabaseManager$Hi r0 = new com.portfolio.platform.manager.EncryptedDatabaseManager$Hi
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r2)
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.i
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getWorkoutSettingDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.mapped.Wg6.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.i
            if (r0 != 0) goto L_0x0032
            com.mapped.Wg6.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.EncryptedDatabaseManager.J(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final boolean K() {
        return b == null || d == null || e == null || f == null || g == null || h == null || i == null || c == null;
    }

    @DexIgnore
    public final /* synthetic */ Object L(Xe6<? super Integer> xe6) {
        return Eu7.g(Bw7.b(), new Ii(null), xe6);
    }

    @DexIgnore
    public final void M() {
        Log.d("EncryptionDM", "resetDatabasePrefix");
        d = null;
        e = null;
        h = null;
        b = null;
        i = null;
        f = null;
        g = null;
    }

    @DexIgnore
    public final AlarmDatabase t(char[] cArr) {
        UserDao userDao;
        MFUser currentUser;
        String userId;
        UserDatabase userDatabase = b;
        String str = (userDatabase == null || (userDao = userDatabase.userDao()) == null || (currentUser = userDao.getCurrentUser()) == null || (userId = currentUser.getUserId()) == null) ? "Anonymous" : userId;
        String str2 = str + "_alarm.db";
        Nn5.Ai c2 = Nn5.a.c(PortfolioApp.get.instance(), str2);
        Log.d("EncryptionDM", "getAlarmDatabase isEncrypted " + c2 + " dbName " + str2);
        if (c2 == Nn5.Ai.UNENCRYPTED) {
            Log.d("EncryptionDM", "getAlarmDatabase encrypt fitness db with passphrase " + ((Object) cArr));
            Nn5 nn5 = Nn5.a;
            PortfolioApp instance = PortfolioApp.get.instance();
            File databasePath = PortfolioApp.get.instance().getDatabasePath(str2);
            Wg6.b(databasePath, "PortfolioApp.instance.getDatabasePath(dbName)");
            nn5.b(instance, databasePath, cArr != null ? (char[]) cArr.clone() : null);
        }
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            if (!Nn5.a.e(PortfolioApp.get.instance(), str2, bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.get.instance().getDatabasePath(str2).delete();
            }
            Oh.Ai a2 = Pw0.a(PortfolioApp.get.instance(), AlarmDatabase.class, str2);
            a2.g(supportFactory);
            a2.b(AlarmDatabase.Companion.migrating3Or4To5(str, 3), AlarmDatabase.Companion.migrating3Or4To5(str, 4), AlarmDatabase.Companion.getMIGRATION_FROM_5_TO_6());
            a2.f();
            Oh d2 = a2.d();
            Wg6.b(d2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (AlarmDatabase) d2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object u(com.mapped.Xe6<? super com.portfolio.platform.data.source.local.alarm.AlarmDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.manager.EncryptedDatabaseManager.Ai
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.portfolio.platform.manager.EncryptedDatabaseManager$Ai r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.manager.EncryptedDatabaseManager r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager) r0
            com.fossil.El7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getAlarmDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.source.local.alarm.AlarmDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.h
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.manager.EncryptedDatabaseManager$Ai r0 = new com.portfolio.platform.manager.EncryptedDatabaseManager$Ai
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r2)
            com.portfolio.platform.data.source.local.alarm.AlarmDatabase r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.h
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getAlarmDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.mapped.Wg6.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.source.local.alarm.AlarmDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.h
            if (r0 != 0) goto L_0x0032
            com.mapped.Wg6.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.EncryptedDatabaseManager.u(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object v(com.mapped.Xe6<? super com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.manager.EncryptedDatabaseManager.Bi
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.portfolio.platform.manager.EncryptedDatabaseManager$Bi r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager.Bi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.manager.EncryptedDatabaseManager r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager) r0
            com.fossil.El7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getDianaCustomizeDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.c
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.manager.EncryptedDatabaseManager$Bi r0 = new com.portfolio.platform.manager.EncryptedDatabaseManager$Bi
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r2)
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.c
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getDianaCustomizeDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.mapped.Wg6.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.c
            if (r0 != 0) goto L_0x0032
            com.mapped.Wg6.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.EncryptedDatabaseManager.v(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final DianaCustomizeDatabase w(char[] cArr) {
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            Nn5.Ai c2 = Nn5.a.c(PortfolioApp.get.instance(), "dianaCustomize.db");
            Log.d("EncryptionDM", "getDianaCustomzieDatabase isEncrypted " + c2 + " dbName dianaCustomize.db");
            if (c2 == Nn5.Ai.UNENCRYPTED) {
                Log.d("EncryptionDM", "getDianaCustomzieDatabase encrypt diana customize db with passphrase " + ((Object) cArr));
                Nn5 nn5 = Nn5.a;
                PortfolioApp instance = PortfolioApp.get.instance();
                File databasePath = PortfolioApp.get.instance().getDatabasePath("dianaCustomize.db");
                Wg6.b(databasePath, "PortfolioApp.instance.ge\u2026_CUSTOMIZE_DATABASE_NAME)");
                nn5.b(instance, databasePath, (char[]) cArr.clone());
            }
            if (!Nn5.a.e(PortfolioApp.get.instance(), "dianaCustomize.db", bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.get.instance().getDatabasePath("dianaCustomize.db").delete();
            }
            Oh.Ai a2 = Pw0.a(PortfolioApp.get.instance(), DianaCustomizeDatabase.class, "dianaCustomize.db");
            a2.g(supportFactory);
            a2.b(DianaCustomizeDatabase.Companion.getMIGRATION_FROM_13_TO_14(), DianaCustomizeDatabase.Companion.getMIGRATION_FROM_14_TO_15(), DianaCustomizeDatabase.Companion.getMIGRATION_FROM_15_TO_16());
            Oh d2 = a2.d();
            Wg6.b(d2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (DianaCustomizeDatabase) d2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final FitnessDatabase x(char[] cArr) {
        String str;
        UserDao userDao;
        MFUser currentUser;
        UserDatabase userDatabase = b;
        if (userDatabase == null || (userDao = userDatabase.userDao()) == null || (currentUser = userDao.getCurrentUser()) == null || (str = currentUser.getUserId()) == null) {
            str = "Anonymous";
        }
        String str2 = str + "_fitness.db";
        Nn5.Ai c2 = Nn5.a.c(PortfolioApp.get.instance(), str2);
        Log.d("EncryptionDM", "getFitnessDatabase isEncrypted " + c2 + " dbName " + str2);
        if (c2 == Nn5.Ai.UNENCRYPTED) {
            Log.d("EncryptionDM", "getFitnessDatabase encrypt fitness db with passphrase " + ((Object) cArr));
            Nn5 nn5 = Nn5.a;
            PortfolioApp instance = PortfolioApp.get.instance();
            File databasePath = PortfolioApp.get.instance().getDatabasePath(str2);
            Wg6.b(databasePath, "PortfolioApp.instance.getDatabasePath(dbName)");
            nn5.b(instance, databasePath, cArr != null ? (char[]) cArr.clone() : null);
        }
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            if (!Nn5.a.e(PortfolioApp.get.instance(), str2, bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.get.instance().getDatabasePath(str2).delete();
            }
            Oh.Ai a2 = Pw0.a(PortfolioApp.get.instance(), FitnessDatabase.class, str2);
            a2.g(supportFactory);
            a2.b(FitnessDatabase.Companion.getMIGRATION_FROM_4_TO_21());
            a2.b(FitnessDatabase.Companion.getMIGRATION_FROM_21_TO_22());
            a2.f();
            a2.e();
            Oh d2 = a2.d();
            Wg6.b(d2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (FitnessDatabase) d2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object y(com.mapped.Xe6<? super com.portfolio.platform.data.source.local.fitness.FitnessDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.manager.EncryptedDatabaseManager.Ci
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.portfolio.platform.manager.EncryptedDatabaseManager$Ci r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager.Ci) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.manager.EncryptedDatabaseManager r0 = (com.portfolio.platform.manager.EncryptedDatabaseManager) r0
            com.fossil.El7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getFitnessDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.d
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.manager.EncryptedDatabaseManager$Ci r0 = new com.portfolio.platform.manager.EncryptedDatabaseManager$Ci
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r2)
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.d
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getFitnessDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.mapped.Wg6.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.d
            if (r0 != 0) goto L_0x0032
            com.mapped.Wg6.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.EncryptedDatabaseManager.y(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final GoalTrackingDatabase z(char[] cArr) {
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            Nn5.Ai c2 = Nn5.a.c(PortfolioApp.get.instance(), "goalTracking.db");
            Log.d("EncryptionDM", "getGoalTrackingDatabase isEncrypted " + c2 + " dbName goalTracking.db");
            if (c2 == Nn5.Ai.UNENCRYPTED) {
                Log.d("EncryptionDM", "getGoalTrackingDatabase encrypt fitness db with passphrase " + ((Object) cArr));
                Nn5 nn5 = Nn5.a;
                PortfolioApp instance = PortfolioApp.get.instance();
                File databasePath = PortfolioApp.get.instance().getDatabasePath("goalTracking.db");
                Wg6.b(databasePath, "PortfolioApp.instance.ge\u2026L_TRACKING_DATABASE_NAME)");
                nn5.b(instance, databasePath, (char[]) cArr.clone());
            }
            if (!Nn5.a.e(PortfolioApp.get.instance(), "goalTracking.db", bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.get.instance().getDatabasePath("goalTracking.db").delete();
            }
            Oh.Ai a2 = Pw0.a(PortfolioApp.get.instance(), GoalTrackingDatabase.class, "goalTracking.db");
            a2.g(supportFactory);
            a2.f();
            Oh d2 = a2.d();
            Wg6.b(d2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (GoalTrackingDatabase) d2;
        }
        Wg6.i();
        throw null;
    }
}
