package com.portfolio.platform.manager.validation;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.An4;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DataValidationManager {
    @DexIgnore
    public /* final */ DianaPresetRepository a;
    @DexIgnore
    public /* final */ An4 b;
    @DexIgnore
    public /* final */ WatchFaceRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.manager.validation.DataValidationManager", f = "DataValidationManager.kt", l = {19, 20, 26, 37}, m = "validateDianaPreset")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DataValidationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(DataValidationManager dataValidationManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = dataValidationManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, this);
        }
    }

    @DexIgnore
    public DataValidationManager(DianaPresetRepository dianaPresetRepository, An4 an4, WatchFaceRepository watchFaceRepository) {
        Wg6.c(dianaPresetRepository, "mDianaPresetRepository");
        Wg6.c(an4, "mSharePreferencesManager");
        Wg6.c(watchFaceRepository, "mWatchFaceRepository");
        this.a = dianaPresetRepository;
        this.b = an4;
        this.c = watchFaceRepository;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0199  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01c0  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01e2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r18, com.mapped.Xe6<? super com.mapped.Cd6> r19) {
        /*
        // Method dump skipped, instructions count: 526
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.manager.validation.DataValidationManager.a(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }
}
