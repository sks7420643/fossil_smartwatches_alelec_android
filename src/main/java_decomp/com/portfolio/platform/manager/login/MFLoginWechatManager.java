package com.portfolio.platform.manager.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import com.fossil.Bw7;
import com.fossil.Df7;
import com.fossil.Ef7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Q47;
import com.fossil.Q88;
import com.fossil.Tf7;
import com.fossil.Tq5;
import com.fossil.Yn5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Dx6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.WechatToken;
import com.portfolio.platform.data.source.remote.WechatApiService;
import com.portfolio.platform.manager.SoLibraryLoader;
import java.net.SocketTimeoutException;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MFLoginWechatManager implements Tf7 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ Ai e; // = new Ai(null);
    @DexIgnore
    public WechatApiService b; // = ((WechatApiService) Tq5.g.b(WechatApiService.class));
    @DexIgnore
    public boolean c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return MFLoginWechatManager.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MFLoginWechatManager b;
        @DexIgnore
        public /* final */ /* synthetic */ Yn5 c;

        @DexIgnore
        public Bi(MFLoginWechatManager mFLoginWechatManager, Yn5 yn5) {
            this.b = mFLoginWechatManager;
            this.c = yn5;
        }

        @DexIgnore
        public final void run() {
            if (!this.b.e()) {
                this.c.b(500, null, "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Q47.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ MFLoginWechatManager a;
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;
        @DexIgnore
        public /* final */ /* synthetic */ Yn5 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.manager.login.MFLoginWechatManager$loginWithWechat$2$onAuthSuccess$1", f = "MFLoginWechatManager.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $authToken;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii implements Dx6<WechatToken> {
                @DexIgnore
                public /* final */ /* synthetic */ Aii a;

                @DexIgnore
                /* JADX WARN: Incorrect args count in method signature: ()V */
                public Aiii(Aii aii) {
                    this.a = aii;
                }

                @DexIgnore
                @Override // com.mapped.Dx6
                public void onFailure(Call<WechatToken> call, Throwable th) {
                    Wg6.c(call, "call");
                    Wg6.c(th, "t");
                    FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.e.a(), "getWechatToken onFailure");
                    if (th instanceof SocketTimeoutException) {
                        this.a.this$0.c.b(MFNetworkReturnCode.CLIENT_TIMEOUT, null, "");
                    } else {
                        this.a.this$0.c.b(601, null, "");
                    }
                }

                @DexIgnore
                @Override // com.mapped.Dx6
                public void onResponse(Call<WechatToken> call, Q88<WechatToken> q88) {
                    Wg6.c(call, "call");
                    Wg6.c(q88, "response");
                    if (q88.e()) {
                        FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.e.a(), "getWechatToken isSuccessful");
                        WechatToken a2 = q88.a();
                        if (a2 != null) {
                            Wg6.b(a2, "response.body()!!");
                            WechatToken wechatToken = a2;
                            this.a.this$0.a.h(wechatToken.getOpenId());
                            SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                            signUpSocialAuth.setToken(wechatToken.getAccessToken());
                            signUpSocialAuth.setService("wechat");
                            this.a.this$0.c.a(signUpSocialAuth);
                            return;
                        }
                        Wg6.i();
                        throw null;
                    }
                    FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.e.a(), "getWechatToken isNotSuccessful");
                    this.a.this$0.c.b(600, null, q88.f());
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, String str, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$authToken = str;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$authToken, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                String str;
                String d;
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    Access c = SoLibraryLoader.f().c(PortfolioApp.get.instance());
                    WechatApiService d2 = this.this$0.a.d();
                    String str2 = (c == null || (d = c.getD()) == null) ? "" : d;
                    if (c == null || (str = c.getE()) == null) {
                        str = "";
                    }
                    d2.getWechatToken(str2, str, this.$authToken, "authorization_code").D(new Aiii(this));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public Ci(MFLoginWechatManager mFLoginWechatManager, Handler handler, Yn5 yn5) {
            this.a = mFLoginWechatManager;
            this.b = handler;
            this.c = yn5;
        }

        @DexIgnore
        @Override // com.fossil.Q47.Ai
        public void a() {
            this.a.i(true);
            this.b.removeCallbacksAndMessages(null);
            FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.e.a(), "Wechat onAuthFailed");
            this.c.b(600, null, "");
        }

        @DexIgnore
        @Override // com.fossil.Q47.Ai
        public void b() {
            this.a.i(true);
            this.b.removeCallbacksAndMessages(null);
            FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.e.a(), "Wechat onAuthAppNotInstalled");
            this.c.b(600, null, "");
        }

        @DexIgnore
        @Override // com.fossil.Q47.Ai
        public void c() {
            this.a.i(true);
            this.b.removeCallbacksAndMessages(null);
            FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.e.a(), "Wechat onAuthCancel");
            this.c.b(2, null, "");
        }

        @DexIgnore
        @Override // com.fossil.Q47.Ai
        public void d(String str) {
            Wg6.c(str, "authToken");
            this.a.i(true);
            this.b.removeCallbacksAndMessages(null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MFLoginWechatManager.e.a();
            local.d(a2, "Wechat onAuthSuccess: " + str);
            FLogger.INSTANCE.getLocal().d(MFLoginWechatManager.e.a(), "Wechat step 1: Login using wechat success");
            Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Aii(this, str, null), 3, null);
        }
    }

    /*
    static {
        String simpleName = MFLoginWechatManager.class.getSimpleName();
        Wg6.b(simpleName, "MFLoginWechatManager::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public MFLoginWechatManager() {
        Tq5.g.d("https://api.weixin.qq.com/sns/");
        Tq5.g.f(null);
    }

    @DexIgnore
    @Override // com.fossil.Tf7
    public void a(Df7 df7) {
        Wg6.c(df7, "baseReq");
        Q47.i().a(df7);
    }

    @DexIgnore
    @Override // com.fossil.Tf7
    public void b(Ef7 ef7) {
        Wg6.c(ef7, "baseResp");
        Q47.i().b(ef7);
    }

    @DexIgnore
    public final WechatApiService d() {
        return this.b;
    }

    @DexIgnore
    public final boolean e() {
        return this.c;
    }

    @DexIgnore
    public final void f(Activity activity, Yn5 yn5) {
        Wg6.c(activity, Constants.ACTIVITY);
        Wg6.c(yn5, Constants.CALLBACK);
        Q47.i().d(activity);
        Q47.i().c(activity.getIntent(), this);
        this.c = false;
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Bi(this, yn5), 120000);
        Q47.i().j(new Ci(this, handler, yn5));
    }

    @DexIgnore
    public final void g(Intent intent) {
        Wg6.c(intent, "intent");
        Q47.i().c(intent, this);
    }

    @DexIgnore
    public final void h(String str) {
    }

    @DexIgnore
    public final void i(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public final void j(String str) {
        Wg6.c(str, "appId");
        Q47.i().h(str);
    }
}
