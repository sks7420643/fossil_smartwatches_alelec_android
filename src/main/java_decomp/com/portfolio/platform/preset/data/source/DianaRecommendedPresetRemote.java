package com.portfolio.platform.preset.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.No5;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaRecommendedPresetRemote {
    @DexIgnore
    public /* final */ ApiServiceV2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.preset.data.source.DianaRecommendedPresetRemote$fetchRecommendedPreset$2", f = "DianaRecommendedPresetRemote.kt", l = {12}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<No5>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ DianaRecommendedPresetRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(DianaRecommendedPresetRemote dianaRecommendedPresetRemote, String str, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = dianaRecommendedPresetRemote;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Ai(this.this$0, this.$serial, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<ApiResponse<No5>>> xe6) {
            throw null;
            //return ((Ai) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                String str = this.$serial;
                this.label = 1;
                Object fetchRecommendedDianaPreset = apiServiceV2.fetchRecommendedDianaPreset(str, this);
                return fetchRecommendedDianaPreset == d ? d : fetchRecommendedDianaPreset;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public DianaRecommendedPresetRemote(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        this.a = apiServiceV2;
    }

    @DexIgnore
    public final Object b(String str, Xe6<? super Ap4<ApiResponse<No5>>> xe6) {
        return ResponseKt.d(new Ai(this, str, null), xe6);
    }
}
