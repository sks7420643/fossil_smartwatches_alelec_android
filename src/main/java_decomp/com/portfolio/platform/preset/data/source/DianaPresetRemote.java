package com.portfolio.platform.preset.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Mo5;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.SecureApiService;
import com.portfolio.platform.response.ResponseKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetRemote {
    @DexIgnore
    public /* final */ ApiServiceV2 a;
    @DexIgnore
    public /* final */ SecureApiService b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.preset.data.source.DianaPresetRemote$deletePresets$2", f = "DianaPresetRemote.kt", l = {24}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPresetRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(DianaPresetRemote dianaPresetRemote, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = dianaPresetRemote;
            this.$jsonObject = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Ai(this.this$0, this.$jsonObject, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Ai) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                Ku3 ku3 = this.$jsonObject;
                this.label = 1;
                Object deleteDianaPresets = apiServiceV2.deleteDianaPresets(ku3, this);
                return deleteDianaPresets == d ? d : deleteDianaPresets;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.preset.data.source.DianaPresetRemote$fetchDianaPreset$2", f = "DianaPresetRemote.kt", l = {16}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<Mo5>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPresetRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(DianaPresetRemote dianaPresetRemote, String str, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = dianaPresetRemote;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Bi(this.this$0, this.$serial, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<ApiResponse<Mo5>>> xe6) {
            throw null;
            //return ((Bi) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                String str = this.$serial;
                this.label = 1;
                Object fetchDianaPreset = apiServiceV2.fetchDianaPreset(str, 100, 0, this);
                return fetchDianaPreset == d ? d : fetchDianaPreset;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.preset.data.source.DianaPresetRemote$generateSharableLink$2", f = "DianaPresetRemote.kt", l = {28}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPresetRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(DianaPresetRemote dianaPresetRemote, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = dianaPresetRemote;
            this.$jsonObject = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Ci(this.this$0, this.$jsonObject, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Ci) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                Ku3 ku3 = this.$jsonObject;
                this.label = 1;
                Object generateSharableLink = apiServiceV2.generateSharableLink(ku3, this);
                return generateSharableLink == d ? d : generateSharableLink;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.preset.data.source.DianaPresetRemote$getDownloadSharingFaceUrl$2", f = "DianaPresetRemote.kt", l = {32}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Hg6<Xe6<? super Q88<Ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $token;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPresetRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(DianaPresetRemote dianaPresetRemote, String str, String str2, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = dianaPresetRemote;
            this.$id = str;
            this.$token = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Di(this.this$0, this.$id, this.$token, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<Ku3>> xe6) {
            throw null;
            //return ((Di) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.a;
                String str = this.$id;
                String str2 = this.$token;
                this.label = 1;
                Object sharingFace = apiServiceV2.getSharingFace(str, str2, this);
                return sharingFace == d ? d : sharingFace;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.preset.data.source.DianaPresetRemote$upsertDianaPreset$2", f = "DianaPresetRemote.kt", l = {20}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<Mo5>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPresetRemote this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(DianaPresetRemote dianaPresetRemote, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = dianaPresetRemote;
            this.$jsonObject = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Ei(this.this$0, this.$jsonObject, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<ApiResponse<Mo5>>> xe6) {
            throw null;
            //return ((Ei) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                SecureApiService secureApiService = this.this$0.b;
                Ku3 ku3 = this.$jsonObject;
                this.label = 1;
                Object upsertPresets = secureApiService.upsertPresets(ku3, this);
                return upsertPresets == d ? d : upsertPresets;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public DianaPresetRemote(ApiServiceV2 apiServiceV2, SecureApiService secureApiService) {
        Wg6.c(apiServiceV2, "api");
        Wg6.c(secureApiService, "secureApi");
        this.a = apiServiceV2;
        this.b = secureApiService;
    }

    @DexIgnore
    public final Object c(Ku3 ku3, Xe6<? super Ap4<Ku3>> xe6) {
        return ResponseKt.d(new Ai(this, ku3, null), xe6);
    }

    @DexIgnore
    public final Object d(String str, Xe6<? super Ap4<ApiResponse<Mo5>>> xe6) {
        return ResponseKt.d(new Bi(this, str, null), xe6);
    }

    @DexIgnore
    public final Object e(Ku3 ku3, Xe6<? super Ap4<Ku3>> xe6) {
        return ResponseKt.d(new Ci(this, ku3, null), xe6);
    }

    @DexIgnore
    public final Object f(String str, String str2, Xe6<? super Ap4<Ku3>> xe6) {
        return ResponseKt.d(new Di(this, str, str2, null), xe6);
    }

    @DexIgnore
    public final Object g(Ku3 ku3, Xe6<? super Ap4<ApiResponse<Mo5>>> xe6) {
        return ResponseKt.d(new Ei(this, ku3, null), xe6);
    }
}
