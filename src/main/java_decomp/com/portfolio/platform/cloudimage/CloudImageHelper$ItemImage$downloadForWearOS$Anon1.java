package com.portfolio.platform.cloudimage;

import android.widget.ImageView;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import java.io.File;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForWearOS$1", f = "CloudImageHelper.kt", l = {145, 149}, m = "invokeSuspend")
public final class CloudImageHelper$ItemImage$downloadForWearOS$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageHelper.ItemImage this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForWearOS$1$1", f = "CloudImageHelper.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageHelper$ItemImage$downloadForWearOS$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(CloudImageHelper$ItemImage$downloadForWearOS$Anon1 cloudImageHelper$ItemImage$downloadForWearOS$Anon1, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = cloudImageHelper$ItemImage$downloadForWearOS$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, xe6);
            anon1_Level2.p$ = (Il6) obj;
            throw null;
            //return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Anon1_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                if (this.this$0.this$0.mWeakReferenceImageView != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String tag = CloudImageHelper.Companion.getTAG();
                    local.d(tag, "download setDefaultImage first, resourceId=" + this.this$0.this$0.mResourceId);
                    WeakReference weakReference = this.this$0.this$0.mWeakReferenceImageView;
                    if (weakReference != null) {
                        ImageView imageView = (ImageView) weakReference.get();
                        if (imageView != null) {
                            Integer num = this.this$0.this$0.mResourceId;
                            if (num != null) {
                                imageView.setImageResource(num.intValue());
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                    } else {
                        throw new Rc6("null cannot be cast to non-null type java.lang.ref.WeakReference<android.widget.ImageView>");
                    }
                }
                File file = this.this$0.this$0.mFile;
                if (file != null) {
                    String str = this.this$0.this$0.mFastPairId;
                    if (str != null) {
                        CloudImageHelper.this.getMAppExecutors().b().execute(new CloudImageRunnable(file, str, ResolutionHelper.INSTANCE.getResolutionFromDevice().getResolution(), Constants.DownloadAssetType.WEAR_OS, this.this$0.this$0.mDeviceType.getType(), this.this$0.this$0.mListener));
                        return Cd6.a;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageHelper$ItemImage$downloadForWearOS$Anon1(CloudImageHelper.ItemImage itemImage, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = itemImage;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        CloudImageHelper$ItemImage$downloadForWearOS$Anon1 cloudImageHelper$ItemImage$downloadForWearOS$Anon1 = new CloudImageHelper$ItemImage$downloadForWearOS$Anon1(this.this$0, xe6);
        cloudImageHelper$ItemImage$downloadForWearOS$Anon1.p$ = (Il6) obj;
        throw null;
        //return cloudImageHelper$ItemImage$downloadForWearOS$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((CloudImageHelper$ItemImage$downloadForWearOS$Anon1) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x008c  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r11) {
        /*
            r10 = this;
            r9 = 2
            r5 = 1
            r8 = 0
            java.lang.Object r7 = com.fossil.Yn7.d()
            int r0 = r10.label
            if (r0 == 0) goto L_0x0036
            if (r0 == r5) goto L_0x0021
            if (r0 != r9) goto L_0x0019
            java.lang.Object r0 = r10.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r11)
        L_0x0016:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0018:
            return r0
        L_0x0019:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0021:
            java.lang.Object r0 = r10.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r11)
            r2 = r0
            r1 = r11
        L_0x002a:
            r0 = r1
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x008c
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
            goto L_0x0018
        L_0x0036:
            com.fossil.El7.b(r11)
            com.mapped.Il6 r6 = r10.p$
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r0 = r10.this$0
            java.io.File r0 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMFile$p(r0)
            if (r0 != 0) goto L_0x0052
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r0 = r10.this$0
            com.portfolio.platform.cloudimage.CloudImageHelper r1 = com.portfolio.platform.cloudimage.CloudImageHelper.this
            com.portfolio.platform.PortfolioApp r1 = r1.getMApp()
            java.io.File r1 = r1.getFilesDir()
            com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$setMFile$p(r0, r1)
        L_0x0052:
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r0 = r10.this$0
            java.lang.String r0 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMFastPairId$p(r0)
            if (r0 != 0) goto L_0x005d
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
            goto L_0x0018
        L_0x005d:
            com.portfolio.platform.cloudimage.AssetUtil r0 = com.portfolio.platform.cloudimage.AssetUtil.INSTANCE
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r1 = r10.this$0
            java.io.File r1 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMFile$p(r1)
            if (r1 == 0) goto L_0x00a6
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r2 = r10.this$0
            java.lang.String r2 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMFastPairId$p(r2)
            if (r2 == 0) goto L_0x00a2
            com.portfolio.platform.cloudimage.ResolutionHelper r3 = com.portfolio.platform.cloudimage.ResolutionHelper.INSTANCE
            com.portfolio.platform.cloudimage.Constants$Resolution r3 = r3.getResolutionFromDevice()
            java.lang.String r3 = r3.getResolution()
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage r4 = r10.this$0
            com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener r4 = com.portfolio.platform.cloudimage.CloudImageHelper.ItemImage.access$getMListener$p(r4)
            r10.L$0 = r6
            r10.label = r5
            r5 = r10
            java.lang.Object r1 = r0.checkWearOSAssetExist(r1, r2, r3, r4, r5)
            if (r1 != r7) goto L_0x00aa
            r0 = r7
            goto L_0x0018
        L_0x008c:
            com.fossil.Jx7 r0 = com.fossil.Bw7.c()
            com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForWearOS$Anon1$Anon1_Level2 r1 = new com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForWearOS$Anon1$Anon1_Level2
            r1.<init>(r10, r8)
            r10.L$0 = r2
            r10.label = r9
            java.lang.Object r0 = com.fossil.Eu7.g(r0, r1, r10)
            if (r0 != r7) goto L_0x0016
            r0 = r7
            goto L_0x0018
        L_0x00a2:
            com.mapped.Wg6.i()
            throw r8
        L_0x00a6:
            com.mapped.Wg6.i()
            throw r8
        L_0x00aa:
            r2 = r6
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForWearOS$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
