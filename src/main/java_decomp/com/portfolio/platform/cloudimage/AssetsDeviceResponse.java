package com.portfolio.platform.cloudimage;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AssetsDeviceResponse {
    @DexIgnore
    public /* final */ String category;
    @DexIgnore
    public /* final */ Data data;
    @DexIgnore
    public /* final */ String id;
    @DexIgnore
    public /* final */ Metadata metadata;
    @DexIgnore
    public /* final */ String sku;

    @DexIgnore
    public AssetsDeviceResponse() {
        this(null, null, null, null, null, 31, null);
    }

    @DexIgnore
    public AssetsDeviceResponse(String str, Data data2, Metadata metadata2, String str2, String str3) {
        this.category = str;
        this.data = data2;
        this.metadata = metadata2;
        this.sku = str2;
        this.id = str3;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ AssetsDeviceResponse(String str, Data data2, Metadata metadata2, String str2, String str3, int i, Qg6 qg6) {
        this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : data2, (i & 4) != 0 ? null : metadata2, (i & 8) != 0 ? null : str2, (i & 16) == 0 ? str3 : null);
    }

    @DexIgnore
    public static /* synthetic */ AssetsDeviceResponse copy$default(AssetsDeviceResponse assetsDeviceResponse, String str, Data data2, Metadata metadata2, String str2, String str3, int i, Object obj) {
        return assetsDeviceResponse.copy((i & 1) != 0 ? assetsDeviceResponse.category : str, (i & 2) != 0 ? assetsDeviceResponse.data : data2, (i & 4) != 0 ? assetsDeviceResponse.metadata : metadata2, (i & 8) != 0 ? assetsDeviceResponse.sku : str2, (i & 16) != 0 ? assetsDeviceResponse.id : str3);
    }

    @DexIgnore
    public final String component1() {
        return this.category;
    }

    @DexIgnore
    public final Data component2() {
        return this.data;
    }

    @DexIgnore
    public final Metadata component3() {
        return this.metadata;
    }

    @DexIgnore
    public final String component4() {
        return this.sku;
    }

    @DexIgnore
    public final String component5() {
        return this.id;
    }

    @DexIgnore
    public final AssetsDeviceResponse copy(String str, Data data2, Metadata metadata2, String str2, String str3) {
        return new AssetsDeviceResponse(str, data2, metadata2, str2, str3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AssetsDeviceResponse) {
                AssetsDeviceResponse assetsDeviceResponse = (AssetsDeviceResponse) obj;
                if (!Wg6.a(this.category, assetsDeviceResponse.category) || !Wg6.a(this.data, assetsDeviceResponse.data) || !Wg6.a(this.metadata, assetsDeviceResponse.metadata) || !Wg6.a(this.sku, assetsDeviceResponse.sku) || !Wg6.a(this.id, assetsDeviceResponse.id)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getCategory() {
        return this.category;
    }

    @DexIgnore
    public final Data getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final Metadata getMetadata() {
        return this.metadata;
    }

    @DexIgnore
    public final String getSku() {
        return this.sku;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.category;
        int hashCode = str != null ? str.hashCode() : 0;
        Data data2 = this.data;
        int hashCode2 = data2 != null ? data2.hashCode() : 0;
        Metadata metadata2 = this.metadata;
        int hashCode3 = metadata2 != null ? metadata2.hashCode() : 0;
        String str2 = this.sku;
        int hashCode4 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.id;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "AssetsDeviceResponse(category=" + this.category + ", data=" + this.data + ", metadata=" + this.metadata + ", sku=" + this.sku + ", id=" + this.id + ")";
    }
}
