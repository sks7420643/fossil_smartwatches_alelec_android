package com.portfolio.platform.cloudimage;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Metadata {
    @DexIgnore
    public /* final */ String checksum;
    @DexIgnore
    public /* final */ String feature;
    @DexIgnore
    public /* final */ String platform;
    @DexIgnore
    public /* final */ String resolution;
    @DexIgnore
    public /* final */ String serialNumber;

    @DexIgnore
    public Metadata() {
        this(null, null, null, null, null, 31, null);
    }

    @DexIgnore
    public Metadata(String str, String str2, String str3, String str4, String str5) {
        this.checksum = str;
        this.feature = str2;
        this.platform = str3;
        this.resolution = str4;
        this.serialNumber = str5;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Metadata(String str, String str2, String str3, String str4, String str5, int i, Qg6 qg6) {
        this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? null : str3, (i & 8) != 0 ? null : str4, (i & 16) == 0 ? str5 : null);
    }

    @DexIgnore
    public static /* synthetic */ Metadata copy$default(Metadata metadata, String str, String str2, String str3, String str4, String str5, int i, Object obj) {
        return metadata.copy((i & 1) != 0 ? metadata.checksum : str, (i & 2) != 0 ? metadata.feature : str2, (i & 4) != 0 ? metadata.platform : str3, (i & 8) != 0 ? metadata.resolution : str4, (i & 16) != 0 ? metadata.serialNumber : str5);
    }

    @DexIgnore
    public final String component1() {
        return this.checksum;
    }

    @DexIgnore
    public final String component2() {
        return this.feature;
    }

    @DexIgnore
    public final String component3() {
        return this.platform;
    }

    @DexIgnore
    public final String component4() {
        return this.resolution;
    }

    @DexIgnore
    public final String component5() {
        return this.serialNumber;
    }

    @DexIgnore
    public final Metadata copy(String str, String str2, String str3, String str4, String str5) {
        return new Metadata(str, str2, str3, str4, str5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Metadata) {
                Metadata metadata = (Metadata) obj;
                if (!Wg6.a(this.checksum, metadata.checksum) || !Wg6.a(this.feature, metadata.feature) || !Wg6.a(this.platform, metadata.platform) || !Wg6.a(this.resolution, metadata.resolution) || !Wg6.a(this.serialNumber, metadata.serialNumber)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getChecksum() {
        return this.checksum;
    }

    @DexIgnore
    public final String getFeature() {
        return this.feature;
    }

    @DexIgnore
    public final String getPlatform() {
        return this.platform;
    }

    @DexIgnore
    public final String getResolution() {
        return this.resolution;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.checksum;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.feature;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.platform;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.resolution;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.serialNumber;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Metadata(checksum=" + this.checksum + ", feature=" + this.feature + ", platform=" + this.platform + ", resolution=" + this.resolution + ", serialNumber=" + this.serialNumber + ")";
    }
}
