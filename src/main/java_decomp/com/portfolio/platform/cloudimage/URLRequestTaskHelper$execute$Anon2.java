package com.portfolio.platform.cloudimage;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.cloudimage.URLRequestTaskHelper$execute$2", f = "URLRequestTaskHelper.kt", l = {}, m = "invokeSuspend")
public final class URLRequestTaskHelper$execute$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ AssetsDeviceResponse $assetsDeviceResponse;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ URLRequestTaskHelper this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public URLRequestTaskHelper$execute$Anon2(URLRequestTaskHelper uRLRequestTaskHelper, AssetsDeviceResponse assetsDeviceResponse, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = uRLRequestTaskHelper;
        this.$assetsDeviceResponse = assetsDeviceResponse;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        URLRequestTaskHelper$execute$Anon2 uRLRequestTaskHelper$execute$Anon2 = new URLRequestTaskHelper$execute$Anon2(this.this$0, this.$assetsDeviceResponse, xe6);
        uRLRequestTaskHelper$execute$Anon2.p$ = (Il6) obj;
        throw null;
        //return uRLRequestTaskHelper$execute$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((URLRequestTaskHelper$execute$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Yn7.d();
        if (this.label == 0) {
            El7.b(obj);
            URLRequestTaskHelper.OnNextTaskListener listener$app_fossilRelease = this.this$0.getListener$app_fossilRelease();
            if (listener$app_fossilRelease == null) {
                return null;
            }
            String zipFilePath$app_fossilRelease = this.this$0.getZipFilePath$app_fossilRelease();
            String destinationUnzipPath$app_fossilRelease = this.this$0.getDestinationUnzipPath$app_fossilRelease();
            AssetsDeviceResponse assetsDeviceResponse = this.$assetsDeviceResponse;
            Wg6.b(assetsDeviceResponse, "assetsDeviceResponse");
            listener$app_fossilRelease.downloadFile(zipFilePath$app_fossilRelease, destinationUnzipPath$app_fossilRelease, assetsDeviceResponse);
            return Cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
