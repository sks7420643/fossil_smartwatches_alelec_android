package com.portfolio.platform.cloudimage;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.cloudimage.CloudImageHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$2", f = "AssetUtil.kt", l = {}, m = "invokeSuspend")
public final class AssetUtil$checkAssetExist$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $filePath1;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageHelper.OnImageCallbackListener $listener;
    @DexIgnore
    public /* final */ /* synthetic */ String $serialNumber;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AssetUtil$checkAssetExist$Anon2(CloudImageHelper.OnImageCallbackListener onImageCallbackListener, String str, String str2, Xe6 xe6) {
        super(2, xe6);
        this.$listener = onImageCallbackListener;
        this.$serialNumber = str;
        this.$filePath1 = str2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        AssetUtil$checkAssetExist$Anon2 assetUtil$checkAssetExist$Anon2 = new AssetUtil$checkAssetExist$Anon2(this.$listener, this.$serialNumber, this.$filePath1, xe6);
        assetUtil$checkAssetExist$Anon2.p$ = (Il6) obj;
        throw null;
        //return assetUtil$checkAssetExist$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((AssetUtil$checkAssetExist$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Yn7.d();
        if (this.label == 0) {
            El7.b(obj);
            CloudImageHelper.OnImageCallbackListener onImageCallbackListener = this.$listener;
            if (onImageCallbackListener == null) {
                return null;
            }
            onImageCallbackListener.onImageCallback(this.$serialNumber, this.$filePath1);
            return Cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
