package com.portfolio.platform.cloudimage;

import com.fossil.J37;
import com.fossil.Z58;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import io.flutter.plugin.common.StandardMessageCodec;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ChecksumUtil {
    @DexIgnore
    public static /* final */ ChecksumUtil INSTANCE; // = new ChecksumUtil();
    @DexIgnore
    public static /* final */ String TAG; // = (Constants.MAIN_TAG + ChecksumUtil.class.getSimpleName());

    @DexIgnore
    private final String bytesToString(byte[] bArr) {
        StringBuilder sb = new StringBuilder("");
        for (byte b : bArr) {
            String num = Integer.toString(((byte) (b & ((byte) 255))) + StandardMessageCodec.NULL, 16);
            Wg6.b(num, "Integer.toString((input[\u2026ff.toByte()) + 0x100, 16)");
            if (num != null) {
                String substring = num.substring(1);
                Wg6.b(substring, "(this as java.lang.String).substring(startIndex)");
                sb.append(substring);
            } else {
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        String sb2 = sb.toString();
        Wg6.b(sb2, "ret.toString()");
        if (sb2 != null) {
            String lowerCase = sb2.toLowerCase();
            Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
            return lowerCase;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final boolean verifyDownloadFile(String str, String str2) {
        Wg6.c(str, "filePath");
        if (str2 == null) {
            return true;
        }
        byte[] c = Z58.c(new File(str));
        Wg6.b(c, "binData");
        String d = J37.d(c);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.e(str3, "filePath: " + str + " - sumFromFile: " + d + " - checkSum: " + str2);
        String lowerCase = str2.toLowerCase();
        Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
        return Wg6.a(lowerCase, d);
    }
}
