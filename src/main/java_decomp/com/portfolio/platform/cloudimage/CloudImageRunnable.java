package com.portfolio.platform.cloudimage;

import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Pm7;
import com.mapped.Cd6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.cloudimage.FileDownloadTaskHelper;
import com.portfolio.platform.cloudimage.URLRequestTaskHelper;
import com.portfolio.platform.cloudimage.UnzippingTaskHelper;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.File;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CloudImageRunnable implements Runnable, FileDownloadTaskHelper.OnDownloadFinishListener, UnzippingTaskHelper.OnUnzipFinishListener, URLRequestTaskHelper.OnNextTaskListener {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static ArrayList<String> downloadingPath; // = new ArrayList<>();
    @DexIgnore
    public Constants.DownloadAssetType downloadAssetType;
    @DexIgnore
    public String fastPairId;
    @DexIgnore
    public /* final */ File file;
    @DexIgnore
    public CloudImageHelper.OnForceDownloadCallbackListener mDownloadListener;
    @DexIgnore
    public CloudImageHelper.OnImageCallbackListener mListener;
    @DexIgnore
    public String resolution;
    @DexIgnore
    public int retryDownloadTime;
    @DexIgnore
    public int retryUnzipTime;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public String serialPrefix;
    @DexIgnore
    public String type;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[Constants.DownloadAssetType.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[Constants.DownloadAssetType.DEVICE.ordinal()] = 1;
            $EnumSwitchMapping$0[Constants.DownloadAssetType.CALIBRATION.ordinal()] = 2;
            $EnumSwitchMapping$0[Constants.DownloadAssetType.WEAR_OS.ordinal()] = 3;
        }
        */
    }

    /*
    static {
        String simpleName = CloudImageRunnable.class.getSimpleName();
        Wg6.b(simpleName, "CloudImageRunnable::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public CloudImageRunnable(File file2) {
        Wg6.c(file2, "file");
        this.file = file2;
        this.downloadAssetType = Constants.DownloadAssetType.BOTH;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable(File file2, String str, String str2, Constants.DownloadAssetType downloadAssetType2, String str3, CloudImageHelper.OnImageCallbackListener onImageCallbackListener) {
        this(file2);
        Wg6.c(file2, "file");
        Wg6.c(str, "fastPairId");
        Wg6.c(str2, "resolution");
        Wg6.c(downloadAssetType2, "downloadAssetType");
        Wg6.c(str3, "type");
        this.downloadAssetType = downloadAssetType2;
        this.type = str3;
        this.mListener = onImageCallbackListener;
        this.fastPairId = str;
        this.resolution = str2;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable(File file2, String str, String str2, String str3) {
        this(file2);
        Wg6.c(file2, "file");
        Wg6.c(str, "serialNumber");
        Wg6.c(str2, "serialPrefix");
        Wg6.c(str3, "resolution");
        this.serialNumber = str;
        this.serialPrefix = str2;
        this.resolution = str3;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable(File file2, String str, String str2, String str3, Constants.DownloadAssetType downloadAssetType2, String str4, CloudImageHelper.OnImageCallbackListener onImageCallbackListener) {
        this(file2, str, str2, str3);
        Wg6.c(file2, "file");
        Wg6.c(str, "serialNumber");
        Wg6.c(str2, "serialPrefix");
        Wg6.c(str3, "resolution");
        Wg6.c(downloadAssetType2, "downloadAssetType");
        Wg6.c(str4, "type");
        this.downloadAssetType = downloadAssetType2;
        this.type = str4;
        this.mListener = onImageCallbackListener;
    }

    @DexIgnore
    public static final /* synthetic */ String access$getFastPairId$p(CloudImageRunnable cloudImageRunnable) {
        String str = cloudImageRunnable.fastPairId;
        if (str != null) {
            return str;
        }
        Wg6.n("fastPairId");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ String access$getResolution$p(CloudImageRunnable cloudImageRunnable) {
        String str = cloudImageRunnable.resolution;
        if (str != null) {
            return str;
        }
        Wg6.n("resolution");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ String access$getSerialNumber$p(CloudImageRunnable cloudImageRunnable) {
        String str = cloudImageRunnable.serialNumber;
        if (str != null) {
            return str;
        }
        Wg6.n("serialNumber");
        throw null;
    }

    @DexIgnore
    private final void downloadingDeviceAssets(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.file.getAbsolutePath());
        sb.append("/");
        String str2 = this.serialNumber;
        if (str2 != null) {
            sb.append(str2);
            sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
            String str3 = this.resolution;
            if (str3 != null) {
                sb.append(str3);
                sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                sb.append(str);
                sb.append(".zip");
                String sb2 = sb.toString();
                StringBuilder sb3 = new StringBuilder();
                sb3.append(this.file.getAbsolutePath());
                sb3.append("/");
                String str4 = this.serialNumber;
                if (str4 != null) {
                    sb3.append(str4);
                    sb3.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                    String str5 = this.resolution;
                    if (str5 != null) {
                        sb3.append(str5);
                        sb3.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                        sb3.append(str);
                        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new CloudImageRunnable$downloadingDeviceAssets$Anon1(this, sb2, sb3.toString(), str, null), 3, null);
                        return;
                    }
                    Wg6.n("resolution");
                    throw null;
                }
                Wg6.n("serialNumber");
                throw null;
            }
            Wg6.n("resolution");
            throw null;
        }
        Wg6.n("serialNumber");
        throw null;
    }

    @DexIgnore
    private final void downloadingWearOsAssets(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("downloadingWearOSAssets, fastPairId=");
        String str3 = this.fastPairId;
        if (str3 != null) {
            sb.append(str3);
            local.d(str2, sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append(this.file.getAbsolutePath());
            sb2.append("/");
            String str4 = this.fastPairId;
            if (str4 != null) {
                sb2.append(str4);
                sb2.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                String str5 = this.resolution;
                if (str5 != null) {
                    sb2.append(str5);
                    sb2.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                    sb2.append(str);
                    sb2.append(".zip");
                    String sb3 = sb2.toString();
                    StringBuilder sb4 = new StringBuilder();
                    sb4.append(this.file.getAbsolutePath());
                    sb4.append("/");
                    String str6 = this.fastPairId;
                    if (str6 != null) {
                        sb4.append(str6);
                        sb4.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                        String str7 = this.resolution;
                        if (str7 != null) {
                            sb4.append(str7);
                            sb4.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                            sb4.append(str);
                            Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new CloudImageRunnable$downloadingWearOsAssets$Anon1(this, sb3, sb4.toString(), str, null), 3, null);
                            return;
                        }
                        Wg6.n("resolution");
                        throw null;
                    }
                    Wg6.n("fastPairId");
                    throw null;
                }
                Wg6.n("resolution");
                throw null;
            }
            Wg6.n("fastPairId");
            throw null;
        }
        Wg6.n("fastPairId");
        throw null;
    }

    @DexIgnore
    private final URLRequestTaskHelper prepareURLRequestTask() {
        URLRequestTaskHelper newInstance = URLRequestTaskHelper.Companion.newInstance();
        newInstance.setOnNextTaskListener(this);
        return newInstance;
    }

    @DexIgnore
    private final void returnImageDownloaded(String str) {
        String str2 = this.type;
        if (!(str2 == null || str2.length() == 0)) {
            Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new CloudImageRunnable$returnImageDownloaded$Anon1(this, str, null), 3, null);
        }
    }

    @DexIgnore
    private final void startDownloadingAssets() {
        int i = WhenMappings.$EnumSwitchMapping$0[this.downloadAssetType.ordinal()];
        if (i == 1) {
            downloadingDeviceAssets(Constants.Feature.DEVICE.getFeature());
        } else if (i == 2) {
            downloadingDeviceAssets(Constants.Feature.CALIBRATION.getFeature());
        } else if (i != 3) {
            downloadingDeviceAssets(Constants.Feature.DEVICE.getFeature());
            downloadingDeviceAssets(Constants.Feature.CALIBRATION.getFeature());
        } else {
            downloadingWearOsAssets(Constants.Feature.DEVICE.getFeature());
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.cloudimage.URLRequestTaskHelper.OnNextTaskListener
    public void downloadFile(String str, String str2, AssetsDeviceResponse assetsDeviceResponse) {
        Wg6.c(assetsDeviceResponse, "response");
        synchronized (downloadingPath) {
            if (Pm7.K(downloadingPath, str) < 0) {
                FileDownloadTaskHelper newInstance = FileDownloadTaskHelper.Companion.newInstance();
                newInstance.setOnDownloadFinishListener(this);
                if (str == null) {
                    Wg6.i();
                    throw null;
                } else if (str2 != null) {
                    newInstance.init(str, str2, assetsDeviceResponse);
                    newInstance.execute();
                    downloadingPath.add(str);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            Cd6 cd6 = Cd6.a;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.cloudimage.FileDownloadTaskHelper.OnDownloadFinishListener
    public void onDownloadFail(String str, String str2, AssetsDeviceResponse assetsDeviceResponse) {
        Wg6.c(str, "zipFilePath");
        Wg6.c(str2, "destinationUnzipPath");
        Wg6.c(assetsDeviceResponse, "response");
        synchronized (downloadingPath) {
            downloadingPath.remove(str);
        }
        int i = this.retryDownloadTime + 1;
        this.retryDownloadTime = i;
        if (i >= 3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("onDownloadFail: retry reaches max retry for serialNumber = [");
            Metadata metadata = assetsDeviceResponse.getMetadata();
            sb.append(metadata != null ? metadata.getSerialNumber() : null);
            sb.append("]");
            local.d(str3, sb.toString());
            this.retryDownloadTime = 0;
            CloudImageHelper.OnForceDownloadCallbackListener onForceDownloadCallbackListener = this.mDownloadListener;
            if (onForceDownloadCallbackListener != null) {
                onForceDownloadCallbackListener.onDownloadCallback(false);
                return;
            }
            return;
        }
        downloadFile(str, str2, assetsDeviceResponse);
    }

    @DexIgnore
    @Override // com.portfolio.platform.cloudimage.FileDownloadTaskHelper.OnDownloadFinishListener
    public void onDownloadSuccess(String str, String str2) {
        Wg6.c(str, "zipFilePath");
        Wg6.c(str2, "destinationUnzipPath");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, "onDownloadSuccess path=" + str);
        synchronized (downloadingPath) {
            downloadingPath.remove(str);
        }
        UnzippingTaskHelper newInstance = UnzippingTaskHelper.Companion.newInstance();
        newInstance.setOnUnzipFinishListener(this);
        newInstance.init(str, str2);
        newInstance.execute();
    }

    @DexIgnore
    @Override // com.portfolio.platform.cloudimage.URLRequestTaskHelper.OnNextTaskListener
    public void onGetDeviceAssetFailed() {
        CloudImageHelper.OnForceDownloadCallbackListener onForceDownloadCallbackListener = this.mDownloadListener;
        if (onForceDownloadCallbackListener != null) {
            onForceDownloadCallbackListener.onDownloadCallback(false);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.cloudimage.UnzippingTaskHelper.OnUnzipFinishListener
    public void onUnzipFail(String str, String str2) {
        Wg6.c(str, "zipFilePath");
        Wg6.c(str2, "destinationUnzipPath");
        int i = this.retryUnzipTime + 1;
        this.retryUnzipTime = i;
        if (i >= 3) {
            FLogger.INSTANCE.getLocal().d(TAG, "onUnzipFail: retry reach max retry!");
            this.retryUnzipTime = 0;
            CloudImageHelper.OnForceDownloadCallbackListener onForceDownloadCallbackListener = this.mDownloadListener;
            if (onForceDownloadCallbackListener != null) {
                onForceDownloadCallbackListener.onDownloadCallback(false);
                return;
            }
            return;
        }
        onDownloadSuccess(str, str2);
    }

    @DexIgnore
    @Override // com.portfolio.platform.cloudimage.UnzippingTaskHelper.OnUnzipFinishListener
    public void onUnzipSuccess(String str, String str2) {
        Wg6.c(str, "zipFilePath");
        Wg6.c(str2, "destinationUnzipPath");
        CloudImageHelper.OnForceDownloadCallbackListener onForceDownloadCallbackListener = this.mDownloadListener;
        if (onForceDownloadCallbackListener != null) {
            onForceDownloadCallbackListener.onDownloadCallback(true);
        }
        returnImageDownloaded(str2);
    }

    @DexIgnore
    public final void returnLogoDownloaded() {
        String str = this.type;
        if (!(str == null || str.length() == 0)) {
            String feature = Constants.Feature.DEVICE.getFeature();
            StringBuilder sb = new StringBuilder();
            sb.append(this.file.getAbsolutePath());
            sb.append('/');
            String str2 = this.serialPrefix;
            if (str2 != null) {
                sb.append(str2);
                sb.append('-');
                String str3 = this.resolution;
                if (str3 != null) {
                    sb.append(str3);
                    sb.append('-');
                    sb.append(feature);
                    Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new CloudImageRunnable$returnLogoDownloaded$Anon1(this, sb.toString(), null), 3, null);
                    return;
                }
                Wg6.n("resolution");
                throw null;
            }
            Wg6.n("serialPrefix");
            throw null;
        }
    }

    @DexIgnore
    public void run() {
        startDownloadingAssets();
    }
}
