package com.portfolio.platform.cloudimage;

import com.mapped.Wg6;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AssetUtil {
    @DexIgnore
    public static /* final */ AssetUtil INSTANCE; // = new AssetUtil();
    @DexIgnore
    public static /* final */ String TAG; // = "AssetUtil";

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x01b1  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x01e1  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x027b  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x02e0  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object checkAssetExist(java.io.File r27, java.lang.String r28, java.lang.String r29, java.lang.String r30, java.lang.String r31, com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener r32, com.mapped.Xe6<? super java.lang.Boolean> r33) {
        /*
        // Method dump skipped, instructions count: 779
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.cloudimage.AssetUtil.checkAssetExist(java.io.File, java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final boolean checkFileExist(String str) {
        Wg6.c(str, "filePath");
        return new File(str).exists();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00d7  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x01c3  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x024d  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x02a7  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object checkWearOSAssetExist(java.io.File r25, java.lang.String r26, java.lang.String r27, com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener r28, com.mapped.Xe6<? super java.lang.Boolean> r29) {
        /*
        // Method dump skipped, instructions count: 722
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.cloudimage.AssetUtil.checkWearOSAssetExist(java.io.File, java.lang.String, java.lang.String, com.portfolio.platform.cloudimage.CloudImageHelper$OnImageCallbackListener, com.mapped.Xe6):java.lang.Object");
    }
}
