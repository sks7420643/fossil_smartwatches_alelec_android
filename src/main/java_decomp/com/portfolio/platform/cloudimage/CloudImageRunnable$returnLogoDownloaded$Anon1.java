package com.portfolio.platform.cloudimage;

import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Jx7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.cloudimage.CloudImageHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1", f = "CloudImageRunnable.kt", l = {183, 186, 189}, m = "invokeSuspend")
public final class CloudImageRunnable$returnLogoDownloaded$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $destinationUnzippedPath;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageRunnable this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1$1", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $filePath1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnLogoDownloaded$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = cloudImageRunnable$returnLogoDownloaded$Anon1;
            this.$filePath1 = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, this.$filePath1, xe6);
            anon1_Level2.p$ = (Il6) obj;
            throw null;
            //return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Anon1_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                CloudImageHelper.OnImageCallbackListener onImageCallbackListener = this.this$0.this$0.mListener;
                if (onImageCallbackListener == null) {
                    return null;
                }
                onImageCallbackListener.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), this.$filePath1);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1$2", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $filePath2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnLogoDownloaded$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = cloudImageRunnable$returnLogoDownloaded$Anon1;
            this.$filePath2 = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$filePath2, xe6);
            anon2_Level2.p$ = (Il6) obj;
            throw null;
            //return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Anon2_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                CloudImageHelper.OnImageCallbackListener onImageCallbackListener = this.this$0.this$0.mListener;
                if (onImageCallbackListener == null) {
                    return null;
                }
                onImageCallbackListener.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), this.$filePath2);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.cloudimage.CloudImageRunnable$returnLogoDownloaded$1$3", f = "CloudImageRunnable.kt", l = {}, m = "invokeSuspend")
    public static final class Anon3_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageRunnable$returnLogoDownloaded$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = cloudImageRunnable$returnLogoDownloaded$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon3_Level2 anon3_Level2 = new Anon3_Level2(this.this$0, xe6);
            anon3_Level2.p$ = (Il6) obj;
            throw null;
            //return anon3_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Anon3_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                CloudImageHelper.OnImageCallbackListener onImageCallbackListener = this.this$0.this$0.mListener;
                if (onImageCallbackListener == null) {
                    return null;
                }
                onImageCallbackListener.onImageCallback(CloudImageRunnable.access$getSerialNumber$p(this.this$0.this$0), "");
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageRunnable$returnLogoDownloaded$Anon1(CloudImageRunnable cloudImageRunnable, String str, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = cloudImageRunnable;
        this.$destinationUnzippedPath = str;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        CloudImageRunnable$returnLogoDownloaded$Anon1 cloudImageRunnable$returnLogoDownloaded$Anon1 = new CloudImageRunnable$returnLogoDownloaded$Anon1(this.this$0, this.$destinationUnzippedPath, xe6);
        cloudImageRunnable$returnLogoDownloaded$Anon1.p$ = (Il6) obj;
        throw null;
        //return cloudImageRunnable$returnLogoDownloaded$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((CloudImageRunnable$returnLogoDownloaded$Anon1) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            String str = this.$destinationUnzippedPath + '/' + this.this$0.type + ".webp";
            String str2 = this.$destinationUnzippedPath + '/' + this.this$0.type + ".png";
            if (AssetUtil.INSTANCE.checkFileExist(str)) {
                Jx7 c = Bw7.c();
                Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, str, null);
                this.L$0 = il6;
                this.L$1 = str;
                this.L$2 = str2;
                this.label = 1;
                if (Eu7.g(c, anon1_Level2, this) == d) {
                    return d;
                }
            } else if (AssetUtil.INSTANCE.checkFileExist(str2)) {
                Jx7 c2 = Bw7.c();
                Anon2_Level2 anon2_Level2 = new Anon2_Level2(this, str2, null);
                this.L$0 = il6;
                this.L$1 = str;
                this.L$2 = str2;
                this.label = 2;
                if (Eu7.g(c2, anon2_Level2, this) == d) {
                    return d;
                }
            } else {
                Jx7 c3 = Bw7.c();
                Anon3_Level2 anon3_Level2 = new Anon3_Level2(this, null);
                this.L$0 = il6;
                this.L$1 = str;
                this.L$2 = str2;
                this.label = 3;
                if (Eu7.g(c3, anon3_Level2, this) == d) {
                    return d;
                }
            }
        } else if (i == 1 || i == 2 || i == 3) {
            String str3 = (String) this.L$2;
            String str4 = (String) this.L$1;
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Cd6.a;
    }
}
