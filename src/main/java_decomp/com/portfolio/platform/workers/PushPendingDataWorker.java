package com.portfolio.platform.workers;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.CoroutineWorker;
import androidx.work.WorkerParameters;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dl7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.G11;
import com.fossil.Gu7;
import com.fossil.H11;
import com.fossil.Jc7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.P01;
import com.fossil.S01;
import com.fossil.Tt4;
import com.fossil.Y01;
import com.fossil.Yn7;
import com.fossil.Z01;
import com.mapped.An4;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Jh6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Lk6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PushPendingDataWorker extends CoroutineWorker {
    @DexIgnore
    public static /* final */ a H; // = new a(null);
    @DexIgnore
    public /* final */ FriendRepository A;
    @DexIgnore
    public /* final */ FileRepository B;
    @DexIgnore
    public /* final */ UserRepository C;
    @DexIgnore
    public /* final */ WorkoutSettingRepository D;
    @DexIgnore
    public /* final */ FCMRepository E;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository F;
    @DexIgnore
    public /* final */ PortfolioApp G;
    @DexIgnore
    public /* final */ ActivitiesRepository i;
    @DexIgnore
    public /* final */ SummariesRepository j;
    @DexIgnore
    public /* final */ SleepSessionsRepository k;
    @DexIgnore
    public /* final */ SleepSummariesRepository l;
    @DexIgnore
    public /* final */ GoalTrackingRepository m;
    @DexIgnore
    public /* final */ HeartRateSampleRepository s;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository t;
    @DexIgnore
    public /* final */ FitnessDataRepository u;
    @DexIgnore
    public /* final */ AlarmsRepository v;
    @DexIgnore
    public /* final */ An4 w;
    @DexIgnore
    public /* final */ HybridPresetRepository x;
    @DexIgnore
    public /* final */ ThirdPartyRepository y;
    @DexIgnore
    public /* final */ Tt4 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a() {
            Z01.Ai ai = new Z01.Ai(PushPendingDataWorker.class);
            P01.Ai ai2 = new P01.Ai();
            ai2.b(Y01.CONNECTED);
            P01 a2 = ai2.a();
            Wg6.b(a2, "Constraints.Builder().se\u2026rkType.CONNECTED).build()");
            ai.e(a2);
            H11 b = ai.b();
            Wg6.b(b, "uploadBuilder.build()");
            Z01 z01 = (Z01) b;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PushPendingDataWorker", "startScheduleUploadPendingData() - id = " + z01.a());
            G11.e(PortfolioApp.get.instance()).c("PushPendingDataWorker", S01.KEEP, z01);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Jc7<PushPendingDataWorker> {
        @DexIgnore
        public /* final */ Provider<ActivitiesRepository> a;
        @DexIgnore
        public /* final */ Provider<SummariesRepository> b;
        @DexIgnore
        public /* final */ Provider<SleepSessionsRepository> c;
        @DexIgnore
        public /* final */ Provider<SleepSummariesRepository> d;
        @DexIgnore
        public /* final */ Provider<GoalTrackingRepository> e;
        @DexIgnore
        public /* final */ Provider<HeartRateSampleRepository> f;
        @DexIgnore
        public /* final */ Provider<HeartRateSummaryRepository> g;
        @DexIgnore
        public /* final */ Provider<FitnessDataRepository> h;
        @DexIgnore
        public /* final */ Provider<AlarmsRepository> i;
        @DexIgnore
        public /* final */ Provider<An4> j;
        @DexIgnore
        public /* final */ Provider<HybridPresetRepository> k;
        @DexIgnore
        public /* final */ Provider<ThirdPartyRepository> l;
        @DexIgnore
        public /* final */ Provider<Tt4> m;
        @DexIgnore
        public /* final */ Provider<FriendRepository> n;
        @DexIgnore
        public /* final */ Provider<FileRepository> o;
        @DexIgnore
        public /* final */ Provider<UserRepository> p;
        @DexIgnore
        public /* final */ Provider<WorkoutSettingRepository> q;
        @DexIgnore
        public /* final */ Provider<FCMRepository> r;
        @DexIgnore
        public /* final */ Provider<DianaWatchFaceRepository> s;
        @DexIgnore
        public /* final */ Provider<PortfolioApp> t;

        @DexIgnore
        public b(Provider<ActivitiesRepository> provider, Provider<SummariesRepository> provider2, Provider<SleepSessionsRepository> provider3, Provider<SleepSummariesRepository> provider4, Provider<GoalTrackingRepository> provider5, Provider<HeartRateSampleRepository> provider6, Provider<HeartRateSummaryRepository> provider7, Provider<FitnessDataRepository> provider8, Provider<AlarmsRepository> provider9, Provider<An4> provider10, Provider<HybridPresetRepository> provider11, Provider<ThirdPartyRepository> provider12, Provider<Tt4> provider13, Provider<FriendRepository> provider14, Provider<FileRepository> provider15, Provider<UserRepository> provider16, Provider<WorkoutSettingRepository> provider17, Provider<FCMRepository> provider18, Provider<DianaWatchFaceRepository> provider19, Provider<PortfolioApp> provider20) {
            Wg6.c(provider, "mActivitiesRepository");
            Wg6.c(provider2, "mSummariesRepository");
            Wg6.c(provider3, "mSleepSessionRepository");
            Wg6.c(provider4, "mSleepSummariesRepository");
            Wg6.c(provider5, "mGoalTrackingRepository");
            Wg6.c(provider6, "mHeartRateSampleRepository");
            Wg6.c(provider7, "mHeartRateSummaryRepository");
            Wg6.c(provider8, "mFitnessDataRepository");
            Wg6.c(provider9, "mAlarmsRepository");
            Wg6.c(provider10, "mSharedPreferencesManager");
            Wg6.c(provider11, "mHybridPresetRepository");
            Wg6.c(provider12, "mThirdPartyRepository");
            Wg6.c(provider13, "mChallengeRepository");
            Wg6.c(provider14, "friendRepository");
            Wg6.c(provider15, "mFileRepository");
            Wg6.c(provider16, "mUserRepository");
            Wg6.c(provider17, "mWorkoutSettingRepository");
            Wg6.c(provider18, "fcmRepository");
            Wg6.c(provider19, "mDianaWatchFaceRepository");
            Wg6.c(provider20, "mApp");
            this.a = provider;
            this.b = provider2;
            this.c = provider3;
            this.d = provider4;
            this.e = provider5;
            this.f = provider6;
            this.g = provider7;
            this.h = provider8;
            this.i = provider9;
            this.j = provider10;
            this.k = provider11;
            this.l = provider12;
            this.m = provider13;
            this.n = provider14;
            this.o = provider15;
            this.p = provider16;
            this.q = provider17;
            this.r = provider18;
            this.s = provider19;
            this.t = provider20;
        }

        @DexIgnore
        /* Return type fixed from 'androidx.work.ListenableWorker' to match base method */
        @Override // com.fossil.Jc7
        public /* bridge */ /* synthetic */ PushPendingDataWorker a(Context context, WorkerParameters workerParameters) {
            return b(context, workerParameters);
        }

        @DexIgnore
        public PushPendingDataWorker b(Context context, WorkerParameters workerParameters) {
            Wg6.c(context, "context");
            Wg6.c(workerParameters, "parameterName");
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "Factory - create()");
            ActivitiesRepository activitiesRepository = this.a.get();
            Wg6.b(activitiesRepository, "mActivitiesRepository.get()");
            SummariesRepository summariesRepository = this.b.get();
            Wg6.b(summariesRepository, "mSummariesRepository.get()");
            SleepSessionsRepository sleepSessionsRepository = this.c.get();
            Wg6.b(sleepSessionsRepository, "mSleepSessionRepository.get()");
            SleepSummariesRepository sleepSummariesRepository = this.d.get();
            Wg6.b(sleepSummariesRepository, "mSleepSummariesRepository.get()");
            GoalTrackingRepository goalTrackingRepository = this.e.get();
            Wg6.b(goalTrackingRepository, "mGoalTrackingRepository.get()");
            HeartRateSampleRepository heartRateSampleRepository = this.f.get();
            Wg6.b(heartRateSampleRepository, "mHeartRateSampleRepository.get()");
            HeartRateSummaryRepository heartRateSummaryRepository = this.g.get();
            Wg6.b(heartRateSummaryRepository, "mHeartRateSummaryRepository.get()");
            FitnessDataRepository fitnessDataRepository = this.h.get();
            Wg6.b(fitnessDataRepository, "mFitnessDataRepository.get()");
            AlarmsRepository alarmsRepository = this.i.get();
            Wg6.b(alarmsRepository, "mAlarmsRepository.get()");
            An4 an4 = this.j.get();
            Wg6.b(an4, "mSharedPreferencesManager.get()");
            HybridPresetRepository hybridPresetRepository = this.k.get();
            Wg6.b(hybridPresetRepository, "mHybridPresetRepository.get()");
            ThirdPartyRepository thirdPartyRepository = this.l.get();
            Wg6.b(thirdPartyRepository, "mThirdPartyRepository.get()");
            Tt4 tt4 = this.m.get();
            Wg6.b(tt4, "mChallengeRepository.get()");
            FriendRepository friendRepository = this.n.get();
            Wg6.b(friendRepository, "friendRepository.get()");
            FileRepository fileRepository = this.o.get();
            Wg6.b(fileRepository, "mFileRepository.get()");
            UserRepository userRepository = this.p.get();
            Wg6.b(userRepository, "mUserRepository.get()");
            WorkoutSettingRepository workoutSettingRepository = this.q.get();
            Wg6.b(workoutSettingRepository, "mWorkoutSettingRepository.get()");
            FCMRepository fCMRepository = this.r.get();
            Wg6.b(fCMRepository, "fcmRepository.get()");
            DianaWatchFaceRepository dianaWatchFaceRepository = this.s.get();
            Wg6.b(dianaWatchFaceRepository, "mDianaWatchFaceRepository.get()");
            PortfolioApp portfolioApp = this.t.get();
            Wg6.b(portfolioApp, "mApp.get()");
            return new PushPendingDataWorker(workerParameters, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, goalTrackingRepository, heartRateSampleRepository, heartRateSummaryRepository, fitnessDataRepository, alarmsRepository, an4, hybridPresetRepository, thirdPartyRepository, tt4, friendRepository, fileRepository, userRepository, workoutSettingRepository, fCMRepository, dianaWatchFaceRepository, portfolioApp);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.workers.PushPendingDataWorker", f = "PushPendingDataWorker.kt", l = {96}, m = "doWork")
    public static final class c extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(PushPendingDataWorker pushPendingDataWorker, Xe6 xe6) {
            super(xe6);
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Lk6 $cancellableContinuation;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements ActivitiesRepository.PushPendingActivitiesCallback {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Jh6 $endDate;
                @DexIgnore
                public /* final */ /* synthetic */ Jh6 $startDate;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(a aVar, Jh6 jh6, Jh6 jh62, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                    this.$startDate = jh6;
                    this.$endDate = jh62;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    a aVar = new a(this.this$0, this.$startDate, this.$endDate, xe6);
                    aVar.p$ = (Il6) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        SummariesRepository summariesRepository = this.this$0.a.this$0.j;
                        Date date = this.$startDate.element.toLocalDateTime().toDate();
                        Wg6.b(date, "startDate.toLocalDateTime().toDate()");
                        Date date2 = this.$endDate.element.toLocalDateTime().toDate();
                        Wg6.b(date2, "endDate.toLocalDateTime().toDate()");
                        this.L$0 = il6;
                        this.label = 1;
                        if (summariesRepository.loadSummaries(date, date2, this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.ActivitiesRepository.PushPendingActivitiesCallback
            public void onFail(int i) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("PushPendingDataWorker", "pushPendingActivities onFail, go to next, errorCode = " + i);
                if (this.a.$cancellableContinuation.isActive()) {
                    Lk6 lk6 = this.a.$cancellableContinuation;
                    Boolean bool = Boolean.FALSE;
                    Dl7.Ai ai = Dl7.Companion;
                    lk6.resumeWith(Dl7.constructor-impl(bool));
                }
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.ActivitiesRepository.PushPendingActivitiesCallback
            public void onSuccess(List<ActivitySample> list) {
                Wg6.c(list, "activityList");
                FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "pushPendingActivities onSuccess, go to next");
                if (!list.isEmpty()) {
                    Jh6 jh6 = new Jh6();
                    jh6.element = (T) list.get(0).getStartTime();
                    Jh6 jh62 = new Jh6();
                    jh62.element = (T) list.get(0).getEndTime();
                    for (ActivitySample activitySample : list) {
                        if (activitySample.getStartTime().getMillis() < jh6.element.getMillis()) {
                            jh6.element = (T) activitySample.getStartTime();
                        }
                        if (activitySample.getEndTime().getMillis() < jh62.element.getMillis()) {
                            jh62.element = (T) activitySample.getEndTime();
                        }
                    }
                    Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new a(this, jh6, jh62, null), 3, null);
                }
                if (this.a.$cancellableContinuation.isActive()) {
                    Lk6 lk6 = this.a.$cancellableContinuation;
                    Boolean bool = Boolean.TRUE;
                    Dl7.Ai ai = Dl7.Companion;
                    lk6.resumeWith(Dl7.constructor-impl(bool));
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(Lk6 lk6, Xe6 xe6, PushPendingDataWorker pushPendingDataWorker) {
            super(2, xe6);
            this.$cancellableContinuation = lk6;
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            d dVar = new d(this.$cancellableContinuation, xe6, this.this$0);
            dVar.p$ = (Il6) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((d) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ActivitiesRepository activitiesRepository = this.this$0.i;
                a aVar = new a(this);
                this.L$0 = il6;
                this.label = 1;
                if (activitiesRepository.pushPendingActivities(aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Lk6 $cancellableContinuation;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements SleepSessionsRepository.PushPendingSleepSessionsCallback {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $sleepSessionList;
                @DexIgnore
                public int I$0;
                @DexIgnore
                public int I$1;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<Ku3>>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ Calendar $end;
                    @DexIgnore
                    public /* final */ /* synthetic */ Calendar $start;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public a(a aVar, Calendar calendar, Calendar calendar2, Xe6 xe6) {
                        super(2, xe6);
                        this.this$0 = aVar;
                        this.$start = calendar;
                        this.$end = calendar2;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        a aVar = new a(this.this$0, this.$start, this.$end, xe6);
                        aVar.p$ = (Il6) obj;
                        throw null;
                        //return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super Ap4<Ku3>> xe6) {
                        throw null;
                        //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = Yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            El7.b(obj);
                            Il6 il6 = this.p$;
                            SleepSummariesRepository sleepSummariesRepository = this.this$0.this$0.a.this$0.l;
                            Calendar calendar = this.$start;
                            Wg6.b(calendar, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                            Date time = calendar.getTime();
                            Wg6.b(time, "start.time");
                            Calendar calendar2 = this.$end;
                            Wg6.b(calendar2, "end");
                            Date time2 = calendar2.getTime();
                            Wg6.b(time2, "end.time");
                            this.L$0 = il6;
                            this.label = 1;
                            Object fetchSleepSummaries = sleepSummariesRepository.fetchSleepSummaries(time, time2, this);
                            return fetchSleepSummaries == d ? d : fetchSleepSummaries;
                        } else if (i == 1) {
                            Il6 il62 = (Il6) this.L$0;
                            El7.b(obj);
                            return obj;
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(a aVar, List list, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                    this.$sleepSessionList = list;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    a aVar = new a(this.this$0, this.$sleepSessionList, xe6);
                    aVar.p$ = (Il6) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        if (!this.$sleepSessionList.isEmpty()) {
                            int realStartTime = ((MFSleepSession) this.$sleepSessionList.get(0)).getRealStartTime();
                            int realEndTime = ((MFSleepSession) this.$sleepSessionList.get(0)).getRealEndTime();
                            int i2 = realEndTime;
                            for (MFSleepSession mFSleepSession : this.$sleepSessionList) {
                                if (mFSleepSession.getRealStartTime() < realStartTime) {
                                    realStartTime = mFSleepSession.getRealStartTime();
                                }
                                if (mFSleepSession.getRealEndTime() > i2) {
                                    i2 = mFSleepSession.getRealEndTime();
                                }
                            }
                            Calendar instance = Calendar.getInstance();
                            Wg6.b(instance, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                            instance.setTimeInMillis((long) realStartTime);
                            Calendar instance2 = Calendar.getInstance();
                            Wg6.b(instance2, "end");
                            instance2.setTimeInMillis((long) i2);
                            Dv7 b = Bw7.b();
                            a aVar = new a(this, instance, instance2, null);
                            this.L$0 = il6;
                            this.I$0 = realStartTime;
                            this.I$1 = i2;
                            this.L$1 = instance;
                            this.L$2 = instance2;
                            this.label = 1;
                            if (Eu7.g(b, aVar, this) == d) {
                                return d;
                            }
                        }
                    } else if (i == 1) {
                        Calendar calendar = (Calendar) this.L$2;
                        Calendar calendar2 = (Calendar) this.L$1;
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (this.this$0.a.$cancellableContinuation.isActive()) {
                        Lk6 lk6 = this.this$0.a.$cancellableContinuation;
                        Boolean a2 = Ao7.a(true);
                        Dl7.Ai ai = Dl7.Companion;
                        lk6.resumeWith(Dl7.constructor-impl(a2));
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            public a(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback
            public void onFail(int i) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("PushPendingDataWorker", "pushPendingSleepSessions onFail, go to next, errorCode = " + i);
                if (this.a.$cancellableContinuation.isActive()) {
                    Lk6 lk6 = this.a.$cancellableContinuation;
                    Boolean bool = Boolean.TRUE;
                    Dl7.Ai ai = Dl7.Companion;
                    lk6.resumeWith(Dl7.constructor-impl(bool));
                }
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.SleepSessionsRepository.PushPendingSleepSessionsCallback
            public void onSuccess(List<MFSleepSession> list) {
                Wg6.c(list, "sleepSessionList");
                FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "pushPendingSleepSessions onSuccess, go to next");
                Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new a(this, list, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(Lk6 lk6, Xe6 xe6, PushPendingDataWorker pushPendingDataWorker) {
            super(2, xe6);
            this.$cancellableContinuation = lk6;
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            e eVar = new e(this.$cancellableContinuation, xe6, this.this$0);
            eVar.p$ = (Il6) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((e) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                SleepSessionsRepository sleepSessionsRepository = this.this$0.k;
                a aVar = new a(this);
                this.L$0 = il6;
                this.label = 1;
                if (sleepSessionsRepository.pushPendingSleepSessions(aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Lk6 $cancellableContinuation;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements GoalTrackingRepository.PushPendingGoalTrackingDataListCallback {
            @DexIgnore
            public /* final */ /* synthetic */ f a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $goalTrackingList;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<ApiResponse<GoalDailySummary>>>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ Jh6 $endDate;
                    @DexIgnore
                    public /* final */ /* synthetic */ Jh6 $startDate;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public a(a aVar, Jh6 jh6, Jh6 jh62, Xe6 xe6) {
                        super(2, xe6);
                        this.this$0 = aVar;
                        this.$startDate = jh6;
                        this.$endDate = jh62;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        a aVar = new a(this.this$0, this.$startDate, this.$endDate, xe6);
                        aVar.p$ = (Il6) obj;
                        throw null;
                        //return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super Ap4<ApiResponse<GoalDailySummary>>> xe6) {
                        throw null;
                        //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = Yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            El7.b(obj);
                            this.L$0 = this.p$;
                            this.label = 1;
                            Object loadSummaries = this.this$0.this$0.a.this$0.m.loadSummaries(this.$startDate.element, this.$endDate.element, this);
                            return loadSummaries == d ? d : loadSummaries;
                        } else if (i == 1) {
                            Il6 il6 = (Il6) this.L$0;
                            El7.b(obj);
                            return obj;
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(a aVar, List list, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                    this.$goalTrackingList = list;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    a aVar = new a(this.this$0, this.$goalTrackingList, xe6);
                    aVar.p$ = (Il6) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Jh6 jh6 = new Jh6();
                        jh6.element = (T) ((GoalTrackingData) this.$goalTrackingList.get(0)).getDate();
                        Jh6 jh62 = new Jh6();
                        jh62.element = (T) ((GoalTrackingData) this.$goalTrackingList.get(0)).getDate();
                        for (GoalTrackingData goalTrackingData : this.$goalTrackingList) {
                            if (goalTrackingData.getDate().getTime() < jh6.element.getTime()) {
                                jh6.element = (T) goalTrackingData.getDate();
                            }
                            if (goalTrackingData.getDate().getTime() > jh62.element.getTime()) {
                                jh62.element = (T) goalTrackingData.getDate();
                            }
                        }
                        Dv7 b = Bw7.b();
                        a aVar = new a(this, jh6, jh62, null);
                        this.L$0 = il6;
                        this.L$1 = jh6;
                        this.L$2 = jh62;
                        this.label = 1;
                        if (Eu7.g(b, aVar, this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Jh6 jh63 = (Jh6) this.L$2;
                        Jh6 jh64 = (Jh6) this.L$1;
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (this.this$0.a.$cancellableContinuation.isActive()) {
                        Lk6 lk6 = this.this$0.a.$cancellableContinuation;
                        Boolean a2 = Ao7.a(true);
                        Dl7.Ai ai = Dl7.Companion;
                        lk6.resumeWith(Dl7.constructor-impl(a2));
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore
            public a(f fVar) {
                this.a = fVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback
            public void onFail(int i) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("PushPendingDataWorker", "pushPendingGoalTrackingDataList onFail, go to next, errorCode = " + i);
                if (this.a.$cancellableContinuation.isActive()) {
                    Lk6 lk6 = this.a.$cancellableContinuation;
                    Boolean bool = Boolean.FALSE;
                    Dl7.Ai ai = Dl7.Companion;
                    lk6.resumeWith(Dl7.constructor-impl(bool));
                }
            }

            @DexIgnore
            @Override // com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback
            public void onSuccess(List<GoalTrackingData> list) {
                Wg6.c(list, "goalTrackingList");
                FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "pushPendingGoalTrackingDataList onSuccess, go to next");
                Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new a(this, list, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(Lk6 lk6, Xe6 xe6, PushPendingDataWorker pushPendingDataWorker) {
            super(2, xe6);
            this.$cancellableContinuation = lk6;
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            f fVar = new f(this.$cancellableContinuation, xe6, this.this$0);
            fVar.p$ = (Il6) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((f) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                GoalTrackingRepository goalTrackingRepository = this.this$0.m;
                a aVar = new a(this);
                this.L$0 = il6;
                this.label = 1;
                if (goalTrackingRepository.pushPendingGoalTrackingDataList(aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.workers.PushPendingDataWorker", f = "PushPendingDataWorker.kt", l = {106, 290, 298, 184, 185, 187, FacebookRequestErrorClassification.EC_INVALID_TOKEN, 192, 226, 229, 306, 263, 265, 271, 272}, m = VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE)
    public static final class g extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(PushPendingDataWorker pushPendingDataWorker, Xe6 xe6) {
            super(xe6);
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.z(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.workers.PushPendingDataWorker$start$4", f = "PushPendingDataWorker.kt", l = {208, 209, 211, 212, 214, 215, 216}, m = "invokeSuspend")
    public static final class h extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $startDate;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(PushPendingDataWorker pushPendingDataWorker, Jh6 jh6, Jh6 jh62, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = pushPendingDataWorker;
            this.$startDate = jh6;
            this.$endDate = jh62;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            h hVar = new h(this.this$0, this.$startDate, this.$endDate, xe6);
            hVar.p$ = (Il6) obj;
            throw null;
            //return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((h) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0070  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00ab  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00e5  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0121  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x015b  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0197  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x019a  */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x0037  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
            // Method dump skipped, instructions count: 436
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.workers.PushPendingDataWorker.h.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements ThirdPartyRepository.PushPendingThirdPartyDataCallback {
        @DexIgnore
        @Override // com.portfolio.platform.data.source.ThirdPartyRepository.PushPendingThirdPartyDataCallback
        public void onComplete() {
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "pushThirdPartyPendingData - Complete");
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public PushPendingDataWorker(androidx.work.WorkerParameters r4, com.portfolio.platform.data.source.ActivitiesRepository r5, com.portfolio.platform.data.source.SummariesRepository r6, com.portfolio.platform.data.source.SleepSessionsRepository r7, com.portfolio.platform.data.source.SleepSummariesRepository r8, com.portfolio.platform.data.source.GoalTrackingRepository r9, com.portfolio.platform.data.source.HeartRateSampleRepository r10, com.portfolio.platform.data.source.HeartRateSummaryRepository r11, com.portfolio.platform.data.source.FitnessDataRepository r12, com.portfolio.platform.data.source.AlarmsRepository r13, com.mapped.An4 r14, com.portfolio.platform.data.source.HybridPresetRepository r15, com.portfolio.platform.data.source.ThirdPartyRepository r16, com.fossil.Tt4 r17, com.portfolio.platform.buddy_challenge.domain.FriendRepository r18, com.portfolio.platform.data.source.FileRepository r19, com.portfolio.platform.data.source.UserRepository r20, com.portfolio.platform.data.source.WorkoutSettingRepository r21, com.portfolio.platform.buddy_challenge.domain.FCMRepository r22, com.portfolio.platform.data.source.DianaWatchFaceRepository r23, com.portfolio.platform.PortfolioApp r24) {
        /*
            r3 = this;
            java.lang.String r1 = "mWorkerParameters"
            com.mapped.Wg6.c(r4, r1)
            java.lang.String r1 = "mActivitiesRepository"
            com.mapped.Wg6.c(r5, r1)
            java.lang.String r1 = "mSummariesRepository"
            com.mapped.Wg6.c(r6, r1)
            java.lang.String r1 = "mSleepSessionRepository"
            com.mapped.Wg6.c(r7, r1)
            java.lang.String r1 = "mSleepSummariesRepository"
            com.mapped.Wg6.c(r8, r1)
            java.lang.String r1 = "mGoalTrackingRepository"
            com.mapped.Wg6.c(r9, r1)
            java.lang.String r1 = "mHeartRateSampleRepository"
            com.mapped.Wg6.c(r10, r1)
            java.lang.String r1 = "mHeartRateSummaryRepository"
            com.mapped.Wg6.c(r11, r1)
            java.lang.String r1 = "mFitnessDataRepository"
            com.mapped.Wg6.c(r12, r1)
            java.lang.String r1 = "mAlarmsRepository"
            com.mapped.Wg6.c(r13, r1)
            java.lang.String r1 = "mSharedPreferencesManager"
            com.mapped.Wg6.c(r14, r1)
            java.lang.String r1 = "mHybridPresetRepository"
            com.mapped.Wg6.c(r15, r1)
            java.lang.String r1 = "mThirdPartyRepository"
            r0 = r16
            com.mapped.Wg6.c(r0, r1)
            java.lang.String r1 = "mChallengeRepository"
            r0 = r17
            com.mapped.Wg6.c(r0, r1)
            java.lang.String r1 = "friendRepository"
            r0 = r18
            com.mapped.Wg6.c(r0, r1)
            java.lang.String r1 = "mFileRepository"
            r0 = r19
            com.mapped.Wg6.c(r0, r1)
            java.lang.String r1 = "mUserRepository"
            r0 = r20
            com.mapped.Wg6.c(r0, r1)
            java.lang.String r1 = "mWorkoutSettingRepository"
            r0 = r21
            com.mapped.Wg6.c(r0, r1)
            java.lang.String r1 = "fcmRepository"
            r0 = r22
            com.mapped.Wg6.c(r0, r1)
            java.lang.String r1 = "mDianaWatchFaceRepository"
            r0 = r23
            com.mapped.Wg6.c(r0, r1)
            java.lang.String r1 = "mApp"
            r0 = r24
            com.mapped.Wg6.c(r0, r1)
            android.content.Context r1 = r24.getApplicationContext()
            java.lang.String r2 = "mApp.applicationContext"
            com.mapped.Wg6.b(r1, r2)
            r3.<init>(r1, r4)
            r3.i = r5
            r3.j = r6
            r3.k = r7
            r3.l = r8
            r3.m = r9
            r3.s = r10
            r3.t = r11
            r3.u = r12
            r3.v = r13
            r3.w = r14
            r3.x = r15
            r0 = r16
            r3.y = r0
            r0 = r17
            r3.z = r0
            r0 = r18
            r3.A = r0
            r0 = r19
            r3.B = r0
            r0 = r20
            r3.C = r0
            r0 = r21
            r3.D = r0
            r0 = r22
            r3.E = r0
            r0 = r23
            r3.F = r0
            r0 = r24
            r3.G = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.workers.PushPendingDataWorker.<init>(androidx.work.WorkerParameters, com.portfolio.platform.data.source.ActivitiesRepository, com.portfolio.platform.data.source.SummariesRepository, com.portfolio.platform.data.source.SleepSessionsRepository, com.portfolio.platform.data.source.SleepSummariesRepository, com.portfolio.platform.data.source.GoalTrackingRepository, com.portfolio.platform.data.source.HeartRateSampleRepository, com.portfolio.platform.data.source.HeartRateSummaryRepository, com.portfolio.platform.data.source.FitnessDataRepository, com.portfolio.platform.data.source.AlarmsRepository, com.mapped.An4, com.portfolio.platform.data.source.HybridPresetRepository, com.portfolio.platform.data.source.ThirdPartyRepository, com.fossil.Tt4, com.portfolio.platform.buddy_challenge.domain.FriendRepository, com.portfolio.platform.data.source.FileRepository, com.portfolio.platform.data.source.UserRepository, com.portfolio.platform.data.source.WorkoutSettingRepository, com.portfolio.platform.buddy_challenge.domain.FCMRepository, com.portfolio.platform.data.source.DianaWatchFaceRepository, com.portfolio.platform.PortfolioApp):void");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    @Override // androidx.work.CoroutineWorker
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object o(com.mapped.Xe6<? super androidx.work.ListenableWorker.a> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.workers.PushPendingDataWorker.c
            if (r0 == 0) goto L_0x0030
            r0 = r7
            com.portfolio.platform.workers.PushPendingDataWorker$c r0 = (com.portfolio.platform.workers.PushPendingDataWorker.c) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0030
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x003e
            if (r3 != r5) goto L_0x0036
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.workers.PushPendingDataWorker r0 = (com.portfolio.platform.workers.PushPendingDataWorker) r0
            com.fossil.El7.b(r2)
        L_0x0026:
            androidx.work.ListenableWorker$a r0 = androidx.work.ListenableWorker.a.c()
            java.lang.String r1 = "Result.success()"
            com.mapped.Wg6.b(r0, r1)
        L_0x002f:
            return r0
        L_0x0030:
            com.portfolio.platform.workers.PushPendingDataWorker$c r0 = new com.portfolio.platform.workers.PushPendingDataWorker$c
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x0036:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003e:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "doWork() - id = "
            r3.append(r4)
            java.util.UUID r4 = r6.d()
            r3.append(r4)
            java.lang.String r4 = "PushPendingDataWorker"
            java.lang.String r3 = r3.toString()
            r2.d(r4, r3)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r0 = r6.z(r0)
            if (r0 != r1) goto L_0x0026
            r0 = r1
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.workers.PushPendingDataWorker.o(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:102:0x041c  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x041f  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0437  */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x047c  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0485  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0489  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x048c  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x005a  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x013b  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0174  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01f4  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0221  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0224  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0240  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x024d  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0272  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0275  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0299  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x029c  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x02c0  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x02c3  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0310  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0315  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0318  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0332  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0336  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0381  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object z(com.mapped.Xe6<? super com.mapped.Cd6> r21) {
        /*
        // Method dump skipped, instructions count: 1206
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.workers.PushPendingDataWorker.z(com.mapped.Xe6):java.lang.Object");
    }
}
