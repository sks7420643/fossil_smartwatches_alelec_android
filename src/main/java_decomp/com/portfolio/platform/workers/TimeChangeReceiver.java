package com.portfolio.platform.workers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.service.microapp.CommuteTimeService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TimeChangeReceiver extends BroadcastReceiver {
    @DexIgnore
    public AlarmHelper a;
    @DexIgnore
    public An4 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ TimeChangeReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Xe6 xe6, TimeChangeReceiver timeChangeReceiver) {
            super(2, xe6);
            this.this$0 = timeChangeReceiver;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            a aVar = new a(xe6, this.this$0);
            aVar.p$ = (Il6) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                String J = PortfolioApp.get.instance().J();
                if (J.length() == 0) {
                    return Cd6.a;
                }
                long C = this.this$0.b().C(J);
                long A = this.this$0.b().A(J);
                long currentTimeMillis = System.currentTimeMillis();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("TimeChangeReceiver", "currentTime = " + currentTimeMillis + "; lastSyncTimeSuccess = " + C + "; lastReminderSyncTimeSuccess = " + A);
                if (A > currentTimeMillis || C > currentTimeMillis) {
                    An4 b = this.this$0.b();
                    long j = currentTimeMillis - ((long) CommuteTimeService.A);
                    b.A1(j, J);
                    this.this$0.b().y1(j, J);
                    return Cd6.a;
                }
                AlarmHelper a2 = this.this$0.a();
                Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                Wg6.b(applicationContext, "PortfolioApp.instance.applicationContext");
                a2.h(applicationContext);
                AlarmHelper a3 = this.this$0.a();
                Context applicationContext2 = PortfolioApp.get.instance().getApplicationContext();
                Wg6.b(applicationContext2, "PortfolioApp.instance.applicationContext");
                a3.g(applicationContext2);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public TimeChangeReceiver() {
        PortfolioApp.get.instance().getIface().J0(this);
    }

    @DexIgnore
    public final AlarmHelper a() {
        AlarmHelper alarmHelper = this.a;
        if (alarmHelper != null) {
            return alarmHelper;
        }
        Wg6.n("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public final An4 b() {
        An4 an4 = this.b;
        if (an4 != null) {
            return an4;
        }
        Wg6.n("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action;
        FLogger.INSTANCE.getLocal().d("TimeChangeReceiver", "onReceive()");
        if (intent != null && (action = intent.getAction()) != null) {
            if (Wg6.a(action, "android.intent.action.TIME_SET") || Wg6.a(action, "android.intent.action.TIMEZONE_CHANGED") || Wg6.a(action, "android.intent.action.TIME_TICK")) {
                Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new a(null, this), 3, null);
            }
        }
    }
}
