package com.portfolio.platform.data;

import android.text.TextUtils;
import com.fossil.Um5;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum NotificationType {
    CONTACT(2131887497),
    CODE_WORD(2131887496),
    CALL(2131887265),
    SMS(2131887266),
    EMAIL(2131887396),
    APP_FILTER(2131887495),
    FITNESS_GOAL_ACHIEVED(2131887498),
    APP_MODE(2131887493),
    OTHER(2131887491),
    CONTACT_EMPTY(2131887494),
    APP_EMPTY(2131887492);
    
    @DexIgnore
    public /* final */ int sectionTitleResId;

    @DexIgnore
    public NotificationType(int i) {
        this.sectionTitleResId = i;
    }

    @DexIgnore
    public static NotificationType find(String str) {
        if (!TextUtils.isEmpty(str)) {
            return valueOf(str);
        }
        return null;
    }

    @DexIgnore
    public int getSectionTitleResId() {
        return this.sectionTitleResId;
    }

    @DexIgnore
    public String getSectionTitleString() {
        return Um5.c(PortfolioApp.d0, this.sectionTitleResId);
    }
}
