package com.portfolio.platform.data;

import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WechatAccessToken {
    @DexIgnore
    @Vu3("access_token")
    public /* final */ String mAccessToken;
    @DexIgnore
    @Vu3("expires_in")
    public /* final */ int mExpiresIn;
    @DexIgnore
    @Vu3("openid")
    public /* final */ String mOpenId;
    @DexIgnore
    @Vu3("refresh_token")
    public /* final */ String mRefreshToken;
    @DexIgnore
    @Vu3("scope")
    public /* final */ String mScope;

    @DexIgnore
    public WechatAccessToken(String str, int i, String str2, String str3, String str4) {
        Wg6.c(str, "mAccessToken");
        Wg6.c(str2, "mRefreshToken");
        Wg6.c(str3, "mOpenId");
        Wg6.c(str4, "mScope");
        this.mAccessToken = str;
        this.mExpiresIn = i;
        this.mRefreshToken = str2;
        this.mOpenId = str3;
        this.mScope = str4;
    }

    @DexIgnore
    public final String getAccessToken() {
        return this.mAccessToken;
    }

    @DexIgnore
    public final int getExpiresIn() {
        return this.mExpiresIn;
    }

    @DexIgnore
    public final String getOpenId() {
        return this.mOpenId;
    }

    @DexIgnore
    public final String getRefreshToken() {
        return this.mRefreshToken;
    }

    @DexIgnore
    public final String getScope() {
        return this.mScope;
    }
}
