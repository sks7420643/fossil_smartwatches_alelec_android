package com.portfolio.platform.data.model.room.microapp;

import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeclarationFile {
    @DexIgnore
    public String appId;
    @DexIgnore
    @Vu3("content")
    public /* final */ String content;
    @DexIgnore
    @Vu3("description")
    public /* final */ String description;
    @DexIgnore
    @Vu3("id")
    public /* final */ String id;
    @DexIgnore
    public String serialNumber;
    @DexIgnore
    public String variantName;

    @DexIgnore
    public DeclarationFile(String str, String str2, String str3) {
        Wg6.c(str, "id");
        this.id = str;
        this.description = str2;
        this.content = str3;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ DeclarationFile(String str, String str2, String str3, int i, Qg6 qg6) {
        this(str, (i & 2) != 0 ? "" : str2, (i & 4) != 0 ? "" : str3);
    }

    @DexIgnore
    public static /* synthetic */ DeclarationFile copy$default(DeclarationFile declarationFile, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = declarationFile.id;
        }
        if ((i & 2) != 0) {
            str2 = declarationFile.description;
        }
        if ((i & 4) != 0) {
            str3 = declarationFile.content;
        }
        return declarationFile.copy(str, str2, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.description;
    }

    @DexIgnore
    public final String component3() {
        return this.content;
    }

    @DexIgnore
    public final DeclarationFile copy(String str, String str2, String str3) {
        Wg6.c(str, "id");
        return new DeclarationFile(str, str2, str3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DeclarationFile) {
                DeclarationFile declarationFile = (DeclarationFile) obj;
                if (!Wg6.a(this.id, declarationFile.id) || !Wg6.a(this.description, declarationFile.description) || !Wg6.a(this.content, declarationFile.content)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getAppId() {
        String str = this.appId;
        if (str != null) {
            return str;
        }
        Wg6.n("appId");
        throw null;
    }

    @DexIgnore
    public final String getContent() {
        return this.content;
    }

    @DexIgnore
    public final String getDescription() {
        return this.description;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getSerialNumber() {
        String str = this.serialNumber;
        if (str != null) {
            return str;
        }
        Wg6.n("serialNumber");
        throw null;
    }

    @DexIgnore
    public final String getVariantName() {
        String str = this.variantName;
        if (str != null) {
            return str;
        }
        Wg6.n("variantName");
        throw null;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.description;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.content;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public final void setAppId(String str) {
        Wg6.c(str, "<set-?>");
        this.appId = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        Wg6.c(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setVariantName(String str) {
        Wg6.c(str, "<set-?>");
        this.variantName = str;
    }

    @DexIgnore
    public String toString() {
        return "DeclarationFile(id=" + this.id + ", description=" + this.description + ", content=" + this.content + ")";
    }
}
