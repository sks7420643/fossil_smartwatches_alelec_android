package com.portfolio.platform.data.model.diana.preset;

import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MetaData {
    @DexIgnore
    @Vu3("selectedBackgroundColor")
    public String selectedBackgroundColor;
    @DexIgnore
    @Vu3("selectedForegroundColor")
    public String selectedForegroundColor;
    @DexIgnore
    @Vu3("unselectedBackgroundColor")
    public String unselectedBackgroundColor;
    @DexIgnore
    @Vu3("unselectedForegroundColor")
    public String unselectedForegroundColor;

    @DexIgnore
    public MetaData(String str, String str2, String str3, String str4) {
        this.selectedForegroundColor = str;
        this.selectedBackgroundColor = str2;
        this.unselectedForegroundColor = str3;
        this.unselectedBackgroundColor = str4;
    }

    @DexIgnore
    public static /* synthetic */ MetaData copy$default(MetaData metaData, String str, String str2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = metaData.selectedForegroundColor;
        }
        if ((i & 2) != 0) {
            str2 = metaData.selectedBackgroundColor;
        }
        if ((i & 4) != 0) {
            str3 = metaData.unselectedForegroundColor;
        }
        if ((i & 8) != 0) {
            str4 = metaData.unselectedBackgroundColor;
        }
        return metaData.copy(str, str2, str3, str4);
    }

    @DexIgnore
    public final String component1() {
        return this.selectedForegroundColor;
    }

    @DexIgnore
    public final String component2() {
        return this.selectedBackgroundColor;
    }

    @DexIgnore
    public final String component3() {
        return this.unselectedForegroundColor;
    }

    @DexIgnore
    public final String component4() {
        return this.unselectedBackgroundColor;
    }

    @DexIgnore
    public final MetaData copy(String str, String str2, String str3, String str4) {
        return new MetaData(str, str2, str3, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof MetaData) {
                MetaData metaData = (MetaData) obj;
                if (!Wg6.a(this.selectedForegroundColor, metaData.selectedForegroundColor) || !Wg6.a(this.selectedBackgroundColor, metaData.selectedBackgroundColor) || !Wg6.a(this.unselectedForegroundColor, metaData.unselectedForegroundColor) || !Wg6.a(this.unselectedBackgroundColor, metaData.unselectedBackgroundColor)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getSelectedBackgroundColor() {
        return this.selectedBackgroundColor;
    }

    @DexIgnore
    public final String getSelectedForegroundColor() {
        return this.selectedForegroundColor;
    }

    @DexIgnore
    public final String getUnselectedBackgroundColor() {
        return this.unselectedBackgroundColor;
    }

    @DexIgnore
    public final String getUnselectedForegroundColor() {
        return this.unselectedForegroundColor;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.selectedForegroundColor;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.selectedBackgroundColor;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.unselectedForegroundColor;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.unselectedBackgroundColor;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public final void setSelectedBackgroundColor(String str) {
        this.selectedBackgroundColor = str;
    }

    @DexIgnore
    public final void setSelectedForegroundColor(String str) {
        this.selectedForegroundColor = str;
    }

    @DexIgnore
    public final void setUnselectedBackgroundColor(String str) {
        this.unselectedBackgroundColor = str;
    }

    @DexIgnore
    public final void setUnselectedForegroundColor(String str) {
        this.unselectedForegroundColor = str;
    }

    @DexIgnore
    public String toString() {
        return "MetaData(selectedForegroundColor=" + this.selectedForegroundColor + ", selectedBackgroundColor=" + this.selectedBackgroundColor + ", unselectedForegroundColor=" + this.unselectedForegroundColor + ", unselectedBackgroundColor=" + this.unselectedBackgroundColor + ")";
    }
}
