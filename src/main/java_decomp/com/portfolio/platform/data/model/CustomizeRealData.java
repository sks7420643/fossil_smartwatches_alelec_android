package com.portfolio.platform.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeRealData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public String id;
    @DexIgnore
    public /* final */ String value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<CustomizeRealData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CustomizeRealData createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new CustomizeRealData(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CustomizeRealData[] newArray(int i) {
            return new CustomizeRealData[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CustomizeRealData(android.os.Parcel r4) {
        /*
            r3 = this;
            java.lang.String r0 = "parcel"
            com.mapped.Wg6.c(r4, r0)
            java.lang.String r0 = r4.readString()
            java.lang.String r2 = ""
            if (r0 == 0) goto L_0x0017
        L_0x000d:
            java.lang.String r1 = r4.readString()
            if (r1 == 0) goto L_0x001a
        L_0x0013:
            r3.<init>(r0, r1)
            return
        L_0x0017:
            java.lang.String r0 = ""
            goto L_0x000d
        L_0x001a:
            r1 = r2
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.CustomizeRealData.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public CustomizeRealData(String str, String str2) {
        Wg6.c(str, "id");
        Wg6.c(str2, "value");
        this.id = str;
        this.value = str2;
    }

    @DexIgnore
    public static /* synthetic */ CustomizeRealData copy$default(CustomizeRealData customizeRealData, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = customizeRealData.id;
        }
        if ((i & 2) != 0) {
            str2 = customizeRealData.value;
        }
        return customizeRealData.copy(str, str2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.value;
    }

    @DexIgnore
    public final CustomizeRealData copy(String str, String str2) {
        Wg6.c(str, "id");
        Wg6.c(str2, "value");
        return new CustomizeRealData(str, str2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof CustomizeRealData) {
                CustomizeRealData customizeRealData = (CustomizeRealData) obj;
                if (!Wg6.a(this.id, customizeRealData.id) || !Wg6.a(this.value, customizeRealData.value)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getValue() {
        return this.value;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.value;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public String toString() {
        return "CustomizeRealData(id=" + this.id + ", value=" + this.value + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeString(this.value);
    }
}
