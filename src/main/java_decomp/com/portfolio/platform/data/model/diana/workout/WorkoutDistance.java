package com.portfolio.platform.data.model.diana.workout;

import com.mapped.B;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDistance {
    @DexIgnore
    public int resolution;
    @DexIgnore
    public double total;
    @DexIgnore
    public List<Double> values;

    @DexIgnore
    public WorkoutDistance(int i, List<Double> list, double d) {
        this.resolution = i;
        this.values = list;
        this.total = d;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.model.diana.workout.WorkoutDistance */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ WorkoutDistance copy$default(WorkoutDistance workoutDistance, int i, List list, double d, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = workoutDistance.resolution;
        }
        if ((i2 & 2) != 0) {
            list = workoutDistance.values;
        }
        if ((i2 & 4) != 0) {
            d = workoutDistance.total;
        }
        return workoutDistance.copy(i, list, d);
    }

    @DexIgnore
    public final WorkoutSpeed calculateSpeed() {
        int i;
        int i2 = 0;
        ArrayList arrayList = new ArrayList();
        List<Double> list = this.values;
        double d = 0.0d;
        if (list != null) {
            Iterator<T> it = list.iterator();
            while (true) {
                i = i2;
                if (!it.hasNext()) {
                    break;
                }
                double doubleValue = it.next().doubleValue();
                arrayList.add(Double.valueOf(doubleValue / ((double) this.resolution)));
                d += doubleValue;
                i2 = i + 1;
            }
        } else {
            i = 0;
        }
        if (i == 0) {
            return null;
        }
        return new WorkoutSpeed(this.resolution, arrayList, d / ((double) i));
    }

    @DexIgnore
    public final int component1() {
        return this.resolution;
    }

    @DexIgnore
    public final List<Double> component2() {
        return this.values;
    }

    @DexIgnore
    public final double component3() {
        return this.total;
    }

    @DexIgnore
    public final WorkoutDistance copy(int i, List<Double> list, double d) {
        return new WorkoutDistance(i, list, d);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutDistance) {
                WorkoutDistance workoutDistance = (WorkoutDistance) obj;
                if (!(this.resolution == workoutDistance.resolution && Wg6.a(this.values, workoutDistance.values) && Double.compare(this.total, workoutDistance.total) == 0)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getResolution() {
        return this.resolution;
    }

    @DexIgnore
    public final double getTotal() {
        return this.total;
    }

    @DexIgnore
    public final List<Double> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.resolution;
        List<Double> list = this.values;
        return (((list != null ? list.hashCode() : 0) + (i * 31)) * 31) + B.a(this.total);
    }

    @DexIgnore
    public final void setResolution(int i) {
        this.resolution = i;
    }

    @DexIgnore
    public final void setTotal(double d) {
        this.total = d;
    }

    @DexIgnore
    public final void setValues(List<Double> list) {
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutDistance(resolution=" + this.resolution + ", values=" + this.values + ", total=" + this.total + ")";
    }
}
