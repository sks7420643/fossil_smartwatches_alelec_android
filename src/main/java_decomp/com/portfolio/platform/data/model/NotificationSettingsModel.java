package com.portfolio.platform.data.model;

import com.mapped.Tu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationSettingsModel {
    @DexIgnore
    @Tu3
    public boolean isCall;
    @DexIgnore
    @Tu3
    public String settingsName;
    @DexIgnore
    @Tu3
    public int settingsType;

    @DexIgnore
    public NotificationSettingsModel(String str, int i, boolean z) {
        Wg6.c(str, "settingsName");
        this.settingsName = str;
        this.settingsType = i;
        this.isCall = z;
    }

    @DexIgnore
    public static /* synthetic */ NotificationSettingsModel copy$default(NotificationSettingsModel notificationSettingsModel, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = notificationSettingsModel.settingsName;
        }
        if ((i2 & 2) != 0) {
            i = notificationSettingsModel.settingsType;
        }
        if ((i2 & 4) != 0) {
            z = notificationSettingsModel.isCall;
        }
        return notificationSettingsModel.copy(str, i, z);
    }

    @DexIgnore
    public final String component1() {
        return this.settingsName;
    }

    @DexIgnore
    public final int component2() {
        return this.settingsType;
    }

    @DexIgnore
    public final boolean component3() {
        return this.isCall;
    }

    @DexIgnore
    public final NotificationSettingsModel copy(String str, int i, boolean z) {
        Wg6.c(str, "settingsName");
        return new NotificationSettingsModel(str, i, z);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof NotificationSettingsModel) {
                NotificationSettingsModel notificationSettingsModel = (NotificationSettingsModel) obj;
                if (!(Wg6.a(this.settingsName, notificationSettingsModel.settingsName) && this.settingsType == notificationSettingsModel.settingsType && this.isCall == notificationSettingsModel.isCall)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getSettingsName() {
        return this.settingsName;
    }

    @DexIgnore
    public final int getSettingsType() {
        return this.settingsType;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.settingsName;
        int hashCode = str != null ? str.hashCode() : 0;
        int i = this.settingsType;
        boolean z = this.isCall;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((hashCode * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public final boolean isCall() {
        return this.isCall;
    }

    @DexIgnore
    public final void setCall(boolean z) {
        this.isCall = z;
    }

    @DexIgnore
    public final void setSettingsName(String str) {
        Wg6.c(str, "<set-?>");
        this.settingsName = str;
    }

    @DexIgnore
    public final void setSettingsType(int i) {
        this.settingsType = i;
    }

    @DexIgnore
    public String toString() {
        return "NotificationSettingsModel(settingsName=" + this.settingsName + ", settingsType=" + this.settingsType + ", isCall=" + this.isCall + ")";
    }
}
