package com.portfolio.platform.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Pj4;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ringtone implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @Pj4
    public String ringtoneId;
    @DexIgnore
    @Vu3("ringTone")
    public String ringtoneName;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<Ringtone> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Ringtone createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new Ringtone(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Ringtone[] newArray(int i) {
            return new Ringtone[i];
        }
    }

    @DexIgnore
    public Ringtone() {
        this(null, null, 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Ringtone(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = "parcel"
            com.mapped.Wg6.c(r3, r0)
            java.lang.String r0 = r3.readString()
            if (r0 == 0) goto L_0x0013
        L_0x000b:
            java.lang.String r1 = r3.readString()
            r2.<init>(r0, r1)
            return
        L_0x0013:
            java.lang.String r0 = ""
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.Ringtone.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public Ringtone(String str, String str2) {
        Wg6.c(str, "ringtoneName");
        this.ringtoneName = str;
        this.ringtoneId = str2;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Ringtone(String str, String str2, int i, Qg6 qg6) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? "" : str2);
    }

    @DexIgnore
    public static /* synthetic */ Ringtone copy$default(Ringtone ringtone, String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = ringtone.ringtoneName;
        }
        if ((i & 2) != 0) {
            str2 = ringtone.ringtoneId;
        }
        return ringtone.copy(str, str2);
    }

    @DexIgnore
    public final String component1() {
        return this.ringtoneName;
    }

    @DexIgnore
    public final String component2() {
        return this.ringtoneId;
    }

    @DexIgnore
    public final Ringtone copy(String str, String str2) {
        Wg6.c(str, "ringtoneName");
        return new Ringtone(str, str2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Ringtone) {
                Ringtone ringtone = (Ringtone) obj;
                if (!Wg6.a(this.ringtoneName, ringtone.ringtoneName) || !Wg6.a(this.ringtoneId, ringtone.ringtoneId)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getRingtoneId() {
        return this.ringtoneId;
    }

    @DexIgnore
    public final String getRingtoneName() {
        return this.ringtoneName;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.ringtoneName;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.ringtoneId;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public final void setRingtoneId(String str) {
        this.ringtoneId = str;
    }

    @DexIgnore
    public final void setRingtoneName(String str) {
        Wg6.c(str, "<set-?>");
        this.ringtoneName = str;
    }

    @DexIgnore
    public String toString() {
        return "Ringtone(ringtoneName=" + this.ringtoneName + ", ringtoneId=" + this.ringtoneId + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(this.ringtoneName);
        parcel.writeString(this.ringtoneId);
    }
}
