package com.portfolio.platform.data.model.goaltracking.response;

import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalEvent {
    @DexIgnore
    @Vu3("createdAt")
    public DateTime mCreatedAt;
    @DexIgnore
    @Vu3("date")
    public Date mDate;
    @DexIgnore
    @Vu3("id")
    public String mId;
    @DexIgnore
    @Vu3("timezoneOffset")
    public /* final */ int mTimeZoneOffsetInSecond;
    @DexIgnore
    @Vu3(GoalTrackingEvent.COLUMN_TRACKED_AT)
    public /* final */ DateTime mTrackedAt;
    @DexIgnore
    @Vu3("updatedAt")
    public DateTime mUpdatedAt;

    @DexIgnore
    public GoalEvent(String str, DateTime dateTime, int i, Date date, DateTime dateTime2, DateTime dateTime3) {
        Wg6.c(str, "mId");
        Wg6.c(dateTime, "mTrackedAt");
        Wg6.c(date, "mDate");
        Wg6.c(dateTime2, "mCreatedAt");
        Wg6.c(dateTime3, "mUpdatedAt");
        this.mId = str;
        this.mTrackedAt = dateTime;
        this.mTimeZoneOffsetInSecond = i;
        this.mDate = date;
        this.mCreatedAt = dateTime2;
        this.mUpdatedAt = dateTime3;
    }

    @DexIgnore
    public static /* synthetic */ GoalEvent copy$default(GoalEvent goalEvent, String str, DateTime dateTime, int i, Date date, DateTime dateTime2, DateTime dateTime3, int i2, Object obj) {
        return goalEvent.copy((i2 & 1) != 0 ? goalEvent.mId : str, (i2 & 2) != 0 ? goalEvent.mTrackedAt : dateTime, (i2 & 4) != 0 ? goalEvent.mTimeZoneOffsetInSecond : i, (i2 & 8) != 0 ? goalEvent.mDate : date, (i2 & 16) != 0 ? goalEvent.mCreatedAt : dateTime2, (i2 & 32) != 0 ? goalEvent.mUpdatedAt : dateTime3);
    }

    @DexIgnore
    public final String component1() {
        return this.mId;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.mTrackedAt;
    }

    @DexIgnore
    public final int component3() {
        return this.mTimeZoneOffsetInSecond;
    }

    @DexIgnore
    public final Date component4() {
        return this.mDate;
    }

    @DexIgnore
    public final DateTime component5() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final DateTime component6() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public final GoalEvent copy(String str, DateTime dateTime, int i, Date date, DateTime dateTime2, DateTime dateTime3) {
        Wg6.c(str, "mId");
        Wg6.c(dateTime, "mTrackedAt");
        Wg6.c(date, "mDate");
        Wg6.c(dateTime2, "mCreatedAt");
        Wg6.c(dateTime3, "mUpdatedAt");
        return new GoalEvent(str, dateTime, i, date, dateTime2, dateTime3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof GoalEvent) {
                GoalEvent goalEvent = (GoalEvent) obj;
                if (!Wg6.a(this.mId, goalEvent.mId) || !Wg6.a(this.mTrackedAt, goalEvent.mTrackedAt) || this.mTimeZoneOffsetInSecond != goalEvent.mTimeZoneOffsetInSecond || !Wg6.a(this.mDate, goalEvent.mDate) || !Wg6.a(this.mCreatedAt, goalEvent.mCreatedAt) || !Wg6.a(this.mUpdatedAt, goalEvent.mUpdatedAt)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final DateTime getMCreatedAt() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final Date getMDate() {
        return this.mDate;
    }

    @DexIgnore
    public final String getMId() {
        return this.mId;
    }

    @DexIgnore
    public final int getMTimeZoneOffsetInSecond() {
        return this.mTimeZoneOffsetInSecond;
    }

    @DexIgnore
    public final DateTime getMTrackedAt() {
        return this.mTrackedAt;
    }

    @DexIgnore
    public final DateTime getMUpdatedAt() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.mId;
        int hashCode = str != null ? str.hashCode() : 0;
        DateTime dateTime = this.mTrackedAt;
        int hashCode2 = dateTime != null ? dateTime.hashCode() : 0;
        int i2 = this.mTimeZoneOffsetInSecond;
        Date date = this.mDate;
        int hashCode3 = date != null ? date.hashCode() : 0;
        DateTime dateTime2 = this.mCreatedAt;
        int hashCode4 = dateTime2 != null ? dateTime2.hashCode() : 0;
        DateTime dateTime3 = this.mUpdatedAt;
        if (dateTime3 != null) {
            i = dateTime3.hashCode();
        }
        return (((((((((hashCode * 31) + hashCode2) * 31) + i2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public final void setMCreatedAt(DateTime dateTime) {
        Wg6.c(dateTime, "<set-?>");
        this.mCreatedAt = dateTime;
    }

    @DexIgnore
    public final void setMDate(Date date) {
        Wg6.c(date, "<set-?>");
        this.mDate = date;
    }

    @DexIgnore
    public final void setMId(String str) {
        Wg6.c(str, "<set-?>");
        this.mId = str;
    }

    @DexIgnore
    public final void setMUpdatedAt(DateTime dateTime) {
        Wg6.c(dateTime, "<set-?>");
        this.mUpdatedAt = dateTime;
    }

    @DexIgnore
    public final GoalTrackingData toGoalTrackingData() {
        try {
            String str = this.mId;
            DateTime withZone = this.mTrackedAt.withZone(DateTimeZone.forOffsetMillis(this.mTimeZoneOffsetInSecond * 1000));
            Wg6.b(withZone, "mTrackedAt.withZone(Date\u2026neOffsetInSecond * 1000))");
            return new GoalTrackingData(str, withZone, this.mTimeZoneOffsetInSecond, this.mDate, this.mCreatedAt.getMillis(), this.mUpdatedAt.getMillis());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("GoalEvent", "toGoalTrackingData exception=" + e);
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public String toString() {
        return "GoalEvent(mId=" + this.mId + ", mTrackedAt=" + this.mTrackedAt + ", mTimeZoneOffsetInSecond=" + this.mTimeZoneOffsetInSecond + ", mDate=" + this.mDate + ", mCreatedAt=" + this.mCreatedAt + ", mUpdatedAt=" + this.mUpdatedAt + ")";
    }
}
