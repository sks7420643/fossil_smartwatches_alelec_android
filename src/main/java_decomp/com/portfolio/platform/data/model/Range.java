package com.portfolio.platform.data.model;

import com.mapped.Vu3;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Range {
    @DexIgnore
    @Vu3("hasNext")
    public boolean hasNext;
    @DexIgnore
    @Vu3("limit")
    public int limit;
    @DexIgnore
    @Vu3(Constants.JSON_KEY_OFFSET)
    public int offset;

    @DexIgnore
    public int getLimit() {
        return this.limit;
    }

    @DexIgnore
    public int getOffset() {
        return this.offset;
    }

    @DexIgnore
    public boolean isHasNext() {
        return this.hasNext;
    }

    @DexIgnore
    public void setHasNext(boolean z) {
        this.hasNext = z;
    }

    @DexIgnore
    public void setOffset(int i) {
        this.offset = i;
    }
}
