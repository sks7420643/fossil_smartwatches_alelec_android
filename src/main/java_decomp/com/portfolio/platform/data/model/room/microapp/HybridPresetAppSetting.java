package com.portfolio.platform.data.model.room.microapp;

import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetAppSetting {
    @DexIgnore
    @Vu3("appId")
    public String appId;
    @DexIgnore
    @Vu3("localUpdatedAt")
    public String localUpdateAt;
    @DexIgnore
    @Vu3("buttonPosition")
    public String position;
    @DexIgnore
    @Vu3(Constants.USER_SETTING)
    public String settings;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public HybridPresetAppSetting(String str, String str2, String str3) {
        this(str, str2, str3, "");
        Wg6.c(str, "position");
        Wg6.c(str2, "complicationId");
        Wg6.c(str3, "localUpdateAt");
    }

    @DexIgnore
    public HybridPresetAppSetting(String str, String str2, String str3, String str4) {
        Wg6.c(str, "position");
        Wg6.c(str2, "appId");
        Wg6.c(str3, "localUpdateAt");
        this.position = str;
        this.appId = str2;
        this.localUpdateAt = str3;
        this.settings = str4;
    }

    @DexIgnore
    public static /* synthetic */ HybridPresetAppSetting copy$default(HybridPresetAppSetting hybridPresetAppSetting, String str, String str2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = hybridPresetAppSetting.position;
        }
        if ((i & 2) != 0) {
            str2 = hybridPresetAppSetting.appId;
        }
        if ((i & 4) != 0) {
            str3 = hybridPresetAppSetting.localUpdateAt;
        }
        if ((i & 8) != 0) {
            str4 = hybridPresetAppSetting.settings;
        }
        return hybridPresetAppSetting.copy(str, str2, str3, str4);
    }

    @DexIgnore
    public final String component1() {
        return this.position;
    }

    @DexIgnore
    public final String component2() {
        return this.appId;
    }

    @DexIgnore
    public final String component3() {
        return this.localUpdateAt;
    }

    @DexIgnore
    public final String component4() {
        return this.settings;
    }

    @DexIgnore
    public final HybridPresetAppSetting copy(String str, String str2, String str3, String str4) {
        Wg6.c(str, "position");
        Wg6.c(str2, "appId");
        Wg6.c(str3, "localUpdateAt");
        return new HybridPresetAppSetting(str, str2, str3, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof HybridPresetAppSetting) {
                HybridPresetAppSetting hybridPresetAppSetting = (HybridPresetAppSetting) obj;
                if (!Wg6.a(this.position, hybridPresetAppSetting.position) || !Wg6.a(this.appId, hybridPresetAppSetting.appId) || !Wg6.a(this.localUpdateAt, hybridPresetAppSetting.localUpdateAt) || !Wg6.a(this.settings, hybridPresetAppSetting.settings)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getAppId() {
        return this.appId;
    }

    @DexIgnore
    public final String getLocalUpdateAt() {
        return this.localUpdateAt;
    }

    @DexIgnore
    public final String getPosition() {
        return this.position;
    }

    @DexIgnore
    public final String getSettings() {
        return this.settings;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.position;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.appId;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.localUpdateAt;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.settings;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public final void setAppId(String str) {
        Wg6.c(str, "<set-?>");
        this.appId = str;
    }

    @DexIgnore
    public final void setLocalUpdateAt(String str) {
        Wg6.c(str, "<set-?>");
        this.localUpdateAt = str;
    }

    @DexIgnore
    public final void setPosition(String str) {
        Wg6.c(str, "<set-?>");
        this.position = str;
    }

    @DexIgnore
    public final void setSettings(String str) {
        this.settings = str;
    }

    @DexIgnore
    public String toString() {
        return "HybridPresetAppSetting(position=" + this.position + ", appId=" + this.appId + ", localUpdateAt=" + this.localUpdateAt + ", settings=" + this.settings + ")";
    }
}
