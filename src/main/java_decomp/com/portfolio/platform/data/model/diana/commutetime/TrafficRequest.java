package com.portfolio.platform.data.model.diana.commutetime;

import com.facebook.share.internal.ShareConstants;
import com.fossil.Hm7;
import com.mapped.Vu3;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TrafficRequest {
    @DexIgnore
    @Vu3("avoid")
    public List<String> avoid;
    @DexIgnore
    @Vu3(ShareConstants.DESTINATION)
    public Address destination;
    @DexIgnore
    @Vu3("mode")
    public String mode; // = "driving";
    @DexIgnore
    @Vu3("origin")
    public Address origin;

    @DexIgnore
    public TrafficRequest(boolean z, Address address, Address address2) {
        ArrayList arrayList;
        Wg6.c(address, ShareConstants.DESTINATION);
        Wg6.c(address2, "origin");
        this.destination = address;
        this.origin = address2;
        if (z) {
            arrayList = Hm7.c("tolls");
        } else {
            arrayList = null;
        }
        this.avoid = arrayList;
        this.mode = "driving";
    }

    @DexIgnore
    public final List<String> getAvoid() {
        return this.avoid;
    }

    @DexIgnore
    public final Address getDestination() {
        return this.destination;
    }

    @DexIgnore
    public final String getMode() {
        return this.mode;
    }

    @DexIgnore
    public final Address getOrigin() {
        return this.origin;
    }

    @DexIgnore
    public final void setAvoid(List<String> list) {
        this.avoid = list;
    }

    @DexIgnore
    public final void setDestination(Address address) {
        Wg6.c(address, "<set-?>");
        this.destination = address;
    }

    @DexIgnore
    public final void setMode(String str) {
        Wg6.c(str, "<set-?>");
        this.mode = str;
    }

    @DexIgnore
    public final void setOrigin(Address address) {
        Wg6.c(address, "<set-?>");
        this.origin = address;
    }
}
