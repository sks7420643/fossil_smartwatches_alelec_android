package com.portfolio.platform.data.model;

import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerSettingList {
    @DexIgnore
    @Vu3(Constants.JSON_KEY_OFFSET)
    public int offset;
    @DexIgnore
    @Vu3(CloudLogWriter.ITEMS_PARAM)
    public List<ServerSetting> serverSettings;
    @DexIgnore
    @Vu3("size")
    public int size;
    @DexIgnore
    @Vu3(Constants.JSON_KEY_TOTAL)
    public int total;

    @DexIgnore
    public final int getOffset() {
        return this.offset;
    }

    @DexIgnore
    public final List<ServerSetting> getServerSettings() {
        return this.serverSettings;
    }

    @DexIgnore
    public final int getSize() {
        return this.size;
    }

    @DexIgnore
    public final int getTotal() {
        return this.total;
    }

    @DexIgnore
    public final void setOffset(int i) {
        this.offset = i;
    }

    @DexIgnore
    public final void setServerSettings(List<ServerSetting> list) {
        this.serverSettings = list;
    }

    @DexIgnore
    public final void setSize(int i) {
        this.size = i;
    }

    @DexIgnore
    public final void setTotal(int i) {
        this.total = i;
    }

    @DexIgnore
    public String toString() {
        if (this.serverSettings == null) {
            return "serverSettings is null";
        }
        StringBuffer stringBuffer = new StringBuffer();
        List<ServerSetting> list = this.serverSettings;
        if (list != null) {
            Iterator<ServerSetting> it = list.iterator();
            while (it.hasNext()) {
                stringBuffer.append(String.valueOf(it.next()) + "\n");
            }
        }
        String stringBuffer2 = stringBuffer.toString();
        Wg6.b(stringBuffer2, "s.toString()");
        return stringBuffer2;
    }
}
