package com.portfolio.platform.data.model.ua;

import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.mapped.Vu3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UADevice {
    @DexIgnore
    @Vu3("description")
    public String description;
    @DexIgnore
    @Vu3("_links")
    public UALinks links;
    @DexIgnore
    @Vu3("manufacturer")
    public String manufacturer;
    @DexIgnore
    @Vu3(DeviceRequestsHelper.DEVICE_INFO_MODEL)
    public String model;
    @DexIgnore
    @Vu3("name")
    public String name;

    @DexIgnore
    public final String getDescription() {
        return this.description;
    }

    @DexIgnore
    public final UALinks getLinks() {
        return this.links;
    }

    @DexIgnore
    public final String getManufacturer() {
        return this.manufacturer;
    }

    @DexIgnore
    public final String getModel() {
        return this.model;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final void setDescription(String str) {
        this.description = str;
    }

    @DexIgnore
    public final void setLinks(UALinks uALinks) {
        this.links = uALinks;
    }

    @DexIgnore
    public final void setManufacturer(String str) {
        this.manufacturer = str;
    }

    @DexIgnore
    public final void setModel(String str) {
        this.model = str;
    }

    @DexIgnore
    public final void setName(String str) {
        this.name = str;
    }
}
