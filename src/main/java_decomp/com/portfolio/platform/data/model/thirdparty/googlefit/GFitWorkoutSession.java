package com.portfolio.platform.data.model.thirdparty.googlefit;

import com.fossil.Hm7;
import com.mapped.C;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitWorkoutSession {
    @DexIgnore
    public List<GFitWOCalorie> calories;
    @DexIgnore
    public List<GFitWODistance> distances;
    @DexIgnore
    public long endTime;
    @DexIgnore
    public List<GFitWOHeartRate> heartRates;
    @DexIgnore
    public int id;
    @DexIgnore
    public long startTime;
    @DexIgnore
    public List<GFitWOStep> steps;
    @DexIgnore
    public int workoutType;

    @DexIgnore
    public GFitWorkoutSession(long j, long j2, int i, List<GFitWOStep> list, List<GFitWOCalorie> list2, List<GFitWODistance> list3, List<GFitWOHeartRate> list4) {
        Wg6.c(list, "steps");
        Wg6.c(list2, "calories");
        Wg6.c(list3, "distances");
        Wg6.c(list4, "heartRates");
        this.startTime = j;
        this.endTime = j2;
        this.workoutType = i;
        this.steps = list;
        this.calories = list2;
        this.distances = list3;
        this.heartRates = list4;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ GFitWorkoutSession(long j, long j2, int i, List list, List list2, List list3, List list4, int i2, Qg6 qg6) {
        this(j, j2, i, (i2 & 8) != 0 ? Hm7.e() : list, (i2 & 16) != 0 ? Hm7.e() : list2, (i2 & 32) != 0 ? Hm7.e() : list3, (i2 & 64) != 0 ? Hm7.e() : list4);
    }

    @DexIgnore
    public static /* synthetic */ GFitWorkoutSession copy$default(GFitWorkoutSession gFitWorkoutSession, long j, long j2, int i, List list, List list2, List list3, List list4, int i2, Object obj) {
        return gFitWorkoutSession.copy((i2 & 1) != 0 ? gFitWorkoutSession.startTime : j, (i2 & 2) != 0 ? gFitWorkoutSession.endTime : j2, (i2 & 4) != 0 ? gFitWorkoutSession.workoutType : i, (i2 & 8) != 0 ? gFitWorkoutSession.steps : list, (i2 & 16) != 0 ? gFitWorkoutSession.calories : list2, (i2 & 32) != 0 ? gFitWorkoutSession.distances : list3, (i2 & 64) != 0 ? gFitWorkoutSession.heartRates : list4);
    }

    @DexIgnore
    public final long component1() {
        return this.startTime;
    }

    @DexIgnore
    public final long component2() {
        return this.endTime;
    }

    @DexIgnore
    public final int component3() {
        return this.workoutType;
    }

    @DexIgnore
    public final List<GFitWOStep> component4() {
        return this.steps;
    }

    @DexIgnore
    public final List<GFitWOCalorie> component5() {
        return this.calories;
    }

    @DexIgnore
    public final List<GFitWODistance> component6() {
        return this.distances;
    }

    @DexIgnore
    public final List<GFitWOHeartRate> component7() {
        return this.heartRates;
    }

    @DexIgnore
    public final GFitWorkoutSession copy(long j, long j2, int i, List<GFitWOStep> list, List<GFitWOCalorie> list2, List<GFitWODistance> list3, List<GFitWOHeartRate> list4) {
        Wg6.c(list, "steps");
        Wg6.c(list2, "calories");
        Wg6.c(list3, "distances");
        Wg6.c(list4, "heartRates");
        return new GFitWorkoutSession(j, j2, i, list, list2, list3, list4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof GFitWorkoutSession) {
                GFitWorkoutSession gFitWorkoutSession = (GFitWorkoutSession) obj;
                if (this.startTime != gFitWorkoutSession.startTime || this.endTime != gFitWorkoutSession.endTime || this.workoutType != gFitWorkoutSession.workoutType || !Wg6.a(this.steps, gFitWorkoutSession.steps) || !Wg6.a(this.calories, gFitWorkoutSession.calories) || !Wg6.a(this.distances, gFitWorkoutSession.distances) || !Wg6.a(this.heartRates, gFitWorkoutSession.heartRates)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final List<GFitWOCalorie> getCalories() {
        return this.calories;
    }

    @DexIgnore
    public final List<GFitWODistance> getDistances() {
        return this.distances;
    }

    @DexIgnore
    public final long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final List<GFitWOHeartRate> getHeartRates() {
        return this.heartRates;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final List<GFitWOStep> getSteps() {
        return this.steps;
    }

    @DexIgnore
    public final int getWorkoutType() {
        return this.workoutType;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int a2 = C.a(this.startTime);
        int a3 = C.a(this.endTime);
        int i2 = this.workoutType;
        List<GFitWOStep> list = this.steps;
        int hashCode = list != null ? list.hashCode() : 0;
        List<GFitWOCalorie> list2 = this.calories;
        int hashCode2 = list2 != null ? list2.hashCode() : 0;
        List<GFitWODistance> list3 = this.distances;
        int hashCode3 = list3 != null ? list3.hashCode() : 0;
        List<GFitWOHeartRate> list4 = this.heartRates;
        if (list4 != null) {
            i = list4.hashCode();
        }
        return ((((((hashCode + (((((a2 * 31) + a3) * 31) + i2) * 31)) * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public final void setCalories(List<GFitWOCalorie> list) {
        Wg6.c(list, "<set-?>");
        this.calories = list;
    }

    @DexIgnore
    public final void setDistances(List<GFitWODistance> list) {
        Wg6.c(list, "<set-?>");
        this.distances = list;
    }

    @DexIgnore
    public final void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public final void setHeartRates(List<GFitWOHeartRate> list) {
        Wg6.c(list, "<set-?>");
        this.heartRates = list;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setStartTime(long j) {
        this.startTime = j;
    }

    @DexIgnore
    public final void setSteps(List<GFitWOStep> list) {
        Wg6.c(list, "<set-?>");
        this.steps = list;
    }

    @DexIgnore
    public final void setWorkoutType(int i) {
        this.workoutType = i;
    }

    @DexIgnore
    public String toString() {
        return "GFitWorkoutSession(startTime=" + this.startTime + ", endTime=" + this.endTime + ", workoutType=" + this.workoutType + ", steps=" + this.steps + ", calories=" + this.calories + ", distances=" + this.distances + ", heartRates=" + this.heartRates + ")";
    }
}
