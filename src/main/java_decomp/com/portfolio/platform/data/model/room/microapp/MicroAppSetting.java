package com.portfolio.platform.data.model.room.microapp;

import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppSetting {
    @DexIgnore
    @Vu3("appId")
    public String appId;
    @DexIgnore
    @Vu3("createdAt")
    public String createdAt;
    @DexIgnore
    @Vu3("id")
    public String id;
    @DexIgnore
    public int pinType;
    @DexIgnore
    @Vu3(Constants.USER_SETTING)
    public String setting;
    @DexIgnore
    @Vu3("updatedAt")
    public String updatedAt;

    @DexIgnore
    public MicroAppSetting(String str, String str2, String str3, String str4, String str5, int i) {
        Wg6.c(str, "id");
        Wg6.c(str2, "appId");
        Wg6.c(str4, "createdAt");
        Wg6.c(str5, "updatedAt");
        this.id = str;
        this.appId = str2;
        this.setting = str3;
        this.createdAt = str4;
        this.updatedAt = str5;
        this.pinType = i;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MicroAppSetting(String str, String str2, String str3, String str4, String str5, int i, int i2, Qg6 qg6) {
        this(str, str2, str3, str4, str5, (i2 & 32) != 0 ? 1 : i);
    }

    @DexIgnore
    public static /* synthetic */ MicroAppSetting copy$default(MicroAppSetting microAppSetting, String str, String str2, String str3, String str4, String str5, int i, int i2, Object obj) {
        return microAppSetting.copy((i2 & 1) != 0 ? microAppSetting.id : str, (i2 & 2) != 0 ? microAppSetting.appId : str2, (i2 & 4) != 0 ? microAppSetting.setting : str3, (i2 & 8) != 0 ? microAppSetting.createdAt : str4, (i2 & 16) != 0 ? microAppSetting.updatedAt : str5, (i2 & 32) != 0 ? microAppSetting.pinType : i);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.appId;
    }

    @DexIgnore
    public final String component3() {
        return this.setting;
    }

    @DexIgnore
    public final String component4() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component5() {
        return this.updatedAt;
    }

    @DexIgnore
    public final int component6() {
        return this.pinType;
    }

    @DexIgnore
    public final MicroAppSetting copy(String str, String str2, String str3, String str4, String str5, int i) {
        Wg6.c(str, "id");
        Wg6.c(str2, "appId");
        Wg6.c(str4, "createdAt");
        Wg6.c(str5, "updatedAt");
        return new MicroAppSetting(str, str2, str3, str4, str5, i);
    }

    @DexIgnore
    public final MicroAppSetting deepCopy() {
        return new MicroAppSetting(this.id, this.appId, this.setting, this.createdAt, this.updatedAt, 0, 32, null);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof MicroAppSetting) {
                MicroAppSetting microAppSetting = (MicroAppSetting) obj;
                if (!Wg6.a(this.id, microAppSetting.id) || !Wg6.a(this.appId, microAppSetting.appId) || !Wg6.a(this.setting, microAppSetting.setting) || !Wg6.a(this.createdAt, microAppSetting.createdAt) || !Wg6.a(this.updatedAt, microAppSetting.updatedAt) || this.pinType != microAppSetting.pinType) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getAppId() {
        return this.appId;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSetting() {
        return this.setting;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.appId;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.setting;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.createdAt;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.updatedAt;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return (((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i) * 31) + this.pinType;
    }

    @DexIgnore
    public final void setAppId(String str) {
        Wg6.c(str, "<set-?>");
        this.appId = str;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSetting(String str) {
        this.setting = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "MicroAppSetting(id=" + this.id + ", appId=" + this.appId + ", setting=" + this.setting + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", pinType=" + this.pinType + ")";
    }
}
