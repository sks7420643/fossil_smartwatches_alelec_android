package com.portfolio.platform.data.model.watchface;

import com.mapped.Vu3;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Firmware;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceTemplate {
    @DexIgnore
    @Vu3("checksum")
    public String checksum;
    @DexIgnore
    @Vu3("createdAt")
    public String createdAt; // = "2016-01-01T01:01:01.001Z";
    @DexIgnore
    @Vu3(Firmware.COLUMN_DOWNLOAD_URL)
    public String downloadURL;
    @DexIgnore
    @Vu3("firmwareOSVersion")
    public String firmwareOSVersion;
    @DexIgnore
    @Vu3("id")
    public String id;
    @DexIgnore
    @Vu3("packageVersion")
    public String packageVersion;
    @DexIgnore
    @Vu3("themeClass")
    public String themeClass;
    @DexIgnore
    @Vu3("updatedAt")
    public String updatedAt; // = "2016-01-01T01:01:01.001Z";

    @DexIgnore
    public DianaWatchFaceTemplate(String str, String str2, String str3, String str4, String str5, String str6) {
        Wg6.c(str, "id");
        Wg6.c(str2, "downloadURL");
        Wg6.c(str3, "checksum");
        Wg6.c(str4, "firmwareOSVersion");
        Wg6.c(str5, "packageVersion");
        Wg6.c(str6, "themeClass");
        this.id = str;
        this.downloadURL = str2;
        this.checksum = str3;
        this.firmwareOSVersion = str4;
        this.packageVersion = str5;
        this.themeClass = str6;
    }

    @DexIgnore
    public static /* synthetic */ DianaWatchFaceTemplate copy$default(DianaWatchFaceTemplate dianaWatchFaceTemplate, String str, String str2, String str3, String str4, String str5, String str6, int i, Object obj) {
        return dianaWatchFaceTemplate.copy((i & 1) != 0 ? dianaWatchFaceTemplate.id : str, (i & 2) != 0 ? dianaWatchFaceTemplate.downloadURL : str2, (i & 4) != 0 ? dianaWatchFaceTemplate.checksum : str3, (i & 8) != 0 ? dianaWatchFaceTemplate.firmwareOSVersion : str4, (i & 16) != 0 ? dianaWatchFaceTemplate.packageVersion : str5, (i & 32) != 0 ? dianaWatchFaceTemplate.themeClass : str6);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.downloadURL;
    }

    @DexIgnore
    public final String component3() {
        return this.checksum;
    }

    @DexIgnore
    public final String component4() {
        return this.firmwareOSVersion;
    }

    @DexIgnore
    public final String component5() {
        return this.packageVersion;
    }

    @DexIgnore
    public final String component6() {
        return this.themeClass;
    }

    @DexIgnore
    public final DianaWatchFaceTemplate copy(String str, String str2, String str3, String str4, String str5, String str6) {
        Wg6.c(str, "id");
        Wg6.c(str2, "downloadURL");
        Wg6.c(str3, "checksum");
        Wg6.c(str4, "firmwareOSVersion");
        Wg6.c(str5, "packageVersion");
        Wg6.c(str6, "themeClass");
        return new DianaWatchFaceTemplate(str, str2, str3, str4, str5, str6);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DianaWatchFaceTemplate) {
                DianaWatchFaceTemplate dianaWatchFaceTemplate = (DianaWatchFaceTemplate) obj;
                if (!Wg6.a(this.id, dianaWatchFaceTemplate.id) || !Wg6.a(this.downloadURL, dianaWatchFaceTemplate.downloadURL) || !Wg6.a(this.checksum, dianaWatchFaceTemplate.checksum) || !Wg6.a(this.firmwareOSVersion, dianaWatchFaceTemplate.firmwareOSVersion) || !Wg6.a(this.packageVersion, dianaWatchFaceTemplate.packageVersion) || !Wg6.a(this.themeClass, dianaWatchFaceTemplate.themeClass)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getChecksum() {
        return this.checksum;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getDownloadURL() {
        return this.downloadURL;
    }

    @DexIgnore
    public final String getFirmwareOSVersion() {
        return this.firmwareOSVersion;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getPackageVersion() {
        return this.packageVersion;
    }

    @DexIgnore
    public final String getThemeClass() {
        return this.themeClass;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.downloadURL;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.checksum;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.firmwareOSVersion;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.packageVersion;
        int hashCode5 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.themeClass;
        if (str6 != null) {
            i = str6.hashCode();
        }
        return (((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + i;
    }

    @DexIgnore
    public final void setChecksum(String str) {
        Wg6.c(str, "<set-?>");
        this.checksum = str;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDownloadURL(String str) {
        Wg6.c(str, "<set-?>");
        this.downloadURL = str;
    }

    @DexIgnore
    public final void setFirmwareOSVersion(String str) {
        Wg6.c(str, "<set-?>");
        this.firmwareOSVersion = str;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setPackageVersion(String str) {
        Wg6.c(str, "<set-?>");
        this.packageVersion = str;
    }

    @DexIgnore
    public final void setThemeClass(String str) {
        Wg6.c(str, "<set-?>");
        this.themeClass = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "DianaWatchFaceTemplate(id=" + this.id + ", downloadURL=" + this.downloadURL + ", checksum=" + this.checksum + ", firmwareOSVersion=" + this.firmwareOSVersion + ", packageVersion=" + this.packageVersion + ", themeClass=" + this.themeClass + ")";
    }
}
