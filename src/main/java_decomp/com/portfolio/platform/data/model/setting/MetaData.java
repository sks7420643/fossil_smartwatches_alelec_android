package com.portfolio.platform.data.model.setting;

import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.watchparams.Version;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MetaData {
    @DexIgnore
    @Vu3("locale")
    public String locale;
    @DexIgnore
    @Vu3("version")
    public Version version;

    @DexIgnore
    public MetaData(String str, Version version2) {
        Wg6.c(str, "locale");
        Wg6.c(version2, "version");
        this.locale = str;
        this.version = version2;
    }

    @DexIgnore
    public static /* synthetic */ MetaData copy$default(MetaData metaData, String str, Version version2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = metaData.locale;
        }
        if ((i & 2) != 0) {
            version2 = metaData.version;
        }
        return metaData.copy(str, version2);
    }

    @DexIgnore
    public final String component1() {
        return this.locale;
    }

    @DexIgnore
    public final Version component2() {
        return this.version;
    }

    @DexIgnore
    public final MetaData copy(String str, Version version2) {
        Wg6.c(str, "locale");
        Wg6.c(version2, "version");
        return new MetaData(str, version2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof MetaData) {
                MetaData metaData = (MetaData) obj;
                if (!Wg6.a(this.locale, metaData.locale) || !Wg6.a(this.version, metaData.version)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getLocale() {
        return this.locale;
    }

    @DexIgnore
    public final Version getVersion() {
        return this.version;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.locale;
        int hashCode = str != null ? str.hashCode() : 0;
        Version version2 = this.version;
        if (version2 != null) {
            i = version2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public final void setLocale(String str) {
        Wg6.c(str, "<set-?>");
        this.locale = str;
    }

    @DexIgnore
    public final void setVersion(Version version2) {
        Wg6.c(version2, "<set-?>");
        this.version = version2;
    }

    @DexIgnore
    public String toString() {
        return "MetaData(locale=" + this.locale + ", version=" + this.version + ")";
    }
}
