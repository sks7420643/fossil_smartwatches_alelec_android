package com.portfolio.platform.data.model.diana.preset;

import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetComplicationSetting {
    @DexIgnore
    @Vu3("appId")
    public String id;
    @DexIgnore
    @Vu3("localUpdatedAt")
    public String localUpdateAt;
    @DexIgnore
    @Vu3("complicationPosition")
    public String position;
    @DexIgnore
    @Vu3(Constants.USER_SETTING)
    public String settings;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DianaPresetComplicationSetting(String str, String str2, String str3) {
        this(str, str2, str3, "");
        Wg6.c(str, "position");
        Wg6.c(str2, "complicationId");
        Wg6.c(str3, "localUpdateAt");
    }

    @DexIgnore
    public DianaPresetComplicationSetting(String str, String str2, String str3, String str4) {
        Wg6.c(str, "position");
        Wg6.c(str2, "id");
        Wg6.c(str3, "localUpdateAt");
        this.position = str;
        this.id = str2;
        this.localUpdateAt = str3;
        this.settings = str4;
    }

    @DexIgnore
    public static /* synthetic */ DianaPresetComplicationSetting copy$default(DianaPresetComplicationSetting dianaPresetComplicationSetting, String str, String str2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = dianaPresetComplicationSetting.position;
        }
        if ((i & 2) != 0) {
            str2 = dianaPresetComplicationSetting.id;
        }
        if ((i & 4) != 0) {
            str3 = dianaPresetComplicationSetting.localUpdateAt;
        }
        if ((i & 8) != 0) {
            str4 = dianaPresetComplicationSetting.settings;
        }
        return dianaPresetComplicationSetting.copy(str, str2, str3, str4);
    }

    @DexIgnore
    public final String component1() {
        return this.position;
    }

    @DexIgnore
    public final String component2() {
        return this.id;
    }

    @DexIgnore
    public final String component3() {
        return this.localUpdateAt;
    }

    @DexIgnore
    public final String component4() {
        return this.settings;
    }

    @DexIgnore
    public final DianaPresetComplicationSetting copy(String str, String str2, String str3, String str4) {
        Wg6.c(str, "position");
        Wg6.c(str2, "id");
        Wg6.c(str3, "localUpdateAt");
        return new DianaPresetComplicationSetting(str, str2, str3, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DianaPresetComplicationSetting) {
                DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) obj;
                if (!Wg6.a(this.position, dianaPresetComplicationSetting.position) || !Wg6.a(this.id, dianaPresetComplicationSetting.id) || !Wg6.a(this.localUpdateAt, dianaPresetComplicationSetting.localUpdateAt) || !Wg6.a(this.settings, dianaPresetComplicationSetting.settings)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getLocalUpdateAt() {
        return this.localUpdateAt;
    }

    @DexIgnore
    public final String getPosition() {
        return this.position;
    }

    @DexIgnore
    public final String getSettings() {
        return this.settings;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.position;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.id;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.localUpdateAt;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.settings;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setLocalUpdateAt(String str) {
        Wg6.c(str, "<set-?>");
        this.localUpdateAt = str;
    }

    @DexIgnore
    public final void setPosition(String str) {
        Wg6.c(str, "<set-?>");
        this.position = str;
    }

    @DexIgnore
    public final void setSettings(String str) {
        this.settings = str;
    }

    @DexIgnore
    public String toString() {
        return "DianaPresetComplicationSetting(position=" + this.position + ", id=" + this.id + ", localUpdateAt=" + this.localUpdateAt + ", settings=" + this.settings + ")";
    }
}
