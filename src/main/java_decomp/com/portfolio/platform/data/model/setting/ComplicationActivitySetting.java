package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationActivitySetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @Vu3("currentValue")
    public String currentValue;
    @DexIgnore
    @Vu3("isRingEnabled")
    public boolean isRingEnabled;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ComplicationActivitySetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ComplicationActivitySetting createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new ComplicationActivitySetting(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ComplicationActivitySetting[] newArray(int i) {
            return new ComplicationActivitySetting[i];
        }
    }

    @DexIgnore
    public ComplicationActivitySetting() {
        this(false, 1, null);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ComplicationActivitySetting(android.os.Parcel r4) {
        /*
            r3 = this;
            r0 = 0
            java.lang.String r1 = "parcel"
            com.mapped.Wg6.c(r4, r1)
            byte r1 = r4.readByte()
            byte r2 = (byte) r0
            if (r1 == r2) goto L_0x000e
            r0 = 1
        L_0x000e:
            r3.<init>(r0)
            java.lang.String r0 = r4.readString()
            if (r0 == 0) goto L_0x001a
        L_0x0017:
            r3.currentValue = r0
            return
        L_0x001a:
            java.lang.String r0 = ""
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.setting.ComplicationActivitySetting.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public ComplicationActivitySetting(boolean z) {
        this.isRingEnabled = z;
        this.currentValue = "";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ComplicationActivitySetting(boolean z, int i, Qg6 qg6) {
        this((i & 1) != 0 ? false : z);
    }

    @DexIgnore
    public static /* synthetic */ ComplicationActivitySetting copy$default(ComplicationActivitySetting complicationActivitySetting, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = complicationActivitySetting.isRingEnabled;
        }
        return complicationActivitySetting.copy(z);
    }

    @DexIgnore
    public final boolean component1() {
        return this.isRingEnabled;
    }

    @DexIgnore
    public final ComplicationActivitySetting copy(boolean z) {
        return new ComplicationActivitySetting(z);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof ComplicationActivitySetting) && this.isRingEnabled == ((ComplicationActivitySetting) obj).isRingEnabled);
    }

    @DexIgnore
    public final String getCurrentValue() {
        return this.currentValue;
    }

    @DexIgnore
    public int hashCode() {
        boolean z = this.isRingEnabled;
        if (z) {
            return 1;
        }
        return z ? 1 : 0;
    }

    @DexIgnore
    public final boolean isRingEnabled() {
        return this.isRingEnabled;
    }

    @DexIgnore
    public final void setCurrentValue(String str) {
        Wg6.c(str, "<set-?>");
        this.currentValue = str;
    }

    @DexIgnore
    public final void setRingEnabled(boolean z) {
        this.isRingEnabled = z;
    }

    @DexIgnore
    public String toString() {
        return "ComplicationActivitySetting(isRingEnabled=" + this.isRingEnabled + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeByte(this.isRingEnabled ? (byte) 1 : 0);
        parcel.writeString(this.currentValue);
    }
}
