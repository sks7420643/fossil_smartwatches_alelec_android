package com.portfolio.platform.data.model.diana.heartrate;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Wt7;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.mapped.C;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSample implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public float average;
    @DexIgnore
    public long createdAt;
    @DexIgnore
    public Date date;
    @DexIgnore
    public DateTime endTime;
    @DexIgnore
    public String id;
    @DexIgnore
    public int max;
    @DexIgnore
    public int min;
    @DexIgnore
    public int minuteCount;
    @DexIgnore
    public Resting resting;
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public int timezoneOffsetInSecond;
    @DexIgnore
    public long updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<HeartRateSample> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public HeartRateSample createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new HeartRateSample(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public HeartRateSample[] newArray(int i) {
            return new HeartRateSample[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public HeartRateSample(float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, String str, int i4, Resting resting2) {
        this(str + ":device:" + (dateTime2.getMillis() / ((long) 1000)), f, date2, j, j2, dateTime, dateTime2, i, i2, i3, i4, resting2);
        Wg6.c(date2, "date");
        Wg6.c(dateTime, SampleRaw.COLUMN_END_TIME);
        Wg6.c(dateTime2, SampleRaw.COLUMN_START_TIME);
        Wg6.c(str, ButtonService.USER_ID);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HeartRateSample(float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, String str, int i4, Resting resting2, int i5, Qg6 qg6) {
        this(f, date2, j, j2, dateTime, dateTime2, i, i2, i3, str, i4, (i5 & 2048) != 0 ? null : resting2);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public HeartRateSample(android.os.Parcel r18) {
        /*
            r17 = this;
            java.lang.String r2 = "parcel"
            r0 = r18
            com.mapped.Wg6.c(r0, r2)
            java.lang.String r3 = r18.readString()
            if (r3 == 0) goto L_0x004d
        L_0x000d:
            float r4 = r18.readFloat()
            java.io.Serializable r5 = r18.readSerializable()
            if (r5 == 0) goto L_0x0060
            java.util.Date r5 = (java.util.Date) r5
            long r6 = r18.readLong()
            long r8 = r18.readLong()
            java.io.Serializable r10 = r18.readSerializable()
            if (r10 == 0) goto L_0x0058
            org.joda.time.DateTime r10 = (org.joda.time.DateTime) r10
            java.io.Serializable r11 = r18.readSerializable()
            if (r11 == 0) goto L_0x0050
            org.joda.time.DateTime r11 = (org.joda.time.DateTime) r11
            int r12 = r18.readInt()
            int r13 = r18.readInt()
            int r14 = r18.readInt()
            int r15 = r18.readInt()
            java.io.Serializable r16 = r18.readSerializable()
            com.portfolio.platform.data.model.diana.heartrate.Resting r16 = (com.portfolio.platform.data.model.diana.heartrate.Resting) r16
            r2 = r17
            r2.<init>(r3, r4, r5, r6, r8, r10, r11, r12, r13, r14, r15, r16)
            return
        L_0x004d:
            java.lang.String r3 = ""
            goto L_0x000d
        L_0x0050:
            com.mapped.Rc6 r2 = new com.mapped.Rc6
            java.lang.String r3 = "null cannot be cast to non-null type org.joda.time.DateTime"
            r2.<init>(r3)
            throw r2
        L_0x0058:
            com.mapped.Rc6 r2 = new com.mapped.Rc6
            java.lang.String r3 = "null cannot be cast to non-null type org.joda.time.DateTime"
            r2.<init>(r3)
            throw r2
        L_0x0060:
            com.mapped.Rc6 r2 = new com.mapped.Rc6
            java.lang.String r3 = "null cannot be cast to non-null type java.util.Date"
            r2.<init>(r3)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.diana.heartrate.HeartRateSample.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public HeartRateSample(String str, float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, Resting resting2) {
        Wg6.c(str, "id");
        Wg6.c(date2, "date");
        Wg6.c(dateTime, SampleRaw.COLUMN_END_TIME);
        Wg6.c(dateTime2, SampleRaw.COLUMN_START_TIME);
        this.id = str;
        this.average = f;
        this.date = date2;
        this.createdAt = j;
        this.updatedAt = j2;
        this.endTime = dateTime;
        this.startTime = dateTime2;
        this.timezoneOffsetInSecond = i;
        this.min = i2;
        this.max = i3;
        this.minuteCount = i4;
        this.resting = resting2;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HeartRateSample(String str, float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, Resting resting2, int i5, Qg6 qg6) {
        this(str, f, date2, j, j2, dateTime, dateTime2, i, i2, i3, i4, (i5 & 2048) != 0 ? null : resting2);
    }

    @DexIgnore
    public static /* synthetic */ HeartRateSample copy$default(HeartRateSample heartRateSample, String str, float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, Resting resting2, int i5, Object obj) {
        return heartRateSample.copy((i5 & 1) != 0 ? heartRateSample.id : str, (i5 & 2) != 0 ? heartRateSample.average : f, (i5 & 4) != 0 ? heartRateSample.date : date2, (i5 & 8) != 0 ? heartRateSample.createdAt : j, (i5 & 16) != 0 ? heartRateSample.updatedAt : j2, (i5 & 32) != 0 ? heartRateSample.endTime : dateTime, (i5 & 64) != 0 ? heartRateSample.startTime : dateTime2, (i5 & 128) != 0 ? heartRateSample.timezoneOffsetInSecond : i, (i5 & 256) != 0 ? heartRateSample.min : i2, (i5 & 512) != 0 ? heartRateSample.max : i3, (i5 & 1024) != 0 ? heartRateSample.minuteCount : i4, (i5 & 2048) != 0 ? heartRateSample.resting : resting2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final int component10() {
        return this.max;
    }

    @DexIgnore
    public final int component11() {
        return this.minuteCount;
    }

    @DexIgnore
    public final Resting component12() {
        return this.resting;
    }

    @DexIgnore
    public final float component2() {
        return this.average;
    }

    @DexIgnore
    public final Date component3() {
        return this.date;
    }

    @DexIgnore
    public final long component4() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component5() {
        return this.updatedAt;
    }

    @DexIgnore
    public final DateTime component6() {
        return this.endTime;
    }

    @DexIgnore
    public final DateTime component7() {
        return this.startTime;
    }

    @DexIgnore
    public final int component8() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int component9() {
        return this.min;
    }

    @DexIgnore
    public final HeartRateSample copy(String str, float f, Date date2, long j, long j2, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, int i4, Resting resting2) {
        Wg6.c(str, "id");
        Wg6.c(date2, "date");
        Wg6.c(dateTime, SampleRaw.COLUMN_END_TIME);
        Wg6.c(dateTime2, SampleRaw.COLUMN_START_TIME);
        return new HeartRateSample(str, f, date2, j, j2, dateTime, dateTime2, i, i2, i3, i4, resting2);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof HeartRateSample) {
                HeartRateSample heartRateSample = (HeartRateSample) obj;
                if (!(Wg6.a(this.id, heartRateSample.id) && Float.compare(this.average, heartRateSample.average) == 0 && Wg6.a(this.date, heartRateSample.date) && this.createdAt == heartRateSample.createdAt && this.updatedAt == heartRateSample.updatedAt && Wg6.a(this.endTime, heartRateSample.endTime) && Wg6.a(this.startTime, heartRateSample.startTime) && this.timezoneOffsetInSecond == heartRateSample.timezoneOffsetInSecond && this.min == heartRateSample.min && this.max == heartRateSample.max && this.minuteCount == heartRateSample.minuteCount && Wg6.a(this.resting, heartRateSample.resting))) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final float getAverage() {
        return this.average;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final int getMax() {
        return this.max;
    }

    @DexIgnore
    public final int getMin() {
        return this.min;
    }

    @DexIgnore
    public final int getMinuteCount() {
        return this.minuteCount;
    }

    @DexIgnore
    public final Resting getResting() {
        return this.resting;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime getStartTimeId() {
        return this.startTime;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        int floatToIntBits = Float.floatToIntBits(this.average);
        Date date2 = this.date;
        int hashCode2 = date2 != null ? date2.hashCode() : 0;
        int a2 = C.a(this.createdAt);
        int a3 = C.a(this.updatedAt);
        DateTime dateTime = this.endTime;
        int hashCode3 = dateTime != null ? dateTime.hashCode() : 0;
        DateTime dateTime2 = this.startTime;
        int hashCode4 = dateTime2 != null ? dateTime2.hashCode() : 0;
        int i2 = this.timezoneOffsetInSecond;
        int i3 = this.min;
        int i4 = this.max;
        int i5 = this.minuteCount;
        Resting resting2 = this.resting;
        if (resting2 != null) {
            i = resting2.hashCode();
        }
        return (((((((((((((((((((((hashCode * 31) + floatToIntBits) * 31) + hashCode2) * 31) + a2) * 31) + a3) * 31) + hashCode3) * 31) + hashCode4) * 31) + i2) * 31) + i3) * 31) + i4) * 31) + i5) * 31) + i;
    }

    @DexIgnore
    public final void setAverage(float f) {
        this.average = f;
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        Wg6.c(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setEndTime(DateTime dateTime) {
        Wg6.c(dateTime, "<set-?>");
        this.endTime = dateTime;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMax(int i) {
        this.max = i;
    }

    @DexIgnore
    public final void setMin(int i) {
        this.min = i;
    }

    @DexIgnore
    public final void setMinuteCount(int i) {
        this.minuteCount = i;
    }

    @DexIgnore
    public final void setResting(Resting resting2) {
        this.resting = resting2;
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        Wg6.c(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setStartTimeId(DateTime dateTime) {
        Wg6.c(dateTime, "value");
        this.startTime = dateTime;
        String str = this.id;
        int K = Wt7.K(str, ':', 0, false, 6, null);
        if (str != null) {
            String substring = str.substring(0, K + 1);
            Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            this.id = substring + (this.startTime.getMillis() / ((long) 1000));
            return;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final void setTimezoneOffsetInSecond(int i) {
        this.timezoneOffsetInSecond = i;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "id=" + this.id + ", average=" + this.average + ", date=" + this.date + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", endTime=" + this.endTime + ", startTime=" + this.startTime + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", min=" + this.min + ", max=" + this.max + ", minuteCount=" + this.minuteCount + ", resting=" + this.resting;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeFloat(this.average);
        parcel.writeSerializable(this.date);
        parcel.writeLong(this.createdAt);
        parcel.writeLong(this.updatedAt);
        parcel.writeSerializable(this.endTime);
        parcel.writeSerializable(this.startTime);
        parcel.writeInt(this.timezoneOffsetInSecond);
        parcel.writeInt(this.min);
        parcel.writeInt(this.max);
        parcel.writeInt(this.minuteCount);
        Resting resting2 = this.resting;
        if (resting2 != null) {
            parcel.writeSerializable(resting2);
        }
    }
}
