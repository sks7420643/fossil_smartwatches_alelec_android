package com.portfolio.platform.data.model.watchface;

import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceRing {
    @DexIgnore
    @Vu3("category")
    public String category;
    @DexIgnore
    @Vu3("createdAt")
    public String createdAt; // = "2016-01-01T01:01:01.001Z";
    @DexIgnore
    @Vu3("data")
    public Data data;
    @DexIgnore
    @Vu3("id")
    public String id;
    @DexIgnore
    @Vu3("metadata")
    public MetaData metaData;
    @DexIgnore
    @Vu3("name")
    public String name;
    @DexIgnore
    @Vu3("updatedAt")
    public String updatedAt; // = "2016-01-01T01:01:01.001Z";

    @DexIgnore
    public DianaWatchFaceRing(String str, String str2, String str3, Data data2, MetaData metaData2) {
        Wg6.c(str, "id");
        Wg6.c(str2, "category");
        Wg6.c(str3, "name");
        Wg6.c(data2, "data");
        Wg6.c(metaData2, "metaData");
        this.id = str;
        this.category = str2;
        this.name = str3;
        this.data = data2;
        this.metaData = metaData2;
    }

    @DexIgnore
    public final String getCategory() {
        return this.category;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Data getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final MetaData getMetaData() {
        return this.metaData;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final void setCategory(String str) {
        Wg6.c(str, "<set-?>");
        this.category = str;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setData(Data data2) {
        Wg6.c(data2, "<set-?>");
        this.data = data2;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMetaData(MetaData metaData2) {
        Wg6.c(metaData2, "<set-?>");
        this.metaData = metaData2;
    }

    @DexIgnore
    public final void setName(String str) {
        Wg6.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.updatedAt = str;
    }
}
