package com.portfolio.platform.data.model.room;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.mapped.Gh;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserDao_Impl implements UserDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Gh<MFUser> __deletionAdapterOfMFUser;
    @DexIgnore
    public /* final */ Hh<MFUser> __insertionAdapterOfMFUser;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAllUser;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<MFUser> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, MFUser mFUser) {
            if (mFUser.getPinType() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, mFUser.getPinType());
            }
            if (mFUser.getCreatedAt() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, mFUser.getCreatedAt());
            }
            if (mFUser.getUpdatedAt() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, mFUser.getUpdatedAt());
            }
            if (mFUser.getUserId() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, mFUser.getUserId());
            }
            if (mFUser.getAuthType() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, mFUser.getAuthType());
            }
            if (mFUser.getBirthday() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, mFUser.getBirthday());
            }
            if (mFUser.getBrand() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, mFUser.getBrand());
            }
            mi.bindLong(8, mFUser.getDiagnosticEnabled() ? 1 : 0);
            if (mFUser.getEmail() == null) {
                mi.bindNull(9);
            } else {
                mi.bindString(9, mFUser.getEmail());
            }
            mi.bindLong(10, mFUser.getEmailOptIn() ? 1 : 0);
            mi.bindLong(11, mFUser.getEmailVerified() ? 1 : 0);
            if (mFUser.getFirstName() == null) {
                mi.bindNull(12);
            } else {
                mi.bindString(12, mFUser.getFirstName());
            }
            if (mFUser.getGender() == null) {
                mi.bindNull(13);
            } else {
                mi.bindString(13, mFUser.getGender());
            }
            mi.bindLong(14, (long) mFUser.getHeightInCentimeters());
            if (mFUser.getIntegrations() == null) {
                mi.bindNull(15);
            } else {
                mi.bindString(15, mFUser.getIntegrations());
            }
            if (mFUser.getLastName() == null) {
                mi.bindNull(16);
            } else {
                mi.bindString(16, mFUser.getLastName());
            }
            if (mFUser.getProfilePicture() == null) {
                mi.bindNull(17);
            } else {
                mi.bindString(17, mFUser.getProfilePicture());
            }
            if (mFUser.getRegisterDate() == null) {
                mi.bindNull(18);
            } else {
                mi.bindString(18, mFUser.getRegisterDate());
            }
            mi.bindLong(19, mFUser.getRegistrationComplete() ? 1 : 0);
            mi.bindLong(20, mFUser.getUseDefaultBiometric() ? 1 : 0);
            mi.bindLong(21, mFUser.isOnboardingComplete() ? 1 : 0);
            mi.bindLong(22, mFUser.getUseDefaultGoals() ? 1 : 0);
            if (mFUser.getUsername() == null) {
                mi.bindNull(23);
            } else {
                mi.bindString(23, mFUser.getUsername());
            }
            mi.bindLong(24, (long) mFUser.getAverageSleep());
            mi.bindLong(25, (long) mFUser.getAverageStep());
            if (mFUser.getActiveDeviceId() == null) {
                mi.bindNull(26);
            } else {
                mi.bindString(26, mFUser.getActiveDeviceId());
            }
            mi.bindLong(27, (long) mFUser.getWeightInGrams());
            MFUser.Auth auth = mFUser.getAuth();
            if (auth != null) {
                if (auth.getAccessToken() == null) {
                    mi.bindNull(28);
                } else {
                    mi.bindString(28, auth.getAccessToken());
                }
                if (auth.getRefreshToken() == null) {
                    mi.bindNull(29);
                } else {
                    mi.bindString(29, auth.getRefreshToken());
                }
                if (auth.getAccessTokenExpiresAt() == null) {
                    mi.bindNull(30);
                } else {
                    mi.bindString(30, auth.getAccessTokenExpiresAt());
                }
                mi.bindLong(31, (long) auth.getAccessTokenExpiresIn());
            } else {
                mi.bindNull(28);
                mi.bindNull(29);
                mi.bindNull(30);
                mi.bindNull(31);
            }
            MFUser.Address addresses = mFUser.getAddresses();
            if (addresses != null) {
                if (addresses.getHome() == null) {
                    mi.bindNull(32);
                } else {
                    mi.bindString(32, addresses.getHome());
                }
                if (addresses.getWork() == null) {
                    mi.bindNull(33);
                } else {
                    mi.bindString(33, addresses.getWork());
                }
            } else {
                mi.bindNull(32);
                mi.bindNull(33);
            }
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                if (unitGroup.getDistance() == null) {
                    mi.bindNull(34);
                } else {
                    mi.bindString(34, unitGroup.getDistance());
                }
                if (unitGroup.getWeight() == null) {
                    mi.bindNull(35);
                } else {
                    mi.bindString(35, unitGroup.getWeight());
                }
                if (unitGroup.getHeight() == null) {
                    mi.bindNull(36);
                } else {
                    mi.bindString(36, unitGroup.getHeight());
                }
                if (unitGroup.getTemperature() == null) {
                    mi.bindNull(37);
                } else {
                    mi.bindString(37, unitGroup.getTemperature());
                }
            } else {
                mi.bindNull(34);
                mi.bindNull(35);
                mi.bindNull(36);
                mi.bindNull(37);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, MFUser mFUser) {
            bind(mi, mFUser);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `user` (`pinType`,`createdAt`,`updatedAt`,`uid`,`authType`,`birthday`,`brand`,`diagnosticEnabled`,`email`,`emailOptIn`,`emailVerified`,`firstName`,`gender`,`heightInCentimeters`,`integrations`,`lastName`,`profilePicture`,`registerDate`,`registrationComplete`,`useDefaultBiometric`,`isOnboardingComplete`,`useDefaultGoals`,`username`,`averageSleep`,`averageStep`,`activeDeviceId`,`weightInGrams`,`userAccessToken`,`refreshToken`,`accessTokenExpiresAt`,`accessTokenExpiresIn`,`home`,`work`,`distanceUnit`,`weightUnit`,`heightUnit`,`temperatureUnit`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Gh<MFUser> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, MFUser mFUser) {
            if (mFUser.getUserId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, mFUser.getUserId());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Gh
        public /* bridge */ /* synthetic */ void bind(Mi mi, MFUser mFUser) {
            bind(mi, mFUser);
        }

        @DexIgnore
        @Override // com.mapped.Vh, com.mapped.Gh
        public String createQuery() {
            return "DELETE FROM `user` WHERE `uid` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM user";
        }
    }

    @DexIgnore
    public UserDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfMFUser = new Anon1(oh);
        this.__deletionAdapterOfMFUser = new Anon2(oh);
        this.__preparedStmtOfClearAllUser = new Anon3(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDao
    public void clearAllUser() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAllUser.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllUser.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDao
    public void deleteUser(MFUser mFUser) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfMFUser.handle(mFUser);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDao
    public MFUser getCurrentUser() {
        Throwable th;
        MFUser mFUser;
        Rh f = Rh.f("SELECT * FROM user", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "createdAt");
            int c3 = Dx0.c(b, "updatedAt");
            int c4 = Dx0.c(b, "uid");
            int c5 = Dx0.c(b, Constants.PROFILE_KEY_AUTHTYPE);
            int c6 = Dx0.c(b, Constants.PROFILE_KEY_BIRTHDAY);
            int c7 = Dx0.c(b, Constants.PROFILE_KEY_BRAND);
            int c8 = Dx0.c(b, Constants.PROFILE_KEY_DIAGNOSTIC_ENABLE);
            int c9 = Dx0.c(b, Constants.EMAIL);
            int c10 = Dx0.c(b, Constants.PROFILE_KEY_EMAIL_OPT_IN);
            int c11 = Dx0.c(b, "emailVerified");
            int c12 = Dx0.c(b, Constants.PROFILE_KEY_FIRST_NAME);
            int c13 = Dx0.c(b, "gender");
            int c14 = Dx0.c(b, Constants.PROFILE_KEY_HEIGHT_IN_CM);
            try {
                int c15 = Dx0.c(b, Constants.PROFILE_KEY_INTEGRATIONS);
                int c16 = Dx0.c(b, Constants.PROFILE_KEY_LAST_NAME);
                int c17 = Dx0.c(b, Constants.PROFILE_KEY_PROFILE_PIC);
                int c18 = Dx0.c(b, Constants.PROFILE_KEY_REGISTER_DATE);
                int c19 = Dx0.c(b, Constants.PROFILE_KEY_REGISTRATION_COMPLETE);
                int c20 = Dx0.c(b, "useDefaultBiometric");
                int c21 = Dx0.c(b, Constants.PROFILE_KEY_IS_ONBOARDING_COMPLETE);
                int c22 = Dx0.c(b, "useDefaultGoals");
                int c23 = Dx0.c(b, "username");
                int c24 = Dx0.c(b, "averageSleep");
                int c25 = Dx0.c(b, "averageStep");
                int c26 = Dx0.c(b, "activeDeviceId");
                int c27 = Dx0.c(b, Constants.PROFILE_KEY_WEIGHT_IN_GRAMS);
                int c28 = Dx0.c(b, "userAccessToken");
                int c29 = Dx0.c(b, Constants.PROFILE_KEY_REFRESH_TOKEN);
                int c30 = Dx0.c(b, "accessTokenExpiresAt");
                int c31 = Dx0.c(b, "accessTokenExpiresIn");
                int c32 = Dx0.c(b, "home");
                int c33 = Dx0.c(b, "work");
                int c34 = Dx0.c(b, "distanceUnit");
                int c35 = Dx0.c(b, "weightUnit");
                int c36 = Dx0.c(b, "heightUnit");
                int c37 = Dx0.c(b, "temperatureUnit");
                if (b.moveToFirst()) {
                    String string = b.getString(c4);
                    String string2 = b.getString(c5);
                    String string3 = b.getString(c6);
                    String string4 = b.getString(c7);
                    boolean z = b.getInt(c8) != 0;
                    String string5 = b.getString(c9);
                    boolean z2 = b.getInt(c10) != 0;
                    boolean z3 = b.getInt(c11) != 0;
                    String string6 = b.getString(c12);
                    String string7 = b.getString(c13);
                    int i = b.getInt(c14);
                    String string8 = b.getString(c15);
                    String string9 = b.getString(c16);
                    String string10 = b.getString(c17);
                    String string11 = b.getString(c18);
                    boolean z4 = b.getInt(c19) != 0;
                    boolean z5 = b.getInt(c20) != 0;
                    boolean z6 = b.getInt(c21) != 0;
                    boolean z7 = b.getInt(c22) != 0;
                    String string12 = b.getString(c23);
                    int i2 = b.getInt(c24);
                    int i3 = b.getInt(c25);
                    String string13 = b.getString(c26);
                    int i4 = b.getInt(c27);
                    MFUser.Auth auth = (!b.isNull(c28) || !b.isNull(c29) || !b.isNull(c30) || !b.isNull(c31)) ? new MFUser.Auth(b.getString(c28), b.getString(c29), b.getString(c30), b.getInt(c31)) : null;
                    mFUser = new MFUser(string, (!b.isNull(c32) || !b.isNull(c33)) ? new MFUser.Address(b.getString(c32), b.getString(c33)) : null, string2, string3, string4, z, string5, z2, z3, string6, string7, i, string8, string9, string10, string11, z4, (!b.isNull(c34) || !b.isNull(c35) || !b.isNull(c36) || !b.isNull(c37)) ? new MFUser.UnitGroup(b.getString(c34), b.getString(c35), b.getString(c36), b.getString(c37)) : null, z5, z6, z7, string12, i2, i3, string13, i4);
                    mFUser.setPinType(b.getString(c));
                    mFUser.setCreatedAt(b.getString(c2));
                    mFUser.setUpdatedAt(b.getString(c3));
                    mFUser.setAuth(auth);
                } else {
                    mFUser = null;
                }
                b.close();
                f.m();
                return mFUser;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDao
    public void insertUser(MFUser mFUser) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFUser.insert((Hh<MFUser>) mFUser);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.model.room.UserDao
    public void updateUser(MFUser mFUser) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFUser.insert((Hh<MFUser>) mFUser);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
