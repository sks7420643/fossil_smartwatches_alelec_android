package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.Im7;
import com.fossil.Pm7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.mapped.Lc6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.portfolio.platform.data.ServerFitnessDataWrapper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDataWrapperKt {
    @DexIgnore
    public static final Lc6<Date, Date> calculateRangeDownload(List<FitnessDataWrapper> list, Date date, Date date2) {
        Wg6.c(list, "$this$calculateRangeDownload");
        Wg6.c(date, GoalPhase.COLUMN_START_DATE);
        Wg6.c(date2, GoalPhase.COLUMN_END_DATE);
        if (list.isEmpty()) {
            return new Lc6<>(date, date2);
        }
        if (!TimeUtils.m0(((FitnessDataWrapper) Pm7.P(list)).getStartTimeTZ().toDate(), date2)) {
            return new Lc6<>(((FitnessDataWrapper) Pm7.P(list)).getStartTimeTZ().toDate(), date2);
        }
        if (TimeUtils.m0(((FitnessDataWrapper) Pm7.F(list)).getStartTimeTZ().toDate(), date)) {
            return null;
        }
        return new Lc6<>(date, TimeUtils.P(((FitnessDataWrapper) Pm7.F(list)).getStartTimeTZ().toDate()));
    }

    @DexIgnore
    public static final List<ServerFitnessDataWrapper> toServerFitnessDataWrapperList(List<FitnessDataWrapper> list) {
        Wg6.c(list, "$this$toServerFitnessDataWrapperList");
        ArrayList arrayList = new ArrayList(Im7.m(list, 10));
        for (T t : list) {
            arrayList.add(new ServerFitnessDataWrapper(t.getStartTime(), t.getEndTime(), t.getSyncTime(), t.getTimezoneOffsetInSecond(), t.getSerialNumber(), t.getStep(), t.getActiveMinute(), t.getCalorie(), t.getDistance(), t.getStress(), t.getResting(), t.getHeartRate(), t.getSleeps(), WorkoutSessionWrapperKt.toServerWorkoutSessionWrapperList(t.getWorkouts()), t.getGoalTrackings()));
        }
        return arrayList;
    }
}
