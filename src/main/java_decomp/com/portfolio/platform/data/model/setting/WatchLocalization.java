package com.portfolio.platform.data.model.setting;

import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchLocalization {
    @DexIgnore
    @Vu3("category")
    public String category;
    @DexIgnore
    @Vu3("data")
    public Data data;
    @DexIgnore
    @Vu3("id")
    public String id;
    @DexIgnore
    @Vu3("metadata")
    public MetaData metaData;
    @DexIgnore
    @Vu3("name")
    public String name;

    @DexIgnore
    public WatchLocalization(String str, String str2, String str3, Data data2, MetaData metaData2) {
        Wg6.c(str, "id");
        Wg6.c(str2, "category");
        Wg6.c(str3, "name");
        Wg6.c(data2, "data");
        Wg6.c(metaData2, "metaData");
        this.id = str;
        this.category = str2;
        this.name = str3;
        this.data = data2;
        this.metaData = metaData2;
    }

    @DexIgnore
    public static /* synthetic */ WatchLocalization copy$default(WatchLocalization watchLocalization, String str, String str2, String str3, Data data2, MetaData metaData2, int i, Object obj) {
        return watchLocalization.copy((i & 1) != 0 ? watchLocalization.id : str, (i & 2) != 0 ? watchLocalization.category : str2, (i & 4) != 0 ? watchLocalization.name : str3, (i & 8) != 0 ? watchLocalization.data : data2, (i & 16) != 0 ? watchLocalization.metaData : metaData2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.category;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final Data component4() {
        return this.data;
    }

    @DexIgnore
    public final MetaData component5() {
        return this.metaData;
    }

    @DexIgnore
    public final WatchLocalization copy(String str, String str2, String str3, Data data2, MetaData metaData2) {
        Wg6.c(str, "id");
        Wg6.c(str2, "category");
        Wg6.c(str3, "name");
        Wg6.c(data2, "data");
        Wg6.c(metaData2, "metaData");
        return new WatchLocalization(str, str2, str3, data2, metaData2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WatchLocalization) {
                WatchLocalization watchLocalization = (WatchLocalization) obj;
                if (!Wg6.a(this.id, watchLocalization.id) || !Wg6.a(this.category, watchLocalization.category) || !Wg6.a(this.name, watchLocalization.name) || !Wg6.a(this.data, watchLocalization.data) || !Wg6.a(this.metaData, watchLocalization.metaData)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getCategory() {
        return this.category;
    }

    @DexIgnore
    public final Data getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final MetaData getMetaData() {
        return this.metaData;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.category;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.name;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        Data data2 = this.data;
        int hashCode4 = data2 != null ? data2.hashCode() : 0;
        MetaData metaData2 = this.metaData;
        if (metaData2 != null) {
            i = metaData2.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public final void setCategory(String str) {
        Wg6.c(str, "<set-?>");
        this.category = str;
    }

    @DexIgnore
    public final void setData(Data data2) {
        Wg6.c(data2, "<set-?>");
        this.data = data2;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMetaData(MetaData metaData2) {
        Wg6.c(metaData2, "<set-?>");
        this.metaData = metaData2;
    }

    @DexIgnore
    public final void setName(String str) {
        Wg6.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public String toString() {
        return "WatchLocalization(id=" + this.id + ", category=" + this.category + ", name=" + this.name + ", data=" + this.data + ", metaData=" + this.metaData + ")";
    }
}
