package com.portfolio.platform.data.model.diana.commutetime;

import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TrafficResponse {
    @DexIgnore
    @Vu3("distance")
    public long distance;
    @DexIgnore
    @Vu3("durationEstimation")
    public long durationEstimation;
    @DexIgnore
    @Vu3("durationInTraffic")
    public long durationInTraffic;
    @DexIgnore
    @Vu3("status")
    public String status;
    @DexIgnore
    @Vu3("trafficStatus")
    public String trafficStatus;

    @DexIgnore
    public TrafficResponse(long j, long j2, long j3, String str, String str2) {
        Wg6.c(str, "trafficStatus");
        this.durationEstimation = j;
        this.distance = j2;
        this.durationInTraffic = j3;
        this.trafficStatus = str;
        this.status = str2;
    }

    @DexIgnore
    public final long getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final long getDurationEstimation() {
        return this.durationEstimation;
    }

    @DexIgnore
    public final long getDurationInTraffic() {
        return this.durationInTraffic;
    }

    @DexIgnore
    public final String getStatus() {
        return this.status;
    }

    @DexIgnore
    public final String getTrafficStatus() {
        return this.trafficStatus;
    }

    @DexIgnore
    public final void setDistance(long j) {
        this.distance = j;
    }

    @DexIgnore
    public final void setDurationEstimation(long j) {
        this.durationEstimation = j;
    }

    @DexIgnore
    public final void setDurationInTraffic(long j) {
        this.durationInTraffic = j;
    }

    @DexIgnore
    public final void setStatus(String str) {
        this.status = str;
    }

    @DexIgnore
    public final void setTrafficStatus(String str) {
        Wg6.c(str, "<set-?>");
        this.trafficStatus = str;
    }
}
