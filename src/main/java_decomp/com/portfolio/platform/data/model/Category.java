package com.portfolio.platform.data.model;

import com.mapped.Tu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Category {
    @DexIgnore
    @Tu3
    public String createdAt;
    @DexIgnore
    @Tu3
    public String englishName;
    @DexIgnore
    @Tu3
    public String id;
    @DexIgnore
    @Tu3
    public String name;
    @DexIgnore
    @Tu3
    public int priority;
    @DexIgnore
    @Tu3
    public String updatedAt;

    @DexIgnore
    public Category(String str, String str2, String str3, String str4, String str5, int i) {
        Wg6.c(str, "id");
        Wg6.c(str2, "englishName");
        Wg6.c(str3, "name");
        Wg6.c(str4, "updatedAt");
        Wg6.c(str5, "createdAt");
        this.id = str;
        this.englishName = str2;
        this.name = str3;
        this.updatedAt = str4;
        this.createdAt = str5;
        this.priority = i;
    }

    @DexIgnore
    public static /* synthetic */ Category copy$default(Category category, String str, String str2, String str3, String str4, String str5, int i, int i2, Object obj) {
        return category.copy((i2 & 1) != 0 ? category.id : str, (i2 & 2) != 0 ? category.englishName : str2, (i2 & 4) != 0 ? category.name : str3, (i2 & 8) != 0 ? category.updatedAt : str4, (i2 & 16) != 0 ? category.createdAt : str5, (i2 & 32) != 0 ? category.priority : i);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.englishName;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final String component4() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String component5() {
        return this.createdAt;
    }

    @DexIgnore
    public final int component6() {
        return this.priority;
    }

    @DexIgnore
    public final Category copy(String str, String str2, String str3, String str4, String str5, int i) {
        Wg6.c(str, "id");
        Wg6.c(str2, "englishName");
        Wg6.c(str3, "name");
        Wg6.c(str4, "updatedAt");
        Wg6.c(str5, "createdAt");
        return new Category(str, str2, str3, str4, str5, i);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Category) {
                Category category = (Category) obj;
                if (!Wg6.a(this.id, category.id) || !Wg6.a(this.englishName, category.englishName) || !Wg6.a(this.name, category.name) || !Wg6.a(this.updatedAt, category.updatedAt) || !Wg6.a(this.createdAt, category.createdAt) || this.priority != category.priority) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getEnglishName() {
        return this.englishName;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final int getPriority() {
        return this.priority;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.englishName;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.name;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.updatedAt;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.createdAt;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return (((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i) * 31) + this.priority;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setEnglishName(String str) {
        Wg6.c(str, "<set-?>");
        this.englishName = str;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        Wg6.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setPriority(int i) {
        this.priority = i;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "Category(id=" + this.id + ", englishName=" + this.englishName + ", name=" + this.name + ", updatedAt=" + this.updatedAt + ", createdAt=" + this.createdAt + ", priority=" + this.priority + ")";
    }
}
