package com.portfolio.platform.data.model.room.fitness;

import com.fossil.wearables.fsl.fitness.SampleDay;
import com.mapped.B;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummary {
    @DexIgnore
    public int activeTime;
    @DexIgnore
    public int activeTimeGoal;
    @DexIgnore
    public double calories;
    @DexIgnore
    public int caloriesGoal;
    @DexIgnore
    public DateTime createdAt;
    @DexIgnore
    public int day;
    @DexIgnore
    public double distance;
    @DexIgnore
    public Integer dstOffset;
    @DexIgnore
    public List<Integer> intensities;
    @DexIgnore
    public int month;
    @DexIgnore
    public int pinType;
    @DexIgnore
    public int stepGoal;
    @DexIgnore
    public double steps;
    @DexIgnore
    public String timezoneName;
    @DexIgnore
    public TotalValuesOfWeek totalValuesOfWeek;
    @DexIgnore
    public DateTime updatedAt;
    @DexIgnore
    public int year;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class TotalValuesOfWeek {
        @DexIgnore
        public int totalActiveTimeOfWeek;
        @DexIgnore
        public double totalCaloriesOfWeek;
        @DexIgnore
        public double totalStepsOfWeek;

        @DexIgnore
        public TotalValuesOfWeek(double d, double d2, int i) {
            this.totalStepsOfWeek = d;
            this.totalCaloriesOfWeek = d2;
            this.totalActiveTimeOfWeek = i;
        }

        @DexIgnore
        public static /* synthetic */ TotalValuesOfWeek copy$default(TotalValuesOfWeek totalValuesOfWeek, double d, double d2, int i, int i2, Object obj) {
            return totalValuesOfWeek.copy((i2 & 1) != 0 ? totalValuesOfWeek.totalStepsOfWeek : d, (i2 & 2) != 0 ? totalValuesOfWeek.totalCaloriesOfWeek : d2, (i2 & 4) != 0 ? totalValuesOfWeek.totalActiveTimeOfWeek : i);
        }

        @DexIgnore
        public final double component1() {
            return this.totalStepsOfWeek;
        }

        @DexIgnore
        public final double component2() {
            return this.totalCaloriesOfWeek;
        }

        @DexIgnore
        public final int component3() {
            return this.totalActiveTimeOfWeek;
        }

        @DexIgnore
        public final TotalValuesOfWeek copy(double d, double d2, int i) {
            return new TotalValuesOfWeek(d, d2, i);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof TotalValuesOfWeek) {
                    TotalValuesOfWeek totalValuesOfWeek = (TotalValuesOfWeek) obj;
                    if (!(Double.compare(this.totalStepsOfWeek, totalValuesOfWeek.totalStepsOfWeek) == 0 && Double.compare(this.totalCaloriesOfWeek, totalValuesOfWeek.totalCaloriesOfWeek) == 0 && this.totalActiveTimeOfWeek == totalValuesOfWeek.totalActiveTimeOfWeek)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final int getTotalActiveTimeOfWeek() {
            return this.totalActiveTimeOfWeek;
        }

        @DexIgnore
        public final double getTotalCaloriesOfWeek() {
            return this.totalCaloriesOfWeek;
        }

        @DexIgnore
        public final double getTotalStepsOfWeek() {
            return this.totalStepsOfWeek;
        }

        @DexIgnore
        public int hashCode() {
            return (((B.a(this.totalStepsOfWeek) * 31) + B.a(this.totalCaloriesOfWeek)) * 31) + this.totalActiveTimeOfWeek;
        }

        @DexIgnore
        public final void setTotalActiveTimeOfWeek(int i) {
            this.totalActiveTimeOfWeek = i;
        }

        @DexIgnore
        public final void setTotalCaloriesOfWeek(double d) {
            this.totalCaloriesOfWeek = d;
        }

        @DexIgnore
        public final void setTotalStepsOfWeek(double d) {
            this.totalStepsOfWeek = d;
        }

        @DexIgnore
        public String toString() {
            return "TotalValuesOfWeek(totalStepsOfWeek=" + this.totalStepsOfWeek + ", totalCaloriesOfWeek=" + this.totalCaloriesOfWeek + ", totalActiveTimeOfWeek=" + this.totalActiveTimeOfWeek + ")";
        }
    }

    @DexIgnore
    public ActivitySummary(int i, int i2, int i3, String str, Integer num, double d, double d2, double d3, List<Integer> list, int i4, int i5, int i6, int i7) {
        Wg6.c(list, SampleDay.COLUMN_INTENSITIES);
        this.year = i;
        this.month = i2;
        this.day = i3;
        this.timezoneName = str;
        this.dstOffset = num;
        this.steps = d;
        this.calories = d2;
        this.distance = d3;
        this.intensities = list;
        this.activeTime = i4;
        this.stepGoal = i5;
        this.caloriesGoal = i6;
        this.activeTimeGoal = i7;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ActivitySummary(int i, int i2, int i3, String str, Integer num, double d, double d2, double d3, List list, int i4, int i5, int i6, int i7, int i8, Qg6 qg6) {
        this(i, i2, i3, str, num, d, d2, d3, list, (i8 & 512) != 0 ? 0 : i4, (i8 & 1024) != 0 ? 0 : i5, (i8 & 2048) != 0 ? 0 : i6, (i8 & 4096) != 0 ? 0 : i7);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivitySummary(ActivitySummary activitySummary) {
        this(activitySummary.year, activitySummary.month, activitySummary.day, activitySummary.timezoneName, activitySummary.dstOffset, activitySummary.steps, activitySummary.calories, activitySummary.distance, activitySummary.intensities, activitySummary.activeTime, activitySummary.stepGoal, activitySummary.caloriesGoal, activitySummary.activeTimeGoal);
        Wg6.c(activitySummary, "activitySummary");
        this.createdAt = activitySummary.createdAt;
        this.updatedAt = activitySummary.updatedAt;
        this.pinType = activitySummary.pinType;
        this.totalValuesOfWeek = activitySummary.totalValuesOfWeek;
    }

    @DexIgnore
    public static /* synthetic */ ActivitySummary copy$default(ActivitySummary activitySummary, int i, int i2, int i3, String str, Integer num, double d, double d2, double d3, List list, int i4, int i5, int i6, int i7, int i8, Object obj) {
        return activitySummary.copy((i8 & 1) != 0 ? activitySummary.year : i, (i8 & 2) != 0 ? activitySummary.month : i2, (i8 & 4) != 0 ? activitySummary.day : i3, (i8 & 8) != 0 ? activitySummary.timezoneName : str, (i8 & 16) != 0 ? activitySummary.dstOffset : num, (i8 & 32) != 0 ? activitySummary.steps : d, (i8 & 64) != 0 ? activitySummary.calories : d2, (i8 & 128) != 0 ? activitySummary.distance : d3, (i8 & 256) != 0 ? activitySummary.intensities : list, (i8 & 512) != 0 ? activitySummary.activeTime : i4, (i8 & 1024) != 0 ? activitySummary.stepGoal : i5, (i8 & 2048) != 0 ? activitySummary.caloriesGoal : i6, (i8 & 4096) != 0 ? activitySummary.activeTimeGoal : i7);
    }

    @DexIgnore
    public final int component1() {
        return this.year;
    }

    @DexIgnore
    public final int component10() {
        return this.activeTime;
    }

    @DexIgnore
    public final int component11() {
        return this.stepGoal;
    }

    @DexIgnore
    public final int component12() {
        return this.caloriesGoal;
    }

    @DexIgnore
    public final int component13() {
        return this.activeTimeGoal;
    }

    @DexIgnore
    public final int component2() {
        return this.month;
    }

    @DexIgnore
    public final int component3() {
        return this.day;
    }

    @DexIgnore
    public final String component4() {
        return this.timezoneName;
    }

    @DexIgnore
    public final Integer component5() {
        return this.dstOffset;
    }

    @DexIgnore
    public final double component6() {
        return this.steps;
    }

    @DexIgnore
    public final double component7() {
        return this.calories;
    }

    @DexIgnore
    public final double component8() {
        return this.distance;
    }

    @DexIgnore
    public final List<Integer> component9() {
        return this.intensities;
    }

    @DexIgnore
    public final ActivitySummary copy(int i, int i2, int i3, String str, Integer num, double d, double d2, double d3, List<Integer> list, int i4, int i5, int i6, int i7) {
        Wg6.c(list, SampleDay.COLUMN_INTENSITIES);
        return new ActivitySummary(i, i2, i3, str, num, d, d2, d3, list, i4, i5, i6, i7);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ActivitySummary) {
                ActivitySummary activitySummary = (ActivitySummary) obj;
                if (!(this.year == activitySummary.year && this.month == activitySummary.month && this.day == activitySummary.day && Wg6.a(this.timezoneName, activitySummary.timezoneName) && Wg6.a(this.dstOffset, activitySummary.dstOffset) && Double.compare(this.steps, activitySummary.steps) == 0 && Double.compare(this.calories, activitySummary.calories) == 0 && Double.compare(this.distance, activitySummary.distance) == 0 && Wg6.a(this.intensities, activitySummary.intensities) && this.activeTime == activitySummary.activeTime && this.stepGoal == activitySummary.stepGoal && this.caloriesGoal == activitySummary.caloriesGoal && this.activeTimeGoal == activitySummary.activeTimeGoal)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getActiveTime() {
        return this.activeTime;
    }

    @DexIgnore
    public final int getActiveTimeGoal() {
        return this.activeTimeGoal;
    }

    @DexIgnore
    public final double getCalories() {
        return this.calories;
    }

    @DexIgnore
    public final int getCaloriesGoal() {
        return this.caloriesGoal;
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        Calendar instance = Calendar.getInstance();
        instance.set(this.year, this.month - 1, this.day);
        Wg6.b(instance, "calendar");
        Date time = instance.getTime();
        Wg6.b(time, "calendar.time");
        return time;
    }

    @DexIgnore
    public final int getDay() {
        return this.day;
    }

    @DexIgnore
    public final double getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final Integer getDstOffset() {
        return this.dstOffset;
    }

    @DexIgnore
    public final List<Integer> getIntensities() {
        return this.intensities;
    }

    @DexIgnore
    public final int getMonth() {
        return this.month;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getStepGoal() {
        return this.stepGoal;
    }

    @DexIgnore
    public final double getSteps() {
        return this.steps;
    }

    @DexIgnore
    public final String getTimezoneName() {
        return this.timezoneName;
    }

    @DexIgnore
    public final TotalValuesOfWeek getTotalValuesOfWeek() {
        return this.totalValuesOfWeek;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final int getYear() {
        return this.year;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int i2 = this.year;
        int i3 = this.month;
        int i4 = this.day;
        String str = this.timezoneName;
        int hashCode = str != null ? str.hashCode() : 0;
        Integer num = this.dstOffset;
        int hashCode2 = num != null ? num.hashCode() : 0;
        int a2 = B.a(this.steps);
        int a3 = B.a(this.calories);
        int a4 = B.a(this.distance);
        List<Integer> list = this.intensities;
        if (list != null) {
            i = list.hashCode();
        }
        return ((((((((((((((((((hashCode + (((((i2 * 31) + i3) * 31) + i4) * 31)) * 31) + hashCode2) * 31) + a2) * 31) + a3) * 31) + a4) * 31) + i) * 31) + this.activeTime) * 31) + this.stepGoal) * 31) + this.caloriesGoal) * 31) + this.activeTimeGoal;
    }

    @DexIgnore
    public final void setActiveTime(int i) {
        this.activeTime = i;
    }

    @DexIgnore
    public final void setActiveTimeGoal(int i) {
        this.activeTimeGoal = i;
    }

    @DexIgnore
    public final void setCalories(double d) {
        this.calories = d;
    }

    @DexIgnore
    public final void setCaloriesGoal(int i) {
        this.caloriesGoal = i;
    }

    @DexIgnore
    public final void setCreatedAt(DateTime dateTime) {
        this.createdAt = dateTime;
    }

    @DexIgnore
    public final void setDay(int i) {
        this.day = i;
    }

    @DexIgnore
    public final void setDistance(double d) {
        this.distance = d;
    }

    @DexIgnore
    public final void setDstOffset(Integer num) {
        this.dstOffset = num;
    }

    @DexIgnore
    public final void setIntensities(List<Integer> list) {
        Wg6.c(list, "<set-?>");
        this.intensities = list;
    }

    @DexIgnore
    public final void setMonth(int i) {
        this.month = i;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setStepGoal(int i) {
        this.stepGoal = i;
    }

    @DexIgnore
    public final void setSteps(double d) {
        this.steps = d;
    }

    @DexIgnore
    public final void setTimezoneName(String str) {
        this.timezoneName = str;
    }

    @DexIgnore
    public final void setTotalValuesOfWeek(TotalValuesOfWeek totalValuesOfWeek2) {
        this.totalValuesOfWeek = totalValuesOfWeek2;
    }

    @DexIgnore
    public final void setUpdatedAt(DateTime dateTime) {
        this.updatedAt = dateTime;
    }

    @DexIgnore
    public final void setYear(int i) {
        this.year = i;
    }

    @DexIgnore
    public String toString() {
        return "ActivitySummary(year=" + this.year + ", month=" + this.month + ", day=" + this.day + ", timezoneName=" + this.timezoneName + ", dstOffset=" + this.dstOffset + ", steps=" + this.steps + ", calories=" + this.calories + ", distance=" + this.distance + ", intensities=" + this.intensities + ", activeTime=" + this.activeTime + ", stepGoal=" + this.stepGoal + ", caloriesGoal=" + this.caloriesGoal + ", activeTimeGoal=" + this.activeTimeGoal + ")";
    }
}
