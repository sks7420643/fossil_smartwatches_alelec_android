package com.portfolio.platform.data.model.room;

import com.mapped.Xh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserDatabase$Companion$MIGRATION_FROM_4_TO_5$Anon1 extends Xh {
    @DexIgnore
    public UserDatabase$Companion$MIGRATION_FROM_4_TO_5$Anon1(int i, int i2) {
        super(i, i2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:186:0x05e9, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x05ea, code lost:
        r41 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x05f4, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x05f5, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().e(com.portfolio.platform.data.model.room.UserDatabase.TAG, "Migration 4 to 5 Exception when add column emailVerified- " + r2.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:275:0x08b6, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:276:0x08b8, code lost:
        r2 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:277:0x08bb, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:278:0x08bd, code lost:
        r2 = e;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x05e9 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x001b] */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x05ee  */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x064d  */
    /* JADX WARNING: Removed duplicated region for block: B:276:0x08b8 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:170:0x03d3] */
    @Override // com.mapped.Xh
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void migrate(com.fossil.Lx0 r43) {
        /*
        // Method dump skipped, instructions count: 2243
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.room.UserDatabase$Companion$MIGRATION_FROM_4_TO_5$Anon1.migrate(com.fossil.Lx0):void");
    }
}
