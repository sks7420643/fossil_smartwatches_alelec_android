package com.portfolio.platform.data.model.diana.workout;

import com.fossil.Gi5;
import com.fossil.Ji5;
import com.fossil.Mi5;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.mapped.C;
import com.mapped.Pj4;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSession {
    @DexIgnore
    public /* final */ WorkoutCadence cadence;
    @DexIgnore
    public /* final */ WorkoutCalorie calorie;
    @DexIgnore
    public /* final */ long createdAt;
    @DexIgnore
    public /* final */ Date date;
    @DexIgnore
    public /* final */ String deviceSerialNumber;
    @DexIgnore
    public /* final */ WorkoutDistance distance;
    @DexIgnore
    public /* final */ int duration;
    @DexIgnore
    public DateTime editedEndTime;
    @DexIgnore
    public Gi5 editedMode;
    @DexIgnore
    public DateTime editedStartTime;
    @DexIgnore
    public Mi5 editedType;
    @DexIgnore
    public /* final */ DateTime endTime;
    @DexIgnore
    public /* final */ String gpsDataPoints;
    @DexIgnore
    public /* final */ WorkoutHeartRate heartRate;
    @DexIgnore
    public /* final */ String id;
    @DexIgnore
    public /* final */ Gi5 mode;
    @DexIgnore
    public /* final */ WorkoutPace pace;
    @DexIgnore
    @Pj4
    public String screenShotUri;
    @DexIgnore
    public /* final */ Ji5 sourceType;
    @DexIgnore
    public WorkoutSpeed speed;
    @DexIgnore
    public /* final */ DateTime startTime;
    @DexIgnore
    public List<WorkoutStateChange> states;
    @DexIgnore
    public /* final */ WorkoutStep step;
    @DexIgnore
    public /* final */ int timezoneOffsetInSecond;
    @DexIgnore
    public /* final */ long updatedAt;
    @DexIgnore
    public /* final */ List<WorkoutGpsPoint> workoutGpsPoints;
    @DexIgnore
    public /* final */ Mi5 workoutType;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public WorkoutSession(com.portfolio.platform.data.model.fitnessdata.WorkoutSessionWrapper r30, java.lang.String r31, java.lang.String r32) {
        /*
        // Method dump skipped, instructions count: 343
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.diana.workout.WorkoutSession.<init>(com.portfolio.platform.data.model.fitnessdata.WorkoutSessionWrapper, java.lang.String, java.lang.String):void");
    }

    @DexIgnore
    public WorkoutSession(String str, Date date2, DateTime dateTime, DateTime dateTime2, String str2, WorkoutStep workoutStep, WorkoutCalorie workoutCalorie, WorkoutDistance workoutDistance, WorkoutHeartRate workoutHeartRate, Ji5 ji5, Mi5 mi5, int i, int i2, long j, long j2, List<WorkoutGpsPoint> list, String str3, Gi5 gi5, WorkoutPace workoutPace, WorkoutCadence workoutCadence, DateTime dateTime3, DateTime dateTime4, Mi5 mi52, Gi5 gi52) {
        Wg6.c(str, "id");
        Wg6.c(date2, "date");
        Wg6.c(dateTime, SampleRaw.COLUMN_START_TIME);
        Wg6.c(dateTime2, SampleRaw.COLUMN_END_TIME);
        Wg6.c(dateTime3, MFSleepSession.COLUMN_EDITED_START_TIME);
        Wg6.c(dateTime4, MFSleepSession.COLUMN_EDITED_END_TIME);
        this.id = str;
        this.date = date2;
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.deviceSerialNumber = str2;
        this.step = workoutStep;
        this.calorie = workoutCalorie;
        this.distance = workoutDistance;
        this.heartRate = workoutHeartRate;
        this.sourceType = ji5;
        this.workoutType = mi5;
        this.timezoneOffsetInSecond = i;
        this.duration = i2;
        this.createdAt = j;
        this.updatedAt = j2;
        this.workoutGpsPoints = list;
        this.gpsDataPoints = str3;
        this.mode = gi5;
        this.pace = workoutPace;
        this.cadence = workoutCadence;
        this.editedStartTime = dateTime3;
        this.editedEndTime = dateTime4;
        this.editedType = mi52;
        this.editedMode = gi52;
        this.speed = workoutDistance != null ? workoutDistance.calculateSpeed() : null;
        this.states = new ArrayList();
    }

    @DexIgnore
    public static /* synthetic */ WorkoutSession copy$default(WorkoutSession workoutSession, String str, Date date2, DateTime dateTime, DateTime dateTime2, String str2, WorkoutStep workoutStep, WorkoutCalorie workoutCalorie, WorkoutDistance workoutDistance, WorkoutHeartRate workoutHeartRate, Ji5 ji5, Mi5 mi5, int i, int i2, long j, long j2, List list, String str3, Gi5 gi5, WorkoutPace workoutPace, WorkoutCadence workoutCadence, DateTime dateTime3, DateTime dateTime4, Mi5 mi52, Gi5 gi52, int i3, Object obj) {
        return workoutSession.copy((i3 & 1) != 0 ? workoutSession.id : str, (i3 & 2) != 0 ? workoutSession.date : date2, (i3 & 4) != 0 ? workoutSession.startTime : dateTime, (i3 & 8) != 0 ? workoutSession.endTime : dateTime2, (i3 & 16) != 0 ? workoutSession.deviceSerialNumber : str2, (i3 & 32) != 0 ? workoutSession.step : workoutStep, (i3 & 64) != 0 ? workoutSession.calorie : workoutCalorie, (i3 & 128) != 0 ? workoutSession.distance : workoutDistance, (i3 & 256) != 0 ? workoutSession.heartRate : workoutHeartRate, (i3 & 512) != 0 ? workoutSession.sourceType : ji5, (i3 & 1024) != 0 ? workoutSession.workoutType : mi5, (i3 & 2048) != 0 ? workoutSession.timezoneOffsetInSecond : i, (i3 & 4096) != 0 ? workoutSession.duration : i2, (i3 & 8192) != 0 ? workoutSession.createdAt : j, (i3 & 16384) != 0 ? workoutSession.updatedAt : j2, (32768 & i3) != 0 ? workoutSession.workoutGpsPoints : list, (65536 & i3) != 0 ? workoutSession.gpsDataPoints : str3, (131072 & i3) != 0 ? workoutSession.mode : gi5, (262144 & i3) != 0 ? workoutSession.pace : workoutPace, (524288 & i3) != 0 ? workoutSession.cadence : workoutCadence, (1048576 & i3) != 0 ? workoutSession.editedStartTime : dateTime3, (2097152 & i3) != 0 ? workoutSession.editedEndTime : dateTime4, (4194304 & i3) != 0 ? workoutSession.editedType : mi52, (8388608 & i3) != 0 ? workoutSession.editedMode : gi52);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final Ji5 component10() {
        return this.sourceType;
    }

    @DexIgnore
    public final Mi5 component11() {
        return this.workoutType;
    }

    @DexIgnore
    public final int component12() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int component13() {
        return this.duration;
    }

    @DexIgnore
    public final long component14() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component15() {
        return this.updatedAt;
    }

    @DexIgnore
    public final List<WorkoutGpsPoint> component16() {
        return this.workoutGpsPoints;
    }

    @DexIgnore
    public final String component17() {
        return this.gpsDataPoints;
    }

    @DexIgnore
    public final Gi5 component18() {
        return this.mode;
    }

    @DexIgnore
    public final WorkoutPace component19() {
        return this.pace;
    }

    @DexIgnore
    public final Date component2() {
        return this.date;
    }

    @DexIgnore
    public final WorkoutCadence component20() {
        return this.cadence;
    }

    @DexIgnore
    public final DateTime component21() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final DateTime component22() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final Mi5 component23() {
        return this.editedType;
    }

    @DexIgnore
    public final Gi5 component24() {
        return this.editedMode;
    }

    @DexIgnore
    public final DateTime component3() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component4() {
        return this.endTime;
    }

    @DexIgnore
    public final String component5() {
        return this.deviceSerialNumber;
    }

    @DexIgnore
    public final WorkoutStep component6() {
        return this.step;
    }

    @DexIgnore
    public final WorkoutCalorie component7() {
        return this.calorie;
    }

    @DexIgnore
    public final WorkoutDistance component8() {
        return this.distance;
    }

    @DexIgnore
    public final WorkoutHeartRate component9() {
        return this.heartRate;
    }

    @DexIgnore
    public final WorkoutSession copy(String str, Date date2, DateTime dateTime, DateTime dateTime2, String str2, WorkoutStep workoutStep, WorkoutCalorie workoutCalorie, WorkoutDistance workoutDistance, WorkoutHeartRate workoutHeartRate, Ji5 ji5, Mi5 mi5, int i, int i2, long j, long j2, List<WorkoutGpsPoint> list, String str3, Gi5 gi5, WorkoutPace workoutPace, WorkoutCadence workoutCadence, DateTime dateTime3, DateTime dateTime4, Mi5 mi52, Gi5 gi52) {
        Wg6.c(str, "id");
        Wg6.c(date2, "date");
        Wg6.c(dateTime, SampleRaw.COLUMN_START_TIME);
        Wg6.c(dateTime2, SampleRaw.COLUMN_END_TIME);
        Wg6.c(dateTime3, MFSleepSession.COLUMN_EDITED_START_TIME);
        Wg6.c(dateTime4, MFSleepSession.COLUMN_EDITED_END_TIME);
        return new WorkoutSession(str, date2, dateTime, dateTime2, str2, workoutStep, workoutCalorie, workoutDistance, workoutHeartRate, ji5, mi5, i, i2, j, j2, list, str3, gi5, workoutPace, workoutCadence, dateTime3, dateTime4, mi52, gi52);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutSession) {
                WorkoutSession workoutSession = (WorkoutSession) obj;
                if (!Wg6.a(this.id, workoutSession.id) || !Wg6.a(this.date, workoutSession.date) || !Wg6.a(this.startTime, workoutSession.startTime) || !Wg6.a(this.endTime, workoutSession.endTime) || !Wg6.a(this.deviceSerialNumber, workoutSession.deviceSerialNumber) || !Wg6.a(this.step, workoutSession.step) || !Wg6.a(this.calorie, workoutSession.calorie) || !Wg6.a(this.distance, workoutSession.distance) || !Wg6.a(this.heartRate, workoutSession.heartRate) || !Wg6.a(this.sourceType, workoutSession.sourceType) || !Wg6.a(this.workoutType, workoutSession.workoutType) || this.timezoneOffsetInSecond != workoutSession.timezoneOffsetInSecond || this.duration != workoutSession.duration || this.createdAt != workoutSession.createdAt || this.updatedAt != workoutSession.updatedAt || !Wg6.a(this.workoutGpsPoints, workoutSession.workoutGpsPoints) || !Wg6.a(this.gpsDataPoints, workoutSession.gpsDataPoints) || !Wg6.a(this.mode, workoutSession.mode) || !Wg6.a(this.pace, workoutSession.pace) || !Wg6.a(this.cadence, workoutSession.cadence) || !Wg6.a(this.editedStartTime, workoutSession.editedStartTime) || !Wg6.a(this.editedEndTime, workoutSession.editedEndTime) || !Wg6.a(this.editedType, workoutSession.editedType) || !Wg6.a(this.editedMode, workoutSession.editedMode)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Float getAverageHeartRate() {
        WorkoutHeartRate workoutHeartRate = this.heartRate;
        if (workoutHeartRate != null) {
            return Float.valueOf(workoutHeartRate.getAverage());
        }
        return null;
    }

    @DexIgnore
    public final WorkoutCadence getCadence() {
        return this.cadence;
    }

    @DexIgnore
    public final WorkoutCalorie getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }

    @DexIgnore
    public final WorkoutDistance getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final int getDuration() {
        return this.duration;
    }

    @DexIgnore
    public final DateTime getEditedEndTime() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final Gi5 getEditedMode() {
        return this.editedMode;
    }

    @DexIgnore
    public final DateTime getEditedStartTime() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final Mi5 getEditedType() {
        return this.editedType;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final String getGpsDataPoints() {
        return this.gpsDataPoints;
    }

    @DexIgnore
    public final WorkoutHeartRate getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final Integer getMaxHeartRate() {
        WorkoutHeartRate workoutHeartRate = this.heartRate;
        if (workoutHeartRate != null) {
            return Integer.valueOf(workoutHeartRate.getMax());
        }
        return null;
    }

    @DexIgnore
    public final Gi5 getMode() {
        return this.mode;
    }

    @DexIgnore
    public final WorkoutPace getPace() {
        return this.pace;
    }

    @DexIgnore
    public final String getScreenShotUri() {
        return this.screenShotUri;
    }

    @DexIgnore
    public final Ji5 getSourceType() {
        return this.sourceType;
    }

    @DexIgnore
    public final WorkoutSpeed getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final List<WorkoutStateChange> getStates() {
        return this.states;
    }

    @DexIgnore
    public final WorkoutStep getStep() {
        return this.step;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final Float getTotalCalorie() {
        WorkoutCalorie workoutCalorie = this.calorie;
        if (workoutCalorie != null) {
            return Float.valueOf(workoutCalorie.getTotal());
        }
        return null;
    }

    @DexIgnore
    public final Double getTotalDistance() {
        WorkoutDistance workoutDistance = this.distance;
        if (workoutDistance != null) {
            return Double.valueOf(workoutDistance.getTotal());
        }
        return null;
    }

    @DexIgnore
    public final Integer getTotalSteps() {
        WorkoutStep workoutStep = this.step;
        if (workoutStep != null) {
            return Integer.valueOf(workoutStep.getTotal());
        }
        return null;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final List<WorkoutGpsPoint> getWorkoutGpsPoints() {
        return this.workoutGpsPoints;
    }

    @DexIgnore
    public final Mi5 getWorkoutType() {
        return this.workoutType;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = str != null ? str.hashCode() : 0;
        Date date2 = this.date;
        int hashCode2 = date2 != null ? date2.hashCode() : 0;
        DateTime dateTime = this.startTime;
        int hashCode3 = dateTime != null ? dateTime.hashCode() : 0;
        DateTime dateTime2 = this.endTime;
        int hashCode4 = dateTime2 != null ? dateTime2.hashCode() : 0;
        String str2 = this.deviceSerialNumber;
        int hashCode5 = str2 != null ? str2.hashCode() : 0;
        WorkoutStep workoutStep = this.step;
        int hashCode6 = workoutStep != null ? workoutStep.hashCode() : 0;
        WorkoutCalorie workoutCalorie = this.calorie;
        int hashCode7 = workoutCalorie != null ? workoutCalorie.hashCode() : 0;
        WorkoutDistance workoutDistance = this.distance;
        int hashCode8 = workoutDistance != null ? workoutDistance.hashCode() : 0;
        WorkoutHeartRate workoutHeartRate = this.heartRate;
        int hashCode9 = workoutHeartRate != null ? workoutHeartRate.hashCode() : 0;
        Ji5 ji5 = this.sourceType;
        int hashCode10 = ji5 != null ? ji5.hashCode() : 0;
        Mi5 mi5 = this.workoutType;
        int hashCode11 = mi5 != null ? mi5.hashCode() : 0;
        int i2 = this.timezoneOffsetInSecond;
        int i3 = this.duration;
        int a2 = C.a(this.createdAt);
        int a3 = C.a(this.updatedAt);
        List<WorkoutGpsPoint> list = this.workoutGpsPoints;
        int hashCode12 = list != null ? list.hashCode() : 0;
        String str3 = this.gpsDataPoints;
        int hashCode13 = str3 != null ? str3.hashCode() : 0;
        Gi5 gi5 = this.mode;
        int hashCode14 = gi5 != null ? gi5.hashCode() : 0;
        WorkoutPace workoutPace = this.pace;
        int hashCode15 = workoutPace != null ? workoutPace.hashCode() : 0;
        WorkoutCadence workoutCadence = this.cadence;
        int hashCode16 = workoutCadence != null ? workoutCadence.hashCode() : 0;
        DateTime dateTime3 = this.editedStartTime;
        int hashCode17 = dateTime3 != null ? dateTime3.hashCode() : 0;
        DateTime dateTime4 = this.editedEndTime;
        int hashCode18 = dateTime4 != null ? dateTime4.hashCode() : 0;
        Mi5 mi52 = this.editedType;
        int hashCode19 = mi52 != null ? mi52.hashCode() : 0;
        Gi5 gi52 = this.editedMode;
        if (gi52 != null) {
            i = gi52.hashCode();
        }
        return (((((((((((((((((((((((((((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + hashCode10) * 31) + hashCode11) * 31) + i2) * 31) + i3) * 31) + a2) * 31) + a3) * 31) + hashCode12) * 31) + hashCode13) * 31) + hashCode14) * 31) + hashCode15) * 31) + hashCode16) * 31) + hashCode17) * 31) + hashCode18) * 31) + hashCode19) * 31) + i;
    }

    @DexIgnore
    public final void setEditedEndTime(DateTime dateTime) {
        Wg6.c(dateTime, "<set-?>");
        this.editedEndTime = dateTime;
    }

    @DexIgnore
    public final void setEditedMode(Gi5 gi5) {
        this.editedMode = gi5;
    }

    @DexIgnore
    public final void setEditedStartTime(DateTime dateTime) {
        Wg6.c(dateTime, "<set-?>");
        this.editedStartTime = dateTime;
    }

    @DexIgnore
    public final void setEditedType(Mi5 mi5) {
        this.editedType = mi5;
    }

    @DexIgnore
    public final void setScreenShotUri(String str) {
        this.screenShotUri = str;
    }

    @DexIgnore
    public final void setSpeed(WorkoutSpeed workoutSpeed) {
        this.speed = workoutSpeed;
    }

    @DexIgnore
    public final void setStates(List<WorkoutStateChange> list) {
        Wg6.c(list, "<set-?>");
        this.states = list;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSession(id=" + this.id + ", date=" + this.date + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", deviceSerialNumber=" + this.deviceSerialNumber + ", step=" + this.step + ", calorie=" + this.calorie + ", distance=" + this.distance + ", heartRate=" + this.heartRate + ", sourceType=" + this.sourceType + ", workoutType=" + this.workoutType + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", duration=" + this.duration + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", workoutGpsPoints=" + this.workoutGpsPoints + ", gpsDataPoints=" + this.gpsDataPoints + ", mode=" + this.mode + ", pace=" + this.pace + ", cadence=" + this.cadence + ", editedStartTime=" + this.editedStartTime + ", editedEndTime=" + this.editedEndTime + ", editedType=" + this.editedType + ", editedMode=" + this.editedMode + ")";
    }
}
