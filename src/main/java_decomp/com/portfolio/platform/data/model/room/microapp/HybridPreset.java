package com.portfolio.platform.data.model.room.microapp;

import com.fossil.Um5;
import com.mapped.CustomizeConfigurationExt;
import com.mapped.Pj4;
import com.mapped.Qg6;
import com.mapped.TimeUtils;
import com.mapped.Uu3;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.gson.HybridPresetAppSettingSerializer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPreset {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    @Uu3(HybridPresetAppSettingSerializer.class)
    @Vu3("buttons")
    public ArrayList<HybridPresetAppSetting> buttons;
    @DexIgnore
    @Vu3("createdAt")
    public String createdAt;
    @DexIgnore
    @Vu3("id")
    public String id;
    @DexIgnore
    @Vu3("isActive")
    public boolean isActive;
    @DexIgnore
    @Vu3("name")
    public String name;
    @DexIgnore
    @Pj4
    public int pinType;
    @DexIgnore
    @Vu3("serialNumber")
    public String serialNumber;
    @DexIgnore
    @Vu3("updatedAt")
    public String updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final HybridPreset cloneFrom(HybridPreset hybridPreset) {
            Wg6.c(hybridPreset, "preset");
            String w0 = TimeUtils.w0(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            Wg6.b(uuid, "UUID.randomUUID().toString()");
            HybridPreset hybridPreset2 = new HybridPreset(uuid, Um5.c(PortfolioApp.get.instance(), 2131886539), hybridPreset.getSerialNumber(), hybridPreset.getButtons(), false);
            hybridPreset2.setCreatedAt(w0);
            hybridPreset2.setUpdatedAt(w0);
            return hybridPreset2;
        }

        @DexIgnore
        public final HybridPreset cloneFromDefault(HybridRecommendPreset hybridRecommendPreset) {
            Wg6.c(hybridRecommendPreset, "preset");
            String w0 = TimeUtils.w0(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            Wg6.b(uuid, "UUID.randomUUID().toString()");
            HybridPreset hybridPreset = new HybridPreset(uuid, hybridRecommendPreset.getName(), hybridRecommendPreset.getSerialNumber(), hybridRecommendPreset.getButtons(), hybridRecommendPreset.isDefault());
            hybridPreset.setCreatedAt(w0);
            hybridPreset.setUpdatedAt(w0);
            Iterator<T> it = hybridPreset.getButtons().iterator();
            while (it.hasNext()) {
                Wg6.b(w0, "timestamp");
                it.next().setLocalUpdateAt(w0);
            }
            return hybridPreset;
        }
    }

    @DexIgnore
    public HybridPreset(String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z) {
        Wg6.c(str, "id");
        Wg6.c(str3, "serialNumber");
        Wg6.c(arrayList, "buttons");
        this.id = str;
        this.name = str2;
        this.serialNumber = str3;
        this.buttons = arrayList;
        this.isActive = z;
        this.pinType = 1;
        this.createdAt = "";
        this.updatedAt = "";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HybridPreset(String str, String str2, String str3, ArrayList arrayList, boolean z, int i, Qg6 qg6) {
        this(str, (i & 2) != 0 ? "" : str2, str3, arrayList, z);
    }

    @DexIgnore
    public static /* synthetic */ HybridPreset copy$default(HybridPreset hybridPreset, String str, String str2, String str3, ArrayList arrayList, boolean z, int i, Object obj) {
        return hybridPreset.copy((i & 1) != 0 ? hybridPreset.id : str, (i & 2) != 0 ? hybridPreset.name : str2, (i & 4) != 0 ? hybridPreset.serialNumber : str3, (i & 8) != 0 ? hybridPreset.buttons : arrayList, (i & 16) != 0 ? hybridPreset.isActive : z);
    }

    @DexIgnore
    public final HybridPreset clone() {
        HybridPreset hybridPreset = new HybridPreset(this.id, this.name, this.serialNumber, CustomizeConfigurationExt.b(this.buttons), this.isActive);
        hybridPreset.createdAt = this.createdAt;
        hybridPreset.updatedAt = this.updatedAt;
        return hybridPreset;
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final String component3() {
        return this.serialNumber;
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> component4() {
        return this.buttons;
    }

    @DexIgnore
    public final boolean component5() {
        return this.isActive;
    }

    @DexIgnore
    public final HybridPreset copy(String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z) {
        Wg6.c(str, "id");
        Wg6.c(str3, "serialNumber");
        Wg6.c(arrayList, "buttons");
        return new HybridPreset(str, str2, str3, arrayList, z);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof HybridPreset) {
                HybridPreset hybridPreset = (HybridPreset) obj;
                if (!Wg6.a(this.id, hybridPreset.id) || !Wg6.a(this.name, hybridPreset.name) || !Wg6.a(this.serialNumber, hybridPreset.serialNumber) || !Wg6.a(this.buttons, hybridPreset.buttons) || this.isActive != hybridPreset.isActive) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> getButtons() {
        return this.buttons;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.name;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.serialNumber;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        ArrayList<HybridPresetAppSetting> arrayList = this.buttons;
        if (arrayList != null) {
            i = arrayList.hashCode();
        }
        boolean z = this.isActive;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public final boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public final void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public final void setButtons(ArrayList<HybridPresetAppSetting> arrayList) {
        Wg6.c(arrayList, "<set-?>");
        this.buttons = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        Wg6.c(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "HybridPreset(id=" + this.id + ", name=" + this.name + ", serialNumber=" + this.serialNumber + ", buttons=" + this.buttons + ", isActive=" + this.isActive + ")";
    }
}
