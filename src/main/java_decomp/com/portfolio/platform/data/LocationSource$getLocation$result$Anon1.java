package com.portfolio.platform.data;

import android.content.Context;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.LocationSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.LocationSource$getLocation$result$1", f = "LocationSource.kt", l = {71}, m = "invokeSuspend")
public final class LocationSource$getLocation$result$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super LocationSource.Result>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Context $context;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LocationSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LocationSource$getLocation$result$Anon1(LocationSource locationSource, Context context, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = locationSource;
        this.$context = context;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        LocationSource$getLocation$result$Anon1 locationSource$getLocation$result$Anon1 = new LocationSource$getLocation$result$Anon1(this.this$0, this.$context, xe6);
        locationSource$getLocation$result$Anon1.p$ = (Il6) obj;
        throw null;
        //return locationSource$getLocation$result$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super LocationSource.Result> xe6) {
        throw null;
        //return ((LocationSource$getLocation$result$Anon1) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            LocationSource locationSource = this.this$0;
            Context context = this.$context;
            this.L$0 = il6;
            this.label = 1;
            Object locationFromFuseLocationManager = locationSource.getLocationFromFuseLocationManager(context, this);
            return locationFromFuseLocationManager == d ? d : locationFromFuseLocationManager;
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
