package com.portfolio.platform.data;

import android.location.Location;
import com.fossil.Dl7;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.mapped.Lk6;
import com.mapped.Wg6;
import com.mapped.Wv2;
import com.mapped.Yv2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1 extends Yv2 {
    @DexIgnore
    public /* final */ /* synthetic */ Lk6 $continuation;
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest $locationRequest$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Wv2 $this_requestLocationUpdates$inlined;

    @DexIgnore
    public LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1(Lk6 lk6, Wv2 wv2, LocationRequest locationRequest) {
        this.$continuation = lk6;
        this.$this_requestLocationUpdates$inlined = wv2;
        this.$locationRequest$inlined = locationRequest;
    }

    @DexIgnore
    @Override // com.mapped.Yv2
    public void onLocationResult(LocationResult locationResult) {
        Wg6.c(locationResult, "locationResult");
        super.onLocationResult(locationResult);
        Location c = locationResult.c();
        if (c != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "requestLocationUpdates lastLocation=" + c);
        } else {
            FLogger.INSTANCE.getLocal().d(LocationSource.Companion.getTAG$app_fossilRelease(), "requestLocationUpdates lastLocation is null");
        }
        this.$this_requestLocationUpdates$inlined.t(this);
        if (this.$continuation.isActive()) {
            Lk6 lk6 = this.$continuation;
            Dl7.Ai ai = Dl7.Companion;
            lk6.resumeWith(Dl7.constructor-impl(c));
        }
    }
}
