package com.portfolio.platform.data;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.RingStyleRepository", f = "RingStyleRepository.kt", l = {18, 24, 45}, m = "downloadRingStyleList")
public final class RingStyleRepository$downloadRingStyleList$Anon1 extends Jf6 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public Object L$7;
    @DexIgnore
    public Object L$8;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ RingStyleRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingStyleRepository$downloadRingStyleList$Anon1(RingStyleRepository ringStyleRepository, Xe6 xe6) {
        super(xe6);
        this.this$0 = ringStyleRepository;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.downloadRingStyleList(null, false, this);
    }
}
