package com.portfolio.platform.data;

import com.fossil.Zi4;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.google.gson.Gson;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Activity extends ServerError {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "Activity";
    @DexIgnore
    @Vu3(SampleDay.COLUMN_ACTIVE_TIME)
    public /* final */ int activeTime;
    @DexIgnore
    @Vu3("calories")
    public /* final */ double calories;
    @DexIgnore
    @Vu3("createdAt")
    public /* final */ DateTime createdAt;
    @DexIgnore
    @Vu3("date")
    public /* final */ Date date;
    @DexIgnore
    @Vu3("distance")
    public /* final */ double distance;
    @DexIgnore
    @Vu3(SampleRaw.COLUMN_END_TIME)
    public /* final */ DateTime endTime;
    @DexIgnore
    @Vu3("id")
    public /* final */ String id;
    @DexIgnore
    @Vu3("intensityDistInSteps")
    public /* final */ ActivityIntensities intensityDistInSteps;
    @DexIgnore
    @Vu3(SampleRaw.COLUMN_SOURCE_ID)
    public /* final */ String sourceId;
    @DexIgnore
    @Vu3(SampleRaw.COLUMN_START_TIME)
    public /* final */ DateTime startTime;
    @DexIgnore
    @Vu3("steps")
    public /* final */ int steps;
    @DexIgnore
    @Vu3("syncTime")
    public /* final */ DateTime syncTime;
    @DexIgnore
    @Vu3("timezoneOffset")
    public /* final */ int timezoneOffset;
    @DexIgnore
    @Vu3("uid")
    public /* final */ String uid;
    @DexIgnore
    @Vu3("updatedAt")
    public /* final */ DateTime updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Gson gsonConverter() {
            Zi4 zi4 = new Zi4();
            zi4.f(Date.class, new GsonConverterShortDate());
            zi4.f(DateTime.class, new GsonConvertDateTime());
            Gson d = zi4.d();
            if (d != null) {
                return d;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        public final Activity toActivity(String str, ActivitySample activitySample) {
            Wg6.c(str, "uid");
            Wg6.c(activitySample, "sample");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(Activity.TAG, "toActivity - sample=" + activitySample);
            DateTime dateTime = new DateTime(activitySample.getSyncTime(), DateTimeZone.UTC);
            DateTime dateTime2 = new DateTime(activitySample.getCreatedAt(), DateTimeZone.UTC);
            DateTime dateTime3 = new DateTime(activitySample.getUpdatedAt(), DateTimeZone.UTC);
            return new Activity(str + activitySample.getId(), str, activitySample.getDate(), activitySample.getStartTime(), activitySample.getEndTime(), (int) activitySample.getSteps(), activitySample.getCalories(), activitySample.getDistance(), activitySample.getActiveTime(), activitySample.getIntensityDistInSteps(), activitySample.getTimeZoneOffsetInSecond(), activitySample.getSourceId(), dateTime, dateTime2, dateTime3);
        }
    }

    @DexIgnore
    public Activity(String str, String str2, Date date2, DateTime dateTime, DateTime dateTime2, int i, double d, double d2, int i2, ActivityIntensities activityIntensities, int i3, String str3, DateTime dateTime3, DateTime dateTime4, DateTime dateTime5) {
        Wg6.c(str, "id");
        Wg6.c(str2, "uid");
        Wg6.c(date2, "date");
        Wg6.c(dateTime, SampleRaw.COLUMN_START_TIME);
        Wg6.c(dateTime2, SampleRaw.COLUMN_END_TIME);
        Wg6.c(activityIntensities, "intensityDistInSteps");
        Wg6.c(str3, SampleRaw.COLUMN_SOURCE_ID);
        Wg6.c(dateTime4, "createdAt");
        Wg6.c(dateTime5, "updatedAt");
        this.id = str;
        this.uid = str2;
        this.date = date2;
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.steps = i;
        this.calories = d;
        this.distance = d2;
        this.activeTime = i2;
        this.intensityDistInSteps = activityIntensities;
        this.timezoneOffset = i3;
        this.sourceId = str3;
        this.syncTime = dateTime3;
        this.createdAt = dateTime4;
        this.updatedAt = dateTime5;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final ActivitySample toActivitySample() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "toActivitySample - id=" + this.id + ", date=" + this.date + ", startTime=" + this.startTime + ", syncTime=" + this.syncTime);
        DateTimeZone forOffsetMillis = DateTimeZone.forOffsetMillis(this.timezoneOffset * 1000);
        String str = this.uid;
        Date date2 = this.date;
        DateTime withZone = this.startTime.withZone(forOffsetMillis);
        Wg6.b(withZone, "startTime.withZone(timeZone)");
        DateTime withZone2 = this.endTime.withZone(forOffsetMillis);
        Wg6.b(withZone2, "endTime.withZone(timeZone)");
        double d = (double) this.steps;
        double d2 = this.calories;
        double d3 = this.distance;
        int i = this.activeTime;
        ActivityIntensities activityIntensities = this.intensityDistInSteps;
        int i2 = this.timezoneOffset;
        String str2 = this.sourceId;
        DateTime dateTime = this.syncTime;
        ActivitySample activitySample = new ActivitySample(str, date2, withZone, withZone2, d, d2, d3, i, activityIntensities, i2, str2, dateTime != null ? dateTime.getMillis() : this.createdAt.getMillis(), this.createdAt.getMillis(), this.updatedAt.getMillis());
        activitySample.setId(this.id);
        return activitySample;
    }

    @DexIgnore
    public final String toJsonString() {
        FLogger.INSTANCE.getLocal().d(TAG, "toJsonString");
        return toJsonString(Companion.gsonConverter());
    }

    @DexIgnore
    public final String toJsonString(Gson gson) {
        String str;
        synchronized (this) {
            Wg6.c(gson, "gson");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(TAG, "toJsonString - gson=" + gson);
            try {
                str = gson.t(this);
                Wg6.b(str, "gson.toJson(this)");
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e(TAG, "toJsonString - e=" + e);
                str = "";
            }
        }
        return str;
    }

    @DexIgnore
    public String toString() {
        return "[Activity: id='" + this.id + "', uid='" + this.uid + "', date=" + this.date + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ", steps=" + this.steps + ", calories=" + this.calories + ", distance=" + this.distance + ", activeTime=" + this.activeTime + ", intensityDistInSteps=" + this.intensityDistInSteps + ", timezoneOffset=" + this.timezoneOffset + ", sourceId='" + this.sourceId + "', syncTime=" + this.syncTime + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")]";
    }
}
