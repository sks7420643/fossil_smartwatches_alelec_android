package com.portfolio.platform.data;

import com.mapped.Tu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemindTimeModel {
    @DexIgnore
    @Tu3
    public int minutes;
    @DexIgnore
    @Tu3
    public String remindTimeName;

    @DexIgnore
    public RemindTimeModel(String str, int i) {
        Wg6.c(str, "remindTimeName");
        this.remindTimeName = str;
        this.minutes = i;
    }

    @DexIgnore
    public final int getMinutes() {
        return this.minutes;
    }

    @DexIgnore
    public final String getRemindTimeName() {
        return this.remindTimeName;
    }

    @DexIgnore
    public final void setMinutes(int i) {
        this.minutes = i;
    }

    @DexIgnore
    public final void setRemindTimeName(String str) {
        Wg6.c(str, "<set-?>");
        this.remindTimeName = str;
    }
}
