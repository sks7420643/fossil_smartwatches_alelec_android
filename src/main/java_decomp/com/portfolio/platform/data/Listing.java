package com.portfolio.platform.data;

import androidx.lifecycle.LiveData;
import com.mapped.Cd6;
import com.mapped.Cf;
import com.mapped.Gg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Listing<T> {
    @DexIgnore
    public /* final */ LiveData<NetworkState> networkState;
    @DexIgnore
    public /* final */ LiveData<Cf<T>> pagedList;
    @DexIgnore
    public /* final */ Gg6<Cd6> refresh;
    @DexIgnore
    public /* final */ Gg6<Cd6> retry;

    @DexIgnore
    public Listing(LiveData<Cf<T>> liveData, LiveData<NetworkState> liveData2, Gg6<Cd6> gg6, Gg6<Cd6> gg62) {
        Wg6.c(liveData, "pagedList");
        Wg6.c(liveData2, "networkState");
        Wg6.c(gg6, "refresh");
        Wg6.c(gg62, "retry");
        this.pagedList = liveData;
        this.networkState = liveData2;
        this.refresh = gg6;
        this.retry = gg62;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.Listing */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Listing copy$default(Listing listing, LiveData liveData, LiveData liveData2, Gg6 gg6, Gg6 gg62, int i, Object obj) {
        if ((i & 1) != 0) {
            liveData = listing.pagedList;
        }
        if ((i & 2) != 0) {
            liveData2 = listing.networkState;
        }
        if ((i & 4) != 0) {
            gg6 = listing.refresh;
        }
        if ((i & 8) != 0) {
            gg62 = listing.retry;
        }
        return listing.copy(liveData, liveData2, gg6, gg62);
    }

    @DexIgnore
    public final LiveData<Cf<T>> component1() {
        return this.pagedList;
    }

    @DexIgnore
    public final LiveData<NetworkState> component2() {
        return this.networkState;
    }

    @DexIgnore
    public final Gg6<Cd6> component3() {
        return this.refresh;
    }

    @DexIgnore
    public final Gg6<Cd6> component4() {
        return this.retry;
    }

    @DexIgnore
    public final Listing<T> copy(LiveData<Cf<T>> liveData, LiveData<NetworkState> liveData2, Gg6<Cd6> gg6, Gg6<Cd6> gg62) {
        Wg6.c(liveData, "pagedList");
        Wg6.c(liveData2, "networkState");
        Wg6.c(gg6, "refresh");
        Wg6.c(gg62, "retry");
        return new Listing<>(liveData, liveData2, gg6, gg62);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Listing) {
                Listing listing = (Listing) obj;
                if (!Wg6.a(this.pagedList, listing.pagedList) || !Wg6.a(this.networkState, listing.networkState) || !Wg6.a(this.refresh, listing.refresh) || !Wg6.a(this.retry, listing.retry)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final LiveData<NetworkState> getNetworkState() {
        return this.networkState;
    }

    @DexIgnore
    public final LiveData<Cf<T>> getPagedList() {
        return this.pagedList;
    }

    @DexIgnore
    public final Gg6<Cd6> getRefresh() {
        return this.refresh;
    }

    @DexIgnore
    public final Gg6<Cd6> getRetry() {
        return this.retry;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        LiveData<Cf<T>> liveData = this.pagedList;
        int hashCode = liveData != null ? liveData.hashCode() : 0;
        LiveData<NetworkState> liveData2 = this.networkState;
        int hashCode2 = liveData2 != null ? liveData2.hashCode() : 0;
        Gg6<Cd6> gg6 = this.refresh;
        int hashCode3 = gg6 != null ? gg6.hashCode() : 0;
        Gg6<Cd6> gg62 = this.retry;
        if (gg62 != null) {
            i = gg62.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Listing(pagedList=" + this.pagedList + ", networkState=" + this.networkState + ", refresh=" + this.refresh + ", retry=" + this.retry + ")";
    }
}
