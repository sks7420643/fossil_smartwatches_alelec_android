package com.portfolio.platform.data.source.local.workoutsetting;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.fossil.Q05;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingDao_Impl implements WorkoutSettingDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<WorkoutSetting> __insertionAdapterOfWorkoutSetting;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ Q05 __workoutTypeConverter; // = new Q05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<WorkoutSetting> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, WorkoutSetting workoutSetting) {
            String l = WorkoutSettingDao_Impl.this.__workoutTypeConverter.l(workoutSetting.getType());
            if (l == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, l);
            }
            String f = WorkoutSettingDao_Impl.this.__workoutTypeConverter.f(workoutSetting.getMode());
            if (f == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, f);
            }
            mi.bindLong(3, workoutSetting.getEnable() ? 1 : 0);
            mi.bindLong(4, workoutSetting.getAskMeFirst() ? 1 : 0);
            mi.bindLong(5, (long) workoutSetting.getStartLatency());
            mi.bindLong(6, (long) workoutSetting.getPauseLatency());
            mi.bindLong(7, (long) workoutSetting.getResumeLatency());
            mi.bindLong(8, (long) workoutSetting.getStopLatency());
            mi.bindLong(9, workoutSetting.getCreatedAt());
            mi.bindLong(10, workoutSetting.getUpdatedAt());
            mi.bindLong(11, (long) workoutSetting.getPinType());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, WorkoutSetting workoutSetting) {
            bind(mi, workoutSetting);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `workoutSetting` (`type`,`mode`,`enable`,`askMeFirst`,`startLatency`,`pauseLatency`,`resumeLatency`,`stopLatency`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM workoutSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<WorkoutSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon3(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WorkoutSetting> call() throws Exception {
            Cursor b = Ex0.b(WorkoutSettingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "type");
                int c2 = Dx0.c(b, "mode");
                int c3 = Dx0.c(b, "enable");
                int c4 = Dx0.c(b, "askMeFirst");
                int c5 = Dx0.c(b, "startLatency");
                int c6 = Dx0.c(b, "pauseLatency");
                int c7 = Dx0.c(b, "resumeLatency");
                int c8 = Dx0.c(b, "stopLatency");
                int c9 = Dx0.c(b, "createdAt");
                int c10 = Dx0.c(b, "updatedAt");
                int c11 = Dx0.c(b, "pinType");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new WorkoutSetting(WorkoutSettingDao_Impl.this.__workoutTypeConverter.x(b.getString(c)), WorkoutSettingDao_Impl.this.__workoutTypeConverter.r(b.getString(c2)), b.getInt(c3) != 0, b.getInt(c4) != 0, b.getInt(c5), b.getInt(c6), b.getInt(c7), b.getInt(c8), b.getLong(c9), b.getLong(c10), b.getInt(c11)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public WorkoutSettingDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfWorkoutSetting = new Anon1(oh);
        this.__preparedStmtOfCleanUp = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public List<WorkoutSetting> getPendingWorkoutSettings() {
        Rh f = Rh.f("SELECT * FROM workoutSetting where pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "type");
            int c2 = Dx0.c(b, "mode");
            int c3 = Dx0.c(b, "enable");
            int c4 = Dx0.c(b, "askMeFirst");
            int c5 = Dx0.c(b, "startLatency");
            int c6 = Dx0.c(b, "pauseLatency");
            int c7 = Dx0.c(b, "resumeLatency");
            int c8 = Dx0.c(b, "stopLatency");
            int c9 = Dx0.c(b, "createdAt");
            int c10 = Dx0.c(b, "updatedAt");
            int c11 = Dx0.c(b, "pinType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WorkoutSetting(this.__workoutTypeConverter.x(b.getString(c)), this.__workoutTypeConverter.r(b.getString(c2)), b.getInt(c3) != 0, b.getInt(c4) != 0, b.getInt(c5), b.getInt(c6), b.getInt(c7), b.getInt(c8), b.getLong(c9), b.getLong(c10), b.getInt(c11)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public List<WorkoutSetting> getWorkoutSettingList() {
        Rh f = Rh.f("SELECT * FROM workoutSetting WHERE pinType != 3 ", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "type");
            int c2 = Dx0.c(b, "mode");
            int c3 = Dx0.c(b, "enable");
            int c4 = Dx0.c(b, "askMeFirst");
            int c5 = Dx0.c(b, "startLatency");
            int c6 = Dx0.c(b, "pauseLatency");
            int c7 = Dx0.c(b, "resumeLatency");
            int c8 = Dx0.c(b, "stopLatency");
            int c9 = Dx0.c(b, "createdAt");
            int c10 = Dx0.c(b, "updatedAt");
            int c11 = Dx0.c(b, "pinType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WorkoutSetting(this.__workoutTypeConverter.x(b.getString(c)), this.__workoutTypeConverter.r(b.getString(c2)), b.getInt(c3) != 0, b.getInt(c4) != 0, b.getInt(c5), b.getInt(c6), b.getInt(c7), b.getInt(c8), b.getLong(c9), b.getLong(c10), b.getInt(c11)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public LiveData<List<WorkoutSetting>> getWorkoutSettingListAsLiveData() {
        Rh f = Rh.f("SELECT * FROM workoutSetting WHERE pinType != 3 ", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"workoutSetting"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public void upsertWorkoutSetting(WorkoutSetting workoutSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkoutSetting.insert((Hh<WorkoutSetting>) workoutSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDao
    public void upsertWorkoutSettingList(List<WorkoutSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWorkoutSetting.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
