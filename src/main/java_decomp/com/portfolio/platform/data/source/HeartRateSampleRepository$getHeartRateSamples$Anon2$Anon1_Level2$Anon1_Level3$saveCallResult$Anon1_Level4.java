package com.portfolio.platform.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Cd6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2;
import com.portfolio.platform.data.source.remote.ApiResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$2$1$1", f = "HeartRateSampleRepository.kt", l = {58}, m = "saveCallResult")
public final class HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 extends Jf6 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSampleRepository$getHeartRateSamples$Anon2.Anon1_Level2.Anon1_Level3 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4(HeartRateSampleRepository$getHeartRateSamples$Anon2.Anon1_Level2.Anon1_Level3 anon1_Level3, Xe6 xe6) {
        super(xe6);
        this.this$0 = anon1_Level3;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.saveCallResult((ApiResponse<HeartRate>) null, (Xe6<? super Cd6>) this);
    }
}
