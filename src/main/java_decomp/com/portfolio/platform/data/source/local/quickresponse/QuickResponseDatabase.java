package com.portfolio.platform.data.source.local.quickresponse;

import com.mapped.Oh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class QuickResponseDatabase extends Oh {
    @DexIgnore
    public abstract QuickResponseMessageDao quickResponseMessageDao();

    @DexIgnore
    public abstract QuickResponseSenderDao quickResponseSenderDao();
}
