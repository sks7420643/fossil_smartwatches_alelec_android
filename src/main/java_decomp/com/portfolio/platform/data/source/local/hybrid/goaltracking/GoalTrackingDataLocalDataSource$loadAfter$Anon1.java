package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.mapped.PagingRequestHelper;
import com.mapped.Rm6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataLocalDataSource$loadAfter$Anon1 implements PagingRequestHelper.Bi {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource this$0;

    @DexIgnore
    public GoalTrackingDataLocalDataSource$loadAfter$Anon1(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
        this.this$0 = goalTrackingDataLocalDataSource;
    }

    @DexIgnore
    @Override // com.mapped.PagingRequestHelper.Bi
    public final void run(PagingRequestHelper.Bi.Aii aii) {
        GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = this.this$0;
        PagingRequestHelper.Di di = PagingRequestHelper.Di.AFTER;
        Wg6.b(aii, "helperCallback");
        Rm6 unused = goalTrackingDataLocalDataSource.loadData(di, aii, this.this$0.mOffset);
    }
}
