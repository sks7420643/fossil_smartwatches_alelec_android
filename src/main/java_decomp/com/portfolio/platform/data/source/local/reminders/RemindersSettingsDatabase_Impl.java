package com.portfolio.platform.data.source.local.reminders;

import com.fossil.Ex0;
import com.fossil.Hw0;
import com.fossil.Ix0;
import com.fossil.Lx0;
import com.fossil.Nw0;
import com.mapped.Ji;
import com.mapped.Oh;
import com.mapped.Qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemindersSettingsDatabase_Impl extends RemindersSettingsDatabase {
    @DexIgnore
    public volatile InactivityNudgeTimeDao _inactivityNudgeTimeDao;
    @DexIgnore
    public volatile RemindTimeDao _remindTimeDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Qh.Ai {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void createAllTables(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `inactivityNudgeTimeModel` (`nudgeTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, `nudgeTimeType` INTEGER NOT NULL, PRIMARY KEY(`nudgeTimeName`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `remindTimeModel` (`remindTimeName` TEXT NOT NULL, `minutes` INTEGER NOT NULL, PRIMARY KEY(`remindTimeName`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '0ee434ba350bbb753b81bda456b5c107')");
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void dropAllTables(Lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `inactivityNudgeTimeModel`");
            lx0.execSQL("DROP TABLE IF EXISTS `remindTimeModel`");
            if (RemindersSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = RemindersSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) RemindersSettingsDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onCreate(Lx0 lx0) {
            if (RemindersSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = RemindersSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) RemindersSettingsDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onOpen(Lx0 lx0) {
            RemindersSettingsDatabase_Impl.this.mDatabase = lx0;
            RemindersSettingsDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (RemindersSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = RemindersSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) RemindersSettingsDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPostMigrate(Lx0 lx0) {
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPreMigrate(Lx0 lx0) {
            Ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public Qh.Bi onValidateSchema(Lx0 lx0) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("nudgeTimeName", new Ix0.Ai("nudgeTimeName", "TEXT", true, 1, null, 1));
            hashMap.put("minutes", new Ix0.Ai("minutes", "INTEGER", true, 0, null, 1));
            hashMap.put("nudgeTimeType", new Ix0.Ai("nudgeTimeType", "INTEGER", true, 0, null, 1));
            Ix0 ix0 = new Ix0("inactivityNudgeTimeModel", hashMap, new HashSet(0), new HashSet(0));
            Ix0 a2 = Ix0.a(lx0, "inactivityNudgeTimeModel");
            if (!ix0.equals(a2)) {
                return new Qh.Bi(false, "inactivityNudgeTimeModel(com.portfolio.platform.data.InactivityNudgeTimeModel).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(2);
            hashMap2.put("remindTimeName", new Ix0.Ai("remindTimeName", "TEXT", true, 1, null, 1));
            hashMap2.put("minutes", new Ix0.Ai("minutes", "INTEGER", true, 0, null, 1));
            Ix0 ix02 = new Ix0("remindTimeModel", hashMap2, new HashSet(0), new HashSet(0));
            Ix0 a3 = Ix0.a(lx0, "remindTimeModel");
            if (ix02.equals(a3)) {
                return new Qh.Bi(true, null);
            }
            return new Qh.Bi(false, "remindTimeModel(com.portfolio.platform.data.RemindTimeModel).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public void clearAllTables() {
        super.assertNotMainThread();
        Lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `inactivityNudgeTimeModel`");
            writableDatabase.execSQL("DELETE FROM `remindTimeModel`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Nw0 createInvalidationTracker() {
        return new Nw0(this, new HashMap(0), new HashMap(0), "inactivityNudgeTimeModel", "remindTimeModel");
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Ji createOpenHelper(Hw0 hw0) {
        Qh qh = new Qh(hw0, new Anon1(1), "0ee434ba350bbb753b81bda456b5c107", "7e66671f5f316f81e6558b840c854b68");
        Ji.Bi.Aii a2 = Ji.Bi.a(hw0.b);
        a2.c(hw0.c);
        a2.b(qh);
        return hw0.a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase
    public InactivityNudgeTimeDao getInactivityNudgeTimeDao() {
        InactivityNudgeTimeDao inactivityNudgeTimeDao;
        if (this._inactivityNudgeTimeDao != null) {
            return this._inactivityNudgeTimeDao;
        }
        synchronized (this) {
            if (this._inactivityNudgeTimeDao == null) {
                this._inactivityNudgeTimeDao = new InactivityNudgeTimeDao_Impl(this);
            }
            inactivityNudgeTimeDao = this._inactivityNudgeTimeDao;
        }
        return inactivityNudgeTimeDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase
    public RemindTimeDao getRemindTimeDao() {
        RemindTimeDao remindTimeDao;
        if (this._remindTimeDao != null) {
            return this._remindTimeDao;
        }
        synchronized (this) {
            if (this._remindTimeDao == null) {
                this._remindTimeDao = new RemindTimeDao_Impl(this);
            }
            remindTimeDao = this._remindTimeDao;
        }
        return remindTimeDao;
    }
}
