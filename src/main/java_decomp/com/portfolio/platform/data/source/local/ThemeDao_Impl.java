package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.N05;
import com.mapped.Gh;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.Theme;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeDao_Impl extends ThemeDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Gh<Theme> __deletionAdapterOfTheme;
    @DexIgnore
    public /* final */ Hh<Theme> __insertionAdapterOfTheme;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearThemeTable;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfResetAllCurrentTheme;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfSetCurrentThemeId;
    @DexIgnore
    public /* final */ N05 __themeConverter; // = new N05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<Theme> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, Theme theme) {
            mi.bindLong(1, theme.isCurrentTheme() ? 1 : 0);
            if (theme.getType() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, theme.getType());
            }
            if (theme.getId() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, theme.getId());
            }
            if (theme.getName() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, theme.getName());
            }
            String b = ThemeDao_Impl.this.__themeConverter.b(theme.getStyles());
            if (b == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, b);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, Theme theme) {
            bind(mi, theme);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `theme` (`isCurrentTheme`,`type`,`id`,`name`,`styles`) VALUES (?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Gh<Theme> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, Theme theme) {
            if (theme.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, theme.getId());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Gh
        public /* bridge */ /* synthetic */ void bind(Mi mi, Theme theme) {
            bind(mi, theme);
        }

        @DexIgnore
        @Override // com.mapped.Vh, com.mapped.Gh
        public String createQuery() {
            return "DELETE FROM `theme` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE theme SET isCurrentTheme=0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends Vh {
        @DexIgnore
        public Anon4(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE theme SET isCurrentTheme=1 WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends Vh {
        @DexIgnore
        public Anon5(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM theme";
        }
    }

    @DexIgnore
    public ThemeDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfTheme = new Anon1(oh);
        this.__deletionAdapterOfTheme = new Anon2(oh);
        this.__preparedStmtOfResetAllCurrentTheme = new Anon3(oh);
        this.__preparedStmtOfSetCurrentThemeId = new Anon4(oh);
        this.__preparedStmtOfClearThemeTable = new Anon5(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void clearThemeTable() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearThemeTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearThemeTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void deleteTheme(Theme theme) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfTheme.handle(theme);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public Theme getCurrentTheme() {
        Theme theme = null;
        boolean z = false;
        Rh f = Rh.f("SELECT * FROM theme WHERE isCurrentTheme=1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "isCurrentTheme");
            int c2 = Dx0.c(b, "type");
            int c3 = Dx0.c(b, "id");
            int c4 = Dx0.c(b, "name");
            int c5 = Dx0.c(b, "styles");
            if (b.moveToFirst()) {
                theme = new Theme(b.getString(c3), b.getString(c4), this.__themeConverter.a(b.getString(c5)));
                if (b.getInt(c) != 0) {
                    z = true;
                }
                theme.setCurrentTheme(z);
                theme.setType(b.getString(c2));
            }
            return theme;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public String getCurrentThemeId() {
        String str = null;
        Rh f = Rh.f("SELECT id FROM theme WHERE isCurrentTheme=1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            if (b.moveToFirst()) {
                str = b.getString(0);
            }
            return str;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public List<Theme> getListTheme() {
        Rh f = Rh.f("SELECT * FROM theme", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "isCurrentTheme");
            int c2 = Dx0.c(b, "type");
            int c3 = Dx0.c(b, "id");
            int c4 = Dx0.c(b, "name");
            int c5 = Dx0.c(b, "styles");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                Theme theme = new Theme(b.getString(c3), b.getString(c4), this.__themeConverter.a(b.getString(c5)));
                theme.setCurrentTheme(b.getInt(c) != 0);
                theme.setType(b.getString(c2));
                arrayList.add(theme);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public List<String> getListThemeId() {
        Rh f = Rh.f("SELECT id FROM theme", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(b.getString(0));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public List<String> getListThemeIdByType(String str) {
        Rh f = Rh.f("SELECT id FROM theme WHERE type=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(b.getString(0));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public String getNameById(String str) {
        String str2 = null;
        Rh f = Rh.f("SELECT name FROM theme WHERE id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            if (b.moveToFirst()) {
                str2 = b.getString(0);
            }
            return str2;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public Theme getThemeById(String str) {
        Theme theme = null;
        boolean z = true;
        Rh f = Rh.f("SELECT * FROM theme WHERE id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "isCurrentTheme");
            int c2 = Dx0.c(b, "type");
            int c3 = Dx0.c(b, "id");
            int c4 = Dx0.c(b, "name");
            int c5 = Dx0.c(b, "styles");
            if (b.moveToFirst()) {
                theme = new Theme(b.getString(c3), b.getString(c4), this.__themeConverter.a(b.getString(c5)));
                if (b.getInt(c) == 0) {
                    z = false;
                }
                theme.setCurrentTheme(z);
                theme.setType(b.getString(c2));
            }
            return theme;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void insertListTheme(List<Theme> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfTheme.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void resetAllCurrentTheme() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfResetAllCurrentTheme.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfResetAllCurrentTheme.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void setCurrentThemeId(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfSetCurrentThemeId.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfSetCurrentThemeId.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDao
    public void upsertTheme(Theme theme) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfTheme.insert((Hh<Theme>) theme);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
