package com.portfolio.platform.data.source.local;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class FitnessDataDao {
    @DexIgnore
    public abstract void deleteAllFitnessData();

    @DexIgnore
    public abstract void deleteFitnessData(List<FitnessDataWrapper> list);

    @DexIgnore
    public final List<FitnessDataWrapper> getFitnessData(Date date, Date date2) {
        Wg6.c(date, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Wg6.c(date2, "end");
        Date V = TimeUtils.V(date);
        Date E = TimeUtils.E(date2);
        Wg6.b(V, SampleRaw.COLUMN_START_TIME);
        DateTime dateTime = new DateTime(V.getTime());
        Wg6.b(E, SampleRaw.COLUMN_END_TIME);
        return getListFitnessData(dateTime, new DateTime(E.getTime()));
    }

    @DexIgnore
    public final LiveData<List<FitnessDataWrapper>> getFitnessDataLiveData(Date date, Date date2) {
        Wg6.c(date, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Wg6.c(date2, "end");
        Date V = TimeUtils.V(date);
        Date E = TimeUtils.E(date2);
        Wg6.b(V, SampleRaw.COLUMN_START_TIME);
        DateTime dateTime = new DateTime(V.getTime());
        Wg6.b(E, SampleRaw.COLUMN_END_TIME);
        return getListFitnessDataLiveData(dateTime, new DateTime(E.getTime()));
    }

    @DexIgnore
    public abstract List<FitnessDataWrapper> getListFitnessData(DateTime dateTime, DateTime dateTime2);

    @DexIgnore
    public abstract LiveData<List<FitnessDataWrapper>> getListFitnessDataLiveData(DateTime dateTime, DateTime dateTime2);

    @DexIgnore
    public abstract List<FitnessDataWrapper> getPendingFitnessData();

    @DexIgnore
    public abstract void insertFitnessDataList(List<FitnessDataWrapper> list);
}
