package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.setting.WatchLocalization;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.WatchLocalizationRepository$getWatchLocalizationFromServer$response$1", f = "WatchLocalizationRepository.kt", l = {24}, m = "invokeSuspend")
public final class WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1 extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<WatchLocalization>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $locale;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WatchLocalizationRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1(WatchLocalizationRepository watchLocalizationRepository, String str, Xe6 xe6) {
        super(1, xe6);
        this.this$0 = watchLocalizationRepository;
        this.$locale = str;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        return new WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1(this.this$0, this.$locale, xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public final Object invoke(Xe6<? super Q88<ApiResponse<WatchLocalization>>> xe6) {
        throw null;
        //return ((WatchLocalizationRepository$getWatchLocalizationFromServer$response$Anon1) create(xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.api;
            String str = this.$locale;
            this.label = 1;
            Object watchLocalizationData = apiServiceV2.getWatchLocalizationData(str, this);
            return watchLocalizationData == d ? d : watchLocalizationData;
        } else if (i == 1) {
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
