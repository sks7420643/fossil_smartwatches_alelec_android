package com.portfolio.platform.data.source.local.diana.notification;

import com.fossil.Ex0;
import com.fossil.Hw0;
import com.fossil.Ix0;
import com.fossil.Lx0;
import com.fossil.Nw0;
import com.mapped.Ji;
import com.mapped.Oh;
import com.mapped.Qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationSettingsDatabase_Impl extends NotificationSettingsDatabase {
    @DexIgnore
    public volatile NotificationSettingsDao _notificationSettingsDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Qh.Ai {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void createAllTables(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `notificationSettings` (`settingsName` TEXT NOT NULL, `settingsType` INTEGER NOT NULL, `isCall` INTEGER NOT NULL, PRIMARY KEY(`settingsName`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '563d2dcf0170d812960699bb88bdb35d')");
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void dropAllTables(Lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `notificationSettings`");
            if (NotificationSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = NotificationSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) NotificationSettingsDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onCreate(Lx0 lx0) {
            if (NotificationSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = NotificationSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) NotificationSettingsDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onOpen(Lx0 lx0) {
            NotificationSettingsDatabase_Impl.this.mDatabase = lx0;
            NotificationSettingsDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (NotificationSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = NotificationSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) NotificationSettingsDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPostMigrate(Lx0 lx0) {
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPreMigrate(Lx0 lx0) {
            Ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public Qh.Bi onValidateSchema(Lx0 lx0) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("settingsName", new Ix0.Ai("settingsName", "TEXT", true, 1, null, 1));
            hashMap.put("settingsType", new Ix0.Ai("settingsType", "INTEGER", true, 0, null, 1));
            hashMap.put("isCall", new Ix0.Ai("isCall", "INTEGER", true, 0, null, 1));
            Ix0 ix0 = new Ix0("notificationSettings", hashMap, new HashSet(0), new HashSet(0));
            Ix0 a2 = Ix0.a(lx0, "notificationSettings");
            if (ix0.equals(a2)) {
                return new Qh.Bi(true, null);
            }
            return new Qh.Bi(false, "notificationSettings(com.portfolio.platform.data.model.NotificationSettingsModel).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public void clearAllTables() {
        super.assertNotMainThread();
        Lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `notificationSettings`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Nw0 createInvalidationTracker() {
        return new Nw0(this, new HashMap(0), new HashMap(0), "notificationSettings");
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Ji createOpenHelper(Hw0 hw0) {
        Qh qh = new Qh(hw0, new Anon1(1), "563d2dcf0170d812960699bb88bdb35d", "d29e13e7c48fbcf2227b41f4f512e273");
        Ji.Bi.Aii a2 = Ji.Bi.a(hw0.b);
        a2.c(hw0.c);
        a2.b(qh);
        return hw0.a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase
    public NotificationSettingsDao getNotificationSettingsDao() {
        NotificationSettingsDao notificationSettingsDao;
        if (this._notificationSettingsDao != null) {
            return this._notificationSettingsDao;
        }
        synchronized (this) {
            if (this._notificationSettingsDao == null) {
                this._notificationSettingsDao = new NotificationSettingsDao_Impl(this);
            }
            notificationSettingsDao = this._notificationSettingsDao;
        }
        return notificationSettingsDao;
    }
}
