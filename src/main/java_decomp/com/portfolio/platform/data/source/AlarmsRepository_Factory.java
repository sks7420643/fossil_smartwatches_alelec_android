package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmsRepository_Factory implements Factory<AlarmsRepository> {
    @DexIgnore
    public /* final */ Provider<AlarmsRemoteDataSource> mAlarmRemoteDataSourceProvider;

    @DexIgnore
    public AlarmsRepository_Factory(Provider<AlarmsRemoteDataSource> provider) {
        this.mAlarmRemoteDataSourceProvider = provider;
    }

    @DexIgnore
    public static AlarmsRepository_Factory create(Provider<AlarmsRemoteDataSource> provider) {
        return new AlarmsRepository_Factory(provider);
    }

    @DexIgnore
    public static AlarmsRepository newInstance(AlarmsRemoteDataSource alarmsRemoteDataSource) {
        return new AlarmsRepository(alarmsRemoteDataSource);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public AlarmsRepository get() {
        return newInstance(this.mAlarmRemoteDataSourceProvider.get());
    }
}
