package com.portfolio.platform.data.source.local.sleep;

import com.fossil.Ex0;
import com.fossil.Hw0;
import com.fossil.Ix0;
import com.fossil.Lx0;
import com.fossil.Nw0;
import com.fossil.wearables.fsl.sleep.MFSleepDay;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.mapped.Ji;
import com.mapped.Oh;
import com.mapped.Qh;
import com.portfolio.platform.data.SleepStatistic;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDatabase_Impl extends SleepDatabase {
    @DexIgnore
    public volatile SleepDao _sleepDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Qh.Ai {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void createAllTables(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `sleep_session` (`pinType` INTEGER NOT NULL, `date` INTEGER NOT NULL, `day` TEXT NOT NULL, `deviceSerialNumber` TEXT, `syncTime` INTEGER, `bookmarkTime` INTEGER, `normalizedSleepQuality` REAL NOT NULL, `source` INTEGER NOT NULL, `realStartTime` INTEGER NOT NULL, `realEndTime` INTEGER NOT NULL, `realSleepMinutes` INTEGER NOT NULL, `realSleepStateDistInMinute` TEXT NOT NULL, `editedStartTime` INTEGER, `editedEndTime` INTEGER, `editedSleepMinutes` INTEGER, `editedSleepStateDistInMinute` TEXT, `sleepStates` TEXT NOT NULL, `heartRate` TEXT, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `timezoneOffset` INTEGER NOT NULL, PRIMARY KEY(`realEndTime`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `sleep_date` (`pinType` INTEGER NOT NULL, `timezoneOffset` INTEGER NOT NULL, `date` TEXT NOT NULL, `goalMinutes` INTEGER NOT NULL, `sleepMinutes` INTEGER NOT NULL, `sleepStateDistInMinute` TEXT, `createdAt` INTEGER, `updatedAt` INTEGER, PRIMARY KEY(`date`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `sleep_settings` (`id` INTEGER NOT NULL, `sleepGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `sleepRecommendedGoals` (`id` INTEGER NOT NULL, `recommendedSleepGoal` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `sleep_statistic` (`id` TEXT NOT NULL, `uid` TEXT NOT NULL, `sleepTimeBestDay` TEXT, `sleepTimeBestStreak` TEXT, `totalDays` INTEGER NOT NULL, `totalSleeps` INTEGER NOT NULL, `totalSleepMinutes` INTEGER NOT NULL, `totalSleepStateDistInMinute` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '6563ab2be16ee1f30194e9dfab2b7513')");
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void dropAllTables(Lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `sleep_session`");
            lx0.execSQL("DROP TABLE IF EXISTS `sleep_date`");
            lx0.execSQL("DROP TABLE IF EXISTS `sleep_settings`");
            lx0.execSQL("DROP TABLE IF EXISTS `sleepRecommendedGoals`");
            lx0.execSQL("DROP TABLE IF EXISTS `sleep_statistic`");
            if (SleepDatabase_Impl.this.mCallbacks != null) {
                int size = SleepDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) SleepDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onCreate(Lx0 lx0) {
            if (SleepDatabase_Impl.this.mCallbacks != null) {
                int size = SleepDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) SleepDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onOpen(Lx0 lx0) {
            SleepDatabase_Impl.this.mDatabase = lx0;
            SleepDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (SleepDatabase_Impl.this.mCallbacks != null) {
                int size = SleepDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) SleepDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPostMigrate(Lx0 lx0) {
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPreMigrate(Lx0 lx0) {
            Ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public Qh.Bi onValidateSchema(Lx0 lx0) {
            HashMap hashMap = new HashMap(21);
            hashMap.put("pinType", new Ix0.Ai("pinType", "INTEGER", true, 0, null, 1));
            hashMap.put("date", new Ix0.Ai("date", "INTEGER", true, 0, null, 1));
            hashMap.put("day", new Ix0.Ai("day", "TEXT", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, new Ix0.Ai(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER, "TEXT", false, 0, null, 1));
            hashMap.put("syncTime", new Ix0.Ai("syncTime", "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_BOOKMARK_TIME, new Ix0.Ai(MFSleepSession.COLUMN_BOOKMARK_TIME, "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY, new Ix0.Ai(MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY, "REAL", true, 0, null, 1));
            hashMap.put("source", new Ix0.Ai("source", "INTEGER", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_REAL_START_TIME, new Ix0.Ai(MFSleepSession.COLUMN_REAL_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_REAL_END_TIME, new Ix0.Ai(MFSleepSession.COLUMN_REAL_END_TIME, "INTEGER", true, 1, null, 1));
            hashMap.put(MFSleepSession.COLUMN_REAL_SLEEP_MINUTES, new Ix0.Ai(MFSleepSession.COLUMN_REAL_SLEEP_MINUTES, "INTEGER", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE, new Ix0.Ai(MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE, "TEXT", true, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_EDITED_START_TIME, new Ix0.Ai(MFSleepSession.COLUMN_EDITED_START_TIME, "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_EDITED_END_TIME, new Ix0.Ai(MFSleepSession.COLUMN_EDITED_END_TIME, "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES, new Ix0.Ai(MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES, "INTEGER", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE, new Ix0.Ai(MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE, "TEXT", false, 0, null, 1));
            hashMap.put(MFSleepSession.COLUMN_SLEEP_STATES, new Ix0.Ai(MFSleepSession.COLUMN_SLEEP_STATES, "TEXT", true, 0, null, 1));
            hashMap.put("heartRate", new Ix0.Ai("heartRate", "TEXT", false, 0, null, 1));
            hashMap.put("createdAt", new Ix0.Ai("createdAt", "INTEGER", true, 0, null, 1));
            hashMap.put("updatedAt", new Ix0.Ai("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap.put("timezoneOffset", new Ix0.Ai("timezoneOffset", "INTEGER", true, 0, null, 1));
            Ix0 ix0 = new Ix0(MFSleepSession.TABLE_NAME, hashMap, new HashSet(0), new HashSet(0));
            Ix0 a2 = Ix0.a(lx0, MFSleepSession.TABLE_NAME);
            if (!ix0.equals(a2)) {
                return new Qh.Bi(false, "sleep_session(com.portfolio.platform.data.model.room.sleep.MFSleepSession).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(8);
            hashMap2.put("pinType", new Ix0.Ai("pinType", "INTEGER", true, 0, null, 1));
            hashMap2.put("timezoneOffset", new Ix0.Ai("timezoneOffset", "INTEGER", true, 0, null, 1));
            hashMap2.put("date", new Ix0.Ai("date", "TEXT", true, 1, null, 1));
            hashMap2.put(MFSleepDay.COLUMN_GOAL_MINUTES, new Ix0.Ai(MFSleepDay.COLUMN_GOAL_MINUTES, "INTEGER", true, 0, null, 1));
            hashMap2.put(MFSleepDay.COLUMN_SLEEP_MINUTES, new Ix0.Ai(MFSleepDay.COLUMN_SLEEP_MINUTES, "INTEGER", true, 0, null, 1));
            hashMap2.put(MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE, new Ix0.Ai(MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE, "TEXT", false, 0, null, 1));
            hashMap2.put("createdAt", new Ix0.Ai("createdAt", "INTEGER", false, 0, null, 1));
            hashMap2.put("updatedAt", new Ix0.Ai("updatedAt", "INTEGER", false, 0, null, 1));
            Ix0 ix02 = new Ix0(MFSleepDay.TABLE_NAME, hashMap2, new HashSet(0), new HashSet(0));
            Ix0 a3 = Ix0.a(lx0, MFSleepDay.TABLE_NAME);
            if (!ix02.equals(a3)) {
                return new Qh.Bi(false, "sleep_date(com.portfolio.platform.data.model.room.sleep.MFSleepDay).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
            }
            HashMap hashMap3 = new HashMap(2);
            hashMap3.put("id", new Ix0.Ai("id", "INTEGER", true, 1, null, 1));
            hashMap3.put("sleepGoal", new Ix0.Ai("sleepGoal", "INTEGER", true, 0, null, 1));
            Ix0 ix03 = new Ix0("sleep_settings", hashMap3, new HashSet(0), new HashSet(0));
            Ix0 a4 = Ix0.a(lx0, "sleep_settings");
            if (!ix03.equals(a4)) {
                return new Qh.Bi(false, "sleep_settings(com.portfolio.platform.data.model.room.sleep.MFSleepSettings).\n Expected:\n" + ix03 + "\n Found:\n" + a4);
            }
            HashMap hashMap4 = new HashMap(2);
            hashMap4.put("id", new Ix0.Ai("id", "INTEGER", true, 1, null, 1));
            hashMap4.put("recommendedSleepGoal", new Ix0.Ai("recommendedSleepGoal", "INTEGER", true, 0, null, 1));
            Ix0 ix04 = new Ix0("sleepRecommendedGoals", hashMap4, new HashSet(0), new HashSet(0));
            Ix0 a5 = Ix0.a(lx0, "sleepRecommendedGoals");
            if (!ix04.equals(a5)) {
                return new Qh.Bi(false, "sleepRecommendedGoals(com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal).\n Expected:\n" + ix04 + "\n Found:\n" + a5);
            }
            HashMap hashMap5 = new HashMap(10);
            hashMap5.put("id", new Ix0.Ai("id", "TEXT", true, 1, null, 1));
            hashMap5.put("uid", new Ix0.Ai("uid", "TEXT", true, 0, null, 1));
            hashMap5.put("sleepTimeBestDay", new Ix0.Ai("sleepTimeBestDay", "TEXT", false, 0, null, 1));
            hashMap5.put("sleepTimeBestStreak", new Ix0.Ai("sleepTimeBestStreak", "TEXT", false, 0, null, 1));
            hashMap5.put("totalDays", new Ix0.Ai("totalDays", "INTEGER", true, 0, null, 1));
            hashMap5.put("totalSleeps", new Ix0.Ai("totalSleeps", "INTEGER", true, 0, null, 1));
            hashMap5.put("totalSleepMinutes", new Ix0.Ai("totalSleepMinutes", "INTEGER", true, 0, null, 1));
            hashMap5.put("totalSleepStateDistInMinute", new Ix0.Ai("totalSleepStateDistInMinute", "TEXT", true, 0, null, 1));
            hashMap5.put("createdAt", new Ix0.Ai("createdAt", "INTEGER", true, 0, null, 1));
            hashMap5.put("updatedAt", new Ix0.Ai("updatedAt", "INTEGER", true, 0, null, 1));
            Ix0 ix05 = new Ix0(SleepStatistic.TABLE_NAME, hashMap5, new HashSet(0), new HashSet(0));
            Ix0 a6 = Ix0.a(lx0, SleepStatistic.TABLE_NAME);
            if (ix05.equals(a6)) {
                return new Qh.Bi(true, null);
            }
            return new Qh.Bi(false, "sleep_statistic(com.portfolio.platform.data.SleepStatistic).\n Expected:\n" + ix05 + "\n Found:\n" + a6);
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public void clearAllTables() {
        super.assertNotMainThread();
        Lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `sleep_session`");
            writableDatabase.execSQL("DELETE FROM `sleep_date`");
            writableDatabase.execSQL("DELETE FROM `sleep_settings`");
            writableDatabase.execSQL("DELETE FROM `sleepRecommendedGoals`");
            writableDatabase.execSQL("DELETE FROM `sleep_statistic`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Nw0 createInvalidationTracker() {
        return new Nw0(this, new HashMap(0), new HashMap(0), MFSleepSession.TABLE_NAME, MFSleepDay.TABLE_NAME, "sleep_settings", "sleepRecommendedGoals", SleepStatistic.TABLE_NAME);
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Ji createOpenHelper(Hw0 hw0) {
        Qh qh = new Qh(hw0, new Anon1(9), "6563ab2be16ee1f30194e9dfab2b7513", "318ff9dd57f69c3e478563bc453dc851");
        Ji.Bi.Aii a2 = Ji.Bi.a(hw0.b);
        a2.c(hw0.c);
        a2.b(qh);
        return hw0.a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDatabase
    public SleepDao sleepDao() {
        SleepDao sleepDao;
        if (this._sleepDao != null) {
            return this._sleepDao;
        }
        synchronized (this) {
            if (this._sleepDao == null) {
                this._sleepDao = new SleepDao_Impl(this);
            }
            sleepDao = this._sleepDao;
        }
        return sleepDao;
    }
}
