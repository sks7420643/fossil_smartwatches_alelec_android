package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.Pm7;
import com.mapped.Lc6;
import com.mapped.PagingRequestHelper;
import com.mapped.Rm6;
import com.mapped.Wg6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummaryLocalDataSource$loadAfter$Anon1 implements PagingRequestHelper.Bi {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource this$0;

    @DexIgnore
    public GoalTrackingSummaryLocalDataSource$loadAfter$Anon1(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        this.this$0 = goalTrackingSummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.mapped.PagingRequestHelper.Bi
    public final void run(PagingRequestHelper.Bi.Aii aii) {
        Lc6 lc6 = (Lc6) Pm7.F(this.this$0.mRequestAfterQueue);
        Wg6.b(aii, "helperCallback");
        Rm6 unused = this.this$0.loadData(PagingRequestHelper.Di.AFTER, (Date) lc6.getFirst(), (Date) lc6.getSecond(), aii);
    }
}
