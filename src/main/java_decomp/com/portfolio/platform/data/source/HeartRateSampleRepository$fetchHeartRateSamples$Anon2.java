package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.HeartRateSampleRepository$fetchHeartRateSamples$2", f = "HeartRateSampleRepository.kt", l = {95, 107, 110}, m = "invokeSuspend")
public final class HeartRateSampleRepository$fetchHeartRateSamples$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<ApiResponse<HeartRate>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSampleRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSampleRepository$fetchHeartRateSamples$Anon2(HeartRateSampleRepository heartRateSampleRepository, Date date, Date date2, int i, int i2, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = heartRateSampleRepository;
        this.$start = date;
        this.$end = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        HeartRateSampleRepository$fetchHeartRateSamples$Anon2 heartRateSampleRepository$fetchHeartRateSamples$Anon2 = new HeartRateSampleRepository$fetchHeartRateSamples$Anon2(this.this$0, this.$start, this.$end, this.$offset, this.$limit, xe6);
        heartRateSampleRepository$fetchHeartRateSamples$Anon2.p$ = (Il6) obj;
        throw null;
        //return heartRateSampleRepository$fetchHeartRateSamples$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<ApiResponse<HeartRate>>> xe6) {
        throw null;
        //return ((HeartRateSampleRepository$fetchHeartRateSamples$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v30, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0082 A[Catch:{ Exception -> 0x01c7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0177  */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r11) {
        /*
        // Method dump skipped, instructions count: 474
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HeartRateSampleRepository$fetchHeartRateSamples$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
