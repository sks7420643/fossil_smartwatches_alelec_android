package com.portfolio.platform.data.source.remote;

import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerSettingRemoteDataSource implements ServerSettingDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 apiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ServerSettingRemoteDataSource.TAG;
        }
    }

    /*
    static {
        String simpleName = ServerSettingRemoteDataSource.class.getSimpleName();
        Wg6.b(simpleName, "ServerSettingRemoteDataS\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ServerSettingRemoteDataSource(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "apiService");
        this.apiService = apiServiceV2;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void addOrUpdateServerSetting(ServerSetting serverSetting) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void addOrUpdateServerSettingList(List<ServerSetting> list) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void clearData() {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public ServerSetting getServerSettingByKey(String str) {
        Wg6.c(str, "key");
        return null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource
    public void getServerSettingList(ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        Wg6.c(onGetServerSettingList, Constants.CALLBACK);
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new ServerSettingRemoteDataSource$getServerSettingList$Anon1(this, onGetServerSettingList, null), 3, null);
    }
}
