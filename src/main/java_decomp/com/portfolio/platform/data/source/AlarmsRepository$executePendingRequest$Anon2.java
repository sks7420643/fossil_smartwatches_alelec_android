package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Kq5;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$2", f = "AlarmsRepository.kt", l = {203, 212, 214, 229, 231}, m = "invokeSuspend")
public final class AlarmsRepository$executePendingRequest$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$2$2", f = "AlarmsRepository.kt", l = {216}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Long[]>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ap4 $upsertResponse;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(Ap4 ap4, Xe6 xe6) {
            super(2, xe6);
            this.$upsertResponse = ap4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.$upsertResponse, xe6);
            anon2_Level2.p$ = (Il6) obj;
            throw null;
            //return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Long[]> xe6) {
            throw null;
            //return ((Anon2_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object u;
            List<Alarm> list;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                List<Alarm> list2 = (List) ((Kq5) this.$upsertResponse).a();
                if (list2 == null) {
                    return null;
                }
                EncryptedDatabaseManager encryptedDatabaseManager = EncryptedDatabaseManager.j;
                this.L$0 = il6;
                this.L$1 = list2;
                this.label = 1;
                u = encryptedDatabaseManager.u(this);
                if (u == d) {
                    return d;
                }
                list = list2;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                list = (List) this.L$1;
                u = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return ((AlarmDatabase) u).alarmDao().insertAlarms(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$2$4", f = "AlarmsRepository.kt", l = {232}, m = "invokeSuspend")
    public static final class Anon4_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $deleteAlarmPendingList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon4_Level2(List list, Xe6 xe6) {
            super(2, xe6);
            this.$deleteAlarmPendingList = list;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon4_Level2 anon4_Level2 = new Anon4_Level2(this.$deleteAlarmPendingList, xe6);
            anon4_Level2.p$ = (Il6) obj;
            throw null;
            //return anon4_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Anon4_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r2v6, types: [java.lang.Iterable] */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0039  */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                r11 = this;
                r10 = 1
                java.lang.Object r5 = com.fossil.Yn7.d()
                int r0 = r11.label
                if (r0 == 0) goto L_0x005d
                if (r0 != r10) goto L_0x0055
                java.lang.Object r0 = r11.L$4
                com.portfolio.platform.data.source.local.alarm.Alarm r0 = (com.portfolio.platform.data.source.local.alarm.Alarm) r0
                java.lang.Object r1 = r11.L$2
                java.util.Iterator r1 = (java.util.Iterator) r1
                java.lang.Object r2 = r11.L$1
                java.lang.Iterable r2 = (java.lang.Iterable) r2
                java.lang.Object r3 = r11.L$0
                com.mapped.Il6 r3 = (com.mapped.Il6) r3
                com.fossil.El7.b(r12)
                r6 = r0
                r7 = r2
                r9 = r3
                r4 = r12
            L_0x0022:
                r0 = r4
                com.portfolio.platform.data.source.local.alarm.AlarmDatabase r0 = (com.portfolio.platform.data.source.local.alarm.AlarmDatabase) r0
                com.portfolio.platform.data.source.local.alarm.AlarmDao r0 = r0.alarmDao()
                java.lang.String r2 = r6.getUri()
                r0.removeAlarm(r2)
                r8 = r1
                r3 = r7
                r2 = r5
            L_0x0033:
                boolean r0 = r8.hasNext()
                if (r0 == 0) goto L_0x006f
                java.lang.Object r1 = r8.next()
                r0 = r1
                com.portfolio.platform.data.source.local.alarm.Alarm r0 = (com.portfolio.platform.data.source.local.alarm.Alarm) r0
                com.portfolio.platform.manager.EncryptedDatabaseManager r4 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
                r11.L$0 = r9
                r11.L$1 = r3
                r11.L$2 = r8
                r11.L$3 = r1
                r11.L$4 = r0
                r11.label = r10
                java.lang.Object r4 = r4.u(r11)
                if (r4 != r2) goto L_0x006a
            L_0x0054:
                return r2
            L_0x0055:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x005d:
                com.fossil.El7.b(r12)
                com.mapped.Il6 r9 = r11.p$
                java.util.List r3 = r11.$deleteAlarmPendingList
                java.util.Iterator r8 = r3.iterator()
                r2 = r5
                goto L_0x0033
            L_0x006a:
                r5 = r2
                r6 = r0
                r7 = r3
                r1 = r8
                goto L_0x0022
            L_0x006f:
                com.mapped.Cd6 r2 = com.mapped.Cd6.a
                goto L_0x0054
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$Anon2.Anon4_Level2.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$executePendingRequest$Anon2(AlarmsRepository alarmsRepository, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = alarmsRepository;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        AlarmsRepository$executePendingRequest$Anon2 alarmsRepository$executePendingRequest$Anon2 = new AlarmsRepository$executePendingRequest$Anon2(this.this$0, xe6);
        alarmsRepository$executePendingRequest$Anon2.p$ = (Il6) obj;
        throw null;
        //return alarmsRepository$executePendingRequest$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
        throw null;
        //return ((AlarmsRepository$executePendingRequest$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v80, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r1v20, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r0v84, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r1v23, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r0v86, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b8 A[LOOP:0: B:25:0x00b2->B:27:0x00b8, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0170  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x018c  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x027f  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x029c  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x02b6  */
    /* JADX WARNING: Unknown variable types count: 5 */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r14) {
        /*
        // Method dump skipped, instructions count: 712
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.AlarmsRepository$executePendingRequest$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
