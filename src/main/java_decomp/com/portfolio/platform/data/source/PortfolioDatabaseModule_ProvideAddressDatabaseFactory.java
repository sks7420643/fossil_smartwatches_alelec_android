package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.AddressDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideAddressDatabaseFactory implements Factory<AddressDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideAddressDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideAddressDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideAddressDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static AddressDatabase provideAddressDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        AddressDatabase provideAddressDatabase = portfolioDatabaseModule.provideAddressDatabase(portfolioApp);
        Lk7.c(provideAddressDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideAddressDatabase;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public AddressDatabase get() {
        return provideAddressDatabase(this.module, this.appProvider.get());
    }
}
