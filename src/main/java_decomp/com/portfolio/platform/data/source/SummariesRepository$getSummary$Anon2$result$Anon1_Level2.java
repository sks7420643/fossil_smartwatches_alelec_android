package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.H47;
import com.fossil.Q88;
import com.fossil.Ss0;
import com.mapped.Cd6;
import com.mapped.Ku3;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$getSummary$Anon2$result$Anon1_Level2<I, O> implements V3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ FitnessDatabase $fitnessDatabase;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummary$Anon2 this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level3 extends NetworkBoundResource<ActivitySummary, Ku3> {
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository$getSummary$Anon2$result$Anon1_Level2 this$0;

        @DexIgnore
        public Anon1_Level3(SummariesRepository$getSummary$Anon2$result$Anon1_Level2 summariesRepository$getSummary$Anon2$result$Anon1_Level2, List list) {
            this.this$0 = summariesRepository$getSummary$Anon2$result$Anon1_Level2;
            this.$fitnessDataList = list;
        }

        @DexIgnore
        @Override // com.portfolio.platform.util.NetworkBoundResource
        public Object createCall(Xe6<? super Q88<Ku3>> xe6) {
            Date V = TimeUtils.V(this.this$0.this$0.$date);
            Date E = TimeUtils.E(this.this$0.this$0.$date);
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "calendar");
            instance.setTimeInMillis(0);
            ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiServiceV2;
            String k = TimeUtils.k(V);
            Wg6.b(k, "DateHelper.formatShortDate(startDate)");
            String k2 = TimeUtils.k(E);
            Wg6.b(k2, "DateHelper.formatShortDate(endDate)");
            return apiServiceV2.getSummaries(k, k2, 0, 100, xe6);
        }

        @DexIgnore
        @Override // com.portfolio.platform.util.NetworkBoundResource
        public Object loadFromDb(Xe6<? super LiveData<ActivitySummary>> xe6) {
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "calendar");
            instance.setTime(this.this$0.this$0.$date);
            LiveData<ActivitySummary> activitySummaryLiveData = this.this$0.$fitnessDatabase.activitySummaryDao().getActivitySummaryLiveData(instance.get(1), instance.get(2) + 1, instance.get(5));
            if (!TimeUtils.p0(this.this$0.this$0.$date).booleanValue()) {
                return activitySummaryLiveData;
            }
            LiveData b = Ss0.b(activitySummaryLiveData, new SummariesRepository$getSummary$Anon2$result$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4(this));
            Wg6.b(b, "Transformations.map(acti\u2026ary\n                    }");
            return b;
        }

        @DexIgnore
        @Override // com.portfolio.platform.util.NetworkBoundResource
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().e(SummariesRepository.TAG, "getSummary - onFetchFailed");
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:27:0x013d  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x0140  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Object saveCallResult(com.mapped.Ku3 r15, com.mapped.Xe6<? super com.mapped.Cd6> r16) {
            /*
            // Method dump skipped, instructions count: 526
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository$getSummary$Anon2$result$Anon1_Level2.Anon1_Level3.saveCallResult(com.mapped.Ku3, com.mapped.Xe6):java.lang.Object");
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.mapped.Xe6] */
        @Override // com.portfolio.platform.util.NetworkBoundResource
        public /* bridge */ /* synthetic */ Object saveCallResult(Ku3 ku3, Xe6 xe6) {
            return saveCallResult(ku3, (Xe6<? super Cd6>) xe6);
        }

        @DexIgnore
        public boolean shouldFetch(ActivitySummary activitySummary) {
            return this.$fitnessDataList.isEmpty();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.util.NetworkBoundResource
        public /* bridge */ /* synthetic */ boolean shouldFetch(ActivitySummary activitySummary) {
            return shouldFetch(activitySummary);
        }
    }

    @DexIgnore
    public SummariesRepository$getSummary$Anon2$result$Anon1_Level2(SummariesRepository$getSummary$Anon2 summariesRepository$getSummary$Anon2, FitnessDatabase fitnessDatabase) {
        this.this$0 = summariesRepository$getSummary$Anon2;
        this.$fitnessDatabase = fitnessDatabase;
    }

    @DexIgnore
    public final LiveData<H47<ActivitySummary>> apply(List<FitnessDataWrapper> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getSummary - date=" + this.this$0.$date + " fitnessDataList=" + list.size());
        return new Anon1_Level3(this, list).asLiveData();
    }

    @DexIgnore
    @Override // com.mapped.V3
    public /* bridge */ /* synthetic */ Object apply(Object obj) {
        return apply((List) obj);
    }
}
