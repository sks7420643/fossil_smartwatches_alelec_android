package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.Category;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CategoryDao_Impl implements CategoryDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<Category> __insertionAdapterOfCategory;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<Category> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, Category category) {
            if (category.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, category.getId());
            }
            if (category.getEnglishName() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, category.getEnglishName());
            }
            if (category.getName() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, category.getName());
            }
            if (category.getUpdatedAt() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, category.getUpdatedAt());
            }
            if (category.getCreatedAt() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, category.getCreatedAt());
            }
            mi.bindLong(6, (long) category.getPriority());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, Category category) {
            bind(mi, category);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `category` (`id`,`englishName`,`name`,`updatedAt`,`createdAt`,`priority`) VALUES (?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM category";
        }
    }

    @DexIgnore
    public CategoryDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfCategory = new Anon1(oh);
        this.__preparedStmtOfClearData = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CategoryDao
    public void clearData() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CategoryDao
    public List<Category> getAllCategory() {
        Rh f = Rh.f("SELECT * FROM category ORDER BY priority DESC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "englishName");
            int c3 = Dx0.c(b, "name");
            int c4 = Dx0.c(b, "updatedAt");
            int c5 = Dx0.c(b, "createdAt");
            int c6 = Dx0.c(b, "priority");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new Category(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5), b.getInt(c6)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CategoryDao
    public Category getCategoryById(String str) {
        Category category = null;
        Rh f = Rh.f("SELECT * FROM category WHERE id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "englishName");
            int c3 = Dx0.c(b, "name");
            int c4 = Dx0.c(b, "updatedAt");
            int c5 = Dx0.c(b, "createdAt");
            int c6 = Dx0.c(b, "priority");
            if (b.moveToFirst()) {
                category = new Category(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5), b.getInt(c6));
            }
            return category;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CategoryDao
    public void upsertCategoryList(List<Category> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfCategory.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
