package com.portfolio.platform.data.source.local.workoutsetting;

import com.fossil.Ex0;
import com.fossil.Hw0;
import com.fossil.Ix0;
import com.fossil.Lx0;
import com.fossil.Nw0;
import com.mapped.Ji;
import com.mapped.Oh;
import com.mapped.Qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingDatabase_Impl extends WorkoutSettingDatabase {
    @DexIgnore
    public volatile WorkoutSettingDao _workoutSettingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Qh.Ai {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void createAllTables(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `workoutSetting` (`type` TEXT NOT NULL, `mode` TEXT, `enable` INTEGER NOT NULL, `askMeFirst` INTEGER NOT NULL, `startLatency` INTEGER NOT NULL, `pauseLatency` INTEGER NOT NULL, `resumeLatency` INTEGER NOT NULL, `stopLatency` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`type`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '8ebd946c2b78fdb442012d32eb1b6201')");
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void dropAllTables(Lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `workoutSetting`");
            if (WorkoutSettingDatabase_Impl.this.mCallbacks != null) {
                int size = WorkoutSettingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) WorkoutSettingDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onCreate(Lx0 lx0) {
            if (WorkoutSettingDatabase_Impl.this.mCallbacks != null) {
                int size = WorkoutSettingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) WorkoutSettingDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onOpen(Lx0 lx0) {
            WorkoutSettingDatabase_Impl.this.mDatabase = lx0;
            WorkoutSettingDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (WorkoutSettingDatabase_Impl.this.mCallbacks != null) {
                int size = WorkoutSettingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) WorkoutSettingDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPostMigrate(Lx0 lx0) {
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPreMigrate(Lx0 lx0) {
            Ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public Qh.Bi onValidateSchema(Lx0 lx0) {
            HashMap hashMap = new HashMap(11);
            hashMap.put("type", new Ix0.Ai("type", "TEXT", true, 1, null, 1));
            hashMap.put("mode", new Ix0.Ai("mode", "TEXT", false, 0, null, 1));
            hashMap.put("enable", new Ix0.Ai("enable", "INTEGER", true, 0, null, 1));
            hashMap.put("askMeFirst", new Ix0.Ai("askMeFirst", "INTEGER", true, 0, null, 1));
            hashMap.put("startLatency", new Ix0.Ai("startLatency", "INTEGER", true, 0, null, 1));
            hashMap.put("pauseLatency", new Ix0.Ai("pauseLatency", "INTEGER", true, 0, null, 1));
            hashMap.put("resumeLatency", new Ix0.Ai("resumeLatency", "INTEGER", true, 0, null, 1));
            hashMap.put("stopLatency", new Ix0.Ai("stopLatency", "INTEGER", true, 0, null, 1));
            hashMap.put("createdAt", new Ix0.Ai("createdAt", "INTEGER", true, 0, null, 1));
            hashMap.put("updatedAt", new Ix0.Ai("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap.put("pinType", new Ix0.Ai("pinType", "INTEGER", true, 0, null, 1));
            Ix0 ix0 = new Ix0("workoutSetting", hashMap, new HashSet(0), new HashSet(0));
            Ix0 a2 = Ix0.a(lx0, "workoutSetting");
            if (ix0.equals(a2)) {
                return new Qh.Bi(true, null);
            }
            return new Qh.Bi(false, "workoutSetting(com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public void clearAllTables() {
        super.assertNotMainThread();
        Lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `workoutSetting`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Nw0 createInvalidationTracker() {
        return new Nw0(this, new HashMap(0), new HashMap(0), "workoutSetting");
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Ji createOpenHelper(Hw0 hw0) {
        Qh qh = new Qh(hw0, new Anon1(2), "8ebd946c2b78fdb442012d32eb1b6201", "2baade49018ab002e0858caff419b24c");
        Ji.Bi.Aii a2 = Ji.Bi.a(hw0.b);
        a2.c(hw0.c);
        a2.b(qh);
        return hw0.a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase
    public WorkoutSettingDao workoutSettingDao() {
        WorkoutSettingDao workoutSettingDao;
        if (this._workoutSettingDao != null) {
            return this._workoutSettingDao;
        }
        synchronized (this) {
            if (this._workoutSettingDao == null) {
                this._workoutSettingDao = new WorkoutSettingDao_Impl(this);
            }
            workoutSettingDao = this._workoutSettingDao;
        }
        return workoutSettingDao;
    }
}
