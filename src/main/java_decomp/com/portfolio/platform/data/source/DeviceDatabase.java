package com.portfolio.platform.data.source;

import com.mapped.Oh;
import com.mapped.Qg6;
import com.mapped.Xh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class DeviceDatabase extends Oh {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ Xh MIGRATION_FROM_1_TO_2; // = new DeviceDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1(1, 2);
    @DexIgnore
    public static /* final */ Xh MIGRATION_FROM_2_TO_3; // = new DeviceDatabase$Companion$MIGRATION_FROM_2_TO_3$Anon1(2, 3);
    @DexIgnore
    public static /* final */ Xh MIGRATION_FROM_3_TO_4; // = new DeviceDatabase$Companion$MIGRATION_FROM_3_TO_4$Anon1(3, 4);
    @DexIgnore
    public static /* final */ String TAG; // = "DeviceDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Xh getMIGRATION_FROM_1_TO_2() {
            return DeviceDatabase.MIGRATION_FROM_1_TO_2;
        }

        @DexIgnore
        public final Xh getMIGRATION_FROM_2_TO_3() {
            return DeviceDatabase.MIGRATION_FROM_2_TO_3;
        }

        @DexIgnore
        public final Xh getMIGRATION_FROM_3_TO_4() {
            return DeviceDatabase.MIGRATION_FROM_3_TO_4;
        }
    }

    @DexIgnore
    public abstract DeviceDao deviceDao();

    @DexIgnore
    public abstract SkuDao skuDao();
}
