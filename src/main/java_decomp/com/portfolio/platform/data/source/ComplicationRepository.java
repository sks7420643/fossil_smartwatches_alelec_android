package com.portfolio.platform.data.source;

import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.remote.ComplicationRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "ComplicationRepository";
    @DexIgnore
    public /* final */ ComplicationRemoteDataSource mComplicationRemoteDataSource;
    @DexIgnore
    public /* final */ PortfolioApp mPortfolioApp;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public ComplicationRepository(ComplicationRemoteDataSource complicationRemoteDataSource, PortfolioApp portfolioApp) {
        Wg6.c(complicationRemoteDataSource, "mComplicationRemoteDataSource");
        Wg6.c(portfolioApp, "mPortfolioApp");
        this.mComplicationRemoteDataSource = complicationRemoteDataSource;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object cleanUp(com.mapped.Xe6<? super com.mapped.Cd6> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.ComplicationRepository$cleanUp$Anon1
            if (r0 == 0) goto L_0x0033
            r0 = r6
            com.portfolio.platform.data.source.ComplicationRepository$cleanUp$Anon1 r0 = (com.portfolio.platform.data.source.ComplicationRepository$cleanUp$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.ComplicationRepository r0 = (com.portfolio.platform.data.source.ComplicationRepository) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.ComplicationDao r0 = r0.getComplicationDao()
            r0.clearAll()
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.data.source.ComplicationRepository$cleanUp$Anon1 r0 = new com.portfolio.platform.data.source.ComplicationRepository$cleanUp$Anon1
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r1)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ComplicationRepository.cleanUp(com.mapped.Xe6):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0057, code lost:
        if ((!((java.util.Collection) r2).isEmpty()) == false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x005d, code lost:
        if (r0.isEmpty() == false) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x005f, code lost:
        r6.clearAll();
        r0 = r1.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0066, code lost:
        if (r0 == null) goto L_0x0147;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0068, code lost:
        r6.upsertComplicationList((java.util.List) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0147, code lost:
        com.mapped.Wg6.i();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x014a, code lost:
        throw null;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x014b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadAllComplication(java.lang.String r12, com.mapped.Xe6<? super com.mapped.Cd6> r13) {
        /*
        // Method dump skipped, instructions count: 399
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ComplicationRepository.downloadAllComplication(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getAllComplicationRaw(com.mapped.Xe6<? super java.util.List<com.portfolio.platform.data.model.diana.Complication>> r7) {
        /*
            r6 = this;
            r5 = 2
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.ComplicationRepository$getAllComplicationRaw$Anon1
            if (r0 == 0) goto L_0x002b
            r0 = r7
            com.portfolio.platform.data.source.ComplicationRepository$getAllComplicationRaw$Anon1 r0 = (com.portfolio.platform.data.source.ComplicationRepository$getAllComplicationRaw$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002b
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0015:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0055
            if (r0 == r4) goto L_0x003a
            if (r0 != r5) goto L_0x0032
            java.lang.Object r0 = r2.L$0
            com.portfolio.platform.data.source.ComplicationRepository r0 = (com.portfolio.platform.data.source.ComplicationRepository) r0
            com.fossil.El7.b(r1)
        L_0x002a:
            return r1
        L_0x002b:
            com.portfolio.platform.data.source.ComplicationRepository$getAllComplicationRaw$Anon1 r0 = new com.portfolio.platform.data.source.ComplicationRepository$getAllComplicationRaw$Anon1
            r0.<init>(r6, r7)
            r2 = r0
            goto L_0x0015
        L_0x0032:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003a:
            java.lang.Object r0 = r2.L$0
            com.portfolio.platform.data.source.ComplicationRepository r0 = (com.portfolio.platform.data.source.ComplicationRepository) r0
            com.fossil.El7.b(r1)
            r6 = r0
        L_0x0042:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.ComplicationDao r0 = r0.getComplicationDao()
            r2.L$0 = r6
            r2.label = r5
            java.lang.Object r1 = r0.getAllComplications(r2)
            if (r1 != r3) goto L_0x002a
            r1 = r3
            goto L_0x002a
        L_0x0055:
            com.fossil.El7.b(r1)
            com.portfolio.platform.manager.EncryptedDatabaseManager r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r2.L$0 = r6
            r2.label = r4
            java.lang.Object r1 = r0.v(r2)
            if (r1 != r3) goto L_0x0042
            r1 = r3
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ComplicationRepository.getAllComplicationRaw(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getComplicationById(java.lang.String r8, com.mapped.Xe6<? super com.portfolio.platform.data.model.diana.Complication> r9) {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.ComplicationRepository$getComplicationById$Anon1
            if (r0 == 0) goto L_0x002f
            r0 = r9
            com.portfolio.platform.data.source.ComplicationRepository$getComplicationById$Anon1 r0 = (com.portfolio.platform.data.source.ComplicationRepository$getComplicationById$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002f
            int r1 = r1 + r3
            r0.label = r1
            r3 = r0
        L_0x0015:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.Yn7.d()
            int r0 = r3.label
            if (r0 == 0) goto L_0x005f
            if (r0 == r5) goto L_0x003e
            if (r0 != r6) goto L_0x0036
            java.lang.Object r0 = r3.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r3.L$0
            com.portfolio.platform.data.source.ComplicationRepository r0 = (com.portfolio.platform.data.source.ComplicationRepository) r0
            com.fossil.El7.b(r2)
        L_0x002e:
            return r2
        L_0x002f:
            com.portfolio.platform.data.source.ComplicationRepository$getComplicationById$Anon1 r0 = new com.portfolio.platform.data.source.ComplicationRepository$getComplicationById$Anon1
            r0.<init>(r7, r9)
            r3 = r0
            goto L_0x0015
        L_0x0036:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003e:
            java.lang.Object r0 = r3.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r3.L$0
            com.portfolio.platform.data.source.ComplicationRepository r1 = (com.portfolio.platform.data.source.ComplicationRepository) r1
            com.fossil.El7.b(r2)
            r8 = r0
        L_0x004a:
            r0 = r2
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.ComplicationDao r0 = r0.getComplicationDao()
            r3.L$0 = r1
            r3.L$1 = r8
            r3.label = r6
            java.lang.Object r2 = r0.getComplicationById(r8, r3)
            if (r2 != r4) goto L_0x002e
            r2 = r4
            goto L_0x002e
        L_0x005f:
            com.fossil.El7.b(r2)
            com.portfolio.platform.manager.EncryptedDatabaseManager r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r3.L$0 = r7
            r3.L$1 = r8
            r3.label = r5
            java.lang.Object r2 = r0.v(r3)
            if (r2 != r4) goto L_0x0072
            r2 = r4
            goto L_0x002e
        L_0x0072:
            r1 = r7
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ComplicationRepository.getComplicationById(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getComplicationByIds(java.util.List<java.lang.String> r6, com.mapped.Xe6<? super java.util.List<com.portfolio.platform.data.model.diana.Complication>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.ComplicationRepository$getComplicationByIds$Anon1
            if (r0 == 0) goto L_0x0042
            r0 = r7
            com.portfolio.platform.data.source.ComplicationRepository$getComplicationByIds$Anon1 r0 = (com.portfolio.platform.data.source.ComplicationRepository$getComplicationByIds$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0042
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0051
            if (r3 != r4) goto L_0x0049
            java.lang.Object r0 = r1.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.ComplicationRepository r1 = (com.portfolio.platform.data.source.ComplicationRepository) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.ComplicationDao r0 = r0.getComplicationDao()
            java.util.List r0 = r0.getComplicationByIds(r6)
            com.portfolio.platform.data.source.ComplicationRepository$getComplicationByIds$$inlined$sortedBy$Anon1 r1 = new com.portfolio.platform.data.source.ComplicationRepository$getComplicationByIds$$inlined$sortedBy$Anon1
            r1.<init>(r6)
            java.util.List r0 = com.fossil.Pm7.b0(r0, r1)
        L_0x0041:
            return r0
        L_0x0042:
            com.portfolio.platform.data.source.ComplicationRepository$getComplicationByIds$Anon1 r0 = new com.portfolio.platform.data.source.ComplicationRepository$getComplicationByIds$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0049:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0051:
            com.fossil.El7.b(r2)
            boolean r2 = r6.isEmpty()
            r2 = r2 ^ 1
            if (r2 == 0) goto L_0x006b
            com.portfolio.platform.manager.EncryptedDatabaseManager r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0041
        L_0x006b:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ComplicationRepository.getComplicationByIds(java.util.List, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v13, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r0v16, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object queryComplicationByName(java.lang.String r10, com.mapped.Xe6<? super java.util.List<com.portfolio.platform.data.model.diana.Complication>> r11) {
        /*
            r9 = this;
            r7 = 2
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r8 = 1
            boolean r0 = r11 instanceof com.portfolio.platform.data.source.ComplicationRepository$queryComplicationByName$Anon1
            if (r0 == 0) goto L_0x0074
            r0 = r11
            com.portfolio.platform.data.source.ComplicationRepository$queryComplicationByName$Anon1 r0 = (com.portfolio.platform.data.source.ComplicationRepository$queryComplicationByName$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0074
            int r1 = r1 + r3
            r0.label = r1
            r4 = r0
        L_0x0015:
            java.lang.Object r3 = r4.result
            java.lang.Object r6 = com.fossil.Yn7.d()
            int r0 = r4.label
            if (r0 == 0) goto L_0x00aa
            if (r0 == r8) goto L_0x0083
            if (r0 != r7) goto L_0x007b
            java.lang.Object r0 = r4.L$2
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r1 = r4.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r4.L$0
            com.portfolio.platform.data.source.ComplicationRepository r2 = (com.portfolio.platform.data.source.ComplicationRepository) r2
            com.fossil.El7.b(r3)
            r5 = r0
        L_0x0033:
            r0 = r3
            java.util.List r0 = (java.util.List) r0
            java.util.Iterator r3 = r0.iterator()
        L_0x003a:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x00a9
            java.lang.Object r0 = r3.next()
            com.portfolio.platform.data.model.diana.Complication r0 = (com.portfolio.platform.data.model.diana.Complication) r0
            com.portfolio.platform.PortfolioApp r4 = r2.mPortfolioApp
            java.lang.String r6 = r0.getNameKey()
            java.lang.String r7 = r0.getName()
            java.lang.String r4 = com.fossil.Um5.d(r4, r6, r7)
            java.text.Normalizer$Form r6 = java.text.Normalizer.Form.NFC
            java.lang.String r4 = java.text.Normalizer.normalize(r4, r6)
            java.text.Normalizer$Form r6 = java.text.Normalizer.Form.NFC
            java.lang.String r6 = java.text.Normalizer.normalize(r1, r6)
            java.lang.String r7 = "name"
            com.mapped.Wg6.b(r4, r7)
            java.lang.String r7 = "searchQuery"
            com.mapped.Wg6.b(r6, r7)
            boolean r4 = com.fossil.Wt7.u(r4, r6, r8)
            if (r4 == 0) goto L_0x003a
            r5.add(r0)
            goto L_0x003a
        L_0x0074:
            com.portfolio.platform.data.source.ComplicationRepository$queryComplicationByName$Anon1 r0 = new com.portfolio.platform.data.source.ComplicationRepository$queryComplicationByName$Anon1
            r0.<init>(r9, r11)
            r4 = r0
            goto L_0x0015
        L_0x007b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0083:
            java.lang.Object r0 = r4.L$2
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r1 = r4.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r4.L$0
            com.portfolio.platform.data.source.ComplicationRepository r2 = (com.portfolio.platform.data.source.ComplicationRepository) r2
            com.fossil.El7.b(r3)
            r5 = r0
        L_0x0093:
            r0 = r3
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.ComplicationDao r0 = r0.getComplicationDao()
            r4.L$0 = r2
            r4.L$1 = r1
            r4.L$2 = r5
            r4.label = r7
            java.lang.Object r3 = r0.getAllComplications(r4)
            if (r3 != r6) goto L_0x0033
            r5 = r6
        L_0x00a9:
            return r5
        L_0x00aa:
            com.fossil.El7.b(r3)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r4.L$0 = r9
            r4.L$1 = r10
            r4.L$2 = r0
            r4.label = r8
            java.lang.Object r3 = r1.v(r4)
            if (r3 != r6) goto L_0x00c4
            r5 = r6
            goto L_0x00a9
        L_0x00c4:
            r1 = r10
            r5 = r0
            r2 = r9
            goto L_0x0093
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ComplicationRepository.queryComplicationByName(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }
}
