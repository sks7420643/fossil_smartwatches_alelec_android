package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.util.Date;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SleepSessionsRepository$fetchSleepSessions$2", f = "SleepSessionsRepository.kt", l = {DateTimeConstants.HOURS_PER_WEEK, 179, 182}, m = "invokeSuspend")
public final class SleepSessionsRepository$fetchSleepSessions$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<Ku3>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$fetchSleepSessions$Anon2(SleepSessionsRepository sleepSessionsRepository, Date date, Date date2, int i, int i2, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = sleepSessionsRepository;
        this.$start = date;
        this.$end = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SleepSessionsRepository$fetchSleepSessions$Anon2 sleepSessionsRepository$fetchSleepSessions$Anon2 = new SleepSessionsRepository$fetchSleepSessions$Anon2(this.this$0, this.$start, this.$end, this.$offset, this.$limit, xe6);
        sleepSessionsRepository$fetchSleepSessions$Anon2.p$ = (Il6) obj;
        throw null;
        //return sleepSessionsRepository$fetchSleepSessions$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<Ku3>> xe6) {
        throw null;
        //return ((SleepSessionsRepository$fetchSleepSessions$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0075 A[SYNTHETIC, Splitter:B:24:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x007b A[Catch:{ Exception -> 0x01e8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x018b  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0195  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r13) {
        /*
        // Method dump skipped, instructions count: 507
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository$fetchSleepSessions$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
