package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getPendingGoalTrackingDataList$2", f = "GoalTrackingRepository.kt", l = {277}, m = "invokeSuspend")
public final class GoalTrackingRepository$getPendingGoalTrackingDataList$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super List<GoalTrackingData>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getPendingGoalTrackingDataList$Anon2(Date date, Date date2, Xe6 xe6) {
        super(2, xe6);
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        GoalTrackingRepository$getPendingGoalTrackingDataList$Anon2 goalTrackingRepository$getPendingGoalTrackingDataList$Anon2 = new GoalTrackingRepository$getPendingGoalTrackingDataList$Anon2(this.$startDate, this.$endDate, xe6);
        goalTrackingRepository$getPendingGoalTrackingDataList$Anon2.p$ = (Il6) obj;
        throw null;
        //return goalTrackingRepository$getPendingGoalTrackingDataList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super List<GoalTrackingData>> xe6) {
        throw null;
        //return ((GoalTrackingRepository$getPendingGoalTrackingDataList$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object A;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            EncryptedDatabaseManager encryptedDatabaseManager = EncryptedDatabaseManager.j;
            this.L$0 = il6;
            this.label = 1;
            A = encryptedDatabaseManager.A(this);
            if (A == d) {
                return d;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            A = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return ((GoalTrackingDatabase) A).getGoalTrackingDao().getPendingGoalTrackingDataList(this.$startDate, this.$endDate);
    }
}
