package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.Bw7;
import com.fossil.Gl5;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Jv7;
import com.fossil.Nw0;
import com.fossil.Pm7;
import com.mapped.Af;
import com.mapped.Lc6;
import com.mapped.PagingRequestHelper;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.U04;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummaryLocalDataSource extends Af<Date, GoalTrackingSummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "GoalTrackingSummaryLocalDataSource";
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ PagingRequestHelper.Ai listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public PagingRequestHelper mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ Nw0.Ci mObserver;
    @DexIgnore
    public List<Lc6<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public Date mStartDate; // = new Date();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends Nw0.Ci {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = goalTrackingSummaryLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Nw0.Ci
        public void onInvalidated(Set<String> set) {
            Wg6.c(set, "tables");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            Wg6.c(date, "date");
            Wg6.c(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(GoalTrackingSummaryLocalDataSource.TAG, "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar I = TimeUtils.I(instance);
            if (TimeUtils.j0(date2, I.getTime())) {
                I.setTime(date2);
            }
            Date time = I.getTime();
            Wg6.b(time, "nextPagedKey.time");
            return time;
        }
    }

    @DexIgnore
    public GoalTrackingSummaryLocalDataSource(GoalTrackingRepository goalTrackingRepository, Date date, GoalTrackingDatabase goalTrackingDatabase, U04 u04, PagingRequestHelper.Ai ai, Calendar calendar) {
        Wg6.c(goalTrackingRepository, "mGoalTrackingRepository");
        Wg6.c(date, "mCreatedDate");
        Wg6.c(goalTrackingDatabase, "mGoalTrackingDatabase");
        Wg6.c(u04, "appExecutors");
        Wg6.c(ai, "listener");
        Wg6.c(calendar, "key");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mCreatedDate = date;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.listener = ai;
        this.key = calendar;
        PagingRequestHelper pagingRequestHelper = new PagingRequestHelper(u04.a());
        this.mHelper = pagingRequestHelper;
        this.mNetworkState = Gl5.b(pagingRequestHelper);
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "goalTrackingDay", new String[0]);
        this.mGoalTrackingDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar I = TimeUtils.I(instance);
        I.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + I.getTime());
        Date time = I.getTime();
        Wg6.b(time, "calendar.time");
        this.mStartDate = time;
        if (TimeUtils.j0(date, time)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final List<GoalTrackingSummary> calculateSummaries(List<GoalTrackingSummary> list) {
        int i;
        int i2;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "endCalendar");
            instance.setTime(((GoalTrackingSummary) Pm7.P(list)).getDate());
            if (instance.get(7) != 1) {
                Calendar b0 = TimeUtils.b0(instance.getTime());
                Wg6.b(b0, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                GoalTrackingDao goalTrackingDao = this.mGoalTrackingDatabase.getGoalTrackingDao();
                Date time = b0.getTime();
                Wg6.b(time, "startCalendar.time");
                Date time2 = instance.getTime();
                Wg6.b(time2, "endCalendar.time");
                GoalTrackingDao.TotalSummary goalTrackingValueAndTarget = goalTrackingDao.getGoalTrackingValueAndTarget(time, time2);
                i2 = goalTrackingValueAndTarget.getValues();
                i = goalTrackingValueAndTarget.getTargets();
            } else {
                i = 0;
                i2 = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            Wg6.b(instance2, "calendar");
            instance2.setTime(((GoalTrackingSummary) Pm7.F(list)).getDate());
            Calendar b02 = TimeUtils.b0(instance2.getTime());
            Wg6.b(b02, "DateHelper.getStartOfWeek(calendar.time)");
            b02.add(5, -1);
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            for (T t : list) {
                if (i3 >= 0) {
                    T t2 = t;
                    Date component1 = t2.component1();
                    int component2 = t2.component2();
                    int component3 = t2.component3();
                    if (TimeUtils.m0(component1, b02.getTime())) {
                        list.get(i6).setTotalValueOfWeek(i5);
                        list.get(i6).setTotalTargetOfWeek(i4);
                        b02.add(5, -7);
                        i4 = 0;
                        i5 = 0;
                        i6 = i3;
                    }
                    i5 += component2;
                    i4 += component3;
                    if (i3 == list.size() - 1) {
                        i5 += i2;
                        i4 += i;
                    }
                    i3++;
                } else {
                    Hm7.l();
                    throw null;
                }
            }
            list.get(i6).setTotalValueOfWeek(i5);
            list.get(i6).setTotalTargetOfWeek(i4);
            FLogger.INSTANCE.getLocal().d(TAG, "calculateSummaries summaries.size=" + list.size());
        }
        return list;
    }

    @DexIgnore
    private final GoalTrackingSummary dummySummary(Date date) {
        return new GoalTrackingSummary(date, 0, this.mGoalTrackingDatabase.getGoalTrackingDao().getNearestGoalTrackingTargetFromDate(date), new Date().getTime(), new Date().getTime());
    }

    @DexIgnore
    private final List<GoalTrackingSummary> getDataInDatabase(Date date, Date date2) {
        FLogger.INSTANCE.getLocal().d(TAG, "getDataInDatabase - startDate=" + date + ", endDate=" + date2);
        List<GoalTrackingSummary> goalTrackingSummaries = this.mGoalTrackingRepository.getGoalTrackingSummaries(date, date2, this.mGoalTrackingDatabase.getGoalTrackingDao());
        List<GoalTrackingSummary> arrayList = new ArrayList<>();
        int size = goalTrackingSummaries.size() + -1;
        FLogger.INSTANCE.getLocal().d(TAG, "getDataInDatabase - summaries.size=" + goalTrackingSummaries.size() + ", startDate=" + date + ", endDateToFill=" + date2);
        while (TimeUtils.k0(date2, date)) {
            if (size < 0 || !TimeUtils.m0(goalTrackingSummaries.get(size).getDate(), date2)) {
                arrayList.add(dummySummary(date2));
                size = size;
            } else {
                arrayList.add(goalTrackingSummaries.get(size));
                size--;
            }
            date2 = TimeUtils.P(date2);
            Wg6.b(date2, "DateHelper.getPrevDate(endDateToFill)");
        }
        calculateSummaries(arrayList);
        if (!arrayList.isEmpty()) {
            GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) Pm7.F(arrayList);
            Boolean p0 = TimeUtils.p0(goalTrackingSummary.getDate());
            Wg6.b(p0, "DateHelper.isToday(todaySummary.date)");
            if (p0.booleanValue()) {
                arrayList.add(0, GoalTrackingSummary.copy$default(goalTrackingSummary, goalTrackingSummary.getDate(), goalTrackingSummary.getTotalTracked(), goalTrackingSummary.getGoalTarget(), 0, 0, 24, null));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final Rm6 loadData(PagingRequestHelper.Di di, Date date, Date date2, PagingRequestHelper.Bi.Aii aii) {
        return Gu7.d(Jv7.a(Bw7.b()), null, null, new GoalTrackingSummaryLocalDataSource$loadData$Anon1(this, date, date2, di, aii, null), 3, null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final PagingRequestHelper getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public boolean isInvalid() {
        this.mGoalTrackingDatabase.getInvalidationTracker().h();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadAfter(Af.Fi<Date> fi, Af.Ai<Date, GoalTrackingSummary> ai) {
        Wg6.c(fi, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ai, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Object) fi.a));
        if (TimeUtils.j0(fi.a, this.mCreatedDate)) {
            Key key2 = fi.a;
            Wg6.b(key2, "params.key");
            Key key3 = key2;
            Companion companion = Companion;
            Key key4 = fi.a;
            Wg6.b(key4, "params.key");
            Date calculateNextKey = companion.calculateNextKey(key4, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date O = TimeUtils.m0(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : TimeUtils.O(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + O + ", endQueryDate=" + ((Object) key3));
            Wg6.b(O, "startQueryDate");
            ai.a(getDataInDatabase(O, key3), calculateNextKey);
            if (TimeUtils.j0(this.mStartDate, key3)) {
                this.mEndDate = key3;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d(TAG, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new Lc6<>(this.mStartDate, this.mEndDate));
                this.mHelper.h(PagingRequestHelper.Di.AFTER, new GoalTrackingSummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadBefore(Af.Fi<Date> fi, Af.Ai<Date, GoalTrackingSummary> ai) {
        Wg6.c(fi, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ai, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadInitial(Af.Ei<Date> ei, Af.Ci<Date, GoalTrackingSummary> ci) {
        Wg6.c(ei, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ci, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date O = TimeUtils.m0(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : TimeUtils.O(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + O + ", endQueryDate=" + date);
        Wg6.b(O, "startQueryDate");
        ci.a(getDataInDatabase(O, date), null, this.key.getTime());
        this.mHelper.h(PagingRequestHelper.Di.INITIAL, new GoalTrackingSummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.f(this.listener);
        this.mGoalTrackingDatabase.getInvalidationTracker().j(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        Wg6.c(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(PagingRequestHelper pagingRequestHelper) {
        Wg6.c(pagingRequestHelper, "<set-?>");
        this.mHelper = pagingRequestHelper;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        Wg6.c(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        Wg6.c(date, "<set-?>");
        this.mStartDate = date;
    }
}
