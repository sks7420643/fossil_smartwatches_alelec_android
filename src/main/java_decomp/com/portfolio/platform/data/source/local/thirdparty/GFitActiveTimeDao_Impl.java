package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Wz4;
import com.mapped.Gh;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitActiveTimeDao_Impl implements GFitActiveTimeDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Gh<GFitActiveTime> __deletionAdapterOfGFitActiveTime;
    @DexIgnore
    public /* final */ Wz4 __gFitActiveTimeConverter; // = new Wz4();
    @DexIgnore
    public /* final */ Hh<GFitActiveTime> __insertionAdapterOfGFitActiveTime;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<GFitActiveTime> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, GFitActiveTime gFitActiveTime) {
            mi.bindLong(1, (long) gFitActiveTime.getId());
            String b = GFitActiveTimeDao_Impl.this.__gFitActiveTimeConverter.b(gFitActiveTime.getActiveTimes());
            if (b == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, b);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, GFitActiveTime gFitActiveTime) {
            bind(mi, gFitActiveTime);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitActiveTime` (`id`,`activeTimes`) VALUES (nullif(?, 0),?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Gh<GFitActiveTime> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, GFitActiveTime gFitActiveTime) {
            mi.bindLong(1, (long) gFitActiveTime.getId());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Gh
        public /* bridge */ /* synthetic */ void bind(Mi mi, GFitActiveTime gFitActiveTime) {
            bind(mi, gFitActiveTime);
        }

        @DexIgnore
        @Override // com.mapped.Vh, com.mapped.Gh
        public String createQuery() {
            return "DELETE FROM `gFitActiveTime` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM gFitActiveTime";
        }
    }

    @DexIgnore
    public GFitActiveTimeDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfGFitActiveTime = new Anon1(oh);
        this.__deletionAdapterOfGFitActiveTime = new Anon2(oh);
        this.__preparedStmtOfClearAll = new Anon3(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitActiveTimeDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitActiveTimeDao
    public void deleteGFitActiveTime(GFitActiveTime gFitActiveTime) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitActiveTime.handle(gFitActiveTime);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitActiveTimeDao
    public List<GFitActiveTime> getAllGFitActiveTime() {
        Rh f = Rh.f("SELECT * FROM gFitActiveTime", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "activeTimes");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GFitActiveTime gFitActiveTime = new GFitActiveTime(this.__gFitActiveTimeConverter.a(b.getString(c2)));
                gFitActiveTime.setId(b.getInt(c));
                arrayList.add(gFitActiveTime);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitActiveTimeDao
    public void insertGFitActiveTime(GFitActiveTime gFitActiveTime) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitActiveTime.insert((Hh<GFitActiveTime>) gFitActiveTime);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitActiveTimeDao
    public void insertListGFitActiveTime(List<GFitActiveTime> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitActiveTime.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
