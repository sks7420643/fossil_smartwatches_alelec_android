package com.portfolio.platform.data.source;

import com.fossil.Bw7;
import com.fossil.Eu7;
import com.fossil.Kz4;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaAppSettingRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "DianaAppSettingRepository";
    @DexIgnore
    public /* final */ ApiServiceV2 api;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public DianaAppSettingRepository(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        this.api = apiServiceV2;
    }

    @DexIgnore
    public final Object cleanUp(Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new DianaAppSettingRepository$cleanUp$Anon2(null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e6  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object fetchDianaAppSetting(com.mapped.Xe6<? super com.mapped.Cd6> r9) {
        /*
        // Method dump skipped, instructions count: 278
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaAppSettingRepository.fetchDianaAppSetting(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object getAllDianaAppSettings(String str, Xe6<? super List<DianaAppSetting>> xe6) {
        return Eu7.g(Bw7.b(), new DianaAppSettingRepository$getAllDianaAppSettings$Anon2(str, null), xe6);
    }

    @DexIgnore
    public final Object getDianaAppSetting(String str, String str2, Xe6<? super DianaAppSetting> xe6) {
        return Eu7.g(Bw7.b(), new DianaAppSettingRepository$getDianaAppSetting$Anon2(str, str2, null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getDianaAppSettingsAsLiveData(java.lang.String r6, com.mapped.Xe6<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.diana.DianaAppSetting>>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.DianaAppSettingRepository$getDianaAppSettingsAsLiveData$Anon1
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.data.source.DianaAppSettingRepository$getDianaAppSettingsAsLiveData$Anon1 r0 = (com.portfolio.platform.data.source.DianaAppSettingRepository$getDianaAppSettingsAsLiveData$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.DianaAppSettingRepository r1 = (com.portfolio.platform.data.source.DianaAppSettingRepository) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaAppSettingDao r0 = r0.getDianaAppSettingDao()
            androidx.lifecycle.LiveData r0 = r0.getAllDianaAppSettingAsLiveData(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.data.source.DianaAppSettingRepository$getDianaAppSettingsAsLiveData$Anon1 r0 = new com.portfolio.platform.data.source.DianaAppSettingRepository$getDianaAppSettingsAsLiveData$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.El7.b(r2)
            com.portfolio.platform.manager.EncryptedDatabaseManager r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaAppSettingRepository.getDianaAppSettingsAsLiveData(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object pushPendingData(com.mapped.Xe6<? super java.lang.Boolean> r9) {
        /*
            r8 = this;
            r7 = 2
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = 1
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.DianaAppSettingRepository$pushPendingData$Anon1
            if (r0 == 0) goto L_0x0033
            r0 = r9
            com.portfolio.platform.data.source.DianaAppSettingRepository$pushPendingData$Anon1 r0 = (com.portfolio.platform.data.source.DianaAppSettingRepository$pushPendingData$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0015:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0087
            if (r0 == r6) goto L_0x0042
            if (r0 != r7) goto L_0x003a
            java.lang.Object r0 = r2.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r2.L$0
            com.portfolio.platform.data.source.DianaAppSettingRepository r0 = (com.portfolio.platform.data.source.DianaAppSettingRepository) r0
            com.fossil.El7.b(r1)
        L_0x002e:
            java.lang.Boolean r0 = com.fossil.Ao7.a(r6)
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.data.source.DianaAppSettingRepository$pushPendingData$Anon1 r0 = new com.portfolio.platform.data.source.DianaAppSettingRepository$pushPendingData$Anon1
            r0.<init>(r8, r9)
            r2 = r0
            goto L_0x0015
        L_0x003a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0042:
            java.lang.Object r0 = r2.L$0
            com.portfolio.platform.data.source.DianaAppSettingRepository r0 = (com.portfolio.platform.data.source.DianaAppSettingRepository) r0
            com.fossil.El7.b(r1)
            r8 = r0
        L_0x004a:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaAppSettingDao r0 = r0.getDianaAppSettingDao()
            java.util.List r0 = r0.getPendingDianaAppSetting()
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "pushPendingData dianaAppSetting pendingData "
            r4.append(r5)
            r4.append(r0)
            java.lang.String r5 = "DianaAppSettingRepository"
            java.lang.String r4 = r4.toString()
            r1.d(r5, r4)
            boolean r1 = r0.isEmpty()
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x002e
            r2.L$0 = r8
            r2.L$1 = r0
            r2.label = r7
            java.lang.Object r0 = r8.upsertDianaAppSetting(r0, r2)
            if (r0 != r3) goto L_0x002e
            r0 = r3
            goto L_0x0032
        L_0x0087:
            com.fossil.El7.b(r1)
            com.portfolio.platform.manager.EncryptedDatabaseManager r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r2.L$0 = r8
            r2.label = r6
            java.lang.Object r1 = r0.v(r2)
            if (r1 != r3) goto L_0x004a
            r0 = r3
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaAppSettingRepository.pushPendingData(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object upsertDianaAppSetting(List<DianaAppSetting> list, Xe6<? super Kz4<List<DianaAppSetting>>> xe6) {
        return Eu7.g(Bw7.b(), new DianaAppSettingRepository$upsertDianaAppSetting$Anon2(this, list, null), xe6);
    }
}
