package com.portfolio.platform.data.source.remote;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$response$1", f = "MicroAppRemoteDataSource.kt", l = {40}, m = "invokeSuspend")
public final class MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1 extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<MicroAppVariant>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $majorNumber;
    @DexIgnore
    public /* final */ /* synthetic */ String $minorNumber;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1(MicroAppRemoteDataSource microAppRemoteDataSource, String str, String str2, String str3, Xe6 xe6) {
        super(1, xe6);
        this.this$0 = microAppRemoteDataSource;
        this.$serial = str;
        this.$majorNumber = str2;
        this.$minorNumber = str3;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        return new MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1(this.this$0, this.$serial, this.$majorNumber, this.$minorNumber, xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public final Object invoke(Xe6<? super Q88<ApiResponse<MicroAppVariant>>> xe6) {
        throw null;
        //return ((MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1) create(xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiServiceV2;
            String str = this.$serial;
            String str2 = this.$majorNumber;
            String str3 = this.$minorNumber;
            this.label = 1;
            Object allMicroAppVariant = apiServiceV2.getAllMicroAppVariant(str, str2, str3, this);
            return allMicroAppVariant == d ? d : allMicroAppVariant;
        } else if (i == 1) {
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
