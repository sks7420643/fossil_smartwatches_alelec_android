package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Ko7;
import com.fossil.Qq7;
import com.fossil.Ss0;
import com.fossil.Yn7;
import com.fossil.Zt0;
import com.mapped.Cd6;
import com.mapped.Cf;
import com.mapped.Coroutine;
import com.mapped.Gg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.PagingRequestHelper;
import com.mapped.U04;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataPaging$2", f = "GoalTrackingRepository.kt", l = {319}, m = "invokeSuspend")
public final class GoalTrackingRepository$getGoalTrackingDataPaging$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Listing<GoalTrackingData>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ U04 $appExecutors;
    @DexIgnore
    public /* final */ /* synthetic */ Date $currentDate;
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.Ai $listener;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public static /* final */ Anon1_Level2 INSTANCE; // = new Anon1_Level2();

        @DexIgnore
        public final LiveData<NetworkState> apply(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
            return goalTrackingDataLocalDataSource.getMNetworkState();
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return apply((GoalTrackingDataLocalDataSource) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2_Level2 extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(GoalTrackingDataSourceFactory goalTrackingDataSourceFactory) {
            super(0);
            this.$sourceFactory = goalTrackingDataSourceFactory;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            GoalTrackingDataLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
            if (e != null) {
                e.invalidate();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3_Level2 extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(GoalTrackingDataSourceFactory goalTrackingDataSourceFactory) {
            super(0);
            this.$sourceFactory = goalTrackingDataSourceFactory;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            PagingRequestHelper mHelper;
            GoalTrackingDataLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
            if (e != null && (mHelper = e.getMHelper()) != null) {
                mHelper.g();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getGoalTrackingDataPaging$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, U04 u04, PagingRequestHelper.Ai ai, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = goalTrackingRepository;
        this.$currentDate = date;
        this.$appExecutors = u04;
        this.$listener = ai;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        GoalTrackingRepository$getGoalTrackingDataPaging$Anon2 goalTrackingRepository$getGoalTrackingDataPaging$Anon2 = new GoalTrackingRepository$getGoalTrackingDataPaging$Anon2(this.this$0, this.$currentDate, this.$appExecutors, this.$listener, xe6);
        goalTrackingRepository$getGoalTrackingDataPaging$Anon2.p$ = (Il6) obj;
        throw null;
        //return goalTrackingRepository$getGoalTrackingDataPaging$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Listing<GoalTrackingData>> xe6) {
        throw null;
        //return ((GoalTrackingRepository$getGoalTrackingDataPaging$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object g;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "getGoalTrackingDataPaging");
            Dv7 b = Bw7.b();
            GoalTrackingRepository$getGoalTrackingDataPaging$Anon2$goalTrackingDatabase$Anon1_Level2 goalTrackingRepository$getGoalTrackingDataPaging$Anon2$goalTrackingDatabase$Anon1_Level2 = new GoalTrackingRepository$getGoalTrackingDataPaging$Anon2$goalTrackingDatabase$Anon1_Level2(null);
            this.L$0 = il6;
            this.label = 1;
            g = Eu7.g(b, goalTrackingRepository$getGoalTrackingDataPaging$Anon2$goalTrackingDatabase$Anon1_Level2, this);
            if (g == d) {
                return d;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            g = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        GoalTrackingDataSourceFactory goalTrackingDataSourceFactory = new GoalTrackingDataSourceFactory(this.this$0, (GoalTrackingDatabase) g, this.$currentDate, this.$appExecutors, this.$listener);
        this.this$0.mSourceDataFactoryList.add(goalTrackingDataSourceFactory);
        Cf.Fi.Aii aii = new Cf.Fi.Aii();
        aii.c(100);
        aii.b(false);
        aii.d(100);
        aii.e(5);
        Cf.Fi a2 = aii.a();
        Wg6.b(a2, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a3 = new Zt0(goalTrackingDataSourceFactory, a2).a();
        Wg6.b(a3, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData c = Ss0.c(goalTrackingDataSourceFactory.getSourceLiveData(), Anon1_Level2.INSTANCE);
        Wg6.b(c, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing(a3, c, new Anon2_Level2(goalTrackingDataSourceFactory), new Anon3_Level2(goalTrackingDataSourceFactory));
    }
}
