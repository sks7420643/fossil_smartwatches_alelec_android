package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.ThirdPartyRepository$saveData$2", f = "ThirdPartyRepository.kt", l = {49}, m = "invokeSuspend")
public final class ThirdPartyRepository$saveData$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GFitActiveTime $gFitActiveTime;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitHeartRate;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitSample;
    @DexIgnore
    public /* final */ /* synthetic */ List $listGFitWorkoutSession;
    @DexIgnore
    public /* final */ /* synthetic */ List $listMFSleepSession;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyDatabase $thirdPartyDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$saveData$Anon2 this$0;

        @DexIgnore
        public Anon1_Level2(ThirdPartyRepository$saveData$Anon2 thirdPartyRepository$saveData$Anon2, ThirdPartyDatabase thirdPartyDatabase) {
            this.this$0 = thirdPartyRepository$saveData$Anon2;
            this.$thirdPartyDatabase = thirdPartyDatabase;
        }

        @DexIgnore
        public final void run() {
            if (!this.this$0.$listGFitSample.isEmpty()) {
                this.$thirdPartyDatabase.getGFitSampleDao().insertListGFitSample(this.this$0.$listGFitSample);
            }
            if (this.this$0.$gFitActiveTime != null) {
                this.$thirdPartyDatabase.getGFitActiveTimeDao().insertGFitActiveTime(this.this$0.$gFitActiveTime);
            }
            if (!this.this$0.$listGFitHeartRate.isEmpty()) {
                this.$thirdPartyDatabase.getGFitHeartRateDao().insertListGFitHeartRate(this.this$0.$listGFitHeartRate);
            }
            if (!this.this$0.$listGFitWorkoutSession.isEmpty()) {
                this.$thirdPartyDatabase.getGFitWorkoutSessionDao().insertListGFitWorkoutSession(this.this$0.$listGFitWorkoutSession);
            }
            if (!this.this$0.$listMFSleepSession.isEmpty()) {
                ThirdPartyRepository$saveData$Anon2 thirdPartyRepository$saveData$Anon2 = this.this$0;
                this.$thirdPartyDatabase.getGFitSleepDao().insertListGFitSleep(thirdPartyRepository$saveData$Anon2.this$0.convertListMFSleepSessionToListGFitSleep(thirdPartyRepository$saveData$Anon2.$listMFSleepSession));
                FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "listMFSleepSession.isNotEmpty");
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThirdPartyRepository$saveData$Anon2(ThirdPartyRepository thirdPartyRepository, List list, GFitActiveTime gFitActiveTime, List list2, List list3, List list4, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = thirdPartyRepository;
        this.$listGFitSample = list;
        this.$gFitActiveTime = gFitActiveTime;
        this.$listGFitHeartRate = list2;
        this.$listGFitWorkoutSession = list3;
        this.$listMFSleepSession = list4;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        ThirdPartyRepository$saveData$Anon2 thirdPartyRepository$saveData$Anon2 = new ThirdPartyRepository$saveData$Anon2(this.this$0, this.$listGFitSample, this.$gFitActiveTime, this.$listGFitHeartRate, this.$listGFitWorkoutSession, this.$listMFSleepSession, xe6);
        thirdPartyRepository$saveData$Anon2.p$ = (Il6) obj;
        throw null;
        //return thirdPartyRepository$saveData$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((ThirdPartyRepository$saveData$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object F;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            EncryptedDatabaseManager encryptedDatabaseManager = EncryptedDatabaseManager.j;
            this.L$0 = il6;
            this.label = 1;
            F = encryptedDatabaseManager.F(this);
            if (F == d) {
                return d;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            F = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ThirdPartyDatabase thirdPartyDatabase = (ThirdPartyDatabase) F;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(ThirdPartyRepository.TAG, "saveData = listGFitSample=" + this.$listGFitSample + ", gFitActiveTime=" + this.$gFitActiveTime + ", listGFitHeartRate=" + this.$listGFitHeartRate + ", listGFitWorkoutSession=" + this.$listGFitWorkoutSession + ", listMFSleepSession= " + this.$listMFSleepSession);
        thirdPartyDatabase.runInTransaction(new Anon1_Level2(this, thirdPartyDatabase));
        return Cd6.a;
    }
}
