package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import com.mapped.An4;
import com.portfolio.platform.helper.FitnessHelper;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideFitnessHelperFactory implements Factory<FitnessHelper> {
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;
    @DexIgnore
    public /* final */ Provider<An4> sharedPreferencesManagerProvider;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideFitnessHelperFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<An4> provider) {
        this.module = portfolioDatabaseModule;
        this.sharedPreferencesManagerProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideFitnessHelperFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<An4> provider) {
        return new PortfolioDatabaseModule_ProvideFitnessHelperFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static FitnessHelper provideFitnessHelper(PortfolioDatabaseModule portfolioDatabaseModule, An4 an4) {
        FitnessHelper provideFitnessHelper = portfolioDatabaseModule.provideFitnessHelper(an4);
        Lk7.c(provideFitnessHelper, "Cannot return null from a non-@Nullable @Provides method");
        return provideFitnessHelper;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public FitnessHelper get() {
        return provideFitnessHelper(this.module, this.sharedPreferencesManagerProvider.get());
    }
}
