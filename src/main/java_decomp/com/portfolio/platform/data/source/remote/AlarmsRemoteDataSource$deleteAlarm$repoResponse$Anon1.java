package com.portfolio.platform.data.source.remote;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$repoResponse$1", f = "AlarmsRemoteDataSource.kt", l = {66}, m = "invokeSuspend")
public final class AlarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1 extends Ko7 implements Hg6<Xe6<? super Q88<Void>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $alarmId;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1(AlarmsRemoteDataSource alarmsRemoteDataSource, String str, Xe6 xe6) {
        super(1, xe6);
        this.this$0 = alarmsRemoteDataSource;
        this.$alarmId = str;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        return new AlarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1(this.this$0, this.$alarmId, xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public final Object invoke(Xe6<? super Q88<Void>> xe6) {
        throw null;
        //return ((AlarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1) create(xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiServiceV2;
            String str = this.$alarmId;
            this.label = 1;
            Object deleteAlarm = apiServiceV2.deleteAlarm(str, this);
            return deleteAlarm == d ? d : deleteAlarm;
        } else if (i == 1) {
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
