package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetRepository_Factory implements Factory<DianaPresetRepository> {
    @DexIgnore
    public /* final */ Provider<DianaPresetRemoteDataSource> mDianaPresetRemoteDataSourceProvider;

    @DexIgnore
    public DianaPresetRepository_Factory(Provider<DianaPresetRemoteDataSource> provider) {
        this.mDianaPresetRemoteDataSourceProvider = provider;
    }

    @DexIgnore
    public static DianaPresetRepository_Factory create(Provider<DianaPresetRemoteDataSource> provider) {
        return new DianaPresetRepository_Factory(provider);
    }

    @DexIgnore
    public static DianaPresetRepository newInstance(DianaPresetRemoteDataSource dianaPresetRemoteDataSource) {
        return new DianaPresetRepository(dianaPresetRemoteDataSource);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public DianaPresetRepository get() {
        return newInstance(this.mDianaPresetRemoteDataSourceProvider.get());
    }
}
