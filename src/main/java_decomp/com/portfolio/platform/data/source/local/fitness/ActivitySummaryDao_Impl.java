package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.C05;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Mz4;
import com.fossil.Nw0;
import com.fossil.Rz4;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryDao_Impl extends ActivitySummaryDao {
    @DexIgnore
    public /* final */ Mz4 __activityStatisticConverter; // = new Mz4();
    @DexIgnore
    public /* final */ Rz4 __dateTimeConverter; // = new Rz4();
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<ActivityRecommendedGoals> __insertionAdapterOfActivityRecommendedGoals;
    @DexIgnore
    public /* final */ Hh<ActivitySettings> __insertionAdapterOfActivitySettings;
    @DexIgnore
    public /* final */ Hh<ActivityStatistic> __insertionAdapterOfActivityStatistic;
    @DexIgnore
    public /* final */ Hh<ActivitySummary> __insertionAdapterOfActivitySummary;
    @DexIgnore
    public /* final */ C05 __integerArrayConverter; // = new C05();
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteAllActivitySummaries;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfUpdateActivitySettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<ActivitySummary> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, ActivitySummary activitySummary) {
            mi.bindLong(1, ActivitySummaryDao_Impl.this.__dateTimeConverter.b(activitySummary.getCreatedAt()));
            mi.bindLong(2, ActivitySummaryDao_Impl.this.__dateTimeConverter.b(activitySummary.getUpdatedAt()));
            mi.bindLong(3, (long) activitySummary.getPinType());
            mi.bindLong(4, (long) activitySummary.getYear());
            mi.bindLong(5, (long) activitySummary.getMonth());
            mi.bindLong(6, (long) activitySummary.getDay());
            if (activitySummary.getTimezoneName() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, activitySummary.getTimezoneName());
            }
            if (activitySummary.getDstOffset() == null) {
                mi.bindNull(8);
            } else {
                mi.bindLong(8, (long) activitySummary.getDstOffset().intValue());
            }
            mi.bindDouble(9, activitySummary.getSteps());
            mi.bindDouble(10, activitySummary.getCalories());
            mi.bindDouble(11, activitySummary.getDistance());
            String b = ActivitySummaryDao_Impl.this.__integerArrayConverter.b(activitySummary.getIntensities());
            if (b == null) {
                mi.bindNull(12);
            } else {
                mi.bindString(12, b);
            }
            mi.bindLong(13, (long) activitySummary.getActiveTime());
            mi.bindLong(14, (long) activitySummary.getStepGoal());
            mi.bindLong(15, (long) activitySummary.getCaloriesGoal());
            mi.bindLong(16, (long) activitySummary.getActiveTimeGoal());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, ActivitySummary activitySummary) {
            bind(mi, activitySummary);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sampleday` (`createdAt`,`updatedAt`,`pinType`,`year`,`month`,`day`,`timezoneName`,`dstOffset`,`steps`,`calories`,`distance`,`intensities`,`activeTime`,`stepGoal`,`caloriesGoal`,`activeTimeGoal`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<ActivityStatistic> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon10(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public ActivityStatistic call() throws Exception {
            ActivityStatistic activityStatistic;
            Cursor b = Ex0.b(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "id");
                int c2 = Dx0.c(b, "uid");
                int c3 = Dx0.c(b, "activeTimeBestDay");
                int c4 = Dx0.c(b, "activeTimeBestStreak");
                int c5 = Dx0.c(b, "caloriesBestDay");
                int c6 = Dx0.c(b, "caloriesBestStreak");
                int c7 = Dx0.c(b, "stepsBestDay");
                int c8 = Dx0.c(b, "stepsBestStreak");
                int c9 = Dx0.c(b, "totalActiveTime");
                int c10 = Dx0.c(b, "totalCalories");
                int c11 = Dx0.c(b, "totalDays");
                int c12 = Dx0.c(b, "totalDistance");
                int c13 = Dx0.c(b, "totalSteps");
                int c14 = Dx0.c(b, "totalIntensityDistInStep");
                int c15 = Dx0.c(b, "createdAt");
                int c16 = Dx0.c(b, "updatedAt");
                if (b.moveToFirst()) {
                    activityStatistic = new ActivityStatistic(b.getString(c), b.getString(c2), ActivitySummaryDao_Impl.this.__activityStatisticConverter.c(b.getString(c3)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.c(b.getString(c4)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.d(b.getString(c5)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.c(b.getString(c6)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.c(b.getString(c7)), ActivitySummaryDao_Impl.this.__activityStatisticConverter.c(b.getString(c8)), b.getInt(c9), b.getDouble(c10), b.getInt(c11), b.getDouble(c12), b.getInt(c13), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(b.getString(c14)), ActivitySummaryDao_Impl.this.__dateTimeConverter.a(b.getLong(c15)), ActivitySummaryDao_Impl.this.__dateTimeConverter.a(b.getLong(c16)));
                } else {
                    activityStatistic = null;
                }
                return activityStatistic;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Hh<ActivitySettings> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, ActivitySettings activitySettings) {
            mi.bindLong(1, (long) activitySettings.getId());
            mi.bindLong(2, (long) activitySettings.getCurrentStepGoal());
            mi.bindLong(3, (long) activitySettings.getCurrentCaloriesGoal());
            mi.bindLong(4, (long) activitySettings.getCurrentActiveTimeGoal());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, ActivitySettings activitySettings) {
            bind(mi, activitySettings);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activitySettings` (`id`,`currentStepGoal`,`currentCaloriesGoal`,`currentActiveTimeGoal`) VALUES (?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Hh<ActivityRecommendedGoals> {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, ActivityRecommendedGoals activityRecommendedGoals) {
            mi.bindLong(1, (long) activityRecommendedGoals.getId());
            mi.bindLong(2, (long) activityRecommendedGoals.getRecommendedStepsGoal());
            mi.bindLong(3, (long) activityRecommendedGoals.getRecommendedCaloriesGoal());
            mi.bindLong(4, (long) activityRecommendedGoals.getRecommendedActiveTimeGoal());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, ActivityRecommendedGoals activityRecommendedGoals) {
            bind(mi, activityRecommendedGoals);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activityRecommendedGoals` (`id`,`recommendedStepsGoal`,`recommendedCaloriesGoal`,`recommendedActiveTimeGoal`) VALUES (?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends Hh<ActivityStatistic> {
        @DexIgnore
        public Anon4(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, ActivityStatistic activityStatistic) {
            if (activityStatistic.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, activityStatistic.getId());
            }
            if (activityStatistic.getUid() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, activityStatistic.getUid());
            }
            String a2 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getActiveTimeBestDay());
            if (a2 == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, a2);
            }
            String a3 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getActiveTimeBestStreak());
            if (a3 == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, a3);
            }
            String b = ActivitySummaryDao_Impl.this.__activityStatisticConverter.b(activityStatistic.getCaloriesBestDay());
            if (b == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, b);
            }
            String a4 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getCaloriesBestStreak());
            if (a4 == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, a4);
            }
            String a5 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getStepsBestDay());
            if (a5 == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, a5);
            }
            String a6 = ActivitySummaryDao_Impl.this.__activityStatisticConverter.a(activityStatistic.getStepsBestStreak());
            if (a6 == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, a6);
            }
            mi.bindLong(9, (long) activityStatistic.getTotalActiveTime());
            mi.bindDouble(10, activityStatistic.getTotalCalories());
            mi.bindLong(11, (long) activityStatistic.getTotalDays());
            mi.bindDouble(12, activityStatistic.getTotalDistance());
            mi.bindLong(13, (long) activityStatistic.getTotalSteps());
            String b2 = ActivitySummaryDao_Impl.this.__integerArrayConverter.b(activityStatistic.getTotalIntensityDistInStep());
            if (b2 == null) {
                mi.bindNull(14);
            } else {
                mi.bindString(14, b2);
            }
            mi.bindLong(15, ActivitySummaryDao_Impl.this.__dateTimeConverter.b(activityStatistic.getCreatedAt()));
            mi.bindLong(16, ActivitySummaryDao_Impl.this.__dateTimeConverter.b(activityStatistic.getUpdatedAt()));
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, ActivityStatistic activityStatistic) {
            bind(mi, activityStatistic);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `activity_statistic` (`id`,`uid`,`activeTimeBestDay`,`activeTimeBestStreak`,`caloriesBestDay`,`caloriesBestStreak`,`stepsBestDay`,`stepsBestStreak`,`totalActiveTime`,`totalCalories`,`totalDays`,`totalDistance`,`totalSteps`,`totalIntensityDistInStep`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends Vh {
        @DexIgnore
        public Anon5(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM sampleday";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends Vh {
        @DexIgnore
        public Anon6(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE activitySettings SET currentStepGoal = ?, currentCaloriesGoal = ?, currentActiveTimeGoal = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements Callable<ActivitySummary> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon7(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public ActivitySummary call() throws Exception {
            ActivitySummary activitySummary;
            Cursor b = Ex0.b(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "createdAt");
                int c2 = Dx0.c(b, "updatedAt");
                int c3 = Dx0.c(b, "pinType");
                int c4 = Dx0.c(b, "year");
                int c5 = Dx0.c(b, "month");
                int c6 = Dx0.c(b, "day");
                int c7 = Dx0.c(b, SampleDay.COLUMN_TIMEZONE_NAME);
                int c8 = Dx0.c(b, SampleDay.COLUMN_DST_OFFSET);
                int c9 = Dx0.c(b, "steps");
                int c10 = Dx0.c(b, "calories");
                int c11 = Dx0.c(b, "distance");
                int c12 = Dx0.c(b, SampleDay.COLUMN_INTENSITIES);
                int c13 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
                int c14 = Dx0.c(b, SampleDay.COLUMN_STEP_GOAL);
                int c15 = Dx0.c(b, SampleDay.COLUMN_CALORIES_GOAL);
                int c16 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                if (b.moveToFirst()) {
                    activitySummary = new ActivitySummary(b.getInt(c4), b.getInt(c5), b.getInt(c6), b.getString(c7), b.isNull(c8) ? null : Integer.valueOf(b.getInt(c8)), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(b.getString(c12)), b.getInt(c13), b.getInt(c14), b.getInt(c15), b.getInt(c16));
                    activitySummary.setCreatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(b.getLong(c)));
                    activitySummary.setUpdatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(b.getLong(c2)));
                    activitySummary.setPinType(b.getInt(c3));
                } else {
                    activitySummary = null;
                }
                return activitySummary;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<List<ActivitySummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon8(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<ActivitySummary> call() throws Exception {
            Cursor b = Ex0.b(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "createdAt");
                int c2 = Dx0.c(b, "updatedAt");
                int c3 = Dx0.c(b, "pinType");
                int c4 = Dx0.c(b, "year");
                int c5 = Dx0.c(b, "month");
                int c6 = Dx0.c(b, "day");
                int c7 = Dx0.c(b, SampleDay.COLUMN_TIMEZONE_NAME);
                int c8 = Dx0.c(b, SampleDay.COLUMN_DST_OFFSET);
                int c9 = Dx0.c(b, "steps");
                int c10 = Dx0.c(b, "calories");
                int c11 = Dx0.c(b, "distance");
                int c12 = Dx0.c(b, SampleDay.COLUMN_INTENSITIES);
                int c13 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
                int c14 = Dx0.c(b, SampleDay.COLUMN_STEP_GOAL);
                int c15 = Dx0.c(b, SampleDay.COLUMN_CALORIES_GOAL);
                int c16 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    ActivitySummary activitySummary = new ActivitySummary(b.getInt(c4), b.getInt(c5), b.getInt(c6), b.getString(c7), b.isNull(c8) ? null : Integer.valueOf(b.getInt(c8)), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), ActivitySummaryDao_Impl.this.__integerArrayConverter.a(b.getString(c12)), b.getInt(c13), b.getInt(c14), b.getInt(c15), b.getInt(c16));
                    activitySummary.setCreatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(b.getLong(c)));
                    activitySummary.setUpdatedAt(ActivitySummaryDao_Impl.this.__dateTimeConverter.a(b.getLong(c2)));
                    activitySummary.setPinType(b.getInt(c3));
                    arrayList.add(activitySummary);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 implements Callable<ActivitySettings> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon9(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public ActivitySettings call() throws Exception {
            ActivitySettings activitySettings = null;
            Cursor b = Ex0.b(ActivitySummaryDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "id");
                int c2 = Dx0.c(b, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_STEP_GOAL);
                int c3 = Dx0.c(b, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_CALORIES_GOAL);
                int c4 = Dx0.c(b, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_ACTIVE_TIME_GOAL);
                if (b.moveToFirst()) {
                    activitySettings = new ActivitySettings(b.getInt(c2), b.getInt(c3), b.getInt(c4));
                    activitySettings.setId(b.getInt(c));
                }
                return activitySettings;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public ActivitySummaryDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfActivitySummary = new Anon1(oh);
        this.__insertionAdapterOfActivitySettings = new Anon2(oh);
        this.__insertionAdapterOfActivityRecommendedGoals = new Anon3(oh);
        this.__insertionAdapterOfActivityStatistic = new Anon4(oh);
        this.__preparedStmtOfDeleteAllActivitySummaries = new Anon5(oh);
        this.__preparedStmtOfUpdateActivitySettings = new Anon6(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void deleteAllActivitySummaries() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteAllActivitySummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySummaries.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySettings getActivitySetting() {
        ActivitySettings activitySettings = null;
        Rh f = Rh.f("SELECT * FROM activitySettings LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_STEP_GOAL);
            int c3 = Dx0.c(b, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_CALORIES_GOAL);
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.fitness.ActivitySettings.COLUMN_CURRENT_ACTIVE_TIME_GOAL);
            if (b.moveToFirst()) {
                activitySettings = new ActivitySettings(b.getInt(c2), b.getInt(c3), b.getInt(c4));
                activitySettings.setId(b.getInt(c));
            }
            return activitySettings;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public LiveData<ActivitySettings> getActivitySettingLiveData() {
        Rh f = Rh.f("SELECT * FROM activitySettings LIMIT 1", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon9 anon9 = new Anon9(f);
        return invalidationTracker.d(new String[]{com.fossil.wearables.fsl.fitness.ActivitySettings.TABLE_NAME}, false, anon9);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivityStatistic getActivityStatistic() {
        Throwable th;
        ActivityStatistic activityStatistic;
        Rh f = Rh.f("SELECT * FROM activity_statistic LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "uid");
            int c3 = Dx0.c(b, "activeTimeBestDay");
            int c4 = Dx0.c(b, "activeTimeBestStreak");
            int c5 = Dx0.c(b, "caloriesBestDay");
            int c6 = Dx0.c(b, "caloriesBestStreak");
            int c7 = Dx0.c(b, "stepsBestDay");
            int c8 = Dx0.c(b, "stepsBestStreak");
            int c9 = Dx0.c(b, "totalActiveTime");
            int c10 = Dx0.c(b, "totalCalories");
            int c11 = Dx0.c(b, "totalDays");
            int c12 = Dx0.c(b, "totalDistance");
            int c13 = Dx0.c(b, "totalSteps");
            try {
                int c14 = Dx0.c(b, "totalIntensityDistInStep");
                int c15 = Dx0.c(b, "createdAt");
                int c16 = Dx0.c(b, "updatedAt");
                if (b.moveToFirst()) {
                    activityStatistic = new ActivityStatistic(b.getString(c), b.getString(c2), this.__activityStatisticConverter.c(b.getString(c3)), this.__activityStatisticConverter.c(b.getString(c4)), this.__activityStatisticConverter.d(b.getString(c5)), this.__activityStatisticConverter.c(b.getString(c6)), this.__activityStatisticConverter.c(b.getString(c7)), this.__activityStatisticConverter.c(b.getString(c8)), b.getInt(c9), b.getDouble(c10), b.getInt(c11), b.getDouble(c12), b.getInt(c13), this.__integerArrayConverter.a(b.getString(c14)), this.__dateTimeConverter.a(b.getLong(c15)), this.__dateTimeConverter.a(b.getLong(c16)));
                } else {
                    activityStatistic = null;
                }
                b.close();
                f.m();
                return activityStatistic;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public LiveData<ActivityStatistic> getActivityStatisticLiveData() {
        Rh f = Rh.f("SELECT * FROM activity_statistic LIMIT 1", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon10 anon10 = new Anon10(f);
        return invalidationTracker.d(new String[]{ActivityStatistic.TABLE_NAME}, false, anon10);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public List<ActivitySummary> getActivitySummariesDesc(int i, int i2, int i3, int i4, int i5, int i6) {
        Rh f = Rh.f("SELECT * FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ORDER BY year DESC, month DESC, day DESC\n    ", 12);
        long j = (long) i3;
        f.bindLong(1, j);
        f.bindLong(2, j);
        long j2 = (long) i2;
        f.bindLong(3, j2);
        f.bindLong(4, j);
        f.bindLong(5, j2);
        f.bindLong(6, (long) i);
        long j3 = (long) i6;
        f.bindLong(7, j3);
        f.bindLong(8, j3);
        long j4 = (long) i5;
        f.bindLong(9, j4);
        f.bindLong(10, j3);
        f.bindLong(11, j4);
        f.bindLong(12, (long) i4);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "year");
            int c5 = Dx0.c(b, "month");
            int c6 = Dx0.c(b, "day");
            int c7 = Dx0.c(b, SampleDay.COLUMN_TIMEZONE_NAME);
            int c8 = Dx0.c(b, SampleDay.COLUMN_DST_OFFSET);
            int c9 = Dx0.c(b, "steps");
            int c10 = Dx0.c(b, "calories");
            int c11 = Dx0.c(b, "distance");
            int c12 = Dx0.c(b, SampleDay.COLUMN_INTENSITIES);
            int c13 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            try {
                int c14 = Dx0.c(b, SampleDay.COLUMN_STEP_GOAL);
                int c15 = Dx0.c(b, SampleDay.COLUMN_CALORIES_GOAL);
                int c16 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    ActivitySummary activitySummary = new ActivitySummary(b.getInt(c4), b.getInt(c5), b.getInt(c6), b.getString(c7), b.isNull(c8) ? null : Integer.valueOf(b.getInt(c8)), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), this.__integerArrayConverter.a(b.getString(c12)), b.getInt(c13), b.getInt(c14), b.getInt(c15), b.getInt(c16));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(b.getLong(c)));
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(b.getLong(c2)));
                    activitySummary.setPinType(b.getInt(c3));
                    arrayList.add(activitySummary);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public LiveData<List<ActivitySummary>> getActivitySummariesLiveData(int i, int i2, int i3, int i4, int i5, int i6) {
        Rh f = Rh.f("SELECT * FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ORDER BY year ASC, month ASC, day ASC\n    ", 12);
        long j = (long) i3;
        f.bindLong(1, j);
        f.bindLong(2, j);
        long j2 = (long) i2;
        f.bindLong(3, j2);
        f.bindLong(4, j);
        f.bindLong(5, j2);
        f.bindLong(6, (long) i);
        long j3 = (long) i6;
        f.bindLong(7, j3);
        f.bindLong(8, j3);
        long j4 = (long) i5;
        f.bindLong(9, j4);
        f.bindLong(10, j3);
        f.bindLong(11, j4);
        f.bindLong(12, (long) i4);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon8 anon8 = new Anon8(f);
        return invalidationTracker.d(new String[]{SampleDay.TABLE_NAME}, false, anon8);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySummary getActivitySummary(int i, int i2, int i3) {
        Throwable th;
        ActivitySummary activitySummary;
        Rh f = Rh.f("SELECT * FROM sampleday WHERE year = ? AND month = ? AND day = ? LIMIT 1", 3);
        f.bindLong(1, (long) i);
        f.bindLong(2, (long) i2);
        f.bindLong(3, (long) i3);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "year");
            int c5 = Dx0.c(b, "month");
            int c6 = Dx0.c(b, "day");
            int c7 = Dx0.c(b, SampleDay.COLUMN_TIMEZONE_NAME);
            int c8 = Dx0.c(b, SampleDay.COLUMN_DST_OFFSET);
            int c9 = Dx0.c(b, "steps");
            int c10 = Dx0.c(b, "calories");
            int c11 = Dx0.c(b, "distance");
            int c12 = Dx0.c(b, SampleDay.COLUMN_INTENSITIES);
            int c13 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            try {
                int c14 = Dx0.c(b, SampleDay.COLUMN_STEP_GOAL);
                int c15 = Dx0.c(b, SampleDay.COLUMN_CALORIES_GOAL);
                int c16 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                if (b.moveToFirst()) {
                    activitySummary = new ActivitySummary(b.getInt(c4), b.getInt(c5), b.getInt(c6), b.getString(c7), b.isNull(c8) ? null : Integer.valueOf(b.getInt(c8)), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), this.__integerArrayConverter.a(b.getString(c12)), b.getInt(c13), b.getInt(c14), b.getInt(c15), b.getInt(c16));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(b.getLong(c)));
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(b.getLong(c2)));
                    activitySummary.setPinType(b.getInt(c3));
                } else {
                    activitySummary = null;
                }
                b.close();
                f.m();
                return activitySummary;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public LiveData<ActivitySummary> getActivitySummaryLiveData(int i, int i2, int i3) {
        Rh f = Rh.f("SELECT * FROM sampleday WHERE year = ? AND month = ? AND day = ?", 3);
        f.bindLong(1, (long) i);
        f.bindLong(2, (long) i2);
        f.bindLong(3, (long) i3);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon7 anon7 = new Anon7(f);
        return invalidationTracker.d(new String[]{SampleDay.TABLE_NAME}, false, anon7);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySummary getLastSummary() {
        Throwable th;
        ActivitySummary activitySummary;
        Rh f = Rh.f("SELECT * FROM sampleday ORDER BY year ASC, month ASC, day ASC LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "year");
            int c5 = Dx0.c(b, "month");
            int c6 = Dx0.c(b, "day");
            int c7 = Dx0.c(b, SampleDay.COLUMN_TIMEZONE_NAME);
            int c8 = Dx0.c(b, SampleDay.COLUMN_DST_OFFSET);
            int c9 = Dx0.c(b, "steps");
            int c10 = Dx0.c(b, "calories");
            int c11 = Dx0.c(b, "distance");
            int c12 = Dx0.c(b, SampleDay.COLUMN_INTENSITIES);
            int c13 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            try {
                int c14 = Dx0.c(b, SampleDay.COLUMN_STEP_GOAL);
                int c15 = Dx0.c(b, SampleDay.COLUMN_CALORIES_GOAL);
                int c16 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                if (b.moveToFirst()) {
                    activitySummary = new ActivitySummary(b.getInt(c4), b.getInt(c5), b.getInt(c6), b.getString(c7), b.isNull(c8) ? null : Integer.valueOf(b.getInt(c8)), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), this.__integerArrayConverter.a(b.getString(c12)), b.getInt(c13), b.getInt(c14), b.getInt(c15), b.getInt(c16));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(b.getLong(c)));
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(b.getLong(c2)));
                    activitySummary.setPinType(b.getInt(c3));
                } else {
                    activitySummary = null;
                }
                b.close();
                f.m();
                return activitySummary;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySummary getNearestSampleDayFromDate(int i, int i2, int i3) {
        Throwable th;
        ActivitySummary activitySummary;
        Rh f = Rh.f("SELECT * FROM sampleday\n        WHERE year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?) ORDER BY year DESC, month DESC, day DESC LIMIT 1", 6);
        long j = (long) i;
        f.bindLong(1, j);
        f.bindLong(2, j);
        long j2 = (long) i2;
        f.bindLong(3, j2);
        f.bindLong(4, j);
        f.bindLong(5, j2);
        f.bindLong(6, (long) i3);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "year");
            int c5 = Dx0.c(b, "month");
            int c6 = Dx0.c(b, "day");
            int c7 = Dx0.c(b, SampleDay.COLUMN_TIMEZONE_NAME);
            int c8 = Dx0.c(b, SampleDay.COLUMN_DST_OFFSET);
            int c9 = Dx0.c(b, "steps");
            int c10 = Dx0.c(b, "calories");
            int c11 = Dx0.c(b, "distance");
            int c12 = Dx0.c(b, SampleDay.COLUMN_INTENSITIES);
            int c13 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            try {
                int c14 = Dx0.c(b, SampleDay.COLUMN_STEP_GOAL);
                int c15 = Dx0.c(b, SampleDay.COLUMN_CALORIES_GOAL);
                int c16 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME_GOAL);
                if (b.moveToFirst()) {
                    activitySummary = new ActivitySummary(b.getInt(c4), b.getInt(c5), b.getInt(c6), b.getString(c7), b.isNull(c8) ? null : Integer.valueOf(b.getInt(c8)), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), this.__integerArrayConverter.a(b.getString(c12)), b.getInt(c13), b.getInt(c14), b.getInt(c15), b.getInt(c16));
                    activitySummary.setCreatedAt(this.__dateTimeConverter.a(b.getLong(c)));
                    activitySummary.setUpdatedAt(this.__dateTimeConverter.a(b.getLong(c2)));
                    activitySummary.setPinType(b.getInt(c3));
                } else {
                    activitySummary = null;
                }
                b.close();
                f.m();
                return activitySummary;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public ActivitySummary.TotalValuesOfWeek getTotalValuesOfWeek(int i, int i2, int i3, int i4, int i5, int i6) {
        ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = null;
        Rh f = Rh.f("SELECT SUM(steps) as totalStepsOfWeek,  SUM(calories) as totalCaloriesOfWeek,  SUM(activeTime) as totalActiveTimeOfWeek FROM sampleday\n        WHERE (year > ? OR (year = ? AND month > ?) OR (year = ? AND month = ? AND day >= ?))\n        AND (year < ? OR (year = ? AND month < ?) OR (year = ? AND month = ? AND day <= ?))\n        ", 12);
        long j = (long) i3;
        f.bindLong(1, j);
        f.bindLong(2, j);
        long j2 = (long) i2;
        f.bindLong(3, j2);
        f.bindLong(4, j);
        f.bindLong(5, j2);
        f.bindLong(6, (long) i);
        long j3 = (long) i6;
        f.bindLong(7, j3);
        f.bindLong(8, j3);
        long j4 = (long) i5;
        f.bindLong(9, j4);
        f.bindLong(10, j3);
        f.bindLong(11, j4);
        f.bindLong(12, (long) i4);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "totalStepsOfWeek");
            int c2 = Dx0.c(b, "totalCaloriesOfWeek");
            int c3 = Dx0.c(b, "totalActiveTimeOfWeek");
            if (b.moveToFirst()) {
                totalValuesOfWeek = new ActivitySummary.TotalValuesOfWeek(b.getDouble(c), b.getDouble(c2), b.getInt(c3));
            }
            return totalValuesOfWeek;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public long insertActivitySettings(ActivitySettings activitySettings) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfActivitySettings.insertAndReturnId(activitySettings);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void updateActivitySettings(int i, int i2, int i3) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfUpdateActivitySettings.acquire();
        acquire.bindLong(1, (long) i);
        acquire.bindLong(2, (long) i2);
        acquire.bindLong(3, (long) i3);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfUpdateActivitySettings.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void upsertActivityRecommendedGoals(ActivityRecommendedGoals activityRecommendedGoals) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivityRecommendedGoals.insert((Hh<ActivityRecommendedGoals>) activityRecommendedGoals);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public long upsertActivityStatistic(ActivityStatistic activityStatistic) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfActivityStatistic.insertAndReturnId(activityStatistic);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void upsertActivitySummaries(List<ActivitySummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivitySummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao
    public void upsertActivitySummary(ActivitySummary activitySummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfActivitySummary.insert((Hh<ActivitySummary>) activitySummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
