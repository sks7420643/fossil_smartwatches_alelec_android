package com.portfolio.platform.data.source.local;

import com.portfolio.platform.data.model.microapp.weather.AddressOfWeather;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface AddressDao {
    @DexIgnore
    Object clearData();  // void declaration

    @DexIgnore
    List<AddressOfWeather> getAllSavedAddress();

    @DexIgnore
    void saveAddress(AddressOfWeather addressOfWeather);
}
