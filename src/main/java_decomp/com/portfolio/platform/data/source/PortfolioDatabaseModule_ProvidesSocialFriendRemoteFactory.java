package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesSocialFriendRemoteFactory implements Factory<FriendRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesSocialFriendRemoteFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        this.module = portfolioDatabaseModule;
        this.apiProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesSocialFriendRemoteFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        return new PortfolioDatabaseModule_ProvidesSocialFriendRemoteFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static FriendRemoteDataSource providesSocialFriendRemote(PortfolioDatabaseModule portfolioDatabaseModule, ApiServiceV2 apiServiceV2) {
        FriendRemoteDataSource providesSocialFriendRemote = portfolioDatabaseModule.providesSocialFriendRemote(apiServiceV2);
        Lk7.c(providesSocialFriendRemote, "Cannot return null from a non-@Nullable @Provides method");
        return providesSocialFriendRemote;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public FriendRemoteDataSource get() {
        return providesSocialFriendRemote(this.module, this.apiProvider.get());
    }
}
