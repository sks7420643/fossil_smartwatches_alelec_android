package com.portfolio.platform.data.source.loader;

import android.content.Context;
import com.portfolio.platform.data.source.NotificationsRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationsLoader_Factory implements Factory<NotificationsLoader> {
    @DexIgnore
    public /* final */ Provider<Context> contextProvider;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> notificationsRepositoryProvider;

    @DexIgnore
    public NotificationsLoader_Factory(Provider<Context> provider, Provider<NotificationsRepository> provider2) {
        this.contextProvider = provider;
        this.notificationsRepositoryProvider = provider2;
    }

    @DexIgnore
    public static NotificationsLoader_Factory create(Provider<Context> provider, Provider<NotificationsRepository> provider2) {
        return new NotificationsLoader_Factory(provider, provider2);
    }

    @DexIgnore
    public static NotificationsLoader newInstance(Context context, NotificationsRepository notificationsRepository) {
        return new NotificationsLoader(context, notificationsRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public NotificationsLoader get() {
        return newInstance(this.contextProvider.get(), this.notificationsRepositoryProvider.get());
    }
}
