package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Rz4;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.diana.WatchAppData;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppDataDao_Impl implements WatchAppDataDao {
    @DexIgnore
    public /* final */ Rz4 __dateTimeConverter; // = new Rz4();
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<WatchAppData> __insertionAdapterOfWatchAppData;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAll;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteWatchAppDataById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<WatchAppData> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, WatchAppData watchAppData) {
            if (watchAppData.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, watchAppData.getId());
            }
            if (watchAppData.getType() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, watchAppData.getType());
            }
            if (watchAppData.getMaxAppVersion() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, watchAppData.getMaxAppVersion());
            }
            if (watchAppData.getMaxFirmwareOSVersion() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, watchAppData.getMaxFirmwareOSVersion());
            }
            if (watchAppData.getMinAppVersion() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, watchAppData.getMinAppVersion());
            }
            if (watchAppData.getMinFirmwareOSVersion() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, watchAppData.getMinFirmwareOSVersion());
            }
            if (watchAppData.getVersion() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, watchAppData.getVersion());
            }
            if (watchAppData.getExecutableBinaryDataUrl() == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, watchAppData.getExecutableBinaryDataUrl());
            }
            mi.bindLong(9, WatchAppDataDao_Impl.this.__dateTimeConverter.b(watchAppData.getUpdatedAt()));
            mi.bindLong(10, WatchAppDataDao_Impl.this.__dateTimeConverter.b(watchAppData.getCreatedAt()));
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, WatchAppData watchAppData) {
            bind(mi, watchAppData);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchAppData` (`id`,`type`,`maxAppVersion`,`maxFirmwareOSVersion`,`minAppVersion`,`minFirmwareOSVersion`,`version`,`executableBinaryDataUrl`,`updatedAt`,`createdAt`) VALUES (?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM watchAppData";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM watchAppData WHERE id=?";
        }
    }

    @DexIgnore
    public WatchAppDataDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfWatchAppData = new Anon1(oh);
        this.__preparedStmtOfClearAll = new Anon2(oh);
        this.__preparedStmtOfDeleteWatchAppDataById = new Anon3(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public void deleteWatchAppDataById(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteWatchAppDataById.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchAppDataById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public List<WatchAppData> getAllWatchAppData() {
        Rh f = Rh.f("SELECT * FROM watchAppData", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "type");
            int c3 = Dx0.c(b, "maxAppVersion");
            int c4 = Dx0.c(b, "maxFirmwareOSVersion");
            int c5 = Dx0.c(b, "minAppVersion");
            int c6 = Dx0.c(b, "minFirmwareOSVersion");
            int c7 = Dx0.c(b, "version");
            int c8 = Dx0.c(b, "executableBinaryDataUrl");
            int c9 = Dx0.c(b, "updatedAt");
            int c10 = Dx0.c(b, "createdAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WatchAppData(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8), this.__dateTimeConverter.a(b.getLong(c9)), this.__dateTimeConverter.a(b.getLong(c10))));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public void upsertWatchAppData(WatchAppData watchAppData) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchAppData.insert((Hh<WatchAppData>) watchAppData);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchAppDataDao
    public void upsertWatchAppDataList(List<WatchAppData> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchAppData.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
