package com.portfolio.platform.data.source.local.diana;

import com.mapped.Xe6;
import com.portfolio.platform.data.model.diana.Complication;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ComplicationDao {
    @DexIgnore
    Object clearAll();  // void declaration

    @DexIgnore
    Object getAllComplications(Xe6<? super List<Complication>> xe6);

    @DexIgnore
    Object getComplicationById(String str, Xe6<? super Complication> xe6);

    @DexIgnore
    List<Complication> getComplicationByIds(List<String> list);

    @DexIgnore
    List<Complication> queryComplicationByName(String str);

    @DexIgnore
    void upsertComplicationList(List<Complication> list);
}
