package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmsRemoteDataSource_Factory implements Factory<AlarmsRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;

    @DexIgnore
    public AlarmsRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceV2Provider = provider;
    }

    @DexIgnore
    public static AlarmsRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new AlarmsRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static AlarmsRemoteDataSource newInstance(ApiServiceV2 apiServiceV2) {
        return new AlarmsRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public AlarmsRemoteDataSource get() {
        return newInstance(this.mApiServiceV2Provider.get());
    }
}
