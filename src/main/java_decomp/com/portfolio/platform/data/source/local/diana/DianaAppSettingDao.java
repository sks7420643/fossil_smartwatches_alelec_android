package com.portfolio.platform.data.source.local.diana;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface DianaAppSettingDao {
    @DexIgnore
    Object cleanUp();  // void declaration

    @DexIgnore
    void deleteDianaAppSetting(String str, String str2);

    @DexIgnore
    List<DianaAppSetting> getAllDianaAppSetting();

    @DexIgnore
    List<DianaAppSetting> getAllDianaAppSetting(String str);

    @DexIgnore
    LiveData<List<DianaAppSetting>> getAllDianaAppSettingAsLiveData(String str);

    @DexIgnore
    DianaAppSetting getDianaAppSetting(String str, String str2);

    @DexIgnore
    List<DianaAppSetting> getPendingDianaAppSetting();

    @DexIgnore
    Long[] insert(List<DianaAppSetting> list);

    @DexIgnore
    void upsertDianaAppSetting(DianaAppSetting dianaAppSetting);

    @DexIgnore
    void upsertDianaAppSettingList(List<DianaAppSetting> list);
}
