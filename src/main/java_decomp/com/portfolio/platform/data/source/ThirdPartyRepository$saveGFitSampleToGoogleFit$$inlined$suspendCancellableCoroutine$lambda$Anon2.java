package com.portfolio.platform.data.source;

import com.fossil.Dl7;
import com.fossil.Rh2;
import com.mapped.Hh6;
import com.mapped.Lc3;
import com.mapped.Lk6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2 implements Lc3 {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Lk6 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Hh6 $countSizeOfLists$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $gFitSampleList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Rh2 $historyClient$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfDataSetList;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfListsOfGFitSampleList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore
    public ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(int i, Rh2 rh2, Hh6 hh6, Lk6 lk6, int i2, ThirdPartyRepository thirdPartyRepository, List list, String str) {
        this.$sizeOfDataSetList = i;
        this.$historyClient$inlined = rh2;
        this.$countSizeOfLists$inlined = hh6;
        this.$continuation$inlined = lk6;
        this.$sizeOfListsOfGFitSampleList$inlined = i2;
        this.this$0 = thirdPartyRepository;
        this.$gFitSampleList$inlined = list;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    @Override // com.mapped.Lc3
    public final void onFailure(Exception exc) {
        Wg6.c(exc, "it");
        FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "saveGFitSampleToGoogleFit - Fail");
        Hh6 hh6 = this.$countSizeOfLists$inlined;
        int i = hh6.element + 1;
        hh6.element = i;
        if (i >= this.$sizeOfListsOfGFitSampleList$inlined * this.$sizeOfDataSetList && this.$continuation$inlined.isActive()) {
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveGFitSampleToGoogleFit - Fail");
            Lk6 lk6 = this.$continuation$inlined;
            Dl7.Ai ai = Dl7.Companion;
            lk6.resumeWith(Dl7.constructor-impl(null));
        }
    }
}
