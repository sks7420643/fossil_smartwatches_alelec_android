package com.portfolio.platform.data.source;

import com.fossil.Q88;
import com.mapped.Cd6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.util.NetworkBoundResource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository$getSleepStatistic$Anon2 extends NetworkBoundResource<SleepStatistic, SleepStatistic> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore
    public SleepSummariesRepository$getSleepStatistic$Anon2(SleepSummariesRepository sleepSummariesRepository, boolean z) {
        this.this$0 = sleepSummariesRepository;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.portfolio.platform.util.NetworkBoundResource
    public Object createCall(Xe6<? super Q88<SleepStatistic>> xe6) {
        return this.this$0.mApiService.getSleepStatistic(xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    @Override // com.portfolio.platform.util.NetworkBoundResource
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object loadFromDb(com.mapped.Xe6<? super androidx.lifecycle.LiveData<com.portfolio.platform.data.SleepStatistic>> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$loadFromDb$Anon1_Level2
            if (r0 == 0) goto L_0x0032
            r0 = r6
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$loadFromDb$Anon1_Level2 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$loadFromDb$Anon1_Level2) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0040
            if (r3 != r4) goto L_0x0038
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r0
            com.portfolio.platform.data.source.local.sleep.SleepDao r0 = r0.sleepDao()
            androidx.lifecycle.LiveData r0 = r0.getSleepStatisticLiveData()
        L_0x0031:
            return r0
        L_0x0032:
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$loadFromDb$Anon1_Level2 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$loadFromDb$Anon1_Level2
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0038:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0040:
            com.fossil.El7.b(r1)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.D(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2.loadFromDb(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    @Override // com.portfolio.platform.util.NetworkBoundResource
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.TAG, "getSleepStatistic - onFetchFailed");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object saveCallResult(com.portfolio.platform.data.SleepStatistic r8, com.mapped.Xe6<? super com.mapped.Cd6> r9) {
        /*
            r7 = this;
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$saveCallResult$Anon1_Level2
            if (r0 == 0) goto L_0x003a
            r0 = r9
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$saveCallResult$Anon1_Level2 r0 = (com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$saveCallResult$Anon1_Level2) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003a
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0049
            if (r3 != r6) goto L_0x0041
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.data.SleepStatistic r0 = (com.portfolio.platform.data.SleepStatistic) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2 r1 = (com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r8 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r0
            com.portfolio.platform.data.source.local.sleep.SleepDao r0 = r0.sleepDao()
            r0.upsertSleepStatistic(r8)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0039:
            return r0
        L_0x003a:
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$saveCallResult$Anon1_Level2 r0 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2$saveCallResult$Anon1_Level2
            r0.<init>(r7, r9)
            r1 = r0
            goto L_0x0014
        L_0x0041:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0049:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.SleepSummariesRepository.access$getTAG$cp()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "getSleepStatistic - saveCallResult -- item="
            r4.append(r5)
            r4.append(r8)
            java.lang.String r4 = r4.toString()
            r2.d(r3, r4)
            com.portfolio.platform.manager.EncryptedDatabaseManager r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r1.L$0 = r7
            r1.L$1 = r8
            r1.label = r6
            java.lang.Object r1 = r2.D(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$Anon2.saveCallResult(com.portfolio.platform.data.SleepStatistic, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.mapped.Xe6] */
    @Override // com.portfolio.platform.util.NetworkBoundResource
    public /* bridge */ /* synthetic */ Object saveCallResult(SleepStatistic sleepStatistic, Xe6 xe6) {
        return saveCallResult(sleepStatistic, (Xe6<? super Cd6>) xe6);
    }

    @DexIgnore
    public boolean shouldFetch(SleepStatistic sleepStatistic) {
        return this.$shouldFetch;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.portfolio.platform.util.NetworkBoundResource
    public /* bridge */ /* synthetic */ boolean shouldFetch(SleepStatistic sleepStatistic) {
        return shouldFetch(sleepStatistic);
    }
}
