package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.Ao7;
import com.fossil.El7;
import com.fossil.H47;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Ss0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hh6;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingDataKt;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.Date;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$2", f = "GoalTrackingRepository.kt", l = {352}, m = "invokeSuspend")
public final class GoalTrackingRepository$getGoalTrackingDataList$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super LiveData<H47<? extends List<GoalTrackingData>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingRepository$getGoalTrackingDataList$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends NetworkBoundResource<List<GoalTrackingData>, ApiResponse<GoalEvent>> {
            @DexIgnore
            public /* final */ /* synthetic */ Lc6 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ int $limit;
            @DexIgnore
            public /* final */ /* synthetic */ Hh6 $offset;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, Hh6 hh6, int i, Lc6 lc6) {
                this.this$0 = anon1_Level2;
                this.$offset = hh6;
                this.$limit = i;
                this.$downloadingDate = lc6;
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public Object createCall(Xe6<? super Q88<ApiResponse<GoalEvent>>> xe6) {
                Date date;
                Date date2;
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiServiceV2;
                Lc6 lc6 = this.$downloadingDate;
                if (lc6 == null || (date = (Date) lc6.getFirst()) == null) {
                    date = this.this$0.this$0.$startDate;
                }
                String k = TimeUtils.k(date);
                Wg6.b(k, "DateHelper.formatShortDa\u2026            ?: startDate)");
                Lc6 lc62 = this.$downloadingDate;
                if (lc62 == null || (date2 = (Date) lc62.getSecond()) == null) {
                    date2 = this.this$0.this$0.$endDate;
                }
                String k2 = TimeUtils.k(date2);
                Wg6.b(k2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return apiServiceV2.getGoalTrackingDataList(k, k2, this.$offset.element, this.$limit, xe6);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:13:0x0049  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object loadFromDb(com.mapped.Xe6<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData>>> r6) {
                /*
                    r5 = this;
                    r4 = 1
                    r3 = -2147483648(0xffffffff80000000, float:-0.0)
                    boolean r0 = r6 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    if (r0 == 0) goto L_0x003b
                    r0 = r6
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = r1 & r3
                    if (r2 == 0) goto L_0x003b
                    int r1 = r1 + r3
                    r0.label = r1
                L_0x0013:
                    java.lang.Object r1 = r0.result
                    java.lang.Object r2 = com.fossil.Yn7.d()
                    int r3 = r0.label
                    if (r3 == 0) goto L_0x0049
                    if (r3 != r4) goto L_0x0041
                    java.lang.Object r0 = r0.L$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository.getGoalTrackingDataList.Anon2.Anon1_Level2.Anon1_Level3) r0
                    com.fossil.El7.b(r1)
                    r5 = r0
                L_0x0027:
                    r0 = r1
                    com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r0
                    com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r0 = r0.getGoalTrackingDao()
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2 r1 = r5.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2 r1 = r1.this$0
                    java.util.Date r2 = r1.$startDate
                    java.util.Date r1 = r1.$endDate
                    androidx.lifecycle.LiveData r0 = r0.getGoalTrackingDataListLiveData(r2, r1)
                L_0x003a:
                    return r0
                L_0x003b:
                    com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    r0.<init>(r5, r6)
                    goto L_0x0013
                L_0x0041:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0049:
                    com.fossil.El7.b(r1)
                    com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
                    r0.L$0 = r5
                    r0.label = r4
                    java.lang.Object r1 = r1.A(r0)
                    if (r1 != r2) goto L_0x0027
                    r0 = r2
                    goto L_0x003a
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getGoalTrackingDataList.Anon2.Anon1_Level2.Anon1_Level3.loadFromDb(com.mapped.Xe6):java.lang.Object");
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().e(GoalTrackingRepository.Companion.getTAG(), "getGoalTrackingDataList onFetchFailed");
            }

            @DexIgnore
            public Object processContinueFetching(ApiResponse<GoalEvent> apiResponse, Xe6<? super Boolean> xe6) {
                Boolean a2;
                Range range = apiResponse.get_range();
                if (range == null || (a2 = Ao7.a(range.isHasNext())) == null || !a2.booleanValue()) {
                    return Ao7.a(false);
                }
                FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "getGoalTrackingDataList processContinueFetching hasNext");
                this.$offset.element += this.$limit;
                return Ao7.a(true);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.mapped.Xe6] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ Object processContinueFetching(ApiResponse<GoalEvent> apiResponse, Xe6 xe6) {
                return processContinueFetching(apiResponse, (Xe6<? super Boolean>) xe6);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:19:0x0053  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object saveCallResult(com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.goaltracking.response.GoalEvent> r8, com.mapped.Xe6<? super com.mapped.Cd6> r9) {
                /*
                // Method dump skipped, instructions count: 286
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getGoalTrackingDataList.Anon2.Anon1_Level2.Anon1_Level3.saveCallResult(com.portfolio.platform.data.source.remote.ApiResponse, com.mapped.Xe6):java.lang.Object");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.mapped.Xe6] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<GoalEvent> apiResponse, Xe6 xe6) {
                return saveCallResult(apiResponse, (Xe6<? super Cd6>) xe6);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ boolean shouldFetch(List<GoalTrackingData> list) {
                return shouldFetch(list);
            }

            @DexIgnore
            public boolean shouldFetch(List<GoalTrackingData> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(GoalTrackingRepository$getGoalTrackingDataList$Anon2 goalTrackingRepository$getGoalTrackingDataList$Anon2) {
            this.this$0 = goalTrackingRepository$getGoalTrackingDataList$Anon2;
        }

        @DexIgnore
        public final LiveData<H47<List<GoalTrackingData>>> apply(List<GoalTrackingData> list) {
            Wg6.b(list, "pendingList");
            GoalTrackingRepository$getGoalTrackingDataList$Anon2 goalTrackingRepository$getGoalTrackingDataList$Anon2 = this.this$0;
            Lc6<Date, Date> calculateRangeDownload = GoalTrackingDataKt.calculateRangeDownload(list, goalTrackingRepository$getGoalTrackingDataList$Anon2.$startDate, goalTrackingRepository$getGoalTrackingDataList$Anon2.$endDate);
            Hh6 hh6 = new Hh6();
            hh6.element = 0;
            return new Anon1_Level3(this, hh6, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS, calculateRangeDownload).asLiveData();
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return apply((List) obj);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getGoalTrackingDataList$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, boolean z, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = goalTrackingRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        GoalTrackingRepository$getGoalTrackingDataList$Anon2 goalTrackingRepository$getGoalTrackingDataList$Anon2 = new GoalTrackingRepository$getGoalTrackingDataList$Anon2(this.this$0, this.$startDate, this.$endDate, this.$shouldFetch, xe6);
        goalTrackingRepository$getGoalTrackingDataList$Anon2.p$ = (Il6) obj;
        throw null;
        //return goalTrackingRepository$getGoalTrackingDataList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super LiveData<H47<? extends List<GoalTrackingData>>>> xe6) {
        throw null;
        //return ((GoalTrackingRepository$getGoalTrackingDataList$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object pendingGoalTrackingDataListLiveData;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = GoalTrackingRepository.Companion.getTAG();
            local.d(tag, "getGoalTrackingDataList startDate=" + this.$startDate + ", endDate=" + this.$endDate);
            GoalTrackingRepository goalTrackingRepository = this.this$0;
            Date date = this.$startDate;
            Date date2 = this.$endDate;
            this.L$0 = il6;
            this.label = 1;
            pendingGoalTrackingDataListLiveData = goalTrackingRepository.getPendingGoalTrackingDataListLiveData(date, date2, this);
            if (pendingGoalTrackingDataListLiveData == d) {
                return d;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            pendingGoalTrackingDataListLiveData = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return Ss0.c((LiveData) pendingGoalTrackingDataListLiveData, new Anon1_Level2(this));
    }
}
