package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.FitnessDataRepository$pushPendingFitnessData$2", f = "FitnessDataRepository.kt", l = {30, 35}, m = "invokeSuspend")
public final class FitnessDataRepository$pushPendingFitnessData$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<List<FitnessDataWrapper>>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ FitnessDataRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FitnessDataRepository$pushPendingFitnessData$Anon2(FitnessDataRepository fitnessDataRepository, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = fitnessDataRepository;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        FitnessDataRepository$pushPendingFitnessData$Anon2 fitnessDataRepository$pushPendingFitnessData$Anon2 = new FitnessDataRepository$pushPendingFitnessData$Anon2(this.this$0, xe6);
        fitnessDataRepository$pushPendingFitnessData$Anon2.p$ = (Il6) obj;
        throw null;
        //return fitnessDataRepository$pushPendingFitnessData$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<List<FitnessDataWrapper>>> xe6) {
        throw null;
        //return ((FitnessDataRepository$pushPendingFitnessData$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00bc  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
            r11 = this;
            r2 = 1
            r10 = 2
            java.lang.Object r7 = com.fossil.Yn7.d()
            int r0 = r11.label
            if (r0 == 0) goto L_0x0066
            if (r0 == r2) goto L_0x0029
            if (r0 != r10) goto L_0x0021
            java.lang.Object r0 = r11.L$2
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r11.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r11.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r12)
            r0 = r12
        L_0x001e:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
        L_0x0020:
            return r0
        L_0x0021:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0029:
            java.lang.Object r0 = r11.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r12)
            r6 = r0
            r1 = r12
        L_0x0032:
            r0 = r1
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            com.portfolio.platform.data.source.local.FitnessDataDao r0 = r0.getFitnessDataDao()
            java.util.List r8 = r0.getPendingFitnessData()
            int r0 = r8.size()
            if (r0 <= 0) goto L_0x00bc
            java.util.ArrayList r9 = new java.util.ArrayList
            r0 = 10
            int r0 = com.fossil.Im7.m(r8, r0)
            r9.<init>(r0)
            java.util.Iterator r1 = r8.iterator()
        L_0x0052:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0079
            java.lang.Object r0 = r1.next()
            com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper r0 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r0
            org.joda.time.DateTime r0 = r0.getStartTime()
            r9.add(r0)
            goto L_0x0052
        L_0x0066:
            com.fossil.El7.b(r12)
            com.mapped.Il6 r0 = r11.p$
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r11.L$0 = r0
            r11.label = r2
            java.lang.Object r1 = r1.y(r11)
            if (r1 != r7) goto L_0x00ca
            r0 = r7
            goto L_0x0020
        L_0x0079:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r0 = r0.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r1 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r2 = com.misfit.frameworks.buttonservice.log.FLogger.Session.PUSH_FITNESS_FILE
            java.lang.String r4 = com.portfolio.platform.data.source.FitnessDataRepository.access$getTAG$cp()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r3 = "[Push Pending Fitness File] Pending fitness file size "
            r5.append(r3)
            int r3 = r8.size()
            r5.append(r3)
            java.lang.String r3 = " startTimestamp "
            r5.append(r3)
            r5.append(r9)
            java.lang.String r3 = ""
            java.lang.String r5 = r5.toString()
            r0.i(r1, r2, r3, r4, r5)
            com.portfolio.platform.data.source.FitnessDataRepository r0 = r11.this$0
            r11.L$0 = r6
            r11.L$1 = r8
            r11.L$2 = r9
            r11.label = r10
            java.lang.Object r0 = r0.pushFitnessData(r8, r11)
            if (r0 != r7) goto L_0x001e
            r0 = r7
            goto L_0x0020
        L_0x00bc:
            com.fossil.Kq5 r0 = new com.fossil.Kq5
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r2 = 0
            r3 = 0
            r0.<init>(r1, r2, r10, r3)
            goto L_0x0020
        L_0x00ca:
            r6 = r0
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.FitnessDataRepository$pushPendingFitnessData$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
