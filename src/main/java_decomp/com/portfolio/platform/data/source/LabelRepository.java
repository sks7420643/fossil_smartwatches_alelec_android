package com.portfolio.platform.data.source;

import android.content.Context;
import com.mapped.Ap4;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.local.label.Label;
import com.portfolio.platform.data.source.remote.LabelRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LabelRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "LabelRepository";
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public /* final */ LabelRemoteDataSource mRemoteSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public LabelRepository(Context context2, LabelRemoteDataSource labelRemoteDataSource) {
        Wg6.c(context2, "context");
        Wg6.c(labelRemoteDataSource, "mRemoteSource");
        this.context = context2;
        this.mRemoteSource = labelRemoteDataSource;
    }

    @DexIgnore
    public final Object downloadLabel(String str, Xe6<? super Ap4<Label>> xe6) {
        return this.mRemoteSource.getLabel(str, xe6);
    }

    @DexIgnore
    public final Context getContext() {
        return this.context;
    }
}
