package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory implements Factory<NotificationSettingsDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideNotificationSettingsDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static NotificationSettingsDatabase provideNotificationSettingsDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        NotificationSettingsDatabase provideNotificationSettingsDatabase = portfolioDatabaseModule.provideNotificationSettingsDatabase(portfolioApp);
        Lk7.c(provideNotificationSettingsDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideNotificationSettingsDatabase;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public NotificationSettingsDatabase get() {
        return provideNotificationSettingsDatabase(this.module, this.appProvider.get());
    }
}
