package com.portfolio.platform.data.source.local.inapp;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.mapped.Gh;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.portfolio.platform.data.InAppNotification;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InAppNotificationDao_Impl extends InAppNotificationDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Gh<InAppNotification> __deletionAdapterOfInAppNotification;
    @DexIgnore
    public /* final */ Hh<InAppNotification> __insertionAdapterOfInAppNotification;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<InAppNotification> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, InAppNotification inAppNotification) {
            if (inAppNotification.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, inAppNotification.getId());
            }
            if (inAppNotification.getTitle() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, inAppNotification.getTitle());
            }
            if (inAppNotification.getContent() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, inAppNotification.getContent());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, InAppNotification inAppNotification) {
            bind(mi, inAppNotification);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `inAppNotification` (`id`,`title`,`content`) VALUES (?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Gh<InAppNotification> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, InAppNotification inAppNotification) {
            if (inAppNotification.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, inAppNotification.getId());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Gh
        public /* bridge */ /* synthetic */ void bind(Mi mi, InAppNotification inAppNotification) {
            bind(mi, inAppNotification);
        }

        @DexIgnore
        @Override // com.mapped.Vh, com.mapped.Gh
        public String createQuery() {
            return "DELETE FROM `inAppNotification` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<InAppNotification>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon3(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<InAppNotification> call() throws Exception {
            Cursor b = Ex0.b(InAppNotificationDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "id");
                int c2 = Dx0.c(b, "title");
                int c3 = Dx0.c(b, "content");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new InAppNotification(b.getString(c), b.getString(c2), b.getString(c3)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public InAppNotificationDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfInAppNotification = new Anon1(oh);
        this.__deletionAdapterOfInAppNotification = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.inapp.InAppNotificationDao
    public void delete(InAppNotification inAppNotification) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfInAppNotification.handle(inAppNotification);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.inapp.InAppNotificationDao
    public LiveData<List<InAppNotification>> getAllInAppNotification() {
        Rh f = Rh.f("SELECT * FROM inAppNotification", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"inAppNotification"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.inapp.InAppNotificationDao
    public void insertListInAppNotification(List<InAppNotification> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfInAppNotification.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
