package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.mapped.Gh;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitSampleDao_Impl implements GFitSampleDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Gh<GFitSample> __deletionAdapterOfGFitSample;
    @DexIgnore
    public /* final */ Hh<GFitSample> __insertionAdapterOfGFitSample;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<GFitSample> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, GFitSample gFitSample) {
            mi.bindLong(1, (long) gFitSample.getId());
            mi.bindLong(2, (long) gFitSample.getStep());
            mi.bindDouble(3, (double) gFitSample.getDistance());
            mi.bindDouble(4, (double) gFitSample.getCalorie());
            mi.bindLong(5, gFitSample.getStartTime());
            mi.bindLong(6, gFitSample.getEndTime());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, GFitSample gFitSample) {
            bind(mi, gFitSample);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitSample` (`id`,`step`,`distance`,`calorie`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Gh<GFitSample> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, GFitSample gFitSample) {
            mi.bindLong(1, (long) gFitSample.getId());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Gh
        public /* bridge */ /* synthetic */ void bind(Mi mi, GFitSample gFitSample) {
            bind(mi, gFitSample);
        }

        @DexIgnore
        @Override // com.mapped.Vh, com.mapped.Gh
        public String createQuery() {
            return "DELETE FROM `gFitSample` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM gFitSample";
        }
    }

    @DexIgnore
    public GFitSampleDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfGFitSample = new Anon1(oh);
        this.__deletionAdapterOfGFitSample = new Anon2(oh);
        this.__preparedStmtOfClearAll = new Anon3(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public void deleteListGFitSample(List<GFitSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitSample.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public List<GFitSample> getAllGFitSample() {
        Rh f = Rh.f("SELECT * FROM gFitSample", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "step");
            int c3 = Dx0.c(b, "distance");
            int c4 = Dx0.c(b, "calorie");
            int c5 = Dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c6 = Dx0.c(b, SampleRaw.COLUMN_END_TIME);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GFitSample gFitSample = new GFitSample(b.getInt(c2), b.getFloat(c3), b.getFloat(c4), b.getLong(c5), b.getLong(c6));
                gFitSample.setId(b.getInt(c));
                arrayList.add(gFitSample);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public void insertGFitSample(GFitSample gFitSample) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSample.insert((Hh<GFitSample>) gFitSample);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSampleDao
    public void insertListGFitSample(List<GFitSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
