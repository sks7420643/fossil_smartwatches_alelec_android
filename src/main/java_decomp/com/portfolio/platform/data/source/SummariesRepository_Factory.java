package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.FitnessHelper;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository_Factory implements Factory<SummariesRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;
    @DexIgnore
    public /* final */ Provider<FitnessHelper> mFitnessHelperProvider;

    @DexIgnore
    public SummariesRepository_Factory(Provider<ApiServiceV2> provider, Provider<FitnessHelper> provider2) {
        this.mApiServiceV2Provider = provider;
        this.mFitnessHelperProvider = provider2;
    }

    @DexIgnore
    public static SummariesRepository_Factory create(Provider<ApiServiceV2> provider, Provider<FitnessHelper> provider2) {
        return new SummariesRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static SummariesRepository newInstance(ApiServiceV2 apiServiceV2, FitnessHelper fitnessHelper) {
        return new SummariesRepository(apiServiceV2, fitnessHelper);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public SummariesRepository get() {
        return newInstance(this.mApiServiceV2Provider.get(), this.mFitnessHelperProvider.get());
    }
}
