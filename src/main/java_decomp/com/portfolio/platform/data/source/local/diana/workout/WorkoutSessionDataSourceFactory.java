package com.portfolio.platform.data.source.local.diana.workout;

import androidx.lifecycle.MutableLiveData;
import com.mapped.PagingRequestHelper;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Xe;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionDataSourceFactory extends Xe.Bi<Long, WorkoutSession> {
    @DexIgnore
    public /* final */ U04 appExecutors;
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public /* final */ PagingRequestHelper.Ai listener;
    @DexIgnore
    public WorkoutSessionLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ MutableLiveData<WorkoutSessionLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ FitnessDatabase workoutDatabase;
    @DexIgnore
    public /* final */ WorkoutSessionRepository workoutSessionRepository;

    @DexIgnore
    public WorkoutSessionDataSourceFactory(WorkoutSessionRepository workoutSessionRepository2, FitnessDatabase fitnessDatabase, Date date, U04 u04, PagingRequestHelper.Ai ai) {
        Wg6.c(workoutSessionRepository2, "workoutSessionRepository");
        Wg6.c(fitnessDatabase, "workoutDatabase");
        Wg6.c(date, "currentDate");
        Wg6.c(u04, "appExecutors");
        Wg6.c(ai, "listener");
        this.workoutSessionRepository = workoutSessionRepository2;
        this.workoutDatabase = fitnessDatabase;
        this.currentDate = date;
        this.appExecutors = u04;
        this.listener = ai;
    }

    @DexIgnore
    @Override // com.mapped.Xe.Bi
    public Xe<Long, WorkoutSession> create() {
        WorkoutSessionLocalDataSource workoutSessionLocalDataSource = new WorkoutSessionLocalDataSource(this.workoutSessionRepository, this.workoutDatabase, this.currentDate, this.appExecutors, this.listener);
        this.localDataSource = workoutSessionLocalDataSource;
        this.sourceLiveData.l(workoutSessionLocalDataSource);
        WorkoutSessionLocalDataSource workoutSessionLocalDataSource2 = this.localDataSource;
        if (workoutSessionLocalDataSource2 != null) {
            return workoutSessionLocalDataSource2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final WorkoutSessionLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<WorkoutSessionLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(WorkoutSessionLocalDataSource workoutSessionLocalDataSource) {
        this.localDataSource = workoutSessionLocalDataSource;
    }
}
