package com.portfolio.platform.data.source.local;

import com.fossil.Ex0;
import com.fossil.Hw0;
import com.fossil.Ix0;
import com.fossil.Lx0;
import com.fossil.Nw0;
import com.mapped.Ji;
import com.mapped.Oh;
import com.mapped.Qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDatabase_Impl extends FileDatabase {
    @DexIgnore
    public volatile FileDao _fileDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Qh.Ai {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void createAllTables(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `localfile` (`pinType` INTEGER NOT NULL, `type` TEXT NOT NULL, `fileName` TEXT NOT NULL, `localUri` TEXT NOT NULL, `remoteUrl` TEXT NOT NULL, `checksum` TEXT, PRIMARY KEY(`remoteUrl`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '415513f336004021a41b46790e07d643')");
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void dropAllTables(Lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `localfile`");
            if (FileDatabase_Impl.this.mCallbacks != null) {
                int size = FileDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) FileDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onCreate(Lx0 lx0) {
            if (FileDatabase_Impl.this.mCallbacks != null) {
                int size = FileDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) FileDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onOpen(Lx0 lx0) {
            FileDatabase_Impl.this.mDatabase = lx0;
            FileDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (FileDatabase_Impl.this.mCallbacks != null) {
                int size = FileDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) FileDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPostMigrate(Lx0 lx0) {
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPreMigrate(Lx0 lx0) {
            Ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public Qh.Bi onValidateSchema(Lx0 lx0) {
            HashMap hashMap = new HashMap(6);
            hashMap.put("pinType", new Ix0.Ai("pinType", "INTEGER", true, 0, null, 1));
            hashMap.put("type", new Ix0.Ai("type", "TEXT", true, 0, null, 1));
            hashMap.put("fileName", new Ix0.Ai("fileName", "TEXT", true, 0, null, 1));
            hashMap.put("localUri", new Ix0.Ai("localUri", "TEXT", true, 0, null, 1));
            hashMap.put("remoteUrl", new Ix0.Ai("remoteUrl", "TEXT", true, 1, null, 1));
            hashMap.put("checksum", new Ix0.Ai("checksum", "TEXT", false, 0, null, 1));
            Ix0 ix0 = new Ix0("localfile", hashMap, new HashSet(0), new HashSet(0));
            Ix0 a2 = Ix0.a(lx0, "localfile");
            if (ix0.equals(a2)) {
                return new Qh.Bi(true, null);
            }
            return new Qh.Bi(false, "localfile(com.portfolio.platform.data.model.LocalFile).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public void clearAllTables() {
        super.assertNotMainThread();
        Lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `localfile`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Nw0 createInvalidationTracker() {
        return new Nw0(this, new HashMap(0), new HashMap(0), "localfile");
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Ji createOpenHelper(Hw0 hw0) {
        Qh qh = new Qh(hw0, new Anon1(2), "415513f336004021a41b46790e07d643", "f5892aa2843b6abf4eab005d74132c1f");
        Ji.Bi.Aii a2 = Ji.Bi.a(hw0.b);
        a2.c(hw0.c);
        a2.b(qh);
        return hw0.a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDatabase
    public FileDao fileDao() {
        FileDao fileDao;
        if (this._fileDao != null) {
            return this._fileDao;
        }
        synchronized (this) {
            if (this._fileDao == null) {
                this._fileDao = new FileDao_Impl(this);
            }
            fileDao = this._fileDao;
        }
        return fileDao;
    }
}
