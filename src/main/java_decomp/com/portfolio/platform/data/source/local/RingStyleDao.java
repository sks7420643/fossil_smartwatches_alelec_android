package com.portfolio.platform.data.source.local;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface RingStyleDao {
    @DexIgnore
    Object clearAllData();  // void declaration

    @DexIgnore
    List<DianaComplicationRingStyle> getRingStyles();

    @DexIgnore
    LiveData<List<DianaComplicationRingStyle>> getRingStylesAsLiveData();

    @DexIgnore
    DianaComplicationRingStyle getRingStylesBySerial(String str);

    @DexIgnore
    void insertRingStyle(DianaComplicationRingStyle dianaComplicationRingStyle);

    @DexIgnore
    void insertRingStyles(List<DianaComplicationRingStyle> list);
}
