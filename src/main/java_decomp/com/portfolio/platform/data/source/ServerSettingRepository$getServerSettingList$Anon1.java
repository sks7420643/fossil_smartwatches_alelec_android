package com.portfolio.platform.data.source;

import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.mapped.Rm6;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerSettingRepository$getServerSettingList$Anon1 implements ServerSettingDataSource.OnGetServerSettingList {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingDataSource.OnGetServerSettingList $callback;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRepository this$0;

    @DexIgnore
    public ServerSettingRepository$getServerSettingList$Anon1(ServerSettingRepository serverSettingRepository, ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        this.this$0 = serverSettingRepository;
        this.$callback = onGetServerSettingList;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
    public void onFailed(int i) {
        this.$callback.onFailed(i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
    public void onSuccess(ServerSettingList serverSettingList) {
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2(this, serverSettingList, null), 3, null);
        this.$callback.onSuccess(serverSettingList);
    }
}
