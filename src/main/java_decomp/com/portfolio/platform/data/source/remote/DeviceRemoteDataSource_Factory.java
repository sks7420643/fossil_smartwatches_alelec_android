package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceRemoteDataSource_Factory implements Factory<DeviceRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;
    @DexIgnore
    public /* final */ Provider<ApiService2Dot1> mApiServiceV21Provider;
    @DexIgnore
    public /* final */ Provider<SecureApiService2Dot1> mSecureApiService2Dot1Provider;

    @DexIgnore
    public DeviceRemoteDataSource_Factory(Provider<ApiServiceV2> provider, Provider<ApiService2Dot1> provider2, Provider<SecureApiService2Dot1> provider3) {
        this.mApiServiceProvider = provider;
        this.mApiServiceV21Provider = provider2;
        this.mSecureApiService2Dot1Provider = provider3;
    }

    @DexIgnore
    public static DeviceRemoteDataSource_Factory create(Provider<ApiServiceV2> provider, Provider<ApiService2Dot1> provider2, Provider<SecureApiService2Dot1> provider3) {
        return new DeviceRemoteDataSource_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static DeviceRemoteDataSource newInstance(ApiServiceV2 apiServiceV2, ApiService2Dot1 apiService2Dot1, SecureApiService2Dot1 secureApiService2Dot1) {
        return new DeviceRemoteDataSource(apiServiceV2, apiService2Dot1, secureApiService2Dot1);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public DeviceRemoteDataSource get() {
        return newInstance(this.mApiServiceProvider.get(), this.mApiServiceV21Provider.get(), this.mSecureApiService2Dot1Provider.get());
    }
}
