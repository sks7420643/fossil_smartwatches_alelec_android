package com.portfolio.platform.data.source.remote;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource$downloadWorkoutSettingList$response$1", f = "WorkoutSettingRemoteDataSource.kt", l = {24}, m = "invokeSuspend")
public final class WorkoutSettingRemoteDataSource$downloadWorkoutSettingList$response$Anon1 extends Ko7 implements Hg6<Xe6<? super Q88<RemoteWorkoutSetting>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSettingRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSettingRemoteDataSource$downloadWorkoutSettingList$response$Anon1(WorkoutSettingRemoteDataSource workoutSettingRemoteDataSource, Xe6 xe6) {
        super(1, xe6);
        this.this$0 = workoutSettingRemoteDataSource;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        return new WorkoutSettingRemoteDataSource$downloadWorkoutSettingList$response$Anon1(this.this$0, xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public final Object invoke(Xe6<? super Q88<RemoteWorkoutSetting>> xe6) {
        throw null;
        //return ((WorkoutSettingRemoteDataSource$downloadWorkoutSettingList$response$Anon1) create(xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiServiceV2;
            this.label = 1;
            Object workoutSettingList = apiServiceV2.getWorkoutSettingList(this);
            return workoutSettingList == d ? d : workoutSettingList;
        } else if (i == 1) {
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
