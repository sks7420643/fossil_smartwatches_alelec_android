package com.portfolio.platform.data.source.remote;

import com.mapped.Wg6;
import com.portfolio.platform.data.model.Range;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ApiResponse<T> {
    @DexIgnore
    public String _etag;
    @DexIgnore
    public List<T> _items; // = new ArrayList();
    @DexIgnore
    public Range _range;
    @DexIgnore
    public List<T> items; // = new ArrayList();
    @DexIgnore
    public String message;

    @DexIgnore
    public final List<T> getItems() {
        return this.items;
    }

    @DexIgnore
    public final String getMessage() {
        return this.message;
    }

    @DexIgnore
    public final String get_etag() {
        return this._etag;
    }

    @DexIgnore
    public final List<T> get_items() {
        return this._items;
    }

    @DexIgnore
    public final Range get_range() {
        return this._range;
    }

    @DexIgnore
    public final void setItems(List<T> list) {
        Wg6.c(list, "<set-?>");
        this.items = list;
    }

    @DexIgnore
    public final void setMessage(String str) {
        this.message = str;
    }

    @DexIgnore
    public final void set_etag(String str) {
        this._etag = str;
    }

    @DexIgnore
    public final void set_items(List<T> list) {
        Wg6.c(list, "<set-?>");
        this._items = list;
    }

    @DexIgnore
    public final void set_range(Range range) {
        this._range = range;
    }
}
