package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.DianaRingRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceRingRepository_Factory implements Factory<DianaWatchFaceRingRepository> {
    @DexIgnore
    public /* final */ Provider<DianaRingRemoteDataSource> mRemoteSourceProvider;

    @DexIgnore
    public DianaWatchFaceRingRepository_Factory(Provider<DianaRingRemoteDataSource> provider) {
        this.mRemoteSourceProvider = provider;
    }

    @DexIgnore
    public static DianaWatchFaceRingRepository_Factory create(Provider<DianaRingRemoteDataSource> provider) {
        return new DianaWatchFaceRingRepository_Factory(provider);
    }

    @DexIgnore
    public static DianaWatchFaceRingRepository newInstance(DianaRingRemoteDataSource dianaRingRemoteDataSource) {
        return new DianaWatchFaceRingRepository(dianaRingRemoteDataSource);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public DianaWatchFaceRingRepository get() {
        return newInstance(this.mRemoteSourceProvider.get());
    }
}
