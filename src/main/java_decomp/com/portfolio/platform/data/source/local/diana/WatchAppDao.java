package com.portfolio.platform.data.source.local.diana;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface WatchAppDao {
    @DexIgnore
    Object clearAll();  // void declaration

    @DexIgnore
    List<WatchApp> getAllWatchApp();

    @DexIgnore
    LiveData<List<WatchApp>> getAllWatchAppAsLiveData();

    @DexIgnore
    WatchApp getWatchAppById(String str);

    @DexIgnore
    List<WatchApp> getWatchAppByIds(List<String> list);

    @DexIgnore
    List<WatchApp> queryWatchAppByName(String str);

    @DexIgnore
    void upsertWatchAppList(List<WatchApp> list);
}
