package com.portfolio.platform.data.source;

import com.fossil.Ak5;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.fossil.Zi4;
import com.google.gson.JsonElement;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SummariesRepository$pushActivitySettingsToServer$2", f = "SummariesRepository.kt", l = {137}, m = "invokeSuspend")
public final class SummariesRepository$pushActivitySettingsToServer$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Q88<ActivitySettings>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySettings $activitySettings;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$pushActivitySettingsToServer$Anon2(SummariesRepository summariesRepository, ActivitySettings activitySettings, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = summariesRepository;
        this.$activitySettings = activitySettings;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SummariesRepository$pushActivitySettingsToServer$Anon2 summariesRepository$pushActivitySettingsToServer$Anon2 = new SummariesRepository$pushActivitySettingsToServer$Anon2(this.this$0, this.$activitySettings, xe6);
        summariesRepository$pushActivitySettingsToServer$Anon2.p$ = (Il6) obj;
        throw null;
        //return summariesRepository$pushActivitySettingsToServer$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Q88<ActivitySettings>> xe6) {
        throw null;
        //return ((SummariesRepository$pushActivitySettingsToServer$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "pushActivitySettingsToServer - settings=" + this.$activitySettings);
            Zi4 zi4 = new Zi4();
            zi4.b(new Ak5());
            JsonElement z = zi4.d().z(this.$activitySettings);
            Wg6.b(z, "GsonBuilder()\n          \u2026sonTree(activitySettings)");
            Ku3 d2 = z.d();
            ApiServiceV2 apiServiceV2 = this.this$0.mApiServiceV2;
            Wg6.b(d2, "jsonObject");
            this.L$0 = il6;
            this.L$1 = d2;
            this.label = 1;
            Object updateActivitySetting = apiServiceV2.updateActivitySetting(d2, this);
            return updateActivitySetting == d ? d : updateActivitySetting;
        } else if (i == 1) {
            Ku3 ku3 = (Ku3) this.L$1;
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
