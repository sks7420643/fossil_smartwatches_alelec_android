package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.fossil.Tz4;
import com.fossil.Vz4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.mapped.Gh;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDataDao_Impl extends FitnessDataDao {
    @DexIgnore
    public /* final */ Tz4 __dateTimeUTCStringConverter; // = new Tz4();
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Gh<FitnessDataWrapper> __deletionAdapterOfFitnessDataWrapper;
    @DexIgnore
    public /* final */ Vz4 __fitnessDataConverter; // = new Vz4();
    @DexIgnore
    public /* final */ Hh<FitnessDataWrapper> __insertionAdapterOfFitnessDataWrapper;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteAllFitnessData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<FitnessDataWrapper> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, FitnessDataWrapper fitnessDataWrapper) {
            String g = FitnessDataDao_Impl.this.__fitnessDataConverter.g(fitnessDataWrapper.step);
            if (g == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, g);
            }
            String a2 = FitnessDataDao_Impl.this.__fitnessDataConverter.a(fitnessDataWrapper.activeMinute);
            if (a2 == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, a2);
            }
            String b = FitnessDataDao_Impl.this.__fitnessDataConverter.b(fitnessDataWrapper.calorie);
            if (b == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, b);
            }
            String c = FitnessDataDao_Impl.this.__fitnessDataConverter.c(fitnessDataWrapper.distance);
            if (c == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, c);
            }
            String h = FitnessDataDao_Impl.this.__fitnessDataConverter.h(fitnessDataWrapper.getStress());
            if (h == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, h);
            }
            String e = FitnessDataDao_Impl.this.__fitnessDataConverter.e(fitnessDataWrapper.getResting());
            if (e == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, e);
            }
            String d = FitnessDataDao_Impl.this.__fitnessDataConverter.d(fitnessDataWrapper.getHeartRate());
            if (d == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, d);
            }
            String f = FitnessDataDao_Impl.this.__fitnessDataConverter.f(fitnessDataWrapper.getSleeps());
            if (f == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, f);
            }
            String r = FitnessDataDao_Impl.this.__fitnessDataConverter.r(fitnessDataWrapper.getWorkouts());
            if (r == null) {
                mi.bindNull(9);
            } else {
                mi.bindString(9, r);
            }
            String a3 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getStartTime());
            if (a3 == null) {
                mi.bindNull(10);
            } else {
                mi.bindString(10, a3);
            }
            String a4 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getEndTime());
            if (a4 == null) {
                mi.bindNull(11);
            } else {
                mi.bindString(11, a4);
            }
            String a5 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getSyncTime());
            if (a5 == null) {
                mi.bindNull(12);
            } else {
                mi.bindString(12, a5);
            }
            mi.bindLong(13, (long) fitnessDataWrapper.getTimezoneOffsetInSecond());
            if (fitnessDataWrapper.getSerialNumber() == null) {
                mi.bindNull(14);
            } else {
                mi.bindString(14, fitnessDataWrapper.getSerialNumber());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, FitnessDataWrapper fitnessDataWrapper) {
            bind(mi, fitnessDataWrapper);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR IGNORE INTO `fitness_data` (`step`,`activeMinute`,`calorie`,`distance`,`stress`,`resting`,`heartRate`,`sleeps`,`workouts`,`startTime`,`endTime`,`syncTime`,`timezoneOffsetInSecond`,`serialNumber`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Gh<FitnessDataWrapper> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, FitnessDataWrapper fitnessDataWrapper) {
            String a2 = FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.a(fitnessDataWrapper.getStartTime());
            if (a2 == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, a2);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Gh
        public /* bridge */ /* synthetic */ void bind(Mi mi, FitnessDataWrapper fitnessDataWrapper) {
            bind(mi, fitnessDataWrapper);
        }

        @DexIgnore
        @Override // com.mapped.Vh, com.mapped.Gh
        public String createQuery() {
            return "DELETE FROM `fitness_data` WHERE `startTime` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM fitness_data";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<FitnessDataWrapper>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon4(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<FitnessDataWrapper> call() throws Exception {
            Cursor b = Ex0.b(FitnessDataDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "step");
                int c2 = Dx0.c(b, "activeMinute");
                int c3 = Dx0.c(b, "calorie");
                int c4 = Dx0.c(b, "distance");
                int c5 = Dx0.c(b, "stress");
                int c6 = Dx0.c(b, "resting");
                int c7 = Dx0.c(b, "heartRate");
                int c8 = Dx0.c(b, "sleeps");
                int c9 = Dx0.c(b, "workouts");
                int c10 = Dx0.c(b, SampleRaw.COLUMN_START_TIME);
                int c11 = Dx0.c(b, SampleRaw.COLUMN_END_TIME);
                int c12 = Dx0.c(b, "syncTime");
                int c13 = Dx0.c(b, "timezoneOffsetInSecond");
                int c14 = Dx0.c(b, "serialNumber");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.b(b.getString(c10)), FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.b(b.getString(c11)), FitnessDataDao_Impl.this.__dateTimeUTCStringConverter.b(b.getString(c12)), b.getInt(c13), b.getString(c14));
                    fitnessDataWrapper.step = FitnessDataDao_Impl.this.__fitnessDataConverter.o(b.getString(c));
                    fitnessDataWrapper.activeMinute = FitnessDataDao_Impl.this.__fitnessDataConverter.i(b.getString(c2));
                    fitnessDataWrapper.calorie = FitnessDataDao_Impl.this.__fitnessDataConverter.j(b.getString(c3));
                    fitnessDataWrapper.distance = FitnessDataDao_Impl.this.__fitnessDataConverter.k(b.getString(c4));
                    fitnessDataWrapper.setStress(FitnessDataDao_Impl.this.__fitnessDataConverter.p(b.getString(c5)));
                    fitnessDataWrapper.setResting(FitnessDataDao_Impl.this.__fitnessDataConverter.m(b.getString(c6)));
                    fitnessDataWrapper.setHeartRate(FitnessDataDao_Impl.this.__fitnessDataConverter.l(b.getString(c7)));
                    fitnessDataWrapper.setSleeps(FitnessDataDao_Impl.this.__fitnessDataConverter.n(b.getString(c8)));
                    fitnessDataWrapper.setWorkouts(FitnessDataDao_Impl.this.__fitnessDataConverter.q(b.getString(c9)));
                    arrayList.add(fitnessDataWrapper);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public FitnessDataDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfFitnessDataWrapper = new Anon1(oh);
        this.__deletionAdapterOfFitnessDataWrapper = new Anon2(oh);
        this.__preparedStmtOfDeleteAllFitnessData = new Anon3(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public void deleteAllFitnessData() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteAllFitnessData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllFitnessData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public void deleteFitnessData(List<FitnessDataWrapper> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfFitnessDataWrapper.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public List<FitnessDataWrapper> getListFitnessData(DateTime dateTime, DateTime dateTime2) {
        Rh f = Rh.f("SELECT * FROM fitness_data WHERE startTime >= ? AND startTime <= ? ORDER BY startTime ASC", 2);
        String a2 = this.__dateTimeUTCStringConverter.a(dateTime);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateTimeUTCStringConverter.a(dateTime2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "step");
            int c2 = Dx0.c(b, "activeMinute");
            int c3 = Dx0.c(b, "calorie");
            int c4 = Dx0.c(b, "distance");
            int c5 = Dx0.c(b, "stress");
            int c6 = Dx0.c(b, "resting");
            int c7 = Dx0.c(b, "heartRate");
            int c8 = Dx0.c(b, "sleeps");
            int c9 = Dx0.c(b, "workouts");
            int c10 = Dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c11 = Dx0.c(b, SampleRaw.COLUMN_END_TIME);
            int c12 = Dx0.c(b, "syncTime");
            int c13 = Dx0.c(b, "timezoneOffsetInSecond");
            try {
                int c14 = Dx0.c(b, "serialNumber");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(this.__dateTimeUTCStringConverter.b(b.getString(c10)), this.__dateTimeUTCStringConverter.b(b.getString(c11)), this.__dateTimeUTCStringConverter.b(b.getString(c12)), b.getInt(c13), b.getString(c14));
                    fitnessDataWrapper.step = this.__fitnessDataConverter.o(b.getString(c));
                    fitnessDataWrapper.activeMinute = this.__fitnessDataConverter.i(b.getString(c2));
                    fitnessDataWrapper.calorie = this.__fitnessDataConverter.j(b.getString(c3));
                    fitnessDataWrapper.distance = this.__fitnessDataConverter.k(b.getString(c4));
                    fitnessDataWrapper.setStress(this.__fitnessDataConverter.p(b.getString(c5)));
                    fitnessDataWrapper.setResting(this.__fitnessDataConverter.m(b.getString(c6)));
                    fitnessDataWrapper.setHeartRate(this.__fitnessDataConverter.l(b.getString(c7)));
                    fitnessDataWrapper.setSleeps(this.__fitnessDataConverter.n(b.getString(c8)));
                    fitnessDataWrapper.setWorkouts(this.__fitnessDataConverter.q(b.getString(c9)));
                    arrayList.add(fitnessDataWrapper);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public LiveData<List<FitnessDataWrapper>> getListFitnessDataLiveData(DateTime dateTime, DateTime dateTime2) {
        Rh f = Rh.f("SELECT * FROM fitness_data WHERE startTime >= ? AND startTime <= ? ORDER BY startTime ASC", 2);
        String a2 = this.__dateTimeUTCStringConverter.a(dateTime);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateTimeUTCStringConverter.a(dateTime2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"fitness_data"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public List<FitnessDataWrapper> getPendingFitnessData() {
        Rh f = Rh.f("SELECT * FROM fitness_data ORDER BY startTime ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "step");
            int c2 = Dx0.c(b, "activeMinute");
            int c3 = Dx0.c(b, "calorie");
            int c4 = Dx0.c(b, "distance");
            int c5 = Dx0.c(b, "stress");
            int c6 = Dx0.c(b, "resting");
            int c7 = Dx0.c(b, "heartRate");
            int c8 = Dx0.c(b, "sleeps");
            int c9 = Dx0.c(b, "workouts");
            int c10 = Dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c11 = Dx0.c(b, SampleRaw.COLUMN_END_TIME);
            int c12 = Dx0.c(b, "syncTime");
            int c13 = Dx0.c(b, "timezoneOffsetInSecond");
            try {
                int c14 = Dx0.c(b, "serialNumber");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    FitnessDataWrapper fitnessDataWrapper = new FitnessDataWrapper(this.__dateTimeUTCStringConverter.b(b.getString(c10)), this.__dateTimeUTCStringConverter.b(b.getString(c11)), this.__dateTimeUTCStringConverter.b(b.getString(c12)), b.getInt(c13), b.getString(c14));
                    fitnessDataWrapper.step = this.__fitnessDataConverter.o(b.getString(c));
                    fitnessDataWrapper.activeMinute = this.__fitnessDataConverter.i(b.getString(c2));
                    fitnessDataWrapper.calorie = this.__fitnessDataConverter.j(b.getString(c3));
                    fitnessDataWrapper.distance = this.__fitnessDataConverter.k(b.getString(c4));
                    fitnessDataWrapper.setStress(this.__fitnessDataConverter.p(b.getString(c5)));
                    fitnessDataWrapper.setResting(this.__fitnessDataConverter.m(b.getString(c6)));
                    fitnessDataWrapper.setHeartRate(this.__fitnessDataConverter.l(b.getString(c7)));
                    fitnessDataWrapper.setSleeps(this.__fitnessDataConverter.n(b.getString(c8)));
                    fitnessDataWrapper.setWorkouts(this.__fitnessDataConverter.q(b.getString(c9)));
                    arrayList.add(fitnessDataWrapper);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FitnessDataDao
    public void insertFitnessDataList(List<FitnessDataWrapper> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfFitnessDataWrapper.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
