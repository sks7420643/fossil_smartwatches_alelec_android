package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRemote;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesWFBackgroundPhotoRemoteFactory implements Factory<WFBackgroundPhotoRemote> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesWFBackgroundPhotoRemoteFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        this.module = portfolioDatabaseModule;
        this.apiProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesWFBackgroundPhotoRemoteFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        return new PortfolioDatabaseModule_ProvidesWFBackgroundPhotoRemoteFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static WFBackgroundPhotoRemote providesWFBackgroundPhotoRemote(PortfolioDatabaseModule portfolioDatabaseModule, ApiServiceV2 apiServiceV2) {
        WFBackgroundPhotoRemote providesWFBackgroundPhotoRemote = portfolioDatabaseModule.providesWFBackgroundPhotoRemote(apiServiceV2);
        Lk7.c(providesWFBackgroundPhotoRemote, "Cannot return null from a non-@Nullable @Provides method");
        return providesWFBackgroundPhotoRemote;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WFBackgroundPhotoRemote get() {
        return providesWFBackgroundPhotoRemote(this.module, this.apiProvider.get());
    }
}
