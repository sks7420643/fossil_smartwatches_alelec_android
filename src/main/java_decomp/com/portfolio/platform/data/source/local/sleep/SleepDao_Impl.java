package com.portfolio.platform.data.source.local.sleep;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.C05;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Hx0;
import com.fossil.J05;
import com.fossil.K05;
import com.fossil.L05;
import com.fossil.Nw0;
import com.fossil.Qz4;
import com.fossil.Rz4;
import com.fossil.Zi0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDao_Impl extends SleepDao {
    @DexIgnore
    public /* final */ Qz4 __dateShortStringConverter; // = new Qz4();
    @DexIgnore
    public /* final */ Rz4 __dateTimeConverter; // = new Rz4();
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<MFSleepDay> __insertionAdapterOfMFSleepDay;
    @DexIgnore
    public /* final */ Hh<MFSleepSession> __insertionAdapterOfMFSleepSession;
    @DexIgnore
    public /* final */ Hh<MFSleepSession> __insertionAdapterOfMFSleepSession_1;
    @DexIgnore
    public /* final */ Hh<MFSleepSettings> __insertionAdapterOfMFSleepSettings;
    @DexIgnore
    public /* final */ Hh<SleepRecommendedGoal> __insertionAdapterOfSleepRecommendedGoal;
    @DexIgnore
    public /* final */ Hh<SleepStatistic> __insertionAdapterOfSleepStatistic;
    @DexIgnore
    public /* final */ C05 __integerArrayConverter; // = new C05();
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteAllSleepDays;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteAllSleepSessions;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfUpdateSleepSettings;
    @DexIgnore
    public /* final */ J05 __sleepDistributionConverter; // = new J05();
    @DexIgnore
    public /* final */ K05 __sleepSessionHeartRateConverter; // = new K05();
    @DexIgnore
    public /* final */ L05 __sleepStatisticConverter; // = new L05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<MFSleepSession> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, MFSleepSession mFSleepSession) {
            mi.bindLong(1, (long) mFSleepSession.getPinType());
            mi.bindLong(2, mFSleepSession.getDate());
            String a2 = SleepDao_Impl.this.__dateShortStringConverter.a(mFSleepSession.getDay());
            if (a2 == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, a2);
            }
            if (mFSleepSession.getDeviceSerialNumber() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, mFSleepSession.getDeviceSerialNumber());
            }
            if (mFSleepSession.getSyncTime() == null) {
                mi.bindNull(5);
            } else {
                mi.bindLong(5, (long) mFSleepSession.getSyncTime().intValue());
            }
            if (mFSleepSession.getBookmarkTime() == null) {
                mi.bindNull(6);
            } else {
                mi.bindLong(6, (long) mFSleepSession.getBookmarkTime().intValue());
            }
            mi.bindDouble(7, mFSleepSession.getNormalizedSleepQuality());
            mi.bindLong(8, (long) mFSleepSession.getSource());
            mi.bindLong(9, (long) mFSleepSession.getRealStartTime());
            mi.bindLong(10, (long) mFSleepSession.getRealEndTime());
            mi.bindLong(11, (long) mFSleepSession.getRealSleepMinutes());
            String b = SleepDao_Impl.this.__sleepDistributionConverter.b(mFSleepSession.getRealSleepStateDistInMinute());
            if (b == null) {
                mi.bindNull(12);
            } else {
                mi.bindString(12, b);
            }
            if (mFSleepSession.getEditedStartTime() == null) {
                mi.bindNull(13);
            } else {
                mi.bindLong(13, (long) mFSleepSession.getEditedStartTime().intValue());
            }
            if (mFSleepSession.getEditedEndTime() == null) {
                mi.bindNull(14);
            } else {
                mi.bindLong(14, (long) mFSleepSession.getEditedEndTime().intValue());
            }
            if (mFSleepSession.getEditedSleepMinutes() == null) {
                mi.bindNull(15);
            } else {
                mi.bindLong(15, (long) mFSleepSession.getEditedSleepMinutes().intValue());
            }
            String b2 = SleepDao_Impl.this.__sleepDistributionConverter.b(mFSleepSession.getEditedSleepStateDistInMinute());
            if (b2 == null) {
                mi.bindNull(16);
            } else {
                mi.bindString(16, b2);
            }
            if (mFSleepSession.getSleepStates() == null) {
                mi.bindNull(17);
            } else {
                mi.bindString(17, mFSleepSession.getSleepStates());
            }
            String b3 = SleepDao_Impl.this.__sleepSessionHeartRateConverter.b(mFSleepSession.getHeartRate());
            if (b3 == null) {
                mi.bindNull(18);
            } else {
                mi.bindString(18, b3);
            }
            mi.bindLong(19, SleepDao_Impl.this.__dateTimeConverter.b(mFSleepSession.getCreatedAt()));
            mi.bindLong(20, SleepDao_Impl.this.__dateTimeConverter.b(mFSleepSession.getUpdatedAt()));
            mi.bindLong(21, (long) mFSleepSession.getTimezoneOffset());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, MFSleepSession mFSleepSession) {
            bind(mi, mFSleepSession);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR IGNORE INTO `sleep_session` (`pinType`,`date`,`day`,`deviceSerialNumber`,`syncTime`,`bookmarkTime`,`normalizedSleepQuality`,`source`,`realStartTime`,`realEndTime`,`realSleepMinutes`,`realSleepStateDistInMinute`,`editedStartTime`,`editedEndTime`,`editedSleepMinutes`,`editedSleepStateDistInMinute`,`sleepStates`,`heartRate`,`createdAt`,`updatedAt`,`timezoneOffset`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<List<MFSleepSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon10(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<MFSleepSession> call() throws Exception {
            Cursor b = Ex0.b(SleepDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "pinType");
                int c2 = Dx0.c(b, "date");
                int c3 = Dx0.c(b, "day");
                int c4 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
                int c5 = Dx0.c(b, "syncTime");
                int c6 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
                int c7 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
                int c8 = Dx0.c(b, "source");
                int c9 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
                int c10 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
                int c11 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
                int c12 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
                int c13 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
                int c14 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int c15 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int c16 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int c17 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int c18 = Dx0.c(b, "heartRate");
                int c19 = Dx0.c(b, "createdAt");
                int c20 = Dx0.c(b, "updatedAt");
                int c21 = Dx0.c(b, "timezoneOffset");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    MFSleepSession mFSleepSession = new MFSleepSession(b.getLong(c2), SleepDao_Impl.this.__dateShortStringConverter.b(b.getString(c3)), b.getString(c4), b.isNull(c5) ? null : Integer.valueOf(b.getInt(c5)), b.isNull(c6) ? null : Integer.valueOf(b.getInt(c6)), b.getDouble(c7), b.getInt(c8), b.getInt(c9), b.getInt(c10), b.getInt(c11), SleepDao_Impl.this.__sleepDistributionConverter.a(b.getString(c12)), b.isNull(c13) ? null : Integer.valueOf(b.getInt(c13)), b.isNull(c14) ? null : Integer.valueOf(b.getInt(c14)), b.isNull(c15) ? null : Integer.valueOf(b.getInt(c15)), SleepDao_Impl.this.__sleepDistributionConverter.a(b.getString(c16)), b.getString(c17), SleepDao_Impl.this.__sleepSessionHeartRateConverter.a(b.getString(c18)), SleepDao_Impl.this.__dateTimeConverter.a(b.getLong(c19)), SleepDao_Impl.this.__dateTimeConverter.a(b.getLong(c20)), b.getInt(c21));
                    mFSleepSession.setPinType(b.getInt(c));
                    arrayList.add(mFSleepSession);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon11 implements Callable<MFSleepDay> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon11(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public MFSleepDay call() throws Exception {
            MFSleepDay mFSleepDay = null;
            Cursor b = Ex0.b(SleepDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "pinType");
                int c2 = Dx0.c(b, "timezoneOffset");
                int c3 = Dx0.c(b, "date");
                int c4 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
                int c5 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
                int c6 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
                int c7 = Dx0.c(b, "createdAt");
                int c8 = Dx0.c(b, "updatedAt");
                if (b.moveToFirst()) {
                    mFSleepDay = new MFSleepDay(SleepDao_Impl.this.__dateShortStringConverter.b(b.getString(c3)), b.getInt(c4), b.getInt(c5), SleepDao_Impl.this.__sleepDistributionConverter.a(b.getString(c6)), SleepDao_Impl.this.__dateTimeConverter.a(b.getLong(c7)), SleepDao_Impl.this.__dateTimeConverter.a(b.getLong(c8)));
                    mFSleepDay.setPinType(b.getInt(c));
                    mFSleepDay.setTimezoneOffset(b.getInt(c2));
                }
                return mFSleepDay;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon12 implements Callable<List<MFSleepDay>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon12(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<MFSleepDay> call() throws Exception {
            Cursor b = Ex0.b(SleepDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "pinType");
                int c2 = Dx0.c(b, "timezoneOffset");
                int c3 = Dx0.c(b, "date");
                int c4 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
                int c5 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
                int c6 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
                int c7 = Dx0.c(b, "createdAt");
                int c8 = Dx0.c(b, "updatedAt");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    MFSleepDay mFSleepDay = new MFSleepDay(SleepDao_Impl.this.__dateShortStringConverter.b(b.getString(c3)), b.getInt(c4), b.getInt(c5), SleepDao_Impl.this.__sleepDistributionConverter.a(b.getString(c6)), SleepDao_Impl.this.__dateTimeConverter.a(b.getLong(c7)), SleepDao_Impl.this.__dateTimeConverter.a(b.getLong(c8)));
                    mFSleepDay.setPinType(b.getInt(c));
                    mFSleepDay.setTimezoneOffset(b.getInt(c2));
                    arrayList.add(mFSleepDay);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon13 implements Callable<SleepRecommendedGoal> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon13(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public SleepRecommendedGoal call() throws Exception {
            SleepRecommendedGoal sleepRecommendedGoal = null;
            Cursor b = Ex0.b(SleepDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "id");
                int c2 = Dx0.c(b, "recommendedSleepGoal");
                if (b.moveToFirst()) {
                    sleepRecommendedGoal = new SleepRecommendedGoal(b.getInt(c2));
                    sleepRecommendedGoal.setId(b.getInt(c));
                }
                return sleepRecommendedGoal;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon14 implements Callable<SleepStatistic> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon14(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public SleepStatistic call() throws Exception {
            SleepStatistic sleepStatistic = null;
            Cursor b = Ex0.b(SleepDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "id");
                int c2 = Dx0.c(b, "uid");
                int c3 = Dx0.c(b, "sleepTimeBestDay");
                int c4 = Dx0.c(b, "sleepTimeBestStreak");
                int c5 = Dx0.c(b, "totalDays");
                int c6 = Dx0.c(b, "totalSleeps");
                int c7 = Dx0.c(b, "totalSleepMinutes");
                int c8 = Dx0.c(b, "totalSleepStateDistInMinute");
                int c9 = Dx0.c(b, "createdAt");
                int c10 = Dx0.c(b, "updatedAt");
                if (b.moveToFirst()) {
                    sleepStatistic = new SleepStatistic(b.getString(c), b.getString(c2), SleepDao_Impl.this.__sleepStatisticConverter.b(b.getString(c3)), SleepDao_Impl.this.__sleepStatisticConverter.b(b.getString(c4)), b.getInt(c5), b.getInt(c6), b.getInt(c7), SleepDao_Impl.this.__integerArrayConverter.a(b.getString(c8)), SleepDao_Impl.this.__dateTimeConverter.a(b.getLong(c9)), SleepDao_Impl.this.__dateTimeConverter.a(b.getLong(c10)));
                }
                return sleepStatistic;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Hh<MFSleepSession> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, MFSleepSession mFSleepSession) {
            mi.bindLong(1, (long) mFSleepSession.getPinType());
            mi.bindLong(2, mFSleepSession.getDate());
            String a2 = SleepDao_Impl.this.__dateShortStringConverter.a(mFSleepSession.getDay());
            if (a2 == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, a2);
            }
            if (mFSleepSession.getDeviceSerialNumber() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, mFSleepSession.getDeviceSerialNumber());
            }
            if (mFSleepSession.getSyncTime() == null) {
                mi.bindNull(5);
            } else {
                mi.bindLong(5, (long) mFSleepSession.getSyncTime().intValue());
            }
            if (mFSleepSession.getBookmarkTime() == null) {
                mi.bindNull(6);
            } else {
                mi.bindLong(6, (long) mFSleepSession.getBookmarkTime().intValue());
            }
            mi.bindDouble(7, mFSleepSession.getNormalizedSleepQuality());
            mi.bindLong(8, (long) mFSleepSession.getSource());
            mi.bindLong(9, (long) mFSleepSession.getRealStartTime());
            mi.bindLong(10, (long) mFSleepSession.getRealEndTime());
            mi.bindLong(11, (long) mFSleepSession.getRealSleepMinutes());
            String b = SleepDao_Impl.this.__sleepDistributionConverter.b(mFSleepSession.getRealSleepStateDistInMinute());
            if (b == null) {
                mi.bindNull(12);
            } else {
                mi.bindString(12, b);
            }
            if (mFSleepSession.getEditedStartTime() == null) {
                mi.bindNull(13);
            } else {
                mi.bindLong(13, (long) mFSleepSession.getEditedStartTime().intValue());
            }
            if (mFSleepSession.getEditedEndTime() == null) {
                mi.bindNull(14);
            } else {
                mi.bindLong(14, (long) mFSleepSession.getEditedEndTime().intValue());
            }
            if (mFSleepSession.getEditedSleepMinutes() == null) {
                mi.bindNull(15);
            } else {
                mi.bindLong(15, (long) mFSleepSession.getEditedSleepMinutes().intValue());
            }
            String b2 = SleepDao_Impl.this.__sleepDistributionConverter.b(mFSleepSession.getEditedSleepStateDistInMinute());
            if (b2 == null) {
                mi.bindNull(16);
            } else {
                mi.bindString(16, b2);
            }
            if (mFSleepSession.getSleepStates() == null) {
                mi.bindNull(17);
            } else {
                mi.bindString(17, mFSleepSession.getSleepStates());
            }
            String b3 = SleepDao_Impl.this.__sleepSessionHeartRateConverter.b(mFSleepSession.getHeartRate());
            if (b3 == null) {
                mi.bindNull(18);
            } else {
                mi.bindString(18, b3);
            }
            mi.bindLong(19, SleepDao_Impl.this.__dateTimeConverter.b(mFSleepSession.getCreatedAt()));
            mi.bindLong(20, SleepDao_Impl.this.__dateTimeConverter.b(mFSleepSession.getUpdatedAt()));
            mi.bindLong(21, (long) mFSleepSession.getTimezoneOffset());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, MFSleepSession mFSleepSession) {
            bind(mi, mFSleepSession);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleep_session` (`pinType`,`date`,`day`,`deviceSerialNumber`,`syncTime`,`bookmarkTime`,`normalizedSleepQuality`,`source`,`realStartTime`,`realEndTime`,`realSleepMinutes`,`realSleepStateDistInMinute`,`editedStartTime`,`editedEndTime`,`editedSleepMinutes`,`editedSleepStateDistInMinute`,`sleepStates`,`heartRate`,`createdAt`,`updatedAt`,`timezoneOffset`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Hh<MFSleepDay> {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, MFSleepDay mFSleepDay) {
            mi.bindLong(1, (long) mFSleepDay.getPinType());
            mi.bindLong(2, (long) mFSleepDay.getTimezoneOffset());
            String a2 = SleepDao_Impl.this.__dateShortStringConverter.a(mFSleepDay.getDate());
            if (a2 == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, a2);
            }
            mi.bindLong(4, (long) mFSleepDay.getGoalMinutes());
            mi.bindLong(5, (long) mFSleepDay.getSleepMinutes());
            String b = SleepDao_Impl.this.__sleepDistributionConverter.b(mFSleepDay.getSleepStateDistInMinute());
            if (b == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, b);
            }
            mi.bindLong(7, SleepDao_Impl.this.__dateTimeConverter.b(mFSleepDay.getCreatedAt()));
            mi.bindLong(8, SleepDao_Impl.this.__dateTimeConverter.b(mFSleepDay.getUpdatedAt()));
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, MFSleepDay mFSleepDay) {
            bind(mi, mFSleepDay);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleep_date` (`pinType`,`timezoneOffset`,`date`,`goalMinutes`,`sleepMinutes`,`sleepStateDistInMinute`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends Hh<MFSleepSettings> {
        @DexIgnore
        public Anon4(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, MFSleepSettings mFSleepSettings) {
            mi.bindLong(1, (long) mFSleepSettings.getId());
            mi.bindLong(2, (long) mFSleepSettings.getSleepGoal());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, MFSleepSettings mFSleepSettings) {
            bind(mi, mFSleepSettings);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR FAIL INTO `sleep_settings` (`id`,`sleepGoal`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends Hh<SleepRecommendedGoal> {
        @DexIgnore
        public Anon5(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, SleepRecommendedGoal sleepRecommendedGoal) {
            mi.bindLong(1, (long) sleepRecommendedGoal.getId());
            mi.bindLong(2, (long) sleepRecommendedGoal.getRecommendedSleepGoal());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, SleepRecommendedGoal sleepRecommendedGoal) {
            bind(mi, sleepRecommendedGoal);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleepRecommendedGoals` (`id`,`recommendedSleepGoal`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends Hh<SleepStatistic> {
        @DexIgnore
        public Anon6(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, SleepStatistic sleepStatistic) {
            if (sleepStatistic.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, sleepStatistic.getId());
            }
            if (sleepStatistic.getUid() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, sleepStatistic.getUid());
            }
            String a2 = SleepDao_Impl.this.__sleepStatisticConverter.a(sleepStatistic.getSleepTimeBestDay());
            if (a2 == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, a2);
            }
            String a3 = SleepDao_Impl.this.__sleepStatisticConverter.a(sleepStatistic.getSleepTimeBestStreak());
            if (a3 == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, a3);
            }
            mi.bindLong(5, (long) sleepStatistic.getTotalDays());
            mi.bindLong(6, (long) sleepStatistic.getTotalSleeps());
            mi.bindLong(7, (long) sleepStatistic.getTotalSleepMinutes());
            String b = SleepDao_Impl.this.__integerArrayConverter.b(sleepStatistic.getTotalSleepStateDistInMinute());
            if (b == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, b);
            }
            mi.bindLong(9, SleepDao_Impl.this.__dateTimeConverter.b(sleepStatistic.getCreatedAt()));
            mi.bindLong(10, SleepDao_Impl.this.__dateTimeConverter.b(sleepStatistic.getUpdatedAt()));
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, SleepStatistic sleepStatistic) {
            bind(mi, sleepStatistic);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sleep_statistic` (`id`,`uid`,`sleepTimeBestDay`,`sleepTimeBestStreak`,`totalDays`,`totalSleeps`,`totalSleepMinutes`,`totalSleepStateDistInMinute`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends Vh {
        @DexIgnore
        public Anon7(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM sleep_session";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 extends Vh {
        @DexIgnore
        public Anon8(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM sleep_date";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 extends Vh {
        @DexIgnore
        public Anon9(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE sleep_settings SET sleepGoal = ?";
        }
    }

    @DexIgnore
    public SleepDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfMFSleepSession = new Anon1(oh);
        this.__insertionAdapterOfMFSleepSession_1 = new Anon2(oh);
        this.__insertionAdapterOfMFSleepDay = new Anon3(oh);
        this.__insertionAdapterOfMFSleepSettings = new Anon4(oh);
        this.__insertionAdapterOfSleepRecommendedGoal = new Anon5(oh);
        this.__insertionAdapterOfSleepStatistic = new Anon6(oh);
        this.__preparedStmtOfDeleteAllSleepSessions = new Anon7(oh);
        this.__preparedStmtOfDeleteAllSleepDays = new Anon8(oh);
        this.__preparedStmtOfUpdateSleepSettings = new Anon9(oh);
    }

    @DexIgnore
    private void __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(Zi0<String, ArrayList<MFSleepSession>> zi0) {
        ArrayList<MFSleepSession> arrayList;
        int i;
        Set<String> keySet = zi0.keySet();
        if (!keySet.isEmpty()) {
            if (zi0.size() > 999) {
                Zi0<String, ArrayList<MFSleepSession>> zi02 = new Zi0<>(999);
                int size = zi0.size();
                int i2 = 0;
                loop0:
                while (true) {
                    i = 0;
                    while (i2 < size) {
                        zi02.put(zi0.j(i2), zi0.n(i2));
                        i2++;
                        i++;
                        if (i == 999) {
                            __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(zi02);
                            zi02 = new Zi0<>(999);
                        }
                    }
                    break loop0;
                }
                if (i > 0) {
                    __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(zi02);
                    return;
                }
                return;
            }
            StringBuilder b = Hx0.b();
            b.append("SELECT `pinType`,`date`,`day`,`deviceSerialNumber`,`syncTime`,`bookmarkTime`,`normalizedSleepQuality`,`source`,`realStartTime`,`realEndTime`,`realSleepMinutes`,`realSleepStateDistInMinute`,`editedStartTime`,`editedEndTime`,`editedSleepMinutes`,`editedSleepStateDistInMinute`,`sleepStates`,`heartRate`,`createdAt`,`updatedAt`,`timezoneOffset` FROM `sleep_session` WHERE `day` IN (");
            int size2 = keySet.size();
            Hx0.a(b, size2);
            b.append(")");
            Rh f = Rh.f(b.toString(), size2 + 0);
            int i3 = 1;
            for (String str : keySet) {
                if (str == null) {
                    f.bindNull(i3);
                } else {
                    f.bindString(i3, str);
                }
                i3++;
            }
            Cursor b2 = Ex0.b(this.__db, f, false, null);
            try {
                int b3 = Dx0.b(b2, "day");
                if (b3 != -1) {
                    int b4 = Dx0.b(b2, "pinType");
                    int b5 = Dx0.b(b2, "date");
                    int b6 = Dx0.b(b2, "day");
                    int b7 = Dx0.b(b2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
                    int b8 = Dx0.b(b2, "syncTime");
                    int b9 = Dx0.b(b2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
                    int b10 = Dx0.b(b2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
                    int b11 = Dx0.b(b2, "source");
                    int b12 = Dx0.b(b2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
                    int b13 = Dx0.b(b2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
                    int b14 = Dx0.b(b2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
                    int b15 = Dx0.b(b2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
                    int b16 = Dx0.b(b2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
                    int b17 = Dx0.b(b2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                    int b18 = Dx0.b(b2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                    int b19 = Dx0.b(b2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                    int b20 = Dx0.b(b2, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                    int b21 = Dx0.b(b2, "heartRate");
                    int b22 = Dx0.b(b2, "createdAt");
                    int b23 = Dx0.b(b2, "updatedAt");
                    int b24 = Dx0.b(b2, "timezoneOffset");
                    while (b2.moveToNext()) {
                        if (!b2.isNull(b3) && (arrayList = zi0.get(b2.getString(b3))) != null) {
                            MFSleepSession mFSleepSession = new MFSleepSession(b5 == -1 ? 0 : b2.getLong(b5), b6 == -1 ? null : this.__dateShortStringConverter.b(b2.getString(b6)), b7 == -1 ? null : b2.getString(b7), (b8 != -1 && !b2.isNull(b8)) ? Integer.valueOf(b2.getInt(b8)) : null, (b9 != -1 && !b2.isNull(b9)) ? Integer.valueOf(b2.getInt(b9)) : null, b10 == -1 ? 0.0d : b2.getDouble(b10), b11 == -1 ? 0 : b2.getInt(b11), b12 == -1 ? 0 : b2.getInt(b12), b13 == -1 ? 0 : b2.getInt(b13), b14 == -1 ? 0 : b2.getInt(b14), b15 == -1 ? null : this.__sleepDistributionConverter.a(b2.getString(b15)), (b16 != -1 && !b2.isNull(b16)) ? Integer.valueOf(b2.getInt(b16)) : null, (b17 != -1 && !b2.isNull(b17)) ? Integer.valueOf(b2.getInt(b17)) : null, (b18 != -1 && !b2.isNull(b18)) ? Integer.valueOf(b2.getInt(b18)) : null, b19 == -1 ? null : this.__sleepDistributionConverter.a(b2.getString(b19)), b20 == -1 ? null : b2.getString(b20), b21 == -1 ? null : this.__sleepSessionHeartRateConverter.a(b2.getString(b21)), b22 == -1 ? null : this.__dateTimeConverter.a(b2.getLong(b22)), b23 == -1 ? null : this.__dateTimeConverter.a(b2.getLong(b23)), b24 == -1 ? 0 : b2.getInt(b24));
                            if (b4 != -1) {
                                mFSleepSession.setPinType(b2.getInt(b4));
                            }
                            arrayList.add(mFSleepSession);
                        }
                        b24 = b24;
                    }
                    b2.close();
                }
            } finally {
                b2.close();
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void deleteAllSleepDays() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteAllSleepDays.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllSleepDays.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void deleteAllSleepSessions() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteAllSleepSessions.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllSleepSessions.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public MFSleepDay getLastSleepDay() {
        MFSleepDay mFSleepDay = null;
        Rh f = Rh.f("SELECT * FROM sleep_date ORDER BY date ASC LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "timezoneOffset");
            int c3 = Dx0.c(b, "date");
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int c5 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int c6 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int c7 = Dx0.c(b, "createdAt");
            int c8 = Dx0.c(b, "updatedAt");
            if (b.moveToFirst()) {
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.b(b.getString(c3)), b.getInt(c4), b.getInt(c5), this.__sleepDistributionConverter.a(b.getString(c6)), this.__dateTimeConverter.a(b.getLong(c7)), this.__dateTimeConverter.a(b.getLong(c8)));
                mFSleepDay.setPinType(b.getInt(c));
                mFSleepDay.setTimezoneOffset(b.getInt(c2));
            }
            return mFSleepDay;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public Integer getLastSleepGoal() {
        Integer num = null;
        Rh f = Rh.f("SELECT sleepGoal FROM sleep_settings LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            if (b.moveToFirst() && !b.isNull(0)) {
                num = Integer.valueOf(b.getInt(0));
            }
            return num;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public MFSleepDay getNearestSleepDayFromDate(String str) {
        MFSleepDay mFSleepDay = null;
        Rh f = Rh.f("SELECT * FROM sleep_date WHERE date <= ? ORDER BY date ASC LIMIT 1", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "timezoneOffset");
            int c3 = Dx0.c(b, "date");
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int c5 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int c6 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int c7 = Dx0.c(b, "createdAt");
            int c8 = Dx0.c(b, "updatedAt");
            if (b.moveToFirst()) {
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.b(b.getString(c3)), b.getInt(c4), b.getInt(c5), this.__sleepDistributionConverter.a(b.getString(c6)), this.__dateTimeConverter.a(b.getLong(c7)), this.__dateTimeConverter.a(b.getLong(c8)));
                mFSleepDay.setPinType(b.getInt(c));
                mFSleepDay.setTimezoneOffset(b.getInt(c2));
            }
            return mFSleepDay;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public List<MFSleepSession> getPendingSleepSessions() {
        Rh f = Rh.f("SELECT * FROM sleep_session WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "date");
            int c3 = Dx0.c(b, "day");
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int c5 = Dx0.c(b, "syncTime");
            int c6 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int c7 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int c8 = Dx0.c(b, "source");
            int c9 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int c10 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int c11 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int c12 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int c13 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            try {
                int c14 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int c15 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int c16 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int c17 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int c18 = Dx0.c(b, "heartRate");
                int c19 = Dx0.c(b, "createdAt");
                int c20 = Dx0.c(b, "updatedAt");
                int c21 = Dx0.c(b, "timezoneOffset");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    MFSleepSession mFSleepSession = new MFSleepSession(b.getLong(c2), this.__dateShortStringConverter.b(b.getString(c3)), b.getString(c4), b.isNull(c5) ? null : Integer.valueOf(b.getInt(c5)), b.isNull(c6) ? null : Integer.valueOf(b.getInt(c6)), b.getDouble(c7), b.getInt(c8), b.getInt(c9), b.getInt(c10), b.getInt(c11), this.__sleepDistributionConverter.a(b.getString(c12)), b.isNull(c13) ? null : Integer.valueOf(b.getInt(c13)), b.isNull(c14) ? null : Integer.valueOf(b.getInt(c14)), b.isNull(c15) ? null : Integer.valueOf(b.getInt(c15)), this.__sleepDistributionConverter.a(b.getString(c16)), b.getString(c17), this.__sleepSessionHeartRateConverter.a(b.getString(c18)), this.__dateTimeConverter.a(b.getLong(c19)), this.__dateTimeConverter.a(b.getLong(c20)), b.getInt(c21));
                    mFSleepSession.setPinType(b.getInt(c));
                    arrayList.add(mFSleepSession);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public List<MFSleepSession> getPendingSleepSessions(long j, long j2) {
        Rh f = Rh.f("SELECT * FROM sleep_session WHERE date >= ? AND date <= ? AND pinType <> 0 ORDER BY editedStartTime ASC", 2);
        f.bindLong(1, j);
        f.bindLong(2, j2);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "date");
            int c3 = Dx0.c(b, "day");
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int c5 = Dx0.c(b, "syncTime");
            int c6 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int c7 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int c8 = Dx0.c(b, "source");
            int c9 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int c10 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int c11 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int c12 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int c13 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            try {
                int c14 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int c15 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int c16 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int c17 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int c18 = Dx0.c(b, "heartRate");
                int c19 = Dx0.c(b, "createdAt");
                int c20 = Dx0.c(b, "updatedAt");
                int c21 = Dx0.c(b, "timezoneOffset");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    MFSleepSession mFSleepSession = new MFSleepSession(b.getLong(c2), this.__dateShortStringConverter.b(b.getString(c3)), b.getString(c4), b.isNull(c5) ? null : Integer.valueOf(b.getInt(c5)), b.isNull(c6) ? null : Integer.valueOf(b.getInt(c6)), b.getDouble(c7), b.getInt(c8), b.getInt(c9), b.getInt(c10), b.getInt(c11), this.__sleepDistributionConverter.a(b.getString(c12)), b.isNull(c13) ? null : Integer.valueOf(b.getInt(c13)), b.isNull(c14) ? null : Integer.valueOf(b.getInt(c14)), b.isNull(c15) ? null : Integer.valueOf(b.getInt(c15)), this.__sleepDistributionConverter.a(b.getString(c16)), b.getString(c17), this.__sleepSessionHeartRateConverter.a(b.getString(c18)), this.__dateTimeConverter.a(b.getLong(c19)), this.__dateTimeConverter.a(b.getLong(c20)), b.getInt(c21));
                    mFSleepSession.setPinType(b.getInt(c));
                    arrayList.add(mFSleepSession);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public MFSleepDay getSleepDay(String str) {
        MFSleepDay mFSleepDay = null;
        Rh f = Rh.f("SELECT * FROM sleep_date WHERE date == ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "timezoneOffset");
            int c3 = Dx0.c(b, "date");
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int c5 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int c6 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int c7 = Dx0.c(b, "createdAt");
            int c8 = Dx0.c(b, "updatedAt");
            if (b.moveToFirst()) {
                mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.b(b.getString(c3)), b.getInt(c4), b.getInt(c5), this.__sleepDistributionConverter.a(b.getString(c6)), this.__dateTimeConverter.a(b.getLong(c7)), this.__dateTimeConverter.a(b.getLong(c8)));
                mFSleepDay.setPinType(b.getInt(c));
                mFSleepDay.setTimezoneOffset(b.getInt(c2));
            }
            return mFSleepDay;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public LiveData<MFSleepDay> getSleepDayLiveData(String str) {
        Rh f = Rh.f("SELECT * FROM sleep_date WHERE date == ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon11 anon11 = new Anon11(f);
        return invalidationTracker.d(new String[]{com.fossil.wearables.fsl.sleep.MFSleepDay.TABLE_NAME}, false, anon11);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public List<MFSleepDay> getSleepDays(String str, String str2) {
        Rh f = Rh.f("SELECT * FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        if (str2 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "timezoneOffset");
            int c3 = Dx0.c(b, "date");
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int c5 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int c6 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int c7 = Dx0.c(b, "createdAt");
            int c8 = Dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                MFSleepDay mFSleepDay = new MFSleepDay(this.__dateShortStringConverter.b(b.getString(c3)), b.getInt(c4), b.getInt(c5), this.__sleepDistributionConverter.a(b.getString(c6)), this.__dateTimeConverter.a(b.getLong(c7)), this.__dateTimeConverter.a(b.getLong(c8)));
                mFSleepDay.setPinType(b.getInt(c));
                mFSleepDay.setTimezoneOffset(b.getInt(c2));
                arrayList.add(mFSleepDay);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public LiveData<List<MFSleepDay>> getSleepDaysLiveData(String str, String str2) {
        Rh f = Rh.f("SELECT * FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        if (str2 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, str2);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon12 anon12 = new Anon12(f);
        return invalidationTracker.d(new String[]{com.fossil.wearables.fsl.sleep.MFSleepDay.TABLE_NAME}, false, anon12);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public LiveData<SleepRecommendedGoal> getSleepRecommendedGoalLiveData() {
        Rh f = Rh.f("SELECT * FROM sleepRecommendedGoals LIMIT 1", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon13 anon13 = new Anon13(f);
        return invalidationTracker.d(new String[]{"sleepRecommendedGoals"}, false, anon13);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public MFSleepSession getSleepSession(long j) {
        Throwable th;
        MFSleepSession mFSleepSession;
        Rh f = Rh.f("SELECT * FROM sleep_session WHERE realEndTime = ?", 1);
        f.bindLong(1, j);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "date");
            int c3 = Dx0.c(b, "day");
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int c5 = Dx0.c(b, "syncTime");
            int c6 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int c7 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int c8 = Dx0.c(b, "source");
            int c9 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int c10 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int c11 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int c12 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int c13 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            try {
                int c14 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int c15 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int c16 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int c17 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int c18 = Dx0.c(b, "heartRate");
                int c19 = Dx0.c(b, "createdAt");
                int c20 = Dx0.c(b, "updatedAt");
                int c21 = Dx0.c(b, "timezoneOffset");
                if (b.moveToFirst()) {
                    mFSleepSession = new MFSleepSession(b.getLong(c2), this.__dateShortStringConverter.b(b.getString(c3)), b.getString(c4), b.isNull(c5) ? null : Integer.valueOf(b.getInt(c5)), b.isNull(c6) ? null : Integer.valueOf(b.getInt(c6)), b.getDouble(c7), b.getInt(c8), b.getInt(c9), b.getInt(c10), b.getInt(c11), this.__sleepDistributionConverter.a(b.getString(c12)), b.isNull(c13) ? null : Integer.valueOf(b.getInt(c13)), b.isNull(c14) ? null : Integer.valueOf(b.getInt(c14)), b.isNull(c15) ? null : Integer.valueOf(b.getInt(c15)), this.__sleepDistributionConverter.a(b.getString(c16)), b.getString(c17), this.__sleepSessionHeartRateConverter.a(b.getString(c18)), this.__dateTimeConverter.a(b.getLong(c19)), this.__dateTimeConverter.a(b.getLong(c20)), b.getInt(c21));
                    mFSleepSession.setPinType(b.getInt(c));
                } else {
                    mFSleepSession = null;
                }
                b.close();
                f.m();
                return mFSleepSession;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public List<MFSleepSession> getSleepSessions(long j, long j2) {
        Rh f = Rh.f("SELECT * FROM sleep_session WHERE date >= ? AND date <= ? ORDER BY editedStartTime ASC", 2);
        f.bindLong(1, j);
        f.bindLong(2, j2);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "date");
            int c3 = Dx0.c(b, "day");
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int c5 = Dx0.c(b, "syncTime");
            int c6 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int c7 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int c8 = Dx0.c(b, "source");
            int c9 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int c10 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int c11 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int c12 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int c13 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            try {
                int c14 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int c15 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int c16 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int c17 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int c18 = Dx0.c(b, "heartRate");
                int c19 = Dx0.c(b, "createdAt");
                int c20 = Dx0.c(b, "updatedAt");
                int c21 = Dx0.c(b, "timezoneOffset");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    MFSleepSession mFSleepSession = new MFSleepSession(b.getLong(c2), this.__dateShortStringConverter.b(b.getString(c3)), b.getString(c4), b.isNull(c5) ? null : Integer.valueOf(b.getInt(c5)), b.isNull(c6) ? null : Integer.valueOf(b.getInt(c6)), b.getDouble(c7), b.getInt(c8), b.getInt(c9), b.getInt(c10), b.getInt(c11), this.__sleepDistributionConverter.a(b.getString(c12)), b.isNull(c13) ? null : Integer.valueOf(b.getInt(c13)), b.isNull(c14) ? null : Integer.valueOf(b.getInt(c14)), b.isNull(c15) ? null : Integer.valueOf(b.getInt(c15)), this.__sleepDistributionConverter.a(b.getString(c16)), b.getString(c17), this.__sleepSessionHeartRateConverter.a(b.getString(c18)), this.__dateTimeConverter.a(b.getLong(c19)), this.__dateTimeConverter.a(b.getLong(c20)), b.getInt(c21));
                    mFSleepSession.setPinType(b.getInt(c));
                    arrayList.add(mFSleepSession);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public List<MFSleepSession> getSleepSessions(String str) {
        Rh f = Rh.f("SELECT * FROM sleep_session WHERE day = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "date");
            int c3 = Dx0.c(b, "day");
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER);
            int c5 = Dx0.c(b, "syncTime");
            int c6 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_BOOKMARK_TIME);
            int c7 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_NORMALIZED_SLEEP_QUALITY);
            int c8 = Dx0.c(b, "source");
            int c9 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_START_TIME);
            int c10 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_END_TIME);
            int c11 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_SLEEP_MINUTES);
            int c12 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_REAL_STATE_DIST_IN_MINUTE);
            int c13 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_START_TIME);
            try {
                int c14 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_END_TIME);
                int c15 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_SLEEP_MINUTES);
                int c16 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_EDITED_STATE_DIST_IN_MINUTE);
                int c17 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepSession.COLUMN_SLEEP_STATES);
                int c18 = Dx0.c(b, "heartRate");
                int c19 = Dx0.c(b, "createdAt");
                int c20 = Dx0.c(b, "updatedAt");
                int c21 = Dx0.c(b, "timezoneOffset");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    MFSleepSession mFSleepSession = new MFSleepSession(b.getLong(c2), this.__dateShortStringConverter.b(b.getString(c3)), b.getString(c4), b.isNull(c5) ? null : Integer.valueOf(b.getInt(c5)), b.isNull(c6) ? null : Integer.valueOf(b.getInt(c6)), b.getDouble(c7), b.getInt(c8), b.getInt(c9), b.getInt(c10), b.getInt(c11), this.__sleepDistributionConverter.a(b.getString(c12)), b.isNull(c13) ? null : Integer.valueOf(b.getInt(c13)), b.isNull(c14) ? null : Integer.valueOf(b.getInt(c14)), b.isNull(c15) ? null : Integer.valueOf(b.getInt(c15)), this.__sleepDistributionConverter.a(b.getString(c16)), b.getString(c17), this.__sleepSessionHeartRateConverter.a(b.getString(c18)), this.__dateTimeConverter.a(b.getLong(c19)), this.__dateTimeConverter.a(b.getLong(c20)), b.getInt(c21));
                    mFSleepSession.setPinType(b.getInt(c));
                    arrayList.add(mFSleepSession);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public LiveData<List<MFSleepSession>> getSleepSessionsLiveData(long j, long j2) {
        Rh f = Rh.f("SELECT * FROM sleep_session WHERE date >= ? AND date <= ? ORDER BY editedStartTime ASC", 2);
        f.bindLong(1, j);
        f.bindLong(2, j2);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon10 anon10 = new Anon10(f);
        return invalidationTracker.d(new String[]{com.fossil.wearables.fsl.sleep.MFSleepSession.TABLE_NAME}, false, anon10);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public MFSleepSettings getSleepSettings() {
        MFSleepSettings mFSleepSettings = null;
        Rh f = Rh.f("SELECT * FROM sleep_settings LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "sleepGoal");
            if (b.moveToFirst()) {
                mFSleepSettings = new MFSleepSettings(b.getInt(c2));
                mFSleepSettings.setId(b.getInt(c));
            }
            return mFSleepSettings;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public SleepStatistic getSleepStatistic() {
        Rh f = Rh.f("SELECT * FROM sleep_statistic LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        SleepStatistic sleepStatistic = null;
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "uid");
            int c3 = Dx0.c(b, "sleepTimeBestDay");
            int c4 = Dx0.c(b, "sleepTimeBestStreak");
            int c5 = Dx0.c(b, "totalDays");
            int c6 = Dx0.c(b, "totalSleeps");
            int c7 = Dx0.c(b, "totalSleepMinutes");
            int c8 = Dx0.c(b, "totalSleepStateDistInMinute");
            int c9 = Dx0.c(b, "createdAt");
            int c10 = Dx0.c(b, "updatedAt");
            if (b.moveToFirst()) {
                sleepStatistic = new SleepStatistic(b.getString(c), b.getString(c2), this.__sleepStatisticConverter.b(b.getString(c3)), this.__sleepStatisticConverter.b(b.getString(c4)), b.getInt(c5), b.getInt(c6), b.getInt(c7), this.__integerArrayConverter.a(b.getString(c8)), this.__dateTimeConverter.a(b.getLong(c9)), this.__dateTimeConverter.a(b.getLong(c10)));
            }
            return sleepStatistic;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public LiveData<SleepStatistic> getSleepStatisticLiveData() {
        Rh f = Rh.f("SELECT * FROM sleep_statistic LIMIT 1", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon14 anon14 = new Anon14(f);
        return invalidationTracker.d(new String[]{SleepStatistic.TABLE_NAME}, false, anon14);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public List<SleepSummary> getSleepSummariesDesc(String str, String str2) {
        MFSleepDay mFSleepDay;
        Rh f = Rh.f("SELECT * FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date DESC", 2);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        if (str2 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, true, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "timezoneOffset");
            int c3 = Dx0.c(b, "date");
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int c5 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int c6 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int c7 = Dx0.c(b, "createdAt");
            int c8 = Dx0.c(b, "updatedAt");
            Zi0<String, ArrayList<MFSleepSession>> zi0 = new Zi0<>();
            while (b.moveToNext()) {
                if (!b.isNull(c3)) {
                    String string = b.getString(c3);
                    if (zi0.get(string) == null) {
                        zi0.put(string, new ArrayList<>());
                    }
                }
            }
            b.moveToPosition(-1);
            __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(zi0);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                if (!b.isNull(c) || !b.isNull(c2) || !b.isNull(c3) || !b.isNull(c4) || !b.isNull(c5) || !b.isNull(c6) || !b.isNull(c7) || !b.isNull(c8)) {
                    MFSleepDay mFSleepDay2 = new MFSleepDay(this.__dateShortStringConverter.b(b.getString(c3)), b.getInt(c4), b.getInt(c5), this.__sleepDistributionConverter.a(b.getString(c6)), this.__dateTimeConverter.a(b.getLong(c7)), this.__dateTimeConverter.a(b.getLong(c8)));
                    mFSleepDay2.setPinType(b.getInt(c));
                    mFSleepDay2.setTimezoneOffset(b.getInt(c2));
                    mFSleepDay = mFSleepDay2;
                } else {
                    mFSleepDay = null;
                }
                ArrayList<MFSleepSession> arrayList2 = !b.isNull(c3) ? zi0.get(b.getString(c3)) : null;
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList<>();
                }
                arrayList.add(new SleepSummary(mFSleepDay, arrayList2));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public SleepSummary getSleepSummary(String str) {
        SleepSummary sleepSummary;
        MFSleepDay mFSleepDay;
        Rh f = Rh.f("SELECT * FROM sleep_date WHERE date == ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, true, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "timezoneOffset");
            int c3 = Dx0.c(b, "date");
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_GOAL_MINUTES);
            int c5 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_MINUTES);
            int c6 = Dx0.c(b, com.fossil.wearables.fsl.sleep.MFSleepDay.COLUMN_SLEEP_STATE_DIST_IN_MINUTE);
            int c7 = Dx0.c(b, "createdAt");
            int c8 = Dx0.c(b, "updatedAt");
            Zi0<String, ArrayList<MFSleepSession>> zi0 = new Zi0<>();
            while (b.moveToNext()) {
                if (!b.isNull(c3)) {
                    String string = b.getString(c3);
                    if (zi0.get(string) == null) {
                        zi0.put(string, new ArrayList<>());
                    }
                }
            }
            b.moveToPosition(-1);
            __fetchRelationshipsleepSessionAscomPortfolioPlatformDataModelRoomSleepMFSleepSession(zi0);
            if (b.moveToFirst()) {
                if (!b.isNull(c) || !b.isNull(c2) || !b.isNull(c3) || !b.isNull(c4) || !b.isNull(c5) || !b.isNull(c6) || !b.isNull(c7) || !b.isNull(c8)) {
                    MFSleepDay mFSleepDay2 = new MFSleepDay(this.__dateShortStringConverter.b(b.getString(c3)), b.getInt(c4), b.getInt(c5), this.__sleepDistributionConverter.a(b.getString(c6)), this.__dateTimeConverter.a(b.getLong(c7)), this.__dateTimeConverter.a(b.getLong(c8)));
                    mFSleepDay2.setPinType(b.getInt(c));
                    mFSleepDay2.setTimezoneOffset(b.getInt(c2));
                    mFSleepDay = mFSleepDay2;
                } else {
                    mFSleepDay = null;
                }
                ArrayList<MFSleepSession> arrayList = !b.isNull(c3) ? zi0.get(b.getString(c3)) : null;
                sleepSummary = new SleepSummary(mFSleepDay, arrayList == null ? new ArrayList<>() : arrayList);
            } else {
                sleepSummary = null;
            }
            return sleepSummary;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public int getTotalSleep(String str, String str2) {
        int i = 0;
        Rh f = Rh.f("SELECT SUM(sleepMinutes) FROM sleep_date WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        if (str2 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            if (b.moveToFirst()) {
                i = b.getInt(0);
            }
            return i;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void insertSleepSettings(MFSleepSettings mFSleepSettings) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepSettings.insert((Hh<MFSleepSettings>) mFSleepSettings);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void updateSleepSettings(int i) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfUpdateSleepSettings.acquire();
        acquire.bindLong(1, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfUpdateSleepSettings.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void upsertSleepDay(MFSleepDay mFSleepDay) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepDay.insert((Hh<MFSleepDay>) mFSleepDay);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void upsertSleepDays(List<MFSleepDay> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepDay.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void upsertSleepRecommendedGoal(SleepRecommendedGoal sleepRecommendedGoal) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSleepRecommendedGoal.insert((Hh<SleepRecommendedGoal>) sleepRecommendedGoal);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void upsertSleepSession(MFSleepSession mFSleepSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepSession.insert((Hh<MFSleepSession>) mFSleepSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public void upsertSleepSessionList(List<MFSleepSession> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMFSleepSession_1.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.sleep.SleepDao
    public long upsertSleepStatistic(SleepStatistic sleepStatistic) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfSleepStatistic.insertAndReturnId(sleepStatistic);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }
}
