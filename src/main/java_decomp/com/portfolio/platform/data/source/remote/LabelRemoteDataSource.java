package com.portfolio.platform.data.source.remote;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LabelRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "LabelRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 api;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public LabelRemoteDataSource(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        this.api = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getLabel(java.lang.String r9, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.source.local.label.Label>> r10) {
        /*
            r8 = this;
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.LabelRemoteDataSource$getLabel$Anon1
            if (r0 == 0) goto L_0x008d
            r0 = r10
            com.portfolio.platform.data.source.remote.LabelRemoteDataSource$getLabel$Anon1 r0 = (com.portfolio.platform.data.source.remote.LabelRemoteDataSource$getLabel$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x008d
            int r1 = r1 + r4
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r4 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x009c
            if (r0 != r5) goto L_0x0094
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.remote.LabelRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.LabelRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x002d:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x00b6
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r1.getLocal()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r1 = "data: "
            r4.append(r1)
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r1 = r0.a()
            if (r1 == 0) goto L_0x00b2
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            java.util.List r1 = r1.get_items()
            r4.append(r1)
            java.lang.String r1 = " - isFromCache: "
            r4.append(r1)
            boolean r1 = r0.b()
            r4.append(r1)
            java.lang.String r1 = "LabelRemoteDataSource"
            java.lang.String r4 = r4.toString()
            r2.d(r1, r4)
            java.lang.Object r1 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            java.util.List r1 = r1.get_items()
            boolean r2 = r1.isEmpty()
            r2 = r2 ^ 1
            if (r2 == 0) goto L_0x00d6
            r2 = 0
            java.lang.Object r1 = r1.get(r2)
            com.portfolio.platform.data.source.local.label.Label r1 = (com.portfolio.platform.data.source.local.label.Label) r1
        L_0x0082:
            com.fossil.Kq5 r2 = new com.fossil.Kq5
            boolean r0 = r0.b()
            r2.<init>(r1, r0)
            r0 = r2
        L_0x008c:
            return r0
        L_0x008d:
            com.portfolio.platform.data.source.remote.LabelRemoteDataSource$getLabel$Anon1 r0 = new com.portfolio.platform.data.source.remote.LabelRemoteDataSource$getLabel$Anon1
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0094:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x009c:
            com.fossil.El7.b(r2)
            com.portfolio.platform.data.source.remote.LabelRemoteDataSource$getLabel$response$Anon1 r0 = new com.portfolio.platform.data.source.remote.LabelRemoteDataSource$getLabel$response$Anon1
            r0.<init>(r8, r9, r3)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r0, r1)
            if (r0 != r4) goto L_0x002d
            r0 = r4
            goto L_0x008c
        L_0x00b2:
            com.mapped.Wg6.i()
            throw r3
        L_0x00b6:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00d0
            r2 = r0
            com.fossil.Hq5 r2 = (com.fossil.Hq5) r2
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r2.a()
            com.portfolio.platform.data.model.ServerError r2 = r2.c()
            r6 = 28
            r4 = r3
            r5 = r3
            r7 = r3
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x008c
        L_0x00d0:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        L_0x00d6:
            r1 = r3
            goto L_0x0082
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.LabelRemoteDataSource.getLabel(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }
}
