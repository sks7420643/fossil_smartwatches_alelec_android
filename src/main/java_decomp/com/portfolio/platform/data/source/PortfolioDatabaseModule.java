package com.portfolio.platform.data.source;

import com.facebook.appevents.UserDataStore;
import com.fossil.Bu4;
import com.fossil.Ft4;
import com.fossil.Fu4;
import com.fossil.H97;
import com.fossil.Jt4;
import com.fossil.Lr4;
import com.fossil.Nr4;
import com.fossil.Pw0;
import com.fossil.Qs4;
import com.fossil.Rt4;
import com.fossil.Xt4;
import com.fossil.Ys4;
import com.mapped.An4;
import com.mapped.Jh6;
import com.mapped.Oh;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.AppSettingsDatabase;
import com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource;
import com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase;
import com.portfolio.platform.buddy_challenge.domain.ChallengeRemoteDataSource;
import com.portfolio.platform.buddy_challenge.domain.FriendRemoteDataSource;
import com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource;
import com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource;
import com.portfolio.platform.data.source.local.AddressDao;
import com.portfolio.platform.data.source.local.AddressDatabase;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.local.CategoryDatabase;
import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import com.portfolio.platform.data.source.local.CustomizeRealDataDatabase;
import com.portfolio.platform.data.source.local.FileDao;
import com.portfolio.platform.data.source.local.FileDatabase;
import com.portfolio.platform.data.source.local.ThemeDao;
import com.portfolio.platform.data.source.local.ThemeDatabase;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDao;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationDatabase;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseDatabase;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.FitnessHelper;
import com.portfolio.platform.preset.data.source.DianaRecommendedPresetRemote;
import com.portfolio.platform.watchface.data.source.WFAssetRemote;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRemote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule {
    @DexIgnore
    public final AddressDao provideAddressDao(AddressDatabase addressDatabase) {
        Wg6.c(addressDatabase, UserDataStore.DATE_OF_BIRTH);
        return addressDatabase.addressDao();
    }

    @DexIgnore
    public final AddressDatabase provideAddressDatabase(PortfolioApp portfolioApp) {
        AddressDatabase addressDatabase;
        synchronized (this) {
            Wg6.c(portfolioApp, "app");
            Oh.Ai a2 = Pw0.a(portfolioApp, AddressDatabase.class, "address.db");
            a2.f();
            Oh d = a2.d();
            Wg6.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
            addressDatabase = (AddressDatabase) d;
        }
        return addressDatabase;
    }

    @DexIgnore
    public final AppSettingsDatabase provideAppSettingsDatabase(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "app");
        Oh.Ai a2 = Pw0.a(portfolioApp, AppSettingsDatabase.class, "app_settings_database.db");
        a2.f();
        Oh d = a2.d();
        Wg6.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (AppSettingsDatabase) d;
    }

    @DexIgnore
    public final CategoryDao provideCategoryDao(CategoryDatabase categoryDatabase) {
        Wg6.c(categoryDatabase, UserDataStore.DATE_OF_BIRTH);
        return categoryDatabase.categoryDao();
    }

    @DexIgnore
    public final CategoryDatabase provideCategoryDatabase(PortfolioApp portfolioApp) {
        CategoryDatabase categoryDatabase;
        synchronized (this) {
            Wg6.c(portfolioApp, "app");
            Oh.Ai a2 = Pw0.a(portfolioApp, CategoryDatabase.class, "category.db");
            a2.f();
            Oh d = a2.d();
            Wg6.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
            categoryDatabase = (CategoryDatabase) d;
        }
        return categoryDatabase;
    }

    @DexIgnore
    public final CustomizeRealDataDao provideCustomizeRealDataDao(CustomizeRealDataDatabase customizeRealDataDatabase) {
        Wg6.c(customizeRealDataDatabase, UserDataStore.DATE_OF_BIRTH);
        return customizeRealDataDatabase.realDataDao();
    }

    @DexIgnore
    public final CustomizeRealDataDatabase provideCustomizeRealDataDatabase(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "app");
        Oh.Ai a2 = Pw0.a(portfolioApp, CustomizeRealDataDatabase.class, "customizeRealData.db");
        a2.f();
        Oh d = a2.d();
        Wg6.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (CustomizeRealDataDatabase) d;
    }

    @DexIgnore
    public final DNDSettingsDatabase provideDNDSettingsDatabase(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "app");
        Oh.Ai a2 = Pw0.a(portfolioApp, DNDSettingsDatabase.class, "dndSettings.db");
        a2.f();
        Oh d = a2.d();
        Wg6.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (DNDSettingsDatabase) d;
    }

    @DexIgnore
    public final DeviceDao provideDeviceDao(DeviceDatabase deviceDatabase) {
        Wg6.c(deviceDatabase, UserDataStore.DATE_OF_BIRTH);
        return deviceDatabase.deviceDao();
    }

    @DexIgnore
    public final DeviceDatabase provideDeviceDatabase(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "app");
        Oh.Ai a2 = Pw0.a(portfolioApp, DeviceDatabase.class, "devices.db");
        a2.b(DeviceDatabase.Companion.getMIGRATION_FROM_1_TO_2(), DeviceDatabase.Companion.getMIGRATION_FROM_2_TO_3(), DeviceDatabase.Companion.getMIGRATION_FROM_3_TO_4());
        a2.f();
        Oh d = a2.d();
        Wg6.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (DeviceDatabase) d;
    }

    @DexIgnore
    public final FileDao provideFileDao(FileDatabase fileDatabase) {
        Wg6.c(fileDatabase, UserDataStore.DATE_OF_BIRTH);
        return fileDatabase.fileDao();
    }

    @DexIgnore
    public final FileDatabase provideFileDatabase(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "app");
        Oh.Ai a2 = Pw0.a(portfolioApp, FileDatabase.class, "file.db");
        a2.b(FileDatabase.Companion.getMIGRATION_FROM_1_TO_2());
        Oh d = a2.d();
        Wg6.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (FileDatabase) d;
    }

    @DexIgnore
    public final FitnessHelper provideFitnessHelper(An4 an4) {
        Wg6.c(an4, "sharedPreferencesManager");
        return new FitnessHelper(an4);
    }

    @DexIgnore
    public final HybridCustomizeDatabase provideHybridCustomizeDatabase(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "app");
        Oh.Ai a2 = Pw0.a(portfolioApp, HybridCustomizeDatabase.class, "hybridCustomize.db");
        a2.f();
        Oh d = a2.d();
        Wg6.b(d, "Room\n                .da\u2026\n                .build()");
        return (HybridCustomizeDatabase) d;
    }

    @DexIgnore
    public final InAppNotificationDao provideInAppNotificationDao(InAppNotificationDatabase inAppNotificationDatabase) {
        Wg6.c(inAppNotificationDatabase, UserDataStore.DATE_OF_BIRTH);
        return inAppNotificationDatabase.inAppNotificationDao();
    }

    @DexIgnore
    public final InAppNotificationDatabase provideInAppNotificationDatabase(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "app");
        Oh.Ai a2 = Pw0.a(portfolioApp, InAppNotificationDatabase.class, "inAppNotification.db");
        a2.f();
        Oh d = a2.d();
        Wg6.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (InAppNotificationDatabase) d;
    }

    @DexIgnore
    public final MicroAppDao provideMicroAppDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        Wg6.c(hybridCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return hybridCustomizeDatabase.microAppDao();
    }

    @DexIgnore
    public final MicroAppLastSettingDao provideMicroAppLastSettingDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        Wg6.c(hybridCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return hybridCustomizeDatabase.microAppLastSettingDao();
    }

    @DexIgnore
    public final NotificationSettingsDao provideNotificationSettingsDao(NotificationSettingsDatabase notificationSettingsDatabase) {
        Wg6.c(notificationSettingsDatabase, UserDataStore.DATE_OF_BIRTH);
        return notificationSettingsDatabase.getNotificationSettingsDao();
    }

    @DexIgnore
    public final NotificationSettingsDatabase provideNotificationSettingsDatabase(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "app");
        Oh.Ai a2 = Pw0.a(portfolioApp, NotificationSettingsDatabase.class, "notificationSettings.db");
        a2.f();
        Oh d = a2.d();
        Wg6.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (NotificationSettingsDatabase) d;
    }

    @DexIgnore
    public final HybridPresetDao providePresetDao(HybridCustomizeDatabase hybridCustomizeDatabase) {
        Wg6.c(hybridCustomizeDatabase, UserDataStore.DATE_OF_BIRTH);
        return hybridCustomizeDatabase.presetDao();
    }

    @DexIgnore
    public final QuickResponseDatabase provideQuickResponseDatabase(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "app");
        Jh6 jh6 = new Jh6();
        jh6.element = null;
        Oh.Ai a2 = Pw0.a(portfolioApp.getApplicationContext(), QuickResponseDatabase.class, "quickResponse.db");
        a2.f();
        a2.a(new PortfolioDatabaseModule$provideQuickResponseDatabase$Anon1(jh6));
        T t = (T) ((QuickResponseDatabase) a2.d());
        jh6.element = t;
        return t;
    }

    @DexIgnore
    public final QuickResponseMessageDao provideQuickResponseMessageDao(QuickResponseDatabase quickResponseDatabase) {
        Wg6.c(quickResponseDatabase, UserDataStore.DATE_OF_BIRTH);
        return quickResponseDatabase.quickResponseMessageDao();
    }

    @DexIgnore
    public final QuickResponseSenderDao provideQuickResponseSenderDao(QuickResponseDatabase quickResponseDatabase) {
        Wg6.c(quickResponseDatabase, UserDataStore.DATE_OF_BIRTH);
        return quickResponseDatabase.quickResponseSenderDao();
    }

    @DexIgnore
    public final RemindersSettingsDatabase provideRemindersSettingsDatabase(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "app");
        Oh.Ai a2 = Pw0.a(portfolioApp, RemindersSettingsDatabase.class, "remindersSettings.db");
        a2.f();
        Oh d = a2.d();
        Wg6.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (RemindersSettingsDatabase) d;
    }

    @DexIgnore
    public final SkuDao provideSkuDao(DeviceDatabase deviceDatabase) {
        Wg6.c(deviceDatabase, UserDataStore.DATE_OF_BIRTH);
        return deviceDatabase.skuDao();
    }

    @DexIgnore
    public final ThemeDao provideThemeDao(ThemeDatabase themeDatabase) {
        Wg6.c(themeDatabase, UserDataStore.DATE_OF_BIRTH);
        return themeDatabase.themeDao();
    }

    @DexIgnore
    public final ThemeDatabase provideThemeDatabase(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "app");
        Oh.Ai a2 = Pw0.a(portfolioApp, ThemeDatabase.class, "theme.db");
        a2.f();
        Oh d = a2.d();
        Wg6.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (ThemeDatabase) d;
    }

    @DexIgnore
    public final UserSettingDao provideUserSettingDao(UserSettingDatabase userSettingDatabase) {
        UserSettingDao userSettingDao;
        synchronized (this) {
            Wg6.c(userSettingDatabase, UserDataStore.DATE_OF_BIRTH);
            userSettingDao = userSettingDatabase.userSettingDao();
        }
        return userSettingDao;
    }

    @DexIgnore
    public final UserSettingDatabase provideUserSettingDatabase(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "app");
        Oh.Ai a2 = Pw0.a(portfolioApp, UserSettingDatabase.class, "userSetting.db");
        a2.f();
        Oh d = a2.d();
        Wg6.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (UserSettingDatabase) d;
    }

    @DexIgnore
    public final BuddyChallengeDatabase providesBuddyChallengeDatabase(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "app");
        Oh.Ai a2 = Pw0.a(portfolioApp, BuddyChallengeDatabase.class, "buddy_challenge.db");
        a2.f();
        Oh d = a2.d();
        Wg6.b(d, "Room.databaseBuilder(app\u2026\n                .build()");
        return (BuddyChallengeDatabase) d;
    }

    @DexIgnore
    public final Qs4 providesChallengeDao(BuddyChallengeDatabase buddyChallengeDatabase) {
        Wg6.c(buddyChallengeDatabase, UserDataStore.DATE_OF_BIRTH);
        return buddyChallengeDatabase.a();
    }

    @DexIgnore
    public final Rt4 providesChallengeLocalDataSource(Qs4 qs4) {
        Wg6.c(qs4, "dao");
        return new Rt4(qs4);
    }

    @DexIgnore
    public final ChallengeRemoteDataSource providesChallengeRemoteDataSource(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        return new ChallengeRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public final DianaRecommendedPresetRemote providesDianaRecommendedPresetRemote(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        return new DianaRecommendedPresetRemote(apiServiceV2);
    }

    @DexIgnore
    public final Lr4 providesFlagDao(AppSettingsDatabase appSettingsDatabase) {
        Wg6.c(appSettingsDatabase, "appSettingsDatabase");
        return appSettingsDatabase.a();
    }

    @DexIgnore
    public final Nr4 providesFlagLocalSource(Lr4 lr4) {
        Wg6.c(lr4, "dao");
        return new Nr4(lr4);
    }

    @DexIgnore
    public final FlagRemoteDataSource providesFlagRemoteSource(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        return new FlagRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public final Ft4 providesNotificationDao(BuddyChallengeDatabase buddyChallengeDatabase) {
        Wg6.c(buddyChallengeDatabase, UserDataStore.DATE_OF_BIRTH);
        return buddyChallengeDatabase.c();
    }

    @DexIgnore
    public final Bu4 providesNotificationLocal(Ft4 ft4) {
        Wg6.c(ft4, "dao");
        return new Bu4(ft4);
    }

    @DexIgnore
    public final NotificationRemoteDataSource providesNotificationRemote(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        return new NotificationRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public final Ys4 providesSocialFriendDao(BuddyChallengeDatabase buddyChallengeDatabase) {
        Wg6.c(buddyChallengeDatabase, UserDataStore.DATE_OF_BIRTH);
        return buddyChallengeDatabase.b();
    }

    @DexIgnore
    public final Xt4 providesSocialFriendLocal(Ys4 ys4) {
        Wg6.c(ys4, "dao");
        return new Xt4(ys4);
    }

    @DexIgnore
    public final FriendRemoteDataSource providesSocialFriendRemote(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        return new FriendRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public final Jt4 providesSocialProfileDao(BuddyChallengeDatabase buddyChallengeDatabase) {
        Wg6.c(buddyChallengeDatabase, UserDataStore.DATE_OF_BIRTH);
        return buddyChallengeDatabase.d();
    }

    @DexIgnore
    public final Fu4 providesSocialProfileLocal(Jt4 jt4) {
        Wg6.c(jt4, "dao");
        return new Fu4(jt4);
    }

    @DexIgnore
    public final ProfileRemoteDataSource providesSocialProfileRemote(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        return new ProfileRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public final H97 providesWFBackgroundPhotoDao(DianaCustomizeDatabase dianaCustomizeDatabase) {
        Wg6.c(dianaCustomizeDatabase, "customizeDatabase");
        return dianaCustomizeDatabase.getWFBackgroundDao();
    }

    @DexIgnore
    public final WFBackgroundPhotoRemote providesWFBackgroundPhotoRemote(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        return new WFBackgroundPhotoRemote(apiServiceV2);
    }

    @DexIgnore
    public final WFAssetRemote providesWFTemplateRemote(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "api");
        return new WFAssetRemote(apiServiceV2);
    }
}
