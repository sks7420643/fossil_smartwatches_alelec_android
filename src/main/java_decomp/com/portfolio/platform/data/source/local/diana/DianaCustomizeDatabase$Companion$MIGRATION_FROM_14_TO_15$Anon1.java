package com.portfolio.platform.data.source.local.diana;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import com.fossil.Lx0;
import com.fossil.Vt7;
import com.fossil.Ym5;
import com.google.gson.Gson;
import com.mapped.Ai4;
import com.mapped.Wg6;
import com.mapped.Xh;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.preset.Background;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeDatabase$Companion$MIGRATION_FROM_14_TO_15$Anon1 extends Xh {
    @DexIgnore
    public DianaCustomizeDatabase$Companion$MIGRATION_FROM_14_TO_15$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.mapped.Xh
    public void migrate(Lx0 lx0) {
        Wg6.c(lx0, "database");
        Log.d(DianaCustomizeDatabase.TAG, "MIGRATION_FROM_14_TO_15 - START");
        lx0.beginTransaction();
        try {
            Gson gson = new Gson();
            Type type = new DianaCustomizeDatabase$Companion$MIGRATION_FROM_14_TO_15$Anon1$migrate$ringStyleItemsType$Anon1_Level2().getType();
            Type type2 = new DianaCustomizeDatabase$Companion$MIGRATION_FROM_14_TO_15$Anon1$migrate$backgroundType$Anon1_Level2().getType();
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            Wg6.b(applicationContext, "PortfolioApp.instance.applicationContext");
            File filesDir = applicationContext.getFilesDir();
            String directory = FileUtils.getDirectory(PortfolioApp.get.instance().getApplicationContext(), FileType.WATCH_FACE);
            File file = new File(directory);
            if (!file.exists()) {
                file.mkdirs();
            }
            Cursor query = lx0.query("SELECT * FROM watch_face");
            query.moveToFirst();
            while (true) {
                Wg6.b(query, "cursor");
                if (query.isAfterLast()) {
                    break;
                }
                int i = query.getInt(query.getColumnIndex("watchFaceType"));
                String string = query.getString(query.getColumnIndex("ringStyleItems"));
                String string2 = query.getString(query.getColumnIndex(Explore.COLUMN_BACKGROUND));
                String string3 = query.getString(query.getColumnIndex("previewUrl"));
                ArrayList<String> arrayList = new ArrayList();
                Object l = gson.l(string, type);
                Wg6.b(l, "gSon.fromJson(ringStyleValue, ringStyleItemsType)");
                for (RingStyleItem ringStyleItem : (List) l) {
                    String previewUrl = ringStyleItem.getRingStyle().getData().getPreviewUrl();
                    String url = ringStyleItem.getRingStyle().getData().getUrl();
                    if (!(previewUrl == null || Vt7.l(previewUrl))) {
                        arrayList.add(previewUrl);
                    }
                    if (!(url == null || Vt7.l(url))) {
                        arrayList.add(url);
                    }
                }
                Object l2 = gson.l(string2, type2);
                Wg6.b(l2, "gSon.fromJson(backgroundValue, backgroundType)");
                Background background = (Background) l2;
                String previewUrl2 = background.getData().getPreviewUrl();
                if (!(previewUrl2 == null || Vt7.l(previewUrl2))) {
                    arrayList.add(previewUrl2);
                }
                if (!(string3 == null || Vt7.l(string3))) {
                    arrayList.add(string3);
                }
                for (String str : arrayList) {
                    String b = Ym5.b(str);
                    moveFile(filesDir + File.separator + b, directory + File.separator + b);
                }
                if (i == Ai4.PHOTO.getValue()) {
                    String string4 = query.getString(query.getColumnIndex("id"));
                    Wg6.b(string4, "imageId");
                    String q = Vt7.q(string4, Constants.PHOTO_IMAGE_NAME_SUFFIX, Constants.PHOTO_BINARY_NAME_SUFFIX, false, 4, null);
                    String str2 = filesDir + File.separator + q;
                    String str3 = directory + File.separator + string4;
                    String str4 = directory + File.separator + q;
                    moveFile(filesDir + File.separator + string4, str3);
                    moveFile(str2, str4);
                    background.getData().setPreviewUrl(str3);
                    background.getData().setUrl(str4);
                    lx0.execSQL("UPDATE `watch_face` SET  `background` = '" + gson.t(background) + "', `previewUrl` = '" + str3 + "' WHERE `id` = '" + string4 + '\'');
                }
                query.moveToNext();
            }
            query.close();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(DianaCustomizeDatabase.TAG, "MIGRATION_FROM_14_TO_15 - ERROR: " + e.getMessage());
        }
        lx0.execSQL("CREATE TABLE IF NOT EXISTS `watchAppData` (`id` TEXT NOT NULL, `type` TEXT NOT NULL, `maxAppVersion` TEXT NOT NULL, `maxFirmwareOSVersion` TEXT NOT NULL, `minAppVersion` TEXT NOT NULL, `minFirmwareOSVersion` TEXT NOT NULL, `version` TEXT NOT NULL, `executableBinaryDataUrl` TEXT NOT NULL, `updatedAt` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        lx0.setTransactionSuccessful();
        lx0.endTransaction();
        Log.d(DianaCustomizeDatabase.TAG, "MIGRATION_FROM_14_TO_15 - END");
    }

    @DexIgnore
    public final void moveFile(String str, String str2) {
        Wg6.c(str, "oldPath");
        Wg6.c(str2, "newPath");
        File file = new File(str);
        if (file.exists()) {
            file.renameTo(new File(str2));
        }
    }
}
