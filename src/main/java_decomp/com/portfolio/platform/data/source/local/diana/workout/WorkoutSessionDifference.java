package com.portfolio.platform.data.source.local.diana.workout;

import com.mapped.Wg6;
import com.mapped.Zf;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionDifference extends Zf.Di<WorkoutSession> {
    @DexIgnore
    public boolean areContentsTheSame(WorkoutSession workoutSession, WorkoutSession workoutSession2) {
        Wg6.c(workoutSession, "oldItem");
        Wg6.c(workoutSession2, "newItem");
        return Wg6.a(workoutSession, workoutSession2) && Wg6.a(workoutSession.getScreenShotUri(), workoutSession2.getScreenShotUri());
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Zf.Di
    public /* bridge */ /* synthetic */ boolean areContentsTheSame(WorkoutSession workoutSession, WorkoutSession workoutSession2) {
        return areContentsTheSame(workoutSession, workoutSession2);
    }

    @DexIgnore
    public boolean areItemsTheSame(WorkoutSession workoutSession, WorkoutSession workoutSession2) {
        Wg6.c(workoutSession, "oldItem");
        Wg6.c(workoutSession2, "newItem");
        return Wg6.a(workoutSession.getId(), workoutSession2.getId());
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Zf.Di
    public /* bridge */ /* synthetic */ boolean areItemsTheSame(WorkoutSession workoutSession, WorkoutSession workoutSession2) {
        return areItemsTheSame(workoutSession, workoutSession2);
    }
}
