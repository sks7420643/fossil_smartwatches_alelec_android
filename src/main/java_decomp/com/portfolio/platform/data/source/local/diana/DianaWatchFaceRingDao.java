package com.portfolio.platform.data.source.local.diana;

import com.mapped.Cd6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceRing;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface DianaWatchFaceRingDao {
    @DexIgnore
    Object clearAll(Xe6<? super Cd6> xe6);

    @DexIgnore
    Object getAllDianaWatchFaceRings(Xe6<? super List<DianaWatchFaceRing>> xe6);

    @DexIgnore
    Object getDianaWatchFaceRing(String str, Xe6<? super DianaWatchFaceRing> xe6);

    @DexIgnore
    Object upsertDianaWatchFaceRing(DianaWatchFaceRing dianaWatchFaceRing, Xe6<? super Cd6> xe6);

    @DexIgnore
    Object upsertDianaWatchFaceRings(List<DianaWatchFaceRing> list, Xe6<? super Cd6> xe6);
}
