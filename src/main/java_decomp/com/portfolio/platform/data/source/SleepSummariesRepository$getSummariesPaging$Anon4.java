package com.portfolio.platform.data.source;

import com.fossil.Qq7;
import com.mapped.Cd6;
import com.mapped.Gg6;
import com.mapped.PagingRequestHelper;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository$getSummariesPaging$Anon4 extends Qq7 implements Gg6<Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSummariesPaging$Anon4(SleepSummaryDataSourceFactory sleepSummaryDataSourceFactory) {
        super(0);
        this.$sourceFactory = sleepSummaryDataSourceFactory;
    }

    @DexIgnore
    @Override // com.mapped.Gg6
    public final void invoke() {
        PagingRequestHelper mHelper;
        SleepSummaryLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
        if (e != null && (mHelper = e.getMHelper()) != null) {
            mHelper.g();
        }
    }
}
