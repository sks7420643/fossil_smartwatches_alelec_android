package com.portfolio.platform.data.source.local.diana.heartrate;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Bx0;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.H05;
import com.fossil.Nw0;
import com.fossil.Qz4;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.mapped.Xe;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateDailySummaryDao_Impl extends HeartRateDailySummaryDao {
    @DexIgnore
    public /* final */ Qz4 __dateShortStringConverter; // = new Qz4();
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<DailyHeartRateSummary> __insertionAdapterOfDailyHeartRateSummary;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteAllHeartRateSummaries;
    @DexIgnore
    public /* final */ H05 __restingConverter; // = new H05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<DailyHeartRateSummary> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, DailyHeartRateSummary dailyHeartRateSummary) {
            mi.bindDouble(1, (double) dailyHeartRateSummary.getAverage());
            String a2 = HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.a(dailyHeartRateSummary.getDate());
            if (a2 == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, a2);
            }
            mi.bindLong(3, dailyHeartRateSummary.getCreatedAt());
            mi.bindLong(4, dailyHeartRateSummary.getUpdatedAt());
            mi.bindLong(5, (long) dailyHeartRateSummary.getMin());
            mi.bindLong(6, (long) dailyHeartRateSummary.getMax());
            mi.bindLong(7, (long) dailyHeartRateSummary.getMinuteCount());
            String b = HeartRateDailySummaryDao_Impl.this.__restingConverter.b(dailyHeartRateSummary.getResting());
            if (b == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, b);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, DailyHeartRateSummary dailyHeartRateSummary) {
            bind(mi, dailyHeartRateSummary);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `daily_heart_rate_summary` (`average`,`date`,`createdAt`,`updatedAt`,`min`,`max`,`minuteCount`,`resting`) VALUES (?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM daily_heart_rate_summary";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<DailyHeartRateSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon3(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DailyHeartRateSummary> call() throws Exception {
            Cursor b = Ex0.b(HeartRateDailySummaryDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, GoalTrackingSummary.COLUMN_AVERAGE);
                int c2 = Dx0.c(b, "date");
                int c3 = Dx0.c(b, "createdAt");
                int c4 = Dx0.c(b, "updatedAt");
                int c5 = Dx0.c(b, "min");
                int c6 = Dx0.c(b, "max");
                int c7 = Dx0.c(b, "minuteCount");
                int c8 = Dx0.c(b, "resting");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new DailyHeartRateSummary(b.getFloat(c), HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.b(b.getString(c2)), b.getLong(c3), b.getLong(c4), b.getInt(c5), b.getInt(c6), b.getInt(c7), HeartRateDailySummaryDao_Impl.this.__restingConverter.a(b.getString(c8))));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends Xe.Bi<Integer, DailyHeartRateSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 extends Bx0<DailyHeartRateSummary> {
            @DexIgnore
            public Anon1_Level2(Oh oh, Rh rh, boolean z, String... strArr) {
                super(oh, rh, z, strArr);
            }

            @DexIgnore
            @Override // com.fossil.Bx0
            public List<DailyHeartRateSummary> convertRows(Cursor cursor) {
                int c = Dx0.c(cursor, GoalTrackingSummary.COLUMN_AVERAGE);
                int c2 = Dx0.c(cursor, "date");
                int c3 = Dx0.c(cursor, "createdAt");
                int c4 = Dx0.c(cursor, "updatedAt");
                int c5 = Dx0.c(cursor, "min");
                int c6 = Dx0.c(cursor, "max");
                int c7 = Dx0.c(cursor, "minuteCount");
                int c8 = Dx0.c(cursor, "resting");
                ArrayList arrayList = new ArrayList(cursor.getCount());
                while (cursor.moveToNext()) {
                    arrayList.add(new DailyHeartRateSummary(cursor.getFloat(c), HeartRateDailySummaryDao_Impl.this.__dateShortStringConverter.b(cursor.getString(c2)), cursor.getLong(c3), cursor.getLong(c4), cursor.getInt(c5), cursor.getInt(c6), cursor.getInt(c7), HeartRateDailySummaryDao_Impl.this.__restingConverter.a(cursor.getString(c8))));
                }
                return arrayList;
            }
        }

        @DexIgnore
        public Anon4(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.Bx0<com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary>' to match base method */
        @Override // com.mapped.Xe.Bi
        public Xe<Integer, DailyHeartRateSummary> create() {
            return new Anon1_Level2(HeartRateDailySummaryDao_Impl.this.__db, this.val$_statement, false, "daily_heart_rate_summary");
        }
    }

    @DexIgnore
    public HeartRateDailySummaryDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfDailyHeartRateSummary = new Anon1(oh);
        this.__preparedStmtOfDeleteAllHeartRateSummaries = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public void deleteAllHeartRateSummaries() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteAllHeartRateSummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllHeartRateSummaries.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public List<DailyHeartRateSummary> getDailyHeartRateSummariesDesc(Date date, Date date2) {
        Rh f = Rh.f("SELECT * FROM daily_heart_rate_summary WHERE date >= ? AND date <= ? ORDER BY date DESC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, GoalTrackingSummary.COLUMN_AVERAGE);
            int c2 = Dx0.c(b, "date");
            int c3 = Dx0.c(b, "createdAt");
            int c4 = Dx0.c(b, "updatedAt");
            int c5 = Dx0.c(b, "min");
            int c6 = Dx0.c(b, "max");
            int c7 = Dx0.c(b, "minuteCount");
            int c8 = Dx0.c(b, "resting");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new DailyHeartRateSummary(b.getFloat(c), this.__dateShortStringConverter.b(b.getString(c2)), b.getLong(c3), b.getLong(c4), b.getInt(c5), b.getInt(c6), b.getInt(c7), this.__restingConverter.a(b.getString(c8))));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public LiveData<List<DailyHeartRateSummary>> getDailyHeartRateSummariesLiveData(Date date, Date date2) {
        Rh f = Rh.f("SELECT * FROM daily_heart_rate_summary WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"daily_heart_rate_summary"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public DailyHeartRateSummary getDailyHeartRateSummary(Date date) {
        DailyHeartRateSummary dailyHeartRateSummary = null;
        Rh f = Rh.f("SELECT * FROM daily_heart_rate_summary WHERE date = ?", 1);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, GoalTrackingSummary.COLUMN_AVERAGE);
            int c2 = Dx0.c(b, "date");
            int c3 = Dx0.c(b, "createdAt");
            int c4 = Dx0.c(b, "updatedAt");
            int c5 = Dx0.c(b, "min");
            int c6 = Dx0.c(b, "max");
            int c7 = Dx0.c(b, "minuteCount");
            int c8 = Dx0.c(b, "resting");
            if (b.moveToFirst()) {
                dailyHeartRateSummary = new DailyHeartRateSummary(b.getFloat(c), this.__dateShortStringConverter.b(b.getString(c2)), b.getLong(c3), b.getLong(c4), b.getInt(c5), b.getInt(c6), b.getInt(c7), this.__restingConverter.a(b.getString(c8)));
            }
            return dailyHeartRateSummary;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public Date getLastDate() {
        Date date = null;
        Rh f = Rh.f("SELECT date FROM daily_heart_rate_summary ORDER BY date ASC LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            if (b.moveToFirst()) {
                date = this.__dateShortStringConverter.b(b.getString(0));
            }
            return date;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public Xe.Bi<Integer, DailyHeartRateSummary> getSummariesDataSource() {
        return new Anon4(Rh.f("SELECT * FROM daily_heart_rate_summary ORDER BY date DESC", 0));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public void insertDailyHeartRateSummary(DailyHeartRateSummary dailyHeartRateSummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDailyHeartRateSummary.insert((Hh<DailyHeartRateSummary>) dailyHeartRateSummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao
    public void insertListDailyHeartRateSummary(List<DailyHeartRateSummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDailyHeartRateSummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
