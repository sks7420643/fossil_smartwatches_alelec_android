package com.portfolio.platform.data.source.local.workoutsetting;

import androidx.lifecycle.LiveData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface WorkoutSettingDao {
    @DexIgnore
    Object cleanUp();  // void declaration

    @DexIgnore
    List<WorkoutSetting> getPendingWorkoutSettings();

    @DexIgnore
    List<WorkoutSetting> getWorkoutSettingList();

    @DexIgnore
    LiveData<List<WorkoutSetting>> getWorkoutSettingListAsLiveData();

    @DexIgnore
    void upsertWorkoutSetting(WorkoutSetting workoutSetting);

    @DexIgnore
    void upsertWorkoutSettingList(List<WorkoutSetting> list);
}
