package com.portfolio.platform.data.source;

import com.fossil.Ex0;
import com.fossil.Hw0;
import com.fossil.Ix0;
import com.fossil.Lx0;
import com.fossil.Nw0;
import com.mapped.Ji;
import com.mapped.Oh;
import com.mapped.Qh;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDatabase_Impl extends DeviceDatabase {
    @DexIgnore
    public volatile DeviceDao _deviceDao;
    @DexIgnore
    public volatile SkuDao _skuDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Qh.Ai {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void createAllTables(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `device` (`major` INTEGER NOT NULL, `minor` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT, `owner` TEXT, `productDisplayName` TEXT, `manufacturer` TEXT, `softwareRevision` TEXT, `hardwareRevision` TEXT, `activationDate` TEXT, `deviceId` TEXT NOT NULL, `macAddress` TEXT, `sku` TEXT, `firmwareRevision` TEXT, `batteryLevel` INTEGER NOT NULL, `vibrationStrength` INTEGER, `isActive` INTEGER NOT NULL, PRIMARY KEY(`deviceId`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `SKU` (`createdAt` TEXT, `updatedAt` TEXT, `serialNumberPrefix` TEXT NOT NULL, `sku` TEXT, `deviceName` TEXT, `groupName` TEXT, `gender` TEXT, `deviceType` TEXT, `fastPairId` TEXT, PRIMARY KEY(`serialNumberPrefix`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `watchParam` (`prefixSerial` TEXT NOT NULL, `versionMajor` TEXT, `versionMinor` TEXT, `data` TEXT, PRIMARY KEY(`prefixSerial`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '33a7c7e78a298d1d8772b73c27f82083')");
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void dropAllTables(Lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `device`");
            lx0.execSQL("DROP TABLE IF EXISTS `SKU`");
            lx0.execSQL("DROP TABLE IF EXISTS `watchParam`");
            if (DeviceDatabase_Impl.this.mCallbacks != null) {
                int size = DeviceDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) DeviceDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onCreate(Lx0 lx0) {
            if (DeviceDatabase_Impl.this.mCallbacks != null) {
                int size = DeviceDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) DeviceDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onOpen(Lx0 lx0) {
            DeviceDatabase_Impl.this.mDatabase = lx0;
            DeviceDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (DeviceDatabase_Impl.this.mCallbacks != null) {
                int size = DeviceDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) DeviceDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPostMigrate(Lx0 lx0) {
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPreMigrate(Lx0 lx0) {
            Ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public Qh.Bi onValidateSchema(Lx0 lx0) {
            HashMap hashMap = new HashMap(17);
            hashMap.put("major", new Ix0.Ai("major", "INTEGER", true, 0, null, 1));
            hashMap.put("minor", new Ix0.Ai("minor", "INTEGER", true, 0, null, 1));
            hashMap.put("createdAt", new Ix0.Ai("createdAt", "TEXT", false, 0, null, 1));
            hashMap.put("updatedAt", new Ix0.Ai("updatedAt", "TEXT", false, 0, null, 1));
            hashMap.put("owner", new Ix0.Ai("owner", "TEXT", false, 0, null, 1));
            hashMap.put("productDisplayName", new Ix0.Ai("productDisplayName", "TEXT", false, 0, null, 1));
            hashMap.put("manufacturer", new Ix0.Ai("manufacturer", "TEXT", false, 0, null, 1));
            hashMap.put("softwareRevision", new Ix0.Ai("softwareRevision", "TEXT", false, 0, null, 1));
            hashMap.put("hardwareRevision", new Ix0.Ai("hardwareRevision", "TEXT", false, 0, null, 1));
            hashMap.put("activationDate", new Ix0.Ai("activationDate", "TEXT", false, 0, null, 1));
            hashMap.put("deviceId", new Ix0.Ai("deviceId", "TEXT", true, 1, null, 1));
            hashMap.put("macAddress", new Ix0.Ai("macAddress", "TEXT", false, 0, null, 1));
            hashMap.put(LegacyDeviceModel.COLUMN_DEVICE_MODEL, new Ix0.Ai(LegacyDeviceModel.COLUMN_DEVICE_MODEL, "TEXT", false, 0, null, 1));
            hashMap.put(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION, new Ix0.Ai(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION, "TEXT", false, 0, null, 1));
            hashMap.put(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, new Ix0.Ai(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, "INTEGER", true, 0, null, 1));
            hashMap.put("vibrationStrength", new Ix0.Ai("vibrationStrength", "INTEGER", false, 0, null, 1));
            hashMap.put("isActive", new Ix0.Ai("isActive", "INTEGER", true, 0, null, 1));
            Ix0 ix0 = new Ix0("device", hashMap, new HashSet(0), new HashSet(0));
            Ix0 a2 = Ix0.a(lx0, "device");
            if (!ix0.equals(a2)) {
                return new Qh.Bi(false, "device(com.portfolio.platform.data.model.Device).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(9);
            hashMap2.put("createdAt", new Ix0.Ai("createdAt", "TEXT", false, 0, null, 1));
            hashMap2.put("updatedAt", new Ix0.Ai("updatedAt", "TEXT", false, 0, null, 1));
            hashMap2.put("serialNumberPrefix", new Ix0.Ai("serialNumberPrefix", "TEXT", true, 1, null, 1));
            hashMap2.put(LegacyDeviceModel.COLUMN_DEVICE_MODEL, new Ix0.Ai(LegacyDeviceModel.COLUMN_DEVICE_MODEL, "TEXT", false, 0, null, 1));
            hashMap2.put("deviceName", new Ix0.Ai("deviceName", "TEXT", false, 0, null, 1));
            hashMap2.put("groupName", new Ix0.Ai("groupName", "TEXT", false, 0, null, 1));
            hashMap2.put("gender", new Ix0.Ai("gender", "TEXT", false, 0, null, 1));
            hashMap2.put("deviceType", new Ix0.Ai("deviceType", "TEXT", false, 0, null, 1));
            hashMap2.put("fastPairId", new Ix0.Ai("fastPairId", "TEXT", false, 0, null, 1));
            Ix0 ix02 = new Ix0("SKU", hashMap2, new HashSet(0), new HashSet(0));
            Ix0 a3 = Ix0.a(lx0, "SKU");
            if (!ix02.equals(a3)) {
                return new Qh.Bi(false, "SKU(com.portfolio.platform.data.model.SKUModel).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
            }
            HashMap hashMap3 = new HashMap(4);
            hashMap3.put("prefixSerial", new Ix0.Ai("prefixSerial", "TEXT", true, 1, null, 1));
            hashMap3.put("versionMajor", new Ix0.Ai("versionMajor", "TEXT", false, 0, null, 1));
            hashMap3.put("versionMinor", new Ix0.Ai("versionMinor", "TEXT", false, 0, null, 1));
            hashMap3.put("data", new Ix0.Ai("data", "TEXT", false, 0, null, 1));
            Ix0 ix03 = new Ix0("watchParam", hashMap3, new HashSet(0), new HashSet(0));
            Ix0 a4 = Ix0.a(lx0, "watchParam");
            if (ix03.equals(a4)) {
                return new Qh.Bi(true, null);
            }
            return new Qh.Bi(false, "watchParam(com.portfolio.platform.data.model.WatchParam).\n Expected:\n" + ix03 + "\n Found:\n" + a4);
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public void clearAllTables() {
        super.assertNotMainThread();
        Lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `device`");
            writableDatabase.execSQL("DELETE FROM `SKU`");
            writableDatabase.execSQL("DELETE FROM `watchParam`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Nw0 createInvalidationTracker() {
        return new Nw0(this, new HashMap(0), new HashMap(0), "device", "SKU", "watchParam");
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Ji createOpenHelper(Hw0 hw0) {
        Qh qh = new Qh(hw0, new Anon1(4), "33a7c7e78a298d1d8772b73c27f82083", "ccea49e2e20194ca42a76fa8a936d66a");
        Ji.Bi.Aii a2 = Ji.Bi.a(hw0.b);
        a2.c(hw0.c);
        a2.b(qh);
        return hw0.a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDatabase
    public DeviceDao deviceDao() {
        DeviceDao deviceDao;
        if (this._deviceDao != null) {
            return this._deviceDao;
        }
        synchronized (this) {
            if (this._deviceDao == null) {
                this._deviceDao = new DeviceDao_Impl(this);
            }
            deviceDao = this._deviceDao;
        }
        return deviceDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDatabase
    public SkuDao skuDao() {
        SkuDao skuDao;
        if (this._skuDao != null) {
            return this._skuDao;
        }
        synchronized (this) {
            if (this._skuDao == null) {
                this._skuDao = new SkuDao_Impl(this);
            }
            skuDao = this._skuDao;
        }
        return skuDao;
    }
}
