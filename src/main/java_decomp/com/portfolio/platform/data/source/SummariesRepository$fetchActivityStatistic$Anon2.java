package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.ActivityStatistic;
import com.zendesk.sdk.support.help.HelpSearchRecyclerViewAdapter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SummariesRepository$fetchActivityStatistic$2", f = "SummariesRepository.kt", l = {HelpSearchRecyclerViewAdapter.TYPE_NO_RESULTS, 444}, m = "invokeSuspend")
public final class SummariesRepository$fetchActivityStatistic$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super ActivityStatistic>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$fetchActivityStatistic$Anon2(SummariesRepository summariesRepository, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = summariesRepository;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SummariesRepository$fetchActivityStatistic$Anon2 summariesRepository$fetchActivityStatistic$Anon2 = new SummariesRepository$fetchActivityStatistic$Anon2(this.this$0, xe6);
        summariesRepository$fetchActivityStatistic$Anon2.p$ = (Il6) obj;
        throw null;
        //return summariesRepository$fetchActivityStatistic$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super ActivityStatistic> xe6) {
        throw null;
        //return ((SummariesRepository$fetchActivityStatistic$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007d  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r7) {
        /*
            r6 = this;
            r5 = 2
            r2 = 1
            r3 = 0
            java.lang.Object r4 = com.fossil.Yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x0065
            if (r0 == r2) goto L_0x003c
            if (r0 != r5) goto L_0x0034
            java.lang.Object r0 = r6.L$1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            java.lang.Object r1 = r6.L$0
            com.mapped.Il6 r1 = (com.mapped.Il6) r1
            com.fossil.El7.b(r7)
            r1 = r7
            r2 = r0
        L_0x001c:
            r0 = r1
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao r3 = r0.activitySummaryDao()
            r0 = r2
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r1 = r0.a()
            com.portfolio.platform.data.ActivityStatistic r1 = (com.portfolio.platform.data.ActivityStatistic) r1
            r3.upsertActivityStatistic(r1)
            java.lang.Object r3 = r0.a()
        L_0x0033:
            return r3
        L_0x0034:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003c:
            java.lang.Object r0 = r6.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r7)
            r2 = r0
            r1 = r7
        L_0x0045:
            r0 = r1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x007d
            r1 = r0
            com.fossil.Kq5 r1 = (com.fossil.Kq5) r1
            java.lang.Object r1 = r1.a()
            if (r1 == 0) goto L_0x0033
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r6.L$0 = r2
            r6.L$1 = r0
            r6.label = r5
            java.lang.Object r1 = r1.y(r6)
            if (r1 != r4) goto L_0x007b
            r3 = r4
            goto L_0x0033
        L_0x0065:
            com.fossil.El7.b(r7)
            com.mapped.Il6 r0 = r6.p$
            com.portfolio.platform.data.source.SummariesRepository$fetchActivityStatistic$Anon2$response$Anon1_Level2 r1 = new com.portfolio.platform.data.source.SummariesRepository$fetchActivityStatistic$Anon2$response$Anon1_Level2
            r1.<init>(r6, r3)
            r6.L$0 = r0
            r6.label = r2
            java.lang.Object r1 = com.portfolio.platform.response.ResponseKt.d(r1, r6)
            if (r1 != r4) goto L_0x00b9
            r3 = r4
            goto L_0x0033
        L_0x007b:
            r2 = r0
            goto L_0x001c
        L_0x007d:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x0033
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "getActivityStatisticAwait - Failure -- code="
            r2.append(r4)
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            int r4 = r0.a()
            r2.append(r4)
            java.lang.String r4 = ", message="
            r2.append(r4)
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00b7
            java.lang.String r0 = r0.getMessage()
        L_0x00a9:
            r2.append(r0)
            java.lang.String r0 = "SummariesRepository"
            java.lang.String r2 = r2.toString()
            r1.e(r0, r2)
            goto L_0x0033
        L_0x00b7:
            r0 = r3
            goto L_0x00a9
        L_0x00b9:
            r2 = r0
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository$fetchActivityStatistic$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
