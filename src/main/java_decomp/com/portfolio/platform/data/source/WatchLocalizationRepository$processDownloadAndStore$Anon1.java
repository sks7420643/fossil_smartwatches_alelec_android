package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Kq5;
import com.fossil.W18;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.FileHelper;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.response.ResponseKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.WatchLocalizationRepository$processDownloadAndStore$1", f = "WatchLocalizationRepository.kt", l = {65}, m = "invokeSuspend")
public final class WatchLocalizationRepository$processDownloadAndStore$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super String>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $path;
    @DexIgnore
    public /* final */ /* synthetic */ String $url;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchLocalizationRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchLocalizationRepository$processDownloadAndStore$Anon1(WatchLocalizationRepository watchLocalizationRepository, String str, String str2, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = watchLocalizationRepository;
        this.$url = str;
        this.$path = str2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        WatchLocalizationRepository$processDownloadAndStore$Anon1 watchLocalizationRepository$processDownloadAndStore$Anon1 = new WatchLocalizationRepository$processDownloadAndStore$Anon1(this.this$0, this.$url, this.$path, xe6);
        watchLocalizationRepository$processDownloadAndStore$Anon1.p$ = (Il6) obj;
        throw null;
        //return watchLocalizationRepository$processDownloadAndStore$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super String> xe6) {
        throw null;
        //return ((WatchLocalizationRepository$processDownloadAndStore$Anon1) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d;
        Object d2 = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2 watchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2 = new WatchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2(this, null);
            this.L$0 = il6;
            this.label = 1;
            d = ResponseKt.d(watchLocalizationRepository$processDownloadAndStore$Anon1$repo$Anon1_Level2, this);
            if (d == d2) {
                return d2;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            d = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Ap4 ap4 = (Ap4) d;
        if (ap4 instanceof Kq5) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.this$0.TAG;
            local.d(str, "start storing for url: " + this.$url + " to path: " + this.$path);
            FileHelper fileHelper = FileHelper.a;
            Object a2 = ((Kq5) ap4).a();
            if (a2 == null) {
                Wg6.i();
                throw null;
            } else if (fileHelper.g((W18) a2, this.$path)) {
                return this.$path;
            } else {
                return null;
            }
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.this$0.TAG;
            local2.d(str2, "download: " + this.$url + " | FAILED");
            return null;
        }
    }
}
