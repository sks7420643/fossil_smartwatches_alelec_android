package com.portfolio.platform.data.source.local.sleep;

import com.fossil.Ko7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.PagingRequestHelper;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.util.Date;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1", f = "SleepSummaryLocalDataSource.kt", l = {DateTimeConstants.HOURS_PER_WEEK, 179, 179}, m = "invokeSuspend")
public final class SleepSummaryLocalDataSource$loadData$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.Bi.Aii $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.Di $requestType;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public Object L$7;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummaryLocalDataSource$loadData$Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource, Date date, Date date2, PagingRequestHelper.Di di, PagingRequestHelper.Bi.Aii aii, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = sleepSummaryLocalDataSource;
        this.$startDate = date;
        this.$endDate = date2;
        this.$requestType = di;
        this.$helperCallback = aii;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SleepSummaryLocalDataSource$loadData$Anon1 sleepSummaryLocalDataSource$loadData$Anon1 = new SleepSummaryLocalDataSource$loadData$Anon1(this.this$0, this.$startDate, this.$endDate, this.$requestType, this.$helperCallback, xe6);
        sleepSummaryLocalDataSource$loadData$Anon1.p$ = (Il6) obj;
        throw null;
        //return sleepSummaryLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((SleepSummaryLocalDataSource$loadData$Anon1) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x011a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x011e  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r14) {
        /*
        // Method dump skipped, instructions count: 293
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
