package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.model.QuickResponseSender;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ QuickResponseMessageDao mQRMessageDao;
    @DexIgnore
    public /* final */ QuickResponseSenderDao mQuickResponseSenderDao;
    @DexIgnore
    public LiveData<List<QuickResponseMessage>> mResponseMessages;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return QuickResponseRepository.TAG;
        }
    }

    /*
    static {
        String simpleName = QuickResponseRepository.class.getSimpleName();
        Wg6.b(simpleName, "QuickResponseRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public QuickResponseRepository(QuickResponseMessageDao quickResponseMessageDao, QuickResponseSenderDao quickResponseSenderDao) {
        Wg6.c(quickResponseMessageDao, "mQRMessageDao");
        Wg6.c(quickResponseSenderDao, "mQuickResponseSenderDao");
        this.mQRMessageDao = quickResponseMessageDao;
        this.mQuickResponseSenderDao = quickResponseSenderDao;
        this.mResponseMessages = quickResponseMessageDao.getAllResponse();
    }

    @DexIgnore
    public final List<QuickResponseMessage> getAllQuickResponse() {
        return this.mQRMessageDao.getAllRawResponse();
    }

    @DexIgnore
    public final LiveData<List<QuickResponseMessage>> getAllQuickResponseLiveData() {
        return this.mResponseMessages;
    }

    @DexIgnore
    public final QuickResponseSender getQuickResponseSender(int i) {
        return this.mQuickResponseSenderDao.getQuickResponseSenderById(i);
    }

    @DexIgnore
    public final void insertQR(QuickResponseMessage quickResponseMessage) {
        Wg6.c(quickResponseMessage, "qr");
        this.mQRMessageDao.insertResponse(quickResponseMessage);
    }

    @DexIgnore
    public final void insertQRs(List<QuickResponseMessage> list) {
        Wg6.c(list, "qrs");
        this.mQRMessageDao.insertResponses(list);
    }

    @DexIgnore
    public final void removeAll() {
        this.mQRMessageDao.removeAll();
    }

    @DexIgnore
    public final void removeQRById(int i) {
        this.mQRMessageDao.removeById(i);
    }

    @DexIgnore
    public final void updateQR(QuickResponseMessage quickResponseMessage) {
        Wg6.c(quickResponseMessage, "qr");
        this.mQRMessageDao.update(quickResponseMessage);
    }

    @DexIgnore
    public final void updateQR(List<QuickResponseMessage> list) {
        Wg6.c(list, "qr");
        this.mQRMessageDao.update(list);
    }

    @DexIgnore
    public final long upsertQuickResponseSender(QuickResponseSender quickResponseSender) {
        Wg6.c(quickResponseSender, "quickResponseSender");
        return this.mQuickResponseSenderDao.insertQuickResponseSender(quickResponseSender);
    }
}
