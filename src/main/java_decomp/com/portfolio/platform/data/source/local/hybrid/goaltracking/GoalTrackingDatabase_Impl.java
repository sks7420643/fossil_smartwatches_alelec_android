package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.Ex0;
import com.fossil.Hw0;
import com.fossil.Ix0;
import com.fossil.Lx0;
import com.fossil.Nw0;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import com.mapped.Ji;
import com.mapped.Oh;
import com.mapped.Qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDatabase_Impl extends GoalTrackingDatabase {
    @DexIgnore
    public volatile GoalTrackingDao _goalTrackingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Qh.Ai {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void createAllTables(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `goalTrackingRaw` (`pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `trackedAt` TEXT NOT NULL, `timezoneOffsetInSecond` INTEGER NOT NULL, `date` TEXT NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `goalTrackingDay` (`pinType` INTEGER NOT NULL, `date` TEXT NOT NULL, `totalTracked` INTEGER NOT NULL, `goalTarget` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `updatedAt` INTEGER NOT NULL, PRIMARY KEY(`date`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `goalSetting` (`id` INTEGER NOT NULL, `currentTarget` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'ac5e072da94cafa67a74f62806ddb95f')");
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void dropAllTables(Lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `goalTrackingRaw`");
            lx0.execSQL("DROP TABLE IF EXISTS `goalTrackingDay`");
            lx0.execSQL("DROP TABLE IF EXISTS `goalSetting`");
            if (GoalTrackingDatabase_Impl.this.mCallbacks != null) {
                int size = GoalTrackingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) GoalTrackingDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onCreate(Lx0 lx0) {
            if (GoalTrackingDatabase_Impl.this.mCallbacks != null) {
                int size = GoalTrackingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) GoalTrackingDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onOpen(Lx0 lx0) {
            GoalTrackingDatabase_Impl.this.mDatabase = lx0;
            GoalTrackingDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (GoalTrackingDatabase_Impl.this.mCallbacks != null) {
                int size = GoalTrackingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) GoalTrackingDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPostMigrate(Lx0 lx0) {
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPreMigrate(Lx0 lx0) {
            Ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public Qh.Bi onValidateSchema(Lx0 lx0) {
            HashMap hashMap = new HashMap(7);
            hashMap.put("pinType", new Ix0.Ai("pinType", "INTEGER", true, 0, null, 1));
            hashMap.put("id", new Ix0.Ai("id", "TEXT", true, 1, null, 1));
            hashMap.put(GoalTrackingEvent.COLUMN_TRACKED_AT, new Ix0.Ai(GoalTrackingEvent.COLUMN_TRACKED_AT, "TEXT", true, 0, null, 1));
            hashMap.put("timezoneOffsetInSecond", new Ix0.Ai("timezoneOffsetInSecond", "INTEGER", true, 0, null, 1));
            hashMap.put("date", new Ix0.Ai("date", "TEXT", true, 0, null, 1));
            hashMap.put("createdAt", new Ix0.Ai("createdAt", "INTEGER", true, 0, null, 1));
            hashMap.put("updatedAt", new Ix0.Ai("updatedAt", "INTEGER", true, 0, null, 1));
            Ix0 ix0 = new Ix0("goalTrackingRaw", hashMap, new HashSet(0), new HashSet(0));
            Ix0 a2 = Ix0.a(lx0, "goalTrackingRaw");
            if (!ix0.equals(a2)) {
                return new Qh.Bi(false, "goalTrackingRaw(com.portfolio.platform.data.model.goaltracking.GoalTrackingData).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(6);
            hashMap2.put("pinType", new Ix0.Ai("pinType", "INTEGER", true, 0, null, 1));
            hashMap2.put("date", new Ix0.Ai("date", "TEXT", true, 1, null, 1));
            hashMap2.put("totalTracked", new Ix0.Ai("totalTracked", "INTEGER", true, 0, null, 1));
            hashMap2.put("goalTarget", new Ix0.Ai("goalTarget", "INTEGER", true, 0, null, 1));
            hashMap2.put("createdAt", new Ix0.Ai("createdAt", "INTEGER", true, 0, null, 1));
            hashMap2.put("updatedAt", new Ix0.Ai("updatedAt", "INTEGER", true, 0, null, 1));
            Ix0 ix02 = new Ix0("goalTrackingDay", hashMap2, new HashSet(0), new HashSet(0));
            Ix0 a3 = Ix0.a(lx0, "goalTrackingDay");
            if (!ix02.equals(a3)) {
                return new Qh.Bi(false, "goalTrackingDay(com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
            }
            HashMap hashMap3 = new HashMap(2);
            hashMap3.put("id", new Ix0.Ai("id", "INTEGER", true, 1, null, 1));
            hashMap3.put("currentTarget", new Ix0.Ai("currentTarget", "INTEGER", true, 0, null, 1));
            Ix0 ix03 = new Ix0("goalSetting", hashMap3, new HashSet(0), new HashSet(0));
            Ix0 a4 = Ix0.a(lx0, "goalSetting");
            if (ix03.equals(a4)) {
                return new Qh.Bi(true, null);
            }
            return new Qh.Bi(false, "goalSetting(com.portfolio.platform.data.model.GoalSetting).\n Expected:\n" + ix03 + "\n Found:\n" + a4);
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public void clearAllTables() {
        super.assertNotMainThread();
        Lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `goalTrackingRaw`");
            writableDatabase.execSQL("DELETE FROM `goalTrackingDay`");
            writableDatabase.execSQL("DELETE FROM `goalSetting`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Nw0 createInvalidationTracker() {
        return new Nw0(this, new HashMap(0), new HashMap(0), "goalTrackingRaw", "goalTrackingDay", "goalSetting");
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Ji createOpenHelper(Hw0 hw0) {
        Qh qh = new Qh(hw0, new Anon1(2), "ac5e072da94cafa67a74f62806ddb95f", "56ec29011feb0e074f1d2ba7ecbcbb17");
        Ji.Bi.Aii a2 = Ji.Bi.a(hw0.b);
        a2.c(hw0.c);
        a2.b(qh);
        return hw0.a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase
    public GoalTrackingDao getGoalTrackingDao() {
        GoalTrackingDao goalTrackingDao;
        if (this._goalTrackingDao != null) {
            return this._goalTrackingDao;
        }
        synchronized (this) {
            if (this._goalTrackingDao == null) {
                this._goalTrackingDao = new GoalTrackingDao_Impl(this);
            }
            goalTrackingDao = this._goalTrackingDao;
        }
        return goalTrackingDao;
    }
}
