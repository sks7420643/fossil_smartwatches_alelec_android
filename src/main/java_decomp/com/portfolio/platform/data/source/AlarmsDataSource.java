package com.portfolio.platform.data.source;

import com.mapped.Ap4;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface AlarmsDataSource {
    @DexIgnore
    Object cleanUp();  // void declaration

    @DexIgnore
    Object deleteAlarm(Alarm alarm, Xe6<? super Ap4<Alarm>> xe6);

    @DexIgnore
    Object findAlarm(String str, Xe6<? super Alarm> xe6);

    @DexIgnore
    Object getActiveAlarms(Xe6<? super List<Alarm>> xe6);

    @DexIgnore
    Object getAlarms(Xe6<? super Ap4<List<Alarm>>> xe6);

    @DexIgnore
    Object getAllAlarmIgnorePinType(Xe6<? super List<Alarm>> xe6);

    @DexIgnore
    Object setAlarm(Alarm alarm, Xe6<? super Ap4<Alarm>> xe6);
}
