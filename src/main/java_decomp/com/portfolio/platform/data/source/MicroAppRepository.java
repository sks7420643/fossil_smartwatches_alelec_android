package com.portfolio.platform.data.source;

import com.fossil.Um5;
import com.fossil.Wt7;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ MicroAppDao mMicroAppDao;
    @DexIgnore
    public /* final */ MicroAppRemoteDataSource mMicroAppRemoteDataSource;
    @DexIgnore
    public /* final */ PortfolioApp mPortfolioApp;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppRepository.TAG;
        }
    }

    /*
    static {
        String simpleName = MicroAppRepository.class.getSimpleName();
        Wg6.b(simpleName, "MicroAppRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppRepository(MicroAppDao microAppDao, MicroAppRemoteDataSource microAppRemoteDataSource, PortfolioApp portfolioApp) {
        Wg6.c(microAppDao, "mMicroAppDao");
        Wg6.c(microAppRemoteDataSource, "mMicroAppRemoteDataSource");
        Wg6.c(portfolioApp, "mPortfolioApp");
        this.mMicroAppDao = microAppDao;
        this.mMicroAppRemoteDataSource = microAppRemoteDataSource;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mMicroAppDao.clearAllDeclarationFileTable();
        this.mMicroAppDao.clearAllMicroAppGalleryTable();
        this.mMicroAppDao.clearAllMicroAppSettingTable();
        this.mMicroAppDao.clearAllMicroAppVariantTable();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadAllMicroApp(java.lang.String r11, com.mapped.Xe6<? super com.mapped.Ap4<java.util.List<com.portfolio.platform.data.model.room.microapp.MicroApp>>> r12) {
        /*
        // Method dump skipped, instructions count: 362
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.MicroAppRepository.downloadAllMicroApp(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00a6  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadMicroAppVariant(java.lang.String r9, java.lang.String r10, java.lang.String r11, com.mapped.Xe6<? super com.mapped.Ap4<java.util.List<com.portfolio.platform.data.model.room.microapp.MicroAppVariant>>> r12) {
        /*
        // Method dump skipped, instructions count: 349
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.MicroAppRepository.downloadMicroAppVariant(java.lang.String, java.lang.String, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final List<MicroApp> getAllMicroApp(String str) {
        Wg6.c(str, "serialNumber");
        return this.mMicroAppDao.getListMicroApp(str);
    }

    @DexIgnore
    public final List<MicroApp> getMicroAppByIds(List<String> list, String str) {
        Wg6.c(list, "ids");
        Wg6.c(str, "serialNumber");
        return this.mMicroAppDao.getMicroAppByIds(list, str);
    }

    @DexIgnore
    public final MicroAppVariant getMicroAppVariant(String str, String str2, int i) {
        Wg6.c(str, "serialNumber");
        Wg6.c(str2, "microAppId");
        MicroAppVariant microAppVariant = this.mMicroAppDao.getMicroAppVariant(str2, str, i);
        if (microAppVariant != null) {
            microAppVariant.getDeclarationFileList().addAll(this.mMicroAppDao.getDeclarationFiles(str2, str, microAppVariant.getName()));
        }
        return microAppVariant;
    }

    @DexIgnore
    public final MicroAppVariant getMicroAppVariant(String str, String str2, String str3, int i) {
        Wg6.c(str, "serialNumber");
        Wg6.c(str2, "microAppId");
        Wg6.c(str3, "variantName");
        MicroAppVariant microAppVariant = this.mMicroAppDao.getMicroAppVariant(str2, str, i, str3);
        if (microAppVariant != null) {
            microAppVariant.getDeclarationFileList().addAll(this.mMicroAppDao.getDeclarationFiles(str2, str, microAppVariant.getName()));
        }
        return microAppVariant;
    }

    @DexIgnore
    public final List<MicroApp> queryMicroAppByName(String str, String str2) {
        Wg6.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        Wg6.c(str2, "serialNumber");
        ArrayList arrayList = new ArrayList();
        for (MicroApp microApp : this.mMicroAppDao.getListMicroApp(str2)) {
            String normalize = Normalizer.normalize(Um5.d(this.mPortfolioApp, microApp.getNameKey(), microApp.getName()), Normalizer.Form.NFC);
            String normalize2 = Normalizer.normalize(str, Normalizer.Form.NFC);
            Wg6.b(normalize, "name");
            Wg6.b(normalize2, "searchQuery");
            if (Wt7.u(normalize, normalize2, true)) {
                arrayList.add(microApp);
            }
        }
        return arrayList;
    }
}
