package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.El7;
import com.fossil.Hq5;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.PagingRequestHelper;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.ServerError;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$1", f = "GoalTrackingDataLocalDataSource.kt", l = {91, 94, 99}, m = "invokeSuspend")
public final class GoalTrackingDataLocalDataSource$loadData$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.Bi.Aii $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$1$1", f = "GoalTrackingDataLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource$loadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(GoalTrackingDataLocalDataSource$loadData$Anon1 goalTrackingDataLocalDataSource$loadData$Anon1, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingDataLocalDataSource$loadData$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, xe6);
            anon1_Level2.p$ = (Il6) obj;
            throw null;
            //return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Anon1_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.$helperCallback.b();
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$1$2", f = "GoalTrackingDataLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ap4 $data;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource$loadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(GoalTrackingDataLocalDataSource$loadData$Anon1 goalTrackingDataLocalDataSource$loadData$Anon1, Ap4 ap4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingDataLocalDataSource$loadData$Anon1;
            this.$data = ap4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$data, xe6);
            anon2_Level2.p$ = (Il6) obj;
            throw null;
            //return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Anon2_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                if (((Hq5) this.$data).d() != null) {
                    this.this$0.$helperCallback.a(((Hq5) this.$data).d());
                } else if (((Hq5) this.$data).c() != null) {
                    ServerError c = ((Hq5) this.$data).c();
                    PagingRequestHelper.Bi.Aii aii = this.this$0.$helperCallback;
                    String userMessage = c.getUserMessage();
                    if (userMessage == null) {
                        userMessage = c.getMessage();
                    }
                    if (userMessage == null) {
                        userMessage = "";
                    }
                    aii.a(new Throwable(userMessage));
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingDataLocalDataSource$loadData$Anon1(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource, int i, PagingRequestHelper.Bi.Aii aii, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = goalTrackingDataLocalDataSource;
        this.$offset = i;
        this.$helperCallback = aii;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        GoalTrackingDataLocalDataSource$loadData$Anon1 goalTrackingDataLocalDataSource$loadData$Anon1 = new GoalTrackingDataLocalDataSource$loadData$Anon1(this.this$0, this.$offset, this.$helperCallback, xe6);
        goalTrackingDataLocalDataSource$loadData$Anon1.p$ = (Il6) obj;
        throw null;
        //return goalTrackingDataLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((GoalTrackingDataLocalDataSource$loadData$Anon1) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00b4  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r13) {
        /*
            r12 = this;
            r11 = 3
            r10 = 2
            r4 = 1
            r7 = 0
            java.lang.Object r9 = com.fossil.Yn7.d()
            int r0 = r12.label
            if (r0 == 0) goto L_0x005a
            if (r0 == r4) goto L_0x0028
            if (r0 == r10) goto L_0x0012
            if (r0 != r11) goto L_0x0020
        L_0x0012:
            java.lang.Object r0 = r12.L$1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            java.lang.Object r0 = r12.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r13)
        L_0x001d:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x001f:
            return r0
        L_0x0020:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0028:
            java.lang.Object r0 = r12.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r13)
            r2 = r0
            r1 = r13
        L_0x0031:
            r0 = r1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x00b4
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource r1 = r12.this$0
            int r3 = com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource.access$getMOffset$p(r1)
            int r3 = r3 + 100
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource.access$setMOffset$p(r1, r3)
            com.fossil.Jx7 r1 = com.fossil.Bw7.c()
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$Anon1$Anon1_Level2 r3 = new com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$Anon1$Anon1_Level2
            r3.<init>(r12, r7)
            r12.L$0 = r2
            r12.L$1 = r0
            r12.label = r10
            java.lang.Object r0 = com.fossil.Eu7.g(r1, r3, r12)
            if (r0 != r9) goto L_0x001d
            r0 = r9
            goto L_0x001f
        L_0x005a:
            com.fossil.El7.b(r13)
            com.mapped.Il6 r8 = r12.p$
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$Companion r1 = com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource.Companion
            java.lang.String r1 = r1.getTAG()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "loadData currentDate="
            r2.append(r3)
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource r3 = r12.this$0
            java.util.Date r3 = com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource.access$getMCurrentDate$p(r3)
            r2.append(r3)
            java.lang.String r3 = ", offset="
            r2.append(r3)
            int r3 = r12.$offset
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource r0 = r12.this$0
            com.portfolio.platform.data.source.GoalTrackingRepository r0 = com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource.access$getMGoalTrackingRepository$p(r0)
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource r1 = r12.this$0
            java.util.Date r1 = com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource.access$getMCurrentDate$p(r1)
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource r2 = r12.this$0
            java.util.Date r2 = com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource.access$getMCurrentDate$p(r2)
            int r3 = r12.$offset
            r12.L$0 = r8
            r12.label = r4
            r4 = 0
            r6 = 8
            r5 = r12
            java.lang.Object r1 = com.portfolio.platform.data.source.GoalTrackingRepository.loadGoalTrackingDataList$default(r0, r1, r2, r3, r4, r5, r6, r7)
            if (r1 != r9) goto L_0x00d0
            r0 = r9
            goto L_0x001f
        L_0x00b4:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x001d
            com.fossil.Jx7 r1 = com.fossil.Bw7.c()
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$Anon1$Anon2_Level2 r3 = new com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$Anon1$Anon2_Level2
            r3.<init>(r12, r0, r7)
            r12.L$0 = r2
            r12.L$1 = r0
            r12.label = r11
            java.lang.Object r0 = com.fossil.Eu7.g(r1, r3, r12)
            if (r0 != r9) goto L_0x001d
            r0 = r9
            goto L_0x001f
        L_0x00d0:
            r2 = r8
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource$loadData$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
