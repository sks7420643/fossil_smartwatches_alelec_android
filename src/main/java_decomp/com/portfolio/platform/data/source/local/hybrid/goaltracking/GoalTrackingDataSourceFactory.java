package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.MutableLiveData;
import com.mapped.PagingRequestHelper;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Xe;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataSourceFactory extends Xe.Bi<Long, GoalTrackingData> {
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public GoalTrackingDataLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ U04 mAppExecutors;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public /* final */ PagingRequestHelper.Ai mListener;
    @DexIgnore
    public /* final */ MutableLiveData<GoalTrackingDataLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public GoalTrackingDataSourceFactory(GoalTrackingRepository goalTrackingRepository, GoalTrackingDatabase goalTrackingDatabase, Date date, U04 u04, PagingRequestHelper.Ai ai) {
        Wg6.c(goalTrackingRepository, "mGoalTrackingRepository");
        Wg6.c(goalTrackingDatabase, "mGoalTrackingDatabase");
        Wg6.c(date, "currentDate");
        Wg6.c(u04, "mAppExecutors");
        Wg6.c(ai, "mListener");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.currentDate = date;
        this.mAppExecutors = u04;
        this.mListener = ai;
    }

    @DexIgnore
    @Override // com.mapped.Xe.Bi
    public Xe<Long, GoalTrackingData> create() {
        GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource = new GoalTrackingDataLocalDataSource(this.mGoalTrackingRepository, this.mGoalTrackingDatabase, this.currentDate, this.mAppExecutors, this.mListener);
        this.localDataSource = goalTrackingDataLocalDataSource;
        this.sourceLiveData.l(goalTrackingDataLocalDataSource);
        GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource2 = this.localDataSource;
        if (goalTrackingDataLocalDataSource2 != null) {
            return goalTrackingDataLocalDataSource2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final GoalTrackingDataLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<GoalTrackingDataLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource) {
        this.localDataSource = goalTrackingDataLocalDataSource;
    }
}
