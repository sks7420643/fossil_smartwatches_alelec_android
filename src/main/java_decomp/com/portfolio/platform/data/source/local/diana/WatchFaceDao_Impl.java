package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.fossil.P05;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceDao_Impl implements WatchFaceDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<WatchFace> __insertionAdapterOfWatchFace;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteAll;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteAllBackgroundWatchface;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteWatchFace;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteWatchFacesWithSerial;
    @DexIgnore
    public /* final */ P05 __watchFaceTypeConverter; // = new P05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<WatchFace> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, WatchFace watchFace) {
            if (watchFace.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, watchFace.getId());
            }
            if (watchFace.getName() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, watchFace.getName());
            }
            String d = WatchFaceDao_Impl.this.__watchFaceTypeConverter.d(watchFace.getRingStyleItems());
            if (d == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, d);
            }
            String a2 = WatchFaceDao_Impl.this.__watchFaceTypeConverter.a(watchFace.getBackground());
            if (a2 == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, a2);
            }
            if (watchFace.getPreviewUrl() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, watchFace.getPreviewUrl());
            }
            if (watchFace.getSerial() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, watchFace.getSerial());
            }
            mi.bindLong(7, (long) watchFace.getWatchFaceType());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, WatchFace watchFace) {
            bind(mi, watchFace);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watch_face` (`id`,`name`,`ringStyleItems`,`background`,`previewUrl`,`serial`,`watchFaceType`) VALUES (?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM watch_face WHERE serial = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM watch_face WHERE id = ? and watchFaceType = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends Vh {
        @DexIgnore
        public Anon4(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM watch_face WHERE watchFaceType = 0";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends Vh {
        @DexIgnore
        public Anon5(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM watch_face";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 implements Callable<List<WatchFace>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon6(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WatchFace> call() throws Exception {
            Cursor b = Ex0.b(WatchFaceDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "id");
                int c2 = Dx0.c(b, "name");
                int c3 = Dx0.c(b, "ringStyleItems");
                int c4 = Dx0.c(b, Explore.COLUMN_BACKGROUND);
                int c5 = Dx0.c(b, "previewUrl");
                int c6 = Dx0.c(b, "serial");
                int c7 = Dx0.c(b, "watchFaceType");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new WatchFace(b.getString(c), b.getString(c2), WatchFaceDao_Impl.this.__watchFaceTypeConverter.c(b.getString(c3)), WatchFaceDao_Impl.this.__watchFaceTypeConverter.b(b.getString(c4)), b.getString(c5), b.getString(c6), b.getInt(c7)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements Callable<List<WatchFace>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon7(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<WatchFace> call() throws Exception {
            Cursor b = Ex0.b(WatchFaceDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "id");
                int c2 = Dx0.c(b, "name");
                int c3 = Dx0.c(b, "ringStyleItems");
                int c4 = Dx0.c(b, Explore.COLUMN_BACKGROUND);
                int c5 = Dx0.c(b, "previewUrl");
                int c6 = Dx0.c(b, "serial");
                int c7 = Dx0.c(b, "watchFaceType");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new WatchFace(b.getString(c), b.getString(c2), WatchFaceDao_Impl.this.__watchFaceTypeConverter.c(b.getString(c3)), WatchFaceDao_Impl.this.__watchFaceTypeConverter.b(b.getString(c4)), b.getString(c5), b.getString(c6), b.getInt(c7)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public WatchFaceDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfWatchFace = new Anon1(oh);
        this.__preparedStmtOfDeleteWatchFacesWithSerial = new Anon2(oh);
        this.__preparedStmtOfDeleteWatchFace = new Anon3(oh);
        this.__preparedStmtOfDeleteAllBackgroundWatchface = new Anon4(oh);
        this.__preparedStmtOfDeleteAll = new Anon5(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void deleteAll() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void deleteAllBackgroundWatchface() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteAllBackgroundWatchface.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllBackgroundWatchface.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void deleteWatchFace(String str, int i) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteWatchFace.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        acquire.bindLong(2, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchFace.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void deleteWatchFacesWithSerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteWatchFacesWithSerial.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchFacesWithSerial.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public List<WatchFace> getAllWatchFaces() {
        Rh f = Rh.f("SELECT * FROM watch_face", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "name");
            int c3 = Dx0.c(b, "ringStyleItems");
            int c4 = Dx0.c(b, Explore.COLUMN_BACKGROUND);
            int c5 = Dx0.c(b, "previewUrl");
            int c6 = Dx0.c(b, "serial");
            int c7 = Dx0.c(b, "watchFaceType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WatchFace(b.getString(c), b.getString(c2), this.__watchFaceTypeConverter.c(b.getString(c3)), this.__watchFaceTypeConverter.b(b.getString(c4)), b.getString(c5), b.getString(c6), b.getInt(c7)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public String getLatestWatchFaceName(int i) {
        String str = null;
        Rh f = Rh.f("SELECT name FROM watch_face WHERE watchFaceType = ? ORDER BY id DESC LIMIT 1", 1);
        f.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            if (b.moveToFirst()) {
                str = b.getString(0);
            }
            return str;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public WatchFace getWatchFaceWithId(String str) {
        WatchFace watchFace = null;
        Rh f = Rh.f("SELECT * FROM watch_face WHERE id = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "name");
            int c3 = Dx0.c(b, "ringStyleItems");
            int c4 = Dx0.c(b, Explore.COLUMN_BACKGROUND);
            int c5 = Dx0.c(b, "previewUrl");
            int c6 = Dx0.c(b, "serial");
            int c7 = Dx0.c(b, "watchFaceType");
            if (b.moveToFirst()) {
                watchFace = new WatchFace(b.getString(c), b.getString(c2), this.__watchFaceTypeConverter.c(b.getString(c3)), this.__watchFaceTypeConverter.b(b.getString(c4)), b.getString(c5), b.getString(c6), b.getInt(c7));
            }
            return watchFace;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public LiveData<List<WatchFace>> getWatchFacesLiveData(String str) {
        Rh f = Rh.f("SELECT * FROM watch_face WHERE serial = ? ", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon6 anon6 = new Anon6(f);
        return invalidationTracker.d(new String[]{"watch_face"}, false, anon6);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public LiveData<List<WatchFace>> getWatchFacesLiveDataWithType(String str, int i) {
        Rh f = Rh.f("SELECT * FROM watch_face WHERE serial = ? and watchFaceType = ? ", 2);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        f.bindLong(2, (long) i);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon7 anon7 = new Anon7(f);
        return invalidationTracker.d(new String[]{"watch_face"}, false, anon7);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public List<WatchFace> getWatchFacesWithSerial(String str) {
        Rh f = Rh.f("SELECT * FROM watch_face WHERE serial = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "name");
            int c3 = Dx0.c(b, "ringStyleItems");
            int c4 = Dx0.c(b, Explore.COLUMN_BACKGROUND);
            int c5 = Dx0.c(b, "previewUrl");
            int c6 = Dx0.c(b, "serial");
            int c7 = Dx0.c(b, "watchFaceType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WatchFace(b.getString(c), b.getString(c2), this.__watchFaceTypeConverter.c(b.getString(c3)), this.__watchFaceTypeConverter.b(b.getString(c4)), b.getString(c5), b.getString(c6), b.getInt(c7)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public List<WatchFace> getWatchFacesWithType(int i) {
        Rh f = Rh.f("SELECT * FROM watch_face WHERE watchFaceType = ?", 1);
        f.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "name");
            int c3 = Dx0.c(b, "ringStyleItems");
            int c4 = Dx0.c(b, Explore.COLUMN_BACKGROUND);
            int c5 = Dx0.c(b, "previewUrl");
            int c6 = Dx0.c(b, "serial");
            int c7 = Dx0.c(b, "watchFaceType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new WatchFace(b.getString(c), b.getString(c2), this.__watchFaceTypeConverter.c(b.getString(c3)), this.__watchFaceTypeConverter.b(b.getString(c4)), b.getString(c5), b.getString(c6), b.getInt(c7)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void insertAllWatchFaces(List<WatchFace> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchFace.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.WatchFaceDao
    public void insertWatchFace(WatchFace watchFace) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchFace.insert((Hh<WatchFace>) watchFace);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
