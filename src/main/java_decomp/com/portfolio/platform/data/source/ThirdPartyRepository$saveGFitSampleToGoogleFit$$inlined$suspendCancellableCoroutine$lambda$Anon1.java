package com.portfolio.platform.data.source;

import com.fossil.Bw7;
import com.fossil.Dl7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Rh2;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hh6;
import com.mapped.Il6;
import com.mapped.Lk6;
import com.mapped.Mc3;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1<TResult> implements Mc3<Void> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Lk6 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Hh6 $count;
    @DexIgnore
    public /* final */ /* synthetic */ Hh6 $countSizeOfLists$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $gFitSampleList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Rh2 $historyClient$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $sampleList;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfDataSetList;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfListsOfGFitSampleList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ ThirdPartyDatabase $db;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(ThirdPartyDatabase thirdPartyDatabase, Anon1_Level2 anon1_Level2) {
                this.$db = thirdPartyDatabase;
                this.this$0 = anon1_Level2;
            }

            @DexIgnore
            public final void run() {
                this.$db.getGFitSampleDao().deleteListGFitSample(this.this$0.this$0.$sampleList);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = thirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, xe6);
            anon1_Level2.p$ = (Il6) obj;
            throw null;
            //return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Anon1_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object F;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                EncryptedDatabaseManager encryptedDatabaseManager = EncryptedDatabaseManager.j;
                this.L$0 = il6;
                this.label = 1;
                F = encryptedDatabaseManager.F(this);
                if (F == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                F = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ThirdPartyDatabase thirdPartyDatabase = (ThirdPartyDatabase) F;
            thirdPartyDatabase.runInTransaction(new Anon1_Level3(thirdPartyDatabase, this));
            return Cd6.a;
        }
    }

    @DexIgnore
    public ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(Hh6 hh6, int i, List list, Rh2 rh2, Hh6 hh62, Lk6 lk6, int i2, ThirdPartyRepository thirdPartyRepository, List list2, String str) {
        this.$count = hh6;
        this.$sizeOfDataSetList = i;
        this.$sampleList = list;
        this.$historyClient$inlined = rh2;
        this.$countSizeOfLists$inlined = hh62;
        this.$continuation$inlined = lk6;
        this.$sizeOfListsOfGFitSampleList$inlined = i2;
        this.this$0 = thirdPartyRepository;
        this.$gFitSampleList$inlined = list2;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Mc3
    public /* bridge */ /* synthetic */ void onSuccess(Void r1) {
        onSuccess(r1);
    }

    @DexIgnore
    public final void onSuccess(Void r7) {
        FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "saveGFitSampleToGoogleFit - Success");
        Hh6 hh6 = this.$count;
        int i = hh6.element + 1;
        hh6.element = i;
        if (i >= this.$sizeOfDataSetList) {
            Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Anon1_Level2(this, null), 3, null);
        }
        Hh6 hh62 = this.$countSizeOfLists$inlined;
        int i2 = hh62.element + 1;
        hh62.element = i2;
        if (i2 >= this.$sizeOfListsOfGFitSampleList$inlined * this.$sizeOfDataSetList && this.$continuation$inlined.isActive()) {
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveGFitSampleToGoogleFit - Success");
            Lk6 lk6 = this.$continuation$inlined;
            Dl7.Ai ai = Dl7.Companion;
            lk6.resumeWith(Dl7.constructor-impl(null));
        }
    }
}
