package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import com.fossil.Xt4;
import com.fossil.Ys4;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory implements Factory<Xt4> {
    @DexIgnore
    public /* final */ Provider<Ys4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<Ys4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<Ys4> provider) {
        return new PortfolioDatabaseModule_ProvidesSocialFriendLocalFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static Xt4 providesSocialFriendLocal(PortfolioDatabaseModule portfolioDatabaseModule, Ys4 ys4) {
        Xt4 providesSocialFriendLocal = portfolioDatabaseModule.providesSocialFriendLocal(ys4);
        Lk7.c(providesSocialFriendLocal, "Cannot return null from a non-@Nullable @Provides method");
        return providesSocialFriendLocal;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public Xt4 get() {
        return providesSocialFriendLocal(this.module, this.daoProvider.get());
    }
}
