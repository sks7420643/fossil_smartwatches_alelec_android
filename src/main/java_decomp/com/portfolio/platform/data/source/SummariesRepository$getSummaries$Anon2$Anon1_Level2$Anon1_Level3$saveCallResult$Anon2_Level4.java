package com.portfolio.platform.data.source;

import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.FitnessDayData;
import com.portfolio.platform.data.source.remote.ApiResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 extends TypeToken<ApiResponse<FitnessDayData>> {
}
