package com.portfolio.platform.data.source.local.quickresponse;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.QuickResponseSender;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseSenderDao_Impl extends QuickResponseSenderDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<QuickResponseSender> __insertionAdapterOfQuickResponseSender;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfRemoveById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<QuickResponseSender> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, QuickResponseSender quickResponseSender) {
            mi.bindLong(1, (long) quickResponseSender.getId());
            if (quickResponseSender.getContent() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, quickResponseSender.getContent());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, QuickResponseSender quickResponseSender) {
            bind(mi, quickResponseSender);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `quickResponseSender` (`id`,`content`) VALUES (nullif(?, 0),?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM quickResponseSender WHERE id = ?";
        }
    }

    @DexIgnore
    public QuickResponseSenderDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfQuickResponseSender = new Anon1(oh);
        this.__preparedStmtOfRemoveById = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao
    public List<QuickResponseSender> getAllQuickResponseSender() {
        Rh f = Rh.f("SELECT * FROM quickResponseSender", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "content");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                QuickResponseSender quickResponseSender = new QuickResponseSender(b.getString(c2));
                quickResponseSender.setId(b.getInt(c));
                arrayList.add(quickResponseSender);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao
    public QuickResponseSender getQuickResponseSenderById(int i) {
        QuickResponseSender quickResponseSender = null;
        Rh f = Rh.f("SELECT * FROM quickResponseSender WHERE id = ?", 1);
        f.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "content");
            if (b.moveToFirst()) {
                quickResponseSender = new QuickResponseSender(b.getString(c2));
                quickResponseSender.setId(b.getInt(c));
            }
            return quickResponseSender;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao
    public long insertQuickResponseSender(QuickResponseSender quickResponseSender) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfQuickResponseSender.insertAndReturnId(quickResponseSender);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao
    public void removeById(int i) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfRemoveById.acquire();
        acquire.bindLong(1, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveById.release(acquire);
        }
    }
}
