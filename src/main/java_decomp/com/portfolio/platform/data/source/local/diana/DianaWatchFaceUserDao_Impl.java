package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceOrder;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceUserDao_Impl implements DianaWatchFaceUserDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<DianaWatchFaceUser> __insertionAdapterOfDianaWatchFaceUser;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteAll;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteDianaWatchFaceUserById;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfUpdatePinType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<DianaWatchFaceUser> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, DianaWatchFaceUser dianaWatchFaceUser) {
            if (dianaWatchFaceUser.getCreatedAt() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, dianaWatchFaceUser.getCreatedAt());
            }
            if (dianaWatchFaceUser.getUpdatedAt() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, dianaWatchFaceUser.getUpdatedAt());
            }
            mi.bindLong(3, (long) dianaWatchFaceUser.getPinType());
            if (dianaWatchFaceUser.getId() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, dianaWatchFaceUser.getId());
            }
            if (dianaWatchFaceUser.getDownloadURL() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, dianaWatchFaceUser.getDownloadURL());
            }
            if (dianaWatchFaceUser.getName() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, dianaWatchFaceUser.getName());
            }
            if (dianaWatchFaceUser.getChecksum() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, dianaWatchFaceUser.getChecksum());
            }
            if (dianaWatchFaceUser.getPreviewURL() == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, dianaWatchFaceUser.getPreviewURL());
            }
            if (dianaWatchFaceUser.getUid() == null) {
                mi.bindNull(9);
            } else {
                mi.bindString(9, dianaWatchFaceUser.getUid());
            }
            DianaWatchFaceOrder order = dianaWatchFaceUser.getOrder();
            if (order != null) {
                if (order.getOrderId() == null) {
                    mi.bindNull(10);
                } else {
                    mi.bindString(10, order.getOrderId());
                }
                if (order.getWatchFaceId() == null) {
                    mi.bindNull(11);
                } else {
                    mi.bindString(11, order.getWatchFaceId());
                }
                if (order.getPackageVersion() == null) {
                    mi.bindNull(12);
                } else {
                    mi.bindString(12, order.getPackageVersion());
                }
            } else {
                mi.bindNull(10);
                mi.bindNull(11);
                mi.bindNull(12);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, DianaWatchFaceUser dianaWatchFaceUser) {
            bind(mi, dianaWatchFaceUser);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaWatchfaceUser` (`createdAt`,`updatedAt`,`pinType`,`id`,`downloadURL`,`name`,`checksum`,`previewURL`,`uid`,`orderId`,`watchFaceId`,`packageVersion`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "UPDATE dianaWatchfaceUser SET pinType=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM dianaWatchfaceUser WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends Vh {
        @DexIgnore
        public Anon4(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM dianaWatchfaceUser";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 implements Callable<List<DianaWatchFaceUser>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon5(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DianaWatchFaceUser> call() throws Exception {
            Cursor b = Ex0.b(DianaWatchFaceUserDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "createdAt");
                int c2 = Dx0.c(b, "updatedAt");
                int c3 = Dx0.c(b, "pinType");
                int c4 = Dx0.c(b, "id");
                int c5 = Dx0.c(b, "downloadURL");
                int c6 = Dx0.c(b, "name");
                int c7 = Dx0.c(b, "checksum");
                int c8 = Dx0.c(b, "previewURL");
                int c9 = Dx0.c(b, "uid");
                int c10 = Dx0.c(b, "orderId");
                int c11 = Dx0.c(b, "watchFaceId");
                int c12 = Dx0.c(b, "packageVersion");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    String string = b.getString(c4);
                    String string2 = b.getString(c5);
                    String string3 = b.getString(c6);
                    String string4 = b.getString(c7);
                    String string5 = b.getString(c8);
                    String string6 = b.getString(c9);
                    DianaWatchFaceOrder dianaWatchFaceOrder = (!b.isNull(c10) || !b.isNull(c11) || !b.isNull(c12)) ? new DianaWatchFaceOrder(b.getString(c10), b.getString(c11), b.getString(c12)) : null;
                    DianaWatchFaceUser dianaWatchFaceUser = new DianaWatchFaceUser(string, string2, string3, string4, string5, string6);
                    dianaWatchFaceUser.setCreatedAt(b.getString(c));
                    dianaWatchFaceUser.setUpdatedAt(b.getString(c2));
                    dianaWatchFaceUser.setPinType(b.getInt(c3));
                    dianaWatchFaceUser.setOrder(dianaWatchFaceOrder);
                    arrayList.add(dianaWatchFaceUser);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public DianaWatchFaceUserDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfDianaWatchFaceUser = new Anon1(oh);
        this.__preparedStmtOfUpdatePinType = new Anon2(oh);
        this.__preparedStmtOfDeleteDianaWatchFaceUserById = new Anon3(oh);
        this.__preparedStmtOfDeleteAll = new Anon4(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public void deleteAll() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public void deleteDianaWatchFaceUserById(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteDianaWatchFaceUserById.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteDianaWatchFaceUserById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public List<DianaWatchFaceUser> getAllDianaWatchFaceUser() {
        Rh f = Rh.f("SELECT * FROM dianaWatchfaceUser WHERE pinType <> 3", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "downloadURL");
            int c6 = Dx0.c(b, "name");
            int c7 = Dx0.c(b, "checksum");
            int c8 = Dx0.c(b, "previewURL");
            int c9 = Dx0.c(b, "uid");
            int c10 = Dx0.c(b, "orderId");
            int c11 = Dx0.c(b, "watchFaceId");
            int c12 = Dx0.c(b, "packageVersion");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                String string = b.getString(c4);
                String string2 = b.getString(c5);
                String string3 = b.getString(c6);
                String string4 = b.getString(c7);
                String string5 = b.getString(c8);
                String string6 = b.getString(c9);
                DianaWatchFaceOrder dianaWatchFaceOrder = (!b.isNull(c10) || !b.isNull(c11) || !b.isNull(c12)) ? new DianaWatchFaceOrder(b.getString(c10), b.getString(c11), b.getString(c12)) : null;
                DianaWatchFaceUser dianaWatchFaceUser = new DianaWatchFaceUser(string, string2, string3, string4, string5, string6);
                dianaWatchFaceUser.setCreatedAt(b.getString(c));
                dianaWatchFaceUser.setUpdatedAt(b.getString(c2));
                dianaWatchFaceUser.setPinType(b.getInt(c3));
                dianaWatchFaceUser.setOrder(dianaWatchFaceOrder);
                arrayList.add(dianaWatchFaceUser);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public LiveData<List<DianaWatchFaceUser>> getAllDianaWatchFaceUserLiveData() {
        Rh f = Rh.f("SELECT * FROM dianaWatchfaceUser WHERE pinType <> 3", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon5 anon5 = new Anon5(f);
        return invalidationTracker.d(new String[]{"dianaWatchfaceUser"}, false, anon5);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public DianaWatchFaceUser getDianaWatchFaceUserById(String str) {
        Rh f = Rh.f("SELECT * FROM dianaWatchfaceUser WHERE id=? AND pinType <> 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        DianaWatchFaceUser dianaWatchFaceUser = null;
        DianaWatchFaceOrder dianaWatchFaceOrder = null;
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "downloadURL");
            int c6 = Dx0.c(b, "name");
            int c7 = Dx0.c(b, "checksum");
            int c8 = Dx0.c(b, "previewURL");
            int c9 = Dx0.c(b, "uid");
            int c10 = Dx0.c(b, "orderId");
            int c11 = Dx0.c(b, "watchFaceId");
            int c12 = Dx0.c(b, "packageVersion");
            if (b.moveToFirst()) {
                String string = b.getString(c4);
                String string2 = b.getString(c5);
                String string3 = b.getString(c6);
                String string4 = b.getString(c7);
                String string5 = b.getString(c8);
                String string6 = b.getString(c9);
                if (!b.isNull(c10) || !b.isNull(c11) || !b.isNull(c12)) {
                    dianaWatchFaceOrder = new DianaWatchFaceOrder(b.getString(c10), b.getString(c11), b.getString(c12));
                }
                dianaWatchFaceUser = new DianaWatchFaceUser(string, string2, string3, string4, string5, string6);
                dianaWatchFaceUser.setCreatedAt(b.getString(c));
                dianaWatchFaceUser.setUpdatedAt(b.getString(c2));
                dianaWatchFaceUser.setPinType(b.getInt(c3));
                dianaWatchFaceUser.setOrder(dianaWatchFaceOrder);
            }
            return dianaWatchFaceUser;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public DianaWatchFaceUser getDianaWatchFaceUserByOrderId(String str) {
        Rh f = Rh.f("SELECT * FROM dianaWatchfaceUser WHERE orderId=? AND pinType <> 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        DianaWatchFaceUser dianaWatchFaceUser = null;
        DianaWatchFaceOrder dianaWatchFaceOrder = null;
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "downloadURL");
            int c6 = Dx0.c(b, "name");
            int c7 = Dx0.c(b, "checksum");
            int c8 = Dx0.c(b, "previewURL");
            int c9 = Dx0.c(b, "uid");
            int c10 = Dx0.c(b, "orderId");
            int c11 = Dx0.c(b, "watchFaceId");
            int c12 = Dx0.c(b, "packageVersion");
            if (b.moveToFirst()) {
                String string = b.getString(c4);
                String string2 = b.getString(c5);
                String string3 = b.getString(c6);
                String string4 = b.getString(c7);
                String string5 = b.getString(c8);
                String string6 = b.getString(c9);
                if (!b.isNull(c10) || !b.isNull(c11) || !b.isNull(c12)) {
                    dianaWatchFaceOrder = new DianaWatchFaceOrder(b.getString(c10), b.getString(c11), b.getString(c12));
                }
                dianaWatchFaceUser = new DianaWatchFaceUser(string, string2, string3, string4, string5, string6);
                dianaWatchFaceUser.setCreatedAt(b.getString(c));
                dianaWatchFaceUser.setUpdatedAt(b.getString(c2));
                dianaWatchFaceUser.setPinType(b.getInt(c3));
                dianaWatchFaceUser.setOrder(dianaWatchFaceOrder);
            }
            return dianaWatchFaceUser;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public List<DianaWatchFaceUser> getPendingDianaWatchFace() {
        Rh f = Rh.f("SELECT * FROM dianaWatchfaceUser WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "downloadURL");
            int c6 = Dx0.c(b, "name");
            int c7 = Dx0.c(b, "checksum");
            int c8 = Dx0.c(b, "previewURL");
            int c9 = Dx0.c(b, "uid");
            int c10 = Dx0.c(b, "orderId");
            int c11 = Dx0.c(b, "watchFaceId");
            int c12 = Dx0.c(b, "packageVersion");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                String string = b.getString(c4);
                String string2 = b.getString(c5);
                String string3 = b.getString(c6);
                String string4 = b.getString(c7);
                String string5 = b.getString(c8);
                String string6 = b.getString(c9);
                DianaWatchFaceOrder dianaWatchFaceOrder = (!b.isNull(c10) || !b.isNull(c11) || !b.isNull(c12)) ? new DianaWatchFaceOrder(b.getString(c10), b.getString(c11), b.getString(c12)) : null;
                DianaWatchFaceUser dianaWatchFaceUser = new DianaWatchFaceUser(string, string2, string3, string4, string5, string6);
                dianaWatchFaceUser.setCreatedAt(b.getString(c));
                dianaWatchFaceUser.setUpdatedAt(b.getString(c2));
                dianaWatchFaceUser.setPinType(b.getInt(c3));
                dianaWatchFaceUser.setOrder(dianaWatchFaceOrder);
                arrayList.add(dianaWatchFaceUser);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public DianaWatchFaceUser getWatchFaceByOrderWatchFaceId(String str) {
        Rh f = Rh.f("SELECT * FROM dianaWatchfaceUser WHERE watchFaceId=? AND pinType <> 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        DianaWatchFaceUser dianaWatchFaceUser = null;
        DianaWatchFaceOrder dianaWatchFaceOrder = null;
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "downloadURL");
            int c6 = Dx0.c(b, "name");
            int c7 = Dx0.c(b, "checksum");
            int c8 = Dx0.c(b, "previewURL");
            int c9 = Dx0.c(b, "uid");
            int c10 = Dx0.c(b, "orderId");
            int c11 = Dx0.c(b, "watchFaceId");
            int c12 = Dx0.c(b, "packageVersion");
            if (b.moveToFirst()) {
                String string = b.getString(c4);
                String string2 = b.getString(c5);
                String string3 = b.getString(c6);
                String string4 = b.getString(c7);
                String string5 = b.getString(c8);
                String string6 = b.getString(c9);
                if (!b.isNull(c10) || !b.isNull(c11) || !b.isNull(c12)) {
                    dianaWatchFaceOrder = new DianaWatchFaceOrder(b.getString(c10), b.getString(c11), b.getString(c12));
                }
                dianaWatchFaceUser = new DianaWatchFaceUser(string, string2, string3, string4, string5, string6);
                dianaWatchFaceUser.setCreatedAt(b.getString(c));
                dianaWatchFaceUser.setUpdatedAt(b.getString(c2));
                dianaWatchFaceUser.setPinType(b.getInt(c3));
                dianaWatchFaceUser.setOrder(dianaWatchFaceOrder);
            }
            return dianaWatchFaceUser;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public void updatePinType(String str, int i) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfUpdatePinType.acquire();
        acquire.bindLong(1, (long) i);
        if (str == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfUpdatePinType.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public void upsertDianaWatchFaceUser(DianaWatchFaceUser dianaWatchFaceUser) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaWatchFaceUser.insert((Hh<DianaWatchFaceUser>) dianaWatchFaceUser);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao
    public void upsertDianaWatchFaceUser(List<DianaWatchFaceUser> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaWatchFaceUser.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
