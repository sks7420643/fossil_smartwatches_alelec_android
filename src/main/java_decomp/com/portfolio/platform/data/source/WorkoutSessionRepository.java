package com.portfolio.platform.data.source;

import com.fossil.Bw7;
import com.fossil.Eu7;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.PagingRequestHelper;
import com.mapped.Qg6;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDataSourceFactory;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public List<WorkoutSessionDataSourceFactory> mSourceDataFactoryList; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return WorkoutSessionRepository.TAG;
        }
    }

    /*
    static {
        String simpleName = WorkoutSessionRepository.class.getSimpleName();
        Wg6.b(simpleName, "WorkoutSessionRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public WorkoutSessionRepository(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "mApiService");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public static /* synthetic */ Object fetchWorkoutSessions$default(WorkoutSessionRepository workoutSessionRepository, Date date, Date date2, int i, int i2, Xe6 xe6, int i3, Object obj) {
        return workoutSessionRepository.fetchWorkoutSessions(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, xe6);
    }

    @DexIgnore
    public final Object cleanUp(Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new WorkoutSessionRepository$cleanUp$Anon2(this, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deleteWorkoutSession(java.lang.String r13, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Object>> r14) {
        /*
        // Method dump skipped, instructions count: 282
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSessionRepository.deleteWorkoutSession(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object fetchWorkoutSessions(Date date, Date date2, int i, int i2, Xe6<? super Ap4<ApiResponse<ServerWorkoutSession>>> xe6) {
        return Eu7.g(Bw7.b(), new WorkoutSessionRepository$fetchWorkoutSessions$Anon2(this, date, date2, i, i2, null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWorkoutSessionById(java.lang.String r6, com.mapped.Xe6<? super com.portfolio.platform.data.model.diana.workout.WorkoutSession> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessionById$Anon1
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessionById$Anon1 r0 = (com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessionById$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.WorkoutSessionRepository r1 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            com.portfolio.platform.data.source.local.diana.workout.WorkoutDao r0 = r0.getWorkoutDao()
            com.portfolio.platform.data.model.diana.workout.WorkoutSession r0 = r0.getWorkoutSessionById(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessionById$Anon1 r0 = new com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessionById$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.El7.b(r2)
            com.portfolio.platform.manager.EncryptedDatabaseManager r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.y(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSessionRepository.getWorkoutSessionById(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWorkoutSessions(java.util.Date r11, java.util.Date r12, boolean r13, com.mapped.Xe6<? super androidx.lifecycle.LiveData<com.fossil.H47<java.util.List<com.portfolio.platform.data.model.diana.workout.WorkoutSession>>>> r14) {
        /*
            r10 = this;
            r9 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon1
            if (r0 == 0) goto L_0x0038
            r0 = r14
            com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon1 r0 = (com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.label = r1
            r6 = r0
        L_0x0014:
            java.lang.Object r1 = r6.result
            java.lang.Object r7 = com.fossil.Yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x0047
            if (r0 != r9) goto L_0x003f
            boolean r0 = r6.Z$0
            java.lang.Object r0 = r6.L$2
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$1
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$0
            com.portfolio.platform.data.source.WorkoutSessionRepository r0 = (com.portfolio.platform.data.source.WorkoutSessionRepository) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0032:
            java.lang.String r1 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.mapped.Wg6.b(r0, r1)
        L_0x0037:
            return r0
        L_0x0038:
            com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon1 r0 = new com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon1
            r0.<init>(r10, r14)
            r6 = r0
            goto L_0x0014
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            com.fossil.El7.b(r1)
            com.fossil.Dv7 r8 = com.fossil.Bw7.b()
            com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon2 r0 = new com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$Anon2
            r5 = 0
            r1 = r10
            r2 = r11
            r3 = r12
            r4 = r13
            r0.<init>(r1, r2, r3, r4, r5)
            r6.L$0 = r10
            r6.L$1 = r11
            r6.L$2 = r12
            r6.Z$0 = r13
            r6.label = r9
            java.lang.Object r0 = com.fossil.Eu7.g(r8, r0, r6)
            if (r0 != r7) goto L_0x0032
            r0 = r7
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSessionRepository.getWorkoutSessions(java.util.Date, java.util.Date, boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object getWorkoutSessionsPaging(Date date, WorkoutSessionRepository workoutSessionRepository, U04 u04, PagingRequestHelper.Ai ai, Xe6<? super Listing<WorkoutSession>> xe6) {
        return Eu7.g(Bw7.c(), new WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2(this, workoutSessionRepository, date, u04, ai, null), xe6);
    }

    @DexIgnore
    public final Object insertFromDevice(List<WorkoutSession> list, Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new WorkoutSessionRepository$insertFromDevice$Anon2(list, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final Object insertWorkoutTetherScreenShot(String str, String str2, Xe6<? super Cd6> xe6) {
        return Eu7.g(Bw7.b(), new WorkoutSessionRepository$insertWorkoutTetherScreenShot$Anon2(str, str2, null), xe6);
    }

    @DexIgnore
    public final void removePagingListener() {
        for (WorkoutSessionDataSourceFactory workoutSessionDataSourceFactory : this.mSourceDataFactoryList) {
            WorkoutSessionLocalDataSource localDataSource = workoutSessionDataSourceFactory.getLocalDataSource();
            if (localDataSource != null) {
                localDataSource.removePagingObserver();
            }
        }
        this.mSourceDataFactoryList.clear();
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v30, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00e6  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x018a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object updateWorkoutSession(java.util.List<com.portfolio.platform.data.model.diana.workout.WorkoutSessionUpdateWrapper> r13, com.mapped.Xe6<? super com.fossil.Kz4<java.util.List<com.portfolio.platform.data.model.diana.workout.WorkoutSession>>> r14) {
        /*
        // Method dump skipped, instructions count: 516
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSessionRepository.updateWorkoutSession(java.util.List, com.mapped.Xe6):java.lang.Object");
    }
}
