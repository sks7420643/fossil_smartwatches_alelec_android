package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lf6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.GoalTrackingRepository$loadSummaries$response$1", f = "GoalTrackingRepository.kt", l = {248}, m = "invokeSuspend")
public final class GoalTrackingRepository$loadSummaries$response$Anon1 extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<GoalDailySummary>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$loadSummaries$response$Anon1(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, Xe6 xe6) {
        super(1, xe6);
        this.this$0 = goalTrackingRepository;
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        return new GoalTrackingRepository$loadSummaries$response$Anon1(this.this$0, this.$startDate, this.$endDate, xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public final Object invoke(Xe6<? super Q88<ApiResponse<GoalDailySummary>>> xe6) {
        throw null;
        //return ((GoalTrackingRepository$loadSummaries$response$Anon1) create(xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiServiceV2;
            String k = TimeUtils.k(this.$startDate);
            Wg6.b(k, "DateHelper.formatShortDate(startDate)");
            String k2 = TimeUtils.k(this.$endDate);
            Wg6.b(k2, "DateHelper.formatShortDate(endDate)");
            this.label = 1;
            Object goalTrackingSummaries = apiServiceV2.getGoalTrackingSummaries(k, k2, 0, 100, this);
            return goalTrackingSummaries == d ? d : goalTrackingSummaries;
        } else if (i == 1) {
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
