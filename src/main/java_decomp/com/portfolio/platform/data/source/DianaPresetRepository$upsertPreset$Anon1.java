package com.portfolio.platform.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.DianaPresetRepository", f = "DianaPresetRepository.kt", l = {91, 116}, m = "upsertPreset")
public final class DianaPresetRepository$upsertPreset$Anon1 extends Jf6 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ DianaPresetRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaPresetRepository$upsertPreset$Anon1(DianaPresetRepository dianaPresetRepository, Xe6 xe6) {
        super(xe6);
        this.this$0 = dianaPresetRepository;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.upsertPreset(null, this);
    }
}
