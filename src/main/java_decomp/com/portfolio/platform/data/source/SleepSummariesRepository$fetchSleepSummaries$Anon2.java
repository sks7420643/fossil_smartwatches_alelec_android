package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.google.gson.reflect.TypeToken;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.response.sleep.SleepDayParse;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SleepSummariesRepository$fetchSleepSummaries$2", f = "SleepSummariesRepository.kt", l = {217, 225}, m = "invokeSuspend")
public final class SleepSummariesRepository$fetchSleepSummaries$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<Ku3>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends TypeToken<ApiResponse<SleepDayParse>> {
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$fetchSleepSummaries$Anon2(SleepSummariesRepository sleepSummariesRepository, Date date, Date date2, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = sleepSummariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SleepSummariesRepository$fetchSleepSummaries$Anon2 sleepSummariesRepository$fetchSleepSummaries$Anon2 = new SleepSummariesRepository$fetchSleepSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, xe6);
        sleepSummariesRepository$fetchSleepSummaries$Anon2.p$ = (Il6) obj;
        throw null;
        //return sleepSummariesRepository$fetchSleepSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<Ku3>> xe6) {
        throw null;
        //return ((SleepSummariesRepository$fetchSleepSummaries$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0109  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r9) {
        /*
        // Method dump skipped, instructions count: 350
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository$fetchSleepSummaries$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
