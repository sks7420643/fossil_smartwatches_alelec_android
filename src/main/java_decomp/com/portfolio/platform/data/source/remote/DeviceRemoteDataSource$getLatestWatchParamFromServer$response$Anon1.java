package com.portfolio.platform.data.source.remote;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.WatchParameterResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.remote.DeviceRemoteDataSource$getLatestWatchParamFromServer$response$1", f = "DeviceRemoteDataSource.kt", l = {143}, m = "invokeSuspend")
public final class DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1 extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<WatchParameterResponse>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $majorVersion;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1(DeviceRemoteDataSource deviceRemoteDataSource, String str, int i, Xe6 xe6) {
        super(1, xe6);
        this.this$0 = deviceRemoteDataSource;
        this.$serial = str;
        this.$majorVersion = i;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        return new DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1(this.this$0, this.$serial, this.$majorVersion, xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public final Object invoke(Xe6<? super Q88<ApiResponse<WatchParameterResponse>>> xe6) {
        throw null;
        //return ((DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1) create(xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiService;
            String str = this.$serial;
            int i2 = this.$majorVersion;
            this.label = 1;
            Object latestWatchParams = apiServiceV2.getLatestWatchParams(str, i2, "-metadata.version.minor", 0, 1, this);
            return latestWatchParams == d ? d : latestWatchParams;
        } else if (i == 1) {
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
