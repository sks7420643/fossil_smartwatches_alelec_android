package com.portfolio.platform.data.source;

import com.fossil.Vt7;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.utils.Constants;
import java.io.File;
import java.io.FileFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileRepository$deleteBackgroundFiles$Anon1 implements FileFilter {
    @DexIgnore
    public static /* final */ FileRepository$deleteBackgroundFiles$Anon1 INSTANCE; // = new FileRepository$deleteBackgroundFiles$Anon1();

    @DexIgnore
    public final boolean accept(File file) {
        Wg6.b(file, "it");
        String name = file.getName();
        Wg6.b(name, "it.name");
        if (!Vt7.h(name, Constants.PHOTO_IMAGE_NAME_SUFFIX, true)) {
            String name2 = file.getName();
            Wg6.b(name2, "it.name");
            if (!Vt7.h(name2, Constants.PHOTO_BINARY_NAME_SUFFIX, true)) {
                return true;
            }
        }
        return false;
    }
}
