package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.microapp.weather.AddressOfWeather;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AddressDao_Impl implements AddressDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<AddressOfWeather> __insertionAdapterOfAddressOfWeather;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<AddressOfWeather> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, AddressOfWeather addressOfWeather) {
            mi.bindLong(1, (long) addressOfWeather.getId());
            mi.bindDouble(2, addressOfWeather.getLat());
            mi.bindDouble(3, addressOfWeather.getLng());
            if (addressOfWeather.getAddress() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, addressOfWeather.getAddress());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, AddressOfWeather addressOfWeather) {
            bind(mi, addressOfWeather);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `addressOfWeather` (`id`,`lat`,`lng`,`address`) VALUES (nullif(?, 0),?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM addressOfWeather";
        }
    }

    @DexIgnore
    public AddressDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfAddressOfWeather = new Anon1(oh);
        this.__preparedStmtOfClearData = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.AddressDao
    public void clearData() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.AddressDao
    public List<AddressOfWeather> getAllSavedAddress() {
        Rh f = Rh.f("SELECT * FROM addressOfWeather", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, Constants.LAT);
            int c3 = Dx0.c(b, "lng");
            int c4 = Dx0.c(b, "address");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                AddressOfWeather addressOfWeather = new AddressOfWeather(b.getDouble(c2), b.getDouble(c3), b.getString(c4));
                addressOfWeather.setId(b.getInt(c));
                arrayList.add(addressOfWeather);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.AddressDao
    public void saveAddress(AddressOfWeather addressOfWeather) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfAddressOfWeather.insert((Hh<AddressOfWeather>) addressOfWeather);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
