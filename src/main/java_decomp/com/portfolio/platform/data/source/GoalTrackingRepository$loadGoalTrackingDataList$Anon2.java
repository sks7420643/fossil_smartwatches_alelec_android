package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.GoalTrackingRepository$loadGoalTrackingDataList$2", f = "GoalTrackingRepository.kt", l = {MFNetworkReturnCode.WRONG_PASSWORD, 410, 413}, m = "invokeSuspend")
public final class GoalTrackingRepository$loadGoalTrackingDataList$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<ApiResponse<GoalEvent>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$loadGoalTrackingDataList$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, int i, int i2, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = goalTrackingRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        GoalTrackingRepository$loadGoalTrackingDataList$Anon2 goalTrackingRepository$loadGoalTrackingDataList$Anon2 = new GoalTrackingRepository$loadGoalTrackingDataList$Anon2(this.this$0, this.$startDate, this.$endDate, this.$offset, this.$limit, xe6);
        goalTrackingRepository$loadGoalTrackingDataList$Anon2.p$ = (Il6) obj;
        throw null;
        //return goalTrackingRepository$loadGoalTrackingDataList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<ApiResponse<GoalEvent>>> xe6) {
        throw null;
        //return ((GoalTrackingRepository$loadGoalTrackingDataList$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0090 A[Catch:{ Exception -> 0x01f1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00aa A[Catch:{ Exception -> 0x01f1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x019f  */
    /* JADX WARNING: Removed duplicated region for block: B:87:? A[RETURN, SYNTHETIC] */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
        // Method dump skipped, instructions count: 509
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository$loadGoalTrackingDataList$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
