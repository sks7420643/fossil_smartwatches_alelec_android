package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SummariesRepository$downloadRecommendedGoals$2$response$1", f = "SummariesRepository.kt", l = {388}, m = "invokeSuspend")
public final class SummariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2 extends Ko7 implements Hg6<Xe6<? super Q88<ActivityRecommendedGoals>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$downloadRecommendedGoals$Anon2 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2(SummariesRepository$downloadRecommendedGoals$Anon2 summariesRepository$downloadRecommendedGoals$Anon2, Xe6 xe6) {
        super(1, xe6);
        this.this$0 = summariesRepository$downloadRecommendedGoals$Anon2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        return new SummariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2(this.this$0, xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public final Object invoke(Xe6<? super Q88<ActivityRecommendedGoals>> xe6) {
        throw null;
        //return ((SummariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2) create(xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.this$0.mApiServiceV2;
            SummariesRepository$downloadRecommendedGoals$Anon2 summariesRepository$downloadRecommendedGoals$Anon2 = this.this$0;
            int i2 = summariesRepository$downloadRecommendedGoals$Anon2.$age;
            int i3 = summariesRepository$downloadRecommendedGoals$Anon2.$weightInGrams;
            int i4 = summariesRepository$downloadRecommendedGoals$Anon2.$heightInCentimeters;
            String str = summariesRepository$downloadRecommendedGoals$Anon2.$gender;
            this.label = 1;
            Object recommendedGoalsRaw = apiServiceV2.getRecommendedGoalsRaw(i2, i3, i4, str, this);
            return recommendedGoalsRaw == d ? d : recommendedGoalsRaw;
        } else if (i == 1) {
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
