package com.portfolio.platform.data.source.local.fitness;

import com.fossil.Lx0;
import com.mapped.Wg6;
import com.mapped.Xh;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDatabase$Companion$MIGRATION_FROM_21_TO_22$Anon1 extends Xh {
    @DexIgnore
    public FitnessDatabase$Companion$MIGRATION_FROM_21_TO_22$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.mapped.Xh
    public void migrate(Lx0 lx0) {
        Wg6.c(lx0, "database");
        FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migration 21 to 22 start");
        lx0.beginTransaction();
        try {
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate workout session data");
            lx0.execSQL("CREATE TABLE workout_session_new (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, deviceSerialNumber TEXT, step TEXT, calorie TEXT, distance TEXT, heartRate TEXT, speed TEXT, states TEXT NOT NULL DEFAULT '', sourceType TEXT, workoutType TEXT, timezoneOffset INTEGER NOT NULL, duration INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL, workoutGpsPoints TEXT, mode TEXT, pace TEXT, cadence TEXT, editedEndTime TEXT NOT NULL, editedStartTime TEXT NOT NULL, editedType TEXT, editedMode TEXT, screenShotUri TEXT, gpsDataPoints TEXT)");
            lx0.execSQL("INSERT INTO workout_session_new (id, date, startTime, endTime, deviceSerialNumber, step, calorie, distance, heartRate, speed, states, sourceType, workoutType, timezoneOffset, duration, createdAt, updatedAt, editedStartTime, editedEndTime, editedType) SELECT id, date, startTime, endTime, deviceSerialNumber, step, calorie, distance, heartRate, speed, states, sourceType, workoutType, timezoneOffset, duration, createdAt, updatedAt, startTime, endTime, workoutType FROM workout_session");
            lx0.execSQL("DROP TABLE workout_session");
            lx0.execSQL("ALTER TABLE workout_session_new RENAME TO workout_session");
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate workout session data - end");
        } catch (Exception e) {
            lx0.execSQL("DROP TABLE IF EXISTS workout_session_new");
            lx0.execSQL("DROP TABLE IF EXISTS workout_session");
            lx0.execSQL("CREATE TABLE workout_session (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, deviceSerialNumber TEXT, step TEXT, calorie TEXT, distance TEXT, heartRate TEXT, speed TEXT, states TEXT NOT NULL DEFAULT '', sourceType TEXT, workoutType TEXT, timezoneOffset INTEGER NOT NULL, duration INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL, workoutGpsPoints TEXT, mode TEXT, pace TEXT, cadence TEXT, editedEndTime TEXT NOT NULL, editedStartTime TEXT NOT NULL, editedType TEXT, editedMode TEXT, screenShotUri TEXT, gpsDataPoints TEXT)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e(FitnessDatabase.TAG, "MIGRATION_FROM_21_TO_22 - end with exception -- e=" + e);
        }
        lx0.setTransactionSuccessful();
        lx0.endTransaction();
    }
}
