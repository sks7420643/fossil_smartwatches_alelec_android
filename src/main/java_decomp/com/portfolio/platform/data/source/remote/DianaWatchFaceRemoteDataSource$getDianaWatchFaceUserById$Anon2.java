package com.portfolio.platform.data.source.remote;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource$getDianaWatchFaceUserById$2", f = "DianaWatchFaceRemoteDataSource.kt", l = {28}, m = "invokeSuspend")
public final class DianaWatchFaceRemoteDataSource$getDianaWatchFaceUserById$Anon2 extends Ko7 implements Hg6<Xe6<? super Q88<DianaWatchFaceUser>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $id;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ DianaWatchFaceRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaWatchFaceRemoteDataSource$getDianaWatchFaceUserById$Anon2(DianaWatchFaceRemoteDataSource dianaWatchFaceRemoteDataSource, String str, Xe6 xe6) {
        super(1, xe6);
        this.this$0 = dianaWatchFaceRemoteDataSource;
        this.$id = str;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        return new DianaWatchFaceRemoteDataSource$getDianaWatchFaceUserById$Anon2(this.this$0, this.$id, xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public final Object invoke(Xe6<? super Q88<DianaWatchFaceUser>> xe6) {
        throw null;
        //return ((DianaWatchFaceRemoteDataSource$getDianaWatchFaceUserById$Anon2) create(xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiServiceV2;
            String str = this.$id;
            this.label = 1;
            Object userDianaWatchFace = apiServiceV2.getUserDianaWatchFace(str, this);
            return userDianaWatchFace == d ? d : userDianaWatchFace;
        } else if (i == 1) {
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
