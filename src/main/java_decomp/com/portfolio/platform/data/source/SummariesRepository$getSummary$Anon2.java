package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.H47;
import com.fossil.Ko7;
import com.fossil.Ss0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SummariesRepository$getSummary$2", f = "SummariesRepository.kt", l = {160}, m = "invokeSuspend")
public final class SummariesRepository$getSummary$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super LiveData<H47<? extends ActivitySummary>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummary$Anon2(SummariesRepository summariesRepository, Date date, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = summariesRepository;
        this.$date = date;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SummariesRepository$getSummary$Anon2 summariesRepository$getSummary$Anon2 = new SummariesRepository$getSummary$Anon2(this.this$0, this.$date, xe6);
        summariesRepository$getSummary$Anon2.p$ = (Il6) obj;
        throw null;
        //return summariesRepository$getSummary$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super LiveData<H47<? extends ActivitySummary>>> xe6) {
        throw null;
        //return ((SummariesRepository$getSummary$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object g;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "getSummary - date=" + this.$date);
            Dv7 b = Bw7.b();
            SummariesRepository$getSummary$Anon2$fitnessDatabase$Anon1_Level2 summariesRepository$getSummary$Anon2$fitnessDatabase$Anon1_Level2 = new SummariesRepository$getSummary$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = il6;
            this.label = 1;
            g = Eu7.g(b, summariesRepository$getSummary$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (g == d) {
                return d;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            g = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) g;
        FitnessDataDao fitnessDataDao = fitnessDatabase.getFitnessDataDao();
        Date date = this.$date;
        LiveData c = Ss0.c(fitnessDataDao.getFitnessDataLiveData(date, date), new SummariesRepository$getSummary$Anon2$result$Anon1_Level2(this, fitnessDatabase));
        Wg6.b(c, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(SummariesRepository.TAG, "XXX- summaryLiveObj " + c);
        return c;
    }
}
