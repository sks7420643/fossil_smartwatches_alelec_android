package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import com.fossil.Lr4;
import com.fossil.Nr4;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory implements Factory<Nr4> {
    @DexIgnore
    public /* final */ Provider<Lr4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<Lr4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<Lr4> provider) {
        return new PortfolioDatabaseModule_ProvidesFlagLocalSourceFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static Nr4 providesFlagLocalSource(PortfolioDatabaseModule portfolioDatabaseModule, Lr4 lr4) {
        Nr4 providesFlagLocalSource = portfolioDatabaseModule.providesFlagLocalSource(lr4);
        Lk7.c(providesFlagLocalSource, "Cannot return null from a non-@Nullable @Provides method");
        return providesFlagLocalSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public Nr4 get() {
        return providesFlagLocalSource(this.module, this.daoProvider.get());
    }
}
