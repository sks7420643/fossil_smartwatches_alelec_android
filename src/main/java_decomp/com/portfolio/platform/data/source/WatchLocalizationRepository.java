package com.portfolio.platform.data.source;

import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.mapped.An4;
import com.mapped.Rl6;
import com.mapped.Wg6;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchLocalizationRepository {
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 api;
    @DexIgnore
    public /* final */ An4 sharedPreferencesManager;

    @DexIgnore
    public WatchLocalizationRepository(ApiServiceV2 apiServiceV2, An4 an4) {
        Wg6.c(apiServiceV2, "api");
        Wg6.c(an4, "sharedPreferencesManager");
        this.api = apiServiceV2;
        this.sharedPreferencesManager = an4;
        String simpleName = WatchLocalizationRepository.class.getSimpleName();
        Wg6.b(simpleName, "WatchLocalizationRepository::class.java.simpleName");
        this.TAG = simpleName;
    }

    @DexIgnore
    private final Rl6<String> processDownloadAndStore(String str, String str2) {
        return Gu7.b(Jv7.a(Bw7.b()), null, null, new WatchLocalizationRepository$processDownloadAndStore$Anon1(this, str, str2, null), 3, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0165, code lost:
        if (com.mapped.FileHelper.a.a(r4 + java.io.File.separator + r3) == false) goto L_0x0167;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01ab  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01eb  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getWatchLocalizationFromServer(boolean r19, com.mapped.Xe6<? super java.lang.String> r20) {
        /*
        // Method dump skipped, instructions count: 509
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WatchLocalizationRepository.getWatchLocalizationFromServer(boolean, com.mapped.Xe6):java.lang.Object");
    }
}
