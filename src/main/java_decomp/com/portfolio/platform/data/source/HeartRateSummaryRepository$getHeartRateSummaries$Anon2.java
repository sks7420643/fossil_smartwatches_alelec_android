package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.H47;
import com.fossil.Im7;
import com.fossil.Ko7;
import com.fossil.Pm7;
import com.fossil.Q88;
import com.fossil.Ss0;
import com.fossil.Yn7;
import com.fossil.Zi4;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Ku3;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConvertDateTimeToLong;
import com.portfolio.platform.helper.GsonConverterShortDate;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$2", f = "HeartRateSummaryRepository.kt", l = {53}, m = "invokeSuspend")
public final class HeartRateSummaryRepository$getHeartRateSummaries$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super LiveData<H47<? extends List<DailyHeartRateSummary>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ FitnessDatabase $fitnessDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSummaryRepository$getHeartRateSummaries$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends NetworkBoundResource<List<DailyHeartRateSummary>, ApiResponse<Ku3>> {
            @DexIgnore
            public /* final */ /* synthetic */ Lc6 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, Lc6 lc6) {
                this.this$0 = anon1_Level2;
                this.$downloadingDate = lc6;
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public Object createCall(Xe6<? super Q88<ApiResponse<Ku3>>> xe6) {
                Date date;
                Date date2;
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiService;
                Lc6 lc6 = this.$downloadingDate;
                if (lc6 == null || (date = (Date) lc6.getFirst()) == null) {
                    date = this.this$0.this$0.$startDate;
                }
                String k = TimeUtils.k(date);
                Wg6.b(k, "DateHelper.formatShortDa\u2026            ?: startDate)");
                Lc6 lc62 = this.$downloadingDate;
                if (lc62 == null || (date2 = (Date) lc62.getSecond()) == null) {
                    date2 = this.this$0.this$0.$endDate;
                }
                String k2 = TimeUtils.k(date2);
                Wg6.b(k2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return apiServiceV2.getDailyHeartRateSummaries(k, k2, 0, 100, xe6);
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public Object loadFromDb(Xe6<? super LiveData<List<DailyHeartRateSummary>>> xe6) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = HeartRateSummaryRepository.TAG;
                local.d(str, "getHeartRateSummaries isNotToday startDate = " + this.this$0.this$0.$startDate + ", endDate = " + this.this$0.this$0.$endDate);
                HeartRateDailySummaryDao heartRateDailySummaryDao = this.this$0.$fitnessDatabase.getHeartRateDailySummaryDao();
                HeartRateSummaryRepository$getHeartRateSummaries$Anon2 heartRateSummaryRepository$getHeartRateSummaries$Anon2 = this.this$0.this$0;
                return heartRateDailySummaryDao.getDailyHeartRateSummariesLiveData(heartRateSummaryRepository$getHeartRateSummaries$Anon2.$startDate, heartRateSummaryRepository$getHeartRateSummaries$Anon2.$endDate);
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(HeartRateSummaryRepository.TAG, "getHeartRateSummaries onFetchFailed");
            }

            @DexIgnore
            public Object saveCallResult(ApiResponse<Ku3> apiResponse, Xe6<? super Cd6> xe6) {
                Date date;
                Date date2;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = HeartRateSummaryRepository.TAG;
                local.d(str, "saveCallResult onResponse: response = " + apiResponse);
                try {
                    if (!apiResponse.get_items().isEmpty()) {
                        Zi4 zi4 = new Zi4();
                        zi4.f(Long.TYPE, new GsonConvertDateTimeToLong());
                        zi4.f(DateTime.class, new GsonConvertDateTime());
                        zi4.f(Date.class, new GsonConverterShortDate());
                        Gson d = zi4.d();
                        List<Ku3> list = apiResponse.get_items();
                        ArrayList arrayList = new ArrayList(Im7.m(list, 10));
                        Iterator<T> it = list.iterator();
                        while (it.hasNext()) {
                            arrayList.add((DailyHeartRateSummary) d.h(it.next(), new HeartRateSummaryRepository$getHeartRateSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$summaries$Anon1_Level4$Anon1_Level5().getType()));
                        }
                        List<DailyHeartRateSummary> j0 = Pm7.j0(arrayList);
                        FLogger.INSTANCE.getLocal().d(HeartRateSummaryRepository.TAG, String.valueOf(j0));
                        this.this$0.$fitnessDatabase.getHeartRateDailySummaryDao().insertListDailyHeartRateSummary(j0);
                        FitnessDataDao fitnessDataDao = this.this$0.$fitnessDatabase.getFitnessDataDao();
                        Lc6 lc6 = this.$downloadingDate;
                        Date date3 = (lc6 == null || (date2 = (Date) lc6.getFirst()) == null) ? this.this$0.this$0.$startDate : date2;
                        Lc6 lc62 = this.$downloadingDate;
                        if (lc62 == null || (date = (Date) lc62.getSecond()) == null) {
                            date = this.this$0.this$0.$endDate;
                        }
                        List<FitnessDataWrapper> fitnessData = fitnessDataDao.getFitnessData(date3, date);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = HeartRateSummaryRepository.TAG;
                        local2.d(str2, "heartrate summary " + j0 + " fitnessDataSize " + fitnessData.size());
                        if (fitnessData.isEmpty()) {
                            this.this$0.$fitnessDatabase.getHeartRateDailySummaryDao().insertListDailyHeartRateSummary(j0);
                        }
                    }
                } catch (Exception e) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = HeartRateSummaryRepository.TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("saveCallResult exception=");
                    e.printStackTrace();
                    sb.append(Cd6.a);
                    local3.e(str3, sb.toString());
                }
                return Cd6.a;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.mapped.Xe6] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<Ku3> apiResponse, Xe6 xe6) {
                return saveCallResult(apiResponse, (Xe6<? super Cd6>) xe6);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ boolean shouldFetch(List<DailyHeartRateSummary> list) {
                return shouldFetch(list);
            }

            @DexIgnore
            public boolean shouldFetch(List<DailyHeartRateSummary> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(HeartRateSummaryRepository$getHeartRateSummaries$Anon2 heartRateSummaryRepository$getHeartRateSummaries$Anon2, FitnessDatabase fitnessDatabase) {
            this.this$0 = heartRateSummaryRepository$getHeartRateSummaries$Anon2;
            this.$fitnessDatabase = fitnessDatabase;
        }

        @DexIgnore
        public final LiveData<H47<List<DailyHeartRateSummary>>> apply(List<FitnessDataWrapper> list) {
            Wg6.b(list, "fitnessDataList");
            HeartRateSummaryRepository$getHeartRateSummaries$Anon2 heartRateSummaryRepository$getHeartRateSummaries$Anon2 = this.this$0;
            return new Anon1_Level3(this, FitnessDataWrapperKt.calculateRangeDownload(list, heartRateSummaryRepository$getHeartRateSummaries$Anon2.$startDate, heartRateSummaryRepository$getHeartRateSummaries$Anon2.$endDate)).asLiveData();
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return apply((List) obj);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSummaryRepository$getHeartRateSummaries$Anon2(HeartRateSummaryRepository heartRateSummaryRepository, Date date, Date date2, boolean z, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = heartRateSummaryRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        HeartRateSummaryRepository$getHeartRateSummaries$Anon2 heartRateSummaryRepository$getHeartRateSummaries$Anon2 = new HeartRateSummaryRepository$getHeartRateSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, this.$shouldFetch, xe6);
        heartRateSummaryRepository$getHeartRateSummaries$Anon2.p$ = (Il6) obj;
        throw null;
        //return heartRateSummaryRepository$getHeartRateSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super LiveData<H47<? extends List<DailyHeartRateSummary>>>> xe6) {
        throw null;
        //return ((HeartRateSummaryRepository$getHeartRateSummaries$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object g;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = HeartRateSummaryRepository.TAG;
            local.d(str, "getHeartRateSummaries: startDate = " + this.$startDate + ", endDate = " + this.$endDate);
            Dv7 b = Bw7.b();
            HeartRateSummaryRepository$getHeartRateSummaries$Anon2$fitnessDatabase$Anon1_Level2 heartRateSummaryRepository$getHeartRateSummaries$Anon2$fitnessDatabase$Anon1_Level2 = new HeartRateSummaryRepository$getHeartRateSummaries$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = il6;
            this.label = 1;
            g = Eu7.g(b, heartRateSummaryRepository$getHeartRateSummaries$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (g == d) {
                return d;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            g = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) g;
        return Ss0.c(fitnessDatabase.getFitnessDataDao().getFitnessDataLiveData(this.$startDate, this.$endDate), new Anon1_Level2(this, fitnessDatabase));
    }
}
