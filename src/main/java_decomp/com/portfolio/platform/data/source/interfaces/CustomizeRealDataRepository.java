package com.portfolio.platform.data.source.interfaces;

import androidx.lifecycle.LiveData;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.source.local.CustomizeRealDataDao;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeRealDataRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "CustomizeRealDataRepository";
    @DexIgnore
    public /* final */ CustomizeRealDataDao mCustomizeRealData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public CustomizeRealDataRepository(CustomizeRealDataDao customizeRealDataDao) {
        Wg6.c(customizeRealDataDao, "mCustomizeRealData");
        this.mCustomizeRealData = customizeRealDataDao;
    }

    @DexIgnore
    public final LiveData<List<CustomizeRealData>> getAllRealDataAsLiveData() {
        return this.mCustomizeRealData.getAllRealDataAsLiveData();
    }

    @DexIgnore
    public final List<CustomizeRealData> getAllRealDataRaw() {
        return this.mCustomizeRealData.getAllRealDataRaw();
    }

    @DexIgnore
    public final void upsertCustomizeRealData(CustomizeRealData customizeRealData) {
        Wg6.c(customizeRealData, "customizeRealData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "upsertCustomizeRealData " + customizeRealData);
        this.mCustomizeRealData.upsertRealData(customizeRealData);
    }
}
