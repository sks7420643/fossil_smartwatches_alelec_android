package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao;
import com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ HybridPresetDao mHybridPresetDao;
    @DexIgnore
    public /* final */ HybridPresetRemoteDataSource mHybridPresetRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return HybridPresetRepository.TAG;
        }
    }

    /*
    static {
        String simpleName = HybridPresetRepository.class.getSimpleName();
        Wg6.b(simpleName, "HybridPresetRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public HybridPresetRepository(HybridPresetDao hybridPresetDao, HybridPresetRemoteDataSource hybridPresetRemoteDataSource) {
        Wg6.c(hybridPresetDao, "mHybridPresetDao");
        Wg6.c(hybridPresetRemoteDataSource, "mHybridPresetRemoteDataSource");
        this.mHybridPresetDao = hybridPresetDao;
        this.mHybridPresetRemoteDataSource = hybridPresetRemoteDataSource;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mHybridPresetDao.clearAllPresetTable();
        this.mHybridPresetDao.clearAllRecommendPresetTable();
    }

    @DexIgnore
    public final void deleteAllPresetBySerial(String str) {
        Wg6.c(str, "serial");
        this.mHybridPresetDao.clearAllPresetBySerial(str);
        this.mHybridPresetDao.clearAllRecommendPresetTable();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deletePresetById(java.lang.String r10, com.mapped.Xe6<? super com.mapped.Cd6> r11) {
        /*
        // Method dump skipped, instructions count: 265
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HybridPresetRepository.deletePresetById(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0133  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadPresetList(java.lang.String r11, com.mapped.Xe6<? super com.mapped.Ap4<java.util.List<com.portfolio.platform.data.model.room.microapp.HybridPreset>>> r12) {
        /*
        // Method dump skipped, instructions count: 333
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HybridPresetRepository.downloadPresetList(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadRecommendPresetList(java.lang.String r9, com.mapped.Xe6<? super com.mapped.Cd6> r10) {
        /*
        // Method dump skipped, instructions count: 263
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HybridPresetRepository.downloadRecommendPresetList(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00d4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object executePendingRequest(java.lang.String r10, com.mapped.Xe6<? super com.mapped.Ap4<java.util.List<com.portfolio.platform.data.model.room.microapp.HybridPreset>>> r11) {
        /*
        // Method dump skipped, instructions count: 314
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HybridPresetRepository.executePendingRequest(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final HybridPreset getActivePresetBySerial(String str) {
        Wg6.c(str, "serial");
        return this.mHybridPresetDao.getActivePresetBySerial(str);
    }

    @DexIgnore
    public final List<HybridRecommendPreset> getHybridRecommendPresetList(String str) {
        Wg6.c(str, "serial");
        return this.mHybridPresetDao.getRecommendPresetList(str);
    }

    @DexIgnore
    public final HybridPreset getPresetById(String str) {
        Wg6.c(str, "id");
        return this.mHybridPresetDao.getPresetById(str);
    }

    @DexIgnore
    public final ArrayList<HybridPreset> getPresetList(String str) {
        Wg6.c(str, "serial");
        List<HybridPreset> allPreset = this.mHybridPresetDao.getAllPreset(str);
        if (allPreset != null) {
            return (ArrayList) allPreset;
        }
        throw new Rc6("null cannot be cast to non-null type java.util.ArrayList<com.portfolio.platform.data.model.room.microapp.HybridPreset>");
    }

    @DexIgnore
    public final LiveData<List<HybridPreset>> getPresetListAsLiveData(String str) {
        Wg6.c(str, "serial");
        return this.mHybridPresetDao.getAllPresetAsLiveData(str);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object upsertHybridPreset(com.portfolio.platform.data.model.room.microapp.HybridPreset r12, com.mapped.Xe6<? super com.mapped.Cd6> r13) {
        /*
        // Method dump skipped, instructions count: 378
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HybridPresetRepository.upsertHybridPreset(com.portfolio.platform.data.model.room.microapp.HybridPreset, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a6  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object upsertHybridPresetList(java.util.List<com.portfolio.platform.data.model.room.microapp.HybridPreset> r8, com.mapped.Xe6<? super com.mapped.Cd6> r9) {
        /*
            r7 = this;
            r6 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPresetList$Anon1
            if (r0 == 0) goto L_0x006b
            r0 = r9
            com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPresetList$Anon1 r0 = (com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPresetList$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x006b
            int r1 = r1 + r4
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r4 = r1.label
            if (r4 == 0) goto L_0x007a
            if (r4 != r6) goto L_0x0072
            java.lang.Object r0 = r1.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.HybridPresetRepository r0 = (com.portfolio.platform.data.source.HybridPresetRepository) r0
            com.fossil.El7.b(r2)
            r1 = r2
            r7 = r0
        L_0x002e:
            r0 = r1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x00a6
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r1.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r1 = "upsertHybridPresetList success "
            r5.append(r1)
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r1 = r0.a()
            if (r1 == 0) goto L_0x00a2
            java.util.ArrayList r1 = (java.util.ArrayList) r1
            r5.append(r1)
            java.lang.String r1 = r5.toString()
            r2.d(r4, r1)
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r1 = r7.mHybridPresetDao
            java.lang.Object r0 = r0.a()
            if (r0 == 0) goto L_0x009e
            java.util.List r0 = (java.util.List) r0
            r1.upsertPresetList(r0)
        L_0x0068:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x006a:
            return r0
        L_0x006b:
            com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPresetList$Anon1 r0 = new com.portfolio.platform.data.source.HybridPresetRepository$upsertHybridPresetList$Anon1
            r0.<init>(r7, r9)
            r1 = r0
            goto L_0x0015
        L_0x0072:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x007a:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.String r5 = "upsertHybridPresetList"
            r2.d(r4, r5)
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r2 = r7.mHybridPresetDao
            r2.upsertPresetList(r8)
            com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource r2 = r7.mHybridPresetRemoteDataSource
            r1.L$0 = r7
            r1.L$1 = r8
            r1.label = r6
            java.lang.Object r1 = r2.upsertHybridPresetList(r8, r1)
            if (r1 != r0) goto L_0x002e
            goto L_0x006a
        L_0x009e:
            com.mapped.Wg6.i()
            throw r3
        L_0x00a2:
            com.mapped.Wg6.i()
            throw r3
        L_0x00a6:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x0068
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.HybridPresetRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "upsertHybridPresetList fail!! "
            r4.append(r5)
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            int r5 = r0.a()
            r4.append(r5)
            java.lang.String r5 = " serverCode "
            r4.append(r5)
            com.portfolio.platform.data.model.ServerError r0 = r0.c()
            if (r0 == 0) goto L_0x00df
            java.lang.Integer r0 = r0.getCode()
        L_0x00d4:
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            r1.d(r2, r0)
            goto L_0x0068
        L_0x00df:
            r0 = r3
            goto L_0x00d4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HybridPresetRepository.upsertHybridPresetList(java.util.List, com.mapped.Xe6):java.lang.Object");
    }
}
