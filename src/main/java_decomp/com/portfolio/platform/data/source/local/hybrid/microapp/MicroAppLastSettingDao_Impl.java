package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppLastSettingDao_Impl implements MicroAppLastSettingDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<MicroAppLastSetting> __insertionAdapterOfMicroAppLastSetting;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteMicroAppLastSettingById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<MicroAppLastSetting> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, MicroAppLastSetting microAppLastSetting) {
            if (microAppLastSetting.getAppId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, microAppLastSetting.getAppId());
            }
            if (microAppLastSetting.getUpdatedAt() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, microAppLastSetting.getUpdatedAt());
            }
            if (microAppLastSetting.getSetting() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, microAppLastSetting.getSetting());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, MicroAppLastSetting microAppLastSetting) {
            bind(mi, microAppLastSetting);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microAppLastSetting` (`appId`,`updatedAt`,`setting`) VALUES (?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM microAppLastSetting WHERE appId=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM microAppLastSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<MicroAppLastSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon4(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<MicroAppLastSetting> call() throws Exception {
            Cursor b = Ex0.b(MicroAppLastSettingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "appId");
                int c2 = Dx0.c(b, "updatedAt");
                int c3 = Dx0.c(b, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new MicroAppLastSetting(b.getString(c), b.getString(c2), b.getString(c3)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public MicroAppLastSettingDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfMicroAppLastSetting = new Anon1(oh);
        this.__preparedStmtOfDeleteMicroAppLastSettingById = new Anon2(oh);
        this.__preparedStmtOfCleanUp = new Anon3(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public void deleteMicroAppLastSettingById(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteMicroAppLastSettingById.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteMicroAppLastSettingById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public List<MicroAppLastSetting> getAllMicroAppLastSetting() {
        Rh f = Rh.f("SELECT * FROM microAppLastSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "appId");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new MicroAppLastSetting(b.getString(c), b.getString(c2), b.getString(c3)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public LiveData<List<MicroAppLastSetting>> getAllMicroAppLastSettingAsLiveData() {
        Rh f = Rh.f("SELECT * FROM microAppLastSetting", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"microAppLastSetting"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public MicroAppLastSetting getMicroAppLastSetting(String str) {
        MicroAppLastSetting microAppLastSetting = null;
        Rh f = Rh.f("SELECT * FROM microAppLastSetting WHERE appId=? ", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "appId");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, MicroAppSetting.SETTING);
            if (b.moveToFirst()) {
                microAppLastSetting = new MicroAppLastSetting(b.getString(c), b.getString(c2), b.getString(c3));
            }
            return microAppLastSetting;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public void upsertMicroAppLastSetting(MicroAppLastSetting microAppLastSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppLastSetting.insert((Hh<MicroAppLastSetting>) microAppLastSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao
    public void upsertMicroAppLastSettingList(List<MicroAppLastSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppLastSetting.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
