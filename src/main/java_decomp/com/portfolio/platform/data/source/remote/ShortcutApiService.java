package com.portfolio.platform.data.source.remote;

import com.fossil.Y98;
import com.mapped.Ny6;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ShortcutApiService {
    @DexIgnore
    @Ny6("default-presets")
    Call<ApiResponse<RecommendedPreset>> getDefaultPreset(@Y98("offset") int i, @Y98("size") int i2, @Y98("serialNumber") String str);

    @DexIgnore
    @Ny6("micro-apps")
    Call<ApiResponse<MicroApp>> getMicroAppGallery(@Y98("page") int i, @Y98("size") int i2, @Y98("serialNumber") String str);
}
