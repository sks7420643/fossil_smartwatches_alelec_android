package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseRepository_Factory implements Factory<QuickResponseRepository> {
    @DexIgnore
    public /* final */ Provider<QuickResponseMessageDao> mQRMessageDaoProvider;
    @DexIgnore
    public /* final */ Provider<QuickResponseSenderDao> mQuickResponseSenderDaoProvider;

    @DexIgnore
    public QuickResponseRepository_Factory(Provider<QuickResponseMessageDao> provider, Provider<QuickResponseSenderDao> provider2) {
        this.mQRMessageDaoProvider = provider;
        this.mQuickResponseSenderDaoProvider = provider2;
    }

    @DexIgnore
    public static QuickResponseRepository_Factory create(Provider<QuickResponseMessageDao> provider, Provider<QuickResponseSenderDao> provider2) {
        return new QuickResponseRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static QuickResponseRepository newInstance(QuickResponseMessageDao quickResponseMessageDao, QuickResponseSenderDao quickResponseSenderDao) {
        return new QuickResponseRepository(quickResponseMessageDao, quickResponseSenderDao);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public QuickResponseRepository get() {
        return newInstance(this.mQRMessageDaoProvider.get(), this.mQuickResponseSenderDaoProvider.get());
    }
}
