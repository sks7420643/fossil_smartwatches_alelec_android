package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserRemoteDataSource_Factory implements Factory<UserRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;
    @DexIgnore
    public /* final */ Provider<AuthApiGuestService> mAuthApiGuestServiceProvider;
    @DexIgnore
    public /* final */ Provider<AuthApiUserService> mAuthApiUserServiceProvider;

    @DexIgnore
    public UserRemoteDataSource_Factory(Provider<ApiServiceV2> provider, Provider<AuthApiGuestService> provider2, Provider<AuthApiUserService> provider3) {
        this.mApiServiceProvider = provider;
        this.mAuthApiGuestServiceProvider = provider2;
        this.mAuthApiUserServiceProvider = provider3;
    }

    @DexIgnore
    public static UserRemoteDataSource_Factory create(Provider<ApiServiceV2> provider, Provider<AuthApiGuestService> provider2, Provider<AuthApiUserService> provider3) {
        return new UserRemoteDataSource_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static UserRemoteDataSource newInstance(ApiServiceV2 apiServiceV2, AuthApiGuestService authApiGuestService, AuthApiUserService authApiUserService) {
        return new UserRemoteDataSource(apiServiceV2, authApiGuestService, authApiUserService);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public UserRemoteDataSource get() {
        return newInstance(this.mApiServiceProvider.get(), this.mAuthApiGuestServiceProvider.get(), this.mAuthApiUserServiceProvider.get());
    }
}
