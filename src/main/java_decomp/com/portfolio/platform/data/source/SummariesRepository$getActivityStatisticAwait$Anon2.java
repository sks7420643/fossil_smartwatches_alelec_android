package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.ActivityStatistic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticAwait$2", f = "SummariesRepository.kt", l = {437, 437}, m = "invokeSuspend")
public final class SummariesRepository$getActivityStatisticAwait$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super ActivityStatistic>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getActivityStatisticAwait$Anon2(SummariesRepository summariesRepository, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = summariesRepository;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SummariesRepository$getActivityStatisticAwait$Anon2 summariesRepository$getActivityStatisticAwait$Anon2 = new SummariesRepository$getActivityStatisticAwait$Anon2(this.this$0, xe6);
        summariesRepository$getActivityStatisticAwait$Anon2.p$ = (Il6) obj;
        throw null;
        //return summariesRepository$getActivityStatisticAwait$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super ActivityStatistic> xe6) {
        throw null;
        //return ((SummariesRepository$getActivityStatisticAwait$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r8) {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r7.label
            if (r0 == 0) goto L_0x003d
            if (r0 == r5) goto L_0x0021
            if (r0 != r6) goto L_0x0019
            java.lang.Object r0 = r7.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r8)
            r0 = r8
        L_0x0016:
            com.portfolio.platform.data.ActivityStatistic r0 = (com.portfolio.platform.data.ActivityStatistic) r0
        L_0x0018:
            return r0
        L_0x0019:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0021:
            java.lang.Object r0 = r7.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r8)
            r2 = r0
            r1 = r8
        L_0x002a:
            r0 = r1
            com.portfolio.platform.data.ActivityStatistic r0 = (com.portfolio.platform.data.ActivityStatistic) r0
            if (r0 != 0) goto L_0x0018
            com.portfolio.platform.data.source.SummariesRepository r0 = r7.this$0
            r7.L$0 = r2
            r7.label = r6
            java.lang.Object r0 = r0.fetchActivityStatistic(r7)
            if (r0 != r3) goto L_0x0016
            r0 = r3
            goto L_0x0018
        L_0x003d:
            com.fossil.El7.b(r8)
            com.mapped.Il6 r0 = r7.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = "SummariesRepository"
            java.lang.String r4 = "getActivityStatisticAwait"
            r1.d(r2, r4)
            com.portfolio.platform.data.source.SummariesRepository r1 = r7.this$0
            r7.L$0 = r0
            r7.label = r5
            java.lang.Object r1 = r1.getActivityStatisticDB(r7)
            if (r1 != r3) goto L_0x005d
            r0 = r3
            goto L_0x0018
        L_0x005d:
            r2 = r0
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository$getActivityStatisticAwait$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
