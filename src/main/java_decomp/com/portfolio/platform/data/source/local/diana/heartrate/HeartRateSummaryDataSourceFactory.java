package com.portfolio.platform.data.source.local.diana.heartrate;

import androidx.lifecycle.MutableLiveData;
import com.mapped.PagingRequestHelper;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Xe;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryDataSourceFactory extends Xe.Bi<Date, DailyHeartRateSummary> {
    @DexIgnore
    public /* final */ U04 appExecutors;
    @DexIgnore
    public /* final */ Date createdDate;
    @DexIgnore
    public /* final */ FitnessDataRepository fitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase fitnessDatabase;
    @DexIgnore
    public /* final */ PagingRequestHelper.Ai listener;
    @DexIgnore
    public HeartRateSummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<HeartRateSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ HeartRateSummaryRepository summariesRepository;

    @DexIgnore
    public HeartRateSummaryDataSourceFactory(HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository2, FitnessDatabase fitnessDatabase2, Date date, U04 u04, PagingRequestHelper.Ai ai, Calendar calendar) {
        Wg6.c(heartRateSummaryRepository, "summariesRepository");
        Wg6.c(fitnessDataRepository2, "fitnessDataRepository");
        Wg6.c(fitnessDatabase2, "fitnessDatabase");
        Wg6.c(date, "createdDate");
        Wg6.c(u04, "appExecutors");
        Wg6.c(ai, "listener");
        Wg6.c(calendar, "mStartCalendar");
        this.summariesRepository = heartRateSummaryRepository;
        this.fitnessDataRepository = fitnessDataRepository2;
        this.fitnessDatabase = fitnessDatabase2;
        this.createdDate = date;
        this.appExecutors = u04;
        this.listener = ai;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    @Override // com.mapped.Xe.Bi
    public Xe<Date, DailyHeartRateSummary> create() {
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource = new HeartRateSummaryLocalDataSource(this.summariesRepository, this.fitnessDataRepository, this.fitnessDatabase, this.createdDate, this.appExecutors, this.listener, this.mStartCalendar);
        this.localDataSource = heartRateSummaryLocalDataSource;
        this.sourceLiveData.l(heartRateSummaryLocalDataSource);
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource2 = this.localDataSource;
        if (heartRateSummaryLocalDataSource2 != null) {
            return heartRateSummaryLocalDataSource2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final HeartRateSummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<HeartRateSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        this.localDataSource = heartRateSummaryLocalDataSource;
    }
}
