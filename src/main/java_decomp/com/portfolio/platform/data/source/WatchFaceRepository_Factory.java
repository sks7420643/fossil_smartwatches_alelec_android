package com.portfolio.platform.data.source;

import android.content.Context;
import com.portfolio.platform.data.source.remote.WatchFaceRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceRepository_Factory implements Factory<WatchFaceRepository> {
    @DexIgnore
    public /* final */ Provider<Context> contextProvider;
    @DexIgnore
    public /* final */ Provider<FileRepository> fileRepositoryProvider;
    @DexIgnore
    public /* final */ Provider<WatchFaceRemoteDataSource> watchFaceRemoteDataSourceProvider;

    @DexIgnore
    public WatchFaceRepository_Factory(Provider<Context> provider, Provider<WatchFaceRemoteDataSource> provider2, Provider<FileRepository> provider3) {
        this.contextProvider = provider;
        this.watchFaceRemoteDataSourceProvider = provider2;
        this.fileRepositoryProvider = provider3;
    }

    @DexIgnore
    public static WatchFaceRepository_Factory create(Provider<Context> provider, Provider<WatchFaceRemoteDataSource> provider2, Provider<FileRepository> provider3) {
        return new WatchFaceRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static WatchFaceRepository newInstance(Context context, WatchFaceRemoteDataSource watchFaceRemoteDataSource, FileRepository fileRepository) {
        return new WatchFaceRepository(context, watchFaceRemoteDataSource, fileRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WatchFaceRepository get() {
        return newInstance(this.contextProvider.get(), this.watchFaceRemoteDataSourceProvider.get(), this.fileRepositoryProvider.get());
    }
}
