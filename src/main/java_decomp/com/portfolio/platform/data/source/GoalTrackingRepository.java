package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.Bw7;
import com.fossil.Eu7;
import com.fossil.H47;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.mapped.An4;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.PagingRequestHelper;
import com.mapped.Qg6;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;
    @DexIgnore
    public /* final */ An4 mSharedPreferencesManager;
    @DexIgnore
    public List<GoalTrackingDataSourceFactory> mSourceDataFactoryList; // = new ArrayList();
    @DexIgnore
    public List<GoalTrackingSummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();
    @DexIgnore
    public /* final */ UserRepository mUserRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return GoalTrackingRepository.TAG;
        }
    }

    @DexIgnore
    public interface PushPendingGoalTrackingDataListCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(List<GoalTrackingData> list);
    }

    /*
    static {
        String simpleName = GoalTrackingRepository.class.getSimpleName();
        Wg6.b(simpleName, "GoalTrackingRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public GoalTrackingRepository(UserRepository userRepository, An4 an4, ApiServiceV2 apiServiceV2) {
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(apiServiceV2, "mApiServiceV2");
        this.mUserRepository = userRepository;
        this.mSharedPreferencesManager = an4;
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    public static /* synthetic */ Object loadGoalTrackingDataList$default(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, int i, int i2, Xe6 xe6, int i3, Object obj) {
        return goalTrackingRepository.loadGoalTrackingDataList(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, xe6);
    }

    @DexIgnore
    public final Object cleanUp(Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new GoalTrackingRepository$cleanUp$Anon2(this, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0175  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object delete(java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> r16, com.mapped.Xe6<? super com.mapped.Ap4<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData>>> r17) {
        /*
        // Method dump skipped, instructions count: 614
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.delete(java.util.List, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object deleteGoalTracking(GoalTrackingData goalTrackingData, Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new GoalTrackingRepository$deleteGoalTracking$Anon2(this, goalTrackingData, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final Object fetchGoalSetting(Xe6<? super Ap4<GoalSetting>> xe6) {
        return Eu7.g(Bw7.b(), new GoalTrackingRepository$fetchGoalSetting$Anon2(this, null), xe6);
    }

    @DexIgnore
    public final Object getGoalTrackingDataInDate(Date date, Xe6<? super List<GoalTrackingData>> xe6) {
        return Eu7.g(Bw7.b(), new GoalTrackingRepository$getGoalTrackingDataInDate$Anon2(date, null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getGoalTrackingDataList(java.util.Date r11, java.util.Date r12, boolean r13, com.mapped.Xe6<? super androidx.lifecycle.LiveData<com.fossil.H47<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData>>>> r14) {
        /*
            r10 = this;
            r9 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon1
            if (r0 == 0) goto L_0x0038
            r0 = r14
            com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon1 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.label = r1
            r6 = r0
        L_0x0014:
            java.lang.Object r1 = r6.result
            java.lang.Object r7 = com.fossil.Yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x0047
            if (r0 != r9) goto L_0x003f
            boolean r0 = r6.Z$0
            java.lang.Object r0 = r6.L$2
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$1
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r0 = (com.portfolio.platform.data.source.GoalTrackingRepository) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0032:
            java.lang.String r1 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.mapped.Wg6.b(r0, r1)
        L_0x0037:
            return r0
        L_0x0038:
            com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon1
            r0.<init>(r10, r14)
            r6 = r0
            goto L_0x0014
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            com.fossil.El7.b(r1)
            com.fossil.Jx7 r8 = com.fossil.Bw7.c()
            com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getGoalTrackingDataList$Anon2
            r5 = 0
            r1 = r10
            r2 = r11
            r3 = r12
            r4 = r13
            r0.<init>(r1, r2, r3, r4, r5)
            r6.L$0 = r10
            r6.L$1 = r11
            r6.L$2 = r12
            r6.Z$0 = r13
            r6.label = r9
            java.lang.Object r0 = com.fossil.Eu7.g(r8, r0, r6)
            if (r0 != r7) goto L_0x0032
            r0 = r7
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getGoalTrackingDataList(java.util.Date, java.util.Date, boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object getGoalTrackingDataPaging(Date date, U04 u04, PagingRequestHelper.Ai ai, Xe6<? super Listing<GoalTrackingData>> xe6) {
        return Eu7.g(Bw7.c(), new GoalTrackingRepository$getGoalTrackingDataPaging$Anon2(this, date, u04, ai, null), xe6);
    }

    @DexIgnore
    public final List<GoalTrackingSummary> getGoalTrackingSummaries(Date date, Date date2, GoalTrackingDao goalTrackingDao) {
        Wg6.c(date, GoalPhase.COLUMN_START_DATE);
        Wg6.c(date2, GoalPhase.COLUMN_END_DATE);
        Wg6.c(goalTrackingDao, "goalTrackingDao");
        return goalTrackingDao.getGoalTrackingSummaries(date, date2);
    }

    @DexIgnore
    public final Object getLastGoalSettingLiveData(Xe6<? super LiveData<H47<Integer>>> xe6) {
        return Eu7.g(Bw7.c(), new GoalTrackingRepository$getLastGoalSettingLiveData$Anon2(this, null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getLastGoalSettings(com.mapped.Xe6<? super java.lang.Integer> r12) {
        /*
        // Method dump skipped, instructions count: 269
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getLastGoalSettings(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object getPendingGoalTrackingDataList(Date date, Date date2, Xe6<? super List<GoalTrackingData>> xe6) {
        return Eu7.g(Bw7.b(), new GoalTrackingRepository$getPendingGoalTrackingDataList$Anon2(date, date2, null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getPendingGoalTrackingDataListLiveData(java.util.Date r6, java.util.Date r7, com.mapped.Xe6<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData>>> r8) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getPendingGoalTrackingDataListLiveData$Anon1
            if (r0 == 0) goto L_0x003d
            r0 = r8
            com.portfolio.platform.data.source.GoalTrackingRepository$getPendingGoalTrackingDataListLiveData$Anon1 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getPendingGoalTrackingDataListLiveData$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003d
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r1 = r2.label
            if (r1 == 0) goto L_0x004c
            if (r1 != r4) goto L_0x0044
            java.lang.Object r0 = r2.L$2
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r1 = r2.L$1
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r2 = (com.portfolio.platform.data.source.GoalTrackingRepository) r2
            com.fossil.El7.b(r3)
            r2 = r3
            r7 = r0
        L_0x0031:
            r0 = r2
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r0
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r0 = r0.getGoalTrackingDao()
            androidx.lifecycle.LiveData r0 = r0.getPendingGoalTrackingDataListLiveData(r1, r7)
        L_0x003c:
            return r0
        L_0x003d:
            com.portfolio.platform.data.source.GoalTrackingRepository$getPendingGoalTrackingDataListLiveData$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getPendingGoalTrackingDataListLiveData$Anon1
            r0.<init>(r5, r8)
            r2 = r0
            goto L_0x0014
        L_0x0044:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004c:
            com.fossil.El7.b(r3)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r2.L$0 = r5
            r2.L$1 = r6
            r2.L$2 = r7
            r2.label = r4
            java.lang.Object r2 = r1.A(r2)
            if (r2 == r0) goto L_0x003c
            r1 = r6
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getPendingGoalTrackingDataListLiveData(java.util.Date, java.util.Date, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSummaries(java.util.Date r11, java.util.Date r12, boolean r13, com.mapped.Xe6<? super androidx.lifecycle.LiveData<com.fossil.H47<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>>>> r14) {
        /*
            r10 = this;
            r9 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon1
            if (r0 == 0) goto L_0x0038
            r0 = r14
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon1 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0038
            int r1 = r1 + r3
            r0.label = r1
            r6 = r0
        L_0x0014:
            java.lang.Object r1 = r6.result
            java.lang.Object r7 = com.fossil.Yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x0047
            if (r0 != r9) goto L_0x003f
            boolean r0 = r6.Z$0
            java.lang.Object r0 = r6.L$2
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$1
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r6.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r0 = (com.portfolio.platform.data.source.GoalTrackingRepository) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0032:
            java.lang.String r1 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.mapped.Wg6.b(r0, r1)
        L_0x0037:
            return r0
        L_0x0038:
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon1
            r0.<init>(r10, r14)
            r6 = r0
            goto L_0x0014
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            com.fossil.El7.b(r1)
            com.fossil.Jx7 r8 = com.fossil.Bw7.c()
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon2 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummaries$Anon2
            r5 = 0
            r1 = r10
            r2 = r11
            r3 = r12
            r4 = r13
            r0.<init>(r1, r2, r3, r4, r5)
            r6.L$0 = r10
            r6.L$1 = r11
            r6.L$2 = r12
            r6.Z$0 = r13
            r6.label = r9
            java.lang.Object r0 = com.fossil.Eu7.g(r8, r0, r6)
            if (r0 != r7) goto L_0x0032
            r0 = r7
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getSummaries(java.util.Date, java.util.Date, boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object getSummariesPaging(Date date, U04 u04, PagingRequestHelper.Ai ai, Xe6<? super Listing<GoalTrackingSummary>> xe6) {
        return Eu7.g(Bw7.b(), new GoalTrackingRepository$getSummariesPaging$Anon2(this, date, u04, ai, null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getSummary(java.util.Date r7, com.mapped.Xe6<? super androidx.lifecycle.LiveData<com.fossil.H47<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>>> r8) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon1
            if (r0 == 0) goto L_0x0032
            r0 = r8
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon1 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0041
            if (r0 != r5) goto L_0x0039
            java.lang.Object r0 = r1.L$1
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r0 = (com.portfolio.platform.data.source.GoalTrackingRepository) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x002c:
            java.lang.String r1 = "withContext(Dispatchers.\u2026iveData()\n        }\n    }"
            com.mapped.Wg6.b(r0, r1)
        L_0x0031:
            return r0
        L_0x0032:
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon1
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r2)
            com.fossil.Jx7 r0 = com.fossil.Bw7.c()
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2 r2 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2
            r4 = 0
            r2.<init>(r6, r7, r4)
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r5
            java.lang.Object r0 = com.fossil.Eu7.g(r0, r2, r1)
            if (r0 != r3) goto L_0x002c
            r0 = r3
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getSummary(java.util.Date, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v61, types: [java.util.List] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00d5  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0121  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x012c  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x019b  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01d5  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0206  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0313  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object insert(java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> r16, com.mapped.Xe6<? super com.mapped.Ap4<java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData>>> r17) {
        /*
        // Method dump skipped, instructions count: 831
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.insert(java.util.List, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object insertFromDevice(List<GoalTrackingData> list, Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new GoalTrackingRepository$insertFromDevice$Anon2(this, list, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final Object loadGoalTrackingDataList(Date date, Date date2, int i, int i2, Xe6<? super Ap4<ApiResponse<GoalEvent>>> xe6) {
        return Eu7.g(Bw7.b(), new GoalTrackingRepository$loadGoalTrackingDataList$Anon2(this, date, date2, i, i2, null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0069 A[Catch:{ Exception -> 0x0086 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0183  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x018b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object loadSummaries(java.util.Date r11, java.util.Date r12, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary>>> r13) {
        /*
        // Method dump skipped, instructions count: 475
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.loadSummaries(java.util.Date, java.util.Date, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object pushPendingGoalTrackingDataList(Xe6<? super Cd6> xe6) {
        FLogger.INSTANCE.getLocal().d(TAG, "pushPendingGoalTrackingDataList");
        Object pushPendingGoalTrackingDataList = pushPendingGoalTrackingDataList(new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon4(this), xe6);
        return pushPendingGoalTrackingDataList == Yn7.d() ? pushPendingGoalTrackingDataList : Cd6.a;
    }

    @DexIgnore
    public final Object pushPendingGoalTrackingDataList(PushPendingGoalTrackingDataListCallback pushPendingGoalTrackingDataListCallback, Xe6<? super Cd6> xe6) {
        return Eu7.g(Bw7.b(), new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon2(this, pushPendingGoalTrackingDataListCallback, null), xe6);
    }

    @DexIgnore
    public final /* synthetic */ Object removeDeletedGoalTrackingList(List<GoalTrackingData> list, Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new GoalTrackingRepository$removeDeletedGoalTrackingList$Anon2(list, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final void removePagingListener() {
        for (GoalTrackingDataSourceFactory goalTrackingDataSourceFactory : this.mSourceDataFactoryList) {
            GoalTrackingDataLocalDataSource localDataSource = goalTrackingDataSourceFactory.getLocalDataSource();
            if (localDataSource != null) {
                localDataSource.removePagingObserver();
            }
        }
        this.mSourceDataFactoryList.clear();
        for (GoalTrackingSummaryDataSourceFactory goalTrackingSummaryDataSourceFactory : this.mSourceFactoryList) {
            GoalTrackingSummaryLocalDataSource localDataSource2 = goalTrackingSummaryDataSourceFactory.getLocalDataSource();
            if (localDataSource2 != null) {
                localDataSource2.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x014c  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0152  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0168  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01ef  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0236  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0272  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x027b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveGoalTrackingDataListToServer(java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> r21, com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback r22, com.mapped.Xe6<? super com.mapped.Cd6> r23) {
        /*
        // Method dump skipped, instructions count: 648
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.saveGoalTrackingDataListToServer(java.util.List, com.portfolio.platform.data.source.GoalTrackingRepository$PushPendingGoalTrackingDataListCallback, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object saveSettingToDB(GoalSetting goalSetting, Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new GoalTrackingRepository$saveSettingToDB$Anon2(goalSetting, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object updateGoalSetting(com.portfolio.platform.data.model.GoalSetting r12, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.model.GoalSetting>> r13) {
        /*
        // Method dump skipped, instructions count: 301
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.updateGoalSetting(com.portfolio.platform.data.model.GoalSetting, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object updateGoalTrackingPinType(java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> r6, int r7, com.mapped.Xe6<? super com.mapped.Cd6> r8) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalTrackingPinType$Anon1
            if (r0 == 0) goto L_0x003c
            r0 = r8
            com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalTrackingPinType$Anon1 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalTrackingPinType$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003c
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x004b
            if (r0 != r4) goto L_0x0043
            int r0 = r1.I$0
            java.lang.Object r0 = r1.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.GoalTrackingRepository r1 = (com.portfolio.platform.data.source.GoalTrackingRepository) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002f:
            r0 = r1
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r0
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r0 = r0.getGoalTrackingDao()
            r0.upsertListGoalTrackingData(r6)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x003b:
            return r0
        L_0x003c:
            com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalTrackingPinType$Anon1 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$updateGoalTrackingPinType$Anon1
            r0.<init>(r5, r8)
            r1 = r0
            goto L_0x0014
        L_0x0043:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004b:
            com.fossil.El7.b(r2)
            java.util.Iterator r2 = r6.iterator()
        L_0x0052:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0062
            java.lang.Object r0 = r2.next()
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r0 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingData) r0
            r0.setPinType(r7)
            goto L_0x0052
        L_0x0062:
            com.portfolio.platform.manager.EncryptedDatabaseManager r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.I$0 = r7
            r1.label = r4
            java.lang.Object r1 = r0.A(r1)
            if (r1 != r3) goto L_0x002f
            r0 = r3
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.updateGoalTrackingPinType(java.util.List, int, com.mapped.Xe6):java.lang.Object");
    }
}
