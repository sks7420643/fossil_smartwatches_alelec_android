package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$Anon2;
import com.portfolio.platform.data.source.local.fitness.ActivitySampleDao;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.ActivitiesRepository$getActivityList$2$1$1$loadFromDb$2", f = "ActivitiesRepository.kt", l = {}, m = "invokeSuspend")
public final class ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4 extends Ko7 implements Coroutine<Il6, Xe6<? super LiveData<List<ActivitySample>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository$getActivityList$Anon2.Anon1_Level2.Anon1_Level3 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4(ActivitiesRepository$getActivityList$Anon2.Anon1_Level2.Anon1_Level3 anon1_Level3, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = anon1_Level3;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4 activitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4 = new ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4(this.this$0, xe6);
        activitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4.p$ = (Il6) obj;
        throw null;
        //return activitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super LiveData<List<ActivitySample>>> xe6) {
        throw null;
        //return ((ActivitiesRepository$getActivityList$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon2_Level4) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Yn7.d();
        if (this.label == 0) {
            El7.b(obj);
            ActivitySampleDao activitySampleDao = this.this$0.this$0.$fitnessDatabase.activitySampleDao();
            Date date = this.this$0.this$0.$startDate;
            Wg6.b(date, GoalPhase.COLUMN_START_DATE);
            Date date2 = this.this$0.this$0.$endDate;
            Wg6.b(date2, GoalPhase.COLUMN_END_DATE);
            return activitySampleDao.getActivitySamplesLiveData(date, date2);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
