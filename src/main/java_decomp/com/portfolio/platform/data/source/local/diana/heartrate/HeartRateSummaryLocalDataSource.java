package com.portfolio.platform.data.source.local.diana.heartrate;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Bw7;
import com.fossil.Gl5;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Jv7;
import com.fossil.Nw0;
import com.fossil.Pm7;
import com.mapped.Af;
import com.mapped.Lc6;
import com.mapped.PagingRequestHelper;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.U04;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryLocalDataSource extends Af<Date, DailyHeartRateSummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ PagingRequestHelper.Ai listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public PagingRequestHelper mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ Nw0.Ci mObserver;
    @DexIgnore
    public List<Lc6<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public Date mStartDate; // = new Date();
    @DexIgnore
    public /* final */ HeartRateSummaryRepository mSummariesRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends Nw0.Ci {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = heartRateSummaryLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Nw0.Ci
        public void onInvalidated(Set<String> set) {
            Wg6.c(set, "tables");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            Wg6.c(date, "date");
            Wg6.c(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(getTAG$app_fossilRelease(), "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar I = TimeUtils.I(instance);
            if (TimeUtils.j0(date2, I.getTime())) {
                I.setTime(date2);
            }
            Date time = I.getTime();
            Wg6.b(time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return HeartRateSummaryLocalDataSource.TAG;
        }
    }

    /*
    static {
        String simpleName = HeartRateSummaryLocalDataSource.class.getSimpleName();
        Wg6.b(simpleName, "HeartRateSummaryLocalDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public HeartRateSummaryLocalDataSource(HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, FitnessDatabase fitnessDatabase, Date date, U04 u04, PagingRequestHelper.Ai ai, Calendar calendar) {
        Wg6.c(heartRateSummaryRepository, "mSummariesRepository");
        Wg6.c(fitnessDataRepository, "mFitnessDataRepository");
        Wg6.c(fitnessDatabase, "mFitnessDatabase");
        Wg6.c(date, "mCreatedDate");
        Wg6.c(u04, "appExecutors");
        Wg6.c(ai, "listener");
        Wg6.c(calendar, "key");
        this.mSummariesRepository = heartRateSummaryRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mFitnessDatabase = fitnessDatabase;
        this.mCreatedDate = date;
        this.listener = ai;
        this.key = calendar;
        PagingRequestHelper pagingRequestHelper = new PagingRequestHelper(u04.a());
        this.mHelper = pagingRequestHelper;
        this.mNetworkState = Gl5.b(pagingRequestHelper);
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "daily_heart_rate_summary", new String[0]);
        this.mFitnessDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar I = TimeUtils.I(instance);
        I.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + I.getTime());
        Date time = I.getTime();
        Wg6.b(time, "calendar.time");
        this.mStartDate = time;
        if (TimeUtils.j0(date, time)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final List<DailyHeartRateSummary> calculateSummaries(List<DailyHeartRateSummary> list) {
        int value;
        Resting resting;
        if (!list.isEmpty()) {
            DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) Pm7.P(list);
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "endCalendar");
            instance.setTime(dailyHeartRateSummary.getDate());
            int value2 = (instance.get(7) == 1 || (resting = dailyHeartRateSummary.getResting()) == null) ? 0 : resting.getValue();
            Calendar instance2 = Calendar.getInstance();
            Wg6.b(instance2, "calendar");
            instance2.setTime(((DailyHeartRateSummary) Pm7.F(list)).getDate());
            Calendar b0 = TimeUtils.b0(instance2.getTime());
            Wg6.b(b0, "DateHelper.getStartOfWeek(calendar.time)");
            b0.add(5, -1);
            Calendar instance3 = Calendar.getInstance();
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            for (T t : list) {
                if (i >= 0) {
                    T t2 = t;
                    Wg6.b(instance3, "mSummaryCalendar");
                    instance3.setTime(t2.getDate());
                    if (instance3.get(5) == b0.get(5)) {
                        DailyHeartRateSummary dailyHeartRateSummary2 = list.get(i4);
                        if (i2 <= 0) {
                            i2 = 1;
                        }
                        dailyHeartRateSummary2.setAvgRestingHeartRateOfWeek(Integer.valueOf(i3 / i2));
                        b0.add(5, -7);
                        i2 = 0;
                        i3 = 0;
                        i4 = i;
                    }
                    Resting resting2 = t2.getResting();
                    if (resting2 != null && (value = resting2.getValue()) > 0) {
                        i3 += value;
                        i2++;
                    }
                    if (i == list.size() - 1 && value2 > 0) {
                        i3 += value2;
                        i2++;
                    }
                    i++;
                    i2 = i2;
                } else {
                    Hm7.l();
                    throw null;
                }
            }
            DailyHeartRateSummary dailyHeartRateSummary3 = list.get(i4);
            if (i2 <= 0) {
                i2 = 1;
            }
            dailyHeartRateSummary3.setAvgRestingHeartRateOfWeek(Integer.valueOf(i3 / i2));
        }
        return list;
    }

    @DexIgnore
    private final DailyHeartRateSummary dummySummary(Date date) {
        DailyHeartRateSummary dailyHeartRateSummary = new DailyHeartRateSummary(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, date, System.currentTimeMillis(), System.currentTimeMillis(), 0, 0, 0, null);
        dailyHeartRateSummary.setCreatedAt(System.currentTimeMillis());
        dailyHeartRateSummary.setUpdatedAt(System.currentTimeMillis());
        return dailyHeartRateSummary;
    }

    @DexIgnore
    private final List<DailyHeartRateSummary> getDataInDatabase(Date date, Date date2) {
        DailyHeartRateSummary dailyHeartRateSummary;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getDataInDatabase - startDate=" + date + ", endDate=" + date2);
        List<DailyHeartRateSummary> calculateSummaries = calculateSummaries(this.mFitnessDatabase.getHeartRateDailySummaryDao().getDailyHeartRateSummariesDesc(TimeUtils.j0(date, date2) ? date2 : date, date2));
        ArrayList arrayList = new ArrayList();
        Date lastDate = this.mFitnessDatabase.getHeartRateDailySummaryDao().getLastDate();
        if (lastDate == null) {
            lastDate = date;
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(calculateSummaries);
        if (!TimeUtils.j0(date, lastDate)) {
            date = lastDate;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "getDataInDatabase - summaries.size=" + calculateSummaries.size() + ", lastDate=" + lastDate + ", startDateToFill=" + date);
        while (TimeUtils.k0(date2, date)) {
            Iterator it = arrayList2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    dailyHeartRateSummary = null;
                    break;
                }
                Object next = it.next();
                if (TimeUtils.m0(((DailyHeartRateSummary) next).getDate(), date2)) {
                    dailyHeartRateSummary = next;
                    break;
                }
            }
            DailyHeartRateSummary dailyHeartRateSummary2 = dailyHeartRateSummary;
            if (dailyHeartRateSummary2 == null) {
                arrayList.add(dummySummary(date2));
            } else {
                arrayList.add(dailyHeartRateSummary2);
                arrayList2.remove(dailyHeartRateSummary2);
            }
            date2 = TimeUtils.P(date2);
            Wg6.b(date2, "DateHelper.getPrevDate(endDateToFill)");
        }
        if (!arrayList.isEmpty()) {
            DailyHeartRateSummary dailyHeartRateSummary3 = (DailyHeartRateSummary) Pm7.F(arrayList);
            Boolean p0 = TimeUtils.p0(dailyHeartRateSummary3.getDate());
            Wg6.b(p0, "DateHelper.isToday(todaySummary.date)");
            if (p0.booleanValue()) {
                arrayList.add(0, new DailyHeartRateSummary(dailyHeartRateSummary3));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final Rm6 loadData(Date date, Date date2, PagingRequestHelper.Bi.Aii aii) {
        return Gu7.d(Jv7.a(Bw7.b()), null, null, new HeartRateSummaryLocalDataSource$loadData$Anon1(this, date, date2, aii, null), 3, null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final PagingRequestHelper getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public boolean isInvalid() {
        this.mFitnessDatabase.getInvalidationTracker().h();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadAfter(Af.Fi<Date> fi, Af.Ai<Date, DailyHeartRateSummary> ai) {
        Wg6.c(fi, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ai, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Object) fi.a));
        if (TimeUtils.j0(fi.a, this.mCreatedDate)) {
            Key key2 = fi.a;
            Wg6.b(key2, "params.key");
            Key key3 = key2;
            Companion companion = Companion;
            Key key4 = fi.a;
            Wg6.b(key4, "params.key");
            Date calculateNextKey = companion.calculateNextKey(key4, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date O = TimeUtils.m0(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : TimeUtils.O(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + O + ", endQueryDate=" + ((Object) key3));
            Wg6.b(O, "startQueryDate");
            ai.a(getDataInDatabase(O, key3), calculateNextKey);
            if (TimeUtils.j0(this.mStartDate, key3)) {
                this.mEndDate = key3;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.d(str3, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new Lc6<>(this.mStartDate, this.mEndDate));
                this.mHelper.h(PagingRequestHelper.Di.AFTER, new HeartRateSummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadBefore(Af.Fi<Date> fi, Af.Ai<Date, DailyHeartRateSummary> ai) {
        Wg6.c(fi, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ai, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadInitial(Af.Ei<Date> ei, Af.Ci<Date, DailyHeartRateSummary> ci) {
        Wg6.c(ei, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ci, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date O = TimeUtils.m0(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : TimeUtils.O(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + O + ", endQueryDate=" + date);
        Wg6.b(O, "startQueryDate");
        ci.a(getDataInDatabase(O, date), null, this.key.getTime());
        this.mHelper.h(PagingRequestHelper.Di.INITIAL, new HeartRateSummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.f(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().j(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        Wg6.c(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(PagingRequestHelper pagingRequestHelper) {
        Wg6.c(pagingRequestHelper, "<set-?>");
        this.mHelper = pagingRequestHelper;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        Wg6.c(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        Wg6.c(date, "<set-?>");
        this.mStartDate = date;
    }
}
