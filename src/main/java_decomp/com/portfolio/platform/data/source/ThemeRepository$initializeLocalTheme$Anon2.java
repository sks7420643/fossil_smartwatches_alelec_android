package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.ThemeRepository$initializeLocalTheme$2", f = "ThemeRepository.kt", l = {44, 45}, m = "invokeSuspend")
public final class ThemeRepository$initializeLocalTheme$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThemeRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThemeRepository$initializeLocalTheme$Anon2(ThemeRepository themeRepository, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = themeRepository;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        ThemeRepository$initializeLocalTheme$Anon2 themeRepository$initializeLocalTheme$Anon2 = new ThemeRepository$initializeLocalTheme$Anon2(this.this$0, xe6);
        themeRepository$initializeLocalTheme$Anon2.p$ = (Il6) obj;
        throw null;
        //return themeRepository$initializeLocalTheme$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((ThemeRepository$initializeLocalTheme$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r8) {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r0 = r7.label
            if (r0 == 0) goto L_0x0039
            if (r0 == r5) goto L_0x0020
            if (r0 != r6) goto L_0x0018
            java.lang.Object r0 = r7.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r8)
        L_0x0015:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0017:
            return r0
        L_0x0018:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0020:
            java.lang.Object r0 = r7.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r8)
        L_0x0027:
            com.portfolio.platform.manager.ThemeManager$Ai r2 = com.portfolio.platform.manager.ThemeManager.l
            com.portfolio.platform.manager.ThemeManager r2 = r2.a()
            r7.L$0 = r0
            r7.label = r6
            java.lang.Object r0 = r2.i(r7)
            if (r0 != r1) goto L_0x0015
            r0 = r1
            goto L_0x0017
        L_0x0039:
            com.fossil.El7.b(r8)
            com.mapped.Il6 r0 = r7.p$
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            com.portfolio.platform.data.source.ThemeRepository$Companion r3 = com.portfolio.platform.data.source.ThemeRepository.Companion
            java.lang.String r3 = r3.getTAG()
            java.lang.String r4 = "initializeLocalTheme"
            r2.d(r3, r4)
            com.portfolio.platform.data.source.ThemeRepository r2 = r7.this$0
            java.lang.String r3 = "theme/default/config"
            com.portfolio.platform.data.source.ThemeRepository.access$loadTheme(r2, r3)
            com.portfolio.platform.data.source.ThemeRepository r2 = r7.this$0
            com.portfolio.platform.PortfolioApp$inner r3 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r3 = r3.instance()
            java.lang.String r3 = r3.U()
            r7.L$0 = r0
            r7.label = r5
            java.lang.Object r2 = r2.setCurrentThemeId(r3, r7)
            if (r2 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ThemeRepository$initializeLocalTheme$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
