package com.portfolio.platform.data.source;

import com.fossil.Fu4;
import com.fossil.Jt4;
import com.fossil.Lk7;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory implements Factory<Fu4> {
    @DexIgnore
    public /* final */ Provider<Jt4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<Jt4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<Jt4> provider) {
        return new PortfolioDatabaseModule_ProvidesSocialProfileLocalFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static Fu4 providesSocialProfileLocal(PortfolioDatabaseModule portfolioDatabaseModule, Jt4 jt4) {
        Fu4 providesSocialProfileLocal = portfolioDatabaseModule.providesSocialProfileLocal(jt4);
        Lk7.c(providesSocialProfileLocal, "Cannot return null from a non-@Nullable @Provides method");
        return providesSocialProfileLocal;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public Fu4 get() {
        return providesSocialProfileLocal(this.module, this.daoProvider.get());
    }
}
