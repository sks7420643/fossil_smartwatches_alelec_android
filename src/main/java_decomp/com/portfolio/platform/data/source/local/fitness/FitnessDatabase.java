package com.portfolio.platform.data.source.local.fitness;

import com.mapped.Oh;
import com.mapped.Qg6;
import com.mapped.Xh;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class FitnessDatabase extends Oh {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ Xh MIGRATION_FROM_21_TO_22; // = new FitnessDatabase$Companion$MIGRATION_FROM_21_TO_22$Anon1(21, 22);
    @DexIgnore
    public static /* final */ Xh MIGRATION_FROM_2_TO_13; // = new FitnessDatabase$Companion$MIGRATION_FROM_2_TO_13$Anon1(2, 13);
    @DexIgnore
    public static /* final */ Xh MIGRATION_FROM_4_TO_21; // = new FitnessDatabase$Companion$MIGRATION_FROM_4_TO_21$Anon1(4, 21);
    @DexIgnore
    public static /* final */ String TAG; // = "FitnessDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_21_TO_22$annotations() {
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_2_TO_13$annotations() {
        }

        @DexIgnore
        public static /* synthetic */ void MIGRATION_FROM_4_TO_21$annotations() {
        }

        @DexIgnore
        public final Xh getMIGRATION_FROM_21_TO_22() {
            return FitnessDatabase.MIGRATION_FROM_21_TO_22;
        }

        @DexIgnore
        public final Xh getMIGRATION_FROM_2_TO_13() {
            return FitnessDatabase.MIGRATION_FROM_2_TO_13;
        }

        @DexIgnore
        public final Xh getMIGRATION_FROM_4_TO_21() {
            return FitnessDatabase.MIGRATION_FROM_4_TO_21;
        }
    }

    @DexIgnore
    public abstract ActivitySampleDao activitySampleDao();

    @DexIgnore
    public abstract ActivitySummaryDao activitySummaryDao();

    @DexIgnore
    public abstract FitnessDataDao getFitnessDataDao();

    @DexIgnore
    public abstract HeartRateDailySummaryDao getHeartRateDailySummaryDao();

    @DexIgnore
    public abstract HeartRateSampleDao getHeartRateDao();

    @DexIgnore
    public abstract WorkoutDao getWorkoutDao();

    @DexIgnore
    public abstract SampleRawDao sampleRawDao();
}
