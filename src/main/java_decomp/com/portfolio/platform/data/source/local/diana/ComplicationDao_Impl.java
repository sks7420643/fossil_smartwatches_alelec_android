package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Fw0;
import com.fossil.G78;
import com.fossil.Hx0;
import com.fossil.M05;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.diana.Complication;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationDao_Impl implements ComplicationDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<Complication> __insertionAdapterOfComplication;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAll;
    @DexIgnore
    public /* final */ M05 __stringArrayConverter; // = new M05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<Complication> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, Complication complication) {
            if (complication.getComplicationId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, complication.getComplicationId());
            }
            if (complication.getName() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, complication.getName());
            }
            if (complication.getNameKey() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, complication.getNameKey());
            }
            String b = ComplicationDao_Impl.this.__stringArrayConverter.b(complication.getCategories());
            if (b == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, b);
            }
            if (complication.getDescription() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, complication.getDescription());
            }
            if (complication.getDescriptionKey() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, complication.getDescriptionKey());
            }
            if (complication.getIcon() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, complication.getIcon());
            }
            if (complication.getCreatedAt() == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, complication.getCreatedAt());
            }
            if (complication.getUpdatedAt() == null) {
                mi.bindNull(9);
            } else {
                mi.bindString(9, complication.getUpdatedAt());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, Complication complication) {
            bind(mi, complication);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `complication` (`complicationId`,`name`,`nameKey`,`categories`,`description`,`descriptionKey`,`icon`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM complication";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<Complication>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon3(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<Complication> call() throws Exception {
            Cursor b = Ex0.b(ComplicationDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "complicationId");
                int c2 = Dx0.c(b, "name");
                int c3 = Dx0.c(b, "nameKey");
                int c4 = Dx0.c(b, "categories");
                int c5 = Dx0.c(b, "description");
                int c6 = Dx0.c(b, "descriptionKey");
                int c7 = Dx0.c(b, "icon");
                int c8 = Dx0.c(b, "createdAt");
                int c9 = Dx0.c(b, "updatedAt");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new Complication(b.getString(c), b.getString(c2), b.getString(c3), ComplicationDao_Impl.this.__stringArrayConverter.a(b.getString(c4)), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8), b.getString(c9)));
                }
                return arrayList;
            } finally {
                b.close();
                this.val$_statement.m();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<Complication> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon4(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Complication call() throws Exception {
            Complication complication = null;
            Cursor b = Ex0.b(ComplicationDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "complicationId");
                int c2 = Dx0.c(b, "name");
                int c3 = Dx0.c(b, "nameKey");
                int c4 = Dx0.c(b, "categories");
                int c5 = Dx0.c(b, "description");
                int c6 = Dx0.c(b, "descriptionKey");
                int c7 = Dx0.c(b, "icon");
                int c8 = Dx0.c(b, "createdAt");
                int c9 = Dx0.c(b, "updatedAt");
                if (b.moveToFirst()) {
                    complication = new Complication(b.getString(c), b.getString(c2), b.getString(c3), ComplicationDao_Impl.this.__stringArrayConverter.a(b.getString(c4)), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8), b.getString(c9));
                }
                return complication;
            } finally {
                b.close();
                this.val$_statement.m();
            }
        }
    }

    @DexIgnore
    public ComplicationDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfComplication = new Anon1(oh);
        this.__preparedStmtOfClearAll = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationDao
    public Object getAllComplications(Xe6<? super List<Complication>> xe6) {
        return Fw0.a(this.__db, false, new Anon3(Rh.f("SELECT * FROM complication WHERE complicationId != 'empty'", 0)), xe6);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationDao
    public Object getComplicationById(String str, Xe6<? super Complication> xe6) {
        Rh f = Rh.f("SELECT * FROM complication WHERE complicationId=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        return Fw0.a(this.__db, false, new Anon4(f), xe6);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationDao
    public List<Complication> getComplicationByIds(List<String> list) {
        StringBuilder b = Hx0.b();
        b.append("SELECT ");
        b.append(G78.ANY_MARKER);
        b.append(" FROM complication WHERE complicationId IN (");
        int size = list.size();
        Hx0.a(b, size);
        b.append(")");
        Rh f = Rh.f(b.toString(), size + 0);
        int i = 1;
        for (String str : list) {
            if (str == null) {
                f.bindNull(i);
            } else {
                f.bindString(i, str);
            }
            i++;
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b2, "complicationId");
            int c2 = Dx0.c(b2, "name");
            int c3 = Dx0.c(b2, "nameKey");
            int c4 = Dx0.c(b2, "categories");
            int c5 = Dx0.c(b2, "description");
            int c6 = Dx0.c(b2, "descriptionKey");
            int c7 = Dx0.c(b2, "icon");
            int c8 = Dx0.c(b2, "createdAt");
            int c9 = Dx0.c(b2, "updatedAt");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new Complication(b2.getString(c), b2.getString(c2), b2.getString(c3), this.__stringArrayConverter.a(b2.getString(c4)), b2.getString(c5), b2.getString(c6), b2.getString(c7), b2.getString(c8), b2.getString(c9)));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationDao
    public List<Complication> queryComplicationByName(String str) {
        Rh f = Rh.f("SELECT * FROM complication WHERE name LIKE '%' || ? || '%'", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "complicationId");
            int c2 = Dx0.c(b, "name");
            int c3 = Dx0.c(b, "nameKey");
            int c4 = Dx0.c(b, "categories");
            int c5 = Dx0.c(b, "description");
            int c6 = Dx0.c(b, "descriptionKey");
            int c7 = Dx0.c(b, "icon");
            int c8 = Dx0.c(b, "createdAt");
            int c9 = Dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new Complication(b.getString(c), b.getString(c2), b.getString(c3), this.__stringArrayConverter.a(b.getString(c4)), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8), b.getString(c9)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationDao
    public void upsertComplicationList(List<Complication> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfComplication.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
