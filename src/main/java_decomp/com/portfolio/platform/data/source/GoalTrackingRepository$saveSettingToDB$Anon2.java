package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.GoalTrackingRepository$saveSettingToDB$2", f = "GoalTrackingRepository.kt", l = {136}, m = "invokeSuspend")
public final class GoalTrackingRepository$saveSettingToDB$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalSetting $goalSetting;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$saveSettingToDB$Anon2(GoalSetting goalSetting, Xe6 xe6) {
        super(2, xe6);
        this.$goalSetting = goalSetting;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        GoalTrackingRepository$saveSettingToDB$Anon2 goalTrackingRepository$saveSettingToDB$Anon2 = new GoalTrackingRepository$saveSettingToDB$Anon2(this.$goalSetting, xe6);
        goalTrackingRepository$saveSettingToDB$Anon2.p$ = (Il6) obj;
        throw null;
        //return goalTrackingRepository$saveSettingToDB$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((GoalTrackingRepository$saveSettingToDB$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object A;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            EncryptedDatabaseManager encryptedDatabaseManager = EncryptedDatabaseManager.j;
            this.L$0 = il6;
            this.label = 1;
            A = encryptedDatabaseManager.A(this);
            if (A == d) {
                return d;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            A = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        GoalTrackingDatabase goalTrackingDatabase = (GoalTrackingDatabase) A;
        goalTrackingDatabase.getGoalTrackingDao().upsertGoalSettings(this.$goalSetting);
        GoalTrackingSummary goalTrackingSummary = goalTrackingDatabase.getGoalTrackingDao().getGoalTrackingSummary(new Date());
        if (goalTrackingSummary == null) {
            goalTrackingSummary = new GoalTrackingSummary(new Date(), 0, this.$goalSetting.getCurrentTarget(), new Date().getTime(), new Date().getTime());
        } else {
            goalTrackingSummary.setGoalTarget(this.$goalSetting.getCurrentTarget());
        }
        goalTrackingDatabase.getGoalTrackingDao().upsertGoalTrackingSummary(goalTrackingSummary);
        return Cd6.a;
    }
}
