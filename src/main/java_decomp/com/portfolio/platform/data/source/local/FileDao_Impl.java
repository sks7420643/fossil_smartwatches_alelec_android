package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.fossil.Uz4;
import com.mapped.Gh;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.LocalFile;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDao_Impl extends FileDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Gh<LocalFile> __deletionAdapterOfLocalFile;
    @DexIgnore
    public /* final */ Uz4 __fileTypeConverter; // = new Uz4();
    @DexIgnore
    public /* final */ Hh<LocalFile> __insertionAdapterOfLocalFile;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearLocalFileTable;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteLocalFileByType;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteLocalFileByUri;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<LocalFile> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, LocalFile localFile) {
            mi.bindLong(1, (long) localFile.getPinType());
            String a2 = FileDao_Impl.this.__fileTypeConverter.a(localFile.getType());
            if (a2 == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, a2);
            }
            if (localFile.getFileName() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, localFile.getFileName());
            }
            if (localFile.getLocalUri() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, localFile.getLocalUri());
            }
            if (localFile.getRemoteUrl() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, localFile.getRemoteUrl());
            }
            if (localFile.getChecksum() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, localFile.getChecksum());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, LocalFile localFile) {
            bind(mi, localFile);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `localfile` (`pinType`,`type`,`fileName`,`localUri`,`remoteUrl`,`checksum`) VALUES (?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Gh<LocalFile> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, LocalFile localFile) {
            if (localFile.getRemoteUrl() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, localFile.getRemoteUrl());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Gh
        public /* bridge */ /* synthetic */ void bind(Mi mi, LocalFile localFile) {
            bind(mi, localFile);
        }

        @DexIgnore
        @Override // com.mapped.Vh, com.mapped.Gh
        public String createQuery() {
            return "DELETE FROM `localfile` WHERE `remoteUrl` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM localfile WHERE localUri=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends Vh {
        @DexIgnore
        public Anon4(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM localfile WHERE type=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends Vh {
        @DexIgnore
        public Anon5(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM localfile";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 implements Callable<List<LocalFile>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon6(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<LocalFile> call() throws Exception {
            Cursor b = Ex0.b(FileDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "pinType");
                int c2 = Dx0.c(b, "type");
                int c3 = Dx0.c(b, "fileName");
                int c4 = Dx0.c(b, "localUri");
                int c5 = Dx0.c(b, "remoteUrl");
                int c6 = Dx0.c(b, "checksum");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    LocalFile localFile = new LocalFile(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6));
                    localFile.setPinType(b.getInt(c));
                    localFile.setType(FileDao_Impl.this.__fileTypeConverter.b(b.getString(c2)));
                    arrayList.add(localFile);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public FileDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfLocalFile = new Anon1(oh);
        this.__deletionAdapterOfLocalFile = new Anon2(oh);
        this.__preparedStmtOfDeleteLocalFileByUri = new Anon3(oh);
        this.__preparedStmtOfDeleteLocalFileByType = new Anon4(oh);
        this.__preparedStmtOfClearLocalFileTable = new Anon5(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void clearLocalFileTable() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearLocalFileTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearLocalFileTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void deleteLocalFile(LocalFile localFile) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfLocalFile.handle(localFile);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void deleteLocalFileByType(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteLocalFileByType.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteLocalFileByType.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void deleteLocalFileByUri(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteLocalFileByUri.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteLocalFileByUri.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public LocalFile getFileByFileName(String str) {
        LocalFile localFile = null;
        Rh f = Rh.f("SELECT * FROM localfile WHERE fileName=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "type");
            int c3 = Dx0.c(b, "fileName");
            int c4 = Dx0.c(b, "localUri");
            int c5 = Dx0.c(b, "remoteUrl");
            int c6 = Dx0.c(b, "checksum");
            if (b.moveToFirst()) {
                localFile = new LocalFile(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6));
                localFile.setPinType(b.getInt(c));
                localFile.setType(this.__fileTypeConverter.b(b.getString(c2)));
            }
            return localFile;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public List<LocalFile> getListLocalFile() {
        Rh f = Rh.f("SELECT * FROM localfile", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "type");
            int c3 = Dx0.c(b, "fileName");
            int c4 = Dx0.c(b, "localUri");
            int c5 = Dx0.c(b, "remoteUrl");
            int c6 = Dx0.c(b, "checksum");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                LocalFile localFile = new LocalFile(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6));
                localFile.setPinType(b.getInt(c));
                localFile.setType(this.__fileTypeConverter.b(b.getString(c2)));
                arrayList.add(localFile);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public List<LocalFile> getListPendingFile() {
        Rh f = Rh.f("SELECT * FROM localfile WHERE pinType  <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "type");
            int c3 = Dx0.c(b, "fileName");
            int c4 = Dx0.c(b, "localUri");
            int c5 = Dx0.c(b, "remoteUrl");
            int c6 = Dx0.c(b, "checksum");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                LocalFile localFile = new LocalFile(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6));
                localFile.setPinType(b.getInt(c));
                localFile.setType(this.__fileTypeConverter.b(b.getString(c2)));
                arrayList.add(localFile);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public LiveData<List<LocalFile>> getListPendingFileAsLiveData() {
        Rh f = Rh.f("SELECT * FROM localfile WHERE pinType  <> 0", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon6 anon6 = new Anon6(f);
        return invalidationTracker.d(new String[]{"localfile"}, false, anon6);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public LocalFile getLocalFileByRemoteUrl(String str) {
        LocalFile localFile = null;
        Rh f = Rh.f("SELECT * FROM localfile WHERE remoteUrl=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "type");
            int c3 = Dx0.c(b, "fileName");
            int c4 = Dx0.c(b, "localUri");
            int c5 = Dx0.c(b, "remoteUrl");
            int c6 = Dx0.c(b, "checksum");
            if (b.moveToFirst()) {
                localFile = new LocalFile(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6));
                localFile.setPinType(b.getInt(c));
                localFile.setType(this.__fileTypeConverter.b(b.getString(c2)));
            }
            return localFile;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void insertListLocalFile(List<LocalFile> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfLocalFile.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.FileDao
    public void upsertLocalFile(LocalFile localFile) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfLocalFile.insert((Hh<LocalFile>) localFile);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
