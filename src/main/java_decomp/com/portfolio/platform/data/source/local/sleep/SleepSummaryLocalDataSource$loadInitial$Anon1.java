package com.portfolio.platform.data.source.local.sleep;

import com.mapped.PagingRequestHelper;
import com.mapped.Rm6;
import com.mapped.Wg6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummaryLocalDataSource$loadInitial$Anon1 implements PagingRequestHelper.Bi {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$0;

    @DexIgnore
    public SleepSummaryLocalDataSource$loadInitial$Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        this.this$0 = sleepSummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.mapped.PagingRequestHelper.Bi
    public final void run(PagingRequestHelper.Bi.Aii aii) {
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource = this.this$0;
        sleepSummaryLocalDataSource.calculateStartDate(sleepSummaryLocalDataSource.mCreatedDate);
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource2 = this.this$0;
        PagingRequestHelper.Di di = PagingRequestHelper.Di.INITIAL;
        Date mStartDate = sleepSummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        Wg6.b(aii, "helperCallback");
        Rm6 unused = sleepSummaryLocalDataSource2.loadData(di, mStartDate, mEndDate, aii);
    }
}
