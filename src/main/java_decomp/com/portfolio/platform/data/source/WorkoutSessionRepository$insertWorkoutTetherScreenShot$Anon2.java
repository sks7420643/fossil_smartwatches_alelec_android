package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.WorkoutSessionRepository$insertWorkoutTetherScreenShot$2", f = "WorkoutSessionRepository.kt", l = {161, 164}, m = "invokeSuspend")
public final class WorkoutSessionRepository$insertWorkoutTetherScreenShot$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $screenShotUri;
    @DexIgnore
    public /* final */ /* synthetic */ String $workoutId;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$insertWorkoutTetherScreenShot$Anon2(String str, String str2, Xe6 xe6) {
        super(2, xe6);
        this.$workoutId = str;
        this.$screenShotUri = str2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        WorkoutSessionRepository$insertWorkoutTetherScreenShot$Anon2 workoutSessionRepository$insertWorkoutTetherScreenShot$Anon2 = new WorkoutSessionRepository$insertWorkoutTetherScreenShot$Anon2(this.$workoutId, this.$screenShotUri, xe6);
        workoutSessionRepository$insertWorkoutTetherScreenShot$Anon2.p$ = (Il6) obj;
        throw null;
        //return workoutSessionRepository$insertWorkoutTetherScreenShot$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((WorkoutSessionRepository$insertWorkoutTetherScreenShot$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00cc A[RETURN, SYNTHETIC] */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r9) {
        /*
            r8 = this;
            r7 = 2
            r6 = 1
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r8.label
            if (r0 == 0) goto L_0x008a
            if (r0 == r6) goto L_0x005b
            if (r0 != r7) goto L_0x0053
            java.lang.Object r0 = r8.L$2
            com.portfolio.platform.data.model.diana.workout.WorkoutSession r0 = (com.portfolio.platform.data.model.diana.workout.WorkoutSession) r0
            java.lang.Object r0 = r8.L$1
            com.portfolio.platform.data.model.diana.workout.WorkoutSession r0 = (com.portfolio.platform.data.model.diana.workout.WorkoutSession) r0
            java.lang.Object r1 = r8.L$0
            com.mapped.Il6 r1 = (com.mapped.Il6) r1
            com.fossil.El7.b(r9)
            r1 = r9
            r2 = r0
        L_0x001f:
            r0 = r1
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            com.portfolio.platform.data.source.local.diana.workout.WorkoutDao r0 = r0.getWorkoutDao()
            r0.upsertWorkoutSession(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            com.portfolio.platform.data.source.WorkoutSessionRepository$Companion r1 = com.portfolio.platform.data.source.WorkoutSessionRepository.Companion
            java.lang.String r1 = r1.getTAG$app_fossilRelease()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "insertWorkoutTetherScreenShot: workoutId = "
            r2.append(r3)
            java.lang.String r3 = r8.$workoutId
            r2.append(r3)
            java.lang.String r3 = " DONE!!!"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0052:
            return r0
        L_0x0053:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005b:
            java.lang.Object r0 = r8.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r9)
            r2 = r0
            r1 = r9
        L_0x0064:
            r0 = r1
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            com.portfolio.platform.data.source.local.diana.workout.WorkoutDao r0 = r0.getWorkoutDao()
            java.lang.String r1 = r8.$workoutId
            com.portfolio.platform.data.model.diana.workout.WorkoutSession r0 = r0.getWorkoutSessionById(r1)
            if (r0 == 0) goto L_0x00cc
            java.lang.String r1 = r8.$screenShotUri
            r0.setScreenShotUri(r1)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r8.L$0 = r2
            r8.L$1 = r0
            r8.L$2 = r0
            r8.label = r7
            java.lang.Object r1 = r1.y(r8)
            if (r1 != r3) goto L_0x00c9
            r0 = r3
            goto L_0x0052
        L_0x008a:
            com.fossil.El7.b(r9)
            com.mapped.Il6 r0 = r8.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            com.portfolio.platform.data.source.WorkoutSessionRepository$Companion r2 = com.portfolio.platform.data.source.WorkoutSessionRepository.Companion
            java.lang.String r2 = r2.getTAG$app_fossilRelease()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "insertWorkoutTetherScreenShot: workoutId = "
            r4.append(r5)
            java.lang.String r5 = r8.$workoutId
            r4.append(r5)
            java.lang.String r5 = " screenshotUri "
            r4.append(r5)
            java.lang.String r5 = r8.$screenShotUri
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.d(r2, r4)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r8.L$0 = r0
            r8.label = r6
            java.lang.Object r1 = r1.y(r8)
            if (r1 != r3) goto L_0x00ce
            r0 = r3
            goto L_0x0052
        L_0x00c9:
            r2 = r0
            goto L_0x001f
        L_0x00cc:
            r0 = 0
            goto L_0x0052
        L_0x00ce:
            r2 = r0
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSessionRepository$insertWorkoutTetherScreenShot$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
