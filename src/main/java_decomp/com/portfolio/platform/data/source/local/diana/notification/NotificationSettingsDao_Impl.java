package com.portfolio.platform.data.source.local.diana.notification;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationSettingsDao_Impl implements NotificationSettingsDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<NotificationSettingsModel> __insertionAdapterOfNotificationSettingsModel;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<NotificationSettingsModel> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, NotificationSettingsModel notificationSettingsModel) {
            if (notificationSettingsModel.getSettingsName() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, notificationSettingsModel.getSettingsName());
            }
            mi.bindLong(2, (long) notificationSettingsModel.getSettingsType());
            mi.bindLong(3, notificationSettingsModel.isCall() ? 1 : 0);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, NotificationSettingsModel notificationSettingsModel) {
            bind(mi, notificationSettingsModel);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `notificationSettings` (`settingsName`,`settingsType`,`isCall`) VALUES (?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM notificationSettings";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<NotificationSettingsModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon3(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<NotificationSettingsModel> call() throws Exception {
            Cursor b = Ex0.b(NotificationSettingsDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "settingsName");
                int c2 = Dx0.c(b, "settingsType");
                int c3 = Dx0.c(b, "isCall");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new NotificationSettingsModel(b.getString(c), b.getInt(c2), b.getInt(c3) != 0));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<NotificationSettingsModel> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon4(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public NotificationSettingsModel call() throws Exception {
            NotificationSettingsModel notificationSettingsModel = null;
            boolean z = false;
            Cursor b = Ex0.b(NotificationSettingsDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "settingsName");
                int c2 = Dx0.c(b, "settingsType");
                int c3 = Dx0.c(b, "isCall");
                if (b.moveToFirst()) {
                    String string = b.getString(c);
                    int i = b.getInt(c2);
                    if (b.getInt(c3) != 0) {
                        z = true;
                    }
                    notificationSettingsModel = new NotificationSettingsModel(string, i, z);
                }
                return notificationSettingsModel;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public NotificationSettingsDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfNotificationSettingsModel = new Anon1(oh);
        this.__preparedStmtOfDelete = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public LiveData<List<NotificationSettingsModel>> getListNotificationSettings() {
        Rh f = Rh.f("SELECT * FROM notificationSettings", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"notificationSettings"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public List<NotificationSettingsModel> getListNotificationSettingsNoLiveData() {
        Rh f = Rh.f("SELECT * FROM notificationSettings", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "settingsName");
            int c2 = Dx0.c(b, "settingsType");
            int c3 = Dx0.c(b, "isCall");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new NotificationSettingsModel(b.getString(c), b.getInt(c2), b.getInt(c3) != 0));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public LiveData<NotificationSettingsModel> getNotificationSettingsWithFieldIsCall(boolean z) {
        Rh f = Rh.f("SELECT * FROM notificationSettings WHERE isCall = ?", 1);
        f.bindLong(1, z ? 1 : 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"notificationSettings"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public NotificationSettingsModel getNotificationSettingsWithIsCallNoLiveData(boolean z) {
        NotificationSettingsModel notificationSettingsModel = null;
        boolean z2 = true;
        Rh f = Rh.f("SELECT * FROM notificationSettings WHERE isCall = ?", 1);
        f.bindLong(1, z ? 1 : 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "settingsName");
            int c2 = Dx0.c(b, "settingsType");
            int c3 = Dx0.c(b, "isCall");
            if (b.moveToFirst()) {
                String string = b.getString(c);
                int i = b.getInt(c2);
                if (b.getInt(c3) == 0) {
                    z2 = false;
                }
                notificationSettingsModel = new NotificationSettingsModel(string, i, z2);
            }
            return notificationSettingsModel;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public void insertListNotificationSettings(List<NotificationSettingsModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfNotificationSettingsModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao
    public void insertNotificationSettings(NotificationSettingsModel notificationSettingsModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfNotificationSettingsModel.insert((Hh<NotificationSettingsModel>) notificationSettingsModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
