package com.portfolio.platform.data.source.local.sleep;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.Bw7;
import com.fossil.Gl5;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Hq5;
import com.fossil.Jv7;
import com.fossil.Kq5;
import com.fossil.Nw0;
import com.fossil.Pm7;
import com.fossil.wearables.fsl.sleep.MFSleepDay;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.mapped.Af;
import com.mapped.Ap4;
import com.mapped.Ku3;
import com.mapped.Lc6;
import com.mapped.PagingRequestHelper;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.U04;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummaryLocalDataSource extends Af<Date, SleepSummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "SleepSummaryLocalDataSource";
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ PagingRequestHelper.Ai listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public PagingRequestHelper mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ Nw0.Ci mObserver;
    @DexIgnore
    public List<Lc6<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public /* final */ SleepDatabase mSleepDatabase;
    @DexIgnore
    public /* final */ SleepSessionsRepository mSleepSessionsRepository;
    @DexIgnore
    public /* final */ SleepSummariesRepository mSleepSummariesRepository;
    @DexIgnore
    public Date mStartDate; // = new Date();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends Nw0.Ci {
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = sleepSummaryLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Nw0.Ci
        public void onInvalidated(Set<String> set) {
            Wg6.c(set, "tables");
            FLogger.INSTANCE.getLocal().d(SleepSummaryLocalDataSource.TAG, "XXX- sleep summary invalidate");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            Wg6.c(date, "date");
            Wg6.c(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(SleepSummaryLocalDataSource.TAG, "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar I = TimeUtils.I(instance);
            if (TimeUtils.j0(date2, I.getTime())) {
                I.setTime(date2);
            }
            Date time = I.getTime();
            Wg6.b(time, "nextPagedKey.time");
            return time;
        }
    }

    @DexIgnore
    public SleepSummaryLocalDataSource(SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDatabase sleepDatabase, Date date, U04 u04, PagingRequestHelper.Ai ai, Calendar calendar) {
        Wg6.c(sleepSummariesRepository, "mSleepSummariesRepository");
        Wg6.c(sleepSessionsRepository, "mSleepSessionsRepository");
        Wg6.c(fitnessDataRepository, "mFitnessDataRepository");
        Wg6.c(sleepDatabase, "mSleepDatabase");
        Wg6.c(date, "mCreatedDate");
        Wg6.c(u04, "appExecutors");
        Wg6.c(ai, "listener");
        Wg6.c(calendar, "key");
        this.mSleepSummariesRepository = sleepSummariesRepository;
        this.mSleepSessionsRepository = sleepSessionsRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mSleepDatabase = sleepDatabase;
        this.mCreatedDate = date;
        this.listener = ai;
        this.key = calendar;
        PagingRequestHelper pagingRequestHelper = new PagingRequestHelper(u04.a());
        this.mHelper = pagingRequestHelper;
        this.mNetworkState = Gl5.b(pagingRequestHelper);
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, MFSleepDay.TABLE_NAME, new String[]{MFSleepSession.TABLE_NAME});
        this.mSleepDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar I = TimeUtils.I(instance);
        I.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + I.getTime());
        Date time = I.getTime();
        Wg6.b(time, "calendar.time");
        this.mStartDate = time;
        if (TimeUtils.j0(date, time)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final List<SleepSummary> calculateSummaries(List<SleepSummary> list) {
        int i;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "endCalendar");
            instance.setTime(((SleepSummary) Pm7.P(list)).getDate());
            if (instance.get(7) != 1) {
                Calendar b0 = TimeUtils.b0(instance.getTime());
                Wg6.b(b0, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                SleepDao sleepDao = this.mSleepDatabase.sleepDao();
                Date time = b0.getTime();
                Wg6.b(time, "startCalendar.time");
                Date time2 = instance.getTime();
                Wg6.b(time2, "endCalendar.time");
                i = sleepDao.getTotalSleep(time, time2);
            } else {
                i = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            Wg6.b(instance2, "calendar");
            instance2.setTime(((SleepSummary) Pm7.F(list)).getDate());
            Calendar b02 = TimeUtils.b0(instance2.getTime());
            Wg6.b(b02, "DateHelper.getStartOfWeek(calendar.time)");
            b02.add(5, -1);
            Iterator<T> it = list.iterator();
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            double d = 0.0d;
            while (true) {
                double d2 = d;
                if (it.hasNext()) {
                    T next = it.next();
                    if (i3 >= 0) {
                        T t = next;
                        if (TimeUtils.m0(t.getDate(), b02.getTime())) {
                            com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay = list.get(i2).getSleepDay();
                            if (sleepDay != null) {
                                if (i4 > 1) {
                                    d2 /= (double) i4;
                                }
                                sleepDay.setAverageSleepOfWeek(Double.valueOf(d2));
                            }
                            b02.add(5, -7);
                            i4 = 0;
                            d2 = 0.0d;
                            i2 = i3;
                        }
                        com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay2 = t.getSleepDay();
                        double sleepMinutes = d2 + ((double) (sleepDay2 != null ? sleepDay2.getSleepMinutes() : 0));
                        com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay3 = t.getSleepDay();
                        if ((sleepDay3 != null ? sleepDay3.getSleepMinutes() : 0) > 0) {
                            i4++;
                        }
                        d = i3 == list.size() + -1 ? ((double) i) + sleepMinutes : sleepMinutes;
                        i3++;
                    } else {
                        Hm7.l();
                        throw null;
                    }
                } else {
                    com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay4 = list.get(i2).getSleepDay();
                    if (sleepDay4 != null) {
                        if (i4 > 1) {
                            d2 /= (double) i4;
                        }
                        sleepDay4.setAverageSleepOfWeek(Double.valueOf(d2));
                    }
                }
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "calculateSummaries summaries.size=" + list.size());
        return list;
    }

    @DexIgnore
    private final void combineData(PagingRequestHelper.Di di, Ap4<Ku3> ap4, Ap4<Ku3> ap42, PagingRequestHelper.Bi.Aii aii) {
        if (!(ap4 instanceof Kq5) || !(ap42 instanceof Kq5)) {
            String str = "";
            if (ap4 instanceof Hq5) {
                Hq5 hq5 = (Hq5) ap4;
                if (hq5.d() != null) {
                    aii.a(hq5.d());
                } else if (hq5.c() != null) {
                    ServerError c = hq5.c();
                    String userMessage = c.getUserMessage();
                    if (userMessage == null) {
                        userMessage = c.getMessage();
                    }
                    if (userMessage == null) {
                        userMessage = str;
                    }
                    aii.a(new Throwable(userMessage));
                }
            } else if (ap42 instanceof Hq5) {
                Hq5 hq52 = (Hq5) ap42;
                if (hq52.d() != null) {
                    aii.a(hq52.d());
                } else if (hq52.c() != null) {
                    ServerError c2 = hq52.c();
                    String userMessage2 = c2.getUserMessage();
                    if (userMessage2 == null) {
                        userMessage2 = c2.getMessage();
                    }
                    if (userMessage2 != null) {
                        str = userMessage2;
                    }
                    aii.a(new Throwable(str));
                }
            }
        } else {
            if (di == PagingRequestHelper.Di.AFTER && (!this.mRequestAfterQueue.isEmpty())) {
                this.mRequestAfterQueue.remove(0);
            }
            aii.b();
        }
    }

    @DexIgnore
    private final SleepSummary dummySummary(SleepSummary sleepSummary, Date date) {
        com.portfolio.platform.data.model.room.sleep.MFSleepDay sleepDay = sleepSummary.getSleepDay();
        return new SleepSummary(new com.portfolio.platform.data.model.room.sleep.MFSleepDay(date, sleepDay != null ? sleepDay.getGoalMinutes() : 0, 0, new SleepDistribution(0, 0, 0, 7, null), DateTime.now(), DateTime.now()), null, 2, null);
    }

    @DexIgnore
    private final List<SleepSummary> getDataInDatabase(Date date, Date date2) {
        SleepSummary sleepSummary;
        Date date3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "getDataInDatabase - startDate=" + date + ", endDate=" + date2);
        List<SleepSummary> calculateSummaries = calculateSummaries(this.mSleepDatabase.sleepDao().getSleepSummariesDesc(TimeUtils.j0(date, date2) ? date2 : date, date2));
        ArrayList arrayList = new ArrayList();
        com.portfolio.platform.data.model.room.sleep.MFSleepDay lastSleepDay = this.mSleepDatabase.sleepDao().getLastSleepDay();
        Date date4 = (lastSleepDay == null || (date3 = lastSleepDay.getDate()) == null) ? date : date3;
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(calculateSummaries);
        SleepSummary sleepSummary2 = this.mSleepDatabase.sleepDao().getSleepSummary(date2);
        SleepSummary sleepSummary3 = sleepSummary2 == null ? new SleepSummary(new com.portfolio.platform.data.model.room.sleep.MFSleepDay(date2, this.mSleepDatabase.sleepDao().getNearestSleepGoalFromDate(date2), 0, new SleepDistribution(0, 0, 0, 7, null), DateTime.now(), DateTime.now()), null, 2, null) : sleepSummary2;
        if (!TimeUtils.j0(date, date4)) {
            date = date4;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "getDataInDatabase - summaries.size=" + calculateSummaries.size() + ", summaryParent=" + sleepSummary3 + ", lastDate=" + date4 + ", startDateToFill=" + date);
        while (TimeUtils.k0(date2, date)) {
            Iterator it = arrayList2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    sleepSummary = null;
                    break;
                }
                Object next = it.next();
                if (TimeUtils.m0(((SleepSummary) next).getDate(), date2)) {
                    sleepSummary = next;
                    break;
                }
            }
            SleepSummary sleepSummary4 = sleepSummary;
            if (sleepSummary4 == null) {
                arrayList.add(dummySummary(sleepSummary3, date2));
            } else {
                arrayList.add(sleepSummary4);
                arrayList2.remove(sleepSummary4);
            }
            date2 = TimeUtils.P(date2);
            Wg6.b(date2, "DateHelper.getPrevDate(endDateToFill)");
        }
        if (!arrayList.isEmpty()) {
            SleepSummary sleepSummary5 = (SleepSummary) Pm7.F(arrayList);
            Boolean p0 = TimeUtils.p0(sleepSummary5.getDate());
            Wg6.b(p0, "DateHelper.isToday(todaySummary.getDate())");
            if (p0.booleanValue()) {
                arrayList.add(0, sleepSummary5.copy(sleepSummary5.getSleepDay(), sleepSummary5.getSleepSessions()));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final Rm6 loadData(PagingRequestHelper.Di di, Date date, Date date2, PagingRequestHelper.Bi.Aii aii) {
        return Gu7.d(Jv7.a(Bw7.b()), null, null, new SleepSummaryLocalDataSource$loadData$Anon1(this, date, date2, di, aii, null), 3, null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final PagingRequestHelper getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public boolean isInvalid() {
        this.mSleepDatabase.getInvalidationTracker().h();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadAfter(Af.Fi<Date> fi, Af.Ai<Date, SleepSummary> ai) {
        Wg6.c(fi, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ai, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Object) fi.a));
        if (TimeUtils.j0(fi.a, this.mCreatedDate)) {
            Key key2 = fi.a;
            Wg6.b(key2, "params.key");
            Key key3 = key2;
            Companion companion = Companion;
            Key key4 = fi.a;
            Wg6.b(key4, "params.key");
            Date calculateNextKey = companion.calculateNextKey(key4, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date O = TimeUtils.m0(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : TimeUtils.O(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + O + ", endQueryDate=" + ((Object) key3));
            Wg6.b(O, "startQueryDate");
            ai.a(getDataInDatabase(O, key3), this.key.getTime());
            if (TimeUtils.j0(this.mStartDate, key3)) {
                this.mEndDate = key3;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d(TAG, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new Lc6<>(this.mStartDate, this.mEndDate));
                this.mHelper.h(PagingRequestHelper.Di.AFTER, new SleepSummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadBefore(Af.Fi<Date> fi, Af.Ai<Date, SleepSummary> ai) {
        Wg6.c(fi, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ai, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadInitial(Af.Ei<Date> ei, Af.Ci<Date, SleepSummary> ci) {
        Wg6.c(ei, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ci, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date O = TimeUtils.m0(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : TimeUtils.O(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + O + ", endQueryDate=" + date);
        Wg6.b(O, "startQueryDate");
        ci.a(getDataInDatabase(O, date), null, this.key.getTime());
        this.mHelper.h(PagingRequestHelper.Di.INITIAL, new SleepSummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.f(this.listener);
        this.mSleepDatabase.getInvalidationTracker().j(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        Wg6.c(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(PagingRequestHelper pagingRequestHelper) {
        Wg6.c(pagingRequestHelper, "<set-?>");
        this.mHelper = pagingRequestHelper;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        Wg6.c(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        Wg6.c(date, "<set-?>");
        this.mStartDate = date;
    }
}
