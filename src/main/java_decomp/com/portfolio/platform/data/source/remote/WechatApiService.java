package com.portfolio.platform.data.source.remote;

import com.fossil.Y98;
import com.mapped.Ny6;
import com.portfolio.platform.data.WechatToken;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface WechatApiService {
    @DexIgnore
    @Ny6("oauth2/access_token")
    Call<WechatToken> getWechatToken(@Y98("appid") String str, @Y98("secret") String str2, @Y98("code") String str3, @Y98("grant_type") String str4);
}
