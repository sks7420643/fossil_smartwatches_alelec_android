package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.ActivitiesRepository$fetchActivitySamples$2", f = "ActivitiesRepository.kt", l = {145, 146, 158}, m = "invokeSuspend")
public final class ActivitiesRepository$fetchActivitySamples$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<ApiResponse<Activity>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ int $limit;
    @DexIgnore
    public /* final */ /* synthetic */ int $offset;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$fetchActivitySamples$Anon2(ActivitiesRepository activitiesRepository, Date date, Date date2, int i, int i2, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = activitiesRepository;
        this.$start = date;
        this.$end = date2;
        this.$offset = i;
        this.$limit = i2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        ActivitiesRepository$fetchActivitySamples$Anon2 activitiesRepository$fetchActivitySamples$Anon2 = new ActivitiesRepository$fetchActivitySamples$Anon2(this.this$0, this.$start, this.$end, this.$offset, this.$limit, xe6);
        activitiesRepository$fetchActivitySamples$Anon2.p$ = (Il6) obj;
        throw null;
        //return activitiesRepository$fetchActivitySamples$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<ApiResponse<Activity>>> xe6) {
        throw null;
        //return ((ActivitiesRepository$fetchActivitySamples$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x016a  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r13) {
        /*
        // Method dump skipped, instructions count: 452
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ActivitiesRepository$fetchActivitySamples$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
