package com.portfolio.platform.data.source.local.label;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Label {
    @DexIgnore
    public /* final */ String category;
    @DexIgnore
    public /* final */ String createdAt;
    @DexIgnore
    public /* final */ Data data;
    @DexIgnore
    public /* final */ String id;
    @DexIgnore
    public /* final */ Metadata metadata;
    @DexIgnore
    public /* final */ String name;
    @DexIgnore
    public /* final */ String updatedAt;

    @DexIgnore
    public Label(String str, String str2, String str3, Data data2, Metadata metadata2, String str4, String str5) {
        Wg6.c(data2, "data");
        this.id = str;
        this.category = str2;
        this.name = str3;
        this.data = data2;
        this.metadata = metadata2;
        this.createdAt = str4;
        this.updatedAt = str5;
    }

    @DexIgnore
    public static /* synthetic */ Label copy$default(Label label, String str, String str2, String str3, Data data2, Metadata metadata2, String str4, String str5, int i, Object obj) {
        return label.copy((i & 1) != 0 ? label.id : str, (i & 2) != 0 ? label.category : str2, (i & 4) != 0 ? label.name : str3, (i & 8) != 0 ? label.data : data2, (i & 16) != 0 ? label.metadata : metadata2, (i & 32) != 0 ? label.createdAt : str4, (i & 64) != 0 ? label.updatedAt : str5);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.category;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final Data component4() {
        return this.data;
    }

    @DexIgnore
    public final Metadata component5() {
        return this.metadata;
    }

    @DexIgnore
    public final String component6() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component7() {
        return this.updatedAt;
    }

    @DexIgnore
    public final Label copy(String str, String str2, String str3, Data data2, Metadata metadata2, String str4, String str5) {
        Wg6.c(data2, "data");
        return new Label(str, str2, str3, data2, metadata2, str4, str5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Label) {
                Label label = (Label) obj;
                if (!Wg6.a(this.id, label.id) || !Wg6.a(this.category, label.category) || !Wg6.a(this.name, label.name) || !Wg6.a(this.data, label.data) || !Wg6.a(this.metadata, label.metadata) || !Wg6.a(this.createdAt, label.createdAt) || !Wg6.a(this.updatedAt, label.updatedAt)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getCategory() {
        return this.category;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Data getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final Metadata getMetadata() {
        return this.metadata;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.category;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.name;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        Data data2 = this.data;
        int hashCode4 = data2 != null ? data2.hashCode() : 0;
        Metadata metadata2 = this.metadata;
        int hashCode5 = metadata2 != null ? metadata2.hashCode() : 0;
        String str4 = this.createdAt;
        int hashCode6 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.updatedAt;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return (((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Label(id=" + this.id + ", category=" + this.category + ", name=" + this.name + ", data=" + this.data + ", metadata=" + this.metadata + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
