package com.portfolio.platform.data.source;

import com.fossil.Ft4;
import com.fossil.Lk7;
import com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesNotificationDaoFactory implements Factory<Ft4> {
    @DexIgnore
    public /* final */ Provider<BuddyChallengeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesNotificationDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<BuddyChallengeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesNotificationDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<BuddyChallengeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvidesNotificationDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static Ft4 providesNotificationDao(PortfolioDatabaseModule portfolioDatabaseModule, BuddyChallengeDatabase buddyChallengeDatabase) {
        Ft4 providesNotificationDao = portfolioDatabaseModule.providesNotificationDao(buddyChallengeDatabase);
        Lk7.c(providesNotificationDao, "Cannot return null from a non-@Nullable @Provides method");
        return providesNotificationDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public Ft4 get() {
        return providesNotificationDao(this.module, this.dbProvider.get());
    }
}
