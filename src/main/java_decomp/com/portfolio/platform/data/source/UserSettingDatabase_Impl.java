package com.portfolio.platform.data.source;

import com.fossil.Ex0;
import com.fossil.Hw0;
import com.fossil.Ix0;
import com.fossil.Lx0;
import com.fossil.Nw0;
import com.mapped.Ji;
import com.mapped.Oh;
import com.mapped.Qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserSettingDatabase_Impl extends UserSettingDatabase {
    @DexIgnore
    public volatile UserSettingDao _userSettingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Qh.Ai {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void createAllTables(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `userSettings` (`isShowGoalRing` INTEGER NOT NULL, `acceptedLocationDataSharing` TEXT NOT NULL, `acceptedPrivacies` TEXT NOT NULL, `acceptedTermsOfService` TEXT NOT NULL, `uid` TEXT NOT NULL, `id` TEXT, `isLatestLocationDataSharingAccepted` INTEGER NOT NULL, `isLatestPrivacyAccepted` INTEGER NOT NULL, `isLatestTermsOfServiceAccepted` INTEGER NOT NULL, `latestLocationDataSharingVersion` TEXT, `latestPrivacyVersion` TEXT, `latestTermsOfServiceVersion` TEXT, `startDayOfWeek` TEXT, `createdAt` TEXT, `updatedAt` TEXT, `pinType` INTEGER NOT NULL, PRIMARY KEY(`uid`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'acf19a46886838b5899188682e5ab582')");
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void dropAllTables(Lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `userSettings`");
            if (UserSettingDatabase_Impl.this.mCallbacks != null) {
                int size = UserSettingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) UserSettingDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onCreate(Lx0 lx0) {
            if (UserSettingDatabase_Impl.this.mCallbacks != null) {
                int size = UserSettingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) UserSettingDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onOpen(Lx0 lx0) {
            UserSettingDatabase_Impl.this.mDatabase = lx0;
            UserSettingDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (UserSettingDatabase_Impl.this.mCallbacks != null) {
                int size = UserSettingDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) UserSettingDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPostMigrate(Lx0 lx0) {
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPreMigrate(Lx0 lx0) {
            Ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public Qh.Bi onValidateSchema(Lx0 lx0) {
            HashMap hashMap = new HashMap(16);
            hashMap.put("isShowGoalRing", new Ix0.Ai("isShowGoalRing", "INTEGER", true, 0, null, 1));
            hashMap.put("acceptedLocationDataSharing", new Ix0.Ai("acceptedLocationDataSharing", "TEXT", true, 0, null, 1));
            hashMap.put("acceptedPrivacies", new Ix0.Ai("acceptedPrivacies", "TEXT", true, 0, null, 1));
            hashMap.put("acceptedTermsOfService", new Ix0.Ai("acceptedTermsOfService", "TEXT", true, 0, null, 1));
            hashMap.put("uid", new Ix0.Ai("uid", "TEXT", true, 1, null, 1));
            hashMap.put("id", new Ix0.Ai("id", "TEXT", false, 0, null, 1));
            hashMap.put("isLatestLocationDataSharingAccepted", new Ix0.Ai("isLatestLocationDataSharingAccepted", "INTEGER", true, 0, null, 1));
            hashMap.put("isLatestPrivacyAccepted", new Ix0.Ai("isLatestPrivacyAccepted", "INTEGER", true, 0, null, 1));
            hashMap.put("isLatestTermsOfServiceAccepted", new Ix0.Ai("isLatestTermsOfServiceAccepted", "INTEGER", true, 0, null, 1));
            hashMap.put("latestLocationDataSharingVersion", new Ix0.Ai("latestLocationDataSharingVersion", "TEXT", false, 0, null, 1));
            hashMap.put("latestPrivacyVersion", new Ix0.Ai("latestPrivacyVersion", "TEXT", false, 0, null, 1));
            hashMap.put("latestTermsOfServiceVersion", new Ix0.Ai("latestTermsOfServiceVersion", "TEXT", false, 0, null, 1));
            hashMap.put("startDayOfWeek", new Ix0.Ai("startDayOfWeek", "TEXT", false, 0, null, 1));
            hashMap.put("createdAt", new Ix0.Ai("createdAt", "TEXT", false, 0, null, 1));
            hashMap.put("updatedAt", new Ix0.Ai("updatedAt", "TEXT", false, 0, null, 1));
            hashMap.put("pinType", new Ix0.Ai("pinType", "INTEGER", true, 0, null, 1));
            Ix0 ix0 = new Ix0("userSettings", hashMap, new HashSet(0), new HashSet(0));
            Ix0 a2 = Ix0.a(lx0, "userSettings");
            if (ix0.equals(a2)) {
                return new Qh.Bi(true, null);
            }
            return new Qh.Bi(false, "userSettings(com.portfolio.platform.data.model.UserSettings).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public void clearAllTables() {
        super.assertNotMainThread();
        Lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `userSettings`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Nw0 createInvalidationTracker() {
        return new Nw0(this, new HashMap(0), new HashMap(0), "userSettings");
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Ji createOpenHelper(Hw0 hw0) {
        Qh qh = new Qh(hw0, new Anon1(1), "acf19a46886838b5899188682e5ab582", "c5f82b465e65adc1cedba3c9f8395495");
        Ji.Bi.Aii a2 = Ji.Bi.a(hw0.b);
        a2.c(hw0.c);
        a2.b(qh);
        return hw0.a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDatabase
    public UserSettingDao userSettingDao() {
        UserSettingDao userSettingDao;
        if (this._userSettingDao != null) {
            return this._userSettingDao;
        }
        synchronized (this) {
            if (this._userSettingDao == null) {
                this._userSettingDao = new UserSettingDao_Impl(this);
            }
            userSettingDao = this._userSettingDao;
        }
        return userSettingDao;
    }
}
