package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Ko7;
import com.fossil.Qq7;
import com.fossil.Ss0;
import com.fossil.Yn7;
import com.fossil.Zt0;
import com.mapped.Cd6;
import com.mapped.Cf;
import com.mapped.Coroutine;
import com.mapped.Gg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.PagingRequestHelper;
import com.mapped.U04;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDataSourceFactory;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionLocalDataSource;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessionsPaging$2", f = "WorkoutSessionRepository.kt", l = {127}, m = "invokeSuspend")
public final class WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Listing<WorkoutSession>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ U04 $appExecutors;
    @DexIgnore
    public /* final */ /* synthetic */ Date $currentDate;
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.Ai $listener;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository $workoutSessionRepository;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public static /* final */ Anon1_Level2 INSTANCE; // = new Anon1_Level2();

        @DexIgnore
        public final LiveData<NetworkState> apply(WorkoutSessionLocalDataSource workoutSessionLocalDataSource) {
            return workoutSessionLocalDataSource.getMNetworkState();
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return apply((WorkoutSessionLocalDataSource) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2_Level2 extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(WorkoutSessionDataSourceFactory workoutSessionDataSourceFactory) {
            super(0);
            this.$sourceFactory = workoutSessionDataSourceFactory;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            WorkoutSessionLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
            if (e != null) {
                e.invalidate();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3_Level2 extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(WorkoutSessionDataSourceFactory workoutSessionDataSourceFactory) {
            super(0);
            this.$sourceFactory = workoutSessionDataSourceFactory;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            PagingRequestHelper mHelper;
            WorkoutSessionLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
            if (e != null && (mHelper = e.getMHelper()) != null) {
                mHelper.g();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2(WorkoutSessionRepository workoutSessionRepository, WorkoutSessionRepository workoutSessionRepository2, Date date, U04 u04, PagingRequestHelper.Ai ai, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = workoutSessionRepository;
        this.$workoutSessionRepository = workoutSessionRepository2;
        this.$currentDate = date;
        this.$appExecutors = u04;
        this.$listener = ai;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2 workoutSessionRepository$getWorkoutSessionsPaging$Anon2 = new WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2(this.this$0, this.$workoutSessionRepository, this.$currentDate, this.$appExecutors, this.$listener, xe6);
        workoutSessionRepository$getWorkoutSessionsPaging$Anon2.p$ = (Il6) obj;
        throw null;
        //return workoutSessionRepository$getWorkoutSessionsPaging$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Listing<WorkoutSession>> xe6) {
        throw null;
        //return ((WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object g;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            Dv7 b = Bw7.b();
            WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2$fitnessDatabase$Anon1_Level2 workoutSessionRepository$getWorkoutSessionsPaging$Anon2$fitnessDatabase$Anon1_Level2 = new WorkoutSessionRepository$getWorkoutSessionsPaging$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = il6;
            this.label = 1;
            g = Eu7.g(b, workoutSessionRepository$getWorkoutSessionsPaging$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (g == d) {
                return d;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            g = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        WorkoutSessionDataSourceFactory workoutSessionDataSourceFactory = new WorkoutSessionDataSourceFactory(this.$workoutSessionRepository, (FitnessDatabase) g, this.$currentDate, this.$appExecutors, this.$listener);
        this.this$0.mSourceDataFactoryList.add(workoutSessionDataSourceFactory);
        Cf.Fi.Aii aii = new Cf.Fi.Aii();
        aii.c(100);
        aii.b(false);
        aii.d(100);
        aii.e(5);
        Cf.Fi a2 = aii.a();
        Wg6.b(a2, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a3 = new Zt0(workoutSessionDataSourceFactory, a2).a();
        Wg6.b(a3, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData c = Ss0.c(workoutSessionDataSourceFactory.getSourceLiveData(), Anon1_Level2.INSTANCE);
        Wg6.b(c, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing(a3, c, new Anon2_Level2(workoutSessionDataSourceFactory), new Anon3_Level2(workoutSessionDataSourceFactory));
    }
}
