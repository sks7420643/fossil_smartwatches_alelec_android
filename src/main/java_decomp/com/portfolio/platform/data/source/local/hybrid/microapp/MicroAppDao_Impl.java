package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.E05;
import com.fossil.Ex0;
import com.fossil.G78;
import com.fossil.Hx0;
import com.fossil.M05;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.room.microapp.DeclarationFile;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppDao_Impl implements MicroAppDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<DeclarationFile> __insertionAdapterOfDeclarationFile;
    @DexIgnore
    public /* final */ Hh<MicroApp> __insertionAdapterOfMicroApp;
    @DexIgnore
    public /* final */ Hh<MicroAppSetting> __insertionAdapterOfMicroAppSetting;
    @DexIgnore
    public /* final */ Hh<MicroAppVariant> __insertionAdapterOfMicroAppVariant;
    @DexIgnore
    public /* final */ E05 __millisecondDateConverter; // = new E05();
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAllDeclarationFileTable;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAllMicroAppGalleryTable;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAllMicroAppSettingTable;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAllMicroAppVariantTable;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteMicroAppsBySerial;
    @DexIgnore
    public /* final */ M05 __stringArrayConverter; // = new M05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<MicroApp> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, MicroApp microApp) {
            if (microApp.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, microApp.getId());
            }
            if (microApp.getName() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, microApp.getName());
            }
            if (microApp.getNameKey() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, microApp.getNameKey());
            }
            if (microApp.getSerialNumber() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, microApp.getSerialNumber());
            }
            String b = MicroAppDao_Impl.this.__stringArrayConverter.b(microApp.getCategories());
            if (b == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, b);
            }
            if (microApp.getDescription() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, microApp.getDescription());
            }
            if (microApp.getDescriptionKey() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, microApp.getDescriptionKey());
            }
            if (microApp.getIcon() == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, microApp.getIcon());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, MicroApp microApp) {
            bind(mi, microApp);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microApp` (`id`,`name`,`nameKey`,`serialNumber`,`categories`,`description`,`descriptionKey`,`icon`) VALUES (?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Hh<MicroAppVariant> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, MicroAppVariant microAppVariant) {
            if (microAppVariant.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, microAppVariant.getId());
            }
            if (microAppVariant.getAppId() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, microAppVariant.getAppId());
            }
            if (microAppVariant.getName() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, microAppVariant.getName());
            }
            if (microAppVariant.getDescription() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, microAppVariant.getDescription());
            }
            mi.bindLong(5, MicroAppDao_Impl.this.__millisecondDateConverter.b(microAppVariant.getCreatedAt()));
            mi.bindLong(6, MicroAppDao_Impl.this.__millisecondDateConverter.b(microAppVariant.getUpdatedAt()));
            mi.bindLong(7, (long) microAppVariant.getMajorNumber());
            mi.bindLong(8, (long) microAppVariant.getMinorNumber());
            if (microAppVariant.getSerialNumber() == null) {
                mi.bindNull(9);
            } else {
                mi.bindString(9, microAppVariant.getSerialNumber());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, MicroAppVariant microAppVariant) {
            bind(mi, microAppVariant);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microAppVariant` (`id`,`appId`,`name`,`description`,`createdAt`,`updatedAt`,`majorNumber`,`minorNumber`,`serialNumber`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Hh<MicroAppSetting> {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, MicroAppSetting microAppSetting) {
            if (microAppSetting.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, microAppSetting.getId());
            }
            if (microAppSetting.getAppId() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, microAppSetting.getAppId());
            }
            if (microAppSetting.getSetting() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, microAppSetting.getSetting());
            }
            if (microAppSetting.getCreatedAt() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, microAppSetting.getCreatedAt());
            }
            if (microAppSetting.getUpdatedAt() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, microAppSetting.getUpdatedAt());
            }
            mi.bindLong(6, (long) microAppSetting.getPinType());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, MicroAppSetting microAppSetting) {
            bind(mi, microAppSetting);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `microAppSetting` (`id`,`appId`,`setting`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends Hh<DeclarationFile> {
        @DexIgnore
        public Anon4(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, DeclarationFile declarationFile) {
            String str = declarationFile.appId;
            if (str == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, str);
            }
            String str2 = declarationFile.serialNumber;
            if (str2 == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, str2);
            }
            String str3 = declarationFile.variantName;
            if (str3 == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, str3);
            }
            if (declarationFile.getId() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, declarationFile.getId());
            }
            if (declarationFile.getDescription() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, declarationFile.getDescription());
            }
            if (declarationFile.getContent() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, declarationFile.getContent());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, DeclarationFile declarationFile) {
            bind(mi, declarationFile);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `declarationFile` (`appId`,`serialNumber`,`variantName`,`id`,`description`,`content`) VALUES (?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends Vh {
        @DexIgnore
        public Anon5(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM microApp";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends Vh {
        @DexIgnore
        public Anon6(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM microAppSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends Vh {
        @DexIgnore
        public Anon7(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM microAppVariant";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 extends Vh {
        @DexIgnore
        public Anon8(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM declarationFile";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 extends Vh {
        @DexIgnore
        public Anon9(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM microApp WHERE serialNumber = ?";
        }
    }

    @DexIgnore
    public MicroAppDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfMicroApp = new Anon1(oh);
        this.__insertionAdapterOfMicroAppVariant = new Anon2(oh);
        this.__insertionAdapterOfMicroAppSetting = new Anon3(oh);
        this.__insertionAdapterOfDeclarationFile = new Anon4(oh);
        this.__preparedStmtOfClearAllMicroAppGalleryTable = new Anon5(oh);
        this.__preparedStmtOfClearAllMicroAppSettingTable = new Anon6(oh);
        this.__preparedStmtOfClearAllMicroAppVariantTable = new Anon7(oh);
        this.__preparedStmtOfClearAllDeclarationFileTable = new Anon8(oh);
        this.__preparedStmtOfDeleteMicroAppsBySerial = new Anon9(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void clearAllDeclarationFileTable() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAllDeclarationFileTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllDeclarationFileTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void clearAllMicroApp() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAllMicroAppGalleryTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppGalleryTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void clearAllMicroAppGalleryTable() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAllMicroAppGalleryTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppGalleryTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void clearAllMicroAppSettingTable() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAllMicroAppSettingTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppSettingTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void clearAllMicroAppVariantTable() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAllMicroAppVariantTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllMicroAppVariantTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void deleteMicroAppsBySerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteMicroAppsBySerial.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteMicroAppsBySerial.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<DeclarationFile> getAllDeclarationFile() {
        Rh f = Rh.f("SELECT*FROM declarationFile", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "appId");
            int c2 = Dx0.c(b, "serialNumber");
            int c3 = Dx0.c(b, "variantName");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "description");
            int c6 = Dx0.c(b, "content");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DeclarationFile declarationFile = new DeclarationFile(b.getString(c4), b.getString(c5), b.getString(c6));
                declarationFile.appId = b.getString(c);
                declarationFile.serialNumber = b.getString(c2);
                declarationFile.variantName = b.getString(c3);
                arrayList.add(declarationFile);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroAppVariant> getAllMicroAppVariant(String str, int i) {
        Rh f = Rh.f("SELECT * FROM microAppVariant WHERE serialNumber = ? AND majorNumber = ?", 2);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        f.bindLong(2, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "appId");
            int c3 = Dx0.c(b, "name");
            int c4 = Dx0.c(b, "description");
            int c5 = Dx0.c(b, "createdAt");
            int c6 = Dx0.c(b, "updatedAt");
            int c7 = Dx0.c(b, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int c8 = Dx0.c(b, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int c9 = Dx0.c(b, "serialNumber");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new MicroAppVariant(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), this.__millisecondDateConverter.a(b.getLong(c5)), this.__millisecondDateConverter.a(b.getLong(c6)), b.getInt(c7), b.getInt(c8), b.getString(c9)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroAppVariant> getAllMicroAppVariantUniqueBySerial() {
        Rh f = Rh.f("SELECT * FROM microAppVariant GROUP BY serialNumber", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "appId");
            int c3 = Dx0.c(b, "name");
            int c4 = Dx0.c(b, "description");
            int c5 = Dx0.c(b, "createdAt");
            int c6 = Dx0.c(b, "updatedAt");
            int c7 = Dx0.c(b, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int c8 = Dx0.c(b, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int c9 = Dx0.c(b, "serialNumber");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new MicroAppVariant(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), this.__millisecondDateConverter.a(b.getLong(c5)), this.__millisecondDateConverter.a(b.getLong(c6)), b.getInt(c7), b.getInt(c8), b.getString(c9)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroAppVariant> getAllMicroAppVariants() {
        Rh f = Rh.f("SELECT*FROM microAppVariant", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "appId");
            int c3 = Dx0.c(b, "name");
            int c4 = Dx0.c(b, "description");
            int c5 = Dx0.c(b, "createdAt");
            int c6 = Dx0.c(b, "updatedAt");
            int c7 = Dx0.c(b, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int c8 = Dx0.c(b, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int c9 = Dx0.c(b, "serialNumber");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new MicroAppVariant(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), this.__millisecondDateConverter.a(b.getLong(c5)), this.__millisecondDateConverter.a(b.getLong(c6)), b.getInt(c7), b.getInt(c8), b.getString(c9)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroApp> getAllMicroApps() {
        Rh f = Rh.f("SELECT*FROM microApp", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "name");
            int c3 = Dx0.c(b, "nameKey");
            int c4 = Dx0.c(b, "serialNumber");
            int c5 = Dx0.c(b, "categories");
            int c6 = Dx0.c(b, "description");
            int c7 = Dx0.c(b, "descriptionKey");
            int c8 = Dx0.c(b, "icon");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new MicroApp(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), this.__stringArrayConverter.a(b.getString(c5)), b.getString(c6), b.getString(c7), b.getString(c8)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<DeclarationFile> getDeclarationFiles(String str, String str2, String str3) {
        Rh f = Rh.f("SELECT * FROM declarationFile WHERE appId = ? AND serialNumber = ? AND variantName = ? ORDER BY rowid ASC", 3);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        if (str2 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, str2);
        }
        if (str3 == null) {
            f.bindNull(3);
        } else {
            f.bindString(3, str3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "appId");
            int c2 = Dx0.c(b, "serialNumber");
            int c3 = Dx0.c(b, "variantName");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "description");
            int c6 = Dx0.c(b, "content");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DeclarationFile declarationFile = new DeclarationFile(b.getString(c4), b.getString(c5), b.getString(c6));
                declarationFile.appId = b.getString(c);
                declarationFile.serialNumber = b.getString(c2);
                declarationFile.variantName = b.getString(c3);
                arrayList.add(declarationFile);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroApp> getListMicroApp(String str) {
        Rh f = Rh.f("SELECT * FROM microApp WHERE serialNumber = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "name");
            int c3 = Dx0.c(b, "nameKey");
            int c4 = Dx0.c(b, "serialNumber");
            int c5 = Dx0.c(b, "categories");
            int c6 = Dx0.c(b, "description");
            int c7 = Dx0.c(b, "descriptionKey");
            int c8 = Dx0.c(b, "icon");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new MicroApp(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), this.__stringArrayConverter.a(b.getString(c5)), b.getString(c6), b.getString(c7), b.getString(c8)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroAppSetting> getListMicroAppSetting() {
        Rh f = Rh.f("SELECT * FROM microAppSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "appId");
            int c3 = Dx0.c(b, com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting.SETTING);
            int c4 = Dx0.c(b, "createdAt");
            int c5 = Dx0.c(b, "updatedAt");
            int c6 = Dx0.c(b, "pinType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new MicroAppSetting(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5), b.getInt(c6)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public MicroApp getMicroApp(String str, String str2) {
        MicroApp microApp = null;
        Rh f = Rh.f("SELECT * FROM microApp WHERE serialNumber = ? AND id = ?", 2);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        if (str2 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "name");
            int c3 = Dx0.c(b, "nameKey");
            int c4 = Dx0.c(b, "serialNumber");
            int c5 = Dx0.c(b, "categories");
            int c6 = Dx0.c(b, "description");
            int c7 = Dx0.c(b, "descriptionKey");
            int c8 = Dx0.c(b, "icon");
            if (b.moveToFirst()) {
                microApp = new MicroApp(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), this.__stringArrayConverter.a(b.getString(c5)), b.getString(c6), b.getString(c7), b.getString(c8));
            }
            return microApp;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public MicroApp getMicroAppById(String str, String str2) {
        MicroApp microApp = null;
        Rh f = Rh.f("SELECT * FROM microApp WHERE id=? AND serialNumber=?", 2);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        if (str2 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "name");
            int c3 = Dx0.c(b, "nameKey");
            int c4 = Dx0.c(b, "serialNumber");
            int c5 = Dx0.c(b, "categories");
            int c6 = Dx0.c(b, "description");
            int c7 = Dx0.c(b, "descriptionKey");
            int c8 = Dx0.c(b, "icon");
            if (b.moveToFirst()) {
                microApp = new MicroApp(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), this.__stringArrayConverter.a(b.getString(c5)), b.getString(c6), b.getString(c7), b.getString(c8));
            }
            return microApp;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroApp> getMicroAppByIds(List<String> list, String str) {
        StringBuilder b = Hx0.b();
        b.append("SELECT ");
        b.append(G78.ANY_MARKER);
        b.append(" FROM microApp WHERE id IN (");
        int size = list.size();
        Hx0.a(b, size);
        b.append(") AND serialNumber=");
        b.append("?");
        String sb = b.toString();
        int i = 1;
        int i2 = size + 1;
        Rh f = Rh.f(sb, i2);
        for (String str2 : list) {
            if (str2 == null) {
                f.bindNull(i);
            } else {
                f.bindString(i, str2);
            }
            i++;
        }
        if (str == null) {
            f.bindNull(i2);
        } else {
            f.bindString(i2, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b2, "id");
            int c2 = Dx0.c(b2, "name");
            int c3 = Dx0.c(b2, "nameKey");
            int c4 = Dx0.c(b2, "serialNumber");
            int c5 = Dx0.c(b2, "categories");
            int c6 = Dx0.c(b2, "description");
            int c7 = Dx0.c(b2, "descriptionKey");
            int c8 = Dx0.c(b2, "icon");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(new MicroApp(b2.getString(c), b2.getString(c2), b2.getString(c3), b2.getString(c4), this.__stringArrayConverter.a(b2.getString(c5)), b2.getString(c6), b2.getString(c7), b2.getString(c8)));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public MicroAppSetting getMicroAppSetting(String str) {
        MicroAppSetting microAppSetting = null;
        Rh f = Rh.f("SELECT * FROM microAppSetting WHERE appId= ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "appId");
            int c3 = Dx0.c(b, com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting.SETTING);
            int c4 = Dx0.c(b, "createdAt");
            int c5 = Dx0.c(b, "updatedAt");
            int c6 = Dx0.c(b, "pinType");
            if (b.moveToFirst()) {
                microAppSetting = new MicroAppSetting(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5), b.getInt(c6));
            }
            return microAppSetting;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public MicroAppVariant getMicroAppVariant(String str, String str2, int i) {
        Rh f = Rh.f("SELECT * FROM microAppVariant WHERE appId = ? AND serialNumber = ? AND majorNumber = ?", 3);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        if (str2 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, str2);
        }
        f.bindLong(3, (long) i);
        this.__db.assertNotSuspendingTransaction();
        MicroAppVariant microAppVariant = null;
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "appId");
            int c3 = Dx0.c(b, "name");
            int c4 = Dx0.c(b, "description");
            int c5 = Dx0.c(b, "createdAt");
            int c6 = Dx0.c(b, "updatedAt");
            int c7 = Dx0.c(b, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int c8 = Dx0.c(b, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int c9 = Dx0.c(b, "serialNumber");
            if (b.moveToFirst()) {
                microAppVariant = new MicroAppVariant(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), this.__millisecondDateConverter.a(b.getLong(c5)), this.__millisecondDateConverter.a(b.getLong(c6)), b.getInt(c7), b.getInt(c8), b.getString(c9));
            }
            return microAppVariant;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public MicroAppVariant getMicroAppVariant(String str, String str2, int i, String str3) {
        Rh f = Rh.f("SELECT * FROM microAppVariant WHERE appId = ? AND serialNumber = ? AND majorNumber = ? AND name = ?", 4);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        if (str2 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, str2);
        }
        f.bindLong(3, (long) i);
        if (str3 == null) {
            f.bindNull(4);
        } else {
            f.bindString(4, str3);
        }
        this.__db.assertNotSuspendingTransaction();
        MicroAppVariant microAppVariant = null;
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "appId");
            int c3 = Dx0.c(b, "name");
            int c4 = Dx0.c(b, "description");
            int c5 = Dx0.c(b, "createdAt");
            int c6 = Dx0.c(b, "updatedAt");
            int c7 = Dx0.c(b, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MAJOR_NUMBER);
            int c8 = Dx0.c(b, com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant.COLUMN_MINOR_NUMBER);
            int c9 = Dx0.c(b, "serialNumber");
            if (b.moveToFirst()) {
                microAppVariant = new MicroAppVariant(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), this.__millisecondDateConverter.a(b.getLong(c5)), this.__millisecondDateConverter.a(b.getLong(c6)), b.getInt(c7), b.getInt(c8), b.getString(c9));
            }
            return microAppVariant;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroAppSetting> getPendingMicroAppSettings() {
        Rh f = Rh.f("SELECT * FROM microAppSetting WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "appId");
            int c3 = Dx0.c(b, com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting.SETTING);
            int c4 = Dx0.c(b, "createdAt");
            int c5 = Dx0.c(b, "updatedAt");
            int c6 = Dx0.c(b, "pinType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new MicroAppSetting(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5), b.getInt(c6)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public List<MicroApp> queryMicroAppByName(String str, String str2) {
        Rh f = Rh.f("SELECT * FROM microApp WHERE name LIKE '%' || ? || '%' AND serialNumber=?", 2);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        if (str2 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "name");
            int c3 = Dx0.c(b, "nameKey");
            int c4 = Dx0.c(b, "serialNumber");
            int c5 = Dx0.c(b, "categories");
            int c6 = Dx0.c(b, "description");
            int c7 = Dx0.c(b, "descriptionKey");
            int c8 = Dx0.c(b, "icon");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new MicroApp(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), this.__stringArrayConverter.a(b.getString(c5)), b.getString(c6), b.getString(c7), b.getString(c8)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void upsertDeclarationFileList(List<DeclarationFile> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDeclarationFile.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void upsertListMicroApp(List<MicroApp> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroApp.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void upsertMicroApp(MicroApp microApp) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroApp.insert((Hh<MicroApp>) microApp);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void upsertMicroAppSetting(MicroAppSetting microAppSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppSetting.insert((Hh<MicroAppSetting>) microAppSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void upsertMicroAppSettingList(List<MicroAppSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppSetting.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void upsertMicroAppVariant(MicroAppVariant microAppVariant) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppVariant.insert((Hh<MicroAppVariant>) microAppVariant);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao
    public void upsertMicroAppVariantList(List<MicroAppVariant> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfMicroAppVariant.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
