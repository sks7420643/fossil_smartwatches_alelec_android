package com.portfolio.platform.data.source.local;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.CustomizeRealData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface CustomizeRealDataDao {
    @DexIgnore
    Object cleanUp();  // void declaration

    @DexIgnore
    LiveData<List<CustomizeRealData>> getAllRealDataAsLiveData();

    @DexIgnore
    List<CustomizeRealData> getAllRealDataRaw();

    @DexIgnore
    CustomizeRealData getRealData(String str);

    @DexIgnore
    void upsertRealData(CustomizeRealData customizeRealData);
}
