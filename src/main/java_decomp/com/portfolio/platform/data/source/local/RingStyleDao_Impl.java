package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.I05;
import com.fossil.Nw0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyleDao_Impl implements RingStyleDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<DianaComplicationRingStyle> __insertionAdapterOfDianaComplicationRingStyle;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAllData;
    @DexIgnore
    public /* final */ I05 __ringStyleConverter; // = new I05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<DianaComplicationRingStyle> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, DianaComplicationRingStyle dianaComplicationRingStyle) {
            if (dianaComplicationRingStyle.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, dianaComplicationRingStyle.getId());
            }
            if (dianaComplicationRingStyle.getCategory() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, dianaComplicationRingStyle.getCategory());
            }
            if (dianaComplicationRingStyle.getName() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, dianaComplicationRingStyle.getName());
            }
            String a2 = RingStyleDao_Impl.this.__ringStyleConverter.a(dianaComplicationRingStyle.getData());
            if (a2 == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, a2);
            }
            String d = RingStyleDao_Impl.this.__ringStyleConverter.d(dianaComplicationRingStyle.getMetaData());
            if (d == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, d);
            }
            if (dianaComplicationRingStyle.getSerial() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, dianaComplicationRingStyle.getSerial());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, DianaComplicationRingStyle dianaComplicationRingStyle) {
            bind(mi, dianaComplicationRingStyle);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `DianaComplicationRingStyle` (`id`,`category`,`name`,`data`,`metaData`,`serial`) VALUES (?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM DianaComplicationRingStyle";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<DianaComplicationRingStyle>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon3(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DianaComplicationRingStyle> call() throws Exception {
            Cursor b = Ex0.b(RingStyleDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "id");
                int c2 = Dx0.c(b, "category");
                int c3 = Dx0.c(b, "name");
                int c4 = Dx0.c(b, "data");
                int c5 = Dx0.c(b, "metaData");
                int c6 = Dx0.c(b, "serial");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new DianaComplicationRingStyle(b.getString(c), b.getString(c2), b.getString(c3), RingStyleDao_Impl.this.__ringStyleConverter.b(b.getString(c4)), RingStyleDao_Impl.this.__ringStyleConverter.c(b.getString(c5)), b.getString(c6)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public RingStyleDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfDianaComplicationRingStyle = new Anon1(oh);
        this.__preparedStmtOfClearAllData = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public void clearAllData() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAllData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public List<DianaComplicationRingStyle> getRingStyles() {
        Rh f = Rh.f("SELECT * FROM DianaComplicationRingStyle", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "category");
            int c3 = Dx0.c(b, "name");
            int c4 = Dx0.c(b, "data");
            int c5 = Dx0.c(b, "metaData");
            int c6 = Dx0.c(b, "serial");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new DianaComplicationRingStyle(b.getString(c), b.getString(c2), b.getString(c3), this.__ringStyleConverter.b(b.getString(c4)), this.__ringStyleConverter.c(b.getString(c5)), b.getString(c6)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public LiveData<List<DianaComplicationRingStyle>> getRingStylesAsLiveData() {
        Rh f = Rh.f("SELECT * FROM DianaComplicationRingStyle", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"DianaComplicationRingStyle"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public DianaComplicationRingStyle getRingStylesBySerial(String str) {
        DianaComplicationRingStyle dianaComplicationRingStyle = null;
        Rh f = Rh.f("SELECT * FROM DianaComplicationRingStyle WHERE serial = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "category");
            int c3 = Dx0.c(b, "name");
            int c4 = Dx0.c(b, "data");
            int c5 = Dx0.c(b, "metaData");
            int c6 = Dx0.c(b, "serial");
            if (b.moveToFirst()) {
                dianaComplicationRingStyle = new DianaComplicationRingStyle(b.getString(c), b.getString(c2), b.getString(c3), this.__ringStyleConverter.b(b.getString(c4)), this.__ringStyleConverter.c(b.getString(c5)), b.getString(c6));
            }
            return dianaComplicationRingStyle;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public void insertRingStyle(DianaComplicationRingStyle dianaComplicationRingStyle) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaComplicationRingStyle.insert((Hh<DianaComplicationRingStyle>) dianaComplicationRingStyle);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.RingStyleDao
    public void insertRingStyles(List<DianaComplicationRingStyle> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaComplicationRingStyle.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
