package com.portfolio.platform.data.source.local.dnd;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DNDScheduledTimeDao_Impl implements DNDScheduledTimeDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<DNDScheduledTimeModel> __insertionAdapterOfDNDScheduledTimeModel;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<DNDScheduledTimeModel> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, DNDScheduledTimeModel dNDScheduledTimeModel) {
            if (dNDScheduledTimeModel.getScheduledTimeName() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, dNDScheduledTimeModel.getScheduledTimeName());
            }
            mi.bindLong(2, (long) dNDScheduledTimeModel.getMinutes());
            mi.bindLong(3, (long) dNDScheduledTimeModel.getScheduledTimeType());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, DNDScheduledTimeModel dNDScheduledTimeModel) {
            bind(mi, dNDScheduledTimeModel);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dndScheduledTimeModel` (`scheduledTimeName`,`minutes`,`scheduledTimeType`) VALUES (?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM dndScheduledTimeModel";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<DNDScheduledTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon3(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DNDScheduledTimeModel> call() throws Exception {
            Cursor b = Ex0.b(DNDScheduledTimeDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "scheduledTimeName");
                int c2 = Dx0.c(b, "minutes");
                int c3 = Dx0.c(b, "scheduledTimeType");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new DNDScheduledTimeModel(b.getString(c), b.getInt(c2), b.getInt(c3)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<DNDScheduledTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon4(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public DNDScheduledTimeModel call() throws Exception {
            DNDScheduledTimeModel dNDScheduledTimeModel = null;
            Cursor b = Ex0.b(DNDScheduledTimeDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "scheduledTimeName");
                int c2 = Dx0.c(b, "minutes");
                int c3 = Dx0.c(b, "scheduledTimeType");
                if (b.moveToFirst()) {
                    dNDScheduledTimeModel = new DNDScheduledTimeModel(b.getString(c), b.getInt(c2), b.getInt(c3));
                }
                return dNDScheduledTimeModel;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public DNDScheduledTimeDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfDNDScheduledTimeModel = new Anon1(oh);
        this.__preparedStmtOfDelete = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public DNDScheduledTimeModel getDNDScheduledTimeModelWithFieldScheduledTimeType(int i) {
        DNDScheduledTimeModel dNDScheduledTimeModel = null;
        Rh f = Rh.f("SELECT * FROM dndScheduledTimeModel WHERE scheduledTimeType = ?", 1);
        f.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "scheduledTimeName");
            int c2 = Dx0.c(b, "minutes");
            int c3 = Dx0.c(b, "scheduledTimeType");
            if (b.moveToFirst()) {
                dNDScheduledTimeModel = new DNDScheduledTimeModel(b.getString(c), b.getInt(c2), b.getInt(c3));
            }
            return dNDScheduledTimeModel;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public LiveData<DNDScheduledTimeModel> getDNDScheduledTimeWithFieldScheduledTimeType(int i) {
        Rh f = Rh.f("SELECT * FROM dndScheduledTimeModel WHERE scheduledTimeType = ?", 1);
        f.bindLong(1, (long) i);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"dndScheduledTimeModel"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public LiveData<List<DNDScheduledTimeModel>> getListDNDScheduledTime() {
        Rh f = Rh.f("SELECT * FROM dndScheduledTimeModel", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"dndScheduledTimeModel"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public List<DNDScheduledTimeModel> getListDNDScheduledTimeModel() {
        Rh f = Rh.f("SELECT * FROM dndScheduledTimeModel", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "scheduledTimeName");
            int c2 = Dx0.c(b, "minutes");
            int c3 = Dx0.c(b, "scheduledTimeType");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new DNDScheduledTimeModel(b.getString(c), b.getInt(c2), b.getInt(c3)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public void insertDNDScheduledTime(DNDScheduledTimeModel dNDScheduledTimeModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDNDScheduledTimeModel.insert((Hh<DNDScheduledTimeModel>) dNDScheduledTimeModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao
    public void insertListDNDScheduledTime(List<DNDScheduledTimeModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDNDScheduledTimeModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
