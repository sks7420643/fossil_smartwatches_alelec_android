package com.portfolio.platform.data.source;

import com.fossil.Bw7;
import com.fossil.Eu7;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDataRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = FitnessDataRepository.class.getSimpleName();
        Wg6.b(simpleName, "FitnessDataRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public FitnessDataRepository(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    public final Object cleanUp(Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new FitnessDataRepository$cleanUp$Anon2(null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final Object getFitnessData(Date date, Date date2, Xe6<? super List<FitnessDataWrapper>> xe6) {
        return Eu7.g(Bw7.b(), new FitnessDataRepository$getFitnessData$Anon2(date, date2, null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0166  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object insert(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> r18, com.mapped.Xe6<? super com.mapped.Ap4<java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper>>> r19) {
        /*
        // Method dump skipped, instructions count: 531
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.FitnessDataRepository.insert(java.util.List, com.mapped.Xe6):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x01c0, code lost:
        if (r5 < r13.size()) goto L_0x01c2;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0180  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x01c5  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object pushFitnessData(java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper> r18, com.mapped.Xe6<? super com.mapped.Ap4<java.util.List<com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper>>> r19) {
        /*
        // Method dump skipped, instructions count: 456
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.FitnessDataRepository.pushFitnessData(java.util.List, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object pushPendingFitnessData(Xe6<? super Ap4<List<FitnessDataWrapper>>> xe6) {
        return Eu7.g(Bw7.b(), new FitnessDataRepository$pushPendingFitnessData$Anon2(this, null), xe6);
    }

    @DexIgnore
    public final Object saveFitnessData(List<FitnessDataWrapper> list, Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new FitnessDataRepository$saveFitnessData$Anon2(list, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }
}
