package com.portfolio.platform.data.source;

import com.fossil.Bu4;
import com.fossil.Ft4;
import com.fossil.Lk7;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesNotificationLocalFactory implements Factory<Bu4> {
    @DexIgnore
    public /* final */ Provider<Ft4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesNotificationLocalFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<Ft4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesNotificationLocalFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<Ft4> provider) {
        return new PortfolioDatabaseModule_ProvidesNotificationLocalFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static Bu4 providesNotificationLocal(PortfolioDatabaseModule portfolioDatabaseModule, Ft4 ft4) {
        Bu4 providesNotificationLocal = portfolioDatabaseModule.providesNotificationLocal(ft4);
        Lk7.c(providesNotificationLocal, "Cannot return null from a non-@Nullable @Provides method");
        return providesNotificationLocal;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public Bu4 get() {
        return providesNotificationLocal(this.module, this.daoProvider.get());
    }
}
