package com.portfolio.platform.data.source.remote;

import com.fossil.I98;
import com.fossil.Q88;
import com.mapped.Ku3;
import com.mapped.Uy6;
import com.mapped.Xe6;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface AuthApiUserService {
    @DexIgnore
    @Uy6("rpc/auth/password/change")
    Call<Ku3> changePassword(@I98 Ku3 ku3);

    @DexIgnore
    @Uy6("rpc/auth/logout")
    Object logout(@I98 Ku3 ku3, Xe6<? super Q88<Void>> xe6);
}
