package com.portfolio.platform.data.source.local.alarm;

import com.fossil.Ex0;
import com.fossil.Hw0;
import com.fossil.Ix0;
import com.fossil.Lx0;
import com.fossil.Nw0;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.mapped.Ji;
import com.mapped.Oh;
import com.mapped.Qh;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDatabase_Impl extends AlarmDatabase {
    @DexIgnore
    public volatile AlarmDao _alarmDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Qh.Ai {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void createAllTables(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `alarm` (`id` TEXT, `uri` TEXT NOT NULL, `title` TEXT NOT NULL, `message` TEXT NOT NULL, `hour` INTEGER NOT NULL, `minute` INTEGER NOT NULL, `days` TEXT, `isActive` INTEGER NOT NULL, `isRepeated` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT NOT NULL, `pinType` INTEGER NOT NULL, PRIMARY KEY(`uri`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '0b8d5ee1081d415aedfc0c7985d749f7')");
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void dropAllTables(Lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `alarm`");
            if (AlarmDatabase_Impl.this.mCallbacks != null) {
                int size = AlarmDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) AlarmDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onCreate(Lx0 lx0) {
            if (AlarmDatabase_Impl.this.mCallbacks != null) {
                int size = AlarmDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) AlarmDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onOpen(Lx0 lx0) {
            AlarmDatabase_Impl.this.mDatabase = lx0;
            AlarmDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (AlarmDatabase_Impl.this.mCallbacks != null) {
                int size = AlarmDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) AlarmDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPostMigrate(Lx0 lx0) {
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPreMigrate(Lx0 lx0) {
            Ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public Qh.Bi onValidateSchema(Lx0 lx0) {
            HashMap hashMap = new HashMap(12);
            hashMap.put("id", new Ix0.Ai("id", "TEXT", false, 0, null, 1));
            hashMap.put("uri", new Ix0.Ai("uri", "TEXT", true, 1, null, 1));
            hashMap.put("title", new Ix0.Ai("title", "TEXT", true, 0, null, 1));
            hashMap.put("message", new Ix0.Ai("message", "TEXT", true, 0, null, 1));
            hashMap.put(AppFilter.COLUMN_HOUR, new Ix0.Ai(AppFilter.COLUMN_HOUR, "INTEGER", true, 0, null, 1));
            hashMap.put(MFSleepGoal.COLUMN_MINUTE, new Ix0.Ai(MFSleepGoal.COLUMN_MINUTE, "INTEGER", true, 0, null, 1));
            hashMap.put(Alarm.COLUMN_DAYS, new Ix0.Ai(Alarm.COLUMN_DAYS, "TEXT", false, 0, null, 1));
            hashMap.put("isActive", new Ix0.Ai("isActive", "INTEGER", true, 0, null, 1));
            hashMap.put("isRepeated", new Ix0.Ai("isRepeated", "INTEGER", true, 0, null, 1));
            hashMap.put("createdAt", new Ix0.Ai("createdAt", "TEXT", false, 0, null, 1));
            hashMap.put("updatedAt", new Ix0.Ai("updatedAt", "TEXT", true, 0, null, 1));
            hashMap.put("pinType", new Ix0.Ai("pinType", "INTEGER", true, 0, null, 1));
            Ix0 ix0 = new Ix0(Alarm.TABLE_NAME, hashMap, new HashSet(0), new HashSet(0));
            Ix0 a2 = Ix0.a(lx0, Alarm.TABLE_NAME);
            if (ix0.equals(a2)) {
                return new Qh.Bi(true, null);
            }
            return new Qh.Bi(false, "alarm(com.portfolio.platform.data.source.local.alarm.Alarm).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDatabase
    public AlarmDao alarmDao() {
        AlarmDao alarmDao;
        if (this._alarmDao != null) {
            return this._alarmDao;
        }
        synchronized (this) {
            if (this._alarmDao == null) {
                this._alarmDao = new AlarmDao_Impl(this);
            }
            alarmDao = this._alarmDao;
        }
        return alarmDao;
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public void clearAllTables() {
        super.assertNotMainThread();
        Lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `alarm`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Nw0 createInvalidationTracker() {
        return new Nw0(this, new HashMap(0), new HashMap(0), Alarm.TABLE_NAME);
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Ji createOpenHelper(Hw0 hw0) {
        Qh qh = new Qh(hw0, new Anon1(6), "0b8d5ee1081d415aedfc0c7985d749f7", "7e35df0dd907bb841b2567561f725af0");
        Ji.Bi.Aii a2 = Ji.Bi.a(hw0.b);
        a2.c(hw0.c);
        a2.b(qh);
        return hw0.a.create(a2.a());
    }
}
