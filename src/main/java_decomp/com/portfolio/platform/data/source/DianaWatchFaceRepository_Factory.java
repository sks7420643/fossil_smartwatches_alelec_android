package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceRepository_Factory implements Factory<DianaWatchFaceRepository> {
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRemoteDataSource> mDianaWatchFaceRemoteDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<FileRepository> mFileRepositoryProvider;

    @DexIgnore
    public DianaWatchFaceRepository_Factory(Provider<FileRepository> provider, Provider<DianaWatchFaceRemoteDataSource> provider2) {
        this.mFileRepositoryProvider = provider;
        this.mDianaWatchFaceRemoteDataSourceProvider = provider2;
    }

    @DexIgnore
    public static DianaWatchFaceRepository_Factory create(Provider<FileRepository> provider, Provider<DianaWatchFaceRemoteDataSource> provider2) {
        return new DianaWatchFaceRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static DianaWatchFaceRepository newInstance(FileRepository fileRepository, DianaWatchFaceRemoteDataSource dianaWatchFaceRemoteDataSource) {
        return new DianaWatchFaceRepository(fileRepository, dianaWatchFaceRemoteDataSource);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public DianaWatchFaceRepository get() {
        return newInstance(this.mFileRepositoryProvider.get(), this.mDianaWatchFaceRemoteDataSourceProvider.get());
    }
}
