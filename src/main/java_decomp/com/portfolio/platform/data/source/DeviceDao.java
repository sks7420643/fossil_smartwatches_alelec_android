package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.Device;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface DeviceDao {
    @DexIgnore
    void addAllDevice(List<Device> list);

    @DexIgnore
    void addOrUpdateDevice(Device device);

    @DexIgnore
    Object cleanUp();  // void declaration

    @DexIgnore
    List<Device> getAllDevice();

    @DexIgnore
    LiveData<List<Device>> getAllDeviceAsLiveData();

    @DexIgnore
    Device getDeviceByDeviceId(String str);

    @DexIgnore
    LiveData<Device> getDeviceBySerialAsLiveData(String str);

    @DexIgnore
    void removeDeviceByDeviceId(String str);
}
