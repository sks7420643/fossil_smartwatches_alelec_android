package com.portfolio.platform.data.source;

import com.mapped.An4;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchLocalizationRepository_Factory implements Factory<WatchLocalizationRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiProvider;
    @DexIgnore
    public /* final */ Provider<An4> sharedPreferencesManagerProvider;

    @DexIgnore
    public WatchLocalizationRepository_Factory(Provider<ApiServiceV2> provider, Provider<An4> provider2) {
        this.apiProvider = provider;
        this.sharedPreferencesManagerProvider = provider2;
    }

    @DexIgnore
    public static WatchLocalizationRepository_Factory create(Provider<ApiServiceV2> provider, Provider<An4> provider2) {
        return new WatchLocalizationRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static WatchLocalizationRepository newInstance(ApiServiceV2 apiServiceV2, An4 an4) {
        return new WatchLocalizationRepository(apiServiceV2, an4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WatchLocalizationRepository get() {
        return newInstance(this.apiProvider.get(), this.sharedPreferencesManagerProvider.get());
    }
}
