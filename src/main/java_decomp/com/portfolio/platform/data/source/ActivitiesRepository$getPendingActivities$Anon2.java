package com.portfolio.platform.data.source;

import com.facebook.share.internal.VideoUploader;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.fitness.SampleRawDao;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.ActivitiesRepository$getPendingActivities$2", f = "ActivitiesRepository.kt", l = {Action.Selfie.TAKE_BURST}, m = "invokeSuspend")
public final class ActivitiesRepository$getPendingActivities$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super List<SampleRaw>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$getPendingActivities$Anon2(Date date, Date date2, Xe6 xe6) {
        super(2, xe6);
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        ActivitiesRepository$getPendingActivities$Anon2 activitiesRepository$getPendingActivities$Anon2 = new ActivitiesRepository$getPendingActivities$Anon2(this.$startDate, this.$endDate, xe6);
        activitiesRepository$getPendingActivities$Anon2.p$ = (Il6) obj;
        throw null;
        //return activitiesRepository$getPendingActivities$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super List<SampleRaw>> xe6) {
        throw null;
        //return ((ActivitiesRepository$getPendingActivities$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Calendar instance;
        Object y;
        Calendar calendar;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            instance = Calendar.getInstance();
            Wg6.b(instance, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
            instance.setTime(this.$startDate);
            Calendar instance2 = Calendar.getInstance();
            Wg6.b(instance2, "end");
            instance2.setTime(this.$endDate);
            EncryptedDatabaseManager encryptedDatabaseManager = EncryptedDatabaseManager.j;
            this.L$0 = il6;
            this.L$1 = instance;
            this.L$2 = instance2;
            this.label = 1;
            y = encryptedDatabaseManager.y(this);
            if (y == d) {
                return d;
            }
            calendar = instance2;
        } else if (i == 1) {
            instance = (Calendar) this.L$1;
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            calendar = (Calendar) this.L$2;
            y = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        SampleRawDao sampleRawDao = ((FitnessDatabase) y).sampleRawDao();
        Wg6.b(instance, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date V = TimeUtils.V(instance.getTime());
        Wg6.b(V, "DateHelper.getStartOfDay(start.time)");
        Wg6.b(calendar, "end");
        Date E = TimeUtils.E(calendar.getTime());
        Wg6.b(E, "DateHelper.getEndOfDay(end.time)");
        return sampleRawDao.getPendingActivitySamples(V, E);
    }
}
