package com.portfolio.platform.data.source.local.workoutsetting;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Gi5;
import com.fossil.Mi5;
import com.mapped.C;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSetting implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator CREATOR; // = new Creator();
    @DexIgnore
    public boolean askMeFirst;
    @DexIgnore
    public /* final */ long createdAt;
    @DexIgnore
    public boolean enable;
    @DexIgnore
    public /* final */ Gi5 mode;
    @DexIgnore
    public /* final */ int pauseLatency;
    @DexIgnore
    public int pinType;
    @DexIgnore
    public /* final */ int resumeLatency;
    @DexIgnore
    public /* final */ int startLatency;
    @DexIgnore
    public /* final */ int stopLatency;
    @DexIgnore
    public /* final */ Mi5 type;
    @DexIgnore
    public /* final */ long updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Creator implements Parcelable.Creator {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public final Object createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "in");
            Mi5 mi5 = (Mi5) Enum.valueOf(Mi5.class, parcel.readString());
            Gi5 gi5 = parcel.readInt() != 0 ? (Gi5) Enum.valueOf(Gi5.class, parcel.readString()) : null;
            boolean z = true;
            boolean z2 = parcel.readInt() != 0;
            if (parcel.readInt() == 0) {
                z = false;
            }
            return new WorkoutSetting(mi5, gi5, z2, z, parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readLong(), parcel.readLong(), parcel.readInt());
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public final Object[] newArray(int i) {
            return new WorkoutSetting[i];
        }
    }

    @DexIgnore
    public WorkoutSetting(Mi5 mi5, Gi5 gi5, boolean z, boolean z2, int i, int i2, int i3, int i4, long j, long j2, int i5) {
        Wg6.c(mi5, "type");
        this.type = mi5;
        this.mode = gi5;
        this.enable = z;
        this.askMeFirst = z2;
        this.startLatency = i;
        this.pauseLatency = i2;
        this.resumeLatency = i3;
        this.stopLatency = i4;
        this.createdAt = j;
        this.updatedAt = j2;
        this.pinType = i5;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WorkoutSetting(Mi5 mi5, Gi5 gi5, boolean z, boolean z2, int i, int i2, int i3, int i4, long j, long j2, int i5, int i6, Qg6 qg6) {
        this(mi5, (i6 & 2) != 0 ? null : gi5, z, z2, i, i2, i3, i4, j, j2, (i6 & 1024) != 0 ? 1 : i5);
    }

    @DexIgnore
    public static /* synthetic */ WorkoutSetting copy$default(WorkoutSetting workoutSetting, Mi5 mi5, Gi5 gi5, boolean z, boolean z2, int i, int i2, int i3, int i4, long j, long j2, int i5, int i6, Object obj) {
        return workoutSetting.copy((i6 & 1) != 0 ? workoutSetting.type : mi5, (i6 & 2) != 0 ? workoutSetting.mode : gi5, (i6 & 4) != 0 ? workoutSetting.enable : z, (i6 & 8) != 0 ? workoutSetting.askMeFirst : z2, (i6 & 16) != 0 ? workoutSetting.startLatency : i, (i6 & 32) != 0 ? workoutSetting.pauseLatency : i2, (i6 & 64) != 0 ? workoutSetting.resumeLatency : i3, (i6 & 128) != 0 ? workoutSetting.stopLatency : i4, (i6 & 256) != 0 ? workoutSetting.createdAt : j, (i6 & 512) != 0 ? workoutSetting.updatedAt : j2, (i6 & 1024) != 0 ? workoutSetting.pinType : i5);
    }

    @DexIgnore
    public final Mi5 component1() {
        return this.type;
    }

    @DexIgnore
    public final long component10() {
        return this.updatedAt;
    }

    @DexIgnore
    public final int component11() {
        return this.pinType;
    }

    @DexIgnore
    public final Gi5 component2() {
        return this.mode;
    }

    @DexIgnore
    public final boolean component3() {
        return this.enable;
    }

    @DexIgnore
    public final boolean component4() {
        return this.askMeFirst;
    }

    @DexIgnore
    public final int component5() {
        return this.startLatency;
    }

    @DexIgnore
    public final int component6() {
        return this.pauseLatency;
    }

    @DexIgnore
    public final int component7() {
        return this.resumeLatency;
    }

    @DexIgnore
    public final int component8() {
        return this.stopLatency;
    }

    @DexIgnore
    public final long component9() {
        return this.createdAt;
    }

    @DexIgnore
    public final WorkoutSetting copy(Mi5 mi5, Gi5 gi5, boolean z, boolean z2, int i, int i2, int i3, int i4, long j, long j2, int i5) {
        Wg6.c(mi5, "type");
        return new WorkoutSetting(mi5, gi5, z, z2, i, i2, i3, i4, j, j2, i5);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutSetting) {
                WorkoutSetting workoutSetting = (WorkoutSetting) obj;
                if (!(Wg6.a(this.type, workoutSetting.type) && Wg6.a(this.mode, workoutSetting.mode) && this.enable == workoutSetting.enable && this.askMeFirst == workoutSetting.askMeFirst && this.startLatency == workoutSetting.startLatency && this.pauseLatency == workoutSetting.pauseLatency && this.resumeLatency == workoutSetting.resumeLatency && this.stopLatency == workoutSetting.stopLatency && this.createdAt == workoutSetting.createdAt && this.updatedAt == workoutSetting.updatedAt && this.pinType == workoutSetting.pinType)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean getAskMeFirst() {
        return this.askMeFirst;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final boolean getEnable() {
        return this.enable;
    }

    @DexIgnore
    public final Gi5 getMode() {
        return this.mode;
    }

    @DexIgnore
    public final int getPauseLatency() {
        return this.pauseLatency;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getResumeLatency() {
        return this.resumeLatency;
    }

    @DexIgnore
    public final int getStartLatency() {
        return this.startLatency;
    }

    @DexIgnore
    public final int getStopLatency() {
        return this.stopLatency;
    }

    @DexIgnore
    public final Mi5 getType() {
        return this.type;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 1;
        int i2 = 0;
        Mi5 mi5 = this.type;
        int hashCode = mi5 != null ? mi5.hashCode() : 0;
        Gi5 gi5 = this.mode;
        if (gi5 != null) {
            i2 = gi5.hashCode();
        }
        boolean z = this.enable;
        if (z) {
            z = true;
        }
        boolean z2 = this.askMeFirst;
        if (!z2) {
            i = z2 ? 1 : 0;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        return (((((((((((((((((((hashCode * 31) + i2) * 31) + i3) * 31) + i) * 31) + this.startLatency) * 31) + this.pauseLatency) * 31) + this.resumeLatency) * 31) + this.stopLatency) * 31) + C.a(this.createdAt)) * 31) + C.a(this.updatedAt)) * 31) + this.pinType;
    }

    @DexIgnore
    public final void setAskMeFirst(boolean z) {
        this.askMeFirst = z;
    }

    @DexIgnore
    public final void setEnable(boolean z) {
        this.enable = z;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSetting(type=" + this.type + ", mode=" + this.mode + ", enable=" + this.enable + ", askMeFirst=" + this.askMeFirst + ", startLatency=" + this.startLatency + ", pauseLatency=" + this.pauseLatency + ", resumeLatency=" + this.resumeLatency + ", stopLatency=" + this.stopLatency + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", pinType=" + this.pinType + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(this.type.name());
        Gi5 gi5 = this.mode;
        if (gi5 != null) {
            parcel.writeInt(1);
            parcel.writeString(gi5.name());
        } else {
            parcel.writeInt(0);
        }
        parcel.writeInt(this.enable ? 1 : 0);
        parcel.writeInt(this.askMeFirst ? 1 : 0);
        parcel.writeInt(this.startLatency);
        parcel.writeInt(this.pauseLatency);
        parcel.writeInt(this.resumeLatency);
        parcel.writeInt(this.stopLatency);
        parcel.writeLong(this.createdAt);
        parcel.writeLong(this.updatedAt);
        parcel.writeInt(this.pinType);
    }
}
