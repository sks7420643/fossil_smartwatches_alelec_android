package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.ThirdPartyRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.ThirdPartyRepository$pushPendingData$2", f = "ThirdPartyRepository.kt", l = {86}, m = "invokeSuspend")
public final class ThirdPartyRepository$pushPendingData$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Rm6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository.PushPendingThirdPartyDataCallback $pushPendingThirdPartyDataCallback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThirdPartyRepository$pushPendingData$Anon2(ThirdPartyRepository thirdPartyRepository, ThirdPartyRepository.PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = thirdPartyRepository;
        this.$pushPendingThirdPartyDataCallback = pushPendingThirdPartyDataCallback;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        ThirdPartyRepository$pushPendingData$Anon2 thirdPartyRepository$pushPendingData$Anon2 = new ThirdPartyRepository$pushPendingData$Anon2(this.this$0, this.$pushPendingThirdPartyDataCallback, xe6);
        thirdPartyRepository$pushPendingData$Anon2.p$ = (Il6) obj;
        throw null;
        //return thirdPartyRepository$pushPendingData$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Rm6> xe6) {
        throw null;
        //return ((ThirdPartyRepository$pushPendingData$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            ThirdPartyRepository thirdPartyRepository = this.this$0;
            ThirdPartyRepository.PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback = this.$pushPendingThirdPartyDataCallback;
            this.L$0 = il6;
            this.label = 1;
            Object uploadData = thirdPartyRepository.uploadData(pushPendingThirdPartyDataCallback, this);
            return uploadData == d ? d : uploadData;
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
