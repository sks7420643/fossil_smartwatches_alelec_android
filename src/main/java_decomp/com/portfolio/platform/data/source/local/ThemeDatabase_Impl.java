package com.portfolio.platform.data.source.local;

import com.fossil.Ex0;
import com.fossil.Hw0;
import com.fossil.Ix0;
import com.fossil.Lx0;
import com.fossil.Nw0;
import com.mapped.Ji;
import com.mapped.Oh;
import com.mapped.Qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeDatabase_Impl extends ThemeDatabase {
    @DexIgnore
    public volatile ThemeDao _themeDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Qh.Ai {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void createAllTables(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `theme` (`isCurrentTheme` INTEGER NOT NULL, `type` TEXT NOT NULL, `id` TEXT NOT NULL, `name` TEXT NOT NULL, `styles` TEXT NOT NULL, PRIMARY KEY(`id`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '3bbfe40ee4044998d625f23932959275')");
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void dropAllTables(Lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `theme`");
            if (ThemeDatabase_Impl.this.mCallbacks != null) {
                int size = ThemeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) ThemeDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onCreate(Lx0 lx0) {
            if (ThemeDatabase_Impl.this.mCallbacks != null) {
                int size = ThemeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) ThemeDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onOpen(Lx0 lx0) {
            ThemeDatabase_Impl.this.mDatabase = lx0;
            ThemeDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (ThemeDatabase_Impl.this.mCallbacks != null) {
                int size = ThemeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) ThemeDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPostMigrate(Lx0 lx0) {
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPreMigrate(Lx0 lx0) {
            Ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public Qh.Bi onValidateSchema(Lx0 lx0) {
            HashMap hashMap = new HashMap(5);
            hashMap.put("isCurrentTheme", new Ix0.Ai("isCurrentTheme", "INTEGER", true, 0, null, 1));
            hashMap.put("type", new Ix0.Ai("type", "TEXT", true, 0, null, 1));
            hashMap.put("id", new Ix0.Ai("id", "TEXT", true, 1, null, 1));
            hashMap.put("name", new Ix0.Ai("name", "TEXT", true, 0, null, 1));
            hashMap.put("styles", new Ix0.Ai("styles", "TEXT", true, 0, null, 1));
            Ix0 ix0 = new Ix0("theme", hashMap, new HashSet(0), new HashSet(0));
            Ix0 a2 = Ix0.a(lx0, "theme");
            if (ix0.equals(a2)) {
                return new Qh.Bi(true, null);
            }
            return new Qh.Bi(false, "theme(com.portfolio.platform.data.model.Theme).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public void clearAllTables() {
        super.assertNotMainThread();
        Lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `theme`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Nw0 createInvalidationTracker() {
        return new Nw0(this, new HashMap(0), new HashMap(0), "theme");
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Ji createOpenHelper(Hw0 hw0) {
        Qh qh = new Qh(hw0, new Anon1(1), "3bbfe40ee4044998d625f23932959275", "68ccf1dee92254a5e6e37fa35b38bb4e");
        Ji.Bi.Aii a2 = Ji.Bi.a(hw0.b);
        a2.c(hw0.c);
        a2.b(qh);
        return hw0.a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.ThemeDatabase
    public ThemeDao themeDao() {
        ThemeDao themeDao;
        if (this._themeDao != null) {
            return this._themeDao;
        }
        synchronized (this) {
            if (this._themeDao == null) {
                this._themeDao = new ThemeDao_Impl(this);
            }
            themeDao = this._themeDao;
        }
        return themeDao;
    }
}
