package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.ComplicationLastSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationLastSettingDao_Impl implements ComplicationLastSettingDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<ComplicationLastSetting> __insertionAdapterOfComplicationLastSetting;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteComplicationLastSettingByComplicationId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<ComplicationLastSetting> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, ComplicationLastSetting complicationLastSetting) {
            if (complicationLastSetting.getComplicationId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, complicationLastSetting.getComplicationId());
            }
            if (complicationLastSetting.getUpdatedAt() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, complicationLastSetting.getUpdatedAt());
            }
            if (complicationLastSetting.getSetting() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, complicationLastSetting.getSetting());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, ComplicationLastSetting complicationLastSetting) {
            bind(mi, complicationLastSetting);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `complicationLastSetting` (`complicationId`,`updatedAt`,`setting`) VALUES (?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM complicationLastSetting WHERE complicationId=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM complicationLastSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<ComplicationLastSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon4(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<ComplicationLastSetting> call() throws Exception {
            Cursor b = Ex0.b(ComplicationLastSettingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "complicationId");
                int c2 = Dx0.c(b, "updatedAt");
                int c3 = Dx0.c(b, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new ComplicationLastSetting(b.getString(c), b.getString(c2), b.getString(c3)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public ComplicationLastSettingDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfComplicationLastSetting = new Anon1(oh);
        this.__preparedStmtOfDeleteComplicationLastSettingByComplicationId = new Anon2(oh);
        this.__preparedStmtOfCleanUp = new Anon3(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao
    public void deleteComplicationLastSettingByComplicationId(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteComplicationLastSettingByComplicationId.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteComplicationLastSettingByComplicationId.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao
    public List<ComplicationLastSetting> getAllComplicationLastSetting() {
        Rh f = Rh.f("SELECT * FROM complicationLastSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "complicationId");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new ComplicationLastSetting(b.getString(c), b.getString(c2), b.getString(c3)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao
    public LiveData<List<ComplicationLastSetting>> getAllComplicationLastSettingAsLiveData() {
        Rh f = Rh.f("SELECT * FROM complicationLastSetting", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"complicationLastSetting"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao
    public ComplicationLastSetting getComplicationLastSetting(String str) {
        ComplicationLastSetting complicationLastSetting = null;
        Rh f = Rh.f("SELECT * FROM complicationLastSetting WHERE complicationId=? ", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "complicationId");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, MicroAppSetting.SETTING);
            if (b.moveToFirst()) {
                complicationLastSetting = new ComplicationLastSetting(b.getString(c), b.getString(c2), b.getString(c3));
            }
            return complicationLastSetting;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.ComplicationLastSettingDao
    public void upsertComplicationLastSetting(ComplicationLastSetting complicationLastSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfComplicationLastSetting.insert((Hh<ComplicationLastSetting>) complicationLastSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
