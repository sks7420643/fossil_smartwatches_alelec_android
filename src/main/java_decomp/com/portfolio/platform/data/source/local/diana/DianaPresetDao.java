package com.portfolio.platform.data.source.local.diana;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface DianaPresetDao {
    @DexIgnore
    void clearAllPresetBySerial(String str);

    @DexIgnore
    Object clearDianaPresetTable();  // void declaration

    @DexIgnore
    Object clearDianaRecommendPresetTable();  // void declaration

    @DexIgnore
    void deletePreset(String str);

    @DexIgnore
    DianaPreset getActivePresetBySerial(String str);

    @DexIgnore
    LiveData<DianaPreset> getActivePresetBySerialLiveData(String str);

    @DexIgnore
    List<DianaPreset> getAllPendingPreset(String str);

    @DexIgnore
    List<DianaPreset> getAllPreset(String str);

    @DexIgnore
    LiveData<List<DianaPreset>> getAllPresetAsLiveData(String str);

    @DexIgnore
    List<DianaPreset> getAllPresets();

    @DexIgnore
    List<DianaRecommendPreset> getDianaRecommendPresetList(String str);

    @DexIgnore
    DianaPreset getPresetById(String str);

    @DexIgnore
    LiveData<DianaPreset> getPresetByIdLive(String str);

    @DexIgnore
    List<DianaPreset> getPresetListByWatchFaceId(String str);

    @DexIgnore
    Object removeAllDeletePinTypePreset();  // void declaration

    @DexIgnore
    void upsertDianaRecommendPresetList(List<DianaRecommendPreset> list);

    @DexIgnore
    void upsertPreset(DianaPreset dianaPreset);

    @DexIgnore
    void upsertPresetList(List<DianaPreset> list);
}
