package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.Bw7;
import com.fossil.Gl5;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Nw0;
import com.fossil.Pm7;
import com.mapped.PagingRequestHelper;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Ye;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataLocalDataSource extends Ye<Long, GoalTrackingData> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public DateTime latestTrackedTime;
    @DexIgnore
    public /* final */ PagingRequestHelper.Ai listener;
    @DexIgnore
    public /* final */ Date mCurrentDate;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public PagingRequestHelper mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ Nw0.Ci mObserver; // = new Anon1(this, "goalTrackingRaw", new String[0]);
    @DexIgnore
    public int mOffset;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends Nw0.Ci {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = goalTrackingDataLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Nw0.Ci
        public void onInvalidated(Set<String> set) {
            Wg6.c(set, "tables");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return GoalTrackingDataLocalDataSource.TAG;
        }
    }

    /*
    static {
        String simpleName = GoalTrackingDataLocalDataSource.class.getSimpleName();
        Wg6.b(simpleName, "GoalTrackingDataLocalDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public GoalTrackingDataLocalDataSource(GoalTrackingRepository goalTrackingRepository, GoalTrackingDatabase goalTrackingDatabase, Date date, U04 u04, PagingRequestHelper.Ai ai) {
        Wg6.c(goalTrackingRepository, "mGoalTrackingRepository");
        Wg6.c(goalTrackingDatabase, "mGoalTrackingDatabase");
        Wg6.c(date, "mCurrentDate");
        Wg6.c(u04, "appExecutors");
        Wg6.c(ai, "listener");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mCurrentDate = date;
        this.listener = ai;
        PagingRequestHelper pagingRequestHelper = new PagingRequestHelper(u04.a());
        this.mHelper = pagingRequestHelper;
        this.mNetworkState = Gl5.b(pagingRequestHelper);
        this.mHelper.a(this.listener);
        this.mGoalTrackingDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final List<GoalTrackingData> getDataInDatabase(int i) {
        return this.mGoalTrackingDatabase.getGoalTrackingDao().getGoalTrackingDataListInitInDate(this.mCurrentDate, i);
    }

    @DexIgnore
    private final Rm6 loadData(PagingRequestHelper.Di di, PagingRequestHelper.Bi.Aii aii, int i) {
        return Gu7.d(Jv7.a(Bw7.b()), null, null, new GoalTrackingDataLocalDataSource$loadData$Anon1(this, i, aii, null), 3, null);
    }

    @DexIgnore
    public static /* synthetic */ Rm6 loadData$default(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource, PagingRequestHelper.Di di, PagingRequestHelper.Bi.Aii aii, int i, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return goalTrackingDataLocalDataSource.loadData(di, aii, i);
    }

    @DexIgnore
    public Long getKey(GoalTrackingData goalTrackingData) {
        Wg6.c(goalTrackingData, "item");
        return Long.valueOf(goalTrackingData.getUpdatedAt());
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Ye
    public /* bridge */ /* synthetic */ Long getKey(GoalTrackingData goalTrackingData) {
        return getKey(goalTrackingData);
    }

    @DexIgnore
    public final PagingRequestHelper getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public boolean isInvalid() {
        this.mGoalTrackingDatabase.getInvalidationTracker().h();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.mapped.Ye
    public void loadAfter(Ye.Fi<Long> fi, Ye.Ai<GoalTrackingData> ai) {
        Wg6.c(fi, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ai, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - currentDate = " + this.mCurrentDate);
        GoalTrackingDao goalTrackingDao = this.mGoalTrackingDatabase.getGoalTrackingDao();
        Date date = this.mCurrentDate;
        DateTime dateTime = this.latestTrackedTime;
        if (dateTime != null) {
            Key key = fi.a;
            Wg6.b(key, "params.key");
            List<GoalTrackingData> goalTrackingDataListAfterInDate = goalTrackingDao.getGoalTrackingDataListAfterInDate(date, dateTime, key.longValue(), fi.b);
            if (!goalTrackingDataListAfterInDate.isEmpty()) {
                this.latestTrackedTime = ((GoalTrackingData) Pm7.P(goalTrackingDataListAfterInDate)).getTrackedAt();
            }
            ai.a(goalTrackingDataListAfterInDate);
            this.mHelper.h(PagingRequestHelper.Di.AFTER, new GoalTrackingDataLocalDataSource$loadAfter$Anon1(this));
            return;
        }
        Wg6.n("latestTrackedTime");
        throw null;
    }

    @DexIgnore
    @Override // com.mapped.Ye
    public void loadBefore(Ye.Fi<Long> fi, Ye.Ai<GoalTrackingData> ai) {
        Wg6.c(fi, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ai, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.mapped.Ye
    public void loadInitial(Ye.Ei<Long> ei, Ye.Ci<GoalTrackingData> ci) {
        Wg6.c(ei, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ci, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - mCurrentDate = " + this.mCurrentDate + ' ');
        List<GoalTrackingData> dataInDatabase = getDataInDatabase(ei.b);
        if (!dataInDatabase.isEmpty()) {
            this.latestTrackedTime = ((GoalTrackingData) Pm7.P(dataInDatabase)).getTrackedAt();
        }
        ci.a(dataInDatabase);
        this.mHelper.h(PagingRequestHelper.Di.INITIAL, new GoalTrackingDataLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.f(this.listener);
        this.mGoalTrackingDatabase.getInvalidationTracker().j(this.mObserver);
    }

    @DexIgnore
    public final void setMHelper(PagingRequestHelper pagingRequestHelper) {
        Wg6.c(pagingRequestHelper, "<set-?>");
        this.mHelper = pagingRequestHelper;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        Wg6.c(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }
}
