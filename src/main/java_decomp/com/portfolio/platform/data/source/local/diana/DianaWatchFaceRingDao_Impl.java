package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Fw0;
import com.fossil.O77;
import com.mapped.Cd6;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceRing;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceRingDao_Impl implements DianaWatchFaceRingDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<DianaWatchFaceRing> __insertionAdapterOfDianaWatchFaceRing;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAll;
    @DexIgnore
    public /* final */ O77 __wFAssetConverter; // = new O77();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<DianaWatchFaceRing> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, DianaWatchFaceRing dianaWatchFaceRing) {
            if (dianaWatchFaceRing.getCreatedAt() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, dianaWatchFaceRing.getCreatedAt());
            }
            if (dianaWatchFaceRing.getUpdatedAt() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, dianaWatchFaceRing.getUpdatedAt());
            }
            if (dianaWatchFaceRing.getId() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, dianaWatchFaceRing.getId());
            }
            if (dianaWatchFaceRing.getCategory() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, dianaWatchFaceRing.getCategory());
            }
            if (dianaWatchFaceRing.getName() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, dianaWatchFaceRing.getName());
            }
            String c = DianaWatchFaceRingDao_Impl.this.__wFAssetConverter.c(dianaWatchFaceRing.getData());
            if (c == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, c);
            }
            String f = DianaWatchFaceRingDao_Impl.this.__wFAssetConverter.f(dianaWatchFaceRing.getMetaData());
            if (f == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, f);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, DianaWatchFaceRing dianaWatchFaceRing) {
            bind(mi, dianaWatchFaceRing);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaWatchFaceRing` (`createdAt`,`updatedAt`,`id`,`category`,`name`,`data`,`metaData`) VALUES (?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM DianaWatchFaceRing";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaWatchFaceRing val$ring;

        @DexIgnore
        public Anon3(DianaWatchFaceRing dianaWatchFaceRing) {
            this.val$ring = dianaWatchFaceRing;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Cd6 call() throws Exception {
            DianaWatchFaceRingDao_Impl.this.__db.beginTransaction();
            try {
                DianaWatchFaceRingDao_Impl.this.__insertionAdapterOfDianaWatchFaceRing.insert((Hh) this.val$ring);
                DianaWatchFaceRingDao_Impl.this.__db.setTransactionSuccessful();
                return Cd6.a;
            } finally {
                DianaWatchFaceRingDao_Impl.this.__db.endTransaction();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ List val$rings;

        @DexIgnore
        public Anon4(List list) {
            this.val$rings = list;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Cd6 call() throws Exception {
            DianaWatchFaceRingDao_Impl.this.__db.beginTransaction();
            try {
                DianaWatchFaceRingDao_Impl.this.__insertionAdapterOfDianaWatchFaceRing.insert((Iterable) this.val$rings);
                DianaWatchFaceRingDao_Impl.this.__db.setTransactionSuccessful();
                return Cd6.a;
            } finally {
                DianaWatchFaceRingDao_Impl.this.__db.endTransaction();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 implements Callable<Cd6> {
        @DexIgnore
        public Anon5() {
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Cd6 call() throws Exception {
            Mi acquire = DianaWatchFaceRingDao_Impl.this.__preparedStmtOfClearAll.acquire();
            DianaWatchFaceRingDao_Impl.this.__db.beginTransaction();
            try {
                acquire.executeUpdateDelete();
                DianaWatchFaceRingDao_Impl.this.__db.setTransactionSuccessful();
                return Cd6.a;
            } finally {
                DianaWatchFaceRingDao_Impl.this.__db.endTransaction();
                DianaWatchFaceRingDao_Impl.this.__preparedStmtOfClearAll.release(acquire);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 implements Callable<List<DianaWatchFaceRing>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon6(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DianaWatchFaceRing> call() throws Exception {
            Cursor b = Ex0.b(DianaWatchFaceRingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "createdAt");
                int c2 = Dx0.c(b, "updatedAt");
                int c3 = Dx0.c(b, "id");
                int c4 = Dx0.c(b, "category");
                int c5 = Dx0.c(b, "name");
                int c6 = Dx0.c(b, "data");
                int c7 = Dx0.c(b, "metaData");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    DianaWatchFaceRing dianaWatchFaceRing = new DianaWatchFaceRing(b.getString(c3), b.getString(c4), b.getString(c5), DianaWatchFaceRingDao_Impl.this.__wFAssetConverter.d(b.getString(c6)), DianaWatchFaceRingDao_Impl.this.__wFAssetConverter.e(b.getString(c7)));
                    dianaWatchFaceRing.setCreatedAt(b.getString(c));
                    dianaWatchFaceRing.setUpdatedAt(b.getString(c2));
                    arrayList.add(dianaWatchFaceRing);
                }
                return arrayList;
            } finally {
                b.close();
                this.val$_statement.m();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 implements Callable<DianaWatchFaceRing> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon7(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public DianaWatchFaceRing call() throws Exception {
            DianaWatchFaceRing dianaWatchFaceRing = null;
            Cursor b = Ex0.b(DianaWatchFaceRingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "createdAt");
                int c2 = Dx0.c(b, "updatedAt");
                int c3 = Dx0.c(b, "id");
                int c4 = Dx0.c(b, "category");
                int c5 = Dx0.c(b, "name");
                int c6 = Dx0.c(b, "data");
                int c7 = Dx0.c(b, "metaData");
                if (b.moveToFirst()) {
                    dianaWatchFaceRing = new DianaWatchFaceRing(b.getString(c3), b.getString(c4), b.getString(c5), DianaWatchFaceRingDao_Impl.this.__wFAssetConverter.d(b.getString(c6)), DianaWatchFaceRingDao_Impl.this.__wFAssetConverter.e(b.getString(c7)));
                    dianaWatchFaceRing.setCreatedAt(b.getString(c));
                    dianaWatchFaceRing.setUpdatedAt(b.getString(c2));
                }
                return dianaWatchFaceRing;
            } finally {
                b.close();
                this.val$_statement.m();
            }
        }
    }

    @DexIgnore
    public DianaWatchFaceRingDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfDianaWatchFaceRing = new Anon1(oh);
        this.__preparedStmtOfClearAll = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceRingDao
    public Object clearAll(Xe6<? super Cd6> xe6) {
        return Fw0.a(this.__db, true, new Anon5(), xe6);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceRingDao
    public Object getAllDianaWatchFaceRings(Xe6<? super List<DianaWatchFaceRing>> xe6) {
        return Fw0.a(this.__db, false, new Anon6(Rh.f("SELECT * FROM DianaWatchFaceRing", 0)), xe6);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceRingDao
    public Object getDianaWatchFaceRing(String str, Xe6<? super DianaWatchFaceRing> xe6) {
        Rh f = Rh.f("SELECT * FROM DianaWatchFaceRing WHERE name = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        return Fw0.a(this.__db, false, new Anon7(f), xe6);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceRingDao
    public Object upsertDianaWatchFaceRing(DianaWatchFaceRing dianaWatchFaceRing, Xe6<? super Cd6> xe6) {
        return Fw0.a(this.__db, true, new Anon3(dianaWatchFaceRing), xe6);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceRingDao
    public Object upsertDianaWatchFaceRings(List<DianaWatchFaceRing> list, Xe6<? super Cd6> xe6) {
        return Fw0.a(this.__db, true, new Anon4(list), xe6);
    }
}
