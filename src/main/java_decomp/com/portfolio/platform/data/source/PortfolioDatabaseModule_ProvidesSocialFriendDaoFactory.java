package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import com.fossil.Ys4;
import com.portfolio.platform.buddy_challenge.data.BuddyChallengeDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesSocialFriendDaoFactory implements Factory<Ys4> {
    @DexIgnore
    public /* final */ Provider<BuddyChallengeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesSocialFriendDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<BuddyChallengeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesSocialFriendDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<BuddyChallengeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvidesSocialFriendDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static Ys4 providesSocialFriendDao(PortfolioDatabaseModule portfolioDatabaseModule, BuddyChallengeDatabase buddyChallengeDatabase) {
        Ys4 providesSocialFriendDao = portfolioDatabaseModule.providesSocialFriendDao(buddyChallengeDatabase);
        Lk7.c(providesSocialFriendDao, "Cannot return null from a non-@Nullable @Provides method");
        return providesSocialFriendDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public Ys4 get() {
        return providesSocialFriendDao(this.module, this.dbProvider.get());
    }
}
