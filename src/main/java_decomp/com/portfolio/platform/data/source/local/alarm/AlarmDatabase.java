package com.portfolio.platform.data.source.local.alarm;

import com.mapped.Oh;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xh;
import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class AlarmDatabase extends Oh {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ Xh MIGRATION_FROM_5_TO_6; // = new AlarmDatabase$Companion$MIGRATION_FROM_5_TO_6$Anon1(5, 6);
    @DexIgnore
    public static /* final */ String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Xh getMIGRATION_FROM_5_TO_6() {
            return AlarmDatabase.MIGRATION_FROM_5_TO_6;
        }

        @DexIgnore
        public final Xh migrating3Or4To5(String str, int i) {
            Wg6.c(str, ButtonService.USER_ID);
            return new AlarmDatabase$Companion$migrating3Or4To5$Anon1(i, str, i, 5);
        }
    }

    /*
    static {
        String simpleName = AlarmDatabase.class.getSimpleName();
        Wg6.b(simpleName, "AlarmDatabase::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public abstract AlarmDao alarmDao();
}
