package com.portfolio.platform.data.legacy.threedotzero;

import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.misfit.frameworks.common.enums.Gesture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class PusherConfiguration {
    @DexIgnore
    public static /* final */ String TAG; // = "PusherConfiguration";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Anon1 {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$common$enums$Gesture;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher;

        /*
        static {
            int[] iArr = new int[Pusher.values().length];
            $SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher = iArr;
            try {
                iArr[Pusher.TOP_PUSHER.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher[Pusher.MID_PUSHER.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher[Pusher.BOTTOM_PUSHER.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher[Pusher.TRIPLE_TAP.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            int[] iArr2 = new int[Gesture.values().length];
            $SwitchMap$com$misfit$frameworks$common$enums$Gesture = iArr2;
            try {
                iArr2[Gesture.SAM_BT1_PRESSED.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_SINGLE_PRESS.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_SINGLE_PRESS_AND_HOLD.ordinal()] = 3;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_DOUBLE_PRESS.ordinal()] = 4;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_DOUBLE_PRESS_AND_HOLD.ordinal()] = 5;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_TRIPLE_PRESS.ordinal()] = 6;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT1_TRIPLE_PRESS_AND_HOLD.ordinal()] = 7;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_PRESSED.ordinal()] = 8;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_SINGLE_PRESS.ordinal()] = 9;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_SINGLE_PRESS_AND_HOLD.ordinal()] = 10;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_DOUBLE_PRESS.ordinal()] = 11;
            } catch (NoSuchFieldError e15) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_DOUBLE_PRESS_AND_HOLD.ordinal()] = 12;
            } catch (NoSuchFieldError e16) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_TRIPLE_PRESS.ordinal()] = 13;
            } catch (NoSuchFieldError e17) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT2_TRIPLE_PRESS_AND_HOLD.ordinal()] = 14;
            } catch (NoSuchFieldError e18) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_PRESSED.ordinal()] = 15;
            } catch (NoSuchFieldError e19) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_SINGLE_PRESS.ordinal()] = 16;
            } catch (NoSuchFieldError e20) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_SINGLE_PRESS_AND_HOLD.ordinal()] = 17;
            } catch (NoSuchFieldError e21) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_DOUBLE_PRESS.ordinal()] = 18;
            } catch (NoSuchFieldError e22) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_DOUBLE_PRESS_AND_HOLD.ordinal()] = 19;
            } catch (NoSuchFieldError e23) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_TRIPLE_PRESS.ordinal()] = 20;
            } catch (NoSuchFieldError e24) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$Gesture[Gesture.SAM_BT3_TRIPLE_PRESS_AND_HOLD.ordinal()] = 21;
            } catch (NoSuchFieldError e25) {
            }
        }
        */
    }

    @DexIgnore
    public enum Pusher {
        TOP_PUSHER(ViewHierarchy.DIMENSION_TOP_KEY),
        MID_PUSHER("middle"),
        BOTTOM_PUSHER("bottom"),
        DOUBLE_TAP("double tap"),
        TRIPLE_TAP("triple tap");
        
        @DexIgnore
        public /* final */ String value;

        @DexIgnore
        public Pusher(String str) {
            this.value = str;
        }

        @DexIgnore
        public static Pusher getPusherFromValue(String str) {
            Pusher[] values = values();
            for (Pusher pusher : values) {
                if (pusher.value.equals(str)) {
                    return pusher;
                }
            }
            return TOP_PUSHER;
        }

        @DexIgnore
        public String getValue() {
            return this.value;
        }

        @DexIgnore
        public String toString() {
            return this.value;
        }
    }

    @DexIgnore
    public static Gesture getGestureWithLinkAction(Pusher pusher, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Get action of pusher=" + pusher);
        int i2 = Anon1.$SwitchMap$com$portfolio$platform$data$legacy$threedotzero$PusherConfiguration$Pusher[pusher.ordinal()];
        if (i2 == 1) {
            return i != 102 ? i != 103 ? i != 202 ? Action.DisplayMode.isActionBelongToThisType(i) ? Gesture.SAM_BT1_PRESSED : Gesture.SAM_BT1_SINGLE_PRESS : Gesture.SAM_BT1_SINGLE_PRESS_AND_HOLD : Gesture.SAM_BT1_SINGLE_PRESS_AND_HOLD : Gesture.SAM_BT1_DOUBLE_PRESS;
        }
        if (i2 == 2) {
            return i != 102 ? i != 103 ? i != 202 ? Action.DisplayMode.isActionBelongToThisType(i) ? Gesture.SAM_BT2_PRESSED : Gesture.SAM_BT2_SINGLE_PRESS : Gesture.SAM_BT2_SINGLE_PRESS_AND_HOLD : Gesture.SAM_BT2_SINGLE_PRESS_AND_HOLD : Gesture.SAM_BT2_DOUBLE_PRESS;
        }
        if (i2 == 3) {
            return i != 102 ? i != 103 ? i != 202 ? Action.DisplayMode.isActionBelongToThisType(i) ? Gesture.SAM_BT3_PRESSED : Gesture.SAM_BT3_SINGLE_PRESS : Gesture.SAM_BT3_SINGLE_PRESS_AND_HOLD : Gesture.SAM_BT3_SINGLE_PRESS_AND_HOLD : Gesture.SAM_BT3_DOUBLE_PRESS;
        }
        if (i2 == 4) {
            return Gesture.TRIPLE_PRESS;
        }
        FLogger.INSTANCE.getLocal().d(TAG, "Error!!! Gesture is undefined");
        return Gesture.NONE;
    }

    @DexIgnore
    public static Pusher getPusherByGesture(Gesture gesture) {
        if (gesture != null) {
            switch (Anon1.$SwitchMap$com$misfit$frameworks$common$enums$Gesture[gesture.ordinal()]) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    return Pusher.TOP_PUSHER;
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                    return Pusher.MID_PUSHER;
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                    return Pusher.BOTTOM_PUSHER;
            }
        }
        return null;
    }
}
