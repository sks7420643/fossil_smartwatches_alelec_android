package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.Lk7;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory implements Factory<MicroAppSettingDataSource> {
    @DexIgnore
    public /* final */ MicroAppSettingRepositoryModule module;

    @DexIgnore
    public MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory(MicroAppSettingRepositoryModule microAppSettingRepositoryModule) {
        this.module = microAppSettingRepositoryModule;
    }

    @DexIgnore
    public static MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory create(MicroAppSettingRepositoryModule microAppSettingRepositoryModule) {
        return new MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory(microAppSettingRepositoryModule);
    }

    @DexIgnore
    public static MicroAppSettingDataSource provideFavoriteMappingSetLocalDataSource(MicroAppSettingRepositoryModule microAppSettingRepositoryModule) {
        MicroAppSettingDataSource provideFavoriteMappingSetLocalDataSource = microAppSettingRepositoryModule.provideFavoriteMappingSetLocalDataSource();
        Lk7.c(provideFavoriteMappingSetLocalDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideFavoriteMappingSetLocalDataSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public MicroAppSettingDataSource get() {
        return provideFavoriteMappingSetLocalDataSource(this.module);
    }
}
