package com.portfolio.platform.data.legacy.threedotzero;

import com.mapped.U04;
import com.portfolio.platform.data.source.UAppSystemVersionRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRepository_Factory implements Factory<MicroAppVariantRepository> {
    @DexIgnore
    public /* final */ Provider<U04> mAppExecutorsProvider;
    @DexIgnore
    public /* final */ Provider<MicroAppVariantDataSource> mMicroAppVariantLocalDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<MicroAppVariantDataSource> mMicroAppVariantRemoteDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<UAppSystemVersionRepository> mUAppSystemVersionRepositoryProvider;

    @DexIgnore
    public MicroAppVariantRepository_Factory(Provider<MicroAppVariantDataSource> provider, Provider<MicroAppVariantDataSource> provider2, Provider<UAppSystemVersionRepository> provider3, Provider<U04> provider4) {
        this.mMicroAppVariantRemoteDataSourceProvider = provider;
        this.mMicroAppVariantLocalDataSourceProvider = provider2;
        this.mUAppSystemVersionRepositoryProvider = provider3;
        this.mAppExecutorsProvider = provider4;
    }

    @DexIgnore
    public static MicroAppVariantRepository_Factory create(Provider<MicroAppVariantDataSource> provider, Provider<MicroAppVariantDataSource> provider2, Provider<UAppSystemVersionRepository> provider3, Provider<U04> provider4) {
        return new MicroAppVariantRepository_Factory(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static MicroAppVariantRepository newInstance(MicroAppVariantDataSource microAppVariantDataSource, MicroAppVariantDataSource microAppVariantDataSource2, UAppSystemVersionRepository uAppSystemVersionRepository, U04 u04) {
        return new MicroAppVariantRepository(microAppVariantDataSource, microAppVariantDataSource2, uAppSystemVersionRepository, u04);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public MicroAppVariantRepository get() {
        return newInstance(this.mMicroAppVariantRemoteDataSourceProvider.get(), this.mMicroAppVariantLocalDataSourceProvider.get(), this.mUAppSystemVersionRepositoryProvider.get(), this.mAppExecutorsProvider.get());
    }
}
