package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.wearables.fsl.location.DeviceLocation;
import com.mapped.Qg6;
import com.mapped.U04;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import com.portfolio.platform.data.source.remote.ShortcutApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRemoteDataSource extends MicroAppGalleryDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ U04 mAppExecutors;
    @DexIgnore
    public /* final */ ShortcutApiService mShortcutApiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppGalleryRemoteDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            Wg6.c(str, "<set-?>");
            MicroAppGalleryRemoteDataSource.TAG = str;
        }
    }

    /*
    static {
        String simpleName = MicroAppGalleryRemoteDataSource.class.getSimpleName();
        Wg6.b(simpleName, "MicroAppGalleryRemoteDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppGalleryRemoteDataSource(ShortcutApiService shortcutApiService, U04 u04) {
        Wg6.c(shortcutApiService, "mShortcutApiService");
        Wg6.c(u04, "mAppExecutors");
        this.mShortcutApiService = shortcutApiService;
        this.mAppExecutors = u04;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void getMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "getMicroAppGallery deviceSerial=" + str);
        this.mShortcutApiService.getMicroAppGallery(0, 100, str).D(new MicroAppGalleryRemoteDataSource$getMicroAppGallery$Anon1(str, getMicroAppGalleryCallback));
    }
}
