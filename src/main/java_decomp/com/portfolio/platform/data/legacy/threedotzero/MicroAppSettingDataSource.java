package com.portfolio.platform.data.legacy.threedotzero;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface MicroAppSettingDataSource {

    @DexIgnore
    public interface MicroAppSettingCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        void onSuccess(MicroAppSetting microAppSetting);
    }

    @DexIgnore
    public interface MicroAppSettingListCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        void onSuccess(List<MicroAppSetting> list);
    }

    @DexIgnore
    public interface PushMicroAppSettingToServerCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        Object onSuccess();  // void declaration
    }

    @DexIgnore
    public interface PushPendingMicroAppSettingsCallback {
        @DexIgnore
        Object onDone();  // void declaration
    }

    @DexIgnore
    void addOrUpdateMicroAppSetting(MicroAppSetting microAppSetting, MicroAppSettingCallback microAppSettingCallback);

    @DexIgnore
    Object clearData();  // void declaration

    @DexIgnore
    void getMicroAppSetting(String str, MicroAppSettingCallback microAppSettingCallback);

    @DexIgnore
    void getMicroAppSettingList(MicroAppSettingListCallback microAppSettingListCallback);

    @DexIgnore
    void mergeMicroAppSetting(MicroAppSetting microAppSetting, MicroAppSettingCallback microAppSettingCallback);
}
