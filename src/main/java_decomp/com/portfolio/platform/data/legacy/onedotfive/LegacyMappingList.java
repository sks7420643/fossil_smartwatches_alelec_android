package com.portfolio.platform.data.legacy.onedotfive;

import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.Mapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LegacyMappingList {
    @DexIgnore
    @Vu3("action")
    public int action;
    @DexIgnore
    @Vu3("deviceId")
    public String deviceId;
    @DexIgnore
    @Vu3(Mapping.COLUMN_EXTRA_INFO)
    public String extraInfo;
    @DexIgnore
    @Vu3("gesture")
    public String gesture;
    @DexIgnore
    @Vu3("isServiceCommand")
    public boolean isServiceCommand;
    @DexIgnore
    @Vu3("mDeviceFamily")
    public String mDeviceFamily;
    @DexIgnore
    @Vu3("updatedAt")
    public String updatedAt;

    @DexIgnore
    public LegacyMappingList(boolean z, int i, String str, String str2, String str3, String str4, String str5) {
        Wg6.c(str, "mDeviceFamily");
        Wg6.c(str2, "deviceId");
        Wg6.c(str3, Mapping.COLUMN_EXTRA_INFO);
        Wg6.c(str4, "gesture");
        Wg6.c(str5, "updatedAt");
        this.isServiceCommand = z;
        this.action = i;
        this.mDeviceFamily = str;
        this.deviceId = str2;
        this.extraInfo = str3;
        this.gesture = str4;
        this.updatedAt = str5;
    }

    @DexIgnore
    public final int getAction() {
        return this.action;
    }

    @DexIgnore
    public final String getDeviceId() {
        return this.deviceId;
    }

    @DexIgnore
    public final String getExtraInfo() {
        return this.extraInfo;
    }

    @DexIgnore
    public final String getGesture() {
        return this.gesture;
    }

    @DexIgnore
    public final String getMDeviceFamily() {
        return this.mDeviceFamily;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final boolean isServiceCommand() {
        return this.isServiceCommand;
    }

    @DexIgnore
    public final void setAction(int i) {
        this.action = i;
    }

    @DexIgnore
    public final void setDeviceId(String str) {
        Wg6.c(str, "<set-?>");
        this.deviceId = str;
    }

    @DexIgnore
    public final void setExtraInfo(String str) {
        Wg6.c(str, "<set-?>");
        this.extraInfo = str;
    }

    @DexIgnore
    public final void setGesture(String str) {
        Wg6.c(str, "<set-?>");
        this.gesture = str;
    }

    @DexIgnore
    public final void setMDeviceFamily(String str) {
        Wg6.c(str, "<set-?>");
        this.mDeviceFamily = str;
    }

    @DexIgnore
    public final void setServiceCommand(boolean z) {
        this.isServiceCommand = z;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.updatedAt = str;
    }
}
