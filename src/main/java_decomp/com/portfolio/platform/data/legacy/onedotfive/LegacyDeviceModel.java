package com.portfolio.platform.data.legacy.onedotfive;

import com.facebook.places.PlaceManager;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mapped.Vu3;
import com.misfit.frameworks.buttonservice.db.DataLogService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "device")
public class LegacyDeviceModel {
    @DexIgnore
    public static /* final */ String COLUMN_BATTERY_LEVEL; // = "batteryLevel";
    @DexIgnore
    public static /* final */ String COLUMN_CREATED_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_ID; // = "deviceId";
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_MODEL; // = "sku";
    @DexIgnore
    public static /* final */ String COLUMN_FIRMWARE_VERSION; // = "firmwareRevision";
    @DexIgnore
    public static /* final */ String COLUMN_IS_CURRENT; // = "isCurrent";
    @DexIgnore
    public static /* final */ String COLUMN_MAC_ADDRESS; // = "macAddress";
    @DexIgnore
    public static /* final */ String COLUMN_MODE; // = "mode";
    @DexIgnore
    @DatabaseField(columnName = COLUMN_BATTERY_LEVEL)
    public int batteryLevel;
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    @Vu3("createdAt")
    public String createdAt;
    @DexIgnore
    @DatabaseField(columnName = "deviceId", id = true)
    @Vu3("deviceId")
    public String deviceId;
    @DexIgnore
    @Vu3("deviceType")
    public String deviceType;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_FIRMWARE_VERSION)
    @Vu3(COLUMN_FIRMWARE_VERSION)
    public String firmwareRevision;
    @DexIgnore
    @Vu3("hardwareRevision")
    public String hardwareRevision;
    @DexIgnore
    @Vu3("hostMaker")
    public String hostMaker;
    @DexIgnore
    @Vu3("hostModel")
    public String hostModel;
    @DexIgnore
    @Vu3("hostOS")
    public String hostOS;
    @DexIgnore
    @Vu3("hostOSVersion")
    public String hostOSVersion;
    @DexIgnore
    @Vu3("hostSystemLanguage")
    public String hostSystemLanguage;
    @DexIgnore
    @Vu3("hostSystemLocale")
    public String hostSystemLocale;
    @DexIgnore
    @Vu3("href")
    public String href;
    @DexIgnore
    @DatabaseField(columnName = "isCurrent")
    public transient boolean isCurrent;
    @DexIgnore
    @Vu3("lastConnection")
    public String lastConnection;
    @DexIgnore
    @Vu3("lastDisconnection")
    public String lastDisconnection;
    @DexIgnore
    @Vu3("lastFirmwareUpdate")
    public String lastFirmwareUpdate;
    @DexIgnore
    @Vu3("lastRecoveryModeEnd")
    public String lastRecoveryModeEnd;
    @DexIgnore
    @Vu3("lastRecoveryModeStart")
    public String lastRecoveryModeStart;
    @DexIgnore
    @DatabaseField(columnName = "macAddress")
    @Vu3(PlaceManager.PARAM_MAC_ADDRESS)
    public String macAddress;
    @DexIgnore
    @Vu3("manufacturer")
    public String manufacturer;
    @DexIgnore
    @DatabaseField(columnName = "mode")
    public int mode;
    @DexIgnore
    @Vu3("owner")
    public String owner;
    @DexIgnore
    @Vu3("productDisplayName")
    public String productDisplayName;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_DEVICE_MODEL)
    @Vu3(COLUMN_DEVICE_MODEL)
    public String sku;
    @DexIgnore
    @Vu3("softwareRevision")
    public String softwreRevision;
    @DexIgnore
    @DatabaseField(columnName = DataLogService.COLUMN_UPDATE_AT)
    @Vu3("updatedAt")
    public String updateAt;

    @DexIgnore
    public LegacyDeviceModel() {
    }

    @DexIgnore
    public LegacyDeviceModel(String str, String str2, boolean z, String str3, String str4, int i) {
        this.deviceId = str;
        this.macAddress = str2;
        this.isCurrent = z;
        this.sku = str3;
        this.firmwareRevision = str4;
        this.batteryLevel = i;
    }

    @DexIgnore
    public int getBatteryLevel() {
        return this.batteryLevel;
    }

    @DexIgnore
    public String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public String getDeviceId() {
        return this.deviceId;
    }

    @DexIgnore
    public String getDeviceType() {
        return this.deviceType;
    }

    @DexIgnore
    public String getFirmwareRevision() {
        return this.firmwareRevision;
    }

    @DexIgnore
    public String getHardwareRevision() {
        return this.hardwareRevision;
    }

    @DexIgnore
    public String getHostMaker() {
        return this.hostMaker;
    }

    @DexIgnore
    public String getHostModel() {
        return this.hostModel;
    }

    @DexIgnore
    public String getHostOS() {
        return this.hostOS;
    }

    @DexIgnore
    public String getHostOSVersion() {
        return this.hostOSVersion;
    }

    @DexIgnore
    public String getHostSystemLanguage() {
        return this.hostSystemLanguage;
    }

    @DexIgnore
    public String getHostSystemLocale() {
        return this.hostSystemLocale;
    }

    @DexIgnore
    public String getHref() {
        return this.href;
    }

    @DexIgnore
    public String getLastConnection() {
        return this.lastConnection;
    }

    @DexIgnore
    public String getLastDisconnection() {
        return this.lastDisconnection;
    }

    @DexIgnore
    public String getLastFirmwareUpdate() {
        return this.lastFirmwareUpdate;
    }

    @DexIgnore
    public String getLastRecoveryModeEnd() {
        return this.lastRecoveryModeEnd;
    }

    @DexIgnore
    public String getLastRecoveryModeStart() {
        return this.lastRecoveryModeStart;
    }

    @DexIgnore
    public String getMacAddress() {
        return this.macAddress;
    }

    @DexIgnore
    public String getManufacturer() {
        return this.manufacturer;
    }

    @DexIgnore
    public int getMode() {
        return this.mode;
    }

    @DexIgnore
    public String getOwner() {
        return this.owner;
    }

    @DexIgnore
    public String getProductDisplayName() {
        return this.productDisplayName;
    }

    @DexIgnore
    public String getSku() {
        return this.sku;
    }

    @DexIgnore
    public String getSoftwreRevision() {
        return this.softwreRevision;
    }

    @DexIgnore
    public String getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public boolean isCurrent() {
        return this.isCurrent;
    }

    @DexIgnore
    public void setBatteryLevel(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public void setDeviceId(String str) {
        this.deviceId = str;
    }

    @DexIgnore
    public void setDeviceType(String str) {
        this.deviceType = str;
    }

    @DexIgnore
    public void setFirmwareRevision(String str) {
        this.firmwareRevision = str;
    }

    @DexIgnore
    public void setHardwareRevision(String str) {
        this.hardwareRevision = str;
    }

    @DexIgnore
    public void setHostMaker(String str) {
        this.hostMaker = str;
    }

    @DexIgnore
    public void setHostModel(String str) {
        this.hostModel = str;
    }

    @DexIgnore
    public void setHostOS(String str) {
        this.hostOS = str;
    }

    @DexIgnore
    public void setHostOSVersion(String str) {
        this.hostOSVersion = str;
    }

    @DexIgnore
    public void setHostSystemLanguage(String str) {
        this.hostSystemLanguage = str;
    }

    @DexIgnore
    public void setHostSystemLocale(String str) {
        this.hostSystemLocale = str;
    }

    @DexIgnore
    public void setHref(String str) {
        this.href = str;
    }

    @DexIgnore
    public void setIsCurrent(boolean z) {
        this.isCurrent = z;
    }

    @DexIgnore
    public void setLastConnection(String str) {
        this.lastConnection = str;
    }

    @DexIgnore
    public void setLastDisconnection(String str) {
        this.lastDisconnection = str;
    }

    @DexIgnore
    public void setLastFirmwareUpdate(String str) {
        this.lastFirmwareUpdate = str;
    }

    @DexIgnore
    public void setLastRecoveryModeEnd(String str) {
        this.lastRecoveryModeEnd = str;
    }

    @DexIgnore
    public void setLastRecoveryModeStart(String str) {
        this.lastRecoveryModeStart = str;
    }

    @DexIgnore
    public void setMacAddress(String str) {
        this.macAddress = str;
    }

    @DexIgnore
    public void setManufacturer(String str) {
        this.manufacturer = str;
    }

    @DexIgnore
    public void setMode(int i) {
        this.mode = i;
    }

    @DexIgnore
    public void setOwner(String str) {
        this.owner = str;
    }

    @DexIgnore
    public void setProductDisplayName(String str) {
        this.productDisplayName = str;
    }

    @DexIgnore
    public void setSku(String str) {
        this.sku = str;
    }

    @DexIgnore
    public void setSoftwreRevision(String str) {
        this.softwreRevision = str;
    }

    @DexIgnore
    public void setUpdateAt(String str) {
        this.updateAt = str;
    }
}
