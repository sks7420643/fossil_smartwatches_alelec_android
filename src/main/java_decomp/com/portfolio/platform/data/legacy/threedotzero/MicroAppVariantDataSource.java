package com.portfolio.platform.data.legacy.threedotzero;

import com.mapped.Hp4;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class MicroAppVariantDataSource {

    @DexIgnore
    public interface AddOrUpdateDeclarationFileCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        void onSuccess(DeclarationFile declarationFile);
    }

    @DexIgnore
    public interface GetVariantCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(MicroAppVariant microAppVariant);
    }

    @DexIgnore
    public interface GetVariantListCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(List<MicroAppVariant> list);
    }

    @DexIgnore
    public interface GetVariantListRemoteCallback extends GetVariantListCallback {
        @DexIgnore
        void onSuccess(ArrayList<Hp4> arrayList);
    }

    @DexIgnore
    public interface MigrateVariantCallback {
        @DexIgnore
        Object onDone();  // void declaration
    }

    @DexIgnore
    public void addOrUpdateDeclarationFile(DeclarationFile declarationFile, AddOrUpdateDeclarationFileCallback addOrUpdateDeclarationFileCallback) {
        Wg6.c(declarationFile, "declarationFile");
        Wg6.c(addOrUpdateDeclarationFileCallback, Constants.CALLBACK);
    }

    @DexIgnore
    public abstract void getAllMicroAppVariants(String str, int i, int i2, GetVariantListCallback getVariantListCallback);

    @DexIgnore
    public void getMicroAppVariant(String str, String str2, String str3, int i, int i2, GetVariantCallback getVariantCallback) {
        Wg6.c(str, "serialNumber");
        Wg6.c(str2, "microAppId");
        Wg6.c(str3, "variantName");
    }

    @DexIgnore
    public void removeMicroAppVariants(String str, int i, int i2) {
        Wg6.c(str, "serialNumber");
    }
}
