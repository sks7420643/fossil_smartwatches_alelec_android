package com.portfolio.platform.data.legacy.threedotzero;

import android.text.TextUtils;
import com.fossil.I14;
import com.fossil.Mn5;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class PresetLocalDataSource extends PresetDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "PresetLocalDataSource";

    @DexIgnore
    private SavedPreset findLocalPreset(String str, List<SavedPreset> list) {
        for (SavedPreset savedPreset : list) {
            if (savedPreset.getId().equalsIgnoreCase(str)) {
                return savedPreset;
            }
        }
        return null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void addOrUpdateActivePreset(ActivePreset activePreset, PresetDataSource.AddOrUpdateActivePresetCallback addOrUpdateActivePresetCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateActivePreset serial=" + activePreset.getSerialNumber());
        if (Mn5.p.a().e().addOrUpdateActivePreset(activePreset)) {
            MFLogger.d(TAG, "addOrUpdateActivePreset onSuccess");
            if (addOrUpdateActivePresetCallback != null) {
                addOrUpdateActivePresetCallback.onSuccess(activePreset);
                return;
            }
            return;
        }
        MFLogger.d(TAG, "addOrUpdateActivePreset onFail");
        if (addOrUpdateActivePresetCallback != null) {
            addOrUpdateActivePresetCallback.onFail();
        }
    }

    @DexIgnore
    public boolean addOrUpdateDefaultPreset(RecommendedPreset recommendedPreset) {
        if (recommendedPreset == null) {
            return false;
        }
        String str = TAG;
        MFLogger.d(str, ".addOrUpdateDefaultPreset - presetListId=" + recommendedPreset.getId() + ", serial=" + recommendedPreset.getSerialNumber());
        DeviceProvider e = Mn5.p.a().e();
        RecommendedPreset defaultPreset = e.getDefaultPreset(recommendedPreset.getSerialNumber());
        if (defaultPreset != null) {
            recommendedPreset.setId(defaultPreset.getId());
        }
        return e.addOrUpdateRecommendedPreset(recommendedPreset);
    }

    @DexIgnore
    public boolean addOrUpdateRecommendedPresets(RecommendedPreset recommendedPreset) {
        return Mn5.p.a().e().addOrUpdateRecommendedPreset(recommendedPreset);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void addOrUpdateSavedPreset(SavedPreset savedPreset, PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback) {
        I14.l(savedPreset);
        SavedPreset savedPreset2 = savedPreset;
        String str = TAG;
        MFLogger.d(str, "addOrUpdateSavedPreset presetId=" + savedPreset2.getId() + " presetName=" + savedPreset2.getName() + ", pinType=" + savedPreset2.getPinType());
        SavedPreset savedPresetById = Mn5.p.a().e().getSavedPresetById(savedPreset2.getId());
        if (savedPresetById != null && savedPresetById.getPinType() == 3) {
            MFLogger.d(TAG, "addOrUpdateSavedPreset onFail, this saved preset is deleted, cannot add or edit.");
            if (addOrUpdateSavedPresetCallback != null) {
                addOrUpdateSavedPresetCallback.onFail();
            }
        } else if (Mn5.p.a().e().addOrUpdateSavedPreset(savedPreset2)) {
            MFLogger.d(TAG, "addOrUpdateSavedPreset onSuccess");
            if (addOrUpdateSavedPresetCallback != null) {
                addOrUpdateSavedPresetCallback.onSuccess(savedPreset2);
            }
        } else {
            MFLogger.d(TAG, "addOrUpdateSavedPreset onFail");
            if (addOrUpdateSavedPresetCallback != null) {
                addOrUpdateSavedPresetCallback.onFail();
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void addOrUpdateSavedPresetList(List<SavedPreset> list, PresetDataSource.AddOrUpdateSavedPresetListCallback addOrUpdateSavedPresetListCallback) {
        MFLogger.d(TAG, "addOrUpdateSavedPresets");
        if (list == null || list.isEmpty()) {
            MFLogger.d(TAG, "addOrUpdateSavedPresetList saved preset list is empty");
            if (addOrUpdateSavedPresetListCallback != null) {
                addOrUpdateSavedPresetListCallback.onFail();
                return;
            }
            return;
        }
        String str = TAG;
        MFLogger.d(str, ".addOrUpdateSavedPresets - mappingSetListSize=" + list.size());
        DeviceProvider e = Mn5.p.a().e();
        e.getAllSavedPresets();
        ArrayList arrayList = new ArrayList();
        for (SavedPreset savedPreset : list) {
            SavedPreset findLocalPreset = findLocalPreset(savedPreset.getId(), arrayList);
            if ((findLocalPreset == null || findLocalPreset.getPinType() != 3) && e.addOrUpdateSavedPreset(savedPreset)) {
                arrayList.add(savedPreset);
            }
        }
        if (addOrUpdateSavedPresetListCallback != null) {
            addOrUpdateSavedPresetListCallback.onSuccess(arrayList);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void clearData() {
        DeviceProvider e = Mn5.p.a().e();
        e.clearActivePreset();
        e.clearRecommendedPreset();
        e.clearSavedPreset();
    }

    @DexIgnore
    public void deleteAllRecommendedPresets(String str, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "deleteRecommendedPreset serial=" + str);
        if (Mn5.p.a().e().deleteRecommendedPreset(str)) {
            MFLogger.d(TAG, "deleteRecommendedPreset onSuccess");
            if (deleteMappingSetCallback != null) {
                deleteMappingSetCallback.onSuccess();
                return;
            }
            return;
        }
        MFLogger.d(TAG, "deleteRecommendedPreset onFail");
        if (deleteMappingSetCallback != null) {
            deleteMappingSetCallback.onFail();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void deleteSavedPreset(SavedPreset savedPreset, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        String str = TAG;
        MFLogger.d(str, "deleteSavedPreset mappingSetId=" + savedPreset.getId());
        if (Mn5.p.a().e().deleteSavedPreset(savedPreset.getId())) {
            MFLogger.d(TAG, "deleteSavedPreset onSuccess");
            if (deleteMappingSetCallback != null) {
                deleteMappingSetCallback.onSuccess();
                return;
            }
            return;
        }
        MFLogger.d(TAG, "deleteSavedPreset onFail");
        if (deleteMappingSetCallback != null) {
            deleteMappingSetCallback.onFail();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void deleteSavedPresetList(List<SavedPreset> list, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        String str = TAG;
        MFLogger.d(str, "deleteSavedPresetList mappingSetId=" + list.size());
        for (SavedPreset savedPreset : list) {
            Mn5.p.a().e().deleteSavedPreset(savedPreset.getId());
        }
        MFLogger.d(TAG, "deleteSavedPresetList onSuccess");
        if (deleteMappingSetCallback != null) {
            deleteMappingSetCallback.onSuccess();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void downloadActivePresetList(PresetDataSource.GetActivePresetListCallback getActivePresetListCallback) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getActivePreset(String str, PresetDataSource.GetActivePresetCallback getActivePresetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getActivePreset serial=" + str);
        ActivePreset activePreset = Mn5.p.a().e().getActivePreset(str);
        if (activePreset != null) {
            String str3 = TAG;
            MFLogger.d(str3, "getActivePreset onSuccess - activePreset: " + activePreset.getButtons());
            if (getActivePresetCallback != null) {
                getActivePresetCallback.onSuccess(activePreset);
                return;
            }
            return;
        }
        MFLogger.d(TAG, "getActivePreset onFail");
        if (getActivePresetCallback != null) {
            getActivePresetCallback.onFail();
        }
    }

    @DexIgnore
    public List<ActivePreset> getAllPendingActivePresets() {
        return Mn5.p.a().e().getPendingActivePresets();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getDefaultPreset(String str, PresetDataSource.GetRecommendedPresetCallback getRecommendedPresetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getDefaultPreset deviceSerial=" + str);
        RecommendedPreset defaultPreset = Mn5.p.a().e().getDefaultPreset(str);
        if (defaultPreset == null) {
            String str3 = TAG;
            MFLogger.d(str3, "getDefaultPreset deviceSerial=" + str + " onFail");
            if (getRecommendedPresetCallback != null) {
                getRecommendedPresetCallback.onFail();
                return;
            }
            return;
        }
        String str4 = TAG;
        MFLogger.d(str4, "getDefaultPreset deviceSerial=" + str + " onSuccess");
        if (getRecommendedPresetCallback != null) {
            getRecommendedPresetCallback.onSuccess(defaultPreset);
        }
    }

    @DexIgnore
    public List<SavedPreset> getPendingDeletedUserPresets() {
        return Mn5.p.a().e().getPendingDeletedUserPresets();
    }

    @DexIgnore
    public List<SavedPreset> getPendingUserPresets() {
        return Mn5.p.a().e().getPendingUserPresets();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getRecommendedPresets(String str, PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getRecommendedPresets deviceSerial=" + str);
        if (!TextUtils.isEmpty(str)) {
            List<RecommendedPreset> listRecommendedPresetBySerial = Mn5.p.a().e().getListRecommendedPresetBySerial(str);
            if (listRecommendedPresetBySerial == null || listRecommendedPresetBySerial.isEmpty()) {
                String str3 = TAG;
                MFLogger.d(str3, "getRecommendedPresets deviceSerial=" + str + " onFail");
                if (getRecommendedPresetListCallback != null) {
                    getRecommendedPresetListCallback.onFail();
                    return;
                }
                return;
            }
            String str4 = TAG;
            MFLogger.d(str4, "getRecommendedPresets deviceSerial=" + str + " onSuccess");
            if (getRecommendedPresetListCallback != null) {
                getRecommendedPresetListCallback.onSuccess(listRecommendedPresetBySerial);
            }
        } else if (getRecommendedPresetListCallback != null) {
            getRecommendedPresetListCallback.onFail();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getSavedPreset(String str, PresetDataSource.GetSavedPresetCallback getSavedPresetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getSavedPreset presetId=" + str);
        SavedPreset savedPresetById = Mn5.p.a().e().getSavedPresetById(str);
        if (savedPresetById != null) {
            MFLogger.d(TAG, "getSavedPreset onSuccess");
            if (getSavedPresetCallback != null) {
                getSavedPresetCallback.onSuccess(savedPresetById);
                return;
            }
            return;
        }
        MFLogger.d(TAG, "getSavedPreset onFail");
        if (getSavedPresetCallback != null) {
            getSavedPresetCallback.onFail();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getSavedPresetList(PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
        MFLogger.d(TAG, "getSavedPresetList");
        List<SavedPreset> allSavedPresetsExcludeDeleteType = Mn5.p.a().e().getAllSavedPresetsExcludeDeleteType();
        if (allSavedPresetsExcludeDeleteType == null || allSavedPresetsExcludeDeleteType.isEmpty()) {
            MFLogger.d(TAG, "getSavedPresetList onFail");
            if (getSavedPresetListCallback != null) {
                getSavedPresetListCallback.onFail();
                return;
            }
            return;
        }
        MFLogger.d(TAG, "getSavedPresetList onSuccess");
        if (getSavedPresetListCallback != null) {
            getSavedPresetListCallback.onSuccess(allSavedPresetsExcludeDeleteType);
        }
    }

    @DexIgnore
    public void updateActivePresetPinType(String str, int i) {
        DeviceProvider e = Mn5.p.a().e();
        ActivePreset activePreset = e.getActivePreset(str);
        if (activePreset != null) {
            activePreset.setPinType(i);
            e.addOrUpdateActivePreset(activePreset);
        }
    }

    @DexIgnore
    public void updateSavedPresetPinType(String str, int i) {
        String str2 = TAG;
        MFLogger.d(str2, "updateSavedPresetPinType presetId=" + str);
        DeviceProvider e = Mn5.p.a().e();
        SavedPreset savedPresetById = e.getSavedPresetById(str);
        String str3 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Is save preset in db yet=");
        sb.append(savedPresetById != null);
        MFLogger.d(str3, sb.toString());
        if (savedPresetById != null) {
            savedPresetById.setPinType(i);
            e.addOrUpdateSavedPreset(savedPresetById);
        }
    }
}
