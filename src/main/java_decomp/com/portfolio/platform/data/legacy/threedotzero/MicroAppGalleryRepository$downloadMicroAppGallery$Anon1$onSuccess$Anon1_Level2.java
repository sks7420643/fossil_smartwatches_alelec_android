package com.portfolio.platform.data.legacy.threedotzero;

import com.misfit.frameworks.common.log.MFLogger;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRepository$downloadMicroAppGallery$Anon1$onSuccess$Anon1_Level2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $listSupportedMicroApp;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppGalleryRepository$downloadMicroAppGallery$Anon1 this$0;

    @DexIgnore
    public MicroAppGalleryRepository$downloadMicroAppGallery$Anon1$onSuccess$Anon1_Level2(MicroAppGalleryRepository$downloadMicroAppGallery$Anon1 microAppGalleryRepository$downloadMicroAppGallery$Anon1, ArrayList arrayList) {
        this.this$0 = microAppGalleryRepository$downloadMicroAppGallery$Anon1;
        this.$listSupportedMicroApp = arrayList;
    }

    @DexIgnore
    public final void run() {
        MFLogger.d(MicroAppGalleryRepository.Companion.getTAG(), "diskIO enter onSuccess downloadMicroAppGallery");
        this.this$0.this$0.mMicroAppSettingLocalDataSource.deleteListMicroApp(this.this$0.$deviceSerial);
        this.this$0.this$0.mMicroAppSettingLocalDataSource.updateListMicroApp(this.$listSupportedMicroApp);
        MFLogger.d(MicroAppGalleryRepository.Companion.getTAG(), "diskIO exit onSuccess downloadMicroAppGallery");
    }
}
