package com.portfolio.platform.data.legacy.threedotzero;

import com.mapped.Dx6;
import com.mapped.Ku3;
import com.mapped.Qg6;
import com.mapped.U04;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRemoteDataSource extends MicroAppVariantDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ U04 mAppExecutors;
    @DexIgnore
    public /* final */ ShortcutApiService mShortcutApiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppVariantRemoteDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            Wg6.c(str, "<set-?>");
            MicroAppVariantRemoteDataSource.TAG = str;
        }
    }

    /*
    static {
        String simpleName = MicroAppVariantRemoteDataSource.class.getSimpleName();
        Wg6.b(simpleName, "MicroAppVariantRemoteDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppVariantRemoteDataSource(ShortcutApiService shortcutApiService, U04 u04) {
        Wg6.c(shortcutApiService, "mShortcutApiService");
        Wg6.c(u04, "mAppExecutors");
        this.mShortcutApiService = shortcutApiService;
        this.mAppExecutors = u04;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppVariantDataSource
    public void getAllMicroAppVariants(String str, int i, int i2, MicroAppVariantDataSource.GetVariantListCallback getVariantListCallback) {
        Wg6.c(str, "serialNumber");
        String str2 = TAG;
        MFLogger.d(str2, "getAllMicroAppVariants serialNumber=" + str);
        getAllMicroAppVariants$app_fossilRelease(str, i, i2, 0, 100, new MicroAppVariantRemoteDataSource$getAllMicroAppVariants$resultCallback$Anon1(this, str, new ArrayList(), i, i2, getVariantListCallback));
    }

    @DexIgnore
    public final void getAllMicroAppVariants$app_fossilRelease(String str, int i, int i2, int i3, int i4, Dx6<Ku3> dx6) {
        Wg6.c(str, "serialNumber");
        Wg6.c(dx6, Constants.CALLBACK);
    }
}
