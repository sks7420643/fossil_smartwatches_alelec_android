package com.portfolio.platform.data.legacy.threedotzero;

import android.text.TextUtils;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.mapped.Qg6;
import com.mapped.U04;
import com.mapped.Wg6;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRepository extends MicroAppGalleryDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static String TAG;
    @DexIgnore
    public /* final */ U04 mAppExecutors;
    @DexIgnore
    public /* final */ MicroAppGalleryDataSource mMicroAppSettingLocalDataSource;
    @DexIgnore
    public /* final */ MicroAppGalleryDataSource mMicroAppSettingRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppGalleryRepository.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            Wg6.c(str, "<set-?>");
            MicroAppGalleryRepository.TAG = str;
        }
    }

    /*
    static {
        String simpleName = MicroAppGalleryRepository.class.getSimpleName();
        Wg6.b(simpleName, "MicroAppGalleryRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public MicroAppGalleryRepository(@Remote MicroAppGalleryDataSource microAppGalleryDataSource, @Local MicroAppGalleryDataSource microAppGalleryDataSource2, U04 u04) {
        Wg6.c(microAppGalleryDataSource, "mMicroAppSettingRemoteDataSource");
        Wg6.c(microAppGalleryDataSource2, "mMicroAppSettingLocalDataSource");
        Wg6.c(u04, "mAppExecutors");
        this.mMicroAppSettingRemoteDataSource = microAppGalleryDataSource;
        this.mMicroAppSettingLocalDataSource = microAppGalleryDataSource2;
        this.mAppExecutors = u04;
    }

    @DexIgnore
    public final void downloadMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        String str2 = TAG;
        MFLogger.d(str2, "downloadMicroAppGallery deviceSerial=" + str);
        this.mMicroAppSettingRemoteDataSource.getMicroAppGallery(str, new MicroAppGalleryRepository$downloadMicroAppGallery$Anon1(this, str, getMicroAppGalleryCallback));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void getMicroApp(String str, String str2, MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback) {
        Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        Wg6.c(str2, "microAppId");
        if (TextUtils.isEmpty(str)) {
            MFLogger.d(TAG, "getMicroApp deviceSerial=empty");
            if (getMicroAppCallback != null) {
                getMicroAppCallback.onFail();
                return;
            }
            return;
        }
        this.mMicroAppSettingLocalDataSource.getMicroApp(str, str2, new MicroAppGalleryRepository$getMicroApp$Anon1(this, str2, getMicroAppCallback, str));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void getMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        String str2 = TAG;
        MFLogger.d(str2, "getMicroAppGallery deviceSerial=" + str);
        if (TextUtils.isEmpty(str)) {
            MFLogger.d(TAG, "getMicroAppGallery deviceSerial=empty");
            if (getMicroAppGalleryCallback != null) {
                getMicroAppGalleryCallback.onFail();
                return;
            }
            return;
        }
        this.mMicroAppSettingLocalDataSource.getMicroAppGallery(str, new MicroAppGalleryRepository$getMicroAppGallery$Anon1(str, getMicroAppGalleryCallback));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void updateMicroApp(MicroApp microApp, MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback) {
        Wg6.c(microApp, "microApp");
        String str = TAG;
        MFLogger.d(str, "updateMicroApp microApp=" + microApp.getAppId());
        this.mMicroAppSettingLocalDataSource.updateMicroApp(microApp, getMicroAppCallback);
    }
}
