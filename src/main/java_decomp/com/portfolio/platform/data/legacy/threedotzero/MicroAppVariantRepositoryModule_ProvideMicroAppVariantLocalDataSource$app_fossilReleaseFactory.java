package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.Lk7;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRepositoryModule_ProvideMicroAppVariantLocalDataSource$app_fossilReleaseFactory implements Factory<MicroAppVariantDataSource> {
    @DexIgnore
    public /* final */ MicroAppVariantRepositoryModule module;

    @DexIgnore
    public MicroAppVariantRepositoryModule_ProvideMicroAppVariantLocalDataSource$app_fossilReleaseFactory(MicroAppVariantRepositoryModule microAppVariantRepositoryModule) {
        this.module = microAppVariantRepositoryModule;
    }

    @DexIgnore
    public static MicroAppVariantRepositoryModule_ProvideMicroAppVariantLocalDataSource$app_fossilReleaseFactory create(MicroAppVariantRepositoryModule microAppVariantRepositoryModule) {
        return new MicroAppVariantRepositoryModule_ProvideMicroAppVariantLocalDataSource$app_fossilReleaseFactory(microAppVariantRepositoryModule);
    }

    @DexIgnore
    public static MicroAppVariantDataSource provideMicroAppVariantLocalDataSource$app_fossilRelease(MicroAppVariantRepositoryModule microAppVariantRepositoryModule) {
        MicroAppVariantDataSource provideMicroAppVariantLocalDataSource$app_fossilRelease = microAppVariantRepositoryModule.provideMicroAppVariantLocalDataSource$app_fossilRelease();
        Lk7.c(provideMicroAppVariantLocalDataSource$app_fossilRelease, "Cannot return null from a non-@Nullable @Provides method");
        return provideMicroAppVariantLocalDataSource$app_fossilRelease;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public MicroAppVariantDataSource get() {
        return provideMicroAppVariantLocalDataSource$app_fossilRelease(this.module);
    }
}
