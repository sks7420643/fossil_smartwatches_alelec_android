package com.portfolio.platform.data.legacy.threedotzero;

import com.mapped.U04;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppSettingRepository_Factory implements Factory<MicroAppSettingRepository> {
    @DexIgnore
    public /* final */ Provider<U04> appExecutorsProvider;
    @DexIgnore
    public /* final */ Provider<MicroAppSettingDataSource> microAppSettingLocalDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<MicroAppSettingDataSource> microAppSettingRemoteDataSourceProvider;

    @DexIgnore
    public MicroAppSettingRepository_Factory(Provider<MicroAppSettingDataSource> provider, Provider<MicroAppSettingDataSource> provider2, Provider<U04> provider3) {
        this.microAppSettingRemoteDataSourceProvider = provider;
        this.microAppSettingLocalDataSourceProvider = provider2;
        this.appExecutorsProvider = provider3;
    }

    @DexIgnore
    public static MicroAppSettingRepository_Factory create(Provider<MicroAppSettingDataSource> provider, Provider<MicroAppSettingDataSource> provider2, Provider<U04> provider3) {
        return new MicroAppSettingRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static MicroAppSettingRepository newInstance(MicroAppSettingDataSource microAppSettingDataSource, MicroAppSettingDataSource microAppSettingDataSource2, U04 u04) {
        return new MicroAppSettingRepository(microAppSettingDataSource, microAppSettingDataSource2, u04);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public MicroAppSettingRepository get() {
        return newInstance(this.microAppSettingRemoteDataSourceProvider.get(), this.microAppSettingLocalDataSourceProvider.get(), this.appExecutorsProvider.get());
    }
}
