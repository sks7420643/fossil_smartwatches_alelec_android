package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.Lk7;
import com.mapped.U04;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory implements Factory<MicroAppSettingDataSource> {
    @DexIgnore
    public /* final */ Provider<U04> appExecutorsProvider;
    @DexIgnore
    public /* final */ MicroAppSettingRepositoryModule module;
    @DexIgnore
    public /* final */ Provider<ShortcutApiService> shortcutApiServiceProvider;

    @DexIgnore
    public MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory(MicroAppSettingRepositoryModule microAppSettingRepositoryModule, Provider<ShortcutApiService> provider, Provider<U04> provider2) {
        this.module = microAppSettingRepositoryModule;
        this.shortcutApiServiceProvider = provider;
        this.appExecutorsProvider = provider2;
    }

    @DexIgnore
    public static MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory create(MicroAppSettingRepositoryModule microAppSettingRepositoryModule, Provider<ShortcutApiService> provider, Provider<U04> provider2) {
        return new MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory(microAppSettingRepositoryModule, provider, provider2);
    }

    @DexIgnore
    public static MicroAppSettingDataSource provideFavoriteMappingSetRemoteDataSource(MicroAppSettingRepositoryModule microAppSettingRepositoryModule, ShortcutApiService shortcutApiService, U04 u04) {
        MicroAppSettingDataSource provideFavoriteMappingSetRemoteDataSource = microAppSettingRepositoryModule.provideFavoriteMappingSetRemoteDataSource(shortcutApiService, u04);
        Lk7.c(provideFavoriteMappingSetRemoteDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideFavoriteMappingSetRemoteDataSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public MicroAppSettingDataSource get() {
        return provideFavoriteMappingSetRemoteDataSource(this.module, this.shortcutApiServiceProvider.get(), this.appExecutorsProvider.get());
    }
}
