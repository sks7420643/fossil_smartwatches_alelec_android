package com.portfolio.platform.data.legacy.threedotzero;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mapped.Tu3;
import com.misfit.frameworks.buttonservice.utils.KeyUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = KeyUtils.KEY_SECOND_TIMEZONE)
public class LegacySecondTimezoneSetting implements Parcelable, Comparable<LegacySecondTimezoneSetting> {
    @DexIgnore
    public static /* final */ String COLUMN_IS_ACTIVE; // = "isActiveAlarm";
    @DexIgnore
    public static /* final */ String COLUMN_TIMEZONE_CITY_NAME; // = "timezone_city_name";
    @DexIgnore
    public static /* final */ String COLUMN_TIMEZONE_ID; // = "timezone_id";
    @DexIgnore
    public static /* final */ String COLUMN_TIMEZONE_OFFSET; // = "timezone_offset";
    @DexIgnore
    public static /* final */ String COLUMN_TIMEZONE_TYPE; // = "timezone_type";
    @DexIgnore
    public static /* final */ String COLUMN_URI; // = "uri";
    @DexIgnore
    public static /* final */ Parcelable.Creator<LegacySecondTimezoneSetting> CREATOR; // = new Anon1();
    @DexIgnore
    @DatabaseField(columnName = "isActiveAlarm")
    public boolean isActive;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_TIMEZONE_CITY_NAME)
    @Tu3
    public String timezoneCityName;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_TIMEZONE_ID)
    @Tu3
    public String timezoneId;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_TIMEZONE_OFFSET)
    @Tu3
    public long timezoneOffset;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_TIMEZONE_TYPE)
    @Tu3
    public String timezoneType;
    @DexIgnore
    @DatabaseField(columnName = "uri", id = true)
    @Tu3
    public String uri;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<LegacySecondTimezoneSetting> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public LegacySecondTimezoneSetting createFromParcel(Parcel parcel) {
            return new LegacySecondTimezoneSetting(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public LegacySecondTimezoneSetting[] newArray(int i) {
            return new LegacySecondTimezoneSetting[i];
        }
    }

    @DexIgnore
    public LegacySecondTimezoneSetting() {
    }

    @DexIgnore
    public LegacySecondTimezoneSetting(Parcel parcel) {
        this.timezoneCityName = parcel.readString();
        this.timezoneOffset = parcel.readLong();
        this.timezoneType = parcel.readString();
        this.timezoneId = parcel.readString();
        this.uri = parcel.readString();
    }

    @DexIgnore
    public LegacySecondTimezoneSetting(String str, String str2, long j, String str3) {
        this.timezoneId = str;
        this.timezoneCityName = str2;
        this.timezoneOffset = j;
        this.timezoneType = str3;
        this.uri = "uri:fsl:timezone:" + str2 + ':' + str;
    }

    @DexIgnore
    @Override // java.lang.Object
    public LegacySecondTimezoneSetting clone() {
        LegacySecondTimezoneSetting legacySecondTimezoneSetting = new LegacySecondTimezoneSetting();
        legacySecondTimezoneSetting.timezoneCityName = this.timezoneCityName;
        legacySecondTimezoneSetting.timezoneOffset = this.timezoneOffset;
        legacySecondTimezoneSetting.timezoneType = this.timezoneType;
        legacySecondTimezoneSetting.timezoneId = this.timezoneId;
        legacySecondTimezoneSetting.generateUri();
        return legacySecondTimezoneSetting;
    }

    @DexIgnore
    public int compareTo(LegacySecondTimezoneSetting legacySecondTimezoneSetting) {
        return this.timezoneCityName.compareTo(legacySecondTimezoneSetting.timezoneCityName);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(LegacySecondTimezoneSetting legacySecondTimezoneSetting) {
        return compareTo(legacySecondTimezoneSetting);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void generateUri() {
        this.uri = "uri:fsl:timezone:" + this.timezoneCityName + ':' + this.timezoneId;
    }

    @DexIgnore
    public String getTimezoneCityName() {
        return this.timezoneCityName;
    }

    @DexIgnore
    public String getTimezoneId() {
        return this.timezoneId;
    }

    @DexIgnore
    public long getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public String getTimezoneType() {
        return this.timezoneType;
    }

    @DexIgnore
    public String getUri() {
        return this.uri;
    }

    @DexIgnore
    public void setTimezoneCityName(String str) {
        this.timezoneCityName = str;
    }

    @DexIgnore
    public void setTimezoneId(String str) {
        this.timezoneId = str;
    }

    @DexIgnore
    public void setTimezoneOffset(long j) {
        this.timezoneOffset = j;
    }

    @DexIgnore
    public void setTimezoneType(String str) {
        this.timezoneType = str;
    }

    @DexIgnore
    public void setUri(String str) {
        this.uri = str;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.timezoneCityName);
        parcel.writeLong(this.timezoneOffset);
        parcel.writeString(this.timezoneType);
        parcel.writeString(this.timezoneId);
        parcel.writeString(this.uri);
    }
}
