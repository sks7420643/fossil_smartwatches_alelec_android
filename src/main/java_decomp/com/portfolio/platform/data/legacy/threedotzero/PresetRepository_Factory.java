package com.portfolio.platform.data.legacy.threedotzero;

import com.mapped.U04;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PresetRepository_Factory implements Factory<PresetRepository> {
    @DexIgnore
    public /* final */ Provider<U04> appExecutorsProvider;
    @DexIgnore
    public /* final */ Provider<PresetDataSource> mappingLocalSetDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<PresetDataSource> mappingRemoteSetDataSourceProvider;

    @DexIgnore
    public PresetRepository_Factory(Provider<PresetDataSource> provider, Provider<PresetDataSource> provider2, Provider<U04> provider3) {
        this.mappingRemoteSetDataSourceProvider = provider;
        this.mappingLocalSetDataSourceProvider = provider2;
        this.appExecutorsProvider = provider3;
    }

    @DexIgnore
    public static PresetRepository_Factory create(Provider<PresetDataSource> provider, Provider<PresetDataSource> provider2, Provider<U04> provider3) {
        return new PresetRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static PresetRepository newInstance(PresetDataSource presetDataSource, PresetDataSource presetDataSource2, U04 u04) {
        return new PresetRepository(presetDataSource, presetDataSource2, u04);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public PresetRepository get() {
        return newInstance(this.mappingRemoteSetDataSourceProvider.get(), this.mappingLocalSetDataSourceProvider.get(), this.appExecutorsProvider.get());
    }
}
