package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.Mq5;
import com.fossil.Q88;
import com.mapped.Dx6;
import com.mapped.Fu3;
import com.mapped.Ku3;
import com.mapped.U04;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.PresetDataSource;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class PresetRemoteDataSource extends PresetDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "PresetRemoteDataSource";
    @DexIgnore
    public /* final */ ShortcutApiService mApiService;
    @DexIgnore
    public /* final */ U04 mAppExecutors;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 implements Dx6<Ku3> {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.GetSavedPresetListCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ List val$savedPresets;

        @DexIgnore
        public Anon1(List list, PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
            this.val$savedPresets = list;
            this.val$callback = getSavedPresetListCallback;
        }

        @DexIgnore
        @Override // com.mapped.Dx6
        public void onFailure(Call<Ku3> call, Throwable th) {
            MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onFailure");
            if (!this.val$savedPresets.isEmpty()) {
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onFailure presetList not null");
                PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback = this.val$callback;
                if (getSavedPresetListCallback != null) {
                    getSavedPresetListCallback.onSuccess(this.val$savedPresets);
                    return;
                }
                return;
            }
            MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onFailure presetList is null");
            PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback2 = this.val$callback;
            if (getSavedPresetListCallback2 != null) {
                getSavedPresetListCallback2.onFail();
            }
        }

        @DexIgnore
        @Override // com.mapped.Dx6
        public void onResponse(Call<Ku3> call, Q88<Ku3> q88) {
            if (!q88.e() || q88.a() == null) {
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList !isSuccessful");
                if (!this.val$savedPresets.isEmpty()) {
                    MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList !isSuccessful presetList not null");
                    PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback = this.val$callback;
                    if (getSavedPresetListCallback != null) {
                        getSavedPresetListCallback.onSuccess(this.val$savedPresets);
                        return;
                    }
                    return;
                }
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList !isSuccessful presetList is null");
                PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback2 = this.val$callback;
                if (getSavedPresetListCallback2 != null) {
                    getSavedPresetListCallback2.onFail();
                    return;
                }
                return;
            }
            Mq5 mq5 = new Mq5();
            mq5.c(q88.a());
            this.val$savedPresets.addAll(mq5.b());
            Range a2 = mq5.a();
            if (a2 != null && a2.isHasNext()) {
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onSuccess hasNext=true");
                PresetRemoteDataSource.this.getUserPresets(a2.getOffset() + a2.getLimit() + 1, a2.getLimit(), this);
            } else if (this.val$callback != null) {
                MFLogger.d(PresetRemoteDataSource.TAG, "getSavedPresetList onSuccess hasNext=false");
                if (!this.val$savedPresets.isEmpty()) {
                    this.val$callback.onSuccess(this.val$savedPresets);
                } else {
                    this.val$callback.onFail();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 implements PresetDataSource.AddOrUpdateSavedPresetListCallback {
        @DexIgnore
        public /* final */ /* synthetic */ PresetDataSource.AddOrUpdateSavedPresetCallback val$callback;
        @DexIgnore
        public /* final */ /* synthetic */ SavedPreset val$savedPreset;

        @DexIgnore
        public Anon2(SavedPreset savedPreset, PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback) {
            this.val$savedPreset = savedPreset;
            this.val$callback = addOrUpdateSavedPresetCallback;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetListCallback
        public void onFail() {
            String str = PresetRemoteDataSource.TAG;
            MFLogger.d(str, "addOrUpdateSavedPreset onFail mappingSetId=" + this.val$savedPreset.getId() + " mappingSetName=" + this.val$savedPreset.getName());
            PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback = this.val$callback;
            if (addOrUpdateSavedPresetCallback != null) {
                addOrUpdateSavedPresetCallback.onFail();
            }
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource.AddOrUpdateSavedPresetListCallback
        public void onSuccess(List<SavedPreset> list) {
            String str = PresetRemoteDataSource.TAG;
            MFLogger.d(str, "addOrUpdateSavedPreset onSuccess mappingSetId=" + this.val$savedPreset.getId() + " mappingSetName=" + this.val$savedPreset.getName());
            if (!list.isEmpty()) {
                PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback = this.val$callback;
                if (addOrUpdateSavedPresetCallback != null) {
                    addOrUpdateSavedPresetCallback.onSuccess(list.get(0));
                    return;
                }
                return;
            }
            PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback2 = this.val$callback;
            if (addOrUpdateSavedPresetCallback2 != null) {
                addOrUpdateSavedPresetCallback2.onFail();
            }
        }
    }

    @DexIgnore
    public PresetRemoteDataSource(ShortcutApiService shortcutApiService, U04 u04) {
        this.mApiService = shortcutApiService;
        this.mAppExecutors = u04;
    }

    @DexIgnore
    private Ku3 createListItemJsonObject(Fu3 fu3) {
        Ku3 ku3 = new Ku3();
        ku3.k(CloudLogWriter.ITEMS_PARAM, fu3);
        return ku3;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void addOrUpdateActivePreset(ActivePreset activePreset, PresetDataSource.AddOrUpdateActivePresetCallback addOrUpdateActivePresetCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateActivePreset serialNumber=" + activePreset.getSerialNumber() + " originalId=" + activePreset.getOriginalId());
        Fu3 fu3 = new Fu3();
        fu3.k(activePreset.getJsonObject());
        Ku3 createListItemJsonObject = createListItemJsonObject(fu3);
        String str2 = TAG;
        MFLogger.d(str2, "initJsonData - json: " + createListItemJsonObject);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void addOrUpdateSavedPreset(SavedPreset savedPreset, PresetDataSource.AddOrUpdateSavedPresetCallback addOrUpdateSavedPresetCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateSavedPreset mappingSetId=" + savedPreset.getId() + " mappingSetName=" + savedPreset.getName());
        ArrayList arrayList = new ArrayList();
        arrayList.add(savedPreset);
        addOrUpdateSavedPresetList(arrayList, new Anon2(savedPreset, addOrUpdateSavedPresetCallback));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void addOrUpdateSavedPresetList(List<SavedPreset> list, PresetDataSource.AddOrUpdateSavedPresetListCallback addOrUpdateSavedPresetListCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateSavedPreset presetListSize=" + list.size());
        Fu3 fu3 = new Fu3();
        for (SavedPreset savedPreset : list) {
            fu3.k(savedPreset.getJsonObject());
        }
        Ku3 createListItemJsonObject = createListItemJsonObject(fu3);
        String str2 = TAG;
        MFLogger.d(str2, "initJsonData - json: " + createListItemJsonObject);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void clearData() {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void deleteSavedPresetList(List<SavedPreset> list, PresetDataSource.DeleteMappingSetCallback deleteMappingSetCallback) {
        String str = TAG;
        MFLogger.d(str, "deleteSavedPresetList presetListSize=" + list.size());
        Fu3 fu3 = new Fu3();
        try {
            for (SavedPreset savedPreset : list) {
                Ku3 ku3 = new Ku3();
                ku3.n("id", savedPreset.getId());
                fu3.k(ku3);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        createListItemJsonObject(fu3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void downloadActivePresetList(PresetDataSource.GetActivePresetListCallback getActivePresetListCallback) {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getDefaultPreset(String str, PresetDataSource.GetRecommendedPresetCallback getRecommendedPresetCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getDefaultPreset deviceSerial=" + str);
        new ArrayList();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getRecommendedPresets(String str, PresetDataSource.GetRecommendedPresetListCallback getRecommendedPresetListCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getRecommendedPresets deviceSerial=" + str);
        new ArrayList();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.PresetDataSource
    public void getSavedPresetList(PresetDataSource.GetSavedPresetListCallback getSavedPresetListCallback) {
        MFLogger.d(TAG, "getSavedPresetList");
        getUserPresets(0, 100, new Anon1(new ArrayList(), getSavedPresetListCallback));
    }

    @DexIgnore
    public void getUserPresets(int i, int i2, Dx6<Ku3> dx6) {
    }
}
