package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.Q88;
import com.mapped.Cd6;
import com.mapped.Dx6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.List;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRemoteDataSource$getMicroAppGallery$Anon1 implements Dx6<ApiResponse<MicroApp>> {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppGalleryDataSource.GetMicroAppGalleryCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceSerial;

    @DexIgnore
    public MicroAppGalleryRemoteDataSource$getMicroAppGallery$Anon1(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        this.$deviceSerial = str;
        this.$callback = getMicroAppGalleryCallback;
    }

    @DexIgnore
    @Override // com.mapped.Dx6
    public void onFailure(Call<ApiResponse<MicroApp>> call, Throwable th) {
        Wg6.c(call, "call");
        Wg6.c(th, "throwable");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = MicroAppGalleryRemoteDataSource.Companion.getTAG();
        StringBuilder sb = new StringBuilder();
        sb.append("getMicroAppGallery deviceSerial=");
        sb.append(this.$deviceSerial);
        sb.append(" onFailure ex=");
        th.printStackTrace();
        sb.append(Cd6.a);
        local.d(tag, sb.toString());
        MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onFail();
        }
    }

    @DexIgnore
    @Override // com.mapped.Dx6
    public void onResponse(Call<ApiResponse<MicroApp>> call, Q88<ApiResponse<MicroApp>> q88) {
        Wg6.c(call, "call");
        Wg6.c(q88, "response");
        if (q88.e()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = MicroAppGalleryRemoteDataSource.Companion.getTAG();
            local.d(tag, "getMicroAppGallery deviceSerial=" + this.$deviceSerial + " onSuccess response=" + q88.a());
            ApiResponse<MicroApp> a2 = q88.a();
            List<MicroApp> list = a2 != null ? a2.get_items() : null;
            if (list == null || !(!list.isEmpty())) {
                MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
                if (getMicroAppGalleryCallback != null) {
                    getMicroAppGalleryCallback.onFail();
                    return;
                }
                return;
            }
            MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback2 = this.$callback;
            if (getMicroAppGalleryCallback2 != null) {
                getMicroAppGalleryCallback2.onSuccess(list);
                return;
            }
            return;
        }
        MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback3 = this.$callback;
        if (getMicroAppGalleryCallback3 != null) {
            getMicroAppGalleryCallback3.onFail();
        }
    }
}
