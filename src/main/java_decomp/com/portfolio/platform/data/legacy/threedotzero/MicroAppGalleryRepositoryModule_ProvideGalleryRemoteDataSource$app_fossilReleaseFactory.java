package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.Lk7;
import com.mapped.U04;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRepositoryModule_ProvideGalleryRemoteDataSource$app_fossilReleaseFactory implements Factory<MicroAppGalleryDataSource> {
    @DexIgnore
    public /* final */ Provider<U04> appExecutorsProvider;
    @DexIgnore
    public /* final */ MicroAppGalleryRepositoryModule module;
    @DexIgnore
    public /* final */ Provider<ShortcutApiService> shortcutApiServiceProvider;

    @DexIgnore
    public MicroAppGalleryRepositoryModule_ProvideGalleryRemoteDataSource$app_fossilReleaseFactory(MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule, Provider<ShortcutApiService> provider, Provider<U04> provider2) {
        this.module = microAppGalleryRepositoryModule;
        this.shortcutApiServiceProvider = provider;
        this.appExecutorsProvider = provider2;
    }

    @DexIgnore
    public static MicroAppGalleryRepositoryModule_ProvideGalleryRemoteDataSource$app_fossilReleaseFactory create(MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule, Provider<ShortcutApiService> provider, Provider<U04> provider2) {
        return new MicroAppGalleryRepositoryModule_ProvideGalleryRemoteDataSource$app_fossilReleaseFactory(microAppGalleryRepositoryModule, provider, provider2);
    }

    @DexIgnore
    public static MicroAppGalleryDataSource provideGalleryRemoteDataSource$app_fossilRelease(MicroAppGalleryRepositoryModule microAppGalleryRepositoryModule, ShortcutApiService shortcutApiService, U04 u04) {
        MicroAppGalleryDataSource provideGalleryRemoteDataSource$app_fossilRelease = microAppGalleryRepositoryModule.provideGalleryRemoteDataSource$app_fossilRelease(shortcutApiService, u04);
        Lk7.c(provideGalleryRemoteDataSource$app_fossilRelease, "Cannot return null from a non-@Nullable @Provides method");
        return provideGalleryRemoteDataSource$app_fossilRelease;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public MicroAppGalleryDataSource get() {
        return provideGalleryRemoteDataSource$app_fossilRelease(this.module, this.shortcutApiServiceProvider.get(), this.appExecutorsProvider.get());
    }
}
