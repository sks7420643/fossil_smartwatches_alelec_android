package com.portfolio.platform.data;

import com.facebook.places.PlaceManager;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.sina.weibo.sdk.statistic.LogBuilder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerGpsPoint {
    @DexIgnore
    @Vu3(PlaceManager.PARAM_ALTITUDE)
    public /* final */ Double altitude;
    @DexIgnore
    @Vu3(PlaceManager.PARAM_HEADING)
    public /* final */ Double heading;
    @DexIgnore
    @Vu3("horizontalAccuracy")
    public /* final */ Float horizontalAccuracy;
    @DexIgnore
    @Vu3("latitude")
    public double latitude;
    @DexIgnore
    @Vu3("longitude")
    public /* final */ double longitude;
    @DexIgnore
    @Vu3(PlaceManager.PARAM_SPEED)
    public /* final */ Double speed;
    @DexIgnore
    @Vu3("at")
    public /* final */ String time;
    @DexIgnore
    @Vu3("verticalAccuracy")
    public /* final */ Float verticalAccuracy;

    @DexIgnore
    public ServerGpsPoint(Float f, double d, double d2, String str, Float f2, Double d3, Double d4, Double d5) {
        Wg6.c(str, LogBuilder.KEY_TIME);
        this.horizontalAccuracy = f;
        this.latitude = d;
        this.longitude = d2;
        this.time = str;
        this.verticalAccuracy = f2;
        this.altitude = d3;
        this.speed = d4;
        this.heading = d5;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ServerGpsPoint(Float f, double d, double d2, String str, Float f2, Double d3, Double d4, Double d5, int i, Qg6 qg6) {
        this((i & 1) != 0 ? null : f, d, d2, str, (i & 16) != 0 ? null : f2, (i & 32) != 0 ? null : d3, (i & 64) != 0 ? null : d4, (i & 128) != 0 ? null : d5);
    }

    @DexIgnore
    public static /* synthetic */ ServerGpsPoint copy$default(ServerGpsPoint serverGpsPoint, Float f, double d, double d2, String str, Float f2, Double d3, Double d4, Double d5, int i, Object obj) {
        return serverGpsPoint.copy((i & 1) != 0 ? serverGpsPoint.horizontalAccuracy : f, (i & 2) != 0 ? serverGpsPoint.latitude : d, (i & 4) != 0 ? serverGpsPoint.longitude : d2, (i & 8) != 0 ? serverGpsPoint.time : str, (i & 16) != 0 ? serverGpsPoint.verticalAccuracy : f2, (i & 32) != 0 ? serverGpsPoint.altitude : d3, (i & 64) != 0 ? serverGpsPoint.speed : d4, (i & 128) != 0 ? serverGpsPoint.heading : d5);
    }

    @DexIgnore
    public final Float component1() {
        return this.horizontalAccuracy;
    }

    @DexIgnore
    public final double component2() {
        return this.latitude;
    }

    @DexIgnore
    public final double component3() {
        return this.longitude;
    }

    @DexIgnore
    public final String component4() {
        return this.time;
    }

    @DexIgnore
    public final Float component5() {
        return this.verticalAccuracy;
    }

    @DexIgnore
    public final Double component6() {
        return this.altitude;
    }

    @DexIgnore
    public final Double component7() {
        return this.speed;
    }

    @DexIgnore
    public final Double component8() {
        return this.heading;
    }

    @DexIgnore
    public final ServerGpsPoint copy(Float f, double d, double d2, String str, Float f2, Double d3, Double d4, Double d5) {
        Wg6.c(str, LogBuilder.KEY_TIME);
        return new ServerGpsPoint(f, d, d2, str, f2, d3, d4, d5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ServerGpsPoint) {
                ServerGpsPoint serverGpsPoint = (ServerGpsPoint) obj;
                if (!Wg6.a(this.horizontalAccuracy, serverGpsPoint.horizontalAccuracy) || Double.compare(this.latitude, serverGpsPoint.latitude) != 0 || Double.compare(this.longitude, serverGpsPoint.longitude) != 0 || !Wg6.a(this.time, serverGpsPoint.time) || !Wg6.a(this.verticalAccuracy, serverGpsPoint.verticalAccuracy) || !Wg6.a(this.altitude, serverGpsPoint.altitude) || !Wg6.a(this.speed, serverGpsPoint.speed) || !Wg6.a(this.heading, serverGpsPoint.heading)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Double getAltitude() {
        return this.altitude;
    }

    @DexIgnore
    public final Double getHeading() {
        return this.heading;
    }

    @DexIgnore
    public final Float getHorizontalAccuracy() {
        return this.horizontalAccuracy;
    }

    @DexIgnore
    public final double getLatitude() {
        return this.latitude;
    }

    @DexIgnore
    public final double getLongitude() {
        return this.longitude;
    }

    @DexIgnore
    public final Double getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public final String getTime() {
        return this.time;
    }

    @DexIgnore
    public final Float getVerticalAccuracy() {
        return this.verticalAccuracy;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Float f = this.horizontalAccuracy;
        int hashCode = f != null ? f.hashCode() : 0;
        int doubleToLongBits = Double.doubleToLongBits(this.latitude);
        int doubleToLongBits2 = Double.doubleToLongBits(this.longitude);
        String str = this.time;
        int hashCode2 = str != null ? str.hashCode() : 0;
        Float f2 = this.verticalAccuracy;
        int hashCode3 = f2 != null ? f2.hashCode() : 0;
        Double d = this.altitude;
        int hashCode4 = d != null ? d.hashCode() : 0;
        Double d2 = this.speed;
        int hashCode5 = d2 != null ? d2.hashCode() : 0;
        Double d3 = this.heading;
        if (d3 != null) {
            i = d3.hashCode();
        }
        return (((((((((((((hashCode * 31) + doubleToLongBits) * 31) + doubleToLongBits2) * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + i;
    }

    @DexIgnore
    public final void setLatitude(double d) {
        this.latitude = d;
    }

    @DexIgnore
    public String toString() {
        return "ServerGpsPoint(horizontalAccuracy=" + this.horizontalAccuracy + ", latitude=" + this.latitude + ", longitude=" + this.longitude + ", time=" + this.time + ", verticalAccuracy=" + this.verticalAccuracy + ", altitude=" + this.altitude + ", speed=" + this.speed + ", heading=" + this.heading + ")";
    }
}
