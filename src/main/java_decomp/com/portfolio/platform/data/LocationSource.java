package com.portfolio.platform.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.os.Looper;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Bw7;
import com.fossil.Dl7;
import com.fossil.Go7;
import com.fossil.Gu7;
import com.fossil.Ha3;
import com.fossil.Ia3;
import com.fossil.Jv7;
import com.fossil.Lu7;
import com.fossil.Xn7;
import com.fossil.Yn7;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.mapped.Jh6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.mapped.Wv2;
import com.mapped.Xe6;
import com.mapped.Yv2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.portfolio.platform.data.model.microapp.weather.AddressOfWeather;
import com.portfolio.platform.data.source.local.AddressDao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocationSource {
    @DexIgnore
    public static /* final */ int CACHE_THRESHOLD_TIME; // = 60000;
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ long LOCATION_WAITING_THRESHOLD_TIME; // = 5000;
    @DexIgnore
    public static /* final */ float SIGNIFICANT_DISPLACEMENT; // = 300.0f;
    @DexIgnore
    public static /* final */ float SMALLEST_DISPLACEMENT; // = 50.0f;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ int TWO_MINUTES; // = 120000;
    @DexIgnore
    public AddressDao addressDao;
    @DexIgnore
    public boolean isMonitoringObserved;
    @DexIgnore
    public boolean isSignificantObserved;
    @DexIgnore
    public long lastRetrievedTime; // = -1;
    @DexIgnore
    public Location locationCache;
    @DexIgnore
    public MonitoringLocationCallback mMonitoringCallback; // = new MonitoringLocationCallback();
    @DexIgnore
    public Wv2 mMonitoringClient;
    @DexIgnore
    public List<LocationListener> mMonitoringLocationListeners; // = new ArrayList();
    @DexIgnore
    public SignificantLocationCallback mSignificantCallback; // = new SignificantLocationCallback();
    @DexIgnore
    public Wv2 mSignificantClient;
    @DexIgnore
    public float mSignificantDistance; // = 300.0f;
    @DexIgnore
    public List<LocationListener> mSignificantLocationListeners; // = new ArrayList();
    @DexIgnore
    public float mSmallestDisplacement; // = 50.0f;
    @DexIgnore
    public int mTimeInterval; // = 120000;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return LocationSource.TAG;
        }
    }

    @DexIgnore
    public enum ErrorState {
        SUCCESS,
        LOCATION_PERMISSION_OFF,
        BACKGROUND_PERMISSION_OFF,
        LOCATION_SERVICE_OFF,
        UNKNOWN
    }

    @DexIgnore
    public interface LocationListener {
        @DexIgnore
        void onLocationResult(Location location);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class MonitoringLocationCallback extends Yv2 {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public MonitoringLocationCallback() {
        }

        @DexIgnore
        @Override // com.mapped.Yv2
        public void onLocationAvailability(LocationAvailability locationAvailability) {
            Wg6.c(locationAvailability, "locationAvailability");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "onLocationAvailability isAvailable " + locationAvailability.c());
        }

        @DexIgnore
        @Override // com.mapped.Yv2
        public void onLocationResult(LocationResult locationResult) {
            Location c = locationResult != null ? locationResult.c() : null;
            if (c != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
                local.d(tAG$app_fossilRelease, "MonitoringLocationCallback onLocationResult lastLocation=" + c);
                LocationSource locationSource = LocationSource.this;
                Calendar instance = Calendar.getInstance();
                Wg6.b(instance, "Calendar.getInstance()");
                locationSource.lastRetrievedTime = instance.getTimeInMillis();
                LocationSource.this.locationCache = c;
                for (LocationListener locationListener : LocationSource.this.mMonitoringLocationListeners) {
                    if (locationListener != null) {
                        locationListener.onLocationResult(c);
                    }
                }
                return;
            }
            FLogger.INSTANCE.getLocal().d(LocationSource.Companion.getTAG$app_fossilRelease(), "MonitoringLocationCallback onLocationResult lastLocation is null");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Result {
        @DexIgnore
        public /* final */ ErrorState errorState;
        @DexIgnore
        public /* final */ boolean fromCache;
        @DexIgnore
        public /* final */ Location location;

        @DexIgnore
        public Result(Location location2, ErrorState errorState2, boolean z) {
            Wg6.c(errorState2, "errorState");
            this.location = location2;
            this.errorState = errorState2;
            this.fromCache = z;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Result(Location location2, ErrorState errorState2, boolean z, int i, Qg6 qg6) {
            this(location2, errorState2, (i & 4) != 0 ? false : z);
        }

        @DexIgnore
        public final ErrorState getErrorState() {
            return this.errorState;
        }

        @DexIgnore
        public final boolean getFromCache() {
            return this.fromCache;
        }

        @DexIgnore
        public final Location getLocation() {
            return this.location;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SignificantLocationCallback extends Yv2 {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SignificantLocationCallback() {
        }

        @DexIgnore
        @Override // com.mapped.Yv2
        public void onLocationResult(LocationResult locationResult) {
            Location c = locationResult != null ? locationResult.c() : null;
            if (c != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
                local.d(tAG$app_fossilRelease, "SignificantLocationCallback onLocationResult lastLocation=" + c);
                LocationSource locationSource = LocationSource.this;
                Calendar instance = Calendar.getInstance();
                Wg6.b(instance, "Calendar.getInstance()");
                locationSource.lastRetrievedTime = instance.getTimeInMillis();
                LocationSource.this.locationCache = c;
                for (LocationListener locationListener : LocationSource.this.mSignificantLocationListeners) {
                    if (locationListener != null) {
                        locationListener.onLocationResult(c);
                    }
                }
                return;
            }
            FLogger.INSTANCE.getLocal().d(LocationSource.Companion.getTAG$app_fossilRelease(), "SignificantLocationCallback onLocationResult lastLocation is null");
        }
    }

    /*
    static {
        String simpleName = LocationSource.class.getSimpleName();
        Wg6.b(simpleName, "LocationSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public static /* synthetic */ Object getLocation$default(LocationSource locationSource, Context context, boolean z, float f, float f2, int i, Xe6 xe6, int i2, Object obj) {
        return locationSource.getLocation(context, z, (i2 & 4) != 0 ? 50.0f : f, (i2 & 8) != 0 ? 300.0f : f2, (i2 & 16) != 0 ? 120000 : i, xe6);
    }

    @DexIgnore
    private final Location isBetterLocation(Location location, Location location2) {
        boolean z = true;
        if (location == null && location2 == null) {
            FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation both newLocation null and currentBestLocation null");
            return null;
        } else if (location != null && location2 != null) {
            FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation newLocation long=" + location.getLongitude() + " lat=" + location.getLatitude() + " time=" + location.getTime());
            FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation currentBestLocation long=" + location2.getLongitude() + " lat=" + location2.getLatitude() + " time=" + location2.getTime());
            long time = location.getTime() - location2.getTime();
            boolean z2 = time > ((long) this.mTimeInterval);
            boolean z3 = time < ((long) (-this.mTimeInterval));
            boolean z4 = time > 0;
            if (z2) {
                FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isSignificantlyNewer");
                return location;
            } else if (z3) {
                FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isSignificantlyOlder");
                return location2;
            } else {
                FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation accuracy location=" + location.getAccuracy() + " currentBestLocation=" + location2.getAccuracy());
                int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
                boolean z5 = accuracy > 0;
                boolean z6 = accuracy < 0;
                if (accuracy <= 200) {
                    z = false;
                }
                boolean isSameProvider = isSameProvider(location.getProvider(), location2.getProvider());
                if (z6) {
                    FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isMoreAccurate");
                    return location;
                } else if (z4 && !z5) {
                    FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isNewer && isLessAccurate=false");
                    return location;
                } else if (!z4 || z || !isSameProvider) {
                    return location2;
                } else {
                    FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation isNewer && isSignificantlyLessAccurate=false && isFromSameProvider");
                    return location;
                }
            }
        } else if (location != null) {
            FLogger.INSTANCE.getLocal().d(TAG, "isBetterLocation bestLocation long=" + location.getLongitude() + " lat=" + location.getLatitude() + " time=" + location.getTime());
            return location;
        } else {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("isBetterLocation bestLocation long=");
            if (location2 != null) {
                sb.append(location2.getLongitude());
                sb.append(" lat=");
                sb.append(location2.getLatitude());
                sb.append(" time=");
                sb.append(location2.getTime());
                local.d(str, sb.toString());
                return location2;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    private final boolean isSameProvider(String str, String str2) {
        FLogger.INSTANCE.getLocal().d(TAG, "isSameProvider");
        return str == null ? str2 == null : Wg6.a(str, str2);
    }

    @DexIgnore
    public static /* synthetic */ void observerLocation$default(LocationSource locationSource, Context context, LocationListener locationListener, boolean z, float f, int i, Object obj) {
        if ((i & 8) != 0) {
            f = 50.0f;
        }
        locationSource.observerLocation(context, locationListener, z, f);
    }

    @DexIgnore
    public final AddressDao getAddressDao$app_fossilRelease() {
        AddressDao addressDao2 = this.addressDao;
        if (addressDao2 != null) {
            return addressDao2;
        }
        Wg6.n("addressDao");
        throw null;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:20:0x009d */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0074, code lost:
        if (r4.locationCache != null) goto L_0x0076;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0024  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x014b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getLocation(android.content.Context r11, boolean r12, float r13, float r14, int r15, com.mapped.Xe6<? super com.portfolio.platform.data.LocationSource.Result> r16) {
        /*
        // Method dump skipped, instructions count: 344
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.LocationSource.getLocation(android.content.Context, boolean, float, float, int, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object getLocationFromFuseLocationManager(Context context, Xe6<? super Result> xe6) {
        Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
        FLogger.INSTANCE.getLocal().d(Companion.getTAG$app_fossilRelease(), "getLocationFromFuseLocationManager");
        Jh6 jh6 = new Jh6();
        jh6.element = null;
        Wv2 a2 = Ha3.a(context);
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.A(0);
        locationRequest.k(0);
        locationRequest.F(100);
        locationRequest.L(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        new Ia3.Ai().a(locationRequest);
        if (W6.a(context, "android.permission.ACCESS_FINE_LOCATION") != 0 || W6.a(context, "android.permission.ACCESS_COARSE_LOCATION") != 0) {
            Result result = new Result(null, ErrorState.LOCATION_PERMISSION_OFF, false, 4, null);
            Dl7.Ai ai = Dl7.Companion;
            lu7.resumeWith(Dl7.constructor-impl(result));
        } else if (Build.VERSION.SDK_INT >= 29 && W6.a(context, "android.permission.ACCESS_BACKGROUND_LOCATION") != 0) {
            Result result2 = new Result(null, ErrorState.BACKGROUND_PERMISSION_OFF, false, 4, null);
            Dl7.Ai ai2 = Dl7.Companion;
            lu7.resumeWith(Dl7.constructor-impl(result2));
        } else if (!LocationUtils.isLocationEnable(context)) {
            Result result3 = new Result(null, ErrorState.LOCATION_SERVICE_OFF, false, 4, null);
            Dl7.Ai ai3 = Dl7.Companion;
            lu7.resumeWith(Dl7.constructor-impl(result3));
        } else {
            try {
                Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1(jh6, a2, locationRequest, lu7, null, this, context), 3, null);
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = Companion.getTAG$app_fossilRelease();
                local.e(tAG$app_fossilRelease, ".getLocationFromFuseLocationManager(), e1=" + e);
            }
        }
        Object t = lu7.t();
        if (t == Yn7.d()) {
            Go7.c(xe6);
        }
        return t;
    }

    @DexIgnore
    public final List<AddressOfWeather> getWeatherAddressList() {
        AddressDao addressDao2 = this.addressDao;
        if (addressDao2 != null) {
            return addressDao2.getAllSavedAddress();
        }
        Wg6.n("addressDao");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0021 A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean isObservingLocation(com.portfolio.platform.data.LocationSource.LocationListener r3, boolean r4) {
        /*
            r2 = this;
            java.lang.String r0 = "listener"
            com.mapped.Wg6.c(r3, r0)
            r0 = 1
            if (r4 == 0) goto L_0x0015
            boolean r1 = r2.isSignificantObserved
            if (r1 == 0) goto L_0x0021
            java.util.List<com.portfolio.platform.data.LocationSource$LocationListener> r1 = r2.mSignificantLocationListeners
            boolean r1 = r1.contains(r3)
            if (r1 == 0) goto L_0x0021
        L_0x0014:
            return r0
        L_0x0015:
            boolean r1 = r2.isMonitoringObserved
            if (r1 == 0) goto L_0x0021
            java.util.List<com.portfolio.platform.data.LocationSource$LocationListener> r1 = r2.mMonitoringLocationListeners
            boolean r1 = r1.contains(r3)
            if (r1 != 0) goto L_0x0014
        L_0x0021:
            r0 = 0
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.LocationSource.isObservingLocation(com.portfolio.platform.data.LocationSource$LocationListener, boolean):boolean");
    }

    @DexIgnore
    public final void observerLocation(Context context, LocationListener locationListener, boolean z, float f) {
        Wg6.c(context, "context");
        Wg6.c(locationListener, "listener");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "observeLocation listener " + locationListener + ", isSignificant " + z + " isSignificantObserved " + this.isSignificantObserved + " isMonitoringObserved " + this.isMonitoringObserved + " smallestDisplacement " + f);
        if (!z || !this.isSignificantObserved) {
            if (z || !this.isMonitoringObserved) {
                Wv2 a2 = Ha3.a(context);
                LocationRequest locationRequest = new LocationRequest();
                locationRequest.A(1000);
                locationRequest.k(1000);
                locationRequest.F(100);
                if (z) {
                    f = this.mSignificantDistance;
                }
                locationRequest.L(f);
                new Ia3.Ai().a(locationRequest);
                if (W6.a(context, "android.permission.ACCESS_FINE_LOCATION") != 0 || W6.a(context, "android.permission.ACCESS_COARSE_LOCATION") != 0 || (Build.VERSION.SDK_INT >= 29 && W6.a(context, "android.permission.ACCESS_BACKGROUND_LOCATION") != 0)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "observerLocation permission missing");
                } else if (!LocationUtils.isLocationEnable(context)) {
                    FLogger.INSTANCE.getLocal().d(TAG, "observerLocation location disable");
                } else if (z) {
                    try {
                        FLogger.INSTANCE.getLocal().d(TAG, "observerLocation start significantObserved");
                        this.mSignificantLocationListeners.add(locationListener);
                        a2.u(locationRequest, this.mSignificantCallback, Looper.getMainLooper());
                        this.isSignificantObserved = true;
                        this.mSignificantClient = a2;
                    } catch (Exception e) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        local2.e(str2, ".getLocationFromFuseLocationManager(), e1=" + e);
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(TAG, "observerLocation start monitoringObserved");
                    this.mMonitoringLocationListeners.add(locationListener);
                    a2.u(locationRequest, this.mMonitoringCallback, Looper.getMainLooper());
                    this.isMonitoringObserved = true;
                    this.mMonitoringClient = a2;
                }
            } else if (!this.mMonitoringLocationListeners.contains(locationListener)) {
                this.mMonitoringLocationListeners.add(locationListener);
            }
        } else if (!this.mSignificantLocationListeners.contains(locationListener)) {
            this.mSignificantLocationListeners.add(locationListener);
        }
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public final Object requestLocationUpdates(Wv2 wv2, LocationRequest locationRequest, Xe6<? super Location> xe6) {
        Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
        wv2.u(locationRequest, new LocationSource$requestLocationUpdates$$inlined$suspendCancellableCoroutine$lambda$Anon1(lu7, wv2, locationRequest), Looper.getMainLooper());
        Object t = lu7.t();
        if (t == Yn7.d()) {
            Go7.c(xe6);
        }
        return t;
    }

    @DexIgnore
    public final void saveWeatherAddress(AddressOfWeather addressOfWeather) {
        Wg6.c(addressOfWeather, "addressOfWeather");
        AddressDao addressDao2 = this.addressDao;
        if (addressDao2 != null) {
            addressDao2.saveAddress(addressOfWeather);
        } else {
            Wg6.n("addressDao");
            throw null;
        }
    }

    @DexIgnore
    public final void setAddressDao$app_fossilRelease(AddressDao addressDao2) {
        Wg6.c(addressDao2, "<set-?>");
        this.addressDao = addressDao2;
    }

    @DexIgnore
    public final void unObserverLocation(LocationListener locationListener, boolean z) {
        Wg6.c(locationListener, "listener");
        if (z) {
            this.mSignificantLocationListeners.remove(locationListener);
            if (this.isSignificantObserved && this.mSignificantLocationListeners.isEmpty()) {
                FLogger.INSTANCE.getLocal().d(TAG, "observerLocation end significantObserved");
                Wv2 wv2 = this.mSignificantClient;
                if (wv2 != null) {
                    wv2.t(this.mSignificantCallback);
                }
                this.isSignificantObserved = false;
                return;
            }
            return;
        }
        this.mMonitoringLocationListeners.remove(locationListener);
        if (this.isMonitoringObserved && this.mMonitoringLocationListeners.isEmpty()) {
            FLogger.INSTANCE.getLocal().d(TAG, "observerLocation end monitoringObserved");
            Wv2 wv22 = this.mMonitoringClient;
            if (wv22 != null) {
                wv22.t(this.mMonitoringCallback);
            }
            this.isMonitoringObserved = false;
        }
    }
}
