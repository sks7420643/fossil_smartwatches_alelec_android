package com.portfolio.platform.data;

import com.facebook.appevents.UserDataStore;
import com.fossil.Qh5;
import com.mapped.Vu3;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WechatUserinfo {
    @DexIgnore
    @Vu3("city")
    public /* final */ String mCity;
    @DexIgnore
    @Vu3(UserDataStore.COUNTRY)
    public /* final */ String mCountry;
    @DexIgnore
    @Vu3("headimgurl")
    public /* final */ String mHeadimgurl;
    @DexIgnore
    @Vu3("nickname")
    public /* final */ String mNickname;
    @DexIgnore
    @Vu3("openid")
    public /* final */ String mOpenid;
    @DexIgnore
    @Vu3("privilege")
    public /* final */ List<String> mPrivilege;
    @DexIgnore
    @Vu3("province")
    public /* final */ String mProvince;
    @DexIgnore
    @Vu3("sex")
    public /* final */ int mSex;
    @DexIgnore
    @Vu3("unionId")
    public /* final */ String mUnionId;

    @DexIgnore
    public WechatUserinfo(String str, String str2, int i, String str3, String str4, String str5, String str6, List<String> list, String str7) {
        Wg6.c(str, "mOpenid");
        Wg6.c(str2, "mNickname");
        Wg6.c(str3, "mCity");
        Wg6.c(str4, "mProvince");
        Wg6.c(str5, "mCountry");
        Wg6.c(str6, "mHeadimgurl");
        Wg6.c(list, "mPrivilege");
        Wg6.c(str7, "mUnionId");
        this.mOpenid = str;
        this.mNickname = str2;
        this.mSex = i;
        this.mCity = str3;
        this.mProvince = str4;
        this.mCountry = str5;
        this.mHeadimgurl = str6;
        this.mPrivilege = list;
        this.mUnionId = str7;
    }

    @DexIgnore
    public final String getHeadimgurl() {
        return this.mHeadimgurl;
    }

    @DexIgnore
    public final String getSex() {
        int i = this.mSex;
        return i != 1 ? i != 2 ? Qh5.OTHER.toString() : Qh5.FEMALE.toString() : Qh5.MALE.toString();
    }
}
