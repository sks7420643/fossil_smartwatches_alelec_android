package com.portfolio.platform.data;

import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepStatistic {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String ID; // = "id";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "sleep_statistic";
    @DexIgnore
    @Vu3("createdAt")
    public /* final */ DateTime createdAt;
    @DexIgnore
    @Vu3("id")
    public /* final */ String id;
    @DexIgnore
    @Vu3("sleepTimeBestDay")
    public /* final */ SleepDailyBest sleepTimeBestDay;
    @DexIgnore
    @Vu3("sleepTimeBestStreak")
    public /* final */ SleepDailyBest sleepTimeBestStreak;
    @DexIgnore
    @Vu3("totalDays")
    public /* final */ int totalDays;
    @DexIgnore
    @Vu3("totalSleepMinutes")
    public /* final */ int totalSleepMinutes;
    @DexIgnore
    @Vu3("totalSleepStateDistInMinute")
    public /* final */ List<Integer> totalSleepStateDistInMinute;
    @DexIgnore
    @Vu3("totalSleeps")
    public /* final */ int totalSleeps;
    @DexIgnore
    @Vu3("uid")
    public /* final */ String uid;
    @DexIgnore
    @Vu3("updatedAt")
    public /* final */ DateTime updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class SleepDailyBest {
        @DexIgnore
        public /* final */ Date date;
        @DexIgnore
        public /* final */ String sleepDailySummaryId;
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public SleepDailyBest(String str, Date date2, int i) {
            Wg6.c(str, "sleepDailySummaryId");
            Wg6.c(date2, "date");
            this.sleepDailySummaryId = str;
            this.date = date2;
            this.value = i;
        }

        @DexIgnore
        public static /* synthetic */ SleepDailyBest copy$default(SleepDailyBest sleepDailyBest, String str, Date date2, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = sleepDailyBest.sleepDailySummaryId;
            }
            if ((i2 & 2) != 0) {
                date2 = sleepDailyBest.date;
            }
            if ((i2 & 4) != 0) {
                i = sleepDailyBest.value;
            }
            return sleepDailyBest.copy(str, date2, i);
        }

        @DexIgnore
        public final String component1() {
            return this.sleepDailySummaryId;
        }

        @DexIgnore
        public final Date component2() {
            return this.date;
        }

        @DexIgnore
        public final int component3() {
            return this.value;
        }

        @DexIgnore
        public final SleepDailyBest copy(String str, Date date2, int i) {
            Wg6.c(str, "sleepDailySummaryId");
            Wg6.c(date2, "date");
            return new SleepDailyBest(str, date2, i);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof SleepDailyBest) {
                    SleepDailyBest sleepDailyBest = (SleepDailyBest) obj;
                    if (!Wg6.a(this.sleepDailySummaryId, sleepDailyBest.sleepDailySummaryId) || !Wg6.a(this.date, sleepDailyBest.date) || this.value != sleepDailyBest.value) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final Date getDate() {
            return this.date;
        }

        @DexIgnore
        public final String getSleepDailySummaryId() {
            return this.sleepDailySummaryId;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.sleepDailySummaryId;
            int hashCode = str != null ? str.hashCode() : 0;
            Date date2 = this.date;
            if (date2 != null) {
                i = date2.hashCode();
            }
            return (((hashCode * 31) + i) * 31) + this.value;
        }

        @DexIgnore
        public String toString() {
            return "SleepDailyBest(sleepDailySummaryId=" + this.sleepDailySummaryId + ", date=" + this.date + ", value=" + this.value + ")";
        }
    }

    @DexIgnore
    public SleepStatistic(String str, String str2, SleepDailyBest sleepDailyBest, SleepDailyBest sleepDailyBest2, int i, int i2, int i3, List<Integer> list, DateTime dateTime, DateTime dateTime2) {
        Wg6.c(str, "id");
        Wg6.c(str2, "uid");
        Wg6.c(list, "totalSleepStateDistInMinute");
        Wg6.c(dateTime, "createdAt");
        Wg6.c(dateTime2, "updatedAt");
        this.id = str;
        this.uid = str2;
        this.sleepTimeBestDay = sleepDailyBest;
        this.sleepTimeBestStreak = sleepDailyBest2;
        this.totalDays = i;
        this.totalSleeps = i2;
        this.totalSleepMinutes = i3;
        this.totalSleepStateDistInMinute = list;
        this.createdAt = dateTime;
        this.updatedAt = dateTime2;
    }

    @DexIgnore
    public static /* synthetic */ SleepStatistic copy$default(SleepStatistic sleepStatistic, String str, String str2, SleepDailyBest sleepDailyBest, SleepDailyBest sleepDailyBest2, int i, int i2, int i3, List list, DateTime dateTime, DateTime dateTime2, int i4, Object obj) {
        return sleepStatistic.copy((i4 & 1) != 0 ? sleepStatistic.id : str, (i4 & 2) != 0 ? sleepStatistic.uid : str2, (i4 & 4) != 0 ? sleepStatistic.sleepTimeBestDay : sleepDailyBest, (i4 & 8) != 0 ? sleepStatistic.sleepTimeBestStreak : sleepDailyBest2, (i4 & 16) != 0 ? sleepStatistic.totalDays : i, (i4 & 32) != 0 ? sleepStatistic.totalSleeps : i2, (i4 & 64) != 0 ? sleepStatistic.totalSleepMinutes : i3, (i4 & 128) != 0 ? sleepStatistic.totalSleepStateDistInMinute : list, (i4 & 256) != 0 ? sleepStatistic.createdAt : dateTime, (i4 & 512) != 0 ? sleepStatistic.updatedAt : dateTime2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final DateTime component10() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String component2() {
        return this.uid;
    }

    @DexIgnore
    public final SleepDailyBest component3() {
        return this.sleepTimeBestDay;
    }

    @DexIgnore
    public final SleepDailyBest component4() {
        return this.sleepTimeBestStreak;
    }

    @DexIgnore
    public final int component5() {
        return this.totalDays;
    }

    @DexIgnore
    public final int component6() {
        return this.totalSleeps;
    }

    @DexIgnore
    public final int component7() {
        return this.totalSleepMinutes;
    }

    @DexIgnore
    public final List<Integer> component8() {
        return this.totalSleepStateDistInMinute;
    }

    @DexIgnore
    public final DateTime component9() {
        return this.createdAt;
    }

    @DexIgnore
    public final SleepStatistic copy(String str, String str2, SleepDailyBest sleepDailyBest, SleepDailyBest sleepDailyBest2, int i, int i2, int i3, List<Integer> list, DateTime dateTime, DateTime dateTime2) {
        Wg6.c(str, "id");
        Wg6.c(str2, "uid");
        Wg6.c(list, "totalSleepStateDistInMinute");
        Wg6.c(dateTime, "createdAt");
        Wg6.c(dateTime2, "updatedAt");
        return new SleepStatistic(str, str2, sleepDailyBest, sleepDailyBest2, i, i2, i3, list, dateTime, dateTime2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SleepStatistic) {
                SleepStatistic sleepStatistic = (SleepStatistic) obj;
                if (!Wg6.a(this.id, sleepStatistic.id) || !Wg6.a(this.uid, sleepStatistic.uid) || !Wg6.a(this.sleepTimeBestDay, sleepStatistic.sleepTimeBestDay) || !Wg6.a(this.sleepTimeBestStreak, sleepStatistic.sleepTimeBestStreak) || this.totalDays != sleepStatistic.totalDays || this.totalSleeps != sleepStatistic.totalSleeps || this.totalSleepMinutes != sleepStatistic.totalSleepMinutes || !Wg6.a(this.totalSleepStateDistInMinute, sleepStatistic.totalSleepStateDistInMinute) || !Wg6.a(this.createdAt, sleepStatistic.createdAt) || !Wg6.a(this.updatedAt, sleepStatistic.updatedAt)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final SleepDailyBest getSleepTimeBestDay() {
        return this.sleepTimeBestDay;
    }

    @DexIgnore
    public final SleepDailyBest getSleepTimeBestStreak() {
        return this.sleepTimeBestStreak;
    }

    @DexIgnore
    public final int getTotalDays() {
        return this.totalDays;
    }

    @DexIgnore
    public final int getTotalSleepMinutes() {
        return this.totalSleepMinutes;
    }

    @DexIgnore
    public final List<Integer> getTotalSleepStateDistInMinute() {
        return this.totalSleepStateDistInMinute;
    }

    @DexIgnore
    public final int getTotalSleeps() {
        return this.totalSleeps;
    }

    @DexIgnore
    public final String getUid() {
        return this.uid;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.uid;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        SleepDailyBest sleepDailyBest = this.sleepTimeBestDay;
        int hashCode3 = sleepDailyBest != null ? sleepDailyBest.hashCode() : 0;
        SleepDailyBest sleepDailyBest2 = this.sleepTimeBestStreak;
        int hashCode4 = sleepDailyBest2 != null ? sleepDailyBest2.hashCode() : 0;
        int i2 = this.totalDays;
        int i3 = this.totalSleeps;
        int i4 = this.totalSleepMinutes;
        List<Integer> list = this.totalSleepStateDistInMinute;
        int hashCode5 = list != null ? list.hashCode() : 0;
        DateTime dateTime = this.createdAt;
        int hashCode6 = dateTime != null ? dateTime.hashCode() : 0;
        DateTime dateTime2 = this.updatedAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return (((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i2) * 31) + i3) * 31) + i4) * 31) + hashCode5) * 31) + hashCode6) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "SleepStatistic(id=" + this.id + ", uid=" + this.uid + ", sleepTimeBestDay=" + this.sleepTimeBestDay + ", sleepTimeBestStreak=" + this.sleepTimeBestStreak + ", totalDays=" + this.totalDays + ", totalSleeps=" + this.totalSleeps + ", totalSleepMinutes=" + this.totalSleepMinutes + ", totalSleepStateDistInMinute=" + this.totalSleepStateDistInMinute + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
