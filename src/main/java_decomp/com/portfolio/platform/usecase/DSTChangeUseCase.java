package com.portfolio.platform.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DSTChangeUseCase extends CoroutineUseCase<Ai, Ci, Bi> {
    @DexIgnore
    public /* final */ DianaPresetRepository d;
    @DexIgnore
    public /* final */ CustomizeRealDataRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Bi {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ai {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.usecase.DSTChangeUseCase", f = "DSTChangeUseCase.kt", l = {29}, m = "run")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DSTChangeUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(DSTChangeUseCase dSTChangeUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = dSTChangeUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, this);
        }
    }

    @DexIgnore
    public DSTChangeUseCase(DianaPresetRepository dianaPresetRepository, CustomizeRealDataRepository customizeRealDataRepository) {
        Wg6.c(dianaPresetRepository, "mDianaPresetRepository");
        Wg6.c(customizeRealDataRepository, "mRealDataRepository");
        this.d = dianaPresetRepository;
        this.e = customizeRealDataRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "DSTChangeUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ai ai, Xe6 xe6) {
        return m(ai, xe6);
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [('+' char), (r2v15 double), ('h' char)] */
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r2v15 double), ('h' char)] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object m(com.portfolio.platform.usecase.DSTChangeUseCase.Ai r7, com.mapped.Xe6<java.lang.Object> r8) {
        /*
        // Method dump skipped, instructions count: 321
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.usecase.DSTChangeUseCase.m(com.portfolio.platform.usecase.DSTChangeUseCase$Ai, com.mapped.Xe6):java.lang.Object");
    }
}
