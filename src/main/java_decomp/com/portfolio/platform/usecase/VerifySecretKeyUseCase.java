package com.portfolio.platform.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Q27;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class VerifySecretKeyUseCase extends CoroutineUseCase<Ai, Ci, Bi> {
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e;
    @DexIgnore
    public /* final */ DeviceRepository f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ Q27 h;
    @DexIgnore
    public /* final */ PortfolioApp i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Ai(String str) {
            Wg6.c(str, "deviceId");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public Bi(int i, String str) {
            Wg6.c(str, "errorMesagge");
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Ci(String str) {
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.usecase.VerifySecretKeyUseCase", f = "VerifySecretKeyUseCase.kt", l = {74}, m = "getLocalSecretKey")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ VerifySecretKeyUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(VerifySecretKeyUseCase verifySecretKeyUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = verifySecretKeyUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.usecase.VerifySecretKeyUseCase", f = "VerifySecretKeyUseCase.kt", l = {90}, m = "getServerSecretKey")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ VerifySecretKeyUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(VerifySecretKeyUseCase verifySecretKeyUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = verifySecretKeyUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.n(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.usecase.VerifySecretKeyUseCase", f = "VerifySecretKeyUseCase.kt", l = {33, 37, 38}, m = "run")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ VerifySecretKeyUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(VerifySecretKeyUseCase verifySecretKeyUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = verifySecretKeyUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o(null, this);
        }
    }

    @DexIgnore
    public VerifySecretKeyUseCase(DeviceRepository deviceRepository, UserRepository userRepository, Q27 q27, PortfolioApp portfolioApp) {
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(q27, "mDecryptValueKeyStoreUseCase");
        Wg6.c(portfolioApp, "mApp");
        this.f = deviceRepository;
        this.g = userRepository;
        this.h = q27;
        this.i = portfolioApp;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "VerifySecretKeyUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ai ai, Xe6 xe6) {
        return o(ai, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object m(com.mapped.Xe6<? super java.lang.String> r9) {
        /*
            r8 = this;
            r2 = 0
            r7 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.usecase.VerifySecretKeyUseCase.Di
            if (r0 == 0) goto L_0x0055
            r0 = r9
            com.portfolio.platform.usecase.VerifySecretKeyUseCase$Di r0 = (com.portfolio.platform.usecase.VerifySecretKeyUseCase.Di) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0055
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0063
            if (r4 != r7) goto L_0x005b
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.usecase.VerifySecretKeyUseCase r0 = (com.portfolio.platform.usecase.VerifySecretKeyUseCase) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0028:
            com.portfolio.platform.CoroutineUseCase$Ci r0 = (com.portfolio.platform.CoroutineUseCase.Ci) r0
            boolean r1 = r0 instanceof com.fossil.Q27.Ci
            if (r1 == 0) goto L_0x008f
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Get local key success "
            r2.append(r3)
            com.fossil.Q27$Ci r0 = (com.fossil.Q27.Ci) r0
            java.lang.String r3 = r0.a()
            r2.append(r3)
            java.lang.String r3 = "VerifySecretKeyUseCase"
            java.lang.String r2 = r2.toString()
            r1.d(r3, r2)
            java.lang.String r0 = r0.a()
        L_0x0054:
            return r0
        L_0x0055:
            com.portfolio.platform.usecase.VerifySecretKeyUseCase$Di r0 = new com.portfolio.platform.usecase.VerifySecretKeyUseCase$Di
            r0.<init>(r8, r9)
            goto L_0x0014
        L_0x005b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0063:
            com.fossil.El7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r4 = "VerifySecretKeyUseCase"
            java.lang.String r5 = "getLocalSecretKey"
            r1.d(r4, r5)
            com.fossil.Q27 r1 = r8.h
            java.lang.String r4 = r8.e
            if (r4 == 0) goto L_0x0093
            com.fossil.Q27$Ai r5 = new com.fossil.Q27$Ai
            com.fossil.Bi5 r6 = new com.fossil.Bi5
            r6.<init>()
            r5.<init>(r4, r6)
            r0.L$0 = r8
            r0.label = r7
            java.lang.Object r0 = com.fossil.Jq4.a(r1, r5, r0)
            if (r0 != r3) goto L_0x0028
            r0 = r3
            goto L_0x0054
        L_0x008f:
            boolean r0 = r0 instanceof com.fossil.Q27.Bi
            r0 = r2
            goto L_0x0054
        L_0x0093:
            com.mapped.Wg6.i()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.usecase.VerifySecretKeyUseCase.m(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object n(com.mapped.Xe6<? super java.lang.String> r7) {
        /*
            r6 = this;
            r2 = 0
            r5 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.usecase.VerifySecretKeyUseCase.Ei
            if (r0 == 0) goto L_0x0055
            r0 = r7
            com.portfolio.platform.usecase.VerifySecretKeyUseCase$Ei r0 = (com.portfolio.platform.usecase.VerifySecretKeyUseCase.Ei) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0055
            int r1 = r1 + r4
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0063
            if (r4 != r5) goto L_0x005b
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.usecase.VerifySecretKeyUseCase r0 = (com.portfolio.platform.usecase.VerifySecretKeyUseCase) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0028:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "getServerSecretKey "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r4 = "VerifySecretKeyUseCase"
            java.lang.String r3 = r3.toString()
            r1.d(r4, r3)
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x007a
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            if (r0 == 0) goto L_0x0076
            java.lang.String r0 = (java.lang.String) r0
        L_0x0054:
            return r0
        L_0x0055:
            com.portfolio.platform.usecase.VerifySecretKeyUseCase$Ei r0 = new com.portfolio.platform.usecase.VerifySecretKeyUseCase$Ei
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x005b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0063:
            com.fossil.El7.b(r1)
            com.portfolio.platform.data.source.DeviceRepository r1 = r6.f
            java.lang.String r4 = r6.d
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r0 = r1.getDeviceSecretKey(r4, r0)
            if (r0 != r3) goto L_0x0028
            r0 = r3
            goto L_0x0054
        L_0x0076:
            com.mapped.Wg6.i()
            throw r2
        L_0x007a:
            boolean r0 = r0 instanceof com.fossil.Hq5
            if (r0 == 0) goto L_0x0080
            r0 = r2
            goto L_0x0054
        L_0x0080:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.usecase.VerifySecretKeyUseCase.n(com.mapped.Xe6):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x008e, code lost:
        if (r1.j(new com.portfolio.platform.usecase.VerifySecretKeyUseCase.Ci(r5)) == null) goto L_0x0157;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0116  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object o(com.portfolio.platform.usecase.VerifySecretKeyUseCase.Ai r11, com.mapped.Xe6<java.lang.Object> r12) {
        /*
        // Method dump skipped, instructions count: 362
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.usecase.VerifySecretKeyUseCase.o(com.portfolio.platform.usecase.VerifySecretKeyUseCase$Ai, com.mapped.Xe6):java.lang.Object");
    }
}
