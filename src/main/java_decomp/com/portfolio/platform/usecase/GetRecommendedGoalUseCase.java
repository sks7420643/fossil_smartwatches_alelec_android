package com.portfolio.platform.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Qh5;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetRecommendedGoalUseCase extends CoroutineUseCase<Bi, Ci, Ai> {
    @DexIgnore
    public /* final */ SummariesRepository d;
    @DexIgnore
    public /* final */ SleepSummariesRepository e;
    @DexIgnore
    public /* final */ SleepSessionsRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ai {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ Qh5 d;

        @DexIgnore
        public Bi(int i, int i2, int i3, Qh5 qh5) {
            Wg6.c(qh5, "gender");
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = qh5;
        }

        @DexIgnore
        public final Qh5 a() {
            return this.d;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.a;
        }

        @DexIgnore
        public final int d() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.usecase.GetRecommendedGoalUseCase", f = "GetRecommendedGoalUseCase.kt", l = {30, 32, 59, 60, 64, 65}, m = "run")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ GetRecommendedGoalUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(GetRecommendedGoalUseCase getRecommendedGoalUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = getRecommendedGoalUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, this);
        }
    }

    @DexIgnore
    public GetRecommendedGoalUseCase(SummariesRepository summariesRepository, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        Wg6.c(summariesRepository, "mSummaryRepository");
        Wg6.c(sleepSummariesRepository, "mSleepSummariesRepository");
        Wg6.c(sleepSessionsRepository, "mSleepSessionsRepository");
        this.d = summariesRepository;
        this.e = sleepSummariesRepository;
        this.f = sleepSessionsRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "GetRecommendedGoalUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return m(bi, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x010f  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0113  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0153  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x016d  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x018a  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x01e1  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x01e5  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0225  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0229  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x02a6  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x02e8  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x031b  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0323  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x032b  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0332  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object m(com.portfolio.platform.usecase.GetRecommendedGoalUseCase.Bi r18, com.mapped.Xe6<java.lang.Object> r19) {
        /*
        // Method dump skipped, instructions count: 842
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.usecase.GetRecommendedGoalUseCase.m(com.portfolio.platform.usecase.GetRecommendedGoalUseCase$Bi, com.mapped.Xe6):java.lang.Object");
    }
}
