package com.portfolio.platform.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.S27;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.CoroutineUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GeneratePassphraseUseCase extends CoroutineUseCase<Ai, Bi, Object> {
    @DexIgnore
    public /* final */ S27 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Bi {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public Bi(String str) {
            Wg6.c(str, "passphrase");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.usecase.GeneratePassphraseUseCase", f = "GeneratePassphraseUseCase.kt", l = {26}, m = "run")
    public static final class Ci extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ GeneratePassphraseUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(GeneratePassphraseUseCase generatePassphraseUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = generatePassphraseUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.m(null, this);
        }
    }

    @DexIgnore
    public GeneratePassphraseUseCase(S27 s27) {
        Wg6.c(s27, "mEncryptValueKeyStoreUseCase");
        this.d = s27;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "GeneratePassphraseUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ai ai, Xe6 xe6) {
        return m(ai, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object m(com.portfolio.platform.usecase.GeneratePassphraseUseCase.Ai r19, com.mapped.Xe6<java.lang.Object> r20) {
        /*
        // Method dump skipped, instructions count: 244
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.usecase.GeneratePassphraseUseCase.m(com.portfolio.platform.usecase.GeneratePassphraseUseCase$Ai, com.mapped.Xe6):java.lang.Object");
    }
}
