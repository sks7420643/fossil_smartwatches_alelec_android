package com.portfolio.platform.localization;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.NativeProtocol;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Um5;
import com.fossil.Wm5;
import com.fossil.Wt7;
import com.fossil.Ym5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.helper.DeviceHelper;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocalizationManager extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ Ai i; // = new Ai(null);
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public WeakReference<Activity> b;
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public Di d;
    @DexIgnore
    public /* final */ Application.ActivityLifecycleCallbacks e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ GuestApiService g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a(byte[] bArr) {
            StringBuilder sb = new StringBuilder();
            for (byte b : bArr) {
                String num = Integer.toString((b & 255) + 256, 16);
                Wg6.b(num, "Integer.toString((bInput\u2026t() and 255) + 0x100, 16)");
                if (num != null) {
                    String substring = num.substring(1);
                    Wg6.b(substring, "(this as java.lang.String).substring(startIndex)");
                    sb.append(substring);
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            String sb2 = sb.toString();
            Wg6.b(sb2, "ret.toString()");
            if (sb2 != null) {
                String lowerCase = sb2.toLowerCase();
                Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                return lowerCase;
            }
            throw new Rc6("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final String b() {
            return LocalizationManager.h;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0073  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x007f  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean c(java.lang.String r8, java.lang.String r9) {
            /*
                r7 = this;
                r2 = 0
                r3 = 0
                java.lang.String r0 = "filePath"
                com.mapped.Wg6.c(r8, r0)
                if (r9 != 0) goto L_0x000b
                r0 = 1
            L_0x000a:
                return r0
            L_0x000b:
                java.lang.String r0 = "MD5"
                java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ Exception -> 0x0078, all -> 0x007b }
                r1 = 2014(0x7de, float:2.822E-42)
                byte[] r4 = new byte[r1]     // Catch:{ Exception -> 0x0078, all -> 0x007b }
                java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0078, all -> 0x007b }
                r1.<init>(r8)     // Catch:{ Exception -> 0x0078, all -> 0x007b }
            L_0x001a:
                int r2 = r1.read(r4)     // Catch:{ Exception -> 0x0046, all -> 0x0085 }
                r5 = -1
                if (r2 != r5) goto L_0x003f
                byte[] r0 = r0.digest()     // Catch:{ Exception -> 0x0046, all -> 0x0085 }
                java.lang.String r2 = "digest.digest()"
                com.mapped.Wg6.b(r0, r2)     // Catch:{ Exception -> 0x0046, all -> 0x0085 }
                java.lang.String r0 = r7.a(r0)     // Catch:{ Exception -> 0x0046, all -> 0x0085 }
                java.lang.String r2 = r9.toLowerCase()     // Catch:{ Exception -> 0x0046, all -> 0x0085 }
                java.lang.String r4 = "(this as java.lang.String).toLowerCase()"
                com.mapped.Wg6.b(r2, r4)     // Catch:{ Exception -> 0x0046, all -> 0x0085 }
                boolean r0 = com.mapped.Wg6.a(r2, r0)     // Catch:{ Exception -> 0x0046, all -> 0x0085 }
                r1.close()
                goto L_0x000a
            L_0x003f:
                if (r2 <= 0) goto L_0x001a
                r5 = 0
                r0.update(r4, r5, r2)
                goto L_0x001a
            L_0x0046:
                r0 = move-exception
            L_0x0047:
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0083 }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ all -> 0x0083 }
                java.lang.String r4 = r7.b()     // Catch:{ all -> 0x0083 }
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0083 }
                r5.<init>()     // Catch:{ all -> 0x0083 }
                java.lang.String r6 = "Error inside "
                r5.append(r6)     // Catch:{ all -> 0x0083 }
                java.lang.String r6 = r7.b()     // Catch:{ all -> 0x0083 }
                r5.append(r6)     // Catch:{ all -> 0x0083 }
                java.lang.String r6 = ".verifyDownloadFile - e="
                r5.append(r6)     // Catch:{ all -> 0x0083 }
                r5.append(r0)     // Catch:{ all -> 0x0083 }
                java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x0083 }
                r2.e(r4, r0)     // Catch:{ all -> 0x0083 }
                if (r1 == 0) goto L_0x0076
                r1.close()
            L_0x0076:
                r0 = r3
                goto L_0x000a
            L_0x0078:
                r0 = move-exception
                r1 = r2
                goto L_0x0047
            L_0x007b:
                r0 = move-exception
                r1 = r2
            L_0x007d:
                if (r1 == 0) goto L_0x0082
                r1.close()
            L_0x0082:
                throw r0
            L_0x0083:
                r0 = move-exception
                goto L_0x007d
            L_0x0085:
                r0 = move-exception
                goto L_0x007d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.localization.LocalizationManager.Ai.c(java.lang.String, java.lang.String):boolean");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends AsyncTask<String, Void, Boolean> {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ Wm5 b;
        @DexIgnore
        public /* final */ /* synthetic */ LocalizationManager c;

        @DexIgnore
        public Bi(LocalizationManager localizationManager, Context context, Wm5 wm5) {
            Wg6.c(context, "context");
            Wg6.c(wm5, Firmware.COLUMN_DOWNLOAD_URL);
            this.c = localizationManager;
            this.a = context;
            this.b = wm5;
        }

        @DexIgnore
        public Boolean a(String... strArr) {
            Wg6.c(strArr, "links");
            if (!TextUtils.isEmpty(this.b.b())) {
                try {
                    StringBuilder sb = new StringBuilder();
                    File filesDir = this.a.getFilesDir();
                    Wg6.b(filesDir, "context.filesDir");
                    sb.append(filesDir.getAbsolutePath());
                    sb.append("/");
                    sb.append("language.zip");
                    String sb2 = sb.toString();
                    if (new File(sb2).exists() && LocalizationManager.i.c(sb2, this.b.a())) {
                        return Boolean.TRUE;
                    }
                    URLConnection openConnection = new URL(this.b.b()).openConnection();
                    openConnection.connect();
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(openConnection.getInputStream());
                    FileOutputStream fileOutputStream = new FileOutputStream(sb2);
                    try {
                        byte[] bArr = new byte[1024];
                        while (true) {
                            int read = bufferedInputStream.read(bArr);
                            if (read == -1) {
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                break;
                            } else if (isCancelled()) {
                                Boolean bool = Boolean.FALSE;
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                bufferedInputStream.close();
                                return bool;
                            } else {
                                fileOutputStream.write(bArr, 0, read);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    } catch (Throwable th) {
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        bufferedInputStream.close();
                        throw th;
                    }
                    bufferedInputStream.close();
                    if (TextUtils.isEmpty(this.b.a())) {
                        FLogger.INSTANCE.getLocal().e(LocalizationManager.i.b(), "Download complete with risk cause by empty checksum");
                        return Boolean.TRUE;
                    } else if (LocalizationManager.i.c(sb2, this.b.a())) {
                        return Boolean.TRUE;
                    } else {
                        FLogger.INSTANCE.getLocal().e(LocalizationManager.i.b(), "Inconsistent checksum, retry download?");
                        return Boolean.TRUE;
                    }
                } catch (Exception e2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b2 = LocalizationManager.i.b();
                    local.e(b2, "Error inside " + LocalizationManager.i.b() + ".onHandleIntent - e=" + e2);
                }
            }
            return Boolean.FALSE;
        }

        @DexIgnore
        public void b(Boolean bool) {
            super.onPostExecute(bool);
            if (bool != null) {
                if (bool.booleanValue()) {
                    new Ei().execute(this.b.c());
                }
                Di h = this.c.h();
                if (h != null) {
                    h.a(false);
                    return;
                }
                return;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object[]] */
        @Override // android.os.AsyncTask
        public /* bridge */ /* synthetic */ Boolean doInBackground(String[] strArr) {
            return a(strArr);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // android.os.AsyncTask
        public /* bridge */ /* synthetic */ void onPostExecute(Boolean bool) {
            b(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci extends AsyncTask<String, Void, Boolean> {
        @DexIgnore
        public String a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ LocalizationManager c;

        @DexIgnore
        public Ci(LocalizationManager localizationManager, String str, boolean z) {
            Wg6.c(str, "mFilePath");
            this.c = localizationManager;
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        public Boolean a(String... strArr) {
            boolean z = true;
            Wg6.c(strArr, NativeProtocol.WEB_DIALOG_PARAMS);
            try {
                if (Wg6.a(Um5.b(), "en_US")) {
                    Ym5.d(this.a, this.b);
                    Ym5.d(this.c.j() + "/strings.json", true);
                } else {
                    Ym5.d(this.a, this.b);
                }
                Ym5.c();
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e(LocalizationManager.i.b(), "load cache failed e=" + e);
                z = false;
            }
            return Boolean.valueOf(z);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object[]] */
        @Override // android.os.AsyncTask
        public /* bridge */ /* synthetic */ Boolean doInBackground(String[] strArr) {
            return a(strArr);
        }
    }

    @DexIgnore
    public interface Di {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ei extends AsyncTask<String, Void, Boolean> {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei() {
        }

        @DexIgnore
        public Boolean a(String... strArr) {
            Wg6.c(strArr, NativeProtocol.WEB_DIALOG_PARAMS);
            String str = strArr[0];
            LocalizationManager localizationManager = LocalizationManager.this;
            return Boolean.valueOf(localizationManager.u(localizationManager.m()));
        }

        @DexIgnore
        public void b(Boolean bool) {
            super.onPostExecute(bool);
            if (bool != null) {
                if (bool.booleanValue()) {
                    LocalizationManager.this.p(new File(LocalizationManager.this.m().getFilesDir().toString() + ""));
                    LocalizationManager localizationManager = LocalizationManager.this;
                    StringBuilder sb = new StringBuilder();
                    File filesDir = LocalizationManager.this.m().getFilesDir();
                    Wg6.b(filesDir, "mContext.filesDir");
                    sb.append(filesDir.getAbsolutePath());
                    sb.append("/");
                    sb.append(LocalizationManager.this.k());
                    localizationManager.f(sb.toString(), true);
                }
                Di h = LocalizationManager.this.h();
                if (h != null) {
                    h.a(bool.booleanValue());
                    return;
                }
                return;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object[]] */
        @Override // android.os.AsyncTask
        public /* bridge */ /* synthetic */ Boolean doInBackground(String[] strArr) {
            return a(strArr);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // android.os.AsyncTask
        public /* bridge */ /* synthetic */ void onPostExecute(Boolean bool) {
            b(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public /* final */ /* synthetic */ LocalizationManager b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Xe6 xe6, Fi fi) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    StringBuilder sb = new StringBuilder();
                    File filesDir = this.this$0.b.m().getFilesDir();
                    Wg6.b(filesDir, "mContext.filesDir");
                    sb.append(filesDir.getAbsolutePath());
                    sb.append("/");
                    sb.append("language.zip");
                    String sb2 = sb.toString();
                    File file = new File(sb2);
                    if (!file.exists()) {
                        LocalizationManager localizationManager = this.this$0.b;
                        this.L$0 = il6;
                        this.L$1 = sb2;
                        this.L$2 = file;
                        this.label = 1;
                        if (localizationManager.e(this) == d) {
                            return d;
                        }
                    }
                } else if (i == 1) {
                    File file2 = (File) this.L$2;
                    String str = (String) this.L$1;
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Fi(LocalizationManager localizationManager) {
            this.b = localizationManager;
        }

        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
            Class<?> cls;
            if (activity != null) {
                this.b.r(new WeakReference<>(activity));
                WeakReference<Activity> n = this.b.n();
                if (n != null) {
                    Activity activity2 = n.get();
                    if (Wg6.a((activity2 == null || (cls = activity2.getClass()) == null) ? null : cls.getSimpleName(), this.b.l())) {
                        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Aii(null, this), 3, null);
                        return;
                    }
                    return;
                }
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public void onActivityDestroyed(Activity activity) {
            Wg6.c(activity, Constants.ACTIVITY);
            if (this.b.n() != null) {
                WeakReference<Activity> n = this.b.n();
                if (n == null) {
                    Wg6.i();
                    throw null;
                } else if (Wg6.a(n.get(), activity)) {
                    this.b.r(null);
                }
            }
        }

        @DexIgnore
        public void onActivityPaused(Activity activity) {
            Wg6.c(activity, Constants.ACTIVITY);
        }

        @DexIgnore
        public void onActivityResumed(Activity activity) {
            Wg6.c(activity, Constants.ACTIVITY);
        }

        @DexIgnore
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            Wg6.c(activity, Constants.ACTIVITY);
            Wg6.c(bundle, "bundle");
        }

        @DexIgnore
        public void onActivityStarted(Activity activity) {
            Wg6.c(activity, Constants.ACTIVITY);
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
            Wg6.c(activity, Constants.ACTIVITY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.localization.LocalizationManager", f = "LocalizationManager.kt", l = {194}, m = "downloadLanguagePack")
    public static final class Gi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ LocalizationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(LocalizationManager localizationManager, Xe6 xe6) {
            super(xe6);
            this.this$0 = localizationManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.localization.LocalizationManager$downloadLanguagePack$response$1", f = "LocalizationManager.kt", l = {194}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<Ku3>>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ LocalizationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(LocalizationManager localizationManager, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = localizationManager;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Hi(this.this$0, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<ApiResponse<Ku3>>> xe6) {
            throw null;
            //return ((Hi) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                GuestApiService guestApiService = this.this$0.g;
                String str = this.this$0.f;
                this.label = 1;
                Object localizations = guestApiService.getLocalizations(str, "android", this);
                return localizations == d ? d : localizations;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    /*
    static {
        String simpleName = LocalizationManager.class.getSimpleName();
        Wg6.b(simpleName, "LocalizationManager::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public LocalizationManager(Application application, String str, GuestApiService guestApiService) {
        Wg6.c(application, "app");
        Wg6.c(str, "mAppVersion");
        Wg6.c(guestApiService, "mGuestApiService");
        this.f = str;
        this.g = guestApiService;
        Wg6.b(application.getSharedPreferences(application.getPackageName() + ".language", 0), "app.getSharedPreferences\u2026e\", Context.MODE_PRIVATE)");
        this.a = application;
        this.e = new Fi(this);
    }

    @DexIgnore
    public final void d(Wm5 wm5) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "downloadFile response=" + wm5);
        new Bi(this, this.a, wm5).execute(new String[0]);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.mapped.Xe6<? super com.mapped.Cd6> r8) {
        /*
            r7 = this;
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 0
            boolean r0 = r8 instanceof com.portfolio.platform.localization.LocalizationManager.Gi
            if (r0 == 0) goto L_0x0088
            r0 = r8
            com.portfolio.platform.localization.LocalizationManager$Gi r0 = (com.portfolio.platform.localization.LocalizationManager.Gi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0088
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0096
            if (r3 != r6) goto L_0x008e
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.localization.LocalizationManager r0 = (com.portfolio.platform.localization.LocalizationManager) r0
            com.fossil.El7.b(r1)
            r7 = r0
        L_0x0028:
            r0 = r1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x00b8
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.localization.LocalizationManager.h
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "download language success isFromCache "
            r3.append(r4)
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            boolean r4 = r0.b()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            boolean r1 = r0.b()
            if (r1 != 0) goto L_0x007e
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r0 = (com.portfolio.platform.data.source.remote.ApiResponse) r0
            if (r0 == 0) goto L_0x007e
            java.util.List r0 = r0.get_items()
            if (r0 == 0) goto L_0x007e
            boolean r1 = r0.isEmpty()
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x007e
            com.fossil.Wm5 r1 = new com.fossil.Wm5
            r1.<init>()
            java.lang.Object r0 = r0.get(r5)
            com.mapped.Ku3 r0 = (com.mapped.Ku3) r0
            r1.d(r0)
            r7.d(r1)
        L_0x007e:
            com.portfolio.platform.localization.LocalizationManager$Di r0 = r7.d
            if (r0 == 0) goto L_0x0085
            r0.a(r5)
        L_0x0085:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0087:
            return r0
        L_0x0088:
            com.portfolio.platform.localization.LocalizationManager$Gi r0 = new com.portfolio.platform.localization.LocalizationManager$Gi
            r0.<init>(r7, r8)
            goto L_0x0014
        L_0x008e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0096:
            com.fossil.El7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = com.portfolio.platform.localization.LocalizationManager.h
            java.lang.String r4 = "downloadLanguagePack() called"
            r1.d(r3, r4)
            com.portfolio.platform.localization.LocalizationManager$Hi r1 = new com.portfolio.platform.localization.LocalizationManager$Hi
            r3 = 0
            r1.<init>(r7, r3)
            r0.L$0 = r7
            r0.label = r6
            java.lang.Object r1 = com.portfolio.platform.response.ResponseKt.d(r1, r0)
            if (r1 != r2) goto L_0x0028
            r0 = r2
            goto L_0x0087
        L_0x00b8:
            boolean r0 = r0 instanceof com.fossil.Hq5
            if (r0 == 0) goto L_0x0085
            com.portfolio.platform.localization.LocalizationManager$Di r0 = r7.d
            if (r0 == 0) goto L_0x0085
            r0.a(r5)
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.localization.LocalizationManager.e(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void f(String str, boolean z) {
        Wg6.c(str, "path");
        new Ci(this, str, z).execute(new String[0]);
    }

    @DexIgnore
    public final Application.ActivityLifecycleCallbacks g() {
        return this.e;
    }

    @DexIgnore
    public final Di h() {
        return this.d;
    }

    @DexIgnore
    public final String i() {
        StringBuilder sb = new StringBuilder();
        File filesDir = this.a.getFilesDir();
        Wg6.b(filesDir, "mContext.filesDir");
        sb.append(filesDir.getAbsolutePath());
        sb.append("/");
        sb.append(k());
        return sb.toString();
    }

    @DexIgnore
    public final String j() {
        StringBuilder sb = new StringBuilder();
        File filesDir = this.a.getFilesDir();
        Wg6.b(filesDir, "mContext.filesDir");
        sb.append(filesDir.getAbsolutePath());
        sb.append("/");
        sb.append("language");
        sb.append("/values");
        return sb.toString();
    }

    @DexIgnore
    public final String k() {
        StringBuilder sb = new StringBuilder("");
        StringBuilder sb2 = new StringBuilder();
        File filesDir = this.a.getFilesDir();
        Wg6.b(filesDir, "mContext.filesDir");
        sb2.append(filesDir.getAbsolutePath());
        sb2.append("/");
        sb2.append("language");
        File file = new File(sb2.toString());
        if (file.exists()) {
            String[] list = file.list();
            Wg6.b(list, "file.list()");
            Locale a2 = Um5.a();
            int length = list.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                }
                String str = list[i2];
                Wg6.b(a2, "l");
                String language = a2.getLanguage();
                Wg6.b(language, "l.language");
                if (Wt7.v(str, language, false, 2, null)) {
                    String sb3 = sb.toString();
                    Wg6.b(sb3, "path.toString()");
                    String language2 = a2.getLanguage();
                    Wg6.b(language2, "l.language");
                    if (!Wt7.v(sb3, language2, false, 2, null)) {
                        sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                        sb.append(a2.getLanguage());
                    }
                    String country = a2.getCountry();
                    Wg6.b(country, "l.country");
                    if (Wt7.v(str, country, false, 2, null)) {
                        sb.append("-r");
                        sb.append(a2.getCountry());
                        break;
                    }
                }
                i2++;
            }
        }
        sb.insert(0, "language/values");
        sb.append("/strings.json");
        FLogger.INSTANCE.getLocal().d(h, "path=" + sb.toString());
        String sb4 = sb.toString();
        Wg6.b(sb4, "path.toString()");
        return sb4;
    }

    @DexIgnore
    public final String l() {
        return this.c;
    }

    @DexIgnore
    public final Context m() {
        return this.a;
    }

    @DexIgnore
    public final WeakReference<Activity> n() {
        return this.b;
    }

    @DexIgnore
    public final void o(String str, String str2) {
        File file = new File(str + str2);
        if (!file.isDirectory()) {
            file.mkdirs();
        }
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        Activity activity;
        Wg6.c(context, "context");
        if (intent != null && !TextUtils.isEmpty(intent.getAction()) && Wg6.a(intent.getAction(), "android.intent.action.LOCALE_CHANGED")) {
            String J = PortfolioApp.get.instance().J();
            if (DeviceHelper.o.x(J)) {
                PortfolioApp.get.instance().A1(J);
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = h;
            local.d(str, "onReceive locale=" + Um5.b());
            WeakReference<Activity> weakReference = this.b;
            if (!(weakReference == null || (activity = weakReference.get()) == null)) {
                Wg6.b(activity, "it");
                if (!activity.isFinishing()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = h;
                    local2.d(str2, "onReceive finish activity=" + activity.getLocalClassName());
                    activity.setResult(0);
                    activity.finishAffinity();
                }
            }
            PortfolioApp.get.instance().t0();
            q();
        }
    }

    @DexIgnore
    public final void p(File file) {
        Wg6.c(file, "folder");
        if (file.exists() && file.isDirectory()) {
            File[] listFiles = file.listFiles();
            for (File file2 : listFiles) {
                Wg6.b(file2, "f");
                if (file2.isDirectory()) {
                    p(file2);
                }
            }
        }
    }

    @DexIgnore
    public final void q() {
        Ym5.a();
        String i2 = i();
        if (new File(i2).exists()) {
            f(i2, true);
        } else {
            f(k(), false);
        }
    }

    @DexIgnore
    public final void r(WeakReference<Activity> weakReference) {
        this.b = weakReference;
    }

    @DexIgnore
    public final LocalizationManager s(Di di) {
        this.d = di;
        return this;
    }

    @DexIgnore
    public final void t(String str) {
        Wg6.c(str, "splashScreen");
        this.c = str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x009a, code lost:
        if (r0 != null) goto L_0x009c;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0101  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean u(android.content.Context r10) {
        /*
        // Method dump skipped, instructions count: 300
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.localization.LocalizationManager.u(android.content.Context):boolean");
    }
}
