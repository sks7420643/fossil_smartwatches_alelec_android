package com.misfit.frameworks.common.model.Cucumber;

import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CucumberUserSetting {
    @DexIgnore
    public boolean stravaEnable;

    @DexIgnore
    public CucumberUserSetting(JSONObject jSONObject) {
        boolean z = true;
        try {
            int i = jSONObject.getInt(Constants.STRAVA_RESULT);
            if (!(i == -2 || i == 1 || i == 0 || i == 1)) {
                z = false;
            }
            this.stravaEnable = z;
        } catch (Exception e) {
        }
    }

    @DexIgnore
    public boolean isStravaEnable() {
        return this.stravaEnable;
    }

    @DexIgnore
    public void setStravaEnable(boolean z) {
        this.stravaEnable = z;
    }
}
