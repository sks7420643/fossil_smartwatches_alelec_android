package com.misfit.frameworks.network.enums;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MFNetworkConstants {
    @DexIgnore
    public static /* final */ String CUCUMBER_USER_AGENT; // = ("MisfitCycling/Android##Cucumber;1.0;" + Build.MODEL + ";Android " + Build.VERSION.RELEASE);
    @DexIgnore
    public static /* final */ String USER_AGENT; // = ("MisfitLink/Android##LinkButton;1.0;" + Build.MODEL + ";Android " + Build.VERSION.RELEASE);
}
