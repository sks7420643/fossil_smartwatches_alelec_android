package com.misfit.frameworks.network.request;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.HTTPMethod;
import com.misfit.frameworks.common.log.MFLogger;
import com.misfit.frameworks.network.configuration.MFConfiguration;
import com.misfit.frameworks.network.configuration.MFHeader;
import com.misfit.frameworks.network.manager.MFNetwork;
import com.misfit.frameworks.network.responses.MFResponse;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class MFBaseRequest {
    @DexIgnore
    public static /* final */ String TAG; // = "MFBaseRequest";
    @DexIgnore
    public String apiMethod;
    @DexIgnore
    public MFResponse buttonApiResponse; // = new MFResponse();
    @DexIgnore
    public MFConfiguration configuration;
    @DexIgnore
    public Context context;
    @DexIgnore
    public Exception exception;
    @DexIgnore
    public HttpURLConnection httpURLConnection;
    @DexIgnore
    public Object jsonData; // = new JSONObject();
    @DexIgnore
    public HTTPMethod method;
    @DexIgnore
    public int requestId;
    @DexIgnore
    public URL url;

    @DexIgnore
    public MFBaseRequest(Context context2) {
        this.context = context2;
    }

    @DexIgnore
    public void buildHeader(MFHeader mFHeader) {
        HashMap<String, String> headerMap;
        this.httpURLConnection.setRequestProperty("Content-Type", Constants.CONTENT_TYPE);
        this.httpURLConnection.setRequestProperty(com.zendesk.sdk.network.Constants.ACCEPT_HEADER, com.zendesk.sdk.network.Constants.APPLICATION_JSON);
        this.httpURLConnection.setRequestProperty("User-Agent", MFNetwork.getInstance(this.context).getUserAgent());
        this.httpURLConnection.setRequestProperty("Installation-ID", MFNetwork.getInstance(this.context).getInstallationId());
        this.httpURLConnection.setRequestProperty("Locale", MFNetwork.getInstance(this.context).getLocale());
        if (this.method == HTTPMethod.PATCH) {
            this.httpURLConnection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
        }
        if (!(mFHeader == null || (headerMap = this.configuration.getHeader().getHeaderMap()) == null)) {
            for (String str : headerMap.keySet()) {
                String str2 = headerMap.get(str);
                if (!TextUtils.isEmpty(str2)) {
                    this.httpURLConnection.setRequestProperty(str, str2);
                }
            }
        }
        this.httpURLConnection.setConnectTimeout(10000);
        this.httpURLConnection.setReadTimeout(10000);
    }

    @DexIgnore
    public void buildRequest() {
        Uri uri;
        this.configuration = initConfiguration();
        this.jsonData = initJsonData();
        this.method = initHttpMethod();
        this.apiMethod = initApiMethod();
        this.buttonApiResponse = initResponse();
        Uri parse = Uri.parse(this.configuration.getBaseServerUrl() + this.apiMethod);
        try {
            if (this.method == HTTPMethod.GET) {
                if (this.jsonData != null) {
                    if (this.jsonData instanceof JSONObject) {
                        JSONObject jSONObject = (JSONObject) this.jsonData;
                        Iterator<String> keys = jSONObject.keys();
                        Uri.Builder buildUpon = parse.buildUpon();
                        while (keys.hasNext()) {
                            String next = keys.next();
                            buildUpon.appendQueryParameter(next, String.valueOf(jSONObject.get(next)));
                        }
                        parse = buildUpon.build();
                    } else {
                        throw new Exception("We do not support JSONArray now for GET request.");
                    }
                }
                URL url2 = new URL(parse.toString());
                this.url = url2;
                HttpURLConnection httpURLConnection2 = (HttpURLConnection) url2.openConnection();
                this.httpURLConnection = httpURLConnection2;
                httpURLConnection2.setDoInput(true);
                this.httpURLConnection.setRequestMethod("GET");
            } else if (this.method == HTTPMethod.POST || this.method == HTTPMethod.PATCH) {
                URL url3 = new URL(parse.toString());
                this.url = url3;
                HttpURLConnection httpURLConnection3 = (HttpURLConnection) url3.openConnection();
                this.httpURLConnection = httpURLConnection3;
                httpURLConnection3.setDoOutput(true);
                this.httpURLConnection.setRequestMethod("POST");
            } else if (this.method == HTTPMethod.PUT) {
                URL url4 = new URL(parse.toString());
                this.url = url4;
                HttpURLConnection httpURLConnection4 = (HttpURLConnection) url4.openConnection();
                this.httpURLConnection = httpURLConnection4;
                httpURLConnection4.setDoOutput(true);
                this.httpURLConnection.setRequestMethod("PUT");
            } else if (this.method == HTTPMethod.DELETE) {
                if (this.jsonData == null) {
                    uri = parse;
                } else if (this.jsonData instanceof JSONObject) {
                    JSONObject jSONObject2 = (JSONObject) this.jsonData;
                    Iterator<String> keys2 = jSONObject2.keys();
                    Uri.Builder buildUpon2 = parse.buildUpon();
                    while (keys2.hasNext()) {
                        String next2 = keys2.next();
                        buildUpon2.appendQueryParameter(next2, String.valueOf(jSONObject2.get(next2)));
                    }
                    uri = buildUpon2.build();
                } else {
                    throw new Exception("We do not support JSONArray now for DELETE request.");
                }
                URL url5 = new URL(uri.toString());
                this.url = url5;
                HttpURLConnection httpURLConnection5 = (HttpURLConnection) url5.openConnection();
                this.httpURLConnection = httpURLConnection5;
                httpURLConnection5.setDoInput(true);
                this.httpURLConnection.setRequestMethod("DELETE");
            }
        } catch (Exception e) {
            String str = TAG;
            MFLogger.d(str, "Exception when build request " + e);
        }
        buildHeader(this.configuration.getHeader());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0090 A[SYNTHETIC, Splitter:B:23:0x0090] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01d6  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01fe  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0214 A[SYNTHETIC, Splitter:B:67:0x0214] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.misfit.frameworks.network.responses.MFResponse execute() {
        /*
        // Method dump skipped, instructions count: 570
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.network.request.MFBaseRequest.execute():com.misfit.frameworks.network.responses.MFResponse");
    }

    @DexIgnore
    public Exception getException() {
        return this.exception;
    }

    @DexIgnore
    public abstract String initApiMethod();

    @DexIgnore
    public byte[] initBinaryData() {
        return null;
    }

    @DexIgnore
    public abstract MFConfiguration initConfiguration();

    @DexIgnore
    public abstract HTTPMethod initHttpMethod();

    @DexIgnore
    public abstract Object initJsonData();

    @DexIgnore
    public abstract MFResponse initResponse();

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0020 A[SYNTHETIC, Splitter:B:11:0x0020] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0035 A[SYNTHETIC, Splitter:B:21:0x0035] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String readStream(java.io.InputStream r5) {
        /*
            r4 = this;
            r2 = 0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0039, all -> 0x0031 }
            java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0039, all -> 0x0031 }
            r0.<init>(r5)     // Catch:{ IOException -> 0x0039, all -> 0x0031 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x0039, all -> 0x0031 }
        L_0x0010:
            java.lang.String r0 = r1.readLine()     // Catch:{ IOException -> 0x001a, all -> 0x0044 }
            if (r0 == 0) goto L_0x0028
            r3.append(r0)     // Catch:{ IOException -> 0x001a, all -> 0x0044 }
            goto L_0x0010
        L_0x001a:
            r0 = move-exception
        L_0x001b:
            r0.printStackTrace()     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ IOException -> 0x002c }
        L_0x0023:
            java.lang.String r0 = r3.toString()
            return r0
        L_0x0028:
            r1.close()
            goto L_0x0023
        L_0x002c:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0023
        L_0x0031:
            r0 = move-exception
        L_0x0032:
            r1 = r2
        L_0x0033:
            if (r1 == 0) goto L_0x0038
            r1.close()     // Catch:{ IOException -> 0x003c }
        L_0x0038:
            throw r0
        L_0x0039:
            r0 = move-exception
            r1 = r2
            goto L_0x001b
        L_0x003c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0038
        L_0x0041:
            r0 = move-exception
            r2 = r1
            goto L_0x0032
        L_0x0044:
            r0 = move-exception
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.network.request.MFBaseRequest.readStream(java.io.InputStream):java.lang.String");
    }

    @DexIgnore
    public void setRequestId(int i) {
        this.requestId = i;
    }
}
