package com.misfit.frameworks.buttonservice.db;

import android.content.Context;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class HeartRateProvider extends BaseDbProvider {
    @DexIgnore
    public static /* final */ int BUFFER_SIZE; // = 1000000;
    @DexIgnore
    public static /* final */ String DB_NAME; // = "heart_rate_file.db";
    @DexIgnore
    public static /* final */ String TAG; // = "HeartRateProvider";
    @DexIgnore
    public static HeartRateProvider sInstance;

    @DexIgnore
    public HeartRateProvider(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<DataFile, String> getDataFileDao() throws SQLException {
        return this.databaseHelper.getDao(DataFile.class);
    }

    @DexIgnore
    public static HeartRateProvider getInstance(Context context) {
        HeartRateProvider heartRateProvider;
        synchronized (HeartRateProvider.class) {
            try {
                if (sInstance == null) {
                    sInstance = new HeartRateProvider(context, DB_NAME);
                }
                heartRateProvider = sInstance;
            } catch (Throwable th) {
                throw th;
            }
        }
        return heartRateProvider;
    }

    @DexIgnore
    public void clearFiles() {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.i(str, "Inside " + TAG + ".clearHwLog");
            getDataFileDao().deleteBuilder().delete();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.e(str2, "Error inside " + TAG + ".clearHwLog - e=" + e);
        }
    }

    @DexIgnore
    public void deleteDataFile(DataFile dataFile) {
        try {
            getDataFileDao().delete((Dao<DataFile, String>) dataFile);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, "Error inside " + TAG + ".deleteDataFile - e=" + e);
        }
    }

    @DexIgnore
    public List<DataFile> getAllDataFiles(long j) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<DataFile, String> queryBuilder = getDataFileDao().queryBuilder();
            queryBuilder.where().lt("syncTime", Long.valueOf(j));
            List<DataFile> query = getDataFileDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, "Error inside " + TAG + ".getAllDataFiles - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    public DataFile getDataFile(String str) {
        return getDataFile(str, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x011d, code lost:
        if (r3 != null) goto L_0x011f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x011f, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0122, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x015b, code lost:
        if (r3 != null) goto L_0x011f;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0161  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.misfit.frameworks.buttonservice.db.DataFile getDataFile(java.lang.String r19, java.lang.String r20) {
        /*
        // Method dump skipped, instructions count: 372
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.db.HeartRateProvider.getDataFile(java.lang.String, java.lang.String):com.misfit.frameworks.buttonservice.db.DataFile");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{DataFile.class};
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    public void saveDataFile(DataFile dataFile) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside " + TAG + ".saveDataFile - dataFile=" + dataFile);
        try {
            if (getDataFile(dataFile.key, dataFile.serial) == null) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "Inside " + TAG + ".saveDataFile - Saving dataFile=" + dataFile);
                getDataFileDao().create((Dao<DataFile, String>) dataFile);
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.e(str3, "Error inside " + TAG + ".saveDataFile -e=Data file existed");
            }
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local4.e(str4, "Error inside " + TAG + ".saveDataFile - e=" + e);
        }
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = TAG;
        local5.d(str5, "Inside " + TAG + ".saveDataFile - dataFile=" + dataFile + " - DONE");
    }
}
