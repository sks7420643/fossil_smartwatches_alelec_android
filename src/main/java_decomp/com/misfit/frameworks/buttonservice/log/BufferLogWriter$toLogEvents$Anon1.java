package com.misfit.frameworks.buttonservice.log;

import com.fossil.Qq7;
import com.google.gson.Gson;
import com.mapped.Hg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferLogWriter$toLogEvents$Anon1 extends Qq7 implements Hg6<String, LogEvent> {
    @DexIgnore
    public /* final */ /* synthetic */ Gson $gson;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$toLogEvents$Anon1(Gson gson) {
        super(1);
        this.$gson = gson;
    }

    @DexIgnore
    public final LogEvent invoke(String str) {
        Wg6.c(str, "it");
        try {
            return (LogEvent) this.$gson.k(str, LogEvent.class);
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ LogEvent invoke(String str) {
        return invoke(str);
    }
}
