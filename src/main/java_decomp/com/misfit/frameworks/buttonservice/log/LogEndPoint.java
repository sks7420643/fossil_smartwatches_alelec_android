package com.misfit.frameworks.buttonservice.log;

import com.fossil.Vt7;
import com.mapped.Wg6;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogEndPoint {
    @DexIgnore
    public static /* final */ LogEndPoint INSTANCE; // = new LogEndPoint();
    @DexIgnore
    public static LogApiService logApiService;

    @DexIgnore
    public final LogApiService getLogApiService() {
        return logApiService;
    }

    @DexIgnore
    public final void init(String str, String str2, String str3, String str4) {
        Wg6.c(str, "logBrandName");
        Wg6.c(str2, "logBaseUrl");
        Wg6.c(str3, "accessKey");
        Wg6.c(str4, "secretKey");
        if ((!Vt7.l(str2)) && (!Vt7.l(str3)) && (!Vt7.l(str4))) {
            if (!Vt7.i(str2, "/", false, 2, null)) {
                str2 = str2 + "/";
            }
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(LogEndPoint$init$httpLogInterceptor$Anon1.INSTANCE);
            httpLoggingInterceptor.c(Wg6.a("release", "release") ? HttpLoggingInterceptor.a.BASIC : HttpLoggingInterceptor.a.BODY);
            Retrofit.b bVar = new Retrofit.b();
            bVar.b(str2);
            bVar.a(GsonConverterFactory.f());
            OkHttpClient.b bVar2 = new OkHttpClient.b();
            bVar2.a(new LogInterceptor(str, str3, str4));
            bVar2.a(httpLoggingInterceptor);
            bVar.f(bVar2.d());
            logApiService = (LogApiService) bVar.d().b(LogApiService.class);
        }
    }

    @DexIgnore
    public final void setLogApiService(LogApiService logApiService2) {
        logApiService = logApiService2;
    }
}
