package com.misfit.frameworks.buttonservice.log;

import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Hr7;
import com.fossil.Jv7;
import com.fossil.U08;
import com.fossil.Ux7;
import com.fossil.W08;
import com.mapped.Il6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.extensions.SynchronizeQueue;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileLogWriter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ int FILE_LOG_SIZE_THRESHOLD; // = 5242880;
    @DexIgnore
    public static /* final */ String FILE_NAME_PATTERN; // = "app_log_%s.txt";
    @DexIgnore
    public static /* final */ String LOG_FOLDER; // = "logs";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public FileDebugOption debugOption;
    @DexIgnore
    public String directoryPath; // = "";
    @DexIgnore
    public /* final */ SynchronizeQueue<String> logEventQueue; // = new SynchronizeQueue<>();
    @DexIgnore
    public int mCount;
    @DexIgnore
    public /* final */ U08 mFileLogWriterMutex; // = W08.b(false, 1, null);
    @DexIgnore
    public /* final */ Il6 mFileLogWriterScope; // = Jv7.a(Bw7.b().plus(Ux7.b(null, 1, null)));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String name = FileLogWriter.class.getName();
        Wg6.b(name, "FileLogWriter::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    private final String getFilePath(String str) {
        return this.directoryPath + File.separatorChar + LOG_FOLDER + File.separatorChar + str;
    }

    @DexIgnore
    private final Rm6 pollLogEvent() {
        return Gu7.d(this.mFileLogWriterScope, null, null, new FileLogWriter$pollLogEvent$Anon1(this, null), 3, null);
    }

    @DexIgnore
    private final void rotateFiles() throws Exception {
        synchronized (this) {
            for (int i = 2; i >= 0; i--) {
                Hr7 hr7 = Hr7.a;
                String format = String.format(FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                File file = new File(getFilePath(format));
                if (file.exists()) {
                    FileChannel channel = new RandomAccessFile(file, "rw").getChannel();
                    FileLock lock = channel.lock();
                    if (i >= 2) {
                        file.delete();
                    } else {
                        Hr7 hr72 = Hr7.a;
                        String format2 = String.format(FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{Integer.valueOf(i + 1)}, 1));
                        Wg6.b(format2, "java.lang.String.format(format, *args)");
                        file.renameTo(new File(getFilePath(format2)));
                    }
                    lock.release();
                    channel.close();
                }
            }
            Hr7 hr73 = Hr7.a;
            String format3 = String.format(FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{0}, 1));
            Wg6.b(format3, "java.lang.String.format(format, *args)");
            new File(getFilePath(format3)).createNewFile();
        }
    }

    @DexIgnore
    public final List<File> exportLogs() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i <= 2; i++) {
            Hr7 hr7 = Hr7.a;
            String format = String.format(FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            File file = new File(getFilePath(format));
            if (file.exists()) {
                arrayList.add(file);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final void startWriter(String str) {
        Wg6.c(str, "directoryPath");
        this.directoryPath = str;
        pollLogEvent();
    }

    @DexIgnore
    public final void startWriter(String str, FileDebugOption fileDebugOption) {
        Wg6.c(str, "directoryPath");
        this.debugOption = fileDebugOption;
        this.mCount = 0;
        startWriter(str);
    }

    @DexIgnore
    public final void writeLog(String str) {
        Wg6.c(str, "logMessage");
        this.logEventQueue.add(str);
        pollLogEvent();
    }
}
