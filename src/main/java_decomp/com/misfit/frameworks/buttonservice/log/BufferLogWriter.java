package com.misfit.frameworks.buttonservice.log;

import com.fossil.At7;
import com.fossil.Bw7;
import com.fossil.Em7;
import com.fossil.Gu7;
import com.fossil.Hr7;
import com.fossil.Jv7;
import com.fossil.Pm7;
import com.fossil.U08;
import com.fossil.Ux7;
import com.fossil.Vt7;
import com.fossil.W08;
import com.google.gson.Gson;
import com.mapped.Il6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.extensions.SynchronizeQueue;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferLogWriter {
    @DexIgnore
    public static /* final */ String ALL_BUFFER_FILE_NAME_REGEX_PATTERN; // = "\\w*buffer_log\\w*\\.txt";
    @DexIgnore
    public static /* final */ String BUFFER_FILE_NAME; // = "%s_buffer_log.txt";
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String FULL_BUFFER_FILE_NAME_PATTERN; // = "%s_buffer_log_%s.txt";
    @DexIgnore
    public static /* final */ String FULL_BUFFER_FILE_NAME_REGEX_PATTERN; // = "\\w*buffer_log_\\d+\\.txt";
    @DexIgnore
    public static /* final */ String LOG_FOLDER; // = "buffer_logs";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public IBufferLogCallback callback;
    @DexIgnore
    public BufferDebugOption debugOption;
    @DexIgnore
    public String directoryPath; // = "";
    @DexIgnore
    public /* final */ String floggerName;
    @DexIgnore
    public boolean isMainFLogger;
    @DexIgnore
    public /* final */ SynchronizeQueue<LogEvent> logEventQueue; // = new SynchronizeQueue<>();
    @DexIgnore
    public String logFilePath; // = "";
    @DexIgnore
    public /* final */ U08 mBufferLogMutex; // = W08.b(false, 1, null);
    @DexIgnore
    public /* final */ Il6 mBufferLogScope; // = Jv7.a(Bw7.b().plus(Ux7.b(null, 1, null)));
    @DexIgnore
    public /* final */ int threshold;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface IBufferLogCallback {
        @DexIgnore
        void onFullBuffer(List<LogEvent> list, boolean z);

        @DexIgnore
        void onWrittenSummaryLog(LogEvent logEvent);
    }

    /*
    static {
        String name = BufferLogWriter.class.getName();
        Wg6.b(name, "BufferLogWriter::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    public BufferLogWriter(String str, int i) {
        Wg6.c(str, "floggerName");
        this.floggerName = str;
        this.threshold = i;
    }

    @DexIgnore
    private final String getFullBufferLogFileName(long j) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.directoryPath);
        sb.append(File.separatorChar);
        Hr7 hr7 = Hr7.a;
        String format = String.format(FULL_BUFFER_FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{this.floggerName, Long.valueOf(j), Locale.US}, 3));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        sb.append(format);
        return sb.toString();
    }

    @DexIgnore
    private final Rm6 pollEventQueue() {
        return Gu7.d(this.mBufferLogScope, null, null, new BufferLogWriter$pollEventQueue$Anon1(this, null), 3, null);
    }

    @DexIgnore
    private final List<LogEvent> toLogEvents(List<String> list) {
        return At7.u(At7.p(Pm7.z(list), new BufferLogWriter$toLogEvents$Anon1(new Gson())));
    }

    @DexIgnore
    public final List<File> exportLogs() {
        List<File> f0;
        File[] listFiles = new File(this.directoryPath).listFiles(BufferLogWriter$exportLogs$Anon1.INSTANCE);
        return (listFiles == null || (f0 = Em7.f0(listFiles)) == null) ? new ArrayList() : f0;
    }

    @DexIgnore
    public final void forceFlushBuffer() {
        writeLog(new FlushLogEvent());
    }

    @DexIgnore
    public final IBufferLogCallback getCallback() {
        return this.callback;
    }

    @DexIgnore
    public final float getLogFileSize() {
        return ConversionUtils.INSTANCE.convertByteToMegaBytes(new File(this.logFilePath).length());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0088, code lost:
        if (r0 != null) goto L_0x008a;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object renameToFullBufferFile(java.io.File r11, boolean r12, com.mapped.Xe6<? super com.mapped.Cd6> r13) {
        /*
        // Method dump skipped, instructions count: 278
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.BufferLogWriter.renameToFullBufferFile(java.io.File, boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void setCallback(IBufferLogCallback iBufferLogCallback) {
        this.callback = iBufferLogCallback;
    }

    @DexIgnore
    public final void startWriter(String str, boolean z, IBufferLogCallback iBufferLogCallback) {
        String str2;
        Wg6.c(str, "directoryPath");
        Wg6.c(iBufferLogCallback, Constants.CALLBACK);
        this.callback = iBufferLogCallback;
        String str3 = str + File.separatorChar + LOG_FOLDER;
        this.directoryPath = str3;
        if (!Vt7.l(str3)) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.directoryPath);
            sb.append(File.separatorChar);
            Hr7 hr7 = Hr7.a;
            String format = String.format(BUFFER_FILE_NAME, Arrays.copyOf(new Object[]{this.floggerName, Locale.US}, 2));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            sb.append(format);
            str2 = sb.toString();
        } else {
            str2 = "";
        }
        this.logFilePath = str2;
        this.isMainFLogger = z;
        pollEventQueue();
    }

    @DexIgnore
    public final void startWriter(String str, boolean z, IBufferLogCallback iBufferLogCallback, BufferDebugOption bufferDebugOption) {
        Wg6.c(str, "directoryPath");
        Wg6.c(iBufferLogCallback, Constants.CALLBACK);
        this.debugOption = bufferDebugOption;
        startWriter(str, z, iBufferLogCallback);
    }

    @DexIgnore
    public final void writeLog(LogEvent logEvent) {
        Wg6.c(logEvent, "logEvent");
        this.logEventQueue.add(logEvent);
        pollEventQueue();
    }
}
