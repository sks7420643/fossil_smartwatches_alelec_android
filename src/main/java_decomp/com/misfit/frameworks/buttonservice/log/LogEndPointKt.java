package com.misfit.frameworks.buttonservice.log;

import com.fossil.Go7;
import com.fossil.Vn7;
import com.fossil.Xn7;
import com.fossil.Yn7;
import com.mapped.Xe6;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogEndPointKt {
    @DexIgnore
    public static final <T> Object await(Call<T> call, Xe6<? super RepoResponse<T>> xe6) {
        Vn7 vn7 = new Vn7(Xn7.c(xe6));
        call.D(new LogEndPointKt$await$Anon2$Anon1_Level2(vn7));
        Object a2 = vn7.a();
        if (a2 == Yn7.d()) {
            Go7.c(xe6);
        }
        return a2;
    }
}
