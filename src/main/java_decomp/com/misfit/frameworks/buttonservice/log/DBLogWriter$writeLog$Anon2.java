package com.misfit.frameworks.buttonservice.log;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.DBLogWriter;
import com.misfit.frameworks.buttonservice.log.db.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$writeLog$2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$writeLog$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$writeLog$Anon2(DBLogWriter dBLogWriter, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = dBLogWriter;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        DBLogWriter$writeLog$Anon2 dBLogWriter$writeLog$Anon2 = new DBLogWriter$writeLog$Anon2(this.this$0, xe6);
        dBLogWriter$writeLog$Anon2.p$ = (Il6) obj;
        throw null;
        //return dBLogWriter$writeLog$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((DBLogWriter$writeLog$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        DBLogWriter.IDBLogWriterCallback access$getCallback$p;
        Yn7.d();
        if (this.label == 0) {
            El7.b(obj);
            int countExcept = DBLogWriter.access$getLogDao$p(this.this$0).countExcept(Log.Flag.SYNCING);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = DBLogWriter.TAG;
            local.d(str, ".writeLog(), dbCount=" + countExcept);
            if (countExcept >= this.this$0.thresholdValue && (access$getCallback$p = DBLogWriter.access$getCallback$p(this.this$0)) != null) {
                access$getCallback$p.onReachDBThreshold();
            }
            return Cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
