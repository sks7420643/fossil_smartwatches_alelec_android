package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Hw0;
import com.fossil.Jv7;
import com.mapped.Oh;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class LogDatabase extends Oh {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "LogDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public abstract LogDao getLogDao();

    @DexIgnore
    @Override // com.mapped.Oh
    public void init(Hw0 hw0) {
        Wg6.c(hw0, "configuration");
        super.init(hw0);
        try {
            Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new LogDatabase$init$Anon1(this, null), 3, null);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(TAG, "exception when init database " + e);
        }
    }
}
