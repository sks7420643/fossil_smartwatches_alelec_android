package com.misfit.frameworks.buttonservice.log;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.facebook.appevents.UserDataStore;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Pm7;
import com.fossil.Wt7;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Ku3;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.BufferLogWriter;
import com.misfit.frameworks.buttonservice.log.DBLogWriter;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.misfit.frameworks.buttonservice.log.model.CloudLogConfig;
import com.misfit.frameworks.buttonservice.log.model.OtaDetailLog;
import com.misfit.frameworks.buttonservice.log.model.RemoveDeviceLog;
import com.misfit.frameworks.buttonservice.log.model.SessionDetailInfo;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.PinObject;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemoteFLogger implements IRemoteFLogger, BufferLogWriter.IBufferLogCallback, DBLogWriter.IDBLogWriterCallback {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String LOG_MESSAGE_INTENT_ACTION; // = "fossil.log.action.messages";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_END_SESSION; // = "action:end_session";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_ERROR_RECORDED; // = "action:error_recorded";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_FLUSH; // = "action:flush_data";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_FULL_BUFFER; // = "action:full_buffer";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_KEY; // = "action";
    @DexIgnore
    public static /* final */ String MESSAGE_ACTION_START_SESSION; // = "action:start_session";
    @DexIgnore
    public static /* final */ String MESSAGE_PARAM_ERROR_CODE; // = "param:error";
    @DexIgnore
    public static /* final */ String MESSAGE_PARAM_SERIAL; // = "param:serial";
    @DexIgnore
    public static /* final */ String MESSAGE_PARAM_SUMMARY_KEY; // = "param:summary_key";
    @DexIgnore
    public static /* final */ String MESSAGE_SENDER_KEY; // = "sender";
    @DexIgnore
    public static /* final */ String MESSAGE_TARGET_KEY; // = "target";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public ActiveDeviceInfo activeDeviceInfo; // = new ActiveDeviceInfo("", "", "");
    @DexIgnore
    public AppLogInfo appLogInfo; // = new AppLogInfo("", "", "", "", "", "", "");
    @DexIgnore
    public BufferLogWriter bufferLogWriter;
    @DexIgnore
    public Context context;
    @DexIgnore
    public DBLogWriter dbLogWriter;
    @DexIgnore
    public String floggerName; // = "";
    @DexIgnore
    public int flushLogTimeThreshold; // = LogConfiguration.RELEASE_FLUSH_LOG_TIME_THRESHOLD;
    @DexIgnore
    public boolean isDebuggable; // = true;
    @DexIgnore
    public volatile boolean isFlushing;
    @DexIgnore
    public boolean isInitialized;
    @DexIgnore
    public boolean isMainFLogger;
    @DexIgnore
    public boolean isPrintToConsole; // = true;
    @DexIgnore
    public long lastSyncTime;
    @DexIgnore
    public /* final */ BroadcastReceiver mMessageBroadcastReceiver; // = new RemoteFLogger$mMessageBroadcastReceiver$Anon1(this);
    @DexIgnore
    public IRemoteLogWriter remoteLogWriter;
    @DexIgnore
    public SessionDetailInfo sessionDetailInfo; // = new SessionDetailInfo(-1, 0, 0);
    @DexIgnore
    public /* final */ HashMap<String, SessionSummary> summarySessionMap; // = new HashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class ErrorEvent {
        @DexIgnore
        public /* final */ FLogger.Component component;
        @DexIgnore
        public /* final */ String error;
        @DexIgnore
        public /* final */ String errorCode;
        @DexIgnore
        public /* final */ String step;

        @DexIgnore
        public ErrorEvent(String str, String str2, FLogger.Component component2, String str3) {
            Wg6.c(str, "errorCode");
            Wg6.c(str2, "step");
            Wg6.c(component2, "component");
            Wg6.c(str3, "error");
            this.errorCode = str;
            this.step = str2;
            this.component = component2;
            this.error = str3;
        }

        @DexIgnore
        public final FLogger.Component getComponent() {
            return this.component;
        }

        @DexIgnore
        public final String getError() {
            return this.error;
        }

        @DexIgnore
        public final String getErrorCode() {
            return this.errorCode;
        }

        @DexIgnore
        public final String getStep() {
            return this.step;
        }

        @DexIgnore
        public final String toJsonString() {
            String t = new Gson().t(this);
            Wg6.b(t, "Gson().toJson(this)");
            return t;
        }

        @DexIgnore
        public String toString() {
            return toJsonString();
        }
    }

    @DexIgnore
    public enum MessageTarget {
        TO_ALL_FLOGGER(1),
        TO_MAIN_FLOGGER(2),
        TO_ALL_EXCEPT_MAIN_FLOGGER(3);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        public /* final */ int value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(Qg6 qg6) {
                this();
            }

            @DexIgnore
            public final MessageTarget fromValue(int i) {
                MessageTarget messageTarget;
                MessageTarget[] values = MessageTarget.values();
                int length = values.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        messageTarget = null;
                        break;
                    }
                    messageTarget = values[i2];
                    if (messageTarget.getValue() == i) {
                        break;
                    }
                    i2++;
                }
                return messageTarget != null ? messageTarget : MessageTarget.TO_ALL_FLOGGER;
            }
        }

        @DexIgnore
        public MessageTarget(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class SessionSummary {
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        @Vu3("details")
        public Object details;
        @DexIgnore
        @Vu3("end_timestamp")
        public long endTimeStamp;
        @DexIgnore
        @Vu3("errors")
        public /* final */ List<String> errors; // = new ArrayList();
        @DexIgnore
        @Vu3("failure_reason")
        public String failureReason; // = "";
        @DexIgnore
        @Vu3("is_success")
        public boolean isSuccess; // = true;
        @DexIgnore
        @Vu3("start_timestamp")
        public long startTimeStamp;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(Qg6 qg6) {
                this();
            }

            @DexIgnore
            public final String getSummaryKey(String str, FLogger.Session session) {
                Wg6.c(str, "serial");
                Wg6.c(session, Constants.SESSION);
                return str + ':' + session;
            }
        }

        @DexIgnore
        public SessionSummary(long j, long j2) {
            this.startTimeStamp = j;
            this.endTimeStamp = j2;
        }

        @DexIgnore
        public final Object getDetails() {
            return this.details;
        }

        @DexIgnore
        public final long getEndTimeStamp() {
            return this.endTimeStamp;
        }

        @DexIgnore
        public final List<String> getErrors() {
            return this.errors;
        }

        @DexIgnore
        public final String getFailureReason() {
            return this.failureReason;
        }

        @DexIgnore
        public final long getStartTimeStamp() {
            return this.startTimeStamp;
        }

        @DexIgnore
        public final boolean isSuccess() {
            return this.isSuccess;
        }

        @DexIgnore
        public final void setDetails(Object obj) {
            this.details = obj;
        }

        @DexIgnore
        public final void setEndTimeStamp(long j) {
            this.endTimeStamp = j;
        }

        @DexIgnore
        public final void setStartTimeStamp(long j) {
            this.startTimeStamp = j;
        }

        @DexIgnore
        public final String toJsonString() {
            String t = FLogUtils.INSTANCE.getGsonForLogEvent().t(this);
            Wg6.b(t, "FLogUtils.getGsonForLogEvent().toJson(this)");
            return t;
        }

        @DexIgnore
        public String toString() {
            return toJsonString();
        }

        @DexIgnore
        public final void update(int i) {
            this.isSuccess = i == 0;
            this.failureReason = this.errors.isEmpty() ^ true ? (String) Pm7.P(this.errors) : "";
            if (!this.isSuccess && this.errors.isEmpty()) {
                this.errors.add(String.valueOf(i));
                this.failureReason = (String) Pm7.P(this.errors);
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = RemoteFLogger.TAG;
            local.d(str, "failureReason " + this.failureReason + " errors " + this.errors + " finalCode " + i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$1;

        /*
        static {
            int[] iArr = new int[MessageTarget.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[MessageTarget.TO_MAIN_FLOGGER.ordinal()] = 1;
            $EnumSwitchMapping$0[MessageTarget.TO_ALL_FLOGGER.ordinal()] = 2;
            $EnumSwitchMapping$0[MessageTarget.TO_ALL_EXCEPT_MAIN_FLOGGER.ordinal()] = 3;
            int[] iArr2 = new int[FLogger.LogLevel.values().length];
            $EnumSwitchMapping$1 = iArr2;
            iArr2[FLogger.LogLevel.DEBUG.ordinal()] = 1;
            $EnumSwitchMapping$1[FLogger.LogLevel.INFO.ordinal()] = 2;
            $EnumSwitchMapping$1[FLogger.LogLevel.ERROR.ordinal()] = 3;
            $EnumSwitchMapping$1[FLogger.LogLevel.SUMMARY.ordinal()] = 4;
        }
        */
    }

    /*
    static {
        String name = RemoteFLogger.class.getName();
        Wg6.b(name, "RemoteFLogger::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    private final String getInternalIntentAction() {
        Context context2 = this.context;
        return Wg6.h(context2 != null ? context2.getPackageName() : null, LOG_MESSAGE_INTENT_ACTION);
    }

    @DexIgnore
    private final Object getSessionDetails(String str, String str2) {
        return Wt7.v(str, FLogger.Session.SYNC.name(), false, 2, null) ? this.sessionDetailInfo : Wt7.v(str, FLogger.Session.OTA.name(), false, 2, null) ? new OtaDetailLog(this.sessionDetailInfo.getBatteryLevel()) : Wt7.v(str, FLogger.Session.PAIR.name(), false, 2, null) ? new OtaDetailLog(this.sessionDetailInfo.getBatteryLevel()) : Wt7.v(str, FLogger.Session.REMOVE_DEVICE.name(), false, 2, null) ? new RemoveDeviceLog(str2) : "";
    }

    @DexIgnore
    private final void registerBroadcastMessages(Context context2) {
        context2.registerReceiver(this.mMessageBroadcastReceiver, new IntentFilter(getInternalIntentAction()));
    }

    @DexIgnore
    private final void sendInternalMessage(String str, String str2, MessageTarget messageTarget, Bundle bundle) {
        Intent intent = new Intent();
        intent.setAction(getInternalIntentAction());
        intent.putExtra(MESSAGE_SENDER_KEY, str2);
        intent.putExtra("action", str);
        intent.putExtra("target", messageTarget.getValue());
        intent.putExtras(bundle);
        Context context2 = this.context;
        if (context2 != null) {
            context2.sendBroadcast(intent);
        }
    }

    @DexIgnore
    private final void startSession(String str, String str2) {
        SessionSummary sessionSummary = this.summarySessionMap.get(str);
        if (sessionSummary != null) {
            sessionSummary.setStartTimeStamp(System.currentTimeMillis());
            sessionSummary.setEndTimeStamp(-1);
            sessionSummary.getErrors().clear();
            SessionSummary sessionSummary2 = this.summarySessionMap.get(str);
            if (sessionSummary2 != null) {
                sessionSummary2.setDetails(getSessionDetails(str, str2));
                return;
            }
            return;
        }
        this.summarySessionMap.put(str, new SessionSummary(System.currentTimeMillis(), -1));
        SessionSummary sessionSummary3 = this.summarySessionMap.get(str);
        if (sessionSummary3 != null) {
            sessionSummary3.setDetails(getSessionDetails(str, str2));
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void d(FLogger.Component component, FLogger.Session session, String str, String str2, String str3) {
        Wg6.c(component, "component");
        Wg6.c(session, Constants.SESSION);
        Wg6.c(str, "serial");
        Wg6.c(str2, PinObject.COLUMN_CLASS_NAME);
        Wg6.c(str3, "message");
        if (this.isInitialized && this.isDebuggable) {
            log(FLogger.LogLevel.DEBUG, component, session, str, str2, str3);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void e(FLogger.Component component, FLogger.Session session, String str, String str2, String str3) {
        Wg6.c(component, "component");
        Wg6.c(session, Constants.SESSION);
        Wg6.c(str, "serial");
        Wg6.c(str2, PinObject.COLUMN_CLASS_NAME);
        Wg6.c(str3, "message");
        if (this.isInitialized) {
            log(FLogger.LogLevel.ERROR, component, session, str, str2, str3);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void e(FLogger.Component component, FLogger.Session session, String str, String str2, String str3, ErrorCodeBuilder.Step step, String str4) {
        List<String> errors;
        Wg6.c(component, "component");
        Wg6.c(session, Constants.SESSION);
        Wg6.c(str, "serial");
        Wg6.c(str2, PinObject.COLUMN_CLASS_NAME);
        Wg6.c(str3, "errorCode");
        Wg6.c(step, "step");
        Wg6.c(str4, "errorMessage");
        if (!this.isInitialized) {
            return;
        }
        if (log(FLogger.LogLevel.ERROR, component, session, str, str2, new ErrorEvent(str3, step.getNameValue(), component, str4).toJsonString())) {
            String summaryKey = SessionSummary.Companion.getSummaryKey(str, session);
            SessionSummary sessionSummary = this.summarySessionMap.get(summaryKey);
            if (!(sessionSummary == null || (errors = sessionSummary.getErrors()) == null)) {
                errors.add(str3);
            }
            String str5 = this.floggerName;
            MessageTarget messageTarget = MessageTarget.TO_ALL_FLOGGER;
            Bundle bundle = new Bundle();
            bundle.putString(MESSAGE_PARAM_SUMMARY_KEY, summaryKey);
            bundle.putString(MESSAGE_PARAM_ERROR_CODE, str3);
            sendInternalMessage(MESSAGE_ACTION_ERROR_RECORDED, str5, messageTarget, bundle);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public List<File> exportAppLogs() {
        List<File> arrayList;
        ArrayList arrayList2 = new ArrayList();
        BufferLogWriter bufferLogWriter2 = this.bufferLogWriter;
        if (bufferLogWriter2 == null || (arrayList = bufferLogWriter2.exportLogs()) == null) {
            arrayList = new ArrayList<>();
        }
        arrayList2.addAll(arrayList);
        return arrayList2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void flush() {
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new RemoteFLogger$flush$Anon1(this, null), 3, null);
    }

    @DexIgnore
    public final /* synthetic */ Object flushBuffer(Xe6<? super Cd6> xe6) {
        Cd6 cd6;
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "Calendar.getInstance()");
        this.lastSyncTime = instance.getTimeInMillis();
        BufferLogWriter bufferLogWriter2 = this.bufferLogWriter;
        if (bufferLogWriter2 != null) {
            bufferLogWriter2.forceFlushBuffer();
            cd6 = Cd6.a;
        } else {
            cd6 = null;
        }
        return cd6 == Yn7.d() ? cd6 : Cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object flushDB(com.mapped.Xe6<? super com.mapped.Cd6> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.misfit.frameworks.buttonservice.log.RemoteFLogger$flushDB$Anon1
            if (r0 == 0) goto L_0x002e
            r0 = r6
            com.misfit.frameworks.buttonservice.log.RemoteFLogger$flushDB$Anon1 r0 = (com.misfit.frameworks.buttonservice.log.RemoteFLogger$flushDB$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002e
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x003d
            if (r3 != r4) goto L_0x0035
            java.lang.Object r0 = r1.L$1
            com.misfit.frameworks.buttonservice.log.IRemoteLogWriter r0 = (com.misfit.frameworks.buttonservice.log.IRemoteLogWriter) r0
            java.lang.Object r0 = r1.L$0
            com.misfit.frameworks.buttonservice.log.RemoteFLogger r0 = (com.misfit.frameworks.buttonservice.log.RemoteFLogger) r0
            com.fossil.El7.b(r2)
        L_0x002b:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x002d:
            return r0
        L_0x002e:
            com.misfit.frameworks.buttonservice.log.RemoteFLogger$flushDB$Anon1 r0 = new com.misfit.frameworks.buttonservice.log.RemoteFLogger$flushDB$Anon1
            r0.<init>(r5, r6)
            r1 = r0
            goto L_0x0014
        L_0x0035:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003d:
            com.fossil.El7.b(r2)
            boolean r2 = r5.isMainFLogger
            if (r2 == 0) goto L_0x002b
            com.misfit.frameworks.buttonservice.log.IRemoteLogWriter r2 = r5.remoteLogWriter
            if (r2 == 0) goto L_0x002b
            com.misfit.frameworks.buttonservice.log.DBLogWriter r3 = r5.dbLogWriter
            if (r3 == 0) goto L_0x002b
            r1.L$0 = r5
            r1.L$1 = r2
            r1.label = r4
            java.lang.Object r1 = r3.flushTo(r2, r1)
            if (r1 != r0) goto L_0x002b
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.RemoteFLogger.flushDB(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void i(FLogger.Component component, FLogger.Session session, String str, String str2, String str3) {
        Wg6.c(component, "component");
        Wg6.c(session, Constants.SESSION);
        Wg6.c(str, "serial");
        Wg6.c(str2, PinObject.COLUMN_CLASS_NAME);
        Wg6.c(str3, "message");
        if (this.isInitialized) {
            log(FLogger.LogLevel.INFO, component, session, str, str2, str3);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void init(String str, AppLogInfo appLogInfo2, ActiveDeviceInfo activeDeviceInfo2, CloudLogConfig cloudLogConfig, Context context2, boolean z, boolean z2) {
        Wg6.c(str, "name");
        Wg6.c(appLogInfo2, "appLogInfo");
        Wg6.c(activeDeviceInfo2, "activeDeviceInfo");
        Wg6.c(cloudLogConfig, "cloudLogConfig");
        Wg6.c(context2, "context");
        int i = z2 ? 10 : 100;
        int i2 = z2 ? 50 : 500;
        this.flushLogTimeThreshold = z2 ? 1800000 : LogConfiguration.RELEASE_FLUSH_LOG_TIME_THRESHOLD;
        this.floggerName = str;
        this.appLogInfo = appLogInfo2;
        this.activeDeviceInfo = activeDeviceInfo2;
        this.context = context2;
        BufferLogWriter bufferLogWriter2 = new BufferLogWriter(str, i);
        this.bufferLogWriter = bufferLogWriter2;
        if (bufferLogWriter2 != null) {
            bufferLogWriter2.setCallback(this);
        }
        BufferLogWriter bufferLogWriter3 = this.bufferLogWriter;
        if (bufferLogWriter3 != null) {
            String file = context2.getFilesDir().toString();
            Wg6.b(file, "context.filesDir.toString()");
            bufferLogWriter3.startWriter(file, z, this);
        }
        this.dbLogWriter = new DBLogWriter(context2, i2, this);
        updateCloudLogConfig(cloudLogConfig);
        registerBroadcastMessages(context2);
        this.isMainFLogger = z;
        this.isDebuggable = z2;
        this.isInitialized = true;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0185  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean log(com.misfit.frameworks.buttonservice.log.FLogger.LogLevel r20, com.misfit.frameworks.buttonservice.log.FLogger.Component r21, com.misfit.frameworks.buttonservice.log.FLogger.Session r22, java.lang.String r23, java.lang.String r24, java.lang.String r25) {
        /*
        // Method dump skipped, instructions count: 535
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.RemoteFLogger.log(com.misfit.frameworks.buttonservice.log.FLogger$LogLevel, com.misfit.frameworks.buttonservice.log.FLogger$Component, com.misfit.frameworks.buttonservice.log.FLogger$Session, java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.DBLogWriter.IDBLogWriterCallback
    public void onDeleteLogs() {
        Ku3 ku3 = new Ku3();
        BufferLogWriter bufferLogWriter2 = this.bufferLogWriter;
        ku3.n("app_log", String.valueOf(bufferLogWriter2 != null ? bufferLogWriter2.getLogFileSize() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
        ku3.n(UserDataStore.DATE_OF_BIRTH, "");
        ku3.n("cache", "");
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.OTHER;
        String deviceSerial = this.activeDeviceInfo.getDeviceSerial();
        String str = TAG;
        String jsonElement = ku3.toString();
        Wg6.b(jsonElement, "jsonObject.toString()");
        i(component, session, deviceSerial, str, jsonElement);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.BufferLogWriter.IBufferLogCallback
    public void onFullBuffer(List<LogEvent> list, boolean z) {
        Wg6.c(list, "logLines");
        Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new RemoteFLogger$onFullBuffer$Anon1(this, list, z, null), 3, null);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.DBLogWriter.IDBLogWriterCallback
    public void onReachDBThreshold() {
        if (this.isMainFLogger) {
            Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new RemoteFLogger$onReachDBThreshold$Anon1(this, null), 3, null);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.BufferLogWriter.IBufferLogCallback
    public void onWrittenSummaryLog(LogEvent logEvent) {
        Wg6.c(logEvent, "logEvent");
        if (this.isDebuggable) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".onWrittenSummaryLog(), logEvent=" + logEvent);
            Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new RemoteFLogger$onWrittenSummaryLog$Anon1(this, null), 3, null);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void setPrintToConsole(boolean z) {
        this.isPrintToConsole = z;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void startSession(FLogger.Session session, String str, String str2) {
        Wg6.c(session, Constants.SESSION);
        Wg6.c(str, "serial");
        Wg6.c(str2, PinObject.COLUMN_CLASS_NAME);
        if (this.isInitialized) {
            String summaryKey = SessionSummary.Companion.getSummaryKey(str, session);
            startSession(summaryKey, str);
            String str3 = this.floggerName;
            MessageTarget messageTarget = MessageTarget.TO_ALL_FLOGGER;
            Bundle bundle = new Bundle();
            bundle.putString(MESSAGE_PARAM_SUMMARY_KEY, summaryKey);
            bundle.putString(MESSAGE_PARAM_SERIAL, str);
            sendInternalMessage(MESSAGE_ACTION_START_SESSION, str3, messageTarget, bundle);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void summary(int i, FLogger.Component component, FLogger.Session session, String str, String str2) {
        Wg6.c(component, "component");
        Wg6.c(session, Constants.SESSION);
        Wg6.c(str, "serial");
        Wg6.c(str2, PinObject.COLUMN_CLASS_NAME);
        if (this.isInitialized) {
            String summaryKey = SessionSummary.Companion.getSummaryKey(str, session);
            long currentTimeMillis = System.currentTimeMillis();
            SessionSummary sessionSummary = this.summarySessionMap.get(summaryKey);
            if (sessionSummary != null) {
                sessionSummary.update(i);
                sessionSummary.setEndTimeStamp(currentTimeMillis);
                if (this.isInitialized) {
                    log(FLogger.LogLevel.SUMMARY, component, session, str, str2, sessionSummary.toJsonString());
                }
                this.summarySessionMap.remove(summaryKey);
            }
            String str3 = this.floggerName;
            MessageTarget messageTarget = MessageTarget.TO_ALL_FLOGGER;
            Bundle bundle = new Bundle();
            bundle.putString(MESSAGE_PARAM_SUMMARY_KEY, summaryKey);
            sendInternalMessage(MESSAGE_ACTION_END_SESSION, str3, messageTarget, bundle);
        }
    }

    @DexIgnore
    public final void unregisterBroadcastMessage(Context context2) {
        Wg6.c(context2, "context");
        context2.unregisterReceiver(this.mMessageBroadcastReceiver);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void updateActiveDeviceInfo(ActiveDeviceInfo activeDeviceInfo2) {
        Wg6.c(activeDeviceInfo2, "activeDeviceInfo");
        this.activeDeviceInfo = activeDeviceInfo2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void updateAppLogInfo(AppLogInfo appLogInfo2) {
        Wg6.c(appLogInfo2, "appLogInfo");
        this.appLogInfo = appLogInfo2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void updateCloudLogConfig(CloudLogConfig cloudLogConfig) {
        Wg6.c(cloudLogConfig, "cloudLogConfig");
        LogEndPoint.INSTANCE.init(cloudLogConfig.getLogBrandName(), cloudLogConfig.getEndPointBaseUrl(), cloudLogConfig.getAccessKey(), cloudLogConfig.getSecretKey());
        LogApiService logApiService = LogEndPoint.INSTANCE.getLogApiService();
        if (logApiService != null) {
            this.remoteLogWriter = new CloudLogWriter(logApiService);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteFLogger
    public void updateSessionDetailInfo(SessionDetailInfo sessionDetailInfo2) {
        Wg6.c(sessionDetailInfo2, "sessionDetailInfo");
        this.sessionDetailInfo = sessionDetailInfo2;
    }
}
