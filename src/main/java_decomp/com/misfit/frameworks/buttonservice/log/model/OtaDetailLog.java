package com.misfit.frameworks.buttonservice.log.model;

import com.google.gson.Gson;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OtaDetailLog {
    @DexIgnore
    @Vu3("battery_levels")
    public int batteryLevel;

    @DexIgnore
    public OtaDetailLog() {
        this(0, 1, null);
    }

    @DexIgnore
    public OtaDetailLog(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ OtaDetailLog(int i, int i2, Qg6 qg6) {
        this((i2 & 1) != 0 ? 0 : i);
    }

    @DexIgnore
    public final int getBatteryLevel() {
        return this.batteryLevel;
    }

    @DexIgnore
    public final void setBatteryLevel(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }
}
