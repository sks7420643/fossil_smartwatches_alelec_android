package com.misfit.frameworks.buttonservice.log.db;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Hx0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.misfit.frameworks.buttonservice.log.db.Log;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogDao_Impl implements LogDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<Log> __insertionAdapterOfLog;
    @DexIgnore
    public /* final */ LogFlagConverter __logFlagConverter; // = new LogFlagConverter();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<Log> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, Log log) {
            mi.bindLong(1, (long) log.getId());
            mi.bindLong(2, log.getTimeStamp());
            if (log.getContent() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, log.getContent());
            }
            String logFlagEnumToString = LogDao_Impl.this.__logFlagConverter.logFlagEnumToString(log.getCloudFlag());
            if (logFlagEnumToString == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, logFlagEnumToString);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, Log log) {
            bind(mi, log);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `log` (`id`,`timeStamp`,`content`,`cloudFlag`) VALUES (nullif(?, 0),?,?,?)";
        }
    }

    @DexIgnore
    public LogDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfLog = new Anon1(oh);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public int countExcept(Log.Flag flag) {
        int i = 0;
        Rh f = Rh.f("SELECT COUNT(id) FROM log WHERE cloudFlag != ?", 1);
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, logFlagEnumToString);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            if (b.moveToFirst()) {
                i = b.getInt(0);
            }
            return i;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public int delete(List<Integer> list) {
        this.__db.assertNotSuspendingTransaction();
        StringBuilder b = Hx0.b();
        b.append("DELETE FROM log WHERE id IN (");
        Hx0.a(b, list.size());
        b.append(")");
        Mi compileStatement = this.__db.compileStatement(b.toString());
        int i = 1;
        for (Integer num : list) {
            if (num == null) {
                compileStatement.bindNull(i);
            } else {
                compileStatement.bindLong(i, (long) num.intValue());
            }
            i++;
        }
        this.__db.beginTransaction();
        try {
            int executeUpdateDelete = compileStatement.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public List<Log> getAllLogEventsExcept(Log.Flag flag) {
        Rh f = Rh.f("SELECT * FROM log WHERE cloudFlag != ?", 1);
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, logFlagEnumToString);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "timeStamp");
            int c3 = Dx0.c(b, "content");
            int c4 = Dx0.c(b, "cloudFlag");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                Log log = new Log(b.getLong(c2), b.getString(c3), this.__logFlagConverter.stringToLogFlag(b.getString(c4)));
                log.setId(b.getInt(c));
                arrayList.add(log);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public void insertLogEvent(List<Log> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfLog.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.log.db.LogDao
    public void updateCloudFlagByIds(List<Integer> list, Log.Flag flag) {
        this.__db.assertNotSuspendingTransaction();
        StringBuilder b = Hx0.b();
        b.append("UPDATE log SET cloudFlag = ");
        b.append("?");
        b.append(" WHERE id IN (");
        Hx0.a(b, list.size());
        b.append(")");
        Mi compileStatement = this.__db.compileStatement(b.toString());
        String logFlagEnumToString = this.__logFlagConverter.logFlagEnumToString(flag);
        if (logFlagEnumToString == null) {
            compileStatement.bindNull(1);
        } else {
            compileStatement.bindString(1, logFlagEnumToString);
        }
        int i = 2;
        for (Integer num : list) {
            if (num == null) {
                compileStatement.bindNull(i);
            } else {
                compileStatement.bindLong(i, (long) num.intValue());
            }
            i++;
        }
        this.__db.beginTransaction();
        try {
            compileStatement.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
