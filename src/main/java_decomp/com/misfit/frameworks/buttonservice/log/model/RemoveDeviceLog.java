package com.misfit.frameworks.buttonservice.log.model;

import com.google.gson.Gson;
import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemoveDeviceLog {
    @DexIgnore
    @Vu3("current_device")
    public String currentDevice;

    @DexIgnore
    public RemoveDeviceLog(String str) {
        Wg6.c(str, "currentDevice");
        this.currentDevice = str;
    }

    @DexIgnore
    public final String getCurrentDevice() {
        return this.currentDevice;
    }

    @DexIgnore
    public final void setCurrentDevice(String str) {
        Wg6.c(str, "<set-?>");
        this.currentDevice = str;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }
}
