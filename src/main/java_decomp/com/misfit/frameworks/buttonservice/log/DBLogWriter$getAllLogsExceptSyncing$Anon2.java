package com.misfit.frameworks.buttonservice.log;

import com.fossil.At7;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Pm7;
import com.fossil.Qq7;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.db.Log;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$getAllLogsExceptSyncing$2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$getAllLogsExceptSyncing$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super List<LogEvent>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends Qq7 implements Hg6<Log, LogEvent> {
        @DexIgnore
        public /* final */ /* synthetic */ Gson $gson;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(Gson gson) {
            super(1);
            this.$gson = gson;
        }

        @DexIgnore
        public final LogEvent invoke(Log log) {
            Wg6.c(log, "it");
            try {
                LogEvent logEvent = (LogEvent) this.$gson.k(log.getContent(), LogEvent.class);
                logEvent.setTag(Integer.valueOf(log.getId()));
                return logEvent;
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = DBLogWriter.TAG;
                local.e(str, ", getAllLogs(), ex: " + e.getMessage());
                return null;
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ LogEvent invoke(Log log) {
            return invoke(log);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$getAllLogsExceptSyncing$Anon2(DBLogWriter dBLogWriter, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = dBLogWriter;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        DBLogWriter$getAllLogsExceptSyncing$Anon2 dBLogWriter$getAllLogsExceptSyncing$Anon2 = new DBLogWriter$getAllLogsExceptSyncing$Anon2(this.this$0, xe6);
        dBLogWriter$getAllLogsExceptSyncing$Anon2.p$ = (Il6) obj;
        throw null;
        //return dBLogWriter$getAllLogsExceptSyncing$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super List<LogEvent>> xe6) {
        throw null;
        //return ((DBLogWriter$getAllLogsExceptSyncing$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Yn7.d();
        if (this.label == 0) {
            El7.b(obj);
            return At7.u(At7.p(Pm7.z(DBLogWriter.access$getLogDao$p(this.this$0).getAllLogEventsExcept(Log.Flag.SYNCING)), new Anon1_Level2(new Gson())));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
