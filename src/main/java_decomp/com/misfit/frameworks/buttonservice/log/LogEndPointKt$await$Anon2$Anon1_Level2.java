package com.misfit.frameworks.buttonservice.log;

import com.fossil.Dl7;
import com.fossil.Q88;
import com.mapped.Dx6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogEndPointKt$await$Anon2$Anon1_Level2 implements Dx6<T> {
    @DexIgnore
    public /* final */ /* synthetic */ Xe6 $continuation;

    @DexIgnore
    public LogEndPointKt$await$Anon2$Anon1_Level2(Xe6 xe6) {
        this.$continuation = xe6;
    }

    @DexIgnore
    @Override // com.mapped.Dx6
    public void onFailure(Call<T> call, Throwable th) {
        Wg6.c(th, "t");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("await", "onFailure=" + th);
        Xe6 xe6 = this.$continuation;
        Failure create = RepoResponse.Companion.create(th);
        Dl7.Ai ai = Dl7.Companion;
        xe6.resumeWith(Dl7.constructor-impl(create));
    }

    @DexIgnore
    @Override // com.mapped.Dx6
    public void onResponse(Call<T> call, Q88<T> q88) {
        Wg6.c(q88, "response");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("await", "onResponse=" + q88);
        Xe6 xe6 = this.$continuation;
        RepoResponse create = RepoResponse.Companion.create(q88);
        Dl7.Ai ai = Dl7.Companion;
        xe6.resumeWith(Dl7.constructor-impl(create));
    }
}
