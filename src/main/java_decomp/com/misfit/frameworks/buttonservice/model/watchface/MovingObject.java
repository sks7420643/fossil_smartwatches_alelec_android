package com.misfit.frameworks.buttonservice.model.watchface;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MovingObject implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public ComplicationData complicationData;
    @DexIgnore
    public MetricObject metric;
    @DexIgnore
    public TextData textData;
    @DexIgnore
    public TickerData tickerData;
    @DexIgnore
    public MovingType type;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<MovingObject> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public MovingObject createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new MovingObject(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public MovingObject[] newArray(int i) {
            return new MovingObject[i];
        }
    }

    @DexIgnore
    public MovingObject() {
        this(null, null, null, null, null, 31, null);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MovingObject(android.os.Parcel r7) {
        /*
            r6 = this;
            java.lang.String r0 = "parcel"
            com.mapped.Wg6.c(r7, r0)
            java.lang.Class<com.misfit.frameworks.buttonservice.model.watchface.MetricObject> r0 = com.misfit.frameworks.buttonservice.model.watchface.MetricObject.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r1 = r7.readParcelable(r0)
            if (r1 == 0) goto L_0x0049
            com.misfit.frameworks.buttonservice.model.watchface.MetricObject r1 = (com.misfit.frameworks.buttonservice.model.watchface.MetricObject) r1
            java.lang.Class<com.misfit.frameworks.buttonservice.model.watchface.ComplicationData> r0 = com.misfit.frameworks.buttonservice.model.watchface.ComplicationData.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r2 = r7.readParcelable(r0)
            com.misfit.frameworks.buttonservice.model.watchface.ComplicationData r2 = (com.misfit.frameworks.buttonservice.model.watchface.ComplicationData) r2
            java.lang.Class<com.misfit.frameworks.buttonservice.model.watchface.TickerData> r0 = com.misfit.frameworks.buttonservice.model.watchface.TickerData.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r3 = r7.readParcelable(r0)
            com.misfit.frameworks.buttonservice.model.watchface.TickerData r3 = (com.misfit.frameworks.buttonservice.model.watchface.TickerData) r3
            java.lang.Class<com.misfit.frameworks.buttonservice.model.watchface.TextData> r0 = com.misfit.frameworks.buttonservice.model.watchface.TextData.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r4 = r7.readParcelable(r0)
            com.misfit.frameworks.buttonservice.model.watchface.TextData r4 = (com.misfit.frameworks.buttonservice.model.watchface.TextData) r4
            java.lang.String r0 = r7.readString()
            if (r0 == 0) goto L_0x0046
        L_0x003d:
            com.misfit.frameworks.buttonservice.model.watchface.MovingType r5 = com.misfit.frameworks.buttonservice.model.watchface.MovingType.valueOf(r0)
            r0 = r6
            r0.<init>(r1, r2, r3, r4, r5)
            return
        L_0x0046:
            java.lang.String r0 = ""
            goto L_0x003d
        L_0x0049:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.watchface.MovingObject.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public MovingObject(MetricObject metricObject, ComplicationData complicationData2, TickerData tickerData2, TextData textData2, MovingType movingType) {
        Wg6.c(metricObject, "metric");
        Wg6.c(movingType, "type");
        this.metric = metricObject;
        this.complicationData = complicationData2;
        this.tickerData = tickerData2;
        this.textData = textData2;
        this.type = movingType;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MovingObject(MetricObject metricObject, ComplicationData complicationData2, TickerData tickerData2, TextData textData2, MovingType movingType, int i, Qg6 qg6) {
        this((i & 1) != 0 ? new MetricObject(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 15, null) : metricObject, (i & 2) != 0 ? null : complicationData2, (i & 4) != 0 ? null : tickerData2, (i & 8) != 0 ? null : textData2, (i & 16) != 0 ? MovingType.STEP : movingType);
    }

    @DexIgnore
    public static /* synthetic */ MovingObject copy$default(MovingObject movingObject, MetricObject metricObject, ComplicationData complicationData2, TickerData tickerData2, TextData textData2, MovingType movingType, int i, Object obj) {
        return movingObject.copy((i & 1) != 0 ? movingObject.metric : metricObject, (i & 2) != 0 ? movingObject.complicationData : complicationData2, (i & 4) != 0 ? movingObject.tickerData : tickerData2, (i & 8) != 0 ? movingObject.textData : textData2, (i & 16) != 0 ? movingObject.type : movingType);
    }

    @DexIgnore
    public final MetricObject component1() {
        return this.metric;
    }

    @DexIgnore
    public final ComplicationData component2() {
        return this.complicationData;
    }

    @DexIgnore
    public final TickerData component3() {
        return this.tickerData;
    }

    @DexIgnore
    public final TextData component4() {
        return this.textData;
    }

    @DexIgnore
    public final MovingType component5() {
        return this.type;
    }

    @DexIgnore
    public final MovingObject copy(MetricObject metricObject, ComplicationData complicationData2, TickerData tickerData2, TextData textData2, MovingType movingType) {
        Wg6.c(metricObject, "metric");
        Wg6.c(movingType, "type");
        return new MovingObject(metricObject, complicationData2, tickerData2, textData2, movingType);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof MovingObject) {
                MovingObject movingObject = (MovingObject) obj;
                if (!Wg6.a(this.metric, movingObject.metric) || !Wg6.a(this.complicationData, movingObject.complicationData) || !Wg6.a(this.tickerData, movingObject.tickerData) || !Wg6.a(this.textData, movingObject.textData) || !Wg6.a(this.type, movingObject.type)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ComplicationData getComplicationData() {
        return this.complicationData;
    }

    @DexIgnore
    public final MetricObject getMetric() {
        return this.metric;
    }

    @DexIgnore
    public final TextData getTextData() {
        return this.textData;
    }

    @DexIgnore
    public final TickerData getTickerData() {
        return this.tickerData;
    }

    @DexIgnore
    public final MovingType getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        MetricObject metricObject = this.metric;
        int hashCode = metricObject != null ? metricObject.hashCode() : 0;
        ComplicationData complicationData2 = this.complicationData;
        int hashCode2 = complicationData2 != null ? complicationData2.hashCode() : 0;
        TickerData tickerData2 = this.tickerData;
        int hashCode3 = tickerData2 != null ? tickerData2.hashCode() : 0;
        TextData textData2 = this.textData;
        int hashCode4 = textData2 != null ? textData2.hashCode() : 0;
        MovingType movingType = this.type;
        if (movingType != null) {
            i = movingType.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public final void setComplicationData(ComplicationData complicationData2) {
        this.complicationData = complicationData2;
    }

    @DexIgnore
    public final void setMetric(MetricObject metricObject) {
        Wg6.c(metricObject, "<set-?>");
        this.metric = metricObject;
    }

    @DexIgnore
    public final void setTextData(TextData textData2) {
        this.textData = textData2;
    }

    @DexIgnore
    public final void setTickerData(TickerData tickerData2) {
        this.tickerData = tickerData2;
    }

    @DexIgnore
    public final void setType(MovingType movingType) {
        Wg6.c(movingType, "<set-?>");
        this.type = movingType;
    }

    @DexIgnore
    public String toString() {
        return "MovingObject(metric=" + this.metric + ", complicationData=" + this.complicationData + ", tickerData=" + this.tickerData + ", textData=" + this.textData + ", type=" + this.type + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeParcelable(this.metric, i);
        parcel.writeParcelable(this.complicationData, i);
        parcel.writeParcelable(this.tickerData, i);
        parcel.writeParcelable(this.textData, i);
        parcel.writeString(this.type.name());
    }
}
