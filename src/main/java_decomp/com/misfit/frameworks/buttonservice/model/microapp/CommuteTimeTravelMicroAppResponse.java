package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import com.fossil.Hq1;
import com.fossil.Kt1;
import com.fossil.Ry1;
import com.mapped.E90;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeTravelMicroAppResponse extends DeviceAppResponse {
    @DexIgnore
    public int mTravelTimeInMinute;

    @DexIgnore
    public CommuteTimeTravelMicroAppResponse(int i) {
        super(E90.COMMUTE_TIME_TRAVEL_MICRO_APP);
        this.mTravelTimeInMinute = i;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeTravelMicroAppResponse(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        this.mTravelTimeInMinute = parcel.readInt();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceResponse(X90 x90, Ry1 ry1) {
        Wg6.c(x90, "deviceRequest");
        if (!(x90 instanceof Hq1) || ry1 == null) {
            return null;
        }
        return new Kt1((Hq1) x90, ry1, this.mTravelTimeInMinute);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.mTravelTimeInMinute);
    }
}
