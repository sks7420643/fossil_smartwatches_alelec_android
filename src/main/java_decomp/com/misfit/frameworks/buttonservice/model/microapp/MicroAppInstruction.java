package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Tc0;
import com.mapped.Td0;
import com.mapped.Ud0;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class MicroAppInstruction implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<MicroAppInstruction> CREATOR; // = new MicroAppInstruction$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public /* final */ String className;
    @DexIgnore
    public Td0 declarationID;
    @DexIgnore
    public Ud0 variantID;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Parcelable.Creator<MicroAppInstruction> getCREATOR() {
            return MicroAppInstruction.CREATOR;
        }

        @DexIgnore
        public final void setCREATOR(Parcelable.Creator<MicroAppInstruction> creator) {
            MicroAppInstruction.CREATOR = creator;
        }
    }

    @DexIgnore
    public enum MicroAppID {
        UAPP_HID_MEDIA_CONTROL_MUSIC("music-control", true, false),
        UAPP_HID_MEDIA_VOL_UP_ID("music-volumn-up", true, false),
        UAPP_HID_MEDIA_VOL_DOWN_ID("music-volumn-down", true, false),
        UAPP_ACTIVITY_TAGGING_ID(Constants.ACTIVITY, false, false),
        UAPP_GOAL_TRACKING_ID("goal-tracking", false, false),
        UAPP_DATE_ID("date", false, false),
        UAPP_TIME2_ID("second-time-zone", false, false),
        UAPP_ALERT_ID("alert", false, false),
        UAPP_ALARM_ID(Alarm.TABLE_NAME, false, false),
        UAPP_PROGRESS_ID(Constants.ACTIVITY, false, false),
        UAPP_WEATHER_STANDARD("weather", false, true),
        UAPP_COMMUTE_TIME("commute-time", false, true),
        UAPP_TOGGLE_MODE("sequence", false, false),
        UAPP_RING_PHONE("ring-my-phone", false, true),
        UAPP_SELFIE(Constants.SELFIE, true, false),
        UAPP_STOPWATCH("stopwatch", false, false),
        UAPP_UNKNOWN("unknown", false, false);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        public /* final */ boolean isNeedHID;
        @DexIgnore
        public /* final */ boolean isNeedStreaming;
        @DexIgnore
        public /* final */ String value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(Qg6 qg6) {
                this();
            }

            @DexIgnore
            public final MicroAppID getMicroAppId(String str) {
                Wg6.c(str, "value");
                MicroAppID[] values = MicroAppID.values();
                int length = values.length;
                for (int i = 0; i < length; i++) {
                    if (Wg6.a(values[i].getValue(), str)) {
                        return values[i];
                    }
                }
                return MicroAppID.UAPP_TOGGLE_MODE;
            }

            @DexIgnore
            public final MicroAppID getMicroAppIdFromDeviceEventId(int i) {
                return i == E90.RING_MY_PHONE_MICRO_APP.ordinal() ? MicroAppID.UAPP_RING_PHONE : (i == E90.COMMUTE_TIME_ETA_MICRO_APP.ordinal() || i == E90.COMMUTE_TIME_TRAVEL_MICRO_APP.ordinal()) ? MicroAppID.UAPP_COMMUTE_TIME : MicroAppID.UAPP_UNKNOWN;
            }
        }

        @DexIgnore
        public MicroAppID(String str, boolean z, boolean z2) {
            this.value = str;
            this.isNeedHID = z;
            this.isNeedStreaming = z2;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }

        @DexIgnore
        public final boolean isNeedHID() {
            return this.isNeedHID;
        }

        @DexIgnore
        public final boolean isNeedStreaming() {
            return this.isNeedStreaming;
        }
    }

    @DexIgnore
    public MicroAppInstruction(Parcel parcel) {
        Wg6.c(parcel, "in");
        String name = MicroAppInstruction.class.getName();
        Wg6.b(name, "javaClass.name");
        this.className = name;
        this.declarationID = Td0.values()[parcel.readInt()];
        this.variantID = Ud0.values()[parcel.readInt()];
    }

    @DexIgnore
    public MicroAppInstruction(Td0 td0, Ud0 ud0) {
        Wg6.c(td0, "declarationID");
        Wg6.c(ud0, "variantID");
        String name = MicroAppInstruction.class.getName();
        Wg6.b(name, "javaClass.name");
        this.className = name;
        this.declarationID = td0;
        this.variantID = ud0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final Td0 getDeclarationID() {
        return this.declarationID;
    }

    @DexIgnore
    public abstract Tc0 getMicroAppData();

    @DexIgnore
    public final Ud0 getVariantID() {
        return this.variantID;
    }

    @DexIgnore
    public final void setDeclarationID(Td0 td0) {
        this.declarationID = td0;
    }

    @DexIgnore
    public final void setVariantID(Ud0 ud0) {
        this.variantID = ud0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(this.className);
        Td0 td0 = this.declarationID;
        if (td0 != null) {
            parcel.writeInt(td0.ordinal());
            Ud0 ud0 = this.variantID;
            if (ud0 != null) {
                parcel.writeInt(ud0.ordinal());
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }
}
