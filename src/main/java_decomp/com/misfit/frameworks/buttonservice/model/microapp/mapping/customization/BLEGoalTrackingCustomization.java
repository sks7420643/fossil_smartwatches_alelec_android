package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.os.Parcel;
import com.fossil.Uu1;
import com.fossil.Vu1;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BLEGoalTrackingCustomization extends BLECustomization {
    @DexIgnore
    public int goalId;

    @DexIgnore
    public BLEGoalTrackingCustomization(int i) {
        super(1);
        this.goalId = i;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BLEGoalTrackingCustomization(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "in");
        this.goalId = parcel.readInt();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization
    public Vu1 getCustomizationFrame() {
        return new Uu1((short) this.goalId);
    }

    @DexIgnore
    public final int getGoalId() {
        return this.goalId;
    }

    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0005: INVOKE  (r1v0 int) = 
      (r2v0 'this' com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLEGoalTrackingCustomization A[IMMUTABLE_TYPE, THIS])
     type: VIRTUAL call: com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization.getType():int), (':' char), (wrap: int : 0x0011: IGET  (r1v2 int) = 
      (r2v0 'this' com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLEGoalTrackingCustomization A[IMMUTABLE_TYPE, THIS])
     com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLEGoalTrackingCustomization.goalId int)] */
    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getType());
        sb.append(':');
        sb.append(this.goalId);
        return sb.toString();
    }

    @DexIgnore
    public final void setGoalId(int i) {
        this.goalId = i;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.goalId);
    }
}
