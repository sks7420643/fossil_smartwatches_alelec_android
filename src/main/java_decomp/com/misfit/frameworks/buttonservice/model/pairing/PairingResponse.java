package com.misfit.frameworks.buttonservice.model.pairing;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.Gson;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class PairingResponse implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<PairingResponse> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final LabelResponse buildLabelResponse(String str, int i) {
            Wg6.c(str, "version");
            return new LabelResponse(str, i);
        }

        @DexIgnore
        public final PairingLinkServerResponse buildPairingLinkServerResponse(boolean z, int i) {
            return new PairingLinkServerResponse(z, i);
        }

        @DexIgnore
        public final PairingUpdateFWResponse buildPairingUpdateFWResponse(FirmwareData firmwareData) {
            Wg6.c(firmwareData, "fwData");
            return new PairingUpdateFWResponse(firmwareData);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public PairingResponse createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            String readString = parcel.readString();
            if (readString != null) {
                try {
                    Class<?> cls = Class.forName(readString);
                    Wg6.b(cls, "Class.forName(dynamicClassName!!)");
                    Constructor<?> declaredConstructor = cls.getDeclaredConstructor(Parcel.class);
                    Wg6.b(declaredConstructor, "dynamicClass.getDeclared\u2026uctor(Parcel::class.java)");
                    declaredConstructor.setAccessible(true);
                    Object newInstance = declaredConstructor.newInstance(parcel);
                    if (newInstance != null) {
                        return (PairingResponse) newInstance;
                    }
                    throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.model.pairing.PairingResponse");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    return null;
                } catch (NoSuchMethodException e2) {
                    e2.printStackTrace();
                    return null;
                } catch (IllegalAccessException e3) {
                    e3.printStackTrace();
                    return null;
                } catch (InstantiationException e4) {
                    e4.printStackTrace();
                    return null;
                } catch (InvocationTargetException e5) {
                    e5.printStackTrace();
                    return null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public PairingResponse[] newArray(int i) {
            return new PairingResponse[i];
        }
    }

    @DexIgnore
    public PairingResponse() {
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public PairingResponse(Parcel parcel) {
        this();
        Wg6.c(parcel, "parcel");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(getClass().getName());
    }
}
