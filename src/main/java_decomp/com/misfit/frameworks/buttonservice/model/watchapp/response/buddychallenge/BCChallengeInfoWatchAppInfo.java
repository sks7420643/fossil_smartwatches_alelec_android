package com.misfit.frameworks.buttonservice.model.watchapp.response.buddychallenge;

import android.os.Parcel;
import com.fossil.Cq1;
import com.fossil.Gt1;
import com.fossil.Ry1;
import com.fossil.Ws1;
import com.mapped.E90;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCChallengeInfoWatchAppInfo extends DeviceAppResponse {
    @DexIgnore
    public Ws1 challenge;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BCChallengeInfoWatchAppInfo(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        this.challenge = (Ws1) parcel.readParcelable(Ws1.class.getClassLoader());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BCChallengeInfoWatchAppInfo(Ws1 ws1) {
        super(E90.BUDDY_CHALLENGE_GET_INFO);
        Wg6.c(ws1, "challenge");
        this.challenge = ws1;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceResponse(X90 x90, Ry1 ry1) {
        Wg6.c(x90, "deviceRequest");
        if (!(x90 instanceof Cq1)) {
            return null;
        }
        Cq1 cq1 = (Cq1) x90;
        Ws1 ws1 = this.challenge;
        if (ws1 != null) {
            return new Gt1(cq1, ws1);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeParcelable(this.challenge, i);
    }
}
