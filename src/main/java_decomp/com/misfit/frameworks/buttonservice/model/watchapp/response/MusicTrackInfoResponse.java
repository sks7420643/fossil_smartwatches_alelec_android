package com.misfit.frameworks.buttonservice.model.watchapp.response;

import android.os.Parcel;
import com.fossil.Yn1;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MusicTrackInfoResponse extends MusicResponse {
    @DexIgnore
    public /* final */ String albumName;
    @DexIgnore
    public /* final */ String appName;
    @DexIgnore
    public /* final */ String artistName;
    @DexIgnore
    public /* final */ String trackTitle;
    @DexIgnore
    public /* final */ byte volume;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicTrackInfoResponse(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        String readString = parcel.readString();
        this.appName = readString == null ? "" : readString;
        this.volume = parcel.readByte();
        String readString2 = parcel.readString();
        this.trackTitle = readString2 == null ? "" : readString2;
        String readString3 = parcel.readString();
        this.artistName = readString3 == null ? "" : readString3;
        String readString4 = parcel.readString();
        this.albumName = readString4 == null ? "" : readString4;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicTrackInfoResponse(String str, byte b, String str2, String str3, String str4) {
        super(MusicResponse.TYPE_MUSIC_TRACK_INFO);
        Wg6.c(str, "appName");
        Wg6.c(str2, "trackTitle");
        Wg6.c(str3, "artistName");
        Wg6.c(str4, "albumName");
        this.appName = str;
        this.volume = (byte) b;
        this.trackTitle = str2;
        this.artistName = str3;
        this.albumName = str4;
    }

    @DexIgnore
    public final String getAlbumName() {
        return this.albumName;
    }

    @DexIgnore
    public final String getAppName() {
        return this.appName;
    }

    @DexIgnore
    public final String getArtistName() {
        return this.artistName;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public String getHash() {
        String str = this.appName + ":" + Byte.valueOf(this.volume) + ":" + this.trackTitle + ":" + this.artistName + ":" + this.albumName;
        Wg6.b(str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public final String getTrackTitle() {
        return this.trackTitle;
    }

    @DexIgnore
    public final byte getVolume() {
        return this.volume;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public String toRemoteLogString() {
        return super.toRemoteLogString() + ", appName=" + this.appName;
    }

    @DexIgnore
    public final Yn1 toSDKTrackInfo() {
        return new Yn1(this.appName, this.volume, this.trackTitle, this.artistName, this.albumName);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.appName);
        parcel.writeByte(this.volume);
        parcel.writeString(this.trackTitle);
        parcel.writeString(this.artistName);
        parcel.writeString(this.albumName);
    }
}
