package com.misfit.frameworks.buttonservice.model.workoutdetection;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.WorkoutMode;
import com.fossil.fitness.WorkoutType;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDetectionItem implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public boolean isEnable;
    @DexIgnore
    public WorkoutMode mode;
    @DexIgnore
    public int pauseLatency;
    @DexIgnore
    public int resumeLatency;
    @DexIgnore
    public int startLatency;
    @DexIgnore
    public int stopLatency;
    @DexIgnore
    public WorkoutType type;
    @DexIgnore
    public boolean userConfirmation;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WorkoutDetectionItem> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutDetectionItem createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new WorkoutDetectionItem(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutDetectionItem[] newArray(int i) {
            return new WorkoutDetectionItem[i];
        }
    }

    @DexIgnore
    public WorkoutDetectionItem() {
        this.type = WorkoutType.UNKNOWN;
        this.mode = WorkoutMode.UNKNOWN;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutDetectionItem(Parcel parcel) {
        this();
        boolean z = true;
        Wg6.c(parcel, "parcel");
        String readString = parcel.readString();
        this.type = WorkoutType.valueOf(readString == null ? "" : readString);
        byte b = (byte) 0;
        this.isEnable = parcel.readByte() != b;
        this.userConfirmation = parcel.readByte() == b ? false : z;
        this.startLatency = parcel.readInt();
        this.pauseLatency = parcel.readInt();
        this.resumeLatency = parcel.readInt();
        this.stopLatency = parcel.readInt();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final WorkoutMode getMode() {
        return this.mode;
    }

    @DexIgnore
    public final int getPauseLatency() {
        return this.pauseLatency;
    }

    @DexIgnore
    public final int getResumeLatency() {
        return this.resumeLatency;
    }

    @DexIgnore
    public final int getStartLatency() {
        return this.startLatency;
    }

    @DexIgnore
    public final int getStopLatency() {
        return this.stopLatency;
    }

    @DexIgnore
    public final WorkoutType getType() {
        return this.type;
    }

    @DexIgnore
    public final boolean getUserConfirmation() {
        return this.userConfirmation;
    }

    @DexIgnore
    public final boolean isEnable() {
        return this.isEnable;
    }

    @DexIgnore
    public final void setEnable(boolean z) {
        this.isEnable = z;
    }

    @DexIgnore
    public final void setMode(WorkoutMode workoutMode) {
        Wg6.c(workoutMode, "<set-?>");
        this.mode = workoutMode;
    }

    @DexIgnore
    public final void setPauseLatency(int i) {
        this.pauseLatency = i;
    }

    @DexIgnore
    public final void setResumeLatency(int i) {
        this.resumeLatency = i;
    }

    @DexIgnore
    public final void setStartLatency(int i) {
        this.startLatency = i;
    }

    @DexIgnore
    public final void setStopLatency(int i) {
        this.stopLatency = i;
    }

    @DexIgnore
    public final void setType(WorkoutType workoutType) {
        Wg6.c(workoutType, "<set-?>");
        this.type = workoutType;
    }

    @DexIgnore
    public final void setUserConfirmation(boolean z) {
        this.userConfirmation = z;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(this.type.name());
        parcel.writeByte(this.isEnable ? (byte) 1 : 0);
        parcel.writeByte(this.userConfirmation ? (byte) 1 : 0);
        parcel.writeInt(this.startLatency);
        parcel.writeInt(this.pauseLatency);
        parcel.writeInt(this.resumeLatency);
        parcel.writeInt(this.stopLatency);
    }
}
