package com.misfit.frameworks.buttonservice.model;

import com.misfit.frameworks.buttonservice.model.microapp.mapping.LinkMapping;
import com.misfit.frameworks.common.enums.Action;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum LinkMode implements Serializable {
    RING_PHONE {
        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.model.LinkMode
        public boolean isHidMode() {
            return false;
        }
    },
    CONTROL_MUSIC {
        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.model.LinkMode
        public boolean isHidMode() {
            return true;
        }
    },
    TAKE_PHOTO {
        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.model.LinkMode
        public boolean isHidMode() {
            return true;
        }
    },
    GOAL_TRACKING {
        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.model.LinkMode
        public boolean isHidMode() {
            return false;
        }
    };

    @DexIgnore
    public static LinkMode fromAction(int i) {
        return Action.Music.isActionBelongToThisType(i) ? CONTROL_MUSIC : Action.Selfie.isActionBelongToThisType(i) ? TAKE_PHOTO : Action.GoalTracking.isActionBelongToThisType(i) ? GOAL_TRACKING : RING_PHONE;
    }

    @DexIgnore
    public static LinkMode fromLinkMappings(List<LinkMapping> list) {
        return (list == null || list.isEmpty()) ? RING_PHONE : fromAction(list.get(0).getAction());
    }

    @DexIgnore
    public static LinkMode fromMappings(List<Mapping> list) {
        return (list == null || list.isEmpty()) ? RING_PHONE : fromAction(list.get(0).getAction());
    }

    @DexIgnore
    public static List<LinkMode> getLinkModesFromMappings(List<Mapping> list) {
        HashMap hashMap = new HashMap();
        if (list != null && !list.isEmpty()) {
            for (Mapping mapping : list) {
                int action = mapping.getAction();
                hashMap.put(fromAction(action), Integer.valueOf(action));
            }
        }
        return new ArrayList(hashMap.keySet());
    }

    @DexIgnore
    public static boolean hasLinkModeInMappings(List<Mapping> list, LinkMode linkMode) {
        if (list != null && !list.isEmpty()) {
            for (Mapping mapping : list) {
                if (fromAction(mapping.getAction()) == linkMode) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public abstract boolean isHidMode();
}
