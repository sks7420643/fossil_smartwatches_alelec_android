package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.os.Parcel;
import com.fossil.Vu1;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BLENonCustomization extends BLECustomization {
    @DexIgnore
    public BLENonCustomization() {
        super(0);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BLENonCustomization(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "in");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization
    public Vu1 getCustomizationFrame() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization
    public String getHash() {
        return getType() + ":non";
    }
}
