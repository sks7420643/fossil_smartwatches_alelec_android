package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import com.fossil.Gq1;
import com.fossil.Jt1;
import com.fossil.Ry1;
import com.mapped.E90;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeETAMicroAppResponse extends DeviceAppResponse {
    @DexIgnore
    public int mHour;
    @DexIgnore
    public int mMinute;

    @DexIgnore
    public CommuteTimeETAMicroAppResponse(int i, int i2) {
        super(E90.COMMUTE_TIME_ETA_MICRO_APP);
        this.mHour = i;
        this.mMinute = i2;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeETAMicroAppResponse(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        this.mHour = parcel.readInt();
        this.mMinute = parcel.readInt();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceResponse(X90 x90, Ry1 ry1) {
        Wg6.c(x90, "deviceRequest");
        if (!(x90 instanceof Gq1) || ry1 == null) {
            return null;
        }
        return new Jt1((Gq1) x90, ry1, this.mHour, this.mMinute);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.mHour);
        parcel.writeInt(this.mMinute);
    }
}
