package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.Ku1;
import com.fossil.Oq1;
import com.fossil.Ry1;
import com.fossil.Tt1;
import com.mapped.E90;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingPhoneComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public Ku1 mRingPhoneState; // = Ku1.OFF;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingPhoneComplicationAppInfo(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        this.mRingPhoneState = Ku1.values()[parcel.readInt()];
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RingPhoneComplicationAppInfo(Ku1 ku1) {
        super(E90.RING_PHONE);
        Wg6.c(ku1, "ringPhoneState");
        this.mRingPhoneState = ku1;
    }

    @DexIgnore
    public final Ku1 getMRingPhoneState() {
        return this.mRingPhoneState;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceData() {
        return new Tt1(this.mRingPhoneState);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceResponse(X90 x90, Ry1 ry1) {
        Wg6.c(x90, "deviceRequest");
        if (x90 instanceof Oq1) {
            return new Tt1((Oq1) x90, this.mRingPhoneState);
        }
        return null;
    }

    @DexIgnore
    public final void setMRingPhoneState(Ku1 ku1) {
        Wg6.c(ku1, "<set-?>");
        this.mRingPhoneState = ku1;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.mRingPhoneState.ordinal());
    }
}
