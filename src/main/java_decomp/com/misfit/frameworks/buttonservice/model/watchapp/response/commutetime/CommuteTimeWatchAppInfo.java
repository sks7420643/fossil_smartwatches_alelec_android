package com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime;

import android.os.Parcel;
import com.facebook.share.internal.ShareConstants;
import com.fossil.Iq1;
import com.fossil.Lt1;
import com.fossil.Ry1;
import com.fossil.Yl1;
import com.mapped.E90;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppInfo extends DeviceAppResponse {
    @DexIgnore
    public int commuteTimeInMinute;
    @DexIgnore
    public String destination; // = "";
    @DexIgnore
    public String traffic; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppInfo(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        String readString = parcel.readString();
        this.destination = readString == null ? "" : readString;
        this.commuteTimeInMinute = parcel.readInt();
        String readString2 = parcel.readString();
        this.traffic = readString2 == null ? "" : readString2;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppInfo(String str, int i, String str2) {
        super(E90.COMMUTE_TIME_WATCH_APP);
        Wg6.c(str, ShareConstants.DESTINATION);
        Wg6.c(str2, "traffic");
        this.destination = str;
        this.commuteTimeInMinute = i;
        this.traffic = str2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceData() {
        return new Lt1(new Yl1(this.destination, this.commuteTimeInMinute, this.traffic));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceResponse(X90 x90, Ry1 ry1) {
        Wg6.c(x90, "deviceRequest");
        if (x90 instanceof Iq1) {
            return new Lt1((Iq1) x90, new Yl1(this.destination, this.commuteTimeInMinute, this.traffic));
        }
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.destination);
        parcel.writeInt(this.commuteTimeInMinute);
        parcel.writeString(this.traffic);
    }
}
