package com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime;

import android.os.Parcel;
import com.fossil.Iq1;
import com.fossil.Lt1;
import com.fossil.Nt1;
import com.fossil.Ry1;
import com.mapped.Dd0;
import com.mapped.E90;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppMessage extends DeviceAppResponse {
    @DexIgnore
    public Dd0 deviceMessageType; // = Dd0.END;
    @DexIgnore
    public String message; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[Dd0.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[Dd0.END.ordinal()] = 1;
            $EnumSwitchMapping$0[Dd0.IN_PROGRESS.ordinal()] = 2;
        }
        */
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMessage(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        String readString = parcel.readString();
        this.message = readString == null ? "" : readString;
        this.deviceMessageType = Dd0.values()[parcel.readInt()];
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMessage(String str, Dd0 dd0) {
        super(E90.COMMUTE_TIME_WATCH_APP);
        Wg6.c(str, "message");
        Wg6.c(dd0, "deviceMessageType");
        this.message = str;
        this.deviceMessageType = dd0;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceData() {
        return new Lt1(new Nt1(this.message, this.deviceMessageType));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceResponse(X90 x90, Ry1 ry1) {
        Wg6.c(x90, "deviceRequest");
        if (!(x90 instanceof Iq1)) {
            return null;
        }
        int i = WhenMappings.$EnumSwitchMapping$0[this.deviceMessageType.ordinal()];
        return i != 1 ? i != 2 ? new Lt1(new Nt1(this.message, this.deviceMessageType)) : new Lt1(new Nt1(this.message, this.deviceMessageType)) : new Lt1((Iq1) x90, new Nt1(this.message, this.deviceMessageType));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.message);
        parcel.writeInt(this.deviceMessageType.ordinal());
    }
}
