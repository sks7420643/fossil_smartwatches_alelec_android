package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FirmwareData implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<FirmwareData> CREATOR; // = new FirmwareData$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public String checkSum;
    @DexIgnore
    public String deviceModel;
    @DexIgnore
    public String firmwareVersion;
    @DexIgnore
    public boolean isEmbedded;
    @DexIgnore
    public int rawBundleResource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public FirmwareData() {
        this.isEmbedded = true;
        this.deviceModel = "";
        this.firmwareVersion = "";
        this.rawBundleResource = 1;
        this.checkSum = "";
    }

    @DexIgnore
    public FirmwareData(Parcel parcel) {
        Wg6.c(parcel, "parcel");
        this.isEmbedded = parcel.readInt() != 0;
        String readString = parcel.readString();
        this.firmwareVersion = readString == null ? "" : readString;
        String readString2 = parcel.readString();
        this.deviceModel = readString2 == null ? "" : readString2;
        this.rawBundleResource = parcel.readInt();
        String readString3 = parcel.readString();
        this.checkSum = readString3 == null ? "" : readString3;
    }

    @DexIgnore
    public FirmwareData(String str, String str2, int i) {
        Wg6.c(str, "firmwareVersion");
        Wg6.c(str2, "deviceModel");
        this.isEmbedded = true;
        this.deviceModel = str2;
        this.firmwareVersion = str;
        this.rawBundleResource = i;
        this.checkSum = "";
    }

    @DexIgnore
    public FirmwareData(String str, String str2, String str3) {
        Wg6.c(str, "firmwareVersion");
        Wg6.c(str2, "deviceModel");
        Wg6.c(str3, "checkSum");
        this.isEmbedded = false;
        this.deviceModel = str2;
        this.firmwareVersion = str;
        this.rawBundleResource = -1;
        this.checkSum = str3;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getCheckSum() {
        return this.checkSum;
    }

    @DexIgnore
    public final String getDeviceModel() {
        return this.deviceModel;
    }

    @DexIgnore
    public final String getFirmwareVersion() {
        return this.firmwareVersion;
    }

    @DexIgnore
    public final int getRawBundleResource() {
        return this.rawBundleResource;
    }

    @DexIgnore
    public final boolean isEmbedded() {
        return this.isEmbedded;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "dest");
        parcel.writeString(getClass().getName());
        parcel.writeInt(this.isEmbedded ? 1 : 0);
        parcel.writeString(this.firmwareVersion);
        parcel.writeString(this.deviceModel);
        parcel.writeInt(this.rawBundleResource);
        parcel.writeString(this.checkSum);
    }
}
