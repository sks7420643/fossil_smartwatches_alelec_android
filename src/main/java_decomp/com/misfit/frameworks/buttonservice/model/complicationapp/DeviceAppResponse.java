package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Ry1;
import com.google.gson.Gson;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import com.misfit.frameworks.buttonservice.model.LifeTimeObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class DeviceAppResponse implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<DeviceAppResponse> CREATOR; // = new DeviceAppResponse$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ long LIFE_TIME; // = 8000;
    @DexIgnore
    public E90 deviceEventId;
    @DexIgnore
    public boolean isForceUpdate;
    @DexIgnore
    public /* final */ LifeTimeObject lifeTimeObject;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DeviceAppResponse(Parcel parcel) {
        this(E90.values()[parcel.readInt()]);
        Wg6.c(parcel, "parcel");
    }

    @DexIgnore
    public DeviceAppResponse(E90 e90) {
        Wg6.c(e90, "deviceEventId");
        this.deviceEventId = e90;
        this.lifeTimeObject = new LifeTimeObject(LIFE_TIME);
    }

    @DexIgnore
    public static /* synthetic */ Tc0 getSDKDeviceResponse$default(DeviceAppResponse deviceAppResponse, X90 x90, Ry1 ry1, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                ry1 = null;
            }
            return deviceAppResponse.getSDKDeviceResponse(x90, ry1);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getSDKDeviceResponse");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final E90 getDeviceEventId() {
        return this.deviceEventId;
    }

    @DexIgnore
    public final LifeTimeObject getLifeTimeObject() {
        return this.lifeTimeObject;
    }

    @DexIgnore
    public abstract Tc0 getSDKDeviceData();

    @DexIgnore
    public abstract Tc0 getSDKDeviceResponse(X90 x90, Ry1 ry1);

    @DexIgnore
    public final boolean isForceUpdate() {
        return this.isForceUpdate;
    }

    @DexIgnore
    public final void setDeviceEventId(E90 e90) {
        Wg6.c(e90, "<set-?>");
        this.deviceEventId = e90;
    }

    @DexIgnore
    public final void setForceUpdate(boolean z) {
        this.isForceUpdate = z;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(getClass().getName());
        parcel.writeInt(this.deviceEventId.ordinal());
    }
}
