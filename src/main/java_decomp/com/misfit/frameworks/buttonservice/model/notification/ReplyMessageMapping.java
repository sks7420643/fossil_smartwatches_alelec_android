package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ReplyMessageMapping implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public String id; // = "";
    @DexIgnore
    public String message; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<ReplyMessageMapping> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ReplyMessageMapping createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new ReplyMessageMapping(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ReplyMessageMapping[] newArray(int i) {
            return new ReplyMessageMapping[i];
        }
    }

    @DexIgnore
    public ReplyMessageMapping(Parcel parcel) {
        Wg6.c(parcel, "parcel");
        this.id = parcel.readString();
        this.message = parcel.readString();
    }

    @DexIgnore
    public ReplyMessageMapping(String str, String str2) {
        Wg6.c(str, "id");
        Wg6.c(str2, "content");
        this.id = str;
        this.message = str2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getMessage() {
        return this.message;
    }

    @DexIgnore
    public final void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public final void setMessage(String str) {
        this.message = str;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.id);
            parcel.writeString(this.message);
        }
    }
}
