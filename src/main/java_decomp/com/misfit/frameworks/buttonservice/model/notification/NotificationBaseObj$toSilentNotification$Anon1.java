package com.misfit.frameworks.buttonservice.model.notification;

import com.fossil.Er7;
import com.mapped.Bh6;
import com.mapped.Hi6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class NotificationBaseObj$toSilentNotification$Anon1 extends Bh6 {
    @DexIgnore
    public NotificationBaseObj$toSilentNotification$Anon1(NotificationBaseObj notificationBaseObj) {
        super(notificationBaseObj);
    }

    @DexIgnore
    @Override // com.mapped.Bh6
    public Object get() {
        return ((NotificationBaseObj) this.receiver).getNotificationFlags();
    }

    @DexIgnore
    @Override // com.fossil.Gq7, com.fossil.Ds7
    public String getName() {
        return "notificationFlags";
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public Hi6 getOwner() {
        return Er7.b(NotificationBaseObj.class);
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public String getSignature() {
        return "getNotificationFlags()Ljava/util/List;";
    }
}
