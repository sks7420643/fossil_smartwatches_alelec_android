package com.misfit.frameworks.buttonservice.model.microapp.mapping;

import com.fossil.Hj4;
import com.fossil.Zi4;
import com.google.gson.JsonElement;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomizationDeserializer;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class BLEMappingDeserializer implements Hu3<BLEMapping> {
    @DexIgnore
    @Override // com.mapped.Hu3
    public BLEMapping deserialize(JsonElement jsonElement, Type type, Gu3 gu3) throws Hj4 {
        FLogger.INSTANCE.getLocal().d(BLEMappingDeserializer.class.getName(), jsonElement.toString());
        int b = jsonElement.d().p("mType").b();
        Zi4 zi4 = new Zi4();
        zi4.f(BLECustomization.class, new BLECustomizationDeserializer());
        if (b == 1) {
            return (BLEMapping) zi4.d().g(jsonElement, LinkMapping.class);
        }
        if (b != 2) {
            return null;
        }
        return (BLEMapping) zi4.d().g(jsonElement, MicroAppMapping.class);
    }
}
