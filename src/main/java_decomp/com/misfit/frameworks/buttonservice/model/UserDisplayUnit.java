package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Pn1;
import com.google.gson.Gson;
import com.mapped.H70;
import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserDisplayUnit implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public DistanceUnit distanceUnit;
    @DexIgnore
    public TemperatureUnit temperatureUnit;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<UserDisplayUnit> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public UserDisplayUnit createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new UserDisplayUnit(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public UserDisplayUnit[] newArray(int i) {
            return new UserDisplayUnit[i];
        }
    }

    @DexIgnore
    public enum DistanceUnit {
        KM("KM"),
        MILE("MILE");
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        public /* final */ String value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(Qg6 qg6) {
                this();
            }

            @DexIgnore
            public final DistanceUnit fromValue(String str) {
                DistanceUnit distanceUnit;
                Wg6.c(str, "value");
                DistanceUnit[] values = DistanceUnit.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        distanceUnit = null;
                        break;
                    }
                    distanceUnit = values[i];
                    if (Wg6.a(distanceUnit.getValue(), str)) {
                        break;
                    }
                    i++;
                }
                return distanceUnit != null ? distanceUnit : DistanceUnit.KM;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[DistanceUnit.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[DistanceUnit.KM.ordinal()] = 1;
                $EnumSwitchMapping$0[DistanceUnit.MILE.ordinal()] = 2;
            }
            */
        }

        @DexIgnore
        public DistanceUnit(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }

        @DexIgnore
        public final Pn1 toSDKDistanceUnit() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return Pn1.KM;
            }
            if (i == 2) {
                return Pn1.MILE;
            }
            throw new Kc6();
        }
    }

    @DexIgnore
    public enum TemperatureUnit {
        C("C"),
        F(DeviceIdentityUtils.FLASH_SERIAL_NUMBER_PREFIX);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        public /* final */ String value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(Qg6 qg6) {
                this();
            }

            @DexIgnore
            public final TemperatureUnit fromValue(String str) {
                TemperatureUnit temperatureUnit;
                Wg6.c(str, "value");
                TemperatureUnit[] values = TemperatureUnit.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        temperatureUnit = null;
                        break;
                    }
                    temperatureUnit = values[i];
                    if (Wg6.a(temperatureUnit.getValue(), str)) {
                        break;
                    }
                    i++;
                }
                return temperatureUnit != null ? temperatureUnit : TemperatureUnit.C;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[TemperatureUnit.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[TemperatureUnit.C.ordinal()] = 1;
                $EnumSwitchMapping$0[TemperatureUnit.F.ordinal()] = 2;
            }
            */
        }

        @DexIgnore
        public TemperatureUnit(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }

        @DexIgnore
        public final H70 toSDKTemperatureUnit() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return H70.C;
            }
            if (i == 2) {
                return H70.F;
            }
            throw new Kc6();
        }
    }

    @DexIgnore
    public UserDisplayUnit(Parcel parcel) {
        Wg6.c(parcel, "parcel");
        TemperatureUnit.Companion companion = TemperatureUnit.Companion;
        String readString = parcel.readString();
        this.temperatureUnit = companion.fromValue(readString == null ? TemperatureUnit.C.getValue() : readString);
        DistanceUnit.Companion companion2 = DistanceUnit.Companion;
        String readString2 = parcel.readString();
        this.distanceUnit = companion2.fromValue(readString2 == null ? TemperatureUnit.C.getValue() : readString2);
    }

    @DexIgnore
    public UserDisplayUnit(TemperatureUnit temperatureUnit2, DistanceUnit distanceUnit2) {
        Wg6.c(temperatureUnit2, "temperatureUnit");
        Wg6.c(distanceUnit2, "distanceUnit");
        this.temperatureUnit = temperatureUnit2;
        this.distanceUnit = distanceUnit2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final DistanceUnit getDistanceUnit() {
        return this.distanceUnit;
    }

    @DexIgnore
    public final TemperatureUnit getTemperatureUnit() {
        return this.temperatureUnit;
    }

    @DexIgnore
    public final void setDistanceUnit(DistanceUnit distanceUnit2) {
        Wg6.c(distanceUnit2, "<set-?>");
        this.distanceUnit = distanceUnit2;
    }

    @DexIgnore
    public final void setTemperatureUnit(TemperatureUnit temperatureUnit2) {
        Wg6.c(temperatureUnit2, "<set-?>");
        this.temperatureUnit = temperatureUnit2;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "dest");
        parcel.writeString(this.temperatureUnit.getValue());
        parcel.writeString(this.distanceUnit.getValue());
    }
}
