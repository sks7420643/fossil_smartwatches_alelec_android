package com.misfit.frameworks.buttonservice.model.watchapp.response.workout;

import android.os.Parcel;
import com.fossil.Mq1;
import com.fossil.Nt1;
import com.fossil.Rt1;
import com.fossil.Ry1;
import com.mapped.Dd0;
import com.mapped.E90;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ResumeWorkoutInfoData extends DeviceAppResponse {
    @DexIgnore
    public Dd0 deviceMessageType; // = Dd0.SUCCESS;
    @DexIgnore
    public Mq1 mResumeRequest;
    @DexIgnore
    public String message; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ResumeWorkoutInfoData(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        String readString = parcel.readString();
        this.message = readString == null ? "" : readString;
        this.deviceMessageType = Dd0.values()[parcel.readInt()];
        this.mResumeRequest = (Mq1) parcel.readParcelable(Mq1.class.getClassLoader());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ResumeWorkoutInfoData(Mq1 mq1, String str, Dd0 dd0) {
        super(E90.WORKOUT_RESUME);
        Wg6.c(mq1, "resumeWorkoutRequest");
        Wg6.c(str, "message");
        Wg6.c(dd0, "deviceMessageType");
        this.message = str;
        this.deviceMessageType = dd0;
        this.mResumeRequest = mq1;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceResponse(X90 x90, Ry1 ry1) {
        Wg6.c(x90, "deviceRequest");
        if (x90 instanceof Mq1) {
            return new Rt1((Mq1) x90, new Nt1(this.message, this.deviceMessageType));
        }
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.message);
        parcel.writeInt(this.deviceMessageType.ordinal());
        parcel.writeParcelable(this.mResumeRequest, i);
    }
}
