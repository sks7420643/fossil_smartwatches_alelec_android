package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaNotificationObj$AApplicationName$Companion$CREATOR$Anon1 implements Parcelable.Creator<DianaNotificationObj.AApplicationName> {
    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public DianaNotificationObj.AApplicationName createFromParcel(Parcel parcel) {
        Wg6.c(parcel, "parcel");
        return new DianaNotificationObj.AApplicationName(parcel);
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public DianaNotificationObj.AApplicationName[] newArray(int i) {
        return new DianaNotificationObj.AApplicationName[i];
    }
}
