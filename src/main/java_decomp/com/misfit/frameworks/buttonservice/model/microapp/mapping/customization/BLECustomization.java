package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Vt7;
import com.fossil.Vu1;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BLECustomization implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<BLECustomization> CREATOR; // = new BLECustomization$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String FIELD_TYPE; // = "type";
    @DexIgnore
    public /* final */ String className;
    @DexIgnore
    public int type;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class BLEMappingCustomizationType {
        @DexIgnore
        public static /* final */ int GOAL_TRACKING_TYPE; // = 1;
        @DexIgnore
        public static /* final */ BLEMappingCustomizationType INSTANCE; // = new BLEMappingCustomizationType();
        @DexIgnore
        public static /* final */ int NON_TYPE; // = 0;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Parcelable.Creator<BLECustomization> getCREATOR() {
            return BLECustomization.CREATOR;
        }

        @DexIgnore
        public final void setCREATOR(Parcelable.Creator<BLECustomization> creator) {
            BLECustomization.CREATOR = creator;
        }
    }

    @DexIgnore
    public BLECustomization(int i) {
        String name = getClass().getName();
        Wg6.b(name, "javaClass.name");
        this.className = name;
        this.type = i;
    }

    @DexIgnore
    public BLECustomization(Parcel parcel) {
        Wg6.c(parcel, "in");
        String name = getClass().getName();
        Wg6.b(name, "javaClass.name");
        this.className = name;
        this.type = parcel.readInt();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BLECustomization)) {
            return false;
        }
        return Vt7.j(getHash(), ((BLECustomization) obj).getHash(), true);
    }

    @DexIgnore
    public abstract Vu1 getCustomizationFrame();

    @DexIgnore
    public abstract String getHash();

    @DexIgnore
    public final int getType() {
        return this.type;
    }

    @DexIgnore
    public final void setType(int i) {
        this.type = i;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "dest");
        parcel.writeString(this.className);
        parcel.writeInt(this.type);
    }
}
