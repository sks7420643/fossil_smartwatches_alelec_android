package com.misfit.frameworks.buttonservice.model.vibration;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface BleVibrationStrengthLevel {
    public static final int LIGHT = 1;
    public static final int MEDIUM = 2;
    public static final int STRONG = 3;
}
