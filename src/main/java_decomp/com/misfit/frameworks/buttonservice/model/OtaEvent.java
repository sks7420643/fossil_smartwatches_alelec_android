package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OtaEvent implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public float process;
    @DexIgnore
    public String serial;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<OtaEvent> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public OtaEvent createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new OtaEvent(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public OtaEvent[] newArray(int i) {
            return new OtaEvent[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public OtaEvent(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = "parcel"
            com.mapped.Wg6.c(r3, r0)
            java.lang.String r0 = r3.readString()
            if (r0 == 0) goto L_0x0013
        L_0x000b:
            float r1 = r3.readFloat()
            r2.<init>(r0, r1)
            return
        L_0x0013:
            java.lang.String r0 = ""
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.OtaEvent.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public OtaEvent(String str, float f) {
        Wg6.c(str, "serial");
        this.serial = str;
        this.process = f;
    }

    @DexIgnore
    public static /* synthetic */ OtaEvent copy$default(OtaEvent otaEvent, String str, float f, int i, Object obj) {
        if ((i & 1) != 0) {
            str = otaEvent.serial;
        }
        if ((i & 2) != 0) {
            f = otaEvent.process;
        }
        return otaEvent.copy(str, f);
    }

    @DexIgnore
    public final String component1() {
        return this.serial;
    }

    @DexIgnore
    public final float component2() {
        return this.process;
    }

    @DexIgnore
    public final OtaEvent copy(String str, float f) {
        Wg6.c(str, "serial");
        return new OtaEvent(str, f);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof OtaEvent) {
                OtaEvent otaEvent = (OtaEvent) obj;
                if (!Wg6.a(this.serial, otaEvent.serial) || Float.compare(this.process, otaEvent.process) != 0) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final float getProcess() {
        return this.process;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.serial;
        return ((str != null ? str.hashCode() : 0) * 31) + Float.floatToIntBits(this.process);
    }

    @DexIgnore
    public final void setProcess(float f) {
        this.process = f;
    }

    @DexIgnore
    public final void setSerial(String str) {
        Wg6.c(str, "<set-?>");
        this.serial = str;
    }

    @DexIgnore
    public String toString() {
        return "OtaEvent(serial=" + this.serial + ", process=" + this.process + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(this.serial);
        parcel.writeFloat(this.process);
    }
}
