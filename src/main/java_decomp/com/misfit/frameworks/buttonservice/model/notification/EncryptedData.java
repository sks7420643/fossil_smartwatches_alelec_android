package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Fu1;
import com.fossil.Gu1;
import com.fossil.Iu1;
import com.fossil.Wp1;
import com.google.gson.Gson;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EncryptedData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ EncryptMethod encryptMethod;
    @DexIgnore
    public /* final */ byte[] encryptedData;
    @DexIgnore
    public /* final */ EncryptedDataType encryptedDataType;
    @DexIgnore
    public /* final */ KeyType keyType;
    @DexIgnore
    public /* final */ short sequence;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<EncryptedData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public EncryptedData createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new EncryptedData(parcel, null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public EncryptedData[] newArray(int i) {
            return new EncryptedData[i];
        }
    }

    @DexIgnore
    public enum EncryptMethod {
        NONE,
        AES_CTR,
        AES_CBC_PKCS5;
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public final /* synthetic */ class WhenMappings {
                @DexIgnore
                public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

                /*
                static {
                    int[] iArr = new int[Fu1.values().length];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[Fu1.AES_CBC_PKCS5.ordinal()] = 1;
                    $EnumSwitchMapping$0[Fu1.AES_CTR.ordinal()] = 2;
                }
                */
            }

            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(Qg6 qg6) {
                this();
            }

            @DexIgnore
            public final EncryptMethod fromSDKEncryptMethod(Fu1 fu1) {
                Wg6.c(fu1, "sdkEncryptMethod");
                int i = WhenMappings.$EnumSwitchMapping$0[fu1.ordinal()];
                return i != 1 ? i != 2 ? EncryptMethod.NONE : EncryptMethod.AES_CTR : EncryptMethod.AES_CBC_PKCS5;
            }
        }
    }

    @DexIgnore
    public enum EncryptedDataType {
        BUDDY_CHALLENGE;
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public final /* synthetic */ class WhenMappings {
                @DexIgnore
                public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

                /*
                static {
                    int[] iArr = new int[Gu1.values().length];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[Gu1.BUDDY_CHALLENGE.ordinal()] = 1;
                }
                */
            }

            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(Qg6 qg6) {
                this();
            }

            @DexIgnore
            public final EncryptedDataType fromSDKEncryptedDataType(Gu1 gu1) {
                Wg6.c(gu1, "sdkEncryptedDataType");
                return WhenMappings.$EnumSwitchMapping$0[gu1.ordinal()] != 1 ? EncryptedDataType.BUDDY_CHALLENGE : EncryptedDataType.BUDDY_CHALLENGE;
            }
        }
    }

    @DexIgnore
    public enum KeyType {
        DEFAULT,
        PRE_SHARED_KEY,
        SHARED_SECRET_KEY;
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public final /* synthetic */ class WhenMappings {
                @DexIgnore
                public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

                /*
                static {
                    int[] iArr = new int[Iu1.values().length];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[Iu1.PRE_SHARED_KEY.ordinal()] = 1;
                    $EnumSwitchMapping$0[Iu1.SHARED_SECRET_KEY.ordinal()] = 2;
                }
                */
            }

            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(Qg6 qg6) {
                this();
            }

            @DexIgnore
            public final KeyType fromSDKKeyType(Iu1 iu1) {
                Wg6.c(iu1, "sdkKeyType");
                int i = WhenMappings.$EnumSwitchMapping$0[iu1.ordinal()];
                return i != 1 ? i != 2 ? KeyType.DEFAULT : KeyType.SHARED_SECRET_KEY : KeyType.PRE_SHARED_KEY;
            }
        }
    }

    @DexIgnore
    public EncryptedData(Parcel parcel) {
        this.encryptMethod = EncryptMethod.values()[parcel.readInt()];
        byte[] bArr = new byte[parcel.readInt()];
        this.encryptedData = bArr;
        parcel.readByteArray(bArr);
        this.encryptedDataType = EncryptedDataType.values()[parcel.readInt()];
        this.keyType = KeyType.values()[parcel.readInt()];
        this.sequence = (short) ((short) parcel.readInt());
    }

    @DexIgnore
    public /* synthetic */ EncryptedData(Parcel parcel, Qg6 qg6) {
        this(parcel);
    }

    @DexIgnore
    public EncryptedData(Wp1 wp1) {
        Wg6.c(wp1, "encryptedDataNotification");
        this.encryptMethod = EncryptMethod.Companion.fromSDKEncryptMethod(wp1.getEncryptMethod());
        this.encryptedData = wp1.getEncryptedData();
        this.encryptedDataType = EncryptedDataType.Companion.fromSDKEncryptedDataType(wp1.getEncryptedDataType());
        this.keyType = KeyType.Companion.fromSDKKeyType(wp1.getKeyType());
        this.sequence = wp1.getSequence();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final EncryptMethod getEncryptMethod() {
        return this.encryptMethod;
    }

    @DexIgnore
    public final byte[] getEncryptedData() {
        return this.encryptedData;
    }

    @DexIgnore
    public final EncryptedDataType getEncryptedDataType() {
        return this.encryptedDataType;
    }

    @DexIgnore
    public final KeyType getKeyType() {
        return this.keyType;
    }

    @DexIgnore
    public final short getSequence() {
        return this.sequence;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeInt(this.encryptMethod.ordinal());
        parcel.writeInt(this.encryptedData.length);
        parcel.writeByteArray(this.encryptedData);
        parcel.writeInt(this.encryptedDataType.ordinal());
        parcel.writeInt(this.keyType.ordinal());
        parcel.writeInt(this.sequence);
    }
}
