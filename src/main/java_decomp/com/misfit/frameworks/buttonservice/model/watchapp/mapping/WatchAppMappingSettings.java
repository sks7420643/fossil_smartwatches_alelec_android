package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Gv1;
import com.fossil.Hv1;
import com.fossil.Zo1;
import com.google.gson.Gson;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppMappingSettings implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<WatchAppMappingSettings> CREATOR; // = new WatchAppMappingSettings$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public WatchAppMapping bottomAppMapping;
    @DexIgnore
    public WatchAppMapping middleAppMapping;
    @DexIgnore
    public /* final */ long timeStamp;
    @DexIgnore
    public WatchAppMapping topAppMapping;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final long compareTimeStamp(WatchAppMappingSettings watchAppMappingSettings, WatchAppMappingSettings watchAppMappingSettings2) {
            long j = 0;
            long timeStamp = watchAppMappingSettings != null ? watchAppMappingSettings.getTimeStamp() : 0;
            if (watchAppMappingSettings2 != null) {
                j = watchAppMappingSettings2.getTimeStamp();
            }
            return timeStamp - j;
        }

        @DexIgnore
        public final boolean isSettingsSame(WatchAppMappingSettings watchAppMappingSettings, WatchAppMappingSettings watchAppMappingSettings2) {
            if ((watchAppMappingSettings != null || watchAppMappingSettings2 == null) && (watchAppMappingSettings == null || watchAppMappingSettings2 != null)) {
                return Wg6.a(watchAppMappingSettings, watchAppMappingSettings2);
            }
            return false;
        }
    }

    @DexIgnore
    public WatchAppMappingSettings(Parcel parcel) {
        this.timeStamp = parcel.readLong();
        WatchAppMapping watchAppMapping = (WatchAppMapping) parcel.readParcelable(WatchAppMapping.class.getClassLoader());
        this.topAppMapping = watchAppMapping == null ? new MusicWatchAppMapping() : watchAppMapping;
        WatchAppMapping watchAppMapping2 = (WatchAppMapping) parcel.readParcelable(WatchAppMapping.class.getClassLoader());
        this.middleAppMapping = watchAppMapping2 == null ? new NoneWatchAppMapping() : watchAppMapping2;
        WatchAppMapping watchAppMapping3 = (WatchAppMapping) parcel.readParcelable(WatchAppMapping.class.getClassLoader());
        this.bottomAppMapping = watchAppMapping3 == null ? new DiagnosticsWatchAppMapping() : watchAppMapping3;
    }

    @DexIgnore
    public /* synthetic */ WatchAppMappingSettings(Parcel parcel, Qg6 qg6) {
        this(parcel);
    }

    @DexIgnore
    public WatchAppMappingSettings(WatchAppMapping watchAppMapping, WatchAppMapping watchAppMapping2, WatchAppMapping watchAppMapping3, long j) {
        Wg6.c(watchAppMapping, "topAppMapping");
        Wg6.c(watchAppMapping2, "middleAppMapping");
        Wg6.c(watchAppMapping3, "bottomAppMapping");
        this.topAppMapping = watchAppMapping;
        this.middleAppMapping = watchAppMapping2;
        this.bottomAppMapping = watchAppMapping3;
        this.timeStamp = j;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof WatchAppMappingSettings)) {
            return false;
        }
        return Wg6.a(getHash(), ((WatchAppMappingSettings) obj).getHash());
    }

    @DexIgnore
    public final WatchAppMapping getBottomAppMapping() {
        return this.bottomAppMapping;
    }

    @DexIgnore
    public final String getHash() {
        String str = this.topAppMapping.getHash() + ":" + this.middleAppMapping.getHash() + ":" + this.bottomAppMapping.getHash() + ":";
        Wg6.b(str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public final long getTimeStamp() {
        return this.timeStamp;
    }

    @DexIgnore
    public final void setBottomAppMapping(WatchAppMapping watchAppMapping) {
        Wg6.c(watchAppMapping, "<set-?>");
        this.bottomAppMapping = watchAppMapping;
    }

    @DexIgnore
    public final Gv1 toSDKSetting() {
        return new Gv1(new Hv1[]{new Zo1(this.topAppMapping.toSDKSetting(), this.middleAppMapping.toSDKSetting(), this.bottomAppMapping.toSDKSetting())});
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeLong(this.timeStamp);
        parcel.writeParcelable(this.topAppMapping, i);
        parcel.writeParcelable(this.middleAppMapping, i);
        parcel.writeParcelable(this.bottomAppMapping, i);
    }
}
