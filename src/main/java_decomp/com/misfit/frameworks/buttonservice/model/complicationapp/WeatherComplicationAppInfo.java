package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.Fp1;
import com.fossil.Qq1;
import com.fossil.Ry1;
import com.fossil.Xt1;
import com.mapped.E90;
import com.mapped.H70;
import com.mapped.J70;
import com.mapped.Kc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public /* final */ long expiredAt;
    @DexIgnore
    public float temperature;
    @DexIgnore
    public TemperatureUnit temperatureUnit;
    @DexIgnore
    public WeatherCondition weatherCondition;

    @DexIgnore
    public enum TemperatureUnit {
        C,
        F;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[TemperatureUnit.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[TemperatureUnit.C.ordinal()] = 1;
                $EnumSwitchMapping$0[TemperatureUnit.F.ordinal()] = 2;
            }
            */
        }

        @DexIgnore
        public final H70 toSdkTemperatureUnit() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return H70.C;
            }
            if (i == 2) {
                return H70.F;
            }
            throw new Kc6();
        }
    }

    @DexIgnore
    public enum WeatherCondition {
        CLEAR_DAY,
        CLEAR_NIGHT,
        RAIN,
        SNOW,
        SLEET,
        WIND,
        FOG,
        CLOUDY,
        PARTLY_CLOUDY_DAY,
        PARTLY_CLOUDY_NIGHT;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[WeatherCondition.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[WeatherCondition.CLEAR_DAY.ordinal()] = 1;
                $EnumSwitchMapping$0[WeatherCondition.CLEAR_NIGHT.ordinal()] = 2;
                $EnumSwitchMapping$0[WeatherCondition.RAIN.ordinal()] = 3;
                $EnumSwitchMapping$0[WeatherCondition.SNOW.ordinal()] = 4;
                $EnumSwitchMapping$0[WeatherCondition.SLEET.ordinal()] = 5;
                $EnumSwitchMapping$0[WeatherCondition.WIND.ordinal()] = 6;
                $EnumSwitchMapping$0[WeatherCondition.FOG.ordinal()] = 7;
                $EnumSwitchMapping$0[WeatherCondition.CLOUDY.ordinal()] = 8;
                $EnumSwitchMapping$0[WeatherCondition.PARTLY_CLOUDY_DAY.ordinal()] = 9;
                $EnumSwitchMapping$0[WeatherCondition.PARTLY_CLOUDY_NIGHT.ordinal()] = 10;
            }
            */
        }

        @DexIgnore
        public final J70 toSdkWeatherCondition() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return J70.CLEAR_DAY;
                case 2:
                    return J70.CLEAR_NIGHT;
                case 3:
                    return J70.RAIN;
                case 4:
                    return J70.SNOW;
                case 5:
                    return J70.SLEET;
                case 6:
                    return J70.WIND;
                case 7:
                    return J70.FOG;
                case 8:
                    return J70.CLOUDY;
                case 9:
                    return J70.PARTLY_CLOUDY_DAY;
                case 10:
                    return J70.PARTLY_CLOUDY_NIGHT;
                default:
                    throw new Kc6();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherComplicationAppInfo(float f, TemperatureUnit temperatureUnit2, WeatherCondition weatherCondition2, long j) {
        super(E90.WEATHER_COMPLICATION);
        Wg6.c(temperatureUnit2, "temperatureUnit");
        Wg6.c(weatherCondition2, "weatherCondition");
        this.temperature = f;
        this.temperatureUnit = temperatureUnit2;
        this.weatherCondition = weatherCondition2;
        this.expiredAt = j;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherComplicationAppInfo(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        this.temperature = parcel.readFloat();
        this.temperatureUnit = TemperatureUnit.values()[parcel.readInt()];
        this.weatherCondition = WeatherCondition.values()[parcel.readInt()];
        this.expiredAt = parcel.readLong();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceData() {
        return new Xt1(new Fp1(this.expiredAt, this.temperatureUnit.toSdkTemperatureUnit(), this.temperature, this.weatherCondition.toSdkWeatherCondition()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceResponse(X90 x90, Ry1 ry1) {
        Wg6.c(x90, "deviceRequest");
        if (!(x90 instanceof Qq1)) {
            return null;
        }
        return new Xt1((Qq1) x90, new Fp1(this.expiredAt, this.temperatureUnit.toSdkTemperatureUnit(), this.temperature, this.weatherCondition.toSdkWeatherCondition()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeFloat(this.temperature);
        parcel.writeInt(this.temperatureUnit.ordinal());
        parcel.writeInt(this.weatherCondition.ordinal());
        parcel.writeLong(this.expiredAt);
    }
}
