package com.misfit.frameworks.buttonservice.model.watchapp.response;

import android.os.Parcel;
import com.mapped.Kc6;
import com.mapped.L70;
import com.mapped.M70;
import com.mapped.N70;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotifyMusicEventResponse extends MusicResponse {
    @DexIgnore
    public /* final */ MusicMediaAction musicAction;
    @DexIgnore
    public /* final */ MusicMediaStatus status;

    @DexIgnore
    public enum MusicMediaAction {
        PLAY,
        PAUSE,
        TOGGLE_PLAY_PAUSE,
        NEXT,
        PREVIOUS,
        VOLUME_UP,
        VOLUME_DOWN,
        NONE;
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public final /* synthetic */ class WhenMappings {
                @DexIgnore
                public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

                /*
                static {
                    int[] iArr = new int[L70.values().length];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[L70.PLAY.ordinal()] = 1;
                    $EnumSwitchMapping$0[L70.PAUSE.ordinal()] = 2;
                    $EnumSwitchMapping$0[L70.TOGGLE_PLAY_PAUSE.ordinal()] = 3;
                    $EnumSwitchMapping$0[L70.NEXT.ordinal()] = 4;
                    $EnumSwitchMapping$0[L70.PREVIOUS.ordinal()] = 5;
                    $EnumSwitchMapping$0[L70.VOLUME_UP.ordinal()] = 6;
                    $EnumSwitchMapping$0[L70.VOLUME_DOWN.ordinal()] = 7;
                }
                */
            }

            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(Qg6 qg6) {
                this();
            }

            @DexIgnore
            public final MusicMediaAction fromSDKMusicAction(L70 l70) {
                Wg6.c(l70, "sdkMusicAction");
                switch (WhenMappings.$EnumSwitchMapping$0[l70.ordinal()]) {
                    case 1:
                        return MusicMediaAction.PLAY;
                    case 2:
                        return MusicMediaAction.PAUSE;
                    case 3:
                        return MusicMediaAction.TOGGLE_PLAY_PAUSE;
                    case 4:
                        return MusicMediaAction.NEXT;
                    case 5:
                        return MusicMediaAction.PREVIOUS;
                    case 6:
                        return MusicMediaAction.VOLUME_UP;
                    case 7:
                        return MusicMediaAction.VOLUME_DOWN;
                    default:
                        return MusicMediaAction.NONE;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[MusicMediaAction.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[MusicMediaAction.PLAY.ordinal()] = 1;
                $EnumSwitchMapping$0[MusicMediaAction.PAUSE.ordinal()] = 2;
                $EnumSwitchMapping$0[MusicMediaAction.TOGGLE_PLAY_PAUSE.ordinal()] = 3;
                $EnumSwitchMapping$0[MusicMediaAction.NEXT.ordinal()] = 4;
                $EnumSwitchMapping$0[MusicMediaAction.PREVIOUS.ordinal()] = 5;
                $EnumSwitchMapping$0[MusicMediaAction.VOLUME_UP.ordinal()] = 6;
                $EnumSwitchMapping$0[MusicMediaAction.VOLUME_DOWN.ordinal()] = 7;
                $EnumSwitchMapping$0[MusicMediaAction.NONE.ordinal()] = 8;
            }
            */
        }

        @DexIgnore
        public final L70 toSDKMusicAction() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return L70.PLAY;
                case 2:
                    return L70.PAUSE;
                case 3:
                    return L70.TOGGLE_PLAY_PAUSE;
                case 4:
                    return L70.NEXT;
                case 5:
                    return L70.PREVIOUS;
                case 6:
                    return L70.VOLUME_UP;
                case 7:
                    return L70.VOLUME_DOWN;
                case 8:
                    return null;
                default:
                    throw new Kc6();
            }
        }
    }

    @DexIgnore
    public enum MusicMediaStatus {
        SUCCESS,
        NO_MUSIC_PLAYER,
        FAIL_TO_TRIGGER;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[MusicMediaStatus.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[MusicMediaStatus.SUCCESS.ordinal()] = 1;
                $EnumSwitchMapping$0[MusicMediaStatus.NO_MUSIC_PLAYER.ordinal()] = 2;
                $EnumSwitchMapping$0[MusicMediaStatus.FAIL_TO_TRIGGER.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public final M70 toSDKMusicActionStatus() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return M70.SUCCESS;
            }
            if (i == 2) {
                return M70.NO_MUSIC_PLAYER;
            }
            if (i == 3) {
                return M70.FAIL_TO_TRIGGER;
            }
            throw new Kc6();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotifyMusicEventResponse(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        this.musicAction = MusicMediaAction.values()[parcel.readInt()];
        this.status = MusicMediaStatus.values()[parcel.readInt()];
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotifyMusicEventResponse(MusicMediaAction musicMediaAction, MusicMediaStatus musicMediaStatus) {
        super("Music Event_" + musicMediaAction);
        Wg6.c(musicMediaAction, "musicAction");
        Wg6.c(musicMediaStatus, "status");
        this.musicAction = musicMediaAction;
        this.status = musicMediaStatus;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public String getHash() {
        String str = this.musicAction + ":" + this.status;
        Wg6.b(str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public final MusicMediaAction getMusicAction() {
        return this.musicAction;
    }

    @DexIgnore
    public final MusicMediaStatus getStatus() {
        return this.status;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public String toRemoteLogString() {
        return super.toRemoteLogString() + ", musicAction=" + this.musicAction + ", status=" + this.status;
    }

    @DexIgnore
    public final N70 toSDKMusicEvent() {
        L70 sDKMusicAction = this.musicAction.toSDKMusicAction();
        if (sDKMusicAction != null) {
            return new N70(sDKMusicAction, this.status.toSDKMusicActionStatus());
        }
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.musicAction.ordinal());
        parcel.writeInt(this.status.ordinal());
    }
}
