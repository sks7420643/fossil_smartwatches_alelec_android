package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.Ep1;
import com.fossil.Fq1;
import com.fossil.It1;
import com.fossil.Ry1;
import com.mapped.E90;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ChanceOfRainComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public /* final */ long expiredAt;
    @DexIgnore
    public int probability;

    @DexIgnore
    public ChanceOfRainComplicationAppInfo(int i, long j) {
        super(E90.CHANCE_OF_RAIN_COMPLICATION);
        this.probability = i;
        this.expiredAt = j;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ChanceOfRainComplicationAppInfo(Parcel parcel) {
        super(parcel);
        Wg6.c(parcel, "parcel");
        this.probability = parcel.readInt();
        this.expiredAt = parcel.readLong();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceData() {
        return new It1(new Ep1(this.expiredAt, this.probability));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public Tc0 getSDKDeviceResponse(X90 x90, Ry1 ry1) {
        Wg6.c(x90, "deviceRequest");
        if (!(x90 instanceof Fq1)) {
            return null;
        }
        return new It1((Fq1) x90, new Ep1(this.expiredAt, this.probability));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.probability);
        parcel.writeLong(this.expiredAt);
    }
}
