package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.Gson;
import com.mapped.HandMovingConfig;
import com.mapped.NotificationHandMovingConfig;
import com.mapped.NotificationVibePattern;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppNotificationFilter implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public static /* final */ int IS_FIELD_EXIST; // = 1;
    @DexIgnore
    public static /* final */ int IS_FIELD_NOT_EXIST; // = 0;
    @DexIgnore
    public static /* final */ String TAG; // = "AppNotificationFilter";
    @DexIgnore
    public FNotification fNotification;
    @DexIgnore
    public NotificationHandMovingConfig handMovingConfig;
    @DexIgnore
    public Short priority;
    @DexIgnore
    public String sender;
    @DexIgnore
    public NotificationVibePattern vibePattern;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<AppNotificationFilter> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public AppNotificationFilter createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new AppNotificationFilter(parcel, null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public AppNotificationFilter[] newArray(int i) {
            return new AppNotificationFilter[i];
        }
    }

    @DexIgnore
    public AppNotificationFilter(Parcel parcel) {
        FNotification fNotification2 = (FNotification) parcel.readParcelable(FNotification.class.getClassLoader());
        this.fNotification = fNotification2 == null ? new FNotification() : fNotification2;
        if (parcel.readInt() == 1) {
            this.sender = parcel.readString();
        }
        if (parcel.readInt() == 1) {
            this.priority = Short.valueOf((short) parcel.readInt());
        }
        if (parcel.readInt() == 1) {
            this.handMovingConfig = (NotificationHandMovingConfig) parcel.readParcelable(HandMovingConfig.class.getClassLoader());
        }
        if (parcel.readInt() == 1) {
            this.vibePattern = NotificationVibePattern.values()[parcel.readInt()];
        }
    }

    @DexIgnore
    public /* synthetic */ AppNotificationFilter(Parcel parcel, Qg6 qg6) {
        this(parcel);
    }

    @DexIgnore
    public AppNotificationFilter(FNotification fNotification2) {
        Wg6.c(fNotification2, "fNotification");
        this.fNotification = fNotification2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AppNotificationFilter)) {
            return false;
        }
        AppNotificationFilter appNotificationFilter = (AppNotificationFilter) obj;
        return Wg6.a(this.fNotification, appNotificationFilter.fNotification) && Wg6.a(this.sender, appNotificationFilter.sender) && Wg6.a(this.priority, appNotificationFilter.priority);
    }

    @DexIgnore
    public final NotificationHandMovingConfig getHandMovingConfig() {
        return this.handMovingConfig;
    }

    @DexIgnore
    public final String getPackageName() {
        return this.fNotification.getPackageName();
    }

    @DexIgnore
    public final Short getPriority() {
        return this.priority;
    }

    @DexIgnore
    public final String getSender() {
        return this.sender;
    }

    @DexIgnore
    public final NotificationVibePattern getVibePattern() {
        return this.vibePattern;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public final void setHandMovingConfig(NotificationHandMovingConfig notificationHandMovingConfig) {
        this.handMovingConfig = notificationHandMovingConfig;
    }

    @DexIgnore
    public final void setPriority(Short sh) {
        this.priority = sh;
    }

    @DexIgnore
    public final void setSender(String str) {
        this.sender = str;
    }

    @DexIgnore
    public final void setVibePattern(NotificationVibePattern notificationVibePattern) {
        this.vibePattern = notificationVibePattern;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x01b5, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x01b6, code lost:
        com.fossil.So7.a(r2, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01b9, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01bc, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01bd, code lost:
        com.fossil.So7.a(r2, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01c0, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x01c3, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01c4, code lost:
        com.fossil.So7.a(r2, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01c7, code lost:
        throw r3;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.blesdk.device.data.notification.NotificationFilter toSDKNotificationFilter(android.content.Context r7) {
        /*
        // Method dump skipped, instructions count: 456
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter.toSDKNotificationFilter(android.content.Context):com.fossil.blesdk.device.data.notification.NotificationFilter");
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeParcelable(this.fNotification, 0);
        String str = this.sender;
        if (str != null) {
            parcel.writeInt(1);
            parcel.writeString(str);
        } else {
            parcel.writeInt(0);
        }
        Short sh = this.priority;
        if (sh != null) {
            short shortValue = sh.shortValue();
            parcel.writeInt(1);
            parcel.writeInt(shortValue);
        } else {
            parcel.writeInt(0);
        }
        NotificationHandMovingConfig notificationHandMovingConfig = this.handMovingConfig;
        if (notificationHandMovingConfig != null) {
            parcel.writeInt(1);
            parcel.writeParcelable(notificationHandMovingConfig, 0);
        } else {
            parcel.writeInt(0);
        }
        NotificationVibePattern notificationVibePattern = this.vibePattern;
        if (notificationVibePattern != null) {
            parcel.writeInt(1);
            parcel.writeInt(notificationVibePattern.ordinal());
            return;
        }
        parcel.writeInt(0);
    }
}
