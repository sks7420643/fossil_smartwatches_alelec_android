package com.misfit.frameworks.buttonservice.model.workoutdetection;

import com.fossil.Im7;
import com.fossil.In1;
import com.fossil.Kn1;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDetectionItemExtentionKt {
    @DexIgnore
    public static final Kn1[] toAutoWorkoutDetectionItemConfig(List<WorkoutDetectionItem> list) {
        Wg6.c(list, "$this$toAutoWorkoutDetectionItemConfig");
        ArrayList arrayList = new ArrayList(Im7.m(list, 10));
        for (T t : list) {
            arrayList.add(new Kn1(t.getType(), t.isEnable(), t.getUserConfirmation(), new In1((short) t.getStartLatency(), (short) t.getPauseLatency(), (short) t.getResumeLatency(), (short) t.getStopLatency())));
        }
        Object[] array = arrayList.toArray(new Kn1[0]);
        if (array != null) {
            return (Kn1[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
