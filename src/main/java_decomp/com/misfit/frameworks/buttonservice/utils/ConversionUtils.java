package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateFormat;
import com.facebook.internal.Utility;
import com.fossil.Hr7;
import com.mapped.I70;
import com.mapped.Wg6;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ConversionUtils {
    @DexIgnore
    public static /* final */ ConversionUtils INSTANCE; // = new ConversionUtils();

    @DexIgnore
    public final String SHA1(String str) {
        Wg6.c(str, "value");
        try {
            MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_SHA1);
            Charset forName = Charset.forName("UTF-8");
            Wg6.b(forName, "Charset.forName(charsetName)");
            byte[] bytes = str.getBytes(forName);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            instance.update(bytes);
            byte[] digest = instance.digest();
            Wg6.b(digest, "messageDigest.digest()");
            return byteArrayToString(digest);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @DexIgnore
    public final String byteArrayToString(byte[] bArr) {
        Wg6.c(bArr, "bytes");
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            Hr7 hr7 = Hr7.a;
            Locale locale = Locale.US;
            Wg6.b(locale, "Locale.US");
            String format = String.format(locale, "%02x", Arrays.copyOf(new Object[]{Byte.valueOf(b)}, 1));
            Wg6.b(format, "java.lang.String.format(locale, format, *args)");
            sb.append(format);
        }
        String sb2 = sb.toString();
        Wg6.b(sb2, "buffer.toString()");
        return sb2;
    }

    @DexIgnore
    public final float convertByteToMegaBytes(long j) {
        return ((float) (j / ((long) 1024))) / ((float) 1024);
    }

    @DexIgnore
    public final I70 getTimeFormat(Context context) {
        Wg6.c(context, "context");
        return DateFormat.is24HourFormat(context) ? I70.TWENTY_FOUR : I70.TWELVE;
    }

    @DexIgnore
    public final int getTimezoneRawOffsetById(String str) {
        Wg6.c(str, "timezoneId");
        if (TextUtils.isEmpty(str)) {
            return 1024;
        }
        TimeZone timeZone = TimeZone.getTimeZone(str);
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "Calendar.getInstance()");
        if (timeZone.inDaylightTime(instance.getTime())) {
            Wg6.b(timeZone, "timeZone");
            return (timeZone.getDSTSavings() + timeZone.getRawOffset()) / 60000;
        }
        Wg6.b(timeZone, "timeZone");
        return timeZone.getRawOffset() / 60000;
    }
}
