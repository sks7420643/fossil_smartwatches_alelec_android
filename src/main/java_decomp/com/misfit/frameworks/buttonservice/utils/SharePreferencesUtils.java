package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SharePreferencesUtils {
    @DexIgnore
    public static /* final */ String BC_APP_READY; // = "bc_app_ready_for";
    @DexIgnore
    public static /* final */ String BC_STATUS; // = "bc_status";
    @DexIgnore
    public static SharePreferencesUtils instance;
    @DexIgnore
    public SharedPreferences sharedPreferences; // = null;

    @DexIgnore
    public SharePreferencesUtils(Context context) {
        this.sharedPreferences = context.getSharedPreferences(KeyUtils.getButtonPreferenceKey(context), 4);
    }

    @DexIgnore
    public static SharePreferencesUtils getInstance(Context context) {
        SharePreferencesUtils sharePreferencesUtils;
        synchronized (SharePreferencesUtils.class) {
            try {
                if (instance == null) {
                    instance = new SharePreferencesUtils(context);
                }
                sharePreferencesUtils = instance;
            } catch (Throwable th) {
                throw th;
            }
        }
        return sharePreferencesUtils;
    }

    @DexIgnore
    public Boolean getBoolean(String str, boolean z) {
        return Boolean.valueOf(this.sharedPreferences.getBoolean(str, z));
    }

    @DexIgnore
    public long getLong(String str) {
        return this.sharedPreferences.getLong(str, -1);
    }

    @DexIgnore
    public String getString(String str) {
        return this.sharedPreferences.getString(str, null);
    }

    @DexIgnore
    public void setBoolean(String str, boolean z) {
        this.sharedPreferences.edit().putBoolean(str, z).apply();
    }

    @DexIgnore
    public void setLong(String str, long j) {
        this.sharedPreferences.edit().putLong(str, j).apply();
    }

    @DexIgnore
    public void setString(String str, String str2) {
        this.sharedPreferences.edit().putString(str, str2).apply();
    }
}
