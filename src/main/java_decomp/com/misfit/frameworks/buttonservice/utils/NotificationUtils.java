package com.misfit.frameworks.buttonservice.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.os.Build;
import com.fossil.Yk7;
import com.fossil.Zk0;
import com.fossil.Zk7;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationUtils {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String DEFAULT_CHANNEL_WATCH_SERVICE; // = "com.fossil.wearables.fossil.channel.defaultService";
    @DexIgnore
    public static /* final */ String DEFAULT_CHANNEL_WATCH_SERVICE_NAME; // = "Sync service";
    @DexIgnore
    public static /* final */ int DEVICE_STATUS_NOTIFICATION_ID; // = 1;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ Yk7 instance$delegate; // = Zk7.a(NotificationUtils$Companion$instance$Anon2.INSTANCE);
    @DexIgnore
    public static Zk0.Ei mNotificationBuilder;
    @DexIgnore
    public String mLastContent;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void instance$annotations() {
        }

        @DexIgnore
        public final NotificationUtils getInstance() {
            Yk7 yk7 = NotificationUtils.instance$delegate;
            Companion companion = NotificationUtils.Companion;
            return (NotificationUtils) yk7.getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Singleton {
        @DexIgnore
        public static /* final */ Singleton INSTANCE; // = new Singleton();

        @DexIgnore
        /* renamed from: INSTANCE  reason: collision with other field name */
        public static /* final */ NotificationUtils f0INSTANCE; // = new NotificationUtils(null);

        @DexIgnore
        public final NotificationUtils getINSTANCE() {
            return f0INSTANCE;
        }
    }

    /*
    static {
        String simpleName = NotificationUtils$Companion$TAG$Anon1.INSTANCE.getClass().getSimpleName();
        Wg6.b(simpleName, "NotificationUtils::javaClass.javaClass.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public NotificationUtils() {
        this.mLastContent = "";
    }

    @DexIgnore
    public /* synthetic */ NotificationUtils(Qg6 qg6) {
        this();
    }

    @DexIgnore
    private final Notification createDeviceStatusNotification(Context context, String str) {
        ArrayList<Zk0.Ai> arrayList;
        if (!(str.length() == 0)) {
            this.mLastContent = str;
        }
        if (mNotificationBuilder == null) {
            mNotificationBuilder = new Zk0.Ei(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        }
        Zk0.Ei ei = mNotificationBuilder;
        if (!(ei == null || (arrayList = ei.b) == null)) {
            arrayList.clear();
        }
        if (Build.VERSION.SDK_INT < 24) {
            Zk0.Ei ei2 = mNotificationBuilder;
            if (ei2 != null) {
                ei2.n(context.getResources().getString(R.string.app_name));
                ei2.i(Constants.SERVICE);
                ei2.v(false);
                ei2.m(str);
                ei2.A(null);
                ei2.y(R.drawable.ic_launcher_transparent);
                ei2.x(false);
                ei2.w(-2);
                Notification c = ei2.c();
                Wg6.b(c, "mNotificationBuilder!!\n \u2026                 .build()");
                return c;
            }
            Wg6.i();
            throw null;
        }
        Zk0.Ei ei3 = mNotificationBuilder;
        if (ei3 != null) {
            ei3.y(R.drawable.ic_launcher_transparent);
            ei3.i(Constants.SERVICE);
            ei3.n(str);
            ei3.A(null);
            ei3.v(false);
            ei3.x(false);
            ei3.w(-2);
            Notification c2 = ei3.c();
            Wg6.b(c2, "builder.build()");
            return c2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    private final Notification createDeviceStatusWithRichTextNotification(Context context, String str, String str2) {
        ArrayList<Zk0.Ai> arrayList;
        if (!(str2.length() == 0)) {
            this.mLastContent = str2;
        }
        if (mNotificationBuilder == null) {
            mNotificationBuilder = new Zk0.Ei(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        }
        Zk0.Ei ei = mNotificationBuilder;
        if (!(ei == null || (arrayList = ei.b) == null)) {
            arrayList.clear();
        }
        if (Build.VERSION.SDK_INT < 24) {
            Zk0.Ei ei2 = mNotificationBuilder;
            if (ei2 != null) {
                ei2.n(context.getResources().getString(R.string.app_name));
                ei2.i(Constants.SERVICE);
                Zk0.Ci ci = new Zk0.Ci();
                ci.g(str2);
                ei2.A(ci);
                ei2.v(false);
                ei2.m(str);
                ei2.y(R.drawable.ic_launcher_transparent);
                ei2.x(false);
                ei2.w(-2);
                Notification c = ei2.c();
                Wg6.b(c, "mNotificationBuilder!!\n \u2026                 .build()");
                return c;
            }
            Wg6.i();
            throw null;
        }
        Zk0.Ei ei3 = mNotificationBuilder;
        if (ei3 != null) {
            ei3.y(R.drawable.ic_launcher_transparent);
            ei3.i(Constants.SERVICE);
            Zk0.Ci ci2 = new Zk0.Ci();
            ci2.g(str2);
            ei3.A(ci2);
            ei3.n(str);
            ei3.v(false);
            ei3.x(false);
            ei3.w(-2);
            Notification c2 = ei3.c();
            Wg6.b(c2, "builder.build()");
            return c2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public static final NotificationUtils getInstance() {
        return Companion.getInstance();
    }

    @DexIgnore
    private final void riseNotification(NotificationManager notificationManager, int i, Notification notification) {
        try {
            notificationManager.notify(i, notification);
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().d(TAG, e.getMessage());
            e.printStackTrace();
        }
    }

    @DexIgnore
    public static /* synthetic */ void updateNotification$default(NotificationUtils notificationUtils, Context context, int i, String str, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z, int i2, Object obj) {
        PendingIntent pendingIntent3 = null;
        PendingIntent pendingIntent4 = (i2 & 8) != 0 ? null : pendingIntent;
        if ((i2 & 16) == 0) {
            pendingIntent3 = pendingIntent2;
        }
        notificationUtils.updateNotification(context, i, str, pendingIntent4, pendingIntent3, z);
    }

    @DexIgnore
    public final Notification createDefaultDeviceStatusNotification(Context context, String str, String str2) {
        Wg6.c(context, "context");
        Wg6.c(str, "content");
        Wg6.c(str2, "subText");
        if (!(str.length() == 0)) {
            this.mLastContent = str;
        }
        Zk0.Ei ei = new Zk0.Ei(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        ArrayList<Zk0.Ai> arrayList = ei.b;
        if (arrayList != null) {
            arrayList.clear();
        }
        if (Build.VERSION.SDK_INT < 24) {
            ei.n(context.getResources().getString(R.string.app_name));
            ei.m(str2);
            ei.y(R.drawable.ic_launcher_transparent);
            ei.i(Constants.SERVICE);
            ei.v(false);
            ei.x(false);
            ei.A(null);
            ei.w(-2);
            Notification c = ei.c();
            Wg6.b(c, "builder\n                \u2026                 .build()");
            return c;
        }
        ei.y(R.drawable.ic_launcher_transparent);
        ei.i(Constants.SERVICE);
        ei.n(str2);
        ei.A(null);
        ei.v(false);
        ei.x(false);
        ei.w(-2);
        Notification c2 = ei.c();
        Wg6.b(c2, "builder\n                \u2026                 .build()");
        return c2;
    }

    @DexIgnore
    public final NotificationManager createNotificationChannels(Context context) {
        Wg6.c(context, "context");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "createNotificationChannels - context=" + context);
        Object systemService = context.getSystemService("notification");
        if (systemService != null) {
            NotificationManager notificationManager = (NotificationManager) systemService;
            if (Build.VERSION.SDK_INT >= 26) {
                NotificationChannel notificationChannel = notificationManager.getNotificationChannel(DEFAULT_CHANNEL_WATCH_SERVICE);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "createNotificationChannels - currentChannel=" + notificationChannel);
                if (notificationChannel == null) {
                    NotificationChannel notificationChannel2 = new NotificationChannel(DEFAULT_CHANNEL_WATCH_SERVICE, DEFAULT_CHANNEL_WATCH_SERVICE_NAME, 1);
                    notificationChannel2.setShowBadge(false);
                    notificationManager.createNotificationChannel(notificationChannel2);
                }
            }
            return notificationManager;
        }
        throw new Rc6("null cannot be cast to non-null type android.app.NotificationManager");
    }

    @DexIgnore
    public final void startForegroundNotification(Context context, Service service, String str, String str2, boolean z) {
        Wg6.c(context, "context");
        Wg6.c(service, Constants.SERVICE);
        Wg6.c(str, "content");
        Wg6.c(str2, "subText");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, "Service Tracking - startForegroundNotification - context=" + context + ", service=" + service + ", content=" + str + ", subText=" + str2 + ", isStopForeground = " + z);
        createNotificationChannels(context);
        service.startForeground(1, createDefaultDeviceStatusNotification(context, str, str2));
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str4 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Service Tracking - startForegroundNotification done bring to foreground for service ");
        sb.append(service);
        local2.d(str4, sb.toString());
        if (z) {
            FLogger.INSTANCE.getLocal().d(TAG, "Service Tracking - startForegroundNotification() - stop foreground service and remove notification");
            service.stopForeground(true);
        }
    }

    @DexIgnore
    public final void updateNotification(Context context, int i, String str, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z) {
        Wg6.c(context, "context");
        Wg6.c(str, "content");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "updateNotification - context=" + context + ", content=" + str + " notificationId=" + i + ", isReset=" + z);
        if (!(str.length() == 0) || z) {
            this.mLastContent = str;
        }
        riseNotification(createNotificationChannels(context), i, createDeviceStatusNotification(context, str));
    }

    @DexIgnore
    public final void updateRichTextNotification(Context context, int i, String str, String str2, boolean z) {
        Wg6.c(context, "context");
        Wg6.c(str, "title");
        Wg6.c(str2, "content");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, "updateNotification - context=" + context + ", title=" + str + ", content=" + str2 + ", notificationId=" + i + ", isReset=" + z);
        if ((str2.length() > 0) || z) {
            this.mLastContent = str2;
        }
        riseNotification(createNotificationChannels(context), i, createDeviceStatusWithRichTextNotification(context, str, str2));
    }
}
