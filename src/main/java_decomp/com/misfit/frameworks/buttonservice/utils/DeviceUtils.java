package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.misfit.frameworks.common.enums.Action;
import com.misfit.frameworks.common.enums.ButtonType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DeviceUtils {
    @DexIgnore
    public static /* final */ String PREFERENCE_NAME; // = "com.misfit.frameworks.buttonservice.cacheddevices";
    @DexIgnore
    public static DeviceUtils sInstance;
    @DexIgnore
    public Context context;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Anon1 {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$common$enums$ButtonType;

        /*
        static {
            int[] iArr = new int[ButtonType.values().length];
            $SwitchMap$com$misfit$frameworks$common$enums$ButtonType = iArr;
            try {
                iArr[ButtonType.SELFIE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.MUSIC.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.PRESENTATION.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.ACTIVITY.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.BOLT_CONTROL.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.CUSTOM.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.PLUTO_TRACKER.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.SILVRETTA_TRACKER.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.BMW_TRACKER.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.SWAROVSKI_TRACKER.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.ThirdPartyApp.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
        }
        */
    }

    @DexIgnore
    public DeviceUtils(Context context2) {
        this.context = context2.getApplicationContext();
    }

    @DexIgnore
    public static DeviceUtils getInstance(Context context2) {
        DeviceUtils deviceUtils;
        synchronized (DeviceUtils.class) {
            try {
                if (sInstance == null) {
                    sInstance = new DeviceUtils(context2);
                }
                deviceUtils = sInstance;
            } catch (Throwable th) {
                throw th;
            }
        }
        return deviceUtils;
    }

    @DexIgnore
    public ButtonType bleCommandToButtonType(int i) {
        return i != 1 ? i != 2 ? i != 3 ? i != 5 ? i != 6 ? i != 7 ? i != 50 ? ButtonType.NONE : ButtonType.PLUTO_TRACKER : ButtonType.CUSTOM : ButtonType.BOLT_CONTROL : ButtonType.ACTIVITY : ButtonType.PRESENTATION : ButtonType.MUSIC : ButtonType.SELFIE;
    }

    @DexIgnore
    public int buttonTypeToBleCommand(ButtonType buttonType) {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$common$enums$ButtonType[buttonType.ordinal()]) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 5;
            case 5:
                return 6;
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                return 7;
            default:
                return 0;
        }
    }

    @DexIgnore
    public void clearMacAddress(Context context2, String str) {
        setString(context2, str, "");
    }

    @DexIgnore
    public ButtonType getButtonTypeByAction(int i) {
        return Action.Music.isActionBelongToThisType(i) ? ButtonType.MUSIC : Action.Selfie.isActionBelongToThisType(i) ? ButtonType.SELFIE : Action.Presenter.isActionBelongToThisType(i) ? ButtonType.PRESENTATION : Action.ActivityTracker.isActionBelongToThisType(i) ? ButtonType.ACTIVITY : Action.DisplayMode.isActionBelongToThisType(i) ? ButtonType.DISPLAY_MODE : i == 505 ? ButtonType.RING_MY_PHONE : i == 1000 ? ButtonType.GOAL_TRACKING : (i <= 600 || i >= 699) ? ButtonType.NONE : ButtonType.BOLT_CONTROL;
    }

    @DexIgnore
    public String getMacAddress(Context context2, String str) {
        return getString(context2, str);
    }

    @DexIgnore
    public SharedPreferences getPreferences(Context context2) {
        return context2.getSharedPreferences(PREFERENCE_NAME, 0);
    }

    @DexIgnore
    public String getSerial(Context context2, String str) {
        return getString(context2, str);
    }

    @DexIgnore
    public String getString(Context context2, String str) {
        SharedPreferences preferences = getPreferences(context2);
        return preferences != null ? preferences.getString(str, "") : "";
    }

    @DexIgnore
    public void saveMacAddress(Context context2, String str, String str2) {
        setString(context2, str, str2);
        setString(context2, str2, str);
    }

    @DexIgnore
    public void setString(Context context2, String str, String str2) {
        SharedPreferences preferences = getPreferences(context2);
        if (preferences != null) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString(str, str2);
            edit.apply();
        }
    }
}
