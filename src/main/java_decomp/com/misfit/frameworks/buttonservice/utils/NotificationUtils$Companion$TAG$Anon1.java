package com.misfit.frameworks.buttonservice.utils;

import com.fossil.Ep7;
import com.fossil.Er7;
import com.mapped.Ch6;
import com.mapped.Hi6;
import com.mapped.Ni6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class NotificationUtils$Companion$TAG$Anon1 extends Ch6 {
    @DexIgnore
    public static /* final */ Ni6 INSTANCE; // = new NotificationUtils$Companion$TAG$Anon1();

    @DexIgnore
    @Override // com.mapped.Ch6
    public Object get(Object obj) {
        return Ep7.a((NotificationUtils) obj);
    }

    @DexIgnore
    @Override // com.fossil.Gq7, com.fossil.Ds7
    public String getName() {
        return "javaClass";
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public Hi6 getOwner() {
        return Er7.c(Ep7.class, "buttonservice_release");
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public String getSignature() {
        return "getJavaClass(Ljava/lang/Object;)Ljava/lang/Class;";
    }
}
