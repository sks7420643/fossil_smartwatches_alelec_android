package com.misfit.frameworks.buttonservice;

import android.os.Bundle;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Lw1;
import com.fossil.Ry1;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;
import com.misfit.frameworks.buttonservice.model.watchparams.Version;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.ThemeExtentionKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.misfit.frameworks.buttonservice.ButtonService$themeDataToBinary$1", f = "ButtonService.kt", l = {}, m = "invokeSuspend")
public final class ButtonService$themeDataToBinary$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public /* final */ /* synthetic */ ThemeData $themeData;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ButtonService this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ButtonService$themeDataToBinary$Anon1(ButtonService buttonService, String str, ThemeData themeData, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = buttonService;
        this.$serial = str;
        this.$themeData = themeData;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        ButtonService$themeDataToBinary$Anon1 buttonService$themeDataToBinary$Anon1 = new ButtonService$themeDataToBinary$Anon1(this.this$0, this.$serial, this.$themeData, xe6);
        buttonService$themeDataToBinary$Anon1.p$ = (Il6) obj;
        throw null;
        //return buttonService$themeDataToBinary$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((ButtonService$themeDataToBinary$Anon1) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Ry1 ry1;
        Yn7.d();
        if (this.label == 0) {
            El7.b(obj);
            Version uiVersion = this.this$0.getUiVersion(this.$serial);
            if (uiVersion != null) {
                ry1 = new Ry1(uiVersion.getMajor(), uiVersion.getMinor());
            } else {
                int uiMajorVersion = DevicePreferenceUtils.getUiMajorVersion(this.this$0, this.$serial);
                int uiMinorVersion = DevicePreferenceUtils.getUiMinorVersion(this.this$0, this.$serial);
                ry1 = (uiMajorVersion <= 0 || uiMinorVersion <= 0) ? null : new Ry1(uiMajorVersion, uiMinorVersion);
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = ButtonService.TAG;
            local.d(str, "themeDataToBinar deviceUiVersion " + uiVersion + " themeData " + this.$themeData + ' ');
            if (ry1 != null) {
                Lw1 f = ThemeExtentionKt.toThemeEditor(this.$themeData).c(ry1).f();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = ButtonService.TAG;
                local2.e(str2, "themeDataToBinary - data: " + f);
                if (f == null) {
                    this.this$0.broadcastServiceBlePhaseEvent(this.$serial, CommunicateMode.PARSE_THEME_DATA_TO_BINARY, ServiceActionResult.FAILED, null);
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putByteArray(ButtonService.Companion.getTHEME_BINARY_EXTRA(), f.getData());
                    this.this$0.broadcastServiceBlePhaseEvent(this.$serial, CommunicateMode.PARSE_THEME_DATA_TO_BINARY, ServiceActionResult.SUCCEEDED, bundle);
                }
            } else {
                this.this$0.broadcastServiceBlePhaseEvent(this.$serial, CommunicateMode.PARSE_THEME_DATA_TO_BINARY, ServiceActionResult.FAILED, null);
            }
            return Cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
