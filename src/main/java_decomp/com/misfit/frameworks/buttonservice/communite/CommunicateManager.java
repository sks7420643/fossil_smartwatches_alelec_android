package com.misfit.frameworks.buttonservice.communite;

import android.content.Context;
import com.fossil.Er7;
import com.fossil.Nq7;
import com.fossil.Pm7;
import com.mapped.Hg6;
import com.mapped.Hi6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator;
import com.misfit.frameworks.buttonservice.utils.DeviceUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommunicateManager {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ConcurrentHashMap<String, BleCommunicator> bleCommunicators;
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public BleCommunicator currentCommunicator;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion extends SingletonHolder<CommunicateManager, Context> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final /* synthetic */ class Anon1 extends Nq7 implements Hg6<Context, CommunicateManager> {
            @DexIgnore
            public static /* final */ Anon1 INSTANCE; // = new Anon1();

            @DexIgnore
            public Anon1() {
                super(1);
            }

            @DexIgnore
            @Override // com.fossil.Gq7, com.fossil.Ds7
            public final String getName() {
                return "<init>";
            }

            @DexIgnore
            @Override // com.fossil.Gq7
            public final Hi6 getOwner() {
                return Er7.b(CommunicateManager.class);
            }

            @DexIgnore
            @Override // com.fossil.Gq7
            public final String getSignature() {
                return "<init>(Landroid/content/Context;)V";
            }

            @DexIgnore
            public final CommunicateManager invoke(Context context) {
                Wg6.c(context, "p1");
                return new CommunicateManager(context, null);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Hg6
            public /* bridge */ /* synthetic */ CommunicateManager invoke(Context context) {
                return invoke(context);
            }
        }

        @DexIgnore
        public Companion() {
            super(Anon1.INSTANCE);
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = CommunicateManager.class.getSimpleName();
        Wg6.b(simpleName, "CommunicateManager::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public CommunicateManager(Context context2) {
        this.context = context2;
        this.bleCommunicators = new ConcurrentHashMap<>();
    }

    @DexIgnore
    public /* synthetic */ CommunicateManager(Context context2, Qg6 qg6) {
        this(context2);
    }

    @DexIgnore
    private final BleCommunicator createDeviceCommunicator(Context context2, String str, String str2, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        return new DeviceCommunicator(context2, str, str2, communicationResultCallback);
    }

    @DexIgnore
    public final void clearCommunicatorSessionQueue(String str) {
        Wg6.c(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        if (bleCommunicator != null) {
            bleCommunicator.clearSessionQueue();
        }
    }

    @DexIgnore
    public final void clearCurrentCommunicatorSessionQueueIfNot(String str) {
        Wg6.c(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        BleCommunicator bleCommunicator2 = this.currentCommunicator;
        if (bleCommunicator != bleCommunicator2 && bleCommunicator2 != null) {
            if (bleCommunicator2 != null) {
                bleCommunicator2.clearSessionQueue();
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final int getBatteryLevel(String str) {
        Wg6.c(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        if (bleCommunicator == null) {
            return -1;
        }
        Wg6.b(bleCommunicator, "bleCommunicators[serial] ?: return -1");
        return bleCommunicator.getBleAdapter().getBatteryLevel();
    }

    @DexIgnore
    public final ConcurrentHashMap<String, BleCommunicator> getBleCommunicators() {
        return this.bleCommunicators;
    }

    @DexIgnore
    public final BleCommunicator getCommunicator(String str, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        Wg6.c(str, "serial");
        Wg6.c(communicationResultCallback, "communicationResultCallback");
        String macAddress = DeviceUtils.getInstance(this.context).getMacAddress(this.context, str);
        Wg6.b(macAddress, "DeviceUtils.getInstance(\u2026cAddress(context, serial)");
        return getCommunicator(str, macAddress, communicationResultCallback);
    }

    @DexIgnore
    public final BleCommunicator getCommunicator(String str, String str2, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        BleCommunicator bleCommunicator;
        synchronized (this) {
            Wg6.c(str, "serial");
            Wg6.c(str2, "macAddress");
            Wg6.c(communicationResultCallback, "communicationResultCallback");
            bleCommunicator = this.bleCommunicators.get(str);
            if (bleCommunicator == null) {
                bleCommunicator = createDeviceCommunicator(this.context, str, str2, communicationResultCallback);
                this.bleCommunicators.put(str, bleCommunicator);
            }
            this.currentCommunicator = bleCommunicator;
        }
        return bleCommunicator;
    }

    @DexIgnore
    public final String getLocale(String str) {
        Wg6.c(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        if (bleCommunicator == null) {
            return "";
        }
        Wg6.b(bleCommunicator, "bleCommunicators[serial] ?: return \"\"");
        return bleCommunicator.getBleAdapter().getLocale();
    }

    @DexIgnore
    public final List<BleCommunicator> getRunningCommunicator() {
        Collection<BleCommunicator> values = this.bleCommunicators.values();
        Wg6.b(values, "bleCommunicators.values");
        ArrayList arrayList = new ArrayList();
        for (T t : values) {
            if (t.isRunning()) {
                arrayList.add(t);
            }
        }
        return Pm7.j0(arrayList);
    }

    @DexIgnore
    public final void removeCommunicator(String str) {
        Wg6.c(str, "serial");
        this.bleCommunicators.remove(str);
    }
}
