package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import android.content.Context;
import com.fossil.Uk1;
import com.fossil.Yx1;
import com.fossil.Zk1;
import com.fossil.Zm1;
import com.fossil.fitness.FitnessData;
import com.mapped.C90;
import com.mapped.Q40;
import com.mapped.Qg6;
import com.mapped.R60;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BleStateAbs extends BleState implements ISessionSdkCallback {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ int MAKE_DEVICE_READY_TIMEOUT; // = 30000;
    @DexIgnore
    public static /* final */ int OTA_SUCCESS_TIMEOUT; // = 60000;
    @DexIgnore
    public static /* final */ int PROGRESS_TASK_MAXIMUM_RETRY; // = 3;
    @DexIgnore
    public static /* final */ int PROGRESS_UPDATE_TIMEOUT; // = 30000;
    @DexIgnore
    public static /* final */ int TASK_MAXIMUM_RETRY; // = 3;
    @DexIgnore
    public int maxRetries; // = 3;
    @DexIgnore
    public int retryCounter;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleStateAbs(String str) {
        super(str);
        Wg6.c(str, "tagName");
        setTimeout(30000);
    }

    @DexIgnore
    public final int getMaxRetries() {
        return this.maxRetries;
    }

    @DexIgnore
    public final int getRetryCounter() {
        return this.retryCounter;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onApplyHandPositionFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onApplyHandPositionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onApplyThemeError(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onApplyThemeProgressChanged(float f) {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onApplyThemeSuccess(byte[] bArr) {
        Wg6.c(bArr, "themeBinary");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onAuthenticateDeviceFail(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onAuthenticateDeviceSuccess(byte[] bArr) {
        Wg6.c(bArr, "randomKey");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onAuthorizeDeviceFailed() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onAuthorizeDeviceSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onConfigureMicroAppFail(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onConfigureMicroAppSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onDataTransferCompleted() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onDataTransferFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onDataTransferProgressChange(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onDeviceFound(Q40 q40, int i) {
        Wg6.c(q40, "device");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onEraseDataFilesFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onEraseDataFilesSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onEraseHWLogFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onEraseHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onExchangeSecretKeyFail(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onExchangeSecretKeySuccess(byte[] bArr) {
        Wg6.c(bArr, "secretKey");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onFetchDeviceInfoFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onFetchDeviceInfoSuccess(Zk1 zk1) {
        Wg6.c(zk1, "deviceInformation");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onGetDeviceConfigFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onGetDeviceConfigSuccess(HashMap<Zm1, R60> hashMap) {
        Wg6.c(hashMap, "deviceConfiguration");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onGetWatchParamsFail() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onMoveHandFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onMoveHandSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onNextSession() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onNotifyNotificationEventFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onNotifyNotificationEventSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onPlayAnimationFail(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onPlayAnimationSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onPlayDeviceAnimation(boolean z, Yx1 yx1) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onPreviewThemeError(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onPreviewThemeProgressChanged(float f) {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onPreviewThemeSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadCurrentWorkoutSessionFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadCurrentWorkoutSessionSuccess(C90 c90) {
        Wg6.c(c90, "workoutSession");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadDataFilesFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadDataFilesProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadDataFilesSuccess(FitnessData[] fitnessDataArr) {
        Wg6.c(fitnessDataArr, "data");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadHWLogFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadHWLogProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadRssiFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadRssiSuccess(int i) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReleaseHandControlFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReleaseHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onRequestHandControlFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onRequestHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onResetHandsFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onResetHandsSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onScanFail(Uk1 uk1) {
        Wg6.c(uk1, "scanError");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSendMicroAppDataFail(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSendMicroAppDataSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSendingEncryptedDataSessionFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSendingEncryptedDataSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetAlarmFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetAlarmSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetBackgroundImageFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetBackgroundImageSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetComplicationFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetComplicationSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetDeviceConfigFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetDeviceConfigSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetFrontLightFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetFrontLightSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetLabelFileFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetLabelFileSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetLocalizationDataFail(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetLocalizationDataSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetMinimumStepThresholdSessionFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetMinimumStepThresholdSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetNotificationFilterFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetNotificationFilterProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetNotificationFilterSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetReplyMessageMappingError(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetReplyMessageMappingSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWatchAppFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWatchAppFileFailed() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWatchAppFileProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWatchAppFileSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWatchAppSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWatchParamsFail(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWatchParamsSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWorkoutGPSDataSessionFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWorkoutGPSDataSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onStopCurrentWorkoutSessionFailed(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onStopCurrentWorkoutSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onVerifySecretKeyFail(Yx1 yx1) {
        Wg6.c(yx1, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onVerifySecretKeySuccess(boolean z) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public final boolean retry(Context context, String str) {
        Wg6.c(context, "context");
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "Retry state: " + getTAG() + ", counter=" + this.retryCounter + ", maxRetries=" + this.maxRetries);
        MFLogManager instance = MFLogManager.getInstance(context);
        instance.addLogForActiveLog(str, "Retry state: " + getTAG() + ", counter=" + this.retryCounter + ", maxRetries=" + this.maxRetries);
        int i = this.retryCounter;
        if (i >= this.maxRetries) {
            return false;
        }
        this.retryCounter = i + 1;
        onEnter();
        return true;
    }

    @DexIgnore
    public final void setMaxRetries(int i) {
        this.maxRetries = i;
    }

    @DexIgnore
    public final void setRetryCounter(int i) {
        this.retryCounter = i;
    }
}
