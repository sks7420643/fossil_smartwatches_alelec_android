package com.misfit.frameworks.buttonservice.communite.ble;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.facebook.internal.NativeProtocol;
import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.K68;
import com.fossil.Vt7;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import com.misfit.frameworks.buttonservice.model.Device;
import com.misfit.frameworks.buttonservice.model.HybridDevice;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BleSession {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public String TAG;
    @DexIgnore
    public BleSessionCallback bleSessionCallback;
    @DexIgnore
    public CommunicateMode communicateMode;
    @DexIgnore
    public Context context;
    @DexIgnore
    public Handler delayHandler;
    @DexIgnore
    public /* final */ DelayTask delayTask;
    @DexIgnore
    public Device device;
    @DexIgnore
    public int endTime;
    @DexIgnore
    public Bundle extraInfoReturned;
    @DexIgnore
    public int failureCode;
    @DexIgnore
    public /* final */ ArrayList<Integer> failureCodes;
    @DexIgnore
    public boolean isExist;
    @DexIgnore
    public boolean isInterruptable;
    @DexIgnore
    public FLogger.Session logSession;
    @DexIgnore
    public MFLog mfLog;
    @DexIgnore
    public /* final */ ArrayList<Integer> requiredPermissionCodes;
    @DexIgnore
    public int retriesCounter;
    @DexIgnore
    public String serial;
    @DexIgnore
    public SessionType sessionType;
    @DexIgnore
    public int startTime;

    @DexIgnore
    public interface BleSessionCallback {
        @DexIgnore
        void broadcastExchangeSecretKeySuccess(String str, String str2);

        @DexIgnore
        void onAskForCurrentSecretKey(String str);

        @DexIgnore
        void onAskForLabelFile(String str, CommunicateMode communicateMode);

        @DexIgnore
        void onAskForLinkServer(CommunicateMode communicateMode, Bundle bundle);

        @DexIgnore
        void onAskForRandomKey(String str);

        @DexIgnore
        void onAskForSecretKey(Bundle bundle);

        @DexIgnore
        void onAskForStopWorkout(String str);

        @DexIgnore
        void onAskForWatchAppFiles(String str, Bundle bundle);

        @DexIgnore
        void onAuthorizeDeviceSuccess(String str);

        @DexIgnore
        void onBleStateResult(int i, Bundle bundle);

        @DexIgnore
        Object onFirmwareLatest();  // void declaration

        @DexIgnore
        void onNeedStartTimer(String str);

        @DexIgnore
        void onReadCurrentWorkoutBeforeSyncSuccess(String str, Bundle bundle);

        @DexIgnore
        void onReceivedSyncData(Bundle bundle);

        @DexIgnore
        void onRequestLatestFirmware(Bundle bundle);

        @DexIgnore
        void onRequestLatestWatchParams(String str, Bundle bundle);

        @DexIgnore
        void onRequestPushSecretKeyToServer(String str, String str2);

        @DexIgnore
        void onStop(int i, List<Integer> list, Bundle bundle, BleSession bleSession);

        @DexIgnore
        Object onUpdateFirmwareFailed();  // void declaration

        @DexIgnore
        Object onUpdateFirmwareSuccess();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final boolean isNull(BleSession bleSession) {
            return bleSession == null || (bleSession instanceof INullSession);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DelayTask implements Runnable {
        @DexIgnore
        public Runnable task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DelayTask() {
        }

        @DexIgnore
        public void run() {
            Runnable runnable = this.task;
            if (runnable == null) {
                return;
            }
            if (runnable != null) {
                runnable.run();
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public final void setTask(Runnable runnable) {
            Wg6.c(runnable, "task");
            this.task = runnable;
        }
    }

    @DexIgnore
    public interface INullSession {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[CommunicateMode.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[CommunicateMode.ENTER_CALIBRATION.ordinal()] = 1;
            $EnumSwitchMapping$0[CommunicateMode.EXIT_CALIBRATION.ordinal()] = 2;
            $EnumSwitchMapping$0[CommunicateMode.APPLY_HAND_POSITION.ordinal()] = 3;
            $EnumSwitchMapping$0[CommunicateMode.MOVE_HAND.ordinal()] = 4;
            $EnumSwitchMapping$0[CommunicateMode.RESET_HAND.ordinal()] = 5;
            $EnumSwitchMapping$0[CommunicateMode.LINK.ordinal()] = 6;
        }
        */
    }

    @DexIgnore
    public BleSession(CommunicateMode communicateMode2, Context context2, String str, String str2, BleSessionCallback bleSessionCallback2) {
        Wg6.c(communicateMode2, "communicateMode");
        Wg6.c(context2, "context");
        Wg6.c(str, "serial");
        Wg6.c(str2, "firmwareVersion");
        this.communicateMode = communicateMode2;
        this.context = context2;
        this.serial = str;
        this.bleSessionCallback = bleSessionCallback2;
        String simpleName = BleSession.class.getSimpleName();
        Wg6.b(simpleName, "BleSession::class.java.simpleName");
        this.TAG = simpleName;
        this.isExist = true;
        this.device = new HybridDevice();
        this.logSession = FLogger.Session.OTHER;
        this.extraInfoReturned = new Bundle();
        this.failureCodes = new ArrayList<>();
        this.requiredPermissionCodes = new ArrayList<>();
        this.delayHandler = new Handler(Looper.getMainLooper());
        this.delayTask = new DelayTask();
        this.sessionType = SessionType.BACK_GROUND;
        this.isExist = true;
        this.startTime = 0;
        this.endTime = 0;
        if (isNeedGetActiveLogForThisSession()) {
            this.mfLog = MFLogManager.getInstance(this.context).getActiveLog(this.serial);
        }
        MFLog mFLog = this.mfLog;
        if (mFLog != null) {
            mFLog.setSerial(mFLog.getSerial());
            mFLog.setSdkVersion(ButtonService.Companion.getSDKVersion());
            mFLog.setFirmwareVersion(str2);
        }
        String simpleName2 = getClass().getSimpleName();
        Wg6.b(simpleName2, "this.javaClass.simpleName");
        this.TAG = simpleName2;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public BleSession(SessionType sessionType2, CommunicateMode communicateMode2, Context context2, String str, String str2, BleSessionCallback bleSessionCallback2) {
        this(communicateMode2, context2, str, str2, bleSessionCallback2);
        Wg6.c(sessionType2, "sessionType");
        Wg6.c(communicateMode2, "communicateMode");
        Wg6.c(context2, "context");
        Wg6.c(str, "serial");
        Wg6.c(str2, "firmwareVersion");
        Wg6.c(bleSessionCallback2, "bleSessionCallback");
        this.sessionType = sessionType2;
    }

    @DexIgnore
    private final boolean isNeedGetActiveLogForThisSession() {
        FLogger.INSTANCE.getLocal().e(this.TAG, ".isNeedGetActiveLogForThisSession");
        CommunicateMode activeCommunicateMode = MFLogManager.getInstance(this.context).getActiveCommunicateMode(this.serial);
        if (activeCommunicateMode != null) {
            Wg6.b(activeCommunicateMode, "MFLogManager.getInstance\u2026          ?: return false");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.e(str, ".isNeedGetActiveLogForThisSession - activeCommunicateMode=" + activeCommunicateMode + ", communicateMode=" + this.communicateMode);
            CommunicateMode communicateMode2 = this.communicateMode;
            if (activeCommunicateMode == communicateMode2 && communicateMode2 != CommunicateMode.LINK) {
                return true;
            }
            if (activeCommunicateMode == CommunicateMode.LINK) {
                switch (WhenMappings.$EnumSwitchMapping$0[this.communicateMode.ordinal()]) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        return true;
                }
            }
            FLogger.INSTANCE.getLocal().e(this.TAG, ".isNeedGetActiveLogForThisSession - result=FALSE");
        }
        return false;
    }

    @DexIgnore
    private final void removeDelayWork() {
        this.delayHandler.removeCallbacks(this.delayTask);
    }

    @DexIgnore
    private final void startWithDelay(Object[] objArr, int i) {
        enterTaskWithDelayTime(new BleSession$startWithDelay$Anon1(this, objArr), i);
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        Wg6.c(bleSession, "bleSession");
        return this.communicateMode != bleSession.communicateMode;
    }

    @DexIgnore
    public void addFailureCode(int i) {
        this.failureCodes.add(Integer.valueOf(i));
    }

    @DexIgnore
    public void addRequiredPermissionCode(int i) {
        this.requiredPermissionCodes.add(Integer.valueOf(i));
    }

    @DexIgnore
    public abstract void buildExtraInfoReturned();

    @DexIgnore
    public boolean canRetry(int i, int i2) {
        return i < getMaxRetries();
    }

    @DexIgnore
    public abstract BleSession copyObject();

    @DexIgnore
    public final BleState createConcreteState(String str) {
        Wg6.c(str, "stateClassName");
        try {
            Class<?> cls = Class.forName(str);
            Wg6.b(cls, "Class.forName(stateClassName)");
            Constructor<?> declaredConstructor = cls.getDeclaredConstructor(cls.getDeclaringClass());
            Wg6.b(declaredConstructor, "innerClass.getDeclaredConstructor(parentClass)");
            declaredConstructor.setAccessible(true);
            Object newInstance = declaredConstructor.newInstance(this);
            if (newInstance != null) {
                return (BleState) newInstance;
            }
            throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.BleState");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local.e(str2, "Inside getState method, cannot instance state " + str + ", e = " + e);
            return null;
        }
    }

    @DexIgnore
    public abstract BleState createNullState();

    @DexIgnore
    public boolean enterState(BleState bleState) {
        Wg6.c(bleState, "newState");
        if (!this.isExist) {
            return false;
        }
        if (!BleState.Companion.isNull(getCurrentState()) && !BleState.Companion.isNull(bleState) && Vt7.j(getCurrentState().getClass().getName(), bleState.getClass().getName(), true)) {
            return true;
        }
        if (!BleState.Companion.isNull(getCurrentState())) {
            getCurrentState().onExit();
        }
        setCurrentState(bleState);
        if (BleState.Companion.isNull(getCurrentState())) {
            return false;
        }
        this.isInterruptable = getCurrentState().isInterruptable();
        boolean onEnter = bleState.onEnter();
        if (!onEnter) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.e(str, "Failed to enter state: " + bleState);
            setCurrentState(createNullState());
            this.isInterruptable = false;
        }
        return onEnter;
    }

    @DexIgnore
    public boolean enterStateAsync(BleState bleState) {
        Wg6.c(bleState, "newState");
        Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new BleSession$enterStateAsync$Anon1(this, bleState, null), 3, null);
        return true;
    }

    @DexIgnore
    public void enterStateWithDelayTime(BleState bleState, int i) {
        Wg6.c(bleState, "state");
        removeDelayWork();
        this.delayTask.setTask(new BleSession$enterStateWithDelayTime$Anon1(this, bleState));
        this.delayHandler.postDelayed(this.delayTask, (long) i);
    }

    @DexIgnore
    public void enterTaskWithDelayTime(Runnable runnable, int i) {
        Wg6.c(runnable, "task");
        removeDelayWork();
        this.delayTask.setTask(new BleSession$enterTaskWithDelayTime$Anon1(runnable));
        this.delayHandler.postDelayed(this.delayTask, (long) i);
    }

    @DexIgnore
    public final BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final CommunicateMode getCommunicateMode() {
        return this.communicateMode;
    }

    @DexIgnore
    public final Context getContext() {
        return this.context;
    }

    @DexIgnore
    public abstract BleState getCurrentState();

    @DexIgnore
    public final Device getDevice() {
        return this.device;
    }

    @DexIgnore
    public final int getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final Bundle getExtraInfoReturned() {
        return this.extraInfoReturned;
    }

    @DexIgnore
    public final int getFailureCode() {
        return this.failureCode;
    }

    @DexIgnore
    public final int getLastFailureCode() {
        if (this.failureCodes.isEmpty()) {
            return 0;
        }
        ArrayList<Integer> arrayList = this.failureCodes;
        Integer num = arrayList.get(arrayList.size() - 1);
        Wg6.b(num, "failureCodes[failureCodes.size - 1]");
        return num.intValue();
    }

    @DexIgnore
    public final FLogger.Session getLogSession() {
        return this.logSession;
    }

    @DexIgnore
    public int getMaxRetries() {
        return 0;
    }

    @DexIgnore
    public final MFLog getMfLog() {
        return this.mfLog;
    }

    @DexIgnore
    public final int getRetriesCounter() {
        return this.retriesCounter;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public final SessionType getSessionType() {
        return this.sessionType;
    }

    @DexIgnore
    public final int getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final String getTAG() {
        return this.TAG;
    }

    @DexIgnore
    public void log(String str) {
        Wg6.c(str, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = MFLogManager.TAG;
        Wg6.b(str2, "MFLogManager.TAG");
        local.d(str2, str);
        MFLog mFLog = this.mfLog;
        if (mFLog != null) {
            if (mFLog != null) {
                mFLog.log('[' + this.serial + "] " + str);
            } else {
                Wg6.i();
                throw null;
            }
        }
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, this.logSession, this.serial, this.TAG, str);
    }

    @DexIgnore
    public boolean mustSendBroadcast() {
        return false;
    }

    @DexIgnore
    public final void onPostStop() {
        setCurrentState(createNullState());
        if (this.startTime == 0) {
            this.endTime = 0;
        }
        long j = (long) (this.endTime - this.startTime);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("___ Stop ");
        sb.append(this.TAG);
        sb.append(": duration=");
        if (j <= 0) {
            j = 0;
        }
        sb.append(K68.d(j * ((long) 1000)));
        sb.append(" ___");
        local.d(str, sb.toString());
    }

    @DexIgnore
    public void onRetry(Object... objArr) {
        Wg6.c(objArr, NativeProtocol.WEB_DIALOG_PARAMS);
    }

    @DexIgnore
    public boolean onStart(Object... objArr) {
        Wg6.c(objArr, NativeProtocol.WEB_DIALOG_PARAMS);
        log("START: " + this.communicateMode);
        return true;
    }

    @DexIgnore
    public void onStop(int i) {
        try {
            buildExtraInfoReturned();
        } catch (Exception e) {
            String message = e.getMessage();
            if (message == null) {
                message = "";
            }
            log(message);
        }
    }

    @DexIgnore
    public void preStart() {
    }

    @DexIgnore
    public final boolean requireBroadCastInAnyCase() {
        SessionType sessionType2 = this.sessionType;
        return sessionType2 == SessionType.UI || sessionType2 == SessionType.SYNC || mustSendBroadcast();
    }

    @DexIgnore
    public final void setBleSessionCallback(BleSessionCallback bleSessionCallback2) {
        this.bleSessionCallback = bleSessionCallback2;
    }

    @DexIgnore
    public final void setCommunicateMode(CommunicateMode communicateMode2) {
        Wg6.c(communicateMode2, "<set-?>");
        this.communicateMode = communicateMode2;
    }

    @DexIgnore
    public final void setContext(Context context2) {
        Wg6.c(context2, "<set-?>");
        this.context = context2;
    }

    @DexIgnore
    public abstract void setCurrentState(BleState bleState);

    @DexIgnore
    public final void setDevice(Device device2) {
        Wg6.c(device2, "<set-?>");
        this.device = device2;
    }

    @DexIgnore
    public final void setEndTime(int i) {
        this.endTime = i;
    }

    @DexIgnore
    public final void setExtraInfoReturned(Bundle bundle) {
        Wg6.c(bundle, "<set-?>");
        this.extraInfoReturned = bundle;
    }

    @DexIgnore
    public final void setFailureCode(int i) {
        this.failureCode = i;
    }

    @DexIgnore
    public final void setLogSession(FLogger.Session session) {
        Wg6.c(session, "<set-?>");
        this.logSession = session;
    }

    @DexIgnore
    public final void setMfLog(MFLog mFLog) {
        this.mfLog = mFLog;
    }

    @DexIgnore
    public final void setRetriesCounter(int i) {
        this.retriesCounter = i;
    }

    @DexIgnore
    public final void setSerial(String str) {
        Wg6.c(str, "<set-?>");
        this.serial = str;
    }

    @DexIgnore
    public final void setSessionType(SessionType sessionType2) {
        Wg6.c(sessionType2, "<set-?>");
        this.sessionType = sessionType2;
    }

    @DexIgnore
    public final void setStartTime(int i) {
        this.startTime = i;
    }

    @DexIgnore
    public final void setTAG(String str) {
        Wg6.c(str, "<set-?>");
        this.TAG = str;
    }

    @DexIgnore
    public final boolean start(Object... objArr) {
        Wg6.c(objArr, NativeProtocol.WEB_DIALOG_PARAMS);
        this.startTime = (int) (System.currentTimeMillis() / ((long) 1000));
        removeDelayWork();
        preStart();
        return onStart(Arrays.copyOf(objArr, objArr.length));
    }

    @DexIgnore
    public void stop(int i) {
        boolean z = true;
        int lastFailureCode = getLastFailureCode();
        FLogger.INSTANCE.getLocal().d(this.TAG, ".stop - failureCode=" + i + ", lastFailureCode=" + lastFailureCode);
        addFailureCode(i);
        if (!BleState.Companion.isNull(getCurrentState())) {
            getCurrentState().stopTimeout();
            getCurrentState().cancelCurrentBleTask();
            enterState(createNullState());
        }
        this.delayHandler.removeCallbacks(this.delayTask);
        if (!((i == 0 || i == 1611 || i == 8888) ? false : true) || !canRetry(this.retriesCounter, i)) {
            this.endTime = (int) (System.currentTimeMillis() / ((long) 1000));
            this.failureCode = i;
            if (i != 0) {
                z = false;
            }
            if (z) {
                FLogger.INSTANCE.getLocal().d(this.TAG, "SUCCESS");
                log("SUCCESS");
            } else {
                FLogger.INSTANCE.getLocal().e(this.TAG, "FAIL: " + this.failureCode);
                if (i != 10000) {
                    FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, this.logSession, this.serial, this.TAG, "FAIL: " + this.communicateMode);
                }
                MFLog mFLog = this.mfLog;
                if (mFLog != null) {
                    if (mFLog != null) {
                        mFLog.log('[' + this.serial + "] FAIL: " + this.failureCode);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                MFLog mFLog2 = this.mfLog;
                if (mFLog2 != null) {
                    if (mFLog2 != null) {
                        mFLog2.setResultCode(this.failureCode);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
            MFLog mFLog3 = this.mfLog;
            if (mFLog3 != null) {
                if (mFLog3 != null) {
                    mFLog3.setErrorCodes(this.failureCodes);
                    FLogger.INSTANCE.getLocal().d(this.TAG, "error code size: " + this.failureCodes.size());
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            FLogger.INSTANCE.getLocal().d(this.TAG, "Call onStop");
            onStop(this.failureCode);
            this.isExist = false;
            BleSessionCallback bleSessionCallback2 = this.bleSessionCallback;
            if (bleSessionCallback2 != null) {
                if (bleSessionCallback2 != null) {
                    bleSessionCallback2.onStop(this.failureCode, this.requiredPermissionCodes, this.extraInfoReturned, this);
                    this.bleSessionCallback = null;
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            onPostStop();
            return;
        }
        this.retriesCounter++;
        log("RETRY " + this.retriesCounter);
        enterTaskWithDelayTime(new BleSession$stop$Anon1(this), 500);
    }
}
