package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import com.fossil.As1;
import com.fossil.Br1;
import com.fossil.Cr1;
import com.fossil.Cs1;
import com.fossil.Dl1;
import com.fossil.Dr1;
import com.fossil.Er1;
import com.fossil.Es1;
import com.fossil.Fs1;
import com.fossil.Gs1;
import com.fossil.Hr1;
import com.fossil.Hs1;
import com.fossil.Im7;
import com.fossil.Is1;
import com.fossil.Js1;
import com.fossil.Ls1;
import com.fossil.Lw1;
import com.fossil.Mm1;
import com.fossil.Mr1;
import com.fossil.Ms1;
import com.fossil.Nn1;
import com.fossil.Nr1;
import com.fossil.Nw1;
import com.fossil.Or1;
import com.fossil.Os1;
import com.fossil.Ou1;
import com.fossil.Pr1;
import com.fossil.Ps1;
import com.fossil.Qr1;
import com.fossil.Qs1;
import com.fossil.Rr1;
import com.fossil.Rs1;
import com.fossil.Ru1;
import com.fossil.Ry1;
import com.fossil.Sr1;
import com.fossil.Tr1;
import com.fossil.Ts1;
import com.fossil.Uk1;
import com.fossil.Ur1;
import com.fossil.Vq1;
import com.fossil.Vr1;
import com.fossil.Wq1;
import com.fossil.Wr1;
import com.fossil.Xr1;
import com.fossil.Yn1;
import com.fossil.Yq1;
import com.fossil.Yr1;
import com.fossil.Yx1;
import com.fossil.Zk1;
import com.fossil.Zm1;
import com.fossil.Zn1;
import com.fossil.Zq1;
import com.fossil.Zr1;
import com.fossil.blesdk.device.data.notification.NotificationFilter;
import com.fossil.blesdk.device.feature.CustomCommandFeature;
import com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor;
import com.fossil.fitness.FitnessData;
import com.fossil.fitness.GpsDataPoint;
import com.fossil.fitness.GpsDataProvider;
import com.google.gson.Gson;
import com.mapped.A70;
import com.mapped.C90;
import com.mapped.Cd6;
import com.mapped.H60;
import com.mapped.HandMovingConfig;
import com.mapped.N50;
import com.mapped.N70;
import com.mapped.Na0;
import com.mapped.Nd0;
import com.mapped.P50;
import com.mapped.Q40;
import com.mapped.Q50;
import com.mapped.Qg6;
import com.mapped.R50;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Ta0;
import com.mapped.Tc0;
import com.mapped.V60;
import com.mapped.Wa0;
import com.mapped.Wg6;
import com.mapped.Yb0;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.ble.ScanService;
import com.misfit.frameworks.buttonservice.communite.ble.BleAdapter;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.extensions.AlarmExtensionKt;
import com.misfit.frameworks.buttonservice.extensions.LocationExtensionKt;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.calibration.DianaCalibrationObj;
import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.misfit.frameworks.buttonservice.utils.ThemeExtentionKt;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl extends BleAdapter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public Q40 deviceObj;
    @DexIgnore
    public boolean isScanning;
    @DexIgnore
    public HashMap<Zm1, R60> mDeviceConfiguration;
    @DexIgnore
    public Q40.Bi mDeviceStateListener;
    @DexIgnore
    public ScanService scanService;
    @DexIgnore
    public byte[] tSecretKey;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class BleScanServiceCallback implements ScanService.Callback {
        @DexIgnore
        public /* final */ ISessionSdkCallback callback;

        @DexIgnore
        public BleScanServiceCallback(ISessionSdkCallback iSessionSdkCallback) {
            this.callback = iSessionSdkCallback;
        }

        @DexIgnore
        public final ISessionSdkCallback getCallback() {
            return this.callback;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.ble.ScanService.Callback
        public void onDeviceFound(Q40 q40, int i) {
            Wg6.c(q40, "device");
            if (q40.getState() == Q40.Ci.CONNECTED || q40.getState() == Q40.Ci.UPGRADING_FIRMWARE) {
                i = 0;
            } else if (q40.P() == Q40.Ai.BONDED) {
                i = ScanService.RETRIEVE_DEVICE_BOND_RSSI_MARK;
            }
            ISessionSdkCallback iSessionSdkCallback = this.callback;
            if (iSessionSdkCallback != null) {
                iSessionSdkCallback.onDeviceFound(q40, i);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.ble.ScanService.Callback
        public void onScanFail(Uk1 uk1) {
            Wg6.c(uk1, "scanError");
            ISessionSdkCallback iSessionSdkCallback = this.callback;
            if (iSessionSdkCallback != null) {
                iSessionSdkCallback.onScanFail(uk1);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[Q40.Ci.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[Q40.Ci.DISCONNECTED.ordinal()] = 1;
            $EnumSwitchMapping$0[Q40.Ci.CONNECTED.ordinal()] = 2;
            $EnumSwitchMapping$0[Q40.Ci.UPGRADING_FIRMWARE.ordinal()] = 3;
            $EnumSwitchMapping$0[Q40.Ci.CONNECTING.ordinal()] = 4;
            $EnumSwitchMapping$0[Q40.Ci.DISCONNECTING.ordinal()] = 5;
        }
        */
    }

    /*
    static {
        String simpleName = BleAdapterImpl.class.getSimpleName();
        Wg6.b(simpleName, "BleAdapterImpl::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl(Context context, String str, String str2) {
        super(context, str, str2);
        Wg6.c(context, "context");
        Wg6.c(str, "serial");
        Wg6.c(str2, "macAddress");
    }

    @DexIgnore
    private final int getGattState(Q40 q40) {
        if (q40 != null) {
            int i = WhenMappings.$EnumSwitchMapping$0[q40.getState().ordinal()];
            if (i != 1) {
                if (i == 2 || i == 3) {
                    return 2;
                }
                if (i != 4) {
                    return i != 5 ? 0 : 3;
                }
                return 1;
            }
        } else {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, "Inside " + TAG + ".getGattState - serial=" + getSerial() + ", shineDevice=NULL");
        }
    }

    @DexIgnore
    private final void requestPairing(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "requestPairing(), serial=" + getSerial());
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Ur1 ur1 = (Ur1) q40.g0(Ur1.class);
            if (ur1 == null || ur1.u().s(new BleAdapterImpl$requestPairing$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).r(new BleAdapterImpl$requestPairing$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback)) == null) {
                log(session, "Device[" + getSerial() + "] does not support RequestPairingFeature");
                iSessionSdkCallback.onAuthorizeDeviceFailed();
                Cd6 cd6 = Cd6.a;
                return;
            }
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Lw1> applyTheme(FLogger.Session session, Lw1 lw1, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(lw1, "themePackage");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Wa0 wa0 = (Wa0) q40.g0(Wa0.class);
            if (wa0 != null) {
                Yb0<Lw1> x = wa0.k(lw1).x(new BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon1(this, lw1, iSessionSdkCallback, session));
                x.w(new BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon2(this, lw1, iSessionSdkCallback, session));
                return x.v(new BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon3(this, lw1, iSessionSdkCallback, session));
            }
            log(session, "Device[" + getSerial() + "] does not support applyTheme");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Lw1> applyTheme(FLogger.Session session, ThemeData themeData, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(themeData, "themeData");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Wa0 wa0 = (Wa0) q40.g0(Wa0.class);
            if (wa0 != null) {
                Yb0<Lw1> x = wa0.r(ThemeExtentionKt.toThemeEditor(themeData)).x(new BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon4(this, themeData, iSessionSdkCallback, session));
                x.w(new BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon5(this, themeData, iSessionSdkCallback, session));
                return x.v(new BleAdapterImpl$applyTheme$$inlined$let$lambda$Anon6(this, themeData, iSessionSdkCallback, session));
            }
            log(session, "Device[" + getSerial() + "] does not support applyTheme");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Q40 buildDeviceBySerial(String str, String str2, long j) {
        Wg6.c(str, "serial");
        Wg6.c(str2, "macAddress");
        if (this.scanService == null) {
            this.scanService = new ScanService(getContext(), null, j);
        }
        ScanService scanService2 = this.scanService;
        if (scanService2 != null) {
            return scanService2.buildDeviceBySerial(str, str2);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> calibrationApplyHandPosition(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Apply Hands Positions");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Cs1 cs1 = (Cs1) q40.g0(Cs1.class);
            if (cs1 != null) {
                return cs1.W().s(new BleAdapterImpl$calibrationApplyHandPosition$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).r(new BleAdapterImpl$calibrationApplyHandPosition$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingCalibrationPositionFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> calibrationMoveHand(FLogger.Session session, HandCalibrationObj handCalibrationObj, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(handCalibrationObj, "handCalibrationObj");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        DianaCalibrationObj consume = DianaCalibrationObj.Companion.consume(handCalibrationObj);
        HandMovingConfig[] handMovingConfigArr = {new HandMovingConfig(consume.getHandId(), consume.getDegree(), consume.getHandMovingDirection(), consume.getHandMovingSpeed())};
        log(session, "Move Hand: handMovingType=" + consume.getHandMovingType() + ", handMovingConfigs=" + handMovingConfigArr + ' ');
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Or1 or1 = (Or1) q40.g0(Or1.class);
            if (or1 != null) {
                return or1.p(consume.getHandMovingType(), handMovingConfigArr).s(new BleAdapterImpl$calibrationMoveHand$$inlined$let$lambda$Anon1(this, consume, handMovingConfigArr, session, iSessionSdkCallback)).r(new BleAdapterImpl$calibrationMoveHand$$inlined$let$lambda$Anon2(this, consume, handMovingConfigArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support MovingHandsFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> calibrationReleaseHandControl(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Release Hand Control");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Tr1 tr1 = (Tr1) q40.g0(Tr1.class);
            if (tr1 != null) {
                return tr1.L().s(new BleAdapterImpl$calibrationReleaseHandControl$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).r(new BleAdapterImpl$calibrationReleaseHandControl$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support ReleasingHandsFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> calibrationRequestHandControl(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Request Hand Control");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Vr1 vr1 = (Vr1) q40.g0(Vr1.class);
            if (vr1 != null) {
                return vr1.O().s(new BleAdapterImpl$calibrationRequestHandControl$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).r(new BleAdapterImpl$calibrationRequestHandControl$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support RequestingHandsFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> calibrationResetHandToZeroDegree(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Reset Hands to 0 degrees.");
        HandMovingConfig[] handMovingConfigArr = {new HandMovingConfig(N50.HOUR, 0, P50.SHORTEST_PATH, Q50.FULL), new HandMovingConfig(N50.MINUTE, 0, P50.SHORTEST_PATH, Q50.FULL), new HandMovingConfig(N50.SUB_EYE, 0, P50.SHORTEST_PATH, Q50.FULL)};
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Or1 or1 = (Or1) q40.g0(Or1.class);
            if (or1 != null) {
                return or1.p(R50.POSITION, handMovingConfigArr).s(new BleAdapterImpl$calibrationResetHandToZeroDegree$$inlined$let$lambda$Anon1(this, handMovingConfigArr, session, iSessionSdkCallback)).r(new BleAdapterImpl$calibrationResetHandToZeroDegree$$inlined$let$lambda$Anon2(this, handMovingConfigArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support MovingHandsFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void clearCache(FLogger.Session session) {
        Wg6.c(session, "logSession");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Clear cache, serial=" + getSerial());
        Q40 q40 = this.deviceObj;
        if (q40 == null || q40.l() == null) {
            log(session, "clearCache() failed, deviceObj is null.");
            Cd6 cd6 = Cd6.a;
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public void closeConnection(FLogger.Session session, boolean z) {
        Wg6.c(session, "logSession");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Close Connection, serial=" + getSerial());
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Br1 br1 = (Br1) q40.g0(Br1.class);
            if (br1 != null) {
                br1.a();
                log(session, "Close Connection, serial=" + getSerial() + ", OK");
                return;
            }
            log(session, "Device[" + getSerial() + "] does not support DisconnectingFeature");
            return;
        }
        logAppError(session, "closeConnection failed. DeviceObject is null. Connect has not been established.", ErrorCodeBuilder.Step.DISCONNECT, ErrorCodeBuilder.AppError.UNKNOWN);
    }

    @DexIgnore
    public final Yb0<Cd6> configureMicroApp(FLogger.Session session, List<Nd0> list, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(list, "mappings");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Configure MicroApp, mapping=" + list);
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Yq1 yq1 = (Yq1) q40.g0(Yq1.class);
            if (yq1 != null) {
                Object[] array = list.toArray(new Nd0[0]);
                if (array != null) {
                    return yq1.C((Nd0[]) array).x(new BleAdapterImpl$configureMicroApp$$inlined$let$lambda$Anon1(this, list, session, iSessionSdkCallback)).v(new BleAdapterImpl$configureMicroApp$$inlined$let$lambda$Anon2(this, list, session, iSessionSdkCallback));
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            log(session, "Device[" + getSerial() + "] does not support ConfiguringMicroAppFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> confirmAuthorization(FLogger.Session session, long j, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "confirmAuthorization(), timeout=" + j);
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Vq1 vq1 = (Vq1) q40.g0(Vq1.class);
            if (vq1 != null) {
                return vq1.d(j).s(new BleAdapterImpl$confirmAuthorization$$inlined$let$lambda$Anon1(this, j, session, iSessionSdkCallback)).r(new BleAdapterImpl$confirmAuthorization$$inlined$let$lambda$Anon2(this, j, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support AuthorizationFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Yb0<String> doOTA(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(bArr, "firmwareData");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Do OTA");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Rr1 rr1 = (Rr1) q40.g0(Rr1.class);
            if (rr1 != null) {
                Yb0<String> E = rr1.E(bArr, null);
                E.w(new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback));
                return E.x(new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback)).v(new BleAdapterImpl$doOTA$$inlined$let$lambda$Anon3(this, bArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support OTAFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Boolean enableMaintainConnection(FLogger.Session session) {
        Wg6.c(session, "logSession");
        log(session, "Enable Maintain Connection");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Cr1 cr1 = (Cr1) q40.g0(Cr1.class);
            if (cr1 != null) {
                return Boolean.valueOf(cr1.F());
            }
            log(session, "Device[" + getSerial() + "] does not support EnablingMaintainConnectionFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> eraseDataFile(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Erase DataFiles");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Na0 na0 = (Na0) q40.g0(Na0.class);
            if (na0 != null) {
                return na0.cleanUp().x(new BleAdapterImpl$eraseDataFile$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).v(new BleAdapterImpl$eraseDataFile$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support CleanUpDeviceFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void error(FLogger.Session session, String str, ErrorCodeBuilder.Step step, String str2) {
        Wg6.c(session, "logSession");
        Wg6.c(str, "errorCode");
        Wg6.c(step, "step");
        Wg6.c(str2, "errorMessage");
        MFLog activeLog = MFLogManager.getInstance(getContext()).getActiveLog(getSerial());
        if (activeLog != null) {
            activeLog.error('[' + getSerial() + "] " + str2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, '[' + getSerial() + "] error=" + str + ' ' + str2);
        FLogger.INSTANCE.getRemote().e(FLogger.Component.BLE, session, getSerial(), TAG, str, step, str2);
    }

    @DexIgnore
    public final Zb0<byte[]> exchangeSecretKey(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        Zb0<byte[]> r;
        Wg6.c(session, "logSession");
        Wg6.c(bArr, "data");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Exchange Secret Key");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Dr1 dr1 = (Dr1) q40.g0(Dr1.class);
            if (dr1 != null && (r = dr1.I(bArr).s(new BleAdapterImpl$exchangeSecretKey$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).r(new BleAdapterImpl$exchangeSecretKey$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback))) != null) {
                return r;
            }
            log(session, "Device[" + getSerial() + "] does not support ExchangingSecretKeyFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Zk1> fetchDeviceInfo(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Fetch Device Information");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Er1 er1 = (Er1) q40.g0(Er1.class);
            if (er1 != null) {
                return er1.m().x(new BleAdapterImpl$fetchDeviceInfo$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).v(new BleAdapterImpl$fetchDeviceInfo$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support FetchingDeviceInformationFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public int getBatteryLevel() {
        HashMap<Zm1, R60> hashMap = this.mDeviceConfiguration;
        if (hashMap == null || !hashMap.containsKey(Zm1.BATTERY)) {
            return -1;
        }
        R60 r60 = hashMap.get(Zm1.BATTERY);
        if (r60 != null) {
            return ((Mm1) r60).getPercentage();
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BatteryConfig");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getCurrentLabelVersion() {
        Zk1 M;
        Dl1 eLabelVersions;
        Ry1 currentVersion;
        String shortDescription;
        Q40 q40 = this.deviceObj;
        return (q40 == null || (M = q40.M()) == null || (eLabelVersions = M.getELabelVersions()) == null || (currentVersion = eLabelVersions.getCurrentVersion()) == null || (shortDescription = currentVersion.getShortDescription()) == null) ? "0.0" : shortDescription;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getCurrentWatchParamVersion() {
        Zk1 M;
        Dl1 watchParameterVersions;
        Ry1 currentVersion;
        String shortDescription;
        Q40 q40 = this.deviceObj;
        return (q40 == null || (M = q40.M()) == null || (watchParameterVersions = M.getWatchParameterVersions()) == null || (currentVersion = watchParameterVersions.getCurrentVersion()) == null || (shortDescription = currentVersion.getShortDescription()) == null) ? "0.0" : shortDescription;
    }

    @DexIgnore
    public final Zb0<HashMap<Zm1, R60>> getDeviceConfig(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Get Device Configuration");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Hr1 hr1 = (Hr1) q40.g0(Hr1.class);
            if (hr1 != null) {
                return hr1.R().x(new BleAdapterImpl$getDeviceConfig$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).v(new BleAdapterImpl$getDeviceConfig$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support GettingConfigurationsFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getDeviceModel() {
        Zk1 M;
        String modelNumber;
        Q40 q40 = this.deviceObj;
        return (q40 == null || (M = q40.M()) == null || (modelNumber = M.getModelNumber()) == null) ? "" : modelNumber;
    }

    @DexIgnore
    public final Q40 getDeviceObj() {
        return this.deviceObj;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getFirmwareVersion() {
        Zk1 M;
        String firmwareVersion;
        Q40 q40 = this.deviceObj;
        return (q40 == null || (M = q40.M()) == null || (firmwareVersion = M.getFirmwareVersion()) == null) ? "" : firmwareVersion;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public int getGattState() {
        return getGattState(this.deviceObj);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public HeartRateMode getHeartRateMode() {
        HeartRateMode heartRateMode;
        HashMap<Zm1, R60> hashMap = this.mDeviceConfiguration;
        if (hashMap != null) {
            if (hashMap.containsKey(Zm1.HEART_RATE_MODE)) {
                HeartRateMode.Companion companion = HeartRateMode.Companion;
                R60 r60 = hashMap.get(Zm1.HEART_RATE_MODE);
                if (r60 != null) {
                    heartRateMode = companion.consume(((V60) r60).getHeartRateMode());
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.HeartRateModeConfig");
                }
            } else {
                heartRateMode = HeartRateMode.NONE;
            }
            if (heartRateMode != null) {
                return heartRateMode;
            }
        }
        return HeartRateMode.NONE;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public int getHidState() {
        return 0;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getLocale() {
        Zk1 M;
        String localeString;
        Q40 q40 = this.deviceObj;
        return (q40 == null || (M = q40.M()) == null || (localeString = M.getLocaleString()) == null) ? "" : localeString;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getLocaleVersion() {
        Zk1 M;
        Dl1 localeVersions;
        Q40 q40 = this.deviceObj;
        return String.valueOf((q40 == null || (M = q40.M()) == null || (localeVersions = M.getLocaleVersions()) == null) ? null : localeVersions.getCurrentVersion());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public short getMicroAppMajorVersion() {
        Zk1 M;
        Ry1 microAppVersion;
        Q40 q40 = this.deviceObj;
        if (q40 == null || (M = q40.M()) == null || (microAppVersion = M.getMicroAppVersion()) == null) {
            return 255;
        }
        return (short) microAppVersion.getMajor();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public short getMicroAppMinorVersion() {
        Zk1 M;
        Ry1 microAppVersion;
        Q40 q40 = this.deviceObj;
        if (q40 == null || (M = q40.M()) == null || (microAppVersion = M.getMicroAppVersion()) == null) {
            return 255;
        }
        return (short) microAppVersion.getMinor();
    }

    @DexIgnore
    public final byte[] getSecretKeyThroughSDK(FLogger.Session session) {
        Wg6.c(session, "logSession");
        log(session, "Get Secret Key Through SDK");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Ta0 ta0 = (Ta0) q40.g0(Ta0.class);
            if (ta0 != null) {
                setTSecretKey(ta0.A());
                log(session, "Secret key from SDK " + getTSecretKey());
                return getTSecretKey();
            }
            log(session, "Device[" + getSerial() + "] does not support GettingSecretKeyFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public int getSupportedWatchParamMajor() {
        Zk1 M;
        Dl1 watchParameterVersions;
        Ry1 supportedVersion;
        Q40 q40 = this.deviceObj;
        if (q40 == null || (M = q40.M()) == null || (watchParameterVersions = M.getWatchParameterVersions()) == null || (supportedVersion = watchParameterVersions.getSupportedVersion()) == null) {
            return 0;
        }
        return supportedVersion.getMajor();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public int getSupportedWatchParamMinor() {
        Zk1 M;
        Dl1 watchParameterVersions;
        Ry1 supportedVersion;
        Q40 q40 = this.deviceObj;
        if (q40 == null || (M = q40.M()) == null || (watchParameterVersions = M.getWatchParameterVersions()) == null || (supportedVersion = watchParameterVersions.getSupportedVersion()) == null) {
            return 0;
        }
        return supportedVersion.getMinor();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public String getSupportedWatchParamVersion() {
        Zk1 M;
        Dl1 watchParameterVersions;
        Ry1 supportedVersion;
        String shortDescription;
        Q40 q40 = this.deviceObj;
        return (q40 == null || (M = q40.M()) == null || (watchParameterVersions = M.getWatchParameterVersions()) == null || (supportedVersion = watchParameterVersions.getSupportedVersion()) == null || (shortDescription = supportedVersion.getShortDescription()) == null) ? "0.0" : shortDescription;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public byte[] getTSecretKey() {
        return this.tSecretKey;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public Ry1 getUiPackageOSVersion() {
        Zk1 M;
        Q40 q40 = this.deviceObj;
        if (q40 == null || (M = q40.M()) == null) {
            return null;
        }
        return M.getUiPackageOSVersion();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public VibrationStrengthObj getVibrationStrength() {
        VibrationStrengthObj consumeSDKVibrationStrengthLevel;
        HashMap<Zm1, R60> hashMap = this.mDeviceConfiguration;
        if (hashMap != null) {
            if (hashMap.containsKey(Zm1.VIBE_STRENGTH)) {
                R60 r60 = hashMap.get(Zm1.VIBE_STRENGTH);
                if (r60 != null) {
                    A70.Ai vibeStrengthLevel = ((A70) r60).getVibeStrengthLevel();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.e(str, "Device config not null, vibeStrengthConfig not null, return vibrationStrength: value = " + vibeStrengthLevel);
                    consumeSDKVibrationStrengthLevel = VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel$default(VibrationStrengthObj.Companion, vibeStrengthLevel, false, 2, null);
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.VibeStrengthConfig");
                }
            } else {
                FLogger.INSTANCE.getLocal().e(TAG, "Device config not null but vibeStrengthConfig is null, vibrationStrength return default value MEDIUM");
                consumeSDKVibrationStrengthLevel = VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel(A70.Ai.MEDIUM, true);
            }
            if (consumeSDKVibrationStrengthLevel != null) {
                return consumeSDKVibrationStrengthLevel;
            }
        }
        FLogger.INSTANCE.getLocal().e(TAG, "Device config null, vibrationStrength return default value MEDIUM");
        return VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel(A70.Ai.MEDIUM, true);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public boolean isDeviceReady() {
        boolean z = false;
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            if (q40.getState() == Q40.Ci.CONNECTED) {
                z = true;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append(".isDeviceReady() - serial=");
            sb.append(getSerial());
            sb.append(", state=");
            sb.append(q40.getState());
            sb.append(", ready=");
            sb.append(!z ? "NO" : "YES");
            local.d(str, sb.toString());
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.e(str2, "Is device ready " + getSerial() + ". FAILED: deviceObject is NULL");
        }
        return z;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleAdapter
    public <T> T isSupportedFeature(Class<T> cls) {
        Wg6.c(cls, "feature");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            return (T) q40.g0(cls);
        }
        return null;
    }

    @DexIgnore
    public final void logAppError(FLogger.Session session, String str, ErrorCodeBuilder.Step step, ErrorCodeBuilder.AppError appError) {
        Wg6.c(session, "logSession");
        Wg6.c(str, "message");
        Wg6.c(step, "step");
        Wg6.c(appError, "error");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, '[' + getSerial() + "] " + str + ", appError=" + appError);
        error(session, ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, appError), step, str);
    }

    @DexIgnore
    public final void logSdkError(FLogger.Session session, String str, ErrorCodeBuilder.Step step, Yx1 yx1) {
        Wg6.c(session, "logSession");
        Wg6.c(str, "message");
        Wg6.c(step, "step");
        Wg6.c(yx1, "sdkError");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, '[' + getSerial() + "] " + str + ", sdkError=" + yx1.getErrorCode().getLogName());
        error(session, ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, yx1), step, str);
    }

    @DexIgnore
    public final Zb0<Cd6> notifyMusicEvent(FLogger.Session session, N70 n70) {
        Wg6.c(session, "logSession");
        Wg6.c(n70, "musicEvent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Notify Music Event, musicEvent=" + n70);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Notify Music Event");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Qr1 qr1 = (Qr1) q40.g0(Qr1.class);
            if (qr1 != null) {
                return qr1.i(n70);
            }
            log(session, "Device[" + getSerial() + "] does not support NotifyingMusicEventFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> notifyNotificationEvent(FLogger.Session session, NotificationBaseObj notificationBaseObj, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(notificationBaseObj, "notifyNotificationEvent");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Do notifyNotificationEvent");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Pr1 pr1 = (Pr1) q40.g0(Pr1.class);
            DianaNotificationObj dianaNotificationObj = (DianaNotificationObj) notificationBaseObj;
            if (pr1 != null) {
                return pr1.z(dianaNotificationObj.toSDKNotifyNotificationEvent()).s(new BleAdapterImpl$notifyNotificationEvent$$inlined$let$lambda$Anon1(this, dianaNotificationObj, session, iSessionSdkCallback)).r(new BleAdapterImpl$notifyNotificationEvent$$inlined$let$lambda$Anon2(this, dianaNotificationObj, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support notifyNotificationEvent");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final ThemeData parseBinaryToThemeData(byte[] bArr) {
        ThemeData themeData;
        Wg6.c(bArr, "byteArray");
        Lw1 a2 = Lw1.CREATOR.a(bArr);
        ThemeEditor edit = a2 != null ? a2.edit() : null;
        if (!(edit instanceof Nw1)) {
            edit = null;
        }
        Nw1 nw1 = (Nw1) edit;
        return (nw1 == null || (themeData = ThemeExtentionKt.toThemeData(nw1)) == null) ? new ThemeData(null, null, 3, null) : themeData;
    }

    @DexIgnore
    public final Zb0<Cd6> playDeviceAnimation(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Play Device Animation");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Nr1 nr1 = (Nr1) q40.g0(Nr1.class);
            if (nr1 != null) {
                return nr1.c().s(new BleAdapterImpl$playDeviceAnimation$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).r(new BleAdapterImpl$playDeviceAnimation$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support HandAnimationFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Yb0<Cd6> previewTheme(FLogger.Session session, Lw1 lw1, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(lw1, "themePackage");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Wa0 wa0 = (Wa0) q40.g0(Wa0.class);
            if (wa0 != null) {
                Yb0<Cd6> x = wa0.N(lw1).x(new BleAdapterImpl$previewTheme$$inlined$let$lambda$Anon1(this, lw1, iSessionSdkCallback, session));
                x.w(new BleAdapterImpl$previewTheme$$inlined$let$lambda$Anon2(this, lw1, iSessionSdkCallback, session));
                return x.v(new BleAdapterImpl$previewTheme$$inlined$let$lambda$Anon3(this, lw1, iSessionSdkCallback, session));
            }
            log(session, "Device[" + getSerial() + "] does not support applyTheme");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Yb0<Cd6> previewTheme(FLogger.Session session, ThemeEditor themeEditor, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(themeEditor, "themeEditor");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Wa0 wa0 = (Wa0) q40.g0(Wa0.class);
            if (wa0 != null) {
                Yb0<Cd6> x = wa0.U(themeEditor).x(new BleAdapterImpl$previewTheme$$inlined$let$lambda$Anon4(this, themeEditor, iSessionSdkCallback, session));
                x.w(new BleAdapterImpl$previewTheme$$inlined$let$lambda$Anon5(this, themeEditor, iSessionSdkCallback, session));
                return x.v(new BleAdapterImpl$previewTheme$$inlined$let$lambda$Anon6(this, themeEditor, iSessionSdkCallback, session));
            }
            log(session, "Device[" + getSerial() + "] does not support applyTheme");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<C90> readCurrentWorkoutSession(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        Zb0<C90> r;
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Get Workout Session");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Mr1 mr1 = (Mr1) q40.g0(Mr1.class);
            if (mr1 != null && (r = mr1.V().s(new BleAdapterImpl$readCurrentWorkoutSession$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).r(new BleAdapterImpl$readCurrentWorkoutSession$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback))) != null) {
                return r;
            }
            log(session, "Device[" + getSerial() + "] does not support GettingWorkoutSessionFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Yb0<FitnessData[]> readDataFile(FLogger.Session session, H60 h60, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(h60, "biometricProfile");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Read DataFiles");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Rs1 rs1 = (Rs1) q40.g0(Rs1.class);
            if (rs1 != null) {
                Yb0<FitnessData[]> g = rs1.g(h60);
                g.w(new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon1(this, h60, session, iSessionSdkCallback));
                return g.x(new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon2(this, h60, session, iSessionSdkCallback)).v(new BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon3(this, h60, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SynchronizationFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Integer> readRssi(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Read Rssi");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Sr1 sr1 = (Sr1) q40.g0(Sr1.class);
            if (sr1 != null) {
                return sr1.H().s(new BleAdapterImpl$readRssi$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).r(new BleAdapterImpl$readRssi$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support ReadingRSSIFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void registerBluetoothStateCallback(Q40.Bi bi) {
        Wg6.c(bi, Constants.CALLBACK);
        this.mDeviceStateListener = bi;
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            q40.B(bi);
        }
    }

    @DexIgnore
    public final void sendCustomCommand(CustomRequest customRequest) {
        Zb0 sendCustomCommand$default;
        Zb0 s;
        Wg6.c(customRequest, Constants.COMMAND);
        log(FLogger.Session.OTHER, "Send Custom Command: " + customRequest);
        Q40 q40 = this.deviceObj;
        if (!(q40 instanceof Zq1)) {
            q40 = null;
        }
        Zq1 zq1 = (Zq1) q40;
        if (zq1 == null || (sendCustomCommand$default = CustomCommandFeature.DefaultImpls.sendCustomCommand$default(zq1, customRequest.getCustomCommand(), Nn1.DC, 0, 0, 12, (Object) null)) == null || (s = sendCustomCommand$default.s(new BleAdapterImpl$sendCustomCommand$Anon1(this, customRequest))) == null || s.r(new BleAdapterImpl$sendCustomCommand$Anon2(this)) == null) {
            log(FLogger.Session.OTHER, "Send Custom Command not support");
            Cd6 cd6 = Cd6.a;
        }
    }

    @DexIgnore
    public final Zb0<Cd6> sendNotification(FLogger.Session session, NotificationBaseObj notificationBaseObj) {
        Wg6.c(session, "logSession");
        Wg6.c(notificationBaseObj, "notification");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Notification, notification=" + notificationBaseObj);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Notification");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Wr1 wr1 = (Wr1) q40.g0(Wr1.class);
            if (wr1 != null) {
                Zn1 sDKNotification = notificationBaseObj.toSDKNotification();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "send sdk notification " + sDKNotification.toString());
                return wr1.s(sDKNotification);
            }
            log(session, "Device[" + getSerial() + "] does not support SendingAppNotificationFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> sendRespond(FLogger.Session session, Tc0 tc0) {
        Wg6.c(session, "logSession");
        Wg6.c(tc0, "deviceData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Respond, deviceData=" + tc0);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Respond");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Xr1 xr1 = (Xr1) q40.g0(Xr1.class);
            if (xr1 != null) {
                return xr1.y(tc0);
            }
            log(session, "Device[" + getSerial() + "] does not support SendingDataFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> sendTrackInfo(FLogger.Session session, Yn1 yn1) {
        Wg6.c(session, "logSession");
        Wg6.c(yn1, "trackInfo");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Send Track Info, trackInfo=" + yn1);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Send Track Info");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Zr1 zr1 = (Zr1) q40.g0(Zr1.class);
            if (zr1 != null) {
                return zr1.Y(yn1);
            }
            log(session, "Device[" + getSerial() + "] does not support SendingTrackInfoFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> sendingEncryptedData(byte[] bArr, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(bArr, "encryptedData");
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "sendingEncryptedData");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Yr1 yr1 = (Yr1) q40.g0(Yr1.class);
            if (yr1 != null) {
                return yr1.x(bArr).x(new BleAdapterImpl$sendingEncryptedData$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).v(new BleAdapterImpl$sendingEncryptedData$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SendingEncryptedDataFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> setAlarms(FLogger.Session session, List<AlarmSetting> list, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(list, "alarms");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set List Alarms: " + list);
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            As1 as1 = (As1) q40.g0(As1.class);
            if (as1 != null) {
                return as1.n(AlarmExtensionKt.toSDKV2Setting(list)).x(new BleAdapterImpl$setAlarms$$inlined$let$lambda$Anon1(this, list, session, iSessionSdkCallback)).v(new BleAdapterImpl$setAlarms$$inlined$let$lambda$Anon2(this, list, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingAlarmFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> setBackgroundImage(FLogger.Session session, BackgroundConfig backgroundConfig, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(backgroundConfig, "backgroundConfig");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Background Image: " + backgroundConfig);
        String directory = FileUtils.getDirectory(getContext(), FileType.WATCH_FACE);
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Js1 js1 = (Js1) q40.g0(Js1.class);
            if (js1 != null) {
                Wg6.b(directory, "directory");
                return js1.h(backgroundConfig.toSDKBackgroundImageConfig(directory)).x(new BleAdapterImpl$setBackgroundImage$$inlined$let$lambda$Anon1(this, backgroundConfig, directory, session, iSessionSdkCallback)).v(new BleAdapterImpl$setBackgroundImage$$inlined$let$lambda$Anon2(this, backgroundConfig, directory, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingPresetFeature - set BackgroundImageFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final boolean setDevice(Q40 q40) {
        Wg6.c(q40, "deviceObj");
        setDeviceObj(q40);
        DevicePreferenceUtils.setUiMinorVersion(getContext(), q40.M().getSerialNumber(), q40.M().getUiPackageOSVersion().getMinor());
        DevicePreferenceUtils.setUiMajorVersion(getContext(), q40.M().getSerialNumber(), q40.M().getUiPackageOSVersion().getMajor());
        return true;
    }

    @DexIgnore
    public final Zb0<Zm1[]> setDeviceConfig(FLogger.Session session, R60[] r60Arr, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(r60Arr, "deviceConfigItems");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Device Config: " + new Gson().t(r60Arr));
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Es1 es1 = (Es1) q40.g0(Es1.class);
            if (es1 != null) {
                return es1.J(r60Arr).x(new BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon1(this, r60Arr, session, iSessionSdkCallback)).v(new BleAdapterImpl$setDeviceConfig$$inlined$let$lambda$Anon2(this, r60Arr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingConfigurationsFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void setDeviceObj(Q40 q40) {
        this.deviceObj = q40;
        if (q40 != null) {
            if (!TextUtils.isEmpty(q40.M().getMacAddress())) {
                setMacAddress(q40.M().getMacAddress());
            }
            q40.B(this.mDeviceStateListener);
        }
    }

    @DexIgnore
    public final Zb0<Cd6> setFrontLightEnable(FLogger.Session session, boolean z, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Front Light Enable: " + z);
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Gs1 gs1 = (Gs1) q40.g0(Gs1.class);
            if (gs1 != null) {
                return gs1.f(z).x(new BleAdapterImpl$setFrontLightEnable$$inlined$let$lambda$Anon1(this, z, session, iSessionSdkCallback)).v(new BleAdapterImpl$setFrontLightEnable$$inlined$let$lambda$Anon2(this, z, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingFrontLightFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Yb0<Ry1> setLabelFile(FLogger.Session session, Ou1 ou1, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(ou1, "labelFile");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "setLabelFile File: " + ou1);
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Fs1 fs1 = (Fs1) q40.g0(Fs1.class);
            if (fs1 != null) {
                return fs1.q(ou1).x(new BleAdapterImpl$setLabelFile$$inlined$let$lambda$Anon1(this, ou1, session, iSessionSdkCallback)).v(new BleAdapterImpl$setLabelFile$$inlined$let$lambda$Anon2(this, ou1, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support setLabelFiles");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Yb0<String> setLocalizationData(LocalizationData localizationData, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(localizationData, "localizationData");
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Localization");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Hs1 hs1 = (Hs1) q40.g0(Hs1.class);
            if (hs1 != null) {
                return hs1.e(localizationData.toLocalizationFile()).x(new BleAdapterImpl$setLocalizationData$$inlined$let$lambda$Anon1(this, localizationData, session, iSessionSdkCallback)).v(new BleAdapterImpl$setLocalizationData$$inlined$let$lambda$Anon2(this, localizationData, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingLocalizationFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> setMinimumStepThreshold(long j, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "setMinimumStepThreshold");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Wq1 wq1 = (Wq1) q40.g0(Wq1.class);
            if (wq1 != null) {
                return wq1.K(j).x(new BleAdapterImpl$setMinimumStepThreshold$$inlined$let$lambda$Anon1(this, j, session, iSessionSdkCallback)).v(new BleAdapterImpl$setMinimumStepThreshold$$inlined$let$lambda$Anon2(this, j, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support BuddyChallengeFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Yb0<Cd6> setNotificationFilters(FLogger.Session session, List<AppNotificationFilter> list, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(list, "notificationFilters");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        ArrayList arrayList = new ArrayList(Im7.m(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().toSDKNotificationFilter(getContext()));
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Set Notification Filter: " + list + " bleNotifications " + arrayList);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Set Notification Filter");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Is1 is1 = (Is1) q40.g0(Is1.class);
            if (is1 != null) {
                Object[] array = arrayList.toArray(new NotificationFilter[0]);
                if (array != null) {
                    Yb0<Cd6> D = is1.D((NotificationFilter[]) array);
                    D.w(new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon1(this, arrayList, session, iSessionSdkCallback));
                    return D.x(new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon2(this, arrayList, session, iSessionSdkCallback)).v(new BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon3(this, arrayList, session, iSessionSdkCallback));
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            log(session, "Device[" + getSerial() + "] does not support SettingLocalizationFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0086, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0087, code lost:
        com.fossil.So7.a(r9, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x008a, code lost:
        throw r1;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.mapped.Yb0<com.mapped.Cd6> setReplyMessageMapping(com.misfit.frameworks.buttonservice.log.FLogger.Session r12, com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup r13, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback r14) {
        /*
        // Method dump skipped, instructions count: 273
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl.setReplyMessageMapping(com.misfit.frameworks.buttonservice.log.FLogger$Session, com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback):com.mapped.Yb0");
    }

    @DexIgnore
    public final void setSecretKey(FLogger.Session session, byte[] bArr) {
        Wg6.c(session, "logSession");
        Wg6.c(bArr, "secretKey");
        log(session, "Set Secret Key");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Ls1 ls1 = (Ls1) q40.g0(Ls1.class);
            if (ls1 != null) {
                ls1.Z(bArr);
                setTSecretKey(bArr);
                return;
            }
            log(session, "Device[" + getSerial() + "] does not support GettingSecretKeyFeature");
            return;
        }
        FLogger.INSTANCE.getLocal().e(TAG, "Set Secret Key. FAILED: deviceObject is NULL");
    }

    @DexIgnore
    public void setTSecretKey(byte[] bArr) {
        this.tSecretKey = bArr;
    }

    @DexIgnore
    public final Yb0<Ru1[]> setWatchAppFiles(FLogger.Session session, Ru1[] ru1Arr, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(ru1Arr, "watchAppFiles");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Watch App Files: " + ru1Arr);
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Ms1 ms1 = (Ms1) q40.g0(Ms1.class);
            if (ms1 != null) {
                Yb0<Ru1[]> v = ms1.v(ru1Arr).x(new BleAdapterImpl$setWatchAppFiles$$inlined$let$lambda$Anon1(this, ru1Arr, session, iSessionSdkCallback)).v(new BleAdapterImpl$setWatchAppFiles$$inlined$let$lambda$Anon2(this, ru1Arr, session, iSessionSdkCallback));
                v.w(new BleAdapterImpl$setWatchAppFiles$$inlined$let$lambda$Anon3(this, ru1Arr, session, iSessionSdkCallback));
                return v;
            }
            log(session, "Device[" + getSerial() + "] does not support SettingUpWatchAppFeature - setWatchAppFiles");
            iSessionSdkCallback.onSetWatchAppFileFailed();
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> setWatchApps(FLogger.Session session, WatchAppMappingSettings watchAppMappingSettings, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(watchAppMappingSettings, "watchAppMappingSettings");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set Watch Apps: " + watchAppMappingSettings);
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Js1 js1 = (Js1) q40.g0(Js1.class);
            if (js1 != null) {
                return js1.h(watchAppMappingSettings.toSDKSetting()).x(new BleAdapterImpl$setWatchApps$$inlined$let$lambda$Anon1(this, watchAppMappingSettings, session, iSessionSdkCallback)).v(new BleAdapterImpl$setWatchApps$$inlined$let$lambda$Anon2(this, watchAppMappingSettings, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingPresetFeature - setWatchApps");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Zb0<Cd6> setWatchParams(FLogger.Session session, WatchParamsFileMapping watchParamsFileMapping, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(watchParamsFileMapping, "watchParamFileMapping");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Set WatchParams: " + watchParamsFileMapping);
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Os1 os1 = (Os1) q40.g0(Os1.class);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "Setting watchParam with file value = " + watchParamsFileMapping.toSDKSetting());
            if (os1 != null) {
                return os1.a0(watchParamsFileMapping.toSDKSetting()).x(new BleAdapterImpl$setWatchParams$$inlined$let$lambda$Anon1(this, watchParamsFileMapping, session, iSessionSdkCallback)).v(new BleAdapterImpl$setWatchParams$$inlined$let$lambda$Anon2(this, watchParamsFileMapping, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support SettingWatchParameterFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void setWorkoutGPSData(Location location, FLogger.Session session) {
        Wg6.c(location, "data");
        Wg6.c(session, "logSession");
        GpsDataPoint gpsDataPoint = LocationExtensionKt.toGpsDataPoint(location);
        GpsDataProvider.defaultProvider().onLocationChanged(gpsDataPoint);
        log(session, "setWorkoutGPSData dataPoint " + gpsDataPoint);
    }

    @DexIgnore
    public final Zb0<byte[]> startAuthenticate(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(bArr, "phoneRandomNumber");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Start Authenticate phoneRandomNumber " + ConversionUtils.INSTANCE.byteArrayToString(bArr));
        FLogger.INSTANCE.getRemote().i(FLogger.Component.BLE, session, getSerial(), "BleAdapter", "Start Authenticate phoneRandomNumber");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Ps1 ps1 = (Ps1) q40.g0(Ps1.class);
            if (ps1 != null) {
                return ps1.b(bArr).s(new BleAdapterImpl$startAuthenticate$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).r(new BleAdapterImpl$startAuthenticate$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support StartingAuthenticateFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void startScanning(FLogger.Session session, long j, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Start scanning...");
        this.isScanning = true;
        ScanService scanService2 = new ScanService(getContext(), new BleScanServiceCallback(iSessionSdkCallback), j);
        this.scanService = scanService2;
        if (scanService2 != null) {
            scanService2.setActiveDeviceLog(getSerial());
            ScanService scanService3 = this.scanService;
            if (scanService3 != null) {
                scanService3.startScanWithAutoStopTimer();
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final Zb0<Cd6> stopCurrentWorkoutSession(FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        Wg6.c(session, "logSession");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Stop Current Workout Session");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Qs1 qs1 = (Qs1) q40.g0(Qs1.class);
            if (qs1 != null) {
                return qs1.t().s(new BleAdapterImpl$stopCurrentWorkoutSession$$inlined$let$lambda$Anon1(this, session, iSessionSdkCallback)).r(new BleAdapterImpl$stopCurrentWorkoutSession$$inlined$let$lambda$Anon2(this, session, iSessionSdkCallback));
            }
            log(session, "Device[" + getSerial() + "] does not support StoppingWorkoutSessionFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void stopScanning(FLogger.Session session) {
        Wg6.c(session, "logSession");
        ScanService scanService2 = this.scanService;
        if (scanService2 != null) {
            scanService2.stopScan();
        }
        if (this.isScanning) {
            log(session, "Stop scanning for " + getSerial());
        }
        this.isScanning = false;
    }

    @DexIgnore
    public final Zb0<Boolean> verifySecretKey(FLogger.Session session, byte[] bArr, ISessionSdkCallback iSessionSdkCallback) {
        Zb0<Boolean> r;
        Wg6.c(session, "logSession");
        Wg6.c(bArr, "secretKey");
        Wg6.c(iSessionSdkCallback, Constants.CALLBACK);
        log(session, "Verify Secret Key");
        Q40 q40 = this.deviceObj;
        if (q40 != null) {
            Ts1 ts1 = (Ts1) q40.g0(Ts1.class);
            if (ts1 != null && (r = ts1.w(bArr).s(new BleAdapterImpl$verifySecretKey$$inlined$let$lambda$Anon1(this, bArr, session, iSessionSdkCallback)).r(new BleAdapterImpl$verifySecretKey$$inlined$let$lambda$Anon2(this, bArr, session, iSessionSdkCallback))) != null) {
                return r;
            }
            log(session, "Device[" + getSerial() + "] does not support VerifyingSecretKeyFeature");
            return null;
        }
        Wg6.i();
        throw null;
    }
}
