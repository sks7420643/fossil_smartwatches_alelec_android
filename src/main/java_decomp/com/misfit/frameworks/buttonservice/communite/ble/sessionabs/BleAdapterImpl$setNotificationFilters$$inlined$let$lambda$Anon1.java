package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.Qq7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon1 extends Qq7 implements Hg6<Float, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $sdkNotificationFilterList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setNotificationFilters$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, List list, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$sdkNotificationFilterList$inlined = list;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Float f) {
        invoke(f.floatValue());
        return Cd6.a;
    }

    @DexIgnore
    public final void invoke(float f) {
        BleAdapterImpl bleAdapterImpl = this.this$0;
        FLogger.Session session = this.$logSession$inlined;
        bleAdapterImpl.log(session, "Set Notification Filter Progress: " + f);
        this.$callback$inlined.onSetNotificationFilterProgressChanged(f);
    }
}
