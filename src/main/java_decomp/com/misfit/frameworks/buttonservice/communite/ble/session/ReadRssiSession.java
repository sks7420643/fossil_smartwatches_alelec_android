package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.facebook.internal.NativeProtocol;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Us1;
import com.fossil.Yx1;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ReadRssiSession extends BleSessionAbs {
    @DexIgnore
    public int mRemoteRssi; // = 100;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadRssiStep extends BleStateAbs {
        @DexIgnore
        public Zb0<Integer> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ReadRssiStep() {
            super(ReadRssiSession.this.getTAG());
            setTimeout(VideoUploader.RETRY_DELAY_UNIT_MS);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Zb0<Integer> readRssi = ReadRssiSession.this.getBleAdapter().readRssi(ReadRssiSession.this.getLogSession(), this);
            this.task = readRssi;
            if (readRssi == null) {
                ReadRssiSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadRssiFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            ReadRssiSession.this.stop(FailureCode.FAILED_TO_READ_RSSI);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadRssiSuccess(int i) {
            stopTimeout();
            ReadRssiSession.this.mRemoteRssi = i;
            ReadRssiSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Integer> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReadRssiSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.URGENT, CommunicateMode.READ_RSSI, bleAdapterImpl, bleSessionCallback);
        Wg6.c(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putInt("rssi", this.mRemoteRssi);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        ReadRssiSession readRssiSession = new ReadRssiSession(getBleAdapter(), getBleSessionCallback());
        readRssiSession.setDevice(getDevice());
        return readRssiSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs
    public void initStateMap() {
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.READ_RSSI_STATE;
        String name = ReadRssiStep.class.getName();
        Wg6.b(name, "ReadRssiStep::class.java.name");
        sessionStateMap.put(sessionState, name);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean onStart(Object... objArr) {
        Wg6.c(objArr, NativeProtocol.WEB_DIALOG_PARAMS);
        super.onStart(Arrays.copyOf(objArr, objArr.length));
        if (!BluetoothUtils.isBluetoothEnable()) {
            enterTaskWithDelayTime(new ReadRssiSession$onStart$Anon1(this), 500);
            return true;
        } else if (getBleAdapter().getDeviceObj() != null) {
            enterStateAsync(createConcreteState(BleSessionAbs.SessionState.READ_RSSI_STATE));
            return true;
        } else {
            enterTaskWithDelayTime(new ReadRssiSession$onStart$Anon3$Anon1_Level2(this), 500);
            return true;
        }
    }
}
