package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Ln1;
import com.fossil.Us1;
import com.fossil.Yx1;
import com.fossil.Zm1;
import com.mapped.R60;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionItemExtentionKt;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetWorkoutDetectionConfigSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ WorkoutDetectionSetting workoutDetectionSetting;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWorkoutDetectionState extends BleStateAbs {
        @DexIgnore
        public Zb0<Zm1[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWorkoutDetectionState() {
            super(SetWorkoutDetectionConfigSession.this.getTAG());
        }

        @DexIgnore
        private final R60[] prepareData() {
            Ln1 ln1 = new Ln1();
            ln1.c(WorkoutDetectionItemExtentionKt.toAutoWorkoutDetectionItemConfig(SetWorkoutDetectionConfigSession.this.workoutDetectionSetting.getWorkoutDetectionList()));
            return ln1.b();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Zb0<Zm1[]> deviceConfig = SetWorkoutDetectionConfigSession.this.getBleAdapter().setDeviceConfig(SetWorkoutDetectionConfigSession.this.getLogSession(), prepareData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetWorkoutDetectionConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            SetWorkoutDetectionConfigSession.this.stop(FailureCode.FAILED_TO_SET_WORKOUT_DETECTION_CONFIG);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetWorkoutDetectionConfigSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Zm1[]> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetWorkoutDetectionConfigSession(WorkoutDetectionSetting workoutDetectionSetting2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_WORKOUT_DETECTION, bleAdapterImpl, bleSessionCallback);
        Wg6.c(workoutDetectionSetting2, "workoutDetectionSetting");
        Wg6.c(bleAdapterImpl, "bleAdapter");
        this.workoutDetectionSetting = workoutDetectionSetting2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetWorkoutDetectionConfigSession setWorkoutDetectionConfigSession = new SetWorkoutDetectionConfigSession(this.workoutDetectionSetting, getBleAdapter(), getBleSessionCallback());
        setWorkoutDetectionConfigSession.setDevice(getDevice());
        return setWorkoutDetectionConfigSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_WORKOUT_DETECTION_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_WORKOUT_DETECTION_STATE;
        String name = SetWorkoutDetectionState.class.getName();
        Wg6.b(name, "SetWorkoutDetectionState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
