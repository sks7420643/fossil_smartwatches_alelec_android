package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import android.os.Bundle;
import com.fossil.Hm7;
import com.fossil.Hr7;
import com.fossil.Mm7;
import com.fossil.Qm1;
import com.fossil.Rm1;
import com.fossil.Sm1;
import com.fossil.Tm1;
import com.fossil.Um1;
import com.fossil.Us1;
import com.fossil.Vm1;
import com.fossil.Wm1;
import com.fossil.Xm1;
import com.fossil.Yx1;
import com.fossil.Zm1;
import com.fossil.fitness.FitnessData;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.Yb0;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.db.DataCollectorHelper;
import com.misfit.frameworks.buttonservice.db.DataFile;
import com.misfit.frameworks.buttonservice.db.DataFileProvider;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFSyncLog;
import com.misfit.frameworks.buttonservice.log.model.SessionDetailInfo;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.service.microapp.CommuteTimeService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseTransferDataSubFlow extends SubFlow {
    @DexIgnore
    public int activeMinute;
    @DexIgnore
    public int activeMinuteGoal;
    @DexIgnore
    public int awakeMin;
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public long calorie;
    @DexIgnore
    public long calorieGoal;
    @DexIgnore
    public long dailyStepInfo;
    @DexIgnore
    public int deepMin;
    @DexIgnore
    public long distanceInCentimeter;
    @DexIgnore
    public /* final */ boolean isClearDataOptional;
    @DexIgnore
    public boolean isNewDevice;
    @DexIgnore
    public int lightMin;
    @DexIgnore
    public List<FitnessData> mSyncData;
    @DexIgnore
    public /* final */ MFLog mflog;
    @DexIgnore
    public long realTimeSteps;
    @DexIgnore
    public long realTimeStepsInfo;
    @DexIgnore
    public long stepGoal;
    @DexIgnore
    public MFSyncLog syncLog;
    @DexIgnore
    public long syncTime;
    @DexIgnore
    public int totalSleepInMin;
    @DexIgnore
    public /* final */ UserProfile userProfile;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class EraseDataFileState extends BleStateAbs {
        @DexIgnore
        public Zb0<Cd6> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public EraseDataFileState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Zb0<Cd6> eraseDataFile = BaseTransferDataSubFlow.this.getBleAdapter().eraseDataFile(BaseTransferDataSubFlow.this.getLogSession(), this);
            this.task = eraseDataFile;
            if (eraseDataFile == null) {
                BaseTransferDataSubFlow.this.stopSubFlow(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onEraseDataFilesFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            if (BaseTransferDataSubFlow.this.isClearDataOptional()) {
                BaseTransferDataSubFlow.this.addFailureCode(FailureCode.FAILED_TO_CLEAR_DATA);
                BaseTransferDataSubFlow.this.stopSubFlow(0);
                return;
            }
            BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_CLEAR_DATA);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onEraseDataFilesSuccess() {
            stopTimeout();
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Erase DataFiles timeout. Cancel.");
            Zb0<Cd6> zb0 = this.task;
            if (zb0 != null) {
                if (zb0 != null) {
                    Us1.a(zb0);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else if (BaseTransferDataSubFlow.this.isClearDataOptional()) {
                BaseTransferDataSubFlow.this.addFailureCode(FailureCode.FAILED_TO_CLEAR_DATA);
                BaseTransferDataSubFlow.this.stopSubFlow(0);
            } else {
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_CLEAR_DATA);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public Zb0<HashMap<Zm1, R60>> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public GetDeviceConfigState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        private final void logConfiguration(HashMap<Zm1, R60> hashMap) {
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            Hr7 hr7 = Hr7.a;
            String format = String.format("Get configuration  " + BaseTransferDataSubFlow.this.getSerial() + "\n\t[timeConfiguration: \n\ttime = %s,\n\tbattery = %s,\n\tbiometric = %s,\n\tdaily steps = %s,\n\tdaily step goal = %s, \n\tdaily calorie: %s, \n\tdaily calorie goal: %s, \n\tdaily total active minute: %s, \n\tdaily active minute goal: %s, \n\tdaily distance: %s, \n\tdaily sleep: %s, \n\tinactive nudge: %s, \n\tvibration strength: %s, \n\tdo not disturb schedule: %s, \n\t]", Arrays.copyOf(new Object[]{hashMap.get(Zm1.TIME), hashMap.get(Zm1.BATTERY), hashMap.get(Zm1.BIOMETRIC_PROFILE), hashMap.get(Zm1.DAILY_STEP), hashMap.get(Zm1.DAILY_STEP_GOAL), hashMap.get(Zm1.DAILY_CALORIE), hashMap.get(Zm1.DAILY_CALORIE_GOAL), hashMap.get(Zm1.DAILY_TOTAL_ACTIVE_MINUTE), hashMap.get(Zm1.DAILY_ACTIVE_MINUTE_GOAL), hashMap.get(Zm1.DAILY_DISTANCE), hashMap.get(Zm1.DAILY_SLEEP), hashMap.get(Zm1.DAILY_DISTANCE), hashMap.get(Zm1.DAILY_DISTANCE), hashMap.get(Zm1.INACTIVE_NUDGE), hashMap.get(Zm1.VIBE_STRENGTH), hashMap.get(Zm1.DO_NOT_DISTURB_SCHEDULE)}, 16));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            baseTransferDataSubFlow.log(format);
        }

        @DexIgnore
        private final void readConfig(HashMap<Zm1, R60> hashMap) {
            if (!BaseTransferDataSubFlow.this.getUserProfile().isNewDevice()) {
                R60 r60 = hashMap.get(Zm1.DAILY_STEP_GOAL);
                if (r60 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                    if (r60 != null) {
                        baseTransferDataSubFlow.stepGoal = ((Wm1) r60).getStep();
                    } else {
                        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepGoalConfig");
                    }
                }
                R60 r602 = hashMap.get(Zm1.DAILY_STEP);
                if (r602 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                    if (r602 != null) {
                        baseTransferDataSubFlow2.realTimeSteps = ((Vm1) r602).getStep();
                        BaseTransferDataSubFlow baseTransferDataSubFlow3 = BaseTransferDataSubFlow.this;
                        baseTransferDataSubFlow3.realTimeStepsInfo = baseTransferDataSubFlow3.realTimeSteps;
                    } else {
                        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepConfig");
                    }
                }
                R60 r603 = hashMap.get(Zm1.DAILY_ACTIVE_MINUTE_GOAL);
                if (r603 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow4 = BaseTransferDataSubFlow.this;
                    if (r603 != null) {
                        baseTransferDataSubFlow4.activeMinuteGoal = ((Qm1) r603).getMinute();
                    } else {
                        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyActiveMinuteGoalConfig");
                    }
                }
                R60 r604 = hashMap.get(Zm1.DAILY_TOTAL_ACTIVE_MINUTE);
                if (r604 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow5 = BaseTransferDataSubFlow.this;
                    if (r604 != null) {
                        baseTransferDataSubFlow5.activeMinute = ((Xm1) r604).getMinute();
                    } else {
                        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyTotalActiveMinuteConfig");
                    }
                }
                R60 r605 = hashMap.get(Zm1.DAILY_CALORIE_GOAL);
                if (r605 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow6 = BaseTransferDataSubFlow.this;
                    if (r605 != null) {
                        baseTransferDataSubFlow6.calorieGoal = ((Sm1) r605).getCalorie();
                    } else {
                        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyCalorieGoalConfig");
                    }
                }
                R60 r606 = hashMap.get(Zm1.DAILY_CALORIE);
                if (r606 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow7 = BaseTransferDataSubFlow.this;
                    if (r606 != null) {
                        baseTransferDataSubFlow7.calorie = ((Rm1) r606).getCalorie();
                    } else {
                        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyCalorieConfig");
                    }
                }
                R60 r607 = hashMap.get(Zm1.DAILY_DISTANCE);
                if (r607 != null) {
                    BaseTransferDataSubFlow baseTransferDataSubFlow8 = BaseTransferDataSubFlow.this;
                    if (r607 != null) {
                        baseTransferDataSubFlow8.distanceInCentimeter = ((Tm1) r607).getCentimeter();
                    } else {
                        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyDistanceConfig");
                    }
                }
                R60 r608 = hashMap.get(Zm1.DAILY_SLEEP);
                if (r608 != null) {
                    if (!(r608 instanceof Um1)) {
                        r608 = null;
                    }
                    Um1 um1 = (Um1) r608;
                    if (um1 != null) {
                        BaseTransferDataSubFlow.this.totalSleepInMin = um1.getTotalSleepInMinute();
                        BaseTransferDataSubFlow.this.awakeMin = um1.getAwakeInMinute();
                        BaseTransferDataSubFlow.this.lightMin = um1.getLightSleepInMinute();
                        BaseTransferDataSubFlow.this.deepMin = um1.getDeepSleepInMinute();
                        return;
                    }
                    return;
                }
                return;
            }
            BaseTransferDataSubFlow.this.log("We skip read real time steps cause by full sync");
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            Zb0<HashMap<Zm1, R60>> deviceConfig = BaseTransferDataSubFlow.this.getBleAdapter().getDeviceConfig(BaseTransferDataSubFlow.this.getLogSession(), this);
            this.task = deviceConfig;
            if (deviceConfig != null) {
                startTimeout();
                return true;
            } else if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return true;
            } else {
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
                return true;
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            BaseTransferDataSubFlow.this.addFailureCode(FailureCode.FAILED_TO_GET_CONFIG);
            if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return;
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigSuccess(HashMap<Zm1, R60> hashMap) {
            Wg6.c(hashMap, "deviceConfiguration");
            stopTimeout();
            logConfiguration(hashMap);
            readConfig(hashMap);
            if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
                return;
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Get device configuration timeout. Cancel.");
            Zb0<HashMap<Zm1, R60>> zb0 = this.task;
            if (zb0 != null) {
                if (zb0 != null) {
                    Us1.a(zb0);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else if (BaseTransferDataSubFlow.this.isNewDevice) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.ERASE_DATA_FILE_STATE));
            } else {
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.READ_DATA_FILE_STATE));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class PlayDeviceAnimationState extends BleStateAbs {
        @DexIgnore
        public Zb0<Cd6> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public PlayDeviceAnimationState() {
            super(BaseTransferDataSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Zb0<Cd6> playDeviceAnimation = BaseTransferDataSubFlow.this.getBleAdapter().playDeviceAnimation(BaseTransferDataSubFlow.this.getLogSession(), this);
            this.task = playDeviceAnimation;
            if (playDeviceAnimation == null) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.GET_DEVICE_CONFIG_STATE));
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onPlayDeviceAnimation(boolean z, Yx1 yx1) {
            stopTimeout();
            if (!z) {
                BaseTransferDataSubFlow.this.addFailureCode(201);
            }
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.GET_DEVICE_CONFIG_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Play device animation timeout. Cancel.");
            Zb0<Cd6> zb0 = this.task;
            if (zb0 == null) {
                BaseTransferDataSubFlow.this.addFailureCode(201);
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.enterSubStateAsync(baseTransferDataSubFlow.createConcreteState(SubFlow.SessionState.GET_DEVICE_CONFIG_STATE));
            } else if (zb0 != null) {
                Us1.a(zb0);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ProcessAndStoreDataState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ProcessAndStoreDataState() {
            super(BaseTransferDataSubFlow.this.getTAG());
            setTimeout(CommuteTimeService.A);
            BaseTransferDataSubFlow.this.log("Process and store data of device with serial " + BaseTransferDataSubFlow.this.getSerial());
        }

        @DexIgnore
        private final Bundle bundleData() {
            Bundle bundle = new Bundle();
            bundle.putLong(Constants.SYNC_RESULT, BaseTransferDataSubFlow.this.getSyncTime());
            bundle.putLong(ButtonService.Companion.getREALTIME_STEPS(), BaseTransferDataSubFlow.this.realTimeSteps);
            return bundle;
        }

        @DexIgnore
        private final List<FitnessData> readDataFromCache() {
            ArrayList arrayList = new ArrayList();
            List<DataFile> allDataFiles = DataFileProvider.getInstance(BaseTransferDataSubFlow.this.getBleAdapter().getContext()).getAllDataFiles(BaseTransferDataSubFlow.this.getSyncTime(), BaseTransferDataSubFlow.this.getSerial());
            Gson gson = new Gson();
            if (allDataFiles.size() > 0) {
                Wg6.b(allDataFiles, "cacheData");
                long j = -1;
                int i = 0;
                for (T t : allDataFiles) {
                    try {
                        Wg6.b(t, "it");
                        FitnessData fitnessData = (FitnessData) gson.k(t.getDataFile(), FitnessData.class);
                        Wg6.b(fitnessData, "fitnessData");
                        arrayList.add(fitnessData);
                        if (t.getSyncTime() != j) {
                            if (j != -1) {
                                BaseTransferDataSubFlow.this.log("Got cache data of " + j + ", fitness data size=" + i);
                            }
                            j = t.getSyncTime();
                            i = 0;
                        }
                        i++;
                    } catch (Exception e) {
                        BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Got cache data of ");
                        Wg6.b(t, "it");
                        sb.append(t.getSyncTime());
                        sb.append(" exception=");
                        sb.append(e);
                        baseTransferDataSubFlow.log(sb.toString());
                    }
                }
                if (j != -1 && i > 0) {
                    BaseTransferDataSubFlow.this.log("Got cache data of " + j + ", fitness data size=" + i);
                }
            } else {
                BaseTransferDataSubFlow.this.log("No cache data.");
            }
            return arrayList;
        }

        /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: long : 0x0043: INVOKE  (r2v6 long) = 
          (wrap: com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow : 0x0041: IGET  (r2v5 com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow) = 
          (r9v0 'this' com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow$ProcessAndStoreDataState A[IMMUTABLE_TYPE, THIS])
         com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow.ProcessAndStoreDataState.this$0 com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow)
         type: VIRTUAL call: com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow.getSyncTime():long), ('_' char), (r0v2 int)] */
        @DexIgnore
        private final void saveAsCache(List<FitnessData> list) {
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("Save latest sync data as cache, sync time=" + BaseTransferDataSubFlow.this.getSyncTime() + ", fitness data size=" + list.size());
            int i = 0;
            for (T t : list) {
                if (i >= 0) {
                    StringBuilder sb = new StringBuilder();
                    sb.append(BaseTransferDataSubFlow.this.getSyncTime());
                    sb.append('_');
                    sb.append(i);
                    DataCollectorHelper.saveDataFile(BaseTransferDataSubFlow.this.getBleAdapter().getContext(), new DataFile(sb.toString(), new Gson().t(t), BaseTransferDataSubFlow.this.getSerial(), BaseTransferDataSubFlow.this.getSyncTime()));
                    i++;
                } else {
                    Hm7.l();
                    throw null;
                }
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            FLogger.INSTANCE.getLocal().d(getTAG(), "onEnter ProcessAndStoreDataState");
            List<FitnessData> readDataFromCache = readDataFromCache();
            saveAsCache(BaseTransferDataSubFlow.this.mSyncData);
            readDataFromCache.addAll(BaseTransferDataSubFlow.this.mSyncData);
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("set syncdata from cache data with size " + readDataFromCache.size());
            BaseTransferDataSubFlow.this.mSyncData = readDataFromCache;
            startTimeout();
            BleSession.BleSessionCallback bleSessionCallback = BaseTransferDataSubFlow.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                bleSessionCallback.onReceivedSyncData(bundleData());
                return true;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Process and Store data timeout.");
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        public final void updateCurrentStepsAndStepGoal(boolean z, UserProfile userProfile) {
            Wg6.c(userProfile, "newUserProfile");
            if (z) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.log("Set percentage goal progress session " + BaseTransferDataSubFlow.this.getSerial() + ", keep setting in device");
            } else {
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.log("Set percentage goal progress session " + BaseTransferDataSubFlow.this.getSerial() + ", {currentSteps=" + userProfile.getCurrentSteps() + ", stepGoal=" + userProfile.getGoalSteps() + ",activeMinute=" + userProfile.getActiveMinute() + ", activeMinuteGoal=" + userProfile.getActiveMinuteGoal() + ",calories=" + userProfile.getCalories() + ", caloriesGoal=" + userProfile.getCaloriesGoal() + ",distanceInCentimeter=" + userProfile.getDistanceInCentimeter() + "totalSleepMin=" + userProfile.getTotalSleepInMinute() + "awakeMin=" + userProfile.getAwakeInMinute() + "lightMin=" + userProfile.getLightSleepInMinute() + "deepMin=" + userProfile.getDeepSleepInMinute() + "}");
            }
            stopTimeout();
            if (!z) {
                BaseTransferDataSubFlow.this.getUserProfile().setCurrentSteps(userProfile.getCurrentSteps());
                BaseTransferDataSubFlow.this.getUserProfile().setGoalSteps(userProfile.getGoalSteps());
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinute(userProfile.getActiveMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinuteGoal(userProfile.getActiveMinuteGoal());
                BaseTransferDataSubFlow.this.getUserProfile().setCalories(userProfile.getCalories());
                BaseTransferDataSubFlow.this.getUserProfile().setCaloriesGoal(userProfile.getCaloriesGoal());
                BaseTransferDataSubFlow.this.getUserProfile().setDistanceInCentimeter(userProfile.getDistanceInCentimeter());
                BaseTransferDataSubFlow.this.getUserProfile().setTotalSleepInMinute(userProfile.getTotalSleepInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setAwakeInMinute(userProfile.getAwakeInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setLightSleepInMinute(userProfile.getLightSleepInMinute());
                BaseTransferDataSubFlow.this.getUserProfile().setDeepSleepInMinute(userProfile.getDeepSleepInMinute());
            } else {
                BaseTransferDataSubFlow.this.getUserProfile().setCurrentSteps(BaseTransferDataSubFlow.this.realTimeSteps);
                BaseTransferDataSubFlow.this.getUserProfile().setGoalSteps(BaseTransferDataSubFlow.this.stepGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinute(BaseTransferDataSubFlow.this.activeMinute);
                BaseTransferDataSubFlow.this.getUserProfile().setActiveMinuteGoal(BaseTransferDataSubFlow.this.activeMinuteGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setCalories(BaseTransferDataSubFlow.this.calorie);
                BaseTransferDataSubFlow.this.getUserProfile().setCaloriesGoal(BaseTransferDataSubFlow.this.calorieGoal);
                BaseTransferDataSubFlow.this.getUserProfile().setDistanceInCentimeter(BaseTransferDataSubFlow.this.distanceInCentimeter);
                BaseTransferDataSubFlow.this.getUserProfile().setTotalSleepInMinute(BaseTransferDataSubFlow.this.totalSleepInMin);
                BaseTransferDataSubFlow.this.getUserProfile().setAwakeInMinute(BaseTransferDataSubFlow.this.awakeMin);
                BaseTransferDataSubFlow.this.getUserProfile().setLightSleepInMinute(BaseTransferDataSubFlow.this.lightMin);
                BaseTransferDataSubFlow.this.getUserProfile().setDeepSleepInMinute(BaseTransferDataSubFlow.this.deepMin);
            }
            FLogger.INSTANCE.updateSessionDetailInfo(new SessionDetailInfo(BaseTransferDataSubFlow.this.getBleAdapter().getBatteryLevel(), (int) BaseTransferDataSubFlow.this.realTimeStepsInfo, (int) BaseTransferDataSubFlow.this.dailyStepInfo));
            MFSyncLog syncLog = BaseTransferDataSubFlow.this.getSyncLog();
            if (syncLog != null) {
                syncLog.setRealTimeStep(BaseTransferDataSubFlow.this.getUserProfile().getCurrentSteps());
            }
            BaseTransferDataSubFlow.this.stopSubFlow(0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadDataFileState extends BleStateAbs {
        @DexIgnore
        public Yb0<FitnessData[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ReadDataFileState() {
            super(BaseTransferDataSubFlow.this.getTAG());
            setMaxRetries(3);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void cancelCurrentBleTask() {
            super.cancelCurrentBleTask();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "cancelCurrentBleTask is " + this.task);
            Yb0<FitnessData[]> yb0 = this.task;
            if (yb0 != null) {
                Us1.a(yb0);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            try {
                Yb0<FitnessData[]> readDataFile = BaseTransferDataSubFlow.this.getBleAdapter().readDataFile(BaseTransferDataSubFlow.this.getLogSession(), BaseTransferDataSubFlow.this.getUserProfile().getUserBiometricData().toSDKBiometricProfile(), this);
                this.task = readDataFile;
                if (readDataFile == null) {
                    BaseTransferDataSubFlow.this.stopSubFlow(10000);
                    return true;
                }
                startTimeout();
                return true;
            } catch (Exception e) {
                BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow.log("Read Data Files:  ex=" + e.getMessage());
                BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
                baseTransferDataSubFlow2.errorLog("ReadDataFileState: catchException=" + e.getMessage(), FLogger.Component.BLE, ErrorCodeBuilder.Step.UNKNOWN, ErrorCodeBuilder.AppError.UNKNOWN);
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SYNC);
                return true;
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadDataFilesFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            if (!retry(BaseTransferDataSubFlow.this.getBleAdapter().getContext(), BaseTransferDataSubFlow.this.getSerial())) {
                BaseTransferDataSubFlow.this.log("Reach the limit retry. Stop.");
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SYNC);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadDataFilesProgressChanged(float f) {
            setTimeout(30000);
            startTimeout();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onReadDataFilesSuccess(FitnessData[] fitnessDataArr) {
            Wg6.c(fitnessDataArr, "data");
            stopTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, ".onReadDataFilesSuccess(), sdk = " + new Gson().t(fitnessDataArr));
            BaseTransferDataSubFlow baseTransferDataSubFlow = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow.log("data size from SDK " + fitnessDataArr.length);
            Mm7.t(BaseTransferDataSubFlow.this.mSyncData, fitnessDataArr);
            BaseTransferDataSubFlow baseTransferDataSubFlow2 = BaseTransferDataSubFlow.this;
            baseTransferDataSubFlow2.enterSubStateAsync(baseTransferDataSubFlow2.createConcreteState(SubFlow.SessionState.PROCESS_AND_STORE_DATA_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferDataSubFlow.this.log("Read DataFiles timeout. Cancel.");
            Yb0<FitnessData[]> yb0 = this.task;
            if (yb0 == null) {
                BaseTransferDataSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SYNC);
            } else if (yb0 != null) {
                Us1.a(yb0);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseTransferDataSubFlow(String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl, UserProfile userProfile2, BleSession.BleSessionCallback bleSessionCallback2, boolean z) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        Wg6.c(str, "tagName");
        Wg6.c(bleSession, "bleSession");
        Wg6.c(session, "logSession");
        Wg6.c(str2, "serial");
        Wg6.c(bleAdapterImpl, "bleAdapter");
        Wg6.c(userProfile2, "userProfile");
        this.mflog = mFLog;
        this.userProfile = userProfile2;
        this.bleSessionCallback = bleSessionCallback2;
        this.isClearDataOptional = z;
        this.syncLog = (MFSyncLog) (!(mFLog instanceof MFSyncLog) ? null : mFLog);
        this.realTimeSteps = this.userProfile.getCurrentSteps();
        this.stepGoal = this.userProfile.getGoalSteps();
        this.activeMinute = this.userProfile.getActiveMinute();
        this.activeMinuteGoal = this.userProfile.getActiveMinuteGoal();
        this.calorie = this.userProfile.getCalories();
        this.calorieGoal = this.userProfile.getCaloriesGoal();
        this.distanceInCentimeter = this.userProfile.getDistanceInCentimeter();
        this.totalSleepInMin = this.userProfile.getTotalSleepInMinute();
        this.awakeMin = this.userProfile.getAwakeInMinute();
        this.lightMin = this.userProfile.getLightSleepInMinute();
        this.deepMin = this.userProfile.getDeepSleepInMinute();
        this.isNewDevice = this.userProfile.isNewDevice();
        this.realTimeStepsInfo = this.userProfile.getCurrentSteps();
        this.dailyStepInfo = this.userProfile.getCurrentSteps();
        this.mSyncData = new ArrayList();
        this.syncTime = System.currentTimeMillis() / ((long) 1000);
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final MFLog getMflog() {
        return this.mflog;
    }

    @DexIgnore
    public final List<FitnessData> getSyncData() {
        log("getSyncData dataSize " + this.mSyncData.size());
        return this.mSyncData;
    }

    @DexIgnore
    public final MFSyncLog getSyncLog() {
        return this.syncLog;
    }

    @DexIgnore
    public final long getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.PLAY_DEVICE_ANIMATION_STATE;
        String name = PlayDeviceAnimationState.class.getName();
        Wg6.b(name, "PlayDeviceAnimationState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.GET_DEVICE_CONFIG_STATE;
        String name2 = GetDeviceConfigState.class.getName();
        Wg6.b(name2, "GetDeviceConfigState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<SubFlow.SessionState, String> sessionStateMap3 = getSessionStateMap();
        SubFlow.SessionState sessionState3 = SubFlow.SessionState.ERASE_DATA_FILE_STATE;
        String name3 = EraseDataFileState.class.getName();
        Wg6.b(name3, "EraseDataFileState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<SubFlow.SessionState, String> sessionStateMap4 = getSessionStateMap();
        SubFlow.SessionState sessionState4 = SubFlow.SessionState.READ_DATA_FILE_STATE;
        String name4 = ReadDataFileState.class.getName();
        Wg6.b(name4, "ReadDataFileState::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<SubFlow.SessionState, String> sessionStateMap5 = getSessionStateMap();
        SubFlow.SessionState sessionState5 = SubFlow.SessionState.PROCESS_AND_STORE_DATA_STATE;
        String name5 = ProcessAndStoreDataState.class.getName();
        Wg6.b(name5, "ProcessAndStoreDataState::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
    }

    @DexIgnore
    public final boolean isClearDataOptional() {
        return this.isClearDataOptional;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.PLAY_DEVICE_ANIMATION_STATE));
        return true;
    }

    @DexIgnore
    public final void setSyncLog(MFSyncLog mFSyncLog) {
        this.syncLog = mFSyncLog;
    }

    @DexIgnore
    public final void setSyncTime(long j) {
        this.syncTime = j;
    }

    @DexIgnore
    public final void updateCurrentStepAndStepGoalFromApp(boolean z, UserProfile userProfile2) {
        Wg6.c(userProfile2, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".updateCurrentStepAndStepGoalFromApp() - currentState=" + getMCurrentState());
        if (getMCurrentState() instanceof ProcessAndStoreDataState) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((ProcessAndStoreDataState) mCurrentState).updateCurrentStepsAndStepGoal(z, userProfile2);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow.ProcessAndStoreDataState");
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String tag2 = getTAG();
        local2.d(tag2, "Inside " + getTAG() + ".updateCurrentStepAndStepGoalFromApp - cannot update current steps and step goal to device caused by current state null or not an instance of ProcessAndStoreDataState.");
    }
}
