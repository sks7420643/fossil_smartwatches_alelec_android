package com.misfit.frameworks.buttonservice.communite;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
public @interface SDKResult {
    public static final int SDK_CALLED = 0;
    public static final int SDK_FAILED = 1;
    public static final int SKIP = 2;
}
