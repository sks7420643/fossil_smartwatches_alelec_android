package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Ln1;
import com.fossil.Mn1;
import com.fossil.On1;
import com.fossil.Us1;
import com.fossil.Yx1;
import com.fossil.Zm1;
import com.mapped.R60;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import java.util.HashMap;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetImplicitDisplayUnitSession extends SetAutoSettingsSession {
    @DexIgnore
    public /* final */ UserDisplayUnit mUserDisplayUnit;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_DISPLAY_UNIT_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneState() {
            super(SetImplicitDisplayUnitSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetImplicitDisplayUnitSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetDisplayUnitState extends BleStateAbs {
        @DexIgnore
        public Zb0<Zm1[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetDisplayUnitState() {
            super(SetImplicitDisplayUnitSession.this.getTAG());
        }

        @DexIgnore
        private final R60[] prepareConfigData() {
            long currentTimeMillis = System.currentTimeMillis();
            long j = (long) 1000;
            long j2 = currentTimeMillis / j;
            short offset = (short) ((TimeZone.getDefault().getOffset(currentTimeMillis) / 1000) / 60);
            Ln1 ln1 = new Ln1();
            ln1.r(j2, (short) ((int) (currentTimeMillis - (j * j2))), offset);
            ln1.n(SetImplicitDisplayUnitSession.this.mUserDisplayUnit.getTemperatureUnit().toSDKTemperatureUnit(), Mn1.KCAL, SetImplicitDisplayUnitSession.this.mUserDisplayUnit.getDistanceUnit().toSDKDistanceUnit(), ConversionUtils.INSTANCE.getTimeFormat(SetImplicitDisplayUnitSession.this.getBleAdapter().getContext()), On1.MONTH_DAY_YEAR);
            return ln1.b();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Zb0<Zm1[]> deviceConfig = SetImplicitDisplayUnitSession.this.getBleAdapter().setDeviceConfig(SetImplicitDisplayUnitSession.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetImplicitDisplayUnitSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            SetImplicitDisplayUnitSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetImplicitDisplayUnitSession setImplicitDisplayUnitSession = SetImplicitDisplayUnitSession.this;
            setImplicitDisplayUnitSession.enterStateAsync(setImplicitDisplayUnitSession.createConcreteState((SetImplicitDisplayUnitSession) BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            SetImplicitDisplayUnitSession.this.log("Set Display Unit timeout. Cancel.");
            Zb0<Zm1[]> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetImplicitDisplayUnitSession(UserDisplayUnit userDisplayUnit, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_IMPLICIT_DISPLAY_UNIT, bleAdapterImpl, bleSessionCallback);
        Wg6.c(userDisplayUnit, "mUserDisplayUnit");
        Wg6.c(bleAdapterImpl, "bleAdapter");
        this.mUserDisplayUnit = userDisplayUnit;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetImplicitDisplayUnitSession setImplicitDisplayUnitSession = new SetImplicitDisplayUnitSession(this.mUserDisplayUnit, getBleAdapter(), getBleSessionCallback());
        setImplicitDisplayUnitSession.setDevice(getDevice());
        return setImplicitDisplayUnitSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_DISPLAY_UNIT_STATE;
        String name = SetDisplayUnitState.class.getName();
        Wg6.b(name, "SetDisplayUnitState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        Wg6.b(name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        Wg6.c(bleState, "<set-?>");
        this.startState = bleState;
    }
}
