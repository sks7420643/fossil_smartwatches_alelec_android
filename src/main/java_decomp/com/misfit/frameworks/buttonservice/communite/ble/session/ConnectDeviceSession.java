package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Hr7;
import com.fossil.Us1;
import com.fossil.Yx1;
import com.fossil.Zk1;
import com.fossil.Zm1;
import com.mapped.Q40;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeConfig;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ConnectDeviceSession extends EnableMaintainingSession {
    @DexIgnore
    public BackgroundConfig backgroundConfig;
    @DexIgnore
    public ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public /* final */ boolean isUIAction;
    @DexIgnore
    public LocalizationData localizationData;
    @DexIgnore
    public List<? extends MicroAppMapping> microAppMappings;
    @DexIgnore
    public List<AlarmSetting> multiAlarmSettings;
    @DexIgnore
    public AppNotificationFilterSettings notificationFilterSettings;
    @DexIgnore
    public int secondTimezoneOffset;
    @DexIgnore
    public ThemeConfig themeConfig;
    @DexIgnore
    public /* final */ UserProfile userProfile;
    @DexIgnore
    public WatchAppMappingSettings watchAppMappingSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class FetchDeviceInfoState extends BleStateAbs {
        @DexIgnore
        public Zb0<Zk1> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public FetchDeviceInfoState() {
            super(ConnectDeviceSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Zb0<Zk1> fetchDeviceInfo = ConnectDeviceSession.this.getBleAdapter().fetchDeviceInfo(ConnectDeviceSession.this.getLogSession(), this);
            this.task = fetchDeviceInfo;
            if (fetchDeviceInfo == null) {
                ConnectDeviceSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onFetchDeviceInfoFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            Q40 deviceObj = ConnectDeviceSession.this.getBleAdapter().getDeviceObj();
            if ((deviceObj != null ? deviceObj.getState() : null) != Q40.Ci.CONNECTED) {
                ConnectDeviceSession.this.stop(FailureCode.FAILED_TO_CONNECT);
            } else if (!retry(ConnectDeviceSession.this.getBleAdapter().getContext(), ConnectDeviceSession.this.getSerial())) {
                ConnectDeviceSession.this.log("Reach the limit retry. Stop.");
                ConnectDeviceSession.this.stop(FailureCode.FAILED_TO_CONNECT);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onFetchDeviceInfoSuccess(Zk1 zk1) {
            Wg6.c(zk1, "deviceInformation");
            stopTimeout();
            ConnectDeviceSession connectDeviceSession = ConnectDeviceSession.this;
            connectDeviceSession.log("onFetchDeviceInfoSuccess fwVersion " + zk1.getFirmwareVersion());
            FLogger.INSTANCE.updateActiveDeviceInfo(new ActiveDeviceInfo(zk1.getSerialNumber(), zk1.getModelNumber(), zk1.getFirmwareVersion()));
            ConnectDeviceSession connectDeviceSession2 = ConnectDeviceSession.this;
            connectDeviceSession2.enterStateAsync(connectDeviceSession2.createConcreteState((ConnectDeviceSession) BleSessionAbs.SessionState.GET_DEVICE_CONFIG_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Zk1> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public Zb0<HashMap<Zm1, R60>> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public GetDeviceConfigState() {
            super(ConnectDeviceSession.this.getTAG());
        }

        @DexIgnore
        private final void logConfiguration(HashMap<Zm1, R60> hashMap) {
            ConnectDeviceSession connectDeviceSession = ConnectDeviceSession.this;
            Hr7 hr7 = Hr7.a;
            String format = String.format("Get configuration  " + ConnectDeviceSession.this.getSerial() + "\n\t[timeConfiguration: \n\ttime = %s,\n\tbattery = %s,\n\tbiometric = %s,\n\tdaily steps = %s,\n\tdaily step goal = %s, \n\tdaily calorie: %s, \n\tdaily calorie goal: %s, \n\tdaily total active minute: %s, \n\tdaily active minute goal: %s, \n\tdaily distance: %s, \n\tinactive nudge: %s, \n\tvibration strength: %s, \n\tdo not disturb schedule: %s, \n\t]", Arrays.copyOf(new Object[]{hashMap.get(Zm1.TIME), hashMap.get(Zm1.BATTERY), hashMap.get(Zm1.BIOMETRIC_PROFILE), hashMap.get(Zm1.DAILY_STEP), hashMap.get(Zm1.DAILY_STEP_GOAL), hashMap.get(Zm1.DAILY_CALORIE), hashMap.get(Zm1.DAILY_CALORIE_GOAL), hashMap.get(Zm1.DAILY_TOTAL_ACTIVE_MINUTE), hashMap.get(Zm1.DAILY_ACTIVE_MINUTE_GOAL), hashMap.get(Zm1.DAILY_DISTANCE), hashMap.get(Zm1.INACTIVE_NUDGE), hashMap.get(Zm1.VIBE_STRENGTH), hashMap.get(Zm1.DO_NOT_DISTURB_SCHEDULE)}, 13));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            connectDeviceSession.log(format);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            Zb0<HashMap<Zm1, R60>> deviceConfig = ConnectDeviceSession.this.getBleAdapter().getDeviceConfig(ConnectDeviceSession.this.getLogSession(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                ConnectDeviceSession connectDeviceSession = ConnectDeviceSession.this;
                connectDeviceSession.enterStateAsync(connectDeviceSession.createConcreteState((ConnectDeviceSession) BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            ConnectDeviceSession.this.addFailureCode(FailureCode.FAILED_TO_GET_CONFIG);
            ConnectDeviceSession connectDeviceSession = ConnectDeviceSession.this;
            connectDeviceSession.enterStateAsync(connectDeviceSession.createConcreteState((ConnectDeviceSession) BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigSuccess(HashMap<Zm1, R60> hashMap) {
            Wg6.c(hashMap, "deviceConfiguration");
            stopTimeout();
            logConfiguration(hashMap);
            ConnectDeviceSession connectDeviceSession = ConnectDeviceSession.this;
            connectDeviceSession.enterStateAsync(connectDeviceSession.createConcreteState((ConnectDeviceSession) BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<HashMap<Zm1, R60>> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferSettingsSubFlow extends BaseTransferSettingsSubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public TransferSettingsSubFlow() {
            super(ConnectDeviceSession.this.getTAG(), ConnectDeviceSession.this, ConnectDeviceSession.this.getMfLog(), ConnectDeviceSession.this.getLogSession(), false, ConnectDeviceSession.this.getSerial(), ConnectDeviceSession.this.getBleAdapter(), ConnectDeviceSession.this.userProfile, ConnectDeviceSession.this.multiAlarmSettings, ConnectDeviceSession.this.complicationAppMappingSettings, ConnectDeviceSession.this.watchAppMappingSettings, ConnectDeviceSession.this.backgroundConfig, ConnectDeviceSession.this.themeConfig, ConnectDeviceSession.this.notificationFilterSettings, ConnectDeviceSession.this.localizationData, ConnectDeviceSession.this.microAppMappings, ConnectDeviceSession.this.secondTimezoneOffset, ConnectDeviceSession.this.getBleSessionCallback());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            ConnectDeviceSession.this.stop(i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ConnectDeviceSession(boolean z, UserProfile userProfile2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(!z ? SessionType.CONNECT : SessionType.UI, !z ? CommunicateMode.RECONNECT : CommunicateMode.FORCE_CONNECT, bleAdapterImpl, bleSessionCallback);
        Wg6.c(bleAdapterImpl, "bleAdapter");
        this.isUIAction = z;
        this.userProfile = userProfile2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(getBleAdapter()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        ConnectDeviceSession connectDeviceSession = new ConnectDeviceSession(this.isUIAction, this.userProfile, getBleAdapter(), getBleSessionCallback());
        connectDeviceSession.setDevice(getDevice());
        return connectDeviceSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void doNextState() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "doNextState, serial=" + getSerial());
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.ConnectDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        super.initSettings();
        this.multiAlarmSettings = DevicePreferenceUtils.getAutoListAlarm(getContext());
        this.complicationAppMappingSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(getContext(), getSerial());
        this.watchAppMappingSettings = DevicePreferenceUtils.getAutoWatchAppSettings(getContext(), getSerial());
        this.backgroundConfig = DevicePreferenceUtils.getAutoBackgroundImageConfig(getContext(), getSerial());
        this.notificationFilterSettings = DevicePreferenceUtils.getAutoNotificationFiltersConfig(getContext(), getSerial());
        this.localizationData = DevicePreferenceUtils.getAutoLocalizationDataSettings(getContext(), getSerial());
        this.microAppMappings = MicroAppMapping.convertToMicroAppMapping(DevicePreferenceUtils.getAutoMapping(getContext(), getSerial()));
        this.secondTimezoneOffset = DevicePreferenceUtils.getAutoSecondTimezone(getContext());
        this.themeConfig = DevicePreferenceUtils.getThemeConfig(getContext(), getSerial());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE;
        String name = FetchDeviceInfoState.class.getName();
        Wg6.b(name, "FetchDeviceInfoState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.GET_DEVICE_CONFIG_STATE;
        String name2 = GetDeviceConfigState.class.getName();
        Wg6.b(name2, "GetDeviceConfigState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap3 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState3 = BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW;
        String name3 = TransferSettingsSubFlow.class.getName();
        Wg6.b(name3, "TransferSettingsSubFlow::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onGetWatchParamFailed() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed");
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.ConnectDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchAppFileSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onWatchAppFilesReady(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onWatchAppFilesReady, isSuccess = " + z);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setWatchAppFiles(z);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.ConnectDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setWatchAppFiles FAILED. It is not in SetWatchAppFilesSession");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        Wg6.c(str, "serial");
        Wg6.c(watchParamsFileMapping, "watchParamsData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setLatestWatchParam, serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String tag2 = getTAG();
        remote.d(component, session, str, tag2, "setLatestWatchParam(), serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setLatestWatchParam(str, watchParamsFileMapping);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.ConnectDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, can't set WatchParams because currentState is not an instance of TransferSettingSubFlow");
    }
}
