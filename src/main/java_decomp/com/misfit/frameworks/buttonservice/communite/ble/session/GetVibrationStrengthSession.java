package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Hr7;
import com.fossil.Us1;
import com.fossil.Yx1;
import com.fossil.Zm1;
import com.mapped.A70;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetVibrationStrengthSession extends EnableMaintainingSession {
    @DexIgnore
    public A70.Ai mVibrationStrengthLevel; // = A70.Ai.MEDIUM;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetVibrationStrengthState extends BleStateAbs {
        @DexIgnore
        public Zb0<HashMap<Zm1, R60>> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public GetVibrationStrengthState() {
            super(GetVibrationStrengthSession.this.getTAG());
        }

        @DexIgnore
        private final void logConfiguration(HashMap<Zm1, R60> hashMap) {
            GetVibrationStrengthSession getVibrationStrengthSession = GetVibrationStrengthSession.this;
            Hr7 hr7 = Hr7.a;
            String format = String.format("Get configuration  " + GetVibrationStrengthSession.this.getSerial() + "\n\t[timeConfiguration: \n\ttime = %s,\n\tbattery = %s,\n\tbiometric = %s,\n\tdaily steps = %s,\n\tdaily step goal = %s, \n\tdaily calorie: %s, \n\tdaily calorie goal: %s, \n\tdaily total active minute: %s, \n\tdaily active minute goal: %s, \n\tdaily distance: %s, \n\tinactive nudge: %s, \n\tvibration strength: %s, \n\tdo not disturb schedule: %s, \n\t]", Arrays.copyOf(new Object[]{hashMap.get(Zm1.TIME), hashMap.get(Zm1.BATTERY), hashMap.get(Zm1.BIOMETRIC_PROFILE), hashMap.get(Zm1.DAILY_STEP), hashMap.get(Zm1.DAILY_STEP_GOAL), hashMap.get(Zm1.DAILY_CALORIE), hashMap.get(Zm1.DAILY_CALORIE_GOAL), hashMap.get(Zm1.DAILY_TOTAL_ACTIVE_MINUTE), hashMap.get(Zm1.DAILY_ACTIVE_MINUTE_GOAL), hashMap.get(Zm1.DAILY_DISTANCE), hashMap.get(Zm1.INACTIVE_NUDGE), hashMap.get(Zm1.VIBE_STRENGTH), hashMap.get(Zm1.DO_NOT_DISTURB_SCHEDULE)}, 13));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            getVibrationStrengthSession.log(format);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            GetVibrationStrengthSession.this.log("Get Vibration Strength Level");
            Zb0<HashMap<Zm1, R60>> deviceConfig = GetVibrationStrengthSession.this.getBleAdapter().getDeviceConfig(GetVibrationStrengthSession.this.getLogSession(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                GetVibrationStrengthSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            GetVibrationStrengthSession.this.stop(FailureCode.FAILED_TO_GET_VIBRATION_STRENGTH);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigSuccess(HashMap<Zm1, R60> hashMap) {
            Wg6.c(hashMap, "deviceConfiguration");
            stopTimeout();
            logConfiguration(hashMap);
            if (hashMap.containsKey(Zm1.VIBE_STRENGTH)) {
                GetVibrationStrengthSession getVibrationStrengthSession = GetVibrationStrengthSession.this;
                R60 r60 = hashMap.get(Zm1.VIBE_STRENGTH);
                if (r60 != null) {
                    getVibrationStrengthSession.mVibrationStrengthLevel = ((A70) r60).getVibeStrengthLevel();
                    GetVibrationStrengthSession getVibrationStrengthSession2 = GetVibrationStrengthSession.this;
                    getVibrationStrengthSession2.log("Get Vibration Strength Level Success, value=" + GetVibrationStrengthSession.this.mVibrationStrengthLevel);
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.VibeStrengthConfig");
                }
            } else {
                GetVibrationStrengthSession.this.log("Get Vibration Strength Level Success, but no value.");
            }
            GetVibrationStrengthSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<HashMap<Zm1, R60>> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetVibrationStrengthSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.READ_REAL_TIME_STEP, bleAdapterImpl, bleSessionCallback);
        Wg6.c(bleAdapterImpl, "bleAdapter");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable(ButtonService.Companion.getVIBRATION_STRENGTH_LEVEL(), VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel$default(VibrationStrengthObj.Companion, this.mVibrationStrengthLevel, false, 2, null));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        GetVibrationStrengthSession getVibrationStrengthSession = new GetVibrationStrengthSession(getBleAdapter(), getBleSessionCallback());
        getVibrationStrengthSession.setDevice(getDevice());
        return getVibrationStrengthSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.GET_VIBRATION_STRENGTH_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.GET_VIBRATION_STRENGTH_STATE;
        String name = GetVibrationStrengthState.class.getName();
        Wg6.b(name, "GetVibrationStrengthState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
