package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.Mp1;
import com.fossil.fitness.FitnessData;
import com.mapped.Q40;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleCommunicatorAbs$mStateCallback$Anon1 implements Q40.Bi {
    @DexIgnore
    public /* final */ /* synthetic */ BleCommunicatorAbs this$0;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public BleCommunicatorAbs$mStateCallback$Anon1(BleCommunicatorAbs bleCommunicatorAbs) {
        this.this$0 = bleCommunicatorAbs;
    }

    @DexIgnore
    @Override // com.mapped.Q40.Bi
    public void onAutoSyncFitnessDataReceived(FitnessData[] fitnessDataArr) {
        Wg6.c(fitnessDataArr, "data");
    }

    @DexIgnore
    @Override // com.mapped.Q40.Bi
    public void onDeviceStateChanged(Q40 q40, Q40.Ci ci, Q40.Ci ci2) {
        Wg6.c(q40, "device");
        Wg6.c(ci, "previousState");
        Wg6.c(ci2, "newState");
        this.this$0.handleDeviceStateChanged(q40, ci, ci2);
    }

    @DexIgnore
    @Override // com.mapped.Q40.Bi
    public void onEventReceived(Q40 q40, Mp1 mp1) {
        Wg6.c(q40, "device");
        Wg6.c(mp1, Constants.EVENT);
        this.this$0.handleEventReceived(q40, mp1);
    }
}
