package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.Qq7;
import com.google.gson.Gson;
import com.mapped.C90;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$readCurrentWorkoutSession$$inlined$let$lambda$Anon1 extends Qq7 implements Hg6<C90, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$readCurrentWorkoutSession$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(C90 c90) {
        invoke(c90);
        return Cd6.a;
    }

    @DexIgnore
    public final void invoke(C90 c90) {
        Wg6.c(c90, "it");
        BleAdapterImpl bleAdapterImpl = this.this$0;
        FLogger.Session session = this.$logSession$inlined;
        bleAdapterImpl.log(session, "Read Current Workout Session Success, session=" + new Gson().t(c90));
        this.$callback$inlined.onReadCurrentWorkoutSessionSuccess(c90);
    }
}
