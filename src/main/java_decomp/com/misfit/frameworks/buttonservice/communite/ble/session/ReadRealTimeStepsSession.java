package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Hr7;
import com.fossil.Us1;
import com.fossil.Vm1;
import com.fossil.Yx1;
import com.fossil.Zm1;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ReadRealTimeStepsSession extends EnableMaintainingSession {
    @DexIgnore
    public long mRealTimeSteps; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadRealTimeStepsState extends BleStateAbs {
        @DexIgnore
        public Zb0<HashMap<Zm1, R60>> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ReadRealTimeStepsState() {
            super(ReadRealTimeStepsSession.this.getTAG());
        }

        @DexIgnore
        private final void logConfiguration(HashMap<Zm1, R60> hashMap) {
            ReadRealTimeStepsSession readRealTimeStepsSession = ReadRealTimeStepsSession.this;
            Hr7 hr7 = Hr7.a;
            String format = String.format("Get configuration  " + ReadRealTimeStepsSession.this.getSerial() + "\n\t[timeConfiguration: \n\ttime = %s,\n\tbattery = %s,\n\tbiometric = %s,\n\tdaily steps = %s,\n\tdaily step goal = %s, \n\tdaily calorie: %s, \n\tdaily calorie goal: %s, \n\tdaily total active minute: %s, \n\tdaily active minute goal: %s, \n\tdaily distance: %s, \n\tinactive nudge: %s, \n\tvibration strength: %s, \n\tdo not disturb schedule: %s, \n\t]", Arrays.copyOf(new Object[]{hashMap.get(Zm1.TIME), hashMap.get(Zm1.BATTERY), hashMap.get(Zm1.BIOMETRIC_PROFILE), hashMap.get(Zm1.DAILY_STEP), hashMap.get(Zm1.DAILY_STEP_GOAL), hashMap.get(Zm1.DAILY_CALORIE), hashMap.get(Zm1.DAILY_CALORIE_GOAL), hashMap.get(Zm1.DAILY_TOTAL_ACTIVE_MINUTE), hashMap.get(Zm1.DAILY_ACTIVE_MINUTE_GOAL), hashMap.get(Zm1.DAILY_DISTANCE), hashMap.get(Zm1.INACTIVE_NUDGE), hashMap.get(Zm1.VIBE_STRENGTH), hashMap.get(Zm1.DO_NOT_DISTURB_SCHEDULE)}, 13));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            readRealTimeStepsSession.log(format);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            ReadRealTimeStepsSession.this.log("Read Real Time Steps");
            Zb0<HashMap<Zm1, R60>> deviceConfig = ReadRealTimeStepsSession.this.getBleAdapter().getDeviceConfig(ReadRealTimeStepsSession.this.getLogSession(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                ReadRealTimeStepsSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            ReadRealTimeStepsSession.this.stop(FailureCode.FAILED_TO_GET_REAL_TIME_STEP);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetDeviceConfigSuccess(HashMap<Zm1, R60> hashMap) {
            Wg6.c(hashMap, "deviceConfiguration");
            stopTimeout();
            logConfiguration(hashMap);
            if (hashMap.containsKey(Zm1.DAILY_STEP)) {
                ReadRealTimeStepsSession readRealTimeStepsSession = ReadRealTimeStepsSession.this;
                R60 r60 = hashMap.get(Zm1.DAILY_STEP);
                if (r60 != null) {
                    readRealTimeStepsSession.mRealTimeSteps = ((Vm1) r60).getStep();
                    ReadRealTimeStepsSession readRealTimeStepsSession2 = ReadRealTimeStepsSession.this;
                    readRealTimeStepsSession2.log("Read Real Time Steps Success, value=" + ReadRealTimeStepsSession.this.mRealTimeSteps);
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyStepConfig");
                }
            } else {
                ReadRealTimeStepsSession.this.log("Read Real Time Steps Success, but no value.");
            }
            ReadRealTimeStepsSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<HashMap<Zm1, R60>> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReadRealTimeStepsSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.READ_REAL_TIME_STEP, bleAdapterImpl, bleSessionCallback);
        Wg6.c(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putLong(Constants.DAILY_STEPS, this.mRealTimeSteps);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        ReadRealTimeStepsSession readRealTimeStepsSession = new ReadRealTimeStepsSession(getBleAdapter(), getBleSessionCallback());
        readRealTimeStepsSession.setDevice(getDevice());
        return readRealTimeStepsSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.READ_REAL_TIME_STEPS_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.READ_REAL_TIME_STEPS_STATE;
        String name = ReadRealTimeStepsState.class.getName();
        Wg6.b(name, "ReadRealTimeStepsState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
