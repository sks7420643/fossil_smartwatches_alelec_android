package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Is1;
import com.fossil.Us1;
import com.fossil.Yx1;
import com.mapped.Cd6;
import com.mapped.Wg6;
import com.mapped.Yb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoNotificationFiltersConfigSession extends SetAutoSettingsSession {
    @DexIgnore
    public /* final */ AppNotificationFilterSettings mNewNotificationFilterSettings;
    @DexIgnore
    public AppNotificationFilterSettings mOldNotificationFilterSettings;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneState() {
            super(SetAutoNotificationFiltersConfigSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoNotificationFiltersConfigSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetNotificationFilters extends BleStateAbs {
        @DexIgnore
        public Yb0<Cd6> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetNotificationFilters() {
            super(SetAutoNotificationFiltersConfigSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Yb0<Cd6> notificationFilters = SetAutoNotificationFiltersConfigSession.this.getBleAdapter().setNotificationFilters(SetAutoNotificationFiltersConfigSession.this.getLogSession(), SetAutoNotificationFiltersConfigSession.this.mNewNotificationFilterSettings.getNotificationFilters(), this);
            this.task = notificationFilters;
            if (notificationFilters == null) {
                SetAutoNotificationFiltersConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetNotificationFilterFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            if (!retry(SetAutoNotificationFiltersConfigSession.this.getContext(), SetAutoNotificationFiltersConfigSession.this.getSerial())) {
                SetAutoNotificationFiltersConfigSession.this.log("Reach the limit retry. Stop.");
                SetAutoNotificationFiltersConfigSession setAutoNotificationFiltersConfigSession = SetAutoNotificationFiltersConfigSession.this;
                setAutoNotificationFiltersConfigSession.storeMappings(setAutoNotificationFiltersConfigSession.mNewNotificationFilterSettings, true);
                SetAutoNotificationFiltersConfigSession.this.stop(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetNotificationFilterProgressChanged(float f) {
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetNotificationFilterSuccess() {
            stopTimeout();
            SetAutoNotificationFiltersConfigSession setAutoNotificationFiltersConfigSession = SetAutoNotificationFiltersConfigSession.this;
            setAutoNotificationFiltersConfigSession.storeMappings(setAutoNotificationFiltersConfigSession.mNewNotificationFilterSettings, false);
            SetAutoNotificationFiltersConfigSession setAutoNotificationFiltersConfigSession2 = SetAutoNotificationFiltersConfigSession.this;
            setAutoNotificationFiltersConfigSession2.enterState(setAutoNotificationFiltersConfigSession2.createConcreteState((SetAutoNotificationFiltersConfigSession) BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Yb0<Cd6> yb0 = this.task;
            if (yb0 != null) {
                Us1.a(yb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoNotificationFiltersConfigSession(AppNotificationFilterSettings appNotificationFilterSettings, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS, bleAdapterImpl, bleSessionCallback);
        Wg6.c(appNotificationFilterSettings, "mNewNotificationFilterSettings");
        Wg6.c(bleAdapterImpl, "bleAdapter");
        this.mNewNotificationFilterSettings = appNotificationFilterSettings;
    }

    @DexIgnore
    private final void storeMappings(AppNotificationFilterSettings appNotificationFilterSettings, boolean z) {
        DevicePreferenceUtils.setAutoNotificationFiltersConfig(getBleAdapter().getContext(), getBleAdapter().getSerial(), appNotificationFilterSettings);
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.NOTIFICATION_FILTERS);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetAutoNotificationFiltersConfigSession setAutoNotificationFiltersConfigSession = new SetAutoNotificationFiltersConfigSession(this.mNewNotificationFilterSettings, getBleAdapter(), getBleSessionCallback());
        setAutoNotificationFiltersConfigSession.setDevice(getDevice());
        return setAutoNotificationFiltersConfigSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        BleState createConcreteState;
        super.initSettings();
        this.mOldNotificationFilterSettings = DevicePreferenceUtils.getAutoNotificationFiltersConfig(getContext(), getSerial());
        if (getBleAdapter().isSupportedFeature(Is1.class) == null) {
            log("This device does not support set complication apps.");
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (AppNotificationFilterSettings.CREATOR.isSettingsSame(this.mOldNotificationFilterSettings, this.mNewNotificationFilterSettings)) {
            log("New notification settings and the old one are the same, no need to store again.");
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_NOTIFICATION_FILTERS_STATE);
        } else if (AppNotificationFilterSettings.CREATOR.compareTimeStamp(this.mNewNotificationFilterSettings, this.mOldNotificationFilterSettings) > 0) {
            storeMappings(this.mNewNotificationFilterSettings, true);
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_NOTIFICATION_FILTERS_STATE);
        } else {
            log("Old notification settings timestamp is greater than the new one, no need to store again.");
            createConcreteState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
        setStartState(createConcreteState);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_NOTIFICATION_FILTERS_STATE;
        String name = SetNotificationFilters.class.getName();
        Wg6.b(name, "SetNotificationFilters::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        Wg6.b(name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        Wg6.c(bleState, "<set-?>");
        this.startState = bleState;
    }
}
