package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.Ln1;
import com.fossil.Us1;
import com.fossil.Yx1;
import com.fossil.Zm1;
import com.mapped.R60;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetWorkoutConfigSession extends EnableMaintainingSession {
    @DexIgnore
    public /* final */ WorkoutConfigData mWorkoutConfigData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWorkoutConfigState extends BleStateAbs {
        @DexIgnore
        public Zb0<Zm1[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWorkoutConfigState() {
            super(SetWorkoutConfigSession.this.getTAG());
        }

        @DexIgnore
        private final R60[] prepareConfigData() {
            Ln1 ln1 = new Ln1();
            ln1.e(SetWorkoutConfigSession.this.mWorkoutConfigData.diameterInMillimeter, SetWorkoutConfigSession.this.mWorkoutConfigData.tireSizeInMillimeter, SetWorkoutConfigSession.this.mWorkoutConfigData.chainring, SetWorkoutConfigSession.this.mWorkoutConfigData.cog);
            return ln1.b();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            SetWorkoutConfigSession setWorkoutConfigSession = SetWorkoutConfigSession.this;
            setWorkoutConfigSession.log("SetWorkoutConfigSession: WorkoutConfigData=" + SetWorkoutConfigSession.this.mWorkoutConfigData);
            Zb0<Zm1[]> deviceConfig = SetWorkoutConfigSession.this.getBleAdapter().setDeviceConfig(SetWorkoutConfigSession.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetWorkoutConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            if (!retry(SetWorkoutConfigSession.this.getContext(), SetWorkoutConfigSession.this.getSerial())) {
                SetWorkoutConfigSession.this.log("Reach the limit retry. Stop.");
                SetWorkoutConfigSession.this.stop(FailureCode.FAILED_TO_SET_WORKOUT_CONFIG_SESSION);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetWorkoutConfigSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Zm1[]> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
            SetWorkoutConfigSession.this.stop(FailureCode.FAILED_TO_SET_WORKOUT_CONFIG_SESSION);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetWorkoutConfigSession(WorkoutConfigData workoutConfigData, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.DEVICE_SETTING, CommunicateMode.SET_WORKOUT_CONFIG_SESSION, bleAdapterImpl, bleSessionCallback);
        Wg6.c(workoutConfigData, "mWorkoutConfigData");
        Wg6.c(bleAdapterImpl, "bleAdapter");
        this.mWorkoutConfigData = workoutConfigData;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetWorkoutConfigSession setWorkoutConfigSession = new SetWorkoutConfigSession(this.mWorkoutConfigData, getBleAdapter(), getBleSessionCallback());
        setWorkoutConfigSession.setDevice(getDevice());
        return setWorkoutConfigSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.SET_WORKOUT_CONFIG_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_WORKOUT_CONFIG_STATE;
        String name = SetWorkoutConfigState.class.getName();
        Wg6.b(name, "SetWorkoutConfigState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
