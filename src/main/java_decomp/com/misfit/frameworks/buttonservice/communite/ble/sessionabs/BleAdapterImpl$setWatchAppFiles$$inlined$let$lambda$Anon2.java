package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.Qq7;
import com.fossil.Ru1;
import com.fossil.Yx1;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$setWatchAppFiles$$inlined$let$lambda$Anon2 extends Qq7 implements Hg6<Yx1, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Ru1[] $watchAppFiles$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setWatchAppFiles$$inlined$let$lambda$Anon2(BleAdapterImpl bleAdapterImpl, Ru1[] ru1Arr, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$watchAppFiles$inlined = ru1Arr;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Yx1 yx1) {
        invoke(yx1);
        return Cd6.a;
    }

    @DexIgnore
    public final void invoke(Yx1 yx1) {
        Wg6.c(yx1, "it");
        this.this$0.logSdkError(this.$logSession$inlined, "Set Watch App Files", ErrorCodeBuilder.Step.SET_WATCH_APP_FILE, yx1);
        this.$callback$inlined.onSetWatchAppFileFailed();
    }
}
