package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.NullBleState;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$stopSubFlow$1", f = "SubFlow.kt", l = {}, m = "invokeSuspend")
public final class SubFlow$stopSubFlow$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $failureCode;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SubFlow this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SubFlow$stopSubFlow$Anon1(SubFlow subFlow, int i, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = subFlow;
        this.$failureCode = i;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SubFlow$stopSubFlow$Anon1 subFlow$stopSubFlow$Anon1 = new SubFlow$stopSubFlow$Anon1(this.this$0, this.$failureCode, xe6);
        subFlow$stopSubFlow$Anon1.p$ = (Il6) obj;
        throw null;
        //return subFlow$stopSubFlow$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((SubFlow$stopSubFlow$Anon1) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Yn7.d();
        if (this.label == 0) {
            El7.b(obj);
            if (!BleState.Companion.isNull(this.this$0.getMCurrentState())) {
                this.this$0.getMCurrentState().stopTimeout();
                boolean unused = this.this$0.enterSubState(new NullBleState(this.this$0.getTAG()));
            }
            this.this$0.onStop(this.$failureCode);
            this.this$0.setExist(false);
            return Cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
