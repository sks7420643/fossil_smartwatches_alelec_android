package com.misfit.frameworks.buttonservice.communite.ble;

import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ICalibrationSession {
    @DexIgnore
    boolean handleApplyHandsPosition();

    @DexIgnore
    boolean handleMoveHandRequest(HandCalibrationObj handCalibrationObj);

    @DexIgnore
    boolean handleReleaseHandControl();

    @DexIgnore
    boolean handleResetHandsPosition();
}
