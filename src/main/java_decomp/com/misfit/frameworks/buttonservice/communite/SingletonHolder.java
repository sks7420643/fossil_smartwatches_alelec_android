package com.misfit.frameworks.buttonservice.communite;

import com.mapped.Hg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SingletonHolder<T, A> {
    @DexIgnore
    public Hg6<? super A, ? extends T> creator;
    @DexIgnore
    public volatile T instance;

    @DexIgnore
    public SingletonHolder(Hg6<? super A, ? extends T> hg6) {
        Wg6.c(hg6, "creator");
        this.creator = hg6;
    }

    @DexIgnore
    public final T getInstance(A a2) {
        T t = this.instance;
        if (t == null) {
            synchronized (this) {
                t = this.instance;
                if (t == null) {
                    Hg6<? super A, ? extends T> hg6 = this.creator;
                    if (hg6 != null) {
                        t = (T) hg6.invoke(a2);
                        this.instance = t;
                        this.creator = null;
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
        }
        return t;
    }
}
