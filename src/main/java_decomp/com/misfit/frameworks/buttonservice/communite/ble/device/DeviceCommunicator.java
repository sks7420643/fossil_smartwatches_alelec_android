package com.misfit.frameworks.buttonservice.communite.ble.device;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.facebook.places.model.PlaceFields;
import com.fossil.Aq1;
import com.fossil.Bq1;
import com.fossil.Cq1;
import com.fossil.Du1;
import com.fossil.Iq1;
import com.fossil.Lq1;
import com.fossil.Lw1;
import com.fossil.Mp1;
import com.fossil.Mq1;
import com.fossil.Oq1;
import com.fossil.Pp1;
import com.fossil.Pq1;
import com.fossil.Qp1;
import com.fossil.Rp1;
import com.fossil.Ry1;
import com.fossil.Tp1;
import com.fossil.Up1;
import com.fossil.Vp1;
import com.fossil.Wp1;
import com.fossil.Xp1;
import com.fossil.Yp1;
import com.fossil.Zk1;
import com.fossil.Zp1;
import com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor;
import com.fossil.fitness.FitnessData;
import com.mapped.Cd6;
import com.mapped.E90;
import com.mapped.N70;
import com.mapped.Q40;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.IReadWorkoutStateSession;
import com.misfit.frameworks.buttonservice.communite.ble.ISetWatchAppFileSession;
import com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession;
import com.misfit.frameworks.buttonservice.communite.ble.IVerifySecretKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ApplyThemeSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.CalibrationDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ClearLinkMappingSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ConnectDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.NotifyNotificationEventSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.OtaSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.PreviewThemeSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.ReadCurrentWorkoutSessionSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SendingEncryptedDataSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoBackgroundImageConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoBiometricDataSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoLocalizationDataSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoMappingsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoMultiAlarmsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoNotificationFiltersConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoSecondTimezoneSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetAutoWatchAppsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetBackgroundImageConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetFrontLightEnableSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetHeartRateModeSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetImplicitDeviceConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetImplicitDisplayUnitSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetInactiveNudgeConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetLinkMappingsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetMinimumStepThresholdSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetMultiAlarmsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetNotificationFiltersConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetReplyMessageMappingSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetSecondTimezoneSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetWatchAppsSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SetWorkoutConfigSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.StopCurrentWorkoutSessionSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SwitchActiveDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession;
import com.misfit.frameworks.buttonservice.communite.ble.session.VerifySecretKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.EncryptedData;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicTrackInfoResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeConfig;
import com.misfit.frameworks.buttonservice.model.watchface.ThemeData;
import com.misfit.frameworks.buttonservice.model.watchparams.Version;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData;
import com.misfit.frameworks.buttonservice.utils.SharePreferencesUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceCommunicator extends BleCommunicatorAbs {
    @DexIgnore
    public /* final */ HashMap<String, X90> mDeviceAppRequests; // = new HashMap<>();

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator(Context context, String str, String str2, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        super(new BleAdapterImpl(context, str, str2), context, str, communicationResultCallback);
        Wg6.c(context, "context");
        Wg6.c(str, "serial");
        Wg6.c(str2, "macAddress");
        Wg6.c(communicationResultCallback, "communicationResultCallback");
    }

    @DexIgnore
    public static final /* synthetic */ String access$getTAG$p(DeviceCommunicator deviceCommunicator) {
        return deviceCommunicator.getTAG();
    }

    @DexIgnore
    private final void sendDeviceAppResponseFromQueue(DeviceAppResponse deviceAppResponse) {
        synchronized (this) {
            if (!deviceAppResponse.getLifeTimeObject().isExpire()) {
                if (deviceAppResponse.isForceUpdate()) {
                    sendResponseToWatch(deviceAppResponse, deviceAppResponse.getSDKDeviceData());
                } else {
                    String name = deviceAppResponse.getDeviceEventId().name();
                    X90 x90 = this.mDeviceAppRequests.get(name);
                    if (x90 != null) {
                        sendResponseToWatch(deviceAppResponse, deviceAppResponse.getSDKDeviceResponse(x90, new Ry1((byte) getBleAdapter().getMicroAppMajorVersion(), (byte) getBleAdapter().getMicroAppMinorVersion())));
                    } else {
                        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                        FLogger.Component component = FLogger.Component.BLE;
                        FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
                        String serial = getSerial();
                        String tag = getTAG();
                        String build = ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SEND_RESPOND, ErrorCodeBuilder.Component.SDK, ErrorCodeBuilder.AppError.UNKNOWN);
                        ErrorCodeBuilder.Step step = ErrorCodeBuilder.Step.SEND_RESPOND;
                        remote.e(component, session, serial, tag, build, step, "Send respond: " + deviceAppResponse.getDeviceEventId().name() + " Failed. deviceAppRequest with key[" + name + "] does not exist.");
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String tag2 = getTAG();
                        local.e(tag2, "device with serial = " + getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue(), deviceAppRequest with key[" + name + "] does not exist.");
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void sendDianaNotification(NotificationBaseObj notificationBaseObj) {
        synchronized (this) {
            addToQuickCommandQueue(notificationBaseObj);
        }
    }

    @DexIgnore
    private final void sendMusicResponseFromQueue(MusicResponse musicResponse) {
        Zb0<Cd6> zb0;
        Zb0<Cd6> s;
        synchronized (this) {
            if (!musicResponse.getLifeCountDownObject().isExpire()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = getTAG();
                local.d(tag, "device with serial = " + getBleAdapter().getSerial() + " .sendMusicResponseFromQueue() = " + musicResponse.toString());
                if (musicResponse instanceof NotifyMusicEventResponse) {
                    N70 sDKMusicEvent = ((NotifyMusicEventResponse) musicResponse).toSDKMusicEvent();
                    if (sDKMusicEvent != null) {
                        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                        FLogger.Component component = FLogger.Component.BLE;
                        FLogger.Session session = FLogger.Session.OTHER;
                        String serial = getSerial();
                        String tag2 = getTAG();
                        remote.d(component, session, serial, tag2, "NotifyMusicEvent: " + musicResponse.toRemoteLogString());
                        zb0 = getBleAdapter().notifyMusicEvent(FLogger.Session.OTHER, sDKMusicEvent);
                    } else {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String tag3 = getTAG();
                        local2.e(tag3, "device with serial = " + getBleAdapter().getSerial() + " .sendMusicResponseFromQueue() fail cause by sdkMusicEvent=" + sDKMusicEvent);
                        zb0 = null;
                    }
                } else if (musicResponse instanceof MusicTrackInfoResponse) {
                    IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                    FLogger.Component component2 = FLogger.Component.BLE;
                    FLogger.Session session2 = FLogger.Session.OTHER;
                    String serial2 = getSerial();
                    String tag4 = getTAG();
                    remote2.d(component2, session2, serial2, tag4, "SendTrackInfo: " + musicResponse.toRemoteLogString());
                    zb0 = getBleAdapter().sendTrackInfo(FLogger.Session.OTHER, ((MusicTrackInfoResponse) musicResponse).toSDKTrackInfo());
                } else {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String tag5 = getTAG();
                    local3.d(tag5, "device with serial = " + getBleAdapter().getSerial() + " .sendMusicResponseFromQueue() no support value = " + musicResponse.toRemoteLogString());
                    zb0 = null;
                }
                musicResponse.getLifeCountDownObject().goDown();
                if (!(zb0 == null || (s = zb0.s(new DeviceCommunicator$sendMusicResponseFromQueue$Anon1(this, musicResponse))) == null)) {
                    s.r(new DeviceCommunicator$sendMusicResponseFromQueue$Anon2(this, musicResponse));
                }
            }
        }
    }

    @DexIgnore
    private final void sendNotificationFromQueue(NotificationBaseObj notificationBaseObj) {
        Zb0<Cd6> s;
        synchronized (this) {
            if (!notificationBaseObj.getLifeCountDownObject().isExpire()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = getTAG();
                local.d(tag, " .sendNotificationFromQueue() = " + notificationBaseObj + ", serial=" + getSerial());
                IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                FLogger.Component component = FLogger.Component.BLE;
                FLogger.Session session = FLogger.Session.OTHER;
                String serial = getSerial();
                String tag2 = getTAG();
                remote.d(component, session, serial, tag2, "Send notification: " + notificationBaseObj.toRemoteLogString());
                Zb0<Cd6> sendNotification = getBleAdapter().sendNotification(FLogger.Session.OTHER, notificationBaseObj);
                notificationBaseObj.getLifeCountDownObject().goDown();
                if (!(sendNotification == null || (s = sendNotification.s(new DeviceCommunicator$sendNotificationFromQueue$Anon1(this, notificationBaseObj))) == null)) {
                    s.r(new DeviceCommunicator$sendNotificationFromQueue$Anon2(this, notificationBaseObj));
                }
            } else {
                getCommunicationResultCallback().onNotificationSent(notificationBaseObj.getUid(), false);
            }
        }
    }

    @DexIgnore
    private final void sendResponseToWatch(DeviceAppResponse deviceAppResponse, Tc0 tc0) {
        Zb0<Cd6> s;
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
        String serial = getSerial();
        String tag = getTAG();
        remote.i(component, session, serial, tag, "Send respond: " + deviceAppResponse);
        if (tc0 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag2 = getTAG();
            local.d(tag2, "device with serial = " + getBleAdapter().getSerial() + " .sendDeviceAppResponseFromQueue() = " + deviceAppResponse.toString());
            Zb0<Cd6> sendRespond = getBleAdapter().sendRespond(FLogger.Session.HANDLE_WATCH_REQUEST, tc0);
            if (sendRespond != null && (s = sendRespond.s(new DeviceCommunicator$sendResponseToWatch$Anon1(this, deviceAppResponse))) != null) {
                s.r(new DeviceCommunicator$sendResponseToWatch$Anon2(this, deviceAppResponse));
                return;
            }
            return;
        }
        IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
        FLogger.Component component2 = FLogger.Component.BLE;
        FLogger.Session session2 = FLogger.Session.HANDLE_WATCH_REQUEST;
        String serial2 = getSerial();
        String tag3 = getTAG();
        String build = ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SEND_RESPOND, ErrorCodeBuilder.Component.SDK, ErrorCodeBuilder.AppError.UNKNOWN);
        ErrorCodeBuilder.Step step = ErrorCodeBuilder.Step.SEND_RESPOND;
        remote2.e(component2, session2, serial2, tag3, build, step, "Send respond: " + deviceAppResponse.getDeviceEventId().name() + " Failed. device request type is not match with device response");
        FLogger.INSTANCE.getLocal().e(getTAG(), " .sendDeviceAppResponseFromQueue(), device request type is not match with device response or could not get DeviceData from DeviceAppResponse when forceUpdate");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean applyTheme(ThemeConfig themeConfig) {
        Wg6.c(themeConfig, "themeConfig");
        queueSessionAndStart(new ApplyThemeSession(themeConfig, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void confirmStopWorkout(String str, boolean z) {
        Wg6.c(str, "serial");
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IReadWorkoutStateSession) {
            ((IReadWorkoutStateSession) currentSession).confirmStopWorkout(str, z);
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".confirmStopWorkout() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String tag2 = getTAG();
        remote.i(component, session, str, tag2, "confirmStopWorkout FAILED, currentSession=" + currentSession);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean disableHeartRateNotification() {
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean enableHeartRateNotification() {
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public List<FitnessData> getSyncData() {
        if (getCurrentSession() instanceof SyncSession) {
            log("Current session is Sync Session, start get sync data.");
            BleSession currentSession = getCurrentSession();
            if (currentSession != null) {
                return ((SyncSession) currentSession).getSyncData();
            }
            throw new Rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession");
        }
        log("Current session is not Sync Session, empty sync data.");
        return new ArrayList();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public Version getUiOSVersion() {
        Zk1 M;
        Q40 deviceObj = getBleAdapter().getDeviceObj();
        Ry1 uiPackageOSVersion = (deviceObj == null || (M = deviceObj.M()) == null) ? null : M.getUiPackageOSVersion();
        if (uiPackageOSVersion == null || uiPackageOSVersion.getMajor() == 0 || uiPackageOSVersion.getMinor() == 0) {
            return null;
        }
        return new Version(uiPackageOSVersion.getMajor(), uiPackageOSVersion.getMinor());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs
    public void handleEventReceived(Q40 q40, Mp1 mp1) {
        Wg6.c(q40, "device");
        Wg6.c(mp1, Constants.EVENT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".handleEventReceived, device = " + q40 + ".deviceInformation.serialNumber, event = " + mp1);
        if (mp1 instanceof X90) {
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.HANDLE_WATCH_REQUEST;
            String serial = getSerial();
            String tag2 = getTAG();
            remote.i(component, session, serial, tag2, "Receive device request, device=" + getSerial() + ", device event=" + mp1);
            this.mDeviceAppRequests.put(mp1.getDeviceEventId().name(), mp1);
            Bundle bundle = new Bundle();
            if (mp1 instanceof Oq1) {
                bundle.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((Oq1) mp1).getAction().ordinal());
            } else if (mp1 instanceof Iq1) {
                bundle.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, Du1.START.ordinal());
                bundle.putString(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, ((Iq1) mp1).getDestination());
            } else if (mp1 instanceof Cq1) {
                bundle.putString(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, ((Cq1) mp1).getChallengeId());
            } else if (mp1 instanceof Pq1) {
                bundle.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, mp1);
            } else if (mp1 instanceof Mq1) {
                bundle.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, mp1);
            }
            getCommunicationResultCallback().onDeviceAppsRequest(mp1.getDeviceEventId().ordinal(), bundle, getSerial());
        } else if (mp1 instanceof Vp1) {
            IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
            FLogger.Component component2 = FLogger.Component.BLE;
            FLogger.Session session2 = FLogger.Session.OTHER;
            String serial2 = getSerial();
            String tag3 = getTAG();
            remote2.i(component2, session2, serial2, tag3, "Receive device notification, device=" + getSerial() + ", event=" + mp1);
            Bundle bundle2 = new Bundle();
            if (mp1 instanceof Xp1) {
                bundle2.putInt(ButtonService.Companion.getMUSIC_ACTION_EVENT(), NotifyMusicEventResponse.MusicMediaAction.Companion.fromSDKMusicAction(((Xp1) mp1).getAction()).ordinal());
            } else if (mp1 instanceof Yp1) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((Yp1) mp1).getAction().ordinal());
            } else if (mp1 instanceof Pp1) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((Pp1) mp1).getAction().ordinal());
            } else if (mp1 instanceof Up1) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((Up1) mp1).getAction().ordinal());
            } else if (mp1 instanceof Qp1) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, E90.APP_NOTIFICATION_CONTROL.ordinal());
                bundle2.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, mp1);
            } else if (mp1 instanceof Tp1) {
                bundle2.putInt(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_ACTION, ((Tp1) mp1).getAction().ordinal());
            } else if (mp1 instanceof Wp1) {
                bundle2.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, new EncryptedData((Wp1) mp1));
            } else if (mp1 instanceof Zp1) {
                bundle2.putLong(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, ((Zp1) mp1).getSessionId());
            } else if (mp1 instanceof Aq1) {
                bundle2.putLong(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, ((Aq1) mp1).getSessionId());
            } else if (mp1 instanceof Bq1) {
                bundle2.putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.DEVICE_REQUEST_EXTRA, mp1);
            }
            getCommunicationResultCallback().onDeviceAppsRequest(mp1.getDeviceEventId().ordinal(), bundle2, getSerial());
        } else if (mp1 instanceof Lq1) {
            IRemoteFLogger remote3 = FLogger.INSTANCE.getRemote();
            FLogger.Component component3 = FLogger.Component.BLE;
            FLogger.Session session3 = FLogger.Session.HANDLE_WATCH_REQUEST;
            String serial3 = getSerial();
            String tag4 = getTAG();
            remote3.i(component3, session3, serial3, tag4, "Receive micro app request, device=" + getSerial() + ", event=" + mp1);
            this.mDeviceAppRequests.put(mp1.getDeviceEventId().name(), mp1);
            getCommunicationResultCallback().onDeviceAppsRequest(mp1.getDeviceEventId().ordinal(), new Bundle(), getSerial());
        } else if (mp1 instanceof Rp1) {
            IRemoteFLogger remote4 = FLogger.INSTANCE.getRemote();
            FLogger.Component component4 = FLogger.Component.BLE;
            FLogger.Session session4 = FLogger.Session.HANDLE_WATCH_REQUEST;
            String serial4 = getSerial();
            String tag5 = getTAG();
            remote4.i(component4, session4, serial4, tag5, "Receive authentication request notification, device=" + getSerial() + ", event=" + mp1);
            getCommunicationResultCallback().onDeviceAppsRequest(mp1.getDeviceEventId().ordinal(), new Bundle(), getSerial());
        } else {
            super.handleEventReceived(q40, mp1);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onPing() {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onPing();
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onPing() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onPing FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs
    public void onQuickCommandAction(Object obj) {
        Wg6.c(obj, Constants.COMMAND);
        if (obj instanceof DeviceAppResponse) {
            sendDeviceAppResponseFromQueue((DeviceAppResponse) obj);
        } else if (obj instanceof MusicResponse) {
            sendMusicResponseFromQueue((MusicResponse) obj);
        } else if (obj instanceof NotificationBaseObj) {
            sendNotificationFromQueue((NotificationBaseObj) obj);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onReceiveCurrentSecretKey(byte[] bArr) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onReceiveCurrentSecretKey(bArr);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceiveCurrentSecretKey() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceiveCurrentSecretKey FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onReceivePushSecretKeyResponse(boolean z) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IVerifySecretKeySession) {
            ((IVerifySecretKeySession) currentSession).onReceiveServerResponse(z);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceivePushSecretKeyResponse() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceivePushSecretKeyResponse FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onReceiveServerRandomKey(byte[] bArr, int i) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onReceiveRandomKey(bArr, i);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceiveServerRandomKey() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceiveServerRandomKey FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean onReceiveServerSecretKey(byte[] bArr, int i) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IExchangeKeySession) {
            ((IExchangeKeySession) currentSession).onReceiveServerSecretKey(bArr, i);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onReceiveServerSecretKey() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = getSerial();
        String tag2 = getTAG();
        remote.i(component, session, serial, tag2, "onReceiveServerSecretKey FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void onSetWatchParamResponse(String str, boolean z, WatchParamsFileMapping watchParamsFileMapping) {
        Wg6.c(str, "serial");
        BleSession currentSession = getCurrentSession();
        if (!(currentSession instanceof ISetWatchParamStateSession)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, ".onSetWatchParamResponse() FAILED, current session=" + currentSession);
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.BLE;
            FLogger.Session session = FLogger.Session.OTHER;
            String tag2 = getTAG();
            remote.i(component, session, str, tag2, "onSetWatchParamResponse FAILED, currentSession=" + currentSession);
        } else if (!z) {
            ((ISetWatchParamStateSession) currentSession).onGetWatchParamFailed();
        } else if (watchParamsFileMapping == null) {
            ((ISetWatchParamStateSession) currentSession).doNextState();
        } else {
            ((ISetWatchParamStateSession) currentSession).setLatestWatchParam(str, watchParamsFileMapping);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public ThemeData parseBinaryToThemeData(byte[] bArr) {
        Wg6.c(bArr, "byteArray");
        return getBleAdapter().parseBinaryToThemeData(bArr);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean previewTheme(Lw1 lw1, ThemeEditor themeEditor) {
        queueSessionAndStart(new PreviewThemeSession(themeEditor, lw1, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean sendingEncryptedDataSession(byte[] bArr, boolean z) {
        Wg6.c(bArr, "encryptedData");
        SharePreferencesUtils.getInstance(getBleAdapter().getContext()).setBoolean(SharePreferencesUtils.BC_STATUS, z);
        queueSessionAndStart(new SendingEncryptedDataSession(bArr, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean setMinimumStepThresholdSession(long j) {
        queueSessionAndStart(new SetMinimumStepThresholdSession(j, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void setSecretKey(byte[] bArr) {
        BleAdapterImpl mBleAdapter = getMBleAdapter();
        FLogger.Session session = FLogger.Session.OTHER;
        if (bArr != null) {
            mBleAdapter.setSecretKey(session, bArr);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean setWorkoutConfigSession(WorkoutConfigData workoutConfigData) {
        Wg6.c(workoutConfigData, "workoutConfigData");
        queueSessionAndStart(new SetWorkoutConfigSession(workoutConfigData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean setWorkoutGPSData(Location location) {
        Wg6.c(location, PlaceFields.LOCATION);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "device with serial = " + getBleAdapter().getSerial() + " .setWorkoutGPSData() = " + location.toString());
        getBleAdapter().setWorkoutGPSData(location, FLogger.Session.WORKOUT_TETHER_GPS);
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startCalibrationSession() {
        queueSessionAndStart(new CalibrationDeviceSession(getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startCleanLinkMappingSession(List<? extends BLEMapping> list) {
        Wg6.c(list, "mappings");
        return queueSessionAndStart(new ClearLinkMappingSession(list, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startConnectionDeviceSession(boolean z, UserProfile userProfile) {
        queueSessionAndStart(new ConnectDeviceSession(z, userProfile, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startInstallWatchApps(boolean z) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof ISetWatchAppFileSession) {
            ((ISetWatchAppFileSession) currentSession).onWatchAppFilesReady(z);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, ".onWatchAppFilesReady() FAILED, current session=" + currentSession);
        return false;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startNotifyNotificationEvent(NotificationBaseObj notificationBaseObj) {
        Wg6.c(notificationBaseObj, "notifyNotificationEvent");
        queueSessionAndStart(new NotifyNotificationEventSession(notificationBaseObj, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startOtaSession(FirmwareData firmwareData, UserProfile userProfile) {
        Wg6.c(firmwareData, "firmwareData");
        Wg6.c(userProfile, "userProfile");
        queueSessionAndStart(new OtaSession(firmwareData, userProfile, getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startPairingSession(UserProfile userProfile) {
        Wg6.c(userProfile, "userProfile");
        queueSessionAndStart(new PairingNewDeviceSession(userProfile, getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startReadCurrentWorkoutSession() {
        queueSessionAndStart(new ReadCurrentWorkoutSessionSession(getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void startSendDeviceAppResponse(DeviceAppResponse deviceAppResponse, boolean z) {
        Wg6.c(deviceAppResponse, "deviceAppResponse");
        deviceAppResponse.getLifeTimeObject().startExpireTimeCountDown();
        deviceAppResponse.setForceUpdate(z);
        addToQuickCommandQueue(deviceAppResponse);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public void startSendMusicAppResponse(MusicResponse musicResponse) {
        synchronized (this) {
            Wg6.c(musicResponse, "musicResponse");
            addToQuickCommandQueue(musicResponse);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSendNotification(NotificationBaseObj notificationBaseObj) {
        Wg6.c(notificationBaseObj, "newNotification");
        sendDianaNotification(notificationBaseObj);
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoBackgroundImageConfig(BackgroundConfig backgroundConfig) {
        Wg6.c(backgroundConfig, "backgroundConfig");
        queueSessionAndStart(new SetAutoBackgroundImageConfigSession(backgroundConfig, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoBiometricData(UserBiometricData userBiometricData) {
        Wg6.c(userBiometricData, "userBiometricData");
        queueSessionAndStart(new SetAutoBiometricDataSession(userBiometricData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoMapping(List<? extends BLEMapping> list) {
        Wg6.c(list, "mappings");
        return queueSessionAndStart(new SetAutoMappingsSession(list, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoMultiAlarms(List<AlarmSetting> list) {
        Wg6.c(list, "multipleAlarmList");
        queueSessionAndStart(new SetAutoMultiAlarmsSession(list, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings) {
        Wg6.c(appNotificationFilterSettings, "notificationFilterSettings");
        queueSessionAndStart(new SetAutoNotificationFiltersConfigSession(appNotificationFilterSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoSecondTimezone(String str) {
        Wg6.c(str, "secondTimezoneId");
        return queueSessionAndStart(new SetAutoSecondTimezoneSession(str, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetAutoWatchApps(WatchAppMappingSettings watchAppMappingSettings) {
        Wg6.c(watchAppMappingSettings, "watchAppMappingSettings");
        queueSessionAndStart(new SetAutoWatchAppsSession(watchAppMappingSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetBackgroundImageConfig(BackgroundConfig backgroundConfig) {
        Wg6.c(backgroundConfig, "backgroundConfig");
        queueSessionAndStart(new SetBackgroundImageConfigSession(backgroundConfig, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetFrontLightEnable(boolean z) {
        queueSessionAndStart(new SetFrontLightEnableSession(z, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetHeartRateMode(HeartRateMode heartRateMode) {
        Wg6.c(heartRateMode, "heartRateMode");
        queueSessionAndStart(new SetHeartRateModeSession(heartRateMode, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetImplicitDeviceConfig(UserProfile userProfile) {
        Wg6.c(userProfile, "userProfile");
        queueSessionAndStart(new SetImplicitDeviceConfigSession(userProfile, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetImplicitDisplayUnitSettings(UserDisplayUnit userDisplayUnit) {
        Wg6.c(userDisplayUnit, "userDisplayUnit");
        queueSessionAndStart(new SetImplicitDisplayUnitSession(userDisplayUnit, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetInactiveNudgeConfigSession(InactiveNudgeData inactiveNudgeData) {
        Wg6.c(inactiveNudgeData, "inactiveNudgeData");
        queueSessionAndStart(new SetInactiveNudgeConfigSession(inactiveNudgeData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetLinkMappingSession(List<? extends BLEMapping> list) {
        Wg6.c(list, "mappings");
        return queueSessionAndStart(new SetLinkMappingsSession(list, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetLocalizationData(LocalizationData localizationData) {
        Wg6.c(localizationData, "localizationData");
        queueSessionAndStart(new SetAutoLocalizationDataSession(localizationData, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleCommunicatorAbs, com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetMultipleAlarmsSession(List<AlarmSetting> list) {
        Wg6.c(list, "multipleAlarmList");
        queueSessionAndStart(new SetMultiAlarmsSession(getBleAdapter(), list, getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings) {
        Wg6.c(appNotificationFilterSettings, "notificationFilterSettings");
        queueSessionAndStart(new SetNotificationFiltersConfigSession(appNotificationFilterSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetReplyMessageMappingSettings(ReplyMessageMappingGroup replyMessageMappingGroup) {
        Wg6.c(replyMessageMappingGroup, "replyMessageMappings");
        queueSessionAndStart(new SetReplyMessageMappingSession(replyMessageMappingGroup, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetSecondTimezoneSession(String str) {
        Wg6.c(str, "secondTimezoneId");
        return queueSessionAndStart(new SetSecondTimezoneSession(str, getBleAdapter(), getBleSessionCallback()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSetWatchApps(WatchAppMappingSettings watchAppMappingSettings) {
        Wg6.c(watchAppMappingSettings, "watchAppMappingSettings");
        queueSessionAndStart(new SetWatchAppsSession(watchAppMappingSettings, getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startStopCurrentWorkoutSession() {
        queueSessionAndStart(new StopCurrentWorkoutSessionSession(getBleAdapter(), getBleSessionCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSwitchDeviceSession(UserProfile userProfile) {
        Wg6.c(userProfile, "userProfile");
        queueSessionAndStart(new SwitchActiveDeviceSession(userProfile, getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback()));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startSyncingSession(UserProfile userProfile) {
        Wg6.c(userProfile, "userProfile");
        queueSessionAndStart(new SyncSession(getBleAdapter(), getBleSessionCallback(), userProfile));
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator
    public boolean startVerifySecretKeySession() {
        return queueSessionAndStart(new VerifySecretKeySession(getBleAdapter(), getBleSessionCallback()));
    }
}
