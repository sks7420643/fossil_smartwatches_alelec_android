package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.Qq7;
import com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$previewTheme$$inlined$let$lambda$Anon5 extends Qq7 implements Hg6<Float, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThemeEditor $themeEditor$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$previewTheme$$inlined$let$lambda$Anon5(BleAdapterImpl bleAdapterImpl, ThemeEditor themeEditor, ISessionSdkCallback iSessionSdkCallback, FLogger.Session session) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$themeEditor$inlined = themeEditor;
        this.$callback$inlined = iSessionSdkCallback;
        this.$logSession$inlined = session;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Float f) {
        invoke(f.floatValue());
        return Cd6.a;
    }

    @DexIgnore
    public final void invoke(float f) {
        this.$callback$inlined.onPreviewThemeProgressChanged(f);
    }
}
