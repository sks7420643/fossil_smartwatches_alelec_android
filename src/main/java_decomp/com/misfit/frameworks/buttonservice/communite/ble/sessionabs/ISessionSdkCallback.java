package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.Uk1;
import com.fossil.Yx1;
import com.fossil.Zk1;
import com.fossil.Zm1;
import com.fossil.fitness.FitnessData;
import com.mapped.C90;
import com.mapped.Q40;
import com.mapped.R60;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ISessionSdkCallback {
    @DexIgnore
    void onApplyHandPositionFailed(Yx1 yx1);

    @DexIgnore
    Object onApplyHandPositionSuccess();  // void declaration

    @DexIgnore
    void onApplyThemeError(Yx1 yx1);

    @DexIgnore
    void onApplyThemeProgressChanged(float f);

    @DexIgnore
    void onApplyThemeSuccess(byte[] bArr);

    @DexIgnore
    void onAuthenticateDeviceFail(Yx1 yx1);

    @DexIgnore
    void onAuthenticateDeviceSuccess(byte[] bArr);

    @DexIgnore
    Object onAuthorizeDeviceFailed();  // void declaration

    @DexIgnore
    Object onAuthorizeDeviceSuccess();  // void declaration

    @DexIgnore
    void onConfigureMicroAppFail(Yx1 yx1);

    @DexIgnore
    Object onConfigureMicroAppSuccess();  // void declaration

    @DexIgnore
    Object onDataTransferCompleted();  // void declaration

    @DexIgnore
    void onDataTransferFailed(Yx1 yx1);

    @DexIgnore
    void onDataTransferProgressChange(float f);

    @DexIgnore
    void onDeviceFound(Q40 q40, int i);

    @DexIgnore
    void onEraseDataFilesFailed(Yx1 yx1);

    @DexIgnore
    Object onEraseDataFilesSuccess();  // void declaration

    @DexIgnore
    void onEraseHWLogFailed(Yx1 yx1);

    @DexIgnore
    Object onEraseHWLogSuccess();  // void declaration

    @DexIgnore
    void onExchangeSecretKeyFail(Yx1 yx1);

    @DexIgnore
    void onExchangeSecretKeySuccess(byte[] bArr);

    @DexIgnore
    void onFetchDeviceInfoFailed(Yx1 yx1);

    @DexIgnore
    void onFetchDeviceInfoSuccess(Zk1 zk1);

    @DexIgnore
    void onGetDeviceConfigFailed(Yx1 yx1);

    @DexIgnore
    void onGetDeviceConfigSuccess(HashMap<Zm1, R60> hashMap);

    @DexIgnore
    Object onGetWatchParamsFail();  // void declaration

    @DexIgnore
    void onMoveHandFailed(Yx1 yx1);

    @DexIgnore
    Object onMoveHandSuccess();  // void declaration

    @DexIgnore
    Object onNextSession();  // void declaration

    @DexIgnore
    void onNotifyNotificationEventFailed(Yx1 yx1);

    @DexIgnore
    Object onNotifyNotificationEventSuccess();  // void declaration

    @DexIgnore
    void onPlayAnimationFail(Yx1 yx1);

    @DexIgnore
    Object onPlayAnimationSuccess();  // void declaration

    @DexIgnore
    void onPlayDeviceAnimation(boolean z, Yx1 yx1);

    @DexIgnore
    void onPreviewThemeError(Yx1 yx1);

    @DexIgnore
    void onPreviewThemeProgressChanged(float f);

    @DexIgnore
    Object onPreviewThemeSuccess();  // void declaration

    @DexIgnore
    void onReadCurrentWorkoutSessionFailed(Yx1 yx1);

    @DexIgnore
    void onReadCurrentWorkoutSessionSuccess(C90 c90);

    @DexIgnore
    void onReadDataFilesFailed(Yx1 yx1);

    @DexIgnore
    void onReadDataFilesProgressChanged(float f);

    @DexIgnore
    void onReadDataFilesSuccess(FitnessData[] fitnessDataArr);

    @DexIgnore
    void onReadHWLogFailed(Yx1 yx1);

    @DexIgnore
    void onReadHWLogProgressChanged(float f);

    @DexIgnore
    Object onReadHWLogSuccess();  // void declaration

    @DexIgnore
    void onReadRssiFailed(Yx1 yx1);

    @DexIgnore
    void onReadRssiSuccess(int i);

    @DexIgnore
    void onReleaseHandControlFailed(Yx1 yx1);

    @DexIgnore
    Object onReleaseHandControlSuccess();  // void declaration

    @DexIgnore
    void onRequestHandControlFailed(Yx1 yx1);

    @DexIgnore
    Object onRequestHandControlSuccess();  // void declaration

    @DexIgnore
    void onResetHandsFailed(Yx1 yx1);

    @DexIgnore
    Object onResetHandsSuccess();  // void declaration

    @DexIgnore
    void onScanFail(Uk1 uk1);

    @DexIgnore
    void onSendMicroAppDataFail(Yx1 yx1);

    @DexIgnore
    Object onSendMicroAppDataSuccess();  // void declaration

    @DexIgnore
    void onSendingEncryptedDataSessionFailed(Yx1 yx1);

    @DexIgnore
    Object onSendingEncryptedDataSessionSuccess();  // void declaration

    @DexIgnore
    void onSetAlarmFailed(Yx1 yx1);

    @DexIgnore
    Object onSetAlarmSuccess();  // void declaration

    @DexIgnore
    void onSetBackgroundImageFailed(Yx1 yx1);

    @DexIgnore
    Object onSetBackgroundImageSuccess();  // void declaration

    @DexIgnore
    void onSetComplicationFailed(Yx1 yx1);

    @DexIgnore
    Object onSetComplicationSuccess();  // void declaration

    @DexIgnore
    void onSetDeviceConfigFailed(Yx1 yx1);

    @DexIgnore
    Object onSetDeviceConfigSuccess();  // void declaration

    @DexIgnore
    void onSetFrontLightFailed(Yx1 yx1);

    @DexIgnore
    Object onSetFrontLightSuccess();  // void declaration

    @DexIgnore
    void onSetLabelFileFailed(Yx1 yx1);

    @DexIgnore
    Object onSetLabelFileSuccess();  // void declaration

    @DexIgnore
    void onSetLocalizationDataFail(Yx1 yx1);

    @DexIgnore
    Object onSetLocalizationDataSuccess();  // void declaration

    @DexIgnore
    void onSetMinimumStepThresholdSessionFailed(Yx1 yx1);

    @DexIgnore
    Object onSetMinimumStepThresholdSessionSuccess();  // void declaration

    @DexIgnore
    void onSetNotificationFilterFailed(Yx1 yx1);

    @DexIgnore
    void onSetNotificationFilterProgressChanged(float f);

    @DexIgnore
    Object onSetNotificationFilterSuccess();  // void declaration

    @DexIgnore
    void onSetReplyMessageMappingError(Yx1 yx1);

    @DexIgnore
    Object onSetReplyMessageMappingSuccess();  // void declaration

    @DexIgnore
    void onSetWatchAppFailed(Yx1 yx1);

    @DexIgnore
    Object onSetWatchAppFileFailed();  // void declaration

    @DexIgnore
    void onSetWatchAppFileProgressChanged(float f);

    @DexIgnore
    Object onSetWatchAppFileSuccess();  // void declaration

    @DexIgnore
    Object onSetWatchAppSuccess();  // void declaration

    @DexIgnore
    void onSetWatchParamsFail(Yx1 yx1);

    @DexIgnore
    Object onSetWatchParamsSuccess();  // void declaration

    @DexIgnore
    void onSetWorkoutGPSDataSessionFailed(Yx1 yx1);

    @DexIgnore
    Object onSetWorkoutGPSDataSessionSuccess();  // void declaration

    @DexIgnore
    void onStopCurrentWorkoutSessionFailed(Yx1 yx1);

    @DexIgnore
    Object onStopCurrentWorkoutSessionSuccess();  // void declaration

    @DexIgnore
    void onVerifySecretKeyFail(Yx1 yx1);

    @DexIgnore
    void onVerifySecretKeySuccess(boolean z);
}
