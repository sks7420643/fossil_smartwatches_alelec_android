package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.Ts1;
import com.fossil.Us1;
import com.fossil.Yx1;
import com.fossil.Zk1;
import com.mapped.Q40;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseSwitchActiveDeviceSession extends EnableMaintainingSession {
    @DexIgnore
    public /* final */ BleCommunicator.CommunicationResultCallback communicationResultCallback;
    @DexIgnore
    public /* final */ UserProfile userProfile;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class FetchDeviceInfoState extends BleStateAbs {
        @DexIgnore
        public Zb0<Zk1> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public FetchDeviceInfoState() {
            super(BaseSwitchActiveDeviceSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            Zb0<Zk1> fetchDeviceInfo = BaseSwitchActiveDeviceSession.this.getBleAdapter().fetchDeviceInfo(BaseSwitchActiveDeviceSession.this.getLogSession(), this);
            this.task = fetchDeviceInfo;
            if (fetchDeviceInfo == null) {
                BaseSwitchActiveDeviceSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onFetchDeviceInfoFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            Q40 deviceObj = BaseSwitchActiveDeviceSession.this.getBleAdapter().getDeviceObj();
            if ((deviceObj != null ? deviceObj.getState() : null) != Q40.Ci.CONNECTED) {
                BaseSwitchActiveDeviceSession.this.stop(FailureCode.FAILED_TO_CONNECT);
            } else if (!retry(BaseSwitchActiveDeviceSession.this.getBleAdapter().getContext(), BaseSwitchActiveDeviceSession.this.getSerial())) {
                BaseSwitchActiveDeviceSession.this.log("Reach the limit retry. Stop.");
                BaseSwitchActiveDeviceSession.this.stop(FailureCode.FAILED_TO_CONNECT);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onFetchDeviceInfoSuccess(Zk1 zk1) {
            Wg6.c(zk1, "deviceInformation");
            stopTimeout();
            if (BaseSwitchActiveDeviceSession.this.getBleAdapter().isSupportedFeature(Ts1.class) != null) {
                BaseSwitchActiveDeviceSession baseSwitchActiveDeviceSession = BaseSwitchActiveDeviceSession.this;
                baseSwitchActiveDeviceSession.enterStateAsync(baseSwitchActiveDeviceSession.createConcreteState(BleSessionAbs.SessionState.VERIFY_SECRET_KEY));
                return;
            }
            BaseSwitchActiveDeviceSession baseSwitchActiveDeviceSession2 = BaseSwitchActiveDeviceSession.this;
            baseSwitchActiveDeviceSession2.enterStateAsync(baseSwitchActiveDeviceSession2.createConcreteState(BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Zk1> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseSwitchActiveDeviceSession(UserProfile userProfile2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback, BleCommunicator.CommunicationResultCallback communicationResultCallback2) {
        super(SessionType.UI, CommunicateMode.SWITCH_DEVICE, bleAdapterImpl, bleSessionCallback);
        Wg6.c(userProfile2, "userProfile");
        Wg6.c(bleAdapterImpl, "bleAdapter");
        this.userProfile = userProfile2;
        this.communicationResultCallback = communicationResultCallback2;
        setSkipEnableMaintainingConnection(true);
        setLogSession(FLogger.Session.OTHER);
        this.userProfile.setNewDevice(true);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(getBleAdapter()));
    }

    @DexIgnore
    public final BleCommunicator.CommunicationResultCallback getCommunicationResultCallback() {
        return this.communicationResultCallback;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE);
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE;
        String name = FetchDeviceInfoState.class.getName();
        Wg6.b(name, "FetchDeviceInfoState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
