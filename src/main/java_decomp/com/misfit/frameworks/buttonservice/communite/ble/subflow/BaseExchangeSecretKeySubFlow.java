package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import android.os.Bundle;
import android.util.Base64;
import com.fossil.Us1;
import com.fossil.Yx1;
import com.mapped.U40;
import com.mapped.Wg6;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseExchangeSecretKeySubFlow extends SubFlow {
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public /* final */ CommunicateMode communicateMode;
    @DexIgnore
    public /* final */ BleAdapterImpl mBleAdapterV2;
    @DexIgnore
    public byte[] mRandomKey;
    @DexIgnore
    public /* final */ MFLog mflog;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class AuthenticateDeviceSessionState extends BleStateAbs {
        @DexIgnore
        public Zb0<byte[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public AuthenticateDeviceSessionState() {
            super(BaseExchangeSecretKeySubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onAuthenticateDeviceFail(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            int code = yx1.getErrorCode().getCode();
            if (code == U40.REQUEST_UNSUPPORTED.getCode()) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(U40.REQUEST_UNSUPPORTED.getCode());
            } else if (code == U40.UNSUPPORTED_FORMAT.getCode()) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(U40.UNSUPPORTED_FORMAT.getCode());
            } else {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(1217);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onAuthenticateDeviceSuccess(byte[] bArr) {
            Wg6.c(bArr, "randomKey");
            stopTimeout();
            BleSession.BleSessionCallback bleSessionCallback = BaseExchangeSecretKeySubFlow.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(BaseExchangeSecretKeySubFlow.this.getBleAdapter()));
                bundle.putString(ButtonService.DEVICE_RANDOM_KEY, Base64.encodeToString(bArr, 2));
                bleSessionCallback.onAskForSecretKey(bundle);
            }
            BaseExchangeSecretKeySubFlow baseExchangeSecretKeySubFlow = BaseExchangeSecretKeySubFlow.this;
            baseExchangeSecretKeySubFlow.enterSubStateAsync(baseExchangeSecretKeySubFlow.createConcreteState(SubFlow.SessionState.EXCHANGE_SECRET_KEY));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "onEnter AuthenticateDeviceSessionState " + BaseExchangeSecretKeySubFlow.this.mRandomKey);
            if (BaseExchangeSecretKeySubFlow.this.mRandomKey != null) {
                BleAdapterImpl mBleAdapterV2 = BaseExchangeSecretKeySubFlow.this.getMBleAdapterV2();
                FLogger.Session logSession = BaseExchangeSecretKeySubFlow.this.getLogSession();
                byte[] bArr = BaseExchangeSecretKeySubFlow.this.mRandomKey;
                if (bArr != null) {
                    Zb0<byte[]> startAuthenticate = mBleAdapterV2.startAuthenticate(logSession, bArr, this);
                    this.task = startAuthenticate;
                    if (startAuthenticate == null) {
                        BaseExchangeSecretKeySubFlow.this.stopSubFlow(10000);
                        return true;
                    }
                    startTimeout();
                    return true;
                }
                Wg6.i();
                throw null;
            }
            BaseExchangeSecretKeySubFlow.this.errorLog("AuthenticateDeviceSession: no random key", FLogger.Component.BLE, ErrorCodeBuilder.Step.START_AUTHEN, ErrorCodeBuilder.AppError.UNKNOWN);
            BaseExchangeSecretKeySubFlow.this.stopSubFlow(FailureCode.FAIL_TO_GET_RANDOM_KEY);
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            FLogger.INSTANCE.getLocal().d(getTAG(), "onTimeout AuthenticateDeviceSessionState");
            Zb0<byte[]> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ExchangeSecretKeySessionState extends BleStateAbs {
        @DexIgnore
        public Zb0<byte[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ExchangeSecretKeySessionState() {
            super(BaseExchangeSecretKeySubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onExchangeSecretKeyFail(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            int code = yx1.getErrorCode().getCode();
            if (code == U40.REQUEST_UNSUPPORTED.getCode()) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(U40.REQUEST_UNSUPPORTED.getCode());
            } else if (code == U40.UNSUPPORTED_FORMAT.getCode()) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(U40.UNSUPPORTED_FORMAT.getCode());
            } else {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(1218);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onExchangeSecretKeySuccess(byte[] bArr) {
            Wg6.c(bArr, "secretKey");
            stopTimeout();
            BleSession.BleSessionCallback bleSessionCallback = BaseExchangeSecretKeySubFlow.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                String serial = BaseExchangeSecretKeySubFlow.this.getSerial();
                String encodeToString = Base64.encodeToString(bArr, 2);
                Wg6.b(encodeToString, "Base64.encodeToString(secretKey, Base64.NO_WRAP)");
                bleSessionCallback.broadcastExchangeSecretKeySuccess(serial, encodeToString);
            }
            BaseExchangeSecretKeySubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        public final void onReceiveSecretKey(byte[] bArr, int i) {
            if (i != 0 || bArr == null) {
                BaseExchangeSecretKeySubFlow baseExchangeSecretKeySubFlow = BaseExchangeSecretKeySubFlow.this;
                baseExchangeSecretKeySubFlow.errorLog("ExchangeSecretKeySession: server errorCode=" + i, FLogger.Component.API, ErrorCodeBuilder.Step.EXCHANGE_SECRET_KEY, ErrorCodeBuilder.AppError.UNKNOWN);
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(i);
                return;
            }
            Zb0<byte[]> exchangeSecretKey = BaseExchangeSecretKeySubFlow.this.getBleAdapter().exchangeSecretKey(BaseExchangeSecretKeySubFlow.this.getLogSession(), bArr, this);
            this.task = exchangeSecretKey;
            if (exchangeSecretKey == null) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(10000);
            } else {
                startTimeout();
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            FLogger.INSTANCE.getLocal().d(getTAG(), "onTimeout ExchangeSecretKeySessionState");
            Zb0<byte[]> zb0 = this.task;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GenerateRandomKeySessionState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public GenerateRandomKeySessionState() {
            super(BaseExchangeSecretKeySubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            FLogger.INSTANCE.getLocal().d(getTAG(), "onEnter GenerateRandomKeySessionState");
            if (BaseExchangeSecretKeySubFlow.this.getBleSessionCallback() != null) {
                BaseExchangeSecretKeySubFlow.this.getBleSessionCallback().onAskForRandomKey(BaseExchangeSecretKeySubFlow.this.getSerial());
                return true;
            }
            BaseExchangeSecretKeySubFlow.this.errorLog("GenerateRandomKeySession: No callback", FLogger.Component.BLE, ErrorCodeBuilder.Step.GENERATE_PAIRING_KEY, ErrorCodeBuilder.AppError.UNKNOWN);
            BaseExchangeSecretKeySubFlow.this.stopSubFlow(FailureCode.FAIL_TO_GET_RANDOM_KEY);
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            FLogger.INSTANCE.getLocal().d(getTAG(), "onTimeout GenerateRandomKeySessionState");
            BaseExchangeSecretKeySubFlow.this.errorLog("GenerateRandomKeySession: timeout", FLogger.Component.BLE, ErrorCodeBuilder.Step.GENERATE_PAIRING_KEY, ErrorCodeBuilder.AppError.UNKNOWN);
            BaseExchangeSecretKeySubFlow.this.stopSubFlow(FailureCode.FAIL_TO_GET_RANDOM_KEY);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseExchangeSecretKeySubFlow(CommunicateMode communicateMode2, String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback2) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        Wg6.c(communicateMode2, "communicateMode");
        Wg6.c(str, "tagName");
        Wg6.c(bleSession, "bleSession");
        Wg6.c(session, "logSession");
        Wg6.c(str2, "serial");
        Wg6.c(bleAdapterImpl, "mBleAdapterV2");
        this.communicateMode = communicateMode2;
        this.mflog = mFLog;
        this.mBleAdapterV2 = bleAdapterImpl;
        this.bleSessionCallback = bleSessionCallback2;
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final CommunicateMode getCommunicateMode() {
        return this.communicateMode;
    }

    @DexIgnore
    public final BleAdapterImpl getMBleAdapterV2() {
        return this.mBleAdapterV2;
    }

    @DexIgnore
    public final MFLog getMflog() {
        return this.mflog;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.GENERATE_RANDOM_KEY;
        String name = GenerateRandomKeySessionState.class.getName();
        Wg6.b(name, "GenerateRandomKeySessionState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.AUTHENTICATE_DEVICE;
        String name2 = AuthenticateDeviceSessionState.class.getName();
        Wg6.b(name2, "AuthenticateDeviceSessionState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<SubFlow.SessionState, String> sessionStateMap3 = getSessionStateMap();
        SubFlow.SessionState sessionState3 = SubFlow.SessionState.EXCHANGE_SECRET_KEY;
        String name3 = ExchangeSecretKeySessionState.class.getName();
        Wg6.b(name3, "ExchangeSecretKeySessionState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.GENERATE_RANDOM_KEY));
        return true;
    }

    @DexIgnore
    public final void onReceiveRandomKey(byte[] bArr, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveRandomKey failureCode " + i + " randomKey " + bArr + " state " + getMCurrentState());
        if (i == 0) {
            this.mRandomKey = bArr;
            enterSubStateAsync(createConcreteState(SubFlow.SessionState.AUTHENTICATE_DEVICE));
            return;
        }
        errorLog("GenerateRandomKeySession: server errorCode=" + i, FLogger.Component.API, ErrorCodeBuilder.Step.GENERATE_PAIRING_KEY, ErrorCodeBuilder.AppError.UNKNOWN);
        stopSubFlow(i);
    }

    @DexIgnore
    public final void onReceiveServerSecretKey(byte[] bArr, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerSecretKey failureCode " + i + " state " + getMCurrentState());
        BleStateAbs mCurrentState = getMCurrentState();
        if (mCurrentState instanceof ExchangeSecretKeySessionState) {
            ((ExchangeSecretKeySessionState) mCurrentState).onReceiveSecretKey(bArr, i);
        }
    }
}
