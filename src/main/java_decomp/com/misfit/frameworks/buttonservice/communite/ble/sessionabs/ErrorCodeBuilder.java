package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.Wt7;
import com.fossil.Yx1;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ErrorCodeBuilder {
    @DexIgnore
    public static /* final */ ErrorCodeBuilder INSTANCE; // = new ErrorCodeBuilder();

    @DexIgnore
    public enum AppError {
        BLUETOOTH_DISABLED("001"),
        LOCATION_SERVICE_DISABLED("002"),
        LOCATION_ACCESS_DENIED("003"),
        FILE_NOT_READY("004"),
        FIRMWARE_NOT_MATCH("005"),
        COMMAND_TIME_OUT("006"),
        USER_CANCELLED("007"),
        SESSION_INTERRUPT("008"),
        APP_KILLED("009"),
        BUTTON_SERVICE_CRASHED("010"),
        APP_LAYER_CRASHED("011"),
        NETWORK_ERROR("012"),
        SERVER_MAINTENANCE("013"),
        BIOMETRIC_BUILD_FAILED("014"),
        DEVICE_NOT_FOUND("015"),
        GET_TEMPERATURE_FAILED("021"),
        COMMUTE_INFO_NOT_AVAILABLE("022"),
        NOT_FETCH_CURRENT_LOCATION("030"),
        UNKNOWN("999");
        
        @DexIgnore
        public /* final */ String value;

        @DexIgnore
        public AppError(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public enum Component {
        APP("1"),
        SDK("2"),
        UNKNOWN(CrashDumperPlugin.OPTION_KILL_DEFAULT);
        
        @DexIgnore
        public /* final */ String value;

        @DexIgnore
        public Component(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public enum Step {
        START_SCAN("001", "start scan"),
        STOP_SCAN("002", "stop scan"),
        RETRIEVE_DEVICE_BY_SERIAL("003", "retrieve device by serial"),
        ENABLE_MAINTAINING_CONNECTION("004", "enable maintaining connection"),
        DISCONNECT("005", "disconnect"),
        FETCH_DEVICE_INFORMATION("006", "fetch device information"),
        PLAY_ANIMATION("007", "play animation"),
        READ_RSSI("008", "read rssi"),
        OTA("009", Constants.OTA),
        SYNC("010", "sync"),
        CLEAN_DEVICE("011", "clean device"),
        GET_CONFIG("012", "get config"),
        SET_CONFIG("013", "set config"),
        SET_COMPLICATION("014", "set complication"),
        SET_WATCH_APP("015", "set watch app"),
        SET_ALARM("016", "set alarm"),
        SEND_RESPOND("017", "send respond"),
        SET_NOTIFICATION_FILTER("018", "set notification filter"),
        SEND_APP_NOTIFICATION("019", "send app notification"),
        REQUEST_HAND("020", "request hand"),
        MOVE_HAND("021", "move hand"),
        RELEASE_HAND("022", "release hand"),
        SET_CALIBRATION_POSITION("023", "set calibration position"),
        SEND_TRACK_INFO("024", "send track info"),
        NOTIFY_MUSIC_EVENT("025", "notify music event"),
        LINK_DEVICE("026", "link device"),
        READ_WORKOUT("027", "read workout"),
        STOP_WORKOUT("028", "stop workout"),
        RETRIEVE_DEVICE_REQUEST_FROM_OUTSIDER("029", "retrieve device request from outsider"),
        GET_ALARM("030", "get alarm"),
        SET_BACKGROUND_IMAGE("031", "set background image"),
        SET_FRONT_LIGHT_ENABLE("032", "set front light enable"),
        ERASE_DATA("033", "erase data"),
        GET_NOTIFICATION_FILTER("034", "get notification filter"),
        GET_WORKOUT_SESSION("035", "get workout session"),
        STOP_WORKOUT_SESSION("036", "stop workout session"),
        GENERATE_PAIRING_KEY("037", "generate pairing key"),
        START_AUTHEN("038", "start authen"),
        SWAP_PAIRING_KEY("039", "swap pairing key"),
        EXCHANGE_SECRET_KEY("040", "exchange secret key"),
        STORE_KEY_TO_CLOUD("041", "store key to cloud"),
        SET_MICRO_APP("042", "set micro app"),
        SETUP("043", "setup"),
        SET_LOCALIZATION("044", "set localization"),
        ENABLE_HID_CONNECTION("045", "enable HID connection"),
        VERIFY_SECRET_KEY("046", "verify secret key"),
        SET_WATCH_PARAM("047", "set watch param"),
        SET_MINIMUM_STEP_THRESHOLD("048", "set minimum step threshold"),
        SENDING_ENCRYPTED_DATA("049", "sending encryted data"),
        SYNC_CURRENT_DEVICE("052", "sync current device"),
        AUTHORIZE_DEVICE("053", "authorize device"),
        SET_REPLY_MESSAGE_MAPPING("054", "reply message mapping"),
        SET_WATCH_APP_FILE("055", "set watch app files"),
        SET_WORKOUT_GPS_DATA("056", "set workout gps data"),
        SET_LABEL_FILE("057", "set label file"),
        PREVIEW_THEME("058", "preview_theme"),
        APPLY_THEME("059", "apply_theme"),
        UNKNOWN("999", "unknown");
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);
        @DexIgnore
        public /* final */ String nameValue;
        @DexIgnore
        public /* final */ String value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public /* synthetic */ Companion(Qg6 qg6) {
                this();
            }

            @DexIgnore
            public final Step fromValue(String str) {
                Wg6.c(str, "value");
                return Wg6.a(str, Step.START_SCAN.getValue()) ? Step.START_SCAN : Wg6.a(str, Step.STOP_SCAN.getValue()) ? Step.STOP_SCAN : Wg6.a(str, Step.RETRIEVE_DEVICE_BY_SERIAL.getValue()) ? Step.RETRIEVE_DEVICE_BY_SERIAL : Wg6.a(str, Step.ENABLE_MAINTAINING_CONNECTION.getValue()) ? Step.ENABLE_MAINTAINING_CONNECTION : Wg6.a(str, Step.DISCONNECT.getValue()) ? Step.DISCONNECT : Wg6.a(str, Step.FETCH_DEVICE_INFORMATION.getValue()) ? Step.FETCH_DEVICE_INFORMATION : Wg6.a(str, Step.PLAY_ANIMATION.getValue()) ? Step.PLAY_ANIMATION : Wg6.a(str, Step.READ_RSSI.getValue()) ? Step.READ_RSSI : Wg6.a(str, Step.OTA.getValue()) ? Step.OTA : Wg6.a(str, Step.SYNC.getValue()) ? Step.SYNC : Wg6.a(str, Step.CLEAN_DEVICE.getValue()) ? Step.CLEAN_DEVICE : Wg6.a(str, Step.GET_CONFIG.getValue()) ? Step.GET_CONFIG : Wg6.a(str, Step.SET_CONFIG.getValue()) ? Step.SET_CONFIG : Wg6.a(str, Step.SET_COMPLICATION.getValue()) ? Step.SET_COMPLICATION : Wg6.a(str, Step.SET_WATCH_APP.getValue()) ? Step.SET_WATCH_APP : Wg6.a(str, Step.SET_ALARM.getValue()) ? Step.SET_ALARM : Wg6.a(str, Step.SEND_RESPOND.getValue()) ? Step.SEND_RESPOND : Wg6.a(str, Step.SET_NOTIFICATION_FILTER.getValue()) ? Step.SET_NOTIFICATION_FILTER : Wg6.a(str, Step.SEND_APP_NOTIFICATION.getValue()) ? Step.SEND_APP_NOTIFICATION : Wg6.a(str, Step.REQUEST_HAND.getValue()) ? Step.REQUEST_HAND : Wg6.a(str, Step.MOVE_HAND.getValue()) ? Step.MOVE_HAND : Wg6.a(str, Step.RELEASE_HAND.getValue()) ? Step.RELEASE_HAND : Wg6.a(str, Step.SET_CALIBRATION_POSITION.getValue()) ? Step.SET_CALIBRATION_POSITION : Wg6.a(str, Step.SEND_TRACK_INFO.getValue()) ? Step.SEND_TRACK_INFO : Wg6.a(str, Step.NOTIFY_MUSIC_EVENT.getValue()) ? Step.NOTIFY_MUSIC_EVENT : Wg6.a(str, Step.LINK_DEVICE.getValue()) ? Step.LINK_DEVICE : Wg6.a(str, Step.READ_WORKOUT.getValue()) ? Step.READ_WORKOUT : Wg6.a(str, Step.STOP_WORKOUT.getValue()) ? Step.STOP_WORKOUT : Wg6.a(str, Step.RETRIEVE_DEVICE_REQUEST_FROM_OUTSIDER.getValue()) ? Step.RETRIEVE_DEVICE_REQUEST_FROM_OUTSIDER : Wg6.a(str, Step.SET_BACKGROUND_IMAGE.getValue()) ? Step.SET_BACKGROUND_IMAGE : Wg6.a(str, Step.SET_FRONT_LIGHT_ENABLE.getValue()) ? Step.SET_FRONT_LIGHT_ENABLE : Wg6.a(str, Step.SYNC_CURRENT_DEVICE.getValue()) ? Step.SYNC_CURRENT_DEVICE : Wg6.a(str, Step.UNKNOWN.getValue()) ? Step.UNKNOWN : Step.UNKNOWN;
            }
        }

        @DexIgnore
        public Step(String str, String str2) {
            this.value = str;
            this.nameValue = str2;
        }

        @DexIgnore
        public final String getNameValue() {
            return this.nameValue;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public final String build(Step step, Component component, Yx1 yx1) {
        Wg6.c(step, "step");
        Wg6.c(component, "component");
        Wg6.c(yx1, "sdkError");
        return step.getValue() + component.getValue() + Wt7.O(yx1.getErrorCode().toString(), 3, '0');
    }

    @DexIgnore
    public final String build(Step step, Component component, AppError appError) {
        Wg6.c(step, "step");
        Wg6.c(component, "component");
        Wg6.c(appError, "appError");
        return step.getValue() + component.getValue() + appError.getValue();
    }
}
