package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Us1;
import com.fossil.Vt7;
import com.fossil.Yx1;
import com.fossil.Zk1;
import com.mapped.Q40;
import com.mapped.Wg6;
import com.mapped.Yb0;
import com.mapped.Zb0;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseOTASubFlow extends SubFlow {
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public /* final */ BleCommunicator.CommunicationResultCallback communicationResultCallback;
    @DexIgnore
    public /* final */ byte[] firmwareBytes;
    @DexIgnore
    public /* final */ FirmwareData firmwareData;
    @DexIgnore
    public /* final */ MFLog mflog;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class FetchDeviceInfoState extends BleStateAbs {
        @DexIgnore
        public Zb0<Zk1> mFetchDeviceInfoTask;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public FetchDeviceInfoState() {
            super(BaseOTASubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (Vt7.l(BaseOTASubFlow.this.getBleAdapter().getFirmwareVersion())) {
                Zb0<Zk1> fetchDeviceInfo = BaseOTASubFlow.this.getBleAdapter().fetchDeviceInfo(BaseOTASubFlow.this.getLogSession(), this);
                this.mFetchDeviceInfoTask = fetchDeviceInfo;
                if (fetchDeviceInfo == null) {
                    BaseOTASubFlow.this.stopSubFlow(10000);
                    return true;
                }
                startTimeout();
                return true;
            }
            BaseOTASubFlow baseOTASubFlow = BaseOTASubFlow.this;
            baseOTASubFlow.enterSubStateAsync(baseOTASubFlow.createConcreteState(SubFlow.SessionState.OTA_STATE));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onFetchDeviceInfoFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            stopTimeout();
            Q40 deviceObj = BaseOTASubFlow.this.getBleAdapter().getDeviceObj();
            if ((deviceObj != null ? deviceObj.getState() : null) != Q40.Ci.CONNECTED) {
                BaseOTASubFlow.this.stopSubFlow(FailureCode.FAILED_TO_CONNECT);
            } else if (!retry(BaseOTASubFlow.this.getBleAdapter().getContext(), BaseOTASubFlow.this.getSerial())) {
                BaseOTASubFlow.this.getBleSession().log("Reach the limit retry. Stop.");
                BaseOTASubFlow.this.stopSubFlow(FailureCode.FAILED_TO_CONNECT);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onFetchDeviceInfoSuccess(Zk1 zk1) {
            Wg6.c(zk1, "deviceInformation");
            stopTimeout();
            BaseOTASubFlow baseOTASubFlow = BaseOTASubFlow.this;
            baseOTASubFlow.enterSubStateAsync(baseOTASubFlow.createConcreteState(SubFlow.SessionState.OTA_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Zb0<Zk1> zb0 = this.mFetchDeviceInfoTask;
            if (zb0 != null) {
                Us1.a(zb0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class OTAState extends BleStateAbs {
        @DexIgnore
        public Yb0<String> mOTATask;
        @DexIgnore
        public float previousProgress;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public OTAState() {
            super(BaseOTASubFlow.this.getTAG());
            setMaxRetries(3);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onDataTransferCompleted() {
            if (isExist()) {
                this.mOTATask = null;
                stopTimeout();
                BaseOTASubFlow.this.stopSubFlow(0);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onDataTransferFailed(Yx1 yx1) {
            Wg6.c(yx1, "error");
            if (isExist()) {
                this.mOTATask = null;
                stopTimeout();
                if (!retry(BaseOTASubFlow.this.getBleAdapter().getContext(), BaseOTASubFlow.this.getSerial())) {
                    BaseOTASubFlow.this.log("Reach the limit retry. Stop.");
                    BaseOTASubFlow.this.stopSubFlow(FailureCode.FAILED_TO_OTA);
                }
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onDataTransferProgressChange(float f) {
            if (isExist()) {
                float f2 = f * ((float) 100);
                if (f2 - this.previousProgress >= ((float) 1) || f2 >= 100.0f) {
                    this.previousProgress = f2;
                    setTimeout(f2 >= 100.0f ? 60000 : 30000);
                    startTimeout();
                }
                BleCommunicator.CommunicationResultCallback communicationResultCallback = BaseOTASubFlow.this.getCommunicationResultCallback();
                if (communicationResultCallback != null) {
                    communicationResultCallback.onOtaProgressUpdated(BaseOTASubFlow.this.getSerial(), f2);
                }
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (Vt7.j(BaseOTASubFlow.this.getFirmwareData().getFirmwareVersion(), BaseOTASubFlow.this.getBleAdapter().getFirmwareVersion(), true)) {
                BaseOTASubFlow baseOTASubFlow = BaseOTASubFlow.this;
                baseOTASubFlow.log("Do OTA: Old fw and New fw are same. " + BaseOTASubFlow.this.getBleAdapter().getFirmwareVersion());
                BaseOTASubFlow.this.stopSubFlow(0);
            } else {
                Yb0<String> doOTA = BaseOTASubFlow.this.getBleAdapter().doOTA(BaseOTASubFlow.this.getLogSession(), BaseOTASubFlow.this.getFirmwareBytes(), this);
                this.mOTATask = doOTA;
                if (doOTA == null) {
                    BaseOTASubFlow.this.stopSubFlow(10000);
                } else {
                    startTimeout();
                    this.previousProgress = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
            }
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onExit() {
            super.onExit();
            Yb0<String> yb0 = this.mOTATask;
            if (yb0 != null) {
                Us1.a(yb0);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            Yb0<String> yb0 = this.mOTATask;
            if (yb0 != null) {
                Us1.a(yb0);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseOTASubFlow(String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl, FirmwareData firmwareData2, byte[] bArr, BleSession.BleSessionCallback bleSessionCallback2, BleCommunicator.CommunicationResultCallback communicationResultCallback2) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        Wg6.c(str, "tagName");
        Wg6.c(bleSession, "bleSession");
        Wg6.c(session, "logSession");
        Wg6.c(str2, "serial");
        Wg6.c(bleAdapterImpl, "bleAdapterV2");
        Wg6.c(firmwareData2, "firmwareData");
        Wg6.c(bArr, "firmwareBytes");
        this.mflog = mFLog;
        this.firmwareData = firmwareData2;
        this.firmwareBytes = bArr;
        this.bleSessionCallback = bleSessionCallback2;
        this.communicationResultCallback = communicationResultCallback2;
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final BleCommunicator.CommunicationResultCallback getCommunicationResultCallback() {
        return this.communicationResultCallback;
    }

    @DexIgnore
    public final byte[] getFirmwareBytes() {
        return this.firmwareBytes;
    }

    @DexIgnore
    public final FirmwareData getFirmwareData() {
        return this.firmwareData;
    }

    @DexIgnore
    public final MFLog getMflog() {
        return this.mflog;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.FETCH_DEVICE_INFO_STATE;
        String name = FetchDeviceInfoState.class.getName();
        Wg6.b(name, "FetchDeviceInfoState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.OTA_STATE;
        String name2 = OTAState.class.getName();
        Wg6.b(name2, "OTAState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.FETCH_DEVICE_INFO_STATE));
        return true;
    }
}
