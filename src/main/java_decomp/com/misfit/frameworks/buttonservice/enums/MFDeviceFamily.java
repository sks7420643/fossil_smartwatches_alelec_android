package com.misfit.frameworks.buttonservice.enums;

import com.facebook.internal.AnalyticsEvents;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum MFDeviceFamily {
    UNKNOWN(AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, 0),
    INTEL("Intel", 1),
    DEVICE_FAMILY_Q_MOTION("Q_MOTION", 2),
    DEVICE_FAMILY_RMM("RMM", 3),
    DEVICE_FAMILY_SAM("SAM", 4),
    DEVICE_FAMILY_SAM_SLIM("SAM_SLIM", 5),
    DEVICE_FAMILY_SAM_MINI("MINI", 6),
    DEVICE_FAMILY_SE0("SE0", 7),
    DEVICE_FAMILY_DIANA("HYBRID HR", 8);
    
    @DexIgnore
    public String serverName;
    @DexIgnore
    public int value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Anon1 {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE;

        /*
        static {
            int[] iArr = new int[FossilDeviceSerialPatternUtil.DEVICE.values().length];
            $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE = iArr;
            try {
                iArr[FossilDeviceSerialPatternUtil.DEVICE.RMM.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SAM.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.FAKE_SAM.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SE0.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.DIANA.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
        }
        */
    }

    @DexIgnore
    public MFDeviceFamily(String str, int i) {
        this.value = i;
        this.serverName = str;
    }

    @DexIgnore
    public static MFDeviceFamily fromInt(int i) {
        MFDeviceFamily[] values = values();
        for (MFDeviceFamily mFDeviceFamily : values) {
            if (mFDeviceFamily.getValue() == i) {
                return mFDeviceFamily;
            }
        }
        return UNKNOWN;
    }

    @DexIgnore
    public static MFDeviceFamily fromServerName(String str) {
        MFDeviceFamily[] values = values();
        for (MFDeviceFamily mFDeviceFamily : values) {
            if (mFDeviceFamily.getServerName().equalsIgnoreCase(str)) {
                return mFDeviceFamily;
            }
        }
        return UNKNOWN;
    }

    @DexIgnore
    public static MFDeviceFamily getDeviceFamily(String str) {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.getDeviceBySerial(str).ordinal()]) {
            case 1:
                return DEVICE_FAMILY_RMM;
            case 2:
            case 3:
                return DEVICE_FAMILY_SAM;
            case 4:
                return DEVICE_FAMILY_SE0;
            case 5:
                return DEVICE_FAMILY_SAM_SLIM;
            case 6:
                return DEVICE_FAMILY_SAM_MINI;
            case 7:
                return DEVICE_FAMILY_Q_MOTION;
            case 8:
                return DEVICE_FAMILY_DIANA;
            default:
                return UNKNOWN;
        }
    }

    @DexIgnore
    public static boolean isHybridSmartWatchFamily(MFDeviceFamily mFDeviceFamily) {
        return mFDeviceFamily == DEVICE_FAMILY_SAM || mFDeviceFamily == DEVICE_FAMILY_SAM_SLIM || mFDeviceFamily == DEVICE_FAMILY_SAM_MINI || mFDeviceFamily == DEVICE_FAMILY_SE0;
    }

    @DexIgnore
    public static boolean isSlimOrMiniFamily(MFDeviceFamily mFDeviceFamily) {
        return mFDeviceFamily == DEVICE_FAMILY_SAM_SLIM || mFDeviceFamily == DEVICE_FAMILY_SAM_MINI;
    }

    @DexIgnore
    public String getServerName() {
        return this.serverName;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
