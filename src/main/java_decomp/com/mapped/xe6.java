package com.mapped;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Xe6<T> {
    @DexIgnore
    Af6 getContext();

    @DexIgnore
    void resumeWith(Object obj);
}
