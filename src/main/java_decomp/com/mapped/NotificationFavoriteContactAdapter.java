package com.mapped;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Dd1;
import com.fossil.Ej1;
import com.fossil.Fj1;
import com.fossil.Gb1;
import com.fossil.Hk5;
import com.fossil.Le5;
import com.fossil.Qj1;
import com.fossil.Sj5;
import com.fossil.Tj5;
import com.fossil.Vj5;
import com.fossil.Wc1;
import com.fossil.Wj5;
import com.fossil.Yi1;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilNotificationImageView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationFavoriteContactAdapter extends RecyclerView.g<Bi> {
    @DexIgnore
    public List<ContactGroup> a; // = new ArrayList();
    @DexIgnore
    public Ai b;
    @DexIgnore
    public /* final */ Fj1 c;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(ContactGroup contactGroup);

        @DexIgnore
        Object b();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Le5 a;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationFavoriteContactAdapter b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Ai ai = this.b.b.b;
                if (ai != null) {
                    ai.b();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Bii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Ai ai;
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1 && (ai = this.b.b.b) != null) {
                    ai.a((ContactGroup) this.b.b.a.get(adapterPosition));
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii implements Ej1<Drawable> {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;
            @DexIgnore
            public /* final */ /* synthetic */ ContactGroup c;

            @DexIgnore
            public Cii(Bi bi, ContactGroup contactGroup) {
                this.b = bi;
                this.c = contactGroup;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, Qj1<Drawable> qj1, Gb1 gb1, boolean z) {
                FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "renderContactData onResourceReady");
                this.b.a.t.setImageDrawable(drawable);
                return false;
            }

            @DexIgnore
            @Override // com.fossil.Ej1
            public boolean e(Dd1 dd1, Object obj, Qj1<Drawable> qj1, boolean z) {
                FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "renderContactData onLoadFailed");
                Contact contact = this.c.getContacts().get(0);
                Wg6.b(contact, "contactGroup.contacts[0]");
                contact.setPhotoThumbUri(null);
                return false;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object, com.fossil.Qj1, com.fossil.Gb1, boolean] */
            @Override // com.fossil.Ej1
            public /* bridge */ /* synthetic */ boolean g(Drawable drawable, Object obj, Qj1<Drawable> qj1, Gb1 gb1, boolean z) {
                return a(drawable, obj, qj1, gb1, z);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(NotificationFavoriteContactAdapter notificationFavoriteContactAdapter, Le5 le5) {
            super(le5.n());
            Wg6.c(le5, "binding");
            this.b = notificationFavoriteContactAdapter;
            this.a = le5;
            String d = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
            String d2 = ThemeManager.l.a().d("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(d)) {
                this.a.q.setBackgroundColor(Color.parseColor(d));
            }
            if (!TextUtils.isEmpty(d2)) {
                this.a.u.setBackgroundColor(Color.parseColor(d2));
            }
            this.a.q.setOnClickListener(new Aii(this));
            this.a.s.setOnClickListener(new Bii(this));
        }

        @DexIgnore
        public final void b(ContactGroup contactGroup) {
            Uri uri;
            Wg6.c(contactGroup, "contactGroup");
            FLogger.INSTANCE.getLocal().d("NotificationFavoriteContactAdapter", "set favorite contact data");
            if (contactGroup.getContacts().size() > 0) {
                Contact contact = contactGroup.getContacts().get(0);
                Wg6.b(contact, "contactGroup.contacts[0]");
                if (!TextUtils.isEmpty(contact.getPhotoThumbUri())) {
                    Contact contact2 = contactGroup.getContacts().get(0);
                    Wg6.b(contact2, "contactGroup.contacts[0]");
                    uri = Uri.parse(contact2.getPhotoThumbUri());
                } else {
                    uri = null;
                }
                FossilNotificationImageView fossilNotificationImageView = this.a.t;
                Wg6.b(fossilNotificationImageView, "binding.ivFavoriteContactIcon");
                Wj5 a2 = Tj5.a(fossilNotificationImageView.getContext());
                Contact contact3 = contactGroup.getContacts().get(0);
                Wg6.b(contact3, "contactGroup.contacts[0]");
                Vj5<Drawable> T0 = a2.I(new Sj5(uri, contact3.getDisplayName())).s1(true).X0(Wc1.a).T0(this.b.c);
                FossilNotificationImageView fossilNotificationImageView2 = this.a.t;
                Wg6.b(fossilNotificationImageView2, "binding.ivFavoriteContactIcon");
                Wj5 a3 = Tj5.a(fossilNotificationImageView2.getContext());
                Contact contact4 = contactGroup.getContacts().get(0);
                Wg6.b(contact4, "contactGroup.contacts[0]");
                Vj5<Drawable> b1 = T0.a1(a3.I(new Sj5((Uri) null, contact4.getDisplayName())).T0(this.b.c)).b1(new Cii(this, contactGroup));
                FossilNotificationImageView fossilNotificationImageView3 = this.a.t;
                Wg6.b(fossilNotificationImageView3, "binding.ivFavoriteContactIcon");
                b1.F0(fossilNotificationImageView3.getFossilCircleImageView());
                Contact contact5 = contactGroup.getContacts().get(0);
                Wg6.b(contact5, "contactGroup.contacts[0]");
                if (TextUtils.isEmpty(contact5.getPhotoThumbUri())) {
                    this.a.t.b();
                    FossilNotificationImageView fossilNotificationImageView4 = this.a.t;
                    Wg6.b(fossilNotificationImageView4, "binding.ivFavoriteContactIcon");
                    fossilNotificationImageView4.setBackground(W6.f(PortfolioApp.get.instance(), 2131231287));
                } else {
                    this.a.t.h();
                    this.a.t.setBackgroundResource(R.color.transparent);
                }
                FlexibleTextView flexibleTextView = this.a.r;
                Wg6.b(flexibleTextView, "binding.ftvFavoriteContactName");
                Contact contact6 = contactGroup.getContacts().get(0);
                Wg6.b(contact6, "contactGroup.contacts[0]");
                flexibleTextView.setText(contact6.getDisplayName());
            }
        }
    }

    @DexIgnore
    public NotificationFavoriteContactAdapter() {
        Yi1 o0 = new Fj1().o0(new Hk5());
        Wg6.b(o0, "RequestOptions().transform(CircleTransform())");
        this.c = (Fj1) o0;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public final List<ContactGroup> j() {
        return this.a;
    }

    @DexIgnore
    public void k(Bi bi, int i) {
        Wg6.c(bi, "holder");
        bi.b(this.a.get(i));
    }

    @DexIgnore
    public Bi l(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Le5 z = Le5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemFavoriteContactNotif\u2026.context), parent, false)");
        return new Bi(this, z);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0075 A[LOOP:0: B:1:0x000e->B:14:0x0075, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0068 A[EDGE_INSN: B:17:0x0068->B:11:0x0068 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m(com.fossil.wearables.fsl.contact.ContactGroup r9) {
        /*
            r8 = this;
            r3 = -1
            r2 = 0
            java.lang.String r0 = "contactGroup"
            com.mapped.Wg6.c(r9, r0)
            java.util.List<com.fossil.wearables.fsl.contact.ContactGroup> r0 = r8.a
            java.util.Iterator r5 = r0.iterator()
            r1 = r2
        L_0x000e:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x0079
            java.lang.Object r0 = r5.next()
            com.fossil.wearables.fsl.contact.ContactGroup r0 = (com.fossil.wearables.fsl.contact.ContactGroup) r0
            java.util.List r4 = r0.getContacts()
            java.lang.String r6 = "it.contacts"
            com.mapped.Wg6.b(r4, r6)
            boolean r6 = r4.isEmpty()
            r4 = 1
            r6 = r6 ^ 1
            if (r6 == 0) goto L_0x0073
            java.util.List r6 = r9.getContacts()
            java.lang.String r7 = "contactGroup.contacts"
            com.mapped.Wg6.b(r6, r7)
            boolean r6 = r6.isEmpty()
            r6 = r6 ^ 1
            if (r6 == 0) goto L_0x0073
            java.util.List r0 = r0.getContacts()
            java.lang.Object r0 = r0.get(r2)
            java.lang.String r6 = "it.contacts[0]"
            com.mapped.Wg6.b(r0, r6)
            com.fossil.wearables.fsl.contact.Contact r0 = (com.fossil.wearables.fsl.contact.Contact) r0
            int r6 = r0.getContactId()
            java.util.List r0 = r9.getContacts()
            java.lang.Object r0 = r0.get(r2)
            java.lang.String r7 = "contactGroup.contacts[0]"
            com.mapped.Wg6.b(r0, r7)
            com.fossil.wearables.fsl.contact.Contact r0 = (com.fossil.wearables.fsl.contact.Contact) r0
            int r0 = r0.getContactId()
            if (r6 != r0) goto L_0x0073
            r0 = r4
        L_0x0066:
            if (r0 == 0) goto L_0x0075
        L_0x0068:
            if (r1 == r3) goto L_0x0072
            java.util.List<com.fossil.wearables.fsl.contact.ContactGroup> r0 = r8.a
            r0.remove(r1)
            r8.notifyItemRemoved(r1)
        L_0x0072:
            return
        L_0x0073:
            r0 = r2
            goto L_0x0066
        L_0x0075:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x000e
        L_0x0079:
            r1 = r3
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.NotificationFavoriteContactAdapter.m(com.fossil.wearables.fsl.contact.ContactGroup):void");
    }

    @DexIgnore
    public final void n(List<ContactGroup> list) {
        Wg6.c(list, "listContactGroup");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void o(Ai ai) {
        Wg6.c(ai, "listener");
        this.b = ai;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        k(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return l(viewGroup, i);
    }
}
