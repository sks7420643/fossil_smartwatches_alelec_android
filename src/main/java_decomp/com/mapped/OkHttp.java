package com.mapped;

import com.facebook.GraphRequest;
import com.facebook.stetho.server.http.HttpHeaders;
import com.fossil.E18;
import com.fossil.I48;
import com.fossil.K48;
import com.fossil.P18;
import com.fossil.R18;
import com.fossil.T18;
import com.fossil.U28;
import com.fossil.V18;
import com.fossil.W18;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.io.EOFException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class OkHttp implements Interceptor {
    @DexIgnore
    public static /* final */ Charset b; // = StandardCharsets.UTF_8;
    @DexIgnore
    public volatile HttpLoggingInterceptor.a a; // = HttpLoggingInterceptor.a.NONE;

    @DexIgnore
    public static boolean b(I48 i48) {
        long j = 64;
        try {
            I48 i482 = new I48();
            if (i48.p0() < 64) {
                j = i48.p0();
            }
            i48.C(i482, 0, j);
            for (int i = 0; i < 16 && !i482.u(); i++) {
                int i0 = i482.i0();
                if (Character.isISOControl(i0) && !Character.isWhitespace(i0)) {
                    return false;
                }
            }
            return true;
        } catch (EOFException e) {
            return false;
        }
    }

    @DexIgnore
    public final boolean a(P18 p18) {
        String c = p18.c(GraphRequest.CONTENT_ENCODING_HEADER);
        return c != null && !c.equalsIgnoreCase("identity");
    }

    @DexIgnore
    public OkHttp c(HttpLoggingInterceptor.a aVar) {
        if (aVar != null) {
            this.a = aVar;
            return this;
        }
        throw new NullPointerException("level == null. Use Level.EMPTY instead.");
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        String str;
        HttpLoggingInterceptor.a aVar = this.a;
        V18 c = chain.c();
        if (aVar == HttpLoggingInterceptor.a.NONE) {
            return chain.d(c);
        }
        boolean z = true;
        boolean z2 = aVar == HttpLoggingInterceptor.a.BODY;
        boolean z3 = z2 || aVar == HttpLoggingInterceptor.a.HEADERS;
        RequestBody a2 = c.a();
        if (a2 == null) {
            z = false;
        }
        E18 e = chain.e();
        T18 a3 = e != null ? e.a() : T18.HTTP_1_1;
        long nanoTime = System.nanoTime();
        StringBuilder sb = new StringBuilder();
        sb.append("--> Id: ");
        sb.append(nanoTime);
        sb.append("\n");
        sb.append("--> ");
        sb.append(c.g());
        sb.append(' ');
        sb.append(c.j());
        sb.append(' ');
        sb.append(a3);
        if (!z3 && z) {
            sb.append(" (");
            sb.append(a2.a());
            sb.append("-byte body)");
        }
        sb.append("\n");
        if (z3) {
            if (z) {
                if (a2.b() != null) {
                    sb.append("--> Content-Type: ");
                    sb.append(a2.b());
                    sb.append("\n");
                }
                if (a2.a() != -1) {
                    sb.append("--> Content-Length: ");
                    sb.append(a2.a());
                    sb.append("\n");
                }
            }
            P18 e2 = c.e();
            Ku3 ku3 = new Ku3();
            int h = e2.h();
            for (int i = 0; i < h; i++) {
                String e3 = e2.e(i);
                if (!"Content-Type".equalsIgnoreCase(e3) && !HttpHeaders.CONTENT_LENGTH.equalsIgnoreCase(e3)) {
                    ku3.n(e3, e2.i(i));
                }
            }
            sb.append("--> Headers: ");
            sb.append(ku3);
            sb.append("\n");
            if (!z2 || !z) {
                str = "-byte body)";
                sb.append("--> END ");
                sb.append(c.g());
                sb.append("\n");
            } else if (a(c.e())) {
                sb.append("--> END ");
                sb.append(c.g());
                sb.append(" (encoded body omitted)");
                sb.append("\n");
                str = "-byte body)";
            } else {
                I48 i48 = new I48();
                a2.h(i48);
                Charset charset = b;
                R18 b2 = a2.b();
                if (b2 != null) {
                    charset = b2.b(b);
                }
                if (b(i48)) {
                    sb.append("--> Body: ");
                    sb.append(charset != null ? i48.I(charset) : "empty");
                    sb.append("\n");
                    sb.append("--> END ");
                    sb.append(c.g());
                    sb.append(" (");
                    sb.append(a2.a());
                    str = "-byte body)";
                    sb.append(str);
                    sb.append("\n");
                } else {
                    str = "-byte body)";
                    sb.append("--> END ");
                    sb.append(c.g());
                    sb.append("(binary ");
                    sb.append(a2.a());
                    sb.append("-byte body omitted)");
                    sb.append("\n");
                }
            }
        } else {
            str = "-byte body)";
        }
        FLogger.INSTANCE.getLocal().d("OkHttp", sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append("<-- Id: ");
        sb2.append(nanoTime);
        sb2.append("\n");
        long nanoTime2 = System.nanoTime();
        try {
            Response d = chain.d(c);
            long millis = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - nanoTime2);
            W18 a4 = d.a();
            long contentLength = a4 != null ? a4.contentLength() : -1;
            String str2 = z3 ? "" : ", " + (contentLength == -1 ? "unknown-length" : contentLength + "-byte") + " body";
            sb2.append("<-- Code ");
            sb2.append(d.f());
            sb2.append(' ');
            sb2.append(d.o());
            sb2.append(' ');
            sb2.append(d.G().j());
            sb2.append(" (");
            sb2.append(millis);
            sb2.append("ms");
            sb2.append(str2);
            sb2.append(')');
            sb2.append("\n");
            if (z3) {
                P18 l = d.l();
                Ku3 ku32 = new Ku3();
                int h2 = l.h();
                for (int i2 = 0; i2 < h2; i2++) {
                    ku32.n(l.e(i2), l.i(i2));
                }
                sb2.append("<-- Headers: ");
                sb2.append(ku32);
                sb2.append("\n");
                if (!z2 || !U28.c(d)) {
                    sb2.append("<-- END HTTP");
                    sb2.append("\n");
                } else if (a(d.l())) {
                    sb2.append("<-- END HTTP (encoded body omitted)");
                    sb2.append("\n");
                } else if (a4 != null) {
                    K48 source = a4.source();
                    source.R(Long.MAX_VALUE);
                    I48 d2 = source.d();
                    Charset charset2 = b;
                    R18 contentType = a4.contentType();
                    if (contentType != null) {
                        charset2 = contentType.b(b);
                    }
                    if (!b(d2)) {
                        sb2.append("<-- END HTTP (binary ");
                        sb2.append(d2.p0());
                        sb2.append("-byte body omitted)");
                        sb2.append("\n");
                        FLogger.INSTANCE.getLocal().d("OkHttp", sb2.toString());
                        return d;
                    }
                    if (contentLength != 0) {
                        sb2.append("<-- Body: ");
                        sb2.append(charset2 != null ? d2.l().I(charset2) : "empty");
                        sb2.append("\n");
                    }
                    sb2.append("<-- END HTTP (");
                    sb2.append(d2.p0());
                    sb2.append(str);
                    sb2.append("\n");
                } else {
                    sb2.append("<-- END HTTP (");
                    sb2.append("-1");
                    sb2.append(str);
                    sb2.append("\n");
                }
            }
            FLogger.INSTANCE.getLocal().d("OkHttp", sb2.toString());
            return d;
        } catch (Exception e4) {
            sb2.append("<-- HTTP FAILED: ");
            sb2.append(e4);
            sb2.append("\n");
            FLogger.INSTANCE.getLocal().d("OkHttp", sb2.toString());
            throw e4;
        }
    }
}
