package com.mapped;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.Hq4;
import com.fossil.Hu6;
import com.fossil.Ku6;
import com.fossil.Ls0;
import com.fossil.Nl5;
import com.fossil.Po4;
import com.fossil.Qv5;
import com.fossil.Rc5;
import com.fossil.S37;
import com.fossil.Ts0;
import com.fossil.Uh5;
import com.fossil.Vs0;
import com.mapped.AlertDialogFragment;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingViewModel;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.RTLImageView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingFragment extends Qv5 implements View.OnClickListener, AlertDialogFragment.Gi, Hu6.Ai {
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public G37<Rc5> h;
    @DexIgnore
    public Hu6 i;
    @DexIgnore
    public WorkoutSettingViewModel j;
    @DexIgnore
    public Po4 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final WorkoutSettingFragment a() {
            return new WorkoutSettingFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<WorkoutSettingViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSettingFragment a;

        @DexIgnore
        public Bi(WorkoutSettingFragment workoutSettingFragment) {
            this.a = workoutSettingFragment;
        }

        @DexIgnore
        public final void a(WorkoutSettingViewModel.Ai ai) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                if (ai != null) {
                    int i = Ku6.a[ai.ordinal()];
                    if (i == 1) {
                        FLogger.INSTANCE.getLocal().d("WorkoutSettingFragment", "Data is changed, wait for user confirm to save");
                        S37 s37 = S37.c;
                        FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                        Wg6.b(childFragmentManager, "childFragmentManager");
                        s37.y0(childFragmentManager);
                        return;
                    } else if (i == 2 || i == 3) {
                        FLogger.INSTANCE.getLocal().d("WorkoutSettingFragment", "close view");
                        activity.finish();
                        return;
                    } else if (i == 4) {
                        FLogger.INSTANCE.getLocal().d("WorkoutSettingFragment", "FAIL_DEVICE_DISCONNECT");
                        S37 s372 = S37.c;
                        FragmentManager childFragmentManager2 = this.a.getChildFragmentManager();
                        Wg6.b(childFragmentManager2, "childFragmentManager");
                        s372.y(childFragmentManager2);
                        return;
                    }
                }
                FLogger.INSTANCE.getLocal().d("WorkoutSettingFragment", "unknown error");
                String J = PortfolioApp.get.instance().J();
                TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
                Context requireContext = this.a.requireContext();
                Wg6.b(requireContext, "requireContext()");
                TroubleshootingActivity.a.c(aVar, requireContext, J, false, false, 12, null);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(WorkoutSettingViewModel.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<Hq4.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSettingFragment a;

        @DexIgnore
        public Ci(WorkoutSettingFragment workoutSettingFragment) {
            this.a = workoutSettingFragment;
        }

        @DexIgnore
        public final void a(Hq4.Ci ci) {
            if (!ci.a().isEmpty()) {
                WorkoutSettingFragment workoutSettingFragment = this.a;
                Object[] array = ci.a().toArray(new Uh5[0]);
                if (array != null) {
                    Uh5[] uh5Arr = (Uh5[]) array;
                    workoutSettingFragment.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Ci ci) {
            a(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Hq4.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSettingFragment a;

        @DexIgnore
        public Di(WorkoutSettingFragment workoutSettingFragment) {
            this.a = workoutSettingFragment;
        }

        @DexIgnore
        public final void a(Hq4.Bi bi) {
            if (bi != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutSettingFragment", "loadingState start " + bi.a() + " stop " + bi.b());
                if (bi.b()) {
                    this.a.a();
                } else if (bi.a()) {
                    this.a.b();
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Bi bi) {
            a(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<List<? extends WorkoutSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSettingFragment a;

        @DexIgnore
        public Ei(WorkoutSettingFragment workoutSettingFragment) {
            this.a = workoutSettingFragment;
        }

        @DexIgnore
        public final void a(List<WorkoutSetting> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutSettingFragment", "update workoutSettingList, " + list);
            Hu6 K6 = WorkoutSettingFragment.K6(this.a);
            Wg6.b(list, "it");
            K6.k(list);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends WorkoutSetting> list) {
            a(list);
        }
    }

    @DexIgnore
    public static final /* synthetic */ Hu6 K6(WorkoutSettingFragment workoutSettingFragment) {
        Hu6 hu6 = workoutSettingFragment.i;
        if (hu6 != null) {
            return hu6;
        }
        Wg6.n("mAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        WorkoutSettingViewModel workoutSettingViewModel = this.j;
        if (workoutSettingViewModel != null) {
            Hu6 hu6 = this.i;
            if (hu6 != null) {
                workoutSettingViewModel.q(hu6.h());
                return true;
            }
            Wg6.n("mAdapter");
            throw null;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Hu6.Ai
    public void M3(WorkoutSetting workoutSetting, boolean z) {
        Wg6.c(workoutSetting, "workout");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutSettingFragment", "onConfirmationChange(), workout = " + workoutSetting + ", value=" + z);
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        Wg6.c(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != -1375614559) {
            if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i2 == 2131363373 && (activity = getActivity()) != null) {
                activity.finish();
            }
        } else if (!str.equals("UNSAVED_CHANGE")) {
        } else {
            if (i2 == 2131363291) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    activity2.finish();
                }
            } else if (i2 == 2131363373) {
                WorkoutSettingViewModel workoutSettingViewModel = this.j;
                if (workoutSettingViewModel != null) {
                    Hu6 hu6 = this.i;
                    if (hu6 != null) {
                        workoutSettingViewModel.u(hu6.h());
                    } else {
                        Wg6.n("mAdapter");
                        throw null;
                    }
                } else {
                    Wg6.n("mViewModel");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Hu6.Ai
    public void m5(WorkoutSetting workoutSetting, boolean z) {
        Wg6.c(workoutSetting, "workout");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutSettingFragment", "onEnableChange(), workout = " + workoutSetting + ", value=" + z);
    }

    @DexIgnore
    public void onClick(View view) {
        if (isActive() && view != null && view.getId() == 2131361851) {
            WorkoutSettingViewModel workoutSettingViewModel = this.j;
            if (workoutSettingViewModel != null) {
                Hu6 hu6 = this.i;
                if (hu6 != null) {
                    workoutSettingViewModel.q(hu6.h());
                } else {
                    Wg6.n("mAdapter");
                    throw null;
                }
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Rc5 rc5 = (Rc5) Aq0.f(layoutInflater, 2131558645, viewGroup, false, A6());
        this.h = new G37<>(this, rc5);
        RTLImageView rTLImageView = rc5.q;
        Wg6.b(rTLImageView, "binding.acivBack");
        Nl5.a(rTLImageView, this);
        this.i = new Hu6();
        RecyclerView recyclerView = rc5.s;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        Hu6 hu6 = this.i;
        if (hu6 != null) {
            recyclerView.setAdapter(hu6);
            Hu6 hu62 = this.i;
            if (hu62 != null) {
                hu62.l(this);
                G37<Rc5> g37 = this.h;
                if (g37 != null) {
                    Rc5 a2 = g37.a();
                    if (a2 != null) {
                        Wg6.b(a2, "mBinding.get()!!");
                        return a2.n();
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("mAdapter");
            throw null;
        }
        Wg6.n("mAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        PortfolioApp.get.instance().getIface().U().a(this);
        FragmentActivity requireActivity = requireActivity();
        Po4 po4 = this.k;
        if (po4 != null) {
            Ts0 a2 = Vs0.f(requireActivity, po4).a(WorkoutSettingViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(re\u2026ingViewModel::class.java)");
            WorkoutSettingViewModel workoutSettingViewModel = (WorkoutSettingViewModel) a2;
            this.j = workoutSettingViewModel;
            if (workoutSettingViewModel != null) {
                workoutSettingViewModel.r().h(getViewLifecycleOwner(), new Bi(this));
                WorkoutSettingViewModel workoutSettingViewModel2 = this.j;
                if (workoutSettingViewModel2 != null) {
                    workoutSettingViewModel2.l().h(getViewLifecycleOwner(), new Ci(this));
                    WorkoutSettingViewModel workoutSettingViewModel3 = this.j;
                    if (workoutSettingViewModel3 != null) {
                        workoutSettingViewModel3.j().h(getViewLifecycleOwner(), new Di(this));
                        WorkoutSettingViewModel workoutSettingViewModel4 = this.j;
                        if (workoutSettingViewModel4 != null) {
                            workoutSettingViewModel4.s().h(getViewLifecycleOwner(), new Ei(this));
                        } else {
                            Wg6.n("mViewModel");
                            throw null;
                        }
                    } else {
                        Wg6.n("mViewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("mViewModel");
                    throw null;
                }
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModelFactory");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
