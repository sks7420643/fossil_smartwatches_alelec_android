package com.mapped;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import com.portfolio.platform.usecase.DSTChangeUseCase;
import java.util.Calendar;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TimeTickReceiver extends BroadcastReceiver {
    @DexIgnore
    public An4 a;
    @DexIgnore
    public DSTChangeUseCase b;
    @DexIgnore
    public AlarmHelper c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Ei<DSTChangeUseCase.Ci, DSTChangeUseCase.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ TimeTickReceiver a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(TimeTickReceiver timeTickReceiver) {
            this.a = timeTickReceiver;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(DSTChangeUseCase.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(DSTChangeUseCase.Bi bi) {
            Wg6.c(bi, "errorValue");
            FLogger.INSTANCE.getLocal().d("TimeTickReceiver", bi.toString());
        }

        @DexIgnore
        public void c(DSTChangeUseCase.Ci ci) {
            Wg6.c(ci, "responseValue");
            FLogger.INSTANCE.getLocal().d("TimeTickReceiver", "Re-schedule SyncDataReminder");
            AlarmHelper a2 = this.a.a();
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            Wg6.b(applicationContext, "PortfolioApp.instance.applicationContext");
            a2.h(applicationContext);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(DSTChangeUseCase.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore
    public TimeTickReceiver() {
        PortfolioApp.get.instance().getIface().w0(this);
    }

    @DexIgnore
    public final AlarmHelper a() {
        AlarmHelper alarmHelper = this.c;
        if (alarmHelper != null) {
            return alarmHelper;
        }
        Wg6.n("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("TimeTickReceiver", "onReceive() - action = " + action);
        An4 an4 = this.a;
        if (an4 != null) {
            long C = an4.C(PortfolioApp.get.instance().J());
            if (!TextUtils.isEmpty(action) && Wg6.a(action, "android.intent.action.TIME_TICK") && System.currentTimeMillis() - C <= 120000) {
                FossilNotificationBar.Ai ai = FossilNotificationBar.c;
                if (context != null) {
                    ai.d(context);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            if (Wg6.a("android.intent.action.TIME_TICK", action)) {
                TimeZone timeZone = TimeZone.getDefault();
                try {
                    Calendar instance = Calendar.getInstance();
                    Wg6.b(instance, "calendar");
                    Calendar instance2 = Calendar.getInstance();
                    Wg6.b(instance2, "Calendar.getInstance()");
                    instance.setTimeInMillis(instance2.getTimeInMillis() - ((long) 60000));
                    Calendar instance3 = Calendar.getInstance();
                    Wg6.b(instance3, "Calendar.getInstance()");
                    if (timeZone.getOffset(instance3.getTimeInMillis()) != timeZone.getOffset(instance.getTimeInMillis())) {
                        FLogger.INSTANCE.getLocal().d("TimeTickReceiver", "DST Changed.");
                        DSTChangeUseCase dSTChangeUseCase = this.b;
                        if (dSTChangeUseCase != null) {
                            dSTChangeUseCase.e(new DSTChangeUseCase.Ai(), new Ai(this));
                        } else {
                            Wg6.n("mDstChangeUseCase");
                            throw null;
                        }
                    }
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.e("TimeTickReceiver", ".timeZoneChangeReceiver - ex=" + e);
                }
            }
        } else {
            Wg6.n("mSharedPreferencesManager");
            throw null;
        }
    }
}
