package com.mapped;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Aq0;
import com.fossil.Br6;
import com.fossil.Fr4;
import com.fossil.G37;
import com.fossil.Ls0;
import com.fossil.Po4;
import com.fossil.Rb5;
import com.fossil.S37;
import com.fossil.Ts0;
import com.fossil.Vs0;
import com.mapped.AlertDialogFragment;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel;
import com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemesFragment extends BaseFragment implements Fr4.Ai, AlertDialogFragment.Gi {
    @DexIgnore
    public static String l; // = "";
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public G37<Rb5> h;
    @DexIgnore
    public ThemesViewModel i;
    @DexIgnore
    public Fr4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final ThemesFragment a() {
            return new ThemesFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<ThemesViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ ThemesFragment a;

        @DexIgnore
        public Bi(ThemesFragment themesFragment) {
            this.a = themesFragment;
        }

        @DexIgnore
        public final void a(ThemesViewModel.Ai ai) {
            FragmentActivity activity;
            ArrayList<Theme> c;
            if (!(ai == null || (c = ai.c()) == null)) {
                this.a.M6(c);
                ThemesFragment themesFragment = this.a;
                String e = ai.e();
                if (e != null) {
                    String a2 = ai.a();
                    if (a2 != null) {
                        themesFragment.L6(e, a2, ai.d());
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            if (ai.b() && (activity = this.a.getActivity()) != null) {
                HomeActivity.a aVar = HomeActivity.B;
                Wg6.b(activity, "it");
                HomeActivity.a.b(aVar, activity, null, 2, null);
                this.a.e0();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(ThemesViewModel.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ThemesFragment b;

        @DexIgnore
        public Ci(ThemesFragment themesFragment) {
            this.b = themesFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ThemesFragment b;

        @DexIgnore
        public Di(ThemesFragment themesFragment) {
            this.b = themesFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            ThemesFragment.K6(this.b).m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ThemesFragment b;

        @DexIgnore
        public Ei(ThemesFragment themesFragment) {
            this.b = themesFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                String valueOf = String.valueOf(System.currentTimeMillis());
                ThemeManager.l.a().c(valueOf);
                UserCustomizeThemeActivity.a aVar = UserCustomizeThemeActivity.A;
                Wg6.b(activity, "it");
                aVar.a(activity, valueOf, false);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ ThemesViewModel K6(ThemesFragment themesFragment) {
        ThemesViewModel themesViewModel = themesFragment.i;
        if (themesViewModel != null) {
            return themesViewModel;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Fr4.Ai
    public void D2(String str) {
        Wg6.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "onUserChooseTheme id=" + str);
        ThemesViewModel themesViewModel = this.i;
        if (themesViewModel != null) {
            themesViewModel.q(str);
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Fr4.Ai
    public void L4(String str) {
        Wg6.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "onUserDeleteTheme id=" + str);
        S37 s37 = S37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        s37.n(childFragmentManager);
        l = str;
    }

    @DexIgnore
    public final void L6(String str, String str2, List<Style> list) {
        Wg6.c(str, "userSelectedThemeId");
        Wg6.c(str2, "currentThemeId");
        G37<Rb5> g37 = this.h;
        if (g37 != null) {
            Rb5 a2 = g37.a();
            if (a2 != null && list != null) {
                Fr4 fr4 = this.j;
                if (fr4 != null) {
                    fr4.l(list, str);
                }
                String e = ThemeManager.l.a().e(Explore.COLUMN_BACKGROUND, list);
                String e2 = ThemeManager.l.a().e("primaryText", list);
                Typeface g2 = ThemeManager.l.a().g("headline2", list);
                if (e != null) {
                    a2.r.setBackgroundColor(Color.parseColor(e));
                }
                if (e2 != null) {
                    a2.v.setTextColor(Color.parseColor(e2));
                    a2.q.setColorFilter(Color.parseColor(e2));
                }
                if (g2 != null) {
                    FlexibleTextView flexibleTextView = a2.v;
                    Wg6.b(flexibleTextView, "it.tvTitle");
                    flexibleTextView.setTypeface(g2);
                }
                String e3 = ThemeManager.l.a().e("onPrimaryButton", list);
                Typeface g3 = ThemeManager.l.a().g("textButtonPrimary", list);
                String e4 = ThemeManager.l.a().e("primaryButton", list);
                if (e3 != null) {
                    a2.t.setTextColor(Color.parseColor(e3));
                    a2.u.setTextColor(Color.parseColor(e3));
                }
                if (g3 != null) {
                    FlexibleButton flexibleButton = a2.t;
                    Wg6.b(flexibleButton, "it.tvApply");
                    flexibleButton.setTypeface(g3);
                    FlexibleButton flexibleButton2 = a2.u;
                    Wg6.b(flexibleButton2, "it.tvCreateNewTheme");
                    flexibleButton2.setTypeface(g3);
                }
                if (e4 != null) {
                    a2.t.setBackgroundColor(Color.parseColor(e4));
                    a2.u.setBackgroundColor(Color.parseColor(e4));
                    return;
                }
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void M6(ArrayList<Theme> arrayList) {
        Wg6.c(arrayList, "listTheme");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "showListTheme size=" + arrayList.size());
        Fr4 fr4 = this.j;
        if (fr4 != null) {
            fr4.o(arrayList);
        }
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        FLogger.INSTANCE.getLocal().d("ThemesFragment", "onDialogFragmentResult");
        if (str.hashCode() == -1478349291 && str.equals("DELETE_THEME") && i2 == 2131363373) {
            ThemesViewModel themesViewModel = this.i;
            if (themesViewModel != null) {
                themesViewModel.n(l);
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.Fr4.Ai
    public void g1(String str) {
        Wg6.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "onUserEditTheme id=" + str);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            UserCustomizeThemeActivity.a aVar = UserCustomizeThemeActivity.A;
            Wg6.b(activity, "it");
            aVar.a(activity, str, true);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Rb5 rb5 = (Rb5) Aq0.f(LayoutInflater.from(getContext()), 2131558629, null, false, A6());
        this.j = new Fr4(new ArrayList(), this);
        PortfolioApp.get.instance().getIface().v(new Br6()).a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(ThemesViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026mesViewModel::class.java)");
            ThemesViewModel themesViewModel = (ThemesViewModel) a2;
            this.i = themesViewModel;
            if (themesViewModel != null) {
                themesViewModel.p().h(getViewLifecycleOwner(), new Bi(this));
                ThemesViewModel themesViewModel2 = this.i;
                if (themesViewModel2 != null) {
                    themesViewModel2.r();
                    this.h = new G37<>(this, rb5);
                    Wg6.b(rb5, "binding");
                    return rb5.n();
                }
                Wg6.n("mViewModel");
                throw null;
            }
            Wg6.n("mViewModel");
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ThemesViewModel themesViewModel = this.i;
        if (themesViewModel != null) {
            themesViewModel.r();
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<Rb5> g37 = this.h;
        if (g37 != null) {
            Rb5 a2 = g37.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new Ci(this));
                a2.t.setOnClickListener(new Di(this));
                a2.u.setOnClickListener(new Ei(this));
                RecyclerView recyclerView = a2.s;
                Wg6.b(recyclerView, "binding.rvThemes");
                recyclerView.setAdapter(this.j);
                RecyclerView recyclerView2 = a2.s;
                Wg6.b(recyclerView2, "binding.rvThemes");
                recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView3 = a2.s;
                Wg6.b(recyclerView3, "binding.rvThemes");
                recyclerView3.setNestedScrollingEnabled(false);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
