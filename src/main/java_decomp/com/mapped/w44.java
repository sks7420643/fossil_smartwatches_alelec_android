package com.mapped;

import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class W44 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppSettingRepository.Anon4 b;
    @DexIgnore
    public /* final */ /* synthetic */ List c;

    @DexIgnore
    public /* synthetic */ W44(MicroAppSettingRepository.Anon4 anon4, List list) {
        this.b = anon4;
        this.c = list;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c);
    }
}
