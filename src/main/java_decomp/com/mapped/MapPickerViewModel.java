package com.mapped;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.model.PlaceFields;
import com.fossil.Ts0;
import com.fossil.Um5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.network.utils.NetworkUtils;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import com.portfolio.platform.ui.device.locate.map.usecase.GetUserLocation;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MapPickerViewModel extends Ts0 {
    @DexIgnore
    public MutableLiveData<Ai> a; // = new MutableLiveData<>();
    @DexIgnore
    public Location b;
    @DexIgnore
    public /* final */ Geocoder c; // = new Geocoder(this.d);
    @DexIgnore
    public /* final */ PortfolioApp d;
    @DexIgnore
    public /* final */ GetUserLocation e;
    @DexIgnore
    public /* final */ GetAddress f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Double b;
        @DexIgnore
        public Double c;
        @DexIgnore
        public String d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public Boolean f;
        @DexIgnore
        public Boolean g;

        @DexIgnore
        public Ai(Integer num, Double d2, Double d3, String str, Boolean bool, Boolean bool2, Boolean bool3) {
            this.a = num;
            this.b = d2;
            this.c = d3;
            this.d = str;
            this.e = bool;
            this.f = bool2;
            this.g = bool3;
        }

        @DexIgnore
        public final String a() {
            return this.d;
        }

        @DexIgnore
        public final Boolean b() {
            return this.e;
        }

        @DexIgnore
        public final Integer c() {
            return this.a;
        }

        @DexIgnore
        public final Double d() {
            return this.b;
        }

        @DexIgnore
        public final Double e() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!Wg6.a(this.a, ai.a) || !Wg6.a(this.b, ai.b) || !Wg6.a(this.c, ai.c) || !Wg6.a(this.d, ai.d) || !Wg6.a(this.e, ai.e) || !Wg6.a(this.f, ai.f) || !Wg6.a(this.g, ai.g)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final Boolean f() {
            return this.g;
        }

        @DexIgnore
        public final Boolean g() {
            return this.f;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            Integer num = this.a;
            int hashCode = num != null ? num.hashCode() : 0;
            Double d2 = this.b;
            int hashCode2 = d2 != null ? d2.hashCode() : 0;
            Double d3 = this.c;
            int hashCode3 = d3 != null ? d3.hashCode() : 0;
            String str = this.d;
            int hashCode4 = str != null ? str.hashCode() : 0;
            Boolean bool = this.e;
            int hashCode5 = bool != null ? bool.hashCode() : 0;
            Boolean bool2 = this.f;
            int hashCode6 = bool2 != null ? bool2.hashCode() : 0;
            Boolean bool3 = this.g;
            if (bool3 != null) {
                i = bool3.hashCode();
            }
            return (((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "UiDataWrapper(errorNetwork=" + this.a + ", latitude=" + this.b + ", longitude=" + this.c + ", address=" + this.d + ", enableBtnConfirm=" + this.e + ", isShowDialogLoading=" + this.f + ", isPermissionGranted=" + this.g + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ei<GetAddress.Ci, GetAddress.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ MapPickerViewModel a;
        @DexIgnore
        public /* final */ /* synthetic */ double b;
        @DexIgnore
        public /* final */ /* synthetic */ double c;

        @DexIgnore
        public Bi(MapPickerViewModel mapPickerViewModel, double d, double d2) {
            this.a = mapPickerViewModel;
            this.b = d;
            this.c = d2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(GetAddress.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(GetAddress.Bi bi) {
            Wg6.c(bi, "errorValue");
            FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetCityName onError");
            MapPickerViewModel.f(this.a, null, null, null, Um5.c(PortfolioApp.get.instance(), 2131886553), null, Boolean.FALSE, null, 87, null);
        }

        @DexIgnore
        public void c(GetAddress.Ci ci) {
            Wg6.c(ci, "responseValue");
            String a2 = ci.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MapPickerViewModel", "GetCityName onSuccess - address: " + a2 + " lat " + this.b + " long " + this.c);
            this.a.b = new Location("gps");
            Location location = this.a.b;
            if (location != null) {
                location.setLatitude(this.b);
                Location location2 = this.a.b;
                if (location2 != null) {
                    location2.setLongitude(this.c);
                    MapPickerViewModel.f(this.a, null, null, null, a2, Boolean.TRUE, Boolean.FALSE, null, 71, null);
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(GetAddress.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Ei<GetUserLocation.Ci, GetUserLocation.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ MapPickerViewModel a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ci(MapPickerViewModel mapPickerViewModel) {
            this.a = mapPickerViewModel;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(GetUserLocation.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(GetUserLocation.Bi bi) {
            Wg6.c(bi, "errorValue");
            FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetLocation onError");
        }

        @DexIgnore
        public void c(GetUserLocation.Ci ci) {
            Wg6.c(ci, "responseValue");
            FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetLocation onSuccess");
            this.a.k(ci.a(), ci.b());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(GetUserLocation.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore
    public MapPickerViewModel(PortfolioApp portfolioApp, GetUserLocation getUserLocation, GetAddress getAddress) {
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(getUserLocation, "mGetLocation");
        Wg6.c(getAddress, "mGetAddress");
        this.d = portfolioApp;
        this.e = getUserLocation;
        this.f = getAddress;
    }

    @DexIgnore
    public static /* synthetic */ void f(MapPickerViewModel mapPickerViewModel, Integer num, Double d2, Double d3, String str, Boolean bool, Boolean bool2, Boolean bool3, int i, Object obj) {
        mapPickerViewModel.e((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : d2, (i & 4) != 0 ? null : d3, (i & 8) != 0 ? null : str, (i & 16) != 0 ? null : bool, (i & 32) != 0 ? null : bool2, (i & 64) != 0 ? null : bool3);
    }

    @DexIgnore
    public final boolean d() {
        if (NetworkUtils.isNetworkAvailable(this.d)) {
            return true;
        }
        FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "No internet connection");
        f(this, 601, null, null, null, null, null, null, 126, null);
        return false;
    }

    @DexIgnore
    public final void e(Integer num, Double d2, Double d3, String str, Boolean bool, Boolean bool2, Boolean bool3) {
        this.a.l(new Ai(num, d2, d3, str, bool, bool2, bool3));
    }

    @DexIgnore
    public final void g(double d2, double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerViewModel", "GetCityName at lat: " + d2 + ", lng: " + d3);
        this.f.e(new GetAddress.Ai(d2, d3), new Bi(this, d2, d3));
    }

    @DexIgnore
    public final void h(String str) {
        Wg6.c(str, "address");
        try {
            List<Address> fromLocationName = this.c.getFromLocationName(str, 5);
            Wg6.b(fromLocationName, PlaceFields.LOCATION);
            if (!fromLocationName.isEmpty()) {
                Address address = fromLocationName.get(0);
                Wg6.b(address, "location[0]");
                double latitude = address.getLatitude();
                Address address2 = fromLocationName.get(0);
                Wg6.b(address2, "location[0]");
                f(this, null, Double.valueOf(latitude), Double.valueOf(address2.getLongitude()), str, null, null, null, 113, null);
                return;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MapPickerViewModel", "No lat lng found for address " + str);
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("MapPickerViewModel", "Exception when get lat lng from address " + str + ' ' + e2);
            j();
        }
    }

    @DexIgnore
    public final LiveData<Ai> i() {
        return this.a;
    }

    @DexIgnore
    public final void j() {
        FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetLocation");
        this.e.e(new GetUserLocation.Ai(), new Ci(this));
    }

    @DexIgnore
    public final void k(double d2, double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerViewModel", "handleLocation latitude=" + d2 + ", longitude=" + d3);
        if (d2 != 0.0d && d3 != 0.0d) {
            f(this, null, Double.valueOf(d2), Double.valueOf(d3), null, null, null, null, 121, null);
            g(d2, d3);
        }
    }

    @DexIgnore
    public final void l(double d2, double d3, String str) {
        Wg6.c(str, "address");
        if (d2 != 0.0d && d3 != 0.0d && !TextUtils.isEmpty(str)) {
            f(this, null, Double.valueOf(d2), Double.valueOf(d3), str, null, Boolean.FALSE, null, 81, null);
        } else if (!TextUtils.isEmpty(str)) {
            h(str);
        } else {
            j();
        }
    }

    @DexIgnore
    public final Location m() {
        return this.b;
    }
}
