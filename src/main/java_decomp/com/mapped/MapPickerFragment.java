package com.mapped;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.model.PlaceFields;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.Hr7;
import com.fossil.Jn5;
import com.fossil.L85;
import com.fossil.Le3;
import com.fossil.Ls0;
import com.fossil.Pb3;
import com.fossil.Po4;
import com.fossil.Qb3;
import com.fossil.Qv5;
import com.fossil.S37;
import com.fossil.Sb3;
import com.fossil.Ts0;
import com.fossil.Um5;
import com.fossil.Vs0;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.mapped.MapPickerViewModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleImageButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MapPickerFragment extends Qv5 implements Qb3.Bi, Qb3.Di {
    @DexIgnore
    public static /* final */ Bi m; // = new Bi(null);
    @DexIgnore
    public G37<L85> h;
    @DexIgnore
    public MapPickerViewModel i;
    @DexIgnore
    public Qb3 j;
    @DexIgnore
    public Po4 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Qb3.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ L85 a;
        @DexIgnore
        public /* final */ /* synthetic */ LatLng b;

        @DexIgnore
        public Ai(L85 l85, LatLng latLng) {
            this.a = l85;
            this.b = latLng;
        }

        @DexIgnore
        @Override // com.fossil.Qb3.Ai
        public void onCancel() {
            FlexibleImageButton flexibleImageButton = this.a.r;
            Wg6.b(flexibleImageButton, "binding.fbCurrentLocation");
            flexibleImageButton.setEnabled(true);
        }

        @DexIgnore
        @Override // com.fossil.Qb3.Ai
        public void onFinish() {
            FlexibleImageButton flexibleImageButton = this.a.r;
            Wg6.b(flexibleImageButton, "binding.fbCurrentLocation");
            flexibleImageButton.setEnabled(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public /* synthetic */ Bi(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final MapPickerFragment a(double d, double d2, String str) {
            Wg6.c(str, "address");
            MapPickerFragment mapPickerFragment = new MapPickerFragment();
            Bundle bundle = new Bundle();
            bundle.putDouble("latitude", d);
            bundle.putDouble("longitude", d2);
            bundle.putString("address", str);
            mapPickerFragment.setArguments(bundle);
            return mapPickerFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Sb3 {
        @DexIgnore
        public /* final */ /* synthetic */ MapPickerFragment b;

        @DexIgnore
        public Ci(MapPickerFragment mapPickerFragment) {
            this.b = mapPickerFragment;
        }

        @DexIgnore
        @Override // com.fossil.Sb3
        public final void onMapReady(Qb3 qb3) {
            this.b.j = qb3;
            Qb3 qb32 = this.b.j;
            if (qb32 != null) {
                qb32.y(this.b);
            }
            Qb3 qb33 = this.b.j;
            if (qb33 != null) {
                qb33.A(this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ MapPickerFragment b;

        @DexIgnore
        public Di(MapPickerFragment mapPickerFragment) {
            this.b = mapPickerFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ MapPickerFragment b;

        @DexIgnore
        public Ei(MapPickerFragment mapPickerFragment) {
            this.b = mapPickerFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            MapPickerFragment.M6(this.b).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ L85 b;
        @DexIgnore
        public /* final */ /* synthetic */ MapPickerFragment c;

        @DexIgnore
        public Fi(L85 l85, MapPickerFragment mapPickerFragment) {
            this.b = l85;
            this.c = mapPickerFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            Intent intent = new Intent();
            FlexibleTextView flexibleTextView = this.b.u;
            Wg6.b(flexibleTextView, "binding.tvTitle");
            String obj = flexibleTextView.getText().toString();
            Bundle bundle = new Bundle();
            Location m = MapPickerFragment.M6(this.c).m();
            if (m != null) {
                bundle.putParcelable(PlaceFields.LOCATION, m);
                bundle.putString("address", obj);
            }
            intent.putExtra(Constants.RESULT, bundle);
            FragmentActivity activity = this.c.getActivity();
            if (activity != null) {
                activity.setResult(100, intent);
            }
            FragmentActivity activity2 = this.c.getActivity();
            if (activity2 != null) {
                activity2.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> implements Ls0<MapPickerViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ MapPickerFragment a;

        @DexIgnore
        public Gi(MapPickerFragment mapPickerFragment) {
            this.a = mapPickerFragment;
        }

        @DexIgnore
        public final void a(MapPickerViewModel.Ai ai) {
            if (ai != null) {
                Integer c = ai.c();
                if (c != null) {
                    this.a.o(c.intValue(), "");
                }
                if (!(ai.d() == null || ai.e() == null)) {
                    this.a.E1(ai.d(), ai.e());
                }
                if (ai.a() != null) {
                    MapPickerFragment mapPickerFragment = this.a;
                    String a2 = ai.a();
                    if (a2 != null) {
                        mapPickerFragment.L2(a2);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                Boolean b = ai.b();
                if (b != null) {
                    b.booleanValue();
                    this.a.Q6();
                }
                Boolean g = ai.g();
                if (g != null) {
                    if (g.booleanValue()) {
                        this.a.i();
                    } else {
                        this.a.h();
                    }
                }
                Boolean f = ai.f();
                if (f != null) {
                    f.booleanValue();
                    Boolean f2 = ai.f();
                    if (f2 == null) {
                        Wg6.i();
                        throw null;
                    } else if (!f2.booleanValue()) {
                        this.a.j1();
                    }
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(MapPickerViewModel.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore
    public static final /* synthetic */ MapPickerViewModel M6(MapPickerFragment mapPickerFragment) {
        MapPickerViewModel mapPickerViewModel = mapPickerFragment.i;
        if (mapPickerViewModel != null) {
            return mapPickerViewModel;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void E1(Double d, Double d2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerFragment", "showLocation: latitude = " + d + ", longitude = " + d2);
        G37<L85> g37 = this.h;
        if (g37 != null) {
            L85 a2 = g37.a();
            if (a2 != null && d != null && d2 != null) {
                ImageView imageView = a2.s;
                Wg6.b(imageView, "binding.imgLocationPinUp");
                imageView.setVisibility(0);
                LatLng latLng = new LatLng(d.doubleValue(), d2.doubleValue());
                Qb3 qb3 = this.j;
                if (qb3 != null) {
                    FlexibleImageButton flexibleImageButton = a2.r;
                    Wg6.b(flexibleImageButton, "binding.fbCurrentLocation");
                    flexibleImageButton.setEnabled(false);
                    qb3.f(Pb3.d(latLng, 16.0f), new Ai(a2, latLng));
                    return;
                }
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void L2(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerFragment", "showAddress: address = " + str);
        G37<L85> g37 = this.h;
        if (g37 != null) {
            L85 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                Wg6.b(flexibleTextView, "it.tvTitle");
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        G37<L85> g37 = this.h;
        if (g37 != null) {
            L85 a2 = g37.a();
            if (a2 != null) {
                a2.q.d("flexible_button_primary");
                FlexibleButton flexibleButton = a2.q;
                Wg6.b(flexibleButton, "it.btConfirm");
                flexibleButton.setEnabled(true);
                FlexibleButton flexibleButton2 = a2.q;
                Wg6.b(flexibleButton2, "it.btConfirm");
                flexibleButton2.setClickable(true);
                FlexibleButton flexibleButton3 = a2.q;
                Wg6.b(flexibleButton3, "it.btConfirm");
                flexibleButton3.setFocusable(true);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void R6() {
        MapPickerViewModel mapPickerViewModel = this.i;
        if (mapPickerViewModel != null) {
            mapPickerViewModel.i().h(getViewLifecycleOwner(), new Gi(this));
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "hide Dialog Loading");
        a();
    }

    @DexIgnore
    public final void i() {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "show Dialog Loading");
        b();
    }

    @DexIgnore
    public final void j1() {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "showLocationPermissionError");
    }

    @DexIgnore
    public final void o(int i2, String str) {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "showErrorDialog");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        FragmentActivity activity;
        if (i2 == 111 && i3 == 2 && (activity = getActivity()) != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qb3.Bi
    public void onCameraIdle() {
        ImageView imageView;
        G37<L85> g37 = this.h;
        if (g37 != null) {
            L85 a2 = g37.a();
            if (!(a2 == null || (imageView = a2.s) == null)) {
                imageView.setImageResource(2131231099);
            }
            Le3 le3 = new Le3();
            Qb3 qb3 = this.j;
            CameraPosition h2 = qb3 != null ? qb3.h() : null;
            if (h2 != null) {
                le3.z0(h2.b);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("onCameraIdle: lat= ");
                Wg6.b(le3, "markerOptions");
                sb.append(le3.p0().b);
                sb.append(", long= ");
                sb.append(le3.p0().c);
                local.d("MapPickerFragment", sb.toString());
                if (le3.p0().b != 0.0d && le3.p0().c != 0.0d) {
                    MapPickerViewModel mapPickerViewModel = this.i;
                    if (mapPickerViewModel != null) {
                        mapPickerViewModel.g(le3.p0().b, le3.p0().c);
                    } else {
                        Wg6.n("mViewModel");
                        throw null;
                    }
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qb3.Di
    public void onCameraMoveStarted(int i2) {
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onCameraMoveStarted");
        G37<L85> g37 = this.h;
        if (g37 != null) {
            L85 a2 = g37.a();
            if (a2 != null) {
                a2.s.setImageResource(2131231098);
                FlexibleTextView flexibleTextView = a2.u;
                Wg6.b(flexibleTextView, "it.tvTitle");
                Hr7 hr7 = Hr7.a;
                String c = Um5.c(getContext(), 2131886552);
                Wg6.b(c, "LanguageHelper.getString\u2026on_DropPin_Text__Loading)");
                String format = String.format(c, Arrays.copyOf(new Object[0], 0));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                a2.q.d("flexible_button_disabled");
                FlexibleButton flexibleButton = a2.q;
                Wg6.b(flexibleButton, "it.btConfirm");
                flexibleButton.setEnabled(false);
                FlexibleButton flexibleButton2 = a2.q;
                Wg6.b(flexibleButton2, "it.btConfirm");
                flexibleButton2.setClickable(false);
                FlexibleButton flexibleButton3 = a2.q;
                Wg6.b(flexibleButton3, "it.btConfirm");
                flexibleButton3.setFocusable(false);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        FragmentManager supportFragmentManager;
        Wg6.c(layoutInflater, "inflater");
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onCreateView");
        L85 l85 = (L85) Aq0.e(LayoutInflater.from(getContext()), 2131558583, viewGroup, false);
        PortfolioApp.get.instance().getIface().x().a(this);
        Po4 po4 = this.k;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(MapPickerViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026kerViewModel::class.java)");
            this.i = (MapPickerViewModel) a2;
            l85.q.d("flexible_button_disabled");
            FragmentActivity activity = getActivity();
            SupportMapFragment supportMapFragment = (SupportMapFragment) ((activity == null || (supportFragmentManager = activity.getSupportFragmentManager()) == null) ? null : supportFragmentManager.Y(2131362251));
            if (supportMapFragment != null) {
                supportMapFragment.v6(new Ci(this));
            }
            R6();
            this.h = new G37<>(this, l85);
            Wg6.b(l85, "binding");
            return l85.n();
        }
        Wg6.n("appViewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onResume");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        double d;
        double d2;
        String string;
        Wg6.c(view, "view");
        FLogger.INSTANCE.getLocal().d("MapPickerFragment", "onViewCreated");
        super.onViewCreated(view, bundle);
        G37<L85> g37 = this.h;
        if (g37 != null) {
            L85 a2 = g37.a();
            if (a2 != null) {
                a2.t.setOnClickListener(new Di(this));
                a2.r.setOnClickListener(new Ei(this));
                a2.q.setOnClickListener(new Fi(a2, this));
                String str = "";
                if (getArguments() != null) {
                    double d3 = requireArguments().containsKey("latitude") ? requireArguments().getDouble("latitude") : 0.0d;
                    d = requireArguments().containsKey("longitude") ? requireArguments().getDouble("longitude") : 0.0d;
                    if (requireArguments().containsKey("address") && (string = requireArguments().getString("address")) != null) {
                        str = string;
                    }
                    d2 = d3;
                } else {
                    d = 0.0d;
                    d2 = 0.0d;
                }
                if ((d2 == 0.0d || d == 0.0d) && TextUtils.isEmpty(str)) {
                    MapPickerViewModel mapPickerViewModel = this.i;
                    if (mapPickerViewModel == null) {
                        Wg6.n("mViewModel");
                        throw null;
                    } else if (!mapPickerViewModel.d()) {
                    } else {
                        if (!Jn5.c(Jn5.b, requireActivity(), Jn5.Ai.FIND_DEVICE, false, true, false, null, 52, null)) {
                            FLogger.INSTANCE.getLocal().d("MapPickerFragment", "No permission granted");
                            return;
                        }
                        MapPickerViewModel mapPickerViewModel2 = this.i;
                        if (mapPickerViewModel2 != null) {
                            mapPickerViewModel2.j();
                        } else {
                            Wg6.n("mViewModel");
                            throw null;
                        }
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d("MapPickerFragment", "initStartLocation lat " + d2 + " long " + d + " address " + str);
                    MapPickerViewModel mapPickerViewModel3 = this.i;
                    if (mapPickerViewModel3 != null) {
                        mapPickerViewModel3.l(d2, d, str);
                    } else {
                        Wg6.n("mViewModel");
                        throw null;
                    }
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
