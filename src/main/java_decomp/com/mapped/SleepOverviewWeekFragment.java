package com.mapped;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.Ik6;
import com.fossil.Jk6;
import com.fossil.Mv5;
import com.fossil.Nb5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepWeekChart;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepOverviewWeekFragment extends BaseFragment implements Jk6 {
    @DexIgnore
    public G37<Nb5> g;
    @DexIgnore
    public Ik6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "SleepOverviewWeekFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void K6() {
        Nb5 a2;
        OverviewSleepWeekChart overviewSleepWeekChart;
        G37<Nb5> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewSleepWeekChart = a2.q) != null) {
            DeviceHelper.Ai ai = DeviceHelper.o;
            Ik6 ik6 = this.h;
            if (ai.w(ik6 != null ? ik6.n() : null)) {
                overviewSleepWeekChart.D("dianaSleepTab", "nonBrandNonReachGoal");
            } else {
                overviewSleepWeekChart.D("hybridSleepTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    public void L6(Ik6 ik6) {
        Wg6.c(ik6, "presenter");
        this.h = ik6;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Ik6 ik6) {
        L6(ik6);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Nb5 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onCreateView");
        this.g = new G37<>(this, (Nb5) Aq0.f(layoutInflater, 2131558627, viewGroup, false, A6()));
        K6();
        G37<Nb5> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onResume");
        K6();
        Ik6 ik6 = this.h;
        if (ik6 != null) {
            ik6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onStop");
        Ik6 ik6 = this.h;
        if (ik6 != null) {
            ik6.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.Jk6
    public void p(Mv5 mv5) {
        Nb5 a2;
        OverviewSleepWeekChart overviewSleepWeekChart;
        Wg6.c(mv5, "baseModel");
        FLogger.INSTANCE.getLocal().d("SleepOverviewWeekFragment", "showWeekDetails");
        G37<Nb5> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewSleepWeekChart = a2.q) != null) {
            new ArrayList();
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(Mv5.a.b(cVar.d()));
            Mv5.Ai ai = Mv5.a;
            Wg6.b(overviewSleepWeekChart, "it");
            Context context = overviewSleepWeekChart.getContext();
            Wg6.b(context, "it.context");
            BarChart.H(overviewSleepWeekChart, ai.a(context, cVar), false, 2, null);
            overviewSleepWeekChart.r(mv5);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
