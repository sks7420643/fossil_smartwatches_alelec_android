package com.mapped;

import com.fossil.Ak4;
import com.fossil.Fj4;
import com.fossil.Jj4;
import com.google.gson.JsonElement;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ku3 extends JsonElement {
    @DexIgnore
    public /* final */ Ak4<String, JsonElement> a; // = new Ak4<>();

    @DexIgnore
    public Set<Map.Entry<String, JsonElement>> entrySet() {
        return this.a.entrySet();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof Ku3) && ((Ku3) obj).a.equals(this.a));
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public void k(String str, JsonElement jsonElement) {
        if (jsonElement == null) {
            jsonElement = Fj4.a;
        }
        this.a.put(str, jsonElement);
    }

    @DexIgnore
    public void l(String str, Boolean bool) {
        k(str, o(bool));
    }

    @DexIgnore
    public void m(String str, Number number) {
        k(str, o(number));
    }

    @DexIgnore
    public void n(String str, String str2) {
        k(str, o(str2));
    }

    @DexIgnore
    public final JsonElement o(Object obj) {
        return obj == null ? Fj4.a : new Jj4(obj);
    }

    @DexIgnore
    public JsonElement p(String str) {
        return this.a.get(str);
    }

    @DexIgnore
    public Fu3 q(String str) {
        return (Fu3) this.a.get(str);
    }

    @DexIgnore
    public Ku3 r(String str) {
        return (Ku3) this.a.get(str);
    }

    @DexIgnore
    public boolean s(String str) {
        return this.a.containsKey(str);
    }

    @DexIgnore
    public int size() {
        return this.a.size();
    }
}
