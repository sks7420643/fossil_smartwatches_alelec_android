package com.mapped;

import com.fossil.Lj5;
import com.fossil.St5;
import com.fossil.Sy6;
import com.fossil.Tt5;
import com.fossil.Ty6;
import com.fossil.Ut5;
import com.fossil.Uy6;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cj4 {
    @DexIgnore
    public /* final */ GetDianaDeviceSettingUseCase a;
    @DexIgnore
    public /* final */ GetHybridDeviceSettingUseCase b;
    @DexIgnore
    public /* final */ HybridSyncUseCase c;
    @DexIgnore
    public /* final */ DianaSyncUseCase d;

    @DexIgnore
    public Cj4(GetDianaDeviceSettingUseCase getDianaDeviceSettingUseCase, GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, HybridSyncUseCase hybridSyncUseCase, DianaSyncUseCase dianaSyncUseCase) {
        Wg6.c(getDianaDeviceSettingUseCase, "mGetDianaDeviceSettingUseCase");
        Wg6.c(getHybridDeviceSettingUseCase, "mGetHybridDeviceSettingUseCase");
        Wg6.c(hybridSyncUseCase, "mHybridSyncUseCase");
        Wg6.c(dianaSyncUseCase, "mDianaSyncUseCase");
        this.a = getDianaDeviceSettingUseCase;
        this.b = getHybridDeviceSettingUseCase;
        this.c = hybridSyncUseCase;
        this.d = dianaSyncUseCase;
    }

    @DexIgnore
    public final CoroutineUseCase<Ty6, Uy6, Sy6> a(String str) {
        Wg6.c(str, "serial");
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
        if (deviceBySerial != null) {
            int i = Lj5.a[deviceBySerial.ordinal()];
            if (i == 1 || i == 2 || i == 3) {
                return this.b;
            }
            if (i == 4 || i == 5) {
                return this.a;
            }
        }
        return this.b;
    }

    @DexIgnore
    public final CoroutineUseCase<Tt5, Ut5, St5> b(String str) {
        Wg6.c(str, "serial");
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
        if (deviceBySerial != null) {
            int i = Lj5.b[deviceBySerial.ordinal()];
            if (i == 1 || i == 2 || i == 3) {
                return this.c;
            }
            if (i == 4 || i == 5) {
                return this.d;
            }
        }
        return this.d;
    }
}
