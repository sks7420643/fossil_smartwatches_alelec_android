package com.mapped;

import android.content.Intent;
import com.fossil.Ao7;
import com.fossil.J06;
import com.fossil.Ll5;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.setting.SpecialSkuSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.util.NotificationAppHelper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetNotificationFiltersUserCase extends CoroutineUseCase<Ai, Ci, Bi> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ Di e; // = new Di();
    @DexIgnore
    public /* final */ NotificationsRepository f;
    @DexIgnore
    public /* final */ DeviceRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ List<J06> a;
        @DexIgnore
        public /* final */ List<AppWrapper> b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public Ai(List<J06> list, List<AppWrapper> list2, int i) {
            Wg6.c(list, "contactWrapperList");
            Wg6.c(list2, "appWrapperList");
            this.a = list;
            this.b = list2;
            this.c = i;
        }

        @DexIgnore
        public final List<AppWrapper> a() {
            return this.b;
        }

        @DexIgnore
        public final List<J06> b() {
            return this.a;
        }

        @DexIgnore
        public final int c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public Bi(int i, int i2, ArrayList<Integer> arrayList) {
            Wg6.c(arrayList, "errorCodes");
            this.a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di implements BleCommandResultManager.Bi {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            boolean z = false;
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetNotificationFiltersUserCase", "Inside .bleReceiver communicateMode= " + communicateMode);
            if (communicateMode == CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS && SetNotificationFiltersUserCase.this.p()) {
                SetNotificationFiltersUserCase.this.s(false);
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    SetNotificationFiltersUserCase.this.j(new Ci());
                    return;
                }
                FLogger.INSTANCE.getLocal().d("SetNotificationFiltersUserCase", "onReceive failed");
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                SetNotificationFiltersUserCase.this.i(new Bi(FailureCode.FAILED_TO_CONNECT, intExtra, integerArrayListExtra));
            }
        }
    }

    @DexIgnore
    public SetNotificationFiltersUserCase(NotificationsRepository notificationsRepository, DeviceRepository deviceRepository) {
        Wg6.c(notificationsRepository, "mNotificationsRepository");
        Wg6.c(deviceRepository, "mDeviceRepository");
        this.f = notificationsRepository;
        this.g = deviceRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "SetNotificationFiltersUserCase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ai ai, Xe6 xe6) {
        return r(ai, xe6);
    }

    @DexIgnore
    public final List<AppNotificationFilter> m(List<AppWrapper> list, short s, boolean z) {
        ArrayList arrayList = new ArrayList();
        short angleForApp = !z ? -1 : (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForApp();
        for (AppWrapper appWrapper : list) {
            InstalledApp installedApp = appWrapper.getInstalledApp();
            if (installedApp != null) {
                Boolean isSelected = installedApp.isSelected();
                Wg6.b(isSelected, "it.isSelected");
                if (isSelected.booleanValue()) {
                    String title = installedApp.getTitle() == null ? "" : installedApp.getTitle();
                    NotificationHandMovingConfig notificationHandMovingConfig = new NotificationHandMovingConfig(s, s, angleForApp, 10000);
                    Wg6.b(title, "appName");
                    String identifier = installedApp.getIdentifier();
                    Wg6.b(identifier, "it.identifier");
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(title, identifier, "", NotificationBaseObj.ANotificationType.NOTIFICATION));
                    appNotificationFilter.setHandMovingConfig(notificationHandMovingConfig);
                    appNotificationFilter.setVibePattern(NotificationVibePattern.DEFAULT_OTHER_APPS);
                    arrayList.add(appNotificationFilter);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<AppNotificationFilter> n(List<J06> list, short s, boolean z) {
        Contact contact;
        short s2;
        ArrayList arrayList = new ArrayList();
        for (J06 j06 : list) {
            if (j06.isAdded() && (contact = j06.getContact()) != null) {
                if (contact.isUseCall()) {
                    s2 = z ? (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForCall() : -1;
                    NotificationHandMovingConfig notificationHandMovingConfig = new NotificationHandMovingConfig(s, s, s2, 10000);
                    DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), "", phone_incoming_call.getNotificationType()));
                    appNotificationFilter.setSender(contact.getDisplayName());
                    appNotificationFilter.setHandMovingConfig(notificationHandMovingConfig);
                    appNotificationFilter.setVibePattern(NotificationVibePattern.CALL);
                    arrayList.add(appNotificationFilter);
                } else {
                    s2 = -1;
                }
                if (contact.isUseSms()) {
                    if (z) {
                        s2 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForSms();
                    }
                    NotificationHandMovingConfig notificationHandMovingConfig2 = new NotificationHandMovingConfig(s, s, s2, 10000);
                    DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType()));
                    appNotificationFilter2.setSender(contact.getDisplayName());
                    appNotificationFilter2.setHandMovingConfig(notificationHandMovingConfig2);
                    appNotificationFilter2.setVibePattern(NotificationVibePattern.TEXT);
                    arrayList.add(appNotificationFilter2);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0243  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0083 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x01a8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x01c8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter> o(android.util.SparseArray<java.util.List<com.fossil.wearables.fsl.shared.BaseFeatureModel>> r22, java.util.List<com.fossil.J06> r23, java.util.List<com.mapped.AppWrapper> r24, short r25, boolean r26) {
        /*
        // Method dump skipped, instructions count: 585
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.SetNotificationFiltersUserCase.o(android.util.SparseArray, java.util.List, java.util.List, short, boolean):java.util.List");
    }

    @DexIgnore
    public final boolean p() {
        return this.d;
    }

    @DexIgnore
    public final void q() {
        BleCommandResultManager.d.e(this.e, CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public Object r(Ai ai, Xe6<Object> xe6) {
        Integer num = null;
        try {
            FLogger.INSTANCE.getLocal().d("SetNotificationFiltersUserCase", "running UseCase");
            this.d = true;
            List<J06> b = ai != null ? ai.b() : null;
            List<AppWrapper> a2 = ai != null ? ai.a() : null;
            if (ai != null) {
                num = Ao7.e(ai.c());
            }
            if (b == null || a2 == null || num == null) {
                i(new Bi(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG, -1, new ArrayList()));
            } else {
                String J = PortfolioApp.get.instance().J();
                boolean j = NotificationAppHelper.b.j(this.g.getSkuModelBySerialPrefix(DeviceHelper.o.m(J)), J);
                short e2 = (short) Ll5.e(num.intValue());
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(o(this.f.getAllNotificationsByHour(J, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()), b, a2, e2, j));
                arrayList.addAll(n(b, e2, j));
                arrayList.addAll(m(a2, e2, j));
                AppNotificationFilterSettings appNotificationFilterSettings = new AppNotificationFilterSettings(arrayList, System.currentTimeMillis());
                PortfolioApp.get.instance().s1(appNotificationFilterSettings, J);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetNotificationFiltersUserCase", "saveNotificationSettingToDevice, total: " + appNotificationFilterSettings.getNotificationFilters().size() + " items");
            }
            return new Object();
        } catch (Exception e3) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.e("SetNotificationFiltersUserCase", "Error inside SetNotificationFiltersUserCase.connectDevice - e=" + e3);
            return new Bi(600, -1, new ArrayList());
        }
    }

    @DexIgnore
    public final void s(boolean z) {
        this.d = z;
    }
}
