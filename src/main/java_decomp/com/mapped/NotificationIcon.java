package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Hy1;
import com.fossil.Nu1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NotificationIcon extends Nu1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<NotificationIcon> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public NotificationIcon createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                byte[] createByteArray = parcel.createByteArray();
                if (createByteArray != null) {
                    Wg6.b(createByteArray, "parcel.createByteArray()!!");
                    return new NotificationIcon(readString, createByteArray);
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public NotificationIcon[] newArray(int i) {
            return new NotificationIcon[i];
        }
    }

    @DexIgnore
    public NotificationIcon(String str, byte[] bArr) {
        super(str, bArr);
    }

    @DexIgnore
    @Override // com.fossil.Nu1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            NotificationIcon notificationIcon = (NotificationIcon) obj;
            return (b() == notificationIcon.b() || Wg6.a(Hy1.k((int) b(), null, 1, null), notificationIcon.getFileName())) ? true : Wg6.a(getFileName(), Hy1.k((int) notificationIcon.b(), null, 1, null));
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.file.NotificationIcon");
    }

    @DexIgnore
    @Override // com.fossil.Nu1
    public int hashCode() {
        return (int) b();
    }
}
