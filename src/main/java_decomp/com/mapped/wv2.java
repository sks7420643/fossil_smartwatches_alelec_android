package com.mapped;

import android.content.Context;
import android.location.Location;
import android.os.Looper;
import com.fossil.Bb3;
import com.fossil.Cb3;
import com.fossil.Db3;
import com.fossil.Eb3;
import com.fossil.F72;
import com.fossil.Ha3;
import com.fossil.Ir2;
import com.fossil.M62;
import com.fossil.Nt3;
import com.fossil.Ot3;
import com.fossil.P72;
import com.fossil.Pq2;
import com.fossil.Pr2;
import com.fossil.Q62;
import com.fossil.Q72;
import com.fossil.Rq2;
import com.fossil.Sq2;
import com.fossil.X72;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wv2 extends Q62<M62.Di.Dii> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Sq2 {
        @DexIgnore
        public /* final */ Ot3<Void> b;

        @DexIgnore
        public Ai(Ot3<Void> ot3) {
            this.b = ot3;
        }

        @DexIgnore
        @Override // com.fossil.Rq2
        public final void M(Pq2 pq2) {
            X72.a(pq2.a(), this.b);
        }
    }

    @DexIgnore
    public Wv2(Context context) {
        super(context, Ha3.c, (M62.Di) null, new F72());
    }

    @DexIgnore
    public Nt3<Location> s() {
        return e(new Bb3(this));
    }

    @DexIgnore
    public Nt3<Void> t(Yv2 yv2) {
        return X72.c(g(Q72.b(yv2, Yv2.class.getSimpleName())));
    }

    @DexIgnore
    public Nt3<Void> u(LocationRequest locationRequest, Yv2 yv2, Looper looper) {
        Ir2 c = Ir2.c(locationRequest);
        P72 a2 = Q72.a(yv2, Pr2.a(looper), Yv2.class.getSimpleName());
        return f(new Cb3(this, a2, c, a2), new Db3(this, a2.b()));
    }

    @DexIgnore
    public final Rq2 w(Ot3<Boolean> ot3) {
        return new Eb3(this, ot3);
    }
}
