package com.mapped;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum H70 {
    C(0),
    F(1);
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final H70 a(int i) {
            H70[] values = H70.values();
            for (H70 h70 : values) {
                if (h70.a() == i) {
                    return h70;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public H70(int i) {
        this.b = i;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }
}
