package com.mapped;

import com.fossil.Nx0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Mi extends Nx0 {
    @DexIgnore
    long executeInsert();

    @DexIgnore
    int executeUpdateDelete();
}
