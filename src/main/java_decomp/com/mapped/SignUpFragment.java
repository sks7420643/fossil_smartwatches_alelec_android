package com.mapped;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.Aq0;
import com.fossil.Db5;
import com.fossil.G37;
import com.fossil.H37;
import com.fossil.Oz6;
import com.fossil.Pz6;
import com.fossil.S37;
import com.fossil.Vl5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.login.AppleAuthorizationActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.signup.verification.EmailOtpVerificationActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SignUpFragment extends BaseFragment implements Pz6 {
    @DexIgnore
    public static /* final */ Ai j; // = new Ai(null);
    @DexIgnore
    public Oz6 g;
    @DexIgnore
    public G37<Db5> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final SignUpFragment a() {
            return new SignUpFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Db5 b;

            @DexIgnore
            public Aii(Db5 db5) {
                this.b = db5;
            }

            @DexIgnore
            public final void run() {
                FlexibleTextView flexibleTextView = this.b.L;
                Wg6.b(flexibleTextView, "it.tvHaveAccount");
                flexibleTextView.setVisibility(0);
                FlexibleButton flexibleButton = this.b.M;
                Wg6.b(flexibleButton, "it.tvLogin");
                flexibleButton.setVisibility(0);
            }
        }

        @DexIgnore
        public Bi(View view, SignUpFragment signUpFragment) {
            this.b = view;
            this.c = signUpFragment;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.b.getWindowVisibleDisplayFrame(rect);
            int height = this.b.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                Db5 db5 = (Db5) SignUpFragment.K6(this.c).a();
                if (db5 != null) {
                    try {
                        FlexibleTextView flexibleTextView = db5.L;
                        Wg6.b(flexibleTextView, "it.tvHaveAccount");
                        flexibleTextView.setVisibility(8);
                        FlexibleButton flexibleButton = db5.M;
                        Wg6.b(flexibleButton, "it.tvLogin");
                        flexibleButton.setVisibility(8);
                        Cd6 cd6 = Cd6.a;
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("SignUpFragment", "onCreateView - e=" + e);
                        Cd6 cd62 = Cd6.a;
                    }
                }
            } else {
                Db5 db52 = (Db5) SignUpFragment.K6(this.c).a();
                if (db52 != null) {
                    try {
                        Wg6.b(db52, "it");
                        db52.n().postDelayed(new Aii(db52), 100);
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("SignUpFragment", "onCreateView - e=" + e2);
                        Cd6 cd63 = Cd6.a;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment b;

        @DexIgnore
        public Ci(SignUpFragment signUpFragment) {
            this.b = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpFragment.L6(this.b).v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment b;

        @DexIgnore
        public Di(SignUpFragment signUpFragment) {
            this.b = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpFragment.L6(this.b).w();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment b;

        @DexIgnore
        public Ei(SignUpFragment signUpFragment) {
            this.b = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            LoginActivity.a aVar = LoginActivity.F;
            Wg6.b(view, "it");
            Context context = view.getContext();
            Wg6.b(context, "it.context");
            aVar.a(context);
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Db5 b;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment c;

        @DexIgnore
        public Fi(Db5 db5, SignUpFragment signUpFragment) {
            this.b = db5;
            this.c = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FlexibleTextInputLayout flexibleTextInputLayout = this.b.w;
            Wg6.b(flexibleTextInputLayout, "binding.inputPassword");
            flexibleTextInputLayout.setErrorEnabled(false);
            FlexibleTextInputLayout flexibleTextInputLayout2 = this.b.v;
            Wg6.b(flexibleTextInputLayout2, "binding.inputEmail");
            flexibleTextInputLayout2.setErrorEnabled(false);
            this.c.O6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment b;

        @DexIgnore
        public Gi(SignUpFragment signUpFragment) {
            this.b = signUpFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            SignUpFragment.L6(this.b).p(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment b;

        @DexIgnore
        public Hi(SignUpFragment signUpFragment) {
            this.b = signUpFragment;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            SignUpFragment.L6(this.b).o(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment b;

        @DexIgnore
        public Ii(SignUpFragment signUpFragment) {
            this.b = signUpFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            SignUpFragment.L6(this.b).q(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements TextView.OnEditorActionListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment a;

        @DexIgnore
        public Ji(SignUpFragment signUpFragment) {
            this.a = signUpFragment;
        }

        @DexIgnore
        public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((i & 255) != 6) {
                return false;
            }
            FLogger.INSTANCE.getLocal().d("SignUpFragment", "Password DONE key, trigger sign up flow");
            this.a.O6();
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment b;

        @DexIgnore
        public Ki(SignUpFragment signUpFragment) {
            this.b = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment b;

        @DexIgnore
        public Li(SignUpFragment signUpFragment) {
            this.b = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpFragment.L6(this.b).t();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment b;

        @DexIgnore
        public Mi(SignUpFragment signUpFragment) {
            this.b = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpFragment.L6(this.b).u();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpFragment b;

        @DexIgnore
        public Ni(SignUpFragment signUpFragment) {
            this.b = signUpFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpFragment.L6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Db5 b;

        @DexIgnore
        public Oi(Db5 db5) {
            this.b = db5;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                FlexibleTextView flexibleTextView = this.b.J;
                Wg6.b(flexibleTextView, "binding.tvErrorCheckCharacter");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = this.b.K;
                Wg6.b(flexibleTextView2, "binding.tvErrorCheckCombine");
                flexibleTextView2.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView3 = this.b.J;
            Wg6.b(flexibleTextView3, "binding.tvErrorCheckCharacter");
            flexibleTextView3.setVisibility(8);
            FlexibleTextView flexibleTextView4 = this.b.K;
            Wg6.b(flexibleTextView4, "binding.tvErrorCheckCombine");
            flexibleTextView4.setVisibility(8);
        }
    }

    @DexIgnore
    public static final /* synthetic */ G37 K6(SignUpFragment signUpFragment) {
        G37<Db5> g37 = signUpFragment.h;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Oz6 L6(SignUpFragment signUpFragment) {
        Oz6 oz6 = signUpFragment.g;
        if (oz6 != null) {
            return oz6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void B4() {
        if (isActive()) {
            G37<Db5> g37 = this.h;
            if (g37 != null) {
                Db5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.s;
                    Wg6.b(flexibleButton, "it.btContinue");
                    flexibleButton.setEnabled(false);
                    FlexibleButton flexibleButton2 = a2.s;
                    Wg6.b(flexibleButton2, "it.btContinue");
                    flexibleButton2.setClickable(false);
                    FlexibleButton flexibleButton3 = a2.s;
                    Wg6.b(flexibleButton3, "it.btContinue");
                    flexibleButton3.setFocusable(false);
                    a2.s.d("flexible_button_disabled");
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void C(int i2, String str) {
        Wg6.c(str, "errorMessage");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "SignUpFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Oz6 oz6) {
        N6(oz6);
    }

    @DexIgnore
    public void N6(Oz6 oz6) {
        Wg6.c(oz6, "presenter");
        this.g = oz6;
    }

    @DexIgnore
    public final void O6() {
        G37<Db5> g37 = this.h;
        if (g37 != null) {
            Db5 a2 = g37.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.s;
                Wg6.b(flexibleButton, "this.btContinue");
                if (flexibleButton.isEnabled()) {
                    Oz6 oz6 = this.g;
                    if (oz6 != null) {
                        FlexibleTextInputEditText flexibleTextInputEditText = a2.t;
                        Wg6.b(flexibleTextInputEditText, "etEmail");
                        String valueOf = String.valueOf(flexibleTextInputEditText.getText());
                        FlexibleTextInputEditText flexibleTextInputEditText2 = a2.u;
                        Wg6.b(flexibleTextInputEditText2, "etPassword");
                        oz6.s(valueOf, String.valueOf(flexibleTextInputEditText2.getText()));
                        return;
                    }
                    Wg6.n("mPresenter");
                    throw null;
                }
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void R() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            String a2 = H37.b.a(5);
            AppleAuthorizationActivity.a aVar = AppleAuthorizationActivity.E;
            Wg6.b(activity, "it");
            aVar.a(activity, a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void U3(boolean z, boolean z2) {
        if (isActive()) {
            G37<Db5> g37 = this.h;
            if (g37 != null) {
                Db5 a2 = g37.a();
                if (a2 != null) {
                    if (z) {
                        a2.J.setCompoundDrawablesWithIntrinsicBounds(W6.f(PortfolioApp.get.instance(), 2131231114), (Drawable) null, (Drawable) null, (Drawable) null);
                    } else {
                        a2.J.setCompoundDrawablesWithIntrinsicBounds(W6.f(PortfolioApp.get.instance(), 2131230967), (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                    if (z2) {
                        a2.K.setCompoundDrawablesWithIntrinsicBounds(W6.f(PortfolioApp.get.instance(), 2131231114), (Drawable) null, (Drawable) null, (Drawable) null);
                    } else {
                        a2.K.setCompoundDrawablesWithIntrinsicBounds(W6.f(PortfolioApp.get.instance(), 2131230967), (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void d3(boolean z) {
        if (isActive()) {
            G37<Db5> g37 = this.h;
            if (g37 != null) {
                Db5 a2 = g37.a();
                if (a2 == null) {
                    return;
                }
                if (z) {
                    ConstraintLayout constraintLayout = a2.D;
                    Wg6.b(constraintLayout, "it.ivWeibo");
                    constraintLayout.setVisibility(0);
                    ConstraintLayout constraintLayout2 = a2.C;
                    Wg6.b(constraintLayout2, "it.ivWechat");
                    constraintLayout2.setVisibility(0);
                    ImageView imageView = a2.B;
                    Wg6.b(imageView, "it.ivGoogle");
                    imageView.setVisibility(8);
                    ImageView imageView2 = a2.A;
                    Wg6.b(imageView2, "it.ivFacebook");
                    imageView2.setVisibility(8);
                    return;
                }
                ConstraintLayout constraintLayout3 = a2.D;
                Wg6.b(constraintLayout3, "it.ivWeibo");
                constraintLayout3.setVisibility(8);
                ConstraintLayout constraintLayout4 = a2.C;
                Wg6.b(constraintLayout4, "it.ivWechat");
                constraintLayout4.setVisibility(8);
                ImageView imageView3 = a2.B;
                Wg6.b(imageView3, "it.ivGoogle");
                imageView3.setVisibility(0);
                ImageView imageView4 = a2.A;
                Wg6.b(imageView4, "it.ivFacebook");
                imageView4.setVisibility(0);
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void f() {
        G37<Db5> g37 = this.h;
        if (g37 != null) {
            Db5 a2 = g37.a();
            if (a2 != null) {
                ObjectAnimator ofInt = ObjectAnimator.ofInt(a2.F, "progress", 0, 10);
                Wg6.b(ofInt, "progressAnimator");
                ofInt.setDuration(500L);
                ofInt.start();
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void g4(SignUpSocialAuth signUpSocialAuth) {
        Wg6.c(signUpSocialAuth, "socialAuth");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ProfileSetupActivity.a aVar = ProfileSetupActivity.B;
            Wg6.b(activity, "it");
            aVar.b(activity, signUpSocialAuth);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void g6(String str) {
        Wg6.c(str, "errorMessage");
        G37<Db5> g37 = this.h;
        if (g37 != null) {
            Db5 a2 = g37.a();
            if (a2 != null) {
                RTLImageView rTLImageView = a2.y;
                Wg6.b(rTLImageView, "it.ivCheckedEmail");
                if (rTLImageView.getVisibility() == 0) {
                    RTLImageView rTLImageView2 = a2.y;
                    Wg6.b(rTLImageView2, "it.ivCheckedEmail");
                    rTLImageView2.setVisibility(8);
                }
                FlexibleTextInputLayout flexibleTextInputLayout = a2.v;
                Wg6.b(flexibleTextInputLayout, "it.inputEmail");
                flexibleTextInputLayout.setErrorEnabled(true);
                FlexibleTextInputLayout flexibleTextInputLayout2 = a2.v;
                Wg6.b(flexibleTextInputLayout2, "it.inputEmail");
                flexibleTextInputLayout2.setError(str);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void h() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void i() {
        String string = getString(2131887002);
        Wg6.b(string, "getString(R.string.Onboa\u2026Account_Text__PleaseWait)");
        H6(string);
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void l() {
        if (getActivity() != null) {
            HomeActivity.a aVar = HomeActivity.B;
            FragmentActivity requireActivity = requireActivity();
            Wg6.b(requireActivity, "requireActivity()");
            HomeActivity.a.b(aVar, requireActivity, null, 2, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void l3(int i2, String str) {
        Wg6.c(str, "errorMessage");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.O(childFragmentManager, i2, str);
        }
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void n0(boolean z, boolean z2, String str) {
        RTLImageView rTLImageView;
        Wg6.c(str, "errorMessage");
        if (isActive()) {
            G37<Db5> g37 = this.h;
            if (g37 != null) {
                Db5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextInputLayout flexibleTextInputLayout = a2.v;
                    Wg6.b(flexibleTextInputLayout, "it.inputEmail");
                    flexibleTextInputLayout.setErrorEnabled(z2);
                    FlexibleTextInputLayout flexibleTextInputLayout2 = a2.v;
                    Wg6.b(flexibleTextInputLayout2, "it.inputEmail");
                    flexibleTextInputLayout2.setError(str);
                }
                G37<Db5> g372 = this.h;
                if (g372 != null) {
                    Db5 a3 = g372.a();
                    if (a3 != null && (rTLImageView = a3.y) != null) {
                        Wg6.b(rTLImageView, "it");
                        rTLImageView.setVisibility(z ? 0 : 8);
                        return;
                    }
                    return;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 3535) {
            return;
        }
        if (i3 != -1) {
            FLogger.INSTANCE.getLocal().e("SignUpFragment", "Something went wrong with Apple login");
        } else if (intent != null) {
            SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) intent.getParcelableExtra("USER_INFO_EXTRA");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SignUpFragment", "Apple Auth Info = " + signUpSocialAuth);
            Oz6 oz6 = this.g;
            if (oz6 != null) {
                Wg6.b(signUpSocialAuth, "authCode");
                oz6.r(signUpSocialAuth);
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        Db5 db5 = (Db5) Aq0.f(layoutInflater, 2131558622, viewGroup, false, A6());
        Wg6.b(db5, "bindingLocal");
        View n = db5.n();
        Wg6.b(n, "bindingLocal.root");
        n.getViewTreeObserver().addOnGlobalLayoutListener(new Bi(n, this));
        G37<Db5> g37 = new G37<>(this, db5);
        this.h = g37;
        if (g37 != null) {
            Db5 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Oz6 oz6 = this.g;
        if (oz6 != null) {
            oz6.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Oz6 oz6 = this.g;
        if (oz6 != null) {
            oz6.l();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<Db5> g37 = this.h;
        if (g37 != null) {
            Db5 a2 = g37.a();
            if (a2 != null) {
                a2.s.setOnClickListener(new Fi(a2, this));
                a2.t.addTextChangedListener(new Gi(this));
                a2.t.setOnFocusChangeListener(new Hi(this));
                a2.u.addTextChangedListener(new Ii(this));
                a2.u.setOnFocusChangeListener(new Oi(a2));
                a2.u.setOnEditorActionListener(new Ji(this));
                a2.z.setOnClickListener(new Ki(this));
                a2.A.setOnClickListener(new Li(this));
                a2.B.setOnClickListener(new Mi(this));
                a2.x.setOnClickListener(new Ni(this));
                a2.C.setOnClickListener(new Ci(this));
                a2.D.setOnClickListener(new Di(this));
                a2.M.setOnClickListener(new Ei(this));
            }
            E6("sign_up_view");
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void x2() {
        if (isActive()) {
            G37<Db5> g37 = this.h;
            if (g37 != null) {
                Db5 a2 = g37.a();
                if (a2 != null) {
                    FlexibleButton flexibleButton = a2.s;
                    Wg6.b(flexibleButton, "it.btContinue");
                    flexibleButton.setEnabled(true);
                    FlexibleButton flexibleButton2 = a2.s;
                    Wg6.b(flexibleButton2, "it.btContinue");
                    flexibleButton2.setClickable(true);
                    FlexibleButton flexibleButton3 = a2.s;
                    Wg6.b(flexibleButton3, "it.btContinue");
                    flexibleButton3.setFocusable(true);
                    a2.s.d("flexible_button_primary");
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Pz6
    public void y3(SignUpEmailAuth signUpEmailAuth) {
        FragmentActivity activity;
        Wg6.c(signUpEmailAuth, "emailAuth");
        if (isActive() && (activity = getActivity()) != null) {
            EmailOtpVerificationActivity.a aVar = EmailOtpVerificationActivity.B;
            Wg6.b(activity, "it");
            aVar.a(activity, signUpEmailAuth);
        }
    }
}
