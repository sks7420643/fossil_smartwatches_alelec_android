package com.mapped;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.Aq0;
import com.fossil.Aw5;
import com.fossil.G37;
import com.fossil.Hl5;
import com.fossil.Hr7;
import com.fossil.Jn5;
import com.fossil.Kb6;
import com.fossil.Lb6;
import com.fossil.Ln0;
import com.fossil.M66;
import com.fossil.N04;
import com.fossil.Qv5;
import com.fossil.Um5;
import com.fossil.Vl5;
import com.fossil.Vt7;
import com.fossil.Wz5;
import com.fossil.Z75;
import com.google.android.material.tabs.TabLayout;
import com.mapped.AlertDialogFragment;
import com.mapped.HybridCustomizePresetDetailAdapter;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeHybridCustomizeFragment extends Qv5 implements Lb6, View.OnClickListener, HybridCustomizePresetDetailAdapter.Ci, AlertDialogFragment.Gi, Aw5 {
    @DexIgnore
    public static /* final */ Ai x; // = new Ai(null);
    @DexIgnore
    public Kb6 h;
    @DexIgnore
    public ConstraintLayout i;
    @DexIgnore
    public ViewPager2 j;
    @DexIgnore
    public int k;
    @DexIgnore
    public HybridCustomizePresetDetailAdapter l;
    @DexIgnore
    public G37<Z75> m;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public HashMap w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final HomeHybridCustomizeFragment a() {
            return new HomeHybridCustomizeFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizeFragment a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(HomeHybridCustomizeFragment homeHybridCustomizeFragment) {
            this.a = homeHybridCustomizeFragment;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            TabLayout tabLayout;
            TabLayout.g v;
            Drawable e;
            TabLayout tabLayout2;
            TabLayout.g v2;
            Drawable e2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "onPageScrolled position " + i + " mCurrentPosition " + this.a.N6());
            super.b(i, f, i2);
            if (!TextUtils.isEmpty(this.a.t)) {
                int parseColor = Color.parseColor(this.a.t);
                Z75 z75 = (Z75) HomeHybridCustomizeFragment.L6(this.a).a();
                if (!(z75 == null || (tabLayout2 = z75.z) == null || (v2 = tabLayout2.v(i)) == null || (e2 = v2.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.a.s) && this.a.N6() != i) {
                int parseColor2 = Color.parseColor(this.a.s);
                Z75 z752 = (Z75) HomeHybridCustomizeFragment.L6(this.a).a();
                if (!(z752 == null || (tabLayout = z752.z) == null || (v = tabLayout.v(this.a.N6())) == null || (e = v.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.a.Q6(i);
            this.a.O6().n(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Wz5.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizeFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public Ci(HomeHybridCustomizeFragment homeHybridCustomizeFragment, String str) {
            this.a = homeHybridCustomizeFragment;
            this.b = str;
        }

        @DexIgnore
        @Override // com.fossil.Wz5.Bi
        public void a(String str) {
            Wg6.c(str, "presetName");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "showRenamePresetDialog - presetName=" + str);
            if (!TextUtils.isEmpty(str)) {
                this.a.O6().q(str, this.b);
            }
        }

        @DexIgnore
        @Override // com.fossil.Wz5.Bi
        public void onCancel() {
            FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "showRenamePresetDialog - onCancel");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements N04.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizeFragment a;

        @DexIgnore
        public Di(HomeHybridCustomizeFragment homeHybridCustomizeFragment) {
            this.a = homeHybridCustomizeFragment;
        }

        @DexIgnore
        @Override // com.fossil.N04.Bi
        public final void a(TabLayout.g gVar, int i) {
            Wg6.c(gVar, "tab");
            int itemCount = this.a.getItemCount();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "tabLayoutConfig dataSize=" + itemCount);
            if (!TextUtils.isEmpty(this.a.s)) {
                int parseColor = Color.parseColor(this.a.s);
                if (i == 0) {
                    gVar.o(2131230943);
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor);
                    }
                } else if (i == itemCount - 1) {
                    gVar.o(2131230817);
                    Drawable e2 = gVar.e();
                    if (e2 != null) {
                        e2.setTint(parseColor);
                    }
                } else {
                    gVar.o(2131230966);
                    Drawable e3 = gVar.e();
                    if (e3 != null) {
                        e3.setTint(parseColor);
                    }
                }
                if (!TextUtils.isEmpty(this.a.t) && i == this.a.N6()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("HomeHybridCustomizeFragment", "tabLayoutConfig primary mCurrentPosition " + this.a.N6());
                    Drawable e4 = gVar.e();
                    if (e4 != null) {
                        e4.setTint(Color.parseColor(this.a.t));
                    }
                }
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ G37 L6(HomeHybridCustomizeFragment homeHybridCustomizeFragment) {
        G37<Z75> g37 = homeHybridCustomizeFragment.m;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Lb6
    public void C5(String str) {
        Wg6.c(str, "microAppId");
        if (isActive()) {
            Jn5.c(Jn5.b, requireContext(), Hl5.a.d(str), false, false, false, null, 60, null);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "HomeHybridCustomizeFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Lb6
    public void J2(List<M66> list) {
        Wg6.c(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizeFragment", "showPresets - data=" + list.size());
        HybridCustomizePresetDetailAdapter hybridCustomizePresetDetailAdapter = this.l;
        if (hybridCustomizePresetDetailAdapter != null) {
            hybridCustomizePresetDetailAdapter.j(new ArrayList<>(list));
            if (!this.v && this.k == 0) {
                this.v = false;
            }
            if (!this.v) {
                ViewPager2 viewPager2 = this.j;
                if (viewPager2 != null) {
                    viewPager2.setCurrentItem(this.k);
                } else {
                    Wg6.n("rvCustomize");
                    throw null;
                }
            }
        } else {
            Wg6.n("mHybridPresetDetailAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        R6((Kb6) obj);
    }

    @DexIgnore
    @Override // com.mapped.HybridCustomizePresetDetailAdapter.Ci
    public void N(String str, String str2) {
        Wg6.c(str, "presetName");
        Wg6.c(str2, "presetId");
        if (getChildFragmentManager().Z("RenamePresetDialogFragment") == null) {
            Wz5 a2 = Wz5.i.a(str, new Ci(this, str2));
            if (isActive()) {
                a2.show(getChildFragmentManager(), "RenamePresetDialogFragment");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Lb6
    public void N3() {
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "showCreateNewSuccessfully");
        HybridCustomizePresetDetailAdapter hybridCustomizePresetDetailAdapter = this.l;
        if (hybridCustomizePresetDetailAdapter != null) {
            int itemCount = hybridCustomizePresetDetailAdapter.getItemCount();
            int i2 = this.k;
            if (itemCount > i2) {
                ViewPager2 viewPager2 = this.j;
                if (viewPager2 != null) {
                    viewPager2.setCurrentItem(i2);
                } else {
                    Wg6.n("rvCustomize");
                    throw null;
                }
            }
        } else {
            Wg6.n("mHybridPresetDetailAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final int N6() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.Lb6
    public void O(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizeFragment", "showDeleteSuccessfully - position=" + i2 + " mCurrentPosition=" + this.k);
        HybridCustomizePresetDetailAdapter hybridCustomizePresetDetailAdapter = this.l;
        if (hybridCustomizePresetDetailAdapter == null) {
            Wg6.n("mHybridPresetDetailAdapter");
            throw null;
        } else if (hybridCustomizePresetDetailAdapter.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.j;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(this.k);
            } else {
                Wg6.n("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    public final Kb6 O6() {
        Kb6 kb6 = this.h;
        if (kb6 != null) {
            return kb6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void P6(Z75 z75) {
        this.s = ThemeManager.l.a().d("nonBrandSwitchDisabledGray");
        this.t = ThemeManager.l.a().d("primaryColor");
        this.u = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
        this.l = new HybridCustomizePresetDetailAdapter(new ArrayList(), this);
        ConstraintLayout constraintLayout = z75.q;
        Wg6.b(constraintLayout, "binding.clNoDevice");
        this.i = constraintLayout;
        z75.u.setOnClickListener(this);
        z75.v.setImageResource(2131230945);
        ViewPager2 viewPager2 = z75.y;
        Wg6.b(viewPager2, "binding.rvPreset");
        this.j = viewPager2;
        if (viewPager2 != null) {
            if (viewPager2.getChildAt(0) != null) {
                ViewPager2 viewPager22 = this.j;
                if (viewPager22 != null) {
                    View childAt = viewPager22.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setOverScrollMode(2);
                    } else {
                        throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                } else {
                    Wg6.n("rvCustomize");
                    throw null;
                }
            }
            ViewPager2 viewPager23 = this.j;
            if (viewPager23 != null) {
                HybridCustomizePresetDetailAdapter hybridCustomizePresetDetailAdapter = this.l;
                if (hybridCustomizePresetDetailAdapter != null) {
                    viewPager23.setAdapter(hybridCustomizePresetDetailAdapter);
                    if (!TextUtils.isEmpty(this.u)) {
                        TabLayout tabLayout = z75.z;
                        Wg6.b(tabLayout, "binding.tab");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.u)));
                    }
                    ViewPager2 viewPager24 = this.j;
                    if (viewPager24 != null) {
                        viewPager24.g(new Bi(this));
                        ViewPager2 viewPager25 = this.j;
                        if (viewPager25 != null) {
                            viewPager25.setCurrentItem(this.k);
                        } else {
                            Wg6.n("rvCustomize");
                            throw null;
                        }
                    } else {
                        Wg6.n("rvCustomize");
                        throw null;
                    }
                } else {
                    Wg6.n("mHybridPresetDetailAdapter");
                    throw null;
                }
            } else {
                Wg6.n("rvCustomize");
                throw null;
            }
        } else {
            Wg6.n("rvCustomize");
            throw null;
        }
    }

    @DexIgnore
    public final void Q6(int i2) {
        this.k = i2;
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        String str2;
        Wg6.c(str, "tag");
        if (str.hashCode() == -1353443012 && str.equals("DIALOG_DELETE_PRESET") && i2 == 2131363373) {
            if (intent != null) {
                str2 = intent.getStringExtra("NEXT_ACTIVE_PRESET_ID");
                Wg6.b(str2, "it.getStringExtra(NEXT_ACTIVE_PRESET_ID)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeHybridCustomizeFragment", "onDialogFragmentResult - nextActivePreset " + str2);
            } else {
                str2 = "";
            }
            Kb6 kb6 = this.h;
            if (kb6 != null) {
                kb6.p(str2);
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void R6(Kb6 kb6) {
        Wg6.c(kb6, "presenter");
        this.h = kb6;
    }

    @DexIgnore
    public final void S6() {
        G37<Z75> g37 = this.m;
        if (g37 != null) {
            Z75 a2 = g37.a();
            TabLayout tabLayout = a2 != null ? a2.z : null;
            if (tabLayout != null) {
                ViewPager2 viewPager2 = this.j;
                if (viewPager2 != null) {
                    new N04(tabLayout, viewPager2, new Di(this)).a();
                } else {
                    Wg6.n("rvCustomize");
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.mapped.HybridCustomizePresetDetailAdapter.Ci
    public void Y3() {
        Kb6 kb6 = this.h;
        if (kb6 != null) {
            kb6.r();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Aw5
    public void b2(boolean z) {
        if (z) {
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Vl5 C62 = C6();
        if (C62 != null) {
            C62.c("");
        }
    }

    @DexIgnore
    @Override // com.fossil.Lb6
    public void c0(int i2) {
        HybridCustomizePresetDetailAdapter hybridCustomizePresetDetailAdapter = this.l;
        if (hybridCustomizePresetDetailAdapter == null) {
            return;
        }
        if (hybridCustomizePresetDetailAdapter == null) {
            Wg6.n("mHybridPresetDetailAdapter");
            throw null;
        } else if (hybridCustomizePresetDetailAdapter.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.j;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(this.k);
            } else {
                Wg6.n("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.HybridCustomizePresetDetailAdapter.Ci
    public void c4() {
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "onAddPresetClick");
        Kb6 kb6 = this.h;
        if (kb6 != null) {
            kb6.o();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Lb6
    public int getItemCount() {
        ViewPager2 viewPager2 = this.j;
        if (viewPager2 != null) {
            RecyclerView.g adapter = viewPager2.getAdapter();
            if (adapter != null) {
                return adapter.getItemCount();
            }
            return 0;
        }
        Wg6.n("rvCustomize");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Lb6
    public void j2(boolean z) {
        if (z) {
            G37<Z75> g37 = this.m;
            if (g37 != null) {
                Z75 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.B;
                    Wg6.b(flexibleTextView, "tvTapIconToCustomize");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        G37<Z75> g372 = this.m;
        if (g372 != null) {
            Z75 a3 = g372.a();
            if (a3 != null) {
                FlexibleTextView flexibleTextView2 = a3.B;
                Wg6.b(flexibleTextView2, "tvTapIconToCustomize");
                flexibleTextView2.setVisibility(4);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Lb6
    public void m0(int i2) {
        this.v = true;
        HybridCustomizePresetDetailAdapter hybridCustomizePresetDetailAdapter = this.l;
        if (hybridCustomizePresetDetailAdapter == null) {
            Wg6.n("mHybridPresetDetailAdapter");
            throw null;
        } else if (hybridCustomizePresetDetailAdapter.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.j;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(i2);
            } else {
                Wg6.n("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        FragmentActivity activity;
        Wg6.c(view, "v");
        if (view.getId() == 2131362501 && (activity = getActivity()) != null) {
            PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
            Wg6.b(activity, "it");
            PairingInstructionsActivity.a.b(aVar, activity, false, false, 6, null);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        Z75 z75 = (Z75) Aq0.f(layoutInflater, 2131558576, viewGroup, false, A6());
        Wg6.b(z75, "binding");
        P6(z75);
        G37<Z75> g37 = new G37<>(this, z75);
        this.m = g37;
        if (g37 != null) {
            Z75 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "onPause");
        Kb6 kb6 = this.h;
        if (kb6 == null) {
            return;
        }
        if (kb6 != null) {
            kb6.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "onResume");
        Kb6 kb6 = this.h;
        if (kb6 == null) {
            return;
        }
        if (kb6 != null) {
            kb6.l();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        S6();
        if (this.h != null) {
            E6("customize_view");
        }
    }

    @DexIgnore
    @Override // com.fossil.Lb6
    public void r(boolean z) {
        if (z) {
            String d = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d)) {
                ConstraintLayout constraintLayout = this.i;
                if (constraintLayout != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(d));
                } else {
                    Wg6.n("clNoDevice");
                    throw null;
                }
            }
            ConstraintLayout constraintLayout2 = this.i;
            if (constraintLayout2 != null) {
                constraintLayout2.setVisibility(0);
            } else {
                Wg6.n("clNoDevice");
                throw null;
            }
        } else {
            ConstraintLayout constraintLayout3 = this.i;
            if (constraintLayout3 != null) {
                constraintLayout3.setVisibility(8);
            } else {
                Wg6.n("clNoDevice");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.HybridCustomizePresetDetailAdapter.Ci
    public void r0(boolean z, String str, String str2, String str3) {
        Wg6.c(str, "currentPresetName");
        Wg6.c(str2, "nextPresetName");
        Wg6.c(str3, "nextPresetId");
        if (isActive()) {
            String string = requireActivity().getString(2131886547);
            Wg6.b(string, "requireActivity().getStr\u2026ingAPresetIsPermanentAnd)");
            if (z) {
                String string2 = requireActivity().getString(2131886548);
                Wg6.b(string2, "requireActivity().getStr\u2026ingAPresetIsPermanentAnd)");
                Hr7 hr7 = Hr7.a;
                string = String.format(string2, Arrays.copyOf(new Object[]{Vt7.g(str2)}, 1));
                Wg6.b(string, "java.lang.String.format(format, *args)");
            }
            Bundle bundle = new Bundle();
            bundle.putString("NEXT_ACTIVE_PRESET_ID", str3);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "nextPresetId " + str3);
            AlertDialogFragment.Fi fi = new AlertDialogFragment.Fi(2131558484);
            Hr7 hr72 = Hr7.a;
            String c = Um5.c(PortfolioApp.get.instance(), 2131886549);
            Wg6.b(c, "LanguageHelper.getString\u2026_Title__DeletePresetName)");
            String format = String.format(c, Arrays.copyOf(new Object[]{str}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            fi.e(2131363410, format);
            fi.e(2131363317, string);
            fi.e(2131363373, Um5.c(PortfolioApp.get.instance(), 2131886546));
            fi.e(2131363291, Um5.c(PortfolioApp.get.instance(), 2131886545));
            fi.b(2131363373);
            fi.b(2131363291);
            fi.m(getChildFragmentManager(), "DIALOG_DELETE_PRESET", bundle);
        }
    }

    @DexIgnore
    @Override // com.mapped.HybridCustomizePresetDetailAdapter.Ci
    public void r4(M66 m66, List<? extends Ln0<View, String>> list, List<? extends Ln0<CustomizeWidget, String>> list2, String str, int i2) {
        Wg6.c(list, "views");
        Wg6.c(list2, "customizeWidgetViews");
        Wg6.c(str, "microAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onPresetWatchClick preset=");
        sb.append(m66 != null ? m66.a() : null);
        sb.append(" microAppPos=");
        sb.append(str);
        sb.append(" position=");
        sb.append(i2);
        local.d("HomeHybridCustomizeFragment", sb.toString());
        if (m66 != null) {
            HybridCustomizeEditActivity.a aVar = HybridCustomizeEditActivity.D;
            FragmentActivity requireActivity = requireActivity();
            Wg6.b(requireActivity, "requireActivity()");
            aVar.b(requireActivity, m66.c(), new ArrayList<>(list), list2, str);
        }
    }

    @DexIgnore
    @Override // com.fossil.Lb6
    public void u() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.B;
            Wg6.b(activity, "it");
            TroubleshootingActivity.a.c(aVar, activity, PortfolioApp.get.instance().J(), false, false, 12, null);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.w;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Lb6
    public void w() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.Lb6
    public void y() {
        String string = getString(2131886814);
        Wg6.b(string, "getString(R.string.Desig\u2026on_Text__ApplyingToWatch)");
        H6(string);
    }
}
