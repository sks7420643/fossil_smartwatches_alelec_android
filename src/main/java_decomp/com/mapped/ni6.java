package com.mapped;

import com.fossil.Ks7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ni6<T, R> extends Ks7<R>, Hg6<T, R> {

    @DexIgnore
    public interface Ai<T, R> extends Ks7.Ai<R>, Hg6<T, R> {
    }

    @DexIgnore
    Object getDelegate(T t);

    @DexIgnore
    Ai<T, R> getGetter();
}
