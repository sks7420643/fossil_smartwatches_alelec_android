package com.mapped;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import com.fossil.Aq0;
import com.fossil.Ct6;
import com.fossil.Ds6;
import com.fossil.Fr6;
import com.fossil.G37;
import com.fossil.Hr4;
import com.fossil.Is6;
import com.fossil.It6;
import com.fossil.Ks6;
import com.fossil.Lr6;
import com.fossil.Ls0;
import com.fossil.Nt6;
import com.fossil.Po4;
import com.fossil.Qs6;
import com.fossil.Rr6;
import com.fossil.Rt6;
import com.fossil.Ts0;
import com.fossil.Vs0;
import com.fossil.Wb5;
import com.fossil.Ws6;
import com.fossil.X47;
import com.fossil.Xr6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserCustomizeThemeFragment extends BaseFragment implements X47 {
    @DexIgnore
    public static String k; // = "";
    @DexIgnore
    public static boolean l;
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public UserCustomizeThemeViewModel h;
    @DexIgnore
    public G37<Wb5> i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return b();
        }

        @DexIgnore
        public final String b() {
            return UserCustomizeThemeFragment.k;
        }

        @DexIgnore
        public final UserCustomizeThemeFragment c() {
            return new UserCustomizeThemeFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<UserCustomizeThemeViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ UserCustomizeThemeFragment a;

        @DexIgnore
        public Bi(UserCustomizeThemeFragment userCustomizeThemeFragment) {
            this.a = userCustomizeThemeFragment;
        }

        @DexIgnore
        public final void a(UserCustomizeThemeViewModel.Ai ai) {
            String a2;
            if (ai != null && (a2 = ai.a()) != null) {
                this.a.N6(a2);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(UserCustomizeThemeViewModel.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ UserCustomizeThemeFragment b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements Nt6.Bi {
            @DexIgnore
            public /* final */ /* synthetic */ Ci a;

            @DexIgnore
            public Aii(Ci ci) {
                this.a = ci;
            }

            @DexIgnore
            @Override // com.fossil.Nt6.Bi
            public void a(String str) {
                Wg6.c(str, "themeName");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("UserCustomizeThemeFragment", "showRenamePresetDialog - themeName=" + str);
                if (!TextUtils.isEmpty(str)) {
                    UserCustomizeThemeFragment.K6(this.a.b).i(str);
                }
            }

            @DexIgnore
            @Override // com.fossil.Nt6.Bi
            public void onCancel() {
                FLogger.INSTANCE.getLocal().d("UserCustomizeThemeFragment", "showRenamePresetDialog - onCancel");
            }
        }

        @DexIgnore
        public Ci(UserCustomizeThemeFragment userCustomizeThemeFragment) {
            this.b = userCustomizeThemeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            Nt6.h.a("", new Aii(this)).show(this.b.getChildFragmentManager(), "RenamePresetDialogFragment");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ UserCustomizeThemeFragment b;

        @DexIgnore
        public Di(UserCustomizeThemeFragment userCustomizeThemeFragment) {
            this.b = userCustomizeThemeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ UserCustomizeThemeViewModel K6(UserCustomizeThemeFragment userCustomizeThemeFragment) {
        UserCustomizeThemeViewModel userCustomizeThemeViewModel = userCustomizeThemeFragment.h;
        if (userCustomizeThemeViewModel != null) {
            return userCustomizeThemeViewModel;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.X47
    public void C3(int i2, int i3) {
        FLogger.INSTANCE.getLocal().d("UserCustomizeThemeFragment", "onColorSelected");
    }

    @DexIgnore
    public final void M6(ViewPager viewPager) {
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        Hr4 hr4 = new Hr4(childFragmentManager);
        It6 it6 = new It6();
        String string = PortfolioApp.get.instance().getString(2131887582);
        Wg6.b(string, "PortfolioApp.instance.getString(R.string.text)");
        hr4.x(it6, string);
        Ds6 ds6 = new Ds6();
        String string2 = PortfolioApp.get.instance().getString(2131887317);
        Wg6.b(string2, "PortfolioApp.instance.getString(R.string.button)");
        hr4.x(ds6, string2);
        Xr6 xr6 = new Xr6();
        String string3 = PortfolioApp.get.instance().getString(2131887294);
        Wg6.b(string3, "PortfolioApp.instance.ge\u2026ring(R.string.background)");
        hr4.x(xr6, string3);
        Ws6 ws6 = new Ws6();
        String string4 = PortfolioApp.get.instance().getString(2131887553);
        Wg6.b(string4, "PortfolioApp.instance.getString(R.string.ring)");
        hr4.x(ws6, string4);
        Rr6 rr6 = new Rr6();
        String string5 = PortfolioApp.get.instance().getString(2131887264);
        Wg6.b(string5, "PortfolioApp.instance.getString(R.string.activity)");
        hr4.x(rr6, string5);
        Lr6 lr6 = new Lr6();
        String string6 = PortfolioApp.get.instance().getString(2131887263);
        Wg6.b(string6, "PortfolioApp.instance.ge\u2026(R.string.active_minutes)");
        hr4.x(lr6, string6);
        Fr6 fr6 = new Fr6();
        String string7 = PortfolioApp.get.instance().getString(2131887262);
        Wg6.b(string7, "PortfolioApp.instance.ge\u2026R.string.active_calories)");
        hr4.x(fr6, string7);
        Qs6 qs6 = new Qs6();
        String string8 = PortfolioApp.get.instance().getString(2131887422);
        Wg6.b(string8, "PortfolioApp.instance.ge\u2026ring(R.string.heart_rate)");
        hr4.x(qs6, string8);
        Ct6 ct6 = new Ct6();
        String string9 = PortfolioApp.get.instance().getString(2131887572);
        Wg6.b(string9, "PortfolioApp.instance.getString(R.string.sleep)");
        hr4.x(ct6, string9);
        Ks6 ks6 = new Ks6();
        String string10 = PortfolioApp.get.instance().getString(2131887415);
        Wg6.b(string10, "PortfolioApp.instance.ge\u2026g(R.string.goal_tracking)");
        hr4.x(ks6, string10);
        Is6 is6 = new Is6();
        String string11 = PortfolioApp.get.instance().getString(2131887409);
        Wg6.b(string11, "PortfolioApp.instance.getString(R.string.font)");
        hr4.x(is6, string11);
        viewPager.setAdapter(hr4);
    }

    @DexIgnore
    public final void N6(String str) {
        Wg6.c(str, "name");
        G37<Wb5> g37 = this.i;
        if (g37 != null) {
            Wb5 a2 = g37.a();
            if (a2 != null) {
                a2.s.setText(str);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        String str;
        Wg6.c(layoutInflater, "inflater");
        Wb5 wb5 = (Wb5) Aq0.f(LayoutInflater.from(getContext()), 2131558633, null, false, A6());
        Bundle arguments = getArguments();
        if (arguments == null || (str = arguments.getString("THEME_ID")) == null) {
            str = "";
        }
        k = str;
        Bundle arguments2 = getArguments();
        l = arguments2 != null ? arguments2.getBoolean("THEME_MODE_EDIT") : false;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UserCustomizeThemeFragment", "themeId=" + k + " isModeEdit=" + l);
        PortfolioApp.get.instance().getIface().d(new Rt6()).a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(UserCustomizeThemeViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026emeViewModel::class.java)");
            UserCustomizeThemeViewModel userCustomizeThemeViewModel = (UserCustomizeThemeViewModel) a2;
            this.h = userCustomizeThemeViewModel;
            if (userCustomizeThemeViewModel != null) {
                userCustomizeThemeViewModel.f().h(getViewLifecycleOwner(), new Bi(this));
                UserCustomizeThemeViewModel userCustomizeThemeViewModel2 = this.h;
                if (userCustomizeThemeViewModel2 != null) {
                    userCustomizeThemeViewModel2.g();
                    this.i = new G37<>(this, wb5);
                    Wg6.b(wb5, "binding");
                    return wb5.n();
                }
                Wg6.n("mViewModel");
                throw null;
            }
            Wg6.n("mViewModel");
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<Wb5> g37 = this.i;
        if (g37 != null) {
            Wb5 a2 = g37.a();
            if (a2 != null) {
                ViewPager viewPager = a2.u;
                Wg6.b(viewPager, "it.viewPager");
                M6(viewPager);
                a2.r.I(a2.u, false);
                a2.s.setOnClickListener(new Ci(this));
                a2.q.setOnClickListener(new Di(this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.X47
    public void q3(int i2) {
        FLogger.INSTANCE.getLocal().d("UserCustomizeThemeFragment", "onColorSelected");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
