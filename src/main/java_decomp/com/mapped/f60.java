package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Dm1;
import com.fossil.Dt1;
import com.fossil.Et1;
import com.fossil.Fm1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F60 extends Dm1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<F60> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public F60 a(Parcel parcel) {
            return new F60(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public F60 createFromParcel(Parcel parcel) {
            return new F60(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public F60[] newArray(int i) {
            return new F60[i];
        }
    }

    @DexIgnore
    public F60() {
        super(Fm1.WEATHER, null, null, null, 14);
    }

    @DexIgnore
    public /* synthetic */ F60(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public F60(Dt1 dt1, Et1 et1) {
        super(Fm1.WEATHER, null, dt1, et1, 2);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ F60(Dt1 dt1, Et1 et1, int i, Qg6 qg6) {
        this(dt1, (i & 2) != 0 ? new Et1(Et1.CREATOR.a()) : et1);
    }
}
