package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.E;
import com.fossil.Ey1;
import com.fossil.G80;
import com.fossil.Hy1;
import com.fossil.Jd0;
import com.fossil.Ox1;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y80 extends Ox1 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ J70 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Y80> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Y80 createFromParcel(Parcel parcel) {
            int readInt = parcel.readInt();
            float readFloat = parcel.readFloat();
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                return new Y80(readInt, readFloat, J70.valueOf(readString));
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Y80[] newArray(int i) {
            return new Y80[i];
        }
    }

    @DexIgnore
    public Y80(int i, float f, J70 j70) throws IllegalArgumentException {
        this.b = i;
        this.c = Hy1.e(f, 2);
        this.d = j70;
        int i2 = this.b;
        if (!(i2 >= 0 && 23 >= i2)) {
            throw new IllegalArgumentException(E.c(E.e("hourIn24Format("), this.b, ") is out of range ", "[0, 23]."));
        }
    }

    @DexIgnore
    public final JSONObject a() {
        JSONObject put = new JSONObject().put(AppFilter.COLUMN_HOUR, this.b).put("temp", Float.valueOf(this.c)).put("cond_id", this.d.a());
        Wg6.b(put, "JSONObject()\n           \u2026_ID, weatherCondition.id)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Y80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Y80 y80 = (Y80) obj;
            if (this.b != y80.b) {
                return false;
            }
            if (this.c != y80.c) {
                return false;
            }
            return this.d == y80.d;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.WeatherHourForecast");
    }

    @DexIgnore
    public final int getHourIn24Format() {
        return this.b;
    }

    @DexIgnore
    public final float getTemperature() {
        return this.c;
    }

    @DexIgnore
    public final J70 getWeatherCondition() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.b;
        return (((i * 31) + Float.valueOf(this.c).hashCode()) * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(new JSONObject(), Jd0.A, Integer.valueOf(this.b)), Jd0.T1, Float.valueOf(this.c)), Jd0.t, Ey1.a(this.d));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeFloat(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
    }
}
