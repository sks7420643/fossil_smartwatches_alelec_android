package com.mapped;

import com.fossil.Hm7;
import com.fossil.Ht4;
import com.fossil.Mj4;
import com.fossil.Ms4;
import com.fossil.Zi4;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HistoryChallengeConverter {
    @DexIgnore
    public /* final */ Gson a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends TypeToken<List<? extends Ms4>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends TypeToken<List<? extends Ms4>> {
    }

    @DexIgnore
    public HistoryChallengeConverter() {
        Gson d = new Zi4().d();
        Wg6.b(d, "GsonBuilder().create()");
        this.a = d;
    }

    @DexIgnore
    public final String a(Ht4 ht4) {
        Wg6.c(ht4, "owner");
        try {
            return this.a.u(ht4, Ht4.class);
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final String b(Ms4 ms4) {
        try {
            String u = this.a.u(ms4, Ms4.class);
            Wg6.b(u, "gson.toJson(player, BCPlayer::class.java)");
            return u;
        } catch (Exception e) {
            return "";
        }
    }

    @DexIgnore
    public final String c(List<Ms4> list) {
        Wg6.c(list, "players");
        String t = new Gson().t(list);
        Wg6.b(t, "Gson().toJson(players)");
        return t;
    }

    @DexIgnore
    public final Ht4 d(String str) {
        Wg6.c(str, "value");
        try {
            return (Ht4) this.a.k(str, Ht4.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final Ms4 e(String str) {
        Wg6.c(str, "value");
        try {
            return (Ms4) this.a.k(str, Ms4.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final List<Ms4> f(String str) {
        List<Ms4> e;
        try {
            Object l = this.a.l(str, new Ai().getType());
            Wg6.b(l, "gson.fromJson(value, obj\u2026ist<BCPlayer>>() {}.type)");
            return (List) l;
        } catch (Exception e2) {
            if (!(e2 instanceof Mj4)) {
                return Hm7.e();
            }
            try {
                FLogger.INSTANCE.getLocal().e("HistoryChallengeConverter", "stringToPlayers - apply date format for special case");
                Zi4 zi4 = new Zi4();
                zi4.g("MM dd, yyyy HH:mm:ss");
                e = (List) zi4.d().l(str, new Bi().getType());
            } catch (Exception e3) {
                e3.printStackTrace();
                e = Hm7.e();
            }
            Wg6.b(e, "try {\n                  \u2026ayer>()\n                }");
            return e;
        }
    }
}
