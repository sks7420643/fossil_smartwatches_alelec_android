package com.mapped;

import com.fossil.Bu0;
import com.fossil.Ju0;
import com.fossil.Vt0;
import com.mapped.Xe;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ye<Key, Value> extends Vt0<Key, Value> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai<Value> {
        @DexIgnore
        public abstract void a(List<Value> list);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<Value> extends Ai<Value> {
        @DexIgnore
        public /* final */ Xe.Di<Value> a;

        @DexIgnore
        public Bi(Ye ye, int i, Executor executor, Bu0.Ai<Value> ai) {
            this.a = new Xe.Di<>(ye, i, executor, ai);
        }

        @DexIgnore
        @Override // com.mapped.Ye.Ai
        public void a(List<Value> list) {
            if (!this.a.a()) {
                this.a.b(new Bu0<>(list, 0, 0, 0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ci<Value> extends Ai<Value> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di<Value> extends Ci<Value> {
        @DexIgnore
        public /* final */ Xe.Di<Value> a;

        @DexIgnore
        public Di(Ye ye, boolean z, Bu0.Ai<Value> ai) {
            this.a = new Xe.Di<>(ye, 0, null, ai);
        }

        @DexIgnore
        @Override // com.mapped.Ye.Ai
        public void a(List<Value> list) {
            if (!this.a.a()) {
                this.a.b(new Bu0<>(list, 0, 0, 0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei<Key> {
        @DexIgnore
        public /* final */ Key a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Ei(Key key, int i, boolean z) {
            this.a = key;
            this.b = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Fi<Key> {
        @DexIgnore
        public /* final */ Key a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Fi(Key key, int i) {
            this.a = key;
            this.b = i;
        }
    }

    @DexIgnore
    @Override // com.fossil.Vt0
    public final void dispatchLoadAfter(int i, Value value, int i2, Executor executor, Bu0.Ai<Value> ai) {
        loadAfter(new Fi<>(getKey(value), i2), new Bi(this, 1, executor, ai));
    }

    @DexIgnore
    @Override // com.fossil.Vt0
    public final void dispatchLoadBefore(int i, Value value, int i2, Executor executor, Bu0.Ai<Value> ai) {
        loadBefore(new Fi<>(getKey(value), i2), new Bi(this, 2, executor, ai));
    }

    @DexIgnore
    @Override // com.fossil.Vt0
    public final void dispatchLoadInitial(Key key, int i, int i2, boolean z, Executor executor, Bu0.Ai<Value> ai) {
        Di di = new Di(this, z, ai);
        loadInitial(new Ei<>(key, i, z), di);
        di.a.c(executor);
    }

    @DexIgnore
    @Override // com.fossil.Vt0
    public final Key getKey(int i, Value value) {
        if (value == null) {
            return null;
        }
        return getKey(value);
    }

    @DexIgnore
    public abstract Key getKey(Value value);

    @DexIgnore
    public abstract void loadAfter(Fi<Key> fi, Ai<Value> ai);

    @DexIgnore
    public abstract void loadBefore(Fi<Key> fi, Ai<Value> ai);

    @DexIgnore
    public abstract void loadInitial(Ei<Key> ei, Ci<Value> ci);

    @DexIgnore
    @Override // com.mapped.Xe
    public final <ToValue> Ye<Key, ToValue> map(V3<Value, ToValue> v3) {
        return mapByPage((V3) Xe.createListFunction(v3));
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public final <ToValue> Ye<Key, ToValue> mapByPage(V3<List<Value>, List<ToValue>> v3) {
        return new Ju0(this, v3);
    }
}
