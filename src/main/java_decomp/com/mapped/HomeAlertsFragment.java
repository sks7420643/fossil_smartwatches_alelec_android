package com.mapped;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.A06;
import com.fossil.Aq0;
import com.fossil.Aw5;
import com.fossil.D06;
import com.fossil.G37;
import com.fossil.Lg5;
import com.fossil.N75;
import com.fossil.Nl5;
import com.fossil.P47;
import com.fossil.Qv5;
import com.fossil.S37;
import com.fossil.Um5;
import com.fossil.Vl5;
import com.fossil.Xq4;
import com.fossil.Xz5;
import com.fossil.Zz5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeAlertsFragment extends Qv5 implements A06, Aw5, View.OnClickListener {
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public G37<N75> h;
    @DexIgnore
    public Zz5 i;
    @DexIgnore
    public Xq4 j;
    @DexIgnore
    public Xz5 k;
    @DexIgnore
    public DoNotDisturbScheduledTimePresenter l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final HomeAlertsFragment a() {
            return new HomeAlertsFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Xq4.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment a;

        @DexIgnore
        public Bi(HomeAlertsFragment homeAlertsFragment) {
            this.a = homeAlertsFragment;
        }

        @DexIgnore
        @Override // com.fossil.Xq4.Ai
        public void a(Alarm alarm) {
            Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            HomeAlertsFragment.L6(this.a).o(alarm, !alarm.isActive());
        }

        @DexIgnore
        @Override // com.fossil.Xq4.Ai
        public void b(Alarm alarm) {
            Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            HomeAlertsFragment.L6(this.a).q(alarm);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment b;

        @DexIgnore
        public Ci(HomeAlertsFragment homeAlertsFragment) {
            this.b = homeAlertsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
                Wg6.b(activity, "it");
                PairingInstructionsActivity.a.b(aVar, activity, false, false, 6, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment b;

        @DexIgnore
        public Di(HomeAlertsFragment homeAlertsFragment) {
            this.b = homeAlertsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationWatchRemindersActivity.B.a(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment b;

        @DexIgnore
        public Ei(HomeAlertsFragment homeAlertsFragment) {
            this.b = homeAlertsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            P47.l(view);
            NotificationAppsActivity.B.a(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment b;

        @DexIgnore
        public Fi(HomeAlertsFragment homeAlertsFragment) {
            this.b = homeAlertsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationCallsAndMessagesActivity.B.a(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment b;

        @DexIgnore
        public Gi(HomeAlertsFragment homeAlertsFragment) {
            this.b = homeAlertsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeAlertsFragment.L6(this.b).p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment b;

        @DexIgnore
        public Hi(HomeAlertsFragment homeAlertsFragment) {
            this.b = homeAlertsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            Xz5 xz5 = this.b.k;
            if (xz5 != null) {
                xz5.D6(0);
            }
            Xz5 xz52 = this.b.k;
            if (xz52 != null) {
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                xz52.show(childFragmentManager, Xz5.u.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsFragment b;

        @DexIgnore
        public Ii(HomeAlertsFragment homeAlertsFragment) {
            this.b = homeAlertsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            Xz5 xz5 = this.b.k;
            if (xz5 != null) {
                xz5.D6(1);
            }
            Xz5 xz52 = this.b.k;
            if (xz52 != null) {
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                xz52.show(childFragmentManager, Xz5.u.a());
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ Zz5 L6(HomeAlertsFragment homeAlertsFragment) {
        Zz5 zz5 = homeAlertsFragment.i;
        if (zz5 != null) {
            return zz5;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "HomeAlertsFragment";
    }

    @DexIgnore
    @Override // com.fossil.A06
    public void F(boolean z) {
        RTLImageView rTLImageView;
        RTLImageView rTLImageView2;
        if (z) {
            G37<N75> g37 = this.h;
            if (g37 != null) {
                N75 a2 = g37.a();
                if (a2 != null && (rTLImageView2 = a2.G) != null) {
                    rTLImageView2.setVisibility(0);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        G37<N75> g372 = this.h;
        if (g372 != null) {
            N75 a3 = g372.a();
            if (a3 != null && (rTLImageView = a3.G) != null) {
                rTLImageView.setVisibility(8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.A06
    public void J0(SpannableString spannableString) {
        FlexibleTextView flexibleTextView;
        Wg6.c(spannableString, LogBuilder.KEY_TIME);
        G37<N75> g37 = this.h;
        if (g37 != null) {
            N75 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.B) != null) {
                flexibleTextView.setText(spannableString);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        M6((Zz5) obj);
    }

    @DexIgnore
    public void M6(Zz5 zz5) {
        Wg6.c(zz5, "presenter");
        this.i = zz5;
    }

    @DexIgnore
    @Override // com.fossil.A06
    public void U() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.Z(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.A06
    public void W() {
        FLogger.INSTANCE.getLocal().d("HomeAlertsFragment", "notifyListAlarm()");
        Xq4 xq4 = this.j;
        if (xq4 != null) {
            xq4.notifyDataSetChanged();
        } else {
            Wg6.n("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Aw5
    public void b2(boolean z) {
        if (z) {
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Vl5 C62 = C6();
        if (C62 != null) {
            C62.c("");
        }
    }

    @DexIgnore
    @Override // com.fossil.A06
    public void c() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.A06
    public void l0(String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        Wg6.c(str, "deviceId");
        Wg6.c(arrayList, "mAlarms");
        AlarmActivity.a aVar = AlarmActivity.B;
        Context requireContext = requireContext();
        Wg6.b(requireContext, "requireContext()");
        aVar.a(requireContext, str, arrayList, alarm);
    }

    @DexIgnore
    @Override // com.fossil.A06
    public void n3(boolean z) {
        Context requireContext;
        int i2;
        G37<N75> g37 = this.h;
        if (g37 != null) {
            N75 a2 = g37.a();
            if (a2 != null) {
                SwitchCompat switchCompat = a2.P;
                Wg6.b(switchCompat, "it.swScheduled");
                switchCompat.setChecked(z);
                ConstraintLayout constraintLayout = a2.s;
                Wg6.b(constraintLayout, "it.clScheduledTimeContainer");
                constraintLayout.setVisibility(z ? 0 : 8);
                FlexibleTextView flexibleTextView = a2.z;
                if (z) {
                    requireContext = requireContext();
                    i2 = 2131099968;
                } else {
                    requireContext = requireContext();
                    i2 = 2131100358;
                }
                flexibleTextView.setTextColor(W6.d(requireContext, i2));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.A06
    public void o1(String str) {
        FlexibleTextView flexibleTextView;
        Wg6.c(str, "notificationAppOverView");
        G37<N75> g37 = this.h;
        if (g37 != null) {
            N75 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.v) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void onClick(View view) {
        Integer valueOf = view != null ? Integer.valueOf(view.getId()) : null;
        if (valueOf != null && valueOf.intValue() == 2131361966) {
            Zz5 zz5 = this.i;
            if (zz5 != null) {
                zz5.q(null);
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        } else if (valueOf != null && valueOf.intValue() == 2131362767) {
            Zz5 zz52 = this.i;
            if (zz52 != null) {
                zz52.n();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        N75 n75 = (N75) Aq0.f(layoutInflater, 2131558571, viewGroup, false, A6());
        Xz5 xz5 = (Xz5) getChildFragmentManager().Z(Xz5.u.a());
        this.k = xz5;
        if (xz5 == null) {
            this.k = Xz5.u.b();
        }
        String d = ThemeManager.l.a().d("onPrimaryButton");
        if (!TextUtils.isEmpty(d)) {
            int parseColor = Color.parseColor(d);
            Drawable drawable = PortfolioApp.get.instance().getDrawable(2131230986);
            if (drawable != null) {
                drawable.setTint(parseColor);
                n75.q.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        n75.G.setOnClickListener(this);
        FlexibleButton flexibleButton = n75.q;
        Wg6.b(flexibleButton, "binding.btnAdd");
        Nl5.a(flexibleButton, this);
        n75.M.setOnClickListener(new Di(this));
        n75.I.setOnClickListener(new Ei(this));
        n75.J.setOnClickListener(new Fi(this));
        n75.P.setOnClickListener(new Gi(this));
        n75.L.setOnClickListener(new Hi(this));
        n75.K.setOnClickListener(new Ii(this));
        Lg5 lg5 = n75.F;
        if (lg5 != null) {
            ConstraintLayout constraintLayout = lg5.q;
            Wg6.b(constraintLayout, "viewNoDeviceBinding.clRoot");
            constraintLayout.setVisibility(0);
            String d2 = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d2)) {
                lg5.q.setBackgroundColor(Color.parseColor(d2));
            }
            lg5.t.setImageResource(2131230818);
            FlexibleTextView flexibleTextView = lg5.r;
            Wg6.b(flexibleTextView, "viewNoDeviceBinding.ftvDescription");
            flexibleTextView.setText(Um5.c(getContext(), 2131887050));
            lg5.s.setOnClickListener(new Ci(this));
        }
        Xq4 xq4 = new Xq4();
        xq4.o(new Bi(this));
        this.j = xq4;
        RecyclerView recyclerView = n75.O;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
        Xq4 xq42 = this.j;
        if (xq42 != null) {
            recyclerView.setAdapter(xq42);
            this.h = new G37<>(this, n75);
            Iface iface = PortfolioApp.get.instance().getIface();
            Xz5 xz52 = this.k;
            if (xz52 != null) {
                iface.I0(new D06(xz52)).a(this);
                G37<N75> g37 = this.h;
                if (g37 != null) {
                    N75 a2 = g37.a();
                    if (a2 != null) {
                        Wg6.b(a2, "mBinding.get()!!");
                        return a2.n();
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.n("mBinding");
                throw null;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimeContract.View");
        }
        Wg6.n("mAlarmsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        Zz5 zz5 = this.i;
        if (zz5 != null) {
            if (zz5 != null) {
                zz5.m();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
        Vl5 C6 = C6();
        if (C6 != null) {
            C6.c("");
        }
        super.onPause();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Zz5 zz5 = this.i;
        if (zz5 != null) {
            if (zz5 != null) {
                zz5.l();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
        Vl5 C6 = C6();
        if (C6 != null) {
            C6.i();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        E6("alert_view");
    }

    @DexIgnore
    @Override // com.fossil.A06
    public void p0(List<Alarm> list) {
        Wg6.c(list, "alarms");
        Xq4 xq4 = this.j;
        if (xq4 != null) {
            xq4.n(list);
        } else {
            Wg6.n("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.A06
    public void q2(SpannableString spannableString) {
        FlexibleTextView flexibleTextView;
        Wg6.c(spannableString, LogBuilder.KEY_TIME);
        G37<N75> g37 = this.h;
        if (g37 != null) {
            N75 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.D) != null) {
                flexibleTextView.setText(spannableString.toString());
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.A06
    public void r(boolean z) {
        ConstraintLayout constraintLayout;
        G37<N75> g37 = this.h;
        if (g37 != null) {
            N75 a2 = g37.a();
            if (a2 != null && (constraintLayout = a2.r) != null) {
                constraintLayout.setVisibility(z ? 0 : 8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.A06
    public void v0() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.r0(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
