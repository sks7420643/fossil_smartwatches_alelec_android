package com.mapped;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Eb7;
import com.fossil.Gc7;
import com.fossil.Hc7;
import com.fossil.Hl5;
import com.fossil.Jn5;
import com.fossil.Ls0;
import com.fossil.Po4;
import com.fossil.Qg5;
import com.fossil.Ts0;
import com.fossil.Um5;
import com.fossil.Vs0;
import com.fossil.Vt7;
import com.mapped.ComplicationsAdapter;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezoneActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.watchface.edit.WatchFaceEditActivity;
import com.portfolio.platform.watchface.edit.complication.complication_selector.WatchFaceComplicationViewModel;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceComplicationFragment extends BaseFragment {
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public WatchFaceComplicationViewModel h;
    @DexIgnore
    public Qg5 i;
    @DexIgnore
    public ComplicationsAdapter j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final WatchFaceComplicationFragment a() {
            return new WatchFaceComplicationFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements ComplicationsAdapter.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationFragment a;

        @DexIgnore
        public Bi(WatchFaceComplicationFragment watchFaceComplicationFragment) {
            this.a = watchFaceComplicationFragment;
        }

        @DexIgnore
        @Override // com.mapped.ComplicationsAdapter.Bi
        public void a(String str) {
            Wg6.c(str, "complicationId");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationFragment", "onItemClicked, complicationId = " + str);
            WatchFaceComplicationFragment.K6(this.a).t(str);
            WatchFaceComplicationFragment.L6(this.a).p(str);
            Gc7 d = Hc7.c.d(this.a);
            if (d != null) {
                d.y(str);
            }
        }

        @DexIgnore
        @Override // com.mapped.ComplicationsAdapter.Bi
        public void b(String str) {
            Wg6.c(str, "complicationId");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationFragment", "onItemWarningClicked, complicationId = " + str);
            Jn5.c(Jn5.b, this.a.getContext(), Hl5.a.b(str), false, false, false, null, 60, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationFragment b;

        @DexIgnore
        public Ci(WatchFaceComplicationFragment watchFaceComplicationFragment) {
            this.b = watchFaceComplicationFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            WatchFaceComplicationFragment.L6(this.b).z();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<List<? extends Complication>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationFragment a;

        @DexIgnore
        public Di(WatchFaceComplicationFragment watchFaceComplicationFragment) {
            this.a = watchFaceComplicationFragment;
        }

        @DexIgnore
        public final void a(List<Complication> list) {
            ComplicationsAdapter K6 = WatchFaceComplicationFragment.K6(this.a);
            Wg6.b(list, "it");
            K6.n(list);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends Complication> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<WatchFaceComplicationViewModel.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationFragment a;

        @DexIgnore
        public Ei(WatchFaceComplicationFragment watchFaceComplicationFragment) {
            this.a = watchFaceComplicationFragment;
        }

        @DexIgnore
        public final void a(WatchFaceComplicationViewModel.Ci ci) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationFragment", "settingViewLiveData changed, value = " + ci);
            this.a.S6(ci.a());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(WatchFaceComplicationViewModel.Ci ci) {
            a(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<WatchFaceComplicationViewModel.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationFragment a;

        @DexIgnore
        public Fi(WatchFaceComplicationFragment watchFaceComplicationFragment) {
            this.a = watchFaceComplicationFragment;
        }

        @DexIgnore
        public final void a(WatchFaceComplicationViewModel.Bi bi) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationFragment", "settingScreenNavigationLiveData changed, value = " + bi);
            String b = bi.b();
            String a2 = bi.a();
            int hashCode = a2.hashCode();
            if (hashCode != -829740640) {
                if (hashCode == 134170930 && a2.equals("second-timezone")) {
                    SearchSecondTimezoneActivity.B.a(this.a, b);
                }
            } else if (a2.equals("commute-time")) {
                CommuteTimeSettingsActivity.B.a(this.a, b);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(WatchFaceComplicationViewModel.Bi bi) {
            a(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> implements Ls0<Complication> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationFragment a;

        @DexIgnore
        public Gi(WatchFaceComplicationFragment watchFaceComplicationFragment) {
            this.a = watchFaceComplicationFragment;
        }

        @DexIgnore
        public final void a(Complication complication) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationFragment", "selectedComplicationLiveData, value = " + complication);
            WatchFaceComplicationFragment watchFaceComplicationFragment = this.a;
            Wg6.b(complication, "it");
            watchFaceComplicationFragment.T6(complication);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Complication complication) {
            a(complication);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> implements Ls0<Eb7> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationFragment a;

        @DexIgnore
        public Hi(WatchFaceComplicationFragment watchFaceComplicationFragment) {
            this.a = watchFaceComplicationFragment;
        }

        @DexIgnore
        public final void a(Eb7 eb7) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationFragment", "watchFaceComplicationsLive, value = " + eb7);
            this.a.R6(eb7.b().a());
            WatchFaceComplicationFragment.K6(this.a).u(eb7.b().b());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Eb7 eb7) {
            a(eb7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii<T> implements Ls0<Rect> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationFragment a;

        @DexIgnore
        public Ii(WatchFaceComplicationFragment watchFaceComplicationFragment) {
            this.a = watchFaceComplicationFragment;
        }

        @DexIgnore
        public final void a(Rect rect) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationFragment", "editorRectLive, value = " + rect);
            ComplicationsAdapter K6 = WatchFaceComplicationFragment.K6(this.a);
            Wg6.b(rect, "it");
            K6.r(rect);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Rect rect) {
            a(rect);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji<T> implements Ls0<Object> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationFragment a;

        @DexIgnore
        public Ji(WatchFaceComplicationFragment watchFaceComplicationFragment) {
            this.a = watchFaceComplicationFragment;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(Object obj) {
            FLogger.INSTANCE.getLocal().d("WatchFaceComplicationFragment", "complicationDroppedLive");
            WatchFaceComplicationFragment.K6(this.a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationFragment a;

        @DexIgnore
        public Ki(WatchFaceComplicationFragment watchFaceComplicationFragment) {
            this.a = watchFaceComplicationFragment;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            Wg6.b(bool, "it");
            if (bool.booleanValue()) {
                WatchFaceComplicationFragment.K6(this.a).notifyDataSetChanged();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li<T> implements Ls0<List<? extends DianaAppSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceComplicationFragment a;

        @DexIgnore
        public Li(WatchFaceComplicationFragment watchFaceComplicationFragment) {
            this.a = watchFaceComplicationFragment;
        }

        @DexIgnore
        public final void a(List<DianaAppSetting> list) {
            Gc7 d = Hc7.c.d(this.a);
            if (d != null) {
                Wg6.b(list, "it");
                d.t(list);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends DianaAppSetting> list) {
            a(list);
        }
    }

    @DexIgnore
    public static final /* synthetic */ ComplicationsAdapter K6(WatchFaceComplicationFragment watchFaceComplicationFragment) {
        ComplicationsAdapter complicationsAdapter = watchFaceComplicationFragment.j;
        if (complicationsAdapter != null) {
            return complicationsAdapter;
        }
        Wg6.n("complicationAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ WatchFaceComplicationViewModel L6(WatchFaceComplicationFragment watchFaceComplicationFragment) {
        WatchFaceComplicationViewModel watchFaceComplicationViewModel = watchFaceComplicationFragment.h;
        if (watchFaceComplicationViewModel != null) {
            return watchFaceComplicationViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "WatchFaceComplicationFragment";
    }

    @DexIgnore
    public final void P6() {
        ComplicationsAdapter complicationsAdapter = new ComplicationsAdapter(null, null, 3, null);
        complicationsAdapter.s(new Bi(this));
        this.j = complicationsAdapter;
        Qg5 qg5 = this.i;
        if (qg5 != null) {
            RecyclerView recyclerView = qg5.f;
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
            ComplicationsAdapter complicationsAdapter2 = this.j;
            if (complicationsAdapter2 != null) {
                recyclerView.setAdapter(complicationsAdapter2);
                qg5.j.setOnClickListener(new Ci(this));
                return;
            }
            Wg6.n("complicationAdapter");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        LiveData<Object> f;
        LiveData<Rect> h2;
        LiveData<Eb7> q;
        WatchFaceComplicationViewModel watchFaceComplicationViewModel = this.h;
        if (watchFaceComplicationViewModel != null) {
            watchFaceComplicationViewModel.v().h(getViewLifecycleOwner(), new Di(this));
            WatchFaceComplicationViewModel watchFaceComplicationViewModel2 = this.h;
            if (watchFaceComplicationViewModel2 != null) {
                watchFaceComplicationViewModel2.y().h(getViewLifecycleOwner(), new Ei(this));
                WatchFaceComplicationViewModel watchFaceComplicationViewModel3 = this.h;
                if (watchFaceComplicationViewModel3 != null) {
                    watchFaceComplicationViewModel3.x().h(getViewLifecycleOwner(), new Fi(this));
                    WatchFaceComplicationViewModel watchFaceComplicationViewModel4 = this.h;
                    if (watchFaceComplicationViewModel4 != null) {
                        watchFaceComplicationViewModel4.w().h(getViewLifecycleOwner(), new Gi(this));
                        Gc7 d = Hc7.c.d(this);
                        if (!(d == null || (q = d.q()) == null)) {
                            q.h(getViewLifecycleOwner(), new Hi(this));
                        }
                        Gc7 d2 = Hc7.c.d(this);
                        if (!(d2 == null || (h2 = d2.h()) == null)) {
                            h2.h(getViewLifecycleOwner(), new Ii(this));
                        }
                        Gc7 d3 = Hc7.c.d(this);
                        if (!(d3 == null || (f = d3.f()) == null)) {
                            f.h(getViewLifecycleOwner(), new Ji(this));
                        }
                        WatchFaceComplicationViewModel watchFaceComplicationViewModel5 = this.h;
                        if (watchFaceComplicationViewModel5 != null) {
                            watchFaceComplicationViewModel5.r().h(getViewLifecycleOwner(), new Ki(this));
                            WatchFaceComplicationViewModel watchFaceComplicationViewModel6 = this.h;
                            if (watchFaceComplicationViewModel6 != null) {
                                watchFaceComplicationViewModel6.u().h(getViewLifecycleOwner(), new Li(this));
                            } else {
                                Wg6.n("viewModel");
                                throw null;
                            }
                        } else {
                            Wg6.n("viewModel");
                            throw null;
                        }
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void R6(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceComplicationFragment", "showSelectedComplication, complicationId = " + str);
        ComplicationsAdapter complicationsAdapter = this.j;
        if (complicationsAdapter != null) {
            complicationsAdapter.t(str);
            if (str != null) {
                WatchFaceComplicationViewModel watchFaceComplicationViewModel = this.h;
                if (watchFaceComplicationViewModel != null) {
                    watchFaceComplicationViewModel.p(str);
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Qg5 qg5 = this.i;
                if (qg5 != null) {
                    ConstraintLayout constraintLayout = qg5.b;
                    Wg6.b(constraintLayout, "it.clComplicationSetting");
                    constraintLayout.setVisibility(4);
                    FlexibleTextView flexibleTextView = qg5.j;
                    Wg6.b(flexibleTextView, "it.tvComplicationSetting");
                    flexibleTextView.setVisibility(4);
                    FlexibleTextView flexibleTextView2 = qg5.l;
                    Wg6.b(flexibleTextView2, "it.tvSelectedComplication");
                    flexibleTextView2.setVisibility(4);
                    FlexibleTextView flexibleTextView3 = qg5.h;
                    Wg6.b(flexibleTextView3, "it.tvComplicationDetail");
                    flexibleTextView3.setVisibility(4);
                    return;
                }
                Wg6.n("mBinding");
                throw null;
            }
        } else {
            Wg6.n("complicationAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final void S6(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceComplicationFragment", "showSettingData, setting = " + str);
        Qg5 qg5 = this.i;
        if (qg5 == null) {
            Wg6.n("mBinding");
            throw null;
        } else if (!Vt7.l(str)) {
            ConstraintLayout constraintLayout = qg5.b;
            Wg6.b(constraintLayout, "it.clComplicationSetting");
            constraintLayout.setVisibility(0);
            FlexibleTextView flexibleTextView = qg5.j;
            Wg6.b(flexibleTextView, "it.tvComplicationSetting");
            flexibleTextView.setVisibility(0);
            qg5.j.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            FlexibleTextView flexibleTextView2 = qg5.j;
            Wg6.b(flexibleTextView2, "it.tvComplicationSetting");
            FragmentActivity activity = getActivity();
            Resources resources = activity != null ? activity.getResources() : null;
            if (resources != null) {
                flexibleTextView2.setCompoundDrawablePadding((int) resources.getDimension(2131165420));
                qg5.j.setCompoundDrawablesWithIntrinsicBounds(0, 0, 2131231049, 0);
                ImageView imageView = qg5.d;
                Wg6.b(imageView, "it.ivComplicationSetting");
                imageView.setVisibility(4);
                FlexibleTextView flexibleTextView3 = qg5.j;
                Wg6.b(flexibleTextView3, "it.tvComplicationSetting");
                flexibleTextView3.setText(str);
                return;
            }
            Wg6.i();
            throw null;
        } else {
            ConstraintLayout constraintLayout2 = qg5.b;
            Wg6.b(constraintLayout2, "it.clComplicationSetting");
            constraintLayout2.setVisibility(4);
            FlexibleTextView flexibleTextView4 = qg5.j;
            Wg6.b(flexibleTextView4, "it.tvComplicationSetting");
            flexibleTextView4.setVisibility(4);
        }
    }

    @DexIgnore
    public final void T6(Complication complication) {
        Qg5 qg5 = this.i;
        if (qg5 != null) {
            FlexibleTextView flexibleTextView = qg5.l;
            Wg6.b(flexibleTextView, "binding.tvSelectedComplication");
            flexibleTextView.setVisibility(0);
            FlexibleTextView flexibleTextView2 = qg5.h;
            Wg6.b(flexibleTextView2, "binding.tvComplicationDetail");
            flexibleTextView2.setVisibility(0);
            FlexibleTextView flexibleTextView3 = qg5.l;
            Wg6.b(flexibleTextView3, "binding.tvSelectedComplication");
            flexibleTextView3.setText(Um5.d(PortfolioApp.get.instance(), complication.getNameKey(), complication.getName()));
            FlexibleTextView flexibleTextView4 = qg5.h;
            Wg6.b(flexibleTextView4, "binding.tvComplicationDetail");
            flexibleTextView4.setText(Um5.d(PortfolioApp.get.instance(), complication.getDescriptionKey(), complication.getDescription()));
            ComplicationsAdapter complicationsAdapter = this.j;
            if (complicationsAdapter != null) {
                int l2 = complicationsAdapter.l(complication.getComplicationId());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchFaceComplicationFragment", "updateDetailComplication complicationId=" + complication.getComplicationId() + " scrollTo " + l2);
                if (l2 >= 0) {
                    qg5.f.scrollToPosition(l2);
                    return;
                }
                return;
            }
            Wg6.n("complicationAdapter");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        PortfolioApp.get.instance().getIface().G0().a(this);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WatchFaceEditActivity watchFaceEditActivity = (WatchFaceEditActivity) activity;
            Po4 po4 = this.g;
            if (po4 != null) {
                Ts0 a2 = Vs0.f(watchFaceEditActivity, po4).a(WatchFaceComplicationViewModel.class);
                Wg6.b(a2, "ViewModelProviders.of(ac\u2026ionViewModel::class.java)");
                this.h = (WatchFaceComplicationViewModel) a2;
                P6();
                Q6();
                return;
            }
            Wg6.n("viewModelFactory");
            throw null;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.watchface.edit.WatchFaceEditActivity");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        SecondTimezoneSetting secondTimezoneSetting;
        CommuteTimeSetting commuteTimeSetting;
        super.onActivityResult(i2, i3, intent);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceComplicationFragment", "onActivityResult requestCode " + i2);
        if (i2 != 100) {
            if (i2 == 106 && i3 == -1 && intent != null && (commuteTimeSetting = (CommuteTimeSetting) intent.getParcelableExtra("COMMUTE_TIME_SETTING")) != null) {
                S6(commuteTimeSetting.getAddress());
            }
        } else if (i3 == -1 && intent != null && (secondTimezoneSetting = (SecondTimezoneSetting) intent.getParcelableExtra("SECOND_TIMEZONE")) != null) {
            S6(secondTimezoneSetting.getTimeZoneName());
            Gc7 d = Hc7.c.d(this);
            if (d != null) {
                d.y("second-timezone");
            }
            WatchFaceComplicationViewModel watchFaceComplicationViewModel = this.h;
            if (watchFaceComplicationViewModel != null) {
                watchFaceComplicationViewModel.A("second-timezone", secondTimezoneSetting);
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Qg5 c = Qg5.c(layoutInflater);
        Wg6.b(c, "WatchFaceComplicationFra\u2026Binding.inflate(inflater)");
        this.i = c;
        if (c != null) {
            ConstraintLayout b = c.b();
            Wg6.b(b, "mBinding.root");
            return b;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        WatchFaceComplicationViewModel watchFaceComplicationViewModel = this.h;
        if (watchFaceComplicationViewModel != null) {
            watchFaceComplicationViewModel.q();
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
