package com.mapped;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Gh<T> extends Vh {
    @DexIgnore
    public Gh(Oh oh) {
        super(oh);
    }

    @DexIgnore
    public abstract void bind(Mi mi, T t);

    @DexIgnore
    @Override // com.mapped.Vh
    public abstract String createQuery();

    @DexIgnore
    public final int handle(T t) {
        Mi acquire = acquire();
        try {
            bind(acquire, t);
            return acquire.executeUpdateDelete();
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.mapped.Gh<T> */
    /* JADX WARN: Multi-variable type inference failed */
    public final int handleMultiple(Iterable<? extends T> iterable) {
        Mi acquire = acquire();
        int i = 0;
        try {
            Iterator<? extends T> it = iterable.iterator();
            while (it.hasNext()) {
                bind(acquire, it.next());
                i += acquire.executeUpdateDelete();
            }
            return i;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final int handleMultiple(T[] tArr) {
        Mi acquire = acquire();
        try {
            int i = 0;
            for (T t : tArr) {
                bind(acquire, t);
                i += acquire.executeUpdateDelete();
            }
            return i;
        } finally {
            release(acquire);
        }
    }
}
