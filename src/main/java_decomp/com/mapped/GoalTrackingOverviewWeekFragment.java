package com.mapped;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.Ai6;
import com.fossil.Aq0;
import com.fossil.Bi6;
import com.fossil.G37;
import com.fossil.Mv5;
import com.fossil.Ts0;
import com.fossil.Vs0;
import com.fossil.X65;
import com.fossil.Y67;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingOverviewWeekFragment extends BaseFragment implements Bi6 {
    @DexIgnore
    public Y67 g;
    @DexIgnore
    public G37<X65> h;
    @DexIgnore
    public Ai6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewWeekFragment b;

        @DexIgnore
        public Ai(GoalTrackingOverviewWeekFragment goalTrackingOverviewWeekFragment, X65 x65) {
            this.b = goalTrackingOverviewWeekFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewWeekFragment.K6(this.b).a().l(2);
        }
    }

    @DexIgnore
    public static final /* synthetic */ Y67 K6(GoalTrackingOverviewWeekFragment goalTrackingOverviewWeekFragment) {
        Y67 y67 = goalTrackingOverviewWeekFragment.g;
        if (y67 != null) {
            return y67;
        }
        Wg6.n("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "GoalTrackingOverviewWeekFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void L6() {
        X65 a2;
        OverviewWeekChart overviewWeekChart;
        G37<X65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null && (overviewWeekChart = a2.v) != null) {
            overviewWeekChart.D("hybridGoalTrackingTab", "nonBrandNonReachGoal");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Ai6 ai6) {
        M6(ai6);
    }

    @DexIgnore
    public void M6(Ai6 ai6) {
        Wg6.c(ai6, "presenter");
        this.i = ai6;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        X65 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onCreateView");
        X65 x65 = (X65) Aq0.f(layoutInflater, 2131558562, viewGroup, false, A6());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Ts0 a3 = Vs0.e(activity).a(Y67.class);
            Wg6.b(a3, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.g = (Y67) a3;
            x65.r.setOnClickListener(new Ai(this, x65));
        }
        this.h = new G37<>(this, x65);
        L6();
        G37<X65> g37 = this.h;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onResume");
        L6();
        Ai6 ai6 = this.i;
        if (ai6 != null) {
            ai6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onStop");
        Ai6 ai6 = this.i;
        if (ai6 != null) {
            ai6.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.Bi6
    public void p(Mv5 mv5) {
        X65 a2;
        OverviewWeekChart overviewWeekChart;
        Wg6.c(mv5, "baseModel");
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "showWeekDetails");
        G37<X65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null && (overviewWeekChart = a2.v) != null) {
            new ArrayList();
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(Mv5.a.b(cVar.d()));
            Mv5.Ai ai = Mv5.a;
            Wg6.b(overviewWeekChart, "it");
            Context context = overviewWeekChart.getContext();
            Wg6.b(context, "it.context");
            BarChart.H(overviewWeekChart, ai.a(context, cVar), false, 2, null);
            overviewWeekChart.r(mv5);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Bi6
    public void x(boolean z) {
        X65 a2;
        G37<X65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null) {
            if (z) {
                OverviewWeekChart overviewWeekChart = a2.v;
                Wg6.b(overviewWeekChart, "binding.weekChart");
                overviewWeekChart.setVisibility(4);
                ConstraintLayout constraintLayout = a2.q;
                Wg6.b(constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            OverviewWeekChart overviewWeekChart2 = a2.v;
            Wg6.b(overviewWeekChart2, "binding.weekChart");
            overviewWeekChart2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.q;
            Wg6.b(constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }
}
