package com.mapped;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.R96;
import com.fossil.Um5;
import com.fossil.imagefilters.FilterType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ImageFilterAdapter extends RecyclerView.g<Bi> {
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ ArrayList<Ai> b;
    @DexIgnore
    public Ci c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ FilterType b;

        @DexIgnore
        public Ai(Bitmap bitmap, FilterType filterType) {
            Wg6.c(bitmap, "image");
            Wg6.c(filterType, "type");
            this.a = bitmap;
            this.b = filterType;
        }

        @DexIgnore
        public final Bitmap a() {
            return this.a;
        }

        @DexIgnore
        public final FilterType b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!Wg6.a(this.a, ai.a) || !Wg6.a(this.b, ai.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            Bitmap bitmap = this.a;
            int hashCode = bitmap != null ? bitmap.hashCode() : 0;
            FilterType filterType = this.b;
            if (filterType != null) {
                i = filterType.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "ImageFilter(image=" + this.a + ", type=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public ImageView a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public /* final */ /* synthetic */ ImageFilterAdapter d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Ci i;
                if (!(this.b.d.getItemCount() <= this.b.getAdapterPosition() || this.b.getAdapterPosition() == -1 || (i = this.b.d.i()) == null)) {
                    Object obj = this.b.d.b.get(this.b.getAdapterPosition());
                    Wg6.b(obj, "mFilterList[adapterPosition]");
                    i.k4((Ai) obj);
                }
                Bi bi = this.b;
                bi.d.n(bi.getAdapterPosition());
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(ImageFilterAdapter imageFilterAdapter, View view) {
            super(view);
            Wg6.c(view, "view");
            this.d = imageFilterAdapter;
            View findViewById = view.findViewById(2131362671);
            Wg6.b(findViewById, "view.findViewById(R.id.iv_background_preview)");
            this.a = (ImageView) findViewById;
            View findViewById2 = view.findViewById(2131363370);
            Wg6.b(findViewById2, "view.findViewById(R.id.tv_name)");
            this.b = (TextView) findViewById2;
            View findViewById3 = view.findViewById(2131363449);
            Wg6.b(findViewById3, "view.findViewById(R.id.v_background_selected)");
            this.c = findViewById3;
            this.a.setOnClickListener(new Aii(this));
        }

        @DexIgnore
        public final void a(Ai ai, int i) {
            String c2;
            Wg6.c(ai, "imageFilter");
            this.a.setImageBitmap(ai.a());
            switch (R96.a[ai.b().ordinal()]) {
                case 1:
                    View view = this.itemView;
                    Wg6.b(view, "itemView");
                    c2 = Um5.c(view.getContext(), 2131886527);
                    break;
                case 2:
                    View view2 = this.itemView;
                    Wg6.b(view2, "itemView");
                    c2 = Um5.c(view2.getContext(), 2131886523);
                    break;
                case 3:
                    View view3 = this.itemView;
                    Wg6.b(view3, "itemView");
                    c2 = Um5.c(view3.getContext(), 2131886525);
                    break;
                case 4:
                    View view4 = this.itemView;
                    Wg6.b(view4, "itemView");
                    c2 = Um5.c(view4.getContext(), 2131886524);
                    break;
                case 5:
                    View view5 = this.itemView;
                    Wg6.b(view5, "itemView");
                    c2 = Um5.c(view5.getContext(), 2131886528);
                    break;
                case 6:
                    View view6 = this.itemView;
                    Wg6.b(view6, "itemView");
                    c2 = Um5.c(view6.getContext(), 2131886526);
                    break;
                default:
                    c2 = "";
                    break;
            }
            this.b.setText(c2);
            if (i == this.d.a) {
                this.c.setVisibility(0);
            } else {
                this.c.setVisibility(8);
            }
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        void k4(Ai ai);
    }

    @DexIgnore
    public ImageFilterAdapter(ArrayList<Ai> arrayList, Ci ci) {
        Wg6.c(arrayList, "mFilterList");
        this.b = arrayList;
        this.c = ci;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ImageFilterAdapter(ArrayList arrayList, Ci ci, int i, Qg6 qg6) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : ci);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public final Ci i() {
        return this.c;
    }

    @DexIgnore
    public void j(Bi bi, int i) {
        Wg6.c(bi, "holder");
        if (getItemCount() > i && i != -1) {
            Ai ai = this.b.get(i);
            Wg6.b(ai, "mFilterList[position]");
            bi.a(ai, i);
        }
    }

    @DexIgnore
    public Bi k(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558683, viewGroup, false);
        Wg6.b(inflate, "LayoutInflater.from(pare\u2026na_filter, parent, false)");
        return new Bi(this, inflate);
    }

    @DexIgnore
    public final void l(List<Ai> list) {
        Wg6.c(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ImageFilterAdapter", "setData size = " + list.size());
        this.b.clear();
        this.b.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void m(Ci ci) {
        this.c = ci;
    }

    @DexIgnore
    public final void n(int i) {
        try {
            if (this.a != i) {
                int i2 = this.a;
                this.a = i;
                notifyItemChanged(i);
                notifyItemChanged(i2);
            }
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().e("ImageFilterAdapter", e.getMessage());
            e.printStackTrace();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        j(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return k(viewGroup, i);
    }
}
