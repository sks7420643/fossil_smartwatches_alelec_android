package com.mapped;

import com.fossil.Dl5;
import com.fossil.Hr7;
import com.fossil.Mn7;
import com.fossil.Um5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;
import java.util.Comparator;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SupportedFunction {
    @DexIgnore
    public static /* final */ SupportedFunction a; // = new SupportedFunction();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return Mn7.c(Long.valueOf(t.getStartTime().getMillis()), Long.valueOf(t2.getStartTime().getMillis()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return Mn7.c(Long.valueOf(t.getTrackedAt().getMillis()), Long.valueOf(t2.getTrackedAt().getMillis()));
        }
    }

    @DexIgnore
    public final String a(long j, int i, boolean z) {
        boolean z2;
        String b;
        int i2 = 12;
        int hourOfDay = new DateTime(j, DateTimeZone.forOffsetMillis(i * 1000)).getHourOfDay();
        if (hourOfDay < 12) {
            if (hourOfDay != 0) {
                i2 = hourOfDay;
            }
            z2 = true;
        } else {
            if (hourOfDay != 12) {
                hourOfDay -= 12;
            }
            i2 = hourOfDay;
            z2 = false;
        }
        String c = Um5.c(PortfolioApp.get.instance(), z2 ? 2131887555 : 2131887557);
        if (i >= 0) {
            b = '+' + Dl5.b(((float) i) / 3600.0f, 1);
        } else {
            b = Dl5.b(((float) i) / 3600.0f, 1);
        }
        if (z) {
            Hr7 hr7 = Hr7.a;
            String c2 = Um5.c(PortfolioApp.get.instance(), 2131887558);
            Wg6.b(c2, "LanguageHelper.getString\u2026ce, R.string.s_time_zone)");
            Hr7 hr72 = Hr7.a;
            Wg6.b(c, "amPmRes");
            String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            String format2 = String.format(c2, Arrays.copyOf(new Object[]{format, b}, 2));
            Wg6.b(format2, "java.lang.String.format(format, *args)");
            return format2;
        } else if (i2 % 6 != 0) {
            return "";
        } else {
            Hr7 hr73 = Hr7.a;
            Wg6.b(c, "amPmRes");
            String format3 = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
            Wg6.b(format3, "java.lang.String.format(format, *args)");
            FLogger.INSTANCE.getLocal().d("SupportedFunction", "temp=" + format3);
            int hashCode = format3.hashCode();
            if (hashCode != 1771) {
                if (hashCode != 1786) {
                    if (hashCode != 48736) {
                        if (hashCode != 48751 || !format3.equals("12p")) {
                            return format3;
                        }
                        String c3 = Um5.c(PortfolioApp.get.instance(), 2131886669);
                        Wg6.b(c3, "LanguageHelper.getString\u2026in_StepsToday_Label__12p)");
                        return c3;
                    } else if (!format3.equals("12a")) {
                        return format3;
                    } else {
                        String c4 = Um5.c(PortfolioApp.get.instance(), 2131886667);
                        Wg6.b(c4, "LanguageHelper.getString\u2026in_StepsToday_Label__12a)");
                        return c4;
                    }
                } else if (!format3.equals("6p")) {
                    return format3;
                } else {
                    String c5 = Um5.c(PortfolioApp.get.instance(), 2131886671);
                    Wg6.b(c5, "LanguageHelper.getString\u2026ain_StepsToday_Label__6p)");
                    return c5;
                }
            } else if (!format3.equals("6a")) {
                return format3;
            } else {
                String c6 = Um5.c(PortfolioApp.get.instance(), 2131886670);
                Wg6.b(c6, "LanguageHelper.getString\u2026ain_StepsToday_Label__6a)");
                return c6;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:137:0x0386  */
    /* JADX WARNING: Removed duplicated region for block: B:227:0x0265 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:236:0x0342 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:241:0x0456 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.mapped.Lc6<java.util.ArrayList<com.portfolio.platform.ui.view.chart.base.BarChart.a>, java.util.ArrayList<java.lang.String>> b(java.util.Date r31, java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample> r32, int r33) {
        /*
        // Method dump skipped, instructions count: 1406
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.SupportedFunction.b(java.util.Date, java.util.List, int):com.mapped.Lc6");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:173:0x021b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x02a5 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x0398 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x02e5  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.mapped.Lc6<java.util.ArrayList<com.portfolio.platform.ui.view.chart.base.BarChart.a>, java.util.ArrayList<java.lang.String>> c(java.util.Date r27, java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> r28) {
        /*
        // Method dump skipped, instructions count: 1175
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.SupportedFunction.c(java.util.Date, java.util.List):com.mapped.Lc6");
    }
}
