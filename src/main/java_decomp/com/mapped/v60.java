package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.E;
import com.fossil.Ey1;
import com.fossil.Zm1;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V60 extends R60 implements Parcelable {
    @DexIgnore
    public static /* final */ Bi CREATOR; // = new Bi(null);
    @DexIgnore
    public /* final */ Ai c;

    @DexIgnore
    public enum Ai {
        CONTINUOUS((byte) 0),
        LOW_POWER((byte) 1),
        DISABLE((byte) 2);
        
        @DexIgnore
        public static /* final */ Aii d; // = new Aii(null);
        @DexIgnore
        public /* final */ byte b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
            }

            @DexIgnore
            public final Ai a(byte b) throws IllegalArgumentException {
                Ai ai;
                Ai[] values = Ai.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        ai = null;
                        break;
                    }
                    ai = values[i];
                    if (ai.a() == b) {
                        break;
                    }
                    i++;
                }
                if (ai != null) {
                    return ai;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public Ai(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Parcelable.Creator<V60> {
        @DexIgnore
        public /* synthetic */ Bi(Qg6 qg6) {
        }

        @DexIgnore
        public final V60 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 1) {
                return new V60(Ai.d.a(bArr[0]));
            }
            throw new IllegalArgumentException(E.b(E.e("Invalid data size: "), bArr.length, ", require: 1"));
        }

        @DexIgnore
        public V60 b(Parcel parcel) {
            return new V60(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public V60 createFromParcel(Parcel parcel) {
            return new V60(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public V60[] newArray(int i) {
            return new V60[i];
        }
    }

    @DexIgnore
    public /* synthetic */ V60(Parcel parcel, Qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            Wg6.b(readString, "parcel.readString()!!");
            this.c = Ai.valueOf(readString);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public V60(Ai ai) {
        super(Zm1.HEART_RATE_MODE);
        this.c = ai;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(this.c.a()).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public String c() {
        return Ey1.a(this.c);
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(V60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((V60) obj).c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.HeartRateModeConfig");
    }

    @DexIgnore
    public final Ai getHeartRateMode() {
        return this.c;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        return (super.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
    }
}
