package com.mapped;

import com.fossil.Hj4;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Hu3<T> {
    @DexIgnore
    T deserialize(JsonElement jsonElement, Type type, Gu3 gu3) throws Hj4;
}
