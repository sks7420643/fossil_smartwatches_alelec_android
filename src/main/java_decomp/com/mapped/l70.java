package com.mapped;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum L70 {
    PLAY((byte) 0),
    PAUSE((byte) 1),
    TOGGLE_PLAY_PAUSE((byte) 2),
    NEXT((byte) 3),
    PREVIOUS((byte) 4),
    VOLUME_UP((byte) 5),
    VOLUME_DOWN((byte) 6);
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final L70 a(byte b) {
            L70[] values = L70.values();
            for (L70 l70 : values) {
                if (l70.a() == b) {
                    return l70;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public L70(byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public final byte a() {
        return this.b;
    }
}
