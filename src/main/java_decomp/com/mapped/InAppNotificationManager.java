package com.mapped;

import android.content.Context;
import com.fossil.Cl5;
import com.fossil.Hr7;
import com.fossil.Hz4;
import com.fossil.Lt4;
import com.fossil.Ss4;
import com.fossil.Um5;
import com.fossil.Ws4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InAppNotificationManager {
    @DexIgnore
    public InAppNotificationManager(InAppNotificationRepository inAppNotificationRepository) {
        Wg6.c(inAppNotificationRepository, "repository");
    }

    @DexIgnore
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void a(Ws4 ws4, Context context) {
        String str;
        String str2;
        String str3;
        String str4;
        String a2;
        String b;
        String a3;
        String b2;
        String a4;
        String e;
        String a5;
        String str5;
        String c;
        int i;
        String str6;
        String str7;
        String a6;
        int i2 = 99;
        Wg6.c(ws4, "notification");
        Wg6.c(context, "context");
        FLogger.INSTANCE.getLocal().e("InAppNotificationManager", "notificationId: " + ws4.c());
        String e2 = ws4.e();
        int hashCode = e2.hashCode();
        if (hashCode == 1433363166 ? !e2.equals("Title_Send_Friend_Request") : hashCode != 1898363949 || !e2.equals("Title_Send_Invitation_Challenge")) {
            String e3 = ws4.e();
            String e4 = ws4.e();
            int hashCode2 = e4.hashCode();
            String str8 = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
            String str9 = "Challenge";
            switch (hashCode2) {
                case -1559094286:
                    if (e4.equals("Title_Notification_End_Challenge")) {
                        str4 = Um5.c(PortfolioApp.get.instance(), 2131887224);
                        Wg6.b(str4, "LanguageHelper.getString\u2026tification_End_Challenge)");
                        Hr7 hr7 = Hr7.a;
                        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886190);
                        Wg6.b(c2, "LanguageHelper.getString\u2026tification_End_Challenge)");
                        Ss4 b3 = ws4.b();
                        if (!(b3 == null || (b = b3.b()) == null)) {
                            str9 = b;
                        }
                        str2 = String.format(c2, Arrays.copyOf(new Object[]{str9}, 1));
                        Wg6.b(str2, "java.lang.String.format(format, *args)");
                        Ss4 b4 = ws4.b();
                        if (!(b4 == null || (a2 = b4.a()) == null)) {
                            i2 = a2.hashCode();
                            str3 = str4;
                            e3 = str2;
                            i = i2;
                            str6 = str3;
                            break;
                        }
                        str3 = str4;
                        e3 = str2;
                        i = i2;
                        str6 = str3;
                    }
                    str6 = "";
                    i = 99;
                    break;
                case -764698119:
                    if (e4.equals("Title_Notification_Start_Challenge")) {
                        str4 = Um5.c(PortfolioApp.get.instance(), 2131887227);
                        Wg6.b(str4, "LanguageHelper.getString\u2026fication_Start_Challenge)");
                        Hr7 hr72 = Hr7.a;
                        String c3 = Um5.c(PortfolioApp.get.instance(), 2131886193);
                        Wg6.b(c3, "LanguageHelper.getString\u2026fication_Start_Challenge)");
                        Ss4 b5 = ws4.b();
                        if (!(b5 == null || (b2 = b5.b()) == null)) {
                            str9 = b2;
                        }
                        str2 = String.format(c3, Arrays.copyOf(new Object[]{str9}, 1));
                        Wg6.b(str2, "java.lang.String.format(format, *args)");
                        Ss4 b6 = ws4.b();
                        if (!(b6 == null || (a3 = b6.a()) == null)) {
                            i2 = a3.hashCode();
                            str3 = str4;
                            e3 = str2;
                            i = i2;
                            str6 = str3;
                            break;
                        }
                        str3 = str4;
                        e3 = str2;
                        i = i2;
                        str6 = str3;
                    }
                    str6 = "";
                    i = 99;
                    break;
                case -473527954:
                    if (e4.equals("Title_Respond_Invitation_Challenge")) {
                        str = Um5.c(PortfolioApp.get.instance(), 2131887228);
                        Wg6.b(str, "LanguageHelper.getString\u2026ond_Invitation_Challenge)");
                        Hz4 hz4 = Hz4.a;
                        Lt4 d = ws4.d();
                        String b7 = d != null ? d.b() : null;
                        Lt4 d2 = ws4.d();
                        String d3 = d2 != null ? d2.d() : null;
                        Lt4 d4 = ws4.d();
                        if (!(d4 == null || (e = d4.e()) == null)) {
                            str8 = e;
                        }
                        String a7 = hz4.a(b7, d3, str8);
                        Hr7 hr73 = Hr7.a;
                        String c4 = Um5.c(PortfolioApp.get.instance(), 2131886194);
                        Wg6.b(c4, "LanguageHelper.getString\u2026ond_Invitation_Challenge)");
                        str2 = String.format(c4, Arrays.copyOf(new Object[]{a7}, 1));
                        Wg6.b(str2, "java.lang.String.format(format, *args)");
                        Ss4 b8 = ws4.b();
                        if (!(b8 == null || (a4 = b8.a()) == null)) {
                            i2 = a4.hashCode();
                            str3 = str;
                            e3 = str2;
                            i = i2;
                            str6 = str3;
                            break;
                        }
                        str3 = str;
                        e3 = str2;
                        i = i2;
                        str6 = str3;
                    }
                    str6 = "";
                    i = 99;
                    break;
                case 238453777:
                    if (e4.equals("Title_Notification_Reached_Goal_Challenge")) {
                        str3 = Um5.c(PortfolioApp.get.instance(), 2131887225);
                        Wg6.b(str3, "LanguageHelper.getString\u2026n_Reached_Goal_Challenge)");
                        String c5 = Um5.c(PortfolioApp.get.instance(), 2131886191);
                        Wg6.b(c5, "LanguageHelper.getString\u2026n_Reached_Goal_Challenge)");
                        Ss4 b9 = ws4.b();
                        i2 = (b9 == null || (a5 = b9.a()) == null) ? 99 : a5.hashCode();
                        e3 = c5;
                        i = i2;
                        str6 = str3;
                        break;
                    }
                    str6 = "";
                    i = 99;
                    break;
                case 634854495:
                    if (e4.equals("Title_Accepted_Friend_Request")) {
                        str = Um5.c(PortfolioApp.get.instance(), 2131887223);
                        Wg6.b(str, "LanguageHelper.getString\u2026_Accepted_Friend_Request)");
                        Hz4 hz42 = Hz4.a;
                        Lt4 d5 = ws4.d();
                        String b10 = d5 != null ? d5.b() : null;
                        Lt4 d6 = ws4.d();
                        String d7 = d6 != null ? d6.d() : null;
                        Lt4 d8 = ws4.d();
                        if (d8 == null || (str5 = d8.e()) == null) {
                            str5 = str8;
                        }
                        String a8 = hz42.a(b10, d7, str5);
                        Hr7 hr74 = Hr7.a;
                        String c6 = Um5.c(PortfolioApp.get.instance(), 2131886189);
                        Wg6.b(c6, "LanguageHelper.getString\u2026_Accepted_Friend_Request)");
                        str2 = String.format(c6, Arrays.copyOf(new Object[]{a8}, 1));
                        Wg6.b(str2, "java.lang.String.format(format, *args)");
                        Lt4 d9 = ws4.d();
                        if (!(d9 == null || (c = d9.c()) == null)) {
                            i2 = c.hashCode();
                            str3 = str;
                            e3 = str2;
                            i = i2;
                            str6 = str3;
                            break;
                        }
                        str3 = str;
                        e3 = str2;
                        i = i2;
                        str6 = str3;
                    }
                    str6 = "";
                    i = 99;
                    break;
                case 1669546642:
                    if (e4.equals("Title_Notification_Remind_End_Challenge")) {
                        str4 = Um5.c(PortfolioApp.get.instance(), 2131887224);
                        Wg6.b(str4, "LanguageHelper.getString\u2026tification_End_Challenge)");
                        Hr7 hr75 = Hr7.a;
                        String c7 = Um5.c(PortfolioApp.get.instance(), 2131886192);
                        Wg6.b(c7, "LanguageHelper.getString\u2026ion_Remind_End_Challenge)");
                        Ss4 b11 = ws4.b();
                        if (b11 == null || (str7 = b11.b()) == null) {
                            str7 = str9;
                        }
                        str2 = String.format(c7, Arrays.copyOf(new Object[]{str7}, 1));
                        Wg6.b(str2, "java.lang.String.format(format, *args)");
                        Ss4 b12 = ws4.b();
                        if (!(b12 == null || (a6 = b12.a()) == null)) {
                            i2 = a6.hashCode();
                            str3 = str4;
                            e3 = str2;
                            i = i2;
                            str6 = str3;
                            break;
                        }
                        str3 = str4;
                        e3 = str2;
                        i = i2;
                        str6 = str3;
                    }
                    str6 = "";
                    i = 99;
                    break;
                default:
                    str6 = "";
                    i = 99;
                    break;
            }
            Cl5.c.g(PortfolioApp.get.instance(), i, str6, e3, null, null);
            return;
        }
        b(ws4, context);
    }

    @DexIgnore
    public final void b(Ws4 ws4, Context context) {
        Cl5.c.i(ws4, context);
    }
}
