package com.mapped;

import com.fossil.Hm7;
import com.fossil.Mj4;
import com.fossil.Ms4;
import com.fossil.Zi4;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCPlayerConverter {
    @DexIgnore
    public /* final */ Gson a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends TypeToken<List<? extends Ms4>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends TypeToken<List<? extends Ms4>> {
    }

    @DexIgnore
    public BCPlayerConverter() {
        Gson d = new Zi4().d();
        Wg6.b(d, "GsonBuilder().create()");
        this.a = d;
    }

    @DexIgnore
    public final String a(List<Ms4> list) {
        Wg6.c(list, "players");
        try {
            String t = this.a.t(list);
            Wg6.b(t, "gson.toJson(players)");
            return t;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @DexIgnore
    public final List<Ms4> b(String str) {
        List<Ms4> e;
        try {
            Object l = this.a.l(str, new Ai().getType());
            Wg6.b(l, "gson.fromJson(value, obj\u2026ist<BCPlayer>>() {}.type)");
            return (List) l;
        } catch (Exception e2) {
            if (!(e2 instanceof Mj4)) {
                return Hm7.e();
            }
            try {
                FLogger.INSTANCE.getLocal().e("BCPlayerConverter", "stringToPlayers - apply date format for special case");
                Zi4 zi4 = new Zi4();
                zi4.g("MM dd, yyyy HH:mm:ss");
                e = (List) zi4.d().l(str, new Bi().getType());
            } catch (Exception e3) {
                e3.printStackTrace();
                e = Hm7.e();
            }
            Wg6.b(e, "try {\n                  \u2026ayer>()\n                }");
            return e;
        }
    }
}
