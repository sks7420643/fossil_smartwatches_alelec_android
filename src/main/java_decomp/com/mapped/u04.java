package com.mapped;

import android.os.Handler;
import android.os.Looper;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class U04 {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Executor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Executor {
        @DexIgnore
        public /* final */ Handler b; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public void execute(Runnable runnable) {
            Wg6.c(runnable, Constants.COMMAND);
            this.b.post(runnable);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public U04() {
        /*
            r3 = this;
            java.util.concurrent.ExecutorService r0 = java.util.concurrent.Executors.newSingleThreadExecutor()
            java.lang.String r1 = "Executors.newSingleThreadExecutor()"
            com.mapped.Wg6.b(r0, r1)
            r1 = 3
            java.util.concurrent.ExecutorService r1 = java.util.concurrent.Executors.newFixedThreadPool(r1)
            java.lang.String r2 = "Executors.newFixedThreadPool(3)"
            com.mapped.Wg6.b(r1, r2)
            com.mapped.U04$Ai r2 = new com.mapped.U04$Ai
            r2.<init>()
            r3.<init>(r0, r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.U04.<init>():void");
    }

    @DexIgnore
    public U04(Executor executor, Executor executor2, Executor executor3) {
        this.a = executor;
        this.b = executor2;
    }

    @DexIgnore
    public Executor a() {
        return this.a;
    }

    @DexIgnore
    public Executor b() {
        return this.b;
    }
}
