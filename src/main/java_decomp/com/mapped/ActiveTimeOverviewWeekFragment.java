package com.mapped;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.Mv5;
import com.fossil.Ne6;
import com.fossil.Oe6;
import com.fossil.Y15;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActiveTimeOverviewWeekFragment extends BaseFragment implements Oe6 {
    @DexIgnore
    public G37<Y15> g;
    @DexIgnore
    public Ne6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "ActiveTimeOverviewWeekFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void K6() {
        Y15 a2;
        OverviewWeekChart overviewWeekChart;
        G37<Y15> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewWeekChart = a2.q) != null) {
            DeviceHelper.Ai ai = DeviceHelper.o;
            Ne6 ne6 = this.h;
            if (ai.w(ne6 != null ? ne6.n() : null)) {
                overviewWeekChart.D("dianaActiveMinutesTab", "nonBrandNonReachGoal");
            } else {
                overviewWeekChart.D("hybridActiveMinutesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    public void L6(Ne6 ne6) {
        Wg6.c(ne6, "presenter");
        this.h = ne6;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Ne6 ne6) {
        L6(ne6);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Y15 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onCreateView");
        this.g = new G37<>(this, (Y15) Aq0.f(layoutInflater, 2131558495, viewGroup, false, A6()));
        K6();
        G37<Y15> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onResume");
        K6();
        Ne6 ne6 = this.h;
        if (ne6 != null) {
            ne6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onStop");
        Ne6 ne6 = this.h;
        if (ne6 != null) {
            ne6.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.Oe6
    public void p(Mv5 mv5) {
        Y15 a2;
        OverviewWeekChart overviewWeekChart;
        Wg6.c(mv5, "baseModel");
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewWeekFragment", "showWeekDetails");
        G37<Y15> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewWeekChart = a2.q) != null) {
            new ArrayList();
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(Mv5.a.b(cVar.d()));
            Mv5.Ai ai = Mv5.a;
            Wg6.b(overviewWeekChart, "it");
            Context context = overviewWeekChart.getContext();
            Wg6.b(context, "it.context");
            BarChart.H(overviewWeekChart, ai.a(context, cVar), false, 2, null);
            overviewWeekChart.r(mv5);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
