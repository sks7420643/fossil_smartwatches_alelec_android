package com.mapped;

import android.os.Parcel;
import com.fossil.G80;
import com.fossil.Jd0;
import com.fossil.Mp1;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class X90 extends Mp1 {
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public X90(Parcel parcel) {
        super(parcel);
        this.d = parcel.readInt();
    }

    @DexIgnore
    public X90(E90 e90, byte b, int i) {
        super(e90, b);
        this.d = i;
    }

    @DexIgnore
    public final int b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.d == ((X90) obj).d;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.DeviceRequest");
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public int hashCode() {
        return (super.hashCode() * 31) + this.d;
    }

    @DexIgnore
    @Override // com.fossil.Mp1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(super.toJSONObject(), Jd0.q, Integer.valueOf(this.d));
    }

    @DexIgnore
    @Override // com.fossil.Mp1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
    }
}
