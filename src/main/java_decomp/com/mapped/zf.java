package com.mapped;

import com.fossil.Jv0;
import com.fossil.Wu0;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zf {
    @DexIgnore
    public static /* final */ Comparator<Gi> a; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Comparator<Gi> {
        @DexIgnore
        public int a(Gi gi, Gi gi2) {
            int i = gi.a - gi2.a;
            return i == 0 ? gi.b - gi2.b : i;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // java.util.Comparator
        public /* bridge */ /* synthetic */ int compare(Gi gi, Gi gi2) {
            return a(gi, gi2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Bi {
        @DexIgnore
        public abstract boolean a(int i, int i2);

        @DexIgnore
        public abstract boolean b(int i, int i2);

        @DexIgnore
        public abstract Object c(int i, int i2);

        @DexIgnore
        public abstract int d();

        @DexIgnore
        public abstract int e();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public /* final */ List<Gi> a;
        @DexIgnore
        public /* final */ int[] b;
        @DexIgnore
        public /* final */ int[] c;
        @DexIgnore
        public /* final */ Bi d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ boolean g;

        @DexIgnore
        public Ci(Bi bi, List<Gi> list, int[] iArr, int[] iArr2, boolean z) {
            this.a = list;
            this.b = iArr;
            this.c = iArr2;
            Arrays.fill(iArr, 0);
            Arrays.fill(this.c, 0);
            this.d = bi;
            this.e = bi.e();
            this.f = bi.d();
            this.g = z;
            a();
            h();
        }

        @DexIgnore
        public static Ei j(List<Ei> list, int i, boolean z) {
            int size = list.size() - 1;
            while (size >= 0) {
                Ei ei = list.get(size);
                if (ei.a == i && ei.c == z) {
                    list.remove(size);
                    while (size < list.size()) {
                        Ei ei2 = list.get(size);
                        ei2.b = (z ? 1 : -1) + ei2.b;
                        size++;
                    }
                    return ei;
                }
                size--;
            }
            return null;
        }

        @DexIgnore
        public final void a() {
            Gi gi = this.a.isEmpty() ? null : this.a.get(0);
            if (gi == null || gi.a != 0 || gi.b != 0) {
                Gi gi2 = new Gi();
                gi2.a = 0;
                gi2.b = 0;
                gi2.d = false;
                gi2.c = 0;
                gi2.e = false;
                this.a.add(0, gi2);
            }
        }

        @DexIgnore
        public int b(int i) {
            if (i < 0 || i >= this.e) {
                throw new IndexOutOfBoundsException("Index out of bounds - passed position = " + i + ", old list size = " + this.e);
            }
            int i2 = this.b[i];
            if ((i2 & 31) == 0) {
                return -1;
            }
            return i2 >> 5;
        }

        @DexIgnore
        public final void c(List<Ei> list, Jv0 jv0, int i, int i2, int i3) {
            if (!this.g) {
                jv0.b(i, i2);
                return;
            }
            for (int i4 = i2 - 1; i4 >= 0; i4--) {
                int i5 = i3 + i4;
                int i6 = this.c[i5] & 31;
                if (i6 == 0) {
                    jv0.b(i, 1);
                    for (Ei ei : list) {
                        ei.b++;
                    }
                } else if (i6 == 4 || i6 == 8) {
                    int i7 = this.c[i5] >> 5;
                    jv0.a(j(list, i7, true).b, i);
                    if (i6 == 4) {
                        jv0.d(i, 1, this.d.c(i7, i5));
                    }
                } else if (i6 == 16) {
                    list.add(new Ei(i5, i, false));
                } else {
                    throw new IllegalStateException("unknown flag for pos " + i5 + " " + Long.toBinaryString((long) i6));
                }
            }
        }

        @DexIgnore
        public final void d(List<Ei> list, Jv0 jv0, int i, int i2, int i3) {
            if (!this.g) {
                jv0.c(i, i2);
                return;
            }
            for (int i4 = i2 - 1; i4 >= 0; i4--) {
                int i5 = i3 + i4;
                int i6 = this.b[i5] & 31;
                if (i6 == 0) {
                    jv0.c(i + i4, 1);
                    for (Ei ei : list) {
                        ei.b--;
                    }
                } else if (i6 == 4 || i6 == 8) {
                    int i7 = this.b[i5] >> 5;
                    Ei j = j(list, i7, false);
                    jv0.a(i + i4, j.b - 1);
                    if (i6 == 4) {
                        jv0.d(j.b - 1, 1, this.d.c(i5, i7));
                    }
                } else if (i6 == 16) {
                    list.add(new Ei(i5, i + i4, true));
                } else {
                    throw new IllegalStateException("unknown flag for pos " + i5 + " " + Long.toBinaryString((long) i6));
                }
            }
        }

        @DexIgnore
        public void e(Jv0 jv0) {
            Wu0 wu0 = jv0 instanceof Wu0 ? (Wu0) jv0 : new Wu0(jv0);
            List<Ei> arrayList = new ArrayList<>();
            int i = this.e;
            int i2 = this.f;
            int size = this.a.size() - 1;
            int i3 = i2;
            while (size >= 0) {
                Gi gi = this.a.get(size);
                int i4 = gi.c;
                int i5 = gi.a + i4;
                int i6 = gi.b + i4;
                if (i5 < i) {
                    d(arrayList, wu0, i5, i - i5, i5);
                }
                if (i6 < i3) {
                    c(arrayList, wu0, i5, i3 - i6, i6);
                }
                for (int i7 = i4 - 1; i7 >= 0; i7--) {
                    int[] iArr = this.b;
                    int i8 = gi.a;
                    if ((iArr[i8 + i7] & 31) == 2) {
                        wu0.d(i8 + i7, 1, this.d.c(i8 + i7, gi.b + i7));
                    }
                }
                i = gi.a;
                size--;
                i3 = gi.b;
            }
            wu0.e();
        }

        @DexIgnore
        public final void f(int i, int i2, int i3) {
            if (this.b[i - 1] == 0) {
                g(i, i2, i3, false);
            }
        }

        @DexIgnore
        public final boolean g(int i, int i2, int i3, boolean z) {
            int i4;
            int i5;
            if (z) {
                int i6 = i2 - 1;
                i4 = i6;
                i2 = i6;
                i5 = i;
            } else {
                int i7 = i - 1;
                i4 = i7;
                i5 = i7;
            }
            while (i3 >= 0) {
                Gi gi = this.a.get(i3);
                int i8 = gi.a;
                int i9 = gi.c;
                int i10 = gi.b;
                int i11 = 8;
                if (z) {
                    while (true) {
                        i5--;
                        if (i5 < i8 + i9) {
                            continue;
                            break;
                        } else if (this.d.b(i5, i4)) {
                            int i12 = this.d.a(i5, i4) ? 8 : 4;
                            this.c[i4] = (i5 << 5) | 16;
                            this.b[i5] = i12 | (i4 << 5);
                            return true;
                        }
                    }
                } else {
                    for (int i13 = i2 - 1; i13 >= i10 + i9; i13--) {
                        if (this.d.b(i4, i13)) {
                            if (!this.d.a(i4, i13)) {
                                i11 = 4;
                            }
                            int i14 = i - 1;
                            this.b[i14] = (i13 << 5) | 16;
                            this.c[i13] = i11 | (i14 << 5);
                            return true;
                        }
                    }
                    continue;
                }
                int i15 = gi.a;
                i3--;
                i2 = gi.b;
                i5 = i15;
            }
            return false;
        }

        @DexIgnore
        public final void h() {
            int i = this.e;
            int i2 = this.f;
            for (int size = this.a.size() - 1; size >= 0; size--) {
                Gi gi = this.a.get(size);
                int i3 = gi.a;
                int i4 = gi.c;
                int i5 = gi.b;
                if (this.g) {
                    while (i > i3 + i4) {
                        f(i, i2, size);
                        i--;
                    }
                    while (i2 > i5 + i4) {
                        i(i, i2, size);
                        i2--;
                    }
                }
                for (int i6 = 0; i6 < gi.c; i6++) {
                    int i7 = gi.a + i6;
                    int i8 = gi.b + i6;
                    int i9 = this.d.a(i7, i8) ? 1 : 2;
                    this.b[i7] = (i8 << 5) | i9;
                    this.c[i8] = i9 | (i7 << 5);
                }
                i = gi.a;
                i2 = gi.b;
            }
        }

        @DexIgnore
        public final void i(int i, int i2, int i3) {
            if (this.c[i2 - 1] == 0) {
                g(i, i2, i3, true);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Di<T> {
        @DexIgnore
        public abstract boolean areContentsTheSame(T t, T t2);

        @DexIgnore
        public abstract boolean areItemsTheSame(T t, T t2);

        @DexIgnore
        public Object getChangePayload(T t, T t2) {
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public Ei(int i, int i2, boolean z) {
            this.a = i;
            this.b = i2;
            this.c = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Fi {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;

        @DexIgnore
        public Fi() {
        }

        @DexIgnore
        public Fi(int i, int i2, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Gi {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public boolean e;
    }

    @DexIgnore
    public static Ci a(Bi bi) {
        return b(bi, true);
    }

    @DexIgnore
    public static Ci b(Bi bi, boolean z) {
        int e = bi.e();
        int d = bi.d();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new Fi(0, e, 0, d));
        int abs = e + d + Math.abs(e - d);
        int i = abs * 2;
        int[] iArr = new int[i];
        int[] iArr2 = new int[i];
        ArrayList arrayList3 = new ArrayList();
        while (!arrayList2.isEmpty()) {
            Fi fi = (Fi) arrayList2.remove(arrayList2.size() - 1);
            Gi c = c(bi, fi.a, fi.b, fi.c, fi.d, iArr, iArr2, abs);
            if (c != null) {
                if (c.c > 0) {
                    arrayList.add(c);
                }
                c.a += fi.a;
                c.b += fi.c;
                Fi fi2 = arrayList3.isEmpty() ? new Fi() : (Fi) arrayList3.remove(arrayList3.size() - 1);
                fi2.a = fi.a;
                fi2.c = fi.c;
                if (c.e) {
                    fi2.b = c.a;
                    fi2.d = c.b;
                } else if (c.d) {
                    fi2.b = c.a - 1;
                    fi2.d = c.b;
                } else {
                    fi2.b = c.a;
                    fi2.d = c.b - 1;
                }
                arrayList2.add(fi2);
                if (!c.e) {
                    int i2 = c.a;
                    int i3 = c.c;
                    fi.a = i2 + i3;
                    fi.c = c.b + i3;
                } else if (c.d) {
                    int i4 = c.a;
                    int i5 = c.c;
                    fi.a = i4 + i5 + 1;
                    fi.c = c.b + i5;
                } else {
                    int i6 = c.a;
                    int i7 = c.c;
                    fi.a = i6 + i7;
                    fi.c = c.b + i7 + 1;
                }
                arrayList2.add(fi);
            } else {
                arrayList3.add(fi);
            }
        }
        Collections.sort(arrayList, a);
        return new Ci(bi, arrayList, iArr, iArr2, z);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0044, code lost:
        if (r20[r2 - 1] < r20[r2 + 1]) goto L_0x0046;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00be, code lost:
        if (r21[r2 - 1] < r21[r2 + 1]) goto L_0x00c0;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.mapped.Zf.Gi c(com.mapped.Zf.Bi r15, int r16, int r17, int r18, int r19, int[] r20, int[] r21, int r22) {
        /*
        // Method dump skipped, instructions count: 296
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.Zf.c(com.mapped.Zf$Bi, int, int, int, int, int[], int[], int):com.mapped.Zf$Gi");
    }
}
