package com.mapped;

import com.fossil.Uk7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Coroutine<P1, P2, R> extends Uk7<R> {
    @DexIgnore
    R invoke(P1 p1, P2 p2);
}
