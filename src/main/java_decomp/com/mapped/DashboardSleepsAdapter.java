package com.mapped;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ax5;
import com.fossil.Du0;
import com.fossil.Hr7;
import com.fossil.Jl5;
import com.fossil.Ll5;
import com.fossil.Uf5;
import com.fossil.Um5;
import com.fossil.Wf5;
import com.fossil.Xq0;
import com.fossil.Zw5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardSleepsAdapter extends Du0<SleepSummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar c; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ PortfolioApp k;
    @DexIgnore
    public /* final */ Zw5 l;
    @DexIgnore
    public /* final */ FragmentManager m;
    @DexIgnore
    public /* final */ BaseFragment n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public Date a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public int f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;

        @DexIgnore
        public Ai(Date date, boolean z, boolean z2, String str, String str2, int i, String str3, String str4) {
            Wg6.c(str, "mDayOfWeek");
            Wg6.c(str2, "mDayOfMonth");
            Wg6.c(str3, "mDailyUnit");
            Wg6.c(str4, "mDailyEst");
            this.a = date;
            this.b = z;
            this.c = z2;
            this.d = str;
            this.e = str2;
            this.f = i;
            this.g = str3;
            this.h = str4;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(Date date, boolean z, boolean z2, String str, String str2, int i, String str3, String str4, int i2, Qg6 qg6) {
            this((i2 & 1) != 0 ? null : date, (i2 & 2) != 0 ? false : z, (i2 & 4) != 0 ? false : z2, (i2 & 8) != 0 ? "" : str, (i2 & 16) != 0 ? "" : str2, (i2 & 32) == 0 ? i : 0, (i2 & 64) != 0 ? "" : str3, (i2 & 128) == 0 ? str4 : "");
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final String b() {
            return this.g;
        }

        @DexIgnore
        public final int c() {
            return this.f;
        }

        @DexIgnore
        public final Date d() {
            return this.a;
        }

        @DexIgnore
        public final String e() {
            return this.e;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final boolean g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        public final void i(String str) {
            Wg6.c(str, "<set-?>");
            this.h = str;
        }

        @DexIgnore
        public final void j(String str) {
            Wg6.c(str, "<set-?>");
            this.g = str;
        }

        @DexIgnore
        public final void k(int i) {
            this.f = i;
        }

        @DexIgnore
        public final void l(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void m(String str) {
            Wg6.c(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final void n(String str) {
            Wg6.c(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final void o(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public final void p(boolean z) {
            this.b = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public Date a;
        @DexIgnore
        public /* final */ Uf5 b;
        @DexIgnore
        public /* final */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ DashboardSleepsAdapter d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date date = this.b.a;
                if (date != null) {
                    this.b.d.l.Q(date);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(DashboardSleepsAdapter dashboardSleepsAdapter, Uf5 uf5, View view) {
            super(view);
            Wg6.c(uf5, "binding");
            Wg6.c(view, "root");
            this.d = dashboardSleepsAdapter;
            this.b = uf5;
            this.c = view;
            uf5.n().setOnClickListener(new Aii(this));
            this.b.s.setTextColor(dashboardSleepsAdapter.f);
            this.b.v.setTextColor(dashboardSleepsAdapter.f);
        }

        @DexIgnore
        public void b(SleepSummary sleepSummary) {
            Ai v = this.d.v(sleepSummary);
            this.a = v.d();
            FlexibleTextView flexibleTextView = this.b.u;
            Wg6.b(flexibleTextView, "binding.ftvDayOfWeek");
            flexibleTextView.setText(v.f());
            FlexibleTextView flexibleTextView2 = this.b.t;
            Wg6.b(flexibleTextView2, "binding.ftvDayOfMonth");
            flexibleTextView2.setText(v.e());
            if (v.c() <= 0) {
                ConstraintLayout constraintLayout = this.b.q;
                Wg6.b(constraintLayout, "binding.clDailyValue");
                constraintLayout.setVisibility(8);
                FlexibleTextView flexibleTextView3 = this.b.s;
                Wg6.b(flexibleTextView3, "binding.ftvDailyUnit");
                flexibleTextView3.setVisibility(0);
            } else {
                ConstraintLayout constraintLayout2 = this.b.q;
                Wg6.b(constraintLayout2, "binding.clDailyValue");
                constraintLayout2.setVisibility(0);
                FlexibleTextView flexibleTextView4 = this.b.A;
                Wg6.b(flexibleTextView4, "binding.tvMin");
                flexibleTextView4.setText(String.valueOf(Ll5.b(v.c())));
                FlexibleTextView flexibleTextView5 = this.b.y;
                Wg6.b(flexibleTextView5, "binding.tvHour");
                flexibleTextView5.setText(String.valueOf(Ll5.a(v.c())));
                FlexibleTextView flexibleTextView6 = this.b.B;
                Wg6.b(flexibleTextView6, "binding.tvMinUnit");
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131886890);
                Wg6.b(c2, "LanguageHelper.getString\u2026reviations_Minutes__Mins)");
                if (c2 != null) {
                    String lowerCase = c2.toLowerCase();
                    Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    flexibleTextView6.setText(lowerCase);
                    FlexibleTextView flexibleTextView7 = this.b.s;
                    Wg6.b(flexibleTextView7, "binding.ftvDailyUnit");
                    flexibleTextView7.setVisibility(8);
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            FlexibleTextView flexibleTextView8 = this.b.s;
            Wg6.b(flexibleTextView8, "binding.ftvDailyUnit");
            flexibleTextView8.setText(v.b());
            FlexibleTextView flexibleTextView9 = this.b.v;
            Wg6.b(flexibleTextView9, "binding.ftvEst");
            flexibleTextView9.setText(v.a());
            if (v.g()) {
                this.b.s.setTextColor(W6.d(PortfolioApp.get.instance(), 2131099938));
                FlexibleTextView flexibleTextView10 = this.b.s;
                Wg6.b(flexibleTextView10, "binding.ftvDailyUnit");
                flexibleTextView10.setAllCaps(true);
            } else {
                this.b.s.setTextColor(W6.d(PortfolioApp.get.instance(), 2131099940));
                FlexibleTextView flexibleTextView11 = this.b.s;
                Wg6.b(flexibleTextView11, "binding.ftvDailyUnit");
                flexibleTextView11.setAllCaps(false);
            }
            ConstraintLayout constraintLayout3 = this.b.r;
            Wg6.b(constraintLayout3, "binding.container");
            constraintLayout3.setSelected(!v.g());
            FlexibleTextView flexibleTextView12 = this.b.u;
            Wg6.b(flexibleTextView12, "binding.ftvDayOfWeek");
            flexibleTextView12.setSelected(v.h());
            FlexibleTextView flexibleTextView13 = this.b.t;
            Wg6.b(flexibleTextView13, "binding.ftvDayOfMonth");
            flexibleTextView13.setSelected(v.h());
            if (v.h()) {
                this.b.r.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.g);
                this.b.t.setBackgroundColor(this.d.g);
                this.b.u.setTextColor(this.d.j);
                this.b.t.setTextColor(this.d.j);
            } else if (v.g()) {
                this.b.r.setBackgroundColor(this.d.e);
                this.b.u.setBackgroundColor(this.d.e);
                this.b.t.setBackgroundColor(this.d.e);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            } else {
                this.b.r.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.i);
                this.b.t.setBackgroundColor(this.d.i);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public Date a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public Ci(Date date, Date date2, String str, String str2) {
            Wg6.c(str, "mWeekly");
            Wg6.c(str2, "mWeeklyValue");
            this.a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ci(Date date, Date date2, String str, String str2, int i, Qg6 qg6) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        public final void e(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void f(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void g(String str) {
            Wg6.c(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void h(String str) {
            Wg6.c(str, "<set-?>");
            this.d = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di extends Bi {
        @DexIgnore
        public Date e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public /* final */ Wf5 g;
        @DexIgnore
        public /* final */ /* synthetic */ DashboardSleepsAdapter h;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Di b;

            @DexIgnore
            public Aii(Di di) {
                this.b = di;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.e != null && this.b.f != null) {
                    Zw5 zw5 = this.b.h.l;
                    Date date = this.b.e;
                    if (date != null) {
                        Date date2 = this.b.f;
                        if (date2 != null) {
                            zw5.q0(date, date2);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public Di(com.mapped.DashboardSleepsAdapter r4, com.fossil.Wf5 r5) {
            /*
                r3 = this;
                java.lang.String r0 = "binding"
                com.mapped.Wg6.c(r5, r0)
                r3.h = r4
                com.fossil.Uf5 r0 = r5.r
                if (r0 == 0) goto L_0x0029
                java.lang.String r1 = "binding.dailyItem!!"
                com.mapped.Wg6.b(r0, r1)
                android.view.View r1 = r5.n()
                java.lang.String r2 = "binding.root"
                com.mapped.Wg6.b(r1, r2)
                r3.<init>(r4, r0, r1)
                r3.g = r5
                androidx.constraintlayout.widget.ConstraintLayout r0 = r5.q
                com.mapped.DashboardSleepsAdapter$Di$Aii r1 = new com.mapped.DashboardSleepsAdapter$Di$Aii
                r1.<init>(r3)
                r0.setOnClickListener(r1)
                return
            L_0x0029:
                com.mapped.Wg6.i()
                r0 = 0
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mapped.DashboardSleepsAdapter.Di.<init>(com.mapped.DashboardSleepsAdapter, com.fossil.Wf5):void");
        }

        @DexIgnore
        @Override // com.mapped.DashboardSleepsAdapter.Bi
        public void b(SleepSummary sleepSummary) {
            Ci w = this.h.w(sleepSummary);
            this.f = w.a();
            this.e = w.b();
            FlexibleTextView flexibleTextView = this.g.s;
            Wg6.b(flexibleTextView, "binding.ftvWeekly");
            flexibleTextView.setText(w.c());
            FlexibleTextView flexibleTextView2 = this.g.t;
            Wg6.b(flexibleTextView2, "binding.ftvWeeklyValue");
            flexibleTextView2.setText(w.d());
            super.b(sleepSummary);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ DashboardSleepsAdapter b;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore
        public Ei(DashboardSleepsAdapter dashboardSleepsAdapter, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.b = dashboardSleepsAdapter;
            this.c = viewHolder;
            this.d = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            Wg6.c(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardSleepsAdapter", "onViewAttachedToWindow - mFragment.id=" + this.b.n.getId() + ", isAdded=" + this.b.n.isAdded());
            this.c.itemView.removeOnAttachStateChangeListener(this);
            Fragment Z = this.b.m.Z(this.b.n.D6());
            if (Z == null) {
                FLogger.INSTANCE.getLocal().d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                Xq0 j = this.b.m.j();
                j.b(view.getId(), this.b.n, this.b.n.D6());
                j.k();
            } else if (this.d) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment.id=" + Z.getId() + ", isAdded=" + Z.isAdded());
                Xq0 j2 = this.b.m.j();
                j2.q(Z);
                j2.k();
                Xq0 j3 = this.b.m.j();
                j3.b(view.getId(), this.b.n, this.b.n.D6());
                j3.k();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardSleepsAdapter", "onViewAttachedToWindow - oldFragment.id=" + Z.getId() + ", isAdded=" + Z.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardSleepsAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.b.n.getId() + ", isAdded2=" + this.b.n.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            Wg6.c(view, "v");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ /* synthetic */ FrameLayout a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(FrameLayout frameLayout, View view) {
            super(view);
            this.a = frameLayout;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DashboardSleepsAdapter(Ax5 ax5, PortfolioApp portfolioApp, Zw5 zw5, FragmentManager fragmentManager, BaseFragment baseFragment) {
        super(ax5);
        Wg6.c(ax5, "sleepDifference");
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(zw5, "mOnItemClick");
        Wg6.c(fragmentManager, "mFragmentManager");
        Wg6.c(baseFragment, "mFragment");
        this.k = portfolioApp;
        this.l = zw5;
        this.m = fragmentManager;
        this.n = baseFragment;
        String d2 = ThemeManager.l.a().d("primaryText");
        this.d = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = ThemeManager.l.a().d("nonBrandSurface");
        this.e = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = ThemeManager.l.a().d("nonBrandNonReachGoal");
        this.f = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = ThemeManager.l.a().d("dianaSleepTab");
        this.g = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
        String d6 = ThemeManager.l.a().d("secondaryText");
        this.h = Color.parseColor(d6 == null ? "#FFFFFF" : d6);
        String d7 = ThemeManager.l.a().d("nonBrandActivityDetailBackground");
        this.i = Color.parseColor(d7 == null ? "#FFFFFF" : d7);
        String d8 = ThemeManager.l.a().d("onDianaSleepTab");
        this.j = Color.parseColor(d8 == null ? "#FFFFFF" : d8);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i2) {
        if (getItemViewType(i2) != 0) {
            return super.getItemId(i2);
        }
        if (this.n.getId() == 0) {
            return 1010101;
        }
        return (long) this.n.getId();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        SleepSummary sleepSummary = (SleepSummary) getItem(i2);
        if (sleepSummary != null) {
            Calendar calendar = this.c;
            Wg6.b(calendar, "mCalendar");
            calendar.setTime(sleepSummary.getDate());
            Calendar calendar2 = this.c;
            Wg6.b(calendar2, "mCalendar");
            Boolean p0 = TimeUtils.p0(calendar2.getTime());
            Wg6.b(p0, "DateHelper.isToday(mCalendar.time)");
            if (p0.booleanValue() || this.c.get(7) == 7) {
                return 2;
            }
        }
        return 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        boolean z = true;
        Wg6.c(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardSleepsAdapter", "onBindViewHolder - position=" + i2 + ", viewType=" + getItemViewType(i2));
        int itemViewType = getItemViewType(i2);
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            Wg6.b(view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            Wg6.b(view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardSleepsAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            Wg6.b(view3, "holder.itemView");
            view3.setId((int) getItemId(i2));
            viewHolder.itemView.addOnAttachStateChangeListener(new Ei(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((Bi) viewHolder).b((SleepSummary) getItem(i2));
        } else if (itemViewType != 2) {
            ((Bi) viewHolder).b((SleepSummary) getItem(i2));
        } else {
            ((Di) viewHolder).b((SleepSummary) getItem(i2));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        Wg6.c(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i2 == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new Fi(frameLayout, frameLayout);
        } else if (i2 == 1) {
            Uf5 z = Uf5.z(from, viewGroup, false);
            Wg6.b(z, "ItemSleepDayBinding.infl\u2026tInflater, parent, false)");
            View n2 = z.n();
            Wg6.b(n2, "itemSleepDayBinding.root");
            return new Bi(this, z, n2);
        } else if (i2 != 2) {
            Uf5 z2 = Uf5.z(from, viewGroup, false);
            Wg6.b(z2, "ItemSleepDayBinding.infl\u2026tInflater, parent, false)");
            View n3 = z2.n();
            Wg6.b(n3, "itemSleepDayBinding.root");
            return new Bi(this, z2, n3);
        } else {
            Wf5 z3 = Wf5.z(from, viewGroup, false);
            Wg6.b(z3, "ItemSleepWeekBinding.inf\u2026tInflater, parent, false)");
            return new Di(this, z3);
        }
    }

    @DexIgnore
    public final Ai v(SleepSummary sleepSummary) {
        MFSleepDay sleepDay;
        Ai ai = new Ai(null, false, false, null, null, 0, null, null, 255, null);
        if (!(sleepSummary == null || (sleepDay = sleepSummary.getSleepDay()) == null)) {
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "calendar");
            instance.setTime(sleepDay.getDate());
            int i2 = instance.get(7);
            Boolean p0 = TimeUtils.p0(instance.getTime());
            Wg6.b(p0, "DateHelper.isToday(calendar.time)");
            if (p0.booleanValue()) {
                String c2 = Um5.c(this.k, 2131886659);
                Wg6.b(c2, "LanguageHelper.getString\u2026n_SleepToday_Text__Today)");
                ai.n(c2);
            } else {
                ai.n(Jl5.b.i(i2));
            }
            ai.l(instance.getTime());
            ai.m(String.valueOf(instance.get(5)));
            int sleepMinutes = sleepDay.getSleepMinutes();
            if (sleepMinutes > 0) {
                ai.k(sleepMinutes);
                ai.j("");
                List<MFSleepSession> sleepSessions = sleepSummary.getSleepSessions();
                if (sleepSessions != null && (!sleepSessions.isEmpty())) {
                    MFSleepSession mFSleepSession = sleepSessions.get(0);
                    int startTime = mFSleepSession.getStartTime();
                    int endTime = mFSleepSession.getEndTime();
                    Jl5 jl5 = Jl5.b;
                    String o = TimeUtils.o(((long) startTime) * 1000, mFSleepSession.getTimezoneOffset());
                    Wg6.b(o, "DateHelper.formatTimeOfD\u2026, session.timezoneOffset)");
                    String k2 = jl5.k(o);
                    Jl5 jl52 = Jl5.b;
                    String o2 = TimeUtils.o(((long) endTime) * 1000, mFSleepSession.getTimezoneOffset());
                    Wg6.b(o2, "DateHelper.formatTimeOfD\u2026, session.timezoneOffset)");
                    String k3 = jl52.k(o2);
                    Hr7 hr7 = Hr7.a;
                    String c3 = Um5.c(this.k, 2131887573);
                    Wg6.b(c3, "LanguageHelper.getString\u2026ing.sleep_start_end_time)");
                    String format = String.format(c3, Arrays.copyOf(new Object[]{k2, k3}, 2));
                    Wg6.b(format, "java.lang.String.format(format, *args)");
                    int size = sleepSessions.size();
                    if (size > 1) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(format);
                        sb.append("\n");
                        Hr7 hr72 = Hr7.a;
                        String c4 = Um5.c(this.k, 2131886649);
                        Wg6.b(c4, "LanguageHelper.getString\u2026ltiple_Label__NumberMore)");
                        String format2 = String.format(c4, Arrays.copyOf(new Object[]{Integer.valueOf(size - 1)}, 1));
                        Wg6.b(format2, "java.lang.String.format(format, *args)");
                        sb.append(format2);
                        ai.i(sb.toString());
                    } else {
                        ai.i(format);
                    }
                }
                if (sleepDay.getGoalMinutes() > 0) {
                    ai.p(sleepMinutes >= sleepDay.getGoalMinutes());
                } else {
                    ai.p(false);
                }
            } else {
                String c5 = Um5.c(this.k, 2131886657);
                Wg6.b(c5, "LanguageHelper.getString\u2026leepToday_Text__NoRecord)");
                ai.j(c5);
                ai.o(true);
            }
        }
        return ai;
    }

    @DexIgnore
    public final Ci w(SleepSummary sleepSummary) {
        String str;
        Double averageSleepOfWeek;
        Ci ci = new Ci(null, null, null, null, 15, null);
        if (sleepSummary != null) {
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "calendar");
            instance.setTime(sleepSummary.getDate());
            Boolean p0 = TimeUtils.p0(instance.getTime());
            int i2 = instance.get(5);
            int i3 = instance.get(2);
            String N = TimeUtils.N(i3);
            int i4 = instance.get(1);
            ci.e(instance.getTime());
            instance.add(5, -6);
            int i5 = instance.get(5);
            int i6 = instance.get(2);
            String N2 = TimeUtils.N(i6);
            int i7 = instance.get(1);
            ci.f(instance.getTime());
            Wg6.b(p0, "isToday");
            if (p0.booleanValue()) {
                str = Um5.c(this.k, 2131886661);
                Wg6.b(str, "LanguageHelper.getString\u2026eepToday_Title__ThisWeek)");
            } else if (i3 == i6) {
                str = N2 + ' ' + i5 + " - " + N2 + ' ' + i2;
            } else if (i7 == i4) {
                str = N2 + ' ' + i5 + " - " + N + ' ' + i2;
            } else {
                str = N2 + ' ' + i5 + ", " + i7 + " - " + N + ' ' + i2 + ", " + i4;
            }
            ci.g(str);
            MFSleepDay sleepDay = sleepSummary.getSleepDay();
            double doubleValue = (sleepDay == null || (averageSleepOfWeek = sleepDay.getAverageSleepOfWeek()) == null) ? 0.0d : averageSleepOfWeek.doubleValue();
            if (doubleValue <= 0.0d) {
                ci.h("");
            } else {
                double d2 = (double) 60;
                Hr7 hr7 = Hr7.a;
                String c2 = Um5.c(this.k, 2131886658);
                Wg6.b(c2, "LanguageHelper.getString\u2026xt__NumberHrNumberMinAvg)");
                String format = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf((int) (doubleValue / d2)), Integer.valueOf((int) (doubleValue % d2))}, 2));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                ci.h(format);
            }
        }
        return ci;
    }

    @DexIgnore
    public final void x(Cf<SleepSummary> cf) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList pagedList=");
        sb.append(cf != null ? Integer.valueOf(cf.size()) : null);
        local.d("DashboardSleepsAdapter", sb.toString());
        super.i(cf);
    }
}
