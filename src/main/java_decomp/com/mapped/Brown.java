package com.mapped;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import com.fossil.Aq0;
import com.fossil.Dl5;
import com.fossil.Er7;
import com.fossil.Fp6;
import com.fossil.G37;
import com.fossil.Gj5;
import com.fossil.Hq4;
import com.fossil.Ks7;
import com.fossil.Ls0;
import com.fossil.Mr7;
import com.fossil.Na5;
import com.fossil.Or7;
import com.fossil.Po4;
import com.fossil.Qv5;
import com.fossil.Rh5;
import com.fossil.S37;
import com.fossil.Tq7;
import com.fossil.Ts0;
import com.fossil.U37;
import com.fossil.Uh5;
import com.fossil.Vl5;
import com.fossil.Wt7;
import com.mapped.AlertDialogFragment;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel;
import com.portfolio.platform.view.CustomEditGoalView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Brown extends Qv5 implements AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ /* synthetic */ Ks7[] t;
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ Ai v; // = new Ai(null);
    @DexIgnore
    public Po4 h;
    @DexIgnore
    public ProfileGoalEditViewModel i;
    @DexIgnore
    public G37<Na5> j;
    @DexIgnore
    public /* final */ InputMethodManager k;
    @DexIgnore
    public /* final */ Or7 l;
    @DexIgnore
    public Rh5 m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Brown.u;
        }

        @DexIgnore
        public final Brown b() {
            return new Brown();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<U37<? extends ProfileGoalEditViewModel.Ei>> {
        @DexIgnore
        public /* final */ /* synthetic */ Brown a;

        @DexIgnore
        public Bi(Brown brown) {
            this.a = brown;
        }

        @DexIgnore
        public final void a(U37<? extends ProfileGoalEditViewModel.Ei> u37) {
            ProfileGoalEditViewModel.Ei ei = (ProfileGoalEditViewModel.Ei) u37.a();
            if (ei == null) {
                return;
            }
            if (ei instanceof ProfileGoalEditViewModel.Ei.Aii) {
                Brown brown = this.a;
                brown.Y6(brown.m);
            } else if (ei instanceof ProfileGoalEditViewModel.Ei.Cii) {
                this.a.e7();
            } else if (ei instanceof ProfileGoalEditViewModel.Ei.Dii) {
                ProfileGoalEditViewModel.Ei.Dii dii = (ProfileGoalEditViewModel.Ei.Dii) ei;
                this.a.l5(dii.a(), dii.b());
            } else if (ei instanceof ProfileGoalEditViewModel.Ei.Bii) {
                this.a.W6();
            } else if (ei instanceof ProfileGoalEditViewModel.Ei.Eii) {
                Brown brown2 = this.a;
                Object[] array = ((ProfileGoalEditViewModel.Ei.Eii) ei).a().toArray(new Uh5[0]);
                if (array != null) {
                    Uh5[] uh5Arr = (Uh5[]) array;
                    brown2.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(U37<? extends ProfileGoalEditViewModel.Ei> u37) {
            a(u37);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<Hq4.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ Brown a;

        @DexIgnore
        public Ci(Brown brown) {
            this.a = brown;
        }

        @DexIgnore
        public final void a(Hq4.Bi bi) {
            if (bi != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = Brown.v.a();
                local.d(a2, "loadingState start " + bi.a() + " stop " + bi.b());
                if (bi.a()) {
                    this.a.b();
                }
                if (bi.b()) {
                    this.a.a();
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Bi bi) {
            a(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Na5 b;
        @DexIgnore
        public /* final */ /* synthetic */ Brown c;

        @DexIgnore
        public Di(Na5 na5, Brown brown) {
            this.b = na5;
            this.c = brown;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            if (editable != null) {
                if (editable.length() > 0) {
                    int g = Dl5.g(editable.toString());
                    if (g <= 9 && editable.length() > 1) {
                        this.b.w.setText(String.valueOf(g));
                    }
                    this.c.V6(g);
                    return;
                }
                this.b.w.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (charSequence != null) {
                if (charSequence.length() > 0) {
                    this.b.w.removeTextChangedListener(this);
                    String obj = charSequence.toString();
                    if (Wt7.v(obj, ",", false, 2, null)) {
                        obj = new Mj6(",").replace(obj, "");
                    }
                    this.b.w.setText(Dl5.f(Integer.parseInt(obj)));
                    FlexibleEditText flexibleEditText = this.b.w;
                    Wg6.b(flexibleEditText, "binding.fetGoalsValue");
                    Editable text = flexibleEditText.getText();
                    if (text != null) {
                        flexibleEditText.setSelection(text.length());
                        this.b.w.addTextChangedListener(this);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                this.b.w.setText(String.valueOf(0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Na5 b;
        @DexIgnore
        public /* final */ /* synthetic */ Brown c;

        @DexIgnore
        public Ei(Na5 na5, Brown brown) {
            this.b = na5;
            this.c = brown;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            int i;
            int i2 = 16;
            if (editable != null) {
                if (editable.length() > 0) {
                    int parseInt = Integer.parseInt(editable.toString());
                    if (parseInt <= 9 && editable.length() > 1) {
                        this.b.x.setText(String.valueOf(parseInt));
                    }
                    if (parseInt > 16) {
                        this.b.x.setText(String.valueOf(16));
                        S37 s37 = S37.c;
                        FragmentManager childFragmentManager = this.c.getChildFragmentManager();
                        Wg6.b(childFragmentManager, "childFragmentManager");
                        s37.s0(childFragmentManager, this.c.m, 16);
                    } else {
                        i2 = parseInt;
                    }
                    Brown brown = this.c;
                    FlexibleEditText flexibleEditText = this.b.y;
                    Wg6.b(flexibleEditText, "binding.fetSleepMinuteValue");
                    Editable text = flexibleEditText.getText();
                    if (text != null) {
                        Wg6.b(text, "binding.fetSleepMinuteValue.text!!");
                        if (text.length() > 0) {
                            FlexibleEditText flexibleEditText2 = this.b.y;
                            Wg6.b(flexibleEditText2, "binding.fetSleepMinuteValue");
                            i = Integer.parseInt(String.valueOf(flexibleEditText2.getText()));
                        } else {
                            i = 0;
                        }
                        Brown.L6(this.c).P(brown.i7(i, i2), this.c.m);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                this.b.x.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Na5 b;
        @DexIgnore
        public /* final */ /* synthetic */ Brown c;

        @DexIgnore
        public Fi(Na5 na5, Brown brown) {
            this.b = na5;
            this.c = brown;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            int i;
            int i2 = 59;
            if (editable != null) {
                if (editable.length() > 0) {
                    int parseInt = Integer.parseInt(editable.toString());
                    if (parseInt <= 9 && editable.length() > 1) {
                        this.b.y.setText(String.valueOf(parseInt));
                    }
                    if (this.c.m == Rh5.TOTAL_SLEEP) {
                        if (parseInt > 59) {
                            this.b.y.setText(String.valueOf(59));
                        } else {
                            i2 = parseInt;
                        }
                        Brown brown = this.c;
                        FlexibleEditText flexibleEditText = this.b.y;
                        Wg6.b(flexibleEditText, "binding.fetSleepMinuteValue");
                        Editable text = flexibleEditText.getText();
                        if (text != null) {
                            Wg6.b(text, "binding.fetSleepMinuteValue.text!!");
                            if (text.length() > 0) {
                                FlexibleEditText flexibleEditText2 = this.b.x;
                                Wg6.b(flexibleEditText2, "binding.fetSleepHourValue");
                                i = Integer.parseInt(String.valueOf(flexibleEditText2.getText()));
                            } else {
                                i = 0;
                            }
                            parseInt = brown.i7(i2, i);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                    Brown.L6(this.c).P(parseInt, this.c.m);
                    return;
                }
                this.b.y.setText(String.valueOf(0));
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Na5 b;
        @DexIgnore
        public /* final */ /* synthetic */ Brown c;

        @DexIgnore
        public Gi(Na5 na5, Brown brown) {
            this.b = na5;
            this.c = brown;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (this.c.m == Rh5.TOTAL_SLEEP) {
                Brown brown = this.c;
                FlexibleEditText flexibleEditText = this.b.y;
                Wg6.b(flexibleEditText, "binding.fetSleepMinuteValue");
                brown.X6(z, flexibleEditText.isFocused());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Na5 b;
        @DexIgnore
        public /* final */ /* synthetic */ Brown c;

        @DexIgnore
        public Hi(Na5 na5, Brown brown) {
            this.b = na5;
            this.c = brown;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            Brown brown = this.c;
            FlexibleEditText flexibleEditText = this.b.x;
            Wg6.b(flexibleEditText, "binding.fetSleepHourValue");
            brown.X6(flexibleEditText.isFocused(), z);
            if (z) {
                FlexibleEditText flexibleEditText2 = this.b.x;
                Wg6.b(flexibleEditText2, "binding.fetSleepHourValue");
                if (Integer.parseInt(String.valueOf(flexibleEditText2.getText())) == 16) {
                    S37 s37 = S37.c;
                    FragmentManager childFragmentManager = this.c.getChildFragmentManager();
                    Wg6.b(childFragmentManager, "childFragmentManager");
                    s37.s0(childFragmentManager, Rh5.TOTAL_SLEEP, 16);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii<T> implements Ls0<ProfileGoalEditViewModel.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ Na5 a;

        @DexIgnore
        public Ii(Na5 na5) {
            this.a = na5;
        }

        @DexIgnore
        public final void a(ProfileGoalEditViewModel.Ci ci) {
            this.a.u.setValue(ci.i());
            this.a.v.setValue(ci.d());
            this.a.s.setValue(ci.f());
            this.a.t.setValue(ci.h());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(ProfileGoalEditViewModel.Ci ci) {
            a(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji<T> implements Ls0<ProfileGoalEditViewModel.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ Na5 a;

        @DexIgnore
        public Ji(Na5 na5) {
            this.a = na5;
        }

        @DexIgnore
        public final void a(ProfileGoalEditViewModel.Ci ci) {
            FLogger.INSTANCE.getLocal().d("Brown", ci.toString());
            this.a.u.setValue(ci.i());
            this.a.v.setValue(ci.f());
            this.a.s.setValue(ci.h());
            this.a.t.setValue(ci.g());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(ProfileGoalEditViewModel.Ci ci) {
            a(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Brown b;

        @DexIgnore
        public Ki(Brown brown) {
            this.b = brown;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Brown b;

        @DexIgnore
        public Li(Brown brown) {
            this.b = brown;
        }

        @DexIgnore
        public final void onClick(View view) {
            Brown brown = this.b;
            if (view != null) {
                brown.Y6(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Brown b;

        @DexIgnore
        public Mi(Brown brown) {
            this.b = brown;
        }

        @DexIgnore
        public final void onClick(View view) {
            Brown brown = this.b;
            if (view != null) {
                brown.Y6(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Brown b;

        @DexIgnore
        public Ni(Brown brown) {
            this.b = brown;
        }

        @DexIgnore
        public final void onClick(View view) {
            Brown brown = this.b;
            if (view != null) {
                brown.Y6(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Brown b;

        @DexIgnore
        public Oi(Brown brown) {
            this.b = brown;
        }

        @DexIgnore
        public final void onClick(View view) {
            Brown brown = this.b;
            if (view != null) {
                brown.Y6(((CustomEditGoalView) view).getMGoalType());
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.view.CustomEditGoalView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Na5 b;

        @DexIgnore
        public Pi(Na5 na5) {
            this.b = na5;
        }

        @DexIgnore
        public final void run() {
            this.b.J.fullScroll(130);
        }
    }

    /*
    static {
        Tq7 tq7 = new Tq7(Er7.b(Brown.class), "mIsDiana", "getMIsDiana()Z");
        Er7.e(tq7);
        t = new Ks7[]{tq7};
        String simpleName = Brown.class.getSimpleName();
        Wg6.b(simpleName, "ProfileGoalEditFragment::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public Brown() {
        Object systemService = PortfolioApp.get.instance().getSystemService("input_method");
        if (systemService != null) {
            this.k = (InputMethodManager) systemService;
            this.l = Mr7.a.a();
            this.m = Rh5.TOTAL_STEPS;
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }

    @DexIgnore
    public static final /* synthetic */ ProfileGoalEditViewModel L6(Brown brown) {
        ProfileGoalEditViewModel profileGoalEditViewModel = brown.i;
        if (profileGoalEditViewModel != null) {
            return profileGoalEditViewModel;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        if (getActivity() == null) {
            return true;
        }
        FragmentActivity requireActivity = requireActivity();
        Wg6.b(requireActivity, "requireActivity()");
        if (requireActivity.isFinishing()) {
            return true;
        }
        FragmentActivity requireActivity2 = requireActivity();
        Wg6.b(requireActivity2, "requireActivity()");
        if (requireActivity2.isDestroyed()) {
            return true;
        }
        ProfileGoalEditViewModel profileGoalEditViewModel = this.i;
        if (profileGoalEditViewModel != null) {
            profileGoalEditViewModel.L();
            return true;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        View view = null;
        Wg6.c(str, "tag");
        if (!(str.length() == 0) && getActivity() != null) {
            int hashCode = str.hashCode();
            if (hashCode != -1375614559) {
                if (hashCode != 927377074) {
                    if (hashCode == 1008390942 && str.equals("NO_INTERNET_CONNECTION")) {
                        if (i2 == 2131363373) {
                            FragmentActivity activity2 = getActivity();
                            if (activity2 != null) {
                                ((BaseActivity) activity2).y();
                                return;
                            }
                            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
                        } else if (i2 == 2131363291) {
                            W6();
                        }
                    }
                } else if (str.equals("GOAL_EXCEED_VALUE") && i2 == 2131363373) {
                    FragmentActivity requireActivity = requireActivity();
                    Wg6.b(requireActivity, "requireActivity()");
                    View currentFocus = requireActivity.getCurrentFocus();
                    if (currentFocus instanceof EditText) {
                        view = currentFocus;
                    }
                    h7((EditText) view);
                }
            } else if (!str.equals("UNSAVED_CHANGE")) {
            } else {
                if (i2 == 2131363373) {
                    ProfileGoalEditViewModel profileGoalEditViewModel = this.i;
                    if (profileGoalEditViewModel != null) {
                        profileGoalEditViewModel.M();
                    } else {
                        Wg6.n("mViewModel");
                        throw null;
                    }
                } else if (i2 == 2131363291 && (activity = getActivity()) != null) {
                    activity.finish();
                }
            }
        }
    }

    @DexIgnore
    public final boolean U6() {
        return ((Boolean) this.l.b(this, t[0])).booleanValue();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004f, code lost:
        if (r9 <= 999) goto L_0x0028;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x006d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void V6(int r9) {
        /*
            r8 = this;
            r1 = 999(0x3e7, float:1.4E-42)
            r2 = 480(0x1e0, float:6.73E-43)
            r3 = 1
            r4 = 0
            r7 = 0
            com.fossil.G37<com.fossil.Na5> r0 = r8.j
            if (r0 == 0) goto L_0x007d
            java.lang.Object r0 = r0.a()
            com.fossil.Na5 r0 = (com.fossil.Na5) r0
            if (r0 == 0) goto L_0x004e
            com.fossil.Rh5 r5 = r8.m
            int[] r6 = com.fossil.Fp6.b
            int r5 = r5.ordinal()
            r5 = r6[r5]
            if (r5 == r3) goto L_0x005e
            r6 = 2
            if (r5 == r6) goto L_0x0057
            r6 = 3
            if (r5 == r6) goto L_0x0053
            r2 = 4
            if (r5 == r2) goto L_0x004f
        L_0x0028:
            r2 = r4
            r1 = r4
        L_0x002a:
            if (r2 == 0) goto L_0x006d
            com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel r2 = r8.i
            if (r2 == 0) goto L_0x0067
            com.fossil.Rh5 r3 = r8.m
            r2.P(r1, r3)
            com.portfolio.platform.view.FlexibleEditText r0 = r0.w
            java.lang.String r2 = com.fossil.Dl5.f(r1)
            r0.setText(r2)
            com.fossil.S37 r0 = com.fossil.S37.c
            androidx.fragment.app.FragmentManager r2 = r8.getChildFragmentManager()
            java.lang.String r3 = "childFragmentManager"
            com.mapped.Wg6.b(r2, r3)
            com.fossil.Rh5 r3 = r8.m
            r0.s0(r2, r3, r1)
        L_0x004e:
            return
        L_0x004f:
            if (r9 <= r1) goto L_0x0028
        L_0x0051:
            r2 = r3
            goto L_0x002a
        L_0x0053:
            if (r9 <= r2) goto L_0x0028
            r1 = r2
            goto L_0x0051
        L_0x0057:
            r1 = 4800(0x12c0, float:6.726E-42)
            if (r9 <= r1) goto L_0x0028
            r1 = 4800(0x12c0, float:6.726E-42)
            goto L_0x0051
        L_0x005e:
            r1 = 50000(0xc350, float:7.0065E-41)
            if (r9 <= r1) goto L_0x0028
            r1 = 50000(0xc350, float:7.0065E-41)
            goto L_0x0051
        L_0x0067:
            java.lang.String r0 = "mViewModel"
            com.mapped.Wg6.n(r0)
            throw r7
        L_0x006d:
            com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditViewModel r0 = r8.i
            if (r0 == 0) goto L_0x0077
            com.fossil.Rh5 r1 = r8.m
            r0.P(r9, r1)
            goto L_0x004e
        L_0x0077:
            java.lang.String r0 = "mViewModel"
            com.mapped.Wg6.n(r0)
            throw r7
        L_0x007d:
            java.lang.String r0 = "mBinding"
            com.mapped.Wg6.n(r0)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.Brown.V6(int):void");
    }

    @DexIgnore
    public final void W6() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    public final void X6(boolean z, boolean z2) {
        G37<Na5> g37 = this.j;
        if (g37 != null) {
            Na5 a2 = g37.a();
            if (a2 != null) {
                String d = ThemeManager.l.a().d("dianaSleepTab");
                String d2 = ThemeManager.l.a().d("nonBrandCoolGray");
                int parseColor = Color.parseColor(d);
                int parseColor2 = Color.parseColor(d2);
                if (z) {
                    a2.x.setTextColor(parseColor);
                    a2.y.setTextColor(parseColor2);
                } else if (z2) {
                    a2.x.setTextColor(parseColor2);
                    a2.y.setTextColor(parseColor);
                } else {
                    a2.x.setTextColor(parseColor);
                    a2.y.setTextColor(parseColor);
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void Y6(Rh5 rh5) {
        boolean z = true;
        G37<Na5> g37 = this.j;
        if (g37 != null) {
            Na5 a2 = g37.a();
            if (a2 != null) {
                if (rh5 != null) {
                    this.m = rh5;
                }
                CustomEditGoalView customEditGoalView = a2.u;
                Wg6.b(customEditGoalView, "cegvTopLeft");
                customEditGoalView.setSelected(a2.u.getMGoalType() == rh5);
                CustomEditGoalView customEditGoalView2 = a2.v;
                Wg6.b(customEditGoalView2, "cegvTopRight");
                customEditGoalView2.setSelected(a2.v.getMGoalType() == rh5);
                CustomEditGoalView customEditGoalView3 = a2.s;
                Wg6.b(customEditGoalView3, "cegvBottomLeft");
                customEditGoalView3.setSelected(a2.s.getMGoalType() == rh5);
                CustomEditGoalView customEditGoalView4 = a2.t;
                Wg6.b(customEditGoalView4, "cegvBottomRight");
                if (a2.t.getMGoalType() != rh5) {
                    z = false;
                }
                customEditGoalView4.setSelected(z);
                f7(rh5);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Z6() {
        ProfileGoalEditViewModel profileGoalEditViewModel = this.i;
        if (profileGoalEditViewModel != null) {
            profileGoalEditViewModel.G().h(getViewLifecycleOwner(), new Bi(this));
            ProfileGoalEditViewModel profileGoalEditViewModel2 = this.i;
            if (profileGoalEditViewModel2 != null) {
                profileGoalEditViewModel2.j().h(getViewLifecycleOwner(), new Ci(this));
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void a7(boolean z) {
        this.l.a(this, t[0], Boolean.valueOf(z));
    }

    @DexIgnore
    public final void b7() {
        G37<Na5> g37 = this.j;
        if (g37 != null) {
            Na5 a2 = g37.a();
            if (a2 != null) {
                a2.w.addTextChangedListener(new Di(a2, this));
                a2.x.addTextChangedListener(new Ei(a2, this));
                a2.y.addTextChangedListener(new Fi(a2, this));
                FlexibleEditText flexibleEditText = a2.x;
                Wg6.b(flexibleEditText, "binding.fetSleepHourValue");
                flexibleEditText.setOnFocusChangeListener(new Gi(a2, this));
                FlexibleEditText flexibleEditText2 = a2.y;
                Wg6.b(flexibleEditText2, "binding.fetSleepMinuteValue");
                flexibleEditText2.setOnFocusChangeListener(new Hi(a2, this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void c7() {
        G37<Na5> g37 = this.j;
        if (g37 != null) {
            Na5 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                String d = ThemeManager.l.a().d("nonBrandSurface");
                if (d != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(d));
                }
                if (U6()) {
                    a2.u.setType(Rh5.TOTAL_STEPS);
                    a2.v.setType(Rh5.ACTIVE_TIME);
                    a2.s.setType(Rh5.CALORIES);
                    a2.t.setType(Rh5.TOTAL_SLEEP);
                    ProfileGoalEditViewModel profileGoalEditViewModel = this.i;
                    if (profileGoalEditViewModel != null) {
                        profileGoalEditViewModel.H().h(getViewLifecycleOwner(), new Ii(a2));
                    } else {
                        Wg6.n("mViewModel");
                        throw null;
                    }
                } else {
                    a2.u.setType(Rh5.TOTAL_STEPS);
                    a2.v.setType(Rh5.CALORIES);
                    a2.s.setType(Rh5.TOTAL_SLEEP);
                    a2.t.setType(Rh5.GOAL_TRACKING);
                    ProfileGoalEditViewModel profileGoalEditViewModel2 = this.i;
                    if (profileGoalEditViewModel2 != null) {
                        profileGoalEditViewModel2.H().h(getViewLifecycleOwner(), new Ji(a2));
                    } else {
                        Wg6.n("mViewModel");
                        throw null;
                    }
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void d7() {
        G37<Na5> g37 = this.j;
        if (g37 != null) {
            Na5 a2 = g37.a();
            if (a2 != null) {
                a2.F.setOnClickListener(new Ki(this));
                a2.u.setOnClickListener(new Li(this));
                a2.v.setOnClickListener(new Mi(this));
                a2.s.setOnClickListener(new Ni(this));
                a2.t.setOnClickListener(new Oi(this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void e7() {
        if (isActive()) {
            a();
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.y0(childFragmentManager);
        }
    }

    @DexIgnore
    public final void f7(Rh5 rh5) {
        G37<Na5> g37 = this.j;
        if (g37 != null) {
            Na5 a2 = g37.a();
            if (a2 != null && rh5 != null) {
                int i2 = Fp6.a[rh5.ordinal()];
                if (i2 == 1) {
                    FlexibleTextView flexibleTextView = a2.A;
                    Wg6.b(flexibleTextView, "ftvGoalTitle");
                    flexibleTextView.setText(getString(2131886630));
                    FlexibleTextView flexibleTextView2 = a2.z;
                    Wg6.b(flexibleTextView2, "ftvDesc");
                    flexibleTextView2.setText(getString(2131887112));
                    String d = ThemeManager.l.a().d("dianaActiveCaloriesTab");
                    if (d != null) {
                        a2.w.setTextColor(Color.parseColor(d));
                    }
                    FlexibleEditText flexibleEditText = a2.w;
                    Wg6.b(flexibleEditText, "fetGoalsValue");
                    flexibleEditText.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                    a2.w.setText(String.valueOf((U6() ? a2.s : a2.v).getValue()));
                    FlexibleTextView flexibleTextView3 = a2.B;
                    Wg6.b(flexibleTextView3, "ftvGoalsUnit");
                    flexibleTextView3.setVisibility(8);
                    g7(false);
                } else if (i2 == 2) {
                    FlexibleTextView flexibleTextView4 = a2.A;
                    Wg6.b(flexibleTextView4, "ftvGoalTitle");
                    flexibleTextView4.setText(getString(2131886638));
                    FlexibleTextView flexibleTextView5 = a2.z;
                    Wg6.b(flexibleTextView5, "ftvDesc");
                    flexibleTextView5.setText(getString(2131887114));
                    String d2 = ThemeManager.l.a().d("dianaActiveMinutesTab");
                    if (d2 != null) {
                        a2.w.setTextColor(Color.parseColor(d2));
                    }
                    FlexibleEditText flexibleEditText2 = a2.w;
                    Wg6.b(flexibleEditText2, "fetGoalsValue");
                    flexibleEditText2.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(5)});
                    a2.w.setText(String.valueOf(a2.v.getValue()));
                    FlexibleTextView flexibleTextView6 = a2.B;
                    Wg6.b(flexibleTextView6, "ftvGoalsUnit");
                    flexibleTextView6.setVisibility(8);
                    g7(false);
                } else if (i2 == 3) {
                    FlexibleTextView flexibleTextView7 = a2.A;
                    Wg6.b(flexibleTextView7, "ftvGoalTitle");
                    flexibleTextView7.setText(getString(2131887124));
                    FlexibleTextView flexibleTextView8 = a2.z;
                    Wg6.b(flexibleTextView8, "ftvDesc");
                    flexibleTextView8.setText(getString(2131887125));
                    String d3 = ThemeManager.l.a().d("hybridStepsTab");
                    if (d3 != null) {
                        a2.w.setTextColor(Color.parseColor(d3));
                    }
                    FlexibleEditText flexibleEditText3 = a2.w;
                    Wg6.b(flexibleEditText3, "fetGoalsValue");
                    flexibleEditText3.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                    a2.w.setText(String.valueOf(a2.u.getValue()));
                    FlexibleTextView flexibleTextView9 = a2.B;
                    Wg6.b(flexibleTextView9, "ftvGoalsUnit");
                    flexibleTextView9.setVisibility(8);
                    g7(false);
                } else if (i2 == 4) {
                    FlexibleTextView flexibleTextView10 = a2.A;
                    Wg6.b(flexibleTextView10, "ftvGoalTitle");
                    flexibleTextView10.setText(getString(2131886767));
                    FlexibleTextView flexibleTextView11 = a2.z;
                    Wg6.b(flexibleTextView11, "ftvDesc");
                    flexibleTextView11.setText(getString(2131887118));
                    String d4 = ThemeManager.l.a().d("dianaSleepTab");
                    if (d4 != null) {
                        a2.w.setTextColor(Color.parseColor(d4));
                    }
                    g7(true);
                } else if (i2 == 5) {
                    FlexibleTextView flexibleTextView12 = a2.A;
                    Wg6.b(flexibleTextView12, "ftvGoalTitle");
                    flexibleTextView12.setText(getString(2131886741));
                    FlexibleTextView flexibleTextView13 = a2.z;
                    Wg6.b(flexibleTextView13, "ftvDesc");
                    flexibleTextView13.setText(getString(2131887130));
                    String d5 = ThemeManager.l.a().d("hybridGoalTrackingTab");
                    if (d5 != null) {
                        a2.w.setTextColor(Color.parseColor(d5));
                    }
                    FlexibleEditText flexibleEditText4 = a2.w;
                    Wg6.b(flexibleEditText4, "fetGoalsValue");
                    flexibleEditText4.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(7)});
                    a2.w.setText(String.valueOf(a2.t.getValue()));
                    FlexibleTextView flexibleTextView14 = a2.B;
                    Wg6.b(flexibleTextView14, "ftvGoalsUnit");
                    flexibleTextView14.setVisibility(0);
                    FlexibleTextView flexibleTextView15 = a2.B;
                    Wg6.b(flexibleTextView15, "ftvGoalsUnit");
                    flexibleTextView15.setText(getString(2131887129));
                    g7(false);
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void g7(boolean z) {
        FLogger.INSTANCE.getLocal().d(u, "showSleepGoalEdit");
        G37<Na5> g37 = this.j;
        if (g37 != null) {
            Na5 a2 = g37.a();
            if (a2 != null) {
                PortfolioApp instance = PortfolioApp.get.instance();
                if (z) {
                    LinearLayout linearLayout = a2.H;
                    Wg6.b(linearLayout, "llSleepGoalValue");
                    linearLayout.setVisibility(0);
                    LinearLayout linearLayout2 = a2.G;
                    Wg6.b(linearLayout2, "llGoalsValue");
                    linearLayout2.setVisibility(8);
                    a2.x.setTextColor(Color.parseColor(ThemeManager.l.a().d("dianaSleepTab")));
                    a2.y.setTextColor(W6.d(instance, 2131099764));
                    FlexibleEditText flexibleEditText = a2.x;
                    Wg6.b(flexibleEditText, "fetSleepHourValue");
                    flexibleEditText.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(2)});
                    FlexibleEditText flexibleEditText2 = a2.y;
                    Wg6.b(flexibleEditText2, "fetSleepMinuteValue");
                    flexibleEditText2.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(2)});
                    FlexibleTextView flexibleTextView = a2.C;
                    Wg6.b(flexibleTextView, "ftvSleepHourUnit");
                    flexibleTextView.setText(getString(2131887116));
                    FlexibleTextView flexibleTextView2 = a2.D;
                    Wg6.b(flexibleTextView2, "ftvSleepMinuteUnit");
                    flexibleTextView2.setText(getString(2131887117));
                    int mValue = (U6() ? a2.t : a2.s).getMValue() / 60;
                    int mValue2 = (U6() ? a2.t : a2.s).getMValue();
                    a2.x.setText(String.valueOf(mValue));
                    a2.y.setText(String.valueOf(mValue2 % 60));
                    h7(a2.x);
                } else {
                    LinearLayout linearLayout3 = a2.H;
                    Wg6.b(linearLayout3, "llSleepGoalValue");
                    linearLayout3.setVisibility(8);
                    LinearLayout linearLayout4 = a2.G;
                    Wg6.b(linearLayout4, "llGoalsValue");
                    linearLayout4.setVisibility(0);
                    h7(a2.w);
                }
                a2.J.post(new Pi(a2));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void h7(EditText editText) {
        if (editText != null) {
            Gj5.b(editText, this.k);
        }
    }

    @DexIgnore
    public final int i7(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "updateSleepGoal minute: " + i2 + " hour: " + i3);
        G37<Na5> g37 = this.j;
        if (g37 != null) {
            Na5 a2 = g37.a();
            if (a2 == null) {
                return 0;
            }
            int i4 = (i3 * 60) + i2;
            if (i4 <= 960) {
                return i4;
            }
            a2.y.setText("0");
            a2.x.setText(String.valueOf(16));
            return 960;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void l5(int i2, String str) {
        if (isActive()) {
            a();
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Na5 na5 = (Na5) Aq0.f(layoutInflater, 2131558611, viewGroup, false, A6());
        PortfolioApp.get.instance().getIface().F1().a(this);
        Po4 po4 = this.h;
        if (po4 != null) {
            Ts0 a2 = new ViewModelProvider(this, po4).a(ProfileGoalEditViewModel.class);
            Wg6.b(a2, "ViewModelProvider(this, \u2026ditViewModel::class.java)");
            this.i = (ProfileGoalEditViewModel) a2;
            this.j = new G37<>(this, na5);
            ProfileGoalEditViewModel profileGoalEditViewModel = this.i;
            if (profileGoalEditViewModel != null) {
                a7(profileGoalEditViewModel.J());
                c7();
                d7();
                b7();
                Z6();
                LinearLayout linearLayout = na5.G;
                Wg6.b(linearLayout, "binding.llGoalsValue");
                linearLayout.setVisibility(4);
                LinearLayout linearLayout2 = na5.H;
                Wg6.b(linearLayout2, "binding.llSleepGoalValue");
                linearLayout2.setVisibility(4);
                E6("set_goal_view");
                Wg6.b(na5, "binding");
                return na5.n();
            }
            Wg6.n("mViewModel");
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        FLogger.INSTANCE.getLocal().d(u, "onResume");
        super.onResume();
        Vl5 C6 = C6();
        if (C6 != null) {
            C6.i();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        FLogger.INSTANCE.getLocal().d(u, "onStop");
        super.onStop();
        Vl5 C6 = C6();
        if (C6 != null) {
            C6.c("");
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
