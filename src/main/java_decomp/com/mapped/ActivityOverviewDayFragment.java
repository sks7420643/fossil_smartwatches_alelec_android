package com.mapped;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Af6;
import com.fossil.Aq0;
import com.fossil.Bf6;
import com.fossil.Cf6;
import com.fossil.Dl5;
import com.fossil.E25;
import com.fossil.Fd5;
import com.fossil.G37;
import com.fossil.Hr7;
import com.fossil.Jl5;
import com.fossil.Mi5;
import com.fossil.Mv5;
import com.fossil.Oi5;
import com.fossil.Um5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityOverviewDayFragment extends BaseFragment implements Bf6 {
    @DexIgnore
    public G37<E25> g;
    @DexIgnore
    public Af6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements View.OnClickListener {
        @DexIgnore
        public static /* final */ Ai b; // = new Ai();

        @DexIgnore
        public final void onClick(View view) {
            ActivityDetailActivity.a aVar = ActivityDetailActivity.C;
            Date date = new Date();
            Wg6.b(view, "it");
            Context context = view.getContext();
            Wg6.b(context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "ActivityOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Bf6
    public void G(Mv5 mv5, ArrayList<String> arrayList) {
        E25 a2;
        OverviewDayChart overviewDayChart;
        Wg6.c(mv5, "baseModel");
        Wg6.c(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityOverviewDayFragment", "showDayDetails - baseModel=" + mv5);
        G37<E25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.q) != null) {
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(Mv5.a.b(cVar.d()));
            if (!arrayList.isEmpty()) {
                BarChart.H(overviewDayChart, arrayList, false, 2, null);
            } else {
                BarChart.H(overviewDayChart, Jl5.b.f(), false, 2, null);
            }
            overviewDayChart.r(mv5);
        }
    }

    @DexIgnore
    public final void K6(Fd5 fd5, WorkoutSession workoutSession) {
        int i2;
        String str;
        if (fd5 != null) {
            View n = fd5.n();
            Wg6.b(n, "binding.root");
            Context context = n.getContext();
            Oi5.Ai ai = Oi5.Companion;
            Lc6<Integer, Integer> a2 = ai.a(ai.d(workoutSession.getEditedType(), workoutSession.getEditedMode()));
            String c = Um5.c(context, a2.getSecond().intValue());
            fd5.t.setImageResource(a2.getFirst().intValue());
            DeviceHelper.Ai ai2 = DeviceHelper.o;
            Af6 af6 = this.h;
            String d = ai2.w(af6 != null ? af6.n() : null) ? ThemeManager.l.a().d("dianaStepsTab") : ThemeManager.l.a().d("hybridStepsTab");
            if (d != null) {
                fd5.t.setColorFilter(Color.parseColor(d));
            }
            FlexibleTextView flexibleTextView = fd5.r;
            Wg6.b(flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(c);
            Mi5 editedType = workoutSession.getEditedType();
            if (editedType == null) {
                editedType = workoutSession.getWorkoutType();
            }
            FlexibleTextView flexibleTextView2 = fd5.s;
            Wg6.b(flexibleTextView2, "it.ftvWorkoutValue");
            if (editedType != null && ((i2 = Cf6.a[editedType.ordinal()]) == 1 || i2 == 2)) {
                str = "";
            } else {
                Hr7 hr7 = Hr7.a;
                String c2 = Um5.c(context, 2131886682);
                Wg6.b(c2, "LanguageHelper.getString\u2026sToday_Text__NumberSteps)");
                Integer totalSteps = workoutSession.getTotalSteps();
                str = String.format(c2, Arrays.copyOf(new Object[]{Dl5.c(totalSteps != null ? (float) totalSteps.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1)}, 1));
                Wg6.b(str, "java.lang.String.format(format, *args)");
            }
            flexibleTextView2.setText(str);
            FlexibleTextView flexibleTextView3 = fd5.q;
            Wg6.b(flexibleTextView3, "it.ftvWorkoutTime");
            flexibleTextView3.setText(TimeUtils.o(workoutSession.getEditedStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
        }
    }

    @DexIgnore
    public final void L6() {
        E25 a2;
        OverviewDayChart overviewDayChart;
        G37<E25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.q) != null) {
            DeviceHelper.Ai ai = DeviceHelper.o;
            Af6 af6 = this.h;
            if (ai.w(af6 != null ? af6.n() : null)) {
                overviewDayChart.D("dianaStepsTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.D("hybridStepsTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Af6 af6) {
        M6(af6);
    }

    @DexIgnore
    public void M6(Af6 af6) {
        Wg6.c(af6, "presenter");
        this.h = af6;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        E25 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onCreateView");
        E25 e25 = (E25) Aq0.f(layoutInflater, 2131558498, viewGroup, false, A6());
        e25.r.setOnClickListener(Ai.b);
        this.g = new G37<>(this, e25);
        L6();
        G37<E25> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onResume");
        L6();
        Af6 af6 = this.h;
        if (af6 != null) {
            af6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onStop");
        Af6 af6 = this.h;
        if (af6 != null) {
            af6.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.Bf6
    public void v(boolean z, List<WorkoutSession> list) {
        E25 a2;
        View n;
        View n2;
        Wg6.c(list, "workoutSessions");
        G37<E25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null) {
            if (z) {
                LinearLayout linearLayout = a2.u;
                Wg6.b(linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                K6(a2.s, list.get(0));
                if (size == 1) {
                    Fd5 fd5 = a2.t;
                    if (fd5 != null && (n2 = fd5.n()) != null) {
                        n2.setVisibility(8);
                        return;
                    }
                    return;
                }
                Fd5 fd52 = a2.t;
                if (!(fd52 == null || (n = fd52.n()) == null)) {
                    n.setVisibility(0);
                }
                K6(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            Wg6.b(linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
