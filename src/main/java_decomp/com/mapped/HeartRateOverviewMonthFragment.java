package com.mapped;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.Aq0;
import com.fossil.F75;
import com.fossil.G37;
import com.fossil.Yi6;
import com.fossil.Zi6;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateOverviewMonthFragment extends BaseFragment implements Zi6 {
    @DexIgnore
    public G37<F75> g;
    @DexIgnore
    public Yi6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements RecyclerViewHeartRateCalendar.b {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewMonthFragment a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(HeartRateOverviewMonthFragment heartRateOverviewMonthFragment) {
            this.a = heartRateOverviewMonthFragment;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar.b
        public void a(Calendar calendar) {
            Wg6.c(calendar, "calendar");
            Yi6 K6 = HeartRateOverviewMonthFragment.K6(this.a);
            Date time = calendar.getTime();
            Wg6.b(time, "calendar.time");
            K6.n(time);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements RecyclerViewHeartRateCalendar.a {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewMonthFragment a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(HeartRateOverviewMonthFragment heartRateOverviewMonthFragment) {
            this.a = heartRateOverviewMonthFragment;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewHeartRateCalendar.a
        public void k0(int i, Calendar calendar) {
            Wg6.c(calendar, "calendar");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                HeartRateDetailActivity.a aVar = HeartRateDetailActivity.C;
                Date time = calendar.getTime();
                Wg6.b(time, "it.time");
                Wg6.b(activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ Yi6 K6(HeartRateOverviewMonthFragment heartRateOverviewMonthFragment) {
        Yi6 yi6 = heartRateOverviewMonthFragment.h;
        if (yi6 != null) {
            return yi6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "HeartRateOverviewMonthFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public void L6(Yi6 yi6) {
        Wg6.c(yi6, "presenter");
        this.h = yi6;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Yi6 yi6) {
        L6(yi6);
    }

    @DexIgnore
    @Override // com.fossil.Zi6
    public void e(TreeMap<Long, Integer> treeMap) {
        RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar;
        RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar2;
        Wg6.c(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        G37<F75> g37 = this.g;
        if (g37 != null) {
            F75 a2 = g37.a();
            if (!(a2 == null || (recyclerViewHeartRateCalendar2 = a2.q) == null)) {
                recyclerViewHeartRateCalendar2.setData(treeMap);
            }
            G37<F75> g372 = this.g;
            if (g372 != null) {
                F75 a3 = g372.a();
                if (a3 != null && (recyclerViewHeartRateCalendar = a3.q) != null) {
                    recyclerViewHeartRateCalendar.setEnableButtonNextAndPrevMonth(true);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zi6
    public void g(Date date, Date date2) {
        Wg6.c(date, "selectDate");
        Wg6.c(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        G37<F75> g37 = this.g;
        if (g37 != null) {
            F75 a2 = g37.a();
            if (a2 != null) {
                Calendar instance = Calendar.getInstance();
                Calendar instance2 = Calendar.getInstance();
                Calendar instance3 = Calendar.getInstance();
                Wg6.b(instance, "selectCalendar");
                instance.setTime(date);
                Wg6.b(instance2, "startCalendar");
                instance2.setTime(TimeUtils.V(date2));
                Wg6.b(instance3, "endCalendar");
                instance3.setTime(TimeUtils.E(instance3.getTime()));
                a2.q.K(instance, instance2, instance3);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onCreateView");
        F75 f75 = (F75) Aq0.f(layoutInflater, 2131558566, viewGroup, false, A6());
        RecyclerViewHeartRateCalendar recyclerViewHeartRateCalendar = f75.q;
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "Calendar.getInstance()");
        recyclerViewHeartRateCalendar.setEndDate(instance);
        f75.q.setOnCalendarMonthChanged(new Ai(this));
        f75.q.setOnCalendarItemClickListener(new Bi(this));
        G37<F75> g37 = new G37<>(this, f75);
        this.g = g37;
        if (g37 != null) {
            F75 a2 = g37.a();
            if (a2 != null) {
                return a2.n();
            }
            return null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onResume");
        Yi6 yi6 = this.h;
        if (yi6 != null) {
            yi6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthFragment", "onStop");
        Yi6 yi6 = this.h;
        if (yi6 != null) {
            yi6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
