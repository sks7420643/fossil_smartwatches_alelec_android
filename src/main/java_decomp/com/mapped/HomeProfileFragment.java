package com.mapped;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ai5;
import com.fossil.Ao6;
import com.fossil.Ao7;
import com.fossil.Aq0;
import com.fossil.Aw5;
import com.fossil.B85;
import com.fossil.Bw7;
import com.fossil.Dd1;
import com.fossil.Dl5;
import com.fossil.Ds0;
import com.fossil.Ej1;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Fj1;
import com.fossil.Fk0;
import com.fossil.G37;
import com.fossil.Gb1;
import com.fossil.Gu7;
import com.fossil.H37;
import com.fossil.Hk5;
import com.fossil.Hr7;
import com.fossil.I37;
import com.fossil.Iw5;
import com.fossil.Jk5;
import com.fossil.Jl5;
import com.fossil.Jn5;
import com.fossil.Jx7;
import com.fossil.Kh5;
import com.fossil.Ko7;
import com.fossil.Lr7;
import com.fossil.Nl5;
import com.fossil.Ps4;
import com.fossil.Qj1;
import com.fossil.Qv5;
import com.fossil.S37;
import com.fossil.Sj5;
import com.fossil.T78;
import com.fossil.Tj5;
import com.fossil.Um5;
import com.fossil.Vl5;
import com.fossil.Vt7;
import com.fossil.Wj5;
import com.fossil.Wr4;
import com.fossil.Xy4;
import com.fossil.Yn6;
import com.fossil.Yn7;
import com.fossil.Zn6;
import com.mapped.AlertDialogFragment;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.BaseWebViewActivity;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardActivity;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsActivity;
import com.portfolio.platform.uirenew.home.profile.HomeProfilePresenter;
import com.portfolio.platform.uirenew.home.profile.about.AboutActivity;
import com.portfolio.platform.uirenew.home.profile.battery.ReplaceBatteryActivity;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditActivity;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInActivity;
import com.portfolio.platform.uirenew.home.profile.password.ProfileChangePasswordActivity;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesActivity;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitActivity;
import com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.view.AutoResizeTextView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeProfileFragment extends Qv5 implements Ao6, Iw5.Bi, AlertDialogFragment.Gi, Aw5 {
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public Zn6 h;
    @DexIgnore
    public G37<B85> i;
    @DexIgnore
    public Wj5 j;
    @DexIgnore
    public Iw5 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final HomeProfileFragment a() {
            return new HomeProfileFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Bi(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                HelpActivity.a aVar = HelpActivity.B;
                Wg6.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Ci(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                AboutActivity.a aVar = AboutActivity.B;
                Wg6.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Di(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ConnectedAppsActivity.a aVar = ConnectedAppsActivity.C;
                Wg6.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Ei(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ProfileOptInActivity.a aVar = ProfileOptInActivity.B;
                Wg6.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Fi(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ReplaceBatteryActivity.a aVar = ReplaceBatteryActivity.B;
                Wg6.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Gi(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ThemesActivity.a aVar = ThemesActivity.A;
                Wg6.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Hi(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                String a2 = H37.b.a(6);
                BaseWebViewActivity.a aVar = BaseWebViewActivity.D;
                Wg6.b(activity, "it");
                aVar.b(activity, "", a2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Ii(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.doCameraTask();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Ji(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.M6().n(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Ki(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ProfileEditActivity.a aVar = ProfileEditActivity.A;
                Wg6.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Li(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.M6().n(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Mi(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
                Wg6.b(activity, "it");
                PairingInstructionsActivity.a.b(aVar, activity, false, false, 4, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Ni(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ProfileGoalEditActivity.a aVar = ProfileGoalEditActivity.B;
                Wg6.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Oi(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                WorkoutSettingActivity.a aVar = WorkoutSettingActivity.A;
                Wg6.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Pi(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ProfileChangePasswordActivity.a aVar = ProfileChangePasswordActivity.B;
                Wg6.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Qi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment b;

        @DexIgnore
        public Qi(HomeProfileFragment homeProfileFragment) {
            this.b = homeProfileFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                PreferredUnitActivity.a aVar = PreferredUnitActivity.B;
                Wg6.b(activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ri implements Ej1<Drawable> {
        @DexIgnore
        public /* final */ /* synthetic */ FossilCircleImageView b;
        @DexIgnore
        public /* final */ /* synthetic */ Handler c;
        @DexIgnore
        public /* final */ /* synthetic */ HomeProfileFragment d;
        @DexIgnore
        public /* final */ /* synthetic */ String e;
        @DexIgnore
        public /* final */ /* synthetic */ String f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ri this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Object>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Bitmap $bitmap;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                public static final class Aiiii implements Runnable {
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii b;

                    @DexIgnore
                    public Aiiii(Aiii aiii) {
                        this.b = aiii;
                    }

                    @DexIgnore
                    public final void run() {
                        Ri ri = this.b.this$0.this$0;
                        ri.d.O6(ri.e);
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Bitmap bitmap, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                    this.$bitmap = bitmap;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$bitmap, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Object> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        Bitmap bitmap = this.$bitmap;
                        if (bitmap == null) {
                            return Ao7.a(this.this$0.this$0.c.post(new Aiiii(this)));
                        }
                        this.this$0.this$0.b.setImageBitmap(bitmap);
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ri ri, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ri;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Bitmap e = I37.e(this.this$0.f);
                    Jx7 c = Bw7.c();
                    Aiii aiii = new Aiii(this, e, null);
                    this.L$0 = il6;
                    this.L$1 = e;
                    this.label = 1;
                    if (Eu7.g(c, aiii, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Bitmap bitmap = (Bitmap) this.L$1;
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Ri b;

            @DexIgnore
            public Bii(Ri ri) {
                this.b = ri;
            }

            @DexIgnore
            public final void run() {
                Ri ri = this.b;
                ri.d.O6(ri.e);
            }
        }

        @DexIgnore
        public Ri(FossilCircleImageView fossilCircleImageView, Handler handler, HomeProfileFragment homeProfileFragment, String str, String str2, MFUser mFUser, String str3) {
            this.b = fossilCircleImageView;
            this.c = handler;
            this.d = homeProfileFragment;
            this.e = str2;
            this.f = str3;
        }

        @DexIgnore
        public boolean a(Drawable drawable, Object obj, Qj1<Drawable> qj1, Gb1 gb1, boolean z) {
            return false;
        }

        @DexIgnore
        @Override // com.fossil.Ej1
        public boolean e(Dd1 dd1, Object obj, Qj1<Drawable> qj1, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onLoadFailed, error = ");
            sb.append(dd1 != null ? dd1.getMessage() : null);
            local.d("HomeProfileFragment", sb.toString());
            String str = this.f;
            if (!(str == null || Vt7.l(str))) {
                Rm6 unused = Gu7.d(Ds0.a(this.d), Bw7.a(), null, new Aii(this, null), 2, null);
            } else {
                this.c.post(new Bii(this));
            }
            return true;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object, com.fossil.Qj1, com.fossil.Gb1, boolean] */
        @Override // com.fossil.Ej1
        public /* bridge */ /* synthetic */ boolean g(Drawable drawable, Object obj, Qj1<Drawable> qj1, Gb1 gb1, boolean z) {
            return a(drawable, obj, qj1, gb1, z);
        }
    }

    @DexIgnore
    @T78(122)
    public final void doCameraTask() {
        if (Jn5.c(Jn5.b, getActivity(), Jn5.Ai.EDIT_AVATAR, false, false, false, null, 60, null)) {
            Zn6 zn6 = this.h;
            if (zn6 != null) {
                zn6.s();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "HomeProfileFragment";
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void E0() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.X(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void G4(ArrayList<HomeProfilePresenter.Bi> arrayList) {
        Wg6.c(arrayList, Constants.DEVICES);
        Iw5 iw5 = this.k;
        if (iw5 != null) {
            iw5.o(arrayList);
        }
        G37<B85> g37 = this.i;
        if (g37 != null) {
            B85 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (arrayList.isEmpty()) {
                FlexibleTextView flexibleTextView = a2.u0;
                Wg6.b(flexibleTextView, "it.tvDevice");
                flexibleTextView.setText(Um5.c(PortfolioApp.get.instance(), 2131887074));
                FlexibleButton flexibleButton = a2.H;
                Wg6.b(flexibleButton, "it.cvPairFirstWatch");
                flexibleButton.setVisibility(0);
                FlexibleButton flexibleButton2 = a2.I;
                Wg6.b(flexibleButton2, "it.fbtAddDevice");
                flexibleButton2.setVisibility(8);
                return;
            }
            FlexibleTextView flexibleTextView2 = a2.u0;
            Wg6.b(flexibleTextView2, "it.tvDevice");
            flexibleTextView2.setText(Um5.c(PortfolioApp.get.instance(), 2131887176));
            FlexibleButton flexibleButton3 = a2.H;
            Wg6.b(flexibleButton3, "it.cvPairFirstWatch");
            flexibleButton3.setVisibility(8);
            FlexibleButton flexibleButton4 = a2.I;
            Wg6.b(flexibleButton4, "it.fbtAddDevice");
            flexibleButton4.setVisibility(0);
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void H3(boolean z, Ps4 ps4, boolean z2) {
        if (z) {
            String c = z2 ? Um5.c(PortfolioApp.get.instance(), 2131886218) : Um5.c(PortfolioApp.get.instance(), 2131886246);
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            Wg6.b(c, "title");
            String c2 = Um5.c(PortfolioApp.get.instance(), 2131886242);
            Wg6.b(c2, "LanguageHelper.getString\u2026ourCurrentChallengeFirst)");
            s37.o(childFragmentManager, c, c2, ps4);
        } else if (z2) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
                Wg6.b(activity, "it");
                PairingInstructionsActivity.a.b(aVar, activity, false, false, 4, null);
            }
        } else {
            S37 s372 = S37.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            Wg6.b(childFragmentManager2, "childFragmentManager");
            s372.P(childFragmentManager2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void J4(Yn6 yn6) {
        Wg6.c(yn6, "activityDailyBest");
        G37<B85> g37 = this.i;
        if (g37 != null) {
            B85 a2 = g37.a();
            if (a2 != null) {
                AutoResizeTextView autoResizeTextView = a2.l0;
                Wg6.b(autoResizeTextView, "it.tvAvgActivity");
                autoResizeTextView.setText(Dl5.d((int) yn6.b()));
                FlexibleTextView flexibleTextView = a2.m0;
                Wg6.b(flexibleTextView, "it.tvAvgActivityDate");
                flexibleTextView.setText(TimeUtils.e(yn6.a()));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Iw5.Bi
    public void M0(String str) {
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeProfileFragment", "user select " + str);
        WatchSettingActivity.a aVar = WatchSettingActivity.B;
        FragmentActivity requireActivity = requireActivity();
        Wg6.b(requireActivity, "requireActivity()");
        aVar.b(requireActivity, str);
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        N6((Zn6) obj);
    }

    @DexIgnore
    public final Zn6 M6() {
        Zn6 zn6 = this.h;
        if (zn6 != null) {
            return zn6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N6(Zn6 zn6) {
        Wg6.c(zn6, "presenter");
        this.h = zn6;
    }

    @DexIgnore
    public final void O6(String str) {
        FossilCircleImageView fossilCircleImageView;
        G37<B85> g37 = this.i;
        if (g37 != null) {
            B85 a2 = g37.a();
            if (a2 != null && (fossilCircleImageView = a2.c0) != null) {
                Wj5 wj5 = this.j;
                if (wj5 != null) {
                    wj5.I(new Sj5("", str)).T0(new Fj1().o0(new Hk5())).F0(fossilCircleImageView);
                    Wg6.b(fossilCircleImageView, "it");
                    fossilCircleImageView.setBorderColor(W6.d(requireContext(), 2131099830));
                    fossilCircleImageView.setBorderWidth(3);
                    fossilCircleImageView.setBackground(W6.f(requireContext(), 2131231287));
                    return;
                }
                Wg6.n("mGlideRequests");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void P6() {
        G37<B85> g37 = this.i;
        if (g37 != null) {
            B85 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.C;
                Wg6.b(constraintLayout, "clActiveTime");
                constraintLayout.setVisibility(0);
                Fk0 fk0 = new Fk0();
                fk0.c(a2.B);
                fk0.e(2131362038, 6, 0, 6);
                fk0.e(2131362038, 7, 2131362033, 6);
                fk0.e(2131362038, 3, 0, 3);
                fk0.e(2131362038, 4, 2131362037, 3);
                fk0.e(2131362033, 6, 2131362038, 7);
                fk0.e(2131362033, 7, 0, 7);
                fk0.e(2131362033, 3, 2131362038, 3);
                fk0.e(2131362033, 4, 2131362038, 4);
                fk0.e(2131362048, 6, 2131362038, 6);
                fk0.e(2131362048, 7, 2131362038, 7);
                fk0.e(2131362048, 3, 2131362038, 4);
                fk0.e(2131362048, 4, 0, 4);
                fk0.e(2131362037, 6, 2131362033, 6);
                fk0.e(2131362037, 7, 2131362033, 7);
                fk0.e(2131362037, 3, 2131362048, 3);
                fk0.e(2131362037, 4, 2131362048, 4);
                fk0.a(a2.B);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void Q2(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeProfileFragment", "hideWorkoutSettings, value = " + z);
        G37<B85> g37 = this.i;
        if (g37 != null) {
            B85 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                RelativeLayout relativeLayout = a2.s;
                Wg6.b(relativeLayout, "it.btAutoWorkout");
                relativeLayout.setVisibility(8);
                View view = a2.C0;
                Wg6.b(view, "it.vAutoWorkoutSeparatorLine");
                view.setVisibility(8);
                return;
            }
            RelativeLayout relativeLayout2 = a2.s;
            Wg6.b(relativeLayout2, "it.btAutoWorkout");
            relativeLayout2.setVisibility(0);
            View view2 = a2.C0;
            Wg6.b(view2, "it.vAutoWorkoutSeparatorLine");
            view2.setVisibility(0);
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        G37<B85> g37 = this.i;
        if (g37 != null) {
            B85 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.C;
                Wg6.b(constraintLayout, "clActiveTime");
                constraintLayout.setVisibility(8);
                Fk0 fk0 = new Fk0();
                fk0.c(a2.B);
                fk0.e(2131362038, 6, 0, 6);
                fk0.e(2131362038, 7, 2131362048, 6);
                fk0.e(2131362038, 3, 0, 3);
                fk0.e(2131362038, 4, 2131362037, 3);
                fk0.e(2131362048, 6, 2131362038, 7);
                fk0.e(2131362048, 7, 0, 7);
                fk0.e(2131362048, 3, 2131362038, 3);
                fk0.e(2131362048, 4, 2131362038, 4);
                fk0.e(2131362037, 6, 2131362038, 6);
                fk0.e(2131362037, 7, 2131362038, 7);
                fk0.e(2131362037, 3, 2131362038, 4);
                fk0.e(2131362037, 4, 0, 4);
                fk0.a(a2.B);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void R4(MFUser mFUser, String str) {
        FossilCircleImageView fossilCircleImageView;
        Wg6.c(mFUser, "user");
        String profilePicture = mFUser.getProfilePicture();
        String str2 = mFUser.getFirstName() + " " + mFUser.getLastName();
        G37<B85> g37 = this.i;
        if (g37 != null) {
            B85 a2 = g37.a();
            if (a2 != null && (fossilCircleImageView = a2.c0) != null) {
                if (TextUtils.isEmpty(profilePicture) || (!URLUtil.isHttpUrl(profilePicture) && !URLUtil.isHttpsUrl(profilePicture))) {
                    O6(str2);
                    return;
                }
                Wj5 wj5 = this.j;
                if (wj5 != null) {
                    fossilCircleImageView.j(wj5, profilePicture, str2);
                    Handler handler = new Handler();
                    Wj5 wj52 = this.j;
                    if (wj52 != null) {
                        wj52.J(mFUser.getProfilePicture()).b1(new Ri(fossilCircleImageView, handler, this, profilePicture, str2, mFUser, str)).T0(new Fj1().o0(new Hk5())).F0(fossilCircleImageView);
                        Wg6.b(fossilCircleImageView, "it");
                        fossilCircleImageView.setBorderColor(W6.d(requireContext(), R.color.transparent));
                        return;
                    }
                    Wg6.n("mGlideRequests");
                    throw null;
                }
                Wg6.n("mGlideRequests");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Qv5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Ps4 ps4 = null;
        Wg6.c(str, "tag");
        if (!(str.length() == 0) && getActivity() != null) {
            int hashCode = str.hashCode();
            if (hashCode != -292748329) {
                if (hashCode == 1970588827 && str.equals("LEAVE_CHALLENGE")) {
                    if (i2 == 2131363373) {
                        if (intent != null) {
                            ps4 = (Ps4) intent.getParcelableExtra("CHALLENGE");
                        }
                        if (ps4 != null) {
                            R6(ps4);
                            return;
                        }
                        return;
                    }
                    return;
                }
            } else if (str.equals("CONFIRM_LOGOUT_ACCOUNT")) {
                if (i2 == 2131363373) {
                    Zn6 zn6 = this.h;
                    if (zn6 != null) {
                        zn6.q();
                        return;
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                } else {
                    return;
                }
            }
            super.R5(str, i2, intent);
        }
    }

    @DexIgnore
    public final void R6(Ps4 ps4) {
        long b = Xy4.a.b();
        Date m2 = ps4.m();
        if (b > (m2 != null ? m2.getTime() : 0)) {
            BCOverviewLeaderBoardActivity.A.a(this, ps4, null);
        } else {
            BCWaitingChallengeDetailActivity.B.a(this, ps4, "joined_challenge", -1, false);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void U2(SleepStatistic sleepStatistic) {
        SleepStatistic.SleepDailyBest sleepTimeBestDay;
        G37<B85> g37 = this.i;
        if (g37 != null) {
            B85 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (sleepStatistic == null || (sleepTimeBestDay = sleepStatistic.getSleepTimeBestDay()) == null) {
                FlexibleTextView flexibleTextView = a2.r0;
                Wg6.b(flexibleTextView, "it.tvAvgSleep");
                flexibleTextView.setText(Um5.c(getContext(), 2131887329));
                FlexibleTextView flexibleTextView2 = a2.s0;
                Wg6.b(flexibleTextView2, "it.tvAvgSleepDate");
                flexibleTextView2.setText("");
                return;
            }
            FlexibleTextView flexibleTextView3 = a2.r0;
            Wg6.b(flexibleTextView3, "it.tvAvgSleep");
            flexibleTextView3.setText(Jl5.b.o(sleepTimeBestDay.getValue()));
            FlexibleTextView flexibleTextView4 = a2.s0;
            Wg6.b(flexibleTextView4, "it.tvAvgSleepDate");
            flexibleTextView4.setText(TimeUtils.e(sleepTimeBestDay.getDate()));
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void X3(ActivityStatistic activityStatistic) {
        G37<B85> g37 = this.i;
        if (g37 != null) {
            B85 a2 = g37.a();
            if (a2 != null) {
                if (activityStatistic != null) {
                    ActivityStatistic.ActivityDailyBest stepsBestDay = activityStatistic.getStepsBestDay();
                    ActivityStatistic.ActivityDailyBest activeTimeBestDay = activityStatistic.getActiveTimeBestDay();
                    ActivityStatistic.CaloriesBestDay caloriesBestDay = activityStatistic.getCaloriesBestDay();
                    String c = Um5.c(getContext(), 2131887328);
                    if (stepsBestDay != null) {
                        AutoResizeTextView autoResizeTextView = a2.l0;
                        Wg6.b(autoResizeTextView, "it.tvAvgActivity");
                        autoResizeTextView.setText(Dl5.d(stepsBestDay.getValue()));
                        FlexibleTextView flexibleTextView = a2.m0;
                        Wg6.b(flexibleTextView, "it.tvAvgActivityDate");
                        flexibleTextView.setText(TimeUtils.e(stepsBestDay.getDate()));
                    } else {
                        AutoResizeTextView autoResizeTextView2 = a2.l0;
                        Wg6.b(autoResizeTextView2, "it.tvAvgActivity");
                        autoResizeTextView2.setText(c);
                        FlexibleTextView flexibleTextView2 = a2.m0;
                        Wg6.b(flexibleTextView2, "it.tvAvgActivityDate");
                        flexibleTextView2.setText("");
                    }
                    if (activeTimeBestDay != null) {
                        AutoResizeTextView autoResizeTextView3 = a2.i0;
                        Wg6.b(autoResizeTextView3, "it.tvAvgActiveTime");
                        autoResizeTextView3.setText(Dl5.d(activeTimeBestDay.getValue()));
                        FlexibleTextView flexibleTextView3 = a2.j0;
                        Wg6.b(flexibleTextView3, "it.tvAvgActiveTimeDate");
                        flexibleTextView3.setText(TimeUtils.e(activeTimeBestDay.getDate()));
                    } else {
                        AutoResizeTextView autoResizeTextView4 = a2.i0;
                        Wg6.b(autoResizeTextView4, "it.tvAvgActiveTime");
                        autoResizeTextView4.setText(c);
                        FlexibleTextView flexibleTextView4 = a2.j0;
                        Wg6.b(flexibleTextView4, "it.tvAvgActiveTimeDate");
                        flexibleTextView4.setText("");
                    }
                    if (caloriesBestDay != null) {
                        AutoResizeTextView autoResizeTextView5 = a2.o0;
                        Wg6.b(autoResizeTextView5, "it.tvAvgCalories");
                        autoResizeTextView5.setText(Dl5.d(Lr7.a(caloriesBestDay.getValue())));
                        FlexibleTextView flexibleTextView5 = a2.p0;
                        Wg6.b(flexibleTextView5, "it.tvAvgCaloriesDate");
                        flexibleTextView5.setText(TimeUtils.e(caloriesBestDay.getDate()));
                    } else {
                        AutoResizeTextView autoResizeTextView6 = a2.o0;
                        Wg6.b(autoResizeTextView6, "it.tvAvgCalories");
                        autoResizeTextView6.setText(c);
                        FlexibleTextView flexibleTextView6 = a2.p0;
                        Wg6.b(flexibleTextView6, "it.tvAvgCaloriesDate");
                        flexibleTextView6.setText("");
                    }
                } else {
                    String c2 = Um5.c(getContext(), 2131887328);
                    AutoResizeTextView autoResizeTextView7 = a2.l0;
                    Wg6.b(autoResizeTextView7, "it.tvAvgActivity");
                    autoResizeTextView7.setText(c2);
                    AutoResizeTextView autoResizeTextView8 = a2.o0;
                    Wg6.b(autoResizeTextView8, "it.tvAvgCalories");
                    autoResizeTextView8.setText(c2);
                    AutoResizeTextView autoResizeTextView9 = a2.i0;
                    Wg6.b(autoResizeTextView9, "it.tvAvgActiveTime");
                    autoResizeTextView9.setText(c2);
                    FlexibleTextView flexibleTextView7 = a2.m0;
                    Wg6.b(flexibleTextView7, "it.tvAvgActivityDate");
                    flexibleTextView7.setText("");
                    FlexibleTextView flexibleTextView8 = a2.p0;
                    Wg6.b(flexibleTextView8, "it.tvAvgCaloriesDate");
                    flexibleTextView8.setText("");
                    FlexibleTextView flexibleTextView9 = a2.j0;
                    Wg6.b(flexibleTextView9, "it.tvAvgActiveTimeDate");
                    flexibleTextView9.setText("");
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeProfileFragment", "active serial =" + PortfolioApp.get.instance().J());
            if (DeviceIdentityUtils.isDianaDevice(PortfolioApp.get.instance().J())) {
                P6();
            } else {
                Q6();
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void Z3(boolean z, boolean z2) {
        G37<B85> g37 = this.i;
        if (g37 != null) {
            B85 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                if (z2) {
                    FlexibleTextView flexibleTextView = a2.L;
                    Wg6.b(flexibleTextView, "it.ftvTitleLowBattery");
                    Hr7 hr7 = Hr7.a;
                    String c = Um5.c(PortfolioApp.get.instance(), 2131886837);
                    Wg6.b(c, "LanguageHelper.getString\u2026YourBatteryIsBelowNumber)");
                    String format = String.format(c, Arrays.copyOf(new Object[]{"10%"}, 1));
                    Wg6.b(format, "java.lang.String.format(format, *args)");
                    flexibleTextView.setText(format);
                    FlexibleTextView flexibleTextView2 = a2.K;
                    Wg6.b(flexibleTextView2, "it.ftvDescriptionLowBattery");
                    flexibleTextView2.setText(Um5.c(PortfolioApp.get.instance(), 2131886836));
                    FlexibleButton flexibleButton = a2.x;
                    Wg6.b(flexibleButton, "it.btReplaceBattery");
                    flexibleButton.setVisibility(8);
                } else {
                    FlexibleTextView flexibleTextView3 = a2.L;
                    Wg6.b(flexibleTextView3, "it.ftvTitleLowBattery");
                    Hr7 hr72 = Hr7.a;
                    String c2 = Um5.c(PortfolioApp.get.instance(), 2131886847);
                    Wg6.b(c2, "LanguageHelper.getString\u2026YourBatteryIsBelowNumber)");
                    String format2 = String.format(c2, Arrays.copyOf(new Object[]{"25%"}, 1));
                    Wg6.b(format2, "java.lang.String.format(format, *args)");
                    flexibleTextView3.setText(format2);
                    FlexibleTextView flexibleTextView4 = a2.K;
                    Wg6.b(flexibleTextView4, "it.ftvDescriptionLowBattery");
                    flexibleTextView4.setText(Um5.c(PortfolioApp.get.instance(), 2131886846));
                    FlexibleButton flexibleButton2 = a2.x;
                    Wg6.b(flexibleButton2, "it.btReplaceBattery");
                    flexibleButton2.setVisibility(0);
                }
                if (Wr4.a.a().g()) {
                    FlexibleButton flexibleButton3 = a2.x;
                    Wg6.b(flexibleButton3, "it.btReplaceBattery");
                    flexibleButton3.setVisibility(8);
                }
                ConstraintLayout constraintLayout = a2.G;
                Wg6.b(constraintLayout, "it.clLowBattery");
                constraintLayout.setVisibility(0);
                return;
            }
            ConstraintLayout constraintLayout2 = a2.G;
            Wg6.b(constraintLayout2, "it.clLowBattery");
            constraintLayout2.setVisibility(8);
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Aw5
    public void b2(boolean z) {
        if (z) {
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
            }
            if (this.i != null) {
                Zn6 zn6 = this.h;
                if (zn6 != null) {
                    zn6.p();
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        } else {
            Vl5 C62 = C6();
            if (C62 != null) {
                C62.c("");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void i3() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WelcomeActivity.a aVar = WelcomeActivity.B;
            Wg6.b(activity, "it");
            aVar.b(activity);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void i5(String str) {
        G37<B85> g37 = this.i;
        if (g37 != null) {
            B85 a2 = g37.a();
            if (a2 != null) {
                if (str == null || str.length() == 0) {
                    FlexibleTextView flexibleTextView = a2.A0;
                    Wg6.b(flexibleTextView, "tvSocialId");
                    flexibleTextView.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView2 = a2.A0;
                Wg6.b(flexibleTextView2, "tvSocialId");
                flexibleTextView2.setVisibility(0);
                FlexibleTextView flexibleTextView3 = a2.A0;
                Wg6.b(flexibleTextView3, "tvSocialId");
                Hr7 hr7 = Hr7.a;
                String string = getString(2131887311);
                Wg6.b(string, "getString(R.string.buddy_challenge_profile_social)");
                String format = String.format(string, Arrays.copyOf(new Object[]{str}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView3.setText(format);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void k() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void m() {
        b();
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void o(int i2, String str) {
        Wg6.c(str, "message");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && i2 == 1234) {
            Zn6 zn6 = this.h;
            if (zn6 != null) {
                zn6.r(intent);
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        B85 b85 = (B85) Aq0.f(layoutInflater, 2131558577, viewGroup, false, A6());
        Wj5 c = Tj5.c(this);
        Wg6.b(c, "GlideApp.with(this)");
        this.j = c;
        ArrayList arrayList = new ArrayList();
        Wj5 wj5 = this.j;
        if (wj5 != null) {
            this.k = new Iw5(arrayList, wj5, this, PortfolioApp.get.instance());
            G37<B85> g37 = new G37<>(this, b85);
            this.i = g37;
            if (g37 != null) {
                B85 a2 = g37.a();
                if (a2 != null) {
                    Wg6.b(a2, "mBinding.get()!!");
                    return a2.n();
                }
                Wg6.i();
                throw null;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mGlideRequests");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Zn6 zn6 = this.h;
        if (zn6 != null) {
            if (zn6 != null) {
                zn6.m();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
        Vl5 C6 = C6();
        if (C6 != null) {
            C6.c("");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Zn6 zn6 = this.h;
        if (zn6 != null) {
            if (zn6 != null) {
                zn6.l();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
        Vl5 C6 = C6();
        if (C6 != null) {
            C6.i();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        String d;
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<B85> g37 = this.i;
        if (g37 != null) {
            B85 a2 = g37.a();
            if (a2 != null) {
                String d2 = ThemeManager.l.a().d("onPrimaryButton");
                if (!TextUtils.isEmpty(d2)) {
                    int parseColor = Color.parseColor(d2);
                    Drawable drawable = PortfolioApp.get.instance().getDrawable(2131230985);
                    if (drawable != null) {
                        drawable.setTint(parseColor);
                        a2.I.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
                FossilCircleImageView fossilCircleImageView = a2.c0;
                Wg6.b(fossilCircleImageView, "binding.ivUserAvatar");
                Nl5.a(fossilCircleImageView, new Ii(this));
                a2.w0.setOnClickListener(new Ji(this));
                a2.W.setOnClickListener(new Ki(this));
                a2.I.setOnClickListener(new Li(this));
                a2.H.setOnClickListener(new Mi(this));
                a2.y.setOnClickListener(new Ni(this));
                a2.s.setOnClickListener(new Oi(this));
                a2.t.setOnClickListener(new Pi(this));
                a2.A.setOnClickListener(new Qi(this));
                a2.v.setOnClickListener(new Bi(this));
                a2.r.setOnClickListener(new Ci(this));
                a2.u.setOnClickListener(new Di(this));
                a2.w.setOnClickListener(new Ei(this));
                a2.x.setOnClickListener(new Fi(this));
                a2.z.setOnClickListener(new Gi(this));
                ConstraintLayout constraintLayout = a2.B;
                if (constraintLayout != null) {
                    String d3 = ThemeManager.l.a().d("nonBrandSurface");
                    if (!TextUtils.isEmpty(d3)) {
                        constraintLayout.setBackgroundColor(Color.parseColor(d3));
                    }
                }
                ConstraintLayout constraintLayout2 = a2.G;
                if (!(constraintLayout2 == null || (d = ThemeManager.l.a().d("nonBrandSurface")) == null)) {
                    constraintLayout2.setBackgroundColor(Color.parseColor(d));
                }
                a2.J.setOnClickListener(new Hi(this));
                Zn6 zn6 = this.h;
                if (zn6 != null) {
                    DeviceHelper.Ai ai = DeviceHelper.o;
                    if (zn6 == null) {
                        Wg6.n("mPresenter");
                        throw null;
                    } else if (ai.w(zn6.o())) {
                        String d4 = ThemeManager.l.a().d("dianaActiveMinutesTab");
                        String d5 = ThemeManager.l.a().d("dianaStepsTab");
                        String d6 = ThemeManager.l.a().d("dianaActiveCaloriesTab");
                        String d7 = ThemeManager.l.a().d("dianaSleepTab");
                        if (!TextUtils.isEmpty(d4)) {
                            a2.O.setColorFilter(Color.parseColor(d4));
                        }
                        if (!TextUtils.isEmpty(d7)) {
                            a2.P.setColorFilter(Color.parseColor(d7));
                        }
                        if (!TextUtils.isEmpty(d5)) {
                            a2.Q.setColorFilter(Color.parseColor(d5));
                        }
                        if (!TextUtils.isEmpty(d6)) {
                            a2.R.setColorFilter(Color.parseColor(d6));
                        }
                        RecyclerView recyclerView = a2.h0;
                        Wg6.b(recyclerView, "binding.rvDevices");
                        recyclerView.setAdapter(this.k);
                        RecyclerView recyclerView2 = a2.h0;
                        Wg6.b(recyclerView2, "binding.rvDevices");
                        recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                        RecyclerView recyclerView3 = a2.h0;
                        Wg6.b(recyclerView3, "binding.rvDevices");
                        recyclerView3.setNestedScrollingEnabled(false);
                    }
                }
                String d8 = ThemeManager.l.a().d("hybridStepsTab");
                String d9 = ThemeManager.l.a().d("hybridActiveCaloriesTab");
                String d10 = ThemeManager.l.a().d("hybridSleepTab");
                if (!TextUtils.isEmpty(d10)) {
                    a2.P.setColorFilter(Color.parseColor(d10));
                }
                if (!TextUtils.isEmpty(d8)) {
                    a2.Q.setColorFilter(Color.parseColor(d8));
                }
                if (!TextUtils.isEmpty(d9)) {
                    a2.R.setColorFilter(Color.parseColor(d9));
                }
                RecyclerView recyclerView4 = a2.h0;
                Wg6.b(recyclerView4, "binding.rvDevices");
                recyclerView4.setAdapter(this.k);
                RecyclerView recyclerView22 = a2.h0;
                Wg6.b(recyclerView22, "binding.rvDevices");
                recyclerView22.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView32 = a2.h0;
                Wg6.b(recyclerView32, "binding.rvDevices");
                recyclerView32.setNestedScrollingEnabled(false);
            }
            E6("profile_view");
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void updateUser(MFUser mFUser) {
        String str;
        String format;
        String str2 = null;
        Wg6.c(mFUser, "user");
        FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "updateUser");
        if (isActive()) {
            G37<B85> g37 = this.i;
            if (g37 != null) {
                B85 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.B0;
                    Wg6.b(flexibleTextView, "it.tvUserName");
                    flexibleTextView.setText(mFUser.getFirstName() + " " + mFUser.getLastName());
                    if (!TextUtils.isEmpty(mFUser.getRegisterDate())) {
                        try {
                            String s = TimeUtils.s(TimeUtils.M(mFUser.getRegisterDate()).toDate());
                            FlexibleTextView flexibleTextView2 = a2.x0;
                            Wg6.b(flexibleTextView2, "it.tvMemberSince");
                            Hr7 hr7 = Hr7.a;
                            String c = Um5.c(PortfolioApp.get.instance(), 2131887174);
                            Wg6.b(c, "LanguageHelper.getString\u2026_Text__JoinedInMonthYear)");
                            String format2 = String.format(c, Arrays.copyOf(new Object[]{s}, 1));
                            Wg6.b(format2, "java.lang.String.format(format, *args)");
                            flexibleTextView2.setText(format2);
                        } catch (Exception e) {
                            FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "Exception when parse register date. " + e);
                        }
                    }
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "useDefaultBiometric = " + mFUser.getUseDefaultBiometric());
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "weightInGrams = " + mFUser.getWeightInGrams());
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "heightInCentimeters = " + mFUser.getHeightInCentimeters());
                    String string = PortfolioApp.get.instance().getString(2131887328);
                    Wg6.b(string, "PortfolioApp.instance.ge\u2026ng.character_dash_double)");
                    MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
                    if (Wg6.a(unitGroup != null ? unitGroup.getWeight() : null, Ai5.IMPERIAL.getValue())) {
                        if (mFUser.getWeightInGrams() == 0 || mFUser.getUseDefaultBiometric()) {
                            Hr7 hr72 = Hr7.a;
                            String c2 = Um5.c(PortfolioApp.get.instance(), 2131886956);
                            Wg6.b(c2, "LanguageHelper.getString\u2026alInformation_Label__Lbs)");
                            if (c2 != null) {
                                String lowerCase = c2.toLowerCase();
                                Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                                String format3 = String.format("%s %s", Arrays.copyOf(new Object[]{string, lowerCase}, 2));
                                Wg6.b(format3, "java.lang.String.format(format, *args)");
                                str = format3;
                            } else {
                                throw new Rc6("null cannot be cast to non-null type java.lang.String");
                            }
                        } else {
                            Hr7 hr73 = Hr7.a;
                            String str3 = Dl5.b(Jk5.h((float) (mFUser.getWeightInGrams() + 1)), 1).toString();
                            String c3 = Um5.c(PortfolioApp.get.instance(), 2131886956);
                            Wg6.b(c3, "LanguageHelper.getString\u2026alInformation_Label__Lbs)");
                            if (c3 != null) {
                                String lowerCase2 = c3.toLowerCase();
                                Wg6.b(lowerCase2, "(this as java.lang.String).toLowerCase()");
                                String format4 = String.format("%s %s", Arrays.copyOf(new Object[]{str3, lowerCase2}, 2));
                                Wg6.b(format4, "java.lang.String.format(format, *args)");
                                str = format4;
                            } else {
                                throw new Rc6("null cannot be cast to non-null type java.lang.String");
                            }
                        }
                    } else if (mFUser.getWeightInGrams() == 0 || mFUser.getUseDefaultBiometric()) {
                        Hr7 hr74 = Hr7.a;
                        String c4 = Um5.c(PortfolioApp.get.instance(), 2131886955);
                        Wg6.b(c4, "LanguageHelper.getString\u2026nalInformation_Label__Kg)");
                        if (c4 != null) {
                            String lowerCase3 = c4.toLowerCase();
                            Wg6.b(lowerCase3, "(this as java.lang.String).toLowerCase()");
                            String format5 = String.format("%s %s", Arrays.copyOf(new Object[]{string, lowerCase3}, 2));
                            Wg6.b(format5, "java.lang.String.format(format, *args)");
                            str = format5;
                        } else {
                            throw new Rc6("null cannot be cast to non-null type java.lang.String");
                        }
                    } else {
                        Hr7 hr75 = Hr7.a;
                        String str4 = Dl5.b(Jk5.g((float) (mFUser.getWeightInGrams() + 1)), 1).toString();
                        String c5 = Um5.c(PortfolioApp.get.instance(), 2131886955);
                        Wg6.b(c5, "LanguageHelper.getString\u2026nalInformation_Label__Kg)");
                        if (c5 != null) {
                            String lowerCase4 = c5.toLowerCase();
                            Wg6.b(lowerCase4, "(this as java.lang.String).toLowerCase()");
                            String format6 = String.format("%s %s", Arrays.copyOf(new Object[]{str4, lowerCase4}, 2));
                            Wg6.b(format6, "java.lang.String.format(format, *args)");
                            str = format6;
                        } else {
                            throw new Rc6("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    String string2 = PortfolioApp.get.instance().getString(2131887328);
                    Wg6.b(string2, "PortfolioApp.instance.ge\u2026ng.character_dash_double)");
                    MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                    if (unitGroup2 != null) {
                        str2 = unitGroup2.getHeight();
                    }
                    if (Wg6.a(str2, Ai5.IMPERIAL.getValue())) {
                        if (mFUser.getHeightInCentimeters() == 0 || mFUser.getUseDefaultBiometric()) {
                            Hr7 hr76 = Hr7.a;
                            String c6 = Um5.c(PortfolioApp.get.instance(), 2131887571);
                            Wg6.b(c6, "LanguageHelper.getString\u2026_height_single_character)");
                            format = String.format(c6, Arrays.copyOf(new Object[]{string2}, 1));
                            Wg6.b(format, "java.lang.String.format(format, *args)");
                        } else {
                            Lc6<Integer, Integer> b = Jk5.b((float) mFUser.getHeightInCentimeters());
                            Hr7 hr77 = Hr7.a;
                            String c7 = Um5.c(PortfolioApp.get.instance(), 2131887570);
                            Wg6.b(c7, "LanguageHelper.getString\u2026_height_double_character)");
                            format = String.format(c7, Arrays.copyOf(new Object[]{String.valueOf(b.getFirst().intValue()), String.valueOf(b.getSecond().intValue())}, 2));
                            Wg6.b(format, "java.lang.String.format(format, *args)");
                        }
                    } else if (mFUser.getHeightInCentimeters() == 0 || mFUser.getUseDefaultBiometric()) {
                        Hr7 hr78 = Hr7.a;
                        String c8 = Um5.c(PortfolioApp.get.instance(), 2131886953);
                        Wg6.b(c8, "LanguageHelper.getString\u2026nalInformation_Label__Cm)");
                        if (c8 != null) {
                            String lowerCase5 = c8.toLowerCase();
                            Wg6.b(lowerCase5, "(this as java.lang.String).toLowerCase()");
                            format = String.format("%s %s", Arrays.copyOf(new Object[]{string2, lowerCase5}, 2));
                            Wg6.b(format, "java.lang.String.format(format, *args)");
                        } else {
                            throw new Rc6("null cannot be cast to non-null type java.lang.String");
                        }
                    } else {
                        Hr7 hr79 = Hr7.a;
                        int heightInCentimeters = mFUser.getHeightInCentimeters();
                        String c9 = Um5.c(PortfolioApp.get.instance(), 2131886953);
                        Wg6.b(c9, "LanguageHelper.getString\u2026nalInformation_Label__Cm)");
                        if (c9 != null) {
                            String lowerCase6 = c9.toLowerCase();
                            Wg6.b(lowerCase6, "(this as java.lang.String).toLowerCase()");
                            format = String.format("%s %s", Arrays.copyOf(new Object[]{String.valueOf(heightInCentimeters), lowerCase6}, 2));
                            Wg6.b(format, "java.lang.String.format(format, *args)");
                        } else {
                            throw new Rc6("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    FlexibleTextView flexibleTextView3 = a2.v0;
                    Wg6.b(flexibleTextView3, "it.tvHeightWeight");
                    flexibleTextView3.setText(str + ", " + format);
                    if (Wg6.a(mFUser.getAuthType(), Kh5.EMAIL.getValue())) {
                        RelativeLayout relativeLayout = a2.t;
                        Wg6.b(relativeLayout, "it.btChangePassword");
                        relativeLayout.setVisibility(0);
                        View view = a2.D0;
                        Wg6.b(view, "it.vChangePasswordSeparatorLine");
                        view.setVisibility(0);
                        return;
                    }
                    RelativeLayout relativeLayout2 = a2.t;
                    Wg6.b(relativeLayout2, "it.btChangePassword");
                    relativeLayout2.setVisibility(8);
                    View view2 = a2.D0;
                    Wg6.b(view2, "it.vChangePasswordSeparatorLine");
                    view2.setVisibility(8);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ao6
    public void z2() {
        Iw5 iw5 = this.k;
        if (iw5 != null) {
            iw5.p();
        }
    }
}
