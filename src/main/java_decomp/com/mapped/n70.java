package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Ey1;
import com.fossil.G80;
import com.fossil.Jd0;
import com.fossil.Ox1;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N70 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ L70 b;
    @DexIgnore
    public /* final */ M70 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<N70> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public N70 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                L70 valueOf = L70.valueOf(readString);
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    Wg6.b(readString2, "parcel.readString()!!");
                    return new N70(valueOf, M70.valueOf(readString2));
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public N70[] newArray(int i) {
            return new N70[i];
        }
    }

    @DexIgnore
    public N70(L70 l70, M70 m70) {
        this.b = l70;
        this.c = m70;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(N70.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            N70 n70 = (N70) obj;
            if (this.b != n70.b) {
                return false;
            }
            return this.c == n70.c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.music.MusicEvent");
    }

    @DexIgnore
    public final L70 getAction() {
        return this.b;
    }

    @DexIgnore
    public final M70 getActionStatus() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(new JSONObject(), Jd0.v0, Ey1.a(this.b)), Jd0.w0, Ey1.a(this.c));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
    }
}
