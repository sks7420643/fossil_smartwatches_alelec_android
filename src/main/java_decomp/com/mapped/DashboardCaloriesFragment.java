package com.mapped;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Aq0;
import com.fossil.Aw5;
import com.fossil.Bd6;
import com.fossil.F67;
import com.fossil.G37;
import com.fossil.N55;
import com.fossil.Pv0;
import com.fossil.Sw5;
import com.fossil.Ts0;
import com.fossil.Vl5;
import com.fossil.Vs0;
import com.fossil.Y67;
import com.fossil.Yf6;
import com.fossil.Zf6;
import com.fossil.Zw5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewFragment;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardCaloriesFragment extends BaseFragment implements Zf6, Zw5, Aw5 {
    @DexIgnore
    public G37<N55> g;
    @DexIgnore
    public Yf6 h;
    @DexIgnore
    public DashboardCaloriesAdapter i;
    @DexIgnore
    public CaloriesOverviewFragment j;
    @DexIgnore
    public F67 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends F67 {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView e;
        @DexIgnore
        public /* final */ /* synthetic */ DashboardCaloriesFragment f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager, DashboardCaloriesFragment dashboardCaloriesFragment, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager);
            this.e = recyclerView;
            this.f = dashboardCaloriesFragment;
        }

        @DexIgnore
        @Override // com.fossil.F67
        public void b(int i) {
            DashboardCaloriesFragment.K6(this.f).p();
        }

        @DexIgnore
        @Override // com.fossil.F67
        public void c(int i, int i2) {
        }
    }

    @DexIgnore
    public static final /* synthetic */ Yf6 K6(DashboardCaloriesFragment dashboardCaloriesFragment) {
        Yf6 yf6 = dashboardCaloriesFragment.h;
        if (yf6 != null) {
            return yf6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "DashboardCaloriesFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    public final N55 L6() {
        G37<N55> g37 = this.g;
        if (g37 != null) {
            return g37.a();
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Yf6 yf6) {
        M6(yf6);
    }

    @DexIgnore
    public void M6(Yf6 yf6) {
        Wg6.c(yf6, "presenter");
        this.h = yf6;
    }

    @DexIgnore
    @Override // com.fossil.Zw5
    public void Q(Date date) {
        Wg6.c(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardCaloriesFragment", "onDayClicked: " + date);
        Context context = getContext();
        if (context != null) {
            CaloriesDetailActivity.a aVar = CaloriesDetailActivity.C;
            Wg6.b(context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    @Override // com.fossil.Aw5
    public void b2(boolean z) {
        N55 L6;
        RecyclerView recyclerView;
        View view;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("DashboardCaloriesFragment visible=");
        sb.append(z);
        sb.append(", tracer=");
        sb.append(C6());
        sb.append(", isRunning=");
        Vl5 C6 = C6();
        sb.append(C6 != null ? Boolean.valueOf(C6.f()) : null);
        local.d("onVisibleChanged", sb.toString());
        if (z) {
            Vl5 C62 = C6();
            if (C62 != null) {
                C62.i();
            }
            if (isVisible() && this.g != null && (L6 = L6()) != null && (recyclerView = L6.q) != null) {
                RecyclerView.ViewHolder findViewHolderForAdapterPosition = recyclerView.findViewHolderForAdapterPosition(0);
                if (findViewHolderForAdapterPosition == null || (view = findViewHolderForAdapterPosition.itemView) == null || view.getY() != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    recyclerView.smoothScrollToPosition(0);
                    F67 f67 = this.k;
                    if (f67 != null) {
                        f67.d();
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        Vl5 C63 = C6();
        if (C63 != null) {
            C63.c("");
        }
    }

    @DexIgnore
    @Override // com.fossil.Zf6
    public void d() {
        F67 f67 = this.k;
        if (f67 != null) {
            f67.d();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        G37<N55> g37 = new G37<>(this, (N55) Aq0.f(layoutInflater, 2131558544, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            N55 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d("DashboardCaloriesFragment", "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        Yf6 yf6 = this.h;
        if (yf6 != null) {
            yf6.o();
            super.onDestroyView();
            v6();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Yf6 yf6 = this.h;
        if (yf6 != null) {
            yf6.l();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        Yf6 yf6 = this.h;
        if (yf6 != null) {
            yf6.m();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        RecyclerView recyclerView;
        RecyclerView recyclerView2;
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        CaloriesOverviewFragment caloriesOverviewFragment = (CaloriesOverviewFragment) getChildFragmentManager().Z("CaloriesOverviewFragment");
        this.j = caloriesOverviewFragment;
        if (caloriesOverviewFragment == null) {
            this.j = new CaloriesOverviewFragment();
        }
        Sw5 sw5 = new Sw5();
        PortfolioApp instance = PortfolioApp.get.instance();
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        CaloriesOverviewFragment caloriesOverviewFragment2 = this.j;
        if (caloriesOverviewFragment2 != null) {
            this.i = new DashboardCaloriesAdapter(sw5, instance, this, childFragmentManager, caloriesOverviewFragment2);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            N55 L6 = L6();
            if (!(L6 == null || (recyclerView2 = L6.q) == null)) {
                Wg6.b(recyclerView2, "it");
                recyclerView2.setLayoutManager(linearLayoutManager);
                DashboardCaloriesAdapter dashboardCaloriesAdapter = this.i;
                if (dashboardCaloriesAdapter != null) {
                    recyclerView2.setAdapter(dashboardCaloriesAdapter);
                    RecyclerView.m layoutManager = recyclerView2.getLayoutManager();
                    if (layoutManager != null) {
                        Ai ai = new Ai(recyclerView2, (LinearLayoutManager) layoutManager, this, linearLayoutManager);
                        this.k = ai;
                        if (ai != null) {
                            recyclerView2.addOnScrollListener(ai);
                            recyclerView2.setItemViewCacheSize(0);
                            Bd6 bd6 = new Bd6(linearLayoutManager.q2());
                            Drawable f = W6.f(recyclerView2.getContext(), 2131230856);
                            if (f != null) {
                                Wg6.b(f, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                                bd6.h(f);
                                recyclerView2.addItemDecoration(bd6);
                                Yf6 yf6 = this.h;
                                if (yf6 != null) {
                                    yf6.n();
                                } else {
                                    Wg6.n("mPresenter");
                                    throw null;
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                    }
                } else {
                    Wg6.n("mDashboardCaloriesAdapter");
                    throw null;
                }
            }
            N55 L62 = L6();
            if (!(L62 == null || (recyclerView = L62.q) == null)) {
                Wg6.b(recyclerView, "recyclerView");
                RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
                if (itemAnimator instanceof Pv0) {
                    ((Pv0) itemAnimator).setSupportsChangeAnimations(false);
                }
            }
            E6("calories_view");
            FragmentActivity activity = getActivity();
            if (activity != null) {
                Ts0 a2 = Vs0.e(activity).a(Y67.class);
                Wg6.b(a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
                Y67 y67 = (Y67) a2;
                return;
            }
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zf6
    public void q(Cf<ActivitySummary> cf) {
        DashboardCaloriesAdapter dashboardCaloriesAdapter = this.i;
        if (dashboardCaloriesAdapter != null) {
            dashboardCaloriesAdapter.x(cf);
        } else {
            Wg6.n("mDashboardCaloriesAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Zw5
    public void q0(Date date, Date date2) {
        Wg6.c(date, "startWeekDate");
        Wg6.c(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardCaloriesFragment", "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
