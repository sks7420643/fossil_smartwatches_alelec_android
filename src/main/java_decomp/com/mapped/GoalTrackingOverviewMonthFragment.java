package com.mapped;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.Ts0;
import com.fossil.Uh6;
import com.fossil.V65;
import com.fossil.Vh6;
import com.fossil.Vs0;
import com.fossil.Y67;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingOverviewMonthFragment extends BaseFragment implements Vh6, RecyclerViewCalendar.a {
    @DexIgnore
    public Y67 g;
    @DexIgnore
    public G37<V65> h;
    @DexIgnore
    public Uh6 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewMonthFragment b;

        @DexIgnore
        public Ai(GoalTrackingOverviewMonthFragment goalTrackingOverviewMonthFragment, V65 v65) {
            this.b = goalTrackingOverviewMonthFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewMonthFragment.K6(this.b).a().l(2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements RecyclerViewCalendar.b {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewMonthFragment a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(GoalTrackingOverviewMonthFragment goalTrackingOverviewMonthFragment) {
            this.a = goalTrackingOverviewMonthFragment;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.b
        public void a(Calendar calendar) {
            Wg6.c(calendar, "calendar");
            Uh6 uh6 = this.a.i;
            if (uh6 != null) {
                Date time = calendar.getTime();
                Wg6.b(time, "calendar.time");
                uh6.n(time);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ Y67 K6(GoalTrackingOverviewMonthFragment goalTrackingOverviewMonthFragment) {
        Y67 y67 = goalTrackingOverviewMonthFragment.g;
        if (y67 != null) {
            return y67;
        }
        Wg6.n("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "GoalTrackingOverviewMonthFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Uh6 uh6) {
        N6(uh6);
    }

    @DexIgnore
    public final void M6() {
        V65 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        G37<V65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.J("hybridGoalTrackingTab");
        }
    }

    @DexIgnore
    public void N6(Uh6 uh6) {
        Wg6.c(uh6, "presenter");
        this.i = uh6;
    }

    @DexIgnore
    @Override // com.fossil.Vh6
    public void e(TreeMap<Long, Float> treeMap) {
        V65 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        Wg6.c(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        G37<V65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.setData(treeMap);
            recyclerViewCalendar.setEnableButtonNextAndPrevMonth(Boolean.TRUE);
        }
    }

    @DexIgnore
    @Override // com.fossil.Vh6
    public void g(Date date, Date date2) {
        V65 a2;
        Wg6.c(date, "selectDate");
        Wg6.c(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        G37<V65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null) {
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            Calendar instance3 = Calendar.getInstance();
            Wg6.b(instance, "selectCalendar");
            instance.setTime(date);
            Wg6.b(instance2, "startCalendar");
            instance2.setTime(TimeUtils.V(date2));
            Wg6.b(instance3, "endCalendar");
            instance3.setTime(TimeUtils.E(instance3.getTime()));
            a2.q.L(instance, instance2, instance3);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.a
    public void k0(int i2, Calendar calendar) {
        Wg6.c(calendar, "calendar");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i2 + ", calendar=" + calendar);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            GoalTrackingDetailActivity.a aVar = GoalTrackingDetailActivity.C;
            Date time = calendar.getTime();
            Wg6.b(time, "it.time");
            Wg6.b(activity, Constants.ACTIVITY);
            aVar.a(time, activity);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        V65 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onCreateView");
        V65 v65 = (V65) Aq0.f(layoutInflater, 2131558561, viewGroup, false, A6());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Ts0 a3 = Vs0.e(activity).a(Y67.class);
            Wg6.b(a3, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.g = (Y67) a3;
            v65.s.setOnClickListener(new Ai(this, v65));
        }
        RecyclerViewCalendar recyclerViewCalendar = v65.q;
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "Calendar.getInstance()");
        recyclerViewCalendar.setEndDate(instance);
        v65.q.setOnCalendarMonthChanged(new Bi(this));
        v65.q.setOnCalendarItemClickListener(this);
        this.h = new G37<>(this, v65);
        M6();
        G37<V65> g37 = this.h;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onResume");
        M6();
        Uh6 uh6 = this.i;
        if (uh6 != null) {
            uh6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onStop");
        Uh6 uh6 = this.i;
        if (uh6 != null) {
            uh6.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Vh6
    public void x(boolean z) {
        V65 a2;
        G37<V65> g37 = this.h;
        if (g37 != null && (a2 = g37.a()) != null) {
            if (z) {
                RecyclerViewCalendar recyclerViewCalendar = a2.q;
                Wg6.b(recyclerViewCalendar, "binding.calendarMonth");
                recyclerViewCalendar.setVisibility(4);
                ConstraintLayout constraintLayout = a2.r;
                Wg6.b(constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            RecyclerViewCalendar recyclerViewCalendar2 = a2.q;
            Wg6.b(recyclerViewCalendar2, "binding.calendarMonth");
            recyclerViewCalendar2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.r;
            Wg6.b(constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }
}
