package com.mapped;

import com.fossil.Bs7;
import com.fossil.Er7;
import com.fossil.Gm7;
import com.fossil.Ht7;
import com.fossil.Kt7;
import com.fossil.Lt7;
import com.fossil.Mm7;
import com.fossil.Mt7;
import com.fossil.Nq7;
import com.fossil.Qq7;
import com.fossil.Ts7;
import com.fossil.Ys7;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mj6 implements Serializable {
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public Set<? extends Mt7> _options;
    @DexIgnore
    public /* final */ Pattern nativePattern;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final int b(int i) {
            return (i & 2) != 0 ? i | 64 : i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Serializable {
        @DexIgnore
        public static /* final */ Aii Companion; // = new Aii(null);
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ int flags;
        @DexIgnore
        public /* final */ String pattern;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
                this();
            }
        }

        @DexIgnore
        public Bi(String str, int i) {
            Wg6.c(str, "pattern");
            this.pattern = str;
            this.flags = i;
        }

        @DexIgnore
        private final Object readResolve() {
            Pattern compile = Pattern.compile(this.pattern, this.flags);
            Wg6.b(compile, "Pattern.compile(pattern, flags)");
            return new Mj6(compile);
        }

        @DexIgnore
        public final int getFlags() {
            return this.flags;
        }

        @DexIgnore
        public final String getPattern() {
            return this.pattern;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Qq7 implements Gg6<Ht7> {
        @DexIgnore
        public /* final */ /* synthetic */ CharSequence $input;
        @DexIgnore
        public /* final */ /* synthetic */ int $startIndex;
        @DexIgnore
        public /* final */ /* synthetic */ Mj6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Mj6 mj6, CharSequence charSequence, int i) {
            super(0);
            this.this$0 = mj6;
            this.$input = charSequence;
            this.$startIndex = i;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final Ht7 invoke() {
            throw null;
            //return this.this$0.find(this.$input, this.$startIndex);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final /* synthetic */ class Di extends Nq7 implements Hg6<Ht7, Ht7> {
        @DexIgnore
        public static /* final */ Di INSTANCE; // = new Di();

        @DexIgnore
        public Di() {
            super(1);
        }

        @DexIgnore
        @Override // com.fossil.Gq7, com.fossil.Ds7
        public final String getName() {
            return "next";
        }

        @DexIgnore
        @Override // com.fossil.Gq7
        public final Hi6 getOwner() {
            return Er7.b(Ht7.class);
        }

        @DexIgnore
        @Override // com.fossil.Gq7
        public final String getSignature() {
            return "next()Lkotlin/text/MatchResult;";
        }

        @DexIgnore
        public final Ht7 invoke(Ht7 ht7) {
            Wg6.c(ht7, "p1");
            return ht7.next();
        }

        @DexIgnore
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Ht7 invoke(Ht7 ht7) {
            return invoke(ht7);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Mj6(java.lang.String r3) {
        /*
            r2 = this;
            java.lang.String r0 = "pattern"
            com.mapped.Wg6.c(r3, r0)
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r3)
            java.lang.String r1 = "Pattern.compile(pattern)"
            com.mapped.Wg6.b(r0, r1)
            r2.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.Mj6.<init>(java.lang.String):void");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Mj6(java.lang.String r3, com.fossil.Mt7 r4) {
        /*
            r2 = this;
            java.lang.String r0 = "pattern"
            com.mapped.Wg6.c(r3, r0)
            java.lang.String r0 = "option"
            com.mapped.Wg6.c(r4, r0)
            com.mapped.Mj6$Ai r0 = com.mapped.Mj6.Companion
            int r1 = r4.getValue()
            int r0 = com.mapped.Mj6.Ai.a(r0, r1)
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r3, r0)
            java.lang.String r1 = "Pattern.compile(pattern,\u2026nicodeCase(option.value))"
            com.mapped.Wg6.b(r0, r1)
            r2.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.Mj6.<init>(java.lang.String, com.fossil.Mt7):void");
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Mj6(java.lang.String r3, java.util.Set<? extends com.fossil.Mt7> r4) {
        /*
            r2 = this;
            java.lang.String r0 = "pattern"
            com.mapped.Wg6.c(r3, r0)
            java.lang.String r0 = "options"
            com.mapped.Wg6.c(r4, r0)
            com.mapped.Mj6$Ai r0 = com.mapped.Mj6.Companion
            int r1 = com.fossil.Lt7.d(r4)
            int r0 = com.mapped.Mj6.Ai.a(r0, r1)
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r3, r0)
            java.lang.String r1 = "Pattern.compile(pattern,\u2026odeCase(options.toInt()))"
            com.mapped.Wg6.b(r0, r1)
            r2.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.Mj6.<init>(java.lang.String, java.util.Set):void");
    }

    @DexIgnore
    public Mj6(Pattern pattern) {
        Wg6.c(pattern, "nativePattern");
        this.nativePattern = pattern;
    }

    @DexIgnore
    public static /* synthetic */ Ht7 find$default(Mj6 mj6, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return mj6.find(charSequence, i);
    }

    @DexIgnore
    public static /* synthetic */ Ts7 findAll$default(Mj6 mj6, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return mj6.findAll(charSequence, i);
    }

    @DexIgnore
    public static /* synthetic */ List split$default(Mj6 mj6, CharSequence charSequence, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return mj6.split(charSequence, i);
    }

    @DexIgnore
    private final Object writeReplace() {
        String pattern = this.nativePattern.pattern();
        Wg6.b(pattern, "nativePattern.pattern()");
        return new Bi(pattern, this.nativePattern.flags());
    }

    @DexIgnore
    public final boolean containsMatchIn(CharSequence charSequence) {
        Wg6.c(charSequence, "input");
        return this.nativePattern.matcher(charSequence).find();
    }

    @DexIgnore
    public final Ht7 find(CharSequence charSequence, int i) {
        Wg6.c(charSequence, "input");
        Matcher matcher = this.nativePattern.matcher(charSequence);
        Wg6.b(matcher, "nativePattern.matcher(input)");
        return Lt7.e(matcher, i, charSequence);
    }

    @DexIgnore
    public final Ts7<Ht7> findAll(CharSequence charSequence, int i) {
        Wg6.c(charSequence, "input");
        return Ys7.d(new Ci(this, charSequence, i), Di.INSTANCE);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.Set<? extends com.fossil.Mt7>, java.util.Set<com.fossil.Mt7> */
    public final Set<Mt7> getOptions() {
        Set set = this._options;
        if (set != null) {
            return set;
        }
        int flags = this.nativePattern.flags();
        EnumSet allOf = EnumSet.allOf(Mt7.class);
        Mm7.x(allOf, new Kt7(flags));
        Set<Mt7> unmodifiableSet = Collections.unmodifiableSet(allOf);
        Wg6.b(unmodifiableSet, "Collections.unmodifiable\u2026mask == it.value }\n    })");
        this._options = unmodifiableSet;
        return unmodifiableSet;
    }

    @DexIgnore
    public final String getPattern() {
        String pattern = this.nativePattern.pattern();
        Wg6.b(pattern, "nativePattern.pattern()");
        return pattern;
    }

    @DexIgnore
    public final Ht7 matchEntire(CharSequence charSequence) {
        Wg6.c(charSequence, "input");
        Matcher matcher = this.nativePattern.matcher(charSequence);
        Wg6.b(matcher, "nativePattern.matcher(input)");
        return Lt7.f(matcher, charSequence);
    }

    @DexIgnore
    public final boolean matches(CharSequence charSequence) {
        Wg6.c(charSequence, "input");
        return this.nativePattern.matcher(charSequence).matches();
    }

    @DexIgnore
    public final String replace(CharSequence charSequence, Hg6<? super Ht7, ? extends CharSequence> hg6) {
        int i = 0;
        Wg6.c(charSequence, "input");
        Wg6.c(hg6, "transform");
        Ht7 find$default = find$default(this, charSequence, 0, 2, null);
        if (find$default == null) {
            return charSequence.toString();
        }
        int length = charSequence.length();
        StringBuilder sb = new StringBuilder(length);
        Ht7 ht7 = find$default;
        while (ht7 != null) {
            sb.append(charSequence, i, ht7.a().h().intValue());
            sb.append((CharSequence) hg6.invoke(ht7));
            i = ht7.a().g().intValue() + 1;
            Ht7 next = ht7.next();
            if (i >= length || next == null) {
                if (i < length) {
                    sb.append(charSequence, i, length);
                }
                String sb2 = sb.toString();
                Wg6.b(sb2, "sb.toString()");
                return sb2;
            }
            ht7 = next;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final String replace(CharSequence charSequence, String str) {
        Wg6.c(charSequence, "input");
        Wg6.c(str, "replacement");
        String replaceAll = this.nativePattern.matcher(charSequence).replaceAll(str);
        Wg6.b(replaceAll, "nativePattern.matcher(in\u2026).replaceAll(replacement)");
        return replaceAll;
    }

    @DexIgnore
    public final String replaceFirst(CharSequence charSequence, String str) {
        Wg6.c(charSequence, "input");
        Wg6.c(str, "replacement");
        String replaceFirst = this.nativePattern.matcher(charSequence).replaceFirst(str);
        Wg6.b(replaceFirst, "nativePattern.matcher(in\u2026replaceFirst(replacement)");
        return replaceFirst;
    }

    @DexIgnore
    public final List<String> split(CharSequence charSequence, int i) {
        int i2 = 10;
        Wg6.c(charSequence, "input");
        if (i >= 0) {
            Matcher matcher = this.nativePattern.matcher(charSequence);
            if (!matcher.find() || i == 1) {
                return Gm7.b(charSequence.toString());
            }
            if (i > 0) {
                i2 = Bs7.g(i, 10);
            }
            ArrayList arrayList = new ArrayList(i2);
            int i3 = i - 1;
            int i4 = 0;
            do {
                arrayList.add(charSequence.subSequence(i4, matcher.start()).toString());
                i4 = matcher.end();
                if (i3 >= 0 && arrayList.size() == i3) {
                    break;
                }
            } while (matcher.find());
            arrayList.add(charSequence.subSequence(i4, charSequence.length()).toString());
            return arrayList;
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i + '.').toString());
    }

    @DexIgnore
    public final Pattern toPattern() {
        return this.nativePattern;
    }

    @DexIgnore
    public String toString() {
        String pattern = this.nativePattern.toString();
        Wg6.b(pattern, "nativePattern.toString()");
        return pattern;
    }
}
