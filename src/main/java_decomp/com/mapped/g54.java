package com.mapped;

import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class G54 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ PresetRepository.Anon21 b;
    @DexIgnore
    public /* final */ /* synthetic */ SavedPreset c;

    @DexIgnore
    public /* synthetic */ G54(PresetRepository.Anon21 anon21, SavedPreset savedPreset) {
        this.b = anon21;
        this.c = savedPreset;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c);
    }
}
