package com.mapped;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ol5;
import com.fossil.Um5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppsAdapter extends RecyclerView.g<Bi> {
    @DexIgnore
    public CustomizeWidget a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<WatchApp> c;
    @DexIgnore
    public Ai d;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(WatchApp watchApp);

        @DexIgnore
        void b(WatchApp watchApp);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public CustomizeWidget a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public View d;
        @DexIgnore
        public WatchApp e;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppsAdapter f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Ai l;
                if (this.b.f.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && (l = this.b.f.l()) != null) {
                    Object obj = this.b.f.c.get(this.b.getAdapterPosition());
                    Wg6.b(obj, "mData[adapterPosition]");
                    l.a((WatchApp) obj);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Bii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Ai l;
                if (this.b.f.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && (l = this.b.f.l()) != null) {
                    Object obj = this.b.f.c.get(this.b.getAdapterPosition());
                    Wg6.b(obj, "mData[adapterPosition]");
                    l.b((WatchApp) obj);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii implements CustomizeWidget.b {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Cii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            @Override // com.portfolio.platform.view.CustomizeWidget.b
            public void a(CustomizeWidget customizeWidget) {
                Wg6.c(customizeWidget, "view");
                this.a.f.a = customizeWidget;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(WatchAppsAdapter watchAppsAdapter, View view) {
            super(view);
            Wg6.c(view, "view");
            this.f = watchAppsAdapter;
            this.c = view.findViewById(2131362728);
            this.d = view.findViewById(2131362767);
            View findViewById = view.findViewById(2131363551);
            Wg6.b(findViewById, "view.findViewById(R.id.wc_watch_app)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363424);
            Wg6.b(findViewById2, "view.findViewById(R.id.tv_watch_app_name)");
            this.b = (TextView) findViewById2;
            this.a.setOnClickListener(new Aii(this));
            this.d.setOnClickListener(new Bii(this));
        }

        @DexIgnore
        public final void a(WatchApp watchApp) {
            String str;
            Wg6.c(watchApp, "watchApp");
            this.e = watchApp;
            if (watchApp != null) {
                this.a.b0(watchApp.getWatchappId());
                this.b.setText(Um5.d(PortfolioApp.get.instance(), watchApp.getNameKey(), watchApp.getName()));
            }
            this.a.setSelectedWc(Wg6.a(watchApp.getWatchappId(), this.f.b));
            View view = this.d;
            Wg6.b(view, "ivWarning");
            view.setVisibility(!Ol5.c.e(watchApp.getWatchappId()) ? 0 : 8);
            if (Wg6.a(watchApp.getWatchappId(), this.f.b)) {
                View view2 = this.c;
                Wg6.b(view2, "ivIndicator");
                view2.setBackground(W6.f(PortfolioApp.get.instance(), 2131230956));
            } else {
                View view3 = this.c;
                Wg6.b(view3, "ivIndicator");
                view3.setBackground(W6.f(PortfolioApp.get.instance(), 2131230957));
            }
            CustomizeWidget customizeWidget = this.a;
            Intent intent = new Intent();
            WatchApp watchApp2 = this.e;
            if (watchApp2 == null || (str = watchApp2.getWatchappId()) == null) {
                str = "";
            }
            Intent putExtra = intent.putExtra("KEY_ID", str);
            Wg6.b(putExtra, "Intent().putExtra(Custom\u2026                   ?: \"\")");
            CustomizeWidget.X(customizeWidget, "WATCH_APP", putExtra, null, new Cii(this), 4, null);
        }
    }

    @DexIgnore
    public WatchAppsAdapter(ArrayList<WatchApp> arrayList, Ai ai) {
        Wg6.c(arrayList, "mData");
        this.c = arrayList;
        this.d = ai;
        this.b = "empty";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WatchAppsAdapter(ArrayList arrayList, Ai ai, int i, Qg6 qg6) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : ai);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final void j() {
        FLogger.INSTANCE.getLocal().d("WatchAppsAdapter", "dragStopped");
        CustomizeWidget customizeWidget = this.a;
        if (customizeWidget != null) {
            customizeWidget.setDragMode(false);
        }
    }

    @DexIgnore
    public final int k(String str) {
        T t;
        Wg6.c(str, "watchAppId");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (Wg6.a(next.getWatchappId(), str)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return this.c.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    public final Ai l() {
        return this.d;
    }

    @DexIgnore
    public void m(Bi bi, int i) {
        Wg6.c(bi, "holder");
        if (getItemCount() > i && i != -1) {
            WatchApp watchApp = this.c.get(i);
            Wg6.b(watchApp, "mData[position]");
            bi.a(watchApp);
        }
    }

    @DexIgnore
    public Bi n(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558724, viewGroup, false);
        Wg6.b(inflate, "LayoutInflater.from(pare\u2026watch_app, parent, false)");
        return new Bi(this, inflate);
    }

    @DexIgnore
    public final void o(List<WatchApp> list) {
        Wg6.c(list, "data");
        this.c.clear();
        this.c.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        m(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return n(viewGroup, i);
    }

    @DexIgnore
    public final void p(Ai ai) {
        Wg6.c(ai, "listener");
        this.d = ai;
    }

    @DexIgnore
    public final void q(String str) {
        Wg6.c(str, "watchAppId");
        this.b = str;
        notifyDataSetChanged();
    }
}
