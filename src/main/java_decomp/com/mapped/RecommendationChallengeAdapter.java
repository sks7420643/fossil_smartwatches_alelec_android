package com.mapped;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Az4;
import com.fossil.Mt4;
import com.fossil.Sx4;
import com.fossil.Yx4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecommendationChallengeAdapter extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ List<Object> a; // = new ArrayList();
    @DexIgnore
    public Az4 b; // = new Az4(1);
    @DexIgnore
    public Yx4 c; // = new Yx4(2);
    @DexIgnore
    public Sx4 d; // = new Sx4(3);

    @DexIgnore
    public final void g(int i) {
        Object obj = this.a.get(i);
        if (!(obj instanceof Mt4)) {
            obj = null;
        }
        Mt4 mt4 = (Mt4) obj;
        if (mt4 != null) {
            mt4.d(false);
        }
        notifyItemChanged(i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        if (this.b.b(this.a, i)) {
            return this.b.a();
        }
        if (this.c.b(this.a, i)) {
            return this.c.a();
        }
        if (this.d.b(this.a, i)) {
            return this.d.a();
        }
        throw new IllegalArgumentException("No delegate for this position : " + i);
    }

    @DexIgnore
    public final Object h(int i) {
        return this.a.get(i);
    }

    @DexIgnore
    public final void i(int i) {
        int size = this.a.size();
        int i2 = i - 1;
        int i3 = i + 1;
        if (i != -1 && i < size) {
            if (size == 2) {
                try {
                    this.a.clear();
                    notifyDataSetChanged();
                } catch (Exception e) {
                    FLogger.INSTANCE.getLocal().e("RecommendationChallengeAdapter", "remove : index: " + i + " - size: " + size);
                }
            } else if (i2 == 0 && i3 < size && (this.a.get(i2) instanceof String) && (this.a.get(i3) instanceof String)) {
                this.a.remove(0);
                this.a.remove(0);
                notifyItemRangeRemoved(0, 2);
            } else if (i2 <= -1 || !(this.a.get(i2) instanceof String) || i3 != size) {
                this.a.remove(i);
                notifyItemRemoved(i);
            } else {
                this.a.remove(i);
                notifyItemRemoved(i);
                this.a.remove(i2);
                notifyItemRemoved(i2);
            }
        }
    }

    @DexIgnore
    public final void j(List<? extends Object> list) {
        Wg6.c(list, "newData");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void k(TimerViewObserver timerViewObserver) {
        Wg6.c(timerViewObserver, "observer");
        this.c.e(timerViewObserver);
        this.d.e(timerViewObserver);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        Wg6.c(viewHolder, "holder");
        int itemViewType = getItemViewType(i);
        if (itemViewType == this.b.a()) {
            this.b.c(this.a, i, viewHolder);
        } else if (itemViewType == this.c.a()) {
            this.c.c(this.a, i, viewHolder);
        } else if (itemViewType == this.d.a()) {
            this.d.c(this.a, i, viewHolder);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        if (i == this.b.a()) {
            return this.b.d(viewGroup);
        }
        if (i == this.c.a()) {
            return this.c.d(viewGroup);
        }
        if (i == this.d.a()) {
            return this.d.d(viewGroup);
        }
        throw new IllegalArgumentException("No support for this viewType: " + i);
    }
}
