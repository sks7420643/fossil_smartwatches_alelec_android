package com.mapped;

import com.fossil.Lx0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Xh {
    @DexIgnore
    public /* final */ int endVersion;
    @DexIgnore
    public /* final */ int startVersion;

    @DexIgnore
    public Xh(int i, int i2) {
        this.startVersion = i;
        this.endVersion = i2;
    }

    @DexIgnore
    public abstract void migrate(Lx0 lx0);
}
