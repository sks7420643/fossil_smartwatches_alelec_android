package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.D90;
import com.fossil.E;
import com.fossil.Ey1;
import com.fossil.Hy1;
import com.fossil.Y8;
import com.fossil.Zm1;
import com.fossil.fitness.Gender;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H60 extends R60 {
    @DexIgnore
    public static /* final */ Ci CREATOR; // = new Ci(null);
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ Ai d;
    @DexIgnore
    public /* final */ short e;
    @DexIgnore
    public /* final */ short f;
    @DexIgnore
    public /* final */ Bi g;

    @DexIgnore
    public enum Ai {
        UNSPECIFIED((byte) 0),
        MALE((byte) 1),
        FEMALE((byte) 2);
        
        @DexIgnore
        public static /* final */ Aii d; // = new Aii(null);
        @DexIgnore
        public /* final */ byte b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
            }

            @DexIgnore
            public final Ai a(byte b) throws IllegalArgumentException {
                Ai ai;
                Ai[] values = Ai.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        ai = null;
                        break;
                    }
                    ai = values[i];
                    if (ai.a() == b) {
                        break;
                    }
                    i++;
                }
                if (ai != null) {
                    return ai;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public Ai(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }

        @DexIgnore
        public final Gender b() {
            int i = Y8.a[ordinal()];
            if (i == 1) {
                return Gender.UNSPECIFIED;
            }
            if (i == 2) {
                return Gender.MALE;
            }
            if (i == 3) {
                return Gender.FEMALE;
            }
            throw new Kc6();
        }
    }

    @DexIgnore
    public enum Bi {
        UNSPECIFIED((byte) 0),
        LEFT_WRIST((byte) 1),
        RIGHT_WRIST((byte) 2),
        UNSPECIFIED_WRIST((byte) 3);
        
        @DexIgnore
        public static /* final */ Aii d; // = new Aii(null);
        @DexIgnore
        public /* final */ byte b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
            }

            @DexIgnore
            public final Bi a(byte b) throws IllegalArgumentException {
                Bi bi;
                Bi[] values = Bi.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        bi = null;
                        break;
                    }
                    bi = values[i];
                    if (bi.a() == b) {
                        break;
                    }
                    i++;
                }
                if (bi != null) {
                    return bi;
                }
                throw new IllegalArgumentException("Invalid id: " + ((int) b));
            }
        }

        @DexIgnore
        public Bi(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Parcelable.Creator<H60> {
        @DexIgnore
        public /* synthetic */ Ci(Qg6 qg6) {
        }

        @DexIgnore
        public final H60 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 7) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new H60(order.get(0), Ai.d.a(order.get(1)), order.getShort(2), order.getShort(4), Bi.d.a(order.get(6)));
            }
            throw new IllegalArgumentException(E.b(E.e("Invalid data size: "), bArr.length, ", require: 7"));
        }

        @DexIgnore
        public H60 b(Parcel parcel) {
            return new H60(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public H60 createFromParcel(Parcel parcel) {
            return new H60(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public H60[] newArray(int i) {
            return new H60[i];
        }
    }

    @DexIgnore
    public H60(byte b, Ai ai, short s, short s2, Bi bi) throws IllegalArgumentException {
        super(Zm1.BIOMETRIC_PROFILE);
        this.c = (byte) b;
        this.d = ai;
        this.e = (short) s;
        this.f = (short) s2;
        this.g = bi;
        d();
    }

    @DexIgnore
    public /* synthetic */ H60(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = parcel.readByte();
        String readString = parcel.readString();
        if (readString != null) {
            Wg6.b(readString, "parcel.readString()!!");
            this.d = Ai.valueOf(readString);
            this.e = (short) ((short) parcel.readInt());
            this.f = (short) ((short) parcel.readInt());
            String readString2 = parcel.readString();
            if (readString2 != null) {
                Wg6.b(readString2, "parcel.readString()!!");
                this.g = Bi.valueOf(readString2);
                d();
                return;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(7).order(ByteOrder.LITTLE_ENDIAN).put(this.c).put(this.d.a()).putShort(this.e).putShort(this.f).put(this.g.a()).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("age", Byte.valueOf(this.c));
            jSONObject.put("gender", Ey1.a(this.d));
            jSONObject.put("height_in_centimeter", Short.valueOf(this.e));
            jSONObject.put("weight_in_kilogram", Short.valueOf(this.f));
            jSONObject.put("wearing_position", Ey1.a(this.g));
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        boolean z = true;
        byte b = this.c;
        if (8 <= b && 110 >= b) {
            short s = this.e;
            if (100 <= s && 250 >= s) {
                short s2 = this.f;
                if (35 > s2 || 250 < s2) {
                    z = false;
                }
                if (!z) {
                    StringBuilder e2 = E.e("weightInKilogram (");
                    e2.append((int) this.f);
                    e2.append(") is out of range ");
                    e2.append("[35, 250]");
                    e2.append(" (in kilogram).");
                    throw new IllegalArgumentException(e2.toString());
                }
                return;
            }
            StringBuilder e3 = E.e("heightInCentimeter (");
            e3.append((int) this.e);
            e3.append(") is out of range ");
            e3.append("[100, 250]");
            e3.append(" (in centimeter.");
            throw new IllegalArgumentException(e3.toString());
        }
        throw new IllegalArgumentException(E.c(E.e("age ("), this.c, ") is out of range [8, ", "110]."));
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(H60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            H60 h60 = (H60) obj;
            if (this.c != h60.c) {
                return false;
            }
            if (this.d != h60.d) {
                return false;
            }
            if (this.e != h60.e) {
                return false;
            }
            if (this.f != h60.f) {
                return false;
            }
            return this.g == h60.g;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.BiometricProfile");
    }

    @DexIgnore
    public final byte getAge() {
        return this.c;
    }

    @DexIgnore
    public final Ai getGender() {
        return this.d;
    }

    @DexIgnore
    public final short getHeightInCentimeter() {
        return this.e;
    }

    @DexIgnore
    public final Bi getWearingPosition() {
        return this.g;
    }

    @DexIgnore
    public final short getWeightInKilogram() {
        return this.f;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        byte b = this.c;
        int hashCode = this.d.hashCode();
        short s = this.e;
        return (((((((b * 31) + hashCode) * 31) + s) * 31) + this.f) * 31) + this.g.hashCode();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeInt(Hy1.n(this.e));
        }
        if (parcel != null) {
            parcel.writeInt(Hy1.n(this.f));
        }
        if (parcel != null) {
            parcel.writeString(this.g.name());
        }
    }
}
