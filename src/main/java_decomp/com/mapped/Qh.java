package com.mapped;

import android.database.Cursor;
import com.fossil.Hw0;
import com.fossil.Kx0;
import com.fossil.Lx0;
import com.fossil.Rw0;
import com.mapped.Ji;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qh extends Ji.Ai {
    @DexIgnore
    public Hw0 b;
    @DexIgnore
    public /* final */ Ai c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public /* final */ int version;

        @DexIgnore
        public Ai(int i) {
            this.version = i;
        }

        @DexIgnore
        public abstract void createAllTables(Lx0 lx0);

        @DexIgnore
        public abstract void dropAllTables(Lx0 lx0);

        @DexIgnore
        public abstract void onCreate(Lx0 lx0);

        @DexIgnore
        public abstract void onOpen(Lx0 lx0);

        @DexIgnore
        public abstract void onPostMigrate(Lx0 lx0);

        @DexIgnore
        public abstract void onPreMigrate(Lx0 lx0);

        @DexIgnore
        public abstract Bi onValidateSchema(Lx0 lx0);

        @DexIgnore
        @Deprecated
        public void validateMigration(Lx0 lx0) {
            throw new UnsupportedOperationException("validateMigration is deprecated");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public /* final */ boolean a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public Bi(boolean z, String str) {
            this.a = z;
            this.b = str;
        }
    }

    @DexIgnore
    public Qh(Hw0 hw0, Ai ai, String str, String str2) {
        super(ai.version);
        this.b = hw0;
        this.c = ai;
        this.d = str;
        this.e = str2;
    }

    @DexIgnore
    public static boolean j(Lx0 lx0) {
        boolean z = false;
        Cursor query = lx0.query("SELECT count(*) FROM sqlite_master WHERE name != 'android_metadata'");
        try {
            if (query.moveToFirst() && query.getInt(0) == 0) {
                z = true;
            }
            return z;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    public static boolean k(Lx0 lx0) {
        boolean z = false;
        Cursor query = lx0.query("SELECT 1 FROM sqlite_master WHERE type = 'table' AND name='room_master_table'");
        try {
            if (query.moveToFirst() && query.getInt(0) != 0) {
                z = true;
            }
            return z;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    @Override // com.mapped.Ji.Ai
    public void b(Lx0 lx0) {
        super.b(lx0);
    }

    @DexIgnore
    @Override // com.mapped.Ji.Ai
    public void d(Lx0 lx0) {
        boolean j = j(lx0);
        this.c.createAllTables(lx0);
        if (!j) {
            Bi onValidateSchema = this.c.onValidateSchema(lx0);
            if (!onValidateSchema.a) {
                throw new IllegalStateException("Pre-packaged database has an invalid schema: " + onValidateSchema.b);
            }
        }
        l(lx0);
        this.c.onCreate(lx0);
    }

    @DexIgnore
    @Override // com.mapped.Ji.Ai
    public void e(Lx0 lx0, int i, int i2) {
        g(lx0, i, i2);
    }

    @DexIgnore
    @Override // com.mapped.Ji.Ai
    public void f(Lx0 lx0) {
        super.f(lx0);
        h(lx0);
        this.c.onOpen(lx0);
        this.b = null;
    }

    @DexIgnore
    @Override // com.mapped.Ji.Ai
    public void g(Lx0 lx0, int i, int i2) {
        boolean z;
        List<Xh> c2;
        Hw0 hw0 = this.b;
        if (hw0 == null || (c2 = hw0.d.c(i, i2)) == null) {
            z = false;
        } else {
            this.c.onPreMigrate(lx0);
            for (Xh xh : c2) {
                xh.migrate(lx0);
            }
            Bi onValidateSchema = this.c.onValidateSchema(lx0);
            if (onValidateSchema.a) {
                this.c.onPostMigrate(lx0);
                l(lx0);
                z = true;
            } else {
                throw new IllegalStateException("Migration didn't properly handle: " + onValidateSchema.b);
            }
        }
        if (!z) {
            Hw0 hw02 = this.b;
            if (hw02 == null || hw02.a(i, i2)) {
                throw new IllegalStateException("A migration from " + i + " to " + i2 + " was required but not found. Please provide the necessary Migration path via RoomDatabase.Builder.addMigration(Migration ...) or allow for destructive migrations via one of the RoomDatabase.Builder.fallbackToDestructiveMigration* methods.");
            }
            this.c.dropAllTables(lx0);
            this.c.createAllTables(lx0);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void h(Lx0 lx0) {
        if (k(lx0)) {
            String str = null;
            Cursor query = lx0.query(new Kx0("SELECT identity_hash FROM room_master_table WHERE id = 42 LIMIT 1"));
            try {
                if (query.moveToFirst()) {
                    str = query.getString(0);
                }
                query.close();
                if (!this.d.equals(str) && !this.e.equals(str)) {
                    throw new IllegalStateException("Room cannot verify the data integrity. Looks like you've changed schema but forgot to update the version number. You can simply fix this by increasing the version number.");
                }
            } catch (Throwable th) {
                query.close();
                throw th;
            }
        } else {
            Bi onValidateSchema = this.c.onValidateSchema(lx0);
            if (onValidateSchema.a) {
                this.c.onPostMigrate(lx0);
                l(lx0);
                return;
            }
            throw new IllegalStateException("Pre-packaged database has an invalid schema: " + onValidateSchema.b);
        }
    }

    @DexIgnore
    public final void i(Lx0 lx0) {
        lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
    }

    @DexIgnore
    public final void l(Lx0 lx0) {
        i(lx0);
        lx0.execSQL(Rw0.a(this.d));
    }
}
