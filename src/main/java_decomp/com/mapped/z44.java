package com.mapped;

import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Z44 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ PresetRepository.Anon12 b;
    @DexIgnore
    public /* final */ /* synthetic */ List c;

    @DexIgnore
    public /* synthetic */ Z44(PresetRepository.Anon12 anon12, List list) {
        this.b = anon12;
        this.c = list;
    }

    @DexIgnore
    public final void run() {
        this.b.a(this.c);
    }
}
