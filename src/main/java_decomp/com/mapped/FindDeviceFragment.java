package com.mapped;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Ae3;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.Hr7;
import com.fossil.J65;
import com.fossil.Le3;
import com.fossil.Oa1;
import com.fossil.P47;
import com.fossil.Pb3;
import com.fossil.Qb3;
import com.fossil.Qv5;
import com.fossil.S37;
import com.fossil.Sb3;
import com.fossil.U17;
import com.fossil.Um5;
import com.fossil.V17;
import com.fossil.Vk5;
import com.fossil.Wa1;
import com.fossil.Xb3;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.mapped.AlertDialogFragment;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FindDeviceFragment extends Qv5 implements V17, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ Ai w; // = new Ai(null);
    @DexIgnore
    public U17 h;
    @DexIgnore
    public G37<J65> i;
    @DexIgnore
    public Qb3 j;
    @DexIgnore
    public View k;
    @DexIgnore
    public Bitmap l;
    @DexIgnore
    public Wa1 m;
    @DexIgnore
    public /* final */ String s; // = ThemeManager.l.a().d("primaryText");
    @DexIgnore
    public /* final */ String t; // = ThemeManager.l.a().d("success");
    @DexIgnore
    public /* final */ String u; // = ThemeManager.l.a().d("secondaryText");
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final FindDeviceFragment a() {
            return new FindDeviceFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ FindDeviceFragment b;

        @DexIgnore
        public Bi(FindDeviceFragment findDeviceFragment) {
            this.b = findDeviceFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.requireActivity().finish();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Sb3 {
        @DexIgnore
        public /* final */ /* synthetic */ FindDeviceFragment b;

        @DexIgnore
        public Ci(FindDeviceFragment findDeviceFragment) {
            this.b = findDeviceFragment;
        }

        @DexIgnore
        @Override // com.fossil.Sb3
        public final void onMapReady(Qb3 qb3) {
            this.b.j = qb3;
            Wg6.b(qb3, "googleMap");
            Xb3 l = qb3.l();
            Wg6.b(l, "googleMap.uiSettings");
            l.j(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ FindDeviceFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ J65 b;

        @DexIgnore
        public Di(FindDeviceFragment findDeviceFragment, J65 j65) {
            this.a = findDeviceFragment;
            this.b = j65;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            P47.l(this.b.C);
            ConstraintLayout constraintLayout = this.b.r;
            Wg6.b(constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(z ? 0 : 4);
            FindDeviceFragment.K6(this.a).n(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ J65 a;
        @DexIgnore
        public /* final */ /* synthetic */ FindDeviceFragment b;

        @DexIgnore
        public Ei(J65 j65, FindDeviceFragment findDeviceFragment, WatchSettingViewModel.Di di) {
            this.a = j65;
            this.b = findDeviceFragment;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            Wg6.c(str, "serial");
            Wg6.c(str2, "filePath");
            FindDeviceFragment.L6(this.b).t(str2).F0(this.a.A);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ FindDeviceFragment b;

        @DexIgnore
        public Fi(View view, String str, FindDeviceFragment findDeviceFragment, WatchSettingViewModel.Di di) {
            this.a = view;
            this.b = findDeviceFragment;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            Wg6.c(str, "serial");
            Wg6.c(str2, "filePath");
            if (this.b.isActive()) {
                View findViewById = this.a.findViewById(2131362616);
                if (findViewById != null) {
                    ((ImageView) findViewById).setImageBitmap(Vk5.c(str2));
                    int dimensionPixelSize = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165442);
                    this.a.measure(View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824), View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824));
                    View view = this.a;
                    view.layout(0, 0, view.getMeasuredWidth(), this.a.getMeasuredHeight());
                    this.b.l = Vk5.h(this.a);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type android.widget.ImageView");
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ U17 K6(FindDeviceFragment findDeviceFragment) {
        U17 u17 = findDeviceFragment.h;
        if (u17 != null) {
            return u17;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Wa1 L6(FindDeviceFragment findDeviceFragment) {
        Wa1 wa1 = findDeviceFragment.m;
        if (wa1 != null) {
            return wa1;
        }
        Wg6.n("mRequestManager");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.V17
    public void E1(Double d, Double d2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showLocation: latitude = " + d + ", longitude = " + d2);
        J65 O6 = O6();
        if (O6 != null) {
            FlexibleTextView flexibleTextView = O6.t;
            Wg6.b(flexibleTextView, "binding.ftvError");
            flexibleTextView.setVisibility(8);
            if (d != null && d2 != null) {
                LatLng latLng = new LatLng(d.doubleValue(), d2.doubleValue());
                Qb3 qb3 = this.j;
                if (qb3 != null) {
                    qb3.o(Pb3.d(latLng, 13.0f));
                    qb3.e(Pb3.j(18.0f));
                    qb3.g();
                    Bitmap bitmap = this.l;
                    if (bitmap != null) {
                        Le3 le3 = new Le3();
                        le3.B0(Um5.c(PortfolioApp.get.instance(), 2131887151));
                        le3.u0(Ae3.d(bitmap));
                        le3.z0(latLng);
                        qb3.b(le3);
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.V17
    public void I2(long j2) {
        FlexibleTextView flexibleTextView;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showTime: time = " + j2);
        G37<J65> g37 = this.i;
        if (g37 != null) {
            J65 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.x) != null) {
                flexibleTextView.setText(DateUtils.getRelativeTimeSpanString(j2));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.V17
    public void L2(String str) {
        FlexibleTextView flexibleTextView;
        Wg6.c(str, "address");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showAddress: address = " + str);
        G37<J65> g37 = this.i;
        if (g37 != null) {
            J65 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.u) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(U17 u17) {
        P6(u17);
    }

    @DexIgnore
    public final J65 O6() {
        G37<J65> g37 = this.i;
        if (g37 != null) {
            return g37.a();
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void P6(U17 u17) {
        Wg6.c(u17, "presenter");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "setPresenter: presenter = " + u17);
        this.h = u17;
    }

    @DexIgnore
    @Override // com.fossil.V17
    public void W0(int i2) {
        G37<J65> g37 = this.i;
        if (g37 != null) {
            J65 a2 = g37.a();
            if (a2 != null) {
                float g = DeviceHelper.o.g(i2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FindDeviceFragment", "showProximity - rssi: " + i2 + ", distanceInFt: " + g);
                if (g >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && g <= 5.0f) {
                    FlexibleTextView flexibleTextView = a2.E;
                    Wg6.b(flexibleTextView, "binding.tvLocationStatus");
                    flexibleTextView.setText(Um5.c(getContext(), 2131887155));
                    FlexibleTextView flexibleTextView2 = a2.E;
                    Wg6.b(flexibleTextView2, "binding.tvLocationStatus");
                    flexibleTextView2.setEnabled(true);
                    String str = this.t;
                    if (str != null) {
                        a2.E.setTextColor(Color.parseColor(str));
                        a2.E.setBackgroundColor(Color.parseColor(str));
                        FlexibleTextView flexibleTextView3 = a2.E;
                        Wg6.b(flexibleTextView3, "binding.tvLocationStatus");
                        Drawable background = flexibleTextView3.getBackground();
                        Wg6.b(background, "binding.tvLocationStatus.background");
                        background.setAlpha(128);
                    }
                } else if (g >= 5.0f && g <= 10.0f) {
                    FlexibleTextView flexibleTextView4 = a2.E;
                    Wg6.b(flexibleTextView4, "binding.tvLocationStatus");
                    flexibleTextView4.setText(Um5.c(getContext(), 2131887153));
                    FlexibleTextView flexibleTextView5 = a2.E;
                    Wg6.b(flexibleTextView5, "binding.tvLocationStatus");
                    flexibleTextView5.setEnabled(true);
                    String str2 = this.t;
                    if (str2 != null) {
                        a2.E.setTextColor(Color.parseColor(str2));
                        a2.E.setBackgroundColor(Color.parseColor(str2));
                        FlexibleTextView flexibleTextView6 = a2.E;
                        Wg6.b(flexibleTextView6, "binding.tvLocationStatus");
                        Drawable background2 = flexibleTextView6.getBackground();
                        Wg6.b(background2, "binding.tvLocationStatus.background");
                        background2.setAlpha(128);
                    }
                } else if (g < 10.0f || g > 30.0f) {
                    FlexibleTextView flexibleTextView7 = a2.E;
                    Wg6.b(flexibleTextView7, "binding.tvLocationStatus");
                    flexibleTextView7.setText(Um5.c(getContext(), 2131887152));
                    FlexibleTextView flexibleTextView8 = a2.E;
                    Wg6.b(flexibleTextView8, "binding.tvLocationStatus");
                    flexibleTextView8.setEnabled(false);
                    String str3 = this.u;
                    if (str3 != null) {
                        a2.E.setTextColor(Color.parseColor(str3));
                        a2.E.setBackgroundColor(Color.parseColor(str3));
                        FlexibleTextView flexibleTextView9 = a2.E;
                        Wg6.b(flexibleTextView9, "binding.tvLocationStatus");
                        Drawable background3 = flexibleTextView9.getBackground();
                        Wg6.b(background3, "binding.tvLocationStatus.background");
                        background3.setAlpha(128);
                    }
                } else {
                    FlexibleTextView flexibleTextView10 = a2.E;
                    Wg6.b(flexibleTextView10, "binding.tvLocationStatus");
                    flexibleTextView10.setText(Um5.c(getContext(), 2131887154));
                    FlexibleTextView flexibleTextView11 = a2.E;
                    Wg6.b(flexibleTextView11, "binding.tvLocationStatus");
                    flexibleTextView11.setEnabled(true);
                    String str4 = this.t;
                    if (str4 != null) {
                        a2.E.setTextColor(Color.parseColor(str4));
                        a2.E.setBackgroundColor(Color.parseColor(str4));
                        FlexibleTextView flexibleTextView12 = a2.E;
                        Wg6.b(flexibleTextView12, "binding.tvLocationStatus");
                        Drawable background4 = flexibleTextView12.getBackground();
                        Wg6.b(background4, "binding.tvLocationStatus.background");
                        background4.setAlpha(128);
                    }
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.V17
    public void f3(boolean z, boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDeviceFragment", "showLocationEnable: enable = " + z + ", needWarning = " + z2);
        G37<J65> g37 = this.i;
        if (g37 != null) {
            J65 a2 = g37.a();
            if (a2 != null) {
                FlexibleSwitchCompat flexibleSwitchCompat = a2.C;
                Wg6.b(flexibleSwitchCompat, "binding.swLocate");
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.V17
    public void j1() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showLocationPermissionError");
        J65 O6 = O6();
        if (O6 != null) {
            FlexibleSwitchCompat flexibleSwitchCompat = O6.C;
            Wg6.b(flexibleSwitchCompat, "binding.swLocate");
            flexibleSwitchCompat.setChecked(false);
            ConstraintLayout constraintLayout = O6.r;
            Wg6.b(constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
            U17 u17 = this.h;
            if (u17 != null) {
                u17.o(false);
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.V17
    public void o(int i2, String str) {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showErrorDialog");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
        J65 O6 = O6();
        if (O6 != null) {
            FlexibleSwitchCompat flexibleSwitchCompat = O6.C;
            Wg6.b(flexibleSwitchCompat, "binding.swLocate");
            flexibleSwitchCompat.setChecked(false);
            ConstraintLayout constraintLayout = O6.r;
            Wg6.b(constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
            U17 u17 = this.h;
            if (u17 != null) {
                u17.o(false);
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Wa1 v2 = Oa1.v(this);
        Wg6.b(v2, "Glide.with(this)");
        this.m = v2;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    @SuppressLint({"InflateParams"})
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        String d;
        Wg6.c(layoutInflater, "inflater");
        J65 j65 = (J65) Aq0.f(layoutInflater, 2131558555, viewGroup, false, A6());
        View inflate = layoutInflater.inflate(2131558736, (ViewGroup) null);
        this.k = inflate;
        if (inflate != null) {
            int dimensionPixelSize = PortfolioApp.get.instance().getResources().getDimensionPixelSize(2131165442);
            inflate.measure(View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824), View.MeasureSpec.makeMeasureSpec(dimensionPixelSize, 1073741824));
            inflate.layout(0, 0, inflate.getMeasuredWidth(), inflate.getMeasuredHeight());
            this.l = Vk5.h(inflate);
        }
        j65.z.setOnClickListener(new Bi(this));
        Fragment Y = getChildFragmentManager().Y(2131362251);
        if (Y != null) {
            ((SupportMapFragment) Y).v6(new Ci(this));
            j65.C.setOnCheckedChangeListener(new Di(this, j65));
            ConstraintLayout constraintLayout = j65.q;
            if (!(constraintLayout == null || (d = ThemeManager.l.a().d("nonBrandSurface")) == null)) {
                constraintLayout.setBackgroundColor(Color.parseColor(d));
            }
            this.i = new G37<>(this, j65);
            Wg6.b(j65, "binding");
            return j65.n();
        }
        throw new Rc6("null cannot be cast to non-null type com.google.android.gms.maps.SupportMapFragment");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "onPause");
        super.onPause();
        U17 u17 = this.h;
        if (u17 != null) {
            u17.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "onResume");
        super.onResume();
        U17 u17 = this.h;
        if (u17 != null) {
            u17.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.V17
    public void r6() {
        FLogger.INSTANCE.getLocal().d("FindDeviceFragment", "showLocationNotFound");
        J65 O6 = O6();
        if (O6 != null) {
            FlexibleSwitchCompat flexibleSwitchCompat = O6.C;
            Wg6.b(flexibleSwitchCompat, "binding.swLocate");
            flexibleSwitchCompat.setEnabled(false);
            FlexibleSwitchCompat flexibleSwitchCompat2 = O6.C;
            Wg6.b(flexibleSwitchCompat2, "binding.swLocate");
            flexibleSwitchCompat2.setChecked(false);
            String str = this.u;
            if (str != null) {
                O6.y.setTextColor(Color.parseColor(str));
            }
            ConstraintLayout constraintLayout = O6.r;
            Wg6.b(constraintLayout, "binding.clLocation");
            constraintLayout.setVisibility(4);
            FlexibleTextView flexibleTextView = O6.t;
            Wg6.b(flexibleTextView, "binding.ftvError");
            flexibleTextView.setVisibility(0);
            U17 u17 = this.h;
            if (u17 != null) {
                u17.o(false);
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.V17
    public void w3(WatchSettingViewModel.Di di) {
        Wg6.c(di, "watchSetting");
        if (isActive()) {
            G37<J65> g37 = this.i;
            if (g37 != null) {
                J65 a2 = g37.a();
                if (a2 != null) {
                    String deviceId = di.a().getDeviceId();
                    FlexibleTextView flexibleTextView = a2.D;
                    Wg6.b(flexibleTextView, "it.tvDeviceName");
                    flexibleTextView.setText(di.b());
                    a2.D.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, (Drawable) null);
                    FlexibleTextView flexibleTextView2 = a2.t;
                    Wg6.b(flexibleTextView2, "it.ftvError");
                    Hr7 hr7 = Hr7.a;
                    String c = Um5.c(PortfolioApp.get.instance(), 2131887158);
                    Wg6.b(c, "LanguageHelper.getString\u2026chNameHasntBeenConnected)");
                    String format = String.format(c, Arrays.copyOf(new Object[]{di.b()}, 1));
                    Wg6.b(format, "java.lang.String.format(format, *args)");
                    flexibleTextView2.setText(format);
                    boolean d = di.d();
                    if (d) {
                        if (di.e()) {
                            String str = this.t;
                            if (str != null) {
                                a2.D.setTextColor(Color.parseColor(str));
                            }
                            FlexibleTextView flexibleTextView3 = a2.D;
                            Wg6.b(flexibleTextView3, "it.tvDeviceName");
                            flexibleTextView3.setAlpha(1.0f);
                            a2.D.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, (Drawable) null, W6.f(PortfolioApp.get.instance(), 2131231075), (Drawable) null);
                        } else {
                            String str2 = this.s;
                            if (str2 != null) {
                                a2.D.setTextColor(Color.parseColor(str2));
                            }
                            FlexibleTextView flexibleTextView4 = a2.D;
                            Wg6.b(flexibleTextView4, "it.tvDeviceName");
                            flexibleTextView4.setAlpha(0.4f);
                            FlexibleTextView flexibleTextView5 = a2.E;
                            Wg6.b(flexibleTextView5, "it.tvLocationStatus");
                            flexibleTextView5.setText(Um5.c(getContext(), 2131887152));
                            FlexibleTextView flexibleTextView6 = a2.E;
                            Wg6.b(flexibleTextView6, "it.tvLocationStatus");
                            flexibleTextView6.setEnabled(false);
                            String str3 = this.u;
                            if (str3 != null) {
                                a2.E.setTextColor(Color.parseColor(str3));
                                a2.E.setBackgroundColor(Color.parseColor(str3));
                                FlexibleTextView flexibleTextView7 = a2.E;
                                Wg6.b(flexibleTextView7, "it.tvLocationStatus");
                                Drawable background = flexibleTextView7.getBackground();
                                Wg6.b(background, "it.tvLocationStatus.background");
                                background.setAlpha(128);
                            }
                        }
                    } else if (!d) {
                        String str4 = this.s;
                        if (str4 != null) {
                            a2.D.setTextColor(Color.parseColor(str4));
                        }
                        FlexibleTextView flexibleTextView8 = a2.D;
                        Wg6.b(flexibleTextView8, "it.tvDeviceName");
                        flexibleTextView8.setAlpha(0.4f);
                        FlexibleTextView flexibleTextView9 = a2.E;
                        Wg6.b(flexibleTextView9, "it.tvLocationStatus");
                        flexibleTextView9.setText(Um5.c(getContext(), 2131887152));
                        FlexibleTextView flexibleTextView10 = a2.E;
                        Wg6.b(flexibleTextView10, "it.tvLocationStatus");
                        flexibleTextView10.setEnabled(false);
                        String str5 = this.u;
                        if (str5 != null) {
                            a2.E.setTextColor(Color.parseColor(str5));
                            a2.E.setBackgroundColor(Color.parseColor(str5));
                            FlexibleTextView flexibleTextView11 = a2.E;
                            Wg6.b(flexibleTextView11, "it.tvLocationStatus");
                            Drawable background2 = flexibleTextView11.getBackground();
                            Wg6.b(background2, "it.tvLocationStatus.background");
                            background2.setAlpha(128);
                        }
                    }
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(deviceId).setSerialPrefix(DeviceHelper.o.m(deviceId)).setType(Constants.DeviceType.TYPE_LARGE);
                    ImageView imageView = a2.A;
                    Wg6.b(imageView, "it.ivDevice");
                    type.setPlaceHolder(imageView, DeviceHelper.o.i(deviceId, DeviceHelper.Bi.SMALL)).setImageCallback(new Ei(a2, this, di)).download();
                    View view = this.k;
                    if (view != null) {
                        new CloudImageHelper().with().setSerialNumber(deviceId).setSerialPrefix(DeviceHelper.o.m(deviceId)).setType(Constants.DeviceType.TYPE_LARGE).setImageCallback(new Fi(view, deviceId, this, di)).download();
                        return;
                    }
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }
}
