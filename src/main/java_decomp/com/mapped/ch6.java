package com.mapped;

import com.fossil.Ds7;
import com.fossil.Er7;
import com.fossil.Yq7;
import com.mapped.Ni6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ch6 extends Yq7 implements Ni6 {
    @DexIgnore
    public Ch6() {
    }

    @DexIgnore
    public Ch6(Object obj) {
        super(obj);
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public Ds7 computeReflected() {
        Er7.g(this);
        return this;
    }

    @DexIgnore
    public abstract /* synthetic */ R get(T t);

    @DexIgnore
    @Override // com.mapped.Ni6
    public Object getDelegate(Object obj) {
        return ((Ni6) getReflected()).getDelegate(obj);
    }

    @DexIgnore
    @Override // com.fossil.Yq7, com.mapped.Ni6
    public Ni6.Ai getGetter() {
        return ((Ni6) getReflected()).getGetter();
    }

    @DexIgnore
    @Override // com.mapped.Hg6
    public Object invoke(Object obj) {
        return get(obj);
    }
}
