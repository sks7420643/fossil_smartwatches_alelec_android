package com.mapped;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.Cp5;
import com.fossil.Dl7;
import com.fossil.Dp5;
import com.fossil.El7;
import com.fossil.Ie5;
import com.fossil.Oo5;
import com.fossil.Rb7;
import com.fossil.S87;
import com.fossil.Ub7;
import com.fossil.Um5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.watchface.WatchFacePreviewView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetAdapter extends RecyclerView.g<Ai> {
    @DexIgnore
    public /* final */ List<Dp5> a; // = new ArrayList();
    @DexIgnore
    public /* final */ Map<String, List<S87>> b; // = new LinkedHashMap();
    @DexIgnore
    public /* final */ Bitmap c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ Bi e;
    @DexIgnore
    public /* final */ Cp5 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public Dp5 b;
        @DexIgnore
        public /* final */ Ie5 c;
        @DexIgnore
        public /* final */ Cp5 d;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPresetAdapter e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(DianaPresetAdapter dianaPresetAdapter, Ie5 ie5, Cp5 cp5) {
            super(ie5.n());
            Wg6.c(ie5, "binding");
            Wg6.c(cp5, "listener");
            this.e = dianaPresetAdapter;
            this.c = ie5;
            this.d = cp5;
            ie5.K.setOnClickListener(this);
            ie5.J.setOnClickListener(this);
            ie5.I.setOnClickListener(this);
            ie5.u.setOnClickListener(this);
            ie5.F.setOnClickListener(this);
            ie5.q.setOnClickListener(this);
            ie5.E.setOnClickListener(this);
            ie5.v.setOnClickListener(this);
        }

        @DexIgnore
        public final void a(Dp5 dp5, List<? extends S87> list) {
            Bitmap bitmap;
            Rb7 a2;
            Wg6.c(dp5, "uiDianaPreset");
            Wg6.c(list, "elementConfigs");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaPresetAdapter", "setData presetName=" + dp5.d());
            this.b = dp5;
            Ie5 ie5 = this.c;
            if (!TextUtils.isEmpty(dp5.e()) && !dp5.h()) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DianaPresetAdapter", "preview url=" + dp5.e());
                WatchFacePreviewView watchFacePreviewView = ie5.L;
                String e2 = dp5.e();
                if (e2 != null) {
                    watchFacePreviewView.P(e2);
                    FlexibleTextView flexibleTextView = ie5.w;
                    Wg6.b(flexibleTextView, "ftvFailed");
                    flexibleTextView.setVisibility(8);
                    FlexibleButton flexibleButton = ie5.v;
                    Wg6.b(flexibleButton, "fbReloadWf");
                    flexibleButton.setVisibility(8);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else if (dp5.f() != null) {
                Ub7 f = dp5.f();
                if (f == null || (a2 = f.a()) == null || (bitmap = a2.b()) == null) {
                    bitmap = this.e.c;
                }
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DianaPresetAdapter", "preview elementConfigs=" + list + ", backgroundBitmap=" + bitmap);
                ie5.L.Q(list, bitmap);
                FlexibleTextView flexibleTextView2 = ie5.w;
                Wg6.b(flexibleTextView2, "ftvFailed");
                flexibleTextView2.setVisibility(8);
                FlexibleButton flexibleButton2 = ie5.v;
                Wg6.b(flexibleButton2, "fbReloadWf");
                flexibleButton2.setVisibility(8);
            } else {
                FLogger.INSTANCE.getLocal().d("DianaPresetAdapter", "preview empty");
                ie5.L.U();
                FlexibleTextView flexibleTextView3 = ie5.w;
                Wg6.b(flexibleTextView3, "ftvFailed");
                flexibleTextView3.setVisibility(0);
                FlexibleButton flexibleButton3 = ie5.v;
                Wg6.b(flexibleButton3, "fbReloadWf");
                flexibleButton3.setVisibility(0);
                FlexibleTextView flexibleTextView4 = ie5.w;
                Wg6.b(flexibleTextView4, "ftvFailed");
                FlexibleTextView flexibleTextView5 = ie5.w;
                Wg6.b(flexibleTextView5, "ftvFailed");
                flexibleTextView4.setText(Um5.c(flexibleTextView5.getContext(), 2131886571));
            }
            FlexibleTextView flexibleTextView6 = ie5.F;
            Wg6.b(flexibleTextView6, "tvPresetName");
            flexibleTextView6.setText(dp5.d());
            List<Oo5> a3 = dp5.a();
            if (a3 != null) {
                for (Oo5 oo5 : a3) {
                    String b2 = oo5.b();
                    int hashCode = b2.hashCode();
                    if (hashCode != -1383228885) {
                        if (hashCode != -1074341483) {
                            if (hashCode == 115029 && b2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                ie5.K.b0(oo5.a());
                                ie5.K.T();
                            }
                        } else if (b2.equals("middle")) {
                            ie5.J.b0(oo5.a());
                            ie5.J.T();
                        }
                    } else if (b2.equals("bottom")) {
                        ie5.I.b0(oo5.a());
                        ie5.I.T();
                    }
                }
            }
            if (dp5.g()) {
                FlexibleButton flexibleButton4 = ie5.q;
                Wg6.b(flexibleButton4, "btnSetToWatch");
                flexibleButton4.setText(Um5.c(PortfolioApp.get.instance(), 2131886550));
                FlexibleButton flexibleButton5 = ie5.q;
                Wg6.b(flexibleButton5, "btnSetToWatch");
                flexibleButton5.setClickable(false);
                ie5.q.d("flexible_button_right_applied");
                if (!TextUtils.isEmpty(this.e.d)) {
                    int parseColor = Color.parseColor(this.e.d);
                    Drawable drawable = PortfolioApp.get.instance().getDrawable(2131231055);
                    if (drawable != null) {
                        drawable.setTint(parseColor);
                        ie5.q.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
            } else {
                FlexibleButton flexibleButton6 = ie5.q;
                Wg6.b(flexibleButton6, "btnSetToWatch");
                flexibleButton6.setText(Um5.c(PortfolioApp.get.instance(), 2131886542));
                FlexibleButton flexibleButton7 = ie5.q;
                Wg6.b(flexibleButton7, "btnSetToWatch");
                flexibleButton7.setClickable(true);
                ie5.q.d("flexible_button_right_apply");
                ie5.q.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
            FlexibleTextView flexibleTextView7 = ie5.E;
            Wg6.b(flexibleTextView7, "tvLeft");
            flexibleTextView7.setVisibility(this.e.j().size() > 1 ? 0 : 4);
        }

        @DexIgnore
        public void onClick(View view) {
            Dp5 dp5;
            if (getAdapterPosition() != -1 && view != null && (dp5 = this.b) != null) {
                switch (view.getId()) {
                    case 2131361980:
                        this.d.f(dp5.c());
                        return;
                    case 2131362254:
                        Cp5 cp5 = this.d;
                        String c2 = dp5.c();
                        Dp5 dp52 = this.b;
                        cp5.b(c2, dp52 != null ? dp52.h() : true, view);
                        return;
                    case 2131362283:
                        Ie5 ie5 = this.c;
                        FlexibleTextView flexibleTextView = ie5.w;
                        Wg6.b(flexibleTextView, "ftvFailed");
                        flexibleTextView.setText(Um5.c(view.getContext(), 2131886552));
                        FlexibleButton flexibleButton = ie5.v;
                        Wg6.b(flexibleButton, "fbReloadWf");
                        flexibleButton.setVisibility(8);
                        this.d.e(dp5.c(), dp5.b());
                        return;
                    case 2131362528:
                        this.d.f(dp5.c());
                        return;
                    case 2131363348:
                        this.d.a();
                        return;
                    case 2131363376:
                        this.d.N(dp5.d(), dp5.c());
                        return;
                    case 2131363529:
                        this.d.d(dp5.c(), "bottom");
                        return;
                    case 2131363530:
                        this.d.d(dp5.c(), "middle");
                        return;
                    case 2131363531:
                        this.d.d(dp5.c(), ViewHierarchy.DIMENSION_TOP_KEY);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends RecyclerView.AdapterDataObserver {
        @DexIgnore
        public /* final */ /* synthetic */ DianaPresetAdapter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(DianaPresetAdapter dianaPresetAdapter) {
            this.a = dianaPresetAdapter;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void a() {
            this.a.f.c();
        }
    }

    @DexIgnore
    public DianaPresetAdapter(Context context, Cp5 cp5) {
        Wg6.c(context, "context");
        Wg6.c(cp5, "listener");
        this.f = cp5;
        this.c = BitmapFactory.decodeResource(context.getResources(), 2131231268);
        this.d = ThemeManager.l.a().d("onPrimaryButton");
        this.e = new Bi(this);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public final List<Dp5> j() {
        return this.a;
    }

    @DexIgnore
    public void k(Ai ai, int i) {
        Wg6.c(ai, "holder");
        Dp5 dp5 = this.a.get(i);
        List<S87> list = this.b.get(dp5.c());
        if (list != null) {
            ai.a(dp5, list);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Ai l(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Ie5 z = Ie5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemDianaPresetLayoutBin\u2026tInflater, parent, false)");
        return new Ai(this, z, this.f);
    }

    @DexIgnore
    public final void m(List<Dp5> list, Map<String, ? extends List<? extends S87>> map) {
        Wg6.c(list, "newUiPresets");
        Wg6.c(map, "newConfigPerPreset");
        this.a.clear();
        this.b.clear();
        this.a.addAll(list);
        this.b.putAll(map);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void n(Dp5 dp5, List<? extends S87> list) {
        boolean z = false;
        Wg6.c(dp5, "uiDianaPreset");
        Iterator<Dp5> it = this.a.iterator();
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            } else if (Wg6.a(it.next().c(), dp5.c())) {
                break;
            } else {
                i++;
            }
        }
        if (i != -1) {
            if (list == null || list.isEmpty()) {
                z = true;
            }
            if (!z) {
                this.b.put(dp5.c(), list);
            }
            this.a.set(i, dp5);
            notifyItemChanged(i);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        Wg6.c(recyclerView, "recyclerView");
        try {
            Dl7.Ai ai = Dl7.Companion;
            registerAdapterDataObserver(this.e);
            Dl7.constructor-impl(Cd6.a);
        } catch (Throwable th) {
            Dl7.Ai ai2 = Dl7.Companion;
            Dl7.constructor-impl(El7.a(th));
        }
        super.onAttachedToRecyclerView(recyclerView);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        k(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return l(viewGroup, i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        Wg6.c(recyclerView, "recyclerView");
        try {
            Dl7.Ai ai = Dl7.Companion;
            unregisterAdapterDataObserver(this.e);
            Dl7.constructor-impl(Cd6.a);
        } catch (Throwable th) {
            Dl7.Ai ai2 = Dl7.Companion;
            Dl7.constructor-impl(El7.a(th));
        }
        super.onDetachedFromRecyclerView(recyclerView);
    }
}
