package com.mapped;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatImageView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Aq0;
import com.fossil.Dl5;
import com.fossil.Fg6;
import com.fossil.G37;
import com.fossil.Gg6;
import com.fossil.Hr7;
import com.fossil.Jl5;
import com.fossil.Mv5;
import com.fossil.Oi5;
import com.fossil.Um5;
import com.fossil.Vd5;
import com.fossil.Z25;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CaloriesOverviewDayFragment extends BaseFragment implements Gg6 {
    @DexIgnore
    public G37<Z25> g;
    @DexIgnore
    public Fg6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements View.OnClickListener {
        @DexIgnore
        public static /* final */ Ai b; // = new Ai();

        @DexIgnore
        public final void onClick(View view) {
            CaloriesDetailActivity.a aVar = CaloriesDetailActivity.C;
            Date date = new Date();
            Wg6.b(view, "it");
            Context context = view.getContext();
            Wg6.b(context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "CaloriesOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Gg6
    public void G(Mv5 mv5, ArrayList<String> arrayList) {
        Z25 a2;
        OverviewDayChart overviewDayChart;
        Wg6.c(mv5, "baseModel");
        Wg6.c(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesOverviewDayFragment", "showDayDetails - baseModel=" + mv5);
        G37<Z25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.q) != null) {
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(Mv5.a.b(cVar.d()));
            if (!arrayList.isEmpty()) {
                BarChart.H(overviewDayChart, arrayList, false, 2, null);
            } else {
                BarChart.H(overviewDayChart, Jl5.b.f(), false, 2, null);
            }
            overviewDayChart.r(mv5);
        }
    }

    @DexIgnore
    public final void K6(Vd5 vd5, WorkoutSession workoutSession) {
        AppCompatImageView appCompatImageView;
        if (vd5 != null) {
            View n = vd5.n();
            Wg6.b(n, "binding.root");
            Context context = n.getContext();
            Oi5.Ai ai = Oi5.Companion;
            Lc6<Integer, Integer> a2 = ai.a(ai.d(workoutSession.getEditedType(), workoutSession.getEditedMode()));
            String c = Um5.c(context, a2.getSecond().intValue());
            vd5.t.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = vd5.r;
            Wg6.b(flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(c);
            FlexibleTextView flexibleTextView2 = vd5.s;
            Wg6.b(flexibleTextView2, "it.ftvWorkoutValue");
            Hr7 hr7 = Hr7.a;
            String c2 = Um5.c(context, 2131886628);
            Wg6.b(c2, "LanguageHelper.getString\u2026esToday_Text__NumberCals)");
            Float totalCalorie = workoutSession.getTotalCalorie();
            String format = String.format(c2, Arrays.copyOf(new Object[]{Dl5.c(totalCalorie != null ? totalCalorie.floatValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1)}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            flexibleTextView2.setText(format);
            FlexibleTextView flexibleTextView3 = vd5.q;
            Wg6.b(flexibleTextView3, "it.ftvWorkoutTime");
            flexibleTextView3.setText(TimeUtils.o(workoutSession.getEditedStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
            DeviceHelper.Ai ai2 = DeviceHelper.o;
            Fg6 fg6 = this.h;
            String d = ai2.w(fg6 != null ? fg6.n() : null) ? ThemeManager.l.a().d("dianaActiveCaloriesTab") : ThemeManager.l.a().d("hybridActiveCaloriesTab");
            if (d != null && (appCompatImageView = vd5.t) != null) {
                appCompatImageView.setColorFilter(Color.parseColor(d));
            }
        }
    }

    @DexIgnore
    public final void L6() {
        Z25 a2;
        OverviewDayChart overviewDayChart;
        G37<Z25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.q) != null) {
            DeviceHelper.Ai ai = DeviceHelper.o;
            Fg6 fg6 = this.h;
            if (ai.w(fg6 != null ? fg6.n() : null)) {
                overviewDayChart.D("dianaActiveCaloriesTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.D("hybridActiveCaloriesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Fg6 fg6) {
        M6(fg6);
    }

    @DexIgnore
    public void M6(Fg6 fg6) {
        Wg6.c(fg6, "presenter");
        this.h = fg6;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Z25 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onCreateView");
        Z25 z25 = (Z25) Aq0.f(layoutInflater, 2131558509, viewGroup, false, A6());
        z25.r.setOnClickListener(Ai.b);
        this.g = new G37<>(this, z25);
        L6();
        G37<Z25> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onResume");
        L6();
        Fg6 fg6 = this.h;
        if (fg6 != null) {
            fg6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onStop");
        Fg6 fg6 = this.h;
        if (fg6 != null) {
            fg6.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.Gg6
    public void v(boolean z, List<WorkoutSession> list) {
        Z25 a2;
        View n;
        View n2;
        Wg6.c(list, "workoutSessions");
        G37<Z25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null) {
            if (z) {
                LinearLayout linearLayout = a2.u;
                Wg6.b(linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                K6(a2.s, list.get(0));
                if (size == 1) {
                    Vd5 vd5 = a2.t;
                    if (vd5 != null && (n2 = vd5.n()) != null) {
                        n2.setVisibility(8);
                        return;
                    }
                    return;
                }
                Vd5 vd52 = a2.t;
                if (!(vd52 == null || (n = vd52.n()) == null)) {
                    n.setVisibility(0);
                }
                K6(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            Wg6.b(linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
