package com.mapped;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.He6;
import com.fossil.Ie6;
import com.fossil.W15;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActiveTimeOverviewMonthFragment extends BaseFragment implements Ie6 {
    @DexIgnore
    public G37<W15> g;
    @DexIgnore
    public He6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements RecyclerViewCalendar.b {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewMonthFragment a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(ActiveTimeOverviewMonthFragment activeTimeOverviewMonthFragment) {
            this.a = activeTimeOverviewMonthFragment;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.b
        public void a(Calendar calendar) {
            Wg6.c(calendar, "calendar");
            He6 he6 = this.a.h;
            if (he6 != null) {
                Date time = calendar.getTime();
                Wg6.b(time, "calendar.time");
                he6.o(time);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements RecyclerViewCalendar.a {
        @DexIgnore
        public /* final */ /* synthetic */ ActiveTimeOverviewMonthFragment b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(ActiveTimeOverviewMonthFragment activeTimeOverviewMonthFragment) {
            this.b = activeTimeOverviewMonthFragment;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.a
        public void k0(int i, Calendar calendar) {
            Wg6.c(calendar, "calendar");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ActiveTimeOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                ActiveTimeDetailActivity.a aVar = ActiveTimeDetailActivity.C;
                Date time = calendar.getTime();
                Wg6.b(time, "it.time");
                Wg6.b(activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "ActiveTimeOverviewMonthFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void L6() {
        W15 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        G37<W15> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            DeviceHelper.Ai ai = DeviceHelper.o;
            He6 he6 = this.h;
            if (ai.w(he6 != null ? he6.n() : null)) {
                recyclerViewCalendar.J("dianaActiveMinutesTab");
            } else {
                recyclerViewCalendar.J("hybridActiveMinutesTab");
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(He6 he6) {
        M6(he6);
    }

    @DexIgnore
    public void M6(He6 he6) {
        Wg6.c(he6, "presenter");
        this.h = he6;
    }

    @DexIgnore
    @Override // com.fossil.Ie6
    public void e(TreeMap<Long, Float> treeMap) {
        W15 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        Wg6.c(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        G37<W15> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.setData(treeMap);
            recyclerViewCalendar.setEnableButtonNextAndPrevMonth(Boolean.TRUE);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ie6
    public void g(Date date, Date date2) {
        W15 a2;
        Wg6.c(date, "selectDate");
        Wg6.c(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        G37<W15> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null) {
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            Calendar instance3 = Calendar.getInstance();
            Wg6.b(instance, "selectCalendar");
            instance.setTime(date);
            Wg6.b(instance2, "startCalendar");
            instance2.setTime(TimeUtils.V(date2));
            Wg6.b(instance3, "endCalendar");
            instance3.setTime(TimeUtils.E(instance3.getTime()));
            a2.q.L(instance, instance2, instance3);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        W15 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onCreateView");
        W15 w15 = (W15) Aq0.f(layoutInflater, 2131558494, viewGroup, false, A6());
        RecyclerViewCalendar recyclerViewCalendar = w15.q;
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "Calendar.getInstance()");
        recyclerViewCalendar.setEndDate(instance);
        w15.q.setOnCalendarMonthChanged(new Ai(this));
        w15.q.setOnCalendarItemClickListener(new Bi(this));
        this.g = new G37<>(this, w15);
        L6();
        G37<W15> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onResume");
        L6();
        He6 he6 = this.h;
        if (he6 != null) {
            he6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onStop");
        He6 he6 = this.h;
        if (he6 != null) {
            he6.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
