package com.mapped;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Rl6<T> extends Rm6 {
    @DexIgnore
    Object l(Xe6<? super T> xe6);
}
