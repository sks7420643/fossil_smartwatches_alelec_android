package com.mapped;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.P77;
import com.fossil.Rf5;
import com.fossil.Tj5;
import com.fossil.Um5;
import com.fossil.Vt7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.watchface.MetaData;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingAdapter extends RecyclerView.g<Bi> {
    @DexIgnore
    public List<P77> a; // = new ArrayList();
    @DexIgnore
    public Ai b;
    @DexIgnore
    public String c;
    @DexIgnore
    public int d; // = -1;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(P77 p77);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Rf5 a;
        @DexIgnore
        public /* final */ /* synthetic */ RingAdapter b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.b.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1) {
                    Ai ai = this.b.b.b;
                    if (ai != null) {
                        ai.a((P77) this.b.b.a.get(this.b.getAdapterPosition()));
                    }
                    Bi bi = this.b;
                    bi.b.d = bi.getAdapterPosition();
                    this.b.b.notifyDataSetChanged();
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(RingAdapter ringAdapter, Rf5 rf5) {
            super(rf5.b());
            Wg6.c(rf5, "view");
            this.b = ringAdapter;
            this.a = rf5;
            rf5.b.setOnClickListener(new Aii(this));
        }

        @DexIgnore
        public final void a(P77 p77, boolean z) {
            Wg6.c(p77, "ring");
            Tj5.b(this.a.b).J(p77.c().a()).F0(this.a.b);
            FlexibleTextView flexibleTextView = this.a.c;
            Wg6.b(flexibleTextView, "view.tvName");
            PortfolioApp instance = PortfolioApp.get.instance();
            MetaData e = p77.e();
            flexibleTextView.setText(Um5.d(instance, e != null ? e.getName() : null, p77.f()));
            if (z) {
                View view = this.a.d;
                Wg6.b(view, "view.vBackgroundSelected");
                view.setVisibility(0);
            } else {
                View view2 = this.a.d;
                Wg6.b(view2, "view.vBackgroundSelected");
                view2.setVisibility(4);
            }
            if (this.b.e) {
                View view3 = this.a.e;
                Wg6.b(view3, "view.vDisable");
                view3.setVisibility(8);
                Rf5 rf5 = this.a;
                FlexibleTextView flexibleTextView2 = rf5.c;
                ConstraintLayout b2 = rf5.b();
                Wg6.b(b2, "view.root");
                flexibleTextView2.setTextColor(W6.d(b2.getContext(), 2131099968));
                ImageView imageView = this.a.b;
                Wg6.b(imageView, "view.ivBackgroundPreview");
                imageView.setClickable(true);
                return;
            }
            Rf5 rf52 = this.a;
            FlexibleTextView flexibleTextView3 = rf52.c;
            ConstraintLayout b3 = rf52.b();
            Wg6.b(b3, "view.root");
            flexibleTextView3.setTextColor(W6.d(b3.getContext(), 2131099938));
            View view4 = this.a.e;
            Wg6.b(view4, "view.vDisable");
            view4.setVisibility(0);
            ImageView imageView2 = this.a.b;
            Wg6.b(imageView2, "view.ivBackgroundPreview");
            imageView2.setClickable(false);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public final int k(String str) {
        T t;
        T t2;
        T t3;
        Wg6.c(str, "ringId");
        if (Vt7.l(str)) {
            Iterator<T> it = this.a.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t3 = null;
                    break;
                }
                T next = it.next();
                if (Vt7.j(next.f(), "empty", true)) {
                    t3 = next;
                    break;
                }
            }
            t2 = t3;
        } else {
            Iterator<T> it2 = this.a.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t = null;
                    break;
                }
                T next2 = it2.next();
                if (Wg6.a(next2.d(), str)) {
                    t = next2;
                    break;
                }
            }
            t2 = t;
        }
        if (t2 != null) {
            return this.a.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    public void l(Bi bi, int i) {
        Wg6.c(bi, "holder");
        int i2 = this.d;
        bi.a(this.a.get(i), i2 != -1 && i2 == i && this.e);
    }

    @DexIgnore
    public Bi m(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Rf5 c2 = Rf5.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(c2, "ItemRingBinding.inflate(\u2026.context), parent, false)");
        return new Bi(this, c2);
    }

    @DexIgnore
    public final void n(List<P77> list) {
        Wg6.c(list, "rings");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void o(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("RingAdapter", "setListEnable, value = " + z);
        this.e = z;
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        l(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return m(viewGroup, i);
    }

    @DexIgnore
    public final void p(Ai ai) {
        Wg6.c(ai, "listener");
        this.b = ai;
    }

    @DexIgnore
    public final void q(String str) {
        int i;
        int i2 = 0;
        FLogger.INSTANCE.getLocal().d("RingAdapter", "selectedRing = " + str);
        this.c = str;
        if (str == null) {
            i = -1;
        } else if (str != null) {
            if (!Vt7.l(str)) {
                Iterator<P77> it = this.a.iterator();
                while (true) {
                    i = i2;
                    if (it.hasNext()) {
                        if (Wg6.a(it.next().d(), str)) {
                            break;
                        }
                        i2 = i + 1;
                    } else {
                        break;
                    }
                }
            } else {
                Iterator<P77> it2 = this.a.iterator();
                while (true) {
                    i = i2;
                    if (it2.hasNext()) {
                        if (Vt7.j(it2.next().f(), "empty", true)) {
                            break;
                        }
                        i2 = i + 1;
                    } else {
                        break;
                    }
                }
            }
            i = -1;
        } else {
            Wg6.i();
            throw null;
        }
        this.d = i;
        notifyDataSetChanged();
    }
}
