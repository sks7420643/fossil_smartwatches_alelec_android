package com.mapped;

import com.fossil.Pn7;
import com.fossil.Qq7;
import com.fossil.Rn7;
import com.fossil.Un7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Af6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Qq7 implements Coroutine<Af6, Bi, Af6> {
            @DexIgnore
            public static /* final */ Aii INSTANCE; // = new Aii();

            @DexIgnore
            public Aii() {
                super(2);
            }

            @DexIgnore
            public final Af6 invoke(Af6 af6, Bi bi) {
                Wg6.c(af6, "acc");
                Wg6.c(bi, "element");
                Af6 minusKey = af6.minusKey(bi.getKey());
                if (minusKey == Un7.INSTANCE) {
                    return bi;
                }
                Rn7 rn7 = (Rn7) minusKey.get(Rn7.p);
                if (rn7 == null) {
                    return new Pn7(minusKey, bi);
                }
                Af6 minusKey2 = minusKey.minusKey(Rn7.p);
                return minusKey2 == Un7.INSTANCE ? new Pn7(bi, rn7) : new Pn7(new Pn7(minusKey2, bi), rn7);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public /* bridge */ /* synthetic */ Af6 invoke(Af6 af6, Bi bi) {
                return invoke(af6, bi);
            }
        }

        @DexIgnore
        public static Af6 a(Af6 af6, Af6 af62) {
            Wg6.c(af62, "context");
            return af62 == Un7.INSTANCE ? af6 : (Af6) af62.fold(af6, Aii.INSTANCE);
        }
    }

    @DexIgnore
    public interface Bi extends Af6 {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public static <R> R a(Bi bi, R r, Coroutine<? super R, ? super Bi, ? extends R> coroutine) {
                Wg6.c(coroutine, "operation");
                return (R) coroutine.invoke(r, bi);
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.mapped.Af6$Bi */
            /* JADX WARN: Multi-variable type inference failed */
            public static <E extends Bi> E b(Bi bi, Ci<E> ci) {
                Wg6.c(ci, "key");
                if (!Wg6.a(bi.getKey(), ci)) {
                    return null;
                }
                if (bi != 0) {
                    return bi;
                }
                throw new Rc6("null cannot be cast to non-null type E");
            }

            @DexIgnore
            public static Af6 c(Bi bi, Ci<?> ci) {
                Wg6.c(ci, "key");
                return Wg6.a(bi.getKey(), ci) ? Un7.INSTANCE : bi;
            }

            @DexIgnore
            public static Af6 d(Bi bi, Af6 af6) {
                Wg6.c(af6, "context");
                return Ai.a(bi, af6);
            }
        }

        @DexIgnore
        @Override // com.mapped.Af6
        <E extends Bi> E get(Ci<E> ci);

        @DexIgnore
        Ci<?> getKey();
    }

    @DexIgnore
    public interface Ci<E extends Bi> {
    }

    @DexIgnore
    <R> R fold(R r, Coroutine<? super R, ? super Bi, ? extends R> coroutine);

    @DexIgnore
    <E extends Bi> E get(Ci<E> ci);

    @DexIgnore
    Af6 minusKey(Ci<?> ci);

    @DexIgnore
    Af6 plus(Af6 af6);
}
