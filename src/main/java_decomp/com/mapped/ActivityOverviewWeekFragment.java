package com.mapped;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.I25;
import com.fossil.Mv5;
import com.fossil.Sf6;
import com.fossil.Tf6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityOverviewWeekFragment extends BaseFragment implements Tf6 {
    @DexIgnore
    public G37<I25> g;
    @DexIgnore
    public Sf6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "ActivityOverviewWeekFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void K6() {
        I25 a2;
        OverviewWeekChart overviewWeekChart;
        G37<I25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewWeekChart = a2.q) != null) {
            DeviceHelper.Ai ai = DeviceHelper.o;
            Sf6 sf6 = this.h;
            if (ai.w(sf6 != null ? sf6.n() : null)) {
                overviewWeekChart.D("dianaStepsTab", "nonBrandNonReachGoal");
            } else {
                overviewWeekChart.D("hybridStepsTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    public void L6(Sf6 sf6) {
        Wg6.c(sf6, "presenter");
        this.h = sf6;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Sf6 sf6) {
        L6(sf6);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        I25 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onCreateView");
        this.g = new G37<>(this, (I25) Aq0.f(layoutInflater, 2131558500, viewGroup, false, A6()));
        K6();
        G37<I25> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onResume");
        K6();
        Sf6 sf6 = this.h;
        if (sf6 != null) {
            sf6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onStop");
        Sf6 sf6 = this.h;
        if (sf6 != null) {
            sf6.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.Tf6
    public void p(Mv5 mv5) {
        I25 a2;
        OverviewWeekChart overviewWeekChart;
        Wg6.c(mv5, "baseModel");
        FLogger.INSTANCE.getLocal().d("ActivityOverviewWeekFragment", "showWeekDetails");
        G37<I25> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewWeekChart = a2.q) != null) {
            new ArrayList();
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(Mv5.a.b(cVar.d()));
            Mv5.Ai ai = Mv5.a;
            Wg6.b(overviewWeekChart, "it");
            Context context = overviewWeekChart.getContext();
            Wg6.b(context, "it.context");
            BarChart.H(overviewWeekChart, ai.a(context, cVar), false, 2, null);
            overviewWeekChart.r(mv5);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
