package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Ap1;
import com.fossil.Bp1;
import com.fossil.Cp1;
import com.fossil.D90;
import com.fossil.Dp1;
import com.fossil.Ey1;
import com.fossil.Ox1;
import com.fossil.Po1;
import com.fossil.Qo1;
import com.fossil.Ro1;
import com.fossil.So1;
import com.fossil.Tc0;
import com.fossil.To1;
import com.fossil.Ub;
import com.fossil.Uo1;
import com.fossil.Vo1;
import com.fossil.Vw1;
import com.fossil.Wo1;
import com.fossil.Xo1;
import com.fossil.Xw1;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class O80 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Ap1 b;
    @DexIgnore
    public Vw1 c;
    @DexIgnore
    public /* final */ Xw1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<O80> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public O80 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                Ap1 valueOf = Ap1.valueOf(readString);
                parcel.setDataPosition(0);
                switch (Ub.a[valueOf.ordinal()]) {
                    case 1:
                        return So1.CREATOR.a(parcel);
                    case 2:
                        return Cp1.CREATOR.a(parcel);
                    case 3:
                        return Dp1.CREATOR.a(parcel);
                    case 4:
                        return Uo1.CREATOR.a(parcel);
                    case 5:
                        return Vo1.CREATOR.a(parcel);
                    case 6:
                        return To1.CREATOR.a(parcel);
                    case 7:
                        return Wo1.CREATOR.a(parcel);
                    case 8:
                        return Po1.CREATOR.a(parcel);
                    case 9:
                        return Xo1.CREATOR.a(parcel);
                    case 10:
                        return Bp1.CREATOR.a(parcel);
                    case 11:
                        return Ro1.CREATOR.a(parcel);
                    case 12:
                        return Qo1.CREATOR.a(parcel);
                    default:
                        throw new Kc6();
                }
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public O80[] newArray(int i) {
            return new O80[i];
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public O80(android.os.Parcel r5) {
        /*
            r4 = this;
            r3 = 0
            java.lang.String r0 = r5.readString()
            if (r0 == 0) goto L_0x0030
            java.lang.String r1 = "parcel.readString()!!"
            com.mapped.Wg6.b(r0, r1)
            com.fossil.Ap1 r1 = com.fossil.Ap1.valueOf(r0)
            com.fossil.Vw1[] r0 = com.fossil.Vw1.values()
            int r2 = r5.readInt()
            r2 = r0[r2]
            java.lang.Class<com.fossil.Xw1> r0 = com.fossil.Xw1.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r5.readParcelable(r0)
            if (r0 == 0) goto L_0x002c
            com.fossil.Xw1 r0 = (com.fossil.Xw1) r0
            r4.<init>(r1, r2, r0)
            return
        L_0x002c:
            com.mapped.Wg6.i()
            throw r3
        L_0x0030:
            com.mapped.Wg6.i()
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.O80.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public O80(Ap1 ap1, Vw1 vw1, Xw1 xw1) {
        this.b = ap1;
        this.c = vw1;
        this.d = xw1;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ O80(Ap1 ap1, Vw1 vw1, Xw1 xw1, int i) {
        this(ap1, (i & 2) != 0 ? Vw1.TOP_SHORT_PRESS_RELEASE : vw1, (i & 4) != 0 ? new Tc0() : xw1);
    }

    @DexIgnore
    public JSONObject a() {
        return new JSONObject();
    }

    @DexIgnore
    public final void a(Vw1 vw1) {
        this.c = vw1;
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            O80 o80 = (O80) obj;
            return this.b == o80.b && this.c == o80.c && !(Wg6.a(this.d, o80.d) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.watchapp.WatchApp");
    }

    @DexIgnore
    public final Vw1 getButtonEvent() {
        return this.c;
    }

    @DexIgnore
    public final Ap1 getId() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        return (((hashCode * 31) + this.c.hashCode()) * 31) + this.d.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", this.b.a()).put("button_evt", Ey1.a(this.c));
        } catch (JSONException e) {
            D90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.c.ordinal());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }
}
