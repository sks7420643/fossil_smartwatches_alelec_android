package com.mapped;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.HorizontalScrollView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"ViewConstructor"})
public final class Xz5 extends HorizontalScrollView {
    @DexIgnore
    public long b; // = -1;
    @DexIgnore
    public Bi c;
    @DexIgnore
    public Runnable d; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void run() {
            long currentTimeMillis = System.currentTimeMillis();
            Xz5 xz5 = Xz5.this;
            if (currentTimeMillis - xz5.b > 100) {
                xz5.b = -1;
                xz5.c.a();
                return;
            }
            xz5.postDelayed(this, 100);
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Object onScrollChanged();  // void declaration
    }

    @DexIgnore
    public Xz5(Context context, Bi bi) {
        super(context);
        this.c = bi;
    }

    @DexIgnore
    public void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
        Bi bi = this.c;
        if (bi != null) {
            bi.onScrollChanged();
            if (this.b == -1) {
                postDelayed(this.d, 100);
            }
            this.b = System.currentTimeMillis();
        }
    }
}
