package com.mapped;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum J70 {
    CLEAR_DAY,
    CLEAR_NIGHT,
    CLOUDY,
    PARTLY_CLOUDY_DAY,
    PARTLY_CLOUDY_NIGHT,
    RAIN,
    SNOW,
    SLEET,
    STORMY,
    FOG,
    WIND;
    
    @DexIgnore
    public /* final */ int b; // = ordinal();

    @DexIgnore
    public J70() {
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }
}
