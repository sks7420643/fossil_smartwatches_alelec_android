package com.mapped;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Ph4 {
    Device("Device"),
    User("User"),
    Mock("Mock");
    
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    public Ph4(String str) {
        this.value = str;
    }

    @DexIgnore
    public static Ph4 fromString(String str) {
        if (!TextUtils.isEmpty(str)) {
            Ph4[] values = values();
            for (Ph4 ph4 : values) {
                if (str.equalsIgnoreCase(ph4.value)) {
                    return ph4;
                }
            }
        }
        return Device;
    }

    @DexIgnore
    public String getName() {
        return this.value;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }
}
