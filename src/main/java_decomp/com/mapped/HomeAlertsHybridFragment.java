package com.mapped;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.Jn5;
import com.fossil.Lg5;
import com.fossil.P75;
import com.fossil.Qv5;
import com.fossil.S37;
import com.fossil.Um5;
import com.fossil.Vl5;
import com.fossil.X36;
import com.fossil.Xq4;
import com.fossil.Y36;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationDialLandingActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeAlertsHybridFragment extends Qv5 implements Y36 {
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public G37<P75> h;
    @DexIgnore
    public X36 i;
    @DexIgnore
    public Xq4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final HomeAlertsHybridFragment a() {
            return new HomeAlertsHybridFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Xq4.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridFragment a;

        @DexIgnore
        public Bi(HomeAlertsHybridFragment homeAlertsHybridFragment) {
            this.a = homeAlertsHybridFragment;
        }

        @DexIgnore
        @Override // com.fossil.Xq4.Ai
        public void a(Alarm alarm) {
            Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            HomeAlertsHybridFragment.K6(this.a).o(alarm, !alarm.isActive());
        }

        @DexIgnore
        @Override // com.fossil.Xq4.Ai
        public void b(Alarm alarm) {
            Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            HomeAlertsHybridFragment.K6(this.a).q(alarm);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridFragment b;

        @DexIgnore
        public Ci(HomeAlertsHybridFragment homeAlertsHybridFragment) {
            this.b = homeAlertsHybridFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
                Wg6.b(activity, "it");
                PairingInstructionsActivity.a.b(aVar, activity, false, false, 6, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridFragment b;

        @DexIgnore
        public Di(HomeAlertsHybridFragment homeAlertsHybridFragment) {
            this.b = homeAlertsHybridFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeAlertsHybridFragment.K6(this.b).q(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridFragment b;

        @DexIgnore
        public Ei(HomeAlertsHybridFragment homeAlertsHybridFragment) {
            this.b = homeAlertsHybridFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (Jn5.c(Jn5.b, this.b.getContext(), Jn5.Ai.NOTIFICATION_GET_CONTACTS, false, false, false, null, 60, null)) {
                NotificationDialLandingActivity.B.a(this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridFragment b;

        @DexIgnore
        public Fi(HomeAlertsHybridFragment homeAlertsHybridFragment) {
            this.b = homeAlertsHybridFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeAlertsHybridFragment.K6(this.b).p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ HomeAlertsHybridFragment b;

        @DexIgnore
        public Gi(HomeAlertsHybridFragment homeAlertsHybridFragment) {
            this.b = homeAlertsHybridFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            HomeAlertsHybridFragment.K6(this.b).n();
        }
    }

    @DexIgnore
    public static final /* synthetic */ X36 K6(HomeAlertsHybridFragment homeAlertsHybridFragment) {
        X36 x36 = homeAlertsHybridFragment.i;
        if (x36 != null) {
            return x36;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "HomeAlertsHybridFragment";
    }

    @DexIgnore
    @Override // com.fossil.Y36
    public void F(boolean z) {
        RTLImageView rTLImageView;
        RTLImageView rTLImageView2;
        if (z) {
            G37<P75> g37 = this.h;
            if (g37 != null) {
                P75 a2 = g37.a();
                if (a2 != null && (rTLImageView2 = a2.I) != null) {
                    rTLImageView2.setVisibility(0);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        G37<P75> g372 = this.h;
        if (g372 != null) {
            P75 a3 = g372.a();
            if (a3 != null && (rTLImageView = a3.I) != null) {
                rTLImageView.setVisibility(8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    public void L6(X36 x36) {
        Wg6.c(x36, "presenter");
        this.i = x36;
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        L6((X36) obj);
    }

    @DexIgnore
    @Override // com.fossil.Y36
    public void N1(boolean z) {
        Context requireContext;
        int i2;
        G37<P75> g37 = this.h;
        if (g37 != null) {
            P75 a2 = g37.a();
            if (a2 != null) {
                FlexibleSwitchCompat flexibleSwitchCompat = a2.P;
                Wg6.b(flexibleSwitchCompat, "it.swScheduled");
                flexibleSwitchCompat.setChecked(z);
                FlexibleTextView flexibleTextView = a2.C;
                if (z) {
                    requireContext = requireContext();
                    i2 = 2131099968;
                } else {
                    requireContext = requireContext();
                    i2 = 2131100358;
                }
                flexibleTextView.setTextColor(W6.d(requireContext, i2));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y36
    public void U() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.Z(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.Y36
    public void W() {
        FLogger.INSTANCE.getLocal().d("HomeAlertsHybridFragment", "notifyListAlarm()");
        Xq4 xq4 = this.j;
        if (xq4 != null) {
            xq4.notifyDataSetChanged();
        } else {
            Wg6.n("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Y36
    public void c() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.Y36
    public void j4(boolean z) {
        G37<P75> g37 = this.h;
        if (g37 != null) {
            P75 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                RecyclerView recyclerView = a2.O;
                Wg6.b(recyclerView, "binding.rvAlarms");
                recyclerView.setVisibility(0);
                FlexibleTextView flexibleTextView = a2.v;
                Wg6.b(flexibleTextView, "binding.ftvAlarmsSection");
                flexibleTextView.setVisibility(0);
                FlexibleButton flexibleButton = a2.s;
                Wg6.b(flexibleButton, "binding.btnAdd");
                flexibleButton.setVisibility(0);
                return;
            }
            RecyclerView recyclerView2 = a2.O;
            Wg6.b(recyclerView2, "binding.rvAlarms");
            recyclerView2.setVisibility(8);
            FlexibleTextView flexibleTextView2 = a2.v;
            Wg6.b(flexibleTextView2, "binding.ftvAlarmsSection");
            flexibleTextView2.setVisibility(8);
            FlexibleButton flexibleButton2 = a2.s;
            Wg6.b(flexibleButton2, "binding.btnAdd");
            flexibleButton2.setVisibility(8);
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y36
    public void l0(String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        Wg6.c(str, "deviceId");
        Wg6.c(arrayList, "mAlarms");
        AlarmActivity.a aVar = AlarmActivity.B;
        Context requireContext = requireContext();
        Wg6.b(requireContext, "requireContext()");
        aVar.a(requireContext, str, arrayList, alarm);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        P75 p75 = (P75) Aq0.f(layoutInflater, 2131558572, viewGroup, false, A6());
        E6("alert_view");
        String d = ThemeManager.l.a().d("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(d)) {
            int parseColor = Color.parseColor(d);
            p75.Q.setBackgroundColor(parseColor);
            p75.R.setBackgroundColor(parseColor);
            p75.q.setBackgroundColor(parseColor);
            p75.r.setBackgroundColor(parseColor);
        }
        String d2 = ThemeManager.l.a().d("onPrimaryButton");
        if (!TextUtils.isEmpty(d2)) {
            int parseColor2 = Color.parseColor(d2);
            Drawable drawable = PortfolioApp.get.instance().getDrawable(2131230986);
            if (drawable != null) {
                drawable.setTint(parseColor2);
                p75.s.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
            }
        }
        p75.s.setOnClickListener(new Di(this));
        p75.K.setOnClickListener(new Ei(this));
        p75.P.setOnClickListener(new Fi(this));
        Lg5 lg5 = p75.H;
        if (lg5 != null) {
            ConstraintLayout constraintLayout = lg5.q;
            Wg6.b(constraintLayout, "viewNoDeviceBinding.clRoot");
            constraintLayout.setVisibility(0);
            lg5.t.setImageResource(2131230818);
            FlexibleTextView flexibleTextView = lg5.r;
            Wg6.b(flexibleTextView, "viewNoDeviceBinding.ftvDescription");
            flexibleTextView.setText(Um5.c(getContext(), 2131887050));
            lg5.s.setOnClickListener(new Ci(this));
        }
        p75.I.setOnClickListener(new Gi(this));
        Xq4 xq4 = new Xq4();
        xq4.o(new Bi(this));
        this.j = xq4;
        RecyclerView recyclerView = p75.O;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
        Xq4 xq42 = this.j;
        if (xq42 != null) {
            recyclerView.setAdapter(xq42);
            G37<P75> g37 = new G37<>(this, p75);
            this.h = g37;
            P75 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mAlarmsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        X36 x36 = this.i;
        if (x36 != null) {
            if (x36 != null) {
                x36.m();
                Vl5 C6 = C6();
                if (C6 != null) {
                    C6.c("");
                }
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
        super.onPause();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        X36 x36 = this.i;
        if (x36 == null) {
            return;
        }
        if (x36 != null) {
            x36.l();
            Vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y36
    public void p0(List<Alarm> list) {
        Wg6.c(list, "alarms");
        Xq4 xq4 = this.j;
        if (xq4 != null) {
            xq4.n(list);
        } else {
            Wg6.n("mAlarmsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Y36
    public void r(boolean z) {
        ConstraintLayout constraintLayout;
        G37<P75> g37 = this.h;
        if (g37 != null) {
            P75 a2 = g37.a();
            if (a2 != null && (constraintLayout = a2.t) != null) {
                constraintLayout.setVisibility(z ? 0 : 8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y36
    public void u3(boolean z) {
        ConstraintLayout constraintLayout;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeAlertsHybridFragment", "updateAssignNotificationState() - isEnable = " + z);
        G37<P75> g37 = this.h;
        if (g37 != null) {
            P75 a2 = g37.a();
            if (a2 != null && (constraintLayout = a2.K) != null) {
                Wg6.b(constraintLayout, "it");
                constraintLayout.setAlpha(z ? 0.5f : 1.0f);
                constraintLayout.setClickable(!z);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Y36
    public void v0() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.r0(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Qv5
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
