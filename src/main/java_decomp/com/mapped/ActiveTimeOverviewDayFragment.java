package com.mapped;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.Aq0;
import com.fossil.G37;
import com.fossil.Gl7;
import com.fossil.Hr7;
import com.fossil.Jl5;
import com.fossil.Mv5;
import com.fossil.Oi5;
import com.fossil.U15;
import com.fossil.Um5;
import com.fossil.Wd6;
import com.fossil.Xd6;
import com.fossil.Zc5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActiveTimeOverviewDayFragment extends BaseFragment implements Xd6 {
    @DexIgnore
    public G37<U15> g;
    @DexIgnore
    public Wd6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements View.OnClickListener {
        @DexIgnore
        public static /* final */ Ai b; // = new Ai();

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeDetailActivity.a aVar = ActiveTimeDetailActivity.C;
            Date date = new Date();
            Wg6.b(view, "it");
            Context context = view.getContext();
            Wg6.b(context, "it.context");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "ActiveTimeOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Xd6
    public void G(Mv5 mv5, ArrayList<String> arrayList) {
        U15 a2;
        OverviewDayChart overviewDayChart;
        Wg6.c(mv5, "baseModel");
        Wg6.c(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeOverviewDayFragment", "showDayDetails - baseModel=" + mv5);
        G37<U15> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.q) != null) {
            BarChart.c cVar = (BarChart.c) mv5;
            cVar.f(Mv5.a.b(cVar.d()));
            if (!arrayList.isEmpty()) {
                BarChart.H(overviewDayChart, arrayList, false, 2, null);
            } else {
                BarChart.H(overviewDayChart, Jl5.b.f(), false, 2, null);
            }
            overviewDayChart.r(mv5);
        }
    }

    @DexIgnore
    public final void K6(Zc5 zc5, WorkoutSession workoutSession) {
        String format;
        AppCompatImageView appCompatImageView;
        if (zc5 != null) {
            View n = zc5.n();
            Wg6.b(n, "binding.root");
            Context context = n.getContext();
            Oi5.Ai ai = Oi5.Companion;
            Lc6<Integer, Integer> a2 = ai.a(ai.d(workoutSession.getEditedType(), workoutSession.getEditedMode()));
            String c = Um5.c(context, a2.getSecond().intValue());
            zc5.t.setImageResource(a2.getFirst().intValue());
            FlexibleTextView flexibleTextView = zc5.r;
            Wg6.b(flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(c);
            DateTime editedEndTime = workoutSession.getEditedEndTime();
            if (editedEndTime != null) {
                long millis = editedEndTime.getMillis();
                DateTime editedStartTime = workoutSession.getEditedStartTime();
                if (editedStartTime != null) {
                    Gl7<Integer, Integer, Integer> e0 = TimeUtils.e0((int) ((millis - editedStartTime.getMillis()) / ((long) 1000)));
                    Wg6.b(e0, "DateHelper.getTimeValues(duration.toInt())");
                    FlexibleTextView flexibleTextView2 = zc5.s;
                    Wg6.b(flexibleTextView2, "it.ftvWorkoutValue");
                    if (Wg6.d(e0.getFirst().intValue(), 0) > 0) {
                        Hr7 hr7 = Hr7.a;
                        String c2 = Um5.c(context, 2131886698);
                        Wg6.b(c2, "LanguageHelper.getString\u2026rHrsNumberMinsNumberSecs)");
                        format = String.format(c2, Arrays.copyOf(new Object[]{e0.getFirst(), e0.getSecond(), e0.getThird()}, 3));
                        Wg6.b(format, "java.lang.String.format(format, *args)");
                    } else if (Wg6.d(e0.getSecond().intValue(), 0) > 0) {
                        Hr7 hr72 = Hr7.a;
                        String c3 = Um5.c(context, 2131886699);
                        Wg6.b(c3, "LanguageHelper.getString\u2026xt__NumberMinsNumberSecs)");
                        format = String.format(c3, Arrays.copyOf(new Object[]{e0.getSecond(), e0.getThird()}, 2));
                        Wg6.b(format, "java.lang.String.format(format, *args)");
                    } else {
                        Hr7 hr73 = Hr7.a;
                        String c4 = Um5.c(context, 2131886700);
                        Wg6.b(c4, "LanguageHelper.getString\u2026ailPage_Text__NumberSecs)");
                        format = String.format(c4, Arrays.copyOf(new Object[]{e0.getThird()}, 1));
                        Wg6.b(format, "java.lang.String.format(format, *args)");
                    }
                    flexibleTextView2.setText(format);
                    FlexibleTextView flexibleTextView3 = zc5.q;
                    Wg6.b(flexibleTextView3, "it.ftvWorkoutTime");
                    flexibleTextView3.setText(TimeUtils.o(workoutSession.getEditedStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
                    DeviceHelper.Ai ai2 = DeviceHelper.o;
                    Wd6 wd6 = this.h;
                    String d = ai2.w(wd6 != null ? wd6.n() : null) ? ThemeManager.l.a().d("dianaActiveMinutesTab") : ThemeManager.l.a().d("hybridActiveMinutesTab");
                    if (d != null && (appCompatImageView = zc5.t) != null) {
                        appCompatImageView.setColorFilter(Color.parseColor(d));
                        return;
                    }
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public final void L6() {
        U15 a2;
        OverviewDayChart overviewDayChart;
        G37<U15> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null && (overviewDayChart = a2.q) != null) {
            DeviceHelper.Ai ai = DeviceHelper.o;
            Wd6 wd6 = this.h;
            if (ai.w(wd6 != null ? wd6.n() : null)) {
                overviewDayChart.D("dianaActiveMinutesTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.D("hybridActiveMinutesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Wd6 wd6) {
        M6(wd6);
    }

    @DexIgnore
    public void M6(Wd6 wd6) {
        Wg6.c(wd6, "presenter");
        this.h = wd6;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        U15 a2;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onCreateView");
        U15 u15 = (U15) Aq0.f(layoutInflater, 2131558493, viewGroup, false, A6());
        u15.r.setOnClickListener(Ai.b);
        this.g = new G37<>(this, u15);
        L6();
        G37<U15> g37 = this.g;
        if (g37 == null || (a2 = g37.a()) == null) {
            return null;
        }
        return a2.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onResume");
        L6();
        Wd6 wd6 = this.h;
        if (wd6 != null) {
            wd6.l();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onStop");
        Wd6 wd6 = this.h;
        if (wd6 != null) {
            wd6.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.Xd6
    public void v(boolean z, List<WorkoutSession> list) {
        U15 a2;
        View n;
        View n2;
        Wg6.c(list, "workoutSessions");
        G37<U15> g37 = this.g;
        if (g37 != null && (a2 = g37.a()) != null) {
            if (z) {
                LinearLayout linearLayout = a2.u;
                Wg6.b(linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                K6(a2.s, list.get(0));
                if (size == 1) {
                    Zc5 zc5 = a2.t;
                    if (zc5 != null && (n2 = zc5.n()) != null) {
                        n2.setVisibility(8);
                        return;
                    }
                    return;
                }
                Zc5 zc52 = a2.t;
                if (!(zc52 == null || (n = zc52.n()) == null)) {
                    n.setVisibility(0);
                }
                K6(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            Wg6.b(linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
