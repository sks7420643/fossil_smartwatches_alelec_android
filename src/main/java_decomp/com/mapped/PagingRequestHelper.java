package com.mapped;

import android.util.Log;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class PagingRequestHelper {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ Ci[] c; // = {new Ci(Di.INITIAL), new Ci(Di.BEFORE), new Ci(Di.AFTER)};
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<Ai> d; // = new CopyOnWriteArrayList<>();

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void e(Gi gi);
    }

    @DexIgnore
    @FunctionalInterface
    public interface Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Aii {
            @DexIgnore
            public /* final */ AtomicBoolean a; // = new AtomicBoolean();
            @DexIgnore
            public /* final */ Ei b;
            @DexIgnore
            public /* final */ PagingRequestHelper c;

            @DexIgnore
            public Aii(Ei ei, PagingRequestHelper pagingRequestHelper) {
                this.b = ei;
                this.c = pagingRequestHelper;
            }

            @DexIgnore
            public final void a(Throwable th) {
                Log.d("PagingRequestHelper", "recordFailure");
                if (th == null) {
                    throw new IllegalArgumentException("You must provide a throwable describing the error to record the failure");
                } else if (this.a.compareAndSet(false, true)) {
                    this.c.e(this.b, th);
                } else {
                    throw new IllegalStateException("already called recordSuccess or recordFailure");
                }
            }

            @DexIgnore
            public final void b() {
                Log.d("PagingRequestHelper", "recordSuccess");
                if (this.a.compareAndSet(false, true)) {
                    this.c.e(this.b, null);
                    return;
                }
                throw new IllegalStateException("already called recordSuccess or recordFailure");
            }
        }

        @DexIgnore
        void run(Aii aii);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public Ei a;
        @DexIgnore
        public Bi b;
        @DexIgnore
        public Throwable c;
        @DexIgnore
        public Fi d; // = Fi.SUCCESS;

        @DexIgnore
        public Ci(Di di) {
        }
    }

    @DexIgnore
    public enum Di {
        INITIAL,
        BEFORE,
        AFTER
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei implements Runnable {
        @DexIgnore
        public /* final */ Bi b;
        @DexIgnore
        public /* final */ PagingRequestHelper c;
        @DexIgnore
        public /* final */ Di d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Runnable {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            public void run() {
                Ei ei = Ei.this;
                ei.c.h(ei.d, ei.b);
            }
        }

        @DexIgnore
        public Ei(Bi bi, PagingRequestHelper pagingRequestHelper, Di di) {
            this.b = bi;
            this.c = pagingRequestHelper;
            this.d = di;
        }

        @DexIgnore
        public void a(Executor executor) {
            executor.execute(new Aii());
        }

        @DexIgnore
        public void run() {
            this.b.run(new Bi.Aii(this, this.c));
        }
    }

    @DexIgnore
    public enum Fi {
        RUNNING,
        SUCCESS,
        FAILED
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi {
        @DexIgnore
        public /* final */ Fi a;
        @DexIgnore
        public /* final */ Fi b;
        @DexIgnore
        public /* final */ Fi c;
        @DexIgnore
        public /* final */ Throwable[] d;

        @DexIgnore
        public Gi(Fi fi, Fi fi2, Fi fi3, Throwable[] thArr) {
            this.a = fi;
            this.b = fi2;
            this.c = fi3;
            this.d = thArr;
        }

        @DexIgnore
        public Throwable a(Di di) {
            return this.d[di.ordinal()];
        }

        @DexIgnore
        public boolean b() {
            Fi fi = this.a;
            Fi fi2 = Fi.FAILED;
            return fi == fi2 || this.b == fi2 || this.c == fi2;
        }

        @DexIgnore
        public boolean c() {
            Fi fi = this.a;
            Fi fi2 = Fi.RUNNING;
            return fi == fi2 || this.b == fi2 || this.c == fi2;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Gi.class != obj.getClass()) {
                return false;
            }
            Gi gi = (Gi) obj;
            if (this.a == gi.a && this.b == gi.b && this.c == gi.c) {
                return Arrays.equals(this.d, gi.d);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + Arrays.hashCode(this.d);
        }

        @DexIgnore
        public String toString() {
            return "StatusReport{initial=" + this.a + ", before=" + this.b + ", after=" + this.c + ", mErrors=" + Arrays.toString(this.d) + '}';
        }
    }

    @DexIgnore
    public PagingRequestHelper(Executor executor) {
        this.b = executor;
    }

    @DexIgnore
    public boolean a(Ai ai) {
        return this.d.add(ai);
    }

    @DexIgnore
    public final void b(Gi gi) {
        Iterator<Ai> it = this.d.iterator();
        while (it.hasNext()) {
            it.next().e(gi);
        }
    }

    @DexIgnore
    public final Fi c(Di di) {
        return this.c[di.ordinal()].d;
    }

    @DexIgnore
    public final Gi d() {
        Ci[] ciArr = this.c;
        Throwable th = ciArr[0].c;
        Throwable th2 = ciArr[1].c;
        Throwable th3 = ciArr[2].c;
        return new Gi(c(Di.INITIAL), c(Di.BEFORE), c(Di.AFTER), new Throwable[]{th, th2, th3});
    }

    @DexIgnore
    public void e(Ei ei, Throwable th) {
        Gi d2;
        boolean z = th == null;
        boolean isEmpty = this.d.isEmpty();
        synchronized (this.a) {
            Ci ci = this.c[ei.d.ordinal()];
            ci.b = null;
            ci.c = th;
            if (z) {
                ci.a = null;
                ci.d = Fi.SUCCESS;
            } else {
                ci.a = ei;
                ci.d = Fi.FAILED;
            }
            d2 = isEmpty ^ true ? d() : null;
        }
        if (d2 != null) {
            b(d2);
        }
    }

    @DexIgnore
    public boolean f(Ai ai) {
        return this.d.remove(ai);
    }

    @DexIgnore
    public boolean g() {
        FLogger.INSTANCE.getLocal().d("PagingRequestHelper", "retryAllFailed");
        int length = Di.values().length;
        Ei[] eiArr = new Ei[length];
        synchronized (this.a) {
            for (int i = 0; i < Di.values().length; i++) {
                eiArr[i] = this.c[i].a;
                this.c[i].a = null;
            }
        }
        boolean z = false;
        for (int i2 = 0; i2 < length; i2++) {
            Ei ei = eiArr[i2];
            if (ei != null) {
                ei.a(this.b);
                z = true;
            }
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002e, code lost:
        if (r0 == null) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0030, code lost:
        b(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0033, code lost:
        new com.mapped.PagingRequestHelper.Ei(r7, r5, r6).run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return true;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean h(com.mapped.PagingRequestHelper.Di r6, com.mapped.PagingRequestHelper.Bi r7) {
        /*
            r5 = this;
            r0 = 0
            java.util.concurrent.CopyOnWriteArrayList<com.mapped.PagingRequestHelper$Ai> r1 = r5.d
            boolean r1 = r1.isEmpty()
            java.lang.Object r2 = r5.a
            monitor-enter(r2)
            com.mapped.PagingRequestHelper$Ci[] r3 = r5.c     // Catch:{ all -> 0x003d }
            int r4 = r6.ordinal()     // Catch:{ all -> 0x003d }
            r3 = r3[r4]     // Catch:{ all -> 0x003d }
            com.mapped.PagingRequestHelper$Bi r4 = r3.b     // Catch:{ all -> 0x003d }
            if (r4 == 0) goto L_0x0019
            monitor-exit(r2)     // Catch:{ all -> 0x003d }
            r0 = 0
        L_0x0018:
            return r0
        L_0x0019:
            r3.b = r7     // Catch:{ all -> 0x003d }
            com.mapped.PagingRequestHelper$Fi r4 = com.mapped.PagingRequestHelper.Fi.RUNNING     // Catch:{ all -> 0x003d }
            r3.d = r4     // Catch:{ all -> 0x003d }
            r4 = 0
            r3.a = r4     // Catch:{ all -> 0x003d }
            r4 = 0
            r3.c = r4     // Catch:{ all -> 0x003d }
            r1 = r1 ^ 1
            if (r1 == 0) goto L_0x002d
            com.mapped.PagingRequestHelper$Gi r0 = r5.d()     // Catch:{ all -> 0x003d }
        L_0x002d:
            monitor-exit(r2)     // Catch:{ all -> 0x003d }
            if (r0 == 0) goto L_0x0033
            r5.b(r0)
        L_0x0033:
            com.mapped.PagingRequestHelper$Ei r0 = new com.mapped.PagingRequestHelper$Ei
            r0.<init>(r7, r5, r6)
            r0.run()
            r0 = 1
            goto L_0x0018
        L_0x003d:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mapped.PagingRequestHelper.h(com.mapped.PagingRequestHelper$Di, com.mapped.PagingRequestHelper$Bi):boolean");
    }
}
