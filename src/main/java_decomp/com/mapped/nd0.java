package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Ey1;
import com.fossil.G80;
import com.fossil.Jd0;
import com.fossil.Ox1;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nd0 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Rd0 b;
    @DexIgnore
    public /* final */ Qd0[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Nd0> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Nd0 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                Rd0 valueOf = Rd0.valueOf(readString);
                Object[] createTypedArray = parcel.createTypedArray(Qd0.CREATOR);
                if (createTypedArray != null) {
                    Wg6.b(createTypedArray, "parcel.createTypedArray(\u2026AppDeclaration.CREATOR)!!");
                    return new Nd0(valueOf, (Qd0[]) createTypedArray);
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Nd0[] newArray(int i) {
            return new Nd0[i];
        }
    }

    @DexIgnore
    public Nd0(Rd0 rd0, Qd0[] qd0Arr) {
        this.b = rd0;
        this.c = qd0Arr;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Nd0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Nd0 nd0 = (Nd0) obj;
            Rd0 rd0 = this.b;
            Rd0 rd02 = nd0.b;
            if (rd0 != rd02) {
                return false;
            }
            return rd0 == rd02 && Arrays.equals(this.c, nd0.c);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.MicroAppMapping");
    }

    @DexIgnore
    public final Rd0 getMicroAppButton() {
        return this.b;
    }

    @DexIgnore
    public final Qd0[] getMicroAppDeclarations() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONArray jSONArray = new JSONArray();
        for (Qd0 qd0 : this.c) {
            jSONArray.put(qd0.toJSONObject());
        }
        return G80.k(G80.k(G80.k(new JSONObject(), Jd0.C3, Ey1.a(this.b)), Jd0.D3, jSONArray), Jd0.E3, Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.c, i);
        }
    }
}
