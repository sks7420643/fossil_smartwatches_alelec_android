package com.mapped;

import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Bi4 {
    CLEAR_DAY(WeatherComplicationAppInfo.WeatherCondition.CLEAR_DAY, "clear-day"),
    CLEAR_NIGHT(WeatherComplicationAppInfo.WeatherCondition.CLEAR_NIGHT, "clear-night"),
    RAIN(WeatherComplicationAppInfo.WeatherCondition.RAIN, "rain"),
    SNOW(WeatherComplicationAppInfo.WeatherCondition.SNOW, "snow"),
    SLEET(WeatherComplicationAppInfo.WeatherCondition.SLEET, "sleet"),
    WIND(WeatherComplicationAppInfo.WeatherCondition.WIND, "wind"),
    FOG(WeatherComplicationAppInfo.WeatherCondition.FOG, "fog"),
    CLOUDY(WeatherComplicationAppInfo.WeatherCondition.CLOUDY, "cloudy"),
    PARTLY_CLOUDY_DAY(WeatherComplicationAppInfo.WeatherCondition.PARTLY_CLOUDY_DAY, "partly-cloudy-day"),
    PARTLY_CLOUDY_NIGHT(WeatherComplicationAppInfo.WeatherCondition.PARTLY_CLOUDY_NIGHT, "partly-cloudy-night");
    
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public /* final */ String mConditionDescription;
    @DexIgnore
    public /* final */ WeatherComplicationAppInfo.WeatherCondition mConditionValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final WeatherComplicationAppInfo.WeatherCondition a(String str) {
            Bi4 bi4;
            WeatherComplicationAppInfo.WeatherCondition weatherCondition;
            Bi4[] values = Bi4.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    bi4 = null;
                    break;
                }
                bi4 = values[i];
                if (Wg6.a(bi4.mConditionDescription, str)) {
                    break;
                }
                i++;
            }
            return (bi4 == null || (weatherCondition = bi4.mConditionValue) == null) ? WeatherComplicationAppInfo.WeatherCondition.CLEAR_DAY : weatherCondition;
        }
    }

    @DexIgnore
    public Bi4(WeatherComplicationAppInfo.WeatherCondition weatherCondition, String str) {
        this.mConditionValue = weatherCondition;
        this.mConditionDescription = str;
    }

    @DexIgnore
    public static final WeatherComplicationAppInfo.WeatherCondition getConditionValue(String str) {
        return Companion.a(str);
    }
}
