package com.mapped;

import com.fossil.Kj4;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Pu3<T> {
    @DexIgnore
    JsonElement serialize(T t, Type type, Kj4 kj4);
}
