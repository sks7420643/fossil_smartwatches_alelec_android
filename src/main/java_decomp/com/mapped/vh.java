package com.mapped;

import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Vh {
    @DexIgnore
    public /* final */ Oh mDatabase;
    @DexIgnore
    public /* final */ AtomicBoolean mLock; // = new AtomicBoolean(false);
    @DexIgnore
    public volatile Mi mStmt;

    @DexIgnore
    public Vh(Oh oh) {
        this.mDatabase = oh;
    }

    @DexIgnore
    private Mi createNewStatement() {
        return this.mDatabase.compileStatement(createQuery());
    }

    @DexIgnore
    private Mi getStmt(boolean z) {
        if (!z) {
            return createNewStatement();
        }
        if (this.mStmt == null) {
            this.mStmt = createNewStatement();
        }
        return this.mStmt;
    }

    @DexIgnore
    public Mi acquire() {
        assertNotMainThread();
        return getStmt(this.mLock.compareAndSet(false, true));
    }

    @DexIgnore
    public void assertNotMainThread() {
        this.mDatabase.assertNotMainThread();
    }

    @DexIgnore
    public abstract String createQuery();

    @DexIgnore
    public void release(Mi mi) {
        if (mi == this.mStmt) {
            this.mLock.set(false);
        }
    }
}
