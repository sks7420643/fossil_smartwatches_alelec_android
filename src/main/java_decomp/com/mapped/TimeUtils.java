package com.mapped;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Pair;
import com.fossil.Gl7;
import com.fossil.Ll5;
import com.fossil.Um5;
import com.portfolio.platform.PortfolioApp;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class TimeUtils {
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> a; // = new Ki();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> b; // = new Vi();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> c; // = new e0();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> d; // = new f0();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> e; // = new h0();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> f; // = new i0();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> g; // = new k0();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> h; // = new Ai();
    @DexIgnore
    @SuppressLint({"ConstantLocale"})
    public static ThreadLocal<SimpleDateFormat> i; // = new Ei();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> j; // = new Fi();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> k; // = new Gi();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> l; // = new Hi();
    @DexIgnore
    public static ThreadLocal<DateTimeFormatter> m; // = new Ii();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> n; // = new Ji();
    @DexIgnore
    public static ThreadLocal<DateTimeFormatter> o; // = new Li();
    @DexIgnore
    public static ThreadLocal<SimpleDateFormat> p; // = new Mi();
    @DexIgnore
    public static TimeZone q; // = TimeZone.getDefault();
    @DexIgnore
    public static long r; // = 259200000;
    @DexIgnore
    public static long s; // = 172800000;
    @DexIgnore
    public static long t; // = LogBuilder.MAX_INTERVAL;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("MM/dd/yy hh:mm aa", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("MM/dd/yy hh:mm aa", Locale.getDefault());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("HH:mm", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("EEE MMM dd ", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii extends ThreadLocal<DateTimeFormatter> {
        @DexIgnore
        public DateTimeFormatter a() {
            return DateTimeFormat.forPattern(com.fossil.wearables.fsl.utils.TimeUtils.SIMPLE_FORMAT_YYYY_MM_DD).withLocale(Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ DateTimeFormatter initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getDefault());
            return simpleDateFormat;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat(com.fossil.wearables.fsl.utils.TimeUtils.SIMPLE_FORMAT_YYYY_MM_DD, Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li extends ThreadLocal<DateTimeFormatter> {
        @DexIgnore
        public DateTimeFormatter a() {
            return ISODateTimeFormat.dateTime().withLocale(Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ DateTimeFormatter initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return simpleDateFormat;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat(com.fossil.wearables.fsl.utils.TimeUtils.SIMPLE_FORMAT_YYYY_MM_DD, Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("yyyyMMdd", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Qi extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSS", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ri extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("EEEE, MMMM dd", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Si extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("MMMM, yyyy", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ti extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("hh:mm aa", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ui extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("MM/dd/yy hh:mm aa", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Vi extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("yyyyMMdd", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Wi extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Xi extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Yi extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getDefault());
            return simpleDateFormat;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Zi extends ThreadLocal<DateTimeFormatter> {
        @DexIgnore
        public DateTimeFormatter a() {
            return DateTimeFormat.forPattern(com.fossil.wearables.fsl.utils.TimeUtils.SIMPLE_FORMAT_YYYY_MM_DD).withLocale(Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ DateTimeFormatter initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return simpleDateFormat;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b0 extends ThreadLocal<DateTimeFormatter> {
        @DexIgnore
        public DateTimeFormatter a() {
            return ISODateTimeFormat.dateTime().withLocale(Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ DateTimeFormatter initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSS", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("EEEE, MMMM dd", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("MMMM, yyyy", Locale.getDefault());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("hh:mm aa", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("hh:mm", Locale.US);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k0 extends ThreadLocal<SimpleDateFormat> {
        @DexIgnore
        public SimpleDateFormat a() {
            return new SimpleDateFormat("hh:mm aa", Locale.getDefault());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.lang.ThreadLocal
        public /* bridge */ /* synthetic */ SimpleDateFormat initialValue() {
            return a();
        }
    }

    /*
    static {
        new g0();
        new j0();
        new Bi();
        new Ci();
        new Di();
    }
    */

    @DexIgnore
    public static String A(Date date) {
        return d.get().format(date);
    }

    @DexIgnore
    public static int B(Long l2) {
        Calendar instance = Calendar.getInstance(Locale.US);
        instance.setTimeInMillis(l2.longValue());
        return instance.get(7);
    }

    @DexIgnore
    public static Calendar C(Calendar calendar) {
        calendar.set(11, 23);
        calendar.set(12, 59);
        calendar.set(13, 59);
        calendar.set(14, 999);
        return calendar;
    }

    @DexIgnore
    public static Calendar D(Calendar calendar, TimeZone timeZone) {
        calendar.setTimeZone(timeZone);
        calendar.set(11, 23);
        calendar.set(12, 59);
        calendar.set(13, 59);
        calendar.set(14, 999);
        return calendar;
    }

    @DexIgnore
    public static Date E(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        C(instance);
        return instance.getTime();
    }

    @DexIgnore
    public static Date F(Date date, TimeZone timeZone) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        D(instance, timeZone);
        return instance.getTime();
    }

    @DexIgnore
    public static Calendar G(Calendar calendar) {
        Calendar calendar2 = (Calendar) calendar.clone();
        calendar2.set(5, calendar2.getActualMaximum(5));
        C(calendar2);
        return calendar2;
    }

    @DexIgnore
    public static Calendar H(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        return G(instance);
    }

    @DexIgnore
    public static Calendar I(Calendar calendar) {
        Calendar calendar2 = (Calendar) calendar.clone();
        calendar2.add(5, -(calendar2.get(7) - 7));
        C(calendar2);
        return calendar2;
    }

    @DexIgnore
    public static Calendar J(Date date) {
        Calendar instance = Calendar.getInstance(Locale.US);
        instance.setTime(date);
        return I(instance);
    }

    @DexIgnore
    public static int K(Long l2) {
        Calendar instance = Calendar.getInstance(Locale.US);
        instance.setTimeInMillis(l2.longValue());
        return instance.get(11);
    }

    @DexIgnore
    public static Pair<Date, Date> L(Date date, Date date2) {
        Pair<Date, Date> h02 = h0(date);
        Date date3 = (Date) h02.first;
        Date date4 = (Date) h02.second;
        if (!j0(date2, date3)) {
            date2 = date3;
        }
        if (j0(date4, new Date())) {
            date4 = new Date();
        }
        return new Pair<>(date2, date4);
    }

    @DexIgnore
    public static DateTime M(String str) {
        return m.get().parseDateTime(str);
    }

    @DexIgnore
    public static String N(int i2) {
        switch (i2) {
            case 0:
                return Um5.c(PortfolioApp.d0, 2131886856);
            case 1:
                return Um5.c(PortfolioApp.d0, 2131886855);
            case 2:
                return Um5.c(PortfolioApp.d0, 2131886859);
            case 3:
                return Um5.c(PortfolioApp.d0, 2131886852);
            case 4:
                return Um5.c(PortfolioApp.d0, 2131886860);
            case 5:
                return Um5.c(PortfolioApp.d0, 2131886858);
            case 6:
                return Um5.c(PortfolioApp.d0, 2131886857);
            case 7:
                return Um5.c(PortfolioApp.d0, 2131886853);
            case 8:
                return Um5.c(PortfolioApp.d0, 2131886863);
            case 9:
                return Um5.c(PortfolioApp.d0, 2131886862);
            case 10:
                return Um5.c(PortfolioApp.d0, 2131886861);
            case 11:
                return Um5.c(PortfolioApp.d0, 2131886854);
            default:
                return "";
        }
    }

    @DexIgnore
    public static Date O(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.add(5, 1);
        return instance.getTime();
    }

    @DexIgnore
    public static Date P(Date date) {
        return Q(date, 1);
    }

    @DexIgnore
    public static Date Q(Date date, int i2) {
        Calendar instance = Calendar.getInstance(Locale.US);
        instance.setTime(date);
        instance.add(5, -i2);
        return instance.getTime();
    }

    @DexIgnore
    public static DateTime R(DateTimeZone dateTimeZone, String str) {
        return o.get().withZone(dateTimeZone).parseDateTime(str);
    }

    @DexIgnore
    public static DateTime S(String str) {
        return o.get().withOffsetParsed().parseDateTime(str);
    }

    @DexIgnore
    public static Calendar T(Calendar calendar) {
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        return calendar;
    }

    @DexIgnore
    public static Calendar U(Calendar calendar, TimeZone timeZone) {
        calendar.setTimeZone(timeZone);
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        return calendar;
    }

    @DexIgnore
    public static Date V(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        T(instance);
        return instance.getTime();
    }

    @DexIgnore
    public static Date W(Date date, TimeZone timeZone) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        U(instance, timeZone);
        return instance.getTime();
    }

    @DexIgnore
    public static Calendar X(Long l2) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(l2.longValue());
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        return instance;
    }

    @DexIgnore
    public static Calendar Y(Calendar calendar) {
        Calendar calendar2 = (Calendar) calendar.clone();
        calendar2.set(5, 1);
        T(calendar2);
        return calendar2;
    }

    @DexIgnore
    public static Calendar Z(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        return Y(instance);
    }

    @DexIgnore
    public static Date a(int i2, String str) {
        try {
            DateTimeZone forOffsetMillis = DateTimeZone.forOffsetMillis(i2 * 1000);
            DateTime parseDateTime = ISODateTimeFormat.dateTime().withZone(forOffsetMillis).parseDateTime(str);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            simpleDateFormat.setTimeZone(forOffsetMillis.toTimeZone());
            return c.get().parse(simpleDateFormat.format(Long.valueOf(parseDateTime.getMillis())));
        } catch (Exception e2) {
            Calendar instance = Calendar.getInstance();
            instance.set(1990, 0, 1);
            return instance.getTime();
        }
    }

    @DexIgnore
    public static Calendar a0(Calendar calendar) {
        Calendar calendar2 = (Calendar) calendar.clone();
        calendar2.add(5, -(calendar2.get(7) - 1));
        T(calendar2);
        return calendar2;
    }

    @DexIgnore
    public static DateTime b(Date date, int i2) {
        return new DateTime(date).withZone(DateTimeZone.forOffsetMillis(i2 * 1000));
    }

    @DexIgnore
    public static Calendar b0(Date date) {
        Calendar instance = Calendar.getInstance(Locale.US);
        instance.setTime(date);
        return a0(instance);
    }

    @DexIgnore
    public static String c(Date date) {
        return new SimpleDateFormat("dd MMM yyyy").format(date);
    }

    @DexIgnore
    public static int c0() {
        return d0(TimeZone.getDefault());
    }

    @DexIgnore
    public static String d(Date date) {
        return l.get().format(date);
    }

    @DexIgnore
    public static int d0(TimeZone timeZone) {
        return timeZone.getRawOffset() / 1000;
    }

    @DexIgnore
    public static String e(Date date) {
        return DateFormat.getMediumDateFormat(PortfolioApp.d0.getApplicationContext()).format(date).toString();
    }

    @DexIgnore
    public static Gl7<Integer, Integer, Integer> e0(int i2) {
        return new Gl7<>(Integer.valueOf(i2 / 3600), Integer.valueOf((i2 % 3600) / 60), Integer.valueOf(i2 % 60));
    }

    @DexIgnore
    public static String f(Date date) {
        j.get().setTimeZone(q);
        return j.get().format(date);
    }

    @DexIgnore
    public static int f0(String str, Date date, boolean z) {
        if (TextUtils.isEmpty(str)) {
            return TimeZone.getDefault().getRawOffset() / 1000;
        }
        TimeZone timeZone = TimeZone.getTimeZone(str);
        if (!timeZone.inDaylightTime(date) || !z) {
            return timeZone.getRawOffset() / 1000;
        }
        return (timeZone.getDSTSavings() + timeZone.getRawOffset()) / 1000;
    }

    @DexIgnore
    public static String g(Date date) {
        c.get().setTimeZone(q);
        return c.get().format(date);
    }

    @DexIgnore
    public static int g0(String str, boolean z) {
        return f0(str, new Date(), z);
    }

    @DexIgnore
    public static String h(long j2) {
        return h.get().format(new Date(j2));
    }

    @DexIgnore
    public static Pair<Date, Date> h0(Date date) {
        return new Pair<>(b0(date).getTime(), J(date).getTime());
    }

    @DexIgnore
    public static String i(Date date) {
        i.get().setTimeZone(q);
        return i.get().format(date);
    }

    @DexIgnore
    public static String i0(Date date) {
        return k.get().format(date);
    }

    @DexIgnore
    public static String j(DateTime dateTime) {
        return dateTime == null ? "" : DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss 'GMT'").withZoneUTC().withLocale(Locale.US).print(dateTime);
    }

    @DexIgnore
    public static boolean j0(Date date, Date date2) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.getTime();
        T(instance);
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(date2);
        instance2.getTime();
        T(instance2);
        return instance.after(instance2);
    }

    @DexIgnore
    public static String k(Date date) {
        a.get().setTimeZone(q);
        return a.get().format(date);
    }

    @DexIgnore
    public static boolean k0(Date date, Date date2) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.getTime();
        T(instance);
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(date2);
        instance2.getTime();
        T(instance2);
        return (instance2 instanceof Calendar) && instance.compareTo(instance2) >= 0;
    }

    @DexIgnore
    public static String l(Date date, TimeZone timeZone) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(com.fossil.wearables.fsl.utils.TimeUtils.SIMPLE_FORMAT_YYYY_MM_DD, Locale.US);
        simpleDateFormat.setTimeZone(timeZone);
        return simpleDateFormat.format(date);
    }

    @DexIgnore
    public static boolean l0(Long l2, Long l3) {
        return V(new Date(l2.longValue())).getTime() == V(new Date(l3.longValue())).getTime();
    }

    @DexIgnore
    public static String m(Date date) {
        return new SimpleDateFormat(com.fossil.wearables.fsl.utils.TimeUtils.SIMPLE_FORMAT_YYYY_MM_DD, Locale.US).format(date);
    }

    @DexIgnore
    public static boolean m0(Date date, Date date2) {
        if (date == null || date2 == null) {
            return false;
        }
        return z(date).equals(z(date2));
    }

    @DexIgnore
    public static String n(long j2) {
        return f.get().format(new Date(j2));
    }

    @DexIgnore
    public static boolean n0(long j2, long j3) {
        Calendar instance = Calendar.getInstance(Locale.US);
        instance.setTimeInMillis(j2);
        Calendar instance2 = Calendar.getInstance(Locale.US);
        instance2.setTimeInMillis(j3);
        return instance.get(1) == instance2.get(1) && instance.get(2) == instance2.get(2);
    }

    @DexIgnore
    public static String o(long j2, int i2) {
        f.get().setTimeZone(t(i2));
        return f.get().format(new Date(j2));
    }

    @DexIgnore
    public static Boolean o0(Date date) {
        if (date == null) {
            return Boolean.FALSE;
        }
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        int i2 = instance.get(1);
        int i3 = instance.get(2);
        Calendar instance2 = Calendar.getInstance();
        instance2.setTime(new Date());
        return (i2 == instance2.get(1) && i3 == instance2.get(2)) ? Boolean.TRUE : Boolean.FALSE;
    }

    @DexIgnore
    public static String p(long j2, int i2) {
        g.set(new SimpleDateFormat("hh:mm aa", Locale.getDefault()));
        g.get().setTimeZone(t(i2));
        return g.get().format(new Date(j2));
    }

    @DexIgnore
    public static Boolean p0(Date date) {
        return Boolean.valueOf(z(date).equals(z(new Date())));
    }

    @DexIgnore
    public static String q(Date date) {
        g.set(new SimpleDateFormat("hh:mm aa", Locale.getDefault()));
        return g.get().format(date);
    }

    @DexIgnore
    public static Date q0(String str) {
        return ISODateTimeFormat.dateTime().parseDateTime(str).toDate();
    }

    @DexIgnore
    public static String r(long j2, int i2) {
        f.get().setTimeZone(u(i2));
        return f.get().format(new Date(j2));
    }

    @DexIgnore
    public static Date r0(String str) throws ParseException {
        return a.get().parse(str);
    }

    @DexIgnore
    public static String s(Date date) {
        e.set(new SimpleDateFormat("MMMM yyyy", Locale.getDefault()));
        e.get().setTimeZone(q);
        return e.get().format(date);
    }

    @DexIgnore
    public static String s0(DateTimeZone dateTimeZone, DateTime dateTime) {
        return o.get().withZone(DateTimeZone.getDefault()).print(dateTime);
    }

    @DexIgnore
    public static TimeZone t(int i2) {
        TimeZone timeZone;
        TimeZone timeZone2 = TimeZone.getDefault();
        if (i2 == timeZone2.getRawOffset() / 1000) {
            return timeZone2;
        }
        String[] availableIDs = TimeZone.getAvailableIDs(i2 * 1000);
        if (availableIDs != null && availableIDs.length > 0) {
            for (String str : availableIDs) {
                if (!(TextUtils.isEmpty(str) || (timeZone = TimeZone.getTimeZone(str)) == null)) {
                    return timeZone;
                }
            }
        }
        return TimeZone.getDefault();
    }

    @DexIgnore
    public static String t0(DateTime dateTime) {
        return o.get().withOffsetParsed().print(dateTime);
    }

    @DexIgnore
    public static TimeZone u(int i2) {
        TimeZone timeZone;
        TimeZone timeZone2 = TimeZone.getDefault();
        if (i2 == timeZone2.getRawOffset() / 1000) {
            return timeZone2;
        }
        int i3 = i2 * 1000;
        String[] availableIDs = TimeZone.getAvailableIDs(i3);
        if (availableIDs != null && availableIDs.length > 0) {
            for (String str : availableIDs) {
                if (!(TextUtils.isEmpty(str) || (timeZone = TimeZone.getTimeZone(str)) == null || timeZone.getOffset(System.currentTimeMillis()) != i3)) {
                    return timeZone;
                }
            }
        }
        return TimeZone.getDefault();
    }

    @DexIgnore
    public static void u0() {
        a = new Ni();
        b = new Oi();
        c = new Pi();
        d = new Qi();
        new Ri();
        e = new Si();
        f = new Ti();
        h = new Ui();
        new Wi();
        new Xi();
        n = new Yi();
        m = new Zi();
        p = new a0();
        o = new b0();
        i = new c0();
        j = new d0();
        q = TimeZone.getDefault();
    }

    @DexIgnore
    public static Calendar v(long j2) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(j2);
        instance.setTimeZone(q);
        return instance;
    }

    @DexIgnore
    public static String v0(Date date) {
        Date date2 = new Date();
        if (l0(Long.valueOf(date.getTime()), Long.valueOf(date2.getTime()))) {
            return Ll5.f(date);
        }
        long time = date2.getTime() - date.getTime();
        long j2 = r;
        if (time > j2) {
            return Ll5.d(date);
        }
        if (time <= j2 && time >= s) {
            return String.format(Um5.c(PortfolioApp.d0, 2131886202), 3);
        } else if (time > s || time <= t) {
            return Um5.c(PortfolioApp.d0, 2131886333);
        } else {
            return String.format(Um5.c(PortfolioApp.d0, 2131886202), 2);
        }
    }

    @DexIgnore
    public static Calendar w(int i2, Calendar calendar) {
        Calendar calendar2 = (Calendar) calendar.clone();
        calendar2.add(1, i2 / 12);
        calendar2.add(2, i2 % 12);
        calendar2.set(5, 1);
        return calendar2;
    }

    @DexIgnore
    public static String w0(Date date) {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        simpleDateFormat.setTimeZone(timeZone);
        return simpleDateFormat.format(date);
    }

    @DexIgnore
    public static Date x(Date date, TimeZone timeZone) {
        Date date2;
        synchronized (TimeUtils.class) {
            try {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
                String format = simpleDateFormat.format(date);
                simpleDateFormat.setTimeZone(timeZone);
                try {
                    date2 = simpleDateFormat.parse(format);
                } catch (ParseException e2) {
                    e2.printStackTrace();
                    date2 = new Date();
                }
            } finally {
            }
        }
        return date2;
    }

    @DexIgnore
    public static String x0(DateTime dateTime) {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        simpleDateFormat.setTimeZone(timeZone);
        return simpleDateFormat.format(new Date(dateTime.getMillis()));
    }

    @DexIgnore
    public static Date y(String str) {
        try {
            return b.get().parse(str);
        } catch (Exception e2) {
            return null;
        }
    }

    @DexIgnore
    public static String z(Date date) {
        b.get().setTimeZone(q);
        return b.get().format(date);
    }
}
