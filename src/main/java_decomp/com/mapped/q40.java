package com.mapped;

import android.os.Parcelable;
import com.fossil.Ez;
import com.fossil.Mp1;
import com.fossil.R4;
import com.fossil.Vx;
import com.fossil.Zk1;
import com.fossil.fitness.FitnessData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Q40 extends Parcelable {

    @DexIgnore
    public enum Ai {
        BOND_NONE,
        BONDING,
        BONDED;
        
        @DexIgnore
        public static /* final */ Aii c; // = new Aii(null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
            }

            @DexIgnore
            public final Ai a(int i) {
                return i != 11 ? i != 12 ? Ai.BOND_NONE : Ai.BONDED : Ai.BONDING;
            }

            @DexIgnore
            public final Ai b(R4 r4) {
                int i = Vx.a[r4.ordinal()];
                if (i == 1) {
                    return Ai.BOND_NONE;
                }
                if (i == 2) {
                    return Ai.BONDING;
                }
                if (i == 3) {
                    return Ai.BONDED;
                }
                throw new Kc6();
            }
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void onAutoSyncFitnessDataReceived(FitnessData[] fitnessDataArr);

        @DexIgnore
        void onDeviceStateChanged(Q40 q40, Ci ci, Ci ci2);

        @DexIgnore
        void onEventReceived(Q40 q40, Mp1 mp1);
    }

    @DexIgnore
    public enum Ci {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        UPGRADING_FIRMWARE,
        DISCONNECTING
    }

    @DexIgnore
    public enum Di {
        b,
        c,
        d,
        e;
        
        @DexIgnore
        public static /* final */ Ez g; // = new Ez(null);
    }

    @DexIgnore
    void B(Bi bi);

    @DexIgnore
    Zk1 M();

    @DexIgnore
    Ai P();

    @DexIgnore
    <T> T g0(Class<T> cls);

    @DexIgnore
    Ci getState();

    @DexIgnore
    boolean isActive();

    @DexIgnore
    Zb0<Cd6> l();
}
