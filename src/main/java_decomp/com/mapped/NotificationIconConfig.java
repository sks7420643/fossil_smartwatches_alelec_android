package com.mapped;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Dm7;
import com.fossil.Ey1;
import com.fossil.G80;
import com.fossil.Hd0;
import com.fossil.Jd0;
import com.fossil.Mo1;
import com.fossil.Ox1;
import com.fossil.Pm7;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationIconConfig extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ NotificationIcon b;
    @DexIgnore
    public /* final */ Hashtable<Mo1, NotificationIcon> c; // = new Hashtable<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<NotificationIconConfig> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public NotificationIconConfig createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(NotificationIcon.class.getClassLoader());
            if (readParcelable != null) {
                Wg6.b(readParcelable, "parcel.readParcelable<No\u2026class.java.classLoader)!!");
                NotificationIconConfig notificationIconConfig = new NotificationIconConfig((NotificationIcon) readParcelable);
                Hashtable<Mo1, NotificationIcon> hashtable = notificationIconConfig.c;
                Serializable readSerializable = parcel.readSerializable();
                if (readSerializable != null) {
                    hashtable.putAll((Hashtable) readSerializable);
                    return notificationIconConfig;
                }
                throw new Rc6("null cannot be cast to non-null type java.util.Hashtable<com.fossil.blesdk.device.data.notification.NotificationType, com.fossil.blesdk.model.file.NotificationIcon>");
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public NotificationIconConfig[] newArray(int i) {
            return new NotificationIconConfig[i];
        }
    }

    @DexIgnore
    public NotificationIconConfig(NotificationIcon notificationIcon) {
        this.b = notificationIcon;
    }

    @DexIgnore
    public final NotificationIcon[] a() {
        Collection<NotificationIcon> values = this.c.values();
        Wg6.b(values, "typeIcons.values");
        Object[] array = Pm7.C(Pm7.W(values, this.b)).toArray(new NotificationIcon[0]);
        if (array != null) {
            return (NotificationIcon[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final byte[] b() {
        int i;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        Mo1[] values = Mo1.values();
        int length = values.length;
        int i2 = 0;
        int i3 = 0;
        while (i3 < length) {
            Mo1 mo1 = values[i3];
            NotificationIcon notificationIcon = this.c.get(mo1);
            int ordinal = 1 << mo1.ordinal();
            if (notificationIcon == null || notificationIcon.d()) {
                i = i2 | ordinal;
            } else {
                String fileName = notificationIcon.getFileName();
                Charset c2 = Hd0.y.c();
                if (fileName != null) {
                    byte[] bytes = fileName.getBytes(c2);
                    Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
                    byte[] p = Dm7.p(bytes, (byte) 0);
                    ByteBuffer order = ByteBuffer.allocate(p.length + 3).order(ByteOrder.LITTLE_ENDIAN);
                    Wg6.b(order, "ByteBuffer.allocate(TYPE\u2026(ByteOrder.LITTLE_ENDIAN)");
                    order.putShort((short) ordinal).put((byte) p.length).put(p);
                    byteArrayOutputStream.write(order.array());
                    i = i2;
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            i3++;
            i2 = i;
        }
        if (i2 != 0) {
            String fileName2 = this.b.getFileName();
            Charset c3 = Hd0.y.c();
            if (fileName2 != null) {
                byte[] bytes2 = fileName2.getBytes(c3);
                Wg6.b(bytes2, "(this as java.lang.String).getBytes(charset)");
                byte[] p2 = Dm7.p(bytes2, (byte) 0);
                ByteBuffer order2 = ByteBuffer.allocate(p2.length + 3).order(ByteOrder.LITTLE_ENDIAN);
                Wg6.b(order2, "ByteBuffer.allocate(TYPE\u2026(ByteOrder.LITTLE_ENDIAN)");
                order2.putShort((short) i2).put((byte) p2.length).put(p2);
                byteArrayOutputStream.write(order2.array());
            } else {
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        Wg6.b(byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(NotificationIconConfig.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            NotificationIconConfig notificationIconConfig = (NotificationIconConfig) obj;
            return Wg6.a(this.b, notificationIconConfig.b) && Wg6.a(this.c, notificationIconConfig.c);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.notification.filter.NotificationIconConfig");
    }

    @DexIgnore
    public final NotificationIcon getDefaultIcon() {
        return this.b;
    }

    @DexIgnore
    public final NotificationIcon getIconForType(Mo1 mo1) {
        return this.c.get(mo1);
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    public final NotificationIconConfig setIconForType(Mo1 mo1, NotificationIcon notificationIcon) {
        if (notificationIcon != null) {
            this.c.put(mo1, notificationIcon);
        } else {
            this.c.remove(mo1);
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject k = G80.k(new JSONObject(), Jd0.a3, this.b.toJSONObject());
        for (Map.Entry<Mo1, NotificationIcon> entry : this.c.entrySet()) {
            Mo1 key = entry.getKey();
            Wg6.b(key, "icon.key");
            k.put(Ey1.a(key), entry.getValue().toJSONObject());
        }
        return k;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
        if (parcel != null) {
            parcel.writeSerializable(this.c);
        }
    }
}
