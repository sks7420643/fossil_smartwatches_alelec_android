package com.mapped;

import com.fossil.Gs4;
import com.fossil.Hm7;
import com.fossil.Py4;
import com.fossil.Um5;
import com.fossil.Vs4;
import com.fossil.Vy4;
import com.fossil.Xy4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCGenerator {
    @DexIgnore
    public static /* final */ BCGenerator a; // = new BCGenerator();

    @DexIgnore
    public final List<Vs4> a() {
        return Hm7.i(new Vs4(PortfolioApp.get.instance().getString(2131886272), PortfolioApp.get.instance().getString(2131886270), 2131231014, "activity_best_result", 0, 86400, "public_with_friend"), new Vs4(PortfolioApp.get.instance().getString(2131886284), PortfolioApp.get.instance().getString(2131886283), 2131231017, "activity_reach_goal", 15000, 259200, "public_with_friend"));
    }

    @DexIgnore
    public final List<Date> b() {
        FLogger.INSTANCE.getLocal().e("BCGenerator", "laterTime - phoneDate: " + new Date() + " - exactDate: " + Xy4.a.a());
        ArrayList arrayList = new ArrayList();
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "calendar");
        instance.setTime(Xy4.a.a());
        Date time = instance.getTime();
        Wg6.b(time, "currentDate");
        arrayList.add(time);
        for (int i = 1; i <= 6; i++) {
            instance.add(6, 1);
            Date time2 = instance.getTime();
            Wg6.b(time2, "calendar.time");
            arrayList.add(time2);
        }
        return arrayList;
    }

    @DexIgnore
    public final List<Gs4> c() {
        String c = Um5.c(PortfolioApp.get.instance(), 2131886319);
        Wg6.b(c, "LanguageHelper.getString\u2026enu_List__AboutChallenge)");
        Gs4 gs4 = new Gs4(c, Vy4.ABOUT, false, 4, null);
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886321);
        Wg6.b(c2, "LanguageHelper.getString\u2026ist__ViewFullLeaderboard)");
        return Hm7.i(gs4, new Gs4(c2, Vy4.LEADER_BOARD, false, 4, null));
    }

    @DexIgnore
    public final List<Gs4> d() {
        String c = Um5.c(PortfolioApp.get.instance(), 2131886319);
        Wg6.b(c, "LanguageHelper.getString\u2026enu_List__AboutChallenge)");
        Gs4 gs4 = new Gs4(c, Vy4.ABOUT, false, 4, null);
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886321);
        Wg6.b(c2, "LanguageHelper.getString\u2026ist__ViewFullLeaderboard)");
        Gs4 gs42 = new Gs4(c2, Vy4.LEADER_BOARD, false, 4, null);
        String c3 = Um5.c(PortfolioApp.get.instance(), 2131886320);
        Wg6.b(c3, "LanguageHelper.getString\u2026enu_List__LeaveChallenge)");
        return Hm7.i(gs4, gs42, new Gs4(c3, Vy4.LEAVE, false, 4, null));
    }

    @DexIgnore
    public final List<Gs4> e() {
        String c = Um5.c(PortfolioApp.get.instance(), 2131886200);
        Wg6.b(c, "LanguageHelper.getString\u2026low_List__LeaveChallenge)");
        return Hm7.i(new Gs4(c, Vy4.LEAVE, false, 4, null));
    }

    @DexIgnore
    public final List<Gs4> f() {
        String c = Um5.c(PortfolioApp.get.instance(), 2131886199);
        Wg6.b(c, "LanguageHelper.getString\u2026ailable_Allow_List__Edit)");
        Gs4 gs4 = new Gs4(c, Vy4.EDIT, false, 4, null);
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886198);
        Wg6.b(c2, "LanguageHelper.getString\u2026e_Allow_List__AddFriends)");
        Gs4 gs42 = new Gs4(c2, Vy4.ADD_FRIENDS, false, 4, null);
        String c3 = Um5.c(PortfolioApp.get.instance(), 2131886200);
        Wg6.b(c3, "LanguageHelper.getString\u2026low_List__LeaveChallenge)");
        return Hm7.i(gs4, gs42, new Gs4(c3, Vy4.LEAVE, false, 4, null));
    }

    @DexIgnore
    public final List<Gs4> g() {
        String c = Um5.c(PortfolioApp.get.instance(), 2131886273);
        Wg6.b(c, "LanguageHelper.getString\u2026cySettings_List__Friends)");
        Gs4 gs4 = new Gs4(c, "public_with_friend", true);
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886274);
        Wg6.b(c2, "LanguageHelper.getString\u2026cySettings_List__Private)");
        return Hm7.i(gs4, new Gs4(c2, "private", false));
    }

    @DexIgnore
    public final List<Gs4> h() {
        ArrayList arrayList = new ArrayList();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("BCGenerator", "soonTime - phoneDate: " + new Date() + " - exactDate: " + Xy4.a.a());
        Calendar instance = Calendar.getInstance();
        Date a2 = Xy4.a.a();
        Wg6.b(instance, "calendar");
        instance.setTime(a2);
        instance.set(12, (((instance.get(12) + 5) / 15) + 1) * 15);
        Date time = instance.getTime();
        Wg6.b(time, "calendar.time");
        arrayList.add(time);
        instance.add(12, 15);
        Date time2 = instance.getTime();
        Wg6.b(time2, "calendar.time");
        arrayList.add(time2);
        instance.add(12, 15);
        Date time3 = instance.getTime();
        Wg6.b(time3, "calendar.time");
        arrayList.add(time3);
        return Py4.e(arrayList, a2);
    }
}
