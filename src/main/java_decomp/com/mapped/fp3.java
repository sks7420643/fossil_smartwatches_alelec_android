package com.mapped;

import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.A34;
import com.fossil.A64;
import com.fossil.B64;
import com.fossil.C64;
import com.fossil.D14;
import com.fossil.E64;
import com.fossil.F64;
import com.fossil.H34;
import com.fossil.I14;
import com.fossil.I44;
import com.fossil.J14;
import com.fossil.K24;
import com.fossil.Q24;
import com.fossil.W54;
import com.fossil.X34;
import com.fossil.Y24;
import com.fossil.Y54;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Fp3<T> extends A64<T> implements Serializable {
    @DexIgnore
    public /* final */ Type runtimeType;
    @DexIgnore
    public transient C64 typeResolver;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Y54.Bi<T> {
        @DexIgnore
        public Ai(Method method) {
            super(method);
        }

        @DexIgnore
        @Override // com.fossil.X54
        public Fp3<T> a() {
            return Fp3.this;
        }

        @DexIgnore
        @Override // com.fossil.X54
        public String toString() {
            return a() + CodelessMatcher.CURRENT_CLASS_NAME + super.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Y54.Ai<T> {
        @DexIgnore
        public Bi(Constructor constructor) {
            super(constructor);
        }

        @DexIgnore
        @Override // com.fossil.X54
        public Fp3<T> a() {
            return Fp3.this;
        }

        @DexIgnore
        @Override // com.fossil.Y54.Ai
        public Type[] b() {
            return Fp3.this.resolveInPlace(super.b());
        }

        @DexIgnore
        @Override // com.fossil.X54
        public String toString() {
            return a() + "(" + D14.i(", ").g(b()) + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends E64 {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void c(GenericArrayType genericArrayType) {
            a(genericArrayType.getGenericComponentType());
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void d(ParameterizedType parameterizedType) {
            a(parameterizedType.getActualTypeArguments());
            a(parameterizedType.getOwnerType());
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void e(TypeVariable<?> typeVariable) {
            throw new IllegalArgumentException(Fp3.this.runtimeType + "contains a type variable and is not safe for the operation");
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void f(WildcardType wildcardType) {
            a(wildcardType.getLowerBounds());
            a(wildcardType.getUpperBounds());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends E64 {
        @DexIgnore
        public /* final */ /* synthetic */ H34.Ai b;

        @DexIgnore
        public Di(Fp3 fp3, H34.Ai ai) {
            this.b = ai;
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void b(Class<?> cls) {
            this.b.g(cls);
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void c(GenericArrayType genericArrayType) {
            this.b.g(F64.i(Fp3.of(genericArrayType.getGenericComponentType()).getRawType()));
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void d(ParameterizedType parameterizedType) {
            this.b.g((Class) parameterizedType.getRawType());
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void e(TypeVariable<?> typeVariable) {
            a(typeVariable.getBounds());
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void f(WildcardType wildcardType) {
            a(wildcardType.getUpperBounds());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei {
        @DexIgnore
        public /* final */ Type[] a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public Ei(Type[] typeArr, boolean z) {
            this.a = typeArr;
            this.b = z;
        }

        @DexIgnore
        public boolean a(Type type) {
            for (Type type2 : this.a) {
                boolean isSubtypeOf = Fp3.of(type2).isSubtypeOf(type);
                boolean z = this.b;
                if (isSubtypeOf == z) {
                    return z;
                }
            }
            return !this.b;
        }

        @DexIgnore
        public boolean b(Type type) {
            Fp3<?> of = Fp3.of(type);
            for (Type type2 : this.a) {
                boolean isSubtypeOf = of.isSubtypeOf(type2);
                boolean z = this.b;
                if (isSubtypeOf == z) {
                    return z;
                }
            }
            return !this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Fi extends Fp3<T>.k {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient H34<Fp3<? super T>> c;

        @DexIgnore
        public Fi() {
            super();
        }

        @DexIgnore
        public /* synthetic */ Fi(Fp3 fp3, Ai ai) {
            this();
        }

        @DexIgnore
        private Object readResolve() {
            return Fp3.this.getTypes().classes();
        }

        @DexIgnore
        public Fp3<T>.k classes() {
            return this;
        }

        @DexIgnore
        public Set<Fp3<? super T>> delegate() {
            H34<Fp3<? super T>> h34 = this.c;
            if (h34 != null) {
                return h34;
            }
            H34<Fp3<? super T>> d = K24.b(Ii.a.a().d(Fp3.this)).a(Ji.IGNORE_TYPE_VARIABLE_OR_WILDCARD).d();
            this.c = d;
            return d;
        }

        @DexIgnore
        public Fp3<T>.k interfaces() {
            throw new UnsupportedOperationException("classes().interfaces() not supported.");
        }

        @DexIgnore
        public Set<Class<? super T>> rawTypes() {
            return H34.copyOf((Collection) Ii.b.a().c(Fp3.this.getRawTypes()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Gi extends Fp3<T>.k {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ transient Fp3<T>.k c;
        @DexIgnore
        public transient H34<Fp3<? super T>> d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements J14<Class<?>> {
            @DexIgnore
            public Aii(Gi gi) {
            }

            @DexIgnore
            public boolean a(Class<?> cls) {
                return cls.isInterface();
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.J14
            public /* bridge */ /* synthetic */ boolean apply(Class<?> cls) {
                return a(cls);
            }
        }

        @DexIgnore
        public Gi(Fp3<T>.k kVar) {
            super();
            this.c = kVar;
        }

        @DexIgnore
        private Object readResolve() {
            return Fp3.this.getTypes().interfaces();
        }

        @DexIgnore
        public Fp3<T>.k classes() {
            throw new UnsupportedOperationException("interfaces().classes() not supported.");
        }

        @DexIgnore
        public Set<Fp3<? super T>> delegate() {
            H34<Fp3<? super T>> h34 = this.d;
            if (h34 != null) {
                return h34;
            }
            H34<Fp3<? super T>> d2 = K24.b(this.c).a(Ji.INTERFACE_ONLY).d();
            this.d = d2;
            return d2;
        }

        @DexIgnore
        public Fp3<T>.k interfaces() {
            return this;
        }

        @DexIgnore
        public Set<Class<? super T>> rawTypes() {
            return K24.b(Ii.b.c(Fp3.this.getRawTypes())).a(new Aii(this)).d();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> extends Fp3<T> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;

        @DexIgnore
        public Hi(Type type) {
            super(type, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ii<K> {
        @DexIgnore
        public static /* final */ Ii<Fp3<?>> a; // = new Aii();
        @DexIgnore
        public static /* final */ Ii<Class<?>> b; // = new Bii();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ii<Fp3<?>> {
            @DexIgnore
            public Aii() {
                super(null);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Iterable' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Fp3.Ii
            public /* bridge */ /* synthetic */ Iterable<? extends Fp3<?>> e(Fp3<?> fp3) {
                return i(fp3);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Fp3.Ii
            public /* bridge */ /* synthetic */ Class f(Fp3<?> fp3) {
                return j(fp3);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Fp3.Ii
            public /* bridge */ /* synthetic */ Fp3<?> g(Fp3<?> fp3) {
                return k(fp3);
            }

            @DexIgnore
            public Iterable<? extends Fp3<?>> i(Fp3<?> fp3) {
                return fp3.getGenericInterfaces();
            }

            @DexIgnore
            public Class<?> j(Fp3<?> fp3) {
                return fp3.getRawType();
            }

            @DexIgnore
            public Fp3<?> k(Fp3<?> fp3) {
                return fp3.getGenericSuperclass();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii extends Ii<Class<?>> {
            @DexIgnore
            public Bii() {
                super(null);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Iterable' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Fp3.Ii
            public /* bridge */ /* synthetic */ Iterable<? extends Class<?>> e(Class<?> cls) {
                return i(cls);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Fp3.Ii
            public /* bridge */ /* synthetic */ Class f(Class<?> cls) {
                Class<?> cls2 = cls;
                j(cls2);
                return cls2;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Fp3.Ii
            public /* bridge */ /* synthetic */ Class<?> g(Class<?> cls) {
                return k(cls);
            }

            @DexIgnore
            public Iterable<? extends Class<?>> i(Class<?> cls) {
                return Arrays.asList(cls.getInterfaces());
            }

            @DexIgnore
            public Class<?> j(Class<?> cls) {
                return cls;
            }

            @DexIgnore
            public Class<?> k(Class<?> cls) {
                return cls.getSuperclass();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Cii extends Eii<K> {
            @DexIgnore
            public Cii(Ii ii, Ii ii2) {
                super(ii2);
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.mapped.Fp3$Ii$Cii */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.mapped.Fp3.Ii
            public Y24<K> c(Iterable<? extends K> iterable) {
                Y24.Bi builder = Y24.builder();
                for (Object obj : iterable) {
                    if (!f(obj).isInterface()) {
                        builder.g(obj);
                    }
                }
                return super.c(builder.i());
            }

            @DexIgnore
            @Override // com.mapped.Fp3.Ii
            public Iterable<? extends K> e(K k) {
                return H34.of();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Dii extends I44<K> {
            @DexIgnore
            public /* final */ /* synthetic */ Comparator b;
            @DexIgnore
            public /* final */ /* synthetic */ Map c;

            @DexIgnore
            public Dii(Comparator comparator, Map map) {
                this.b = comparator;
                this.c = map;
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.util.Comparator */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.I44, java.util.Comparator
            public int compare(K k, K k2) {
                return this.b.compare(this.c.get(k), this.c.get(k2));
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Eii<K> extends Ii<K> {
            @DexIgnore
            public /* final */ Ii<K> c;

            @DexIgnore
            public Eii(Ii<K> ii) {
                super(null);
                this.c = ii;
            }

            @DexIgnore
            @Override // com.mapped.Fp3.Ii
            public Class<?> f(K k) {
                return this.c.f(k);
            }

            @DexIgnore
            @Override // com.mapped.Fp3.Ii
            public K g(K k) {
                return this.c.g(k);
            }
        }

        @DexIgnore
        public Ii() {
        }

        @DexIgnore
        public /* synthetic */ Ii(Ai ai) {
            this();
        }

        @DexIgnore
        public static <K, V> Y24<K> h(Map<K, V> map, Comparator<? super V> comparator) {
            return new Dii(comparator, map).immutableSortedCopy(map.keySet());
        }

        @DexIgnore
        public final Ii<K> a() {
            return new Cii(this, this);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r0v11, resolved type: int */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public final int b(K k, Map<? super K, Integer> map) {
            Integer num = map.get(k);
            if (num != null) {
                return num.intValue();
            }
            boolean isInterface = f(k).isInterface();
            Iterator<? extends K> it = e(k).iterator();
            int i = isInterface;
            while (it.hasNext()) {
                i = Math.max(i, b(it.next(), map));
            }
            K g = g(k);
            int i2 = i;
            if (g != null) {
                i2 = Math.max(i, b(g, map));
            }
            int i3 = (i2 == 1 ? 1 : 0) + 1;
            map.put(k, Integer.valueOf(i3));
            return i3;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.mapped.Fp3$Ii<K> */
        /* JADX WARN: Multi-variable type inference failed */
        public Y24<K> c(Iterable<? extends K> iterable) {
            HashMap j = X34.j();
            Iterator<? extends K> it = iterable.iterator();
            while (it.hasNext()) {
                b(it.next(), j);
            }
            return h(j, I44.natural().reverse());
        }

        @DexIgnore
        public final Y24<K> d(K k) {
            return c(Y24.of(k));
        }

        @DexIgnore
        public abstract Iterable<? extends K> e(K k);

        @DexIgnore
        public abstract Class<?> f(K k);

        @DexIgnore
        public abstract K g(K k);
    }

    @DexIgnore
    public enum Ji implements J14<Fp3<?>> {
        IGNORE_TYPE_VARIABLE_OR_WILDCARD {
            @DexIgnore
            public boolean apply(Fp3<?> fp3) {
                return !(fp3.runtimeType instanceof TypeVariable) && !(fp3.runtimeType instanceof WildcardType);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Fp3.Ji, com.fossil.J14
            public /* bridge */ /* synthetic */ boolean apply(Fp3<?> fp3) {
                return apply(fp3);
            }
        },
        INTERFACE_ONLY {
            @DexIgnore
            public boolean apply(Fp3<?> fp3) {
                return fp3.getRawType().isInterface();
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Fp3.Ji, com.fossil.J14
            public /* bridge */ /* synthetic */ boolean apply(Fp3<?> fp3) {
                return apply(fp3);
            }
        };

        @DexIgnore
        public /* synthetic */ Ji(Ai ai) {
            this();
        }

        @DexIgnore
        @Override // com.fossil.J14
        @CanIgnoreReturnValue
        public abstract /* synthetic */ boolean apply(T t);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ki extends Q24<Fp3<? super T>> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient H34<Fp3<? super T>> b;

        @DexIgnore
        public Ki() {
        }

        @DexIgnore
        public Fp3<T>.k classes() {
            return new Fi(Fp3.this, null);
        }

        @DexIgnore
        @Override // com.fossil.L24, com.fossil.L24, com.fossil.O24, com.fossil.Q24, com.fossil.Q24, com.fossil.Q24
        public Set<Fp3<? super T>> delegate() {
            H34<Fp3<? super T>> h34 = this.b;
            if (h34 != null) {
                return h34;
            }
            H34<Fp3<? super T>> d = K24.b(Ii.a.d(Fp3.this)).a(Ji.IGNORE_TYPE_VARIABLE_OR_WILDCARD).d();
            this.b = d;
            return d;
        }

        @DexIgnore
        public Fp3<T>.k interfaces() {
            return new Gi(this);
        }

        @DexIgnore
        public Set<Class<? super T>> rawTypes() {
            return H34.copyOf((Collection) Ii.b.c(Fp3.this.getRawTypes()));
        }
    }

    @DexIgnore
    public Fp3() {
        Type capture = capture();
        this.runtimeType = capture;
        I14.v(!(capture instanceof TypeVariable), "Cannot construct a TypeToken for a type variable.\nYou probably meant to call new TypeToken<%s>(getClass()) that can resolve the type variable for you.\nIf you do need to create a TypeToken of a type variable, please use TypeToken.of() instead.", capture);
    }

    @DexIgnore
    public Fp3(Class<?> cls) {
        Type capture = super.capture();
        if (capture instanceof Class) {
            this.runtimeType = capture;
        } else {
            this.runtimeType = of((Class) cls).resolveType(capture).runtimeType;
        }
    }

    @DexIgnore
    public Fp3(Type type) {
        I14.l(type);
        this.runtimeType = type;
    }

    @DexIgnore
    public /* synthetic */ Fp3(Type type, Ai ai) {
        this(type);
    }

    @DexIgnore
    public static Ei any(Type[] typeArr) {
        return new Ei(typeArr, true);
    }

    @DexIgnore
    private Fp3<? super T> boundAsSuperclass(Type type) {
        Fp3<? super T> fp3 = (Fp3<? super T>) of(type);
        if (fp3.getRawType().isInterface()) {
            return null;
        }
        return fp3;
    }

    @DexIgnore
    private Y24<Fp3<? super T>> boundsAsInterfaces(Type[] typeArr) {
        Y24.Bi builder = Y24.builder();
        for (Type type : typeArr) {
            Fp3<?> of = of(type);
            if (of.getRawType().isInterface()) {
                builder.g(of);
            }
        }
        return builder.i();
    }

    @DexIgnore
    public static Ei every(Type[] typeArr) {
        return new Ei(typeArr, false);
    }

    @DexIgnore
    private Fp3<? extends T> getArraySubtype(Class<?> cls) {
        return (Fp3<? extends T>) of(newArrayClassOrGenericArrayType(getComponentType().getSubtype(cls.getComponentType()).runtimeType));
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r1v1. Raw type applied. Possible types: java.lang.Class<?>, java.lang.Class<? super ?> */
    private Fp3<? super T> getArraySupertype(Class<? super T> cls) {
        Fp3<?> componentType = getComponentType();
        I14.n(componentType, "%s isn't a super type of %s", cls, this);
        return (Fp3<? super T>) of(newArrayClassOrGenericArrayType(componentType.getSupertype(cls.getComponentType()).runtimeType));
    }

    @DexIgnore
    private Type getOwnerTypeIfPresent() {
        Type type = this.runtimeType;
        if (type instanceof ParameterizedType) {
            return ((ParameterizedType) type).getOwnerType();
        }
        if (type instanceof Class) {
            return ((Class) type).getEnclosingClass();
        }
        return null;
    }

    @DexIgnore
    private H34<Class<? super T>> getRawTypes() {
        H34.Ai builder = H34.builder();
        new Di(this, builder).a(this.runtimeType);
        return builder.j();
    }

    @DexIgnore
    private Fp3<? extends T> getSubtypeFromLowerBounds(Class<?> cls, Type[] typeArr) {
        if (typeArr.length > 0) {
            return (Fp3<? extends T>) of(typeArr[0]).getSubtype(cls);
        }
        throw new IllegalArgumentException(cls + " isn't a subclass of " + this);
    }

    @DexIgnore
    private Fp3<? super T> getSupertypeFromUpperBounds(Class<? super T> cls, Type[] typeArr) {
        for (Type type : typeArr) {
            Fp3<?> of = of(type);
            if (of.isSubtypeOf(cls)) {
                return (Fp3<? super T>) of.getSupertype(cls);
            }
        }
        throw new IllegalArgumentException(cls + " isn't a super type of " + this);
    }

    @DexIgnore
    private boolean is(Type type) {
        if (this.runtimeType.equals(type)) {
            return true;
        }
        if (!(type instanceof WildcardType)) {
            return false;
        }
        WildcardType wildcardType = (WildcardType) type;
        return every(wildcardType.getUpperBounds()).b(this.runtimeType) && every(wildcardType.getLowerBounds()).a(this.runtimeType);
    }

    @DexIgnore
    private boolean isOwnedBySubtypeOf(Type type) {
        Iterator it = getTypes().iterator();
        while (it.hasNext()) {
            Type ownerTypeIfPresent = ((Fp3) it.next()).getOwnerTypeIfPresent();
            if (ownerTypeIfPresent != null && of(ownerTypeIfPresent).isSubtypeOf(type)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    private boolean isSubtypeOfArrayType(GenericArrayType genericArrayType) {
        Type type = this.runtimeType;
        if (type instanceof Class) {
            Class cls = (Class) type;
            if (!cls.isArray()) {
                return false;
            }
            return of((Class) cls.getComponentType()).isSubtypeOf(genericArrayType.getGenericComponentType());
        } else if (type instanceof GenericArrayType) {
            return of(((GenericArrayType) type).getGenericComponentType()).isSubtypeOf(genericArrayType.getGenericComponentType());
        } else {
            return false;
        }
    }

    @DexIgnore
    private boolean isSubtypeOfParameterizedType(ParameterizedType parameterizedType) {
        Class<? super Object> rawType = of(parameterizedType).getRawType();
        if (!someRawTypeIsSubclassOf(rawType)) {
            return false;
        }
        TypeVariable<Class<? super Object>>[] typeParameters = rawType.getTypeParameters();
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        for (int i = 0; i < typeParameters.length; i++) {
            if (!resolveType(typeParameters[i]).is(actualTypeArguments[i])) {
                return false;
            }
        }
        return Modifier.isStatic(((Class) parameterizedType.getRawType()).getModifiers()) || parameterizedType.getOwnerType() == null || isOwnedBySubtypeOf(parameterizedType.getOwnerType());
    }

    @DexIgnore
    private boolean isSupertypeOfArray(GenericArrayType genericArrayType) {
        Type type = this.runtimeType;
        if (type instanceof Class) {
            Class cls = (Class) type;
            return !cls.isArray() ? cls.isAssignableFrom(Object[].class) : of(genericArrayType.getGenericComponentType()).isSubtypeOf(cls.getComponentType());
        } else if (type instanceof GenericArrayType) {
            return of(genericArrayType.getGenericComponentType()).isSubtypeOf(((GenericArrayType) this.runtimeType).getGenericComponentType());
        } else {
            return false;
        }
    }

    @DexIgnore
    private boolean isWrapper() {
        return W54.b().contains(this.runtimeType);
    }

    @DexIgnore
    public static Type newArrayClassOrGenericArrayType(Type type) {
        return F64.Ei.JAVA7.newArrayType(type);
    }

    @DexIgnore
    public static <T> Fp3<T> of(Class<T> cls) {
        return new Hi(cls);
    }

    @DexIgnore
    public static Fp3<?> of(Type type) {
        return new Hi(type);
    }

    @DexIgnore
    private Type[] resolveInPlace(Type[] typeArr) {
        for (int i = 0; i < typeArr.length; i++) {
            typeArr[i] = resolveType(typeArr[i]).getType();
        }
        return typeArr;
    }

    @DexIgnore
    private Fp3<?> resolveSupertype(Type type) {
        Fp3<?> resolveType = resolveType(type);
        resolveType.typeResolver = this.typeResolver;
        return resolveType;
    }

    @DexIgnore
    private Type resolveTypeArgsForSubclass(Class<?> cls) {
        if ((this.runtimeType instanceof Class) && (cls.getTypeParameters().length == 0 || getRawType().getTypeParameters().length != 0)) {
            return cls;
        }
        Fp3 genericType = toGenericType(cls);
        return new C64().l(genericType.getSupertype(getRawType()).runtimeType, this.runtimeType).i(genericType.runtimeType);
    }

    @DexIgnore
    private boolean someRawTypeIsSubclassOf(Class<?> cls) {
        Iterator it = getRawTypes().iterator();
        while (it.hasNext()) {
            if (cls.isAssignableFrom((Class) it.next())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static <T> Fp3<? extends T> toGenericType(Class<T> cls) {
        if (cls.isArray()) {
            return (Fp3<? extends T>) of(F64.k(toGenericType(cls.getComponentType()).runtimeType));
        }
        TypeVariable<Class<T>>[] typeParameters = cls.getTypeParameters();
        Type type = (!cls.isMemberClass() || Modifier.isStatic(cls.getModifiers())) ? null : toGenericType(cls.getEnclosingClass()).runtimeType;
        return (typeParameters.length > 0 || !(type == null || type == cls.getEnclosingClass())) ? (Fp3<? extends T>) of(F64.n(type, cls, typeParameters)) : of((Class) cls);
    }

    @DexIgnore
    public final Y54<T, T> constructor(Constructor<?> constructor) {
        I14.i(constructor.getDeclaringClass() == getRawType(), "%s not declared by %s", constructor, getRawType());
        return new Bi(constructor);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof Fp3) {
            return this.runtimeType.equals(((Fp3) obj).runtimeType);
        }
        return false;
    }

    @DexIgnore
    public final Fp3<?> getComponentType() {
        Type j = F64.j(this.runtimeType);
        if (j == null) {
            return null;
        }
        return of(j);
    }

    @DexIgnore
    public final Y24<Fp3<? super T>> getGenericInterfaces() {
        Type type = this.runtimeType;
        if (type instanceof TypeVariable) {
            return boundsAsInterfaces(((TypeVariable) type).getBounds());
        }
        if (type instanceof WildcardType) {
            return boundsAsInterfaces(((WildcardType) type).getUpperBounds());
        }
        Y24.Bi builder = Y24.builder();
        for (Type type2 : getRawType().getGenericInterfaces()) {
            builder.g(resolveSupertype(type2));
        }
        return builder.i();
    }

    @DexIgnore
    public final Fp3<? super T> getGenericSuperclass() {
        Type type = this.runtimeType;
        if (type instanceof TypeVariable) {
            return boundAsSuperclass(((TypeVariable) type).getBounds()[0]);
        }
        if (type instanceof WildcardType) {
            return boundAsSuperclass(((WildcardType) type).getUpperBounds()[0]);
        }
        Type genericSuperclass = getRawType().getGenericSuperclass();
        if (genericSuperclass == null) {
            return null;
        }
        return (Fp3<? super T>) resolveSupertype(genericSuperclass);
    }

    @DexIgnore
    public final Class<? super T> getRawType() {
        return getRawTypes().iterator().next();
    }

    @DexIgnore
    public final Fp3<? extends T> getSubtype(Class<?> cls) {
        I14.h(!(this.runtimeType instanceof TypeVariable), "Cannot get subtype of type variable <%s>", this);
        Type type = this.runtimeType;
        if (type instanceof WildcardType) {
            return getSubtypeFromLowerBounds(cls, ((WildcardType) type).getLowerBounds());
        }
        if (isArray()) {
            return getArraySubtype(cls);
        }
        I14.i(getRawType().isAssignableFrom(cls), "%s isn't a subclass of %s", cls, this);
        return (Fp3<? extends T>) of(resolveTypeArgsForSubclass(cls));
    }

    @DexIgnore
    public final Fp3<? super T> getSupertype(Class<? super T> cls) {
        I14.i(someRawTypeIsSubclassOf(cls), "%s is not a super class of %s", cls, this);
        Type type = this.runtimeType;
        return type instanceof TypeVariable ? getSupertypeFromUpperBounds(cls, ((TypeVariable) type).getBounds()) : type instanceof WildcardType ? getSupertypeFromUpperBounds(cls, ((WildcardType) type).getUpperBounds()) : cls.isArray() ? getArraySupertype(cls) : (Fp3<? super T>) resolveSupertype(toGenericType(cls).runtimeType);
    }

    @DexIgnore
    public final Type getType() {
        return this.runtimeType;
    }

    @DexIgnore
    public final Fp3<T>.k getTypes() {
        return new Ki();
    }

    @DexIgnore
    public int hashCode() {
        return this.runtimeType.hashCode();
    }

    @DexIgnore
    public final boolean isArray() {
        return getComponentType() != null;
    }

    @DexIgnore
    public final boolean isPrimitive() {
        Type type = this.runtimeType;
        return (type instanceof Class) && ((Class) type).isPrimitive();
    }

    @DexIgnore
    public final boolean isSubtypeOf(Fp3<?> fp3) {
        return isSubtypeOf(fp3.getType());
    }

    @DexIgnore
    public final boolean isSubtypeOf(Type type) {
        I14.l(type);
        if (type instanceof WildcardType) {
            return any(((WildcardType) type).getLowerBounds()).b(this.runtimeType);
        }
        Type type2 = this.runtimeType;
        if (type2 instanceof WildcardType) {
            return any(((WildcardType) type2).getUpperBounds()).a(type);
        }
        if (type2 instanceof TypeVariable) {
            return type2.equals(type) || any(((TypeVariable) this.runtimeType).getBounds()).a(type);
        }
        if (type2 instanceof GenericArrayType) {
            return of(type).isSupertypeOfArray((GenericArrayType) this.runtimeType);
        }
        if (type instanceof Class) {
            return someRawTypeIsSubclassOf((Class) type);
        }
        if (type instanceof ParameterizedType) {
            return isSubtypeOfParameterizedType((ParameterizedType) type);
        }
        if (type instanceof GenericArrayType) {
            return isSubtypeOfArrayType((GenericArrayType) type);
        }
        return false;
    }

    @DexIgnore
    public final boolean isSupertypeOf(Fp3<?> fp3) {
        return fp3.isSubtypeOf(getType());
    }

    @DexIgnore
    public final boolean isSupertypeOf(Type type) {
        return of(type).isSubtypeOf(getType());
    }

    @DexIgnore
    public final Y54<T, Object> method(Method method) {
        I14.i(someRawTypeIsSubclassOf(method.getDeclaringClass()), "%s not declared by %s", method, this);
        return new Ai(method);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public final Fp3<T> rejectTypeVariables() {
        new Ci().a(this.runtimeType);
        return this;
    }

    @DexIgnore
    public final Fp3<?> resolveType(Type type) {
        I14.l(type);
        C64 c64 = this.typeResolver;
        if (c64 == null) {
            c64 = C64.d(this.runtimeType);
            this.typeResolver = c64;
        }
        return of(c64.i(type));
    }

    @DexIgnore
    public String toString() {
        return F64.t(this.runtimeType);
    }

    @DexIgnore
    public final Fp3<T> unwrap() {
        return isWrapper() ? of(W54.c((Class) this.runtimeType)) : this;
    }

    @DexIgnore
    public final <X> Fp3<T> where(B64<X> b64, Fp3<X> fp3) {
        return new Hi(new C64().m(A34.of(new C64.Di(b64.a), fp3.runtimeType)).i(this.runtimeType));
    }

    @DexIgnore
    public final <X> Fp3<T> where(B64<X> b64, Class<X> cls) {
        return where(b64, of((Class) cls));
    }

    @DexIgnore
    public final Fp3<T> wrap() {
        return isPrimitive() ? of(W54.d((Class) this.runtimeType)) : this;
    }

    @DexIgnore
    public Object writeReplace() {
        return of(new C64().i(this.runtimeType));
    }
}
