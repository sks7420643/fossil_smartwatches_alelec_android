package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X82 implements A72<Status> {
    @DexIgnore
    public /* final */ /* synthetic */ V72 a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ R62 c;
    @DexIgnore
    public /* final */ /* synthetic */ T82 d;

    @DexIgnore
    public X82(T82 t82, V72 v72, boolean z, R62 r62) {
        this.d = t82;
        this.a = v72;
        this.b = z;
        this.c = r62;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.Z62] */
    @Override // com.fossil.A72
    public final /* synthetic */ void a(Status status) {
        Status status2 = status;
        O42.b(this.d.g).l();
        if (status2.D() && this.d.n()) {
            this.d.v();
        }
        this.a.j(status2);
        if (this.b) {
            this.c.g();
        }
    }
}
