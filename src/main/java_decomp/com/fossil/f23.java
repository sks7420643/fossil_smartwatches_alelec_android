package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F23 implements N23 {
    @DexIgnore
    public N23[] a;

    @DexIgnore
    public F23(N23... n23Arr) {
        this.a = n23Arr;
    }

    @DexIgnore
    @Override // com.fossil.N23
    public final boolean zza(Class<?> cls) {
        for (N23 n23 : this.a) {
            if (n23.zza(cls)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.N23
    public final K23 zzb(Class<?> cls) {
        N23[] n23Arr = this.a;
        for (N23 n23 : n23Arr) {
            if (n23.zza(cls)) {
                return n23.zzb(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }
}
