package com.fossil;

import com.fossil.Hz1;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Nz1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public Ai a(int i) {
            e(Integer.valueOf(i));
            return this;
        }

        @DexIgnore
        public abstract Ai b(long j);

        @DexIgnore
        public abstract Ai c(Lz1 lz1);

        @DexIgnore
        public abstract Ai d(Qz1 qz1);

        @DexIgnore
        public abstract Ai e(Integer num);

        @DexIgnore
        public abstract Ai f(String str);

        @DexIgnore
        public abstract Ai g(List<Mz1> list);

        @DexIgnore
        public abstract Nz1 h();

        @DexIgnore
        public abstract Ai i(long j);

        @DexIgnore
        public Ai j(String str) {
            f(str);
            return this;
        }
    }

    @DexIgnore
    public static Ai a() {
        return new Hz1.Bi();
    }

    @DexIgnore
    public abstract Lz1 b();

    @DexIgnore
    public abstract List<Mz1> c();

    @DexIgnore
    public abstract Integer d();

    @DexIgnore
    public abstract String e();

    @DexIgnore
    public abstract Qz1 f();

    @DexIgnore
    public abstract long g();

    @DexIgnore
    public abstract long h();
}
