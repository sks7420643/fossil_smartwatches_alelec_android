package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.SharePreferencesUtils;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class on5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ SharedPreferences f2700a;
    @DexIgnore
    public /* final */ SharedPreferences b;
    @DexIgnore
    public /* final */ SharedPreferences c;
    @DexIgnore
    public /* final */ SharedPreferences d;
    @DexIgnore
    public /* final */ SharedPreferences e;
    @DexIgnore
    public /* final */ SharedPreferences f;
    @DexIgnore
    public /* final */ SharedPreferences g;
    @DexIgnore
    public /* final */ SharedPreferences h;
    @DexIgnore
    public /* final */ SharedPreferences i;
    @DexIgnore
    public /* final */ SharedPreferences j;
    @DexIgnore
    public /* final */ SharedPreferences.Editor k;
    @DexIgnore
    public /* final */ SharedPreferences l;
    @DexIgnore
    public /* final */ SharedPreferences m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends TypeToken<List<String>> {
        @DexIgnore
        public a(on5 on5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends TypeToken<List<String>> {
        @DexIgnore
        public b(on5 on5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends TypeToken<List<String>> {
        @DexIgnore
        public c(on5 on5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends TypeToken<List<String>> {
        @DexIgnore
        public d(on5 on5) {
        }
    }

    @DexIgnore
    public on5(Context context) {
        this.f2700a = context.getSharedPreferences("PREFERENCE_DEVICE", 0);
        this.b = context.getSharedPreferences("PREFERENCE_AUTH", 0);
        this.c = context.getSharedPreferences("PREFERENCE_ONBOARDING", 0);
        this.d = context.getSharedPreferences("PREFERENCE_MIGRATE", 0);
        this.e = context.getSharedPreferences("PREFERENCE_BUDDY_CHALLENGE", 0);
        this.f = context.getSharedPreferences("PREFERENCE_CONNECTED_APPS", 0);
        this.m = context.getSharedPreferences("project_fossil_wearables_fossil", 0);
        this.g = context.getSharedPreferences("PREFERENCE_APPS", 0);
        this.h = context.getSharedPreferences("PREFERENCE_USER", 0);
        this.i = context.getSharedPreferences("PREFERENCE_FIREBASE", 0);
        this.j = context.getSharedPreferences("PREFERENCE_QUICK_RESPONSE", 0);
        SharedPreferences sharedPreferences = context.getSharedPreferences("PREFERENCE_PERMISSION", 0);
        this.l = sharedPreferences;
        this.k = sharedPreferences.edit();
    }

    @DexIgnore
    public long A(String str) {
        SharedPreferences sharedPreferences = this.f2700a;
        return F(sharedPreferences, "lastReminderSyncTimeSuccess" + str, 0);
    }

    @DexIgnore
    public final void A0(SharedPreferences sharedPreferences, String str) {
        sharedPreferences.edit().remove(str).apply();
    }

    @DexIgnore
    public void A1(long j2, String str) {
        SharedPreferences sharedPreferences = this.f2700a;
        D1(sharedPreferences, "lastSyncTimeSuccess" + str, j2);
    }

    @DexIgnore
    public long B(String str) {
        SharedPreferences sharedPreferences = this.f2700a;
        return F(sharedPreferences, "lastSyncTime" + str, 0);
    }

    @DexIgnore
    public void B0(String str) {
        SharedPreferences sharedPreferences = this.f2700a;
        A0(sharedPreferences, "tempDeviceFirmwareInfo_" + str);
    }

    @DexIgnore
    public void B1(String str) {
        T1(this.g, "KEY_LOCALE_VERSION", str);
    }

    @DexIgnore
    public long C(String str) {
        SharedPreferences sharedPreferences = this.f2700a;
        return F(sharedPreferences, "lastSyncTimeSuccess" + str, 0);
    }

    @DexIgnore
    public void C0() {
        this.f2700a.edit().clear().apply();
        this.b.edit().clear().apply();
        this.c.edit().clear().apply();
        this.e.edit().clear().apply();
        this.h.edit().clear().apply();
        this.i.edit().clear().apply();
        D0();
    }

    @DexIgnore
    public void C1(String str, String str2) {
        SharedPreferences sharedPreferences = this.g;
        T1(sharedPreferences, "localization_" + str2, str);
    }

    @DexIgnore
    public String D() {
        return O(this.g, "KEY_LOCALE_VERSION", "");
    }

    @DexIgnore
    public void D0() {
        boolean n0 = n0();
        this.h.edit().clear().apply();
        H1(n0);
    }

    @DexIgnore
    public final void D1(SharedPreferences sharedPreferences, String str, long j2) {
        sharedPreferences.edit().putLong(str, j2).apply();
    }

    @DexIgnore
    public String E(String str) {
        SharedPreferences sharedPreferences = this.g;
        return O(sharedPreferences, "localization_" + str, "");
    }

    @DexIgnore
    public void E0(String str) {
        T1(this.f2700a, "active_device_address", str);
    }

    @DexIgnore
    public void E1(String str) {
        T1(this.f2700a, "KEY_LOW_BATTERY_DEVICE", str);
    }

    @DexIgnore
    public final long F(SharedPreferences sharedPreferences, String str, long j2) {
        return sharedPreferences.getLong(str, j2);
    }

    @DexIgnore
    public void F0(String str) {
        T1(this.f2700a, "installation_id_android", str);
    }

    @DexIgnore
    public void F1(List<String> list) {
        if (!list.isEmpty()) {
            T1(this.h, "KEY_MICRO_APP_SEARCHED_RECENT", new Gson().t(list));
        }
    }

    @DexIgnore
    public List<String> G() {
        String O = O(this.h, "KEY_MICRO_APP_SEARCHED_RECENT", "");
        if (TextUtils.isEmpty(O)) {
            return new ArrayList();
        }
        try {
            return (List) new Gson().l(O, new d(this).getType());
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SharedPreferencesManager", "getMicroAppSearchedIdsRecent - parse json failed, json=" + O + ", ex=" + e2);
            return new ArrayList();
        }
    }

    @DexIgnore
    public void G0(String str) {
        T1(this.h, "social_profile_id", str);
    }

    @DexIgnore
    public void G1(Boolean bool) {
        P0(this.g, "KEY_MIGRATE_PRESET_TO_WATCHFACE", bool.booleanValue());
    }

    @DexIgnore
    public boolean H() {
        return this.m.getBoolean("KEY_ENABLE_FIND_MY_DEVICE", false);
    }

    @DexIgnore
    public void H0(long j2) {
        D1(this.b, "ACCESS_TOKEN_TIME", j2);
    }

    @DexIgnore
    public void H1(boolean z) {
        P0(this.h, "KEY_SHOWING_RATING_PROMPT_CURRENT_VERSION", z);
    }

    @DexIgnore
    public String I() {
        return O(this.m, "KEY_LOW_BATTERY_DEVICE", "");
    }

    @DexIgnore
    public void I0(List<String> list) {
        if (!list.isEmpty()) {
            T1(this.h, "KEY_ADDRESS_SEARCHED_RECENT", new Gson().t(list));
        }
    }

    @DexIgnore
    public void I1(String str, Boolean bool) {
        SharedPreferences sharedPreferences = this.g;
        P0(sharedPreferences, "is_challenge_id_" + str, bool.booleanValue());
    }

    @DexIgnore
    public String J() {
        return this.m.getString("active_device_address", "");
    }

    @DexIgnore
    public void J0(boolean z) {
        P0(this.h, "KEY_ALL_APPS_TOGGLE_ENABLE", z);
    }

    @DexIgnore
    public void J1(boolean z) {
        P0(this.h, "KEY_NEW_SOLUTION_FOR_SMS_ENABLE", z);
    }

    @DexIgnore
    public int K() {
        return s(this.m, "KEY_DEBUG_REPLACE_BATTERY_LEVEL", 25);
    }

    @DexIgnore
    public void K0(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setAppDataVersion - appDataVersion=" + i2);
        l1(this.g, "KEY_USER_LAST_DATA_VERSION", i2);
    }

    @DexIgnore
    public void K1(Boolean bool) {
        P0(this.j, "KEY_QUICK_RESPONSE", bool.booleanValue());
    }

    @DexIgnore
    public String L() {
        return O(this.f2700a, "realTimeStepTimeStamp_", "");
    }

    @DexIgnore
    public void L0(String str) {
        T1(this.d, "KEY_USER_LAST_APP_VERSION_NAME_Q", str);
    }

    @DexIgnore
    public void L1(String str) {
        T1(this.f2700a, "realTimeStepTimeStamp_", str);
    }

    @DexIgnore
    public int M() {
        return s(this.f2700a, "KEY_DEBUG_REPLACE_BATTERY_LEVEL", 25);
    }

    @DexIgnore
    public void M0(String str) {
        T1(this.i, "KEY_AUTHORISED_FIREBASE_TOKEN", str);
    }

    @DexIgnore
    public void M1(boolean z) {
        P0(this.h, "KEY_REMINDERS_BEDTIME_REMINDER_ENABLE", z);
    }

    @DexIgnore
    public String N() {
        return O(this.h, "social_profile_id", "");
    }

    @DexIgnore
    public void N0(boolean z) {
        P0(this.f2700a, "AutoSyncEnable", z);
    }

    @DexIgnore
    public void N1(boolean z) {
        P0(this.h, "KEY_REMINDERS_INACTIVITY_NUDGE_ENABLE", z);
    }

    @DexIgnore
    public final String O(SharedPreferences sharedPreferences, String str, String str2) {
        return sharedPreferences.getString(str, str2);
    }

    @DexIgnore
    public void O0(Boolean bool) {
        P0(this.g, SharePreferencesUtils.BC_STATUS, bool.booleanValue());
    }

    @DexIgnore
    public void O1(boolean z) {
        P0(this.h, "KEY_REMINDERS_KEEP_GOING_ENABLE", z);
    }

    @DexIgnore
    public final String P(wh5 wh5, String str, String str2) {
        return wh5 instanceof jh5 ? this.g.getString(str, str2) : wh5 instanceof bi5 ? this.h.getString(str, str2) : "";
    }

    @DexIgnore
    public final void P0(SharedPreferences sharedPreferences, String str, boolean z) {
        sharedPreferences.edit().putBoolean(str, z).apply();
    }

    @DexIgnore
    public void P1(boolean z) {
        P0(this.h, "KEY_REMINDERS_SLEEP_SUMMARY_ENABLE", z);
    }

    @DexIgnore
    public String Q(String str) {
        SharedPreferences sharedPreferences = this.f2700a;
        return O(sharedPreferences, "tempDeviceFirmwareInfo_" + str, "");
    }

    @DexIgnore
    public void Q0(Firmware firmware, String str) {
        SharedPreferences sharedPreferences = this.f2700a;
        T1(sharedPreferences, "bundle_latest_firmware_data" + str, new Gson().t(firmware));
    }

    @DexIgnore
    public void Q1(int i2) {
        l1(this.f2700a, "KEY_DEBUG_REPLACE_BATTERY_LEVEL", i2);
    }

    @DexIgnore
    public String R() {
        return O(this.b, "USER_ACCESS_TOKEN", "");
    }

    @DexIgnore
    public void R0(List<String> list) {
        if (!list.isEmpty()) {
            T1(this.h, "KEY_COMPLICATION_SEARCHED_RECENT", new Gson().t(list));
        }
    }

    @DexIgnore
    public void R1(boolean z) {
        P0(this.f2700a, "showAllDevices", z);
    }

    @DexIgnore
    public int S(String str) {
        SharedPreferences sharedPreferences = this.f2700a;
        return s(sharedPreferences, "PREFS_VIBRATION_STRENGTH" + str, 25);
    }

    @DexIgnore
    public void S0(boolean z) {
        P0(this.f2700a, "consider_bundle_firmware_as_latest", z);
    }

    @DexIgnore
    public void S1(boolean z) {
        P0(this.f2700a, "skipOta", z);
    }

    @DexIgnore
    public List<String> T() {
        String O = O(this.h, "KEY_WATCH_APP_SEARCHED_RECENT", "");
        if (TextUtils.isEmpty(O)) {
            return new ArrayList();
        }
        try {
            return (List) new Gson().l(O, new c(this).getType());
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SharedPreferencesManager", "getWatchAppSearchedIdsRecent - parse json failed, json=" + O + ", ex=" + e2);
            return new ArrayList();
        }
    }

    @DexIgnore
    public void T0(int i2) {
        l1(this.g, "set_sync_data_retry_time", i2);
    }

    @DexIgnore
    public final void T1(SharedPreferences sharedPreferences, String str, String str2) {
        sharedPreferences.edit().putString(str, str2).apply();
    }

    @DexIgnore
    public String U() {
        return O(this.g, "KEY_WIDGETS_DATE_CHANGED", "");
    }

    @DexIgnore
    public void U0(int i2) {
        l1(this.h, "KEY_CURRENT_SYNC_SUCCESSULLY_SESSIONS", i2);
    }

    @DexIgnore
    public final void U1(wh5 wh5, String str, String str2) {
        if (wh5 instanceof jh5) {
            this.g.edit().putString(str, str2).apply();
        } else if (wh5 instanceof bi5) {
            this.h.edit().putString(str, str2).apply();
        }
    }

    @DexIgnore
    public boolean V() {
        return j(this.e, "KEY_SHOW_SUGGESTION_FIND_FRIEND", false);
    }

    @DexIgnore
    public void V0(boolean z) {
        P0(this.h, "KEY_DND_MODE_HYBRID_ENABLE", z);
    }

    @DexIgnore
    public void V1(String str) {
        T1(this.b, "USER_ACCESS_TOKEN", str);
    }

    @DexIgnore
    public boolean W() {
        return j(this.h, "KEY_ALL_APPS_TOGGLE_ENABLE", false);
    }

    @DexIgnore
    public void W0(boolean z) {
        P0(this.h, "KEY_DND_SCHEDULED_ENABLE", z);
    }

    @DexIgnore
    public void W1(List<String> list) {
        if (!list.isEmpty()) {
            T1(this.h, "KEY_WATCH_APP_SEARCHED_RECENT", new Gson().t(list));
        }
    }

    @DexIgnore
    public boolean X() {
        return j(this.f2700a, "AutoSyncEnable", true);
    }

    @DexIgnore
    public void X0(String str, long j2) {
        SharedPreferences sharedPreferences = this.f2700a;
        D1(sharedPreferences, "DELAY_OTA_BY_USER" + str, j2);
    }

    @DexIgnore
    public void X1(String str) {
        T1(this.g, "KEY_WIDGETS_DATE_CHANGED", str);
    }

    @DexIgnore
    public boolean Y() {
        return j(this.f2700a, "consider_bundle_firmware_as_latest", false);
    }

    @DexIgnore
    public void Y0(String str, String str2, boolean z) {
        SharedPreferences sharedPreferences = this.f2700a;
        P0(sharedPreferences, "calibrationState_" + str + str2, z);
    }

    @DexIgnore
    public void Y1(boolean z) {
        P0(this.g, "KEY_FORCE_REGISTER_FIREBASE_TOKEN", z);
    }

    @DexIgnore
    public boolean Z() {
        return j(this.h, "KEY_DND_MODE_HYBRID_ENABLE", false);
    }

    @DexIgnore
    public void Z0(String str, long j2, boolean z) {
        String str2;
        SharedPreferences sharedPreferences = this.f2700a;
        if (z) {
            str2 = str + j2;
        } else {
            str2 = "";
        }
        T1(sharedPreferences, "FULL_SYNC_DEVICE", str2);
    }

    @DexIgnore
    public boolean Z1() {
        return j(this.g, "KEY_FORCE_REGISTER_FIREBASE_TOKEN", false);
    }

    @DexIgnore
    public void a(String str, String str2) {
        SharedPreferences sharedPreferences = this.f2700a;
        T1(sharedPreferences, "tempDeviceFirmwareInfo_" + str, str2);
    }

    @DexIgnore
    public boolean a0() {
        return j(this.h, "KEY_DND_SCHEDULED_ENABLE", false);
    }

    @DexIgnore
    public void a1(boolean z) {
        P0(this.f2700a, "KEY_IS_DEVICE_LOCATE_ENABLE", z);
    }

    @DexIgnore
    public Boolean b() {
        return Boolean.valueOf(j(this.g, SharePreferencesUtils.BC_STATUS, true));
    }

    @DexIgnore
    public boolean b0(String str) {
        return O(this.f2700a, "FULL_SYNC_DEVICE", "").startsWith(str);
    }

    @DexIgnore
    public void b1(String str, boolean z) {
        SharedPreferences sharedPreferences = this.f2700a;
        P0(sharedPreferences, "KEY_PAIR_COMPLETE_DEVICE" + str, z);
    }

    @DexIgnore
    public int c() {
        return s(this.g, "set_sync_data_retry_time", 0);
    }

    @DexIgnore
    public boolean c0() {
        return this.f2700a.getBoolean("KEY_IS_DEVICE_LOCATE_ENABLE", false);
    }

    @DexIgnore
    public void c1(String str, wh5 wh5, String str2) {
        U1(wh5, "cipherIv" + str, str2);
    }

    @DexIgnore
    public Long d() {
        return Long.valueOf(F(this.g, "set_sync_first_time", 0));
    }

    @DexIgnore
    public boolean d0() {
        return this.c.getBoolean("KEY_DEVICE_LOCATE_NEED_WARNING", true);
    }

    @DexIgnore
    public void d1(String str, wh5 wh5, String str2) {
        U1(wh5, "encryptedDataInKeyStore" + str, str2);
    }

    @DexIgnore
    public void e(Long l2) {
        D1(this.g, "set_sync_first_time", l2.longValue());
    }

    @DexIgnore
    public Boolean e0(String str) {
        return Boolean.valueOf(this.l.getBoolean(str, true));
    }

    @DexIgnore
    public void e1(String str) {
        T1(this.i, "KEY_FIREBASE_TOKEN", str);
    }

    @DexIgnore
    public long f() {
        return F(this.b, "ACCESS_TOKEN_TIME", 0);
    }

    @DexIgnore
    public boolean f0(String str) {
        return j(this.f2700a, "FrontLightEnable", false);
    }

    @DexIgnore
    public void f1(String str, Boolean bool) {
        this.k.putBoolean(str, bool.booleanValue());
        this.k.commit();
    }

    @DexIgnore
    public String g() {
        return O(this.f2700a, "active_device_address", "");
    }

    @DexIgnore
    public boolean g0() {
        return j(this.h, "KEY_IS_GOAL_TRACKING_SETTING_COMPLETED", false);
    }

    @DexIgnore
    public void g1(boolean z) {
        P0(this.f2700a, "FrontLightEnable", z);
    }

    @DexIgnore
    public List<String> h() {
        String O = O(this.h, "KEY_ADDRESS_SEARCHED_RECENT", "");
        if (TextUtils.isEmpty(O)) {
            return new ArrayList();
        }
        try {
            return (List) new Gson().l(O, new b(this).getType());
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SharedPreferencesManager", "getAddressSearchedRecent - parse json failed, json=" + O + ", ex=" + e2);
            return new ArrayList();
        }
    }

    @DexIgnore
    public boolean h0() {
        return j(this.f2700a, "HWLogSyncEnable", true);
    }

    @DexIgnore
    public void h1(boolean z) {
        P0(this.f, "KEY_GOOGLE_FIT_USER_ENABLE", z);
    }

    @DexIgnore
    public String i() {
        return O(this.i, "KEY_AUTHORISED_FIREBASE_TOKEN", "");
    }

    @DexIgnore
    public boolean i0() {
        return j(this.f2700a, "KEY_DISPLAY_DEVICE_INFO", false);
    }

    @DexIgnore
    public void i1(boolean z) {
        P0(this.f2700a, "HWLogSyncEnable", z);
    }

    @DexIgnore
    public final boolean j(SharedPreferences sharedPreferences, String str, boolean z) {
        return sharedPreferences.getBoolean(str, z);
    }

    @DexIgnore
    public Boolean j0() {
        return Boolean.valueOf(this.g.getBoolean("KEY_MIGRATE_PRESET_TO_WATCHFACE", false));
    }

    @DexIgnore
    public void j1() {
        P0(this.e, "KEY_SHOW_SUGGESTION_FIND_FRIEND", true);
    }

    @DexIgnore
    public Firmware k(String str) {
        SharedPreferences sharedPreferences = this.f2700a;
        try {
            return (Firmware) new Gson().k(O(sharedPreferences, "bundle_latest_firmware_data" + str, ""), Firmware.class);
        } catch (mj4 e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("SharedPreferencesManager", ".getBundleLatestFirmwareModel() failed, ex=" + e2);
        } catch (Exception e3) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.e("SharedPreferencesManager", ".getBundleLatestFirmwareModel() failed, ex=" + e3);
        }
        return null;
    }

    @DexIgnore
    public boolean k0(String str) {
        SharedPreferences sharedPreferences = this.g;
        return j(sharedPreferences, "KEY_MIGRATION_COMPLETE" + str, false);
    }

    @DexIgnore
    public void k1(HeartRateMode heartRateMode) {
        l1(this.f2700a, "HeartRateMode", heartRateMode.getValue());
    }

    @DexIgnore
    public List<String> l() {
        String O = O(this.h, "KEY_COMPLICATION_SEARCHED_RECENT", "");
        if (TextUtils.isEmpty(O)) {
            return new ArrayList();
        }
        try {
            return (List) new Gson().l(O, new a(this).getType());
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SharedPreferencesManager", "getComplicationSearchedIdsRecent - parse json failed, json=" + O + ", ex=" + e2);
            return new ArrayList();
        }
    }

    @DexIgnore
    public Boolean l0() {
        return Boolean.valueOf(j(this.h, "is_need_sync_with_watch", false));
    }

    @DexIgnore
    public final void l1(SharedPreferences sharedPreferences, String str, int i2) {
        sharedPreferences.edit().putInt(str, i2).apply();
    }

    @DexIgnore
    public int m() {
        return s(this.h, "KEY_CURRENT_SYNC_SUCCESSULLY_SESSIONS", 0);
    }

    @DexIgnore
    public void m0(Boolean bool) {
        P0(this.h, "is_need_sync_with_watch", bool.booleanValue());
    }

    @DexIgnore
    public void m1(boolean z) {
        P0(this.h, "KEY_IS_GOAL_TRACKING_SETTING_COMPLETED", z);
    }

    @DexIgnore
    public String n(String str, wh5 wh5) {
        return P(wh5, "cipherIv" + str, "");
    }

    @DexIgnore
    public boolean n0() {
        return j(this.h, "KEY_SHOWING_RATING_PROMPT_CURRENT_VERSION", true);
    }

    @DexIgnore
    public void n1(boolean z, String str) {
        SharedPreferences sharedPreferences = this.g;
        P0(sharedPreferences, "KEY_MIGRATION_COMPLETE" + str, z);
    }

    @DexIgnore
    public String o(String str, wh5 wh5) {
        return P(wh5, "encryptedDataInKeyStore" + str, "");
    }

    @DexIgnore
    public boolean o0() {
        return j(this.g, "KEY_NEED_TO_UPDATE_BLE_WHEN_UPGRADE_LEGACY", false);
    }

    @DexIgnore
    public void o1(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setIsNeedRemoveRedundantContact - isNeed=" + z);
        P0(this.g, "KEY_NEED_REMOVE_REDUNDANT_CONTACT", z);
    }

    @DexIgnore
    public String p() {
        return O(this.i, "KEY_FIREBASE_TOKEN", "");
    }

    @DexIgnore
    public Boolean p0(String str) {
        SharedPreferences sharedPreferences = this.g;
        return Boolean.valueOf(j(sharedPreferences, "is_challenge_id_" + str, true));
    }

    @DexIgnore
    public void p1(boolean z) {
        P0(this.f2700a, "KEY_NEED_TO_SHOW_LEGACY_POPUP", z);
    }

    @DexIgnore
    public boolean q() {
        return j(this.f, "KEY_GOOGLE_FIT_USER_ENABLE", false);
    }

    @DexIgnore
    public boolean q0() {
        return j(this.h, "KEY_NEW_SOLUTION_FOR_SMS_ENABLE", true);
    }

    @DexIgnore
    public void q1(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setIsNeedToUpdateBLEWhenUpgradeLegacy - isNeedToUpdate=" + z);
        P0(this.g, "KEY_NEED_TO_UPDATE_BLE_WHEN_UPGRADE_LEGACY", z);
    }

    @DexIgnore
    public HeartRateMode r(String str) {
        return HeartRateMode.Companion.fromValue(s(this.f2700a, "HeartRateMode", HeartRateMode.NONE.getValue()));
    }

    @DexIgnore
    public Boolean r0() {
        return Boolean.valueOf(j(this.j, "KEY_QUICK_RESPONSE", false));
    }

    @DexIgnore
    public void r1(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setIsRegisteredContactObserver - isSet=" + z);
        P0(this.g, "KEY_IS_REGISTER_CONTACT_OBSERVER", z);
    }

    @DexIgnore
    public final int s(SharedPreferences sharedPreferences, String str, int i2) {
        return sharedPreferences.getInt(str, i2);
    }

    @DexIgnore
    public boolean s0() {
        return j(this.g, "KEY_IS_REGISTER_CONTACT_OBSERVER", false);
    }

    @DexIgnore
    public void s1(boolean z) {
        P0(this.h, "KEY_COMMUTE_TIME_TUTORIAL", z);
    }

    @DexIgnore
    public boolean t() {
        return j(this.h, "KEY_COMMUTE_TIME_TUTORIAL", false);
    }

    @DexIgnore
    public boolean t0() {
        return j(this.h, "KEY_REMINDERS_BEDTIME_REMINDER_ENABLE", false);
    }

    @DexIgnore
    public void t1(Boolean bool) {
        P0(this.f2700a, "KEY_SYNC_SESSION_RUNNING", bool.booleanValue());
    }

    @DexIgnore
    public int u() {
        return s(this.g, "KEY_USING_SWITCH_URL_SETTING", 0);
    }

    @DexIgnore
    public boolean u0() {
        return j(this.h, "KEY_REMINDERS_INACTIVITY_NUDGE_ENABLE", false);
    }

    @DexIgnore
    public void u1(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setKeyWeatherSettings - weatherSettings=" + str);
        T1(this.f2700a, "KEY_WEATHER_SETTINGS", str);
    }

    @DexIgnore
    public String v() {
        return O(this.f2700a, "KEY_WEARING_POSITION", "unspecified");
    }

    @DexIgnore
    public boolean v0() {
        return j(this.h, "KEY_REMINDERS_KEEP_GOING_ENABLE", false);
    }

    @DexIgnore
    public void v1(long j2) {
        D1(this.f2700a, "lastHWSyncTime", j2);
    }

    @DexIgnore
    public String w() {
        String O = O(this.f2700a, "KEY_WEATHER_SETTINGS", "");
        if (!TextUtils.isEmpty(O)) {
            return O;
        }
        return new Gson().u(new WeatherSettings(), WeatherSettings.class);
    }

    @DexIgnore
    public boolean w0() {
        return j(this.h, "KEY_REMINDERS_SLEEP_SUMMARY_ENABLE", false);
    }

    @DexIgnore
    public void w1(long j2) {
        D1(this.h, "KEY_LAST_NO_CRASH_EVENT_TIMESTAMP", j2);
    }

    @DexIgnore
    public String x() {
        return this.d.getString("KEY_USER_LAST_APP_VERSION_NAME_Q", "");
    }

    @DexIgnore
    public boolean x0() {
        return j(this.f2700a, "showAllDevices", false);
    }

    @DexIgnore
    public void x1(int i2) {
        l1(this.f2700a, "lastNotificationID", i2);
    }

    @DexIgnore
    public long y() {
        long F = F(this.h, "KEY_LAST_NO_CRASH_EVENT_TIMESTAMP", -1);
        if (F != -1) {
            return F;
        }
        long currentTimeMillis = System.currentTimeMillis();
        w1(currentTimeMillis);
        return currentTimeMillis;
    }

    @DexIgnore
    public boolean y0() {
        return j(this.f2700a, "skipOta", false);
    }

    @DexIgnore
    public void y1(long j2, String str) {
        SharedPreferences sharedPreferences = this.f2700a;
        D1(sharedPreferences, "lastReminderSyncTimeSuccess" + str, j2);
    }

    @DexIgnore
    public int z() {
        return s(this.f2700a, "lastNotificationID", 0);
    }

    @DexIgnore
    public boolean z0() {
        return j(this.f2700a, "KEY_SYNC_SESSION_RUNNING", false);
    }

    @DexIgnore
    public void z1(long j2, String str) {
        SharedPreferences sharedPreferences = this.f2700a;
        D1(sharedPreferences, "lastSyncTime" + str, j2);
    }
}
