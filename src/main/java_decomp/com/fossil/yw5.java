package com.fossil;

import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Zf;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yw5 extends Zf.Di<GoalTrackingSummary> {
    @DexIgnore
    public boolean a(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        Wg6.c(goalTrackingSummary, "oldItem");
        Wg6.c(goalTrackingSummary2, "newItem");
        return Wg6.a(goalTrackingSummary, goalTrackingSummary2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Zf.Di
    public /* bridge */ /* synthetic */ boolean areContentsTheSame(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        return a(goalTrackingSummary, goalTrackingSummary2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Zf.Di
    public /* bridge */ /* synthetic */ boolean areItemsTheSame(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        return b(goalTrackingSummary, goalTrackingSummary2);
    }

    @DexIgnore
    public boolean b(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        Wg6.c(goalTrackingSummary, "oldItem");
        Wg6.c(goalTrackingSummary2, "newItem");
        return TimeUtils.m0(goalTrackingSummary.getDate(), goalTrackingSummary2.getDate());
    }
}
