package com.fossil;

import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I6 extends M5 {
    @DexIgnore
    public byte[] m; // = new byte[0];
    @DexIgnore
    public /* final */ boolean n;

    @DexIgnore
    public I6(N6 n6, boolean z, N4 n4) {
        super(V5.h, n6, n4);
        this.n = z;
    }

    @DexIgnore
    @Override // com.fossil.M5, com.fossil.U5
    public JSONObject b(boolean z) {
        return G80.k(super.b(z), Jd0.F, Boolean.valueOf(this.n));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d9  */
    @Override // com.fossil.U5
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d(com.fossil.K5 r11) {
        /*
        // Method dump skipped, instructions count: 364
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.I6.d(com.fossil.K5):void");
    }

    @DexIgnore
    @Override // com.fossil.M5, com.fossil.U5
    public void f(H7 h7) {
        S5 a2;
        this.k = false;
        G7 g7 = h7.a;
        if (g7.b == F7.b) {
            a2 = S5.a(this.e, null, Arrays.equals(this.m, ((B7) h7).d) ? R5.b : R5.e, h7.a, 1);
        } else {
            S5 a3 = S5.e.a(g7);
            a2 = S5.a(this.e, null, a3.c, a3.d, 1);
        }
        this.e = a2;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public boolean i(H7 h7) {
        if (h7 instanceof B7) {
            B7 b7 = (B7) h7;
            return b7.b == this.l && b7.c == U6.b;
        }
    }

    @DexIgnore
    @Override // com.fossil.U5
    public Fd0<H7> j() {
        return this.j.b;
    }
}
