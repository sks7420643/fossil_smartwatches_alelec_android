package com.fossil;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.mapped.Wg6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hr4 extends Tq0 {
    @DexIgnore
    public /* final */ ArrayList<Fragment> g; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<String> h; // = new ArrayList<>();

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Hr4(FragmentManager fragmentManager) {
        super(fragmentManager);
        Wg6.c(fragmentManager, "fm");
    }

    @DexIgnore
    @Override // com.fossil.E01
    public int e() {
        return this.g.size();
    }

    @DexIgnore
    @Override // com.fossil.E01
    public CharSequence g(int i) {
        String str = this.h.get(i);
        Wg6.b(str, "mFragmentTitleList.get(position)");
        return str;
    }

    @DexIgnore
    @Override // com.fossil.Tq0
    public Fragment u(int i) {
        Fragment fragment = this.g.get(i);
        Wg6.b(fragment, "mFragmentList.get(position)");
        return fragment;
    }

    @DexIgnore
    public final void x(Fragment fragment, String str) {
        Wg6.c(fragment, "fragment");
        Wg6.c(str, "title");
        this.g.add(fragment);
        this.h.add(str);
    }
}
