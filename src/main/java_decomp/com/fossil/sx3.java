package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.os.Build;
import android.util.Property;
import android.view.View;
import android.view.ViewAnimationUtils;
import com.fossil.Ux3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sx3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ Ux3 a;

        @DexIgnore
        public Ai(Ux3 ux3) {
            this.a = ux3;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.a.b();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            this.a.a();
        }
    }

    @DexIgnore
    public static Animator a(Ux3 ux3, float f, float f2, float f3) {
        ObjectAnimator ofObject = ObjectAnimator.ofObject(ux3, (Property<Ux3, V>) Ux3.Ci.a, (TypeEvaluator) Ux3.Bi.b, (Object[]) new Ux3.Ei[]{new Ux3.Ei(f, f2, f3)});
        if (Build.VERSION.SDK_INT < 21) {
            return ofObject;
        }
        Ux3.Ei revealInfo = ux3.getRevealInfo();
        if (revealInfo != null) {
            Animator createCircularReveal = ViewAnimationUtils.createCircularReveal((View) ux3, (int) f, (int) f2, revealInfo.c, f3);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(ofObject, createCircularReveal);
            return animatorSet;
        }
        throw new IllegalStateException("Caller must set a non-null RevealInfo before calling this.");
    }

    @DexIgnore
    public static Animator.AnimatorListener b(Ux3 ux3) {
        return new Ai(ux3);
    }
}
