package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Om1 extends R60 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ short c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Om1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Om1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 1) {
                return new Om1(Hy1.p(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).get(0)));
            }
            throw new IllegalArgumentException(E.c(E.e("Invalid data size: "), bArr.length, ", ", "require: 1"));
        }

        @DexIgnore
        public Om1 b(Parcel parcel) {
            return new Om1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Om1 createFromParcel(Parcel parcel) {
            return new Om1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Om1[] newArray(int i) {
            return new Om1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Om1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = Hy1.p(parcel.readByte());
    }

    @DexIgnore
    public Om1(short s) {
        super(Zm1.CURRENT_HEART_RATE);
        this.c = (short) s;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put((byte) this.c).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            G80.k(jSONObject, Jd0.y3, Short.valueOf(this.c));
        } catch (JSONException e) {
            D90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Om1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((Om1) obj).c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.CurrentHeartRateConfig");
    }

    @DexIgnore
    public final short getCurrentHeartRate() {
        return this.c;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        return (super.hashCode() * 31) + this.c;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte((byte) this.c);
        }
    }
}
