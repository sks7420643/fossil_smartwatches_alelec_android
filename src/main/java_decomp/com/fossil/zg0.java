package com.fossil;

import android.view.textclassifier.TextClassificationManager;
import android.view.textclassifier.TextClassifier;
import android.widget.TextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zg0 {
    @DexIgnore
    public TextView a;
    @DexIgnore
    public TextClassifier b;

    @DexIgnore
    public Zg0(TextView textView) {
        Pn0.d(textView);
        this.a = textView;
    }

    @DexIgnore
    public TextClassifier a() {
        TextClassifier textClassifier = this.b;
        if (textClassifier != null) {
            return textClassifier;
        }
        TextClassificationManager textClassificationManager = (TextClassificationManager) this.a.getContext().getSystemService(TextClassificationManager.class);
        return textClassificationManager != null ? textClassificationManager.getTextClassifier() : TextClassifier.NO_OP;
    }

    @DexIgnore
    public void b(TextClassifier textClassifier) {
        this.b = textClassifier;
    }
}
