package com.fossil;

import android.view.View;
import android.view.ViewGroup;
import com.mapped.Wg6;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Po0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Ts7<View> {
        @DexIgnore
        public /* final */ /* synthetic */ ViewGroup a;

        @DexIgnore
        public Ai(ViewGroup viewGroup) {
            this.a = viewGroup;
        }

        @DexIgnore
        @Override // com.fossil.Ts7
        public Iterator<View> iterator() {
            return Po0.b(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Iterator<View>, Object {
        @DexIgnore
        public int b;
        @DexIgnore
        public /* final */ /* synthetic */ ViewGroup c;

        @DexIgnore
        public Bi(ViewGroup viewGroup) {
            this.c = viewGroup;
        }

        @DexIgnore
        public View a() {
            ViewGroup viewGroup = this.c;
            int i = this.b;
            this.b = i + 1;
            View childAt = viewGroup.getChildAt(i);
            if (childAt != null) {
                return childAt;
            }
            throw new IndexOutOfBoundsException();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b < this.c.getChildCount();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.Iterator
        public /* bridge */ /* synthetic */ View next() {
            return a();
        }

        @DexIgnore
        public void remove() {
            ViewGroup viewGroup = this.c;
            int i = this.b - 1;
            this.b = i;
            viewGroup.removeViewAt(i);
        }
    }

    @DexIgnore
    public static final Ts7<View> a(ViewGroup viewGroup) {
        Wg6.c(viewGroup, "$this$children");
        return new Ai(viewGroup);
    }

    @DexIgnore
    public static final Iterator<View> b(ViewGroup viewGroup) {
        Wg6.c(viewGroup, "$this$iterator");
        return new Bi(viewGroup);
    }
}
