package com.fossil;

import com.mapped.PermissionUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ol5 {
    @DexIgnore
    public static /* final */ ArrayList<String> a; // = Hm7.c("weather", "commute-time");
    @DexIgnore
    public static /* final */ ArrayList<String> b; // = Hm7.c("commute-time");
    @DexIgnore
    public static /* final */ Ol5 c; // = new Ol5();

    /*
    static {
        Hm7.c(Constants.MUSIC, "weather", "commute-time");
    }
    */

    @DexIgnore
    public final WatchApp a() {
        return new WatchApp("empty_watch_app_id", "", "", "", "", Hm7.c("category_all"), "", "", "");
    }

    @DexIgnore
    public final String b(String str) {
        Wg6.c(str, "watchAppId");
        int hashCode = str.hashCode();
        if (hashCode != -829740640) {
            if (hashCode == 1223440372 && str.equals("weather")) {
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131886533);
                Wg6.b(c2, "LanguageHelper.getString\u2026tCity_Title__ChooseACity)");
                return c2;
            }
        } else if (str.equals("commute-time")) {
            String c3 = Um5.c(PortfolioApp.get.instance(), 2131886369);
            Wg6.b(c3, "LanguageHelper.getString\u2026Time_CTA__SetDestination)");
            return c3;
        }
        return "";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0049, code lost:
        if (r0 < 29) goto L_0x007b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0078, code lost:
        if (r8.equals("commute-time") != false) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return com.fossil.Hm7.c(com.portfolio.platform.data.InAppPermission.ACCESS_FINE_LOCATION, com.portfolio.platform.data.InAppPermission.LOCATION_SERVICE, com.portfolio.platform.data.InAppPermission.ACCESS_BACKGROUND_LOCATION);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return com.fossil.Hm7.c(com.portfolio.platform.data.InAppPermission.ACCESS_FINE_LOCATION, com.portfolio.platform.data.InAppPermission.LOCATION_SERVICE);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0027, code lost:
        if (r8.equals("weather") == false) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0029, code lost:
        r0 = android.os.Build.VERSION.SDK_INT;
        r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
        r1.d("WatchAppHelper", "android.os.Build.VERSION.SDK_INT=" + r0);
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> c(java.lang.String r8) {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            r4 = 0
            java.lang.String r0 = "watchAppId"
            com.mapped.Wg6.c(r8, r0)
            int r0 = r8.hashCode()
            r1 = -829740640(0xffffffffce8b29a0, float:-1.16738048E9)
            if (r0 == r1) goto L_0x0072
            r1 = 104263205(0x636ee25, float:3.4405356E-35)
            if (r0 == r1) goto L_0x005f
            r1 = 1223440372(0x48ec37f4, float:483775.62)
            if (r0 == r1) goto L_0x0021
        L_0x001b:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
        L_0x0020:
            return r0
        L_0x0021:
            java.lang.String r0 = "weather"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x001b
        L_0x0029:
            int r0 = android.os.Build.VERSION.SDK_INT
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "android.os.Build.VERSION.SDK_INT="
            r2.append(r3)
            r2.append(r0)
            java.lang.String r3 = "WatchAppHelper"
            java.lang.String r2 = r2.toString()
            r1.d(r3, r2)
            r1 = 29
            if (r0 < r1) goto L_0x007b
            r0 = 3
            java.lang.String[] r0 = new java.lang.String[r0]
            java.lang.String r1 = "ACCESS_FINE_LOCATION"
            r0[r4] = r1
            java.lang.String r1 = "LOCATION_SERVICE"
            r0[r5] = r1
            java.lang.String r1 = "ACCESS_BACKGROUND_LOCATION"
            r0[r6] = r1
            java.util.ArrayList r0 = com.fossil.Hm7.c(r0)
            goto L_0x0020
        L_0x005f:
            java.lang.String r0 = "music"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x001b
            java.lang.String[] r0 = new java.lang.String[r5]
            java.lang.String r1 = "NOTIFICATION_ACCESS"
            r0[r4] = r1
            java.util.ArrayList r0 = com.fossil.Hm7.c(r0)
            goto L_0x0020
        L_0x0072:
            java.lang.String r0 = "commute-time"
            boolean r0 = r8.equals(r0)
            if (r0 == 0) goto L_0x001b
            goto L_0x0029
        L_0x007b:
            java.lang.String[] r0 = new java.lang.String[r6]
            java.lang.String r1 = "ACCESS_FINE_LOCATION"
            r0[r4] = r1
            java.lang.String r1 = "LOCATION_SERVICE"
            r0[r5] = r1
            java.util.ArrayList r0 = com.fossil.Hm7.c(r0)
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ol5.c(java.lang.String):java.util.List");
    }

    @DexIgnore
    public final Integer d(String str) {
        Wg6.c(str, "watchAppId");
        return (str.hashCode() == -829740640 && str.equals("commute-time")) ? 2131820544 : null;
    }

    @DexIgnore
    public final boolean e(String str) {
        Wg6.c(str, "watchAppId");
        List<String> c2 = c(str);
        String[] a2 = PermissionUtils.a.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppHelper", "isPermissionGrantedForWatchApp " + str + " granted=" + a2 + " required=" + c2);
        Iterator<T> it = c2.iterator();
        while (it.hasNext()) {
            if (!Em7.B(a2, it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final boolean f(String str) {
        Wg6.c(str, "watchAppId");
        return b.contains(str);
    }

    @DexIgnore
    public final boolean g(String str) {
        Wg6.c(str, "watchAppId");
        return a.contains(str);
    }
}
