package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.UserSettingDao;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jt5 extends iq4<b, d, c> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ wq5.b e; // = new e(this);
    @DexIgnore
    public /* final */ PortfolioApp f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ UserSettingDao h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return jt5.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f1800a;
        @DexIgnore
        public /* final */ ActivitySettings b;

        @DexIgnore
        public b(String str, ActivitySettings activitySettings) {
            pq7.c(str, "serial");
            pq7.c(activitySettings, com.fossil.wearables.fsl.fitness.ActivitySettings.TABLE_NAME);
            this.f1800a = str;
            this.b = activitySettings;
        }

        @DexIgnore
        public final ActivitySettings a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.f1800a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f1801a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public c(int i, String str, ArrayList<Integer> arrayList) {
            this.f1801a = i;
            this.b = str;
            this.c = arrayList;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(int i, String str, ArrayList arrayList, int i2, kq7 kq7) {
            this(i, str, (i2 & 4) != 0 ? null : arrayList);
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.c;
        }

        @DexIgnore
        public final int b() {
            return this.f1801a;
        }

        @DexIgnore
        public final String c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements wq5.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ jt5 f1802a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase$mSetGoalReceiver$1$receive$1", f = "SetActivityGoalUserCase.kt", l = {50, 59}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isGoalRingEnabled;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, boolean z, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
                this.$isGoalRingEnabled = z;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$isGoalRingEnabled, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x0042  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x00e5  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r10) {
                /*
                // Method dump skipped, instructions count: 238
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.jt5.e.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(jt5 jt5) {
            this.f1802a = jt5;
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            Bundle extras = intent.getExtras();
            boolean z = extras != null ? extras.getBoolean(ButtonService.GOAL_RING_ENABLED, true) : true;
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            if (communicateMode == CommunicateMode.SET_STEP_GOAL && pq7.a(stringExtra, this.f1802a.d)) {
                if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    xw7 unused = gu7.d(this.f1802a.g(), null, null, new a(this, z, null), 3, null);
                } else if (intExtra == ServiceActionResult.FAILED.ordinal()) {
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                    int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), FailureCode.UNKNOWN_ERROR);
                    if (intExtra2 == 1101) {
                        this.f1802a.i(new c(125, "", integerArrayListExtra));
                    } else if (intExtra2 == 1505) {
                        this.f1802a.i(new c(123, "", null, 4, null));
                    } else if (intExtra2 != 1920) {
                        this.f1802a.i(new c(600, "", null, 4, null));
                    } else {
                        this.f1802a.i(new c(124, "", null, 4, null));
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase", f = "SetActivityGoalUserCase.kt", l = {85}, m = "sendUserSettingToServer")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ jt5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(jt5 jt5, qn7 qn7) {
            super(qn7);
            this.this$0 = jt5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(null, this);
        }
    }

    /*
    static {
        String simpleName = jt5.class.getSimpleName();
        pq7.b(simpleName, "SetActivityGoalUserCase::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public jt5(PortfolioApp portfolioApp, UserRepository userRepository, UserSettingDao userSettingDao) {
        pq7.c(portfolioApp, "mPortfolioApp");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(userSettingDao, "mUserSettingDao");
        this.f = portfolioApp;
        this.g = userRepository;
        this.h = userSettingDao;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return i;
    }

    @DexIgnore
    public final void q() {
        FLogger.INSTANCE.getLocal().d(i, "registerReceiver");
        wq5.d.j(this.e, CommunicateMode.SET_STEP_GOAL);
        wq5.d.e(this.e, CommunicateMode.SET_STEP_GOAL);
        wq5.d.g(CommunicateMode.SET_STEP_GOAL);
    }

    @DexIgnore
    /* renamed from: r */
    public Object k(b bVar, qn7<Object> qn7) {
        if (bVar == null) {
            return new c(600, "", null, 4, null);
        }
        FLogger.INSTANCE.getLocal().d(i, "running UseCase");
        this.d = bVar.b();
        this.f.K1(bVar.b(), bVar.a().getCurrentStepGoal(), bVar.a().getCurrentCaloriesGoal(), bVar.a().getCurrentActiveTimeGoal());
        return new Object();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    @android.annotation.SuppressLint({"VisibleForTests"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object s(com.portfolio.platform.data.model.UserSettings r6, com.fossil.qn7<? super com.fossil.tl7> r7) {
        /*
            r5 = this;
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 1
            boolean r0 = r7 instanceof com.fossil.jt5.f
            if (r0 == 0) goto L_0x0054
            r0 = r7
            com.fossil.jt5$f r0 = (com.fossil.jt5.f) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0054
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0063
            if (r3 != r4) goto L_0x005b
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.data.model.UserSettings r0 = (com.portfolio.platform.data.model.UserSettings) r0
            java.lang.Object r1 = r1.L$0
            com.fossil.jt5 r1 = (com.fossil.jt5) r1
            com.fossil.el7.b(r2)
            r6 = r0
            r5 = r1
        L_0x002d:
            r0 = r2
            com.fossil.iq5 r0 = (com.fossil.iq5) r0
            if (r0 == 0) goto L_0x0051
            boolean r1 = r0 instanceof com.fossil.kq5
            if (r1 == 0) goto L_0x007a
            com.fossil.kq5 r0 = (com.fossil.kq5) r0
            java.lang.Object r1 = r0.a()
            com.portfolio.platform.data.model.UserSettings r1 = (com.portfolio.platform.data.model.UserSettings) r1
            if (r1 == 0) goto L_0x0044
            r2 = 0
            r1.setPinType(r2)
        L_0x0044:
            com.portfolio.platform.data.source.UserSettingDao r1 = r5.h
            java.lang.Object r0 = r0.a()
            if (r0 == 0) goto L_0x0075
            com.portfolio.platform.data.model.UserSettings r0 = (com.portfolio.platform.data.model.UserSettings) r0
            r1.addOrUpdateUserSetting(r0)
        L_0x0051:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0053:
            return r0
        L_0x0054:
            com.fossil.jt5$f r0 = new com.fossil.jt5$f
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x005b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0063:
            com.fossil.el7.b(r2)
            com.portfolio.platform.data.source.UserRepository r2 = r5.g
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r2 = r2.sendUserSettingToServer(r6, r1)
            if (r2 != r0) goto L_0x002d
            goto L_0x0053
        L_0x0075:
            com.fossil.pq7.i()
            r0 = 0
            throw r0
        L_0x007a:
            boolean r0 = r0 instanceof com.fossil.hq5
            if (r0 == 0) goto L_0x0051
            r6.setPinType(r4)
            com.portfolio.platform.data.source.UserSettingDao r0 = r5.h
            r0.addOrUpdateUserSetting(r6)
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jt5.s(com.portfolio.platform.data.model.UserSettings, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void t() {
        FLogger.INSTANCE.getLocal().d(i, "unRegisterReceiver");
        wq5.d.j(this.e, CommunicateMode.SET_STEP_GOAL);
    }
}
