package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.fossil.Tu4;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputActivity;
import com.portfolio.platform.buddy_challenge.screens.create_intro.BCCreateChallengeIntroViewModel;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pu4 extends BaseFragment implements Tu4.Ai {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public G37<D45> h;
    @DexIgnore
    public BCCreateChallengeIntroViewModel i;
    @DexIgnore
    public Tu4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Pu4.l;
        }

        @DexIgnore
        public final Pu4 b() {
            return new Pu4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Pu4 b;

        @DexIgnore
        public Bi(Pu4 pu4) {
            this.b = pu4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Pu4 a;

        @DexIgnore
        public Ci(Pu4 pu4) {
            this.a = pu4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            ProgressBar progressBar;
            D45 d45 = (D45) Pu4.L6(this.a).a();
            if (d45 != null && (progressBar = d45.s) != null) {
                Wg6.b(bool, "it");
                progressBar.setVisibility(bool.booleanValue() ? 0 : 8);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<List<Vs4>> {
        @DexIgnore
        public /* final */ /* synthetic */ Pu4 a;

        @DexIgnore
        public Di(Pu4 pu4) {
            this.a = pu4;
        }

        @DexIgnore
        public final void a(List<Vs4> list) {
            Tu4 tu4;
            if (list != null && (tu4 = this.a.j) != null) {
                tu4.n(list);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<Vs4> list) {
            a(list);
        }
    }

    /*
    static {
        String simpleName = Pu4.class.getSimpleName();
        Wg6.b(simpleName, "BCCreateChallengeIntroFr\u2026nt::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 L6(Pu4 pu4) {
        G37<D45> g37 = pu4.h;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(0);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 == null) {
            return true;
        }
        activity2.finish();
        return true;
    }

    @DexIgnore
    public final void N6() {
        G37<D45> g37 = this.h;
        if (g37 != null) {
            D45 a2 = g37.a();
            if (a2 != null) {
                Tu4 tu4 = new Tu4();
                this.j = tu4;
                if (tu4 != null) {
                    tu4.m(this);
                }
                RecyclerView recyclerView = a2.u;
                Wg6.b(recyclerView, "rvTemplates");
                recyclerView.setAdapter(this.j);
                RecyclerView recyclerView2 = a2.u;
                Wg6.b(recyclerView2, "rvTemplates");
                recyclerView2.setLayoutManager(new LinearLayoutManager(getContext()));
                a2.r.setOnClickListener(new Bi(this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void O6() {
        BCCreateChallengeIntroViewModel bCCreateChallengeIntroViewModel = this.i;
        if (bCCreateChallengeIntroViewModel != null) {
            bCCreateChallengeIntroViewModel.e().h(getViewLifecycleOwner(), new Ci(this));
            BCCreateChallengeIntroViewModel bCCreateChallengeIntroViewModel2 = this.i;
            if (bCCreateChallengeIntroViewModel2 != null) {
                bCCreateChallengeIntroViewModel2.f().h(getViewLifecycleOwner(), new Di(this));
            } else {
                Wg6.n("mViewModelHome");
                throw null;
            }
        } else {
            Wg6.n("mViewModelHome");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Tu4.Ai
    public void k6(Vs4 vs4) {
        Wg6.c(vs4, MessengerShareContentUtility.ATTACHMENT_TEMPLATE_TYPE);
        BCCreateChallengeInputActivity.A.b(this, vs4);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 11 && i3 == -1) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.setResult(i3);
            }
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                activity2.finish();
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().B0().a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCCreateChallengeIntroViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026troViewModel::class.java)");
            this.i = (BCCreateChallengeIntroViewModel) a2;
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        D45 d45 = (D45) Aq0.f(layoutInflater, 2131558526, viewGroup, false, A6());
        this.h = new G37<>(this, d45);
        Wg6.b(d45, "binding");
        return d45.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        AnalyticsHelper g2 = AnalyticsHelper.f.g();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            g2.m("bc_challenge_mode", activity);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        BCCreateChallengeIntroViewModel bCCreateChallengeIntroViewModel = this.i;
        if (bCCreateChallengeIntroViewModel != null) {
            bCCreateChallengeIntroViewModel.d();
            N6();
            O6();
            return;
        }
        Wg6.n("mViewModelHome");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
