package com.fossil;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.AppWrapper;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gx5 extends RecyclerView.g<Bi> implements Filterable {
    @DexIgnore
    public List<AppWrapper> b; // = new ArrayList();
    @DexIgnore
    public int c;
    @DexIgnore
    public List<AppWrapper> d;
    @DexIgnore
    public Ai e;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(AppWrapper appWrapper, boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Ld5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Gx5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List list = this.b.b.d;
                    if (list != null) {
                        InstalledApp installedApp = ((AppWrapper) list.get(adapterPosition)).getInstalledApp();
                        Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                        if (isSelected != null) {
                            boolean booleanValue = isSelected.booleanValue();
                            if (booleanValue) {
                                List list2 = this.b.b.d;
                                if (list2 == null) {
                                    Wg6.i();
                                    throw null;
                                } else if (((AppWrapper) list2.get(adapterPosition)).getCurrentHandGroup() != this.b.b.c) {
                                    Ai ai = this.b.b.e;
                                    if (ai != null) {
                                        List list3 = this.b.b.d;
                                        if (list3 != null) {
                                            ai.a((AppWrapper) list3.get(adapterPosition), booleanValue);
                                            return;
                                        } else {
                                            Wg6.i();
                                            throw null;
                                        }
                                    } else {
                                        return;
                                    }
                                }
                            }
                            Ai ai2 = this.b.b.e;
                            if (ai2 != null) {
                                List list4 = this.b.b.d;
                                if (list4 != null) {
                                    ai2.a((AppWrapper) list4.get(adapterPosition), !booleanValue);
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Gx5 gx5, Ld5 ld5) {
            super(ld5.n());
            Wg6.c(ld5, "binding");
            this.b = gx5;
            this.a = ld5;
            ld5.w.setOnClickListener(new Aii(this));
            String d = ThemeManager.l.a().d("nonBrandSeparatorLine");
            String d2 = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d2)) {
                this.a.q.setBackgroundColor(Color.parseColor(d2));
            }
            if (!TextUtils.isEmpty(d)) {
                this.a.u.setBackgroundColor(Color.parseColor(d));
            }
        }

        @DexIgnore
        public final void a(AppWrapper appWrapper) {
            Wg6.c(appWrapper, "appWrapper");
            ImageView imageView = this.a.t;
            Wg6.b(imageView, "binding.ivAppIcon");
            Tj5.a(imageView.getContext()).G(appWrapper.getUri()).T0(((Fj1) ((Fj1) new Fj1().n()).l(Wc1.a)).m0(true)).Y0().F0(this.a.t);
            FlexibleTextView flexibleTextView = this.a.r;
            Wg6.b(flexibleTextView, "binding.ftvAppName");
            InstalledApp installedApp = appWrapper.getInstalledApp();
            flexibleTextView.setText(installedApp != null ? installedApp.getTitle() : null);
            FlexibleSwitchCompat flexibleSwitchCompat = this.a.w;
            Wg6.b(flexibleSwitchCompat, "binding.swEnabled");
            InstalledApp installedApp2 = appWrapper.getInstalledApp();
            Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
            if (isSelected != null) {
                flexibleSwitchCompat.setChecked(isSelected.booleanValue());
                if (appWrapper.getCurrentHandGroup() == 0 || appWrapper.getCurrentHandGroup() == this.b.c) {
                    FlexibleTextView flexibleTextView2 = this.a.s;
                    Wg6.b(flexibleTextView2, "binding.ftvAssigned");
                    flexibleTextView2.setText("");
                    FlexibleTextView flexibleTextView3 = this.a.s;
                    Wg6.b(flexibleTextView3, "binding.ftvAssigned");
                    flexibleTextView3.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView4 = this.a.s;
                Wg6.b(flexibleTextView4, "binding.ftvAssigned");
                Hr7 hr7 = Hr7.a;
                String c = Um5.c(PortfolioApp.get.instance(), 2131886157);
                Wg6.b(c, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(appWrapper.getCurrentHandGroup())}, 1));
                Wg6.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView4.setText(format);
                FlexibleTextView flexibleTextView5 = this.a.s;
                Wg6.b(flexibleTextView5, "binding.ftvAssigned");
                flexibleTextView5.setVisibility(0);
                FlexibleSwitchCompat flexibleSwitchCompat2 = this.a.w;
                Wg6.b(flexibleSwitchCompat2, "binding.swEnabled");
                flexibleSwitchCompat2.setChecked(false);
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Filter {
        @DexIgnore
        public /* final */ /* synthetic */ Gx5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ci(Gx5 gx5) {
            this.a = gx5;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            String str;
            String title;
            Wg6.c(charSequence, "constraint");
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (TextUtils.isEmpty(charSequence)) {
                filterResults.values = this.a.b;
            } else {
                ArrayList arrayList = new ArrayList();
                String obj = charSequence.toString();
                if (obj != null) {
                    String lowerCase = obj.toLowerCase();
                    Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    for (AppWrapper appWrapper : this.a.b) {
                        InstalledApp installedApp = appWrapper.getInstalledApp();
                        if (installedApp == null || (title = installedApp.getTitle()) == null) {
                            str = null;
                        } else if (title != null) {
                            str = title.toLowerCase();
                            Wg6.b(str, "(this as java.lang.String).toLowerCase()");
                        } else {
                            throw new Rc6("null cannot be cast to non-null type java.lang.String");
                        }
                        if (str != null && Wt7.v(str, lowerCase, false, 2, null)) {
                            arrayList.add(appWrapper);
                        }
                    }
                    filterResults.values = arrayList;
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            Wg6.c(charSequence, "charSequence");
            Wg6.c(filterResults, "results");
            this.a.d = (List) filterResults.values;
            this.a.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public Filter getFilter() {
        return new Ci(this);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<AppWrapper> list = this.d;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public void l(Bi bi, int i) {
        Wg6.c(bi, "holder");
        List<AppWrapper> list = this.d;
        if (list != null) {
            bi.a(list.get(i));
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Bi m(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Ld5 z = Ld5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemAppHybridNotificatio\u2026.context), parent, false)");
        return new Bi(this, z);
    }

    @DexIgnore
    public final void n(List<AppWrapper> list, int i) {
        Wg6.c(list, "listAppWrapper");
        this.c = i;
        this.b = list;
        this.d = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void o(Ai ai) {
        Wg6.c(ai, "listener");
        this.e = ai;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        l(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return m(viewGroup, i);
    }
}
