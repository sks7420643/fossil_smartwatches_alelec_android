package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.buddy_challenge.domain.ProfileRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bx4 implements Factory<Ax4> {
    @DexIgnore
    public /* final */ Provider<An4> a;
    @DexIgnore
    public /* final */ Provider<ProfileRepository> b;

    @DexIgnore
    public Bx4(Provider<An4> provider, Provider<ProfileRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Bx4 a(Provider<An4> provider, Provider<ProfileRepository> provider2) {
        return new Bx4(provider, provider2);
    }

    @DexIgnore
    public static Ax4 c(An4 an4, ProfileRepository profileRepository) {
        return new Ax4(an4, profileRepository);
    }

    @DexIgnore
    public Ax4 b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
