package com.fossil;

import com.mapped.Cd6;
import java.util.concurrent.Future;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hu7 extends Iu7 {
    @DexIgnore
    public /* final */ Future<?> b;

    @DexIgnore
    public Hu7(Future<?> future) {
        this.b = future;
    }

    @DexIgnore
    @Override // com.fossil.Ju7
    public void a(Throwable th) {
        this.b.cancel(false);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
        a(th);
        return Cd6.a;
    }

    @DexIgnore
    public String toString() {
        return "CancelFutureOnCancel[" + this.b + ']';
    }
}
