package com.fossil;

import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Lc4 implements Wy1 {
    @DexIgnore
    public static /* final */ Lc4 a; // = new Lc4();

    @DexIgnore
    public static Wy1 a() {
        return a;
    }

    @DexIgnore
    @Override // com.fossil.Wy1
    public Object apply(Object obj) {
        return Mc4.b.E((Ta4) obj).getBytes(Charset.forName("UTF-8"));
    }
}
