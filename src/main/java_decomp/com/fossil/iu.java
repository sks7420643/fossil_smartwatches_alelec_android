package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Iu extends Enum<Iu> {
    @DexIgnore
    public static /* final */ Iu c;
    @DexIgnore
    public static /* final */ Iu d;
    @DexIgnore
    public static /* final */ Iu e;
    @DexIgnore
    public static /* final */ Iu f;
    @DexIgnore
    public static /* final */ Iu g;
    @DexIgnore
    public static /* final */ Iu h;
    @DexIgnore
    public static /* final */ Iu i;
    @DexIgnore
    public static /* final */ Iu j;
    @DexIgnore
    public static /* final */ Iu k;
    @DexIgnore
    public static /* final */ Iu l;
    @DexIgnore
    public static /* final */ /* synthetic */ Iu[] m;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        Iu iu = new Iu("GET_FILE", 0, (byte) 1);
        c = iu;
        Iu iu2 = new Iu("LIST_FILE", 1, (byte) 2);
        d = iu2;
        Iu iu3 = new Iu("PUT_FILE", 2, (byte) 3);
        e = iu3;
        Iu iu4 = new Iu("VERIFY_FILE", 3, (byte) 4);
        f = iu4;
        Iu iu5 = new Iu("GET_SIZE_WRITTEN", 4, (byte) 5);
        g = iu5;
        Iu iu6 = new Iu("VERIFY_DATA", 5, (byte) 6);
        h = iu6;
        Iu iu7 = new Iu("ERASE_DATA", 6, (byte) 7);
        Iu iu8 = new Iu("EOF_REACH", 7, (byte) 8);
        i = iu8;
        Iu iu9 = new Iu("ABORT_FILE", 8, (byte) 9);
        j = iu9;
        Iu iu10 = new Iu("WAITING_REQUEST", 9, (byte) 10);
        k = iu10;
        Iu iu11 = new Iu("DELETE_FILE", 10, (byte) 11);
        l = iu11;
        m = new Iu[]{iu, iu2, iu3, iu4, iu5, iu6, iu7, iu8, iu9, iu10, iu11};
    }
    */

    @DexIgnore
    public Iu(String str, int i2, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static Iu valueOf(String str) {
        return (Iu) Enum.valueOf(Iu.class, str);
    }

    @DexIgnore
    public static Iu[] values() {
        return (Iu[]) m.clone();
    }

    @DexIgnore
    public final byte a() {
        return (byte) (this.b | Byte.MIN_VALUE);
    }
}
