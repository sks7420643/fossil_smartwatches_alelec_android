package com.fossil;

import com.fossil.M64;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T64 {
    @DexIgnore
    public M64.Bi a;
    @DexIgnore
    public Fg3 b;
    @DexIgnore
    public S64 c;

    @DexIgnore
    public T64(Fg3 fg3, M64.Bi bi) {
        this.a = bi;
        this.b = fg3;
        S64 s64 = new S64(this);
        this.c = s64;
        this.b.b(s64);
    }
}
