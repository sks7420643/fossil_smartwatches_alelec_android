package com.fossil;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Gg2 extends Eg2 {
    @DexIgnore
    public static /* final */ WeakReference<byte[]> d; // = new WeakReference<>(null);
    @DexIgnore
    public WeakReference<byte[]> c; // = d;

    @DexIgnore
    public Gg2(byte[] bArr) {
        super(bArr);
    }

    @DexIgnore
    public abstract byte[] X2();

    @DexIgnore
    @Override // com.fossil.Eg2
    public final byte[] i() {
        byte[] bArr;
        synchronized (this) {
            bArr = this.c.get();
            if (bArr == null) {
                bArr = X2();
                this.c = new WeakReference<>(bArr);
            }
        }
        return bArr;
    }
}
