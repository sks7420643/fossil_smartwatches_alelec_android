package com.fossil;

import android.util.Log;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Eb1 {
    @DexIgnore
    public /* final */ byte[] a; // = new byte[256];
    @DexIgnore
    public ByteBuffer b;
    @DexIgnore
    public Db1 c;
    @DexIgnore
    public int d; // = 0;

    @DexIgnore
    public void a() {
        this.b = null;
        this.c = null;
    }

    @DexIgnore
    public final boolean b() {
        return this.c.b != 0;
    }

    @DexIgnore
    public Db1 c() {
        if (this.b == null) {
            throw new IllegalStateException("You must call setData() before parseHeader()");
        } else if (b()) {
            return this.c;
        } else {
            k();
            if (!b()) {
                h();
                Db1 db1 = this.c;
                if (db1.c < 0) {
                    db1.b = 1;
                }
            }
            return this.c;
        }
    }

    @DexIgnore
    public final int d() {
        try {
            return this.b.get() & 255;
        } catch (Exception e) {
            this.c.b = 1;
            return 0;
        }
    }

    @DexIgnore
    public final void e() {
        boolean z = true;
        this.c.d.a = n();
        this.c.d.b = n();
        this.c.d.c = n();
        this.c.d.d = n();
        int d2 = d();
        boolean z2 = (d2 & 128) != 0;
        int pow = (int) Math.pow(2.0d, (double) ((d2 & 7) + 1));
        Cb1 cb1 = this.c.d;
        if ((d2 & 64) == 0) {
            z = false;
        }
        cb1.e = z;
        if (z2) {
            this.c.d.k = g(pow);
        } else {
            this.c.d.k = null;
        }
        this.c.d.j = this.b.position();
        r();
        if (!b()) {
            Db1 db1 = this.c;
            db1.c++;
            db1.e.add(db1.d);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0028  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void f() {
        /*
            r5 = this;
            r1 = 0
            int r0 = r5.d()
            r5.d = r0
            if (r0 <= 0) goto L_0x0055
            r0 = r1
            r2 = r1
        L_0x000b:
            int r1 = r5.d     // Catch:{ Exception -> 0x001c }
            if (r0 >= r1) goto L_0x0055
            int r1 = r5.d     // Catch:{ Exception -> 0x001c }
            int r1 = r1 - r0
            java.nio.ByteBuffer r2 = r5.b     // Catch:{ Exception -> 0x0056 }
            byte[] r3 = r5.a     // Catch:{ Exception -> 0x0056 }
            r2.get(r3, r0, r1)     // Catch:{ Exception -> 0x0056 }
            int r0 = r0 + r1
            r2 = r1
            goto L_0x000b
        L_0x001c:
            r1 = move-exception
            r3 = r1
            r4 = r2
        L_0x001f:
            java.lang.String r1 = "GifHeaderParser"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)
            if (r1 == 0) goto L_0x0050
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Error Reading Block n: "
            r1.append(r2)
            r1.append(r0)
            java.lang.String r0 = " count: "
            r1.append(r0)
            r1.append(r4)
            java.lang.String r0 = " blockSize: "
            r1.append(r0)
            int r0 = r5.d
            r1.append(r0)
            java.lang.String r0 = "GifHeaderParser"
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r0, r1, r3)
        L_0x0050:
            com.fossil.Db1 r0 = r5.c
            r1 = 1
            r0.b = r1
        L_0x0055:
            return
        L_0x0056:
            r2 = move-exception
            r3 = r2
            r4 = r1
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Eb1.f():void");
    }

    @DexIgnore
    public final int[] g(int i) {
        byte[] bArr = new byte[(i * 3)];
        int[] iArr = null;
        try {
            this.b.get(bArr);
            iArr = new int[256];
            int i2 = 0;
            for (int i3 = 0; i3 < i; i3++) {
                int i4 = i2 + 1;
                int i5 = i4 + 1;
                iArr[i3] = ((bArr[i2] & 255) << 16) | -16777216 | ((bArr[i4] & 255) << 8) | (bArr[i5] & 255);
                i2 = i5 + 1;
            }
        } catch (BufferUnderflowException e) {
            if (Log.isLoggable("GifHeaderParser", 3)) {
                Log.d("GifHeaderParser", "Format Error Reading Color Table", e);
            }
            this.c.b = 1;
        }
        return iArr;
    }

    @DexIgnore
    public final void h() {
        i(Integer.MAX_VALUE);
    }

    @DexIgnore
    public final void i(int i) {
        boolean z = false;
        while (!z && !b() && this.c.c <= i) {
            int d2 = d();
            if (d2 == 33) {
                int d3 = d();
                if (d3 == 1) {
                    q();
                } else if (d3 == 249) {
                    this.c.d = new Cb1();
                    j();
                } else if (d3 == 254) {
                    q();
                } else if (d3 != 255) {
                    q();
                } else {
                    f();
                    StringBuilder sb = new StringBuilder();
                    for (int i2 = 0; i2 < 11; i2++) {
                        sb.append((char) this.a[i2]);
                    }
                    if (sb.toString().equals("NETSCAPE2.0")) {
                        m();
                    } else {
                        q();
                    }
                }
            } else if (d2 == 44) {
                Db1 db1 = this.c;
                if (db1.d == null) {
                    db1.d = new Cb1();
                }
                e();
            } else if (d2 != 59) {
                this.c.b = 1;
            } else {
                z = true;
            }
        }
    }

    @DexIgnore
    public final void j() {
        boolean z = true;
        d();
        int d2 = d();
        Cb1 cb1 = this.c.d;
        int i = (d2 & 28) >> 2;
        cb1.g = i;
        if (i == 0) {
            cb1.g = 1;
        }
        Cb1 cb12 = this.c.d;
        if ((d2 & 1) == 0) {
            z = false;
        }
        cb12.f = z;
        int n = n();
        if (n < 2) {
            n = 10;
        }
        Cb1 cb13 = this.c.d;
        cb13.i = n * 10;
        cb13.h = d();
        d();
    }

    @DexIgnore
    public final void k() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            sb.append((char) d());
        }
        if (!sb.toString().startsWith("GIF")) {
            this.c.b = 1;
            return;
        }
        l();
        if (this.c.h && !b()) {
            Db1 db1 = this.c;
            db1.a = g(db1.i);
            Db1 db12 = this.c;
            db12.l = db12.a[db12.j];
        }
    }

    @DexIgnore
    public final void l() {
        this.c.f = n();
        this.c.g = n();
        int d2 = d();
        this.c.h = (d2 & 128) != 0;
        this.c.i = (int) Math.pow(2.0d, (double) ((d2 & 7) + 1));
        this.c.j = d();
        this.c.k = d();
    }

    @DexIgnore
    public final void m() {
        do {
            f();
            byte[] bArr = this.a;
            if (bArr[0] == 1) {
                byte b2 = bArr[1];
                this.c.m = ((bArr[2] & 255) << 8) | (b2 & 255);
            }
            if (this.d <= 0) {
                return;
            }
        } while (!b());
    }

    @DexIgnore
    public final int n() {
        return this.b.getShort();
    }

    @DexIgnore
    public final void o() {
        this.b = null;
        Arrays.fill(this.a, (byte) 0);
        this.c = new Db1();
        this.d = 0;
    }

    @DexIgnore
    public Eb1 p(ByteBuffer byteBuffer) {
        o();
        ByteBuffer asReadOnlyBuffer = byteBuffer.asReadOnlyBuffer();
        this.b = asReadOnlyBuffer;
        asReadOnlyBuffer.position(0);
        this.b.order(ByteOrder.LITTLE_ENDIAN);
        return this;
    }

    @DexIgnore
    public final void q() {
        int d2;
        do {
            d2 = d();
            this.b.position(Math.min(this.b.position() + d2, this.b.limit()));
        } while (d2 > 0);
    }

    @DexIgnore
    public final void r() {
        d();
        q();
    }
}
