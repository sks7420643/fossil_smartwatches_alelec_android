package com.fossil;

import com.fossil.Jn5;
import com.mapped.Rc6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class By6 extends Xx6 {
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ Yx6 f;

    @DexIgnore
    public By6(Yx6 yx6) {
        Wg6.c(yx6, "mPairingInstructionsView");
        this.f = yx6;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        this.f.f();
        this.f.y0(!this.e);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.Xx6
    public void n() {
        Jn5 jn5 = Jn5.b;
        Yx6 yx6 = this.f;
        if (yx6 == null) {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.pairing.PairingInstructionsFragment");
        } else if (Jn5.c(jn5, ((Sx6) yx6).getContext(), Jn5.Ai.PAIR_DEVICE, false, false, false, null, 60, null)) {
            this.f.l1();
        }
    }

    @DexIgnore
    @Override // com.fossil.Xx6
    public boolean o() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Xx6
    public void p(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public void q() {
        this.f.M5(this);
    }
}
