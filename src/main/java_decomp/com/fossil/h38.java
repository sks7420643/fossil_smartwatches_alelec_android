package com.fossil;

import com.fossil.P18;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H38 implements S28 {
    @DexIgnore
    public static /* final */ List<String> f; // = B28.u("connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade", ":method", ":path", ":scheme", ":authority");
    @DexIgnore
    public static /* final */ List<String> g; // = B28.u("connection", "host", "keep-alive", "proxy-connection", "te", "transfer-encoding", "encoding", "upgrade");
    @DexIgnore
    public /* final */ Interceptor.Chain a;
    @DexIgnore
    public /* final */ P28 b;
    @DexIgnore
    public /* final */ I38 c;
    @DexIgnore
    public K38 d;
    @DexIgnore
    public /* final */ T18 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends N48 {
        @DexIgnore
        public boolean c; // = false;
        @DexIgnore
        public long d; // = 0;

        @DexIgnore
        public Ai(C58 c58) {
            super(c58);
        }

        @DexIgnore
        public final void b(IOException iOException) {
            if (!this.c) {
                this.c = true;
                H38 h38 = H38.this;
                h38.b.r(false, h38, this.d, iOException);
            }
        }

        @DexIgnore
        @Override // com.fossil.N48, com.fossil.C58, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            super.close();
            b(null);
        }

        @DexIgnore
        @Override // com.fossil.N48, com.fossil.C58
        public long d0(I48 i48, long j) throws IOException {
            try {
                long d0 = a().d0(i48, j);
                if (d0 > 0) {
                    this.d += d0;
                }
                return d0;
            } catch (IOException e2) {
                b(e2);
                throw e2;
            }
        }
    }

    @DexIgnore
    public H38(OkHttpClient okHttpClient, Interceptor.Chain chain, P28 p28, I38 i38) {
        this.a = chain;
        this.b = p28;
        this.c = i38;
        this.e = okHttpClient.C().contains(T18.H2_PRIOR_KNOWLEDGE) ? T18.H2_PRIOR_KNOWLEDGE : T18.HTTP_2;
    }

    @DexIgnore
    public static List<E38> g(V18 v18) {
        P18 e2 = v18.e();
        ArrayList arrayList = new ArrayList(e2.h() + 4);
        arrayList.add(new E38(E38.f, v18.g()));
        arrayList.add(new E38(E38.g, Y28.c(v18.j())));
        String c2 = v18.c("Host");
        if (c2 != null) {
            arrayList.add(new E38(E38.i, c2));
        }
        arrayList.add(new E38(E38.h, v18.j().E()));
        int h = e2.h();
        for (int i = 0; i < h; i++) {
            L48 encodeUtf8 = L48.encodeUtf8(e2.e(i).toLowerCase(Locale.US));
            if (!f.contains(encodeUtf8.utf8())) {
                arrayList.add(new E38(encodeUtf8, e2.i(i)));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static Response.a h(P18 p18, T18 t18) throws IOException {
        P18.Ai ai = new P18.Ai();
        int h = p18.h();
        A38 a38 = null;
        for (int i = 0; i < h; i++) {
            String e2 = p18.e(i);
            String i2 = p18.i(i);
            if (e2.equals(":status")) {
                a38 = A38.a("HTTP/1.1 " + i2);
            } else if (!g.contains(e2)) {
                Z18.a.b(ai, e2, i2);
            }
        }
        if (a38 != null) {
            Response.a aVar = new Response.a();
            aVar.n(t18);
            aVar.g(a38.b);
            aVar.k(a38.c);
            aVar.j(ai.e());
            return aVar;
        }
        throw new ProtocolException("Expected ':status' header not present");
    }

    @DexIgnore
    @Override // com.fossil.S28
    public void a() throws IOException {
        this.d.j().close();
    }

    @DexIgnore
    @Override // com.fossil.S28
    public void b(V18 v18) throws IOException {
        if (this.d == null) {
            K38 M = this.c.M(g(v18), v18.a() != null);
            this.d = M;
            M.n().g((long) this.a.a(), TimeUnit.MILLISECONDS);
            this.d.u().g((long) this.a.b(), TimeUnit.MILLISECONDS);
        }
    }

    @DexIgnore
    @Override // com.fossil.S28
    public W18 c(Response response) throws IOException {
        P28 p28 = this.b;
        p28.f.q(p28.e);
        return new X28(response.j("Content-Type"), U28.b(response), S48.d(new Ai(this.d.k())));
    }

    @DexIgnore
    @Override // com.fossil.S28
    public void cancel() {
        K38 k38 = this.d;
        if (k38 != null) {
            k38.h(D38.CANCEL);
        }
    }

    @DexIgnore
    @Override // com.fossil.S28
    public Response.a d(boolean z) throws IOException {
        Response.a h = h(this.d.s(), this.e);
        if (!z || Z18.a.d(h) != 100) {
            return h;
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.S28
    public void e() throws IOException {
        this.c.flush();
    }

    @DexIgnore
    @Override // com.fossil.S28
    public A58 f(V18 v18, long j) {
        return this.d.j();
    }
}
