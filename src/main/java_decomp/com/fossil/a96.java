package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A96 implements MembersInjector<ComplicationSearchActivity> {
    @DexIgnore
    public static void a(ComplicationSearchActivity complicationSearchActivity, ComplicationSearchPresenter complicationSearchPresenter) {
        complicationSearchActivity.A = complicationSearchPresenter;
    }
}
