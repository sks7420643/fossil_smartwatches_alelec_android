package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import java.util.Map;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class Yj2 {
    @DexIgnore
    public static Map<String, Yj2> b; // = new Zi0();
    @DexIgnore
    public static Gk2 c;
    @DexIgnore
    public String a; // = "";

    /*
    static {
        Ek2.b().a("gcm_check_for_different_iid_in_token", true);
        TimeUnit.DAYS.toMillis(7);
    }
    */

    @DexIgnore
    public Yj2(Context context, String str) {
        context.getApplicationContext();
        this.a = str;
    }

    @DexIgnore
    public static Yj2 a(Context context, Bundle bundle) {
        Yj2 yj2;
        synchronized (Yj2.class) {
            String string = bundle == null ? "" : bundle.getString("subtype");
            String str = string == null ? "" : string;
            try {
                Context applicationContext = context.getApplicationContext();
                if (c == null) {
                    String packageName = applicationContext.getPackageName();
                    StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 73);
                    sb.append("Instance ID SDK is deprecated, ");
                    sb.append(packageName);
                    sb.append(" should update to use Firebase Instance ID");
                    Log.w("InstanceID", sb.toString());
                    c = new Gk2(applicationContext);
                    new Dk2(applicationContext);
                }
                Integer.toString(b(applicationContext));
                yj2 = b.get(str);
                if (yj2 == null) {
                    yj2 = new Yj2(applicationContext, str);
                    b.put(str, yj2);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return yj2;
    }

    @DexIgnore
    public static int b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            String valueOf = String.valueOf(e);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 38);
            sb.append("Never happens: can't find own package ");
            sb.append(valueOf);
            Log.w("InstanceID", sb.toString());
            return 0;
        }
    }

    @DexIgnore
    public static Gk2 d() {
        return c;
    }

    @DexIgnore
    public final void c() {
        c.c(this.a);
    }
}
