package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P82 extends F92 {
    @DexIgnore
    public /* final */ /* synthetic */ H82 b;
    @DexIgnore
    public /* final */ /* synthetic */ Us3 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public P82(Q82 q82, D92 d92, H82 h82, Us3 us3) {
        super(d92);
        this.b = h82;
        this.c = us3;
    }

    @DexIgnore
    @Override // com.fossil.F92
    public final void a() {
        this.b.m(this.c);
    }
}
