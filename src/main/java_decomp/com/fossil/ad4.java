package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ad4 implements Zc4 {
    @DexIgnore
    public /* final */ Wc4 a;
    @DexIgnore
    public /* final */ Yc4 b;
    @DexIgnore
    public /* final */ Xc4 c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public Ad4(long j, Wc4 wc4, Yc4 yc4, Xc4 xc4, int i, int i2) {
        this.d = j;
        this.a = wc4;
        this.b = yc4;
        this.c = xc4;
    }

    @DexIgnore
    @Override // com.fossil.Zc4
    public Xc4 a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Zc4
    public Yc4 b() {
        return this.b;
    }

    @DexIgnore
    public Wc4 c() {
        return this.a;
    }

    @DexIgnore
    public long d() {
        return this.d;
    }

    @DexIgnore
    public boolean e(long j) {
        return this.d < j;
    }
}
