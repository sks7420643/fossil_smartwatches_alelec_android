package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.mapped.Cd6;
import com.mapped.Gg6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class U5 implements Gd0<H7> {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public /* final */ N5 c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public S5 e;
    @DexIgnore
    public Hg6<? super U5, Cd6> f;
    @DexIgnore
    public Hg6<? super U5, Cd6> g;
    @DexIgnore
    public Gg6<Cd6> h;
    @DexIgnore
    public /* final */ V5 i;
    @DexIgnore
    public /* final */ N4 j;

    @DexIgnore
    public U5(V5 v5, N4 n4) {
        this.i = v5;
        this.j = n4;
        this.a = Ey1.a(v5);
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.b = new Handler(myLooper);
            this.c = N5.b;
            this.e = new S5(this.i, R5.c, null, 4);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final U5 a(Gg6<Cd6> gg6) {
        this.h = gg6;
        return this;
    }

    @DexIgnore
    public JSONObject b(boolean z) {
        return new JSONObject();
    }

    @DexIgnore
    public final void c() {
        if (!this.d) {
            this.d = true;
            Fd0<H7> j2 = j();
            if (j2 != null) {
                j2.d(this);
            }
            R5 r5 = R5.b;
            S5 s5 = this.e;
            if (r5 == s5.c) {
                M80.c.a(this.a, "Command success: %s.", Ox1.toJSONString$default(s5, 0, 1, null));
                Hg6<? super U5, Cd6> hg6 = this.f;
                if (hg6 != null) {
                    hg6.invoke(this);
                }
            } else {
                M80.c.b(this.a, "Command error: %s.", Ox1.toJSONString$default(s5, 0, 1, null));
                Hg6<? super U5, Cd6> hg62 = this.g;
                if (hg62 != null) {
                    hg62.invoke(this);
                }
            }
            Gg6<Cd6> gg6 = this.h;
            if (gg6 != null) {
                gg6.invoke();
            }
        }
    }

    @DexIgnore
    public abstract void d(K5 k5);

    @DexIgnore
    public final void e(R5 r5) {
        if (!this.d) {
            M80.c.a(this.a, "command stop: %s.", r5);
            this.e = S5.a(this.e, null, r5, null, 5);
            if (l()) {
                this.b.postDelayed(new T5(this), 5000);
            } else {
                c();
            }
        }
    }

    @DexIgnore
    public void f(H7 h7) {
        k(h7);
        S5 a2 = S5.e.a(h7.a);
        this.e = S5.a(this.e, null, a2.c, a2.d, 1);
    }

    @DexIgnore
    public void g(Object obj) {
        H7 h7 = (H7) obj;
        if (i(h7)) {
            R5 r5 = this.e.c;
            if (r5 == R5.m || r5 == R5.f) {
                this.e = S5.a(this.e, null, null, h7.a, 3);
            } else {
                f(h7);
            }
            c();
        }
    }

    @DexIgnore
    public N5 h() {
        return this.c;
    }

    @DexIgnore
    public abstract boolean i(H7 h7);

    @DexIgnore
    public abstract Fd0<H7> j();

    @DexIgnore
    public void k(H7 h7) {
    }

    @DexIgnore
    public boolean l() {
        return false;
    }
}
