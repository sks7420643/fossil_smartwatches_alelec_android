package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qy1<V> extends py1<V, yx1> {
    @DexIgnore
    public qy1<V> p(rp7<? super tl7, tl7> rp7) {
        pq7.c(rp7, "actionOnFinal");
        super.e(rp7);
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.rp7<? super com.fossil.yx1, com.fossil.tl7> */
    /* JADX WARN: Multi-variable type inference failed */
    /* renamed from: q */
    public qy1<V> k(rp7<? super yx1, tl7> rp7) {
        pq7.c(rp7, "actionOnCancel");
        super.k(rp7);
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.rp7<? super com.fossil.yx1, com.fossil.tl7> */
    /* JADX WARN: Multi-variable type inference failed */
    /* renamed from: r */
    public qy1<V> l(rp7<? super yx1, tl7> rp7) {
        pq7.c(rp7, "actionOnError");
        super.l(rp7);
        return this;
    }

    @DexIgnore
    /* renamed from: s */
    public qy1<V> m(rp7<? super V, tl7> rp7) {
        pq7.c(rp7, "actionOnSuccess");
        super.m(rp7);
        return this;
    }
}
