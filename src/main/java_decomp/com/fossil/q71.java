package com.fossil;

import android.annotation.SuppressLint;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import com.fossil.O71;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"MissingPermission"})
public final class Q71 implements O71 {
    @DexIgnore
    public static /* final */ NetworkRequest e; // = new NetworkRequest.Builder().addCapability(12).build();
    @DexIgnore
    public /* final */ Ai b; // = new Ai(this);
    @DexIgnore
    public /* final */ ConnectivityManager c;
    @DexIgnore
    public /* final */ O71.Bi d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends ConnectivityManager.NetworkCallback {
        @DexIgnore
        public /* final */ /* synthetic */ Q71 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(Q71 q71) {
            this.a = q71;
        }

        @DexIgnore
        public void onAvailable(Network network) {
            Wg6.c(network, "network");
            this.a.d(network, true);
        }

        @DexIgnore
        public void onLost(Network network) {
            Wg6.c(network, "network");
            this.a.d(network, false);
        }
    }

    @DexIgnore
    public Q71(ConnectivityManager connectivityManager, O71.Bi bi) {
        Wg6.c(connectivityManager, "connectivityManager");
        Wg6.c(bi, "listener");
        this.c = connectivityManager;
        this.d = bi;
    }

    @DexIgnore
    @Override // com.fossil.O71
    public boolean a() {
        Network[] allNetworks = this.c.getAllNetworks();
        Wg6.b(allNetworks, "connectivityManager.allNetworks");
        for (Network network : allNetworks) {
            Wg6.b(network, "it");
            if (c(network)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean c(Network network) {
        NetworkCapabilities networkCapabilities = this.c.getNetworkCapabilities(network);
        return networkCapabilities != null && networkCapabilities.hasCapability(12);
    }

    @DexIgnore
    public final void d(Network network, boolean z) {
        boolean c2;
        boolean z2 = false;
        Network[] allNetworks = this.c.getAllNetworks();
        Wg6.b(allNetworks, "connectivityManager.allNetworks");
        int length = allNetworks.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            Network network2 = allNetworks[i];
            if (Wg6.a(network2, network)) {
                c2 = z;
            } else {
                Wg6.b(network2, "it");
                c2 = c(network2);
            }
            if (c2) {
                z2 = true;
                break;
            }
            i++;
        }
        this.d.a(z2);
    }

    @DexIgnore
    @Override // com.fossil.O71
    public void start() {
        this.c.registerNetworkCallback(e, this.b);
    }

    @DexIgnore
    @Override // com.fossil.O71
    public void stop() {
        this.c.unregisterNetworkCallback(this.b);
    }
}
