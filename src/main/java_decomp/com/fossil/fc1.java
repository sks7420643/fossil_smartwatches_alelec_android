package com.fossil;

import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import com.fossil.Xb1;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fc1 implements Xb1<ParcelFileDescriptor> {
    @DexIgnore
    public /* final */ Bi a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Xb1.Ai<ParcelFileDescriptor> {
        @DexIgnore
        /* Return type fixed from 'com.fossil.Xb1' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Xb1.Ai
        public /* bridge */ /* synthetic */ Xb1<ParcelFileDescriptor> a(ParcelFileDescriptor parcelFileDescriptor) {
            return b(parcelFileDescriptor);
        }

        @DexIgnore
        public Xb1<ParcelFileDescriptor> b(ParcelFileDescriptor parcelFileDescriptor) {
            return new Fc1(parcelFileDescriptor);
        }

        @DexIgnore
        @Override // com.fossil.Xb1.Ai
        public Class<ParcelFileDescriptor> getDataClass() {
            return ParcelFileDescriptor.class;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ ParcelFileDescriptor a;

        @DexIgnore
        public Bi(ParcelFileDescriptor parcelFileDescriptor) {
            this.a = parcelFileDescriptor;
        }

        @DexIgnore
        public ParcelFileDescriptor a() throws IOException {
            try {
                Os.lseek(this.a.getFileDescriptor(), 0, OsConstants.SEEK_SET);
                return this.a;
            } catch (ErrnoException e) {
                throw new IOException(e);
            }
        }
    }

    @DexIgnore
    public Fc1(ParcelFileDescriptor parcelFileDescriptor) {
        this.a = new Bi(parcelFileDescriptor);
    }

    @DexIgnore
    public static boolean c() {
        return Build.VERSION.SDK_INT >= 21;
    }

    @DexIgnore
    @Override // com.fossil.Xb1
    public void a() {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xb1
    public /* bridge */ /* synthetic */ ParcelFileDescriptor b() throws IOException {
        return d();
    }

    @DexIgnore
    public ParcelFileDescriptor d() throws IOException {
        return this.a.a();
    }
}
