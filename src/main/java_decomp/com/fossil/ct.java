package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ct extends Bt {
    @DexIgnore
    public long K;
    @DexIgnore
    public /* final */ long L;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Ct(K5 k5, long j) {
        super(k5, Ut.i, Hs.f0, 0, 8);
        boolean z = false;
        this.L = j;
        if (j >= 0 && j <= Hy1.b(Oq7.a)) {
            z = true;
        }
        if (z) {
            this.K = this.L;
            return;
        }
        StringBuilder e = E.e("timeoutInMs (");
        e.append(this.L);
        e.append(") must be in [0, [");
        e.append(Hy1.b(Oq7.a));
        e.append("]].");
        throw new IllegalArgumentException(e.toString().toString());
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.L).array();
        Wg6.b(array, "ByteBuffer.allocate(4)\n \u2026\n                .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void f(long j) {
        this.K = j;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public long x() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(super.z(), Jd0.K4, Long.valueOf(this.L));
    }
}
