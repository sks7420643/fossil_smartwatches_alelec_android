package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Di1 {
    @DexIgnore
    void a(Ei1 ei1);

    @DexIgnore
    void b(Ei1 ei1);
}
