package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gf3 implements Parcelable.Creator<Qe3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Qe3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int i = 0;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        int i2 = 0;
        ArrayList arrayList = null;
        Ce3 ce3 = null;
        Ce3 ce32 = null;
        ArrayList arrayList2 = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 2:
                    arrayList2 = Ad2.j(parcel, t, LatLng.CREATOR);
                    break;
                case 3:
                    f = Ad2.r(parcel, t);
                    break;
                case 4:
                    i = Ad2.v(parcel, t);
                    break;
                case 5:
                    f2 = Ad2.r(parcel, t);
                    break;
                case 6:
                    z = Ad2.m(parcel, t);
                    break;
                case 7:
                    z2 = Ad2.m(parcel, t);
                    break;
                case 8:
                    z3 = Ad2.m(parcel, t);
                    break;
                case 9:
                    ce32 = (Ce3) Ad2.e(parcel, t, Ce3.CREATOR);
                    break;
                case 10:
                    ce3 = (Ce3) Ad2.e(parcel, t, Ce3.CREATOR);
                    break;
                case 11:
                    i2 = Ad2.v(parcel, t);
                    break;
                case 12:
                    arrayList = Ad2.j(parcel, t, Me3.CREATOR);
                    break;
                default:
                    Ad2.B(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new Qe3(arrayList2, f, i, f2, z, z2, z3, ce32, ce3, i2, arrayList);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Qe3[] newArray(int i) {
        return new Qe3[i];
    }
}
