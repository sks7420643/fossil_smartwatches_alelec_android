package com.fossil;

import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.watchface.usecase.PreviewWatchFaceUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xb7 implements Factory<PreviewWatchFaceUseCase> {
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> a;
    @DexIgnore
    public /* final */ Provider<FileRepository> b;

    @DexIgnore
    public Xb7(Provider<DianaWatchFaceRepository> provider, Provider<FileRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Xb7 a(Provider<DianaWatchFaceRepository> provider, Provider<FileRepository> provider2) {
        return new Xb7(provider, provider2);
    }

    @DexIgnore
    public static PreviewWatchFaceUseCase c(DianaWatchFaceRepository dianaWatchFaceRepository, FileRepository fileRepository) {
        return new PreviewWatchFaceUseCase(dianaWatchFaceRepository, fileRepository);
    }

    @DexIgnore
    public PreviewWatchFaceUseCase b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
