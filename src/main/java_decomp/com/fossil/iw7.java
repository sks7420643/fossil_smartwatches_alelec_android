package com.fossil;

import com.fossil.Tv7;
import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Lk6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Iw7 extends Jw7 implements Tv7 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater f; // = AtomicReferenceFieldUpdater.newUpdater(Iw7.class, Object.class, "_queue");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater g; // = AtomicReferenceFieldUpdater.newUpdater(Iw7.class, Object.class, "_delayed");
    @DexIgnore
    public volatile Object _delayed; // = null;
    @DexIgnore
    public volatile int _isCompleted; // = 0;
    @DexIgnore
    public volatile Object _queue; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends Ci {
        @DexIgnore
        public /* final */ Lk6<Cd6> e;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.mapped.Lk6<? super com.mapped.Cd6> */
        /* JADX WARN: Multi-variable type inference failed */
        public Ai(long j, Lk6<? super Cd6> lk6) {
            super(j);
            this.e = lk6;
        }

        @DexIgnore
        public void run() {
            this.e.f(Iw7.this, Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Iw7.Ci
        public String toString() {
            return super.toString() + this.e.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ci {
        @DexIgnore
        public /* final */ Runnable e;

        @DexIgnore
        public Bi(long j, Runnable runnable) {
            super(j);
            this.e = runnable;
        }

        @DexIgnore
        public void run() {
            this.e.run();
        }

        @DexIgnore
        @Override // com.fossil.Iw7.Ci
        public String toString() {
            return super.toString() + this.e.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ci implements Runnable, Comparable<Ci>, Dw7, B08 {
        @DexIgnore
        public Object b;
        @DexIgnore
        public int c; // = -1;
        @DexIgnore
        public long d;

        @DexIgnore
        public Ci(long j) {
            this.d = j;
        }

        @DexIgnore
        @Override // com.fossil.B08
        public int a() {
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.B08
        public void b(A08<?> a08) {
            if (this.b != Lw7.b()) {
                this.b = a08;
                return;
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // java.lang.Comparable
        public /* bridge */ /* synthetic */ int compareTo(Ci ci) {
            return h(ci);
        }

        @DexIgnore
        @Override // com.fossil.Dw7
        public final void dispose() {
            synchronized (this) {
                Object obj = this.b;
                if (obj != Lw7.b()) {
                    if (!(obj instanceof Di)) {
                        obj = null;
                    }
                    Di di = (Di) obj;
                    if (di != null) {
                        di.g(this);
                    }
                    this.b = Lw7.b();
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.B08
        public A08<?> e() {
            Object obj = this.b;
            if (!(obj instanceof A08)) {
                obj = null;
            }
            return (A08) obj;
        }

        @DexIgnore
        @Override // com.fossil.B08
        public void f(int i) {
            this.c = i;
        }

        @DexIgnore
        public int h(Ci ci) {
            int i = ((this.d - ci.d) > 0 ? 1 : ((this.d - ci.d) == 0 ? 0 : -1));
            if (i > 0) {
                return 1;
            }
            return i < 0 ? -1 : 0;
        }

        @DexIgnore
        public final int i(long j, Di di, Iw7 iw7) {
            synchronized (this) {
                if (this.b == Lw7.b()) {
                    return 2;
                }
                synchronized (di) {
                    Ci ci = (Ci) di.b();
                    if (iw7.B0()) {
                        return 1;
                    }
                    if (ci == null) {
                        di.b = j;
                    } else {
                        long j2 = ci.d;
                        if (j2 - j < 0) {
                            j = j2;
                        }
                        if (j - di.b > 0) {
                            di.b = j;
                        }
                    }
                    if (this.d - di.b < 0) {
                        this.d = di.b;
                    }
                    di.a(this);
                    return 0;
                }
            }
        }

        @DexIgnore
        public final boolean j(long j) {
            return j - this.d >= 0;
        }

        @DexIgnore
        public String toString() {
            return "Delayed[nanos=" + this.d + ']';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends A08<Ci> {
        @DexIgnore
        public long b;

        @DexIgnore
        public Di(long j) {
            this.b = j;
        }
    }

    @DexIgnore
    public final boolean A0(Runnable runnable) {
        while (true) {
            Object obj = this._queue;
            if (B0()) {
                return false;
            }
            if (obj == null) {
                if (f.compareAndSet(this, null, runnable)) {
                    return true;
                }
            } else if (obj instanceof Nz7) {
                if (obj != null) {
                    Nz7 nz7 = (Nz7) obj;
                    int a2 = nz7.a(runnable);
                    if (a2 == 0) {
                        return true;
                    }
                    if (a2 == 1) {
                        f.compareAndSet(this, obj, nz7.i());
                    } else if (a2 == 2) {
                        return false;
                    }
                } else {
                    throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.Queue<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> /* = kotlinx.coroutines.internal.LockFreeTaskQueueCore<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> */");
                }
            } else if (obj == Lw7.a()) {
                return false;
            } else {
                Nz7 nz72 = new Nz7(8, true);
                if (obj != null) {
                    nz72.a((Runnable) obj);
                    nz72.a(runnable);
                    if (f.compareAndSet(this, obj, nz72)) {
                        return true;
                    }
                } else {
                    throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                }
            }
        }
    }

    @DexIgnore
    /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean B0() {
        /*
            r1 = this;
            int r0 = r1._isCompleted
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Iw7.B0():boolean");
    }

    @DexIgnore
    public boolean C0() {
        boolean z;
        if (!p0()) {
            return false;
        }
        Di di = (Di) this._delayed;
        if (di != null && !di.d()) {
            return false;
        }
        Object obj = this._queue;
        if (obj != null) {
            if (obj instanceof Nz7) {
                z = ((Nz7) obj).g();
            } else if (obj != Lw7.a()) {
                z = false;
            }
            return z;
        }
        z = true;
        return z;
    }

    @DexIgnore
    public final void D0() {
        Ci ci;
        Xx7 a2 = Yx7.a();
        long a3 = a2 != null ? a2.a() : System.nanoTime();
        while (true) {
            Di di = (Di) this._delayed;
            if (di != null && (ci = (Ci) di.i()) != null) {
                u0(a3, ci);
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final void E0() {
        this._queue = null;
        this._delayed = null;
    }

    @DexIgnore
    public final void F0(long j, Ci ci) {
        int G0 = G0(j, ci);
        if (G0 != 0) {
            if (G0 == 1) {
                u0(j, ci);
            } else if (G0 != 2) {
                throw new IllegalStateException("unexpected result".toString());
            }
        } else if (J0(ci)) {
            v0();
        }
    }

    @DexIgnore
    @Override // com.fossil.Tv7
    public Dw7 G(long j, Runnable runnable) {
        return Tv7.Ai.a(this, j, runnable);
    }

    @DexIgnore
    public final int G0(long j, Ci ci) {
        if (B0()) {
            return 1;
        }
        Di di = (Di) this._delayed;
        if (di == null) {
            g.compareAndSet(this, null, new Di(j));
            Object obj = this._delayed;
            if (obj != null) {
                di = (Di) obj;
            } else {
                Wg6.i();
                throw null;
            }
        }
        return ci.i(j, di, this);
    }

    @DexIgnore
    public final Dw7 H0(long j, Runnable runnable) {
        long c = Lw7.c(j);
        if (c >= 4611686018427387903L) {
            return Lx7.b;
        }
        Xx7 a2 = Yx7.a();
        long a3 = a2 != null ? a2.a() : System.nanoTime();
        Bi bi = new Bi(c + a3, runnable);
        F0(a3, bi);
        return bi;
    }

    @DexIgnore
    public final void I0(boolean z) {
        this._isCompleted = z ? 1 : 0;
    }

    @DexIgnore
    public final boolean J0(Ci ci) {
        Di di = (Di) this._delayed;
        return (di != null ? (Ci) di.e() : null) == ci;
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public final void M(Af6 af6, Runnable runnable) {
        z0(runnable);
    }

    @DexIgnore
    @Override // com.fossil.Hw7
    public long b0() {
        Ci ci;
        if (super.b0() == 0) {
            return 0;
        }
        Object obj = this._queue;
        if (obj != null) {
            if (!(obj instanceof Nz7)) {
                return obj == Lw7.a() ? Long.MAX_VALUE : 0;
            }
            if (!((Nz7) obj).g()) {
                return 0;
            }
        }
        Di di = (Di) this._delayed;
        if (di == null || (ci = (Ci) di.e()) == null) {
            return Long.MAX_VALUE;
        }
        long j = ci.d;
        Xx7 a2 = Yx7.a();
        return Bs7.e(j - (a2 != null ? a2.a() : System.nanoTime()), 0);
    }

    @DexIgnore
    @Override // com.fossil.Tv7
    public void f(long j, Lk6<? super Cd6> lk6) {
        long c = Lw7.c(j);
        if (c < 4611686018427387903L) {
            Xx7 a2 = Yx7.a();
            long a3 = a2 != null ? a2.a() : System.nanoTime();
            Ai ai = new Ai(c + a3, lk6);
            Nu7.a(lk6, ai);
            F0(a3, ai);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0048  */
    @Override // com.fossil.Hw7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long q0() {
        /*
            r7 = this;
            r2 = 0
            r3 = 0
            boolean r0 = r7.r0()
            if (r0 == 0) goto L_0x000d
            long r0 = r7.b0()
        L_0x000c:
            return r0
        L_0x000d:
            java.lang.Object r0 = r7._delayed
            com.fossil.Iw7$Di r0 = (com.fossil.Iw7.Di) r0
            if (r0 == 0) goto L_0x0042
            boolean r1 = r0.d()
            if (r1 != 0) goto L_0x0042
            com.fossil.Xx7 r1 = com.fossil.Yx7.a()
            if (r1 == 0) goto L_0x0050
            long r4 = r1.a()
        L_0x0023:
            monitor-enter(r0)
            com.fossil.B08 r1 = r0.b()     // Catch:{ all -> 0x005a }
            if (r1 == 0) goto L_0x0057
            com.fossil.Iw7$Ci r1 = (com.fossil.Iw7.Ci) r1     // Catch:{ all -> 0x005a }
            boolean r6 = r1.j(r4)     // Catch:{ all -> 0x005a }
            if (r6 == 0) goto L_0x0055
            boolean r1 = r7.A0(r1)     // Catch:{ all -> 0x005a }
        L_0x0036:
            if (r1 == 0) goto L_0x005d
            r1 = 0
            com.fossil.B08 r1 = r0.h(r1)     // Catch:{ all -> 0x005a }
        L_0x003d:
            monitor-exit(r0)
        L_0x003e:
            com.fossil.Iw7$Ci r1 = (com.fossil.Iw7.Ci) r1
            if (r1 != 0) goto L_0x0023
        L_0x0042:
            java.lang.Runnable r0 = r7.y0()
            if (r0 == 0) goto L_0x004b
            r0.run()
        L_0x004b:
            long r0 = r7.b0()
            goto L_0x000c
        L_0x0050:
            long r4 = java.lang.System.nanoTime()
            goto L_0x0023
        L_0x0055:
            r1 = r3
            goto L_0x0036
        L_0x0057:
            monitor-exit(r0)
            r1 = r2
            goto L_0x003e
        L_0x005a:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        L_0x005d:
            r1 = r2
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Iw7.q0():long");
    }

    @DexIgnore
    @Override // com.fossil.Hw7
    public void shutdown() {
        Wx7.b.c();
        I0(true);
        x0();
        do {
        } while (q0() <= 0);
        D0();
    }

    @DexIgnore
    public final void x0() {
        if (!Nv7.a() || B0()) {
            while (true) {
                Object obj = this._queue;
                if (obj == null) {
                    if (f.compareAndSet(this, null, Lw7.a())) {
                        return;
                    }
                } else if (obj instanceof Nz7) {
                    ((Nz7) obj).d();
                    return;
                } else if (obj != Lw7.a()) {
                    Nz7 nz7 = new Nz7(8, true);
                    if (obj != null) {
                        nz7.a((Runnable) obj);
                        if (f.compareAndSet(this, obj, nz7)) {
                            return;
                        }
                    } else {
                        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                    }
                } else {
                    return;
                }
            }
        } else {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public final Runnable y0() {
        while (true) {
            Object obj = this._queue;
            if (obj == null) {
                return null;
            }
            if (obj instanceof Nz7) {
                if (obj != null) {
                    Nz7 nz7 = (Nz7) obj;
                    Object j = nz7.j();
                    if (j != Nz7.g) {
                        return (Runnable) j;
                    }
                    f.compareAndSet(this, obj, nz7.i());
                } else {
                    throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.Queue<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> /* = kotlinx.coroutines.internal.LockFreeTaskQueueCore<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> */");
                }
            } else if (obj == Lw7.a()) {
                return null;
            } else {
                if (f.compareAndSet(this, obj, null)) {
                    if (obj != null) {
                        return (Runnable) obj;
                    }
                    throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                }
            }
        }
    }

    @DexIgnore
    public final void z0(Runnable runnable) {
        if (A0(runnable)) {
            v0();
        } else {
            Pv7.i.z0(runnable);
        }
    }
}
