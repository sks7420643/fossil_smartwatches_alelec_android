package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface P91 {
    @DexIgnore
    void a(M91<?> m91, O91<?> o91);

    @DexIgnore
    void b(M91<?> m91, O91<?> o91, Runnable runnable);

    @DexIgnore
    void c(M91<?> m91, T91 t91);
}
