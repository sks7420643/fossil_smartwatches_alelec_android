package com.fossil;

import android.location.Location;
import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.model.PlaceFields;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ka6 extends ts0 {
    @DexIgnore
    public static /* final */ String f;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public PlacesClient f1888a;
    @DexIgnore
    public AddressWrapper b;
    @DexIgnore
    public AddressWrapper c;
    @DexIgnore
    public List<String> d;
    @DexIgnore
    public MutableLiveData<a> e; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public AddressWrapper.AddressType f1889a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Boolean d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public Boolean f;
        @DexIgnore
        public PlacesClient g;
        @DexIgnore
        public Boolean h;
        @DexIgnore
        public Boolean i;

        @DexIgnore
        public a(AddressWrapper.AddressType addressType, String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4, Boolean bool5) {
            this.f1889a = addressType;
            this.b = str;
            this.c = str2;
            this.d = bool;
            this.e = bool2;
            this.f = bool3;
            this.g = placesClient;
            this.h = bool4;
            this.i = bool5;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final Boolean b() {
            return this.d;
        }

        @DexIgnore
        public final String c() {
            return this.b;
        }

        @DexIgnore
        public final PlacesClient d() {
            return this.g;
        }

        @DexIgnore
        public final AddressWrapper.AddressType e() {
            return this.f1889a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (!pq7.a(this.f1889a, aVar.f1889a) || !pq7.a(this.b, aVar.b) || !pq7.a(this.c, aVar.c) || !pq7.a(this.d, aVar.d) || !pq7.a(this.e, aVar.e) || !pq7.a(this.f, aVar.f) || !pq7.a(this.g, aVar.g) || !pq7.a(this.h, aVar.h) || !pq7.a(this.i, aVar.i)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final Boolean f() {
            return this.h;
        }

        @DexIgnore
        public final Boolean g() {
            return this.i;
        }

        @DexIgnore
        public final Boolean h() {
            return this.e;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = 0;
            AddressWrapper.AddressType addressType = this.f1889a;
            int hashCode = addressType != null ? addressType.hashCode() : 0;
            String str = this.b;
            int hashCode2 = str != null ? str.hashCode() : 0;
            String str2 = this.c;
            int hashCode3 = str2 != null ? str2.hashCode() : 0;
            Boolean bool = this.d;
            int hashCode4 = bool != null ? bool.hashCode() : 0;
            Boolean bool2 = this.e;
            int hashCode5 = bool2 != null ? bool2.hashCode() : 0;
            Boolean bool3 = this.f;
            int hashCode6 = bool3 != null ? bool3.hashCode() : 0;
            PlacesClient placesClient = this.g;
            int hashCode7 = placesClient != null ? placesClient.hashCode() : 0;
            Boolean bool4 = this.h;
            int hashCode8 = bool4 != null ? bool4.hashCode() : 0;
            Boolean bool5 = this.i;
            if (bool5 != null) {
                i2 = bool5.hashCode();
            }
            return (((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + i2;
        }

        @DexIgnore
        public final Boolean i() {
            return this.f;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(type=" + this.f1889a + ", name=" + this.b + ", address=" + this.c + ", avoidTolls=" + this.d + ", isNameEditable=" + this.e + ", isValid=" + this.f + ", placesClient=" + this.g + ", isLoading=" + this.h + ", isLocationUnavailable=" + this.i + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<TResult> implements jt3<FetchPlaceResponse> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ka6 f1890a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public b(ka6 ka6, String str) {
            this.f1890a = ka6;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
            AddressWrapper addressWrapper = this.f1890a.b;
            if (addressWrapper != null) {
                pq7.b(fetchPlaceResponse, "response");
                Place place = fetchPlaceResponse.getPlace();
                pq7.b(place, "response.place");
                LatLng latLng = place.getLatLng();
                if (latLng != null) {
                    addressWrapper.setLat(latLng.b);
                } else {
                    pq7.i();
                    throw null;
                }
            }
            AddressWrapper addressWrapper2 = this.f1890a.b;
            if (addressWrapper2 != null) {
                pq7.b(fetchPlaceResponse, "response");
                Place place2 = fetchPlaceResponse.getPlace();
                pq7.b(place2, "response.place");
                LatLng latLng2 = place2.getLatLng();
                if (latLng2 != null) {
                    addressWrapper2.setLng(latLng2.c);
                } else {
                    pq7.i();
                    throw null;
                }
            }
            AddressWrapper addressWrapper3 = this.f1890a.b;
            if (addressWrapper3 != null) {
                addressWrapper3.setAddress(this.b);
            }
            ka6 ka6 = this.f1890a;
            ka6.f(ka6, null, null, null, null, null, Boolean.valueOf(ka6.l()), null, Boolean.FALSE, Boolean.FALSE, 95, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements it3 {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ka6 f1891a;

        @DexIgnore
        public c(ka6 ka6) {
            this.f1891a = ka6;
        }

        @DexIgnore
        @Override // com.fossil.it3
        public final void onFailure(Exception exc) {
            pq7.c(exc, "exception");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = ka6.f;
            local.e(str, "FetchPlaceRequest - exception=" + exc);
            ka6 ka6 = this.f1891a;
            ka6.f(ka6, null, null, null, null, null, Boolean.valueOf(ka6.l()), null, Boolean.FALSE, Boolean.TRUE, 95, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel$start$1", f = "CommuteTimeSettingsDetailViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ka6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ka6 ka6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ka6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.f1888a = Places.createClient(PortfolioApp.h0.c());
                AddressWrapper addressWrapper = this.this$0.b;
                boolean z = (addressWrapper != null ? addressWrapper.getType() : null) == AddressWrapper.AddressType.OTHER;
                ka6 ka6 = this.this$0;
                AddressWrapper addressWrapper2 = ka6.b;
                AddressWrapper.AddressType type = addressWrapper2 != null ? addressWrapper2.getType() : null;
                AddressWrapper addressWrapper3 = this.this$0.b;
                String name = addressWrapper3 != null ? addressWrapper3.getName() : null;
                Boolean a2 = ao7.a(z);
                AddressWrapper addressWrapper4 = this.this$0.b;
                String address = addressWrapper4 != null ? addressWrapper4.getAddress() : null;
                AddressWrapper addressWrapper5 = this.this$0.b;
                ka6.f(ka6, type, name, address, addressWrapper5 != null ? ao7.a(addressWrapper5.getAvoidTolls()) : null, a2, null, this.this$0.f1888a, null, null, 416, null);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = ka6.class.getSimpleName();
        pq7.b(simpleName, "CommuteTimeSettingsDetai\u2026el::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public ka6(on5 on5) {
        pq7.c(on5, "mSharedPreferencesManager");
    }

    @DexIgnore
    public static /* synthetic */ void f(ka6 ka6, AddressWrapper.AddressType addressType, String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4, Boolean bool5, int i, Object obj) {
        ka6.e((i & 1) != 0 ? null : addressType, (i & 2) != 0 ? null : str, (i & 4) != 0 ? null : str2, (i & 8) != 0 ? null : bool, (i & 16) != 0 ? null : bool2, (i & 32) != 0 ? null : bool3, (i & 64) != 0 ? null : placesClient, (i & 128) != 0 ? null : bool4, (i & 256) != 0 ? null : bool5);
    }

    @DexIgnore
    public static /* synthetic */ void n(ka6 ka6, String str, String str2, AutocompleteSessionToken autocompleteSessionToken, int i, Object obj) {
        if ((i & 1) != 0) {
            str = "";
        }
        if ((i & 2) != 0) {
            str2 = null;
        }
        if ((i & 4) != 0) {
            autocompleteSessionToken = null;
        }
        ka6.m(str, str2, autocompleteSessionToken);
    }

    @DexIgnore
    public final void e(AddressWrapper.AddressType addressType, String str, String str2, Boolean bool, Boolean bool2, Boolean bool3, PlacesClient placesClient, Boolean bool4, Boolean bool5) {
        this.e.l(new a(addressType, str, str2, bool, bool2, bool3, placesClient, bool4, bool5));
    }

    @DexIgnore
    public final AddressWrapper g() {
        return this.b;
    }

    @DexIgnore
    public final void h(String str, AutocompleteSessionToken autocompleteSessionToken, String str2) {
        if (this.f1888a != null && this.b != null) {
            f(this, null, null, null, null, null, null, null, Boolean.TRUE, null, 383, null);
            ArrayList arrayList = new ArrayList();
            arrayList.add(Place.Field.ADDRESS);
            arrayList.add(Place.Field.LAT_LNG);
            FetchPlaceRequest.Builder builder = FetchPlaceRequest.builder(str, arrayList);
            pq7.b(builder, "FetchPlaceRequest.builder(placeId, placeFields)");
            builder.setSessionToken(autocompleteSessionToken);
            PlacesClient placesClient = this.f1888a;
            if (placesClient != null) {
                nt3<FetchPlaceResponse> fetchPlace = placesClient.fetchPlace(builder.build());
                pq7.b(fetchPlace, "mPlacesClient!!.fetchPlace(request.build())");
                fetchPlace.f(new b(this, str2));
                fetchPlace.d(new c(this));
                return;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final MutableLiveData<a> i() {
        return this.e;
    }

    @DexIgnore
    public final void j(AddressWrapper addressWrapper, ArrayList<String> arrayList) {
        this.c = addressWrapper;
        this.b = addressWrapper != null ? addressWrapper.clone() : null;
        this.d = arrayList;
    }

    @DexIgnore
    public final boolean k() {
        String str;
        String name;
        List<String> list = this.d;
        if (list != null) {
            for (String str2 : list) {
                AddressWrapper addressWrapper = this.b;
                if (addressWrapper == null || (name = addressWrapper.getName()) == null) {
                    str = null;
                } else if (name != null) {
                    str = name.toUpperCase();
                    pq7.b(str, "(this as java.lang.String).toUpperCase()");
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
                if (str2 != null) {
                    String upperCase = str2.toUpperCase();
                    pq7.b(upperCase, "(this as java.lang.String).toUpperCase()");
                    if (pq7.a(str, upperCase)) {
                        return true;
                    }
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
            }
            return false;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final boolean l() {
        boolean z;
        String str;
        String str2;
        String name;
        String name2;
        String str3 = null;
        AddressWrapper addressWrapper = this.b;
        if (TextUtils.isEmpty(addressWrapper != null ? addressWrapper.getAddress() : null)) {
            return false;
        }
        AddressWrapper addressWrapper2 = this.b;
        if (TextUtils.isEmpty(addressWrapper2 != null ? addressWrapper2.getName() : null)) {
            return false;
        }
        AddressWrapper addressWrapper3 = this.b;
        Boolean valueOf = addressWrapper3 != null ? Boolean.valueOf(addressWrapper3.getAvoidTolls()) : null;
        AddressWrapper addressWrapper4 = this.c;
        if (!(!pq7.a(valueOf, addressWrapper4 != null ? Boolean.valueOf(addressWrapper4.getAvoidTolls()) : null))) {
            AddressWrapper addressWrapper5 = this.b;
            AddressWrapper.AddressType type = addressWrapper5 != null ? addressWrapper5.getType() : null;
            AddressWrapper addressWrapper6 = this.c;
            if (type == (addressWrapper6 != null ? addressWrapper6.getType() : null)) {
                AddressWrapper addressWrapper7 = this.b;
                if (addressWrapper7 == null || (name2 = addressWrapper7.getName()) == null) {
                    str = null;
                } else if (name2 != null) {
                    str = name2.toUpperCase();
                    pq7.b(str, "(this as java.lang.String).toUpperCase()");
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
                AddressWrapper addressWrapper8 = this.c;
                if (addressWrapper8 == null || (name = addressWrapper8.getName()) == null) {
                    str2 = null;
                } else if (name != null) {
                    str2 = name.toUpperCase();
                    pq7.b(str2, "(this as java.lang.String).toUpperCase()");
                } else {
                    throw new il7("null cannot be cast to non-null type java.lang.String");
                }
                if (!(!pq7.a(str, str2))) {
                    AddressWrapper addressWrapper9 = this.b;
                    String address = addressWrapper9 != null ? addressWrapper9.getAddress() : null;
                    AddressWrapper addressWrapper10 = this.c;
                    if (addressWrapper10 != null) {
                        str3 = addressWrapper10.getAddress();
                    }
                    if (!(!pq7.a(address, str3))) {
                        z = false;
                        return z;
                    }
                }
            }
        }
        z = true;
        return z;
    }

    @DexIgnore
    public final void m(String str, String str2, AutocompleteSessionToken autocompleteSessionToken) {
        pq7.c(str, "address");
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2) && autocompleteSessionToken != null) {
            if (str2 != null) {
                h(str2, autocompleteSessionToken, str);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void o(String str, Location location) {
        pq7.c(str, "address");
        pq7.c(location, PlaceFields.LOCATION);
        AddressWrapper addressWrapper = this.b;
        if (addressWrapper != null) {
            addressWrapper.setAddress(str);
        }
        AddressWrapper addressWrapper2 = this.b;
        if (addressWrapper2 != null) {
            addressWrapper2.setLat(location.getLatitude());
        }
        AddressWrapper addressWrapper3 = this.b;
        if (addressWrapper3 != null) {
            addressWrapper3.setLng(location.getLongitude());
        }
        f(this, null, null, null, null, null, Boolean.valueOf(l()), null, null, null, 479, null);
    }

    @DexIgnore
    public final void p(boolean z) {
        AddressWrapper addressWrapper = this.b;
        if (addressWrapper != null) {
            addressWrapper.setAvoidTolls(z);
        }
        f(this, null, null, null, null, null, Boolean.valueOf(l()), null, null, null, 479, null);
    }

    @DexIgnore
    public final void q(String str) {
        pq7.c(str, "name");
        AddressWrapper addressWrapper = this.b;
        if (addressWrapper != null) {
            addressWrapper.setName(str);
        }
        f(this, null, null, null, null, null, Boolean.valueOf(l()), null, null, null, 479, null);
    }

    @DexIgnore
    public final void r() {
        xw7 unused = gu7.d(us0.a(this), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    public final void s() {
        this.f1888a = null;
    }
}
