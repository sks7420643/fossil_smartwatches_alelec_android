package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bv3 implements Parcelable.Creator<Av3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Av3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        ArrayList arrayList = null;
        String str = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 2) {
                str = Ad2.f(parcel, t);
            } else if (l != 3) {
                Ad2.B(parcel, t);
            } else {
                arrayList = Ad2.j(parcel, t, Pv3.CREATOR);
            }
        }
        Ad2.k(parcel, C);
        return new Av3(str, arrayList);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Av3[] newArray(int i) {
        return new Av3[i];
    }
}
