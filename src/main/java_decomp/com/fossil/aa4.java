package com.fossil;

import com.fossil.Ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Aa4 extends Ta4.Bi {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4.Bi.Aii {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;

        @DexIgnore
        @Override // com.fossil.Ta4.Bi.Aii
        public Ta4.Bi a() {
            String str = "";
            if (this.a == null) {
                str = " key";
            }
            if (this.b == null) {
                str = str + " value";
            }
            if (str.isEmpty()) {
                return new Aa4(this.a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Bi.Aii
        public Ta4.Bi.Aii b(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null key");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Bi.Aii
        public Ta4.Bi.Aii c(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null value");
        }
    }

    @DexIgnore
    public Aa4(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Bi
    public String b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Bi
    public String c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4.Bi)) {
            return false;
        }
        Ta4.Bi bi = (Ta4.Bi) obj;
        return this.a.equals(bi.b()) && this.b.equals(bi.c());
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "CustomAttribute{key=" + this.a + ", value=" + this.b + "}";
    }
}
