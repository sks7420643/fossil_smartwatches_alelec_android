package com.fossil;

import com.fossil.V34.Ii;
import com.fossil.V34.Ni;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.j2objc.annotations.Weak;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class V34<K, V, E extends Ii<K, V, E>, S extends Ni<K, V, E, S>> extends AbstractMap<K, V> implements ConcurrentMap<K, V>, Serializable {
    @DexIgnore
    public static /* final */ long CLEANUP_EXECUTOR_DELAY_SECS; // = 60;
    @DexIgnore
    public static /* final */ int CONTAINS_VALUE_RETRIES; // = 3;
    @DexIgnore
    public static /* final */ int DRAIN_MAX; // = 16;
    @DexIgnore
    public static /* final */ int DRAIN_THRESHOLD; // = 63;
    @DexIgnore
    public static /* final */ int MAXIMUM_CAPACITY; // = 1073741824;
    @DexIgnore
    public static /* final */ int MAX_SEGMENTS; // = 65536;
    @DexIgnore
    public static /* final */ b0<Object, Object, Ei> UNSET_WEAK_VALUE_REFERENCE; // = new Ai();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 5;
    @DexIgnore
    public /* final */ int concurrencyLevel;
    @DexIgnore
    public /* final */ transient Ji<K, V, E, S> entryHelper;
    @DexIgnore
    public transient Set<Map.Entry<K, V>> entrySet;
    @DexIgnore
    public /* final */ Z04<Object> keyEquivalence;
    @DexIgnore
    public transient Set<K> keySet;
    @DexIgnore
    public /* final */ transient int segmentMask;
    @DexIgnore
    public /* final */ transient int segmentShift;
    @DexIgnore
    public /* final */ transient Ni<K, V, E, S>[] segments;
    @DexIgnore
    public transient Collection<V> values;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements b0<Object, Object, Ei> {
        @DexIgnore
        /* Return type fixed from 'com.fossil.V34$Ii' to match base method */
        @Override // com.fossil.V34.b0
        public /* bridge */ /* synthetic */ Ei a() {
            return d();
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.V34$b0' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.ref.ReferenceQueue, com.fossil.V34$Ii] */
        @Override // com.fossil.V34.b0
        public /* bridge */ /* synthetic */ b0<Object, Object, Ei> b(ReferenceQueue<Object> referenceQueue, Ei ei) {
            c(referenceQueue, ei);
            return this;
        }

        @DexIgnore
        public b0<Object, Object, Ei> c(ReferenceQueue<Object> referenceQueue, Ei ei) {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.V34.b0
        public void clear() {
        }

        @DexIgnore
        public Ei d() {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.V34.b0
        public Object get() {
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Bi<K, V> extends M24<K, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 3;
        @DexIgnore
        public /* final */ int concurrencyLevel;
        @DexIgnore
        public transient ConcurrentMap<K, V> delegate;
        @DexIgnore
        public /* final */ Z04<Object> keyEquivalence;
        @DexIgnore
        public /* final */ Pi keyStrength;
        @DexIgnore
        public /* final */ Z04<Object> valueEquivalence;
        @DexIgnore
        public /* final */ Pi valueStrength;

        @DexIgnore
        public Bi(Pi pi, Pi pi2, Z04<Object> z04, Z04<Object> z042, int i, ConcurrentMap<K, V> concurrentMap) {
            this.keyStrength = pi;
            this.valueStrength = pi2;
            this.keyEquivalence = z04;
            this.valueEquivalence = z042;
            this.concurrencyLevel = i;
            this.delegate = concurrentMap;
        }

        @DexIgnore
        @Override // com.fossil.M24, com.fossil.M24, com.fossil.M24, com.fossil.N24, com.fossil.N24, com.fossil.O24
        public ConcurrentMap<K, V> delegate() {
            return this.delegate;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.concurrent.ConcurrentMap<K, V> */
        /* JADX WARN: Multi-variable type inference failed */
        public void readEntries(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            while (true) {
                Object readObject = objectInputStream.readObject();
                if (readObject != null) {
                    this.delegate.put(readObject, objectInputStream.readObject());
                } else {
                    return;
                }
            }
        }

        @DexIgnore
        public U34 readMapMaker(ObjectInputStream objectInputStream) throws IOException {
            int readInt = objectInputStream.readInt();
            U34 u34 = new U34();
            u34.g(readInt);
            u34.j(this.keyStrength);
            u34.k(this.valueStrength);
            u34.h(this.keyEquivalence);
            u34.a(this.concurrencyLevel);
            return u34;
        }

        @DexIgnore
        public void writeMapTo(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.writeInt(this.delegate.size());
            for (Map.Entry<K, V> entry : this.delegate.entrySet()) {
                objectOutputStream.writeObject(entry.getKey());
                objectOutputStream.writeObject(entry.getValue());
            }
            objectOutputStream.writeObject(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ci<K, V, E extends Ii<K, V, E>> implements Ii<K, V, E> {
        @DexIgnore
        public /* final */ K a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ E c;

        @DexIgnore
        public Ci(K k, int i, E e) {
            this.a = k;
            this.b = i;
            this.c = e;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ii
        public E a() {
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ii
        public int c() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ii
        public K getKey() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Di<K, V, E extends Ii<K, V, E>> extends WeakReference<K> implements Ii<K, V, E> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ E b;

        @DexIgnore
        public Di(ReferenceQueue<K> referenceQueue, K k, int i, E e) {
            super(k, referenceQueue);
            this.a = i;
            this.b = e;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ii
        public E a() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ii
        public int c() {
            return this.a;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ii
        public K getKey() {
            return (K) get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements Ii<Object, Object, Ei> {
        @DexIgnore
        public Ei() {
            throw new AssertionError();
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.V34$Ii' to match base method */
        @Override // com.fossil.V34.Ii
        public /* bridge */ /* synthetic */ Ei a() {
            d();
            throw null;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ii
        public int c() {
            throw new AssertionError();
        }

        @DexIgnore
        public Ei d() {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // com.fossil.V34.Ii
        public Object getKey() {
            throw new AssertionError();
        }

        @DexIgnore
        @Override // com.fossil.V34.Ii
        public Object getValue() {
            throw new AssertionError();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Fi extends V34<K, V, E, S>.h {
        @DexIgnore
        public Fi(V34 v34) {
            super();
        }

        @DexIgnore
        public Map.Entry<K, V> f() {
            return c();
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object next() {
            return f();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Gi extends Mi<Map.Entry<K, V>> {
        @DexIgnore
        public Gi() {
            super(null);
        }

        @DexIgnore
        public void clear() {
            V34.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            Map.Entry entry;
            Object key;
            Object obj2;
            return (obj instanceof Map.Entry) && (key = (entry = (Map.Entry) obj).getKey()) != null && (obj2 = V34.this.get(key)) != null && V34.this.valueEquivalence().equivalent(entry.getValue(), obj2);
        }

        @DexIgnore
        public boolean isEmpty() {
            return V34.this.isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<Map.Entry<K, V>> iterator() {
            return new Fi(V34.this);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            Map.Entry entry;
            Object key;
            return (obj instanceof Map.Entry) && (key = (entry = (Map.Entry) obj).getKey()) != null && V34.this.remove(key, entry.getValue());
        }

        @DexIgnore
        public int size() {
            return V34.this.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class Hi<T> implements Iterator<T> {
        @DexIgnore
        public int b;
        @DexIgnore
        public int c; // = -1;
        @DexIgnore
        public Ni<K, V, E, S> d;
        @DexIgnore
        public AtomicReferenceArray<E> e;
        @DexIgnore
        public E f;
        @DexIgnore
        public V34<K, V, E, S>.d0 g;
        @DexIgnore
        public V34<K, V, E, S>.d0 h;

        @DexIgnore
        public Hi() {
            this.b = V34.this.segments.length - 1;
            a();
        }

        @DexIgnore
        public final void a() {
            this.g = null;
            if (!d() && !e()) {
                while (true) {
                    int i2 = this.b;
                    if (i2 >= 0) {
                        Ni<K, V, E, S>[] niArr = V34.this.segments;
                        this.b = i2 - 1;
                        Ni<K, V, E, S> ni = niArr[i2];
                        this.d = ni;
                        if (ni.count != 0) {
                            AtomicReferenceArray<E> atomicReferenceArray = this.d.table;
                            this.e = atomicReferenceArray;
                            this.c = atomicReferenceArray.length() - 1;
                            if (e()) {
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }

        @DexIgnore
        public boolean b(E e2) {
            boolean z;
            try {
                Object key = e2.getKey();
                Object liveValue = V34.this.getLiveValue(e2);
                if (liveValue != null) {
                    this.g = new d0(key, liveValue);
                    z = true;
                } else {
                    z = false;
                }
                return z;
            } finally {
                this.d.postReadCleanup();
            }
        }

        @DexIgnore
        public V34<K, V, E, S>.d0 c() {
            V34<K, V, E, S>.d0 d0Var = this.g;
            if (d0Var != null) {
                this.h = d0Var;
                a();
                return this.h;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public boolean d() {
            E e2 = this.f;
            if (e2 != null) {
                while (true) {
                    this.f = (E) e2.a();
                    E e3 = this.f;
                    if (e3 == null) {
                        break;
                    } else if (b(e3)) {
                        return true;
                    } else {
                        e2 = this.f;
                    }
                }
            }
            return false;
        }

        @DexIgnore
        public boolean e() {
            while (true) {
                int i2 = this.c;
                if (i2 < 0) {
                    return false;
                }
                AtomicReferenceArray<E> atomicReferenceArray = this.e;
                this.c = i2 - 1;
                E e2 = atomicReferenceArray.get(i2);
                this.f = e2;
                if (e2 == null || (!b(e2) && !d())) {
                }
            }
            return true;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.g != null;
        }

        @DexIgnore
        public void remove() {
            A24.c(this.h != null);
            V34.this.remove(this.h.getKey());
            this.h = null;
        }
    }

    @DexIgnore
    public interface Ii<K, V, E extends Ii<K, V, E>> {
        @DexIgnore
        E a();

        @DexIgnore
        int c();

        @DexIgnore
        K getKey();

        @DexIgnore
        V getValue();
    }

    @DexIgnore
    public interface Ji<K, V, E extends Ii<K, V, E>, S extends Ni<K, V, E, S>> {
        @DexIgnore
        E a(S s, E e, E e2);

        @DexIgnore
        Pi b();

        @DexIgnore
        Pi c();

        @DexIgnore
        void d(S s, E e, V v);

        @DexIgnore
        S e(V34<K, V, E, S> v34, int i, int i2);

        @DexIgnore
        E f(S s, K k, int i, E e);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ki extends V34<K, V, E, S>.h {
        @DexIgnore
        public Ki(V34 v34) {
            super();
        }

        @DexIgnore
        public K next() {
            return (K) c().getKey();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Li extends Mi<K> {
        @DexIgnore
        public Li() {
            super(null);
        }

        @DexIgnore
        public void clear() {
            V34.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return V34.this.containsKey(obj);
        }

        @DexIgnore
        public boolean isEmpty() {
            return V34.this.isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<K> iterator() {
            return new Ki(V34.this);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            return V34.this.remove(obj) != null;
        }

        @DexIgnore
        public int size() {
            return V34.this.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Mi<E> extends AbstractSet<E> {
        @DexIgnore
        public Mi() {
        }

        @DexIgnore
        public /* synthetic */ Mi(Ai ai) {
            this();
        }

        @DexIgnore
        public Object[] toArray() {
            return V34.a(this).toArray();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public <E> E[] toArray(E[] eArr) {
            return (E[]) V34.a(this).toArray(eArr);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ni<K, V, E extends Ii<K, V, E>, S extends Ni<K, V, E, S>> extends ReentrantLock {
        @DexIgnore
        public volatile int count;
        @DexIgnore
        @Weak
        public /* final */ V34<K, V, E, S> map;
        @DexIgnore
        public /* final */ int maxSegmentSize;
        @DexIgnore
        public int modCount;
        @DexIgnore
        public /* final */ AtomicInteger readCount; // = new AtomicInteger();
        @DexIgnore
        public volatile AtomicReferenceArray<E> table;
        @DexIgnore
        public int threshold;

        @DexIgnore
        public Ni(V34<K, V, E, S> v34, int i, int i2) {
            this.map = v34;
            this.maxSegmentSize = i2;
            initTable(newEntryArray(i));
        }

        @DexIgnore
        public static <K, V, E extends Ii<K, V, E>> boolean isCollected(E e) {
            return e.getValue() == null;
        }

        @DexIgnore
        public abstract E castForTesting(Ii<K, V, ?> ii);

        @DexIgnore
        public void clear() {
            if (this.count != 0) {
                lock();
                try {
                    AtomicReferenceArray<E> atomicReferenceArray = this.table;
                    for (int i = 0; i < atomicReferenceArray.length(); i++) {
                        atomicReferenceArray.set(i, null);
                    }
                    maybeClearReferenceQueues();
                    this.readCount.set(0);
                    this.modCount++;
                    this.count = 0;
                } finally {
                    unlock();
                }
            }
        }

        @DexIgnore
        public <T> void clearReferenceQueue(ReferenceQueue<T> referenceQueue) {
            do {
            } while (referenceQueue.poll() != null);
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: com.fossil.V34$Ni<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public boolean clearValueForTesting(K k, int i, b0<K, V, ? extends Ii<K, V, ?>> b0Var) {
            lock();
            try {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (Ii ii = e; ii != null; ii = ii.a()) {
                    Object key = ii.getKey();
                    if (ii.c() == i && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        if (((a0) ii).b() == b0Var) {
                            atomicReferenceArray.set(length, removeFromChain(e, ii));
                            unlock();
                            return true;
                        } else {
                            unlock();
                            return false;
                        }
                    }
                }
                unlock();
                return false;
            } catch (Throwable th) {
                unlock();
                throw th;
            }
        }

        @DexIgnore
        public boolean containsKey(Object obj, int i) {
            boolean z = false;
            try {
                if (this.count != 0) {
                    E liveEntry = getLiveEntry(obj, i);
                    if (!(liveEntry == null || liveEntry.getValue() == null)) {
                        z = true;
                    }
                } else {
                    postReadCleanup();
                }
                return z;
            } finally {
                postReadCleanup();
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.fossil.V34$Ni<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public boolean containsValue(Object obj) {
            try {
                if (this.count != 0) {
                    AtomicReferenceArray<E> atomicReferenceArray = this.table;
                    int length = atomicReferenceArray.length();
                    for (int i = 0; i < length; i++) {
                        for (E e = atomicReferenceArray.get(i); e != null; e = e.a()) {
                            Object liveValue = getLiveValue(e);
                            if (liveValue != null && this.map.valueEquivalence().equivalent(obj, liveValue)) {
                                postReadCleanup();
                                return true;
                            }
                        }
                    }
                }
                postReadCleanup();
                return false;
            } catch (Throwable th) {
                postReadCleanup();
                throw th;
            }
        }

        @DexIgnore
        public E copyEntry(E e, E e2) {
            return this.map.entryHelper.a(self(), e, e2);
        }

        @DexIgnore
        public E copyForTesting(Ii<K, V, ?> ii, Ii<K, V, ?> ii2) {
            return this.map.entryHelper.a(self(), castForTesting(ii), castForTesting(ii2));
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.V34<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public void drainKeyReferenceQueue(ReferenceQueue<K> referenceQueue) {
            int i = 0;
            while (true) {
                Reference<? extends K> poll = referenceQueue.poll();
                if (poll != null) {
                    this.map.reclaimKey((Ii) poll);
                    i++;
                    if (i == 16) {
                        return;
                    }
                } else {
                    return;
                }
            }
        }

        @DexIgnore
        public void drainValueReferenceQueue(ReferenceQueue<V> referenceQueue) {
            int i = 0;
            while (true) {
                Reference<? extends V> poll = referenceQueue.poll();
                if (poll != null) {
                    this.map.reclaimValue((b0) poll);
                    i++;
                    if (i == 16) {
                        return;
                    }
                } else {
                    return;
                }
            }
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX DEBUG: Type inference failed for r9v0. Raw type applied. Possible types: java.util.concurrent.atomic.AtomicReferenceArray<E extends com.fossil.V34$Ii<K, V, E>>, java.util.concurrent.atomic.AtomicReferenceArray */
        /* JADX WARN: Type inference failed for: r11v0, types: [com.fossil.V34$Ni<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>>, com.fossil.V34$Ni] */
        /* JADX WARN: Type inference failed for: r4v0, types: [com.fossil.V34$Ii] */
        /* JADX WARN: Type inference failed for: r4v3, types: [com.fossil.V34$Ii] */
        /* JADX WARNING: Unknown variable types count: 2 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void expand() {
            /*
                r11 = this;
                java.util.concurrent.atomic.AtomicReferenceArray<E extends com.fossil.V34$Ii<K, V, E>> r7 = r11.table
                int r8 = r7.length()
                r0 = 1073741824(0x40000000, float:2.0)
                if (r8 < r0) goto L_0x000b
            L_0x000a:
                return
            L_0x000b:
                int r5 = r11.count
                int r0 = r8 << 1
                java.util.concurrent.atomic.AtomicReferenceArray r9 = r11.newEntryArray(r0)
                int r0 = r9.length()
                int r0 = r0 * 3
                int r0 = r0 / 4
                r11.threshold = r0
                int r0 = r9.length()
                int r10 = r0 + -1
                r0 = 0
                r6 = r0
            L_0x0025:
                if (r6 >= r8) goto L_0x007b
                java.lang.Object r0 = r7.get(r6)
                com.fossil.V34$Ii r0 = (com.fossil.V34.Ii) r0
                if (r0 == 0) goto L_0x0082
                com.fossil.V34$Ii r4 = r0.a()
                int r1 = r0.c()
                r2 = r1 & r10
                if (r4 != 0) goto L_0x0044
                r9.set(r2, r0)
                r1 = r5
            L_0x003f:
                int r0 = r6 + 1
                r6 = r0
                r5 = r1
                goto L_0x0025
            L_0x0044:
                r3 = r0
            L_0x0045:
                if (r4 == 0) goto L_0x0055
                int r1 = r4.c()
                r1 = r1 & r10
                if (r1 == r2) goto L_0x0080
                r3 = r4
            L_0x004f:
                com.fossil.V34$Ii r4 = r4.a()
                r2 = r1
                goto L_0x0045
            L_0x0055:
                r9.set(r2, r3)
                r2 = r0
                r1 = r5
            L_0x005a:
                if (r2 == r3) goto L_0x003f
                int r0 = r2.c()
                r4 = r0 & r10
                java.lang.Object r0 = r9.get(r4)
                com.fossil.V34$Ii r0 = (com.fossil.V34.Ii) r0
                com.fossil.V34$Ii r0 = r11.copyEntry(r2, r0)
                if (r0 == 0) goto L_0x0078
                r9.set(r4, r0)
                r0 = r1
            L_0x0072:
                com.fossil.V34$Ii r2 = r2.a()
                r1 = r0
                goto L_0x005a
            L_0x0078:
                int r0 = r1 + -1
                goto L_0x0072
            L_0x007b:
                r11.table = r9
                r11.count = r5
                goto L_0x000a
            L_0x0080:
                r1 = r2
                goto L_0x004f
            L_0x0082:
                r1 = r5
                goto L_0x003f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.V34.Ni.expand():void");
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public V get(Object obj, int i) {
            try {
                E liveEntry = getLiveEntry(obj, i);
                if (liveEntry == null) {
                    postReadCleanup();
                    return null;
                }
                V v = (V) liveEntry.getValue();
                if (v == null) {
                    tryDrainReferenceQueues();
                }
                postReadCleanup();
                return v;
            } catch (Throwable th) {
                postReadCleanup();
                throw th;
            }
        }

        @DexIgnore
        public E getEntry(Object obj, int i) {
            if (this.count != 0) {
                for (E first = getFirst(i); first != null; first = (E) first.a()) {
                    if (first.c() == i) {
                        Object key = first.getKey();
                        if (key == null) {
                            tryDrainReferenceQueues();
                        } else if (this.map.keyEquivalence.equivalent(obj, key)) {
                            return first;
                        }
                    }
                }
            }
            return null;
        }

        @DexIgnore
        public E getFirst(int i) {
            AtomicReferenceArray<E> atomicReferenceArray = this.table;
            return atomicReferenceArray.get((atomicReferenceArray.length() - 1) & i);
        }

        @DexIgnore
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            throw new AssertionError();
        }

        @DexIgnore
        public E getLiveEntry(Object obj, int i) {
            return getEntry(obj, i);
        }

        @DexIgnore
        public V getLiveValue(E e) {
            if (e.getKey() == null) {
                tryDrainReferenceQueues();
                return null;
            }
            V v = (V) e.getValue();
            if (v != null) {
                return v;
            }
            tryDrainReferenceQueues();
            return null;
        }

        @DexIgnore
        public V getLiveValueForTesting(Ii<K, V, ?> ii) {
            return getLiveValue(castForTesting(ii));
        }

        @DexIgnore
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            throw new AssertionError();
        }

        @DexIgnore
        public b0<K, V, E> getWeakValueReferenceForTesting(Ii<K, V, ?> ii) {
            throw new AssertionError();
        }

        @DexIgnore
        public void initTable(AtomicReferenceArray<E> atomicReferenceArray) {
            int length = (atomicReferenceArray.length() * 3) / 4;
            this.threshold = length;
            if (length == this.maxSegmentSize) {
                this.threshold = length + 1;
            }
            this.table = atomicReferenceArray;
        }

        @DexIgnore
        public void maybeClearReferenceQueues() {
        }

        @DexIgnore
        public void maybeDrainReferenceQueues() {
        }

        @DexIgnore
        public AtomicReferenceArray<E> newEntryArray(int i) {
            return new AtomicReferenceArray<>(i);
        }

        @DexIgnore
        public E newEntryForTesting(K k, int i, Ii<K, V, ?> ii) {
            return this.map.entryHelper.f(self(), k, i, castForTesting(ii));
        }

        @DexIgnore
        public b0<K, V, E> newWeakValueReferenceForTesting(Ii<K, V, ?> ii, V v) {
            throw new AssertionError();
        }

        @DexIgnore
        public void postReadCleanup() {
            if ((this.readCount.incrementAndGet() & 63) == 0) {
                runCleanup();
            }
        }

        @DexIgnore
        public void preWriteCleanup() {
            runLockedCleanup();
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: com.fossil.V34$Ni<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public V put(K k, int i, V v, boolean z) {
            int i2;
            lock();
            try {
                preWriteCleanup();
                int i3 = this.count + 1;
                if (i3 > this.threshold) {
                    expand();
                    i2 = this.count + 1;
                } else {
                    i2 = i3;
                }
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (Ii ii = e; ii != null; ii = ii.a()) {
                    Object key = ii.getKey();
                    if (ii.c() == i && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        V v2 = (V) ii.getValue();
                        if (v2 == null) {
                            this.modCount++;
                            setValue(ii, v);
                            this.count = this.count;
                            return null;
                        } else if (z) {
                            unlock();
                            return v2;
                        } else {
                            this.modCount++;
                            setValue(ii, v);
                            unlock();
                            return v2;
                        }
                    }
                }
                this.modCount++;
                E f = this.map.entryHelper.f(self(), k, i, e);
                setValue(f, v);
                atomicReferenceArray.set(length, f);
                this.count = i2;
                unlock();
                return null;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.V34$Ni<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public boolean reclaimKey(E e, int i) {
            lock();
            try {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = i & (atomicReferenceArray.length() - 1);
                E e2 = atomicReferenceArray.get(length);
                for (Ii ii = e2; ii != null; ii = ii.a()) {
                    if (ii == e) {
                        this.modCount++;
                        E removeFromChain = removeFromChain(e2, ii);
                        int i2 = this.count;
                        atomicReferenceArray.set(length, removeFromChain);
                        this.count = i2 - 1;
                        unlock();
                        return true;
                    }
                }
                unlock();
                return false;
            } catch (Throwable th) {
                unlock();
                throw th;
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: com.fossil.V34$Ni<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public boolean reclaimValue(K k, int i, b0<K, V, E> b0Var) {
            lock();
            try {
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (Ii ii = e; ii != null; ii = ii.a()) {
                    Object key = ii.getKey();
                    if (ii.c() == i && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        if (((a0) ii).b() == b0Var) {
                            this.modCount++;
                            E removeFromChain = removeFromChain(e, ii);
                            int i2 = this.count;
                            atomicReferenceArray.set(length, removeFromChain);
                            this.count = i2 - 1;
                            unlock();
                            return true;
                        } else {
                            unlock();
                            return false;
                        }
                    }
                }
                unlock();
                return false;
            } catch (Throwable th) {
                unlock();
                throw th;
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.fossil.V34$Ni<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        @CanIgnoreReturnValue
        public V remove(Object obj, int i) {
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (Ii ii = e; ii != null; ii = ii.a()) {
                    Object key = ii.getKey();
                    if (ii.c() == i && key != null && this.map.keyEquivalence.equivalent(obj, key)) {
                        V v = (V) ii.getValue();
                        if (v == null && !isCollected(ii)) {
                            unlock();
                            return null;
                        }
                        this.modCount++;
                        E removeFromChain = removeFromChain(e, ii);
                        int i2 = this.count;
                        atomicReferenceArray.set(length, removeFromChain);
                        this.count = i2 - 1;
                        return v;
                    }
                }
                unlock();
                return null;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.fossil.V34$Ni<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public boolean remove(Object obj, int i, Object obj2) {
            boolean z = false;
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (Ii ii = e; ii != null; ii = ii.a()) {
                    Object key = ii.getKey();
                    if (ii.c() == i && key != null && this.map.keyEquivalence.equivalent(obj, key)) {
                        if (this.map.valueEquivalence().equivalent(obj2, ii.getValue())) {
                            z = true;
                        } else if (!isCollected(ii)) {
                            unlock();
                            return false;
                        }
                        this.modCount++;
                        E removeFromChain = removeFromChain(e, ii);
                        int i2 = this.count;
                        atomicReferenceArray.set(length, removeFromChain);
                        this.count = i2 - 1;
                        return z;
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.fossil.V34$Ni<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public boolean removeEntryForTesting(E e) {
            int c = e.c();
            AtomicReferenceArray<E> atomicReferenceArray = this.table;
            int length = c & (atomicReferenceArray.length() - 1);
            E e2 = atomicReferenceArray.get(length);
            for (Ii ii = e2; ii != null; ii = ii.a()) {
                if (ii == e) {
                    this.modCount++;
                    E removeFromChain = removeFromChain(e2, ii);
                    int i = this.count;
                    atomicReferenceArray.set(length, removeFromChain);
                    this.count = i - 1;
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public E removeFromChain(E e, E e2) {
            int i = this.count;
            E e3 = (E) e2.a();
            while (e != e2) {
                E copyEntry = copyEntry(e, e3);
                if (copyEntry == null) {
                    i--;
                    copyEntry = e3;
                }
                e = (E) e.a();
                e3 = copyEntry;
            }
            this.count = i;
            return e3;
        }

        @DexIgnore
        public E removeFromChainForTesting(Ii<K, V, ?> ii, Ii<K, V, ?> ii2) {
            return removeFromChain(castForTesting(ii), castForTesting(ii2));
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public boolean removeTableEntryForTesting(Ii<K, V, ?> ii) {
            return removeEntryForTesting(castForTesting(ii));
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.fossil.V34$Ni<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public V replace(K k, int i, V v) {
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (Ii ii = e; ii != null; ii = ii.a()) {
                    Object key = ii.getKey();
                    if (ii.c() == i && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        V v2 = (V) ii.getValue();
                        if (v2 == null) {
                            if (isCollected(ii)) {
                                this.modCount++;
                                E removeFromChain = removeFromChain(e, ii);
                                int i2 = this.count;
                                atomicReferenceArray.set(length, removeFromChain);
                                this.count = i2 - 1;
                            }
                            return null;
                        }
                        this.modCount++;
                        setValue(ii, v);
                        unlock();
                        return v2;
                    }
                }
                unlock();
                return null;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: com.fossil.V34$Ni<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>> */
        /* JADX WARN: Multi-variable type inference failed */
        public boolean replace(K k, int i, V v, V v2) {
            lock();
            try {
                preWriteCleanup();
                AtomicReferenceArray<E> atomicReferenceArray = this.table;
                int length = (atomicReferenceArray.length() - 1) & i;
                E e = atomicReferenceArray.get(length);
                for (Ii ii = e; ii != null; ii = ii.a()) {
                    Object key = ii.getKey();
                    if (ii.c() == i && key != null && this.map.keyEquivalence.equivalent(k, key)) {
                        Object value = ii.getValue();
                        if (value == null) {
                            if (isCollected(ii)) {
                                this.modCount++;
                                E removeFromChain = removeFromChain(e, ii);
                                int i2 = this.count;
                                atomicReferenceArray.set(length, removeFromChain);
                                this.count = i2 - 1;
                            }
                            return false;
                        } else if (this.map.valueEquivalence().equivalent(v, value)) {
                            this.modCount++;
                            setValue(ii, v2);
                            unlock();
                            return true;
                        } else {
                            unlock();
                            return false;
                        }
                    }
                }
                unlock();
                return false;
            } finally {
                unlock();
            }
        }

        @DexIgnore
        public void runCleanup() {
            runLockedCleanup();
        }

        @DexIgnore
        public void runLockedCleanup() {
            if (tryLock()) {
                try {
                    maybeDrainReferenceQueues();
                    this.readCount.set(0);
                } finally {
                    unlock();
                }
            }
        }

        @DexIgnore
        public abstract S self();

        @DexIgnore
        public void setTableEntryForTesting(int i, Ii<K, V, ?> ii) {
            this.table.set(i, castForTesting(ii));
        }

        @DexIgnore
        public void setValue(E e, V v) {
            this.map.entryHelper.d(self(), e, v);
        }

        @DexIgnore
        public void setValueForTesting(Ii<K, V, ?> ii, V v) {
            this.map.entryHelper.d(self(), castForTesting(ii), v);
        }

        @DexIgnore
        public void setWeakValueReferenceForTesting(Ii<K, V, ?> ii, b0<K, V, ? extends Ii<K, V, ?>> b0Var) {
            throw new AssertionError();
        }

        @DexIgnore
        public void tryDrainReferenceQueues() {
            if (tryLock()) {
                try {
                    maybeDrainReferenceQueues();
                } finally {
                    unlock();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi<K, V> extends Bi<K, V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 3;

        @DexIgnore
        public Oi(Pi pi, Pi pi2, Z04<Object> z04, Z04<Object> z042, int i, ConcurrentMap<K, V> concurrentMap) {
            super(pi, pi2, z04, z042, i, concurrentMap);
        }

        @DexIgnore
        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.delegate = readMapMaker(objectInputStream).i();
            readEntries(objectInputStream);
        }

        @DexIgnore
        private Object readResolve() {
            return this.delegate;
        }

        @DexIgnore
        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            writeMapTo(objectOutputStream);
        }
    }

    @DexIgnore
    public enum Pi {
        STRONG {
            @DexIgnore
            @Override // com.fossil.V34.Pi
            public Z04<Object> defaultEquivalence() {
                return Z04.equals();
            }
        },
        WEAK {
            @DexIgnore
            @Override // com.fossil.V34.Pi
            public Z04<Object> defaultEquivalence() {
                return Z04.identity();
            }
        };

        @DexIgnore
        public /* synthetic */ Pi(Ai ai) {
            this();
        }

        @DexIgnore
        public abstract Z04<Object> defaultEquivalence();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Qi<K, V> extends Ci<K, V, Qi<K, V>> implements Object<K, V, Qi<K, V>> {
        @DexIgnore
        public volatile V d; // = null;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii<K, V> implements Ji<K, V, Qi<K, V>, Ri<K, V>> {
            @DexIgnore
            public static /* final */ Aii<?, ?> a; // = new Aii<>();

            @DexIgnore
            public static <K, V> Aii<K, V> h() {
                return (Aii<K, V>) a;
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ Ii a(Ni ni, Ii ii, Ii ii2) {
                return g((Ri) ni, (Qi) ii, (Qi) ii2);
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public Pi b() {
                return Pi.STRONG;
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public Pi c() {
                return Pi.STRONG;
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ void d(Ni ni, Ii ii, Object obj) {
                k((Ri) ni, (Qi) ii, obj);
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ Ni e(V34 v34, int i, int i2) {
                return j(v34, i, i2);
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ Ii f(Ni ni, Object obj, int i, Ii ii) {
                return i((Ri) ni, obj, i, (Qi) ii);
            }

            @DexIgnore
            public Qi<K, V> g(Ri<K, V> ri, Qi<K, V> qi, Qi<K, V> qi2) {
                return qi.d(qi2);
            }

            @DexIgnore
            public Qi<K, V> i(Ri<K, V> ri, K k, int i, Qi<K, V> qi) {
                return new Qi<>(k, i, qi);
            }

            @DexIgnore
            public Ri<K, V> j(V34<K, V, Qi<K, V>, Ri<K, V>> v34, int i, int i2) {
                return new Ri<>(v34, i, i2);
            }

            @DexIgnore
            public void k(Ri<K, V> ri, Qi<K, V> qi, V v) {
                qi.e(v);
            }
        }

        @DexIgnore
        public Qi(K k, int i, Qi<K, V> qi) {
            super(k, i, qi);
        }

        @DexIgnore
        public Qi<K, V> d(Qi<K, V> qi) {
            Qi<K, V> qi2 = new Qi<>(this.a, this.b, qi);
            qi2.d = this.d;
            return qi2;
        }

        @DexIgnore
        public void e(V v) {
            this.d = v;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ii
        public V getValue() {
            return this.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ri<K, V> extends Ni<K, V, Qi<K, V>, Ri<K, V>> {
        @DexIgnore
        public Ri(V34<K, V, Qi<K, V>, Ri<K, V>> v34, int i, int i2) {
            super(v34, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public Qi<K, V> castForTesting(Ii<K, V, ?> ii) {
            return (Qi) ii;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public Ri<K, V> self() {
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Si<K, V> extends Ci<K, V, Si<K, V>> implements a0<K, V, Si<K, V>> {
        @DexIgnore
        public volatile b0<K, V, Si<K, V>> d; // = V34.unsetWeakValueReference();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii<K, V> implements Ji<K, V, Si<K, V>, Ti<K, V>> {
            @DexIgnore
            public static /* final */ Aii<?, ?> a; // = new Aii<>();

            @DexIgnore
            public static <K, V> Aii<K, V> h() {
                return (Aii<K, V>) a;
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ Ii a(Ni ni, Ii ii, Ii ii2) {
                return g((Ti) ni, (Si) ii, (Si) ii2);
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public Pi b() {
                return Pi.STRONG;
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public Pi c() {
                return Pi.WEAK;
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ void d(Ni ni, Ii ii, Object obj) {
                k((Ti) ni, (Si) ii, obj);
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ Ni e(V34 v34, int i, int i2) {
                return j(v34, i, i2);
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ Ii f(Ni ni, Object obj, int i, Ii ii) {
                return i((Ti) ni, obj, i, (Si) ii);
            }

            @DexIgnore
            public Si<K, V> g(Ti<K, V> ti, Si<K, V> si, Si<K, V> si2) {
                if (Ni.isCollected(si)) {
                    return null;
                }
                return si.f(ti.queueForValues, si2);
            }

            @DexIgnore
            public Si<K, V> i(Ti<K, V> ti, K k, int i, Si<K, V> si) {
                return new Si<>(k, i, si);
            }

            @DexIgnore
            public Ti<K, V> j(V34<K, V, Si<K, V>, Ti<K, V>> v34, int i, int i2) {
                return new Ti<>(v34, i, i2);
            }

            @DexIgnore
            public void k(Ti<K, V> ti, Si<K, V> si, V v) {
                si.g(v, ti.queueForValues);
            }
        }

        @DexIgnore
        public Si(K k, int i, Si<K, V> si) {
            super(k, i, si);
        }

        @DexIgnore
        @Override // com.fossil.V34.a0
        public b0<K, V, Si<K, V>> b() {
            return this.d;
        }

        @DexIgnore
        public Si<K, V> f(ReferenceQueue<V> referenceQueue, Si<K, V> si) {
            Si<K, V> si2 = new Si<>(this.a, this.b, si);
            si2.d = this.d.b(referenceQueue, si2);
            return si2;
        }

        @DexIgnore
        public void g(V v, ReferenceQueue<V> referenceQueue) {
            b0<K, V, Si<K, V>> b0Var = this.d;
            this.d = new c0(referenceQueue, v, this);
            b0Var.clear();
        }

        @DexIgnore
        @Override // com.fossil.V34.Ii
        public V getValue() {
            return this.d.get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ti<K, V> extends Ni<K, V, Si<K, V>, Ti<K, V>> {
        @DexIgnore
        public /* final */ ReferenceQueue<V> queueForValues; // = new ReferenceQueue<>();

        @DexIgnore
        public Ti(V34<K, V, Si<K, V>, Ti<K, V>> v34, int i, int i2) {
            super(v34, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public Si<K, V> castForTesting(Ii<K, V, ?> ii) {
            return (Si) ii;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            return this.queueForValues;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public b0<K, V, Si<K, V>> getWeakValueReferenceForTesting(Ii<K, V, ?> ii) {
            return castForTesting((Ii) ii).b();
        }

        @DexIgnore
        /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.lang.ref.ReferenceQueue<V>, java.lang.ref.ReferenceQueue<T> */
        @Override // com.fossil.V34.Ni
        public void maybeClearReferenceQueues() {
            clearReferenceQueue((ReferenceQueue<V>) this.queueForValues);
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public void maybeDrainReferenceQueues() {
            drainValueReferenceQueue(this.queueForValues);
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public b0<K, V, Si<K, V>> newWeakValueReferenceForTesting(Ii<K, V, ?> ii, V v) {
            return new c0(this.queueForValues, v, castForTesting((Ii) ii));
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public Ti<K, V> self() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public void setWeakValueReferenceForTesting(Ii<K, V, ?> ii, b0<K, V, ? extends Ii<K, V, ?>> b0Var) {
            Si<K, V> castForTesting = castForTesting((Ii) ii);
            b0 b0Var2 = castForTesting.d;
            castForTesting.d = b0Var;
            b0Var2.clear();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ui extends V34<K, V, E, S>.h {
        @DexIgnore
        public Ui(V34 v34) {
            super();
        }

        @DexIgnore
        public V next() {
            return (V) c().getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Vi extends AbstractCollection<V> {
        @DexIgnore
        public Vi() {
        }

        @DexIgnore
        public void clear() {
            V34.this.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return V34.this.containsValue(obj);
        }

        @DexIgnore
        public boolean isEmpty() {
            return V34.this.isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            return new Ui(V34.this);
        }

        @DexIgnore
        public int size() {
            return V34.this.size();
        }

        @DexIgnore
        public Object[] toArray() {
            return V34.a(this).toArray();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public <E> E[] toArray(E[] eArr) {
            return (E[]) V34.a(this).toArray(eArr);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Wi<K, V> extends Di<K, V, Wi<K, V>> implements Object<K, V, Wi<K, V>> {
        @DexIgnore
        public volatile V c; // = null;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii<K, V> implements Ji<K, V, Wi<K, V>, Xi<K, V>> {
            @DexIgnore
            public static /* final */ Aii<?, ?> a; // = new Aii<>();

            @DexIgnore
            public static <K, V> Aii<K, V> h() {
                return (Aii<K, V>) a;
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ Ii a(Ni ni, Ii ii, Ii ii2) {
                return g((Xi) ni, (Wi) ii, (Wi) ii2);
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public Pi b() {
                return Pi.WEAK;
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public Pi c() {
                return Pi.STRONG;
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ void d(Ni ni, Ii ii, Object obj) {
                k((Xi) ni, (Wi) ii, obj);
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ Ni e(V34 v34, int i, int i2) {
                return j(v34, i, i2);
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ Ii f(Ni ni, Object obj, int i, Ii ii) {
                return i((Xi) ni, obj, i, (Wi) ii);
            }

            @DexIgnore
            public Wi<K, V> g(Xi<K, V> xi, Wi<K, V> wi, Wi<K, V> wi2) {
                if (wi.getKey() == null) {
                    return null;
                }
                return wi.d(xi.queueForKeys, wi2);
            }

            @DexIgnore
            public Wi<K, V> i(Xi<K, V> xi, K k, int i, Wi<K, V> wi) {
                return new Wi<>(xi.queueForKeys, k, i, wi);
            }

            @DexIgnore
            public Xi<K, V> j(V34<K, V, Wi<K, V>, Xi<K, V>> v34, int i, int i2) {
                return new Xi<>(v34, i, i2);
            }

            @DexIgnore
            public void k(Xi<K, V> xi, Wi<K, V> wi, V v) {
                wi.e(v);
            }
        }

        @DexIgnore
        public Wi(ReferenceQueue<K> referenceQueue, K k, int i, Wi<K, V> wi) {
            super(referenceQueue, k, i, wi);
        }

        @DexIgnore
        public Wi<K, V> d(ReferenceQueue<K> referenceQueue, Wi<K, V> wi) {
            Wi<K, V> wi2 = new Wi<>(referenceQueue, getKey(), this.a, wi);
            wi2.e(this.c);
            return wi2;
        }

        @DexIgnore
        public void e(V v) {
            this.c = v;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ii
        public V getValue() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Xi<K, V> extends Ni<K, V, Wi<K, V>, Xi<K, V>> {
        @DexIgnore
        public /* final */ ReferenceQueue<K> queueForKeys; // = new ReferenceQueue<>();

        @DexIgnore
        public Xi(V34<K, V, Wi<K, V>, Xi<K, V>> v34, int i, int i2) {
            super(v34, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public Wi<K, V> castForTesting(Ii<K, V, ?> ii) {
            return (Wi) ii;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            return this.queueForKeys;
        }

        @DexIgnore
        /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.lang.ref.ReferenceQueue<K>, java.lang.ref.ReferenceQueue<T> */
        @Override // com.fossil.V34.Ni
        public void maybeClearReferenceQueues() {
            clearReferenceQueue((ReferenceQueue<K>) this.queueForKeys);
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public void maybeDrainReferenceQueues() {
            drainKeyReferenceQueue(this.queueForKeys);
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public Xi<K, V> self() {
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Yi<K, V> extends Di<K, V, Yi<K, V>> implements a0<K, V, Yi<K, V>> {
        @DexIgnore
        public volatile b0<K, V, Yi<K, V>> c; // = V34.unsetWeakValueReference();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii<K, V> implements Ji<K, V, Yi<K, V>, Zi<K, V>> {
            @DexIgnore
            public static /* final */ Aii<?, ?> a; // = new Aii<>();

            @DexIgnore
            public static <K, V> Aii<K, V> h() {
                return (Aii<K, V>) a;
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ Ii a(Ni ni, Ii ii, Ii ii2) {
                return g((Zi) ni, (Yi) ii, (Yi) ii2);
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public Pi b() {
                return Pi.WEAK;
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public Pi c() {
                return Pi.WEAK;
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ void d(Ni ni, Ii ii, Object obj) {
                k((Zi) ni, (Yi) ii, obj);
            }

            @DexIgnore
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ Ni e(V34 v34, int i, int i2) {
                return j(v34, i, i2);
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.V34.Ji
            public /* bridge */ /* synthetic */ Ii f(Ni ni, Object obj, int i, Ii ii) {
                return i((Zi) ni, obj, i, (Yi) ii);
            }

            @DexIgnore
            public Yi<K, V> g(Zi<K, V> zi, Yi<K, V> yi, Yi<K, V> yi2) {
                if (yi.getKey() != null && !Ni.isCollected(yi)) {
                    return yi.f(zi.queueForKeys, zi.queueForValues, yi2);
                }
                return null;
            }

            @DexIgnore
            public Yi<K, V> i(Zi<K, V> zi, K k, int i, Yi<K, V> yi) {
                return new Yi<>(zi.queueForKeys, k, i, yi);
            }

            @DexIgnore
            public Zi<K, V> j(V34<K, V, Yi<K, V>, Zi<K, V>> v34, int i, int i2) {
                return new Zi<>(v34, i, i2);
            }

            @DexIgnore
            public void k(Zi<K, V> zi, Yi<K, V> yi, V v) {
                yi.g(v, zi.queueForValues);
            }
        }

        @DexIgnore
        public Yi(ReferenceQueue<K> referenceQueue, K k, int i, Yi<K, V> yi) {
            super(referenceQueue, k, i, yi);
        }

        @DexIgnore
        @Override // com.fossil.V34.a0
        public b0<K, V, Yi<K, V>> b() {
            return this.c;
        }

        @DexIgnore
        public Yi<K, V> f(ReferenceQueue<K> referenceQueue, ReferenceQueue<V> referenceQueue2, Yi<K, V> yi) {
            Yi<K, V> yi2 = new Yi<>(referenceQueue, getKey(), this.a, yi);
            yi2.c = this.c.b(referenceQueue2, yi2);
            return yi2;
        }

        @DexIgnore
        public void g(V v, ReferenceQueue<V> referenceQueue) {
            b0<K, V, Yi<K, V>> b0Var = this.c;
            this.c = new c0(referenceQueue, v, this);
            b0Var.clear();
        }

        @DexIgnore
        @Override // com.fossil.V34.Ii
        public V getValue() {
            return this.c.get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Zi<K, V> extends Ni<K, V, Yi<K, V>, Zi<K, V>> {
        @DexIgnore
        public /* final */ ReferenceQueue<K> queueForKeys; // = new ReferenceQueue<>();
        @DexIgnore
        public /* final */ ReferenceQueue<V> queueForValues; // = new ReferenceQueue<>();

        @DexIgnore
        public Zi(V34<K, V, Yi<K, V>, Zi<K, V>> v34, int i, int i2) {
            super(v34, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public Yi<K, V> castForTesting(Ii<K, V, ?> ii) {
            return (Yi) ii;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public ReferenceQueue<K> getKeyReferenceQueueForTesting() {
            return this.queueForKeys;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public ReferenceQueue<V> getValueReferenceQueueForTesting() {
            return this.queueForValues;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public b0<K, V, Yi<K, V>> getWeakValueReferenceForTesting(Ii<K, V, ?> ii) {
            return castForTesting((Ii) ii).b();
        }

        @DexIgnore
        /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.lang.ref.ReferenceQueue<K>, java.lang.ref.ReferenceQueue<T> */
        @Override // com.fossil.V34.Ni
        public void maybeClearReferenceQueues() {
            clearReferenceQueue((ReferenceQueue<K>) this.queueForKeys);
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public void maybeDrainReferenceQueues() {
            drainKeyReferenceQueue(this.queueForKeys);
            drainValueReferenceQueue(this.queueForValues);
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public b0<K, V, Yi<K, V>> newWeakValueReferenceForTesting(Ii<K, V, ?> ii, V v) {
            return new c0(this.queueForValues, v, castForTesting((Ii) ii));
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public Zi<K, V> self() {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.V34.Ni
        public void setWeakValueReferenceForTesting(Ii<K, V, ?> ii, b0<K, V, ? extends Ii<K, V, ?>> b0Var) {
            Yi<K, V> castForTesting = castForTesting((Ii) ii);
            b0 b0Var2 = castForTesting.c;
            castForTesting.c = b0Var;
            b0Var2.clear();
        }
    }

    @DexIgnore
    public interface a0<K, V, E extends Ii<K, V, E>> extends Ii<K, V, E> {
        @DexIgnore
        b0<K, V, E> b();
    }

    @DexIgnore
    public interface b0<K, V, E extends Ii<K, V, E>> {
        @DexIgnore
        E a();

        @DexIgnore
        b0<K, V, E> b(ReferenceQueue<V> referenceQueue, E e);

        @DexIgnore
        Object clear();  // void declaration

        @DexIgnore
        V get();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c0<K, V, E extends Ii<K, V, E>> extends WeakReference<V> implements b0<K, V, E> {
        @DexIgnore
        public /* final */ E a;

        @DexIgnore
        public c0(ReferenceQueue<V> referenceQueue, V v, E e) {
            super(v, referenceQueue);
            this.a = e;
        }

        @DexIgnore
        @Override // com.fossil.V34.b0
        public E a() {
            return this.a;
        }

        @DexIgnore
        @Override // com.fossil.V34.b0
        public b0<K, V, E> b(ReferenceQueue<V> referenceQueue, E e) {
            return new c0(referenceQueue, get(), e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d0 extends T14<K, V> {
        @DexIgnore
        public /* final */ K b;
        @DexIgnore
        public V c;

        @DexIgnore
        public d0(K k, V v) {
            this.b = k;
            this.c = v;
        }

        @DexIgnore
        @Override // com.fossil.T14
        public boolean equals(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return this.b.equals(entry.getKey()) && this.c.equals(entry.getValue());
        }

        @DexIgnore
        @Override // java.util.Map.Entry, com.fossil.T14
        public K getKey() {
            return this.b;
        }

        @DexIgnore
        @Override // java.util.Map.Entry, com.fossil.T14
        public V getValue() {
            return this.c;
        }

        @DexIgnore
        @Override // com.fossil.T14
        public int hashCode() {
            return this.b.hashCode() ^ this.c.hashCode();
        }

        @DexIgnore
        @Override // java.util.Map.Entry, com.fossil.T14
        public V setValue(V v) {
            V v2 = (V) V34.this.put(this.b, v);
            this.c = v;
            return v2;
        }
    }

    @DexIgnore
    public V34(U34 u34, Ji<K, V, E, S> ji) {
        int i = 1;
        int i2 = 0;
        this.concurrencyLevel = Math.min(u34.b(), 65536);
        this.keyEquivalence = u34.d();
        this.entryHelper = ji;
        int min = Math.min(u34.c(), 1073741824);
        int i3 = 0;
        int i4 = 1;
        while (i4 < this.concurrencyLevel) {
            i3++;
            i4 <<= 1;
        }
        this.segmentShift = 32 - i3;
        this.segmentMask = i4 - 1;
        this.segments = newSegmentArray(i4);
        int i5 = min / i4;
        while (i < (i4 * i5 < min ? i5 + 1 : i5)) {
            i <<= 1;
        }
        while (true) {
            Ni<K, V, E, S>[] niArr = this.segments;
            if (i2 < niArr.length) {
                niArr[i2] = createSegment(i, -1);
                i2++;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public static <E> ArrayList<E> a(Collection<E> collection) {
        ArrayList<E> arrayList = new ArrayList<>(collection.size());
        P34.a(arrayList, collection.iterator());
        return arrayList;
    }

    @DexIgnore
    public static <K, V> V34<K, V, ? extends Ii<K, V, ?>, ?> create(U34 u34) {
        if (u34.e() == Pi.STRONG && u34.f() == Pi.STRONG) {
            return new V34<>(u34, Qi.Aii.h());
        }
        if (u34.e() == Pi.STRONG && u34.f() == Pi.WEAK) {
            return new V34<>(u34, Si.Aii.h());
        }
        if (u34.e() == Pi.WEAK && u34.f() == Pi.STRONG) {
            return new V34<>(u34, Wi.Aii.h());
        }
        if (u34.e() == Pi.WEAK && u34.f() == Pi.WEAK) {
            return new V34<>(u34, Yi.Aii.h());
        }
        throw new AssertionError();
    }

    @DexIgnore
    public static int rehash(int i) {
        int i2 = ((i << 15) ^ -12931) + i;
        int i3 = i2 ^ (i2 >>> 10);
        int i4 = i3 + (i3 << 3);
        int i5 = i4 ^ (i4 >>> 6);
        int i6 = i5 + (i5 << 2) + (i5 << 14);
        return i6 ^ (i6 >>> 16);
    }

    @DexIgnore
    public static <K, V, E extends Ii<K, V, E>> b0<K, V, E> unsetWeakValueReference() {
        return (b0<K, V, E>) UNSET_WEAK_VALUE_REFERENCE;
    }

    @DexIgnore
    public void clear() {
        for (Ni<K, V, E, S> ni : this.segments) {
            ni.clear();
        }
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        if (obj == null) {
            return false;
        }
        int hash = hash(obj);
        return segmentFor(hash).containsKey(obj, hash);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: com.fossil.V34$Ni<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>>[] */
    /* JADX DEBUG: Multi-variable search result rejected for r10v0, resolved type: com.fossil.V34$Zi */
    /* JADX WARN: Multi-variable type inference failed */
    public boolean containsValue(Object obj) {
        if (obj == null) {
            return false;
        }
        Ni<K, V, E, S>[] niArr = this.segments;
        long j = -1;
        for (int i = 0; i < 3; i++) {
            j = 0;
            for (Zi zi : niArr) {
                int i2 = zi.count;
                AtomicReferenceArray<E> atomicReferenceArray = zi.table;
                for (int i3 = 0; i3 < atomicReferenceArray.length(); i3++) {
                    for (E e = atomicReferenceArray.get(i3); e != null; e = e.a()) {
                        Object liveValue = zi.getLiveValue(e);
                        if (liveValue != null && valueEquivalence().equivalent(obj, liveValue)) {
                            return true;
                        }
                    }
                }
                j += (long) zi.modCount;
            }
            if (j == j) {
                break;
            }
        }
        return false;
    }

    @DexIgnore
    public E copyEntry(E e, E e2) {
        return segmentFor(e.c()).copyEntry(e, e2);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v1. Raw type applied. Possible types: S extends com.fossil.V34$Ni<K, V, E, S>, com.fossil.V34$Ni<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>> */
    public Ni<K, V, E, S> createSegment(int i, int i2) {
        return (S) this.entryHelper.e(this, i, i2);
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> set = this.entrySet;
        if (set != null) {
            return set;
        }
        Gi gi = new Gi();
        this.entrySet = gi;
        return gi;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public V get(Object obj) {
        if (obj == null) {
            return null;
        }
        int hash = hash(obj);
        return segmentFor(hash).get(obj, hash);
    }

    @DexIgnore
    public E getEntry(Object obj) {
        if (obj == null) {
            return null;
        }
        int hash = hash(obj);
        return segmentFor(hash).getEntry(obj, hash);
    }

    @DexIgnore
    public V getLiveValue(E e) {
        V v;
        if (e.getKey() == null || (v = (V) e.getValue()) == null) {
            return null;
        }
        return v;
    }

    @DexIgnore
    public int hash(Object obj) {
        return rehash(this.keyEquivalence.hash(obj));
    }

    @DexIgnore
    public boolean isEmpty() {
        Ni<K, V, E, S>[] niArr = this.segments;
        long j = 0;
        for (int i = 0; i < niArr.length; i++) {
            if (niArr[i].count != 0) {
                return false;
            }
            j += (long) niArr[i].modCount;
        }
        if (j != 0) {
            for (int i2 = 0; i2 < niArr.length; i2++) {
                if (niArr[i2].count != 0) {
                    return false;
                }
                j -= (long) niArr[i2].modCount;
            }
            return j == 0;
        }
    }

    @DexIgnore
    public boolean isLiveForTesting(Ii<K, V, ?> ii) {
        return segmentFor(ii.c()).getLiveValueForTesting(ii) != null;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Set<K> keySet() {
        Set<K> set = this.keySet;
        if (set != null) {
            return set;
        }
        Li li = new Li();
        this.keySet = li;
        return li;
    }

    @DexIgnore
    public Pi keyStrength() {
        return this.entryHelper.b();
    }

    @DexIgnore
    public final Ni<K, V, E, S>[] newSegmentArray(int i) {
        return new Ni[i];
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    @CanIgnoreReturnValue
    public V put(K k, V v) {
        I14.l(k);
        I14.l(v);
        int hash = hash(k);
        return segmentFor(hash).put(k, hash, v, false);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.V34<K, V, E extends com.fossil.V34$Ii<K, V, E>, S extends com.fossil.V34$Ni<K, V, E, S>> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map
    public void putAll(Map<? extends K, ? extends V> map) {
        for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @DexIgnore
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public V putIfAbsent(K k, V v) {
        I14.l(k);
        I14.l(v);
        int hash = hash(k);
        return segmentFor(hash).put(k, hash, v, true);
    }

    @DexIgnore
    public void reclaimKey(E e) {
        int c = e.c();
        segmentFor(c).reclaimKey(e, c);
    }

    @DexIgnore
    public void reclaimValue(b0<K, V, E> b0Var) {
        E a2 = b0Var.a();
        int c = a2.c();
        segmentFor(c).reclaimValue((K) a2.getKey(), c, b0Var);
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    @CanIgnoreReturnValue
    public V remove(Object obj) {
        if (obj == null) {
            return null;
        }
        int hash = hash(obj);
        return segmentFor(hash).remove(obj, hash);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public boolean remove(Object obj, Object obj2) {
        if (obj == null || obj2 == null) {
            return false;
        }
        int hash = hash(obj);
        return segmentFor(hash).remove(obj, hash, obj2);
    }

    @DexIgnore
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public V replace(K k, V v) {
        I14.l(k);
        I14.l(v);
        int hash = hash(k);
        return segmentFor(hash).replace(k, hash, v);
    }

    @DexIgnore
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    @CanIgnoreReturnValue
    public boolean replace(K k, V v, V v2) {
        I14.l(k);
        I14.l(v2);
        if (v == null) {
            return false;
        }
        int hash = hash(k);
        return segmentFor(hash).replace(k, hash, v, v2);
    }

    @DexIgnore
    public Ni<K, V, E, S> segmentFor(int i) {
        return this.segments[(i >>> this.segmentShift) & this.segmentMask];
    }

    @DexIgnore
    public int size() {
        Ni<K, V, E, S>[] niArr;
        long j = 0;
        for (Ni<K, V, E, S> ni : this.segments) {
            j += (long) ni.count;
        }
        return V54.b(j);
    }

    @DexIgnore
    public Z04<Object> valueEquivalence() {
        return this.entryHelper.c().defaultEquivalence();
    }

    @DexIgnore
    public Pi valueStrength() {
        return this.entryHelper.c();
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map
    public Collection<V> values() {
        Collection<V> collection = this.values;
        if (collection != null) {
            return collection;
        }
        Vi vi = new Vi();
        this.values = vi;
        return vi;
    }

    @DexIgnore
    public Object writeReplace() {
        return new Oi(this.entryHelper.b(), this.entryHelper.c(), this.keyEquivalence, this.entryHelper.c().defaultEquivalence(), this.concurrencyLevel, this);
    }
}
