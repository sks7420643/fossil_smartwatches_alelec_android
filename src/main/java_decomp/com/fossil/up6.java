package com.fossil;

import com.portfolio.platform.uirenew.home.profile.help.HelpPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Up6 implements MembersInjector<HelpPresenter> {
    @DexIgnore
    public static void a(HelpPresenter helpPresenter) {
        helpPresenter.u();
    }
}
