package com.fossil;

import com.mapped.Af6;
import com.mapped.Il6;
import com.mapped.Wg6;
import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lr0 implements Closeable, Il6 {
    @DexIgnore
    public /* final */ Af6 b;

    @DexIgnore
    public Lr0(Af6 af6) {
        Wg6.c(af6, "context");
        this.b = af6;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        Dx7.d(h(), null, 1, null);
    }

    @DexIgnore
    @Override // com.mapped.Il6
    public Af6 h() {
        return this.b;
    }
}
