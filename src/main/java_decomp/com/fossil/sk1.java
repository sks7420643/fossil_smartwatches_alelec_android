package com.fossil;

import android.os.Build;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sk1 implements FlutterPlugin, MethodChannel.MethodCallHandler {
    @DexIgnore
    @Override // io.flutter.embedding.engine.plugins.FlutterPlugin
    public void onAttachedToEngine(FlutterPlugin.FlutterPluginBinding flutterPluginBinding) {
        Wg6.c(flutterPluginBinding, "flutterPluginBinding");
        new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "fs_log").setMethodCallHandler(new Sk1());
    }

    @DexIgnore
    @Override // io.flutter.embedding.engine.plugins.FlutterPlugin
    public void onDetachedFromEngine(FlutterPlugin.FlutterPluginBinding flutterPluginBinding) {
        Wg6.c(flutterPluginBinding, "binding");
    }

    @DexIgnore
    @Override // io.flutter.plugin.common.MethodChannel.MethodCallHandler
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        Wg6.c(methodCall, "call");
        Wg6.c(result, Constants.RESULT);
        if (Wg6.a(methodCall.method, "getPlatformVersion")) {
            result.success("Android " + Build.VERSION.RELEASE);
        }
    }
}
