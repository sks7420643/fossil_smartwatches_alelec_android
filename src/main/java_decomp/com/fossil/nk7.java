package com.fossil;

import io.flutter.embedding.engine.systemchannels.KeyEventChannel;
import io.flutter.plugin.common.BasicMessageChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Nk7 implements BasicMessageChannel.Reply {
    @DexIgnore
    public /* final */ /* synthetic */ KeyEventChannel a;
    @DexIgnore
    public /* final */ /* synthetic */ long b;

    @DexIgnore
    public /* synthetic */ Nk7(KeyEventChannel keyEventChannel, long j) {
        this.a = keyEventChannel;
        this.b = j;
    }

    @DexIgnore
    @Override // io.flutter.plugin.common.BasicMessageChannel.Reply
    public final void reply(Object obj) {
        this.a.a(this.b, obj);
    }
}
