package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Cw1;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uv1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Cw1 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Uv1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Uv1 a(JSONObject jSONObject) throws IllegalArgumentException {
            try {
                Cw1.Ai ai = Cw1.d;
                String string = jSONObject.getString(BaseFeatureModel.COLUMN_COLOR);
                Wg6.b(string, "themeConfigObject.getStr\u2026g(UIScriptConstant.COLOR)");
                Cw1 a2 = ai.a(string);
                if (a2 != null) {
                    return new Uv1(a2);
                }
                throw new IllegalArgumentException("Required value was null.".toString());
            } catch (JSONException e) {
                return null;
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Uv1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                return new Uv1(Cw1.valueOf(readString));
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Uv1[] newArray(int i) {
            return new Uv1[i];
        }
    }

    @DexIgnore
    public Uv1(Cw1 cw1) {
        this.b = cw1;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = new JSONObject().put(BaseFeatureModel.COLUMN_COLOR, Ey1.a(this.b));
        Wg6.b(put, "JSONObject()\n           \u2026 fontColor.lowerCaseName)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Uv1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((Uv1) obj).b;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.ComplicationTheme");
    }

    @DexIgnore
    public final Cw1 getFontColor() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return a();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
    }
}
