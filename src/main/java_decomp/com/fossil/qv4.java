package com.fossil;

import com.portfolio.platform.buddy_challenge.domain.ProfileRepository;
import com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qv4 implements Factory<BCMainViewModel> {
    @DexIgnore
    public /* final */ Provider<ProfileRepository> a;

    @DexIgnore
    public Qv4(Provider<ProfileRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Qv4 a(Provider<ProfileRepository> provider) {
        return new Qv4(provider);
    }

    @DexIgnore
    public static BCMainViewModel c(ProfileRepository profileRepository) {
        return new BCMainViewModel(profileRepository);
    }

    @DexIgnore
    public BCMainViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
