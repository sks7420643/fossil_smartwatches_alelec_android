package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y4 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ K5 b;
    @DexIgnore
    public /* final */ /* synthetic */ G7 c;
    @DexIgnore
    public /* final */ /* synthetic */ N6 d;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] e;

    @DexIgnore
    public Y4(K5 k5, G7 g7, N6 n6, byte[] bArr) {
        this.b = k5;
        this.c = g7;
        this.d = n6;
        this.e = bArr;
    }

    @DexIgnore
    public final void run() {
        this.b.z.d.f();
        this.b.z.d.c(new I7(this.c, this.d, this.e));
    }
}
