package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.In6;
import com.fossil.Jr4;
import com.mapped.AlertDialogFragment;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutEditViewModel;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fn6 extends BaseFragment implements AlertDialogFragment.Gi, In6.Bi {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public WorkoutEditViewModel g;
    @DexIgnore
    public G37<Lc5> h;
    @DexIgnore
    public Jr4 i;
    @DexIgnore
    public In6 j;
    @DexIgnore
    public Po4 k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Fn6 a(String str) {
            Wg6.c(str, "workoutSessionId");
            Fn6 fn6 = new Fn6();
            Bundle bundle = new Bundle();
            bundle.putString("EXTRA_WORKOUT_SESSION_ID", str);
            fn6.setArguments(bundle);
            return fn6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Jr4.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ Fn6 a;

        @DexIgnore
        public Bi(Fn6 fn6) {
            this.a = fn6;
        }

        @DexIgnore
        @Override // com.fossil.Jr4.Ai
        public void a(Oi5 oi5) {
            Wg6.c(oi5, "workoutWrapperType");
            Fn6.L6(this.a).s(oi5);
            Fn6.N6(this.a).o(oi5);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Fn6 b;

        @DexIgnore
        public Ci(Fn6 fn6) {
            this.b = fn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            In6 in6 = this.b.j;
            if (in6 != null) {
                String c = Um5.c(PortfolioApp.get.instance(), 2131886729);
                Wg6.b(c, "LanguageHelper.getString\u2026ctivity_Title__StartTime)");
                in6.setTitle(c);
            }
            In6 in62 = this.b.j;
            if (in62 != null) {
                in62.M6(Hi5.START_TIME);
            }
            In6 in63 = this.b.j;
            if (in63 != null) {
                Lc6<Ii5, Ii5> i = Fn6.L6(this.b).i();
                in63.N6(i != null ? i.getFirst() : null);
            }
            In6 in64 = this.b.j;
            if (in64 != null) {
                Lc6<Ii5, Ii5> i2 = Fn6.L6(this.b).i();
                in64.P6(i2 != null ? i2.getSecond() : null);
            }
            In6 in65 = this.b.j;
            if (in65 != null) {
                in65.Q6(Fn6.L6(this.b).k());
            }
            In6 in66 = this.b.j;
            if (in66 != null) {
                in66.R6(Fn6.L6(this.b).l());
            }
            In6 in67 = this.b.j;
            if (in67 != null) {
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                in67.show(childFragmentManager, Nn6.v.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Fn6 b;

        @DexIgnore
        public Di(Fn6 fn6) {
            this.b = fn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            In6 in6 = this.b.j;
            if (in6 != null) {
                String c = Um5.c(PortfolioApp.get.instance(), 2131886727);
                Wg6.b(c, "LanguageHelper.getString\u2026Activity_Label__Duration)");
                in6.setTitle(c);
            }
            In6 in62 = this.b.j;
            if (in62 != null) {
                in62.M6(Hi5.DURATION);
            }
            In6 in63 = this.b.j;
            if (in63 != null) {
                Lc6<Ii5, Ii5> h = Fn6.L6(this.b).h();
                in63.N6(h != null ? h.getFirst() : null);
            }
            In6 in64 = this.b.j;
            if (in64 != null) {
                Lc6<Ii5, Ii5> h2 = Fn6.L6(this.b).h();
                in64.P6(h2 != null ? h2.getSecond() : null);
            }
            In6 in65 = this.b.j;
            if (in65 != null) {
                in65.S6(Fn6.L6(this.b).m());
            }
            In6 in66 = this.b.j;
            if (in66 != null) {
                in66.Q6(Fn6.L6(this.b).k());
            }
            In6 in67 = this.b.j;
            if (in67 != null) {
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                in67.show(childFragmentManager, Nn6.v.a());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Fn6 b;

        @DexIgnore
        public Ei(Fn6 fn6) {
            this.b = fn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.V6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Fn6 b;

        @DexIgnore
        public Fi(Fn6 fn6) {
            this.b = fn6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> implements Ls0<WorkoutEditViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ Fn6 a;

        @DexIgnore
        public Gi(Fn6 fn6) {
            this.a = fn6;
        }

        @DexIgnore
        public final void a(WorkoutEditViewModel.Ai ai) {
            WorkoutSession d = ai.d();
            if (d != null) {
                this.a.U6(d);
            }
            Boolean f = ai.f();
            if (f != null) {
                this.a.W6(f.booleanValue());
            }
            if (ai.a()) {
                this.a.m();
            } else {
                this.a.k();
            }
            Boolean c = ai.c();
            if (c != null && c.booleanValue()) {
                this.a.e0();
            }
            Lc6<Integer, String> b = ai.b();
            if (b != null) {
                this.a.o(b.getFirst().intValue(), b.getSecond());
            }
            List<Oi5> e = ai.e();
            if (e != null) {
                Fn6.N6(this.a).m(e);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(WorkoutEditViewModel.Ai ai) {
            a(ai);
        }
    }

    /*
    static {
        String simpleName = Fn6.class.getSimpleName();
        Wg6.b(simpleName, "WorkoutEditFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ WorkoutEditViewModel L6(Fn6 fn6) {
        WorkoutEditViewModel workoutEditViewModel = fn6.g;
        if (workoutEditViewModel != null) {
            return workoutEditViewModel;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Jr4 N6(Fn6 fn6) {
        Jr4 jr4 = fn6.i;
        if (jr4 != null) {
            return jr4;
        }
        Wg6.n("mWorkoutTypeAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        WorkoutEditViewModel workoutEditViewModel = this.g;
        if (workoutEditViewModel == null) {
            Wg6.n("mViewModel");
            throw null;
        } else if (workoutEditViewModel.n()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.y0(childFragmentManager);
            return true;
        } else {
            FragmentActivity activity = getActivity();
            if (activity == null) {
                return true;
            }
            activity.finish();
            return true;
        }
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        Wg6.c(intent, "data");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).R5(str, i2, intent);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        } else if (i2 == 2131363291) {
            e0();
        } else if (i2 == 2131363373) {
            V6();
        }
    }

    @DexIgnore
    public final void U6(WorkoutSession workoutSession) {
        Integer second;
        Integer second2;
        G37<Lc5> g37 = this.h;
        if (g37 != null) {
            Lc5 a2 = g37.a();
            if (a2 != null) {
                DateTime editedEndTime = workoutSession.getEditedEndTime();
                if (editedEndTime != null) {
                    long millis = editedEndTime.getMillis();
                    DateTime editedStartTime = workoutSession.getEditedStartTime();
                    if (editedStartTime != null) {
                        Gl7<Integer, Integer, Integer> e0 = TimeUtils.e0((int) ((millis - editedStartTime.getMillis()) / ((long) 1000)));
                        Wg6.b(e0, "DateHelper.getTimeValues(duration.toInt())");
                        Oi5 d = Oi5.Companion.d(workoutSession.getEditedType(), workoutSession.getEditedMode());
                        Jr4 jr4 = this.i;
                        if (jr4 != null) {
                            jr4.o(d);
                            FlexibleTextView flexibleTextView = a2.A;
                            Wg6.b(flexibleTextView, "binding.tvStartTimeValue");
                            DateTime editedStartTime2 = workoutSession.getEditedStartTime();
                            flexibleTextView.setText(TimeUtils.i0(editedStartTime2 != null ? editedStartTime2.toDate() : null));
                            Integer first = e0.getFirst();
                            if (first != null && first.intValue() == 0 && ((second = e0.getSecond()) == null || second.intValue() != 0)) {
                                FlexibleTextView flexibleTextView2 = a2.y;
                                Wg6.b(flexibleTextView2, "binding.tvDurationValue");
                                Hr7 hr7 = Hr7.a;
                                String string = getResources().getString(2131886697);
                                Wg6.b(string, "resources.getString(R.st\u2026ilPage_Label__NumberMins)");
                                String format = String.format(string, Arrays.copyOf(new Object[]{e0.getSecond()}, 1));
                                Wg6.b(format, "java.lang.String.format(format, *args)");
                                flexibleTextView2.setText(format);
                            } else {
                                Integer first2 = e0.getFirst();
                                if ((first2 != null && first2.intValue() == 0) || (second2 = e0.getSecond()) == null || second2.intValue() != 0) {
                                    FlexibleTextView flexibleTextView3 = a2.y;
                                    Wg6.b(flexibleTextView3, "binding.tvDurationValue");
                                    Hr7 hr72 = Hr7.a;
                                    String string2 = getResources().getString(2131886655);
                                    Wg6.b(string2, "resources.getString(R.st\u2026bel__NumberHrsNumberMins)");
                                    String format2 = String.format(string2, Arrays.copyOf(new Object[]{e0.getFirst(), e0.getSecond()}, 2));
                                    Wg6.b(format2, "java.lang.String.format(format, *args)");
                                    flexibleTextView3.setText(format2);
                                } else {
                                    FlexibleTextView flexibleTextView4 = a2.y;
                                    Wg6.b(flexibleTextView4, "binding.tvDurationValue");
                                    Hr7 hr73 = Hr7.a;
                                    String string3 = getResources().getString(2131886203);
                                    Wg6.b(string3, "resources.getString(R.st\u2026enge_Label__StartInHours)");
                                    String format3 = String.format(string3, Arrays.copyOf(new Object[]{e0.getFirst()}, 1));
                                    Wg6.b(format3, "java.lang.String.format(format, *args)");
                                    flexibleTextView4.setText(format3);
                                }
                            }
                            Jr4 jr42 = this.i;
                            if (jr42 != null) {
                                int i2 = jr42.i(d);
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String str = m;
                                local.d(str, "receiveWorkoutSession scrollTo " + i2);
                                if (i2 >= 0) {
                                    a2.v.scrollToPosition(i2);
                                    return;
                                }
                                return;
                            }
                            Wg6.n("mWorkoutTypeAdapter");
                            throw null;
                        }
                        Wg6.n("mWorkoutTypeAdapter");
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void V6() {
        WorkoutEditViewModel workoutEditViewModel = this.g;
        if (workoutEditViewModel != null) {
            workoutEditViewModel.u();
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void W6(boolean z) {
        G37<Lc5> g37 = this.h;
        if (g37 != null) {
            Lc5 a2 = g37.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.s;
                Wg6.b(flexibleButton, "it.fbSave");
                flexibleButton.setEnabled(z);
                if (z) {
                    a2.s.d("flexible_button_primary");
                } else {
                    a2.s.d("flexible_button_disabled");
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public final void k() {
        a();
    }

    @DexIgnore
    public final void m() {
        b();
    }

    @DexIgnore
    public final void o(int i2, String str) {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Lc5 lc5 = (Lc5) Aq0.f(LayoutInflater.from(getContext()), 2131558642, null, false, A6());
        PortfolioApp.get.instance().getIface().W().a(this);
        Po4 po4 = this.k;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(WorkoutEditViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ditViewModel::class.java)");
            this.g = (WorkoutEditViewModel) a2;
            In6 in6 = (In6) getChildFragmentManager().Z(In6.G.a());
            this.j = in6;
            if (in6 == null) {
                this.j = In6.G.b();
            }
            In6 in62 = this.j;
            if (in62 != null) {
                in62.O6(this);
                lc5.A.setOnClickListener(new Ci(this));
                lc5.y.setOnClickListener(new Di(this));
                lc5.s.setOnClickListener(new Ei(this));
                lc5.t.setOnClickListener(new Fi(this));
                Jr4 jr4 = new Jr4(null, null, 3, null);
                jr4.n(new Bi(this));
                this.i = jr4;
                RecyclerView recyclerView = lc5.v;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                Jr4 jr42 = this.i;
                if (jr42 != null) {
                    recyclerView.setAdapter(jr42);
                    WorkoutEditViewModel workoutEditViewModel = this.g;
                    if (workoutEditViewModel != null) {
                        workoutEditViewModel.j().h(getViewLifecycleOwner(), new Gi(this));
                        this.h = new G37<>(this, lc5);
                        Wg6.b(lc5, "binding");
                        return lc5.n();
                    }
                    Wg6.n("mViewModel");
                    throw null;
                }
                Wg6.n("mWorkoutTypeAdapter");
                throw null;
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Vl5 C6 = C6();
        if (C6 != null) {
            C6.c("");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        WorkoutEditViewModel workoutEditViewModel = this.g;
        if (workoutEditViewModel != null) {
            Bundle arguments = getArguments();
            String string = arguments != null ? arguments.getString("EXTRA_WORKOUT_SESSION_ID") : null;
            if (string != null) {
                Wg6.b(string, "arguments?.getString(EXTRA_WORKOUT_SESSION_ID)!!");
                workoutEditViewModel.t(string);
                Vl5 C6 = C6();
                if (C6 != null) {
                    C6.i();
                    return;
                }
                return;
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.In6.Bi
    public void z4(Hi5 hi5, int i2, Integer num, float f, Float f2) {
        int i3 = 0;
        Wg6.c(hi5, "editMode");
        int i4 = Gn6.a[hi5.ordinal()];
        if (i4 == 1) {
            WorkoutEditViewModel workoutEditViewModel = this.g;
            if (workoutEditViewModel != null) {
                if (num != null) {
                    i3 = num.intValue();
                }
                workoutEditViewModel.r(i2, i3);
                return;
            }
            Wg6.n("mViewModel");
            throw null;
        } else if (i4 == 2) {
            WorkoutEditViewModel workoutEditViewModel2 = this.g;
            if (workoutEditViewModel2 != null) {
                if (num != null) {
                    i3 = num.intValue();
                }
                workoutEditViewModel2.q(i2, i3);
                return;
            }
            Wg6.n("mViewModel");
            throw null;
        } else if (i4 == 3) {
            WorkoutEditViewModel workoutEditViewModel3 = this.g;
            if (workoutEditViewModel3 != null) {
                float f3 = (float) i2;
                if (num != null) {
                    i3 = num.intValue();
                }
                float f4 = (float) i3;
                if (f2 != null) {
                    workoutEditViewModel3.p((double) Dl5.h((f4 * f2.floatValue()) + f3, 1));
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        } else if (i4 == 4) {
            WorkoutEditViewModel workoutEditViewModel4 = this.g;
            if (workoutEditViewModel4 != null) {
                workoutEditViewModel4.o((int) (((float) i2) * f));
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        }
    }
}
