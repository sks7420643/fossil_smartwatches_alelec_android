package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rn4 {
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public int b; // = 0;

    @DexIgnore
    public Rn4(int i) {
        this.a = new byte[i];
    }

    @DexIgnore
    public void a(boolean z, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = this.b;
            this.b = i3 + 1;
            c(i3, z);
        }
    }

    @DexIgnore
    public byte[] b(int i) {
        int length = this.a.length * i;
        byte[] bArr = new byte[length];
        for (int i2 = 0; i2 < length; i2++) {
            bArr[i2] = (byte) this.a[i2 / i];
        }
        return bArr;
    }

    @DexIgnore
    public final void c(int i, boolean z) {
        this.a[i] = (byte) (z ? (byte) 1 : 0);
    }
}
