package com.fossil;

import com.baseflow.geolocator.utils.LocaleConverter;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R23 {
    @DexIgnore
    public static String a(M23 m23, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("# ");
        sb.append(str);
        c(m23, sb, 0);
        return sb.toString();
    }

    @DexIgnore
    public static final String b(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (Character.isUpperCase(charAt)) {
                sb.append(LocaleConverter.LOCALE_DELIMITER);
            }
            sb.append(Character.toLowerCase(charAt));
        }
        return sb.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01e2, code lost:
        if (((java.lang.Boolean) r2).booleanValue() == false) goto L_0x01e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0229, code lost:
        if (((java.lang.Integer) r2).intValue() == 0) goto L_0x01e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x023a, code lost:
        if (((java.lang.Float) r2).floatValue() == com.facebook.places.internal.LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) goto L_0x01e4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x024c, code lost:
        if (((java.lang.Double) r2).doubleValue() == 0.0d) goto L_0x01e4;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:113:0x0283  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x01e7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void c(com.fossil.M23 r12, java.lang.StringBuilder r13, int r14) {
        /*
        // Method dump skipped, instructions count: 707
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.R23.c(com.fossil.M23, java.lang.StringBuilder, int):void");
    }

    @DexIgnore
    public static final void d(StringBuilder sb, int i, String str, Object obj) {
        int i2 = 0;
        if (obj instanceof List) {
            for (Object obj2 : (List) obj) {
                d(sb, i, str, obj2);
            }
        } else if (obj instanceof Map) {
            for (Map.Entry entry : ((Map) obj).entrySet()) {
                d(sb, i, str, entry);
            }
        } else {
            sb.append('\n');
            for (int i3 = 0; i3 < i; i3++) {
                sb.append(' ');
            }
            sb.append(str);
            if (obj instanceof String) {
                sb.append(": \"");
                sb.append(T33.a(Xz2.zza((String) obj)));
                sb.append('\"');
            } else if (obj instanceof Xz2) {
                sb.append(": \"");
                sb.append(T33.a((Xz2) obj));
                sb.append('\"');
            } else if (obj instanceof E13) {
                sb.append(" {");
                c((E13) obj, sb, i + 2);
                sb.append("\n");
                while (i2 < i) {
                    sb.append(' ');
                    i2++;
                }
                sb.append("}");
            } else if (obj instanceof Map.Entry) {
                sb.append(" {");
                Map.Entry entry2 = (Map.Entry) obj;
                int i4 = i + 2;
                d(sb, i4, "key", entry2.getKey());
                d(sb, i4, "value", entry2.getValue());
                sb.append("\n");
                while (i2 < i) {
                    sb.append(' ');
                    i2++;
                }
                sb.append("}");
            } else {
                sb.append(": ");
                sb.append(obj.toString());
            }
        }
    }
}
