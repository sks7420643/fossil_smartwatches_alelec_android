package com.fossil;

import android.bluetooth.BluetoothGatt;
import android.os.Handler;
import android.os.Looper;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X5 extends U5 {
    @DexIgnore
    public /* final */ N5 k; // = N5.e;
    @DexIgnore
    public /* final */ Fd0<H7> l;

    @DexIgnore
    public X5(N4 n4) {
        super(V5.b, n4);
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void d(K5 k5) {
        BluetoothGatt bluetoothGatt = k5.c;
        if (bluetoothGatt != null) {
            if (k5.y) {
                k5.v(bluetoothGatt);
            }
            Ky1 ky1 = Ky1.DEBUG;
            bluetoothGatt.close();
        }
        k5.c = null;
        k5.e = false;
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        if (myLooper != null) {
            new Handler(myLooper).postDelayed(new W5(this), 1000);
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.U5
    public N5 h() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public boolean i(H7 h7) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public Fd0<H7> j() {
        return this.l;
    }
}
