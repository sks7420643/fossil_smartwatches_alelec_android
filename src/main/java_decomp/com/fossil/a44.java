package com.fossil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class A44<K0, V0> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ei<Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int a;

        @DexIgnore
        public Ai(int i) {
            this.a = i;
        }

        @DexIgnore
        @Override // com.fossil.A44.Ei
        public <K, V> Map<K, Collection<V>> c() {
            return X34.k(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<V> implements M14<List<V>>, Serializable {
        @DexIgnore
        public /* final */ int expectedValuesPerKey;

        @DexIgnore
        public Bi(int i) {
            A24.b(i, "expectedValuesPerKey");
            this.expectedValuesPerKey = i;
        }

        @DexIgnore
        @Override // com.fossil.M14
        public List<V> get() {
            return new ArrayList(this.expectedValuesPerKey);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<V> implements M14<Set<V>>, Serializable {
        @DexIgnore
        public /* final */ int expectedValuesPerKey;

        @DexIgnore
        public Ci(int i) {
            A24.b(i, "expectedValuesPerKey");
            this.expectedValuesPerKey = i;
        }

        @DexIgnore
        @Override // com.fossil.M14
        public Set<V> get() {
            return X44.e(this.expectedValuesPerKey);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Di<K0, V0> extends A44<K0, V0> {
        @DexIgnore
        public Di() {
            super(null);
        }

        @DexIgnore
        public abstract <K extends K0, V extends V0> S34<K, V> c();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ei<K0> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends Di<K0, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ int a;

            @DexIgnore
            public Aii(int i) {
                this.a = i;
            }

            @DexIgnore
            @Override // com.fossil.A44.Di
            public <K extends K0, V> S34<K, V> c() {
                return B44.b(Ei.this.c(), new Bi(this.a));
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Bii extends Fi<K0, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ int a;

            @DexIgnore
            public Bii(int i) {
                this.a = i;
            }

            @DexIgnore
            @Override // com.fossil.A44.Fi
            public <K extends K0, V> W44<K, V> c() {
                return B44.c(Ei.this.c(), new Ci(this.a));
            }
        }

        @DexIgnore
        public Di<K0, Object> a() {
            return b(2);
        }

        @DexIgnore
        public Di<K0, Object> b(int i) {
            A24.b(i, "expectedValuesPerKey");
            return new Aii(i);
        }

        @DexIgnore
        public abstract <K extends K0, V> Map<K, Collection<V>> c();

        @DexIgnore
        public Fi<K0, Object> d() {
            return e(2);
        }

        @DexIgnore
        public Fi<K0, Object> e(int i) {
            A24.b(i, "expectedValuesPerKey");
            return new Bii(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Fi<K0, V0> extends A44<K0, V0> {
        @DexIgnore
        public Fi() {
            super(null);
        }

        @DexIgnore
        public abstract <K extends K0, V extends V0> W44<K, V> c();
    }

    @DexIgnore
    public A44() {
    }

    @DexIgnore
    public /* synthetic */ A44(Z34 z34) {
        this();
    }

    @DexIgnore
    public static Ei<Object> a() {
        return b(8);
    }

    @DexIgnore
    public static Ei<Object> b(int i) {
        A24.b(i, "expectedKeys");
        return new Ai(i);
    }
}
