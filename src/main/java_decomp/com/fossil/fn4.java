package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fn4 extends Mn4 {
    @DexIgnore
    @Override // com.fossil.Jn4, com.fossil.Ql4
    public Bm4 a(String str, Kl4 kl4, int i, int i2, Map<Ml4, ?> map) throws Rl4 {
        if (kl4 == Kl4.EAN_8) {
            return super.a(str, kl4, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode EAN_8, but got " + kl4);
    }

    @DexIgnore
    @Override // com.fossil.Jn4
    public boolean[] c(String str) {
        if (str.length() == 8) {
            boolean[] zArr = new boolean[67];
            int i = 0;
            int b = Jn4.b(zArr, 0, Ln4.a, true) + 0;
            while (i <= 3) {
                int i2 = i + 1;
                b += Jn4.b(zArr, b, Ln4.d[Integer.parseInt(str.substring(i, i2))], false);
                i = i2;
            }
            int i3 = 4;
            int b2 = b + Jn4.b(zArr, b, Ln4.b, false);
            while (i3 <= 7) {
                int i4 = i3 + 1;
                b2 += Jn4.b(zArr, b2, Ln4.d[Integer.parseInt(str.substring(i3, i4))], true);
                i3 = i4;
            }
            Jn4.b(zArr, b2, Ln4.a, true);
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be 8 digits long, but got " + str.length());
    }
}
