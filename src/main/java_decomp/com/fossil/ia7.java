package com.fossil;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ia7 extends RecyclerView.g<Bi> {
    @DexIgnore
    public boolean a; // = true;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public List<Ai> c; // = Hm7.e();
    @DexIgnore
    public /* final */ Hg6<Typeface, Cd6> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ Typeface a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public Ai(Typeface typeface, boolean z) {
            Wg6.c(typeface, "typeface");
            this.a = typeface;
            this.b = z;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(Typeface typeface, boolean z, int i, Qg6 qg6) {
            this(typeface, (i & 2) != 0 ? false : z);
        }

        @DexIgnore
        public final Typeface a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public final void c(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!Wg6.a(this.a, ai.a) || this.b != ai.b) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            Typeface typeface = this.a;
            int hashCode = typeface != null ? typeface.hashCode() : 0;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "TypeFacePreviewModel(typeface=" + this.a + ", isSelected=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Dg5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Ia7 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.b.b.h()) {
                    Bi bi = this.b;
                    bi.b.l(bi.getAdapterPosition());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Ia7 ia7, Dg5 dg5) {
            super(dg5.b());
            Wg6.c(dg5, "binding");
            this.b = ia7;
            this.a = dg5;
        }

        @DexIgnore
        public final void a(Ai ai) {
            Wg6.c(ai, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            TextView textView = this.a.b;
            Wg6.b(textView, "binding.previewText");
            textView.setActivated(ai.b());
            TextView textView2 = this.a.b;
            Wg6.b(textView2, "binding.previewText");
            textView2.setTypeface(ai.a());
            this.a.b().setOnClickListener(new Aii(this));
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.mapped.Hg6<? super android.graphics.Typeface, com.mapped.Cd6> */
    /* JADX WARN: Multi-variable type inference failed */
    public Ia7(Hg6<? super Typeface, Cd6> hg6) {
        this.d = hg6;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final boolean h() {
        return this.a;
    }

    @DexIgnore
    public void i(Bi bi, int i) {
        Wg6.c(bi, "holder");
        bi.a(this.c.get(i));
    }

    @DexIgnore
    public Bi j(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Dg5 c2 = Dg5.c(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(c2, "ItemTypefacePreviewBindi\u2026tInflater, parent, false)");
        return new Bi(this, c2);
    }

    @DexIgnore
    public final int k(Typeface typeface) {
        int i;
        Wg6.c(typeface, "typeface");
        Iterator<Ai> it = this.c.iterator();
        int i2 = 0;
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                i = -1;
                break;
            } else if (Wg6.a(it.next().a(), typeface)) {
                break;
            } else {
                i2 = i + 1;
            }
        }
        if (i != -1) {
            l(i);
        }
        return i;
    }

    @DexIgnore
    public final void l(int i) {
        int i2 = this.b;
        if (i2 != -1) {
            this.c.get(i2).c(false);
            notifyItemChanged(this.b);
        }
        this.b = i;
        this.c.get(i).c(true);
        notifyItemChanged(i);
        Hg6<Typeface, Cd6> hg6 = this.d;
        if (hg6 != null) {
            hg6.invoke(this.c.get(i).a());
        }
    }

    @DexIgnore
    public final void m(List<? extends Typeface> list) {
        Wg6.c(list, "typefaceList");
        ArrayList arrayList = new ArrayList(Im7.m(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(new Ai(it.next(), false, 2, null));
        }
        this.c = arrayList;
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        i(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return j(viewGroup, i);
    }
}
