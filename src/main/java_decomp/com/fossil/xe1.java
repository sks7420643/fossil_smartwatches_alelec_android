package com.fossil;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.fossil.Af1;
import com.fossil.Wb1;
import java.io.File;
import java.io.FileNotFoundException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xe1 implements Af1<Uri, File> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Bf1<Uri, File> {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public Ai(Context context) {
            this.a = context;
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Uri, File> b(Ef1 ef1) {
            return new Xe1(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Wb1<File> {
        @DexIgnore
        public static /* final */ String[] d; // = {"_data"};
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public /* final */ Uri c;

        @DexIgnore
        public Bi(Context context, Uri uri) {
            this.b = context;
            this.c = uri;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Gb1 c() {
            return Gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void d(Sa1 sa1, Wb1.Ai<? super File> ai) {
            String str = null;
            Cursor query = this.b.getContentResolver().query(this.c, d, null, null, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        str = query.getString(query.getColumnIndexOrThrow("_data"));
                    }
                } finally {
                    query.close();
                }
            }
            if (TextUtils.isEmpty(str)) {
                ai.b(new FileNotFoundException("Failed to find file path for: " + this.c));
                return;
            }
            ai.e(new File(str));
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Class<File> getDataClass() {
            return File.class;
        }
    }

    @DexIgnore
    public Xe1(Context context) {
        this.a = context;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(Uri uri) {
        return d(uri);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Af1$Ai' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai<File> b(Uri uri, int i, int i2, Ob1 ob1) {
        return c(uri, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<File> c(Uri uri, int i, int i2, Ob1 ob1) {
        return new Af1.Ai<>(new Yj1(uri), new Bi(this.a, uri));
    }

    @DexIgnore
    public boolean d(Uri uri) {
        return Jc1.b(uri);
    }
}
