package com.fossil;

import com.mapped.Wg6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E66 {
    @DexIgnore
    public /* final */ C66 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<J06> c;

    @DexIgnore
    public E66(C66 c66, int i, ArrayList<J06> arrayList) {
        Wg6.c(c66, "mView");
        this.a = c66;
        this.b = i;
        this.c = arrayList;
    }

    @DexIgnore
    public final ArrayList<J06> a() {
        ArrayList<J06> arrayList = this.c;
        return arrayList != null ? arrayList : new ArrayList<>();
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final C66 c() {
        return this.a;
    }
}
