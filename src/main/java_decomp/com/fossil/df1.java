package com.fossil;

import com.fossil.Af1;
import com.fossil.Wb1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Df1<Model, Data> implements Af1<Model, Data> {
    @DexIgnore
    public /* final */ List<Af1<Model, Data>> a;
    @DexIgnore
    public /* final */ Mn0<List<Throwable>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<Data> implements Wb1<Data>, Wb1.Ai<Data> {
        @DexIgnore
        public /* final */ List<Wb1<Data>> b;
        @DexIgnore
        public /* final */ Mn0<List<Throwable>> c;
        @DexIgnore
        public int d; // = 0;
        @DexIgnore
        public Sa1 e;
        @DexIgnore
        public Wb1.Ai<? super Data> f;
        @DexIgnore
        public List<Throwable> g;
        @DexIgnore
        public boolean h;

        @DexIgnore
        public Ai(List<Wb1<Data>> list, Mn0<List<Throwable>> mn0) {
            this.c = mn0;
            Ik1.c(list);
            this.b = list;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void a() {
            List<Throwable> list = this.g;
            if (list != null) {
                this.c.a(list);
            }
            this.g = null;
            for (Wb1<Data> wb1 : this.b) {
                wb1.a();
            }
        }

        @DexIgnore
        @Override // com.fossil.Wb1.Ai
        public void b(Exception exc) {
            List<Throwable> list = this.g;
            Ik1.d(list);
            list.add(exc);
            f();
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Gb1 c() {
            return this.b.get(0).c();
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void cancel() {
            this.h = true;
            for (Wb1<Data> wb1 : this.b) {
                wb1.cancel();
            }
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void d(Sa1 sa1, Wb1.Ai<? super Data> ai) {
            this.e = sa1;
            this.f = ai;
            this.g = this.c.b();
            this.b.get(this.d).d(sa1, this);
            if (this.h) {
                cancel();
            }
        }

        @DexIgnore
        @Override // com.fossil.Wb1.Ai
        public void e(Data data) {
            if (data != null) {
                this.f.e(data);
            } else {
                f();
            }
        }

        @DexIgnore
        public final void f() {
            if (!this.h) {
                if (this.d < this.b.size() - 1) {
                    this.d++;
                    d(this.e, this.f);
                    return;
                }
                Ik1.d(this.g);
                this.f.b(new Dd1("Fetch failed", new ArrayList(this.g)));
            }
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Class<Data> getDataClass() {
            return this.b.get(0).getDataClass();
        }
    }

    @DexIgnore
    public Df1(List<Af1<Model, Data>> list, Mn0<List<Throwable>> mn0) {
        this.a = list;
        this.b = mn0;
    }

    @DexIgnore
    @Override // com.fossil.Af1
    public boolean a(Model model) {
        for (Af1<Model, Data> af1 : this.a) {
            if (af1.a(model)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Af1
    public Af1.Ai<Data> b(Model model, int i, int i2, Ob1 ob1) {
        Mb1 mb1;
        Af1.Ai<Data> b2;
        int size = this.a.size();
        ArrayList arrayList = new ArrayList(size);
        Mb1 mb12 = null;
        int i3 = 0;
        while (i3 < size) {
            Af1<Model, Data> af1 = this.a.get(i3);
            if (!af1.a(model) || (b2 = af1.b(model, i, i2, ob1)) == null) {
                mb1 = mb12;
            } else {
                mb1 = b2.a;
                arrayList.add(b2.c);
            }
            i3++;
            mb12 = mb1;
        }
        if (arrayList.isEmpty() || mb12 == null) {
            return null;
        }
        return new Af1.Ai<>(mb12, new Ai(arrayList, this.b));
    }

    @DexIgnore
    public String toString() {
        return "MultiModelLoader{modelLoaders=" + Arrays.toString(this.a.toArray()) + '}';
    }
}
