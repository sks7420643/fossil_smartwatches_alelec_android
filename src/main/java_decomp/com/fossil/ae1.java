package com.fossil;

import android.graphics.Bitmap;
import android.os.Build;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ae1 implements Yd1 {
    @DexIgnore
    public static /* final */ Bitmap.Config[] d;
    @DexIgnore
    public static /* final */ Bitmap.Config[] e;
    @DexIgnore
    public static /* final */ Bitmap.Config[] f; // = {Bitmap.Config.RGB_565};
    @DexIgnore
    public static /* final */ Bitmap.Config[] g; // = {Bitmap.Config.ARGB_4444};
    @DexIgnore
    public static /* final */ Bitmap.Config[] h; // = {Bitmap.Config.ALPHA_8};
    @DexIgnore
    public /* final */ Ci a; // = new Ci();
    @DexIgnore
    public /* final */ Ud1<Bi, Bitmap> b; // = new Ud1<>();
    @DexIgnore
    public /* final */ Map<Bitmap.Config, NavigableMap<Integer, Integer>> c; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;

        /*
        static {
            int[] iArr = new int[Bitmap.Config.values().length];
            a = iArr;
            try {
                iArr[Bitmap.Config.ARGB_8888.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Bitmap.Config.RGB_565.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[Bitmap.Config.ARGB_4444.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[Bitmap.Config.ALPHA_8.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Zd1 {
        @DexIgnore
        public /* final */ Ci a;
        @DexIgnore
        public int b;
        @DexIgnore
        public Bitmap.Config c;

        @DexIgnore
        public Bi(Ci ci) {
            this.a = ci;
        }

        @DexIgnore
        @Override // com.fossil.Zd1
        public void a() {
            this.a.c(this);
        }

        @DexIgnore
        public void b(int i, Bitmap.Config config) {
            this.b = i;
            this.c = config;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof Bi)) {
                return false;
            }
            Bi bi = (Bi) obj;
            return this.b == bi.b && Jk1.d(this.c, bi.c);
        }

        @DexIgnore
        public int hashCode() {
            int i = this.b;
            Bitmap.Config config = this.c;
            return (config != null ? config.hashCode() : 0) + (i * 31);
        }

        @DexIgnore
        public String toString() {
            return Ae1.h(this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci extends Qd1<Bi> {
        @DexIgnore
        /* Return type fixed from 'com.fossil.Zd1' to match base method */
        @Override // com.fossil.Qd1
        public /* bridge */ /* synthetic */ Bi a() {
            return d();
        }

        @DexIgnore
        public Bi d() {
            return new Bi(this);
        }

        @DexIgnore
        public Bi e(int i, Bitmap.Config config) {
            Bi bi = (Bi) b();
            bi.b(i, config);
            return bi;
        }
    }

    /*
    static {
        Bitmap.Config[] configArr = {Bitmap.Config.ARGB_8888, null};
        if (Build.VERSION.SDK_INT >= 26) {
            configArr = (Bitmap.Config[]) Arrays.copyOf(configArr, 3);
            configArr[configArr.length - 1] = Bitmap.Config.RGBA_F16;
        }
        d = configArr;
        e = configArr;
    }
    */

    @DexIgnore
    public static String h(int i, Bitmap.Config config) {
        return "[" + i + "](" + config + ")";
    }

    @DexIgnore
    public static Bitmap.Config[] i(Bitmap.Config config) {
        if (Build.VERSION.SDK_INT >= 26 && Bitmap.Config.RGBA_F16.equals(config)) {
            return e;
        }
        int i = Ai.a[config.ordinal()];
        if (i == 1) {
            return d;
        }
        if (i == 2) {
            return f;
        }
        if (i == 3) {
            return g;
        }
        if (i == 4) {
            return h;
        }
        return new Bitmap.Config[]{config};
    }

    @DexIgnore
    @Override // com.fossil.Yd1
    public String a(int i, int i2, Bitmap.Config config) {
        return h(Jk1.g(i, i2, config), config);
    }

    @DexIgnore
    @Override // com.fossil.Yd1
    public void b(Bitmap bitmap) {
        Bi e2 = this.a.e(Jk1.h(bitmap), bitmap.getConfig());
        this.b.d(e2, bitmap);
        NavigableMap<Integer, Integer> j = j(bitmap.getConfig());
        Integer num = (Integer) j.get(Integer.valueOf(e2.b));
        j.put(Integer.valueOf(e2.b), Integer.valueOf(num == null ? 1 : num.intValue() + 1));
    }

    @DexIgnore
    @Override // com.fossil.Yd1
    public Bitmap c(int i, int i2, Bitmap.Config config) {
        Bi g2 = g(Jk1.g(i, i2, config), config);
        Bitmap a2 = this.b.a(g2);
        if (a2 != null) {
            f(Integer.valueOf(g2.b), a2);
            a2.reconfigure(i, i2, config);
        }
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.Yd1
    public String d(Bitmap bitmap) {
        return h(Jk1.h(bitmap), bitmap.getConfig());
    }

    @DexIgnore
    @Override // com.fossil.Yd1
    public int e(Bitmap bitmap) {
        return Jk1.h(bitmap);
    }

    @DexIgnore
    public final void f(Integer num, Bitmap bitmap) {
        NavigableMap<Integer, Integer> j = j(bitmap.getConfig());
        Integer num2 = (Integer) j.get(num);
        if (num2 == null) {
            throw new NullPointerException("Tried to decrement empty size, size: " + num + ", removed: " + d(bitmap) + ", this: " + this);
        } else if (num2.intValue() == 1) {
            j.remove(num);
        } else {
            j.put(num, Integer.valueOf(num2.intValue() - 1));
        }
    }

    @DexIgnore
    public final Bi g(int i, Bitmap.Config config) {
        Bi e2 = this.a.e(i, config);
        Bitmap.Config[] i2 = i(config);
        int length = i2.length;
        int i3 = 0;
        while (true) {
            if (i3 >= length) {
                break;
            }
            Bitmap.Config config2 = i2[i3];
            Integer ceilingKey = j(config2).ceilingKey(Integer.valueOf(i));
            if (ceilingKey == null || ceilingKey.intValue() > i * 8) {
                i3++;
            } else if (ceilingKey.intValue() != i || (config2 != null ? !config2.equals(config) : config != null)) {
                this.a.c(e2);
                return this.a.e(ceilingKey.intValue(), config2);
            }
        }
        return e2;
    }

    @DexIgnore
    public final NavigableMap<Integer, Integer> j(Bitmap.Config config) {
        NavigableMap<Integer, Integer> navigableMap = this.c.get(config);
        if (navigableMap != null) {
            return navigableMap;
        }
        TreeMap treeMap = new TreeMap();
        this.c.put(config, treeMap);
        return treeMap;
    }

    @DexIgnore
    @Override // com.fossil.Yd1
    public Bitmap removeLast() {
        Bitmap f2 = this.b.f();
        if (f2 != null) {
            f(Integer.valueOf(Jk1.h(f2)), f2);
        }
        return f2;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SizeConfigStrategy{groupedMap=");
        sb.append(this.b);
        sb.append(", sortedSizes=(");
        for (Map.Entry<Bitmap.Config, NavigableMap<Integer, Integer>> entry : this.c.entrySet()) {
            sb.append(entry.getKey());
            sb.append('[');
            sb.append(entry.getValue());
            sb.append("], ");
        }
        if (!this.c.isEmpty()) {
            sb.replace(sb.length() - 2, sb.length(), "");
        }
        sb.append(")}");
        return sb.toString();
    }
}
