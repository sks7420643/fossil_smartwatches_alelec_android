package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Si1 {
    @DexIgnore
    public /* final */ List<Ai<?>> a; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<T> {
        @DexIgnore
        public /* final */ Class<T> a;
        @DexIgnore
        public /* final */ Jb1<T> b;

        @DexIgnore
        public Ai(Class<T> cls, Jb1<T> jb1) {
            this.a = cls;
            this.b = jb1;
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    @DexIgnore
    public <T> void a(Class<T> cls, Jb1<T> jb1) {
        synchronized (this) {
            this.a.add(new Ai<>(cls, jb1));
        }
    }

    @DexIgnore
    public <T> Jb1<T> b(Class<T> cls) {
        synchronized (this) {
            for (Ai<?> ai : this.a) {
                if (ai.a(cls)) {
                    return ai.b;
                }
            }
            return null;
        }
    }
}
