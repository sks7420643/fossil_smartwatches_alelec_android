package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ej1<R> {
    @DexIgnore
    boolean e(Dd1 dd1, Object obj, Qj1<R> qj1, boolean z);

    @DexIgnore
    boolean g(R r, Object obj, Qj1<R> qj1, Gb1 gb1, boolean z);
}
