package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.welcome.CompatibleModelsActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H27 extends BaseFragment implements G27, View.OnClickListener {
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public F27 g;
    @DexIgnore
    public G37<Jc5> h;
    @DexIgnore
    public int i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final H27 a() {
            H27 h27 = new H27();
            h27.setArguments(Nm0.a(new Lc6("KEY_DIANA_REQUIRE", Boolean.TRUE)));
            return h27;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Jc5 a;

        @DexIgnore
        public Bi(Jc5 jc5) {
            this.a = jc5;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            FlexibleButton flexibleButton = this.a.r;
            Wg6.b(flexibleButton, "it.fbGetStarted");
            flexibleButton.setEnabled(z);
            this.a.r.d(z ? "flexible_button_secondary" : "flexible_button_disabled");
        }
    }

    @DexIgnore
    public H27() {
        String d = ThemeManager.l.a().d("nonBrandBlack");
        this.i = Color.parseColor(d == null ? "#FFFFFF" : d);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    public void K6(F27 f27) {
        Wg6.c(f27, "presenter");
        this.g = f27;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(F27 f27) {
        K6(f27);
    }

    @DexIgnore
    @Override // com.fossil.G27
    public void V5() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            SignUpActivity.a aVar = SignUpActivity.F;
            Wg6.b(activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    @Override // com.fossil.G27
    public void d2() {
        S37 s37 = S37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        s37.i(childFragmentManager);
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131362274) {
                F27 f27 = this.g;
                if (f27 != null) {
                    f27.n();
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            } else if (id == 2131362738) {
                F27 f272 = this.g;
                if (f272 != null) {
                    f272.o();
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        G37<Jc5> g37 = new G37<>(this, (Jc5) Aq0.f(layoutInflater, 2131558641, viewGroup, false, A6()));
        this.h = g37;
        if (g37 != null) {
            Jc5 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        ShimmerFrameLayout shimmerFrameLayout;
        super.onPause();
        G37<Jc5> g37 = this.h;
        if (g37 != null) {
            Jc5 a2 = g37.a();
            if (!(a2 == null || (shimmerFrameLayout = a2.w) == null)) {
                shimmerFrameLayout.stopShimmerAnimation();
            }
            F27 f27 = this.g;
            if (f27 != null) {
                f27.m();
                Vl5 C6 = C6();
                if (C6 != null) {
                    C6.c("");
                    return;
                }
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        ShimmerFrameLayout shimmerFrameLayout;
        super.onResume();
        G37<Jc5> g37 = this.h;
        if (g37 != null) {
            Jc5 a2 = g37.a();
            if (!(a2 == null || (shimmerFrameLayout = a2.w) == null)) {
                shimmerFrameLayout.startShimmerAnimation();
            }
            F27 f27 = this.g;
            if (f27 != null) {
                f27.l();
                Vl5 C6 = C6();
                if (C6 != null) {
                    C6.i();
                    return;
                }
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<Jc5> g37 = this.h;
        if (g37 != null) {
            Jc5 a2 = g37.a();
            if (a2 != null) {
                if (!Wr4.a.a().d()) {
                    ShimmerFrameLayout shimmerFrameLayout = a2.w;
                    Wg6.b(shimmerFrameLayout, "it.shmNote");
                    shimmerFrameLayout.setVisibility(4);
                }
                a2.r.setOnClickListener(this);
                a2.v.setOnClickListener(this);
                if (Wg6.a(Wr4.a.a().h(), "CN")) {
                    FlexibleCheckBox flexibleCheckBox = a2.q;
                    Wg6.b(flexibleCheckBox, "it.cbTermsService");
                    flexibleCheckBox.setVisibility(0);
                    FlexibleTextView flexibleTextView = a2.u;
                    Wg6.b(flexibleTextView, "it.ftvTermsService");
                    flexibleTextView.setVisibility(0);
                    FlexibleButton flexibleButton = a2.r;
                    Wg6.b(flexibleButton, "it.fbGetStarted");
                    flexibleButton.setEnabled(false);
                    a2.r.d("flexible_button_disabled");
                    a2.u.setLinkTextColor(this.i);
                    FlexibleTextView flexibleTextView2 = a2.u;
                    Wg6.b(flexibleTextView2, "it.ftvTermsService");
                    flexibleTextView2.setMovementMethod(new LinkMovementMethod());
                    a2.q.setOnCheckedChangeListener(new Bi(a2));
                } else {
                    FlexibleCheckBox flexibleCheckBox2 = a2.q;
                    Wg6.b(flexibleCheckBox2, "it.cbTermsService");
                    flexibleCheckBox2.setVisibility(8);
                    FlexibleTextView flexibleTextView3 = a2.u;
                    Wg6.b(flexibleTextView3, "it.ftvTermsService");
                    flexibleTextView3.setVisibility(8);
                    FlexibleButton flexibleButton2 = a2.r;
                    Wg6.b(flexibleButton2, "it.fbGetStarted");
                    flexibleButton2.setEnabled(true);
                    a2.r.d("flexible_button_secondary");
                }
            }
            Bundle arguments = getArguments();
            if (arguments != null && arguments.getBoolean("KEY_DIANA_REQUIRE")) {
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                s37.u(childFragmentManager);
            }
            E6("tutorial_view");
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.G27
    public void w0(Spanned spanned) {
        FlexibleTextView flexibleTextView;
        Wg6.c(spanned, "message");
        if (isActive()) {
            G37<Jc5> g37 = this.h;
            if (g37 != null) {
                Jc5 a2 = g37.a();
                if (a2 != null && (flexibleTextView = a2.u) != null) {
                    flexibleTextView.setText(spanned);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.G27
    public void y5() {
        CompatibleModelsActivity.a aVar = CompatibleModelsActivity.A;
        Context requireContext = requireContext();
        Wg6.b(requireContext, "requireContext()");
        aVar.a(requireContext);
    }
}
