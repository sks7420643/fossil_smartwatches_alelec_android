package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Af6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Rl6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sv7<T> extends Au7<T> implements Rl6<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "kotlinx.coroutines.DeferredCoroutine", f = "Builders.common.kt", l = {99}, m = "await$suspendImpl")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ Sv7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Sv7 sv7, Xe6 xe6) {
            super(xe6);
            this.this$0 = sv7;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return Sv7.A0(this.this$0, this);
        }
    }

    @DexIgnore
    public Sv7(Af6 af6, boolean z) {
        super(af6, z);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ java.lang.Object A0(com.fossil.Sv7 r5, com.mapped.Xe6 r6) {
        /*
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.fossil.Sv7.Ai
            if (r0 == 0) goto L_0x0028
            r0 = r6
            com.fossil.Sv7$Ai r0 = (com.fossil.Sv7.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0028
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0036
            if (r3 != r4) goto L_0x002e
            java.lang.Object r0 = r0.L$0
            com.fossil.Sv7 r0 = (com.fossil.Sv7) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            return r0
        L_0x0028:
            com.fossil.Sv7$Ai r0 = new com.fossil.Sv7$Ai
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x002e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0036:
            com.fossil.El7.b(r1)
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r5.q(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0027
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Sv7.A0(com.fossil.Sv7, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    @Override // com.mapped.Rl6
    public Object l(Xe6<? super T> xe6) {
        return A0(this, xe6);
    }
}
