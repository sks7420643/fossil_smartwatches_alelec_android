package com.fossil;

import androidx.loader.app.LoaderManager;
import com.mapped.Wg6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R56 {
    @DexIgnore
    public /* final */ P56 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<J06> c;
    @DexIgnore
    public /* final */ LoaderManager d;

    @DexIgnore
    public R56(P56 p56, int i, ArrayList<J06> arrayList, LoaderManager loaderManager) {
        Wg6.c(p56, "mView");
        Wg6.c(loaderManager, "mLoaderManager");
        this.a = p56;
        this.b = i;
        this.c = arrayList;
        this.d = loaderManager;
    }

    @DexIgnore
    public final ArrayList<J06> a() {
        ArrayList<J06> arrayList = this.c;
        return arrayList != null ? arrayList : new ArrayList<>();
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final LoaderManager c() {
        return this.d;
    }

    @DexIgnore
    public final P56 d() {
        return this.a;
    }
}
