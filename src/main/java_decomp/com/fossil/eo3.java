package com.fossil;

import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eo3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ String d;
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ Un3 f;

    @DexIgnore
    public Eo3(Un3 un3, AtomicReference atomicReference, String str, String str2, String str3) {
        this.f = un3;
        this.b = atomicReference;
        this.c = str;
        this.d = str2;
        this.e = str3;
    }

    @DexIgnore
    public final void run() {
        this.f.a.O().T(this.b, this.c, this.d, this.e);
    }
}
