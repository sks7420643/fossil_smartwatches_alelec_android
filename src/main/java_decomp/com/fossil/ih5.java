package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ih5 implements Factory<Hh5> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;

    @DexIgnore
    public Ih5(Provider<UserRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Ih5 a(Provider<UserRepository> provider) {
        return new Ih5(provider);
    }

    @DexIgnore
    public static Hh5 c(UserRepository userRepository) {
        return new Hh5(userRepository);
    }

    @DexIgnore
    public Hh5 b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
