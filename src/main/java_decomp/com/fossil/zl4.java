package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Zl4 {
    @DexIgnore
    public static /* final */ Zl4 b; // = new Xl4(null, 0, 0);
    @DexIgnore
    public /* final */ Zl4 a;

    @DexIgnore
    public Zl4(Zl4 zl4) {
        this.a = zl4;
    }

    @DexIgnore
    public final Zl4 a(int i, int i2) {
        return new Xl4(this, i, i2);
    }

    @DexIgnore
    public final Zl4 b(int i, int i2) {
        return new Ul4(this, i, i2);
    }

    @DexIgnore
    public abstract void c(Am4 am4, byte[] bArr);

    @DexIgnore
    public final Zl4 d() {
        return this.a;
    }
}
