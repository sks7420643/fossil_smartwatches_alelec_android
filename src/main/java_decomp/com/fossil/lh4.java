package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import com.mapped.W6;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lh4 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ SharedPreferences b;
    @DexIgnore
    public /* final */ AtomicBoolean c; // = new AtomicBoolean(c());

    @DexIgnore
    public Lh4(Context context, String str, Fe4 fe4) {
        this.a = a(context);
        this.b = context.getSharedPreferences("com.google.firebase.common.prefs:" + str, 0);
    }

    @DexIgnore
    public static Context a(Context context) {
        return (Build.VERSION.SDK_INT < 24 || W6.l(context)) ? context : W6.b(context);
    }

    @DexIgnore
    public boolean b() {
        return this.c.get();
    }

    @DexIgnore
    public final boolean c() {
        ApplicationInfo applicationInfo;
        if (this.b.contains("firebase_data_collection_default_enabled")) {
            return this.b.getBoolean("firebase_data_collection_default_enabled", true);
        }
        try {
            PackageManager packageManager = this.a.getPackageManager();
            if (packageManager == null || (applicationInfo = packageManager.getApplicationInfo(this.a.getPackageName(), 128)) == null || applicationInfo.metaData == null || !applicationInfo.metaData.containsKey("firebase_data_collection_default_enabled")) {
                return true;
            }
            return applicationInfo.metaData.getBoolean("firebase_data_collection_default_enabled");
        } catch (PackageManager.NameNotFoundException e) {
            return true;
        }
    }
}
