package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Bu {
    c(new byte[]{1}),
    d(new byte[]{2}),
    e(new byte[]{3}),
    f(new byte[]{DateTimeFieldType.HOUR_OF_HALFDAY});
    
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public Bu(byte[] bArr) {
        this.b = bArr;
    }
}
