package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F2 implements Parcelable.Creator<G2> {
    @DexIgnore
    public /* synthetic */ F2(Qg6 qg6) {
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0057 A[LOOP:1: B:10:0x0046->B:12:0x0057, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x005b A[EDGE_INSN: B:29:0x005b->B:14:0x005b ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.G2 a(byte r12, byte[] r13) throws java.lang.IllegalArgumentException {
        /*
            r11 = this;
            r1 = 0
            java.nio.ByteBuffer r0 = java.nio.ByteBuffer.wrap(r13)
            java.nio.ByteOrder r2 = java.nio.ByteOrder.LITTLE_ENDIAN
            java.nio.ByteBuffer r3 = r0.order(r2)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            r0 = r1
        L_0x0011:
            int r2 = r13.length
            if (r0 >= r2) goto L_0x00a3
            byte r2 = r3.get(r0)
            short r5 = com.fossil.Hy1.p(r2)
            int r2 = r0 + 1
            int r6 = r2 + r5
            int r7 = r13.length
            if (r6 > r7) goto L_0x0074
            byte[] r6 = com.fossil.Dm7.k(r13, r2, r6)
            int r2 = r6.length
            int r2 = r2 % 3
            if (r2 != 0) goto L_0x005f
            int r2 = r6.length
            com.fossil.Wr7 r2 = com.fossil.Bs7.m(r1, r2)
            r7 = 3
            com.fossil.Ur7 r7 = com.fossil.Bs7.l(r2, r7)
            int r2 = r7.a()
            int r8 = r7.b()
            int r7 = r7.c()
            if (r7 < 0) goto L_0x0059
            if (r2 > r8) goto L_0x005b
        L_0x0046:
            com.fossil.U8 r9 = com.fossil.V8.CREATOR
            int r10 = r2 + 3
            byte[] r10 = com.fossil.Dm7.k(r6, r2, r10)
            com.fossil.V8 r9 = r9.a(r10)
            r4.add(r9)
            if (r2 == r8) goto L_0x005b
            int r2 = r2 + r7
            goto L_0x0046
        L_0x0059:
            if (r2 >= r8) goto L_0x0046
        L_0x005b:
            int r2 = r5 + 1
            int r0 = r0 + r2
            goto L_0x0011
        L_0x005f:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Chain size ("
            java.lang.StringBuilder r1 = com.fossil.E.e(r1)
            int r2 = r6.length
            java.lang.String r3 = ") is not "
            java.lang.String r4 = "divide to 3."
            java.lang.String r1 = com.fossil.E.c(r1, r2, r3, r4)
            r0.<init>(r1)
            throw r0
        L_0x0074:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Invalid Chain length "
            r0.append(r1)
            r1 = 40
            r0.append(r1)
            r0.append(r6)
            java.lang.String r1 = "), "
            r0.append(r1)
            java.lang.String r1 = "remain "
            r0.append(r1)
            int r1 = r13.length
            r0.append(r1)
            r1 = 46
            r0.append(r1)
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x00a3:
            com.fossil.V8[] r0 = new com.fossil.V8[r1]
            java.lang.Object[] r0 = r4.toArray(r0)
            if (r0 == 0) goto L_0x00b3
            com.fossil.G2 r1 = new com.fossil.G2
            com.fossil.V8[] r0 = (com.fossil.V8[]) r0
            r1.<init>(r12, r0, r13)
            return r1
        L_0x00b3:
            com.mapped.Rc6 r0 = new com.mapped.Rc6
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Array<T>"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.F2.a(byte, byte[]):com.fossil.G2");
    }

    @DexIgnore
    public G2 b(Parcel parcel) {
        return new G2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public G2 createFromParcel(Parcel parcel) {
        return new G2(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public G2[] newArray(int i) {
        return new G2[i];
    }
}
