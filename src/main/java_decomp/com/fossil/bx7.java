package com.fossil;

import com.mapped.Af6;
import com.mapped.Lk6;
import com.mapped.Rm6;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Future;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bx7 {
    @DexIgnore
    public static final Uu7 a(Rm6 rm6) {
        return Dx7.a(rm6);
    }

    @DexIgnore
    public static final void c(Af6 af6, CancellationException cancellationException) {
        Dx7.c(af6, cancellationException);
    }

    @DexIgnore
    public static final void e(Lk6<?> lk6, Future<?> future) {
        Cx7.a(lk6, future);
    }

    @DexIgnore
    public static final Dw7 f(Rm6 rm6, Dw7 dw7) {
        return Dx7.e(rm6, dw7);
    }

    @DexIgnore
    public static final void g(Af6 af6) {
        Dx7.f(af6);
    }

    @DexIgnore
    public static final void h(Rm6 rm6) {
        Dx7.g(rm6);
    }
}
