package com.fossil;

import android.bluetooth.BluetoothAdapter;
import com.fossil.Ry1;
import com.mapped.Cd6;
import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zk1 extends Ox1 {
    @DexIgnore
    public static /* final */ Ci u; // = new Ci(null);
    @DexIgnore
    public Al1 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;
    @DexIgnore
    public /* final */ String i;
    @DexIgnore
    public /* final */ Ry1 j;
    @DexIgnore
    public /* final */ Ry1 k;
    @DexIgnore
    public /* final */ Ry1 l;
    @DexIgnore
    public /* final */ LinkedHashMap<Short, Ry1> m;
    @DexIgnore
    public /* final */ LinkedHashMap<Short, Ry1> n;
    @DexIgnore
    public /* final */ Ai o;
    @DexIgnore
    public /* final */ Zm1[] p;
    @DexIgnore
    public /* final */ Ry1 q;
    @DexIgnore
    public /* final */ String r;
    @DexIgnore
    public /* final */ Ry1 s;
    @DexIgnore
    public /* final */ String t;

    @DexIgnore
    public enum Ai {
        NO_REQUIRE((byte) 0),
        REQUIRE((byte) 1);
        
        @DexIgnore
        public static /* final */ Aii d; // = new Aii(null);
        @DexIgnore
        public /* final */ byte b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
            }

            @DexIgnore
            public final Ai a(byte b) {
                Ai[] values = Ai.values();
                for (Ai ai : values) {
                    if (ai.a() == b) {
                        return ai;
                    }
                }
                return null;
            }
        }

        @DexIgnore
        public Ai(byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public final byte a() {
            return this.b;
        }
    }

    @DexIgnore
    public enum Bi {
        SERIAL_NUMBER(1),
        HARDWARE_REVISION(2),
        FIRMWARE_VERSION(3),
        MODEL_NUMBER(4),
        HEART_RATE_SERIAL_NUMBER(5),
        BOOTLOADER_VERSION(6),
        WATCH_APP_VERSION(7),
        FONT_VERSION(8),
        LUTS_VERSION(9),
        SUPPORTED_FILES_VERSION(10),
        BOND_REQUIREMENT(11),
        SUPPORTED_DEVICE_CONFIGS(12),
        DEVICE_SECURITY_VERSION(14),
        SOCKET_INFO(15),
        LOCALE(16),
        MICRO_APP_SYSTEM_VERSION(17),
        LOCALE_VERSION(18),
        CURRENT_FILES_VERSION(19),
        UNKNOWN((short) 65535);
        
        @DexIgnore
        public static /* final */ Aii d; // = new Aii(null);
        @DexIgnore
        public /* final */ short b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
            }

            @DexIgnore
            public final Bi a(short s) {
                Bi bi;
                Bi[] values = Bi.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        bi = null;
                        break;
                    }
                    bi = values[i];
                    if (bi.a() == s) {
                        break;
                    }
                    i++;
                }
                return bi != null ? bi : Bi.UNKNOWN;
            }
        }

        @DexIgnore
        public Bi(short s) {
            this.b = (short) s;
        }

        @DexIgnore
        public final short a() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public /* synthetic */ Ci(Qg6 qg6) {
        }

        @DexIgnore
        public final Zk1 a(Zk1 zk1, Zk1 zk12) {
            return new Zk1(zk12.getName().length() > 0 ? zk12.getName() : zk1.getName(), zk12.getMacAddress().length() > 0 ? zk12.getMacAddress() : zk1.getMacAddress(), d(zk12.getSerialNumber()) ? zk12.getSerialNumber() : zk1.getSerialNumber(), zk12.getHardwareRevision().length() > 0 ? zk12.getHardwareRevision() : zk1.getHardwareRevision(), zk12.getFirmwareVersion().length() > 0 ? zk12.getFirmwareVersion() : zk1.getFirmwareVersion(), zk12.getModelNumber().length() > 0 ? zk12.getModelNumber() : zk1.getModelNumber(), zk12.f().length() > 0 ? zk12.f() : zk1.f(), Wg6.a(zk12.b(), new Ry1(0, 0)) ^ true ? zk12.b() : zk1.b(), Wg6.a(zk12.i(), new Ry1(0, 0)) ^ true ? zk12.i() : zk1.i(), Wg6.a(zk12.e(), new Ry1(0, 0)) ^ true ? zk12.e() : zk1.e(), zk12.h().isEmpty() ^ true ? zk12.h() : zk1.h(), zk12.c().isEmpty() ^ true ? zk12.c() : zk1.c(), zk12.a(), (zk12.g().length == 0) ^ true ? zk12.g() : zk1.g(), Wg6.a(zk12.d(), new Ry1(0, 0)) ^ true ? zk12.d() : zk1.d(), Wg6.a(zk12.getLocaleString(), "en_US") ^ true ? zk12.getLocaleString() : zk1.getLocaleString(), Wg6.a(zk12.getMicroAppVersion(), new Ry1(0, 0)) ^ true ? zk12.getMicroAppVersion() : zk1.getMicroAppVersion(), Wg6.a(zk12.getFastPairIdInHexString(), "") ^ true ? zk12.getFastPairIdInHexString() : zk1.getFastPairIdInHexString());
        }

        @DexIgnore
        public final Zk1 b(JSONObject jSONObject) {
            String optString = jSONObject.optString(Ey1.a(Jd0.H), "");
            String optString2 = jSONObject.optString(Ey1.a(Jd0.k0), "");
            Wg6.b(optString, "name");
            if ((optString.length() == 0) || !BluetoothAdapter.checkBluetoothAddress(optString2)) {
                return null;
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            JSONArray optJSONArray = jSONObject.optJSONArray(Ey1.a(Jd0.I2));
            if (optJSONArray != null) {
                int length = optJSONArray.length();
                for (int i = 0; i < length; i++) {
                    try {
                        JSONObject optJSONObject = optJSONArray.optJSONObject(i);
                        if (optJSONObject != null) {
                            short parseShort = Short.parseShort(optJSONObject.get(Ey1.a(Jd0.A0)).toString());
                            Ry1.Ai ai = Ry1.CREATOR;
                            String optString3 = optJSONObject.optString(Ey1.a(Jd0.K2));
                            Wg6.b(optString3, "fileVersionJSON.optStrin\u2026ey.VERSION.lowerCaseName)");
                            linkedHashMap.put(Short.valueOf(parseShort), ai.c(optString3));
                        }
                    } catch (JSONException e) {
                        D90.i.i(e);
                    }
                }
            }
            LinkedHashMap linkedHashMap2 = new LinkedHashMap();
            JSONArray optJSONArray2 = jSONObject.optJSONArray(Ey1.a(Jd0.A4));
            if (optJSONArray2 != null) {
                int length2 = optJSONArray2.length();
                for (int i2 = 0; i2 < length2; i2++) {
                    try {
                        JSONObject optJSONObject2 = optJSONArray2.optJSONObject(i2);
                        if (optJSONObject2 != null) {
                            short parseShort2 = Short.parseShort(optJSONObject2.get(Ey1.a(Jd0.A0)).toString());
                            Ry1.Ai ai2 = Ry1.CREATOR;
                            String optString4 = optJSONObject2.optString(Ey1.a(Jd0.K2));
                            Wg6.b(optString4, "fileVersionJSON.optStrin\u2026ey.VERSION.lowerCaseName)");
                            linkedHashMap2.put(Short.valueOf(parseShort2), ai2.c(optString4));
                        }
                    } catch (JSONException e2) {
                        D90.i.i(e2);
                    }
                }
            }
            ArrayList arrayList = new ArrayList();
            JSONArray optJSONArray3 = jSONObject.optJSONArray(Ey1.a(Jd0.t0));
            if (optJSONArray3 != null) {
                int length3 = optJSONArray3.length();
                for (int i3 = 0; i3 < length3; i3++) {
                    try {
                        Zm1 a2 = Zm1.e.a(optJSONArray3.get(i3).toString());
                        if (a2 != null) {
                            arrayList.add(a2);
                        }
                    } catch (JSONException e3) {
                        D90.i.i(e3);
                    }
                }
            }
            Wg6.b(optString2, "macAddress");
            String optString5 = jSONObject.optString(Ey1.a(Jd0.l0), "");
            Wg6.b(optString5, "jsonObject.optString(JSO\u2026NUMBER.lowerCaseName, \"\")");
            String optString6 = jSONObject.optString(Ey1.a(Jd0.m0), "");
            Wg6.b(optString6, "jsonObject.optString(JSO\u2026VISION.lowerCaseName, \"\")");
            String optString7 = jSONObject.optString(Ey1.a(Jd0.n0), "");
            Wg6.b(optString7, "jsonObject.optString(JSO\u2026ERSION.lowerCaseName, \"\")");
            String optString8 = jSONObject.optString(Ey1.a(Jd0.o0), "");
            Wg6.b(optString8, "jsonObject.optString(JSO\u2026NUMBER.lowerCaseName, \"\")");
            String optString9 = jSONObject.optString(Ey1.a(Jd0.p0), "");
            Wg6.b(optString9, "jsonObject.optString(JSO\u2026NUMBER.lowerCaseName, \"\")");
            Ry1.Ai ai3 = Ry1.CREATOR;
            String optString10 = jSONObject.optString(Ey1.a(Jd0.q0), "");
            Wg6.b(optString10, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
            Ry1 c = ai3.c(optString10);
            Ry1.Ai ai4 = Ry1.CREATOR;
            String optString11 = jSONObject.optString(Ey1.a(Jd0.r0), "");
            Wg6.b(optString11, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
            Ry1 c2 = ai4.c(optString11);
            Ry1.Ai ai5 = Ry1.CREATOR;
            String optString12 = jSONObject.optString(Ey1.a(Jd0.s0), "");
            Wg6.b(optString12, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
            Ry1 c3 = ai5.c(optString12);
            Ai a3 = Ai.d.a((byte) jSONObject.optInt(Ey1.a(Jd0.s3), 0));
            if (a3 == null) {
                a3 = Ai.NO_REQUIRE;
            }
            Object[] array = arrayList.toArray(new Zm1[0]);
            if (array != null) {
                Ry1.Ai ai6 = Ry1.CREATOR;
                String optString13 = jSONObject.optString(Ey1.a(Jd0.v2), "");
                Wg6.b(optString13, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
                Ry1 c4 = ai6.c(optString13);
                String optString14 = jSONObject.optString(Ey1.a(Jd0.m3), "en_US");
                Wg6.b(optString14, "jsonObject.optString(JSO\u2026nt.DEFAULT_LOCALE_STRING)");
                Ry1.Ai ai7 = Ry1.CREATOR;
                String optString15 = jSONObject.optString(Ey1.a(Jd0.t3), "");
                Wg6.b(optString15, "jsonObject\n             \u2026ERSION.lowerCaseName, \"\")");
                Ry1 c5 = ai7.c(optString15);
                String optString16 = jSONObject.optString(Ey1.a(Jd0.u3), "");
                Wg6.b(optString16, "jsonObject.optString(JSO\u2026STRING.lowerCaseName, \"\")");
                return new Zk1(optString, optString2, optString5, optString6, optString7, optString8, optString9, c, c2, c3, linkedHashMap, linkedHashMap2, a3, (Zm1[]) array, c4, optString14, c5, optString16);
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public final Zk1 c(byte[] bArr) throws IllegalArgumentException {
            Ai a2;
            Zk1 zk1 = new Zk1("", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262136);
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            int i = 0;
            while (i <= bArr.length - 3) {
                short s = order.getShort(i);
                short p = Hy1.p(order.get(i + 2));
                Bi a3 = Bi.d.a(s);
                int i2 = i + 3;
                int i3 = i2 + p;
                if (bArr.length < i3) {
                    return zk1;
                }
                byte[] k = Dm7.k(bArr, i2, i3);
                switch (W60.a[a3.ordinal()]) {
                    case 1:
                        Charset forName = Charset.forName("US-ASCII");
                        Wg6.b(forName, "Charset.forName(\"US-ASCII\")");
                        String str = new String(k, forName);
                        if (d(str)) {
                            zk1 = Zk1.a(zk1, null, null, str, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262139);
                            break;
                        } else {
                            throw new IllegalArgumentException("Invalid Serial Number: " + str);
                        }
                    case 2:
                        Charset forName2 = Charset.forName("US-ASCII");
                        Wg6.b(forName2, "Charset.forName(\"US-ASCII\")");
                        zk1 = Zk1.a(zk1, null, null, null, new String(k, forName2), null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262135);
                        break;
                    case 3:
                        Charset forName3 = Charset.forName("US-ASCII");
                        Wg6.b(forName3, "Charset.forName(\"US-ASCII\")");
                        zk1 = Zk1.a(zk1, null, null, null, null, new String(k, forName3), null, null, null, null, null, null, null, null, null, null, null, null, null, 262127);
                        break;
                    case 4:
                        Charset forName4 = Charset.forName("US-ASCII");
                        Wg6.b(forName4, "Charset.forName(\"US-ASCII\")");
                        zk1 = Zk1.a(zk1, null, null, null, null, null, new String(k, forName4), null, null, null, null, null, null, null, null, null, null, null, null, 262111);
                        break;
                    case 5:
                        Charset forName5 = Charset.forName("US-ASCII");
                        Wg6.b(forName5, "Charset.forName(\"US-ASCII\")");
                        zk1 = Zk1.a(zk1, null, null, null, null, null, null, new String(k, forName5), null, null, null, null, null, null, null, null, null, null, null, 262079);
                        break;
                    case 6:
                        Ry1 b = Ry1.CREATOR.b(k);
                        if (b == null) {
                            break;
                        } else {
                            zk1 = Zk1.a(zk1, null, null, null, null, null, null, null, b, null, null, null, null, null, null, null, null, null, null, 262015);
                            break;
                        }
                    case 7:
                        Ry1 b2 = Ry1.CREATOR.b(k);
                        if (b2 == null) {
                            break;
                        } else {
                            zk1 = Zk1.a(zk1, null, null, null, null, null, null, null, null, b2, null, null, null, null, null, null, null, null, null, 261887);
                            break;
                        }
                    case 8:
                        Ry1 b3 = Ry1.CREATOR.b(k);
                        if (b3 == null) {
                            break;
                        } else {
                            zk1 = Zk1.a(zk1, null, null, null, null, null, null, null, null, null, b3, null, null, null, null, null, null, null, null, 261631);
                            break;
                        }
                    case 9:
                        Ry1 b4 = Ry1.CREATOR.b(k);
                        if (b4 == null) {
                            break;
                        } else {
                            zk1.c().put(Short.valueOf(Ob.s.b), b4);
                            break;
                        }
                    case 10:
                        Ur7 l = Bs7.l(Em7.G(k), 3);
                        int a4 = l.a();
                        int b5 = l.b();
                        int c = l.c();
                        if (c < 0) {
                            if (a4 < b5) {
                                break;
                            }
                        } else if (a4 > b5) {
                            break;
                        }
                        while (true) {
                            Ob a5 = Ob.A.a(k[a4]);
                            if (a5 != null) {
                                zk1.h().put(Short.valueOf(a5.b), new Ry1(k[a4 + 1], k[a4 + 2]));
                                Cd6 cd6 = Cd6.a;
                            }
                            if (a4 == b5) {
                                break;
                            } else {
                                a4 += c;
                            }
                        }
                    case 11:
                        Ur7 l2 = Bs7.l(Em7.G(k), 4);
                        int a6 = l2.a();
                        int b6 = l2.b();
                        int c2 = l2.c();
                        if (c2 < 0) {
                            if (a6 < b6) {
                                break;
                            }
                        } else if (a6 > b6) {
                            break;
                        }
                        while (true) {
                            ByteBuffer order2 = ByteBuffer.wrap(k).order(ByteOrder.LITTLE_ENDIAN);
                            zk1.c().put(Short.valueOf(order2.getShort(a6)), new Ry1(Hy1.p(order2.get(a6 + 2)), Hy1.p(order2.get(a6 + 3))));
                            if (a6 == b6) {
                                break;
                            } else {
                                a6 += c2;
                            }
                        }
                    case 12:
                        if ((!(k.length == 0)) && (a2 = Ai.d.a(k[0])) != null) {
                            zk1 = Zk1.a(zk1, null, null, null, null, null, null, null, null, null, null, null, null, a2, null, null, null, null, null, 258047);
                            Cd6 cd62 = Cd6.a;
                            break;
                        }
                    case 13:
                        ByteBuffer order3 = ByteBuffer.wrap(k).order(ByteOrder.LITTLE_ENDIAN);
                        ArrayList arrayList = new ArrayList();
                        Ur7 l3 = Bs7.l(Em7.G(k), 2);
                        int a7 = l3.a();
                        int b7 = l3.b();
                        int c3 = l3.c();
                        if (c3 < 0 ? a7 >= b7 : a7 <= b7) {
                            while (true) {
                                Zm1 b8 = Zm1.e.b(order3.getShort(a7));
                                if (b8 != null) {
                                    arrayList.add(b8);
                                }
                                if (a7 != b7) {
                                    a7 += c3;
                                }
                            }
                        }
                        Object[] array = arrayList.toArray(new Zm1[0]);
                        if (array != null) {
                            zk1 = Zk1.a(zk1, null, null, null, null, null, null, null, null, null, null, null, null, null, (Zm1[]) array, null, null, null, null, 253951);
                            break;
                        } else {
                            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                    case 14:
                        Ry1 b9 = Ry1.CREATOR.b(k);
                        if (b9 == null) {
                            break;
                        } else {
                            zk1 = Zk1.a(zk1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, b9, null, null, null, 245759);
                            break;
                        }
                    case 16:
                        Charset forName6 = Charset.forName("US-ASCII");
                        Wg6.b(forName6, "Charset.forName(\"US-ASCII\")");
                        zk1 = Zk1.a(zk1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, new String(k, forName6), null, null, 229375);
                        break;
                    case 17:
                        Ry1 b10 = Ry1.CREATOR.b(k);
                        if (b10 == null) {
                            break;
                        } else {
                            zk1.c().put((short) 1794, b10);
                            break;
                        }
                    case 18:
                        Ry1 b11 = Ry1.CREATOR.b(k);
                        if (b11 == null) {
                            break;
                        } else {
                            zk1 = Zk1.a(zk1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, b11, null, 196607);
                            break;
                        }
                }
                i = p + 3 + i;
            }
            return zk1;
        }

        @DexIgnore
        public final boolean d(String str) {
            if (str.length() != 10) {
                return false;
            }
            for (int i = 0; i < str.length(); i++) {
                if (!Character.isLetterOrDigit(str.charAt(i))) {
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    public Zk1(String str, String str2, String str3, String str4, String str5, String str6, String str7, Ry1 ry1, Ry1 ry12, Ry1 ry13, LinkedHashMap<Short, Ry1> linkedHashMap, LinkedHashMap<Short, Ry1> linkedHashMap2, Ai ai, Zm1[] zm1Arr, Ry1 ry14, String str8, Ry1 ry15, String str9) {
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = str4;
        this.g = str5;
        this.h = str6;
        this.i = str7;
        this.j = ry1;
        this.k = ry12;
        this.l = ry13;
        this.m = linkedHashMap;
        this.n = linkedHashMap2;
        this.o = ai;
        this.p = zm1Arr;
        this.q = ry14;
        this.r = str8;
        this.s = ry15;
        this.t = str9;
        this.b = Al1.Companion.a(str6, str3);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Zk1(String str, String str2, String str3, String str4, String str5, String str6, String str7, Ry1 ry1, Ry1 ry12, Ry1 ry13, LinkedHashMap linkedHashMap, LinkedHashMap linkedHashMap2, Ai ai, Zm1[] zm1Arr, Ry1 ry14, String str8, Ry1 ry15, String str9, int i2) {
        this(str, str2, str3, (i2 & 8) != 0 ? "" : str4, (i2 & 16) != 0 ? "" : str5, (i2 & 32) != 0 ? "" : str6, (i2 & 64) != 0 ? "" : str7, (i2 & 128) != 0 ? new Ry1(0, 0) : ry1, (i2 & 256) != 0 ? new Ry1(0, 0) : ry12, (i2 & 512) != 0 ? new Ry1(0, 0) : ry13, (i2 & 1024) != 0 ? new LinkedHashMap() : linkedHashMap, (i2 & 2048) != 0 ? new LinkedHashMap() : linkedHashMap2, (i2 & 4096) != 0 ? Ai.NO_REQUIRE : ai, (i2 & 8192) != 0 ? new Zm1[0] : zm1Arr, (i2 & 16384) != 0 ? new Ry1(0, 0) : ry14, (32768 & i2) != 0 ? "en_US" : str8, (65536 & i2) != 0 ? new Ry1(0, 0) : ry15, (131072 & i2) != 0 ? "" : str9);
    }

    @DexIgnore
    public static /* synthetic */ Zk1 a(Zk1 zk1, String str, String str2, String str3, String str4, String str5, String str6, String str7, Ry1 ry1, Ry1 ry12, Ry1 ry13, LinkedHashMap linkedHashMap, LinkedHashMap linkedHashMap2, Ai ai, Zm1[] zm1Arr, Ry1 ry14, String str8, Ry1 ry15, String str9, int i2) {
        return zk1.a((i2 & 1) != 0 ? zk1.c : str, (i2 & 2) != 0 ? zk1.d : str2, (i2 & 4) != 0 ? zk1.e : str3, (i2 & 8) != 0 ? zk1.f : str4, (i2 & 16) != 0 ? zk1.g : str5, (i2 & 32) != 0 ? zk1.h : str6, (i2 & 64) != 0 ? zk1.i : str7, (i2 & 128) != 0 ? zk1.j : ry1, (i2 & 256) != 0 ? zk1.k : ry12, (i2 & 512) != 0 ? zk1.l : ry13, (i2 & 1024) != 0 ? zk1.m : linkedHashMap, (i2 & 2048) != 0 ? zk1.n : linkedHashMap2, (i2 & 4096) != 0 ? zk1.o : ai, (i2 & 8192) != 0 ? zk1.p : zm1Arr, (i2 & 16384) != 0 ? zk1.q : ry14, (32768 & i2) != 0 ? zk1.r : str8, (65536 & i2) != 0 ? zk1.s : ry15, (131072 & i2) != 0 ? zk1.t : str9);
    }

    @DexIgnore
    public final Ai a() {
        return this.o;
    }

    @DexIgnore
    public final Zk1 a(String str, String str2, String str3, String str4, String str5, String str6, String str7, Ry1 ry1, Ry1 ry12, Ry1 ry13, LinkedHashMap<Short, Ry1> linkedHashMap, LinkedHashMap<Short, Ry1> linkedHashMap2, Ai ai, Zm1[] zm1Arr, Ry1 ry14, String str8, Ry1 ry15, String str9) {
        return new Zk1(str, str2, str3, str4, str5, str6, str7, ry1, ry12, ry13, linkedHashMap, linkedHashMap2, ai, zm1Arr, ry14, str8, ry15, str9);
    }

    @DexIgnore
    public final JSONArray a(HashMap<Short, Ry1> hashMap) {
        JSONArray jSONArray = new JSONArray();
        for (Map.Entry<Short, Ry1> entry : hashMap.entrySet()) {
            jSONArray.put(G80.k(G80.k(G80.k(new JSONObject(), Jd0.A0, Hy1.l(entry.getKey().shortValue(), null, 1, null)), Jd0.g4, Kb.e.a(entry.getKey().shortValue())), Jd0.K2, entry.getValue().toString()));
        }
        return jSONArray;
    }

    @DexIgnore
    public final void a(Al1 al1) {
        this.b = al1;
    }

    @DexIgnore
    public final Ry1 b() {
        return this.j;
    }

    @DexIgnore
    public final LinkedHashMap<Short, Ry1> c() {
        return this.n;
    }

    @DexIgnore
    public final Ry1 d() {
        return this.q;
    }

    @DexIgnore
    public final Ry1 e() {
        return this.l;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Zk1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Zk1 zk1 = (Zk1) obj;
            return !(Wg6.a(this.c, zk1.c) ^ true) && !(Wg6.a(this.d, zk1.d) ^ true) && !(Wg6.a(this.e, zk1.e) ^ true) && !(Wg6.a(this.f, zk1.f) ^ true) && !(Wg6.a(this.g, zk1.g) ^ true) && !(Wg6.a(this.h, zk1.h) ^ true) && !(Wg6.a(this.i, zk1.i) ^ true) && !(Wg6.a(this.j, zk1.j) ^ true) && !(Wg6.a(this.k, zk1.k) ^ true) && !(Wg6.a(this.l, zk1.l) ^ true) && !(Wg6.a(this.m, zk1.m) ^ true) && !(Wg6.a(this.n, zk1.n) ^ true) && this.o == zk1.o && Arrays.equals(this.p, zk1.p) && !(Wg6.a(this.q, zk1.q) ^ true) && !(Wg6.a(this.r, zk1.r) ^ true) && !(Wg6.a(this.s, zk1.s) ^ true) && !(Wg6.a(this.t, zk1.t) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.DeviceInformation");
    }

    @DexIgnore
    public final String f() {
        return this.i;
    }

    @DexIgnore
    public final Zm1[] g() {
        return this.p;
    }

    @DexIgnore
    public final Al1 getDeviceType() {
        return this.b;
    }

    @DexIgnore
    public final Dl1 getELabelVersions() {
        Ry1 ry1 = this.n.get((short) 1797);
        Ry1 d2 = ry1 != null ? ry1 : Hd0.y.d();
        Ry1 ry12 = this.m.get((short) 1797);
        if (ry12 == null) {
            ry12 = Hd0.y.d();
        }
        return new Dl1(d2, ry12);
    }

    @DexIgnore
    public final String getFastPairIdInHexString() {
        return this.t;
    }

    @DexIgnore
    public final String getFirmwareVersion() {
        return this.g;
    }

    @DexIgnore
    public final String getHardwareRevision() {
        return this.f;
    }

    @DexIgnore
    public final String getLocaleString() {
        return this.r;
    }

    @DexIgnore
    public final Dl1 getLocaleVersions() {
        Ry1 ry1 = this.n.get((short) 1794);
        Ry1 d2 = ry1 != null ? ry1 : Hd0.y.d();
        Ry1 ry12 = this.m.get(Short.valueOf(Ob.k.b));
        if (ry12 == null) {
            ry12 = Hd0.y.d();
        }
        return new Dl1(d2, ry12);
    }

    @DexIgnore
    public final String getMacAddress() {
        return this.d;
    }

    @DexIgnore
    public final Ry1 getMicroAppVersion() {
        return this.s;
    }

    @DexIgnore
    public final String getModelNumber() {
        return this.h;
    }

    @DexIgnore
    public final String getName() {
        return this.c;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.e;
    }

    @DexIgnore
    public final Ry1 getUiPackageOSVersion() {
        Ry1 ry1 = this.m.get(Short.valueOf(Ob.x.b));
        return ry1 != null ? ry1 : new Ry1(0, 0);
    }

    @DexIgnore
    public final Dl1 getWatchParameterVersions() {
        Ry1 ry1 = this.n.get(Short.valueOf(Ob.r.b));
        Ry1 d2 = ry1 != null ? ry1 : Hd0.y.d();
        Ry1 ry12 = this.m.get(Short.valueOf(Ob.r.b));
        if (ry12 == null) {
            ry12 = Hd0.y.d();
        }
        return new Dl1(d2, ry12);
    }

    @DexIgnore
    public final LinkedHashMap<Short, Ry1> h() {
        return this.m;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        int hashCode4 = this.f.hashCode();
        int hashCode5 = this.g.hashCode();
        int hashCode6 = this.h.hashCode();
        int hashCode7 = this.i.hashCode();
        int hashCode8 = this.j.hashCode();
        int hashCode9 = this.k.hashCode();
        int hashCode10 = this.l.hashCode();
        int hashCode11 = this.m.hashCode();
        int hashCode12 = this.n.hashCode();
        int hashCode13 = this.o.hashCode();
        int hashCode14 = Arrays.hashCode(this.p);
        int hashCode15 = this.q.hashCode();
        int hashCode16 = this.r.hashCode();
        return (((((((((((((((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + hashCode10) * 31) + hashCode11) * 31) + hashCode12) * 31) + hashCode13) * 31) + hashCode14) * 31) + hashCode15) * 31) + hashCode16) * 31) + this.s.hashCode()) * 31) + this.t.hashCode();
    }

    @DexIgnore
    public final Ry1 i() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        boolean z = true;
        JSONObject k2 = G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(new JSONObject(), Jd0.H, this.c), Jd0.L2, Ey1.a(this.b)), Jd0.k0, this.d), Jd0.l0, this.e), Jd0.m0, this.f), Jd0.n0, this.g), Jd0.o0, this.h), Jd0.p0, this.i), Jd0.q0, this.j.getShortDescription()), Jd0.r0, this.k.getShortDescription()), Jd0.s0, this.l.getShortDescription()), Jd0.I2, a(this.m)), Jd0.A4, a(this.n));
        Jd0 jd0 = Jd0.s3;
        int i2 = O70.a[this.o.ordinal()];
        if (i2 == 1) {
            z = false;
        } else if (i2 != 2) {
            throw new Kc6();
        }
        return G80.k(G80.k(G80.k(G80.k(G80.k(G80.k(k2, jd0, Boolean.valueOf(z)), Jd0.t0, G80.h(this.p)), Jd0.v2, this.q.getShortDescription()), Jd0.m3, this.r), Jd0.t3, this.s.getShortDescription()), Jd0.u3, this.t);
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public String toString() {
        StringBuilder e2 = E.e("DeviceInformation(name=");
        e2.append(this.c);
        e2.append(", macAddress=");
        e2.append(this.d);
        e2.append(", serialNumber=");
        e2.append(this.e);
        e2.append(", hardwareRevision=");
        e2.append(this.f);
        e2.append(", firmwareVersion=");
        e2.append(this.g);
        e2.append(", modelNumber=");
        e2.append(this.h);
        e2.append(", heartRateSerialNumber=");
        e2.append(this.i);
        e2.append(", bootloaderVersion=");
        e2.append(this.j);
        e2.append(", watchAppVersion=");
        e2.append(this.k);
        e2.append(", fontVersion=");
        e2.append(this.l);
        e2.append(", supportedFilesVersion=");
        e2.append(this.m);
        e2.append(", currentFilesVersion=");
        e2.append(this.n);
        e2.append(", bondRequired=");
        e2.append(this.o);
        e2.append(", supportedDeviceConfigKeys=");
        e2.append(Arrays.toString(this.p));
        e2.append(", deviceSecurityVersion=");
        e2.append(this.q);
        e2.append(", localeString=");
        e2.append(this.r);
        e2.append(", microAppVersion=");
        e2.append(this.s);
        e2.append(", fastPairIdInHexString=");
        e2.append(this.t);
        e2.append(")");
        return e2.toString();
    }
}
