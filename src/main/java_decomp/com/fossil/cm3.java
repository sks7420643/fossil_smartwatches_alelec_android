package com.fossil;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cm3 {
    @DexIgnore
    public /* final */ Pm3 a;

    @DexIgnore
    public Cm3(Pm3 pm3) {
        this.a = pm3;
    }

    @DexIgnore
    public final Bundle a(String str, Bw2 bw2) {
        this.a.c().h();
        if (bw2 == null) {
            this.a.d().I().a("Attempting to use Install Referrer Service while it is not initialized");
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString("package_name", str);
        try {
            Bundle c = bw2.c(bundle);
            if (c != null) {
                return c;
            }
            this.a.d().F().a("Install Referrer Service returned a null response");
            return null;
        } catch (Exception e) {
            this.a.d().F().b("Exception occurred while retrieving the Install Referrer", e.getMessage());
            return null;
        }
    }

    @DexIgnore
    public final void b(String str) {
        if (str == null || str.isEmpty()) {
            this.a.d().J().a("Install Referrer Reporter was called with invalid app package name");
            return;
        }
        this.a.c().h();
        if (!c()) {
            this.a.d().L().a("Install Referrer Reporter is not available");
            return;
        }
        Fm3 fm3 = new Fm3(this, str);
        this.a.c().h();
        Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
        intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
        PackageManager packageManager = this.a.e().getPackageManager();
        if (packageManager == null) {
            this.a.d().J().a("Failed to obtain Package Manager to verify binding conditions for Install Referrer");
            return;
        }
        List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent, 0);
        if (queryIntentServices == null || queryIntentServices.isEmpty()) {
            this.a.d().L().a("Play Service for fetching Install Referrer is unavailable on device");
            return;
        }
        ServiceInfo serviceInfo = queryIntentServices.get(0).serviceInfo;
        if (serviceInfo != null) {
            String str2 = serviceInfo.packageName;
            if (serviceInfo.name == null || !"com.android.vending".equals(str2) || !c()) {
                this.a.d().I().a("Play Store version 8.3.73 or higher required for Install Referrer");
                return;
            }
            try {
                this.a.d().N().b("Install Referrer Service is", Ve2.b().a(this.a.e(), new Intent(intent), fm3, 1) ? "available" : "not available");
            } catch (Exception e) {
                this.a.d().F().b("Exception occurred while binding to Install Referrer Service", e.getMessage());
            }
        }
    }

    @DexIgnore
    public final boolean c() {
        try {
            Zf2 a2 = Ag2.a(this.a.e());
            if (a2 != null) {
                return a2.e("com.android.vending", 128).versionCode >= 80837300;
            }
            this.a.d().N().a("Failed to get PackageManager for Install Referrer Play Store compatibility check");
            return false;
        } catch (Exception e) {
            this.a.d().N().b("Failed to retrieve Play Store version for Install Referrer", e);
            return false;
        }
    }
}
