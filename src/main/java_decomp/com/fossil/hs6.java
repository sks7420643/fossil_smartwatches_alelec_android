package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeButtonViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hs6 implements Factory<CustomizeButtonViewModel> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public Hs6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Hs6 a(Provider<ThemeRepository> provider) {
        return new Hs6(provider);
    }

    @DexIgnore
    public static CustomizeButtonViewModel c(ThemeRepository themeRepository) {
        return new CustomizeButtonViewModel(themeRepository);
    }

    @DexIgnore
    public CustomizeButtonViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
