package com.fossil;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ String d;
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ boolean f;
    @DexIgnore
    public /* final */ /* synthetic */ Or3 g;
    @DexIgnore
    public /* final */ /* synthetic */ Fp3 h;

    @DexIgnore
    public Yp3(Fp3 fp3, AtomicReference atomicReference, String str, String str2, String str3, boolean z, Or3 or3) {
        this.h = fp3;
        this.b = atomicReference;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = z;
        this.g = or3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.b) {
            try {
                Cl3 cl3 = this.h.d;
                if (cl3 == null) {
                    this.h.d().F().d("(legacy) Failed to get user properties; not connected to service", Kl3.w(this.c), this.d, this.e);
                    this.b.set(Collections.emptyList());
                    return;
                }
                if (TextUtils.isEmpty(this.c)) {
                    this.b.set(cl3.o1(this.d, this.e, this.f, this.g));
                } else {
                    this.b.set(cl3.S(this.c, this.d, this.e, this.f));
                }
                this.h.e0();
                this.b.notify();
            } catch (RemoteException e2) {
                this.h.d().F().d("(legacy) Failed to get user properties; remote exception", Kl3.w(this.c), this.d, e2);
                this.b.set(Collections.emptyList());
            } finally {
                this.b.notify();
            }
        }
    }
}
