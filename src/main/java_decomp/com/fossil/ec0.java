package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Ec0 {
    c("static"),
    d("complications"),
    e("goal_rings");
    
    @DexIgnore
    public static /* final */ Dc0 g; // = new Dc0(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public Ec0(String str) {
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }
}
