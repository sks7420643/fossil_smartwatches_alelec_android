package com.fossil;

import com.fossil.E13;
import com.fossil.Hu2;
import com.fossil.Ku2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gu2 extends E13<Gu2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Gu2 zzi;
    @DexIgnore
    public static volatile Z23<Gu2> zzj;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public M13<Ku2> zze; // = E13.B();
    @DexIgnore
    public M13<Hu2> zzf; // = E13.B();
    @DexIgnore
    public boolean zzg;
    @DexIgnore
    public boolean zzh;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Gu2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Gu2.zzi);
        }

        @DexIgnore
        public /* synthetic */ Ai(Fu2 fu2) {
            this();
        }

        @DexIgnore
        public final Ku2 B(int i) {
            return ((Gu2) this.c).C(i);
        }

        @DexIgnore
        public final int C() {
            return ((Gu2) this.c).O();
        }

        @DexIgnore
        public final Hu2 E(int i) {
            return ((Gu2) this.c).K(i);
        }

        @DexIgnore
        public final int x() {
            return ((Gu2) this.c).M();
        }

        @DexIgnore
        public final Ai y(int i, Hu2.Ai ai) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Gu2) this.c).D(i, (Hu2) ((E13) ai.h()));
            return this;
        }

        @DexIgnore
        public final Ai z(int i, Ku2.Ai ai) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Gu2) this.c).E(i, (Ku2) ((E13) ai.h()));
            return this;
        }
    }

    /*
    static {
        Gu2 gu2 = new Gu2();
        zzi = gu2;
        E13.u(Gu2.class, gu2);
    }
    */

    @DexIgnore
    public final Ku2 C(int i) {
        return this.zze.get(i);
    }

    @DexIgnore
    public final void D(int i, Hu2 hu2) {
        hu2.getClass();
        M13<Hu2> m13 = this.zzf;
        if (!m13.zza()) {
            this.zzf = E13.q(m13);
        }
        this.zzf.set(i, hu2);
    }

    @DexIgnore
    public final void E(int i, Ku2 ku2) {
        ku2.getClass();
        M13<Ku2> m13 = this.zze;
        if (!m13.zza()) {
            this.zze = E13.q(m13);
        }
        this.zze.set(i, ku2);
    }

    @DexIgnore
    public final boolean I() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int J() {
        return this.zzd;
    }

    @DexIgnore
    public final Hu2 K(int i) {
        return this.zzf.get(i);
    }

    @DexIgnore
    public final List<Ku2> L() {
        return this.zze;
    }

    @DexIgnore
    public final int M() {
        return this.zze.size();
    }

    @DexIgnore
    public final List<Hu2> N() {
        return this.zzf;
    }

    @DexIgnore
    public final int O() {
        return this.zzf.size();
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Fu2.a[i - 1]) {
            case 1:
                return new Gu2();
            case 2:
                return new Ai(null);
            case 3:
                return E13.s(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0002\u0000\u0001\u1004\u0000\u0002\u001b\u0003\u001b\u0004\u1007\u0001\u0005\u1007\u0002", new Object[]{"zzc", "zzd", "zze", Ku2.class, "zzf", Hu2.class, "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                Z23<Gu2> z232 = zzj;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Gu2.class) {
                    try {
                        z23 = zzj;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzi);
                            zzj = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
