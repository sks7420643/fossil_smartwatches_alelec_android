package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I14 {
    @DexIgnore
    public static String a(int i, int i2, String str) {
        if (i < 0) {
            return w("%s (%s) must not be negative", str, Integer.valueOf(i));
        } else if (i2 >= 0) {
            return w("%s (%s) must be less than size (%s)", str, Integer.valueOf(i), Integer.valueOf(i2));
        } else {
            throw new IllegalArgumentException("negative size: " + i2);
        }
    }

    @DexIgnore
    public static String b(int i, int i2, String str) {
        if (i < 0) {
            return w("%s (%s) must not be negative", str, Integer.valueOf(i));
        } else if (i2 >= 0) {
            return w("%s (%s) must not be greater than size (%s)", str, Integer.valueOf(i), Integer.valueOf(i2));
        } else {
            throw new IllegalArgumentException("negative size: " + i2);
        }
    }

    @DexIgnore
    public static String c(int i, int i2, int i3) {
        if (i < 0 || i > i3) {
            return b(i, i3, "start index");
        }
        if (i2 < 0 || i2 > i3) {
            return b(i2, i3, "end index");
        }
        return w("end index (%s) must not be less than start index (%s)", Integer.valueOf(i2), Integer.valueOf(i));
    }

    @DexIgnore
    public static void d(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
    }

    @DexIgnore
    public static void e(boolean z, Object obj) {
        if (!z) {
            throw new IllegalArgumentException(String.valueOf(obj));
        }
    }

    @DexIgnore
    public static void f(boolean z, String str, int i) {
        if (!z) {
            throw new IllegalArgumentException(w(str, Integer.valueOf(i)));
        }
    }

    @DexIgnore
    public static void g(boolean z, String str, long j) {
        if (!z) {
            throw new IllegalArgumentException(w(str, Long.valueOf(j)));
        }
    }

    @DexIgnore
    public static void h(boolean z, String str, Object obj) {
        if (!z) {
            throw new IllegalArgumentException(w(str, obj));
        }
    }

    @DexIgnore
    public static void i(boolean z, String str, Object obj, Object obj2) {
        if (!z) {
            throw new IllegalArgumentException(w(str, obj, obj2));
        }
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static int j(int i, int i2) {
        k(i, i2, "index");
        return i;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static int k(int i, int i2, String str) {
        if (i >= 0 && i < i2) {
            return i;
        }
        throw new IndexOutOfBoundsException(a(i, i2, str));
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> T l(T t) {
        if (t != null) {
            return t;
        }
        throw null;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> T m(T t, Object obj) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(String.valueOf(obj));
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> T n(T t, String str, Object obj, Object obj2) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(w(str, obj, obj2));
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> T o(T t, String str, Object... objArr) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(w(str, objArr));
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static int p(int i, int i2) {
        q(i, i2, "index");
        return i;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static int q(int i, int i2, String str) {
        if (i >= 0 && i <= i2) {
            return i;
        }
        throw new IndexOutOfBoundsException(b(i, i2, str));
    }

    @DexIgnore
    public static void r(int i, int i2, int i3) {
        if (i < 0 || i2 < i || i2 > i3) {
            throw new IndexOutOfBoundsException(c(i, i2, i3));
        }
    }

    @DexIgnore
    public static void s(boolean z) {
        if (!z) {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public static void t(boolean z, Object obj) {
        if (!z) {
            throw new IllegalStateException(String.valueOf(obj));
        }
    }

    @DexIgnore
    public static void u(boolean z, String str, int i) {
        if (!z) {
            throw new IllegalStateException(w(str, Integer.valueOf(i)));
        }
    }

    @DexIgnore
    public static void v(boolean z, String str, Object obj) {
        if (!z) {
            throw new IllegalStateException(w(str, obj));
        }
    }

    @DexIgnore
    public static String w(String str, Object... objArr) {
        int indexOf;
        int i = 0;
        String valueOf = String.valueOf(str);
        StringBuilder sb = new StringBuilder(valueOf.length() + (objArr.length * 16));
        int i2 = 0;
        while (i < objArr.length && (indexOf = valueOf.indexOf("%s", i2)) != -1) {
            sb.append((CharSequence) valueOf, i2, indexOf);
            sb.append(objArr[i]);
            i2 = indexOf + 2;
            i++;
        }
        sb.append((CharSequence) valueOf, i2, valueOf.length());
        if (i < objArr.length) {
            sb.append(" [");
            sb.append(objArr[i]);
            for (int i3 = i + 1; i3 < objArr.length; i3++) {
                sb.append(", ");
                sb.append(objArr[i3]);
            }
            sb.append(']');
        }
        return sb.toString();
    }
}
