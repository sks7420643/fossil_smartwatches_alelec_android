package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ij extends Bi {
    @DexIgnore
    public Zk1 V;

    @DexIgnore
    public Ij(K5 k5, I60 i60, short s, String str) {
        super(k5, i60, Yp.d, s, false, Zm7.i(Hl7.a(Hu1.SKIP_ERASE, Boolean.TRUE), Hl7.a(Hu1.NUMBER_OF_FILE_REQUIRED, 1), Hl7.a(Hu1.ERASE_CACHE_FILE_BEFORE_GET, Boolean.TRUE)), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 80);
        this.V = new Zk1(k5.C(), k5.x, "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
    }

    @DexIgnore
    @Override // com.fossil.Bi, com.fossil.Lp
    public JSONObject E() {
        return G80.k(super.E(), Jd0.b, this.V.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Bi
    public void M(ArrayList<J0> arrayList) {
        l(this.v);
    }

    @DexIgnore
    @Override // com.fossil.Bi
    public void Q(J0 j0) {
        Zq zq;
        super.Q(j0);
        try {
            this.V = Zk1.a((Zk1) Eb.d.f(j0.f), this.V.getName(), this.V.getMacAddress(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262140);
            zq = Zq.b;
        } catch (Sx1 e) {
            D90.i.i(e);
            zq = Zq.q;
        }
        this.v = Nr.a(this.v, null, zq, null, null, 13);
    }

    @DexIgnore
    @Override // com.fossil.Bi, com.fossil.Lp
    public Object x() {
        return this.V;
    }
}
