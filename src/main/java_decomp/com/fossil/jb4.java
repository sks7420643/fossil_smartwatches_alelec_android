package com.fossil;

import com.fossil.Q18;
import com.fossil.S18;
import com.fossil.V18;
import com.fossil.Z08;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Jb4 {
    @DexIgnore
    public static /* final */ OkHttpClient f;
    @DexIgnore
    public /* final */ Ib4 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Map<String, String> c;
    @DexIgnore
    public /* final */ Map<String, String> d;
    @DexIgnore
    public S18.Ai e; // = null;

    /*
    static {
        OkHttpClient.b y = new OkHttpClient().y();
        y.f(ButtonService.CONNECT_TIMEOUT, TimeUnit.MILLISECONDS);
        f = y.d();
    }
    */

    @DexIgnore
    public Jb4(Ib4 ib4, String str, Map<String, String> map) {
        this.a = ib4;
        this.b = str;
        this.c = map;
        this.d = new HashMap();
    }

    @DexIgnore
    public final V18 a() {
        V18.Ai ai = new V18.Ai();
        Z08.Ai ai2 = new Z08.Ai();
        ai2.c();
        ai.c(ai2.a());
        Q18.Ai p = Q18.r(this.b).p();
        for (Map.Entry<String, String> entry : this.c.entrySet()) {
            p.a(entry.getKey(), entry.getValue());
        }
        ai.l(p.c());
        for (Map.Entry<String, String> entry2 : this.d.entrySet()) {
            ai.e(entry2.getKey(), entry2.getValue());
        }
        S18.Ai ai3 = this.e;
        ai.g(this.a.name(), ai3 == null ? null : ai3.e());
        return ai.b();
    }

    @DexIgnore
    public Lb4 b() throws IOException {
        return Lb4.c(f.d(a()).a());
    }

    @DexIgnore
    public final S18.Ai c() {
        if (this.e == null) {
            S18.Ai ai = new S18.Ai();
            ai.f(S18.f);
            this.e = ai;
        }
        return this.e;
    }

    @DexIgnore
    public Jb4 d(String str, String str2) {
        this.d.put(str, str2);
        return this;
    }

    @DexIgnore
    public Jb4 e(Map.Entry<String, String> entry) {
        d(entry.getKey(), entry.getValue());
        return this;
    }

    @DexIgnore
    public String f() {
        return this.a.name();
    }

    @DexIgnore
    public Jb4 g(String str, String str2) {
        S18.Ai c2 = c();
        c2.a(str, str2);
        this.e = c2;
        return this;
    }

    @DexIgnore
    public Jb4 h(String str, String str2, String str3, File file) {
        RequestBody c2 = RequestBody.c(R18.d(str3), file);
        S18.Ai c3 = c();
        c3.b(str, str2, c2);
        this.e = c3;
        return this;
    }
}
