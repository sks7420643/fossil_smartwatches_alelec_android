package com.fossil;

import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ge7 extends Ee7 {
    @DexIgnore
    public /* final */ Be7 a;
    @DexIgnore
    public /* final */ MethodChannel.Result b;
    @DexIgnore
    public /* final */ Boolean c;

    @DexIgnore
    public Ge7(MethodChannel.Result result, Be7 be7, Boolean bool) {
        this.b = result;
        this.a = be7;
        this.c = bool;
    }

    @DexIgnore
    @Override // com.fossil.Ie7
    public <T> T a(String str) {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Ee7, com.fossil.Ie7
    public Be7 b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Ee7, com.fossil.Ie7
    public Boolean d() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Je7
    public void error(String str, String str2, Object obj) {
        this.b.error(str, str2, obj);
    }

    @DexIgnore
    @Override // com.fossil.Je7
    public void success(Object obj) {
        this.b.success(obj);
    }
}
