package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class D14 {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends D14 {
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(D14 d14, String str) {
            super(d14, null);
            this.b = str;
        }

        @DexIgnore
        @Override // com.fossil.D14
        public CharSequence j(Object obj) {
            return obj == null ? this.b : D14.this.j(obj);
        }

        @DexIgnore
        @Override // com.fossil.D14
        public D14 k(String str) {
            throw new UnsupportedOperationException("already specified useForNull");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ D14 a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public Bi(D14 d14, String str) {
            this.a = d14;
            I14.l(str);
            this.b = str;
        }

        @DexIgnore
        public /* synthetic */ Bi(D14 d14, String str, Ai ai) {
            this(d14, str);
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public <A extends Appendable> A a(A a2, Iterator<? extends Map.Entry<?, ?>> it) throws IOException {
            I14.l(a2);
            if (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                a2.append(this.a.j(entry.getKey()));
                a2.append(this.b);
                a2.append(this.a.j(entry.getValue()));
                while (it.hasNext()) {
                    a2.append(this.a.a);
                    Map.Entry entry2 = (Map.Entry) it.next();
                    a2.append(this.a.j(entry2.getKey()));
                    a2.append(this.b);
                    a2.append(this.a.j(entry2.getValue()));
                }
            }
            return a2;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public StringBuilder b(StringBuilder sb, Iterable<? extends Map.Entry<?, ?>> iterable) {
            c(sb, iterable.iterator());
            return sb;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public StringBuilder c(StringBuilder sb, Iterator<? extends Map.Entry<?, ?>> it) {
            try {
                a(sb, it);
                return sb;
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public StringBuilder d(StringBuilder sb, Map<?, ?> map) {
            b(sb, map.entrySet());
            return sb;
        }
    }

    @DexIgnore
    public D14(D14 d14) {
        this.a = d14.a;
    }

    @DexIgnore
    public /* synthetic */ D14(D14 d14, Ai ai) {
        this(d14);
    }

    @DexIgnore
    public D14(String str) {
        I14.l(str);
        this.a = str;
    }

    @DexIgnore
    public static D14 h(char c) {
        return new D14(String.valueOf(c));
    }

    @DexIgnore
    public static D14 i(String str) {
        return new D14(str);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <A extends Appendable> A b(A a2, Iterator<?> it) throws IOException {
        I14.l(a2);
        if (it.hasNext()) {
            a2.append(j(it.next()));
            while (it.hasNext()) {
                a2.append(this.a);
                a2.append(j(it.next()));
            }
        }
        return a2;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public final StringBuilder c(StringBuilder sb, Iterable<?> iterable) {
        d(sb, iterable.iterator());
        return sb;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public final StringBuilder d(StringBuilder sb, Iterator<?> it) {
        try {
            b(sb, it);
            return sb;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    @DexIgnore
    public final String e(Iterable<?> iterable) {
        return f(iterable.iterator());
    }

    @DexIgnore
    public final String f(Iterator<?> it) {
        StringBuilder sb = new StringBuilder();
        d(sb, it);
        return sb.toString();
    }

    @DexIgnore
    public final String g(Object[] objArr) {
        return e(Arrays.asList(objArr));
    }

    @DexIgnore
    public CharSequence j(Object obj) {
        I14.l(obj);
        return obj instanceof CharSequence ? (CharSequence) obj : obj.toString();
    }

    @DexIgnore
    public D14 k(String str) {
        I14.l(str);
        return new Ai(this, str);
    }

    @DexIgnore
    public Bi l(String str) {
        return new Bi(this, str, null);
    }
}
