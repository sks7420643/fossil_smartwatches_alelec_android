package com.fossil;

import android.content.Context;
import android.text.SpannableString;
import android.text.format.DateFormat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.iq4;
import com.fossil.jn5;
import com.fossil.tq4;
import com.fossil.u06;
import com.fossil.v36;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f06 extends zz5 {
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ a x; // = new a(null);
    @DexIgnore
    public LiveData<String> e; // = PortfolioApp.h0.c().K();
    @DexIgnore
    public /* final */ LiveData<List<DNDScheduledTimeModel>> f; // = this.v.getDNDScheduledTimeDao().getListDNDScheduledTime();
    @DexIgnore
    public ArrayList<Alarm> g; // = new ArrayList<>();
    @DexIgnore
    public boolean h;
    @DexIgnore
    public volatile boolean i;
    @DexIgnore
    public List<i06> j; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> k; // = new ArrayList();
    @DexIgnore
    public /* final */ a06 l;
    @DexIgnore
    public /* final */ uq4 m;
    @DexIgnore
    public /* final */ bk5 n;
    @DexIgnore
    public /* final */ v36 o;
    @DexIgnore
    public /* final */ d26 p;
    @DexIgnore
    public /* final */ u06 q;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase r;
    @DexIgnore
    public /* final */ yx5 s;
    @DexIgnore
    public /* final */ AlarmsRepository t;
    @DexIgnore
    public /* final */ on5 u;
    @DexIgnore
    public /* final */ DNDSettingsDatabase v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return f06.w;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.e<yx5.d, yx5.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f06 f1016a;
        @DexIgnore
        public /* final */ /* synthetic */ Alarm b;

        @DexIgnore
        public b(f06 f06, Alarm alarm) {
            this.f1016a = f06;
            this.b = alarm;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(yx5.b bVar) {
            pq7.c(bVar, "errorValue");
            this.f1016a.l.a();
            int c = bVar.c();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = f06.x.a();
            local.d(a2, "enableAlarm() - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.f1016a.l.c();
                } else if (!(c == 1112 || c == 1113)) {
                    this.f1016a.l.v0();
                }
                this.f1016a.N(bVar.a(), false);
                return;
            }
            List<uh5> convertBLEPermissionErrorCode = uh5.convertBLEPermissionErrorCode(bVar.b());
            pq7.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            a06 a06 = this.f1016a.l;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh5[0]);
            if (array != null) {
                uh5[] uh5Arr = (uh5[]) array;
                a06.M((uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                this.f1016a.N(bVar.a(), false);
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(yx5.d dVar) {
            pq7.c(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = f06.x.a();
            local.d(a2, "enableAlarm - onSuccess: alarmUri = " + dVar.a().getUri() + ", alarmId = " + dVar.a().getId());
            this.f1016a.l.a();
            this.f1016a.N(this.b, true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements tq4.d<u06.b, tq4.a> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f06 f1017a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(f06 f06) {
            this.f1017a = f06;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(tq4.a aVar) {
            FLogger.INSTANCE.getLocal().d(f06.x.a(), ".Inside mSaveAppsNotification onError");
            String c = um5.c(PortfolioApp.h0.c(), 2131886996);
            a06 a06 = this.f1017a.l;
            pq7.b(c, "notificationAppOverView");
            a06.o1(c);
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(u06.b bVar) {
            FLogger.INSTANCE.getLocal().d(f06.x.a(), ".Inside mSaveAppsNotification onSuccess");
            String c = um5.c(PortfolioApp.h0.c(), 2131886503);
            a06 a06 = this.f1017a.l;
            pq7.b(c, "notificationAppOverView");
            a06.o1(c);
            this.f1017a.U();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1", f = "HomeAlertsPresenter.kt", l = {435, 437}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ f06 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1", f = "HomeAlertsPresenter.kt", l = {356, 362, 371, 379}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends AppNotificationFilter>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.f06$d$a$a")
            @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.f06$d$a$a  reason: collision with other inner class name */
            public static final class C0073a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $notificationSettings;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.f06$d$a$a$a")
                /* renamed from: com.fossil.f06$d$a$a$a  reason: collision with other inner class name */
                public static final class RunnableC0074a implements Runnable {
                    @DexIgnore
                    public /* final */ /* synthetic */ C0073a b;

                    @DexIgnore
                    public RunnableC0074a(C0073a aVar) {
                        this.b = aVar;
                    }

                    @DexIgnore
                    public final void run() {
                        this.b.this$0.this$0.this$0.r.getNotificationSettingsDao().insertListNotificationSettings(this.b.$notificationSettings);
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0073a(a aVar, List list, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$notificationSettings = list;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0073a aVar = new C0073a(this.this$0, this.$notificationSettings, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((C0073a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        this.this$0.this$0.this$0.r.runInTransaction(new RunnableC0074a(this));
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$2", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ dr7 $contactMessageAppFilters;
                @DexIgnore
                public /* final */ /* synthetic */ List $listNotificationSettings;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, List list, dr7 dr7, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$listNotificationSettings = list;
                    this.$contactMessageAppFilters = dr7;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    b bVar = new b(this.this$0, this.$listNotificationSettings, this.$contactMessageAppFilters, qn7);
                    bVar.p$ = (iv7) obj;
                    throw null;
                    //return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                    throw null;
                    //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        for (NotificationSettingsModel notificationSettingsModel : this.$listNotificationSettings) {
                            int component2 = notificationSettingsModel.component2();
                            if (notificationSettingsModel.component3()) {
                                String P = this.this$0.this$0.this$0.P(component2);
                                FLogger.INSTANCE.getLocal().d(f06.x.a(), "CALL settingsTypeName=" + P);
                                if (component2 == 0) {
                                    FLogger.INSTANCE.getLocal().d(f06.x.a(), "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL());
                                    DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                    this.$contactMessageAppFilters.element.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
                                } else if (component2 == 1) {
                                    int size = this.this$0.this$0.this$0.k.size();
                                    for (int i = 0; i < size; i++) {
                                        ContactGroup contactGroup = (ContactGroup) this.this$0.this$0.this$0.k.get(i);
                                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                        String a2 = f06.x.a();
                                        StringBuilder sb = new StringBuilder();
                                        sb.append("mListAppNotificationFilter add PHONE item - ");
                                        sb.append(i);
                                        sb.append(" name = ");
                                        Contact contact = contactGroup.getContacts().get(0);
                                        pq7.b(contact, "item.contacts[0]");
                                        sb.append(contact.getDisplayName());
                                        local.d(a2, sb.toString());
                                        DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                        FNotification fNotification = new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType());
                                        List<Contact> contacts = contactGroup.getContacts();
                                        pq7.b(contacts, "item.contacts");
                                        if (!contacts.isEmpty()) {
                                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification);
                                            Contact contact2 = contactGroup.getContacts().get(0);
                                            pq7.b(contact2, "item.contacts[0]");
                                            appNotificationFilter.setSender(contact2.getDisplayName());
                                            this.$contactMessageAppFilters.element.add(appNotificationFilter);
                                        }
                                    }
                                }
                            } else {
                                String P2 = this.this$0.this$0.this$0.P(component2);
                                FLogger.INSTANCE.getLocal().d(f06.x.a(), "MESSAGE settingsTypeName=" + P2);
                                if (component2 == 0) {
                                    FLogger.INSTANCE.getLocal().d(f06.x.a(), "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getMESSAGES());
                                    DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                    this.$contactMessageAppFilters.element.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
                                } else if (component2 == 1) {
                                    int size2 = this.this$0.this$0.this$0.k.size();
                                    for (int i2 = 0; i2 < size2; i2++) {
                                        ContactGroup contactGroup2 = (ContactGroup) this.this$0.this$0.this$0.k.get(i2);
                                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                        String a3 = f06.x.a();
                                        StringBuilder sb2 = new StringBuilder();
                                        sb2.append("mListAppNotificationFilter add MESSAGE item - ");
                                        sb2.append(i2);
                                        sb2.append(" name = ");
                                        Contact contact3 = contactGroup2.getContacts().get(0);
                                        pq7.b(contact3, "item.contacts[0]");
                                        sb2.append(contact3.getDisplayName());
                                        local2.d(a3, sb2.toString());
                                        DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                        FNotification fNotification2 = new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType());
                                        List<Contact> contacts2 = contactGroup2.getContacts();
                                        pq7.b(contacts2, "item.contacts");
                                        if (!contacts2.isEmpty()) {
                                            AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification2);
                                            Contact contact4 = contactGroup2.getContacts().get(0);
                                            pq7.b(contact4, "item.contacts[0]");
                                            appNotificationFilter2.setSender(contact4.getDisplayName());
                                            this.$contactMessageAppFilters.element.add(appNotificationFilter2);
                                        }
                                    }
                                }
                            }
                        }
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$listNotificationSettings$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class c extends ko7 implements vp7<iv7, qn7<? super List<? extends NotificationSettingsModel>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    c cVar = new c(this.this$0, qn7);
                    cVar.p$ = (iv7) obj;
                    throw null;
                    //return cVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super List<? extends NotificationSettingsModel>> qn7) {
                    throw null;
                    //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        return this.this$0.this$0.this$0.r.getNotificationSettingsDao().getListNotificationSettingsNoLiveData();
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends AppNotificationFilter>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:16:0x005e  */
            /* JADX WARNING: Removed duplicated region for block: B:22:0x00b1  */
            /* JADX WARNING: Removed duplicated region for block: B:29:0x0141  */
            /* JADX WARNING: Removed duplicated region for block: B:32:0x0186  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r15) {
                /*
                // Method dump skipped, instructions count: 422
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.f06.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$otherAppDeffer$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super List<? extends AppNotificationFilter>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends AppNotificationFilter>> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return d47.c(this.this$0.this$0.O(), false, 1, null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(f06 f06, qn7 qn7) {
            super(2, qn7);
            this.this$0 = f06;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v15, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x00d0  */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 362
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.f06.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f06 f1018a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1", f = "HomeAlertsPresenter.kt", l = {85}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.f06$e$a$a")
            /* renamed from: com.fossil.f06$e$a$a  reason: collision with other inner class name */
            public static final class C0075a implements iq4.e<v36.a, iq4.a> {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ a f1019a;

                @DexIgnore
                /* JADX WARN: Incorrect args count in method signature: ()V */
                public C0075a(a aVar) {
                    this.f1019a = aVar;
                }

                @DexIgnore
                /* renamed from: b */
                public void a(iq4.a aVar) {
                    pq7.c(aVar, "errorValue");
                    FLogger.INSTANCE.getLocal().d(f06.x.a(), "GetApps onError");
                }

                @DexIgnore
                /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r1v3 int), ('/' char), (wrap: int : 0x0156: INVOKE  (r1v5 int) = (r4v0 java.util.ArrayList) type: INTERFACE call: java.util.List.size():int)] */
                /* renamed from: c */
                public void onSuccess(v36.a aVar) {
                    String sb;
                    pq7.c(aVar, "responseValue");
                    FLogger.INSTANCE.getLocal().d(f06.x.a(), "GetApps onSuccess");
                    ArrayList<i06> arrayList = new ArrayList();
                    arrayList.addAll(aVar.a());
                    if (this.f1019a.this$0.f1018a.u.W()) {
                        ArrayList arrayList2 = new ArrayList();
                        for (i06 i06 : arrayList) {
                            InstalledApp installedApp = i06.getInstalledApp();
                            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                            if (isSelected == null) {
                                pq7.i();
                                throw null;
                            } else if (!isSelected.booleanValue()) {
                                arrayList2.add(i06);
                            }
                        }
                        this.f1019a.this$0.f1018a.O().clear();
                        this.f1019a.this$0.f1018a.O().addAll(arrayList);
                        if (!arrayList2.isEmpty()) {
                            this.f1019a.this$0.f1018a.T(arrayList2);
                            return;
                        }
                        String c = um5.c(PortfolioApp.h0.c(), 2131886503);
                        a06 a06 = this.f1019a.this$0.f1018a.l;
                        pq7.b(c, "notificationAppOverView");
                        a06.o1(c);
                        f06 f06 = this.f1019a.this$0.f1018a;
                        if (f06.R(f06.O(), arrayList)) {
                            this.f1019a.this$0.f1018a.U();
                            return;
                        }
                        return;
                    }
                    this.f1019a.this$0.f1018a.O().clear();
                    int i = 0;
                    for (i06 i062 : arrayList) {
                        InstalledApp installedApp2 = i062.getInstalledApp();
                        Boolean isSelected2 = installedApp2 != null ? installedApp2.isSelected() : null;
                        if (isSelected2 == null) {
                            pq7.i();
                            throw null;
                        } else if (isSelected2.booleanValue()) {
                            this.f1019a.this$0.f1018a.O().add(i062);
                            i++;
                        }
                    }
                    if (i == aVar.a().size()) {
                        sb = um5.c(PortfolioApp.h0.c(), 2131886503);
                    } else if (i == 0) {
                        sb = um5.c(PortfolioApp.h0.c(), 2131886996);
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(i);
                        sb2.append('/');
                        sb2.append(arrayList.size());
                        sb = sb2.toString();
                    }
                    a06 a062 = this.f1019a.this$0.f1018a.l;
                    pq7.b(sb, "notificationAppOverView");
                    a062.o1(sb);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b<T> implements ls0<List<? extends DNDScheduledTimeModel>> {

                @DexIgnore
                /* renamed from: a  reason: collision with root package name */
                public /* final */ /* synthetic */ a f1020a;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.f06$e$a$b$a")
                @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3$1", f = "HomeAlertsPresenter.kt", l = {166}, m = "invokeSuspend")
                /* renamed from: com.fossil.f06$e$a$b$a  reason: collision with other inner class name */
                public static final class C0076a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ List $lDndScheduledTimeModel;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public iv7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ b this$0;

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.f06$e$a$b$a$a")
                    @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$3$1$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
                    /* renamed from: com.fossil.f06$e$a$b$a$a  reason: collision with other inner class name */
                    public static final class C0077a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
                        @DexIgnore
                        public int label;
                        @DexIgnore
                        public iv7 p$;
                        @DexIgnore
                        public /* final */ /* synthetic */ C0076a this$0;

                        @DexIgnore
                        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                        public C0077a(C0076a aVar, qn7 qn7) {
                            super(2, qn7);
                            this.this$0 = aVar;
                        }

                        @DexIgnore
                        @Override // com.fossil.zn7
                        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                            pq7.c(qn7, "completion");
                            C0077a aVar = new C0077a(this.this$0, qn7);
                            aVar.p$ = (iv7) obj;
                            throw null;
                            //return aVar;
                        }

                        @DexIgnore
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                        @Override // com.fossil.vp7
                        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                            throw null;
                            //return ((C0077a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                        }

                        @DexIgnore
                        @Override // com.fossil.zn7
                        public final Object invokeSuspend(Object obj) {
                            yn7.d();
                            if (this.label == 0) {
                                el7.b(obj);
                                this.this$0.this$0.f1020a.this$0.f1018a.v.getDNDScheduledTimeDao().insertListDNDScheduledTime(this.this$0.$lDndScheduledTimeModel);
                                return tl7.f3441a;
                            }
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                    }

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0076a(b bVar, List list, qn7 qn7) {
                        super(2, qn7);
                        this.this$0 = bVar;
                        this.$lDndScheduledTimeModel = list;
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                        pq7.c(qn7, "completion");
                        C0076a aVar = new C0076a(this.this$0, this.$lDndScheduledTimeModel, qn7);
                        aVar.p$ = (iv7) obj;
                        throw null;
                        //return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.vp7
                    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                        throw null;
                        //return ((C0076a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                    }

                    @DexIgnore
                    @Override // com.fossil.zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            el7.b(obj);
                            iv7 iv7 = this.p$;
                            dv7 i2 = this.this$0.f1020a.this$0.f1018a.i();
                            C0077a aVar = new C0077a(this, null);
                            this.L$0 = iv7;
                            this.label = 1;
                            if (eu7.g(i2, aVar, this) == d) {
                                return d;
                            }
                        } else if (i == 1) {
                            iv7 iv72 = (iv7) this.L$0;
                            el7.b(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        return tl7.f3441a;
                    }
                }

                @DexIgnore
                public b(a aVar) {
                    this.f1020a = aVar;
                }

                @DexIgnore
                /* renamed from: a */
                public final void onChanged(List<DNDScheduledTimeModel> list) {
                    if (list == null || list.isEmpty()) {
                        ArrayList arrayList = new ArrayList();
                        DNDScheduledTimeModel dNDScheduledTimeModel = new DNDScheduledTimeModel("Start", 1380, 0);
                        DNDScheduledTimeModel dNDScheduledTimeModel2 = new DNDScheduledTimeModel("End", 1140, 1);
                        arrayList.add(dNDScheduledTimeModel);
                        arrayList.add(dNDScheduledTimeModel2);
                        xw7 unused = gu7.d(this.f1020a.this$0.f1018a.k(), null, null, new C0076a(this, arrayList, null), 3, null);
                        return;
                    }
                    for (T t : list) {
                        if (t.getScheduledTimeType() == 0) {
                            this.f1020a.this$0.f1018a.l.q2(this.f1020a.this$0.f1018a.Q(t.getMinutes()));
                        } else {
                            this.f1020a.this$0.f1018a.l.J0(this.f1020a.this$0.f1018a.Q(t.getMinutes()));
                        }
                    }
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @eo7(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$start$1$1$allAlarms$1", f = "HomeAlertsPresenter.kt", l = {86, 91}, m = "invokeSuspend")
            public static final class c extends ko7 implements vp7<iv7, qn7<? super List<Alarm>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    c cVar = new c(this.this$0, qn7);
                    cVar.p$ = (iv7) obj;
                    throw null;
                    //return cVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super List<Alarm>> qn7) {
                    throw null;
                    //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    iv7 iv7;
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 = this.p$;
                        bk5 bk5 = this.this$0.this$0.f1018a.n;
                        this.L$0 = iv7;
                        this.label = 1;
                        if (bk5.j(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        iv7 = (iv7) this.L$0;
                        el7.b(obj);
                    } else if (i == 2) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (!this.this$0.this$0.f1018a.i) {
                        this.this$0.this$0.f1018a.i = true;
                        bk5 bk52 = this.this$0.this$0.f1018a.n;
                        Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
                        pq7.b(applicationContext, "PortfolioApp.instance.applicationContext");
                        bk52.g(applicationContext);
                    }
                    AlarmsRepository alarmsRepository = this.this$0.this$0.f1018a.t;
                    this.L$0 = iv7;
                    this.label = 2;
                    Object allAlarmIgnoreDeletePinType = alarmsRepository.getAllAlarmIgnoreDeletePinType(this);
                    return allAlarmIgnoreDeletePinType == d ? d : allAlarmIgnoreDeletePinType;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class d<T> implements Comparator<T> {
                @DexIgnore
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    return mn7.c(Integer.valueOf(t.getTotalMinutes()), Integer.valueOf(t2.getTotalMinutes()));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d2 = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    dv7 i2 = this.this$0.f1018a.i();
                    c cVar = new c(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    g = eu7.g(i2, cVar, this);
                    if (g == d2) {
                        return d2;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List<Alarm> list = (List) g;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = f06.x.a();
                local.d(a2, "GetAlarms onSuccess: size = " + list.size());
                this.this$0.f1018a.g.clear();
                for (Alarm alarm : list) {
                    this.this$0.f1018a.g.add(Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
                }
                ArrayList arrayList = this.this$0.f1018a.g;
                if (arrayList.size() > 1) {
                    lm7.r(arrayList, new d());
                }
                this.this$0.f1018a.l.p0(this.this$0.f1018a.g);
                this.this$0.f1018a.o.e(null, new C0075a(this));
                f06 f06 = this.this$0.f1018a;
                f06.h = f06.u.a0();
                this.this$0.f1018a.l.n3(this.this$0.f1018a.h);
                this.this$0.f1018a.f.h((LifecycleOwner) this.this$0.f1018a.l, new b(this));
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public e(f06 f06) {
            this.f1018a = f06;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            if (str == null || str.length() == 0) {
                this.f1018a.l.r(true);
            } else {
                xw7 unused = gu7.d(this.f1018a.k(), null, null, new a(this, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f06 f1021a;

        @DexIgnore
        public f(f06 f06) {
            this.f1021a = f06;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            a06 unused = this.f1021a.l;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ls0<List<? extends DNDScheduledTimeModel>> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ f06 f1022a;

        @DexIgnore
        public g(f06 f06) {
            this.f1022a = f06;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<DNDScheduledTimeModel> list) {
            a06 unused = this.f1022a.l;
        }
    }

    /*
    static {
        String simpleName = f06.class.getSimpleName();
        pq7.b(simpleName, "HomeAlertsPresenter::class.java.simpleName");
        w = simpleName;
    }
    */

    @DexIgnore
    public f06(a06 a06, uq4 uq4, bk5 bk5, v36 v36, d26 d26, u06 u06, NotificationSettingsDatabase notificationSettingsDatabase, yx5 yx5, AlarmsRepository alarmsRepository, on5 on5, DNDSettingsDatabase dNDSettingsDatabase) {
        pq7.c(a06, "mView");
        pq7.c(uq4, "mUseCaseHandler");
        pq7.c(bk5, "mAlarmHelper");
        pq7.c(v36, "mGetApps");
        pq7.c(d26, "mGetAllContactGroup");
        pq7.c(u06, "mSaveAppsNotification");
        pq7.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        pq7.c(yx5, "mSetAlarms");
        pq7.c(alarmsRepository, "mAlarmRepository");
        pq7.c(on5, "mSharedPreferencesManager");
        pq7.c(dNDSettingsDatabase, "mDNDSettingsDatabase");
        this.l = a06;
        this.m = uq4;
        this.n = bk5;
        this.o = v36;
        this.p = d26;
        this.q = u06;
        this.r = notificationSettingsDatabase;
        this.s = yx5;
        this.t = alarmsRepository;
        this.u = on5;
        this.v = dNDSettingsDatabase;
    }

    @DexIgnore
    public final void N(Alarm alarm, boolean z) {
        pq7.c(alarm, "editAlarm");
        Iterator<Alarm> it = this.g.iterator();
        while (it.hasNext()) {
            Alarm next = it.next();
            if (pq7.a(next.getUri(), alarm.getUri())) {
                ArrayList<Alarm> arrayList = this.g;
                arrayList.set(arrayList.indexOf(next), Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
                if (!z) {
                    break;
                }
                S();
            }
        }
        this.l.p0(this.g);
    }

    @DexIgnore
    public final List<i06> O() {
        return this.j;
    }

    @DexIgnore
    public final String P(int i2) {
        if (i2 == 0) {
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886089);
            pq7.b(c2, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return c2;
        } else if (i2 != 1) {
            String c3 = um5.c(PortfolioApp.h0.c(), 2131886091);
            pq7.b(c3, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return c3;
        } else {
            String c4 = um5.c(PortfolioApp.h0.c(), 2131886090);
            pq7.b(c4, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return c4;
        }
    }

    @DexIgnore
    public final SpannableString Q(int i2) {
        int i3 = 12;
        int i4 = i2 / 60;
        int i5 = i2 % 60;
        if (DateFormat.is24HourFormat(PortfolioApp.h0.c())) {
            StringBuilder sb = new StringBuilder();
            hr7 hr7 = hr7.f1520a;
            Locale locale = Locale.US;
            pq7.b(locale, "Locale.US");
            String format = String.format(locale, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i4)}, 1));
            pq7.b(format, "java.lang.String.format(locale, format, *args)");
            sb.append(format);
            sb.append(':');
            hr7 hr72 = hr7.f1520a;
            Locale locale2 = Locale.US;
            pq7.b(locale2, "Locale.US");
            String format2 = String.format(locale2, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i5)}, 1));
            pq7.b(format2, "java.lang.String.format(locale, format, *args)");
            sb.append(format2);
            return new SpannableString(sb.toString());
        } else if (i2 < 720) {
            if (i4 != 0) {
                i3 = i4;
            }
            jl5 jl5 = jl5.b;
            StringBuilder sb2 = new StringBuilder();
            hr7 hr73 = hr7.f1520a;
            Locale locale3 = Locale.US;
            pq7.b(locale3, "Locale.US");
            String format3 = String.format(locale3, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i3)}, 1));
            pq7.b(format3, "java.lang.String.format(locale, format, *args)");
            sb2.append(format3);
            sb2.append(':');
            hr7 hr74 = hr7.f1520a;
            Locale locale4 = Locale.US;
            pq7.b(locale4, "Locale.US");
            String format4 = String.format(locale4, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i5)}, 1));
            pq7.b(format4, "java.lang.String.format(locale, format, *args)");
            sb2.append(format4);
            String sb3 = sb2.toString();
            String c2 = um5.c(PortfolioApp.h0.c(), 2131886102);
            pq7.b(c2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
            return jl5.g(sb3, c2, 1.0f);
        } else {
            if (i4 > 12) {
                i3 = i4 - 12;
            }
            jl5 jl52 = jl5.b;
            StringBuilder sb4 = new StringBuilder();
            hr7 hr75 = hr7.f1520a;
            Locale locale5 = Locale.US;
            pq7.b(locale5, "Locale.US");
            String format5 = String.format(locale5, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i3)}, 1));
            pq7.b(format5, "java.lang.String.format(locale, format, *args)");
            sb4.append(format5);
            sb4.append(':');
            hr7 hr76 = hr7.f1520a;
            Locale locale6 = Locale.US;
            pq7.b(locale6, "Locale.US");
            String format6 = String.format(locale6, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(i5)}, 1));
            pq7.b(format6, "java.lang.String.format(locale, format, *args)");
            sb4.append(format6);
            String sb5 = sb4.toString();
            String c3 = um5.c(PortfolioApp.h0.c(), 2131886104);
            pq7.b(c3, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
            return jl52.g(sb5, c3, 1.0f);
        }
    }

    @DexIgnore
    public final boolean R(List<i06> list, List<i06> list2) {
        T t2;
        pq7.c(list, "listDatabaseAppWrapper");
        pq7.c(list2, "listAppWrapper");
        if (list.size() != list2.size()) {
            return true;
        }
        boolean z = false;
        for (T t3 : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                T next = it.next();
                InstalledApp installedApp = next.getInstalledApp();
                String identifier = installedApp != null ? installedApp.getIdentifier() : null;
                InstalledApp installedApp2 = t3.getInstalledApp();
                if (pq7.a(identifier, installedApp2 != null ? installedApp2.getIdentifier() : null)) {
                    t2 = next;
                    break;
                }
            }
            T t4 = t2;
            if (t4 != null) {
                InstalledApp installedApp3 = t4.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                InstalledApp installedApp4 = t3.getInstalledApp();
                if (!(!pq7.a(isSelected, installedApp4 != null ? installedApp4.isSelected() : null))) {
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final void S() {
        FLogger.INSTANCE.getLocal().d(w, "onSetAlarmsSuccess");
        this.n.g(PortfolioApp.h0.c());
        String e2 = this.e.e();
        if (e2 != null) {
            PortfolioApp c2 = PortfolioApp.h0.c();
            pq7.b(e2, "it");
            c2.P0(e2);
        }
    }

    @DexIgnore
    public final void T(List<i06> list) {
        pq7.c(list, "listAppWrapperNotEnabled");
        for (i06 i06 : list) {
            InstalledApp installedApp = i06.getInstalledApp();
            if (installedApp != null) {
                installedApp.setSelected(true);
            }
        }
        this.m.a(this.q, new u06.a(list), new c(this));
    }

    @DexIgnore
    public final void U() {
        jn5 jn5 = jn5.b;
        a06 a06 = this.l;
        if (a06 == null) {
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
        } else if (jn5.c(jn5, ((b06) a06).getContext(), jn5.a.SET_BLE_COMMAND, false, false, false, null, 60, null)) {
            xw7 unused = gu7.d(k(), null, null, new d(this, null), 3, null);
        }
    }

    @DexIgnore
    public void V() {
        this.l.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(w, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.s.s();
        PortfolioApp.h0.h(this);
        LiveData<String> liveData = this.e;
        a06 a06 = this.l;
        if (a06 != null) {
            liveData.h((b06) a06, new e(this));
            wq5.d.g(CommunicateMode.SET_LIST_ALARM);
            a06 a062 = this.l;
            a062.F(!jn5.c(jn5.b, ((b06) a062).getContext(), jn5.a.NOTIFICATION_DIANA, false, false, false, null, 56, null));
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(w, "stop");
        this.e.m(new f(this));
        this.f.m(new g(this));
        this.s.w();
        PortfolioApp.h0.l(this);
    }

    @DexIgnore
    @Override // com.fossil.zz5
    public void n() {
        jn5 jn5 = jn5.b;
        a06 a06 = this.l;
        if (a06 != null) {
            jn5.c(jn5, ((b06) a06).getContext(), jn5.a.NOTIFICATION_DIANA, false, false, false, null, 60, null);
            return;
        }
        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
    }

    @DexIgnore
    @Override // com.fossil.zz5
    public void o(Alarm alarm, boolean z) {
        pq7.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        String e2 = this.e.e();
        if (!(e2 == null || e2.length() == 0)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "enableAlarm - alarmTotalMinue: " + alarm.getTotalMinutes() + " - enable: " + z);
            alarm.setActive(z);
            this.l.b();
            yx5 yx5 = this.s;
            String e3 = this.e.e();
            if (e3 != null) {
                pq7.b(e3, "mActiveSerial.value!!");
                yx5.e(new yx5.c(e3, this.g, alarm), new b(this, alarm));
                return;
            }
            pq7.i();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(w, "enableAlarm - Current Active Device Serial Is Empty");
    }

    @DexIgnore
    @tc7
    public final void onSetAlarmEventEndComplete(qi5 qi5) {
        if (qi5 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = w;
            local.d(str, "onSetAlarmEventEndComplete() - event = " + qi5);
            if (qi5.b()) {
                String a2 = qi5.a();
                Iterator<Alarm> it = this.g.iterator();
                while (it.hasNext()) {
                    Alarm next = it.next();
                    if (pq7.a(next.getUri(), a2)) {
                        next.setActive(false);
                    }
                }
                this.l.W();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.zz5
    public void p() {
        boolean z = !this.h;
        this.h = z;
        this.u.W0(z);
        this.l.n3(this.h);
    }

    @DexIgnore
    @Override // com.fossil.zz5
    public void q(Alarm alarm) {
        String e2 = this.e.e();
        if (e2 == null || e2.length() == 0) {
            FLogger.INSTANCE.getLocal().d("HomeAlertsFragment", "Current Active Device Serial Is Empty");
        } else if (alarm != null || this.g.size() < 32) {
            a06 a06 = this.l;
            String e3 = this.e.e();
            if (e3 != null) {
                pq7.b(e3, "mActiveSerial.value!!");
                a06.l0(e3, this.g, alarm);
                return;
            }
            pq7.i();
            throw null;
        } else {
            this.l.U();
        }
    }
}
