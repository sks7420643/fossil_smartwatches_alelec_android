package com.fossil;

import com.fossil.Dl7;
import com.fossil.Lz7;
import com.fossil.Yy7;
import com.mapped.Cd6;
import com.mapped.Lk6;
import com.mapped.Rc6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Jy7<E> extends Ly7<E> implements My7<E> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<E> extends Uy7<E> {
        @DexIgnore
        public /* final */ Lk6<Object> e;
        @DexIgnore
        public /* final */ int f;

        @DexIgnore
        public Ai(Lk6<Object> lk6, int i) {
            this.e = lk6;
            this.f = i;
        }

        @DexIgnore
        @Override // com.fossil.Wy7
        public void d(E e2) {
            this.e.g(Mu7.a);
        }

        @DexIgnore
        @Override // com.fossil.Wy7
        public Vz7 e(E e2, Lz7.Ci ci) {
            Object b = this.e.b(x(e2), ci != null ? ci.a : null);
            if (b == null) {
                return null;
            }
            if (Nv7.a()) {
                if (!(b == Mu7.a)) {
                    throw new AssertionError();
                }
            }
            if (ci == null) {
                return Mu7.a;
            }
            ci.d();
            throw null;
        }

        @DexIgnore
        @Override // com.fossil.Lz7
        public String toString() {
            return "ReceiveElement@" + Ov7.b(this) + "[receiveMode=" + this.f + ']';
        }

        @DexIgnore
        @Override // com.fossil.Uy7
        public void w(Py7<?> py7) {
            if (this.f == 1 && py7.e == null) {
                Lk6<Object> lk6 = this.e;
                Dl7.Ai ai = Dl7.Companion;
                lk6.resumeWith(Dl7.constructor-impl(null));
            } else if (this.f == 2) {
                Lk6<Object> lk62 = this.e;
                Yy7.Bi bi = Yy7.b;
                Yy7.Ai ai2 = new Yy7.Ai(py7.e);
                Yy7.b(ai2);
                Yy7 a2 = Yy7.a(ai2);
                Dl7.Ai ai3 = Dl7.Companion;
                lk62.resumeWith(Dl7.constructor-impl(a2));
            } else {
                Lk6<Object> lk63 = this.e;
                Throwable x = py7.x();
                Dl7.Ai ai4 = Dl7.Companion;
                lk63.resumeWith(Dl7.constructor-impl(El7.a(x)));
            }
        }

        @DexIgnore
        public final Object x(E e2) {
            if (this.f != 2) {
                return e2;
            }
            Yy7.Bi bi = Yy7.b;
            Yy7.b(e2);
            return Yy7.a(e2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends Iu7 {
        @DexIgnore
        public /* final */ Uy7<?> b;

        @DexIgnore
        public Bi(Uy7<?> uy7) {
            this.b = uy7;
        }

        @DexIgnore
        @Override // com.fossil.Ju7
        public void a(Throwable th) {
            if (this.b.r()) {
                Jy7.this.s();
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
            a(th);
            return Cd6.a;
        }

        @DexIgnore
        public String toString() {
            return "RemoveReceiveOnCancel[" + this.b + ']';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Lz7.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Jy7 d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Lz7 lz7, Lz7 lz72, Jy7 jy7) {
            super(lz72);
            this.d = jy7;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Cz7
        public /* bridge */ /* synthetic */ Object g(Lz7 lz7) {
            return i(lz7);
        }

        @DexIgnore
        public Object i(Lz7 lz7) {
            if (this.d.r()) {
                return null;
            }
            return Kz7.a();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.mapped.Xe6<? super E> */
    /* JADX WARN: Multi-variable type inference failed */
    public final Object a(Xe6<? super E> xe6) {
        Object u = u();
        return (u == Ky7.b || (u instanceof Py7)) ? v(1, xe6) : u;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.mapped.Xe6<? super E> */
    /* JADX WARN: Multi-variable type inference failed */
    public final Object b(Xe6<? super E> xe6) {
        Object u = u();
        return (u == Ky7.b || (u instanceof Py7)) ? v(0, xe6) : u;
    }

    @DexIgnore
    @Override // com.fossil.Ly7
    public Wy7<E> l() {
        Wy7<E> l = super.l();
        if (l != null && !(l instanceof Py7)) {
            s();
        }
        return l;
    }

    @DexIgnore
    public final boolean o(Uy7<? super E> uy7) {
        boolean p = p(uy7);
        if (p) {
            t();
        }
        return p;
    }

    @DexIgnore
    public boolean p(Uy7<? super E> uy7) {
        int v;
        Lz7 n;
        if (q()) {
            Lz7 g = g();
            do {
                n = g.n();
                if (!(!(n instanceof Xy7))) {
                    return false;
                }
            } while (!n.g(uy7, g));
        } else {
            Lz7 g2 = g();
            Ci ci = new Ci(uy7, uy7, this);
            do {
                Lz7 n2 = g2.n();
                if (!(!(n2 instanceof Xy7))) {
                    return false;
                }
                v = n2.v(uy7, g2, ci);
                if (v != 1) {
                }
            } while (v != 2);
            return false;
        }
        return true;
    }

    @DexIgnore
    public abstract boolean q();

    @DexIgnore
    public abstract boolean r();

    @DexIgnore
    public void s() {
    }

    @DexIgnore
    public void t() {
    }

    @DexIgnore
    public abstract Object u();

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.Jy7$Ai */
    /* JADX WARN: Multi-variable type inference failed */
    public final /* synthetic */ <R> Object v(int i, Xe6<? super R> xe6) {
        Lu7 b = Nu7.b(Xn7.c(xe6));
        if (b != null) {
            Ai ai = new Ai(b, i);
            while (true) {
                if (!o(ai)) {
                    Object u = u();
                    if (!(u instanceof Py7)) {
                        if (u != Ky7.b) {
                            Object x = ai.x(u);
                            Dl7.Ai ai2 = Dl7.Companion;
                            b.resumeWith(Dl7.constructor-impl(x));
                            break;
                        }
                    } else {
                        ai.w((Py7) u);
                        break;
                    }
                } else {
                    w(b, ai);
                    break;
                }
            }
            Object t = b.t();
            if (t == Yn7.d()) {
                Go7.c(xe6);
            }
            return t;
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.CancellableContinuation<kotlin.Any?>");
    }

    @DexIgnore
    public final void w(Lk6<?> lk6, Uy7<?> uy7) {
        lk6.e(new Bi(uy7));
    }
}
