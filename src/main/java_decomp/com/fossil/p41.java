package com.fossil;

import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P41 {
    @DexIgnore
    public static /* final */ P41 c; // = new P41();
    @DexIgnore
    public /* final */ ExecutorService a;
    @DexIgnore
    public /* final */ Executor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Executor {
        @DexIgnore
        public ThreadLocal<Integer> b;

        @DexIgnore
        public Bi() {
            this.b = new ThreadLocal<>();
        }

        @DexIgnore
        public final int a() {
            Integer num = this.b.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.b.remove();
            } else {
                this.b.set(Integer.valueOf(intValue));
            }
            return intValue;
        }

        @DexIgnore
        public final int b() {
            Integer num = this.b.get();
            if (num == null) {
                num = 0;
            }
            int intValue = num.intValue() + 1;
            this.b.set(Integer.valueOf(intValue));
            return intValue;
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            if (b() <= 15) {
                try {
                    runnable.run();
                } catch (Throwable th) {
                    a();
                    throw th;
                }
            } else {
                P41.a().execute(runnable);
            }
            a();
        }
    }

    @DexIgnore
    public P41() {
        this.a = !c() ? Executors.newCachedThreadPool() : M41.b();
        Executors.newSingleThreadScheduledExecutor();
        this.b = new Bi();
    }

    @DexIgnore
    public static ExecutorService a() {
        return c.a;
    }

    @DexIgnore
    public static Executor b() {
        return c.b;
    }

    @DexIgnore
    public static boolean c() {
        String property = System.getProperty("java.runtime.name");
        if (property == null) {
            return false;
        }
        return property.toLowerCase(Locale.US).contains("android");
    }
}
