package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zy6 implements Factory<GetHybridDeviceSettingUseCase> {
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> a;
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> b;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> c;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> d;
    @DexIgnore
    public /* final */ Provider<CategoryRepository> e;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> f;

    @DexIgnore
    public Zy6(Provider<HybridPresetRepository> provider, Provider<MicroAppRepository> provider2, Provider<DeviceRepository> provider3, Provider<NotificationsRepository> provider4, Provider<CategoryRepository> provider5, Provider<AlarmsRepository> provider6) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
    }

    @DexIgnore
    public static Zy6 a(Provider<HybridPresetRepository> provider, Provider<MicroAppRepository> provider2, Provider<DeviceRepository> provider3, Provider<NotificationsRepository> provider4, Provider<CategoryRepository> provider5, Provider<AlarmsRepository> provider6) {
        return new Zy6(provider, provider2, provider3, provider4, provider5, provider6);
    }

    @DexIgnore
    public static GetHybridDeviceSettingUseCase c(HybridPresetRepository hybridPresetRepository, MicroAppRepository microAppRepository, DeviceRepository deviceRepository, NotificationsRepository notificationsRepository, CategoryRepository categoryRepository, AlarmsRepository alarmsRepository) {
        return new GetHybridDeviceSettingUseCase(hybridPresetRepository, microAppRepository, deviceRepository, notificationsRepository, categoryRepository, alarmsRepository);
    }

    @DexIgnore
    public GetHybridDeviceSettingUseCase b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
