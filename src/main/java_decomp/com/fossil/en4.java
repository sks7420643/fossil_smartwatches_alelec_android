package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class En4 extends Mn4 {
    @DexIgnore
    @Override // com.fossil.Jn4, com.fossil.Ql4
    public Bm4 a(String str, Kl4 kl4, int i, int i2, Map<Ml4, ?> map) throws Rl4 {
        if (kl4 == Kl4.EAN_13) {
            return super.a(str, kl4, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode EAN_13, but got " + kl4);
    }

    @DexIgnore
    @Override // com.fossil.Jn4
    public boolean[] c(String str) {
        if (str.length() == 13) {
            try {
                if (Ln4.a(str)) {
                    int i = Dn4.f[Integer.parseInt(str.substring(0, 1))];
                    boolean[] zArr = new boolean[95];
                    int b = Jn4.b(zArr, 0, Ln4.a, true) + 0;
                    int i2 = 1;
                    while (i2 <= 6) {
                        int i3 = i2 + 1;
                        int parseInt = Integer.parseInt(str.substring(i2, i3));
                        if (((i >> (6 - i2)) & 1) == 1) {
                            parseInt += 10;
                        }
                        b = Jn4.b(zArr, b, Ln4.e[parseInt], false) + b;
                        i2 = i3;
                    }
                    int b2 = b + Jn4.b(zArr, b, Ln4.b, false);
                    int i4 = 7;
                    int i5 = b2;
                    while (i4 <= 12) {
                        int i6 = i4 + 1;
                        i5 += Jn4.b(zArr, i5, Ln4.d[Integer.parseInt(str.substring(i4, i6))], true);
                        i4 = i6;
                    }
                    Jn4.b(zArr, i5, Ln4.a, true);
                    return zArr;
                }
                throw new IllegalArgumentException("Contents do not pass checksum");
            } catch (Nl4 e) {
                throw new IllegalArgumentException("Illegal contents");
            }
        } else {
            throw new IllegalArgumentException("Requested contents should be 13 digits long, but got " + str.length());
        }
    }
}
