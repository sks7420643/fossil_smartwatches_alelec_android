package com.fossil;

import android.os.Process;
import com.fossil.Cd1;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nc1 {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ Map<Mb1, Di> b;
    @DexIgnore
    public /* final */ ReferenceQueue<Cd1<?>> c;
    @DexIgnore
    public Cd1.Ai d;
    @DexIgnore
    public volatile boolean e;
    @DexIgnore
    public volatile Ci f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements ThreadFactory {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Runnable b;

            @DexIgnore
            public Aii(Ai ai, Runnable runnable) {
                this.b = runnable;
            }

            @DexIgnore
            public void run() {
                Process.setThreadPriority(10);
                this.b.run();
            }
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(new Aii(this, runnable), "glide-active-resources");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Runnable {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public void run() {
            Nc1.this.b();
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends WeakReference<Cd1<?>> {
        @DexIgnore
        public /* final */ Mb1 a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public Id1<?> c;

        @DexIgnore
        public Di(Mb1 mb1, Cd1<?> cd1, ReferenceQueue<? super Cd1<?>> referenceQueue, boolean z) {
            super(cd1, referenceQueue);
            Id1<?> id1;
            Ik1.d(mb1);
            this.a = mb1;
            if (!cd1.f() || !z) {
                id1 = null;
            } else {
                Id1<?> e = cd1.e();
                Ik1.d(e);
                id1 = e;
            }
            this.c = id1;
            this.b = cd1.f();
        }

        @DexIgnore
        public void a() {
            this.c = null;
            clear();
        }
    }

    @DexIgnore
    public Nc1(boolean z) {
        this(z, Executors.newSingleThreadExecutor(new Ai()));
    }

    @DexIgnore
    public Nc1(boolean z, Executor executor) {
        this.b = new HashMap();
        this.c = new ReferenceQueue<>();
        this.a = z;
        executor.execute(new Bi());
    }

    @DexIgnore
    public void a(Mb1 mb1, Cd1<?> cd1) {
        synchronized (this) {
            Di put = this.b.put(mb1, new Di(mb1, cd1, this.c, this.a));
            if (put != null) {
                put.a();
            }
        }
    }

    @DexIgnore
    public void b() {
        while (!this.e) {
            try {
                c((Di) this.c.remove());
                Ci ci = this.f;
                if (ci != null) {
                    ci.a();
                }
            } catch (InterruptedException e2) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @DexIgnore
    public void c(Di di) {
        synchronized (this) {
            this.b.remove(di.a);
            if (di.b && di.c != null) {
                this.d.d(di.a, new Cd1<>(di.c, true, false, di.a, this.d));
            }
        }
    }

    @DexIgnore
    public void d(Mb1 mb1) {
        synchronized (this) {
            Di remove = this.b.remove(mb1);
            if (remove != null) {
                remove.a();
            }
        }
    }

    @DexIgnore
    public Cd1<?> e(Mb1 mb1) {
        synchronized (this) {
            Di di = this.b.get(mb1);
            if (di == null) {
                return null;
            }
            Cd1<?> cd1 = (Cd1) di.get();
            if (cd1 == null) {
                c(di);
            }
            return cd1;
        }
    }

    @DexIgnore
    public void f(Cd1.Ai ai) {
        synchronized (ai) {
            synchronized (this) {
                this.d = ai;
            }
        }
    }
}
