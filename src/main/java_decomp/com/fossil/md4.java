package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Md4 implements Nd4 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public boolean b; // = false;
    @DexIgnore
    public String c;

    @DexIgnore
    public Md4(Context context) {
        this.a = context;
    }

    @DexIgnore
    @Override // com.fossil.Nd4
    public String a() {
        if (!this.b) {
            this.c = R84.F(this.a);
            this.b = true;
        }
        String str = this.c;
        if (str != null) {
            return str;
        }
        return null;
    }
}
