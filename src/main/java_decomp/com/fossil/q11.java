package com.fossil;

import android.content.Context;
import android.os.Build;
import com.mapped.Xh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Q11 {
    @DexIgnore
    public static Xh a; // = new Ai(1, 2);
    @DexIgnore
    public static Xh b; // = new Bi(3, 4);
    @DexIgnore
    public static Xh c; // = new Ci(4, 5);
    @DexIgnore
    public static Xh d; // = new Di(6, 7);
    @DexIgnore
    public static Xh e; // = new Ei(7, 8);
    @DexIgnore
    public static Xh f; // = new Fi(8, 9);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Xh {
        @DexIgnore
        public Ai(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.mapped.Xh
        public void migrate(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `SystemIdInfo` (`work_spec_id` TEXT NOT NULL, `system_id` INTEGER NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
            lx0.execSQL("INSERT INTO SystemIdInfo(work_spec_id, system_id) SELECT work_spec_id, alarm_id AS system_id FROM alarmInfo");
            lx0.execSQL("DROP TABLE IF EXISTS alarmInfo");
            lx0.execSQL("INSERT OR IGNORE INTO worktag(tag, work_spec_id) SELECT worker_class_name AS tag, id AS work_spec_id FROM workspec");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Xh {
        @DexIgnore
        public Bi(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.mapped.Xh
        public void migrate(Lx0 lx0) {
            if (Build.VERSION.SDK_INT >= 23) {
                lx0.execSQL("UPDATE workspec SET schedule_requested_at=0 WHERE state NOT IN (2, 3, 5) AND schedule_requested_at=-1 AND interval_duration<>0");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Xh {
        @DexIgnore
        public Ci(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.mapped.Xh
        public void migrate(Lx0 lx0) {
            lx0.execSQL("ALTER TABLE workspec ADD COLUMN `trigger_content_update_delay` INTEGER NOT NULL DEFAULT -1");
            lx0.execSQL("ALTER TABLE workspec ADD COLUMN `trigger_max_content_delay` INTEGER NOT NULL DEFAULT -1");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends Xh {
        @DexIgnore
        public Di(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.mapped.Xh
        public void migrate(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `WorkProgress` (`work_spec_id` TEXT NOT NULL, `progress` BLOB NOT NULL, PRIMARY KEY(`work_spec_id`), FOREIGN KEY(`work_spec_id`) REFERENCES `WorkSpec`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei extends Xh {
        @DexIgnore
        public Ei(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.mapped.Xh
        public void migrate(Lx0 lx0) {
            lx0.execSQL("CREATE INDEX IF NOT EXISTS `index_WorkSpec_period_start_time` ON `workspec` (`period_start_time`)");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi extends Xh {
        @DexIgnore
        public Fi(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.mapped.Xh
        public void migrate(Lx0 lx0) {
            lx0.execSQL("ALTER TABLE workspec ADD COLUMN `run_in_foreground` INTEGER NOT NULL DEFAULT 0");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Gi extends Xh {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public Gi(Context context, int i, int i2) {
            super(i, i2);
            this.a = context;
        }

        @DexIgnore
        @Override // com.mapped.Xh
        public void migrate(Lx0 lx0) {
            if (this.endVersion >= 10) {
                lx0.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"reschedule_needed", 1});
                return;
            }
            this.a.getSharedPreferences("androidx.work.util.preferences", 0).edit().putBoolean("reschedule_needed", true).apply();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Hi extends Xh {
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public Hi(Context context) {
            super(9, 10);
            this.a = context;
        }

        @DexIgnore
        @Override // com.mapped.Xh
        public void migrate(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `Preference` (`key` TEXT NOT NULL, `long_value` INTEGER, PRIMARY KEY(`key`))");
            Z31.b(this.a, lx0);
            X31.a(this.a, lx0);
        }
    }
}
