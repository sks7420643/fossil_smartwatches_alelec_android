package com.fossil;

import com.fossil.V03;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T03<T extends V03<T>> {
    @DexIgnore
    public static /* final */ T03 d; // = new T03(true);
    @DexIgnore
    public /* final */ G33<T, Object> a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public T03() {
        this.a = G33.b(16);
    }

    @DexIgnore
    public T03(G33<T, Object> g33) {
        this.a = g33;
        k();
    }

    @DexIgnore
    public T03(boolean z) {
        this(G33.b(0));
        k();
    }

    @DexIgnore
    public static int a(V03<?> v03, Object obj) {
        int i = 0;
        L43 zzb = v03.zzb();
        int zza = v03.zza();
        if (!v03.zzd()) {
            return b(zzb, zza, obj);
        }
        if (v03.zze()) {
            for (Object obj2 : (List) obj) {
                i += j(zzb, obj2);
            }
            return L03.E0(i) + L03.h0(zza) + i;
        }
        for (Object obj3 : (List) obj) {
            i += b(zzb, zza, obj3);
        }
        return i;
    }

    @DexIgnore
    public static int b(L43 l43, int i, Object obj) {
        int i2;
        int h0 = L03.h0(i);
        if (l43 == L43.zzj) {
            H13.g((M23) obj);
            i2 = h0 << 1;
        } else {
            i2 = h0;
        }
        return i2 + j(l43, obj);
    }

    @DexIgnore
    public static <T extends V03<T>> T03<T> c() {
        return d;
    }

    @DexIgnore
    public static Object e(Object obj) {
        if (obj instanceof V23) {
            return ((V23) obj).zza();
        }
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    @DexIgnore
    public static void f(L03 l03, L43 l43, int i, Object obj) throws IOException {
        if (l43 == L43.zzj) {
            M23 m23 = (M23) obj;
            H13.g(m23);
            l03.m(i, 3);
            m23.n(l03);
            l03.m(i, 4);
            return;
        }
        l03.m(i, l43.zzb());
        switch (W03.b[l43.ordinal()]) {
            case 1:
                l03.h(((Double) obj).doubleValue());
                return;
            case 2:
                l03.i(((Float) obj).floatValue());
                return;
            case 3:
                l03.t(((Long) obj).longValue());
                return;
            case 4:
                l03.t(((Long) obj).longValue());
                return;
            case 5:
                l03.j(((Integer) obj).intValue());
                return;
            case 6:
                l03.a0(((Long) obj).longValue());
                return;
            case 7:
                l03.f0(((Integer) obj).intValue());
                return;
            case 8:
                l03.y(((Boolean) obj).booleanValue());
                return;
            case 9:
                ((M23) obj).n(l03);
                return;
            case 10:
                l03.v((M23) obj);
                return;
            case 11:
                if (obj instanceof Xz2) {
                    l03.u((Xz2) obj);
                    return;
                } else {
                    l03.w((String) obj);
                    return;
                }
            case 12:
                if (obj instanceof Xz2) {
                    l03.u((Xz2) obj);
                    return;
                }
                byte[] bArr = (byte[]) obj;
                l03.T(bArr, 0, bArr.length);
                return;
            case 13:
                l03.O(((Integer) obj).intValue());
                return;
            case 14:
                l03.f0(((Integer) obj).intValue());
                return;
            case 15:
                l03.a0(((Long) obj).longValue());
                return;
            case 16:
                l03.X(((Integer) obj).intValue());
                return;
            case 17:
                l03.S(((Long) obj).longValue());
                return;
            case 18:
                if (obj instanceof G13) {
                    l03.j(((G13) obj).zza());
                    return;
                } else {
                    l03.j(((Integer) obj).intValue());
                    return;
                }
            default:
                return;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0026, code lost:
        if ((r3 instanceof com.fossil.G13) == false) goto L_0x0013;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        if ((r3 instanceof byte[]) == false) goto L_0x0013;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
        if ((r3 instanceof com.fossil.Q13) == false) goto L_0x0013;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void h(com.fossil.L43 r2, java.lang.Object r3) {
        /*
            com.fossil.H13.d(r3)
            int[] r0 = com.fossil.W03.a
            com.fossil.S43 r1 = r2.zza()
            int r1 = r1.ordinal()
            r1 = r0[r1]
            r0 = 1
            switch(r1) {
                case 1: goto L_0x0041;
                case 2: goto L_0x003e;
                case 3: goto L_0x003b;
                case 4: goto L_0x0038;
                case 5: goto L_0x0035;
                case 6: goto L_0x0032;
                case 7: goto L_0x0029;
                case 8: goto L_0x0020;
                case 9: goto L_0x0017;
                default: goto L_0x0013;
            }
        L_0x0013:
            r0 = 0
        L_0x0014:
            if (r0 == 0) goto L_0x0044
            return
        L_0x0017:
            boolean r1 = r3 instanceof com.fossil.M23
            if (r1 != 0) goto L_0x0014
            boolean r1 = r3 instanceof com.fossil.Q13
            if (r1 == 0) goto L_0x0013
            goto L_0x0014
        L_0x0020:
            boolean r1 = r3 instanceof java.lang.Integer
            if (r1 != 0) goto L_0x0014
            boolean r1 = r3 instanceof com.fossil.G13
            if (r1 == 0) goto L_0x0013
            goto L_0x0014
        L_0x0029:
            boolean r1 = r3 instanceof com.fossil.Xz2
            if (r1 != 0) goto L_0x0014
            boolean r1 = r3 instanceof byte[]
            if (r1 == 0) goto L_0x0013
            goto L_0x0014
        L_0x0032:
            boolean r0 = r3 instanceof java.lang.String
            goto L_0x0014
        L_0x0035:
            boolean r0 = r3 instanceof java.lang.Boolean
            goto L_0x0014
        L_0x0038:
            boolean r0 = r3 instanceof java.lang.Double
            goto L_0x0014
        L_0x003b:
            boolean r0 = r3 instanceof java.lang.Float
            goto L_0x0014
        L_0x003e:
            boolean r0 = r3 instanceof java.lang.Long
            goto L_0x0014
        L_0x0041:
            boolean r0 = r3 instanceof java.lang.Integer
            goto L_0x0014
        L_0x0044:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Wrong object type used with protocol message reflection."
            r0.<init>(r1)
            throw r0
            switch-data {1->0x0041, 2->0x003e, 3->0x003b, 4->0x0038, 5->0x0035, 6->0x0032, 7->0x0029, 8->0x0020, 9->0x0017, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.T03.h(com.fossil.L43, java.lang.Object):void");
    }

    @DexIgnore
    public static <T extends V03<T>> boolean i(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        if (key.zzc() == S43.zzi) {
            if (key.zzd()) {
                for (M23 m23 : (List) entry.getValue()) {
                    if (!m23.j()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof M23) {
                    if (!((M23) value).j()) {
                        return false;
                    }
                } else if (value instanceof Q13) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    @DexIgnore
    public static int j(L43 l43, Object obj) {
        switch (W03.b[l43.ordinal()]) {
            case 1:
                return L03.z(((Double) obj).doubleValue());
            case 2:
                return L03.A(((Float) obj).floatValue());
            case 3:
                return L03.e0(((Long) obj).longValue());
            case 4:
                return L03.j0(((Long) obj).longValue());
            case 5:
                return L03.l0(((Integer) obj).intValue());
            case 6:
                return L03.s0(((Long) obj).longValue());
            case 7:
                return L03.x0(((Integer) obj).intValue());
            case 8:
                return L03.L(((Boolean) obj).booleanValue());
            case 9:
                return L03.W((M23) obj);
            case 10:
                return obj instanceof Q13 ? L03.d((Q13) obj) : L03.J((M23) obj);
            case 11:
                return obj instanceof Xz2 ? L03.I((Xz2) obj) : L03.K((String) obj);
            case 12:
                return obj instanceof Xz2 ? L03.I((Xz2) obj) : L03.M((byte[]) obj);
            case 13:
                return L03.p0(((Integer) obj).intValue());
            case 14:
                return L03.A0(((Integer) obj).intValue());
            case 15:
                return L03.w0(((Long) obj).longValue());
            case 16:
                return L03.t0(((Integer) obj).intValue());
            case 17:
                return L03.o0(((Long) obj).longValue());
            case 18:
                return obj instanceof G13 ? L03.C0(((G13) obj).zza()) : L03.C0(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    @DexIgnore
    public static int n(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        Object value = entry.getValue();
        return (key.zzc() != S43.zzi || key.zzd() || key.zze()) ? a(key, value) : value instanceof Q13 ? L03.D(entry.getKey().zza(), (Q13) value) : L03.E(entry.getKey().zza(), (M23) value);
    }

    @DexIgnore
    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        T03 t03 = new T03();
        for (int i = 0; i < this.a.k(); i++) {
            Map.Entry<T, Object> i2 = this.a.i(i);
            t03.l(i2.getKey(), i2.getValue());
        }
        for (Map.Entry<T, Object> entry : this.a.n()) {
            t03.l(entry.getKey(), entry.getValue());
        }
        t03.c = this.c;
        return t03;
    }

    @DexIgnore
    public final Object d(T t) {
        Object obj = this.a.get(t);
        if (!(obj instanceof Q13)) {
            return obj;
        }
        Q13 q13 = (Q13) obj;
        Q13.e();
        throw null;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof T03)) {
            return false;
        }
        return this.a.equals(((T03) obj).a);
    }

    @DexIgnore
    public final void g(T03<T> t03) {
        for (int i = 0; i < t03.a.k(); i++) {
            m(t03.a.i(i));
        }
        for (Map.Entry<T, Object> entry : t03.a.n()) {
            m(entry);
        }
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public final void k() {
        if (!this.b) {
            this.a.f();
            this.b = true;
        }
    }

    @DexIgnore
    public final void l(T t, Object obj) {
        if (!t.zzd()) {
            h(t.zzb(), obj);
        } else if (obj instanceof List) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj2 = arrayList.get(i);
                i++;
                h(t.zzb(), obj2);
            }
            obj = arrayList;
        } else {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
        if (obj instanceof Q13) {
            this.c = true;
        }
        this.a.e(t, obj);
    }

    @DexIgnore
    public final void m(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        Object value = entry.getValue();
        if (value instanceof Q13) {
            Q13 q13 = (Q13) value;
            Q13.e();
            throw null;
        } else if (key.zzd()) {
            Object d2 = d(key);
            if (d2 == null) {
                d2 = new ArrayList();
            }
            for (Object obj : (List) value) {
                ((List) d2).add(e(obj));
            }
            this.a.e(key, d2);
        } else if (key.zzc() == S43.zzi) {
            Object d3 = d(key);
            if (d3 == null) {
                this.a.e(key, e(value));
            } else {
                this.a.e(key, d3 instanceof V23 ? key.g((V23) d3, (V23) value) : key.d(((M23) d3).e(), (M23) value).h());
            }
        } else {
            this.a.e(key, e(value));
        }
    }

    @DexIgnore
    public final boolean o() {
        return this.b;
    }

    @DexIgnore
    public final Iterator<Map.Entry<T, Object>> p() {
        return this.c ? new R13(this.a.entrySet().iterator()) : this.a.entrySet().iterator();
    }

    @DexIgnore
    public final Iterator<Map.Entry<T, Object>> q() {
        return this.c ? new R13(this.a.p().iterator()) : this.a.p().iterator();
    }

    @DexIgnore
    public final boolean r() {
        for (int i = 0; i < this.a.k(); i++) {
            if (!i(this.a.i(i))) {
                return false;
            }
        }
        for (Map.Entry<T, Object> entry : this.a.n()) {
            if (!i(entry)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final int s() {
        int i = 0;
        for (int i2 = 0; i2 < this.a.k(); i2++) {
            i += n(this.a.i(i2));
        }
        for (Map.Entry<T, Object> entry : this.a.n()) {
            i = n(entry) + i;
        }
        return i;
    }
}
