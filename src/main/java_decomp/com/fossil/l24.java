package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class L24<E> extends O24 implements Collection<E> {
    @DexIgnore
    @Override // java.util.Collection
    @CanIgnoreReturnValue
    public boolean add(E e) {
        return delegate().add(e);
    }

    @DexIgnore
    @Override // java.util.Collection
    @CanIgnoreReturnValue
    public boolean addAll(Collection<? extends E> collection) {
        return delegate().addAll(collection);
    }

    @DexIgnore
    public void clear() {
        delegate().clear();
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return delegate().contains(obj);
    }

    @DexIgnore
    @Override // java.util.Collection
    public boolean containsAll(Collection<?> collection) {
        return delegate().containsAll(collection);
    }

    @DexIgnore
    @Override // com.fossil.O24
    public abstract /* bridge */ /* synthetic */ Object delegate();

    @DexIgnore
    @Override // com.fossil.O24
    public abstract Collection<E> delegate();

    @DexIgnore
    public boolean isEmpty() {
        return delegate().isEmpty();
    }

    @DexIgnore
    @Override // java.util.Collection, java.lang.Iterable
    public Iterator<E> iterator() {
        return delegate().iterator();
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public boolean remove(Object obj) {
        return delegate().remove(obj);
    }

    @DexIgnore
    @Override // java.util.Collection
    @CanIgnoreReturnValue
    public boolean removeAll(Collection<?> collection) {
        return delegate().removeAll(collection);
    }

    @DexIgnore
    @Override // java.util.Collection
    @CanIgnoreReturnValue
    public boolean retainAll(Collection<?> collection) {
        return delegate().retainAll(collection);
    }

    @DexIgnore
    public int size() {
        return delegate().size();
    }

    @DexIgnore
    public boolean standardAddAll(Collection<? extends E> collection) {
        return P34.a(this, collection.iterator());
    }

    @DexIgnore
    public void standardClear() {
        P34.e(iterator());
    }

    @DexIgnore
    public boolean standardContains(Object obj) {
        return P34.f(iterator(), obj);
    }

    @DexIgnore
    public boolean standardContainsAll(Collection<?> collection) {
        return B24.b(this, collection);
    }

    @DexIgnore
    public boolean standardIsEmpty() {
        return !iterator().hasNext();
    }

    @DexIgnore
    public boolean standardRemove(Object obj) {
        Iterator<E> it = iterator();
        while (it.hasNext()) {
            if (F14.a(it.next(), obj)) {
                it.remove();
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean standardRemoveAll(Collection<?> collection) {
        return P34.r(iterator(), collection);
    }

    @DexIgnore
    public boolean standardRetainAll(Collection<?> collection) {
        return P34.t(iterator(), collection);
    }

    @DexIgnore
    public Object[] standardToArray() {
        return toArray(new Object[size()]);
    }

    @DexIgnore
    public <T> T[] standardToArray(T[] tArr) {
        return (T[]) H44.g(this, tArr);
    }

    @DexIgnore
    public String standardToString() {
        return B24.e(this);
    }

    @DexIgnore
    public Object[] toArray() {
        return delegate().toArray();
    }

    @DexIgnore
    @Override // java.util.Collection
    @CanIgnoreReturnValue
    public <T> T[] toArray(T[] tArr) {
        return (T[]) delegate().toArray(tArr);
    }
}
