package com.fossil;

import java.io.IOException;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W28 implements Interceptor.Chain {
    @DexIgnore
    public /* final */ List<Interceptor> a;
    @DexIgnore
    public /* final */ P28 b;
    @DexIgnore
    public /* final */ S28 c;
    @DexIgnore
    public /* final */ L28 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ V18 f;
    @DexIgnore
    public /* final */ A18 g;
    @DexIgnore
    public /* final */ M18 h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public int l;

    @DexIgnore
    public W28(List<Interceptor> list, P28 p28, S28 s28, L28 l28, int i2, V18 v18, A18 a18, M18 m18, int i3, int i4, int i5) {
        this.a = list;
        this.d = l28;
        this.b = p28;
        this.c = s28;
        this.e = i2;
        this.f = v18;
        this.g = a18;
        this.h = m18;
        this.i = i3;
        this.j = i4;
        this.k = i5;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public int a() {
        return this.j;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public int b() {
        return this.k;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public V18 c() {
        return this.f;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public Response d(V18 v18) throws IOException {
        return j(v18, this.b, this.c, this.d);
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public E18 e() {
        return this.d;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor.Chain
    public int f() {
        return this.i;
    }

    @DexIgnore
    public A18 g() {
        return this.g;
    }

    @DexIgnore
    public M18 h() {
        return this.h;
    }

    @DexIgnore
    public S28 i() {
        return this.c;
    }

    @DexIgnore
    public Response j(V18 v18, P28 p28, S28 s28, L28 l28) throws IOException {
        if (this.e < this.a.size()) {
            this.l++;
            if (this.c != null && !this.d.t(v18.j())) {
                throw new IllegalStateException("network interceptor " + this.a.get(this.e - 1) + " must retain the same host and port");
            } else if (this.c == null || this.l <= 1) {
                W28 w28 = new W28(this.a, p28, s28, l28, this.e + 1, v18, this.g, this.h, this.i, this.j, this.k);
                Interceptor interceptor = this.a.get(this.e);
                Response intercept = interceptor.intercept(w28);
                if (s28 != null && this.e + 1 < this.a.size() && w28.l != 1) {
                    throw new IllegalStateException("network interceptor " + interceptor + " must call proceed() exactly once");
                } else if (intercept == null) {
                    throw new NullPointerException("interceptor " + interceptor + " returned null");
                } else if (intercept.a() != null) {
                    return intercept;
                } else {
                    throw new IllegalStateException("interceptor " + interceptor + " returned a response with no body");
                }
            } else {
                throw new IllegalStateException("network interceptor " + this.a.get(this.e - 1) + " must call proceed() exactly once");
            }
        } else {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public P28 k() {
        return this.b;
    }
}
