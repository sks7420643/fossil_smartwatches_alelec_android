package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ig0;
import com.fossil.Yo0;
import com.google.android.material.internal.NavigationMenuItemView;
import com.google.android.material.internal.NavigationMenuView;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dz3 implements Ig0 {
    @DexIgnore
    public int A; // = -1;
    @DexIgnore
    public /* final */ View.OnClickListener B; // = new Ai();
    @DexIgnore
    public NavigationMenuView b;
    @DexIgnore
    public LinearLayout c;
    @DexIgnore
    public Ig0.Ai d;
    @DexIgnore
    public Cg0 e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Ci g;
    @DexIgnore
    public LayoutInflater h;
    @DexIgnore
    public int i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public ColorStateList l;
    @DexIgnore
    public Drawable m;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w; // = true;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements View.OnClickListener {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void onClick(View view) {
            boolean z = true;
            Dz3.this.J(true);
            Eg0 itemData = ((NavigationMenuItemView) view).getItemData();
            Dz3 dz3 = Dz3.this;
            boolean O = dz3.e.O(itemData, dz3, 0);
            if (itemData == null || !itemData.isCheckable() || !O) {
                z = false;
            } else {
                Dz3.this.g.p(itemData);
            }
            Dz3.this.J(false);
            if (z) {
                Dz3.this.c(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi extends Li {
        @DexIgnore
        public Bi(View view) {
            super(view);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends RecyclerView.g<Li> {
        @DexIgnore
        public /* final */ ArrayList<Ei> a; // = new ArrayList<>();
        @DexIgnore
        public Eg0 b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public Ci() {
            n();
        }

        @DexIgnore
        public final void g(int i, int i2) {
            while (i < i2) {
                ((Gi) this.a.get(i)).b = true;
                i++;
            }
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public int getItemCount() {
            return this.a.size();
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public int getItemViewType(int i) {
            Ei ei = this.a.get(i);
            if (ei instanceof Fi) {
                return 2;
            }
            if (ei instanceof Di) {
                return 3;
            }
            if (ei instanceof Gi) {
                return ((Gi) ei).a().hasSubMenu() ? 1 : 0;
            }
            throw new RuntimeException("Unknown item type.");
        }

        @DexIgnore
        public Bundle h() {
            Bundle bundle = new Bundle();
            Eg0 eg0 = this.b;
            if (eg0 != null) {
                bundle.putInt("android:menu:checked", eg0.getItemId());
            }
            SparseArray<? extends Parcelable> sparseArray = new SparseArray<>();
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                Ei ei = this.a.get(i);
                if (ei instanceof Gi) {
                    Eg0 a2 = ((Gi) ei).a();
                    View actionView = a2 != null ? a2.getActionView() : null;
                    if (actionView != null) {
                        Fz3 fz3 = new Fz3();
                        actionView.saveHierarchyState(fz3);
                        sparseArray.put(a2.getItemId(), fz3);
                    }
                }
            }
            bundle.putSparseParcelableArray("android:menu:action_views", sparseArray);
            return bundle;
        }

        @DexIgnore
        public Eg0 i() {
            return this.b;
        }

        @DexIgnore
        public int j() {
            int i;
            int i2;
            if (Dz3.this.c.getChildCount() == 0) {
                i2 = 0;
                i = 0;
            } else {
                i = 1;
                i2 = 0;
            }
            while (i2 < Dz3.this.g.getItemCount()) {
                int i3 = Dz3.this.g.getItemViewType(i2) == 0 ? i + 1 : i;
                i2++;
                i = i3;
            }
            return i;
        }

        @DexIgnore
        public void k(Li li, int i) {
            int itemViewType = getItemViewType(i);
            if (itemViewType == 0) {
                NavigationMenuItemView navigationMenuItemView = (NavigationMenuItemView) li.itemView;
                navigationMenuItemView.setIconTintList(Dz3.this.l);
                Dz3 dz3 = Dz3.this;
                if (dz3.j) {
                    navigationMenuItemView.setTextAppearance(dz3.i);
                }
                ColorStateList colorStateList = Dz3.this.k;
                if (colorStateList != null) {
                    navigationMenuItemView.setTextColor(colorStateList);
                }
                Drawable drawable = Dz3.this.m;
                Mo0.o0(navigationMenuItemView, drawable != null ? drawable.getConstantState().newDrawable() : null);
                Gi gi = (Gi) this.a.get(i);
                navigationMenuItemView.setNeedsEmptyIcon(gi.b);
                navigationMenuItemView.setHorizontalPadding(Dz3.this.s);
                navigationMenuItemView.setIconPadding(Dz3.this.t);
                Dz3 dz32 = Dz3.this;
                if (dz32.v) {
                    navigationMenuItemView.setIconSize(dz32.u);
                }
                navigationMenuItemView.setMaxLines(Dz3.this.x);
                navigationMenuItemView.f(gi.a(), 0);
            } else if (itemViewType == 1) {
                ((TextView) li.itemView).setText(((Gi) this.a.get(i)).a().getTitle());
            } else if (itemViewType == 2) {
                Fi fi = (Fi) this.a.get(i);
                li.itemView.setPadding(0, fi.b(), 0, fi.a());
            }
        }

        @DexIgnore
        public Li l(ViewGroup viewGroup, int i) {
            if (i == 0) {
                Dz3 dz3 = Dz3.this;
                return new Ii(dz3.h, viewGroup, dz3.B);
            } else if (i == 1) {
                return new Ki(Dz3.this.h, viewGroup);
            } else {
                if (i == 2) {
                    return new Ji(Dz3.this.h, viewGroup);
                }
                if (i != 3) {
                    return null;
                }
                return new Bi(Dz3.this.c);
            }
        }

        @DexIgnore
        public void m(Li li) {
            if (li instanceof Ii) {
                ((NavigationMenuItemView) li.itemView).g();
            }
        }

        @DexIgnore
        public final void n() {
            int i;
            boolean z;
            int i2;
            if (!this.c) {
                this.c = true;
                this.a.clear();
                this.a.add(new Di());
                int i3 = -1;
                int size = Dz3.this.e.G().size();
                boolean z2 = false;
                int i4 = 0;
                int i5 = 0;
                while (i5 < size) {
                    Eg0 eg0 = Dz3.this.e.G().get(i5);
                    if (eg0.isChecked()) {
                        p(eg0);
                    }
                    if (eg0.isCheckable()) {
                        eg0.t(false);
                    }
                    if (eg0.hasSubMenu()) {
                        SubMenu subMenu = eg0.getSubMenu();
                        if (subMenu.hasVisibleItems()) {
                            if (i5 != 0) {
                                this.a.add(new Fi(Dz3.this.z, 0));
                            }
                            this.a.add(new Gi(eg0));
                            int size2 = this.a.size();
                            int size3 = subMenu.size();
                            boolean z3 = false;
                            for (int i6 = 0; i6 < size3; i6++) {
                                Eg0 eg02 = (Eg0) subMenu.getItem(i6);
                                if (eg02.isVisible()) {
                                    if (!z3 && eg02.getIcon() != null) {
                                        z3 = true;
                                    }
                                    if (eg02.isCheckable()) {
                                        eg02.t(false);
                                    }
                                    if (eg0.isChecked()) {
                                        p(eg0);
                                    }
                                    this.a.add(new Gi(eg02));
                                }
                            }
                            if (z3) {
                                g(size2, this.a.size());
                                i2 = i3;
                            }
                        }
                        i2 = i3;
                    } else {
                        int groupId = eg0.getGroupId();
                        if (groupId != i3) {
                            i = this.a.size();
                            z = eg0.getIcon() != null;
                            if (i5 != 0) {
                                i++;
                                ArrayList<Ei> arrayList = this.a;
                                int i7 = Dz3.this.z;
                                arrayList.add(new Fi(i7, i7));
                            }
                        } else if (z2 || eg0.getIcon() == null) {
                            i = i4;
                            z = z2;
                        } else {
                            g(i4, this.a.size());
                            z = true;
                            i = i4;
                        }
                        Gi gi = new Gi(eg0);
                        gi.b = z;
                        this.a.add(gi);
                        i4 = i;
                        z2 = z;
                        i2 = groupId;
                    }
                    i5++;
                    i3 = i2;
                }
                this.c = false;
            }
        }

        @DexIgnore
        public void o(Bundle bundle) {
            Eg0 a2;
            View actionView;
            Fz3 fz3;
            Eg0 a3;
            int i = bundle.getInt("android:menu:checked", 0);
            if (i != 0) {
                this.c = true;
                int size = this.a.size();
                int i2 = 0;
                while (true) {
                    if (i2 >= size) {
                        break;
                    }
                    Ei ei = this.a.get(i2);
                    if ((ei instanceof Gi) && (a3 = ((Gi) ei).a()) != null && a3.getItemId() == i) {
                        p(a3);
                        break;
                    }
                    i2++;
                }
                this.c = false;
                n();
            }
            SparseArray sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:action_views");
            if (sparseParcelableArray != null) {
                int size2 = this.a.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    Ei ei2 = this.a.get(i3);
                    if (!(!(ei2 instanceof Gi) || (a2 = ((Gi) ei2).a()) == null || (actionView = a2.getActionView()) == null || (fz3 = (Fz3) sparseParcelableArray.get(a2.getItemId())) == null)) {
                        actionView.restoreHierarchyState(fz3);
                    }
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public /* bridge */ /* synthetic */ void onBindViewHolder(Li li, int i) {
            k(li, i);
        }

        @DexIgnore
        /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public /* bridge */ /* synthetic */ Li onCreateViewHolder(ViewGroup viewGroup, int i) {
            return l(viewGroup, i);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder] */
        @Override // androidx.recyclerview.widget.RecyclerView.g
        public /* bridge */ /* synthetic */ void onViewRecycled(Li li) {
            m(li);
        }

        @DexIgnore
        public void p(Eg0 eg0) {
            if (this.b != eg0 && eg0.isCheckable()) {
                Eg0 eg02 = this.b;
                if (eg02 != null) {
                    eg02.setChecked(false);
                }
                this.b = eg0;
                eg0.setChecked(true);
            }
        }

        @DexIgnore
        public void q(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public void r() {
            n();
            notifyDataSetChanged();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di implements Ei {
    }

    @DexIgnore
    public interface Ei {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Fi implements Ei {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Fi(int i, int i2) {
            this.a = i;
            this.b = i2;
        }

        @DexIgnore
        public int a() {
            return this.b;
        }

        @DexIgnore
        public int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Gi implements Ei {
        @DexIgnore
        public /* final */ Eg0 a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public Gi(Eg0 eg0) {
            this.a = eg0;
        }

        @DexIgnore
        public Eg0 a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Hi extends Nv0 {
        @DexIgnore
        public Hi(RecyclerView recyclerView) {
            super(recyclerView);
        }

        @DexIgnore
        @Override // com.fossil.Rn0, com.fossil.Nv0
        public void g(View view, Yo0 yo0) {
            super.g(view, yo0);
            yo0.e0(Yo0.Bi.a(Dz3.this.g.j(), 0, false));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ii extends Li {
        @DexIgnore
        public Ii(LayoutInflater layoutInflater, ViewGroup viewGroup, View.OnClickListener onClickListener) {
            super(layoutInflater.inflate(Pw3.design_navigation_item, viewGroup, false));
            this.itemView.setOnClickListener(onClickListener);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ji extends Li {
        @DexIgnore
        public Ji(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(Pw3.design_navigation_item_separator, viewGroup, false));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ki extends Li {
        @DexIgnore
        public Ki(LayoutInflater layoutInflater, ViewGroup viewGroup) {
            super(layoutInflater.inflate(Pw3.design_navigation_item_subheader, viewGroup, false));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Li extends RecyclerView.ViewHolder {
        @DexIgnore
        public Li(View view) {
            super(view);
        }
    }

    @DexIgnore
    public void A(Drawable drawable) {
        this.m = drawable;
        c(false);
    }

    @DexIgnore
    public void B(int i2) {
        this.s = i2;
        c(false);
    }

    @DexIgnore
    public void C(int i2) {
        this.t = i2;
        c(false);
    }

    @DexIgnore
    public void D(int i2) {
        if (this.u != i2) {
            this.u = i2;
            this.v = true;
            c(false);
        }
    }

    @DexIgnore
    public void E(ColorStateList colorStateList) {
        this.l = colorStateList;
        c(false);
    }

    @DexIgnore
    public void F(int i2) {
        this.x = i2;
        c(false);
    }

    @DexIgnore
    public void G(int i2) {
        this.i = i2;
        this.j = true;
        c(false);
    }

    @DexIgnore
    public void H(ColorStateList colorStateList) {
        this.k = colorStateList;
        c(false);
    }

    @DexIgnore
    public void I(int i2) {
        this.A = i2;
        NavigationMenuView navigationMenuView = this.b;
        if (navigationMenuView != null) {
            navigationMenuView.setOverScrollMode(i2);
        }
    }

    @DexIgnore
    public void J(boolean z2) {
        Ci ci = this.g;
        if (ci != null) {
            ci.q(z2);
        }
    }

    @DexIgnore
    public final void K() {
        int i2 = (this.c.getChildCount() != 0 || !this.w) ? 0 : this.y;
        NavigationMenuView navigationMenuView = this.b;
        navigationMenuView.setPadding(0, i2, 0, navigationMenuView.getPaddingBottom());
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void b(Cg0 cg0, boolean z2) {
        Ig0.Ai ai = this.d;
        if (ai != null) {
            ai.b(cg0, z2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void c(boolean z2) {
        Ci ci = this.g;
        if (ci != null) {
            ci.r();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean d() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean e(Cg0 cg0, Eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean f(Cg0 cg0, Eg0 eg0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void g(Ig0.Ai ai) {
        this.d = ai;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public int getId() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void h(Context context, Cg0 cg0) {
        this.h = LayoutInflater.from(context);
        this.e = cg0;
        this.z = context.getResources().getDimensionPixelOffset(Lw3.design_navigation_separator_vertical_padding);
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public void i(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            SparseArray<Parcelable> sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
            if (sparseParcelableArray != null) {
                this.b.restoreHierarchyState(sparseParcelableArray);
            }
            Bundle bundle2 = bundle.getBundle("android:menu:adapter");
            if (bundle2 != null) {
                this.g.o(bundle2);
            }
            SparseArray sparseParcelableArray2 = bundle.getSparseParcelableArray("android:menu:header");
            if (sparseParcelableArray2 != null) {
                this.c.restoreHierarchyState(sparseParcelableArray2);
            }
        }
    }

    @DexIgnore
    public void j(View view) {
        this.c.addView(view);
        NavigationMenuView navigationMenuView = this.b;
        navigationMenuView.setPadding(0, 0, 0, navigationMenuView.getPaddingBottom());
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public boolean k(Ng0 ng0) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ig0
    public Parcelable l() {
        Bundle bundle = new Bundle();
        if (this.b != null) {
            SparseArray<Parcelable> sparseArray = new SparseArray<>();
            this.b.saveHierarchyState(sparseArray);
            bundle.putSparseParcelableArray("android:menu:list", sparseArray);
        }
        Ci ci = this.g;
        if (ci != null) {
            bundle.putBundle("android:menu:adapter", ci.h());
        }
        if (this.c != null) {
            SparseArray<? extends Parcelable> sparseArray2 = new SparseArray<>();
            this.c.saveHierarchyState(sparseArray2);
            bundle.putSparseParcelableArray("android:menu:header", sparseArray2);
        }
        return bundle;
    }

    @DexIgnore
    public void m(Vo0 vo0) {
        int g2 = vo0.g();
        if (this.y != g2) {
            this.y = g2;
            K();
        }
        NavigationMenuView navigationMenuView = this.b;
        navigationMenuView.setPadding(0, navigationMenuView.getPaddingTop(), 0, vo0.d());
        Mo0.g(this.c, vo0);
    }

    @DexIgnore
    public Eg0 n() {
        return this.g.i();
    }

    @DexIgnore
    public int o() {
        return this.c.getChildCount();
    }

    @DexIgnore
    public Drawable p() {
        return this.m;
    }

    @DexIgnore
    public int q() {
        return this.s;
    }

    @DexIgnore
    public int r() {
        return this.t;
    }

    @DexIgnore
    public int s() {
        return this.x;
    }

    @DexIgnore
    public ColorStateList t() {
        return this.k;
    }

    @DexIgnore
    public ColorStateList u() {
        return this.l;
    }

    @DexIgnore
    public Jg0 v(ViewGroup viewGroup) {
        if (this.b == null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) this.h.inflate(Pw3.design_navigation_menu, viewGroup, false);
            this.b = navigationMenuView;
            navigationMenuView.setAccessibilityDelegateCompat(new Hi(this.b));
            if (this.g == null) {
                this.g = new Ci();
            }
            int i2 = this.A;
            if (i2 != -1) {
                this.b.setOverScrollMode(i2);
            }
            this.c = (LinearLayout) this.h.inflate(Pw3.design_navigation_item_header, (ViewGroup) this.b, false);
            this.b.setAdapter(this.g);
        }
        return this.b;
    }

    @DexIgnore
    public View w(int i2) {
        View inflate = this.h.inflate(i2, (ViewGroup) this.c, false);
        j(inflate);
        return inflate;
    }

    @DexIgnore
    public void x(boolean z2) {
        if (this.w != z2) {
            this.w = z2;
            K();
        }
    }

    @DexIgnore
    public void y(Eg0 eg0) {
        this.g.p(eg0);
    }

    @DexIgnore
    public void z(int i2) {
        this.f = i2;
    }
}
