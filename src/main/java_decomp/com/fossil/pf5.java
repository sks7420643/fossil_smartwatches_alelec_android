package com.fossil;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Pf5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ RTLImageView t;
    @DexIgnore
    public /* final */ View u;

    @DexIgnore
    public Pf5(Object obj, View view, int i, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, RTLImageView rTLImageView, View view2) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = flexibleTextView2;
        this.s = flexibleTextView3;
        this.t = rTLImageView;
        this.u = view2;
    }
}
