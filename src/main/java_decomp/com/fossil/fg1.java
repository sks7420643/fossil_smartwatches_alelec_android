package com.fossil;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Fg1 {
    @DexIgnore
    public static /* final */ Fg1 a; // = new Ci();
    @DexIgnore
    public static /* final */ Fg1 b; // = new Ai();
    @DexIgnore
    public static /* final */ Fg1 c; // = new Bi();
    @DexIgnore
    public static /* final */ Fg1 d; // = new Di();
    @DexIgnore
    public static /* final */ Fg1 e;
    @DexIgnore
    public static /* final */ Nb1<Fg1> f;
    @DexIgnore
    public static /* final */ boolean g; // = (Build.VERSION.SDK_INT >= 19);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends Fg1 {
        @DexIgnore
        @Override // com.fossil.Fg1
        public Ei a(int i, int i2, int i3, int i4) {
            return b(i, i2, i3, i4) == 1.0f ? Ei.QUALITY : Fg1.a.a(i, i2, i3, i4);
        }

        @DexIgnore
        @Override // com.fossil.Fg1
        public float b(int i, int i2, int i3, int i4) {
            return Math.min(1.0f, Fg1.a.b(i, i2, i3, i4));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi extends Fg1 {
        @DexIgnore
        @Override // com.fossil.Fg1
        public Ei a(int i, int i2, int i3, int i4) {
            return Ei.QUALITY;
        }

        @DexIgnore
        @Override // com.fossil.Fg1
        public float b(int i, int i2, int i3, int i4) {
            return Math.max(((float) i3) / ((float) i), ((float) i4) / ((float) i2));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci extends Fg1 {
        @DexIgnore
        @Override // com.fossil.Fg1
        public Ei a(int i, int i2, int i3, int i4) {
            return Fg1.g ? Ei.QUALITY : Ei.MEMORY;
        }

        @DexIgnore
        @Override // com.fossil.Fg1
        public float b(int i, int i2, int i3, int i4) {
            if (Fg1.g) {
                return Math.min(((float) i3) / ((float) i), ((float) i4) / ((float) i2));
            }
            int max = Math.max(i2 / i4, i / i3);
            if (max != 0) {
                return 1.0f / ((float) Integer.highestOneBit(max));
            }
            return 1.0f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di extends Fg1 {
        @DexIgnore
        @Override // com.fossil.Fg1
        public Ei a(int i, int i2, int i3, int i4) {
            return Ei.QUALITY;
        }

        @DexIgnore
        @Override // com.fossil.Fg1
        public float b(int i, int i2, int i3, int i4) {
            return 1.0f;
        }
    }

    @DexIgnore
    public enum Ei {
        MEMORY,
        QUALITY
    }

    /*
    static {
        Fg1 fg1 = c;
        e = fg1;
        f = Nb1.f("com.bumptech.glide.load.resource.bitmap.Downsampler.DownsampleStrategy", fg1);
    }
    */

    @DexIgnore
    public abstract Ei a(int i, int i2, int i3, int i4);

    @DexIgnore
    public abstract float b(int i, int i2, int i3, int i4);
}
