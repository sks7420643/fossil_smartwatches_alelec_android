package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G63 implements D63 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a;
    @DexIgnore
    public static /* final */ Xv2<Boolean> b;
    @DexIgnore
    public static /* final */ Xv2<Boolean> c;
    @DexIgnore
    public static /* final */ Xv2<Boolean> d;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        a = hw2.d("measurement.service.audience.fix_skip_audience_with_failed_filters", true);
        b = hw2.d("measurement.audience.refresh_event_count_filters_timestamp", false);
        c = hw2.d("measurement.audience.use_bundle_end_timestamp_for_non_sequence_property_filters", false);
        d = hw2.d("measurement.audience.use_bundle_timestamp_for_event_count_filters", false);
    }
    */

    @DexIgnore
    @Override // com.fossil.D63
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.D63
    public final boolean zzb() {
        return a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.D63
    public final boolean zzc() {
        return b.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.D63
    public final boolean zzd() {
        return c.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.D63
    public final boolean zze() {
        return d.o().booleanValue();
    }
}
