package com.fossil;

import android.content.Context;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ub4 implements Tb4 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public Ub4(Context context) {
        this.a = context;
    }

    @DexIgnore
    @Override // com.fossil.Tb4
    public String a() {
        return new File(this.a.getFilesDir(), ".com.google.firebase.crashlytics").getPath();
    }

    @DexIgnore
    @Override // com.fossil.Tb4
    public File b() {
        return c(new File(this.a.getFilesDir(), ".com.google.firebase.crashlytics"));
    }

    @DexIgnore
    public File c(File file) {
        if (file == null) {
            X74.f().b("Null File");
        } else if (file.exists() || file.mkdirs()) {
            return file;
        } else {
            X74.f().i("Couldn't create file");
        }
        return null;
    }
}
