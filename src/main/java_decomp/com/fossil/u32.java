package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class U32 {
    @DexIgnore
    public static T32 a() {
        return new Y32();
    }

    @DexIgnore
    public static T32 b() {
        return new X32();
    }
}
