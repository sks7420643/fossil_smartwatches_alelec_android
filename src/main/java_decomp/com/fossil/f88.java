package com.fossil;

import com.fossil.B88;
import com.mapped.Dx6;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.Executor;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F88 extends B88.Ai {
    @DexIgnore
    public /* final */ Executor a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements B88<Object, Call<?>> {
        @DexIgnore
        public /* final */ /* synthetic */ Type a;
        @DexIgnore
        public /* final */ /* synthetic */ Executor b;

        @DexIgnore
        public Ai(F88 f88, Type type, Executor executor) {
            this.a = type;
            this.b = executor;
        }

        @DexIgnore
        @Override // com.fossil.B88
        public Type a() {
            return this.a;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [retrofit2.Call] */
        @Override // com.fossil.B88
        public /* bridge */ /* synthetic */ Call<?> b(Call<Object> call) {
            return c(call);
        }

        @DexIgnore
        public Call<Object> c(Call<Object> call) {
            Executor executor = this.b;
            return executor == null ? call : new Bi(executor, call);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Call<T> {
        @DexIgnore
        public /* final */ Executor b;
        @DexIgnore
        public /* final */ Call<T> c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Dx6<T> {
            @DexIgnore
            public /* final */ /* synthetic */ Dx6 a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public class Aiii implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ Q88 b;

                @DexIgnore
                public Aiii(Q88 q88) {
                    this.b = q88;
                }

                @DexIgnore
                public void run() {
                    if (Bi.this.c.f()) {
                        Aii aii = Aii.this;
                        aii.a.onFailure(Bi.this, new IOException("Canceled"));
                        return;
                    }
                    Aii aii2 = Aii.this;
                    aii2.a.onResponse(Bi.this, this.b);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public class Biii implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ Throwable b;

                @DexIgnore
                public Biii(Throwable th) {
                    this.b = th;
                }

                @DexIgnore
                public void run() {
                    Aii aii = Aii.this;
                    aii.a.onFailure(Bi.this, this.b);
                }
            }

            @DexIgnore
            public Aii(Dx6 dx6) {
                this.a = dx6;
            }

            @DexIgnore
            @Override // com.mapped.Dx6
            public void onFailure(Call<T> call, Throwable th) {
                Bi.this.b.execute(new Biii(th));
            }

            @DexIgnore
            @Override // com.mapped.Dx6
            public void onResponse(Call<T> call, Q88<T> q88) {
                Bi.this.b.execute(new Aiii(q88));
            }
        }

        @DexIgnore
        public Bi(Executor executor, Call<T> call) {
            this.b = executor;
            this.c = call;
        }

        @DexIgnore
        @Override // retrofit2.Call
        public void D(Dx6<T> dx6) {
            U88.b(dx6, "callback == null");
            this.c.D(new Aii(dx6));
        }

        @DexIgnore
        @Override // retrofit2.Call
        public Call<T> L() {
            return new Bi(this.b, this.c.L());
        }

        @DexIgnore
        @Override // retrofit2.Call
        public Q88<T> a() throws IOException {
            return this.c.a();
        }

        @DexIgnore
        @Override // retrofit2.Call
        public V18 c() {
            return this.c.c();
        }

        @DexIgnore
        @Override // retrofit2.Call
        public void cancel() {
            this.c.cancel();
        }

        @DexIgnore
        @Override // java.lang.Object
        public /* bridge */ /* synthetic */ Object clone() throws CloneNotSupportedException {
            return L();
        }

        @DexIgnore
        @Override // retrofit2.Call
        public boolean f() {
            return this.c.f();
        }
    }

    @DexIgnore
    public F88(Executor executor) {
        this.a = executor;
    }

    @DexIgnore
    @Override // com.fossil.B88.Ai
    public B88<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        Executor executor = null;
        if (B88.Ai.c(type) != Call.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type h = U88.h(0, (ParameterizedType) type);
            if (!U88.m(annotationArr, S88.class)) {
                executor = this.a;
            }
            return new Ai(this, h, executor);
        }
        throw new IllegalArgumentException("Call return type must be parameterized as Call<Foo> or Call<? extends Foo>");
    }
}
