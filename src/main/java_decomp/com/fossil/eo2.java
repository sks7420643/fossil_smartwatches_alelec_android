package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eo2 extends Tn2 implements Fo2 {
    @DexIgnore
    public Eo2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitHistoryApi");
    }

    @DexIgnore
    @Override // com.fossil.Fo2
    public final void E(Cj2 cj2) throws RemoteException {
        Parcel d = d();
        Qo2.b(d, cj2);
        e(2, d);
    }
}
