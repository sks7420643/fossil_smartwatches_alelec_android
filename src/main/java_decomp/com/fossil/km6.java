package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Jl6;
import com.google.android.material.appbar.AppBarLayout;
import com.mapped.Cf;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Km6 extends BaseFragment implements Jm6, View.OnClickListener, Jl6.Ai {
    @DexIgnore
    public static /* final */ Ai y; // = new Ai(null);
    @DexIgnore
    public G37<Z65> g;
    @DexIgnore
    public Im6 h;
    @DexIgnore
    public Date i; // = new Date();
    @DexIgnore
    public Jl6 j;
    @DexIgnore
    public /* final */ Calendar k; // = Calendar.getInstance();
    @DexIgnore
    public String l;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public HashMap x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Km6 a(Date date) {
            Wg6.c(date, "date");
            Km6 km6 = new Km6();
            Bundle bundle = new Bundle();
            bundle.putLong("KEY_LONG_TIME", date.getTime());
            km6.setArguments(bundle);
            return km6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends AppBarLayout.Behavior.a {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ Cf b;

        @DexIgnore
        public Bi(Km6 km6, boolean z, Cf cf, Ai5 ai5) {
            this.a = z;
            this.b = cf;
        }

        @DexIgnore
        @Override // com.google.android.material.appbar.AppBarLayout.BaseBehavior.b
        public boolean a(AppBarLayout appBarLayout) {
            Wg6.c(appBarLayout, "appBarLayout");
            return this.a && (this.b.isEmpty() ^ true);
        }
    }

    @DexIgnore
    public Km6() {
        String d = ThemeManager.l.a().d("nonBrandSurface");
        this.m = Color.parseColor(d == null ? "#FFFFFF" : d);
        String d2 = ThemeManager.l.a().d("backgroundDashboard");
        this.s = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = ThemeManager.l.a().d("secondaryText");
        this.t = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = ThemeManager.l.a().d("primaryText");
        this.u = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
        String d5 = ThemeManager.l.a().d("nonBrandNonReachGoal");
        this.v = Color.parseColor(d5 == null ? "#FFFFFF" : d5);
        String d6 = ThemeManager.l.a().d("nonBrandActivityDetailBackground");
        this.w = Color.parseColor(d6 == null ? "#FFFFFF" : d6);
    }

    @DexIgnore
    @Override // com.fossil.Jm6
    public void A5(int i2, int i3) {
        G37<Z65> g37 = this.g;
        if (g37 != null) {
            Z65 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (i2 == 0 && i3 == 0) {
                FlexibleTextView flexibleTextView = a2.A;
                Wg6.b(flexibleTextView, "it.ftvNoRecord");
                flexibleTextView.setVisibility(0);
                ConstraintLayout constraintLayout = a2.s;
                Wg6.b(constraintLayout, "it.clContainer");
                constraintLayout.setVisibility(8);
                return;
            }
            FlexibleTextView flexibleTextView2 = a2.A;
            Wg6.b(flexibleTextView2, "it.ftvNoRecord");
            flexibleTextView2.setVisibility(8);
            ConstraintLayout constraintLayout2 = a2.s;
            Wg6.b(constraintLayout2, "it.clContainer");
            constraintLayout2.setVisibility(0);
            FlexibleTextView flexibleTextView3 = a2.D;
            Wg6.b(flexibleTextView3, "it.ftvRestingValue");
            flexibleTextView3.setText(String.valueOf(i2));
            FlexibleTextView flexibleTextView4 = a2.z;
            Wg6.b(flexibleTextView4, "it.ftvMaxValue");
            flexibleTextView4.setText(String.valueOf(i3));
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Jl6.Ai
    public void B2(WorkoutSession workoutSession) {
        String name;
        Ai5 n;
        Wg6.c(workoutSession, "workoutSession");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Im6 im6 = this.h;
            if (im6 != null) {
                if (im6 == null || (n = im6.n()) == null || (name = n.name()) == null) {
                    name = Ai5.METRIC.name();
                }
                WorkoutDetailActivity.a aVar = WorkoutDetailActivity.f;
                Wg6.b(activity, "it");
                aVar.a(activity, workoutSession.getId(), name);
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "HeartRateDetailFragment";
    }

    @DexIgnore
    public final void K6(Z65 z65) {
        z65.F.setOnClickListener(this);
        z65.G.setOnClickListener(this);
        z65.H.setOnClickListener(this);
        this.l = ThemeManager.l.a().d("dianaHeartRateTab");
        Jl6 jl6 = new Jl6(Jl6.Ci.HEART_RATE, Ai5.IMPERIAL, new WorkoutSessionDifference(), this.l);
        this.j = jl6;
        if (jl6 != null) {
            jl6.u(this);
        }
        RecyclerView recyclerView = z65.L;
        Wg6.b(recyclerView, "it");
        recyclerView.setAdapter(this.j);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        Drawable f = W6.f(recyclerView.getContext(), 2131230857);
        if (f != null) {
            Ok6 ok6 = new Ok6(linearLayoutManager.q2(), false, false, 6, null);
            Wg6.b(f, ResourceManager.DRAWABLE);
            ok6.h(f);
            recyclerView.addItemDecoration(ok6);
        }
        z65.J.setBackgroundColor(this.s);
        z65.r.setBackgroundColor(this.m);
        z65.D.setTextColor(this.u);
        z65.C.setTextColor(this.v);
        z65.z.setTextColor(this.u);
        z65.y.setTextColor(this.v);
        z65.x.setTextColor(this.t);
        z65.w.setTextColor(this.u);
        z65.t.setBackgroundColor(this.w);
        z65.v.setBackgroundColor(this.s);
    }

    @DexIgnore
    public final void L6() {
        TodayHeartRateChart todayHeartRateChart;
        G37<Z65> g37 = this.g;
        if (g37 != null) {
            Z65 a2 = g37.a();
            if (a2 != null && (todayHeartRateChart = a2.v) != null) {
                todayHeartRateChart.o("maxHeartRate", "lowestHeartRate");
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Im6 im6) {
        M6(im6);
    }

    @DexIgnore
    public void M6(Im6 im6) {
        Wg6.c(im6, "presenter");
        this.h = im6;
        Lifecycle lifecycle = getLifecycle();
        Im6 im62 = this.h;
        if (im62 == null) {
            Wg6.n("mPresenter");
            throw null;
        } else if (im62 != null) {
            lifecycle.a(im62.o());
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Jm6
    public void b0(int i2, List<W57> list, List<Gl7<Integer, Lc6<Integer, Float>, String>> list2) {
        TodayHeartRateChart todayHeartRateChart;
        Wg6.c(list, "listTodayHeartRateModel");
        Wg6.c(list2, "listTimeZoneChange");
        G37<Z65> g37 = this.g;
        if (g37 != null) {
            Z65 a2 = g37.a();
            if (a2 != null && (todayHeartRateChart = a2.v) != null) {
                todayHeartRateChart.setDayInMinuteWithTimeZone(i2);
                todayHeartRateChart.setListTimeZoneChange(list2);
                todayHeartRateChart.m(list);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Jm6
    public void j(Date date, boolean z, boolean z2, boolean z3) {
        Wg6.c(date, "date");
        this.i = date;
        Calendar calendar = this.k;
        Wg6.b(calendar, "calendar");
        calendar.setTime(date);
        int i2 = this.k.get(7);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateDetailFragment", "showDay - date=" + date + " - isCreateAt: " + z + " - isToday - " + z2 + " - isDateAfter: " + z3 + " - calendar: " + this.k);
        G37<Z65> g37 = this.g;
        if (g37 != null) {
            Z65 a2 = g37.a();
            if (a2 != null) {
                a2.q.r(true, true);
                FlexibleTextView flexibleTextView = a2.w;
                Wg6.b(flexibleTextView, "binding.ftvDayOfMonth");
                flexibleTextView.setText(String.valueOf(this.k.get(5)));
                if (z) {
                    ImageView imageView = a2.G;
                    Wg6.b(imageView, "binding.ivBackDate");
                    imageView.setVisibility(4);
                } else {
                    ImageView imageView2 = a2.G;
                    Wg6.b(imageView2, "binding.ivBackDate");
                    imageView2.setVisibility(0);
                }
                if (z2 || z3) {
                    ImageView imageView3 = a2.H;
                    Wg6.b(imageView3, "binding.ivNextDate");
                    imageView3.setVisibility(8);
                    if (z2) {
                        FlexibleTextView flexibleTextView2 = a2.x;
                        Wg6.b(flexibleTextView2, "binding.ftvDayOfWeek");
                        flexibleTextView2.setText(Um5.c(getContext(), 2131886662));
                        return;
                    }
                    FlexibleTextView flexibleTextView3 = a2.x;
                    Wg6.b(flexibleTextView3, "binding.ftvDayOfWeek");
                    flexibleTextView3.setText(Jl5.b.i(i2));
                    return;
                }
                ImageView imageView4 = a2.H;
                Wg6.b(imageView4, "binding.ivNextDate");
                imageView4.setVisibility(0);
                FlexibleTextView flexibleTextView4 = a2.x;
                Wg6.b(flexibleTextView4, "binding.ftvDayOfWeek");
                flexibleTextView4.setText(Jl5.b.i(i2));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void onClick(View view) {
        StringBuilder sb = new StringBuilder();
        sb.append("onClick - v=");
        sb.append(view != null ? Integer.valueOf(view.getId()) : null);
        MFLogger.d("HeartRateDetailFragment", sb.toString());
        if (view != null) {
            switch (view.getId()) {
                case 2131362666:
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                case 2131362667:
                    Im6 im6 = this.h;
                    if (im6 != null) {
                        im6.u();
                        return;
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                case 2131362735:
                    Im6 im62 = this.h;
                    if (im62 != null) {
                        im62.t();
                        return;
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                default:
                    return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        long timeInMillis;
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        Z65 z65 = (Z65) Aq0.f(layoutInflater, 2131558563, viewGroup, false, A6());
        Bundle arguments = getArguments();
        if (arguments != null) {
            timeInMillis = arguments.getLong("KEY_LONG_TIME");
        } else {
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "Calendar.getInstance()");
            timeInMillis = instance.getTimeInMillis();
        }
        this.i = new Date(timeInMillis);
        if (bundle != null && bundle.containsKey("KEY_LONG_TIME")) {
            this.i = new Date(bundle.getLong("KEY_LONG_TIME"));
        }
        Wg6.b(z65, "binding");
        K6(z65);
        Im6 im6 = this.h;
        if (im6 != null) {
            im6.p(this.i);
            this.g = new G37<>(this, z65);
            L6();
            G37<Z65> g37 = this.g;
            if (g37 != null) {
                Z65 a2 = g37.a();
                if (a2 != null) {
                    return a2.n();
                }
                return null;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        Im6 im6 = this.h;
        if (im6 != null) {
            im6.q();
            Lifecycle lifecycle = getLifecycle();
            Im6 im62 = this.h;
            if (im62 != null) {
                lifecycle.c(im62.o());
                super.onDestroyView();
                v6();
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Im6 im6 = this.h;
        if (im6 != null) {
            im6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        L6();
        Im6 im6 = this.h;
        if (im6 != null) {
            im6.s(this.i);
            Im6 im62 = this.h;
            if (im62 != null) {
                im62.l();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.c(bundle, "outState");
        Im6 im6 = this.h;
        if (im6 != null) {
            im6.r(bundle);
            super.onSaveInstanceState(bundle);
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Jm6
    public void s(boolean z, Ai5 ai5, Cf<WorkoutSession> cf) {
        Wg6.c(ai5, "distanceUnit");
        Wg6.c(cf, "workoutSessions");
        G37<Z65> g37 = this.g;
        if (g37 != null) {
            Z65 a2 = g37.a();
            if (a2 != null) {
                if (z) {
                    LinearLayout linearLayout = a2.J;
                    Wg6.b(linearLayout, "it.llWorkout");
                    linearLayout.setVisibility(0);
                    if (!cf.isEmpty()) {
                        FlexibleTextView flexibleTextView = a2.B;
                        Wg6.b(flexibleTextView, "it.ftvNoWorkoutRecorded");
                        flexibleTextView.setVisibility(8);
                        RecyclerView recyclerView = a2.L;
                        Wg6.b(recyclerView, "it.rvWorkout");
                        recyclerView.setVisibility(0);
                        Jl6 jl6 = this.j;
                        if (jl6 != null) {
                            jl6.t(ai5, cf);
                        }
                    } else {
                        FlexibleTextView flexibleTextView2 = a2.B;
                        Wg6.b(flexibleTextView2, "it.ftvNoWorkoutRecorded");
                        flexibleTextView2.setVisibility(0);
                        RecyclerView recyclerView2 = a2.L;
                        Wg6.b(recyclerView2, "it.rvWorkout");
                        recyclerView2.setVisibility(8);
                        Jl6 jl62 = this.j;
                        if (jl62 != null) {
                            jl62.t(ai5, cf);
                        }
                    }
                } else {
                    LinearLayout linearLayout2 = a2.J;
                    Wg6.b(linearLayout2, "it.llWorkout");
                    linearLayout2.setVisibility(8);
                }
                AppBarLayout appBarLayout = a2.q;
                Wg6.b(appBarLayout, "it.appBarLayout");
                ViewGroup.LayoutParams layoutParams = appBarLayout.getLayoutParams();
                if (layoutParams != null) {
                    CoordinatorLayout.e eVar = (CoordinatorLayout.e) layoutParams;
                    AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) eVar.f();
                    if (behavior == null) {
                        behavior = new AppBarLayout.Behavior();
                    }
                    behavior.setDragCallback(new Bi(this, z, cf, ai5));
                    eVar.o(behavior);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Jl6.Ai
    public void t4(WorkoutSession workoutSession) {
        Wg6.c(workoutSession, "workoutSession");
        Nn6 b = Nn6.v.b(workoutSession.getId());
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        b.show(childFragmentManager, Nn6.v.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.x;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
