package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vs1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Vs1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Vs1 createFromParcel(Parcel parcel) {
            return new Vs1(parcel.readInt(), parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Vs1[] newArray(int i) {
            return new Vs1[i];
        }
    }

    @DexIgnore
    public Vs1(int i, int i2, int i3) throws IllegalArgumentException {
        boolean z = true;
        this.b = i;
        this.c = i2;
        this.d = i3;
        if (i >= 0 && 359 >= i) {
            int i4 = this.c;
            if (i4 >= 0 && 120 >= i4) {
                if (!(this.d < 0 ? false : z)) {
                    throw new IllegalArgumentException(E.c(E.e("z index("), this.d, ") must be larger than ", "[0]."));
                }
                return;
            }
            throw new IllegalArgumentException(E.c(E.e("distanceFromCenter("), this.c, ") is out of ", "range [0, 120]."));
        }
        throw new IllegalArgumentException(E.c(E.e("angle("), this.b, ") is out of range ", "[0, 359]."));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Vs1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Vs1 vs1 = (Vs1) obj;
            if (this.b != vs1.b) {
                return false;
            }
            if (this.c != vs1.c) {
                return false;
            }
            return this.d == vs1.d;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.background.config.position.BackgroundPositionConfig");
    }

    @DexIgnore
    public final int getAngle() {
        return this.b;
    }

    @DexIgnore
    public final int getDistanceFromCenter() {
        return this.c;
    }

    @DexIgnore
    public final int getZIndex() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.b * 31) + this.c) * 31) + this.d;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put("angle", this.b).put("distance", this.c).put("z_index", this.d);
        Wg6.b(put, "JSONObject()\n           \u2026Constant.Z_INDEX, zIndex)");
        return put;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
    }
}
