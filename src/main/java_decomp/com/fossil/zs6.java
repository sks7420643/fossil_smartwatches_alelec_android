package com.fossil;

import android.graphics.Color;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zs6 extends ts0 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e; // = "#bdbdbd";

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public MutableLiveData<a> f4524a; // = new MutableLiveData<>();
    @DexIgnore
    public a b; // = new a(null, null, null, null, null, null, null, null, 255, null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public Integer f4525a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public Integer e;
        @DexIgnore
        public Integer f;
        @DexIgnore
        public Integer g;
        @DexIgnore
        public Integer h;

        @DexIgnore
        public a() {
            this(null, null, null, null, null, null, null, null, 255, null);
        }

        @DexIgnore
        public a(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8) {
            this.f4525a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
            this.e = num5;
            this.f = num6;
            this.g = num7;
            this.h = num8;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8, int i, kq7 kq7) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2, (i & 4) != 0 ? null : num3, (i & 8) != 0 ? null : num4, (i & 16) != 0 ? null : num5, (i & 32) != 0 ? null : num6, (i & 64) != 0 ? null : num7, (i & 128) == 0 ? num8 : null);
        }

        @DexIgnore
        public static /* synthetic */ void j(a aVar, Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8, int i, Object obj) {
            aVar.i((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2, (i & 4) != 0 ? null : num3, (i & 8) != 0 ? null : num4, (i & 16) != 0 ? null : num5, (i & 32) != 0 ? null : num6, (i & 64) != 0 ? null : num7, (i & 128) != 0 ? null : num8);
        }

        @DexIgnore
        public final Integer a() {
            return this.d;
        }

        @DexIgnore
        public final Integer b() {
            return this.c;
        }

        @DexIgnore
        public final Integer c() {
            return this.b;
        }

        @DexIgnore
        public final Integer d() {
            return this.f4525a;
        }

        @DexIgnore
        public final Integer e() {
            return this.f;
        }

        @DexIgnore
        public final Integer f() {
            return this.e;
        }

        @DexIgnore
        public final Integer g() {
            return this.h;
        }

        @DexIgnore
        public final Integer h() {
            return this.g;
        }

        @DexIgnore
        public final void i(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, Integer num7, Integer num8) {
            this.f4525a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
            this.e = num5;
            this.f = num6;
            this.g = num7;
            this.h = num8;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeRingChartViewModel$saveColor$1", f = "CustomizeRingChartViewModel.kt", l = {76, 77, 120}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $bigColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $biggestColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $mediumColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $smallColor;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ zs6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(zs6 zs6, String str, String str2, String str3, String str4, String str5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = zs6;
            this.$id = str;
            this.$biggestColor = str2;
            this.$bigColor = str3;
            this.$mediumColor = str4;
            this.$smallColor = str5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$id, this.$biggestColor, this.$bigColor, this.$mediumColor, this.$smallColor, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0066  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0097  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00fa  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0135  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x015f  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x01ab  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x01f9  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x02a1  */
        /* JADX WARNING: Removed duplicated region for block: B:69:0x02a4  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r19) {
            /*
            // Method dump skipped, instructions count: 681
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.zs6.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = zs6.class.getSimpleName();
        pq7.b(simpleName, "CustomizeRingChartViewModel::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public zs6(ThemeRepository themeRepository) {
        pq7.c(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void d() {
        this.f4524a.l(this.b);
    }

    @DexIgnore
    public final MutableLiveData<a> e() {
        return this.f4524a;
    }

    @DexIgnore
    public final void f(String str, String str2, String str3, String str4, String str5) {
        pq7.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str6 = d;
        local.d(str6, "saveColor biggestColor=" + str2 + " bigColor=" + str3 + " mediumColor=" + str4 + " smallColor=" + str5);
        xw7 unused = gu7.d(jv7.a(bw7.b()), null, null, new b(this, str, str2, str3, str4, str5, null), 3, null);
    }

    @DexIgnore
    public final void g() {
        String b2 = ws6.u.b();
        int parseColor = b2 != null ? Color.parseColor(b2) : Color.parseColor(e);
        String a2 = ws6.u.a();
        int parseColor2 = a2 != null ? Color.parseColor(a2) : Color.parseColor(e);
        String c2 = ws6.u.c();
        int parseColor3 = c2 != null ? Color.parseColor(c2) : Color.parseColor(e);
        String d2 = ws6.u.d();
        int parseColor4 = d2 != null ? Color.parseColor(d2) : Color.parseColor(e);
        this.b.i(Integer.valueOf(parseColor), Integer.valueOf(parseColor), Integer.valueOf(parseColor2), Integer.valueOf(parseColor2), Integer.valueOf(parseColor3), Integer.valueOf(parseColor3), Integer.valueOf(parseColor4), Integer.valueOf(parseColor4));
        d();
    }

    @DexIgnore
    public final void h(int i, int i2) {
        switch (i) {
            case 401:
                a.j(this.b, Integer.valueOf(i2), Integer.valueOf(i2), null, null, null, null, null, null, 252, null);
                d();
                return;
            case Action.ActivityTracker.TAG_ACTIVITY /* 402 */:
                a.j(this.b, null, null, Integer.valueOf(i2), Integer.valueOf(i2), null, null, null, null, 243, null);
                d();
                return;
            case MFNetworkReturnCode.WRONG_PASSWORD /* 403 */:
                a.j(this.b, null, null, null, null, Integer.valueOf(i2), Integer.valueOf(i2), null, null, 207, null);
                d();
                return;
            case 404:
                a.j(this.b, null, null, null, null, null, null, Integer.valueOf(i2), Integer.valueOf(i2), 63, null);
                d();
                return;
            default:
                return;
        }
    }
}
