package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Oo2 extends Xm2 implements Mo2 {
    @DexIgnore
    public Oo2() {
        super("com.google.android.gms.fitness.internal.IStatusCallback");
    }

    @DexIgnore
    public static Mo2 e(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IStatusCallback");
        return queryLocalInterface instanceof Mo2 ? (Mo2) queryLocalInterface : new No2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.Xm2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        m0((Status) Qo2.a(parcel, Status.CREATOR));
        return true;
    }
}
