package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Ot extends Enum<Ot> {
    @DexIgnore
    public static /* final */ Ot c;
    @DexIgnore
    public static /* final */ /* synthetic */ Ot[] d;
    @DexIgnore
    public static /* final */ Nt e; // = new Nt(null);
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        Ot ot = new Ot("REQUEST", 0, (byte) 1);
        Ot ot2 = new Ot("NOTIFY", 1, (byte) 2);
        c = ot2;
        d = new Ot[]{ot, ot2};
    }
    */

    @DexIgnore
    public Ot(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static Ot valueOf(String str) {
        return (Ot) Enum.valueOf(Ot.class, str);
    }

    @DexIgnore
    public static Ot[] values() {
        return (Ot[]) d.clone();
    }
}
