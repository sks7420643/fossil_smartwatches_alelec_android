package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G06 implements Factory<HomeAlertsPresenter> {
    @DexIgnore
    public static HomeAlertsPresenter a(A06 a06, Uq4 uq4, AlarmHelper alarmHelper, V36 v36, D26 d26, U06 u06, NotificationSettingsDatabase notificationSettingsDatabase, SetAlarms setAlarms, AlarmsRepository alarmsRepository, An4 an4, DNDSettingsDatabase dNDSettingsDatabase) {
        return new HomeAlertsPresenter(a06, uq4, alarmHelper, v36, d26, u06, notificationSettingsDatabase, setAlarms, alarmsRepository, an4, dNDSettingsDatabase);
    }
}
