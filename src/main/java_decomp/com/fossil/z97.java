package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Lc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.watchface.edit.template.WatchFaceTemplateViewModel;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z97 extends BaseFragment {
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public G37<Bh5> h;
    @DexIgnore
    public WatchFaceTemplateViewModel i;
    @DexIgnore
    public X97 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements E77 {
        @DexIgnore
        public /* final */ /* synthetic */ Z97 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(Z97 z97) {
            this.a = z97;
        }

        @DexIgnore
        @Override // com.fossil.E77
        public void a(Fb7 fb7, int i) {
            RecyclerView recyclerView;
            Wg6.c(fb7, "wf");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("WatchFaceTemplateFragment", "onTemplateClicked template: " + fb7);
            Z97.L6(this.a).g(i);
            Bh5 bh5 = (Bh5) Z97.K6(this.a).a();
            if (!(bh5 == null || (recyclerView = bh5.q) == null)) {
                recyclerView.smoothScrollToPosition(i);
            }
            Z97.M6(this.a).s(fb7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<Lc6<? extends List<Fb7>, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ Z97 a;

        @DexIgnore
        public Bi(Z97 z97) {
            this.a = z97;
        }

        @DexIgnore
        public final void a(Lc6<? extends List<Fb7>, Integer> lc6) {
            int intValue = lc6.getSecond().intValue();
            Z97.L6(this.a).h((List) lc6.getFirst());
            Z97.L6(this.a).g(intValue);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends List<Fb7>, ? extends Integer> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<Fb7> {
        @DexIgnore
        public /* final */ /* synthetic */ Z97 a;

        @DexIgnore
        public Ci(Z97 z97) {
            this.a = z97;
        }

        @DexIgnore
        public final void a(Fb7 fb7) {
            Gc7 e;
            if (fb7 != null && (e = Hc7.c.e(this.a)) != null) {
                e.B(fb7);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Fb7 fb7) {
            a(fb7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Fb7> {
        @DexIgnore
        public /* final */ /* synthetic */ Z97 a;

        @DexIgnore
        public Di(Z97 z97) {
            this.a = z97;
        }

        @DexIgnore
        public final void a(Fb7 fb7) {
            String str;
            WatchFaceTemplateViewModel M6 = Z97.M6(this.a);
            if (fb7 == null || (str = fb7.b()) == null) {
                str = "";
            }
            M6.v(str);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Fb7 fb7) {
            a(fb7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<String> {
        @DexIgnore
        public /* final */ /* synthetic */ Z97 a;

        @DexIgnore
        public Ei(Z97 z97) {
            this.a = z97;
        }

        @DexIgnore
        public final void a(String str) {
            WatchFaceTemplateViewModel M6 = Z97.M6(this.a);
            if (str == null) {
                str = "";
            }
            M6.v(str);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(String str) {
            a(str);
        }
    }

    @DexIgnore
    public static final /* synthetic */ G37 K6(Z97 z97) {
        G37<Bh5> g37 = z97.h;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ X97 L6(Z97 z97) {
        X97 x97 = z97.j;
        if (x97 != null) {
            return x97;
        }
        Wg6.n("templateAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ WatchFaceTemplateViewModel M6(Z97 z97) {
        WatchFaceTemplateViewModel watchFaceTemplateViewModel = z97.i;
        if (watchFaceTemplateViewModel != null) {
            return watchFaceTemplateViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "WatchFaceTemplateFragment";
    }

    @DexIgnore
    public final void N6() {
        this.j = new X97(new Ai(this));
        G37<Bh5> g37 = this.h;
        if (g37 != null) {
            Bh5 a2 = g37.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.q;
                recyclerView.setHasFixedSize(true);
                recyclerView.setItemAnimator(null);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                X97 x97 = this.j;
                if (x97 != null) {
                    recyclerView.setAdapter(x97);
                } else {
                    Wg6.n("templateAdapter");
                    throw null;
                }
            }
        } else {
            Wg6.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public final void O6() {
        MutableLiveData<String> r;
        LiveData<Fb7> l;
        WatchFaceTemplateViewModel watchFaceTemplateViewModel = this.i;
        if (watchFaceTemplateViewModel != null) {
            watchFaceTemplateViewModel.q().h(getViewLifecycleOwner(), new Bi(this));
            WatchFaceTemplateViewModel watchFaceTemplateViewModel2 = this.i;
            if (watchFaceTemplateViewModel2 != null) {
                watchFaceTemplateViewModel2.r().h(getViewLifecycleOwner(), new Ci(this));
                Gc7 e = Hc7.c.e(this);
                if (!(e == null || (l = e.l()) == null)) {
                    l.h(getViewLifecycleOwner(), new Di(this));
                }
                Gc7 e2 = Hc7.c.e(this);
                if (e2 != null && (r = e2.r()) != null) {
                    r.h(getViewLifecycleOwner(), new Ei(this));
                    return;
                }
                return;
            }
            Wg6.n("viewModel");
            throw null;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        ScrollView scrollView;
        super.onActivityCreated(bundle);
        PortfolioApp.get.instance().getIface().X0().a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(WatchFaceTemplateViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ateViewModel::class.java)");
            this.i = (WatchFaceTemplateViewModel) a2;
            String d = ThemeManager.l.a().d("nonBrandSurface");
            if (!TextUtils.isEmpty(d)) {
                G37<Bh5> g37 = this.h;
                if (g37 != null) {
                    Bh5 a3 = g37.a();
                    if (!(a3 == null || (scrollView = a3.r) == null)) {
                        scrollView.setBackgroundColor(Color.parseColor(d));
                    }
                } else {
                    Wg6.n("binding");
                    throw null;
                }
            }
            N6();
            O6();
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Bh5 bh5 = (Bh5) Aq0.f(layoutInflater, 2131558863, viewGroup, false, A6());
        this.h = new G37<>(this, bh5);
        Wg6.b(bh5, "binding");
        View n = bh5.n();
        Wg6.b(n, "binding.root");
        return n;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
