package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.Ta4;
import com.fossil.Ta4$d$d$a$b$a;
import com.fossil.Ta4$d$d$a$b$c;
import com.fossil.Ta4$d$d$a$b$d;
import com.fossil.Ta4$d$d$a$b$e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Y84 {
    @DexIgnore
    public static /* final */ String e; // = String.format(Locale.US, "Crashlytics Android SDK/%s", "17.1.1");
    @DexIgnore
    public static /* final */ Map<String, Integer> f;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ H94 b;
    @DexIgnore
    public /* final */ L84 c;
    @DexIgnore
    public /* final */ Kd4 d;

    /*
    static {
        HashMap hashMap = new HashMap();
        f = hashMap;
        hashMap.put("armeabi", 5);
        f.put("armeabi-v7a", 6);
        f.put("arm64-v8a", 9);
        f.put("x86", 0);
        f.put("x86_64", 1);
    }
    */

    @DexIgnore
    public Y84(Context context, H94 h94, L84 l84, Kd4 kd4) {
        this.a = context;
        this.b = h94;
        this.c = l84;
        this.d = kd4;
    }

    @DexIgnore
    public static int d() {
        String str = Build.CPU_ABI;
        if (TextUtils.isEmpty(str)) {
            return 7;
        }
        Integer num = f.get(str.toLowerCase(Locale.US));
        if (num == null) {
            return 7;
        }
        return num.intValue();
    }

    @DexIgnore
    public final Ta4.Ai a() {
        Ta4.Ai b2 = Ta4.b();
        b2.h("17.1.1");
        b2.d(this.c.a);
        b2.e(this.b.a());
        b2.b(this.c.e);
        b2.c(this.c.f);
        b2.g(4);
        return b2;
    }

    @DexIgnore
    public Ta4.Di.Dii b(Throwable th, Thread thread, String str, long j, int i, int i2, boolean z) {
        int i3 = this.a.getResources().getConfiguration().orientation;
        Ld4 ld4 = new Ld4(th, this.d);
        Ta4.Di.Dii.Biii a2 = Ta4.Di.Dii.a();
        a2.f(str);
        a2.e(j);
        a2.b(g(i3, ld4, thread, i, i2, z));
        a2.c(h(i3));
        return a2.a();
    }

    @DexIgnore
    public Ta4 c(String str, long j) {
        Ta4.Ai a2 = a();
        a2.i(o(str, j));
        return a2.a();
    }

    @DexIgnore
    public final Ta4$d$d$a$b$a e() {
        Ta4$d$d$a$b$a.a a2 = Ta4$d$d$a$b$a.a();
        a2.b(0);
        a2.d(0);
        a2.c(this.c.d);
        a2.e(this.c.b);
        return a2.a();
    }

    @DexIgnore
    public final Ua4<Ta4$d$d$a$b$a> f() {
        return Ua4.b(e());
    }

    @DexIgnore
    public final Ta4.Di.Dii.Aiii g(int i, Ld4 ld4, Thread thread, int i2, int i3, boolean z) {
        Boolean bool;
        ActivityManager.RunningAppProcessInfo k = R84.k(this.c.d, this.a);
        if (k != null) {
            bool = Boolean.valueOf(k.importance != 100);
        } else {
            bool = null;
        }
        Ta4.Di.Dii.Aiii.Aiiii a2 = Ta4.Di.Dii.Aiii.a();
        a2.b(bool);
        a2.e(i);
        a2.d(k(ld4, thread, i2, i3, z));
        return a2.a();
    }

    @DexIgnore
    public final Ta4.Di.Dii.Ciii h(int i) {
        O84 a2 = O84.a(this.a);
        Float b2 = a2.b();
        Double valueOf = b2 != null ? Double.valueOf(b2.doubleValue()) : null;
        int c2 = a2.c();
        boolean q = R84.q(this.a);
        long v = R84.v();
        long a3 = R84.a(this.a);
        long b3 = R84.b(Environment.getDataDirectory().getPath());
        Ta4.Di.Dii.Ciii.Aiiii a4 = Ta4.Di.Dii.Ciii.a();
        a4.b(valueOf);
        a4.c(c2);
        a4.f(q);
        a4.e(i);
        a4.g(v - a3);
        a4.d(b3);
        return a4.a();
    }

    @DexIgnore
    public final Ta4$d$d$a$b$c i(Ld4 ld4, int i, int i2) {
        return j(ld4, i, i2, 0);
    }

    @DexIgnore
    public final Ta4$d$d$a$b$c j(Ld4 ld4, int i, int i2, int i3) {
        int i4 = 0;
        String str = ld4.b;
        String str2 = ld4.a;
        StackTraceElement[] stackTraceElementArr = ld4.c;
        if (stackTraceElementArr == null) {
            stackTraceElementArr = new StackTraceElement[0];
        }
        Ld4 ld42 = ld4.d;
        if (i3 >= i2) {
            Ld4 ld43 = ld42;
            while (ld43 != null) {
                ld43 = ld43.d;
                i4++;
            }
        }
        Ta4$d$d$a$b$c.a a2 = Ta4$d$d$a$b$c.a();
        a2.f(str);
        a2.e(str2);
        a2.c(Ua4.a(m(stackTraceElementArr, i)));
        a2.d(i4);
        if (ld42 != null && i4 == 0) {
            a2.b(j(ld42, i, i2, i3 + 1));
        }
        return a2.a();
    }

    @DexIgnore
    public final Ta4.Di.Dii.Aiii.Biiii k(Ld4 ld4, Thread thread, int i, int i2, boolean z) {
        Ta4$d$d$a$b$b a2 = Ta4.Di.Dii.Aiii.Biiii.a();
        a2.e(u(ld4, thread, i, z));
        a2.c(i(ld4, i, i2));
        a2.d(r());
        a2.b(f());
        return a2.a();
    }

    @DexIgnore
    public final Ta4$d$d$a$b$e.b l(StackTraceElement stackTraceElement, Ta4$d$d$a$b$e.b.a aVar) {
        long j = 0;
        long max = stackTraceElement.isNativeMethod() ? Math.max((long) stackTraceElement.getLineNumber(), 0L) : 0;
        String str = stackTraceElement.getClassName() + CodelessMatcher.CURRENT_CLASS_NAME + stackTraceElement.getMethodName();
        String fileName = stackTraceElement.getFileName();
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            j = (long) stackTraceElement.getLineNumber();
        }
        aVar.e(max);
        aVar.f(str);
        aVar.b(fileName);
        aVar.d(j);
        return aVar.a();
    }

    @DexIgnore
    public final Ua4<Ta4$d$d$a$b$e.b> m(StackTraceElement[] stackTraceElementArr, int i) {
        ArrayList arrayList = new ArrayList();
        for (StackTraceElement stackTraceElement : stackTraceElementArr) {
            Ta4$d$d$a$b$e.b.a a2 = Ta4$d$d$a$b$e.b.a();
            a2.c(i);
            arrayList.add(l(stackTraceElement, a2));
        }
        return Ua4.a(arrayList);
    }

    @DexIgnore
    public final Ta4.Di.Aii n() {
        Ta4.Di.Aii.Aiii a2 = Ta4.Di.Aii.a();
        a2.c(this.b.d());
        a2.e(this.c.e);
        a2.b(this.c.f);
        a2.d(this.b.a());
        return a2.a();
    }

    @DexIgnore
    public final Ta4.Di o(String str, long j) {
        Ta4.Di.Bii a2 = Ta4.Di.a();
        a2.l(j);
        a2.i(str);
        a2.g(e);
        a2.b(n());
        a2.k(q());
        a2.d(p());
        a2.h(3);
        return a2.a();
    }

    @DexIgnore
    public final Ta4.Di.Cii p() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        int d2 = d();
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        long v = R84.v();
        long blockCount = (long) statFs.getBlockCount();
        long blockSize = (long) statFs.getBlockSize();
        boolean C = R84.C(this.a);
        int n = R84.n(this.a);
        String str = Build.MANUFACTURER;
        String str2 = Build.PRODUCT;
        Ta4.Di.Cii.Aiii a2 = Ta4.Di.Cii.a();
        a2.b(d2);
        a2.f(Build.MODEL);
        a2.c(availableProcessors);
        a2.h(v);
        a2.d(blockCount * blockSize);
        a2.i(C);
        a2.j(n);
        a2.e(str);
        a2.g(str2);
        return a2.a();
    }

    @DexIgnore
    public final Ta4.Di.Eii q() {
        Ta4.Di.Eii.Aiii a2 = Ta4.Di.Eii.a();
        a2.d(3);
        a2.e(Build.VERSION.RELEASE);
        a2.b(Build.VERSION.CODENAME);
        a2.c(R84.E(this.a));
        return a2.a();
    }

    @DexIgnore
    public final Ta4$d$d$a$b$d r() {
        Ta4$d$d$a$b$d.a a2 = Ta4$d$d$a$b$d.a();
        a2.d("0");
        a2.c("0");
        a2.b(0);
        return a2.a();
    }

    @DexIgnore
    public final Ta4$d$d$a$b$e s(Thread thread, StackTraceElement[] stackTraceElementArr) {
        return t(thread, stackTraceElementArr, 0);
    }

    @DexIgnore
    public final Ta4$d$d$a$b$e t(Thread thread, StackTraceElement[] stackTraceElementArr, int i) {
        Ta4$d$d$a$b$e.a a2 = Ta4$d$d$a$b$e.a();
        a2.d(thread.getName());
        a2.c(i);
        a2.b(Ua4.a(m(stackTraceElementArr, i)));
        return a2.a();
    }

    @DexIgnore
    public final Ua4<Ta4$d$d$a$b$e> u(Ld4 ld4, Thread thread, int i, boolean z) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(t(thread, ld4.c, i));
        if (z) {
            for (Map.Entry<Thread, StackTraceElement[]> entry : Thread.getAllStackTraces().entrySet()) {
                Thread key = entry.getKey();
                if (!key.equals(thread)) {
                    arrayList.add(s(key, this.d.a(entry.getValue())));
                }
            }
        }
        return Ua4.a(arrayList);
    }
}
