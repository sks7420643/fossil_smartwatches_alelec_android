package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q73 implements N73 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a; // = new Hw2(Yv2.a("com.google.android.gms.measurement")).d("measurement.referrer.enable_logging_install_referrer_cmp_from_apk", true);

    @DexIgnore
    @Override // com.fossil.N73
    public final boolean zza() {
        return a.o().booleanValue();
    }
}
