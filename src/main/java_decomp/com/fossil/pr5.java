package com.fossil;

import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pr5 implements Factory<MusicControlComponent> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ Pr5 a; // = new Pr5();
    }

    @DexIgnore
    public static Pr5 a() {
        return Ai.a;
    }

    @DexIgnore
    public static MusicControlComponent c() {
        return new MusicControlComponent();
    }

    @DexIgnore
    public MusicControlComponent b() {
        return c();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
