package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.H60;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gm extends Lp {
    @DexIgnore
    public /* final */ boolean C; // = true;
    @DexIgnore
    public int D;
    @DexIgnore
    public /* final */ H60 E; // = new H60((byte) 26, H60.Ai.MALE, 170, 65, H60.Bi.LEFT_WRIST);
    @DexIgnore
    public /* final */ ArrayList<Ow> F; // = By1.a(this.i, Hm7.c(Ow.c, Ow.d));

    @DexIgnore
    public Gm(K5 k5, I60 i60) {
        super(k5, i60, Yp.L, null, false, 24);
    }

    @DexIgnore
    public static final /* synthetic */ void G(Gm gm) {
        int i = gm.D;
        if (i < 2) {
            gm.D = i + 1;
            if (Q3.f.f(gm.x.a())) {
                Lp.h(gm, new Nm(gm.w, gm.x, gm.E, true, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, gm.z, 32), new Wj(gm), new Ik(gm), new Uk(gm), null, null, 48, null);
            } else {
                Lp.i(gm, new Cv(L8.c.b, gm.w, 0, 4), new Hl(gm), new Ul(gm), null, null, null, 56, null);
            }
        } else {
            gm.I();
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        Lp.i(this, new Vu(this.w), new Xi(this), new Jj(this), null, null, null, 56, null);
    }

    @DexIgnore
    public final void I() {
        U.a.a(this.w.x);
        l(Nr.a(this.v, null, Zq.b, null, null, 13));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public boolean t() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.F;
    }
}
