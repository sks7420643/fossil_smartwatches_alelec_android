package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uw7 extends Iu7 {
    @DexIgnore
    public /* final */ Hg6<Throwable, Cd6> b;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.mapped.Hg6<? super java.lang.Throwable, com.mapped.Cd6> */
    /* JADX WARN: Multi-variable type inference failed */
    public Uw7(Hg6<? super Throwable, Cd6> hg6) {
        this.b = hg6;
    }

    @DexIgnore
    @Override // com.fossil.Ju7
    public void a(Throwable th) {
        this.b.invoke(th);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
        a(th);
        return Cd6.a;
    }

    @DexIgnore
    public String toString() {
        return "InvokeOnCancel[" + Ov7.a(this.b) + '@' + Ov7.b(this) + ']';
    }
}
