package com.fossil;

import com.portfolio.platform.preset.data.source.DianaPresetRemote;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vo5 implements Factory<DianaPresetRepository> {
    @DexIgnore
    public /* final */ Provider<DianaPresetRemote> a;

    @DexIgnore
    public Vo5(Provider<DianaPresetRemote> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Vo5 a(Provider<DianaPresetRemote> provider) {
        return new Vo5(provider);
    }

    @DexIgnore
    public static DianaPresetRepository c(DianaPresetRemote dianaPresetRemote) {
        return new DianaPresetRepository(dianaPresetRemote);
    }

    @DexIgnore
    public DianaPresetRepository b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
