package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M93 implements Xw2<P93> {
    @DexIgnore
    public static M93 c; // = new M93();
    @DexIgnore
    public /* final */ Xw2<P93> b;

    @DexIgnore
    public M93() {
        this(Ww2.b(new O93()));
    }

    @DexIgnore
    public M93(Xw2<P93> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((P93) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ P93 zza() {
        return this.b.zza();
    }
}
