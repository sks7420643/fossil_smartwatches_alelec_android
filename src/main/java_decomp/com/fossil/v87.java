package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.S87;
import com.mapped.Kc6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.chrono.BasicChronology;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V87 {
    @DexIgnore
    public static final List<S87> a(List<? extends S87> list) {
        S87 f;
        Wg6.c(list, "$this$clone");
        ArrayList arrayList = new ArrayList(Im7.m(list, 10));
        for (T t : list) {
            if (t instanceof S87.Ci) {
                f = S87.Ci.f(t, null, null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, null, 0, 0, 127, null);
            } else if (t instanceof S87.Bi) {
                f = S87.Bi.g(t, null, null, null, null, 0, 0, 63, null);
            } else if (t instanceof S87.Ai) {
                f = S87.Ai.f(t, null, null, null, false, null, null, null, null, 0, 0, BasicChronology.CACHE_MASK, null);
            } else {
                throw new Kc6();
            }
            arrayList.add(f);
        }
        return arrayList;
    }

    @DexIgnore
    public static final String b(Lb7 lb7) {
        Wg6.c(lb7, "$this$toComplicationId");
        switch (U87.a[lb7.ordinal()]) {
            case 1:
                return "weather";
            case 2:
                return "second-timezone";
            case 3:
                return "active-minutes";
            case 4:
                return Constants.BATTERY;
            case 5:
                return "calories";
            case 6:
                return "chance-of-rain";
            case 7:
                return "date";
            case 8:
                return "heart-rate";
            case 9:
            default:
                return "steps";
        }
    }

    @DexIgnore
    public static final W87 c(Jb7 jb7) {
        Wg6.c(jb7, "$this$toMetric");
        return new W87(jb7.c(), jb7.d(), jb7.b(), jb7.a(), 1.0f);
    }
}
