package com.fossil;

import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class H04 {
    @DexIgnore
    public /* final */ I04[] a; // = new I04[4];
    @DexIgnore
    public /* final */ Matrix[] b; // = new Matrix[4];
    @DexIgnore
    public /* final */ Matrix[] c; // = new Matrix[4];
    @DexIgnore
    public /* final */ PointF d; // = new PointF();
    @DexIgnore
    public /* final */ I04 e; // = new I04();
    @DexIgnore
    public /* final */ float[] f; // = new float[2];
    @DexIgnore
    public /* final */ float[] g; // = new float[2];

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(I04 i04, Matrix matrix, int i);

        @DexIgnore
        void b(I04 i04, Matrix matrix, int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ G04 a;
        @DexIgnore
        public /* final */ Path b;
        @DexIgnore
        public /* final */ RectF c;
        @DexIgnore
        public /* final */ Ai d;
        @DexIgnore
        public /* final */ float e;

        @DexIgnore
        public Bi(G04 g04, float f, RectF rectF, Ai ai, Path path) {
            this.d = ai;
            this.a = g04;
            this.e = f;
            this.c = rectF;
            this.b = path;
        }
    }

    @DexIgnore
    public H04() {
        for (int i = 0; i < 4; i++) {
            this.a[i] = new I04();
            this.b[i] = new Matrix();
            this.c[i] = new Matrix();
        }
    }

    @DexIgnore
    public final float a(int i) {
        return (float) ((i + 1) * 90);
    }

    @DexIgnore
    public final void b(Bi bi, int i) {
        this.f[0] = this.a[i].j();
        this.f[1] = this.a[i].k();
        this.b[i].mapPoints(this.f);
        if (i == 0) {
            Path path = bi.b;
            float[] fArr = this.f;
            path.moveTo(fArr[0], fArr[1]);
        } else {
            Path path2 = bi.b;
            float[] fArr2 = this.f;
            path2.lineTo(fArr2[0], fArr2[1]);
        }
        this.a[i].d(this.b[i], bi.b);
        Ai ai = bi.d;
        if (ai != null) {
            ai.a(this.a[i], this.b[i], i);
        }
    }

    @DexIgnore
    public final void c(Bi bi, int i) {
        int i2 = (i + 1) % 4;
        this.f[0] = this.a[i].h();
        this.f[1] = this.a[i].i();
        this.b[i].mapPoints(this.f);
        this.g[0] = this.a[i2].j();
        this.g[1] = this.a[i2].k();
        this.b[i2].mapPoints(this.g);
        float[] fArr = this.f;
        float f2 = fArr[0];
        float[] fArr2 = this.g;
        float max = Math.max(((float) Math.hypot((double) (f2 - fArr2[0]), (double) (fArr[1] - fArr2[1]))) - 0.001f, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float i3 = i(bi.c, i);
        this.e.m(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        j(i, bi.a).d(max, i3, bi.e, this.e);
        this.e.d(this.c[i], bi.b);
        Ai ai = bi.d;
        if (ai != null) {
            ai.b(this.e, this.c[i], i);
        }
    }

    @DexIgnore
    public void d(G04 g04, float f2, RectF rectF, Path path) {
        e(g04, f2, rectF, null, path);
    }

    @DexIgnore
    public void e(G04 g04, float f2, RectF rectF, Ai ai, Path path) {
        path.rewind();
        Bi bi = new Bi(g04, f2, rectF, ai, path);
        for (int i = 0; i < 4; i++) {
            k(bi, i);
            l(i);
        }
        for (int i2 = 0; i2 < 4; i2++) {
            b(bi, i2);
            c(bi, i2);
        }
        path.close();
    }

    @DexIgnore
    public final void f(int i, RectF rectF, PointF pointF) {
        if (i == 1) {
            pointF.set(rectF.right, rectF.bottom);
        } else if (i == 2) {
            pointF.set(rectF.left, rectF.bottom);
        } else if (i != 3) {
            pointF.set(rectF.right, rectF.top);
        } else {
            pointF.set(rectF.left, rectF.top);
        }
    }

    @DexIgnore
    public final Yz3 g(int i, G04 g04) {
        return i != 1 ? i != 2 ? i != 3 ? g04.t() : g04.r() : g04.j() : g04.l();
    }

    @DexIgnore
    public final Zz3 h(int i, G04 g04) {
        return i != 1 ? i != 2 ? i != 3 ? g04.s() : g04.q() : g04.i() : g04.k();
    }

    @DexIgnore
    public final float i(RectF rectF, int i) {
        float[] fArr = this.f;
        I04[] i04Arr = this.a;
        fArr[0] = i04Arr[i].c;
        fArr[1] = i04Arr[i].d;
        this.b[i].mapPoints(fArr);
        return (i == 1 || i == 3) ? Math.abs(rectF.centerX() - this.f[0]) : Math.abs(rectF.centerY() - this.f[1]);
    }

    @DexIgnore
    public final B04 j(int i, G04 g04) {
        return i != 1 ? i != 2 ? i != 3 ? g04.o() : g04.p() : g04.n() : g04.h();
    }

    @DexIgnore
    public final void k(Bi bi, int i) {
        h(i, bi.a).b(this.a[i], 90.0f, bi.e, bi.c, g(i, bi.a));
        float a2 = a(i);
        this.b[i].reset();
        f(i, bi.c, this.d);
        Matrix matrix = this.b[i];
        PointF pointF = this.d;
        matrix.setTranslate(pointF.x, pointF.y);
        this.b[i].preRotate(a2);
    }

    @DexIgnore
    public final void l(int i) {
        this.f[0] = this.a[i].h();
        this.f[1] = this.a[i].i();
        this.b[i].mapPoints(this.f);
        float a2 = a(i);
        this.c[i].reset();
        Matrix matrix = this.c[i];
        float[] fArr = this.f;
        matrix.setTranslate(fArr[0], fArr[1]);
        this.c[i].preRotate(a2);
    }
}
