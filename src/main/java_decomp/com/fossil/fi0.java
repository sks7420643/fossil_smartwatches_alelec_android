package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.Iterator;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fi0<K, V> implements Iterable<Map.Entry<K, V>> {
    @DexIgnore
    public Ci<K, V> b;
    @DexIgnore
    public Ci<K, V> c;
    @DexIgnore
    public WeakHashMap<Fi<K, V>, Boolean> d; // = new WeakHashMap<>();
    @DexIgnore
    public int e; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<K, V> extends Ei<K, V> {
        @DexIgnore
        public Ai(Ci<K, V> ci, Ci<K, V> ci2) {
            super(ci, ci2);
        }

        @DexIgnore
        @Override // com.fossil.Fi0.Ei
        public Ci<K, V> b(Ci<K, V> ci) {
            return ci.e;
        }

        @DexIgnore
        @Override // com.fossil.Fi0.Ei
        public Ci<K, V> c(Ci<K, V> ci) {
            return ci.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<K, V> extends Ei<K, V> {
        @DexIgnore
        public Bi(Ci<K, V> ci, Ci<K, V> ci2) {
            super(ci, ci2);
        }

        @DexIgnore
        @Override // com.fossil.Fi0.Ei
        public Ci<K, V> b(Ci<K, V> ci) {
            return ci.d;
        }

        @DexIgnore
        @Override // com.fossil.Fi0.Ei
        public Ci<K, V> c(Ci<K, V> ci) {
            return ci.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci<K, V> implements Map.Entry<K, V> {
        @DexIgnore
        public /* final */ K b;
        @DexIgnore
        public /* final */ V c;
        @DexIgnore
        public Ci<K, V> d;
        @DexIgnore
        public Ci<K, V> e;

        @DexIgnore
        public Ci(K k, V v) {
            this.b = k;
            this.c = v;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Ci)) {
                return false;
            }
            Ci ci = (Ci) obj;
            return this.b.equals(ci.b) && this.c.equals(ci.c);
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public K getKey() {
            return this.b;
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public V getValue() {
            return this.c;
        }

        @DexIgnore
        public int hashCode() {
            return this.b.hashCode() ^ this.c.hashCode();
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public V setValue(V v) {
            throw new UnsupportedOperationException("An entry modification is not supported");
        }

        @DexIgnore
        public String toString() {
            return ((Object) this.b) + SimpleComparison.EQUAL_TO_OPERATION + ((Object) this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements Iterator<Map.Entry<K, V>>, Fi<K, V> {
        @DexIgnore
        public Ci<K, V> b;
        @DexIgnore
        public boolean c; // = true;

        @DexIgnore
        public Di() {
        }

        @DexIgnore
        @Override // com.fossil.Fi0.Fi
        public void a(Ci<K, V> ci) {
            Ci<K, V> ci2 = this.b;
            if (ci == ci2) {
                Ci<K, V> ci3 = ci2.e;
                this.b = ci3;
                this.c = ci3 == null;
            }
        }

        @DexIgnore
        public Map.Entry<K, V> b() {
            if (this.c) {
                this.c = false;
                this.b = Fi0.this.b;
            } else {
                Ci<K, V> ci = this.b;
                this.b = ci != null ? ci.d : null;
            }
            return this.b;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.c) {
                return Fi0.this.b != null;
            }
            Ci<K, V> ci = this.b;
            return (ci == null || ci.d == null) ? false : true;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public /* bridge */ /* synthetic */ Object next() {
            return b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ei<K, V> implements Iterator<Map.Entry<K, V>>, Fi<K, V> {
        @DexIgnore
        public Ci<K, V> b;
        @DexIgnore
        public Ci<K, V> c;

        @DexIgnore
        public Ei(Ci<K, V> ci, Ci<K, V> ci2) {
            this.b = ci2;
            this.c = ci;
        }

        @DexIgnore
        @Override // com.fossil.Fi0.Fi
        public void a(Ci<K, V> ci) {
            if (this.b == ci && ci == this.c) {
                this.c = null;
                this.b = null;
            }
            Ci<K, V> ci2 = this.b;
            if (ci2 == ci) {
                this.b = b(ci2);
            }
            if (this.c == ci) {
                this.c = e();
            }
        }

        @DexIgnore
        public abstract Ci<K, V> b(Ci<K, V> ci);

        @DexIgnore
        public abstract Ci<K, V> c(Ci<K, V> ci);

        @DexIgnore
        public Map.Entry<K, V> d() {
            Ci<K, V> ci = this.c;
            this.c = e();
            return ci;
        }

        @DexIgnore
        public final Ci<K, V> e() {
            Ci<K, V> ci = this.c;
            Ci<K, V> ci2 = this.b;
            if (ci == ci2 || ci2 == null) {
                return null;
            }
            return c(ci);
        }

        @DexIgnore
        public boolean hasNext() {
            return this.c != null;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public /* bridge */ /* synthetic */ Object next() {
            return d();
        }
    }

    @DexIgnore
    public interface Fi<K, V> {
        @DexIgnore
        void a(Ci<K, V> ci);
    }

    @DexIgnore
    public Map.Entry<K, V> a() {
        return this.b;
    }

    @DexIgnore
    public Ci<K, V> b(K k) {
        Ci<K, V> ci = this.b;
        while (ci != null && !ci.b.equals(k)) {
            ci = ci.d;
        }
        return ci;
    }

    @DexIgnore
    public Fi0<K, V>.d c() {
        Di di = new Di();
        this.d.put(di, Boolean.FALSE);
        return di;
    }

    @DexIgnore
    public Map.Entry<K, V> d() {
        return this.c;
    }

    @DexIgnore
    public Iterator<Map.Entry<K, V>> descendingIterator() {
        Bi bi = new Bi(this.c, this.b);
        this.d.put(bi, Boolean.FALSE);
        return bi;
    }

    @DexIgnore
    public Ci<K, V> e(K k, V v) {
        Ci<K, V> ci = new Ci<>(k, v);
        this.e++;
        Ci<K, V> ci2 = this.c;
        if (ci2 == null) {
            this.b = ci;
            this.c = ci;
        } else {
            ci2.d = ci;
            ci.e = ci2;
            this.c = ci;
        }
        return ci;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Fi0)) {
            return false;
        }
        Fi0 fi0 = (Fi0) obj;
        if (size() != fi0.size()) {
            return false;
        }
        Iterator<Map.Entry<K, V>> it = iterator();
        Iterator<Map.Entry<K, V>> it2 = fi0.iterator();
        while (it.hasNext() && it2.hasNext()) {
            Map.Entry<K, V> next = it.next();
            Map.Entry<K, V> next2 = it2.next();
            if (next == null && next2 != null) {
                return false;
            }
            if (next != null && !next.equals(next2)) {
                return false;
            }
        }
        return !it.hasNext() && !it2.hasNext();
    }

    @DexIgnore
    public V f(K k, V v) {
        Ci<K, V> b2 = b(k);
        if (b2 != null) {
            return b2.c;
        }
        e(k, v);
        return null;
    }

    @DexIgnore
    public V g(K k) {
        Ci<K, V> b2 = b(k);
        if (b2 == null) {
            return null;
        }
        this.e--;
        if (!this.d.isEmpty()) {
            for (Fi<K, V> fi : this.d.keySet()) {
                fi.a(b2);
            }
        }
        Ci<K, V> ci = b2.e;
        if (ci != null) {
            ci.d = b2.d;
        } else {
            this.b = b2.d;
        }
        Ci<K, V> ci2 = b2.d;
        if (ci2 != null) {
            ci2.e = b2.e;
        } else {
            this.c = b2.e;
        }
        b2.d = null;
        b2.e = null;
        return b2.c;
    }

    @DexIgnore
    public int hashCode() {
        Iterator<Map.Entry<K, V>> it = iterator();
        int i = 0;
        while (it.hasNext()) {
            i = it.next().hashCode() + i;
        }
        return i;
    }

    @DexIgnore
    @Override // java.lang.Iterable
    public Iterator<Map.Entry<K, V>> iterator() {
        Ai ai = new Ai(this.b, this.c);
        this.d.put(ai, Boolean.FALSE);
        return ai;
    }

    @DexIgnore
    public int size() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator<Map.Entry<K, V>> it = iterator();
        while (it.hasNext()) {
            sb.append(it.next().toString());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }
}
