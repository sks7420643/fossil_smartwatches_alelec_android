package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Y90 extends Enum<Y90> {
    @DexIgnore
    public static /* final */ /* synthetic */ Y90[] d; // = {new Y90("PLAY_PAUSE", 0, 205, Z90.c), new Y90("VOLUME_UP", 1, 233, Z90.c), new Y90("VOLUME_DOWN", 2, 234, Z90.c), new Y90("NEXT", 3, 181, Z90.c), new Y90("PREVIOUS", 4, 182, Z90.c), new Y90("MUTE", 5, 226, Z90.c), new Y90("LOW_WHITE", 6, 5, Z90.d), new Y90("LOW_BLACK", 7, 26, Z90.d), new Y90("RIGHT", 8, 79, Z90.d), new Y90("LEFT", 9, 80, Z90.d), new Y90("PAGE_UP", 10, 75, Z90.d), new Y90("PAGE_DOWN", 11, 78, Z90.d), new Y90("ENTER", 12, 88, Z90.d)};
    @DexIgnore
    public /* final */ short b;
    @DexIgnore
    public /* final */ Z90 c;

    @DexIgnore
    public Y90(String str, int i, short s, Z90 z90) {
        this.b = (short) s;
        this.c = z90;
    }

    @DexIgnore
    public static Y90 valueOf(String str) {
        return (Y90) Enum.valueOf(Y90.class, str);
    }

    @DexIgnore
    public static Y90[] values() {
        return (Y90[]) d.clone();
    }
}
