package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q90 implements Parcelable.Creator<R90> {
    @DexIgnore
    public /* synthetic */ Q90(Qg6 qg6) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public R90 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            Wg6.b(readString, "parcel.readString()!!");
            Ca0 valueOf = Ca0.valueOf(readString);
            String readString2 = parcel.readString();
            if (readString2 != null) {
                Wg6.b(readString2, "parcel.readString()!!");
                W90 valueOf2 = W90.valueOf(readString2);
                String readString3 = parcel.readString();
                if (readString3 != null) {
                    Wg6.b(readString3, "parcel.readString()!!");
                    Da0 valueOf3 = Da0.valueOf(readString3);
                    String readString4 = parcel.readString();
                    if (readString4 != null) {
                        Wg6.b(readString4, "parcel.readString()!!");
                        return new R90(valueOf, valueOf2, valueOf3, Ea0.valueOf(readString4), (short) parcel.readInt());
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public R90[] newArray(int i) {
        return new R90[i];
    }
}
