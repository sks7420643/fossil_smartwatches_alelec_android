package com.fossil;

import com.mapped.Rc6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sa extends Xx1<Ac0[]> {
    @DexIgnore
    public static /* final */ Sa f; // = new Sa();

    @DexIgnore
    public Sa() {
        super(DateTimeFieldType.SECOND_OF_MINUTE, (byte) 253, new Ry1(3, 0));
    }

    @DexIgnore
    @Override // com.fossil.Wx1
    public /* bridge */ /* synthetic */ Object d(byte[] bArr) {
        return f(bArr);
    }

    @DexIgnore
    public Ac0[] f(byte[] bArr) {
        ArrayList arrayList = new ArrayList();
        int i = 12;
        while (i < bArr.length - 1) {
            int i2 = i + 2;
            int n = Hy1.n(ByteBuffer.wrap(Dm7.k(bArr, i, i2)).order(ByteOrder.LITTLE_ENDIAN).getShort(0));
            if (i >= (bArr.length - 1) - n) {
                break;
            }
            Ac0 a2 = Ac0.CREATOR.a(Dm7.k(bArr, i2, i2 + n));
            if (a2 != null) {
                arrayList.add(a2);
            }
            i += n + 2;
        }
        Object[] array = arrayList.toArray(new Ac0[0]);
        if (array != null) {
            return (Ac0[]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
