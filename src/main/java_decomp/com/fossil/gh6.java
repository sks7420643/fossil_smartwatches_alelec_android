package com.fossil;

import com.mapped.U04;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gh6 implements Factory<DashboardGoalTrackingPresenter> {
    @DexIgnore
    public static DashboardGoalTrackingPresenter a(Dh6 dh6, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, U04 u04) {
        return new DashboardGoalTrackingPresenter(dh6, goalTrackingRepository, userRepository, u04);
    }
}
