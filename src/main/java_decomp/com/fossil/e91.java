package com.fossil;

import android.os.Handler;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class E91 implements P91 {
    @DexIgnore
    public /* final */ Executor a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Executor {
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;

        @DexIgnore
        public Ai(E91 e91, Handler handler) {
            this.b = handler;
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            this.b.post(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Runnable {
        @DexIgnore
        public /* final */ M91 b;
        @DexIgnore
        public /* final */ O91 c;
        @DexIgnore
        public /* final */ Runnable d;

        @DexIgnore
        public Bi(M91 m91, O91 o91, Runnable runnable) {
            this.b = m91;
            this.c = o91;
            this.d = runnable;
        }

        @DexIgnore
        public void run() {
            if (this.b.isCanceled()) {
                this.b.finish("canceled-at-delivery");
                return;
            }
            if (this.c.b()) {
                this.b.deliverResponse(this.c.a);
            } else {
                this.b.deliverError(this.c.c);
            }
            if (this.c.d) {
                this.b.addMarker("intermediate-response");
            } else {
                this.b.finish("done");
            }
            Runnable runnable = this.d;
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    @DexIgnore
    public E91(Handler handler) {
        this.a = new Ai(this, handler);
    }

    @DexIgnore
    @Override // com.fossil.P91
    public void a(M91<?> m91, O91<?> o91) {
        b(m91, o91, null);
    }

    @DexIgnore
    @Override // com.fossil.P91
    public void b(M91<?> m91, O91<?> o91, Runnable runnable) {
        m91.markDelivered();
        m91.addMarker("post-response");
        this.a.execute(new Bi(m91, o91, runnable));
    }

    @DexIgnore
    @Override // com.fossil.P91
    public void c(M91<?> m91, T91 t91) {
        m91.addMarker("post-error");
        this.a.execute(new Bi(m91, O91.a(t91), null));
    }
}
