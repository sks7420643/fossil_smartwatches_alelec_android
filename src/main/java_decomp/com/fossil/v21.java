package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class V21 extends S21<Boolean> {
    @DexIgnore
    public static /* final */ String i; // = X01.f("StorageNotLowTracker");

    @DexIgnore
    public V21(Context context, K41 k41) {
        super(context, k41);
    }

    @DexIgnore
    @Override // com.fossil.T21
    public /* bridge */ /* synthetic */ Object b() {
        return i();
    }

    @DexIgnore
    @Override // com.fossil.S21
    public IntentFilter g() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_OK");
        intentFilter.addAction("android.intent.action.DEVICE_STORAGE_LOW");
        return intentFilter;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0045, code lost:
        if (r2.equals("android.intent.action.DEVICE_STORAGE_OK") == false) goto L_0x0034;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0037  */
    @Override // com.fossil.S21
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void h(android.content.Context r8, android.content.Intent r9) {
        /*
            r7 = this;
            r1 = 1
            r0 = 0
            java.lang.String r2 = r9.getAction()
            if (r2 != 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            com.fossil.X01 r2 = com.fossil.X01.c()
            java.lang.String r3 = com.fossil.V21.i
            java.lang.String r4 = r9.getAction()
            java.lang.String r5 = "Received %s"
            java.lang.Object[] r6 = new java.lang.Object[r1]
            r6[r0] = r4
            java.lang.String r4 = java.lang.String.format(r5, r6)
            java.lang.Throwable[] r5 = new java.lang.Throwable[r0]
            r2.a(r3, r4, r5)
            java.lang.String r2 = r9.getAction()
            int r3 = r2.hashCode()
            r4 = -1181163412(0xffffffffb998e06c, float:-2.9158907E-4)
            if (r3 == r4) goto L_0x0048
            r4 = -730838620(0xffffffffd47049a4, float:-4.12811054E12)
            if (r3 == r4) goto L_0x003f
        L_0x0034:
            r0 = -1
        L_0x0035:
            if (r0 == 0) goto L_0x0052
            if (r0 != r1) goto L_0x0008
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            r7.d(r0)
            goto L_0x0008
        L_0x003f:
            java.lang.String r3 = "android.intent.action.DEVICE_STORAGE_OK"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0034
            goto L_0x0035
        L_0x0048:
            java.lang.String r0 = "android.intent.action.DEVICE_STORAGE_LOW"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0034
            r0 = r1
            goto L_0x0035
        L_0x0052:
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            r7.d(r0)
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.V21.h(android.content.Context, android.content.Intent):void");
    }

    @DexIgnore
    public Boolean i() {
        Intent registerReceiver = this.b.registerReceiver(null, g());
        if (registerReceiver == null || registerReceiver.getAction() == null) {
            return Boolean.TRUE;
        }
        String action = registerReceiver.getAction();
        char c = '\uffff';
        int hashCode = action.hashCode();
        if (hashCode != -1181163412) {
            if (hashCode == -730838620 && action.equals("android.intent.action.DEVICE_STORAGE_OK")) {
                c = 0;
            }
        } else if (action.equals("android.intent.action.DEVICE_STORAGE_LOW")) {
            c = 1;
        }
        if (c == 0) {
            return Boolean.TRUE;
        }
        if (c == 1) {
            return Boolean.FALSE;
        }
        return null;
    }
}
