package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class V5 extends Enum<V5> {
    @DexIgnore
    public static /* final */ V5 b;
    @DexIgnore
    public static /* final */ V5 c;
    @DexIgnore
    public static /* final */ V5 d;
    @DexIgnore
    public static /* final */ V5 e;
    @DexIgnore
    public static /* final */ V5 f;
    @DexIgnore
    public static /* final */ V5 g;
    @DexIgnore
    public static /* final */ V5 h;
    @DexIgnore
    public static /* final */ V5 i;
    @DexIgnore
    public static /* final */ V5 j;
    @DexIgnore
    public static /* final */ V5 k;
    @DexIgnore
    public static /* final */ V5 l;
    @DexIgnore
    public static /* final */ V5 m;
    @DexIgnore
    public static /* final */ V5 n;
    @DexIgnore
    public static /* final */ V5 o;
    @DexIgnore
    public static /* final */ /* synthetic */ V5[] p;

    /*
    static {
        V5 v5 = new V5("CLOSE", 0);
        b = v5;
        V5 v52 = new V5("CONNECT", 1);
        c = v52;
        V5 v53 = new V5("DISCONNECT", 2);
        d = v53;
        V5 v54 = new V5("DISCOVER_SERVICE", 3);
        e = v54;
        V5 v55 = new V5("READ_CHARACTERISTIC", 4);
        f = v55;
        V5 v56 = new V5("READ_DESCRIPTOR", 5);
        V5 v57 = new V5("REQUEST_MTU", 6);
        g = v57;
        V5 v58 = new V5("SUBSCRIBE_CHARACTERISTIC", 7);
        h = v58;
        V5 v59 = new V5("WRITE_CHARACTERISTIC", 8);
        i = v59;
        V5 v510 = new V5("WRITE_DESCRIPTOR", 9);
        V5 v511 = new V5("READ_RSSI", 10);
        j = v511;
        V5 v512 = new V5("CREATE_BOND", 11);
        k = v512;
        V5 v513 = new V5("REMOVE_BOND", 12);
        l = v513;
        V5 v514 = new V5("CONNECT_HID", 13);
        m = v514;
        V5 v515 = new V5("DISCONNECT_HID", 14);
        n = v515;
        V5 v516 = new V5("UNKNOWN", 15);
        o = v516;
        p = new V5[]{v5, v52, v53, v54, v55, v56, v57, v58, v59, v510, v511, v512, v513, v514, v515, v516};
    }
    */

    @DexIgnore
    public V5(String str, int i2) {
    }

    @DexIgnore
    public static V5 valueOf(String str) {
        return (V5) Enum.valueOf(V5.class, str);
    }

    @DexIgnore
    public static V5[] values() {
        return (V5[]) p.clone();
    }
}
