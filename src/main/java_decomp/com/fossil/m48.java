package com.fossil;

import com.mapped.Wg6;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class M48 implements A58 {
    @DexIgnore
    public /* final */ A58 b;

    @DexIgnore
    public M48(A58 a58) {
        Wg6.c(a58, "delegate");
        this.b = a58;
    }

    @DexIgnore
    @Override // com.fossil.A58
    public void K(I48 i48, long j) throws IOException {
        Wg6.c(i48, "source");
        this.b.K(i48, j);
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.A58, java.lang.AutoCloseable
    public void close() throws IOException {
        this.b.close();
    }

    @DexIgnore
    @Override // com.fossil.A58
    public D58 e() {
        return this.b.e();
    }

    @DexIgnore
    @Override // com.fossil.A58, java.io.Flushable
    public void flush() throws IOException {
        this.b.flush();
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + '(' + this.b + ')';
    }
}
