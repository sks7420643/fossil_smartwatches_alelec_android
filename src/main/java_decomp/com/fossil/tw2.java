package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Tw2<T> implements Serializable {
    @DexIgnore
    public static <T> Tw2<T> zza(T t) {
        Sw2.b(t);
        return new Vw2(t);
    }

    @DexIgnore
    public static <T> Tw2<T> zzc() {
        return Pw2.zza;
    }

    @DexIgnore
    public abstract boolean zza();

    @DexIgnore
    public abstract T zzb();
}
