package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sp3 extends Ng3 {
    @DexIgnore
    public /* final */ /* synthetic */ Fp3 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Sp3(Fp3 fp3, Ln3 ln3) {
        super(ln3);
        this.e = fp3;
    }

    @DexIgnore
    @Override // com.fossil.Ng3
    public final void b() {
        this.e.d().I().a("Tasks have been queued for a long time");
    }
}
