package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.receiver.SmsMmsReceiver;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eq5 implements Factory<SmsMmsReceiver> {
    @DexIgnore
    public /* final */ Provider<DianaNotificationComponent> a;
    @DexIgnore
    public /* final */ Provider<An4> b;

    @DexIgnore
    public Eq5(Provider<DianaNotificationComponent> provider, Provider<An4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Eq5 a(Provider<DianaNotificationComponent> provider, Provider<An4> provider2) {
        return new Eq5(provider, provider2);
    }

    @DexIgnore
    public static SmsMmsReceiver c() {
        return new SmsMmsReceiver();
    }

    @DexIgnore
    public SmsMmsReceiver b() {
        SmsMmsReceiver c = c();
        Fq5.a(c, this.a.get());
        Fq5.b(c, this.b.get());
        return c;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
