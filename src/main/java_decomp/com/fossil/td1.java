package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Td1 implements Nd1<byte[]> {
    @DexIgnore
    @Override // com.fossil.Nd1
    public int a() {
        return 1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Nd1
    public /* bridge */ /* synthetic */ int b(byte[] bArr) {
        return c(bArr);
    }

    @DexIgnore
    public int c(byte[] bArr) {
        return bArr.length;
    }

    @DexIgnore
    public byte[] d(int i) {
        return new byte[i];
    }

    @DexIgnore
    @Override // com.fossil.Nd1
    public String getTag() {
        return "ByteArrayPool";
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Nd1
    public /* bridge */ /* synthetic */ byte[] newArray(int i) {
        return d(i);
    }
}
