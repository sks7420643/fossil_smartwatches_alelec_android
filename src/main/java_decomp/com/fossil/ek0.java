package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ek0 extends Uj0 {
    @DexIgnore
    public ArrayList<Uj0> k0; // = new ArrayList<>();

    @DexIgnore
    @Override // com.fossil.Uj0
    public void F0() {
        super.F0();
        ArrayList<Uj0> arrayList = this.k0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                Uj0 uj0 = this.k0.get(i);
                uj0.n0(p(), q());
                if (!(uj0 instanceof Vj0)) {
                    uj0.F0();
                }
            }
        }
    }

    @DexIgnore
    public void I0(Uj0 uj0) {
        this.k0.add(uj0);
        if (uj0.u() != null) {
            ((Ek0) uj0.u()).L0(uj0);
        }
        uj0.p0(this);
    }

    @DexIgnore
    public Vj0 J0() {
        Uj0 u = u();
        Vj0 vj0 = this instanceof Vj0 ? (Vj0) this : null;
        while (u != null) {
            Uj0 u2 = u.u();
            if (u instanceof Vj0) {
                vj0 = (Vj0) u;
            }
            u = u2;
        }
        return vj0;
    }

    @DexIgnore
    public void K0() {
        F0();
        ArrayList<Uj0> arrayList = this.k0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                Uj0 uj0 = this.k0.get(i);
                if (uj0 instanceof Ek0) {
                    ((Ek0) uj0).K0();
                }
            }
        }
    }

    @DexIgnore
    public void L0(Uj0 uj0) {
        this.k0.remove(uj0);
        uj0.p0(null);
    }

    @DexIgnore
    public void M0() {
        this.k0.clear();
    }

    @DexIgnore
    @Override // com.fossil.Uj0
    public void Q() {
        this.k0.clear();
        super.Q();
    }

    @DexIgnore
    @Override // com.fossil.Uj0
    public void T(Ij0 ij0) {
        super.T(ij0);
        int size = this.k0.size();
        for (int i = 0; i < size; i++) {
            this.k0.get(i).T(ij0);
        }
    }

    @DexIgnore
    @Override // com.fossil.Uj0
    public void n0(int i, int i2) {
        super.n0(i, i2);
        int size = this.k0.size();
        for (int i3 = 0; i3 < size; i3++) {
            this.k0.get(i3).n0(z(), A());
        }
    }
}
