package com.fossil;

import com.mapped.Er4;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ys5 {
    @DexIgnore
    public String a;
    @DexIgnore
    public ArrayList<Er4> b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public Ys5(String str, String str2, ArrayList<Er4> arrayList, boolean z) {
        Wg6.c(str, "tagName");
        Wg6.c(str2, "title");
        Wg6.c(arrayList, "listItem");
        this.a = str2;
        this.b = arrayList;
        this.c = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Ys5(String str, String str2, ArrayList arrayList, boolean z, int i, Qg6 qg6) {
        this(str, str2, arrayList, (i & 8) != 0 ? false : z);
    }

    @DexIgnore
    public final ArrayList<Er4> a() {
        return this.b;
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final boolean c() {
        return this.c;
    }

    @DexIgnore
    public final void d(boolean z) {
        this.c = z;
    }
}
