package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class As3 implements Parcelable.Creator<Xr3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Xr3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        boolean z = false;
        Vg3 vg3 = null;
        Vg3 vg32 = null;
        Vg3 vg33 = null;
        String str = null;
        Fr3 fr3 = null;
        String str2 = null;
        String str3 = null;
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 2:
                    str3 = Ad2.f(parcel, t);
                    break;
                case 3:
                    str2 = Ad2.f(parcel, t);
                    break;
                case 4:
                    fr3 = (Fr3) Ad2.e(parcel, t, Fr3.CREATOR);
                    break;
                case 5:
                    j3 = Ad2.y(parcel, t);
                    break;
                case 6:
                    z = Ad2.m(parcel, t);
                    break;
                case 7:
                    str = Ad2.f(parcel, t);
                    break;
                case 8:
                    vg33 = (Vg3) Ad2.e(parcel, t, Vg3.CREATOR);
                    break;
                case 9:
                    j2 = Ad2.y(parcel, t);
                    break;
                case 10:
                    vg32 = (Vg3) Ad2.e(parcel, t, Vg3.CREATOR);
                    break;
                case 11:
                    j = Ad2.y(parcel, t);
                    break;
                case 12:
                    vg3 = (Vg3) Ad2.e(parcel, t, Vg3.CREATOR);
                    break;
                default:
                    Ad2.B(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new Xr3(str3, str2, fr3, j3, z, str, vg33, j2, vg32, j, vg3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Xr3[] newArray(int i) {
        return new Xr3[i];
    }
}
