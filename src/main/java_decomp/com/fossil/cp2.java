package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cp2 implements Parcelable.Creator<Zo2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Zo2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        Uh2 uh2 = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            if (Ad2.l(t) != 1) {
                Ad2.B(parcel, t);
            } else {
                uh2 = (Uh2) Ad2.e(parcel, t, Uh2.CREATOR);
            }
        }
        Ad2.k(parcel, C);
        return new Zo2(uh2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Zo2[] newArray(int i) {
        return new Zo2[i];
    }
}
