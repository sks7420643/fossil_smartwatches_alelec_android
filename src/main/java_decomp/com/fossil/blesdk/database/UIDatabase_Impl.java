package com.fossil.blesdk.database;

import com.fossil.Ex0;
import com.fossil.G0;
import com.fossil.Hw0;
import com.fossil.Ix0;
import com.fossil.Lx0;
import com.fossil.Nw0;
import com.mapped.Ji;
import com.mapped.Oh;
import com.mapped.Qh;
import com.portfolio.platform.data.model.Firmware;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UIDatabase_Impl extends UIDatabase {
    @DexIgnore
    public volatile G0 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends Qh.Ai {
        @DexIgnore
        public a(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void createAllTables(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `ThemeTemplateEntity` (`id` TEXT NOT NULL, `classifier` INTEGER NOT NULL, `packageOSVersion` TEXT NOT NULL, `checksum` TEXT NOT NULL, `downloadUrl` TEXT NOT NULL, `updatedAt` INTEGER NOT NULL, `createdAt` INTEGER NOT NULL, `data` BLOB, PRIMARY KEY(`classifier`, `packageOSVersion`))");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '61814d7411cfec8e2060487dde0de32b')");
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void dropAllTables(Lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `ThemeTemplateEntity`");
            if (UIDatabase_Impl.this.mCallbacks != null) {
                int size = UIDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) UIDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onCreate(Lx0 lx0) {
            if (UIDatabase_Impl.this.mCallbacks != null) {
                int size = UIDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) UIDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onOpen(Lx0 lx0) {
            UIDatabase_Impl.this.mDatabase = lx0;
            UIDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (UIDatabase_Impl.this.mCallbacks != null) {
                int size = UIDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) UIDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPostMigrate(Lx0 lx0) {
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPreMigrate(Lx0 lx0) {
            Ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public Qh.Bi onValidateSchema(Lx0 lx0) {
            HashMap hashMap = new HashMap(8);
            hashMap.put("id", new Ix0.Ai("id", "TEXT", true, 0, null, 1));
            hashMap.put("classifier", new Ix0.Ai("classifier", "INTEGER", true, 1, null, 1));
            hashMap.put("packageOSVersion", new Ix0.Ai("packageOSVersion", "TEXT", true, 2, null, 1));
            hashMap.put("checksum", new Ix0.Ai("checksum", "TEXT", true, 0, null, 1));
            hashMap.put(Firmware.COLUMN_DOWNLOAD_URL, new Ix0.Ai(Firmware.COLUMN_DOWNLOAD_URL, "TEXT", true, 0, null, 1));
            hashMap.put("updatedAt", new Ix0.Ai("updatedAt", "INTEGER", true, 0, null, 1));
            hashMap.put("createdAt", new Ix0.Ai("createdAt", "INTEGER", true, 0, null, 1));
            hashMap.put("data", new Ix0.Ai("data", "BLOB", false, 0, null, 1));
            Ix0 ix0 = new Ix0("ThemeTemplateEntity", hashMap, new HashSet(0), new HashSet(0));
            Ix0 a2 = Ix0.a(lx0, "ThemeTemplateEntity");
            if (ix0.equals(a2)) {
                return new Qh.Bi(true, null);
            }
            return new Qh.Bi(false, "ThemeTemplateEntity(com.fossil.blesdk.database.entity.ThemeTemplateEntity).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
        }
    }

    @DexIgnore
    @Override // com.fossil.blesdk.database.UIDatabase
    public G0 a() {
        G0 g0;
        if (this.e != null) {
            return this.e;
        }
        synchronized (this) {
            if (this.e == null) {
                this.e = new G0(this);
            }
            g0 = this.e;
        }
        return g0;
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public void clearAllTables() {
        super.assertNotMainThread();
        Lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `ThemeTemplateEntity`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Nw0 createInvalidationTracker() {
        return new Nw0(this, new HashMap(0), new HashMap(0), "ThemeTemplateEntity");
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Ji createOpenHelper(Hw0 hw0) {
        Qh qh = new Qh(hw0, new a(1), "61814d7411cfec8e2060487dde0de32b", "52e083e2630cbc37ce444cf873a985b3");
        Ji.Bi.Aii a2 = Ji.Bi.a(hw0.b);
        a2.c(hw0.c);
        a2.b(qh);
        return hw0.a.create(a2.a());
    }
}
