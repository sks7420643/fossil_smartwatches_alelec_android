package com.fossil.blesdk.model.uiframework.packages.theme;

import com.fossil.Ax1;
import com.fossil.Bx1;
import com.fossil.E;
import com.fossil.Ec0;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hc0;
import com.fossil.Id0;
import com.fossil.Ko7;
import com.fossil.Lw1;
import com.fossil.Mw1;
import com.fossil.Nc0;
import com.fossil.Nx1;
import com.fossil.O;
import com.fossil.Ry1;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.mapped.Zb0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ThemeEditor implements Nx1 {
    @DexIgnore
    public static /* final */ Ry1 f; // = new Ry1(3, 2);
    @DexIgnore
    public String b;
    @DexIgnore
    public Mw1 c;
    @DexIgnore
    public /* final */ Ry1 d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor$buildThemePackage$1", f = "ThemeEditor.kt", l = {68}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Il6 b;
        @DexIgnore
        public Object c;
        @DexIgnore
        public int d;
        @DexIgnore
        public /* final */ /* synthetic */ ThemeEditor e;
        @DexIgnore
        public /* final */ /* synthetic */ Ry1 f;
        @DexIgnore
        public /* final */ /* synthetic */ Zb0 g;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(ThemeEditor themeEditor, Ry1 ry1, Zb0 zb0, Xe6 xe6) {
            super(2, xe6);
            this.e = themeEditor;
            this.f = ry1;
            this.g = zb0;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Ai ai = new Ai(this.e, this.f, this.g, xe6);
            ai.b = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object a2;
            Object d2 = Yn7.d();
            int i = this.d;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.b;
                Nc0 nc0 = Nc0.a;
                Ec0 g2 = this.e.g();
                Ry1 ry1 = this.f;
                if (ry1 == null) {
                    ry1 = ThemeEditor.f;
                }
                this.c = il6;
                this.d = 1;
                a2 = nc0.a(g2, ry1, true, this);
                if (a2 == d2) {
                    return d2;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.c;
                El7.b(obj);
                a2 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            O o = (O) a2;
            V v = o.a;
            if (v != null) {
                Lw1 a3 = this.e.a(v);
                if (a3 != null) {
                    this.g.o(a3);
                } else {
                    Zb0 zb0 = this.g;
                    Ax1 ax1 = o.b;
                    if (ax1 == null) {
                        ax1 = new Ax1(Bx1.NETWORK_UNAVAILABLE, null);
                    }
                    zb0.n(ax1);
                }
            } else {
                Zb0 zb02 = this.g;
                Ax1 ax12 = o.b;
                if (ax12 == null) {
                    ax12 = new Ax1(Bx1.NETWORK_UNAVAILABLE, null);
                }
                zb02.n(ax12);
            }
            return Cd6.a;
        }
    }

    @DexIgnore
    public ThemeEditor(Ry1 ry1, String str, String str2) {
        this.d = ry1;
        this.e = str;
        this.b = str2;
    }

    @DexIgnore
    public /* synthetic */ ThemeEditor(Ry1 ry1, String str, String str2, int i) {
        String str3;
        if ((i & 2) != 0) {
            String a2 = E.a("UUID.randomUUID().toString()");
            if (a2 != null) {
                str3 = a2.substring(0, 16);
                Wg6.b(str3, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            } else {
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
        } else {
            str3 = str;
        }
        str2 = (i & 4) != 0 ? str3 : str2;
        this.d = ry1;
        this.e = str3;
        this.b = str2;
    }

    @DexIgnore
    public abstract Lw1 a(Hc0 hc0);

    @DexIgnore
    public final Ry1 b() {
        return this.d;
    }

    @DexIgnore
    public final Zb0<Lw1> c(Ry1 ry1) {
        Zb0<Lw1> zb0 = new Zb0<>();
        Rm6 unused = Gu7.d(Id0.i.e(), null, null, new Ai(this, ry1, zb0, null), 3, null);
        return zb0;
    }

    @DexIgnore
    public final String d() {
        return this.e;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public final Mw1 f() {
        return this.c;
    }

    @DexIgnore
    public abstract Ec0 g();
}
