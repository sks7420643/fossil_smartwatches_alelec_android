package com.fossil.blesdk.adapter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.PlaceManager;
import com.fossil.A90;
import com.fossil.Al1;
import com.fossil.Ct0;
import com.fossil.D90;
import com.fossil.Dm7;
import com.fossil.Dy1;
import com.fossil.E60;
import com.fossil.Em7;
import com.fossil.Et7;
import com.fossil.Ey1;
import com.fossil.F;
import com.fossil.G;
import com.fossil.G80;
import com.fossil.H;
import com.fossil.Hd0;
import com.fossil.Hl7;
import com.fossil.Hy1;
import com.fossil.I;
import com.fossil.Id0;
import com.fossil.J;
import com.fossil.Jd0;
import com.fossil.K;
import com.fossil.L80;
import com.fossil.M80;
import com.fossil.Nr;
import com.fossil.Ox1;
import com.fossil.Pm7;
import com.fossil.Q3;
import com.fossil.R4;
import com.fossil.Uk1;
import com.fossil.V80;
import com.fossil.Vk1;
import com.fossil.W00;
import com.fossil.Xk1;
import com.fossil.Ym;
import com.fossil.Zk1;
import com.fossil.Zm7;
import com.fossil.Zq;
import com.fossil.common.task.Promise;
import com.mapped.Cd6;
import com.mapped.Q40;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.joda.time.DateTimeFieldType;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BluetoothLeAdapter {
    @DexIgnore
    public static /* final */ Handler a;
    @DexIgnore
    public static /* final */ BluetoothAdapter b; // = BluetoothAdapter.getDefaultAdapter();
    @DexIgnore
    public static Fi c;
    @DexIgnore
    public static /* final */ Hashtable<String, Promise<Zk1, Nr>> d; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ ScanCallback e; // = new Ii();
    @DexIgnore
    public static /* final */ LinkedHashMap<Bi, Xk1> f; // = new LinkedHashMap<>();
    @DexIgnore
    public static /* final */ BroadcastReceiver g; // = new Gi();
    @DexIgnore
    public static /* final */ BroadcastReceiver h; // = new Hi();
    @DexIgnore
    public static boolean i;
    @DexIgnore
    public static Ai j;
    @DexIgnore
    public static /* final */ BluetoothLeAdapter k; // = new BluetoothLeAdapter();

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(BluetoothLeAdapter bluetoothLeAdapter, Ci ci, Ci ci2);
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void onDeviceFound(Q40 q40, int i);

        @DexIgnore
        void onStartScanFailed(Uk1 uk1);
    }

    @DexIgnore
    public enum Ci {
        DISABLED(10),
        ENABLING(11),
        ENABLED(12),
        DISABLING(13);
        
        @DexIgnore
        public static /* final */ Aii d; // = new Aii(null);
        @DexIgnore
        public /* final */ int b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
            }

            @DexIgnore
            public final Ci a(int i) {
                Ci ci;
                Ci[] values = Ci.values();
                int length = values.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        ci = null;
                        break;
                    }
                    ci = values[i2];
                    if (ci.a() == i) {
                        break;
                    }
                    i2++;
                }
                return ci != null ? ci : Ci.DISABLED;
            }
        }

        @DexIgnore
        public Ci(int i) {
            this.b = i;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class Di extends Enum<Di> {
        @DexIgnore
        public static /* final */ Di c;
        @DexIgnore
        public static /* final */ Di d;
        @DexIgnore
        public static /* final */ Di e;
        @DexIgnore
        public static /* final */ Di f;
        @DexIgnore
        public static /* final */ Di g;
        @DexIgnore
        public static /* final */ Di h;
        @DexIgnore
        public static /* final */ Di i;
        @DexIgnore
        public static /* final */ Di j;
        @DexIgnore
        public static /* final */ Di k;
        @DexIgnore
        public static /* final */ /* synthetic */ Di[] l;
        @DexIgnore
        public static /* final */ F m; // = new F(null);
        @DexIgnore
        public /* final */ byte b;

        /*
        static {
            Di di = new Di("FLAG", 0, (byte) 1);
            Di di2 = new Di("INCOMPLETE_16_BIT_SERVICE_CLASS_UUID", 1, (byte) 2);
            c = di2;
            Di di3 = new Di("COMPLETE_16_BIT_SERVICE_CLASS_UUID", 2, (byte) 3);
            d = di3;
            Di di4 = new Di("INCOMPLETE_32_BIT_SERVICE_CLASS_UUID", 3, (byte) 4);
            e = di4;
            Di di5 = new Di("COMPLETE_32_BIT_SERVICE_CLASS_UUID", 4, (byte) 5);
            f = di5;
            Di di6 = new Di("INCOMPLETE_128_BIT_SERVICE_CLASS_UUID", 5, (byte) 6);
            g = di6;
            Di di7 = new Di("COMPLETE_128_BIT_SERVICE_CLASS_UUID", 6, (byte) 7);
            h = di7;
            Di di8 = new Di("COMPLETE_LOCAL_NAME", 7, (byte) 9);
            i = di8;
            Di di9 = new Di("SERVICE_DATA_16_BIT_SERVICE_CLASS_UUID", 8, DateTimeFieldType.MILLIS_OF_DAY);
            j = di9;
            Di di10 = new Di("MANUFACTURER_SPECIFIC_DATA", 9, (byte) 255);
            k = di10;
            l = new Di[]{di, di2, di3, di4, di5, di6, di7, di8, di9, di10};
        }
        */

        @DexIgnore
        public Di(String str, int i2, byte b2) {
            this.b = (byte) b2;
        }

        @DexIgnore
        public static Di valueOf(String str) {
            return (Di) Enum.valueOf(Di.class, str);
        }

        @DexIgnore
        public static Di[] values() {
            return (Di[]) l.clone();
        }
    }

    @DexIgnore
    public enum Ei {
        b,
        c,
        d,
        e,
        f,
        g,
        h,
        i,
        j
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements Runnable {
        @DexIgnore
        public boolean b;

        @DexIgnore
        public final JSONArray a(List<BluetoothDevice> list) {
            JSONArray jSONArray = new JSONArray();
            for (T t : list) {
                JSONObject jSONObject = new JSONObject();
                Jd0 jd0 = Jd0.H;
                Object name = t.getName();
                if (name == null) {
                    name = JSONObject.NULL;
                }
                jSONArray.put(G80.k(G80.k(G80.k(jSONObject, jd0, name), Jd0.k0, t.getAddress()), Jd0.o1, R4.f.a(t.getBondState())));
            }
            return jSONArray;
        }

        @DexIgnore
        public void run() {
            if (!this.b) {
                M80.c.a("BluetoothLeAdapter", "getGattConnectedDevice.", new Object[0]);
                List<BluetoothDevice> b2 = BluetoothLeAdapter.k.b();
                M80.c.a("BluetoothLeAdapter", "gattConnectedBluetoothDevices: %s.", Pm7.N(b2, null, null, null, 0, null, G.b, 31, null));
                D90.i.d(new A90(Ey1.a(Ei.g), V80.h, "", "", "", true, null, null, null, G80.k(new JSONObject(), Jd0.D1, a(b2)), 448));
                List<BluetoothDevice> o = BluetoothLeAdapter.k.o();
                M80.c.a("BluetoothLeAdapter", "hidConnectedBluetoothDevices: %s.", Pm7.N(o, null, null, null, 0, null, H.b, 31, null));
                D90.i.d(new A90(Ey1.a(Ei.h), V80.h, "", "", "", true, null, null, null, G80.k(new JSONObject(), Jd0.E1, a(o)), 448));
                ArrayList arrayList = new ArrayList(b2);
                arrayList.addAll(o);
                for (BluetoothDevice bluetoothDevice : Pm7.C(arrayList)) {
                    if (bluetoothDevice.getType() != 1) {
                        W00 w00 = W00.c;
                        String address = bluetoothDevice.getAddress();
                        Wg6.b(address, "bluetoothDevice.address");
                        String e = w00.e(address);
                        if (e != null) {
                            E60 a2 = W00.c.a(bluetoothDevice, e);
                            BluetoothLeAdapter bluetoothLeAdapter = BluetoothLeAdapter.k;
                            if (bluetoothLeAdapter.p(a2)) {
                                bluetoothLeAdapter.d(a2, 0);
                            }
                        } else {
                            BluetoothLeAdapter bluetoothLeAdapter2 = BluetoothLeAdapter.k;
                            if (BluetoothLeAdapter.d.get(bluetoothDevice.getAddress()) == null) {
                                E60 a3 = W00.c.a(bluetoothDevice, "");
                                HashMap<Ym, Object> i = Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 30000L));
                                M80.c.a("BluetoothLeAdapter", "getGattConnectedDevice: makeDeviceReady to get Serial Number.", new Object[0]);
                                Promise<Zk1, Nr> z0 = a3.z0(i);
                                BluetoothLeAdapter bluetoothLeAdapter3 = BluetoothLeAdapter.k;
                                BluetoothLeAdapter.d.put(bluetoothDevice.getAddress(), z0);
                                z0.m(new I(bluetoothDevice, a3)).l(new J(bluetoothDevice));
                            }
                        }
                    }
                }
                BluetoothLeAdapter bluetoothLeAdapter4 = BluetoothLeAdapter.k;
                BluetoothLeAdapter.a.postDelayed(this, 5000);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            String action;
            if (context != null && intent != null && (action = intent.getAction()) != null && action.hashCode() == -1530327060 && action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                BluetoothLeAdapter.k.k(Ci.d.a(intent.getIntExtra("android.bluetooth.adapter.extra.PREVIOUS_STATE", RecyclerView.UNDEFINED_DURATION)), Ci.d.a(intent.getIntExtra("android.bluetooth.adapter.extra.STATE", RecyclerView.UNDEFINED_DURATION)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (context != null && intent != null) {
                BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                int intExtra = intent.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", -1);
                int intExtra2 = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -1);
                if (bluetoothDevice != null) {
                    BluetoothLeAdapter.k.e(bluetoothDevice, intExtra, intExtra2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii extends ScanCallback {
        @DexIgnore
        public void onScanFailed(int i) {
            BluetoothLeAdapter.k.m(new Uk1(Vk1.f.a(i)));
        }

        @DexIgnore
        public void onScanResult(int i, ScanResult scanResult) {
            ScanRecord scanRecord;
            byte[] bytes = (scanResult == null || (scanRecord = scanResult.getScanRecord()) == null) ? null : scanRecord.getBytes();
            BluetoothDevice device = scanResult != null ? scanResult.getDevice() : null;
            if (bytes != null && device != null) {
                if (BluetoothLeAdapter.k.t(bytes)) {
                    String v = BluetoothLeAdapter.k.v(bytes);
                    if (Zk1.u.d(v)) {
                        E60 a2 = W00.c.a(device, v);
                        if (BluetoothLeAdapter.k.p(a2)) {
                            BluetoothLeAdapter.k.d(a2, scanResult.getRssi());
                        }
                    }
                } else if (BluetoothLeAdapter.k.u(bytes)) {
                    E60 a3 = W00.c.a(device, "");
                    Zk1 a4 = Zk1.a(a3.u, BluetoothLeAdapter.k.a(bytes), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, Dy1.e(BluetoothLeAdapter.k.q(bytes), null, 1, null), 131070);
                    a3.u = a4;
                    a4.a(Al1.WEAR_OS);
                    BluetoothLeAdapter.k.d(a3, scanResult.getRssi());
                }
            }
        }
    }

    /*
    static {
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        if (myLooper != null) {
            a = new Handler(myLooper);
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ void l(BluetoothLeAdapter bluetoothLeAdapter, E60 e60) {
        if (bluetoothLeAdapter.p(e60)) {
            bluetoothLeAdapter.d(e60, 0);
        }
    }

    @DexIgnore
    public final String a(byte[] bArr) {
        int i2 = 0;
        while (i2 < bArr.length) {
            int i3 = i2 + 1;
            short p = Hy1.p((byte) (bArr[i2] & ((byte) 255)));
            if (p == 0 || i3 + 1 > bArr.length) {
                break;
            } else if (Di.m.a(bArr[i3]) == Di.i) {
                return new String(Dm7.k(Dm7.k(bArr, i3, i3 + p), 1, p), Hd0.y.c());
            } else {
                i2 = p + i3;
            }
        }
        return "";
    }

    @DexIgnore
    public final List<BluetoothDevice> b() {
        List<BluetoothDevice> list = null;
        ArrayList arrayList = new ArrayList();
        Context a2 = Id0.i.a();
        BluetoothManager bluetoothManager = (BluetoothManager) (a2 != null ? a2.getSystemService(PlaceManager.PARAM_BLUETOOTH) : null);
        if (bluetoothManager != null) {
            list = bluetoothManager.getConnectedDevices(7);
        }
        if (list != null) {
            arrayList.addAll(list);
        }
        return arrayList;
    }

    @DexIgnore
    public final JSONObject c(E60 e60) {
        return G80.k(G80.k(G80.k(G80.k(new JSONObject(), Jd0.l0, e60.u.getSerialNumber()), Jd0.n1, Ey1.a(e60.v)), Jd0.k0, e60.u.getMacAddress()), Jd0.o1, Ey1.a(Q40.Ai.c.b(e60.d.H())));
    }

    @DexIgnore
    public final void d(E60 e60, int i2) {
        Set<Map.Entry<Bi, Xk1>> entrySet;
        synchronized (f) {
            entrySet = f.entrySet();
            Wg6.b(entrySet, "activeLeScanCallbacks.entries");
            Cd6 cd6 = Cd6.a;
        }
        for (T t : entrySet) {
            if (((Xk1) t.getValue()).a(e60)) {
                k.h((Bi) t.getKey(), e60, i2);
            }
        }
    }

    @DexIgnore
    public final void e(BluetoothDevice bluetoothDevice, int i2, int i3) {
        String str;
        M80 m80 = M80.c;
        String address = bluetoothDevice.getAddress();
        String str2 = "UNKNOWN";
        switch (i2) {
            case 10:
                str = "BOND_NONE";
                break;
            case 11:
                str = "BOND_BONDING";
                break;
            case 12:
                str = "BOND_BONDED";
                break;
            default:
                str = "UNKNOWN";
                break;
        }
        switch (i3) {
            case 10:
                str2 = "BOND_NONE";
                break;
            case 11:
                str2 = "BOND_BONDING";
                break;
            case 12:
                str2 = "BOND_BONDED";
                break;
        }
        m80.a("BluetoothLeAdapter", "Bond state changed: device=%s, %s(%d) -> %s(%d).", address, str, Integer.valueOf(i2), str2, Integer.valueOf(i3));
        D90 d90 = D90.i;
        String a2 = Ey1.a(Ei.i);
        V80 v80 = V80.h;
        String address2 = bluetoothDevice.getAddress();
        if (address2 == null) {
            address2 = "";
        }
        JSONObject jSONObject = new JSONObject();
        Jd0 jd0 = Jd0.k0;
        String address3 = bluetoothDevice.getAddress();
        d90.d(new A90(a2, v80, address2, "", "", true, null, null, null, G80.k(G80.k(G80.k(jSONObject, jd0, address3 != null ? address3 : ""), Jd0.C0, Ey1.a(Q40.Ai.c.a(i2))), Jd0.B0, Ey1.a(Q40.Ai.c.a(i3))), 448));
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.BLUETOOTH_DEVICE", bluetoothDevice);
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_BOND_STATE", i2);
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_BOND_STATE", i3);
        Context a3 = Id0.i.a();
        if (a3 != null) {
            Ct0.b(a3).d(intent);
        }
    }

    @DexIgnore
    public final void f(Context context) {
        context.registerReceiver(g, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        context.registerReceiver(h, new IntentFilter("android.bluetooth.device.action.BOND_STATE_CHANGED"));
    }

    @DexIgnore
    public final void g(Bi bi) {
        boolean z;
        BluetoothLeScanner bluetoothLeScanner;
        M80.c.a("BluetoothLeAdapter", "internalStopScan invoked: leScanCallback=%s.", Hy1.k(bi.hashCode(), null, 1, null));
        synchronized (f) {
            f.remove(bi);
            z = f.size() == 0;
            Cd6 cd6 = Cd6.a;
        }
        if (z) {
            Fi fi = c;
            if (fi != null) {
                fi.b = true;
            }
            Fi fi2 = c;
            if (fi2 != null) {
                a.removeCallbacks(fi2);
            }
            c = null;
            Collection<Promise<Zk1, Nr>> values = d.values();
            Wg6.b(values, "deviceInFetchingInformation.values");
            Object[] array = values.toArray(new Promise[0]);
            if (array != null) {
                for (Object obj : array) {
                    ((Promise) obj).d(new Nr(null, Zq.G, null, null, 13));
                }
                BluetoothAdapter bluetoothAdapter = b;
                if (!(bluetoothAdapter == null || (bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner()) == null)) {
                    bluetoothLeScanner.stopScan(e);
                }
                W00.c.c();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public final void h(Bi bi, E60 e60, int i2) {
        M80.c.a("BluetoothLeAdapter", "return found device: MAC=%s, Serial=%s, rssi=%d.", e60.u.getMacAddress(), e60.u.getSerialNumber(), Integer.valueOf(i2));
        D90.i.d(new A90(Ey1.a(Ei.f), V80.h, "", "", "", true, null, null, null, G80.k(G80.k(G80.k(new JSONObject(), Jd0.F1, Hy1.k(bi.hashCode(), null, 1, null)), Jd0.m1, c(e60)), Jd0.c, Integer.valueOf(i2)), 448));
        bi.onDeviceFound(e60, i2);
    }

    @DexIgnore
    public final void i(Bi bi, Uk1 uk1) {
        D90.i.d(new A90(Ey1.a(Ei.c), V80.h, "", "", "", false, null, null, null, G80.k(G80.k(new JSONObject(), Jd0.F1, String.valueOf(bi.hashCode())), Jd0.G1, uk1.toJSONObject()), 448));
        M80.c.a("BluetoothLeAdapter", "startScan FAILED: leScanCallback=%s, error=%s.", Hy1.k(bi.hashCode(), null, 1, null), uk1.toJSONString(2));
        bi.onStartScanFailed(uk1);
    }

    @DexIgnore
    public final void j(Bi bi, Xk1 xk1) {
        boolean z = true;
        M80.c.a("BluetoothLeAdapter", "internalStartScan invoked: deviceScanFilter=%s, leScanCallback=%s.", Ox1.toJSONString$default(xk1, 0, 1, null), Hy1.k(bi.hashCode(), null, 1, null));
        if (w() != Ci.ENABLED) {
            i(bi, new Uk1(Vk1.BLUETOOTH_OFF));
            return;
        }
        synchronized (f) {
            f.put(bi, xk1);
            if (f.size() != 1) {
                z = false;
            }
            Cd6 cd6 = Cd6.a;
        }
        if (z) {
            ArrayList arrayList = new ArrayList();
            ScanSettings build = new ScanSettings.Builder().setScanMode(2).build();
            Wg6.b(build, "ScanSettings.Builder()\n \u2026MODE_LOW_LATENCY).build()");
            n(arrayList, build);
            Fi fi = new Fi();
            c = fi;
            if (fi != null) {
                fi.run();
            }
        }
    }

    @DexIgnore
    public final void k(Ci ci, Ci ci2) {
        M80.c.a("BluetoothLeAdapter", "Bluetooth state changed: %s -> %s.", ci, ci2);
        D90.i.d(new A90(Ey1.a(Ei.j), V80.g, "", "", "", true, null, null, null, G80.k(G80.k(new JSONObject(), Jd0.C0, Ey1.a(ci)), Jd0.B0, Ey1.a(ci2)), 448));
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_STATE", ci);
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_STATE", ci2);
        Context a2 = Id0.i.a();
        if (a2 != null) {
            Ct0.b(a2).d(intent);
        }
        int i2 = K.b[ci2.ordinal()];
        if (i2 == 2 || i2 == 3 || i2 == 4) {
            s();
            M80.c.a("BluetoothLeAdapter", "Stop all done: %d.", Integer.valueOf(f.size()));
        }
        Ai ai = j;
        if (ai != null) {
            ai.a(this, ci, ci2);
        }
    }

    @DexIgnore
    public final void m(Uk1 uk1) {
        Set<Bi> keySet;
        synchronized (f) {
            keySet = f.keySet();
            Wg6.b(keySet, "activeLeScanCallbacks.keys");
            Cd6 cd6 = Cd6.a;
        }
        Iterator<T> it = keySet.iterator();
        while (it.hasNext()) {
            k.i(it.next(), uk1);
        }
    }

    @DexIgnore
    public final void n(List<ScanFilter> list, ScanSettings scanSettings) {
        BluetoothAdapter bluetoothAdapter = b;
        BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter != null ? bluetoothAdapter.getBluetoothLeScanner() : null;
        if (bluetoothLeScanner == null) {
            m(new Uk1(Vk1.UNKNOWN_ERROR));
        } else {
            bluetoothLeScanner.startScan(list, scanSettings, e);
        }
    }

    @DexIgnore
    public final List<BluetoothDevice> o() {
        return L80.d.d();
    }

    @DexIgnore
    public final boolean p(E60 e60) {
        return i || e60.u.getDeviceType() != Al1.UNKNOWN;
    }

    @DexIgnore
    public final byte[] q(byte[] bArr) {
        int i2 = 0;
        while (i2 < bArr.length) {
            int i3 = i2 + 1;
            short p = Hy1.p((byte) (bArr[i2] & ((byte) 255)));
            if (p == 0 || i3 + 1 > bArr.length) {
                break;
            }
            if (Di.m.a(bArr[i3]) == Di.j && p >= 3) {
                byte[] k2 = Dm7.k(bArr, i3, i3 + p);
                ByteBuffer order = ByteBuffer.wrap(Dm7.k(k2, 1, 3)).order(ByteOrder.LITTLE_ENDIAN);
                Wg6.b(order, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
                if (order.getShort() == -468) {
                    return Dm7.k(k2, 3, p);
                }
            }
            i2 = p + i3;
        }
        return new byte[0];
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.HashMap<com.fossil.blesdk.adapter.BluetoothLeAdapter$Di, java.lang.String[]> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d A[LOOP:1: B:17:0x005b->B:18:0x005d, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0096 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.HashMap<com.fossil.blesdk.adapter.BluetoothLeAdapter.Di, java.lang.String[]> r(byte[] r14) {
        /*
            r13 = this;
            r12 = 0
            r1 = 0
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            r0 = r1
        L_0x0008:
            int r2 = r14.length
            if (r0 >= r2) goto L_0x001f
            int r4 = r0 + 1
            byte r0 = r14[r0]
            r2 = 255(0xff, float:3.57E-43)
            byte r2 = (byte) r2
            r0 = r0 & r2
            byte r0 = (byte) r0
            short r5 = com.fossil.Hy1.p(r0)
            if (r5 == 0) goto L_0x001f
            int r6 = r4 + 1
            int r0 = r14.length
            if (r6 <= r0) goto L_0x0020
        L_0x001f:
            return r3
        L_0x0020:
            com.fossil.F r0 = com.fossil.blesdk.adapter.BluetoothLeAdapter.Di.m
            byte r2 = r14[r4]
            com.fossil.blesdk.adapter.BluetoothLeAdapter$Di r7 = r0.a(r2)
            if (r7 != 0) goto L_0x006e
        L_0x002a:
            r2 = r1
        L_0x002b:
            if (r7 == 0) goto L_0x0092
            if (r2 <= 0) goto L_0x0092
            int r0 = r4 + r5
            byte[] r6 = java.util.Arrays.copyOfRange(r14, r6, r0)
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            java.lang.Object r0 = r3.get(r7)
            java.lang.String[] r0 = (java.lang.String[]) r0
            if (r0 == 0) goto L_0x004a
            java.lang.String r9 = "currentServiceUUIDs"
            com.mapped.Wg6.b(r0, r9)
            com.fossil.Mm7.t(r8, r0)
        L_0x004a:
            java.lang.String r0 = "serviceUUIDRaw"
            com.mapped.Wg6.b(r6, r0)
            byte[][] r2 = com.fossil.Dy1.b(r6, r2)
            java.util.ArrayList r6 = new java.util.ArrayList
            int r0 = r2.length
            r6.<init>(r0)
            int r9 = r2.length
            r0 = r1
        L_0x005b:
            if (r0 >= r9) goto L_0x0084
            r10 = r2[r0]
            byte[] r10 = com.fossil.Em7.V(r10)
            r11 = 1
            java.lang.String r10 = com.fossil.Dy1.e(r10, r12, r11, r12)
            r6.add(r10)
            int r0 = r0 + 1
            goto L_0x005b
        L_0x006e:
            int[] r0 = com.fossil.K.a
            int r2 = r7.ordinal()
            r0 = r0[r2]
            switch(r0) {
                case 1: goto L_0x007a;
                case 2: goto L_0x007a;
                case 3: goto L_0x0081;
                case 4: goto L_0x0081;
                case 5: goto L_0x007d;
                case 6: goto L_0x007d;
                default: goto L_0x0079;
            }
        L_0x0079:
            goto L_0x002a
        L_0x007a:
            r0 = 2
            r2 = r0
            goto L_0x002b
        L_0x007d:
            r0 = 16
            r2 = r0
            goto L_0x002b
        L_0x0081:
            r0 = 4
            r2 = r0
            goto L_0x002b
        L_0x0084:
            r8.addAll(r6)
            java.lang.String[] r0 = new java.lang.String[r1]
            java.lang.Object[] r0 = r8.toArray(r0)
            if (r0 == 0) goto L_0x0096
            r3.put(r7, r0)
        L_0x0092:
            int r0 = r5 + r4
            goto L_0x0008
        L_0x0096:
            com.mapped.Rc6 r0 = new com.mapped.Rc6
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.Array<T>"
            r0.<init>(r1)
            throw r0
            switch-data {1->0x007a, 2->0x007a, 3->0x0081, 4->0x0081, 5->0x007d, 6->0x007d, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.blesdk.adapter.BluetoothLeAdapter.r(byte[]):java.util.HashMap");
    }

    @DexIgnore
    public final void s() {
        Bi[] biArr;
        synchronized (f) {
            Set<Bi> keySet = f.keySet();
            Wg6.b(keySet, "activeLeScanCallbacks.keys");
            Object[] array = keySet.toArray(new Bi[0]);
            if (array != null) {
                biArr = (Bi[]) array;
                Cd6 cd6 = Cd6.a;
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        for (Bi bi : biArr) {
            k.g(bi);
        }
    }

    @DexIgnore
    public final boolean t(byte[] bArr) {
        HashMap<Di, String[]> r = r(bArr);
        String[] strArr = r.get(Di.g);
        if (strArr != null && Em7.B(strArr, Hd0.y.v())) {
            return true;
        }
        String[] strArr2 = r.get(Di.h);
        return strArr2 != null && Em7.B(strArr2, Hd0.y.v());
    }

    @DexIgnore
    public final boolean u(byte[] bArr) {
        int i2 = 0;
        while (i2 < bArr.length) {
            int i3 = i2 + 1;
            short p = Hy1.p((byte) (bArr[i2] & ((byte) 255)));
            if (p == 0 || i3 + 1 > bArr.length) {
                return false;
            }
            if (Di.m.a(bArr[i3]) == Di.k && p >= 3) {
                ByteBuffer order = ByteBuffer.wrap(Dm7.k(Dm7.k(bArr, i3, i3 + p), 1, 3)).order(ByteOrder.LITTLE_ENDIAN);
                Wg6.b(order, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
                if (order.getShort() == 224) {
                    return true;
                }
            }
            i2 = p + i3;
        }
        return false;
    }

    @DexIgnore
    public final String v(byte[] bArr) {
        byte[] bArr2;
        byte b2;
        if (bArr == null) {
            return new String();
        }
        int i2 = 0;
        while (true) {
            if (i2 >= bArr.length) {
                break;
            }
            int i3 = i2 + 1;
            short p = Hy1.p(bArr[i2]);
            if (p != 0 && (b2 = bArr[i3]) != ((byte) 0)) {
                if (b2 == -1 && p >= 13) {
                    int i4 = i3 + 1 + 2;
                    bArr2 = Arrays.copyOfRange(bArr, i4, i4 + 10);
                    Wg6.b(bArr2, "Arrays.copyOfRange(adver\u2026RER_SERIAL_NUMBER_LENGTH)");
                    break;
                }
                i2 = p + i3;
            } else {
                break;
            }
        }
        bArr2 = new byte[0];
        return new String(bArr2, Et7.a);
    }

    @DexIgnore
    public final Ci w() {
        return b == null ? Ci.DISABLED : Ci.d.a(b.getState());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0077  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.mapped.Q40 x(java.lang.String r16, java.lang.String r17) throws java.lang.IllegalArgumentException {
        /*
        // Method dump skipped, instructions count: 441
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.blesdk.adapter.BluetoothLeAdapter.x(java.lang.String, java.lang.String):com.mapped.Q40");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void y(com.fossil.Xk1 r21, com.fossil.blesdk.adapter.BluetoothLeAdapter.Bi r22) throws java.lang.IllegalStateException {
        /*
        // Method dump skipped, instructions count: 421
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.blesdk.adapter.BluetoothLeAdapter.y(com.fossil.Xk1, com.fossil.blesdk.adapter.BluetoothLeAdapter$Bi):void");
    }

    @DexIgnore
    public final void z(Bi bi) {
        D90.i.d(new A90(Ey1.a(Ei.d), V80.h, "", "", "", true, null, null, null, G80.k(new JSONObject(), Jd0.F1, Hy1.k(bi.hashCode(), null, 1, null)), 448));
        g(bi);
    }
}
