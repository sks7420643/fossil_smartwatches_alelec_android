package com.fossil.blesdk.device;

import com.fossil.Bl1;
import com.fossil.E60;
import com.fossil.Ec0;
import com.fossil.El7;
import com.fossil.Hy;
import com.fossil.Ko7;
import com.fossil.Lw1;
import com.fossil.Nc0;
import com.fossil.O;
import com.fossil.Px;
import com.fossil.Ry1;
import com.fossil.Yn7;
import com.fossil.Yy;
import com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.U40;
import com.mapped.Xe6;
import com.mapped.Yb0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.fossil.blesdk.device.DeviceImplementation$previewTheme$4", f = "DeviceImplementation.kt", l = {1653}, m = "invokeSuspend")
public final class DeviceImplementation$previewTheme$Anon4 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public Il6 b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public int d;
    @DexIgnore
    public /* final */ /* synthetic */ E60 e;
    @DexIgnore
    public /* final */ /* synthetic */ ThemeEditor f;
    @DexIgnore
    public /* final */ /* synthetic */ Yb0 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$previewTheme$Anon4(E60 e60, ThemeEditor themeEditor, Yb0 yb0, Xe6 xe6) {
        super(2, xe6);
        this.e = e60;
        this.f = themeEditor;
        this.g = yb0;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        DeviceImplementation$previewTheme$Anon4 deviceImplementation$previewTheme$Anon4 = new DeviceImplementation$previewTheme$Anon4(this.e, this.f, this.g, xe6);
        deviceImplementation$previewTheme$Anon4.b = (Il6) obj;
        throw null;
        //return deviceImplementation$previewTheme$Anon4;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((DeviceImplementation$previewTheme$Anon4) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object a2;
        Object d2 = Yn7.d();
        int i = this.d;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.b;
            Nc0 nc0 = Nc0.a;
            Ec0 g2 = this.f.g();
            Ry1 uiPackageOSVersion = this.e.u.getUiPackageOSVersion();
            this.c = il6;
            this.d = 1;
            a2 = nc0.a(g2, uiPackageOSVersion, true, this);
            if (a2 == d2) {
                return d2;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.c;
            El7.b(obj);
            a2 = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        O o = (O) a2;
        V v = o.a;
        if (v != null) {
            Lw1 a3 = this.f.a(v);
            if (a3 != null) {
                this.e.N(a3).x(new Px(this)).v(new Hy(this)).w(new Yy(this));
            } else {
                this.g.n(new Bl1(U40.UNKNOWN_ERROR, null, 2));
            }
        } else {
            Yb0 yb0 = this.g;
            Bl1 bl1 = o.b;
            if (bl1 == null) {
                bl1 = new Bl1(U40.UNKNOWN_ERROR, null, 2);
            }
            yb0.n(bl1);
        }
        return Cd6.a;
    }
}
