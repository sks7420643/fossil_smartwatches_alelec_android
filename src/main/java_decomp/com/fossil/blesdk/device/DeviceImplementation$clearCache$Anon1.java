package com.fossil.blesdk.device;

import com.fossil.E60;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.U;
import com.fossil.Yn7;
import com.fossil.fitness.AlgorithmManager;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Xe6;
import com.mapped.Zb0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.fossil.blesdk.device.DeviceImplementation$clearCache$1", f = "DeviceImplementation.kt", l = {}, m = "invokeSuspend")
public final class DeviceImplementation$clearCache$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public Il6 b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ /* synthetic */ E60 d;
    @DexIgnore
    public /* final */ /* synthetic */ Zb0 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceImplementation$clearCache$Anon1(E60 e60, Zb0 zb0, Xe6 xe6) {
        super(2, xe6);
        this.d = e60;
        this.e = zb0;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        DeviceImplementation$clearCache$Anon1 deviceImplementation$clearCache$Anon1 = new DeviceImplementation$clearCache$Anon1(this.d, this.e, xe6);
        deviceImplementation$clearCache$Anon1.b = (Il6) obj;
        throw null;
        //return deviceImplementation$clearCache$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        DeviceImplementation$clearCache$Anon1 deviceImplementation$clearCache$Anon1 = new DeviceImplementation$clearCache$Anon1(this.d, this.e, xe6);
        deviceImplementation$clearCache$Anon1.b = il6;
        return deviceImplementation$clearCache$Anon1.invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Yn7.d();
        if (this.c == 0) {
            El7.b(obj);
            U.a.a(this.d.d.x);
            AlgorithmManager.defaultManager().clearCache();
            this.e.o(Cd6.a);
            return Cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
