package com.fossil.blesdk.device.logic.phase;

import com.fossil.Ao7;
import com.fossil.Bg;
import com.fossil.Ec0;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Lw1;
import com.fossil.Nc0;
import com.fossil.O;
import com.fossil.Ry1;
import com.fossil.Yn7;
import com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.fossil.blesdk.device.logic.phase.InstallThemePackagePhase$onStart$1", f = "InstallThemePackagePhase.kt", l = {33}, m = "invokeSuspend")
public final class InstallThemePackagePhase$onStart$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public Il6 b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public int d;
    @DexIgnore
    public /* final */ /* synthetic */ Bg e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public InstallThemePackagePhase$onStart$Anon1(Bg bg, Xe6 xe6) {
        super(2, xe6);
        this.e = bg;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        InstallThemePackagePhase$onStart$Anon1 installThemePackagePhase$onStart$Anon1 = new InstallThemePackagePhase$onStart$Anon1(this.e, xe6);
        installThemePackagePhase$onStart$Anon1.b = (Il6) obj;
        throw null;
        //return installThemePackagePhase$onStart$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        InstallThemePackagePhase$onStart$Anon1 installThemePackagePhase$onStart$Anon1 = new InstallThemePackagePhase$onStart$Anon1(this.e, xe6);
        installThemePackagePhase$onStart$Anon1.b = il6;
        return installThemePackagePhase$onStart$Anon1.invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object a2;
        ThemeEditor edit;
        Lw1 a3;
        Ry1 packageVersion;
        Integer e2;
        Object d2 = Yn7.d();
        int i = this.d;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.b;
            Nc0 nc0 = Nc0.a;
            Ec0 themeClassifier = this.e.C.getThemeClassifier();
            Ry1 uiPackageOSVersion = this.e.x.a().getUiPackageOSVersion();
            this.c = il6;
            this.d = 1;
            a2 = nc0.a(themeClassifier, uiPackageOSVersion, false, this);
            if (a2 == d2) {
                return d2;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.c;
            El7.b(obj);
            a2 = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        V v = ((O) a2).a;
        if (v != null) {
            V v2 = !(v instanceof Lw1) ? null : v;
            if (!(((v2 == null || (packageVersion = v2.getPackageVersion()) == null || (e2 = Ao7.e(packageVersion.getMajor())) == null) ? 0 : e2.intValue()) <= this.e.C.getPackageVersion().getMajor() || (edit = this.e.C.edit()) == null || (a3 = edit.a(v)) == null)) {
                this.e.C = a3;
            }
        }
        Bg.G(this.e);
        return Cd6.a;
    }
}
