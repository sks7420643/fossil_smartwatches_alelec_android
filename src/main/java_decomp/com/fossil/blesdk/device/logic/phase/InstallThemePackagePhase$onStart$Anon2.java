package com.fossil.blesdk.device.logic.phase;

import com.fossil.Ax1;
import com.fossil.Bg;
import com.fossil.Ec0;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Lw1;
import com.fossil.Nc0;
import com.fossil.Nr;
import com.fossil.O;
import com.fossil.Ry1;
import com.fossil.Yn7;
import com.fossil.Zq;
import com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.fossil.blesdk.device.logic.phase.InstallThemePackagePhase$onStart$2", f = "InstallThemePackagePhase.kt", l = {56}, m = "invokeSuspend")
public final class InstallThemePackagePhase$onStart$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public Il6 b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public int d;
    @DexIgnore
    public /* final */ /* synthetic */ Bg e;
    @DexIgnore
    public /* final */ /* synthetic */ ThemeEditor f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public InstallThemePackagePhase$onStart$Anon2(Bg bg, ThemeEditor themeEditor, Xe6 xe6) {
        super(2, xe6);
        this.e = bg;
        this.f = themeEditor;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        InstallThemePackagePhase$onStart$Anon2 installThemePackagePhase$onStart$Anon2 = new InstallThemePackagePhase$onStart$Anon2(this.e, this.f, xe6);
        installThemePackagePhase$onStart$Anon2.b = (Il6) obj;
        throw null;
        //return installThemePackagePhase$onStart$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        InstallThemePackagePhase$onStart$Anon2 installThemePackagePhase$onStart$Anon2 = new InstallThemePackagePhase$onStart$Anon2(this.e, this.f, xe6);
        installThemePackagePhase$onStart$Anon2.b = il6;
        return installThemePackagePhase$onStart$Anon2.invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object a2;
        Object d2 = Yn7.d();
        int i = this.d;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.b;
            Nc0 nc0 = Nc0.a;
            Ec0 themeClassifier = this.e.C.getThemeClassifier();
            Ry1 uiPackageOSVersion = this.e.x.a().getUiPackageOSVersion();
            this.c = il6;
            this.d = 1;
            a2 = nc0.a(themeClassifier, uiPackageOSVersion, true, this);
            if (a2 == d2) {
                return d2;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.c;
            El7.b(obj);
            a2 = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        O o = (O) a2;
        V v = o.a;
        if (v != null) {
            Lw1 a3 = this.f.a(v);
            if (a3 != null) {
                Bg bg = this.e;
                bg.C = a3;
                Bg.G(bg);
            } else {
                Bg bg2 = this.e;
                bg2.l(Nr.a(bg2.v, null, Zq.i, null, null, 13));
            }
        } else {
            Bg bg3 = this.e;
            Nr nr = bg3.v;
            Zq zq = Zq.E;
            E e2 = o.b;
            if (!(e2 instanceof Ax1)) {
                e2 = null;
            }
            bg3.l(Nr.a(nr, null, zq, null, (Ax1) e2, 5));
        }
        return Cd6.a;
    }
}
