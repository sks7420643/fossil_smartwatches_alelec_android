package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dt extends Bt {
    @DexIgnore
    public byte[] K; // = new byte[0];
    @DexIgnore
    public /* final */ byte[] L;

    @DexIgnore
    public Dt(K5 k5, byte[] bArr) {
        super(k5, Ut.h, Hs.J, 0, 8);
        this.L = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(super.A(), Jd0.s2, Dy1.e(this.K, null, 1, null));
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 32) {
            byte[] k = Dm7.k(bArr, 0, 32);
            this.K = k;
            G80.k(jSONObject, Jd0.s2, Dy1.e(k, null, 1, null));
            this.v = Mw.a(this.v, null, null, Lw.b, null, null, 27);
        } else {
            this.v = Mw.a(this.v, null, null, Lw.k, null, null, 27);
        }
        this.E = true;
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(this.L.length).order(ByteOrder.LITTLE_ENDIAN).put(this.L).array();
        Wg6.b(array, "ByteBuffer.allocate(phon\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(super.z(), Jd0.r2, Dy1.e(this.L, null, 1, null));
    }
}
