package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.X90;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iq1 extends X90 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Iq1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Iq1 createFromParcel(Parcel parcel) {
            return new Iq1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Iq1[] newArray(int i) {
            return new Iq1[i];
        }
    }

    @DexIgnore
    public Iq1(byte b, int i, String str) {
        super(E90.COMMUTE_TIME_WATCH_APP, b, i);
        this.e = str;
    }

    @DexIgnore
    public /* synthetic */ Iq1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.e = readString;
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Iq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(Wg6.a(this.e, ((Iq1) obj).e) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.CommuteTimeWatchAppRequest");
    }

    @DexIgnore
    public final String getDestination() {
        return this.e;
    }

    @DexIgnore
    @Override // com.mapped.X90, com.fossil.Mp1
    public int hashCode() {
        return (super.hashCode() * 31) + this.e.hashCode();
    }
}
