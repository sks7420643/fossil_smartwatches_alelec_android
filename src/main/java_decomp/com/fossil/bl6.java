package com.fossil;

import android.os.Bundle;
import com.mapped.Fd;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Bl6 extends Fq4 {
    @DexIgnore
    public abstract FossilDeviceSerialPatternUtil.DEVICE n();

    @DexIgnore
    public abstract Ai5 o();

    @DexIgnore
    public abstract Fd p();

    @DexIgnore
    public abstract void q(Date date);

    @DexIgnore
    public abstract void r();

    @DexIgnore
    public abstract void s(Bundle bundle);

    @DexIgnore
    public abstract void t(Date date);

    @DexIgnore
    public abstract void u();

    @DexIgnore
    public abstract void v();
}
