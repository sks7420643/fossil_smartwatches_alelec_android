package com.fossil;

import android.os.Bundle;
import com.fossil.M62;
import java.util.Collections;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U82 implements D92 {
    @DexIgnore
    public /* final */ C92 a;

    @DexIgnore
    public U82(C92 c92) {
        this.a = c92;
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final boolean a() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void b() {
        this.a.r();
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void d(int i) {
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void e(Bundle bundle) {
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void f() {
        for (M62.Fi fi : this.a.g.values()) {
            fi.a();
        }
        this.a.t.q = Collections.emptySet();
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final void i(Z52 z52, M62<?> m62, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final <A extends M62.Bi, T extends I72<? extends Z62, A>> T j(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    @DexIgnore
    @Override // com.fossil.D92
    public final <A extends M62.Bi, R extends Z62, T extends I72<R, A>> T k(T t) {
        this.a.t.i.add(t);
        return t;
    }
}
