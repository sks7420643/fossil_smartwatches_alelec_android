package com.fossil;

import android.graphics.Point;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ub3 {
    @DexIgnore
    public /* final */ Cc3 a;

    @DexIgnore
    public Ub3(Cc3 cc3) {
        this.a = cc3;
    }

    @DexIgnore
    public final LatLng a(Point point) {
        try {
            return this.a.m2(Tg2.n(point));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final We3 b() {
        try {
            return this.a.H0();
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public final Point c(LatLng latLng) {
        try {
            return (Point) Tg2.i(this.a.k0(latLng));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }
}
