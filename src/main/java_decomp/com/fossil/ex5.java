package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.AppWrapper;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ex5 extends RecyclerView.g<Ci> implements Filterable {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ Ai f; // = new Ai(null);
    @DexIgnore
    public List<AppWrapper> b; // = new ArrayList();
    @DexIgnore
    public Bi c;
    @DexIgnore
    public List<AppWrapper> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Ex5.e;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(AppWrapper appWrapper, boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Nd5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Ex5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ci b;

            @DexIgnore
            public Aii(Ci ci) {
                this.b = ci;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List list = this.b.b.d;
                    if (list == null) {
                        Wg6.i();
                        throw null;
                    } else if (((AppWrapper) list.get(adapterPosition)).getUri() != null) {
                        List list2 = this.b.b.d;
                        if (list2 != null) {
                            InstalledApp installedApp = ((AppWrapper) list2.get(adapterPosition)).getInstalledApp();
                            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                            if (isSelected != null) {
                                boolean booleanValue = isSelected.booleanValue();
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String a2 = Ex5.f.a();
                                local.d(a2, "isSelected = " + booleanValue);
                                Bi bi = this.b.b.c;
                                if (bi != null) {
                                    List list3 = this.b.b.d;
                                    if (list3 != null) {
                                        bi.a((AppWrapper) list3.get(adapterPosition), !booleanValue);
                                    } else {
                                        Wg6.i();
                                        throw null;
                                    }
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Ex5 ex5, Nd5 nd5) {
            super(nd5.n());
            Wg6.c(nd5, "binding");
            this.b = ex5;
            this.a = nd5;
            String d = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
            String d2 = ThemeManager.l.a().d("nonBrandSeparatorLine");
            if (d != null) {
                this.a.q.setBackgroundColor(Color.parseColor(d));
            }
            if (d2 != null) {
                this.a.t.setBackgroundColor(Color.parseColor(d2));
            }
            this.a.u.setOnClickListener(new Aii(this));
        }

        @DexIgnore
        public final void a(AppWrapper appWrapper) {
            Wg6.c(appWrapper, "appWrapper");
            ImageView imageView = this.a.s;
            Wg6.b(imageView, "binding.ivAppIcon");
            Wj5 a2 = Tj5.a(imageView.getContext());
            InstalledApp installedApp = appWrapper.getInstalledApp();
            if (installedApp != null) {
                a2.I(new Qj5(installedApp)).m1(appWrapper.getIconResourceId()).Y0().F0(this.a.s);
                FlexibleTextView flexibleTextView = this.a.r;
                Wg6.b(flexibleTextView, "binding.ftvAppName");
                InstalledApp installedApp2 = appWrapper.getInstalledApp();
                flexibleTextView.setText(installedApp2 != null ? installedApp2.getTitle() : null);
                if (appWrapper.getUri() != null) {
                    FlexibleTextView flexibleTextView2 = this.a.r;
                    Wg6.b(flexibleTextView2, "binding.ftvAppName");
                    flexibleTextView2.setTextColor(flexibleTextView2.getTextColors().withAlpha(255));
                    FlexibleSwitchCompat flexibleSwitchCompat = this.a.u;
                    Wg6.b(flexibleSwitchCompat, "binding.swEnabled");
                    flexibleSwitchCompat.setEnabled(true);
                    FlexibleSwitchCompat flexibleSwitchCompat2 = this.a.u;
                    Wg6.b(flexibleSwitchCompat2, "binding.swEnabled");
                    Drawable background = flexibleSwitchCompat2.getBackground();
                    Wg6.b(background, "binding.swEnabled.background");
                    background.setAlpha(255);
                } else {
                    FlexibleTextView flexibleTextView3 = this.a.r;
                    Wg6.b(flexibleTextView3, "binding.ftvAppName");
                    flexibleTextView3.setTextColor(flexibleTextView3.getTextColors().withAlpha(100));
                    FlexibleSwitchCompat flexibleSwitchCompat3 = this.a.u;
                    Wg6.b(flexibleSwitchCompat3, "binding.swEnabled");
                    flexibleSwitchCompat3.setEnabled(false);
                    FlexibleSwitchCompat flexibleSwitchCompat4 = this.a.u;
                    Wg6.b(flexibleSwitchCompat4, "binding.swEnabled");
                    Drawable background2 = flexibleSwitchCompat4.getBackground();
                    Wg6.b(background2, "binding.swEnabled.background");
                    background2.setAlpha(100);
                }
                FlexibleSwitchCompat flexibleSwitchCompat5 = this.a.u;
                Wg6.b(flexibleSwitchCompat5, "binding.swEnabled");
                InstalledApp installedApp3 = appWrapper.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                if (isSelected != null) {
                    flexibleSwitchCompat5.setChecked(isSelected.booleanValue());
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Filter {
        @DexIgnore
        public /* final */ /* synthetic */ Ex5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di(Ex5 ex5) {
            this.a = ex5;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            String str;
            String title;
            Wg6.c(charSequence, "constraint");
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (TextUtils.isEmpty(charSequence)) {
                filterResults.values = this.a.b;
            } else {
                ArrayList arrayList = new ArrayList();
                String obj = charSequence.toString();
                if (obj != null) {
                    String lowerCase = obj.toLowerCase();
                    Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    for (AppWrapper appWrapper : this.a.b) {
                        InstalledApp installedApp = appWrapper.getInstalledApp();
                        if (installedApp == null || (title = installedApp.getTitle()) == null) {
                            str = null;
                        } else if (title != null) {
                            str = title.toLowerCase();
                            Wg6.b(str, "(this as java.lang.String).toLowerCase()");
                        } else {
                            throw new Rc6("null cannot be cast to non-null type java.lang.String");
                        }
                        if (str != null && Wt7.v(str, lowerCase, false, 2, null)) {
                            arrayList.add(appWrapper);
                        }
                    }
                    filterResults.values = arrayList;
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            Wg6.c(charSequence, "charSequence");
            Wg6.c(filterResults, "results");
            this.a.d = (List) filterResults.values;
            this.a.notifyDataSetChanged();
        }
    }

    /*
    static {
        String simpleName = Ex5.class.getSimpleName();
        Wg6.b(simpleName, "NotificationAppsAdapter::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public Ex5() {
        setHasStableIds(true);
    }

    @DexIgnore
    public Filter getFilter() {
        return new Di(this);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<AppWrapper> list = this.d;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    public void l(Ci ci, int i) {
        Wg6.c(ci, "holder");
        List<AppWrapper> list = this.d;
        if (list != null) {
            ci.a(list.get(i));
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Ci m(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Nd5 z = Nd5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemAppNotificationBindi\u2026.context), parent, false)");
        return new Ci(this, z);
    }

    @DexIgnore
    public final void n(List<AppWrapper> list) {
        Wg6.c(list, "listAppWrapper");
        this.b.clear();
        this.b.addAll(list);
        this.d = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void o(Bi bi) {
        Wg6.c(bi, "listener");
        this.c = bi;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ci ci, int i) {
        l(ci, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ci onCreateViewHolder(ViewGroup viewGroup, int i) {
        return m(viewGroup, i);
    }
}
