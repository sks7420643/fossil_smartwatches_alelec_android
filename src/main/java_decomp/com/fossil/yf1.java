package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yf1 implements Id1<Bitmap>, Ed1 {
    @DexIgnore
    public /* final */ Bitmap b;
    @DexIgnore
    public /* final */ Rd1 c;

    @DexIgnore
    public Yf1(Bitmap bitmap, Rd1 rd1) {
        Ik1.e(bitmap, "Bitmap must not be null");
        this.b = bitmap;
        Ik1.e(rd1, "BitmapPool must not be null");
        this.c = rd1;
    }

    @DexIgnore
    public static Yf1 f(Bitmap bitmap, Rd1 rd1) {
        if (bitmap == null) {
            return null;
        }
        return new Yf1(bitmap, rd1);
    }

    @DexIgnore
    @Override // com.fossil.Ed1
    public void a() {
        this.b.prepareToDraw();
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public void b() {
        this.c.b(this.b);
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public int c() {
        return Jk1.h(this.b);
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public Class<Bitmap> d() {
        return Bitmap.class;
    }

    @DexIgnore
    public Bitmap e() {
        return this.b;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Id1
    public /* bridge */ /* synthetic */ Bitmap get() {
        return e();
    }
}
