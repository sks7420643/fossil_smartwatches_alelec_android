package com.fossil;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.mapped.Qg6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sq6 extends BaseFragment implements Rq6 {
    @DexIgnore
    public static /* final */ Ai A; // = new Ai(null);
    @DexIgnore
    public static /* final */ Pattern z;
    @DexIgnore
    public Qq6 g;
    @DexIgnore
    public TextInputEditText h;
    @DexIgnore
    public TextInputEditText i;
    @DexIgnore
    public TextInputEditText j;
    @DexIgnore
    public TextInputLayout k;
    @DexIgnore
    public TextInputLayout l;
    @DexIgnore
    public TextInputLayout m;
    @DexIgnore
    public ProgressButton s;
    @DexIgnore
    public FlexibleTextView t;
    @DexIgnore
    public FlexibleTextView u;
    @DexIgnore
    public RTLImageView v;
    @DexIgnore
    public String w;
    @DexIgnore
    public boolean x; // = true;
    @DexIgnore
    public HashMap y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Sq6 a() {
            return new Sq6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Sq6 b;

        @DexIgnore
        public Bi(Sq6 sq6) {
            this.b = sq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Sq6.M6(this.b).clearFocus();
            Sq6.L6(this.b).clearFocus();
            Sq6.K6(this.b).clearFocus();
            Wg6.b(view, "it");
            view.setFocusable(true);
            if (this.b.V6()) {
                Sq6.N6(this.b).n(Sq6.L6(this.b).getEditableText().toString(), Sq6.K6(this.b).getEditableText().toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Sq6 b;

        @DexIgnore
        public Ci(Sq6 sq6) {
            this.b = sq6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.b.getActivity() != null) {
                FragmentActivity requireActivity = this.b.requireActivity();
                Wg6.b(requireActivity, "requireActivity()");
                if (!requireActivity.isFinishing()) {
                    FragmentActivity requireActivity2 = this.b.requireActivity();
                    Wg6.b(requireActivity2, "requireActivity()");
                    if (!requireActivity2.isDestroyed()) {
                        this.b.requireActivity().finish();
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Sq6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di(Sq6 sq6) {
            this.b = sq6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            Wg6.c(editable, "s");
            Sq6.O6(this.b).setEnabled(this.b.V6());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Wg6.c(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Wg6.c(charSequence, "s");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Sq6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei(Sq6 sq6) {
            this.b = sq6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            Wg6.c(editable, "s");
            Sq6.O6(this.b).setEnabled(this.b.V6());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Wg6.c(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Wg6.c(charSequence, "s");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Sq6 b;

        @DexIgnore
        public Fi(Sq6 sq6) {
            this.b = sq6;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            boolean z2 = false;
            if (z) {
                Sq6.P6(this.b).setVisibility(0);
                Sq6.Q6(this.b).setVisibility(0);
                return;
            }
            Editable editableText = Sq6.K6(this.b).getEditableText();
            Wg6.b(editableText, "mEdtNew.editableText");
            if (editableText.length() == 0) {
                z2 = true;
            }
            if (z2 || this.b.x) {
                Sq6.P6(this.b).setVisibility(8);
                Sq6.Q6(this.b).setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Sq6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Gi(Sq6 sq6) {
            this.b = sq6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            Wg6.c(editable, "s");
            Sq6.O6(this.b).setEnabled(this.b.V6());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Wg6.c(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Wg6.c(charSequence, "s");
        }
    }

    /*
    static {
        Wg6.b(Sq6.class.getSimpleName(), "ProfileChangePasswordFra\u2026nt::class.java.simpleName");
        Pattern compile = Pattern.compile("((?=.*\\d)(?=.*[a-zA-Z]).+)");
        if (compile != null) {
            z = compile;
        } else {
            Wg6.i();
            throw null;
        }
    }
    */

    @DexIgnore
    public static final /* synthetic */ TextInputEditText K6(Sq6 sq6) {
        TextInputEditText textInputEditText = sq6.i;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        Wg6.n("mEdtNew");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ TextInputEditText L6(Sq6 sq6) {
        TextInputEditText textInputEditText = sq6.h;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        Wg6.n("mEdtOld");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ TextInputEditText M6(Sq6 sq6) {
        TextInputEditText textInputEditText = sq6.j;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        Wg6.n("mEdtRepeat");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Qq6 N6(Sq6 sq6) {
        Qq6 qq6 = sq6.g;
        if (qq6 != null) {
            return qq6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ProgressButton O6(Sq6 sq6) {
        ProgressButton progressButton = sq6.s;
        if (progressButton != null) {
            return progressButton;
        }
        Wg6.n("mSaveButton");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView P6(Sq6 sq6) {
        FlexibleTextView flexibleTextView = sq6.t;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        Wg6.n("mTvCheckChar");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView Q6(Sq6 sq6) {
        FlexibleTextView flexibleTextView = sq6.u;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        Wg6.n("mTvCheckCombine");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        if (getActivity() != null) {
            FragmentActivity requireActivity = requireActivity();
            Wg6.b(requireActivity, "requireActivity()");
            if (!requireActivity.isFinishing()) {
                FragmentActivity requireActivity2 = requireActivity();
                Wg6.b(requireActivity2, "requireActivity()");
                if (!requireActivity2.isDestroyed()) {
                    requireActivity().finish();
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Rq6
    public void I0(int i2, String str) {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Qq6 qq6) {
        W6(qq6);
    }

    @DexIgnore
    @Override // com.fossil.Rq6
    public void P2() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager requireFragmentManager = requireFragmentManager();
            Wg6.b(requireFragmentManager, "requireFragmentManager()");
            s37.W(requireFragmentManager);
        }
    }

    @DexIgnore
    public final void T6(boolean z2) {
        TextInputLayout textInputLayout = this.k;
        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(z2);
            if (z2) {
                TextInputLayout textInputLayout2 = this.k;
                if (textInputLayout2 != null) {
                    textInputLayout2.setError(Um5.c(PortfolioApp.get.instance(), 2131887086));
                } else {
                    Wg6.n("mTvOldError");
                    throw null;
                }
            } else {
                TextInputLayout textInputLayout3 = this.k;
                if (textInputLayout3 != null) {
                    textInputLayout3.setError("");
                } else {
                    Wg6.n("mTvOldError");
                    throw null;
                }
            }
        } else {
            Wg6.n("mTvOldError");
            throw null;
        }
    }

    @DexIgnore
    public final void U6(Ja5 ja5) {
        FlexibleTextInputEditText flexibleTextInputEditText = ja5.s;
        Wg6.b(flexibleTextInputEditText, "binding.etOldPass");
        this.h = flexibleTextInputEditText;
        FlexibleTextInputEditText flexibleTextInputEditText2 = ja5.r;
        Wg6.b(flexibleTextInputEditText2, "binding.etNewPass");
        this.i = flexibleTextInputEditText2;
        FlexibleTextInputEditText flexibleTextInputEditText3 = ja5.t;
        Wg6.b(flexibleTextInputEditText3, "binding.etRepeatPass");
        this.j = flexibleTextInputEditText3;
        FlexibleTextInputLayout flexibleTextInputLayout = ja5.v;
        Wg6.b(flexibleTextInputLayout, "binding.inputOldPass");
        this.k = flexibleTextInputLayout;
        FlexibleTextInputLayout flexibleTextInputLayout2 = ja5.u;
        Wg6.b(flexibleTextInputLayout2, "binding.inputNewPass");
        this.l = flexibleTextInputLayout2;
        FlexibleTextInputLayout flexibleTextInputLayout3 = ja5.w;
        Wg6.b(flexibleTextInputLayout3, "binding.inputRepeatPass");
        this.m = flexibleTextInputLayout3;
        ProgressButton progressButton = ja5.z;
        Wg6.b(progressButton, "binding.save");
        this.s = progressButton;
        FlexibleTextView flexibleTextView = ja5.A;
        Wg6.b(flexibleTextView, "binding.tvErrorCheckCharacter");
        this.t = flexibleTextView;
        FlexibleTextView flexibleTextView2 = ja5.B;
        Wg6.b(flexibleTextView2, "binding.tvErrorCheckCombine");
        this.u = flexibleTextView2;
        RTLImageView rTLImageView = ja5.x;
        Wg6.b(rTLImageView, "binding.ivBack");
        this.v = rTLImageView;
        ProgressButton progressButton2 = this.s;
        if (progressButton2 != null) {
            progressButton2.setOnClickListener(new Bi(this));
            RTLImageView rTLImageView2 = this.v;
            if (rTLImageView2 != null) {
                rTLImageView2.setOnClickListener(new Ci(this));
                TextInputEditText textInputEditText = this.h;
                if (textInputEditText != null) {
                    textInputEditText.addTextChangedListener(new Di(this));
                    TextInputEditText textInputEditText2 = this.i;
                    if (textInputEditText2 != null) {
                        textInputEditText2.addTextChangedListener(new Ei(this));
                        TextInputEditText textInputEditText3 = this.i;
                        if (textInputEditText3 != null) {
                            textInputEditText3.setOnFocusChangeListener(new Fi(this));
                            TextInputEditText textInputEditText4 = this.j;
                            if (textInputEditText4 != null) {
                                textInputEditText4.addTextChangedListener(new Gi(this));
                            } else {
                                Wg6.n("mEdtRepeat");
                                throw null;
                            }
                        } else {
                            Wg6.n("mEdtNew");
                            throw null;
                        }
                    } else {
                        Wg6.n("mEdtNew");
                        throw null;
                    }
                } else {
                    Wg6.n("mEdtOld");
                    throw null;
                }
            } else {
                Wg6.n("mBackButton");
                throw null;
            }
        } else {
            Wg6.n("mSaveButton");
            throw null;
        }
    }

    @DexIgnore
    public final boolean V6() {
        boolean z2;
        boolean z3 = true;
        TextInputEditText textInputEditText = this.h;
        if (textInputEditText != null) {
            String obj = textInputEditText.getEditableText().toString();
            TextInputEditText textInputEditText2 = this.i;
            if (textInputEditText2 != null) {
                String obj2 = textInputEditText2.getEditableText().toString();
                TextInputEditText textInputEditText3 = this.j;
                if (textInputEditText3 != null) {
                    String obj3 = textInputEditText3.getEditableText().toString();
                    if (obj2.length() >= 7) {
                        FlexibleTextView flexibleTextView = this.t;
                        if (flexibleTextView != null) {
                            flexibleTextView.setCompoundDrawablesWithIntrinsicBounds(W6.f(PortfolioApp.get.instance(), 2131231054), (Drawable) null, (Drawable) null, (Drawable) null);
                            z2 = true;
                        } else {
                            Wg6.n("mTvCheckChar");
                            throw null;
                        }
                    } else {
                        FlexibleTextView flexibleTextView2 = this.t;
                        if (flexibleTextView2 != null) {
                            flexibleTextView2.setCompoundDrawablesWithIntrinsicBounds(W6.f(PortfolioApp.get.instance(), 2131230967), (Drawable) null, (Drawable) null, (Drawable) null);
                            z2 = false;
                        } else {
                            Wg6.n("mTvCheckChar");
                            throw null;
                        }
                    }
                    if (z.matcher(obj2).matches()) {
                        FlexibleTextView flexibleTextView3 = this.u;
                        if (flexibleTextView3 != null) {
                            flexibleTextView3.setCompoundDrawablesWithIntrinsicBounds(W6.f(PortfolioApp.get.instance(), 2131231054), (Drawable) null, (Drawable) null, (Drawable) null);
                        } else {
                            Wg6.n("mTvCheckCombine");
                            throw null;
                        }
                    } else {
                        FlexibleTextView flexibleTextView4 = this.u;
                        if (flexibleTextView4 != null) {
                            flexibleTextView4.setCompoundDrawablesWithIntrinsicBounds(W6.f(PortfolioApp.get.instance(), 2131230967), (Drawable) null, (Drawable) null, (Drawable) null);
                            z2 = false;
                        } else {
                            Wg6.n("mTvCheckCombine");
                            throw null;
                        }
                    }
                    if (obj.length() == 0) {
                        TextInputLayout textInputLayout = this.k;
                        if (textInputLayout != null) {
                            textInputLayout.setErrorEnabled(false);
                            TextInputLayout textInputLayout2 = this.k;
                            if (textInputLayout2 != null) {
                                textInputLayout2.setError("");
                            } else {
                                Wg6.n("mTvOldError");
                                throw null;
                            }
                        } else {
                            Wg6.n("mTvOldError");
                            throw null;
                        }
                    } else {
                        if (Wg6.a(this.w, obj)) {
                            TextInputLayout textInputLayout3 = this.k;
                            if (textInputLayout3 != null) {
                                textInputLayout3.setErrorEnabled(true);
                                TextInputLayout textInputLayout4 = this.k;
                                if (textInputLayout4 != null) {
                                    textInputLayout4.setError(Um5.c(PortfolioApp.get.instance(), 2131887086));
                                } else {
                                    Wg6.n("mTvOldError");
                                    throw null;
                                }
                            } else {
                                Wg6.n("mTvOldError");
                                throw null;
                            }
                        } else {
                            TextInputLayout textInputLayout5 = this.k;
                            if (textInputLayout5 != null) {
                                textInputLayout5.setErrorEnabled(false);
                                TextInputLayout textInputLayout6 = this.k;
                                if (textInputLayout6 != null) {
                                    textInputLayout6.setError("");
                                } else {
                                    Wg6.n("mTvOldError");
                                    throw null;
                                }
                            } else {
                                Wg6.n("mTvOldError");
                                throw null;
                            }
                        }
                        if ((obj2.length() == 0) || !Wg6.a(obj2, obj)) {
                            TextInputLayout textInputLayout7 = this.l;
                            if (textInputLayout7 != null) {
                                textInputLayout7.setErrorEnabled(false);
                                TextInputLayout textInputLayout8 = this.l;
                                if (textInputLayout8 != null) {
                                    textInputLayout8.setError("");
                                } else {
                                    Wg6.n("mTvNewError");
                                    throw null;
                                }
                            } else {
                                Wg6.n("mTvNewError");
                                throw null;
                            }
                        } else {
                            TextInputLayout textInputLayout9 = this.l;
                            if (textInputLayout9 != null) {
                                textInputLayout9.setErrorEnabled(true);
                                TextInputLayout textInputLayout10 = this.l;
                                if (textInputLayout10 != null) {
                                    textInputLayout10.setError(Um5.c(PortfolioApp.get.instance(), 2131886992));
                                } else {
                                    Wg6.n("mTvNewError");
                                    throw null;
                                }
                            } else {
                                Wg6.n("mTvNewError");
                                throw null;
                            }
                        }
                    }
                    if (obj2.length() == 0) {
                        TextInputLayout textInputLayout11 = this.l;
                        if (textInputLayout11 != null) {
                            textInputLayout11.setErrorEnabled(false);
                            TextInputLayout textInputLayout12 = this.l;
                            if (textInputLayout12 != null) {
                                textInputLayout12.setError("");
                                TextInputLayout textInputLayout13 = this.m;
                                if (textInputLayout13 != null) {
                                    textInputLayout13.setErrorEnabled(false);
                                    TextInputLayout textInputLayout14 = this.m;
                                    if (textInputLayout14 != null) {
                                        textInputLayout14.setError("");
                                    } else {
                                        Wg6.n("mTvRepeatError");
                                        throw null;
                                    }
                                } else {
                                    Wg6.n("mTvRepeatError");
                                    throw null;
                                }
                            } else {
                                Wg6.n("mTvNewError");
                                throw null;
                            }
                        } else {
                            Wg6.n("mTvNewError");
                            throw null;
                        }
                    }
                    if ((obj3.length() == 0) || Wg6.a(obj3, obj2)) {
                        TextInputLayout textInputLayout15 = this.m;
                        if (textInputLayout15 != null) {
                            textInputLayout15.setErrorEnabled(false);
                            TextInputLayout textInputLayout16 = this.m;
                            if (textInputLayout16 != null) {
                                textInputLayout16.setError("");
                            } else {
                                Wg6.n("mTvRepeatError");
                                throw null;
                            }
                        } else {
                            Wg6.n("mTvRepeatError");
                            throw null;
                        }
                    } else {
                        if (!(obj2.length() == 0) && (!Wg6.a(obj3, obj2))) {
                            TextInputLayout textInputLayout17 = this.m;
                            if (textInputLayout17 != null) {
                                textInputLayout17.setErrorEnabled(true);
                                TextInputLayout textInputLayout18 = this.m;
                                if (textInputLayout18 != null) {
                                    textInputLayout18.setError(Um5.c(PortfolioApp.get.instance(), 2131887089));
                                } else {
                                    Wg6.n("mTvRepeatError");
                                    throw null;
                                }
                            } else {
                                Wg6.n("mTvRepeatError");
                                throw null;
                            }
                        }
                    }
                    this.x = z2;
                    if (z2) {
                        if (!(obj.length() == 0)) {
                            if (!(obj2.length() == 0) && (!Wg6.a(obj, obj2)) && Wg6.a(obj2, obj3) && (!Wg6.a(obj, this.w))) {
                                TextInputLayout textInputLayout19 = this.l;
                                if (textInputLayout19 != null) {
                                    textInputLayout19.setErrorEnabled(false);
                                    TextInputLayout textInputLayout20 = this.l;
                                    if (textInputLayout20 != null) {
                                        textInputLayout20.setError("");
                                        TextInputLayout textInputLayout21 = this.m;
                                        if (textInputLayout21 != null) {
                                            textInputLayout21.setErrorEnabled(false);
                                            TextInputLayout textInputLayout22 = this.m;
                                            if (textInputLayout22 != null) {
                                                textInputLayout22.setError("");
                                                return true;
                                            }
                                            Wg6.n("mTvRepeatError");
                                            throw null;
                                        }
                                        Wg6.n("mTvRepeatError");
                                        throw null;
                                    }
                                    Wg6.n("mTvNewError");
                                    throw null;
                                }
                                Wg6.n("mTvNewError");
                                throw null;
                            }
                        }
                    }
                    if (!this.x) {
                        if (obj2.length() != 0) {
                            z3 = false;
                        }
                        if (!z3) {
                            FlexibleTextView flexibleTextView5 = this.t;
                            if (flexibleTextView5 != null) {
                                flexibleTextView5.setVisibility(0);
                                FlexibleTextView flexibleTextView6 = this.u;
                                if (flexibleTextView6 != null) {
                                    flexibleTextView6.setVisibility(0);
                                } else {
                                    Wg6.n("mTvCheckCombine");
                                    throw null;
                                }
                            } else {
                                Wg6.n("mTvCheckChar");
                                throw null;
                            }
                        }
                    }
                    return false;
                }
                Wg6.n("mEdtRepeat");
                throw null;
            }
            Wg6.n("mEdtNew");
            throw null;
        }
        Wg6.n("mEdtOld");
        throw null;
    }

    @DexIgnore
    public void W6(Qq6 qq6) {
        Wg6.c(qq6, "presenter");
        I14.l(qq6);
        Wg6.b(qq6, "checkNotNull(presenter)");
        this.g = qq6;
    }

    @DexIgnore
    @Override // com.fossil.Rq6
    public void f4() {
        if (isActive()) {
            ProgressButton progressButton = this.s;
            if (progressButton != null) {
                progressButton.setEnabled(false);
                TextInputEditText textInputEditText = this.h;
                if (textInputEditText != null) {
                    this.w = textInputEditText.getEditableText().toString();
                    T6(true);
                    return;
                }
                Wg6.n("mEdtOld");
                throw null;
            }
            Wg6.n("mSaveButton");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Rq6
    public void k() {
        if (isActive()) {
            ProgressButton progressButton = this.s;
            if (progressButton != null) {
                progressButton.f();
            } else {
                Wg6.n("mSaveButton");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Rq6
    public void l6() {
        if (isActive()) {
            String c = Um5.c(PortfolioApp.get.instance(), 2131886986);
            Wg6.b(c, "LanguageHelper.getString\u2026urPasswordHasBeenChanged)");
            I6(c);
            requireActivity().finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.Rq6
    public void m() {
        if (isActive()) {
            ProgressButton progressButton = this.s;
            if (progressButton != null) {
                progressButton.g();
            } else {
                Wg6.n("mSaveButton");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Ja5 ja5 = (Ja5) Aq0.f(LayoutInflater.from(getContext()), 2131558609, null, false, A6());
        Wg6.b(ja5, "binding");
        U6(ja5);
        new G37(this, ja5);
        ProgressButton progressButton = this.s;
        if (progressButton != null) {
            progressButton.setEnabled(false);
            return ja5.n();
        }
        Wg6.n("mSaveButton");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Qq6 qq6 = this.g;
        if (qq6 != null) {
            qq6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Qq6 qq6 = this.g;
        if (qq6 != null) {
            qq6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
