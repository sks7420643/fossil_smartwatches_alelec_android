package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ef7 {
    @DexIgnore
    public int a;
    @DexIgnore
    public String b;

    @DexIgnore
    public void a(Bundle bundle) {
        this.a = bundle.getInt("_wxapi_baseresp_errcode");
        bundle.getString("_wxapi_baseresp_errstr");
        bundle.getString("_wxapi_baseresp_transaction");
        this.b = bundle.getString("_wxapi_baseresp_openId");
    }

    @DexIgnore
    public abstract int b();
}
