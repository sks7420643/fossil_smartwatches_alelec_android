package com.fossil;

import com.mapped.An4;
import com.mapped.U04;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.analytic.BCAnalytic;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppDataRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.FitnessHelper;
import com.portfolio.platform.manager.validation.DataValidationManager;
import com.portfolio.platform.receiver.SmsMmsReceiver;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.service.workout.WorkoutTetherGpsManager;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.usecase.GeneratePassphraseUseCase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qq4 implements MembersInjector<PortfolioApp> {
    @DexIgnore
    public static void A(PortfolioApp portfolioApp, WatchLocalizationRepository watchLocalizationRepository) {
        portfolioApp.z = watchLocalizationRepository;
    }

    @DexIgnore
    public static void B(PortfolioApp portfolioApp, WorkoutTetherGpsManager workoutTetherGpsManager) {
        portfolioApp.G = workoutTetherGpsManager;
    }

    @DexIgnore
    public static void C(PortfolioApp portfolioApp, WorkoutSettingRepository workoutSettingRepository) {
        portfolioApp.E = workoutSettingRepository;
    }

    @DexIgnore
    public static void D(PortfolioApp portfolioApp, An4 an4) {
        portfolioApp.d = an4;
    }

    @DexIgnore
    public static void E(PortfolioApp portfolioApp, WatchFaceRepository watchFaceRepository) {
        portfolioApp.C = watchFaceRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, BCAnalytic bCAnalytic) {
        portfolioApp.Z = bCAnalytic;
    }

    @DexIgnore
    public static void b(PortfolioApp portfolioApp, AlarmHelper alarmHelper) {
        portfolioApp.g = alarmHelper;
    }

    @DexIgnore
    public static void c(PortfolioApp portfolioApp, AnalyticsHelper analyticsHelper) {
        portfolioApp.s = analyticsHelper;
    }

    @DexIgnore
    public static void d(PortfolioApp portfolioApp, ApiServiceV2 apiServiceV2) {
        portfolioApp.j = apiServiceV2;
    }

    @DexIgnore
    public static void e(PortfolioApp portfolioApp, ApplicationEventListener applicationEventListener) {
        portfolioApp.t = applicationEventListener;
    }

    @DexIgnore
    public static void f(PortfolioApp portfolioApp, U04 u04) {
        portfolioApp.i = u04;
    }

    @DexIgnore
    public static void g(PortfolioApp portfolioApp, L37 l37) {
        portfolioApp.k = l37;
    }

    @DexIgnore
    public static void h(PortfolioApp portfolioApp, Ic7 ic7) {
        portfolioApp.y = ic7;
    }

    @DexIgnore
    public static void i(PortfolioApp portfolioApp, DataValidationManager dataValidationManager) {
        portfolioApp.A = dataValidationManager;
    }

    @DexIgnore
    public static void j(PortfolioApp portfolioApp, Q27 q27) {
        portfolioApp.X = q27;
    }

    @DexIgnore
    public static void k(PortfolioApp portfolioApp, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        portfolioApp.m = deleteLogoutUserUseCase;
    }

    @DexIgnore
    public static void l(PortfolioApp portfolioApp, DeviceRepository deviceRepository) {
        portfolioApp.u = deviceRepository;
    }

    @DexIgnore
    public static void m(PortfolioApp portfolioApp, DianaPresetRepository dianaPresetRepository) {
        portfolioApp.x = dianaPresetRepository;
    }

    @DexIgnore
    public static void n(PortfolioApp portfolioApp, FitnessHelper fitnessHelper) {
        portfolioApp.v = fitnessHelper;
    }

    @DexIgnore
    public static void o(PortfolioApp portfolioApp, GeneratePassphraseUseCase generatePassphraseUseCase) {
        portfolioApp.Y = generatePassphraseUseCase;
    }

    @DexIgnore
    public static void p(PortfolioApp portfolioApp, GuestApiService guestApiService) {
        portfolioApp.h = guestApiService;
    }

    @DexIgnore
    public static void q(PortfolioApp portfolioApp, SmsMmsReceiver smsMmsReceiver) {
        portfolioApp.V = smsMmsReceiver;
    }

    @DexIgnore
    public static void r(PortfolioApp portfolioApp, Aq5 aq5) {
        portfolioApp.U = aq5;
    }

    @DexIgnore
    public static void s(PortfolioApp portfolioApp, QuickResponseRepository quickResponseRepository) {
        portfolioApp.D = quickResponseRepository;
    }

    @DexIgnore
    public static void t(PortfolioApp portfolioApp, ShakeFeedbackService shakeFeedbackService) {
        portfolioApp.w = shakeFeedbackService;
    }

    @DexIgnore
    public static void u(PortfolioApp portfolioApp, SleepSummariesRepository sleepSummariesRepository) {
        portfolioApp.f = sleepSummariesRepository;
    }

    @DexIgnore
    public static void v(PortfolioApp portfolioApp, SummariesRepository summariesRepository) {
        portfolioApp.e = summariesRepository;
    }

    @DexIgnore
    public static void w(PortfolioApp portfolioApp, ThemeRepository themeRepository) {
        portfolioApp.W = themeRepository;
    }

    @DexIgnore
    public static void x(PortfolioApp portfolioApp, Uq4 uq4) {
        portfolioApp.l = uq4;
    }

    @DexIgnore
    public static void y(PortfolioApp portfolioApp, UserRepository userRepository) {
        portfolioApp.B = userRepository;
    }

    @DexIgnore
    public static void z(PortfolioApp portfolioApp, WatchAppDataRepository watchAppDataRepository) {
        portfolioApp.F = watchAppDataRepository;
    }
}
