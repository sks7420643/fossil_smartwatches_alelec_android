package com.fossil;

import com.facebook.internal.FileLruCache;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import com.mapped.Hg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pm7 extends Om7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Ts7<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable a;

        @DexIgnore
        public Ai(Iterable iterable) {
            this.a = iterable;
        }

        @DexIgnore
        @Override // com.fossil.Ts7
        public Iterator<T> iterator() {
            return this.a.iterator();
        }
    }

    @DexIgnore
    public static final <T> List<List<T>> A(Iterable<? extends T> iterable, int i) {
        Wg6.c(iterable, "$this$chunked");
        return m0(iterable, i, i, true);
    }

    @DexIgnore
    public static final <T> boolean B(Iterable<? extends T> iterable, T t) {
        Wg6.c(iterable, "$this$contains");
        return iterable instanceof Collection ? ((Collection) iterable).contains(t) : J(iterable, t) >= 0;
    }

    @DexIgnore
    public static final <T> List<T> C(Iterable<? extends T> iterable) {
        Wg6.c(iterable, "$this$distinct");
        return h0(k0(iterable));
    }

    @DexIgnore
    public static final <T> List<T> D(Iterable<? extends T> iterable, int i) {
        ArrayList arrayList;
        Wg6.c(iterable, "$this$drop");
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("Requested element count " + i + " is less than zero.").toString());
        } else if (i == 0) {
            return h0(iterable);
        } else {
            if (iterable instanceof Collection) {
                Collection collection = (Collection) iterable;
                int size = collection.size() - i;
                if (size <= 0) {
                    return Hm7.e();
                }
                if (size == 1) {
                    return Gm7.b(O(iterable));
                }
                ArrayList arrayList2 = new ArrayList(size);
                if (!(iterable instanceof List)) {
                    arrayList = arrayList2;
                } else if (iterable instanceof RandomAccess) {
                    int size2 = collection.size();
                    while (i < size2) {
                        arrayList2.add(((List) iterable).get(i));
                        i++;
                    }
                    return arrayList2;
                } else {
                    ListIterator listIterator = ((List) iterable).listIterator(i);
                    while (listIterator.hasNext()) {
                        arrayList2.add(listIterator.next());
                    }
                    return arrayList2;
                }
            } else {
                arrayList = new ArrayList();
            }
            int i2 = 0;
            for (Object obj : iterable) {
                if (i2 >= i) {
                    arrayList.add(obj);
                } else {
                    i2++;
                }
            }
            return Hm7.j(arrayList);
        }
    }

    @DexIgnore
    public static final <T> T E(Iterable<? extends T> iterable) {
        Wg6.c(iterable, "$this$first");
        if (iterable instanceof List) {
            return (T) F((List) iterable);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            return (T) it.next();
        }
        throw new NoSuchElementException("Collection is empty.");
    }

    @DexIgnore
    public static final <T> T F(List<? extends T> list) {
        Wg6.c(list, "$this$first");
        if (!list.isEmpty()) {
            return (T) list.get(0);
        }
        throw new NoSuchElementException("List is empty.");
    }

    @DexIgnore
    public static final <T> T G(Iterable<? extends T> iterable) {
        Wg6.c(iterable, "$this$firstOrNull");
        if (iterable instanceof List) {
            List list = (List) iterable;
            if (list.isEmpty()) {
                return null;
            }
            return (T) list.get(0);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            return (T) it.next();
        }
        return null;
    }

    @DexIgnore
    public static final <T> T H(List<? extends T> list) {
        Wg6.c(list, "$this$firstOrNull");
        if (list.isEmpty()) {
            return null;
        }
        return (T) list.get(0);
    }

    @DexIgnore
    public static final <T> T I(List<? extends T> list, int i) {
        Wg6.c(list, "$this$getOrNull");
        if (i < 0 || i > Hm7.g(list)) {
            return null;
        }
        return (T) list.get(i);
    }

    @DexIgnore
    public static final <T> int J(Iterable<? extends T> iterable, T t) {
        Wg6.c(iterable, "$this$indexOf");
        if (iterable instanceof List) {
            return ((List) iterable).indexOf(t);
        }
        int i = 0;
        for (Object obj : iterable) {
            if (i < 0) {
                Hm7.l();
                throw null;
            } else if (Wg6.a(t, obj)) {
                return i;
            } else {
                i++;
            }
        }
        return -1;
    }

    @DexIgnore
    public static final <T> int K(List<? extends T> list, T t) {
        Wg6.c(list, "$this$indexOf");
        return list.indexOf(t);
    }

    @DexIgnore
    public static final <T, A extends Appendable> A L(Iterable<? extends T> iterable, A a2, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Hg6<? super T, ? extends CharSequence> hg6) {
        Wg6.c(iterable, "$this$joinTo");
        Wg6.c(a2, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        Wg6.c(charSequence, "separator");
        Wg6.c(charSequence2, "prefix");
        Wg6.c(charSequence3, "postfix");
        Wg6.c(charSequence4, "truncated");
        a2.append(charSequence2);
        int i2 = 0;
        for (Object obj : iterable) {
            i2++;
            if (i2 > 1) {
                a2.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            Nt7.a(a2, obj, hg6);
        }
        if (i >= 0 && i2 > i) {
            a2.append(charSequence4);
        }
        a2.append(charSequence3);
        return a2;
    }

    @DexIgnore
    public static final <T> String M(Iterable<? extends T> iterable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Hg6<? super T, ? extends CharSequence> hg6) {
        Wg6.c(iterable, "$this$joinToString");
        Wg6.c(charSequence, "separator");
        Wg6.c(charSequence2, "prefix");
        Wg6.c(charSequence3, "postfix");
        Wg6.c(charSequence4, "truncated");
        StringBuilder sb = new StringBuilder();
        L(iterable, sb, charSequence, charSequence2, charSequence3, i, charSequence4, hg6);
        String sb2 = sb.toString();
        Wg6.b(sb2, "joinTo(StringBuilder(), \u2026ed, transform).toString()");
        return sb2;
    }

    @DexIgnore
    public static /* synthetic */ String N(Iterable iterable, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Hg6 hg6, int i2, Object obj) {
        String str = (i2 & 1) != 0 ? ", " : charSequence;
        CharSequence charSequence5 = "";
        String str2 = (i2 & 2) != 0 ? "" : charSequence2;
        if ((i2 & 4) == 0) {
            charSequence5 = charSequence3;
        }
        return M(iterable, str, str2, charSequence5, (i2 & 8) != 0 ? -1 : i, (i2 & 16) != 0 ? "..." : charSequence4, (i2 & 32) != 0 ? null : hg6);
    }

    @DexIgnore
    public static final <T> T O(Iterable<? extends T> iterable) {
        Wg6.c(iterable, "$this$last");
        if (iterable instanceof List) {
            return (T) P((List) iterable);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            T t = (T) it.next();
            while (it.hasNext()) {
                t = (T) it.next();
            }
            return t;
        }
        throw new NoSuchElementException("Collection is empty.");
    }

    @DexIgnore
    public static final <T> T P(List<? extends T> list) {
        Wg6.c(list, "$this$last");
        if (!list.isEmpty()) {
            return (T) list.get(Hm7.g(list));
        }
        throw new NoSuchElementException("List is empty.");
    }

    @DexIgnore
    public static final <T> T Q(Iterable<? extends T> iterable) {
        Wg6.c(iterable, "$this$lastOrNull");
        if (iterable instanceof List) {
            List list = (List) iterable;
            if (list.isEmpty()) {
                return null;
            }
            return (T) list.get(list.size() - 1);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (!it.hasNext()) {
            return null;
        }
        T t = (T) it.next();
        while (it.hasNext()) {
            t = (T) it.next();
        }
        return t;
    }

    @DexIgnore
    public static final <T> T R(List<? extends T> list) {
        Wg6.c(list, "$this$lastOrNull");
        if (list.isEmpty()) {
            return null;
        }
        return (T) list.get(list.size() - 1);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v6, types: [java.lang.Comparable, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T extends java.lang.Comparable<? super T>> T S(java.lang.Iterable<? extends T> r4) {
        /*
            java.lang.String r0 = "$this$max"
            com.mapped.Wg6.c(r4, r0)
            java.util.Iterator r2 = r4.iterator()
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x0011
            r1 = 0
        L_0x0010:
            return r1
        L_0x0011:
            java.lang.Object r0 = r2.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            r1 = r0
        L_0x0018:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0010
            java.lang.Object r0 = r2.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            int r3 = r1.compareTo(r0)
            if (r3 >= 0) goto L_0x0018
            r1 = r0
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Pm7.S(java.lang.Iterable):java.lang.Comparable");
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v6, types: [java.lang.Comparable, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T extends java.lang.Comparable<? super T>> T T(java.lang.Iterable<? extends T> r4) {
        /*
            java.lang.String r0 = "$this$min"
            com.mapped.Wg6.c(r4, r0)
            java.util.Iterator r2 = r4.iterator()
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x0011
            r1 = 0
        L_0x0010:
            return r1
        L_0x0011:
            java.lang.Object r0 = r2.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            r1 = r0
        L_0x0018:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0010
            java.lang.Object r0 = r2.next()
            java.lang.Comparable r0 = (java.lang.Comparable) r0
            int r3 = r1.compareTo(r0)
            if (r3 <= 0) goto L_0x0018
            r1 = r0
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Pm7.T(java.lang.Iterable):java.lang.Comparable");
    }

    @DexIgnore
    public static final <T> List<T> U(Iterable<? extends T> iterable, Iterable<? extends T> iterable2) {
        Wg6.c(iterable, "$this$minus");
        Wg6.c(iterable2, MessengerShareContentUtility.ELEMENTS);
        Collection n = Im7.n(iterable2, iterable);
        if (n.isEmpty()) {
            return h0(iterable);
        }
        ArrayList arrayList = new ArrayList();
        for (Object obj : iterable) {
            if (!n.contains(obj)) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final <T> List<T> V(Collection<? extends T> collection, Iterable<? extends T> iterable) {
        Wg6.c(collection, "$this$plus");
        Wg6.c(iterable, MessengerShareContentUtility.ELEMENTS);
        if (iterable instanceof Collection) {
            Collection collection2 = (Collection) iterable;
            ArrayList arrayList = new ArrayList(collection.size() + collection2.size());
            arrayList.addAll(collection);
            arrayList.addAll(collection2);
            return arrayList;
        }
        ArrayList arrayList2 = new ArrayList(collection);
        Mm7.s(arrayList2, iterable);
        return arrayList2;
    }

    @DexIgnore
    public static final <T> List<T> W(Collection<? extends T> collection, T t) {
        Wg6.c(collection, "$this$plus");
        ArrayList arrayList = new ArrayList(collection.size() + 1);
        arrayList.addAll(collection);
        arrayList.add(t);
        return arrayList;
    }

    @DexIgnore
    public static final <T> List<T> X(Iterable<? extends T> iterable) {
        Wg6.c(iterable, "$this$reversed");
        if ((iterable instanceof Collection) && ((Collection) iterable).size() <= 1) {
            return h0(iterable);
        }
        List<T> i0 = i0(iterable);
        Om7.y(i0);
        return i0;
    }

    @DexIgnore
    public static final <T> T Y(Iterable<? extends T> iterable) {
        Wg6.c(iterable, "$this$single");
        if (iterable instanceof List) {
            return (T) Z((List) iterable);
        }
        Iterator<? extends T> it = iterable.iterator();
        if (it.hasNext()) {
            T t = (T) it.next();
            if (!it.hasNext()) {
                return t;
            }
            throw new IllegalArgumentException("Collection has more than one element.");
        }
        throw new NoSuchElementException("Collection is empty.");
    }

    @DexIgnore
    public static final <T> T Z(List<? extends T> list) {
        Wg6.c(list, "$this$single");
        int size = list.size();
        if (size == 0) {
            throw new NoSuchElementException("List is empty.");
        } else if (size == 1) {
            return (T) list.get(0);
        } else {
            throw new IllegalArgumentException("List has more than one element.");
        }
    }

    @DexIgnore
    public static final <T extends Comparable<? super T>> List<T> a0(Iterable<? extends T> iterable) {
        Wg6.c(iterable, "$this$sorted");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (collection.size() <= 1) {
                return h0(iterable);
            }
            Object[] array = collection.toArray(new Comparable[0]);
            if (array == null) {
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            } else if (array != null) {
                Comparable[] comparableArr = (Comparable[]) array;
                if (comparableArr != null) {
                    Dm7.u(comparableArr);
                    return Dm7.d(comparableArr);
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<kotlin.Any?>");
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            List<T> i0 = i0(iterable);
            Lm7.q(i0);
            return i0;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: java.util.Collection */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> List<T> b0(Iterable<? extends T> iterable, Comparator<? super T> comparator) {
        Wg6.c(iterable, "$this$sortedWith");
        Wg6.c(comparator, "comparator");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            if (collection.size() <= 1) {
                return h0(iterable);
            }
            Object[] array = collection.toArray(new Object[0]);
            if (array == null) {
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            } else if (array != null) {
                Dm7.v(array, comparator);
                return Dm7.d(array);
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            List<T> i0 = i0(iterable);
            Lm7.r(i0, comparator);
            return i0;
        }
    }

    @DexIgnore
    public static final int c0(Iterable<Integer> iterable) {
        Wg6.c(iterable, "$this$sum");
        int i = 0;
        for (Integer num : iterable) {
            i = num.intValue() + i;
        }
        return i;
    }

    @DexIgnore
    public static final <T> List<T> d0(Iterable<? extends T> iterable, int i) {
        Wg6.c(iterable, "$this$take");
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("Requested element count " + i + " is less than zero.").toString());
        } else if (i == 0) {
            return Hm7.e();
        } else {
            if (iterable instanceof Collection) {
                if (i >= ((Collection) iterable).size()) {
                    return h0(iterable);
                }
                if (i == 1) {
                    return Gm7.b(E(iterable));
                }
            }
            ArrayList arrayList = new ArrayList(i);
            Iterator<? extends T> it = iterable.iterator();
            int i2 = 0;
            while (it.hasNext()) {
                arrayList.add(it.next());
                i2++;
                if (i2 == i) {
                    break;
                }
            }
            return Hm7.j(arrayList);
        }
    }

    @DexIgnore
    public static final <T, C extends Collection<? super T>> C e0(Iterable<? extends T> iterable, C c) {
        Wg6.c(iterable, "$this$toCollection");
        Wg6.c(c, ShareConstants.DESTINATION);
        Iterator<? extends T> it = iterable.iterator();
        while (it.hasNext()) {
            c.add(it.next());
        }
        return c;
    }

    @DexIgnore
    public static final <T> HashSet<T> f0(Iterable<? extends T> iterable) {
        Wg6.c(iterable, "$this$toHashSet");
        HashSet<T> hashSet = new HashSet<>(Ym7.b(Im7.m(iterable, 12)));
        e0(iterable, hashSet);
        return hashSet;
    }

    @DexIgnore
    public static final int[] g0(Collection<Integer> collection) {
        Wg6.c(collection, "$this$toIntArray");
        int[] iArr = new int[collection.size()];
        int i = 0;
        for (Integer num : collection) {
            iArr[i] = num.intValue();
            i++;
        }
        return iArr;
    }

    @DexIgnore
    public static final <T> List<T> h0(Iterable<? extends T> iterable) {
        Wg6.c(iterable, "$this$toList");
        if (!(iterable instanceof Collection)) {
            return Hm7.j(i0(iterable));
        }
        Collection collection = (Collection) iterable;
        int size = collection.size();
        if (size == 0) {
            return Hm7.e();
        }
        if (size != 1) {
            return j0(collection);
        }
        return Gm7.b(iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next());
    }

    @DexIgnore
    public static final <T> List<T> i0(Iterable<? extends T> iterable) {
        Wg6.c(iterable, "$this$toMutableList");
        if (iterable instanceof Collection) {
            return j0((Collection) iterable);
        }
        ArrayList arrayList = new ArrayList();
        e0(iterable, arrayList);
        return arrayList;
    }

    @DexIgnore
    public static final <T> List<T> j0(Collection<? extends T> collection) {
        Wg6.c(collection, "$this$toMutableList");
        return new ArrayList(collection);
    }

    @DexIgnore
    public static final <T> Set<T> k0(Iterable<? extends T> iterable) {
        Wg6.c(iterable, "$this$toMutableSet");
        if (iterable instanceof Collection) {
            return new LinkedHashSet((Collection) iterable);
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        e0(iterable, linkedHashSet);
        return linkedHashSet;
    }

    @DexIgnore
    public static final <T> Set<T> l0(Iterable<? extends T> iterable) {
        Wg6.c(iterable, "$this$toSet");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            int size = collection.size();
            if (size == 0) {
                return Dn7.b();
            }
            if (size != 1) {
                LinkedHashSet linkedHashSet = new LinkedHashSet(Ym7.b(collection.size()));
                e0(iterable, linkedHashSet);
                return linkedHashSet;
            }
            return Cn7.a(iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next());
        }
        LinkedHashSet linkedHashSet2 = new LinkedHashSet();
        e0(iterable, linkedHashSet2);
        return Dn7.d(linkedHashSet2);
    }

    @DexIgnore
    public static final <T> List<List<T>> m0(Iterable<? extends T> iterable, int i, int i2, boolean z) {
        Wg6.c(iterable, "$this$windowed");
        En7.a(i, i2);
        if (!(iterable instanceof RandomAccess) || !(iterable instanceof List)) {
            ArrayList arrayList = new ArrayList();
            Iterator b = En7.b(iterable.iterator(), i, i2, z, false);
            while (b.hasNext()) {
                arrayList.add((List) b.next());
            }
            return arrayList;
        }
        List list = (List) iterable;
        int size = list.size();
        ArrayList arrayList2 = new ArrayList((size % i2 == 0 ? 0 : 1) + (size / i2));
        int i3 = 0;
        while (i3 >= 0 && size > i3) {
            int g = Bs7.g(i, size - i3);
            if (g < i && !z) {
                break;
            }
            ArrayList arrayList3 = new ArrayList(g);
            for (int i4 = 0; i4 < g; i4++) {
                arrayList3.add(list.get(i4 + i3));
            }
            arrayList2.add(arrayList3);
            i3 += i2;
        }
        return arrayList2;
    }

    @DexIgnore
    public static final <T> Ts7<T> z(Iterable<? extends T> iterable) {
        Wg6.c(iterable, "$this$asSequence");
        return new Ai(iterable);
    }
}
