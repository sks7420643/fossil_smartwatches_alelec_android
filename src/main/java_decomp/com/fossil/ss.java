package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ss extends Rs {
    @DexIgnore
    public Ss(Hs hs, K5 k5) {
        super(hs, k5);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public final String p(Mw mw) {
        F7 f7 = mw.e.d.b;
        F7 f72 = F7.f;
        return f7 == f72 ? Ey1.a(f72) : Ey1.a(Lw.b);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public final void r(U5 u5) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        JSONObject k;
        this.v = Mw.a(this.v, null, null, Mw.g.a(u5.e).d, u5.e, null, 19);
        if (u5.e.d.b == F7.f) {
            A90 a90 = this.f;
            if (a90 != null) {
                a90.j = false;
            }
            A90 a902 = this.f;
            if (!(a902 == null || (jSONObject2 = a902.n) == null || (k = G80.k(jSONObject2, Jd0.k, Ey1.a(X6.f))) == null)) {
                G80.k(k, Jd0.P0, u5.e.toJSONObject());
            }
        } else {
            A90 a903 = this.f;
            if (a903 != null) {
                a903.j = true;
            }
            A90 a904 = this.f;
            if (!(a904 == null || (jSONObject = a904.n) == null)) {
                G80.k(jSONObject, Jd0.k, Ey1.a(Lw.b));
            }
        }
        C();
        m(this.v);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public final String u(Mw mw) {
        Lw lw = mw.d;
        Lw lw2 = Lw.s;
        if (lw == lw2) {
            return Ey1.a(lw2);
        }
        G7 g7 = mw.e.d;
        F7 f7 = g7.b;
        F7 f72 = F7.f;
        return f7 == f72 ? Ey1.a(f72) : (lw == Lw.q || lw == Lw.r) ? Ey1.a(Lw.r) : Ey1.a(X6.n.a(g7.c));
    }
}
