package com.fossil;

import com.mapped.Er4;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ws5 extends Er4 {
    @DexIgnore
    public boolean d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Ws5(String str, String str2, boolean z) {
        super(str, str2);
        Wg6.c(str, "tagName");
        Wg6.c(str2, "title");
        this.d = z;
    }

    @DexIgnore
    public final boolean f() {
        return this.d;
    }

    @DexIgnore
    public final void g(boolean z) {
        this.d = z;
    }
}
