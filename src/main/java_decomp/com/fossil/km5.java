package com.fossil;

import android.content.res.Resources;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class km5 {
    @DexIgnore
    public static byte[] d;
    @DexIgnore
    public static byte[] e;

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public ByteArrayInputStream f1932a; // = null;
    @DexIgnore
    public jm5 b; // = null;
    @DexIgnore
    public hm5 c; // = null;

    @DexIgnore
    public km5(byte[] bArr) {
        this.f1932a = new ByteArrayInputStream(bArr);
    }

    @DexIgnore
    public static boolean a(jm5 jm5) {
        if (jm5 == null) {
            return false;
        }
        int e2 = jm5.e(ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
        if (jm5.e(141) == 0) {
            return false;
        }
        switch (e2) {
            case 128:
                if (jm5.f(132) == null || jm5.b(137) == null || jm5.f(152) == null) {
                    return false;
                }
                break;
            case 129:
                if (jm5.e(146) == 0 || jm5.f(152) == null) {
                    return false;
                }
                break;
            case 130:
                if (jm5.f(131) == null || -1 == jm5.d(136) || jm5.f(138) == null || -1 == jm5.d(142) || jm5.f(152) == null) {
                    return false;
                }
                break;
            case 131:
                if (jm5.e(149) == 0 || jm5.f(152) == null) {
                    return false;
                }
                break;
            case 132:
                if (jm5.f(132) == null || -1 == jm5.d(133)) {
                    return false;
                }
                break;
            case 133:
                if (jm5.f(152) == null) {
                    return false;
                }
                break;
            case 134:
                if (-1 == jm5.d(133) || jm5.f(139) == null || jm5.e(149) == 0 || jm5.c(151) == null) {
                    return false;
                }
                break;
            case 135:
                if (jm5.b(137) == null || jm5.f(139) == null || jm5.e(155) == 0 || jm5.c(151) == null) {
                    return false;
                }
                break;
            case 136:
                if (-1 == jm5.d(133) || jm5.b(137) == null || jm5.f(139) == null || jm5.e(155) == 0 || jm5.c(151) == null) {
                    return false;
                }
                break;
            default:
                return false;
        }
        return true;
    }

    @DexIgnore
    public static int b(lm5 lm5) {
        byte[] d2;
        byte[] a2;
        if (d == null && e == null) {
            return 1;
        }
        if (e == null || (a2 = lm5.a()) == null || true != Arrays.equals(e, a2)) {
            return (d == null || (d2 = lm5.d()) == null || true != Arrays.equals(d, d2)) ? 1 : 0;
        }
        return 0;
    }

    @DexIgnore
    public static int c(ByteArrayInputStream byteArrayInputStream) {
        return byteArrayInputStream.read() & 255;
    }

    @DexIgnore
    public static byte[] d(ByteArrayInputStream byteArrayInputStream, int i) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int read = byteArrayInputStream.read();
        while (-1 != read && read != 0) {
            if (i == 2) {
                if (f(read)) {
                    byteArrayOutputStream.write(read);
                }
            } else if (e(read)) {
                byteArrayOutputStream.write(read);
            }
            read = byteArrayInputStream.read();
        }
        if (byteArrayOutputStream.size() > 0) {
            return byteArrayOutputStream.toByteArray();
        }
        return null;
    }

    @DexIgnore
    public static boolean e(int i) {
        if (i < 32 || i > 126) {
            return (i >= 128 && i <= 255) || i == 9 || i == 10 || i == 13;
        }
        return true;
    }

    @DexIgnore
    public static boolean f(int i) {
        if (!(i < 33 || i > 126 || i == 34 || i == 44 || i == 47 || i == 123 || i == 125 || i == 40 || i == 41)) {
            switch (i) {
                case 58:
                case 59:
                case 60:
                case 61:
                case 62:
                case 63:
                case 64:
                    break;
                default:
                    switch (i) {
                        case 91:
                        case 92:
                        case 93:
                            break;
                        default:
                            return true;
                    }
            }
        }
        return false;
    }

    @DexIgnore
    public static void g(String str) {
    }

    @DexIgnore
    public static byte[] i(ByteArrayInputStream byteArrayInputStream, HashMap<Integer, Object> hashMap) {
        byte[] t;
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read();
        byteArrayInputStream.reset();
        int i = read & 255;
        if (i >= 32) {
            return i <= 127 ? t(byteArrayInputStream, 0) : im5.f1643a[q(byteArrayInputStream)].getBytes();
        }
        int s = s(byteArrayInputStream);
        int available = byteArrayInputStream.available();
        byteArrayInputStream.mark(1);
        int read2 = byteArrayInputStream.read();
        byteArrayInputStream.reset();
        int i2 = read2 & 255;
        if (i2 >= 32 && i2 <= 127) {
            t = t(byteArrayInputStream, 0);
        } else if (i2 > 127) {
            int q = q(byteArrayInputStream);
            String[] strArr = im5.f1643a;
            if (q < strArr.length) {
                t = strArr[q].getBytes();
            } else {
                byteArrayInputStream.reset();
                t = t(byteArrayInputStream, 0);
            }
        } else {
            FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt content-type");
            return im5.f1643a[0].getBytes();
        }
        int available2 = s - (available - byteArrayInputStream.available());
        if (available2 > 0) {
            j(byteArrayInputStream, hashMap, Integer.valueOf(available2));
        }
        if (available2 >= 0) {
            return t;
        }
        FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt MMS message");
        return im5.f1643a[0].getBytes();
    }

    @DexIgnore
    public static void j(ByteArrayInputStream byteArrayInputStream, HashMap<Integer, Object> hashMap, Integer num) {
        int available;
        int intValue;
        int available2 = byteArrayInputStream.available();
        int intValue2 = num.intValue();
        while (intValue2 > 0) {
            int read = byteArrayInputStream.read();
            intValue2--;
            if (read != 129) {
                if (read != 131) {
                    if (read == 133 || read == 151) {
                        byte[] t = t(byteArrayInputStream, 0);
                        if (!(t == null || hashMap == null)) {
                            hashMap.put(151, t);
                        }
                        available = byteArrayInputStream.available();
                        intValue = num.intValue();
                    } else {
                        if (read != 153) {
                            if (read != 137) {
                                if (read != 138) {
                                    if (-1 == u(byteArrayInputStream, intValue2)) {
                                        FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Content-Type");
                                    } else {
                                        intValue2 = 0;
                                    }
                                }
                            }
                        }
                        byte[] t2 = t(byteArrayInputStream, 0);
                        if (!(t2 == null || hashMap == null)) {
                            hashMap.put(153, t2);
                        }
                        available = byteArrayInputStream.available();
                        intValue = num.intValue();
                    }
                }
                byteArrayInputStream.mark(1);
                int c2 = c(byteArrayInputStream);
                byteArrayInputStream.reset();
                if (c2 > 127) {
                    int q = q(byteArrayInputStream);
                    String[] strArr = im5.f1643a;
                    if (q < strArr.length) {
                        hashMap.put(131, strArr[q].getBytes());
                    }
                } else {
                    byte[] t3 = t(byteArrayInputStream, 0);
                    if (!(t3 == null || hashMap == null)) {
                        hashMap.put(131, t3);
                    }
                }
                available = byteArrayInputStream.available();
                intValue = num.intValue();
            } else {
                byteArrayInputStream.mark(1);
                int c3 = c(byteArrayInputStream);
                byteArrayInputStream.reset();
                if ((c3 <= 32 || c3 >= 127) && c3 != 0) {
                    int m = (int) m(byteArrayInputStream);
                    if (hashMap != null) {
                        hashMap.put(129, Integer.valueOf(m));
                    }
                } else {
                    try {
                        hashMap.put(129, Integer.valueOf(am5.a(new String(t(byteArrayInputStream, 0)))));
                    } catch (UnsupportedEncodingException e2) {
                        hashMap.put(129, 0);
                    }
                }
                available = byteArrayInputStream.available();
                intValue = num.intValue();
            }
            intValue2 = intValue - (available2 - available);
        }
        if (intValue2 != 0) {
            FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Content-Type");
        }
    }

    @DexIgnore
    public static cm5 k(ByteArrayInputStream byteArrayInputStream) {
        int i;
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read() & 255;
        if (read == 0) {
            return new cm5("");
        }
        byteArrayInputStream.reset();
        if (read < 32) {
            s(byteArrayInputStream);
            i = q(byteArrayInputStream);
        } else {
            i = 0;
        }
        byte[] t = t(byteArrayInputStream, 0);
        if (i == 0) {
            return new cm5(t);
        }
        try {
            return new cm5(i, t);
        } catch (Exception e2) {
            return null;
        }
    }

    @DexIgnore
    public static long m(ByteArrayInputStream byteArrayInputStream) {
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read();
        byteArrayInputStream.reset();
        return read > 127 ? (long) q(byteArrayInputStream) : n(byteArrayInputStream);
    }

    @DexIgnore
    public static long n(ByteArrayInputStream byteArrayInputStream) {
        int read = byteArrayInputStream.read() & 255;
        if (read <= 8) {
            long j = 0;
            for (int i = 0; i < read; i++) {
                j = (j << 8) + ((long) (byteArrayInputStream.read() & 255));
            }
            return j;
        }
        throw new RuntimeException("Octet count greater than 8 and I can't represent that!");
    }

    @DexIgnore
    public static boolean o(ByteArrayInputStream byteArrayInputStream, lm5 lm5, int i) {
        int available;
        int available2 = byteArrayInputStream.available();
        int i2 = i;
        while (i2 > 0) {
            int read = byteArrayInputStream.read();
            i2--;
            if (read > 127) {
                if (read != 142) {
                    if (read != 174) {
                        if (read == 192) {
                            byte[] t = t(byteArrayInputStream, 1);
                            if (t != null) {
                                lm5.i(t);
                            }
                            available = byteArrayInputStream.available();
                        } else if (read != 197) {
                            if (-1 == u(byteArrayInputStream, i2)) {
                                FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Part headers");
                                return false;
                            }
                            i2 = 0;
                        }
                    }
                    if (Resources.getSystem().getBoolean(Resources.getSystem().getIdentifier("config_mms_content_disposition_support", "id", "android"))) {
                        int s = s(byteArrayInputStream);
                        byteArrayInputStream.mark(1);
                        int available3 = byteArrayInputStream.available();
                        int read2 = byteArrayInputStream.read();
                        if (read2 == 128) {
                            lm5.h(lm5.c);
                        } else if (read2 == 129) {
                            lm5.h(lm5.d);
                        } else if (read2 == 130) {
                            lm5.h(lm5.e);
                        } else {
                            byteArrayInputStream.reset();
                            lm5.h(t(byteArrayInputStream, 0));
                        }
                        if (available3 - byteArrayInputStream.available() < s) {
                            if (byteArrayInputStream.read() == 152) {
                                lm5.n(t(byteArrayInputStream, 0));
                            }
                            int available4 = available3 - byteArrayInputStream.available();
                            if (available4 < s) {
                                int i3 = s - available4;
                                byteArrayInputStream.read(new byte[i3], 0, i3);
                            }
                        }
                        available = byteArrayInputStream.available();
                    }
                } else {
                    byte[] t2 = t(byteArrayInputStream, 0);
                    if (t2 != null) {
                        lm5.j(t2);
                    }
                    available = byteArrayInputStream.available();
                }
            } else if (read < 32 || read > 127) {
                if (-1 == u(byteArrayInputStream, i2)) {
                    FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Part headers");
                    return false;
                }
                i2 = 0;
            } else {
                byte[] t3 = t(byteArrayInputStream, 0);
                byte[] t4 = t(byteArrayInputStream, 0);
                if (true == "Content-Transfer-Encoding".equalsIgnoreCase(new String(t3))) {
                    lm5.k(t4);
                }
                available = byteArrayInputStream.available();
            }
            i2 = i - (available2 - available);
        }
        if (i2 == 0) {
            return true;
        }
        FLogger.INSTANCE.getLocal().e("PduParser", "Corrupt Part headers");
        return false;
    }

    @DexIgnore
    public static hm5 p(ByteArrayInputStream byteArrayInputStream) {
        lm5 lm5;
        if (byteArrayInputStream == null) {
            return null;
        }
        int r = r(byteArrayInputStream);
        hm5 hm5 = new hm5();
        for (int i = 0; i < r; i++) {
            int r2 = r(byteArrayInputStream);
            int r3 = r(byteArrayInputStream);
            lm5 lm52 = new lm5();
            int available = byteArrayInputStream.available();
            if (available <= 0) {
                return null;
            }
            HashMap hashMap = new HashMap();
            byte[] i2 = i(byteArrayInputStream, hashMap);
            if (i2 != null) {
                lm52.l(i2);
            } else {
                lm52.l(im5.f1643a[0].getBytes());
            }
            byte[] bArr = (byte[]) hashMap.get(151);
            if (bArr != null) {
                lm52.o(bArr);
            }
            Integer num = (Integer) hashMap.get(129);
            if (num != null) {
                lm52.g(num.intValue());
            }
            int available2 = r2 - (available - byteArrayInputStream.available());
            if (available2 > 0) {
                if (!o(byteArrayInputStream, lm52, available2)) {
                    return null;
                }
            } else if (available2 < 0) {
                return null;
            }
            if (lm52.b() == null && lm52.f() == null && lm52.e() == null && lm52.a() == null) {
                lm52.j(Long.toOctalString(System.currentTimeMillis()).getBytes());
            }
            if (r3 > 0) {
                byte[] bArr2 = new byte[r3];
                String str = new String(lm52.d());
                byteArrayInputStream.read(bArr2, 0, r3);
                if (str.equalsIgnoreCase("application/vnd.wap.multipart.alternative")) {
                    lm5 = p(new ByteArrayInputStream(bArr2)).c(0);
                } else {
                    byte[] c2 = lm52.c();
                    if (c2 != null) {
                        String str2 = new String(c2);
                        if (str2.equalsIgnoreCase("base64")) {
                            bArr2 = zl5.a(bArr2);
                        } else if (str2.equalsIgnoreCase("quoted-printable")) {
                            bArr2 = mm5.a(bArr2);
                        }
                    }
                    if (bArr2 == null) {
                        g("Decode part data error!");
                        return null;
                    }
                    lm52.m(bArr2);
                    lm5 = lm52;
                }
            } else {
                lm5 = lm52;
            }
            if (b(lm5) == 0) {
                hm5.a(0, lm5);
            } else {
                hm5.b(lm5);
            }
        }
        return hm5;
    }

    @DexIgnore
    public static int q(ByteArrayInputStream byteArrayInputStream) {
        return byteArrayInputStream.read() & 127;
    }

    @DexIgnore
    public static int r(ByteArrayInputStream byteArrayInputStream) {
        int i = 0;
        int read = byteArrayInputStream.read();
        if (read == -1) {
            return read;
        }
        while ((read & 128) != 0) {
            i = (i << 7) | (read & 127);
            read = byteArrayInputStream.read();
            if (read == -1) {
                return read;
            }
        }
        return (read & 127) | (i << 7);
    }

    @DexIgnore
    public static int s(ByteArrayInputStream byteArrayInputStream) {
        int read = byteArrayInputStream.read() & 255;
        if (read <= 30) {
            return read;
        }
        if (read == 31) {
            return r(byteArrayInputStream);
        }
        throw new RuntimeException("Value length > LENGTH_QUOTE!");
    }

    @DexIgnore
    public static byte[] t(ByteArrayInputStream byteArrayInputStream, int i) {
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read();
        if (1 == i && 34 == read) {
            byteArrayInputStream.mark(1);
        } else if (i == 0 && 127 == read) {
            byteArrayInputStream.mark(1);
        } else {
            byteArrayInputStream.reset();
        }
        return d(byteArrayInputStream, i);
    }

    @DexIgnore
    public static int u(ByteArrayInputStream byteArrayInputStream, int i) {
        int read = byteArrayInputStream.read(new byte[i], 0, i);
        if (read < i) {
            return -1;
        }
        return read;
    }

    @DexIgnore
    public dm5 h() {
        ByteArrayInputStream byteArrayInputStream = this.f1932a;
        if (byteArrayInputStream == null) {
            return null;
        }
        jm5 l = l(byteArrayInputStream);
        this.b = l;
        if (l == null) {
            return null;
        }
        int e2 = l.e(ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
        if (!a(this.b)) {
            g("check mandatory headers failed!");
            return null;
        }
        if (128 == e2 || 132 == e2) {
            hm5 p = p(this.f1932a);
            this.c = p;
            if (p == null) {
                return null;
            }
        }
        switch (e2) {
            case 128:
                return new rm5(this.b, this.c);
            case 129:
                return new qm5(this.b);
            case 130:
                return new fm5(this.b);
            case 131:
                return new gm5(this.b);
            case 132:
                pm5 pm5 = new pm5(this.b, this.c);
                byte[] b2 = pm5.b();
                if (b2 == null) {
                    return null;
                }
                String str = new String(b2);
                if (str.equals("application/vnd.wap.multipart.mixed") || str.equals("application/vnd.wap.multipart.related") || str.equals("application/vnd.wap.multipart.alternative")) {
                    return pm5;
                }
                if (!str.equals("application/vnd.wap.multipart.alternative")) {
                    return null;
                }
                lm5 c2 = this.c.c(0);
                this.c.e();
                this.c.a(0, c2);
                return pm5;
            case 133:
                return new yl5(this.b);
            case 134:
                return new bm5(this.b);
            case 135:
                return new om5(this.b);
            case 136:
                return new nm5(this.b);
            default:
                g("Parser doesn't support this message type in this version!");
                return null;
        }
    }

    @DexIgnore
    public jm5 l(ByteArrayInputStream byteArrayInputStream) {
        cm5 cm5;
        byte[] e2;
        if (byteArrayInputStream == null) {
            return null;
        }
        jm5 jm5 = new jm5();
        boolean z = true;
        while (z && byteArrayInputStream.available() > 0) {
            byteArrayInputStream.mark(1);
            int c2 = c(byteArrayInputStream);
            if (c2 < 32 || c2 > 127) {
                switch (c2) {
                    case 129:
                    case 130:
                    case 151:
                        cm5 k = k(byteArrayInputStream);
                        if (k == null) {
                            continue;
                        } else {
                            byte[] e3 = k.e();
                            if (e3 != null) {
                                String str = new String(e3);
                                int indexOf = str.indexOf(47);
                                if (indexOf > 0) {
                                    str = str.substring(0, indexOf);
                                }
                                try {
                                    k.g(str.getBytes());
                                } catch (NullPointerException e4) {
                                    g("null pointer error!");
                                    return null;
                                }
                            }
                            try {
                                jm5.a(k, c2);
                                break;
                            } catch (NullPointerException e5) {
                                g("null pointer error!");
                                break;
                            } catch (RuntimeException e6) {
                                g(c2 + "is not Encoded-String-Value header field!");
                                return null;
                            }
                        }
                    case 131:
                    case 139:
                    case 152:
                    case 158:
                    case 183:
                    case 184:
                    case 185:
                    case 189:
                    case FacebookRequestErrorClassification.EC_INVALID_TOKEN /* 190 */:
                        byte[] t = t(byteArrayInputStream, 0);
                        if (t != null) {
                            try {
                                jm5.j(t, c2);
                                break;
                            } catch (NullPointerException e7) {
                                g("null pointer error!");
                                break;
                            } catch (RuntimeException e8) {
                                g(c2 + "is not Text-String header field!");
                                return null;
                            }
                        } else {
                            continue;
                        }
                    case 132:
                        HashMap hashMap = new HashMap();
                        byte[] i = i(byteArrayInputStream, hashMap);
                        if (i != null) {
                            try {
                                jm5.j(i, 132);
                            } catch (NullPointerException e9) {
                                g("null pointer error!");
                            } catch (RuntimeException e10) {
                                g(c2 + "is not Text-String header field!");
                                return null;
                            }
                        }
                        e = (byte[]) hashMap.get(153);
                        d = (byte[]) hashMap.get(131);
                        z = false;
                        continue;
                    case 133:
                    case 142:
                    case 159:
                        try {
                            jm5.h(n(byteArrayInputStream), c2);
                            continue;
                        } catch (RuntimeException e11) {
                            g(c2 + "is not Long-Integer header field!");
                            return null;
                        }
                    case 134:
                    case 143:
                    case 144:
                    case 145:
                    case 146:
                    case 148:
                    case 149:
                    case 153:
                    case 155:
                    case 156:
                    case 162:
                    case 163:
                    case 165:
                    case 167:
                    case 169:
                    case 171:
                    case 177:
                    case 180:
                    case 186:
                    case 187:
                    case 188:
                    case 191:
                        int c3 = c(byteArrayInputStream);
                        try {
                            jm5.i(c3, c2);
                            continue;
                        } catch (wl5 e12) {
                            g("Set invalid Octet value: " + c3 + " into the header filed: " + c2);
                            return null;
                        } catch (RuntimeException e13) {
                            g(c2 + "is not Octet header field!");
                            return null;
                        }
                    case 135:
                    case 136:
                    case 157:
                        s(byteArrayInputStream);
                        int c4 = c(byteArrayInputStream);
                        try {
                            long n = n(byteArrayInputStream);
                            if (129 == c4) {
                                n += System.currentTimeMillis() / 1000;
                            }
                            try {
                                jm5.h(n, c2);
                                continue;
                            } catch (RuntimeException e14) {
                                g(c2 + "is not Long-Integer header field!");
                                return null;
                            }
                        } catch (RuntimeException e15) {
                            g(c2 + "is not Long-Integer header field!");
                            return null;
                        }
                    case 137:
                        s(byteArrayInputStream);
                        if (128 == c(byteArrayInputStream)) {
                            cm5 = k(byteArrayInputStream);
                            if (!(cm5 == null || (e2 = cm5.e()) == null)) {
                                String str2 = new String(e2);
                                int indexOf2 = str2.indexOf(47);
                                if (indexOf2 > 0) {
                                    str2 = str2.substring(0, indexOf2);
                                }
                                try {
                                    cm5.g(str2.getBytes());
                                } catch (NullPointerException e16) {
                                    g("null pointer error!");
                                    return null;
                                }
                            }
                        } else {
                            try {
                                cm5 = new cm5("insert-address-token".getBytes());
                            } catch (NullPointerException e17) {
                                g(c2 + "is not Encoded-String-Value header field!");
                                return null;
                            }
                        }
                        try {
                            jm5.g(cm5, 137);
                            continue;
                        } catch (NullPointerException e18) {
                            g("null pointer error!");
                            break;
                        } catch (RuntimeException e19) {
                            g(c2 + "is not Encoded-String-Value header field!");
                            return null;
                        }
                    case 138:
                        byteArrayInputStream.mark(1);
                        int c5 = c(byteArrayInputStream);
                        if (c5 >= 128) {
                            if (128 != c5) {
                                if (129 != c5) {
                                    if (130 != c5) {
                                        if (131 != c5) {
                                            break;
                                        } else {
                                            jm5.j("auto".getBytes(), 138);
                                            break;
                                        }
                                    } else {
                                        jm5.j("informational".getBytes(), 138);
                                        break;
                                    }
                                } else {
                                    jm5.j("advertisement".getBytes(), 138);
                                    break;
                                }
                            } else {
                                try {
                                    jm5.j("personal".getBytes(), 138);
                                    continue;
                                } catch (NullPointerException e20) {
                                    g("null pointer error!");
                                    break;
                                } catch (RuntimeException e21) {
                                    g(c2 + "is not Text-String header field!");
                                    return null;
                                }
                            }
                        } else {
                            byteArrayInputStream.reset();
                            byte[] t2 = t(byteArrayInputStream, 0);
                            if (t2 == null) {
                                break;
                            } else {
                                try {
                                    jm5.j(t2, 138);
                                    break;
                                } catch (NullPointerException e22) {
                                    g("null pointer error!");
                                    break;
                                } catch (RuntimeException e23) {
                                    g(c2 + "is not Text-String header field!");
                                    return null;
                                }
                            }
                        }
                    case ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL /* 140 */:
                        int c6 = c(byteArrayInputStream);
                        switch (c6) {
                            case 137:
                            case 138:
                            case 139:
                            case ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL /* 140 */:
                            case 141:
                            case 142:
                            case 143:
                            case 144:
                            case 145:
                            case 146:
                            case 147:
                            case 148:
                            case 149:
                            case 150:
                            case 151:
                                return null;
                            default:
                                try {
                                    jm5.i(c6, c2);
                                    continue;
                                    continue;
                                } catch (wl5 e24) {
                                    g("Set invalid Octet value: " + c6 + " into the header filed: " + c2);
                                    return null;
                                } catch (RuntimeException e25) {
                                    g(c2 + "is not Octet header field!");
                                    return null;
                                }
                        }
                    case 141:
                        int q = q(byteArrayInputStream);
                        try {
                            jm5.i(q, 141);
                            continue;
                        } catch (wl5 e26) {
                            g("Set invalid Octet value: " + q + " into the header filed: " + c2);
                            return null;
                        } catch (RuntimeException e27) {
                            g(c2 + "is not Octet header field!");
                            return null;
                        }
                    case 147:
                    case 150:
                    case 154:
                    case 166:
                    case 181:
                    case 182:
                        cm5 k2 = k(byteArrayInputStream);
                        if (k2 != null) {
                            try {
                                jm5.g(k2, c2);
                                break;
                            } catch (NullPointerException e28) {
                                g("null pointer error!");
                                break;
                            } catch (RuntimeException e29) {
                                g(c2 + "is not Encoded-String-Value header field!");
                                return null;
                            }
                        } else {
                            continue;
                        }
                    case 160:
                        s(byteArrayInputStream);
                        try {
                            m(byteArrayInputStream);
                            cm5 k3 = k(byteArrayInputStream);
                            if (k3 != null) {
                                try {
                                    jm5.g(k3, 160);
                                    break;
                                } catch (NullPointerException e30) {
                                    g("null pointer error!");
                                    break;
                                } catch (RuntimeException e31) {
                                    g(c2 + "is not Encoded-String-Value header field!");
                                    return null;
                                }
                            } else {
                                continue;
                            }
                        } catch (RuntimeException e32) {
                            g(c2 + " is not Integer-Value");
                            return null;
                        }
                    case 161:
                        s(byteArrayInputStream);
                        try {
                            m(byteArrayInputStream);
                            try {
                                jm5.h(n(byteArrayInputStream), 161);
                                continue;
                            } catch (RuntimeException e33) {
                                g(c2 + "is not Long-Integer header field!");
                                return null;
                            }
                        } catch (RuntimeException e34) {
                            g(c2 + " is not Integer-Value");
                            return null;
                        }
                    case 164:
                        s(byteArrayInputStream);
                        c(byteArrayInputStream);
                        k(byteArrayInputStream);
                        continue;
                    case DateTimeConstants.HOURS_PER_WEEK /* 168 */:
                    case 174:
                    case 176:
                    default:
                        g("Unknown header");
                        continue;
                    case 170:
                    case 172:
                        s(byteArrayInputStream);
                        c(byteArrayInputStream);
                        try {
                            m(byteArrayInputStream);
                            continue;
                        } catch (RuntimeException e35) {
                            g(c2 + " is not Integer-Value");
                            return null;
                        }
                    case 173:
                    case 175:
                    case 179:
                        try {
                            jm5.h(m(byteArrayInputStream), c2);
                            continue;
                        } catch (RuntimeException e36) {
                            g(c2 + "is not Long-Integer header field!");
                            return null;
                        }
                    case 178:
                        i(byteArrayInputStream, null);
                        continue;
                }
            } else {
                byteArrayInputStream.reset();
                t(byteArrayInputStream, 0);
            }
        }
        return jm5;
    }
}
