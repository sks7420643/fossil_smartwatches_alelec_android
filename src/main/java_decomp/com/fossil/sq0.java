package com.fossil;

import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sq0 extends Ts0 {
    @DexIgnore
    public static /* final */ ViewModelProvider.Factory g; // = new Ai();
    @DexIgnore
    public /* final */ HashMap<String, Fragment> a; // = new HashMap<>();
    @DexIgnore
    public /* final */ HashMap<String, Sq0> b; // = new HashMap<>();
    @DexIgnore
    public /* final */ HashMap<String, ViewModelStore> c; // = new HashMap<>();
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public boolean e; // = false;
    @DexIgnore
    public boolean f; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements ViewModelProvider.Factory {
        @DexIgnore
        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends Ts0> T create(Class<T> cls) {
            return new Sq0(true);
        }
    }

    @DexIgnore
    public Sq0(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public static Sq0 e(ViewModelStore viewModelStore) {
        return (Sq0) new ViewModelProvider(viewModelStore, g).a(Sq0.class);
    }

    @DexIgnore
    public boolean a(Fragment fragment) {
        if (this.a.containsKey(fragment.mWho)) {
            return false;
        }
        this.a.put(fragment.mWho, fragment);
        return true;
    }

    @DexIgnore
    public void b(Fragment fragment) {
        if (FragmentManager.s0(3)) {
            Log.d("FragmentManager", "Clearing non-config state for " + fragment);
        }
        Sq0 sq0 = this.b.get(fragment.mWho);
        if (sq0 != null) {
            sq0.onCleared();
            this.b.remove(fragment.mWho);
        }
        ViewModelStore viewModelStore = this.c.get(fragment.mWho);
        if (viewModelStore != null) {
            viewModelStore.a();
            this.c.remove(fragment.mWho);
        }
    }

    @DexIgnore
    public Fragment c(String str) {
        return this.a.get(str);
    }

    @DexIgnore
    public Sq0 d(Fragment fragment) {
        Sq0 sq0 = this.b.get(fragment.mWho);
        if (sq0 != null) {
            return sq0;
        }
        Sq0 sq02 = new Sq0(this.d);
        this.b.put(fragment.mWho, sq02);
        return sq02;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Sq0.class != obj.getClass()) {
            return false;
        }
        Sq0 sq0 = (Sq0) obj;
        return this.a.equals(sq0.a) && this.b.equals(sq0.b) && this.c.equals(sq0.c);
    }

    @DexIgnore
    public Collection<Fragment> f() {
        return this.a.values();
    }

    @DexIgnore
    public ViewModelStore g(Fragment fragment) {
        ViewModelStore viewModelStore = this.c.get(fragment.mWho);
        if (viewModelStore != null) {
            return viewModelStore;
        }
        ViewModelStore viewModelStore2 = new ViewModelStore();
        this.c.put(fragment.mWho, viewModelStore2);
        return viewModelStore2;
    }

    @DexIgnore
    public boolean h() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode();
    }

    @DexIgnore
    public boolean i(Fragment fragment) {
        return this.a.remove(fragment.mWho) != null;
    }

    @DexIgnore
    public boolean j(Fragment fragment) {
        if (!this.a.containsKey(fragment.mWho)) {
            return true;
        }
        return this.d ? this.e : !this.f;
    }

    @DexIgnore
    @Override // com.fossil.Ts0
    public void onCleared() {
        if (FragmentManager.s0(3)) {
            Log.d("FragmentManager", "onCleared called for " + this);
        }
        this.e = true;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("FragmentManagerViewModel{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("} Fragments (");
        Iterator<Fragment> it = this.a.values().iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") Child Non Config (");
        Iterator<String> it2 = this.b.keySet().iterator();
        while (it2.hasNext()) {
            sb.append(it2.next());
            if (it2.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") ViewModelStores (");
        Iterator<String> it3 = this.c.keySet().iterator();
        while (it3.hasNext()) {
            sb.append(it3.next());
            if (it3.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(')');
        return sb.toString();
    }
}
