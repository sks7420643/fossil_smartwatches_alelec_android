package com.fossil;

import androidx.collection.SimpleArrayMap;
import java.util.Collection;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zi0<K, V> extends SimpleArrayMap<K, V> implements Map<K, V> {
    @DexIgnore
    public Fj0<K, V> i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Fj0<K, V> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.Fj0
        public void a() {
            Zi0.this.clear();
        }

        @DexIgnore
        @Override // com.fossil.Fj0
        public Object b(int i, int i2) {
            return Zi0.this.c[(i << 1) + i2];
        }

        @DexIgnore
        @Override // com.fossil.Fj0
        public Map<K, V> c() {
            return Zi0.this;
        }

        @DexIgnore
        @Override // com.fossil.Fj0
        public int d() {
            return Zi0.this.d;
        }

        @DexIgnore
        @Override // com.fossil.Fj0
        public int e(Object obj) {
            return Zi0.this.g(obj);
        }

        @DexIgnore
        @Override // com.fossil.Fj0
        public int f(Object obj) {
            return Zi0.this.i(obj);
        }

        @DexIgnore
        @Override // com.fossil.Fj0
        public void g(K k, V v) {
            Zi0.this.put(k, v);
        }

        @DexIgnore
        @Override // com.fossil.Fj0
        public void h(int i) {
            Zi0.this.l(i);
        }

        @DexIgnore
        @Override // com.fossil.Fj0
        public V i(int i, V v) {
            return (V) Zi0.this.m(i, v);
        }
    }

    @DexIgnore
    public Zi0() {
    }

    @DexIgnore
    public Zi0(int i2) {
        super(i2);
    }

    @DexIgnore
    public Zi0(SimpleArrayMap simpleArrayMap) {
        super(simpleArrayMap);
    }

    @DexIgnore
    @Override // java.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        return o().l();
    }

    @DexIgnore
    @Override // java.util.Map
    public Set<K> keySet() {
        return o().m();
    }

    @DexIgnore
    public final Fj0<K, V> o() {
        if (this.i == null) {
            this.i = new Ai();
        }
        return this.i;
    }

    @DexIgnore
    public boolean p(Collection<?> collection) {
        return Fj0.p(this, collection);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.Zi0<K, V> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Map
    public void putAll(Map<? extends K, ? extends V> map) {
        c(this.d + map.size());
        for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
            put(entry.getKey(), entry.getValue());
        }
    }

    @DexIgnore
    @Override // java.util.Map
    public Collection<V> values() {
        return o().n();
    }
}
