package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Us3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Us3> CREATOR; // = new Ts3();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Z52 c;
    @DexIgnore
    public /* final */ Tc2 d;

    @DexIgnore
    public Us3(int i) {
        this(new Z52(8, null), null);
    }

    @DexIgnore
    public Us3(int i, Z52 z52, Tc2 tc2) {
        this.b = i;
        this.c = z52;
        this.d = tc2;
    }

    @DexIgnore
    public Us3(Z52 z52, Tc2 tc2) {
        this(1, z52, null);
    }

    @DexIgnore
    public final Z52 c() {
        return this.c;
    }

    @DexIgnore
    public final Tc2 f() {
        return this.d;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.n(parcel, 1, this.b);
        Bd2.t(parcel, 2, this.c, i, false);
        Bd2.t(parcel, 3, this.d, i, false);
        Bd2.b(parcel, a2);
    }
}
