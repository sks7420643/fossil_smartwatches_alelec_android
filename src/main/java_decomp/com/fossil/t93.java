package com.fossil;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface T93 extends IInterface {
    @DexIgnore
    void beginAdUnitExposure(String str, long j) throws RemoteException;

    @DexIgnore
    void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException;

    @DexIgnore
    void endAdUnitExposure(String str, long j) throws RemoteException;

    @DexIgnore
    void generateEventId(U93 u93) throws RemoteException;

    @DexIgnore
    void getAppInstanceId(U93 u93) throws RemoteException;

    @DexIgnore
    void getCachedAppInstanceId(U93 u93) throws RemoteException;

    @DexIgnore
    void getConditionalUserProperties(String str, String str2, U93 u93) throws RemoteException;

    @DexIgnore
    void getCurrentScreenClass(U93 u93) throws RemoteException;

    @DexIgnore
    void getCurrentScreenName(U93 u93) throws RemoteException;

    @DexIgnore
    void getGmpAppId(U93 u93) throws RemoteException;

    @DexIgnore
    void getMaxUserProperties(String str, U93 u93) throws RemoteException;

    @DexIgnore
    void getTestFlag(U93 u93, int i) throws RemoteException;

    @DexIgnore
    void getUserProperties(String str, String str2, boolean z, U93 u93) throws RemoteException;

    @DexIgnore
    void initForTests(Map map) throws RemoteException;

    @DexIgnore
    void initialize(Rg2 rg2, Xs2 xs2, long j) throws RemoteException;

    @DexIgnore
    void isDataCollectionEnabled(U93 u93) throws RemoteException;

    @DexIgnore
    void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) throws RemoteException;

    @DexIgnore
    void logEventAndBundle(String str, String str2, Bundle bundle, U93 u93, long j) throws RemoteException;

    @DexIgnore
    void logHealthData(int i, String str, Rg2 rg2, Rg2 rg22, Rg2 rg23) throws RemoteException;

    @DexIgnore
    void onActivityCreated(Rg2 rg2, Bundle bundle, long j) throws RemoteException;

    @DexIgnore
    void onActivityDestroyed(Rg2 rg2, long j) throws RemoteException;

    @DexIgnore
    void onActivityPaused(Rg2 rg2, long j) throws RemoteException;

    @DexIgnore
    void onActivityResumed(Rg2 rg2, long j) throws RemoteException;

    @DexIgnore
    void onActivitySaveInstanceState(Rg2 rg2, U93 u93, long j) throws RemoteException;

    @DexIgnore
    void onActivityStarted(Rg2 rg2, long j) throws RemoteException;

    @DexIgnore
    void onActivityStopped(Rg2 rg2, long j) throws RemoteException;

    @DexIgnore
    void performAction(Bundle bundle, U93 u93, long j) throws RemoteException;

    @DexIgnore
    void registerOnMeasurementEventListener(Us2 us2) throws RemoteException;

    @DexIgnore
    void resetAnalyticsData(long j) throws RemoteException;

    @DexIgnore
    void setConditionalUserProperty(Bundle bundle, long j) throws RemoteException;

    @DexIgnore
    void setCurrentScreen(Rg2 rg2, String str, String str2, long j) throws RemoteException;

    @DexIgnore
    void setDataCollectionEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setDefaultEventParameters(Bundle bundle) throws RemoteException;

    @DexIgnore
    void setEventInterceptor(Us2 us2) throws RemoteException;

    @DexIgnore
    void setInstanceIdProvider(Vs2 vs2) throws RemoteException;

    @DexIgnore
    void setMeasurementEnabled(boolean z, long j) throws RemoteException;

    @DexIgnore
    void setMinimumSessionDuration(long j) throws RemoteException;

    @DexIgnore
    void setSessionTimeoutDuration(long j) throws RemoteException;

    @DexIgnore
    void setUserId(String str, long j) throws RemoteException;

    @DexIgnore
    void setUserProperty(String str, String str2, Rg2 rg2, boolean z, long j) throws RemoteException;

    @DexIgnore
    void unregisterOnMeasurementEventListener(Us2 us2) throws RemoteException;
}
