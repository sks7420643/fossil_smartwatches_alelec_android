package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.fossil.S87;
import com.fossil.W67;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.view.watchface.WatchFaceEditorView;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S67 extends R67 {
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public S87.Bi l;
    @DexIgnore
    public ImageView m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Lc6<S67, ViewGroup.LayoutParams> a(WatchFaceEditorView watchFaceEditorView, S87.Bi bi, int i) {
            Wg6.c(watchFaceEditorView, "editorView");
            Wg6.c(bi, "stickerConfig");
            ImageView imageView = new ImageView(watchFaceEditorView.getContext());
            Bitmap i2 = bi.i();
            if (i2 == null) {
                i2 = bi.l();
            }
            imageView.setImageBitmap(i2);
            W87 b = bi.b();
            FrameLayout.LayoutParams layoutParams = b != null ? new FrameLayout.LayoutParams((int) b.c(), (int) b.a()) : new FrameLayout.LayoutParams(-2, -2);
            Context context = watchFaceEditorView.getContext();
            Wg6.b(context, "editorView.context");
            S67 s67 = new S67(context, null);
            s67.z(imageView, bi, layoutParams, i);
            s67.setElementEventHandler(watchFaceEditorView);
            return Hl7.a(s67, new FrameLayout.LayoutParams(-2, -2));
        }

        @DexIgnore
        public final Lc6<S67, ViewGroup.LayoutParams> b(Context context, S87.Bi bi) {
            String str;
            Wg6.c(context, "context");
            Wg6.c(bi, "stickerConfig");
            ImageView imageView = new ImageView(context);
            X87 j = bi.j();
            if (j == null || (str = j.c()) == null) {
                str = "";
            }
            Bitmap i = bi.i();
            BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(), bi.l());
            if (i != null) {
                imageView.setImageBitmap(i);
            } else {
                if (str.length() > 0) {
                    Ty4.a(imageView, str, bitmapDrawable);
                } else {
                    imageView.setImageBitmap(bi.l());
                }
            }
            W87 b = bi.b();
            if (b != null) {
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((int) b.c(), (int) b.a());
                S67 s67 = new S67(context, null);
                S67.A(s67, imageView, bi, layoutParams, 0, 8, null);
                return Hl7.a(s67, new FrameLayout.LayoutParams(-2, -2));
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public S67(Context context) {
        super(context);
    }

    @DexIgnore
    public /* synthetic */ S67(Context context, Qg6 qg6) {
        this(context);
    }

    @DexIgnore
    public static /* synthetic */ S67 A(S67 s67, ImageView imageView, S87.Bi bi, ViewGroup.LayoutParams layoutParams, int i, int i2, Object obj) {
        if ((i2 & 8) != 0) {
            i = 0;
        }
        s67.z(imageView, bi, layoutParams, i);
        return s67;
    }

    @DexIgnore
    @Override // com.fossil.W67
    public S87 c(boolean z) {
        int i = 0;
        S87.Bi bi = this.l;
        if (bi != null) {
            bi.d(getMetric());
        }
        if (z) {
            W67.Ai handler = getHandler();
            int c = handler != null ? handler.c() : 0;
            S87.Bi bi2 = this.l;
            if (bi2 != null) {
                if (bi2 != null) {
                    i = bi2.a();
                }
                bi2.p(i);
            }
            S87.Bi bi3 = this.l;
            if (bi3 != null) {
                bi3.o(c);
            }
        }
        S87.Bi bi4 = this.l;
        if (bi4 != null) {
            return bi4;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.W67
    public W67.Ci getType() {
        return W67.Ci.STICKER;
    }

    @DexIgnore
    @Override // com.fossil.W67
    public void o() {
        if (getTouchCount$app_fossilRelease() > 1) {
            x();
        }
    }

    @DexIgnore
    public final void x() {
        List<X87> m2;
        int i = 0;
        Bitmap bitmap = null;
        S87.Bi bi = this.l;
        if (!(bi == null || (m2 = bi.m()) == null || ((m2 instanceof Collection) && m2.isEmpty()))) {
            Iterator<T> it = m2.iterator();
            int i2 = 0;
            while (it.hasNext()) {
                if (it.next().a() != null) {
                    int i3 = i2 + 1;
                    if (i3 >= 0) {
                        i2 = i3;
                    } else {
                        Hm7.k();
                        throw null;
                    }
                }
            }
            i = i2;
        }
        if (i >= 2) {
            S87.Bi bi2 = this.l;
            O87 h = bi2 != null ? bi2.h() : null;
            O87 o87 = O87.WHITE;
            if (h == o87) {
                S87.Bi bi3 = this.l;
                if (bi3 != null) {
                    bi3.n(O87.BLACK);
                }
                ImageView imageView = this.m;
                if (imageView != null) {
                    S87.Bi bi4 = this.l;
                    if (bi4 != null) {
                        bitmap = bi4.i();
                    }
                    imageView.setImageBitmap(bitmap);
                }
            } else {
                S87.Bi bi5 = this.l;
                if (bi5 != null) {
                    bi5.n(o87);
                }
                ImageView imageView2 = this.m;
                if (imageView2 != null) {
                    S87.Bi bi6 = this.l;
                    if (bi6 != null) {
                        bitmap = bi6.i();
                    }
                    imageView2.setImageBitmap(bitmap);
                }
            }
            m();
        }
    }

    @DexIgnore
    public void y() {
        Bitmap l2;
        S87.Bi bi = this.l;
        if (bi == null || (l2 = bi.i()) == null) {
            S87.Bi bi2 = this.l;
            l2 = bi2 != null ? bi2.l() : null;
        }
        ImageView imageView = this.m;
        if (imageView != null) {
            A51 b = X41.b();
            Context context = imageView.getContext();
            Wg6.b(context, "context");
            U71 u71 = new U71(context, b.a());
            u71.x(l2);
            u71.z(imageView);
            b.b(u71.w());
        }
    }

    @DexIgnore
    public final S67 z(ImageView imageView, S87.Bi bi, ViewGroup.LayoutParams layoutParams, int i) {
        addView(imageView, layoutParams);
        this.m = imageView;
        bi.o(i);
        bi.p(i);
        this.l = bi;
        return this;
    }
}
