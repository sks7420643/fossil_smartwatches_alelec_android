package com.fossil;

import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;
import com.fossil.Rd7;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wc7 extends Rd7 {
    @DexIgnore
    public static /* final */ int b; // = 22;
    @DexIgnore
    public /* final */ AssetManager a;

    @DexIgnore
    public Wc7(Context context) {
        this.a = context.getAssets();
    }

    @DexIgnore
    public static String j(Pd7 pd7) {
        return pd7.d.toString().substring(b);
    }

    @DexIgnore
    @Override // com.fossil.Rd7
    public boolean c(Pd7 pd7) {
        Uri uri = pd7.d;
        return "file".equals(uri.getScheme()) && !uri.getPathSegments().isEmpty() && "android_asset".equals(uri.getPathSegments().get(0));
    }

    @DexIgnore
    @Override // com.fossil.Rd7
    public Rd7.Ai f(Pd7 pd7, int i) throws IOException {
        return new Rd7.Ai(this.a.open(j(pd7)), Picasso.LoadedFrom.DISK);
    }
}
