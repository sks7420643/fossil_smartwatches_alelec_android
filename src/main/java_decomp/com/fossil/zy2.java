package com.fossil;

import java.util.Comparator;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zy2 {
    @DexIgnore
    public static boolean a(Comparator<?> comparator, Iterable<?> iterable) {
        Object comparator2;
        Sw2.b(comparator);
        Sw2.b(iterable);
        if (iterable instanceof SortedSet) {
            comparator2 = ((SortedSet) iterable).comparator();
            if (comparator2 == null) {
                comparator2 = Ly2.zza;
            }
        } else if (!(iterable instanceof Az2)) {
            return false;
        } else {
            comparator2 = ((Az2) iterable).comparator();
        }
        return comparator.equals(comparator2);
    }
}
