package com.fossil;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Base64;
import com.facebook.applinks.AppLinkData;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.AlarmManagerSchedulerBroadcastReceiver;
import com.misfit.frameworks.buttonservice.model.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class P12 implements H22 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ K22 b;
    @DexIgnore
    public AlarmManager c;
    @DexIgnore
    public /* final */ V12 d;
    @DexIgnore
    public /* final */ T32 e;

    @DexIgnore
    public P12(Context context, K22 k22, AlarmManager alarmManager, T32 t32, V12 v12) {
        this.a = context;
        this.b = k22;
        this.c = alarmManager;
        this.e = t32;
        this.d = v12;
    }

    @DexIgnore
    public P12(Context context, K22 k22, T32 t32, V12 v12) {
        this(context, k22, (AlarmManager) context.getSystemService(Alarm.TABLE_NAME), t32, v12);
    }

    @DexIgnore
    @Override // com.fossil.H22
    public void a(H02 h02, int i) {
        Uri.Builder builder = new Uri.Builder();
        builder.appendQueryParameter("backendName", h02.b());
        builder.appendQueryParameter("priority", String.valueOf(Z32.a(h02.d())));
        if (h02.c() != null) {
            builder.appendQueryParameter(AppLinkData.ARGUMENTS_EXTRAS_KEY, Base64.encodeToString(h02.c(), 0));
        }
        Intent intent = new Intent(this.a, AlarmManagerSchedulerBroadcastReceiver.class);
        intent.setData(builder.build());
        intent.putExtra("attemptNumber", i);
        if (b(intent)) {
            C12.a("AlarmManagerScheduler", "Upload for context %s is already scheduled. Returning...", h02);
            return;
        }
        long c0 = this.b.c0(h02);
        long f = this.d.f(h02.d(), c0, i);
        C12.b("AlarmManagerScheduler", "Scheduling upload for context %s in %dms(Backend next call timestamp %d). Attempt %d", h02, Long.valueOf(f), Long.valueOf(c0), Integer.valueOf(i));
        this.c.set(3, this.e.a() + f, PendingIntent.getBroadcast(this.a, 0, intent, 0));
    }

    @DexIgnore
    public boolean b(Intent intent) {
        return PendingIntent.getBroadcast(this.a, 0, intent, 536870912) != null;
    }
}
