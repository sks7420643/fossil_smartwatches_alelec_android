package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ar extends Zj {
    @DexIgnore
    public /* final */ Ou1 T;

    @DexIgnore
    public Ar(K5 k5, I60 i60, Ou1 ou1) {
        super(k5, i60, Yp.L0, true, 1797, ou1.getData(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.T = ou1;
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro, com.fossil.Mj
    public JSONObject C() {
        return G80.k(super.C(), Jd0.T5, this.T.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        return this.T.getFileVersion();
    }
}
