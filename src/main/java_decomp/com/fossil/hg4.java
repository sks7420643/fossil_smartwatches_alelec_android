package com.fossil;

import android.content.Intent;
import android.os.Binder;
import android.os.Process;
import android.util.Log;
import com.fossil.Kg4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hg4 extends Binder {
    @DexIgnore
    public /* final */ Ai b;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        Nt3<Void> a(Intent intent);
    }

    @DexIgnore
    public Hg4(Ai ai) {
        this.b = ai;
    }

    @DexIgnore
    public void b(Kg4.Ai ai) {
        if (Binder.getCallingUid() == Process.myUid()) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "service received new intent via bind strategy");
            }
            this.b.a(ai.a).c(Se4.a(), new Gg4(ai));
            return;
        }
        throw new SecurityException("Binding only allowed within app");
    }
}
