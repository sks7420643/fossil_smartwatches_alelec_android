package com.fossil;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class Nu0 {
    @DexIgnore
    public /* final */ ViewGroup a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Deprecated
    public static class Ai {
        @DexIgnore
        public float a; // = -1.0f;
        @DexIgnore
        public float b; // = -1.0f;
        @DexIgnore
        public float c; // = -1.0f;
        @DexIgnore
        public float d; // = -1.0f;
        @DexIgnore
        public float e; // = -1.0f;
        @DexIgnore
        public float f; // = -1.0f;
        @DexIgnore
        public float g; // = -1.0f;
        @DexIgnore
        public float h; // = -1.0f;
        @DexIgnore
        public float i;
        @DexIgnore
        public /* final */ Ci j; // = new Ci(0, 0);

        @DexIgnore
        public void a(ViewGroup.LayoutParams layoutParams, int i2, int i3) {
            boolean z = false;
            Ci ci = this.j;
            int i4 = layoutParams.width;
            ((ViewGroup.MarginLayoutParams) ci).width = i4;
            ((ViewGroup.MarginLayoutParams) ci).height = layoutParams.height;
            boolean z2 = (ci.b || i4 == 0) && this.a < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            Ci ci2 = this.j;
            if ((ci2.a || ((ViewGroup.MarginLayoutParams) ci2).height == 0) && this.b < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                z = true;
            }
            float f2 = this.a;
            if (f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                layoutParams.width = Math.round(f2 * ((float) i2));
            }
            float f3 = this.b;
            if (f3 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                layoutParams.height = Math.round(f3 * ((float) i3));
            }
            float f4 = this.i;
            if (f4 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                if (z2) {
                    layoutParams.width = Math.round(((float) layoutParams.height) * f4);
                    this.j.b = true;
                }
                if (z) {
                    layoutParams.height = Math.round(((float) layoutParams.width) / this.i);
                    this.j.a = true;
                }
            }
        }

        @DexIgnore
        public void b(View view, ViewGroup.MarginLayoutParams marginLayoutParams, int i2, int i3) {
            boolean z = true;
            a(marginLayoutParams, i2, i3);
            Ci ci = this.j;
            ((ViewGroup.MarginLayoutParams) ci).leftMargin = marginLayoutParams.leftMargin;
            ((ViewGroup.MarginLayoutParams) ci).topMargin = marginLayoutParams.topMargin;
            ((ViewGroup.MarginLayoutParams) ci).rightMargin = marginLayoutParams.rightMargin;
            ((ViewGroup.MarginLayoutParams) ci).bottomMargin = marginLayoutParams.bottomMargin;
            Zn0.e(ci, Zn0.b(marginLayoutParams));
            Zn0.d(this.j, Zn0.a(marginLayoutParams));
            float f2 = this.c;
            if (f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.leftMargin = Math.round(f2 * ((float) i2));
            }
            float f3 = this.d;
            if (f3 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.topMargin = Math.round(f3 * ((float) i3));
            }
            float f4 = this.e;
            if (f4 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.rightMargin = Math.round(f4 * ((float) i2));
            }
            float f5 = this.f;
            if (f5 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                marginLayoutParams.bottomMargin = Math.round(f5 * ((float) i3));
            }
            boolean z2 = false;
            float f6 = this.g;
            if (f6 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                Zn0.e(marginLayoutParams, Math.round(((float) i2) * f6));
                z2 = true;
            }
            float f7 = this.h;
            if (f7 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                Zn0.d(marginLayoutParams, Math.round(((float) i2) * f7));
            } else {
                z = z2;
            }
            if (z && view != null) {
                Zn0.c(marginLayoutParams, Mo0.z(view));
            }
        }

        @DexIgnore
        public void c(ViewGroup.LayoutParams layoutParams) {
            Ci ci = this.j;
            if (!ci.b) {
                layoutParams.width = ((ViewGroup.MarginLayoutParams) ci).width;
            }
            Ci ci2 = this.j;
            if (!ci2.a) {
                layoutParams.height = ((ViewGroup.MarginLayoutParams) ci2).height;
            }
            Ci ci3 = this.j;
            ci3.b = false;
            ci3.a = false;
        }

        @DexIgnore
        public void d(ViewGroup.MarginLayoutParams marginLayoutParams) {
            c(marginLayoutParams);
            Ci ci = this.j;
            marginLayoutParams.leftMargin = ((ViewGroup.MarginLayoutParams) ci).leftMargin;
            marginLayoutParams.topMargin = ((ViewGroup.MarginLayoutParams) ci).topMargin;
            marginLayoutParams.rightMargin = ((ViewGroup.MarginLayoutParams) ci).rightMargin;
            marginLayoutParams.bottomMargin = ((ViewGroup.MarginLayoutParams) ci).bottomMargin;
            Zn0.e(marginLayoutParams, Zn0.b(ci));
            Zn0.d(marginLayoutParams, Zn0.a(this.j));
        }

        @DexIgnore
        public String toString() {
            return String.format("PercentLayoutInformation width: %f height %f, margins (%f, %f,  %f, %f, %f, %f)", Float.valueOf(this.a), Float.valueOf(this.b), Float.valueOf(this.c), Float.valueOf(this.d), Float.valueOf(this.e), Float.valueOf(this.f), Float.valueOf(this.g), Float.valueOf(this.h));
        }
    }

    @DexIgnore
    @Deprecated
    public interface Bi {
        @DexIgnore
        Ai a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci extends ViewGroup.MarginLayoutParams {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public Ci(int i, int i2) {
            super(i, i2);
        }
    }

    @DexIgnore
    public Nu0(ViewGroup viewGroup) {
        if (viewGroup != null) {
            this.a = viewGroup;
            return;
        }
        throw new IllegalArgumentException("host must be non-null");
    }

    @DexIgnore
    public static void b(ViewGroup.LayoutParams layoutParams, TypedArray typedArray, int i, int i2) {
        layoutParams.width = typedArray.getLayoutDimension(i, 0);
        layoutParams.height = typedArray.getLayoutDimension(i2, 0);
    }

    @DexIgnore
    public static Ai c(Context context, AttributeSet attributeSet) {
        Ai ai;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, Mu0.PercentLayout_Layout);
        float fraction = obtainStyledAttributes.getFraction(Mu0.PercentLayout_Layout_layout_widthPercent, 1, 1, -1.0f);
        if (fraction != -1.0f) {
            ai = new Ai();
            ai.a = fraction;
        } else {
            ai = null;
        }
        float fraction2 = obtainStyledAttributes.getFraction(Mu0.PercentLayout_Layout_layout_heightPercent, 1, 1, -1.0f);
        if (fraction2 != -1.0f) {
            if (ai == null) {
                ai = new Ai();
            }
            ai.b = fraction2;
        }
        float fraction3 = obtainStyledAttributes.getFraction(Mu0.PercentLayout_Layout_layout_marginPercent, 1, 1, -1.0f);
        if (fraction3 != -1.0f) {
            if (ai == null) {
                ai = new Ai();
            }
            ai.c = fraction3;
            ai.d = fraction3;
            ai.e = fraction3;
            ai.f = fraction3;
        }
        float fraction4 = obtainStyledAttributes.getFraction(Mu0.PercentLayout_Layout_layout_marginLeftPercent, 1, 1, -1.0f);
        if (fraction4 != -1.0f) {
            if (ai == null) {
                ai = new Ai();
            }
            ai.c = fraction4;
        }
        float fraction5 = obtainStyledAttributes.getFraction(Mu0.PercentLayout_Layout_layout_marginTopPercent, 1, 1, -1.0f);
        if (fraction5 != -1.0f) {
            if (ai == null) {
                ai = new Ai();
            }
            ai.d = fraction5;
        }
        float fraction6 = obtainStyledAttributes.getFraction(Mu0.PercentLayout_Layout_layout_marginRightPercent, 1, 1, -1.0f);
        if (fraction6 != -1.0f) {
            if (ai == null) {
                ai = new Ai();
            }
            ai.e = fraction6;
        }
        float fraction7 = obtainStyledAttributes.getFraction(Mu0.PercentLayout_Layout_layout_marginBottomPercent, 1, 1, -1.0f);
        if (fraction7 != -1.0f) {
            if (ai == null) {
                ai = new Ai();
            }
            ai.f = fraction7;
        }
        float fraction8 = obtainStyledAttributes.getFraction(Mu0.PercentLayout_Layout_layout_marginStartPercent, 1, 1, -1.0f);
        if (fraction8 != -1.0f) {
            if (ai == null) {
                ai = new Ai();
            }
            ai.g = fraction8;
        }
        float fraction9 = obtainStyledAttributes.getFraction(Mu0.PercentLayout_Layout_layout_marginEndPercent, 1, 1, -1.0f);
        if (fraction9 != -1.0f) {
            if (ai == null) {
                ai = new Ai();
            }
            ai.h = fraction9;
        }
        float fraction10 = obtainStyledAttributes.getFraction(Mu0.PercentLayout_Layout_layout_aspectRatio, 1, 1, -1.0f);
        if (fraction10 != -1.0f) {
            if (ai == null) {
                ai = new Ai();
            }
            ai.i = fraction10;
        }
        obtainStyledAttributes.recycle();
        return ai;
    }

    @DexIgnore
    public static boolean f(View view, Ai ai) {
        return (view.getMeasuredHeightAndState() & -16777216) == 16777216 && ai.b >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && ((ViewGroup.MarginLayoutParams) ai.j).height == -2;
    }

    @DexIgnore
    public static boolean g(View view, Ai ai) {
        return (view.getMeasuredWidthAndState() & -16777216) == 16777216 && ai.a >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && ((ViewGroup.MarginLayoutParams) ai.j).width == -2;
    }

    @DexIgnore
    public void a(int i, int i2) {
        Ai a2;
        int size = (View.MeasureSpec.getSize(i) - this.a.getPaddingLeft()) - this.a.getPaddingRight();
        int size2 = (View.MeasureSpec.getSize(i2) - this.a.getPaddingTop()) - this.a.getPaddingBottom();
        int childCount = this.a.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = this.a.getChildAt(i3);
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if ((layoutParams instanceof Bi) && (a2 = ((Bi) layoutParams).a()) != null) {
                if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                    a2.b(childAt, (ViewGroup.MarginLayoutParams) layoutParams, size, size2);
                } else {
                    a2.a(layoutParams, size, size2);
                }
            }
        }
    }

    @DexIgnore
    public boolean d() {
        Ai a2;
        boolean z;
        int childCount = this.a.getChildCount();
        boolean z2 = false;
        for (int i = 0; i < childCount; i++) {
            View childAt = this.a.getChildAt(i);
            ViewGroup.LayoutParams layoutParams = childAt.getLayoutParams();
            if ((layoutParams instanceof Bi) && (a2 = ((Bi) layoutParams).a()) != null) {
                if (g(childAt, a2)) {
                    layoutParams.width = -2;
                    z = true;
                } else {
                    z = z2;
                }
                if (f(childAt, a2)) {
                    layoutParams.height = -2;
                    z2 = true;
                } else {
                    z2 = z;
                }
            }
        }
        return z2;
    }

    @DexIgnore
    public void e() {
        Ai a2;
        int childCount = this.a.getChildCount();
        for (int i = 0; i < childCount; i++) {
            ViewGroup.LayoutParams layoutParams = this.a.getChildAt(i).getLayoutParams();
            if ((layoutParams instanceof Bi) && (a2 = ((Bi) layoutParams).a()) != null) {
                if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                    a2.d((ViewGroup.MarginLayoutParams) layoutParams);
                } else {
                    a2.c(layoutParams);
                }
            }
        }
    }
}
