package com.fossil;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Jc2;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dc2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Dc2> CREATOR; // = new Fe2();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public String e;
    @DexIgnore
    public IBinder f;
    @DexIgnore
    public Scope[] g;
    @DexIgnore
    public Bundle h;
    @DexIgnore
    public Account i;
    @DexIgnore
    public B62[] j;
    @DexIgnore
    public B62[] k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public int m;

    @DexIgnore
    public Dc2(int i2) {
        this.b = 4;
        this.d = D62.a;
        this.c = i2;
        this.l = true;
    }

    @DexIgnore
    public Dc2(int i2, int i3, int i4, String str, IBinder iBinder, Scope[] scopeArr, Bundle bundle, Account account, B62[] b62Arr, B62[] b62Arr2, boolean z, int i5) {
        this.b = i2;
        this.c = i3;
        this.d = i4;
        if ("com.google.android.gms".equals(str)) {
            this.e = "com.google.android.gms";
        } else {
            this.e = str;
        }
        if (i2 < 2) {
            this.i = iBinder != null ? Wb2.i(Jc2.Ai.e(iBinder)) : null;
        } else {
            this.f = iBinder;
            this.i = account;
        }
        this.g = scopeArr;
        this.h = bundle;
        this.j = b62Arr;
        this.k = b62Arr2;
        this.l = z;
        this.m = i5;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = Bd2.a(parcel);
        Bd2.n(parcel, 1, this.b);
        Bd2.n(parcel, 2, this.c);
        Bd2.n(parcel, 3, this.d);
        Bd2.u(parcel, 4, this.e, false);
        Bd2.m(parcel, 5, this.f, false);
        Bd2.x(parcel, 6, this.g, i2, false);
        Bd2.e(parcel, 7, this.h, false);
        Bd2.t(parcel, 8, this.i, i2, false);
        Bd2.x(parcel, 10, this.j, i2, false);
        Bd2.x(parcel, 11, this.k, i2, false);
        Bd2.c(parcel, 12, this.l);
        Bd2.n(parcel, 13, this.m);
        Bd2.b(parcel, a2);
    }
}
