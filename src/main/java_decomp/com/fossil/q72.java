package com.fossil;

import android.os.Looper;
import com.fossil.P72;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Q72 {
    @DexIgnore
    public /* final */ Set<P72<?>> a; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    public static <L> P72<L> a(L l, Looper looper, String str) {
        Rc2.l(l, "Listener must not be null");
        Rc2.l(looper, "Looper must not be null");
        Rc2.l(str, "Listener type must not be null");
        return new P72<>(looper, l, str);
    }

    @DexIgnore
    public static <L> P72.Ai<L> b(L l, String str) {
        Rc2.l(l, "Listener must not be null");
        Rc2.l(str, "Listener type must not be null");
        Rc2.h(str, "Listener type must not be empty");
        return new P72.Ai<>(l, str);
    }

    @DexIgnore
    public final void c() {
        for (P72<?> p72 : this.a) {
            p72.a();
        }
        this.a.clear();
    }
}
