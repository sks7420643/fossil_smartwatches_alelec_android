package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.mapped.AlertDialogFragment;
import com.mapped.Qg6;
import com.mapped.UserCustomizeThemeFragment;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeRingChartViewModel;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ws6 extends BaseFragment implements X47, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static String l;
    @DexIgnore
    public static String m;
    @DexIgnore
    public static String s;
    @DexIgnore
    public static String t;
    @DexIgnore
    public static /* final */ Ai u; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public CustomizeRingChartViewModel h;
    @DexIgnore
    public G37<Z45> i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Ws6.m;
        }

        @DexIgnore
        public final String b() {
            return Ws6.l;
        }

        @DexIgnore
        public final String c() {
            return Ws6.s;
        }

        @DexIgnore
        public final String d() {
            return Ws6.t;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<CustomizeRingChartViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ Ws6 a;

        @DexIgnore
        public Bi(Ws6 ws6) {
            this.a = ws6;
        }

        @DexIgnore
        public final void a(CustomizeRingChartViewModel.Ai ai) {
            if (ai != null) {
                Integer d = ai.d();
                if (d != null) {
                    this.a.R6(d.intValue());
                }
                Integer c = ai.c();
                if (c != null) {
                    this.a.Q6(c.intValue());
                }
                Integer b = ai.b();
                if (b != null) {
                    this.a.P6(b.intValue());
                }
                Integer a2 = ai.a();
                if (a2 != null) {
                    this.a.O6(a2.intValue());
                }
                Integer f = ai.f();
                if (f != null) {
                    this.a.T6(f.intValue());
                }
                Integer e = ai.e();
                if (e != null) {
                    this.a.S6(e.intValue());
                }
                Integer h = ai.h();
                if (h != null) {
                    this.a.V6(h.intValue());
                }
                Integer g = ai.g();
                if (g != null) {
                    this.a.U6(g.intValue());
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(CustomizeRingChartViewModel.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ws6 b;

        @DexIgnore
        public Ci(Ws6 ws6) {
            this.b = ws6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, 401);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ws6 b;

        @DexIgnore
        public Di(Ws6 ws6) {
            this.b = ws6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, Action.ActivityTracker.TAG_ACTIVITY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ws6 b;

        @DexIgnore
        public Ei(Ws6 ws6) {
            this.b = ws6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, MFNetworkReturnCode.WRONG_PASSWORD);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ws6 b;

        @DexIgnore
        public Fi(Ws6 ws6) {
            this.b = ws6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, 404);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ws6 b;

        @DexIgnore
        public Gi(Ws6 ws6) {
            this.b = ws6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.p(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = Ws6.class.getSimpleName();
        Wg6.b(simpleName, "CustomizeRingChartFragment::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.X47
    public void C3(int i2, int i3) {
        Hr7 hr7 = Hr7.a;
        String format = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(16777215 & i3)}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3 + " hexColor=" + format);
        CustomizeRingChartViewModel customizeRingChartViewModel = this.h;
        if (customizeRingChartViewModel != null) {
            customizeRingChartViewModel.h(i2, Color.parseColor(format));
            switch (i2) {
                case 401:
                    l = format;
                    return;
                case Action.ActivityTracker.TAG_ACTIVITY /* 402 */:
                    m = format;
                    return;
                case MFNetworkReturnCode.WRONG_PASSWORD /* 403 */:
                    s = format;
                    return;
                case 404:
                    t = format;
                    return;
                default:
                    return;
            }
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void O6(int i2) {
        G37<Z45> g37 = this.i;
        if (g37 != null) {
            Z45 a2 = g37.a();
            if (a2 != null) {
                a2.A.setProgressRingColorPreview(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void P6(int i2) {
        G37<Z45> g37 = this.i;
        if (g37 != null) {
            Z45 a2 = g37.a();
            if (a2 != null) {
                a2.I.setBackgroundColor(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void Q6(int i2) {
        G37<Z45> g37 = this.i;
        if (g37 != null) {
            Z45 a2 = g37.a();
            if (a2 != null) {
                a2.B.setProgressRingColorPreview(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        FLogger.INSTANCE.getLocal().d(k, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363373) {
            CustomizeRingChartViewModel customizeRingChartViewModel = this.h;
            if (customizeRingChartViewModel != null) {
                customizeRingChartViewModel.f(UserCustomizeThemeFragment.m.a(), l, m, s, t);
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void R6(int i2) {
        G37<Z45> g37 = this.i;
        if (g37 != null) {
            Z45 a2 = g37.a();
            if (a2 != null) {
                a2.J.setBackgroundColor(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void S6(int i2) {
        G37<Z45> g37 = this.i;
        if (g37 != null) {
            Z45 a2 = g37.a();
            if (a2 != null) {
                a2.C.setProgressRingColorPreview(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void T6(int i2) {
        G37<Z45> g37 = this.i;
        if (g37 != null) {
            Z45 a2 = g37.a();
            if (a2 != null) {
                a2.K.setBackgroundColor(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void U6(int i2) {
        G37<Z45> g37 = this.i;
        if (g37 != null) {
            Z45 a2 = g37.a();
            if (a2 != null) {
                a2.D.setProgressRingColorPreview(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void V6(int i2) {
        G37<Z45> g37 = this.i;
        if (g37 != null) {
            Z45 a2 = g37.a();
            if (a2 != null) {
                a2.L.setBackgroundColor(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Z45 z45 = (Z45) Aq0.f(LayoutInflater.from(getContext()), 2131558537, null, false, A6());
        PortfolioApp.get.instance().getIface().e(new Ys6()).a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(CustomizeRingChartViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026artViewModel::class.java)");
            CustomizeRingChartViewModel customizeRingChartViewModel = (CustomizeRingChartViewModel) a2;
            this.h = customizeRingChartViewModel;
            if (customizeRingChartViewModel != null) {
                customizeRingChartViewModel.e().h(getViewLifecycleOwner(), new Bi(this));
                CustomizeRingChartViewModel customizeRingChartViewModel2 = this.h;
                if (customizeRingChartViewModel2 != null) {
                    customizeRingChartViewModel2.g();
                    this.i = new G37<>(this, z45);
                    Wg6.b(z45, "binding");
                    return z45.n();
                }
                Wg6.n("mViewModel");
                throw null;
            }
            Wg6.n("mViewModel");
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(k, "onDestroy");
        l = null;
        m = null;
        s = null;
        t = null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(k, "onResume");
        CustomizeRingChartViewModel customizeRingChartViewModel = this.h;
        if (customizeRingChartViewModel != null) {
            customizeRingChartViewModel.g();
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<Z45> g37 = this.i;
        if (g37 != null) {
            Z45 a2 = g37.a();
            if (a2 != null) {
                a2.B.setProgress(1.0f);
                a2.A.setProgress(1.0f);
                a2.C.setProgress(1.0f);
                a2.D.setProgress(1.0f);
                a2.x.setOnClickListener(new Ci(this));
                a2.w.setOnClickListener(new Di(this));
                a2.y.setOnClickListener(new Ei(this));
                a2.z.setOnClickListener(new Fi(this));
                a2.v.setOnClickListener(new Gi(this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.X47
    public void q3(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
