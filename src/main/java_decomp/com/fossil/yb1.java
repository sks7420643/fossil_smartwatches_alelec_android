package com.fossil;

import com.fossil.Xb1;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yb1 {
    @DexIgnore
    public static /* final */ Xb1.Ai<?> b; // = new Ai();
    @DexIgnore
    public /* final */ Map<Class<?>, Xb1.Ai<?>> a; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Xb1.Ai<Object> {
        @DexIgnore
        @Override // com.fossil.Xb1.Ai
        public Xb1<Object> a(Object obj) {
            return new Bi(obj);
        }

        @DexIgnore
        @Override // com.fossil.Xb1.Ai
        public Class<Object> getDataClass() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Xb1<Object> {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public Bi(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        @Override // com.fossil.Xb1
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.Xb1
        public Object b() {
            return this.a;
        }
    }

    @DexIgnore
    public <T> Xb1<T> a(T t) {
        Xb1<T> xb1;
        synchronized (this) {
            Ik1.d(t);
            Xb1.Ai<?> ai = this.a.get(t.getClass());
            if (ai == null) {
                Iterator<Xb1.Ai<?>> it = this.a.values().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    Xb1.Ai<?> next = it.next();
                    if (next.getDataClass().isAssignableFrom(t.getClass())) {
                        ai = next;
                        break;
                    }
                }
            }
            if (ai == null) {
                ai = b;
            }
            xb1 = (Xb1<T>) ai.a(t);
        }
        return xb1;
    }

    @DexIgnore
    public void b(Xb1.Ai<?> ai) {
        synchronized (this) {
            this.a.put(ai.getDataClass(), ai);
        }
    }
}
