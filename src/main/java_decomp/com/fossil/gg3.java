package com.fossil;

import android.os.Bundle;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gg3 extends Jj3 {
    @DexIgnore
    public /* final */ Map<String, Long> b; // = new Zi0();
    @DexIgnore
    public /* final */ Map<String, Integer> c; // = new Zi0();
    @DexIgnore
    public long d;

    @DexIgnore
    public Gg3(Pm3 pm3) {
        super(pm3);
    }

    @DexIgnore
    public final void A(String str, long j, Xo3 xo3) {
        if (xo3 == null) {
            d().N().a("Not logging ad unit exposure. No active activity");
        } else if (j < 1000) {
            d().N().b("Not logging ad unit exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("_ai", str);
            bundle.putLong("_xt", j);
            Ap3.L(xo3, bundle, true);
            p().Q("am", "_xu", bundle);
        }
    }

    @DexIgnore
    public final void B(long j) {
        for (String str : this.b.keySet()) {
            this.b.put(str, Long.valueOf(j));
        }
        if (!this.b.isEmpty()) {
            this.d = j;
        }
    }

    @DexIgnore
    public final void D(String str, long j) {
        if (str == null || str.length() == 0) {
            d().F().a("Ad unit id must be a non-empty string");
        } else {
            c().y(new Hh3(this, str, j));
        }
    }

    @DexIgnore
    public final void E(String str, long j) {
        f();
        h();
        Rc2.g(str);
        if (this.c.isEmpty()) {
            this.d = j;
        }
        Integer num = this.c.get(str);
        if (num != null) {
            this.c.put(str, Integer.valueOf(num.intValue() + 1));
        } else if (this.c.size() >= 100) {
            d().I().a("Too many ads visible");
        } else {
            this.c.put(str, 1);
            this.b.put(str, Long.valueOf(j));
        }
    }

    @DexIgnore
    public final void F(String str, long j) {
        f();
        h();
        Rc2.g(str);
        Integer num = this.c.get(str);
        if (num != null) {
            Xo3 D = s().D(false);
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.c.remove(str);
                Long l = this.b.get(str);
                if (l == null) {
                    d().F().a("First ad unit exposure time was never set");
                } else {
                    long longValue = l.longValue();
                    this.b.remove(str);
                    A(str, j - longValue, D);
                }
                if (this.c.isEmpty()) {
                    long j2 = this.d;
                    if (j2 == 0) {
                        d().F().a("First ad exposure time was never set");
                        return;
                    }
                    w(j - j2, D);
                    this.d = 0;
                    return;
                }
                return;
            }
            this.c.put(str, Integer.valueOf(intValue));
            return;
        }
        d().F().b("Call to endAdUnitExposure for unknown ad unit id", str);
    }

    @DexIgnore
    public final void v(long j) {
        Xo3 D = s().D(false);
        for (String str : this.b.keySet()) {
            A(str, j - this.b.get(str).longValue(), D);
        }
        if (!this.b.isEmpty()) {
            w(j - this.d, D);
        }
        B(j);
    }

    @DexIgnore
    public final void w(long j, Xo3 xo3) {
        if (xo3 == null) {
            d().N().a("Not logging ad exposure. No active activity");
        } else if (j < 1000) {
            d().N().b("Not logging ad exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong("_xt", j);
            Ap3.L(xo3, bundle, true);
            p().Q("am", "_xa", bundle);
        }
    }

    @DexIgnore
    public final void z(String str, long j) {
        if (str == null || str.length() == 0) {
            d().F().a("Ad unit id must be a non-empty string");
        } else {
            c().y(new Ii3(this, str, j));
        }
    }
}
