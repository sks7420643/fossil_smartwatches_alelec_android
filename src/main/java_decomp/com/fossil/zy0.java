package com.fossil;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zy0 extends Ez0 implements Bz0 {
    @DexIgnore
    public Zy0(Context context, ViewGroup viewGroup, View view) {
        super(context, viewGroup, view);
    }

    @DexIgnore
    public static Zy0 g(ViewGroup viewGroup) {
        return (Zy0) Ez0.e(viewGroup);
    }

    @DexIgnore
    @Override // com.fossil.Bz0
    public void c(View view) {
        this.a.b(view);
    }

    @DexIgnore
    @Override // com.fossil.Bz0
    public void d(View view) {
        this.a.g(view);
    }
}
