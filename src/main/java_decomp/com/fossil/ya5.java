package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ya5 extends Xa5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B;
    @DexIgnore
    public /* final */ ScrollView y;
    @DexIgnore
    public long z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        B = sparseIntArray;
        sparseIntArray.put(2131362080, 1);
        B.put(2131361851, 2);
        B.put(2131362546, 3);
        B.put(2131362407, 4);
        B.put(2131362408, 5);
        B.put(2131361953, 6);
        B.put(2131362409, 7);
        B.put(2131361948, 8);
    }
    */

    @DexIgnore
    public Ya5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 9, A, B));
    }

    @DexIgnore
    public Ya5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (RTLImageView) objArr[2], (FlexibleButton) objArr[8], (FlexibleButton) objArr[6], (ConstraintLayout) objArr[1], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[3]);
        this.z = -1;
        ScrollView scrollView = (ScrollView) objArr[0];
        this.y = scrollView;
        scrollView.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.z != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.z = 1;
        }
        w();
    }
}
