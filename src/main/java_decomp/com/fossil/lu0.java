package com.fossil;

import com.fossil.Gu0;
import com.mapped.V3;
import com.mapped.Xe;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lu0<A, B> extends Gu0<B> {
    @DexIgnore
    public /* final */ Gu0<A> a;
    @DexIgnore
    public /* final */ V3<List<A>, List<B>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Gu0.Bi<A> {
        @DexIgnore
        public /* final */ /* synthetic */ Gu0.Bi a;

        @DexIgnore
        public Ai(Gu0.Bi bi) {
            this.a = bi;
        }

        @DexIgnore
        @Override // com.fossil.Gu0.Bi
        public void a(List<A> list, int i, int i2) {
            this.a.a(Xe.convert(Lu0.this.b, list), i, i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Gu0.Ei<A> {
        @DexIgnore
        public /* final */ /* synthetic */ Gu0.Ei a;

        @DexIgnore
        public Bi(Gu0.Ei ei) {
            this.a = ei;
        }

        @DexIgnore
        @Override // com.fossil.Gu0.Ei
        public void a(List<A> list) {
            this.a.a(Xe.convert(Lu0.this.b, list));
        }
    }

    @DexIgnore
    public Lu0(Gu0<A> gu0, V3<List<A>, List<B>> v3) {
        this.a = gu0;
        this.b = v3;
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public void addInvalidatedCallback(Xe.Ci ci) {
        this.a.addInvalidatedCallback(ci);
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.Gu0
    public void loadInitial(Gu0.Di di, Gu0.Bi<B> bi) {
        this.a.loadInitial(di, new Ai(bi));
    }

    @DexIgnore
    @Override // com.fossil.Gu0
    public void loadRange(Gu0.Gi gi, Gu0.Ei<B> ei) {
        this.a.loadRange(gi, new Bi(ei));
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public void removeInvalidatedCallback(Xe.Ci ci) {
        this.a.removeInvalidatedCallback(ci);
    }
}
