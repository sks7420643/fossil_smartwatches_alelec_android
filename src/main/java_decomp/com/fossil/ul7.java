package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ul7 {
    @DexIgnore
    public static final int a(int i, int i2) {
        return Wg6.d(i ^ RecyclerView.UNDEFINED_DURATION, Integer.MIN_VALUE ^ i2);
    }

    @DexIgnore
    public static final int b(long j, long j2) {
        return ((j ^ Long.MIN_VALUE) > (Long.MIN_VALUE ^ j2) ? 1 : ((j ^ Long.MIN_VALUE) == (Long.MIN_VALUE ^ j2) ? 0 : -1));
    }

    @DexIgnore
    public static final String c(long j) {
        return d(j, 10);
    }

    @DexIgnore
    public static final String d(long j, int i) {
        if (j >= 0) {
            Ct7.a(i);
            String l = Long.toString(j, i);
            Wg6.b(l, "java.lang.Long.toString(this, checkRadix(radix))");
            return l;
        }
        long j2 = (long) i;
        long j3 = ((j >>> 1) / j2) << 1;
        long j4 = j - (j3 * j2);
        if (j4 >= j2) {
            j4 -= j2;
            j3++;
        }
        StringBuilder sb = new StringBuilder();
        Ct7.a(i);
        String l2 = Long.toString(j3, i);
        Wg6.b(l2, "java.lang.Long.toString(this, checkRadix(radix))");
        sb.append(l2);
        Ct7.a(i);
        String l3 = Long.toString(j4, i);
        Wg6.b(l3, "java.lang.Long.toString(this, checkRadix(radix))");
        sb.append(l3);
        return sb.toString();
    }
}
