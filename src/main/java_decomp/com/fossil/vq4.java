package com.fossil;

import com.fossil.Tq4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Vq4 {
    @DexIgnore
    <R extends Tq4.Ci, E extends Tq4.Ai> void a(R r, Tq4.Di<R, E> di);

    @DexIgnore
    <R extends Tq4.Ci, E extends Tq4.Ai> void b(E e, Tq4.Di<R, E> di);

    @DexIgnore
    void execute(Runnable runnable);
}
