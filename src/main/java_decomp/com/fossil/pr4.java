package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pr4 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f2860a;
    @DexIgnore
    public /* final */ nr4 b;
    @DexIgnore
    public /* final */ or4 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.app_setting.flag.data.FlagRepository$fetchFlags$2", f = "FlagRepository.kt", l = {24}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends rr4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $agent;
        @DexIgnore
        public /* final */ /* synthetic */ String[] $flags;
        @DexIgnore
        public /* final */ /* synthetic */ String $serialNumber;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pr4 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.pr4$a$a")
        /* renamed from: com.fossil.pr4$a$a  reason: collision with other inner class name */
        public static final class C0193a extends TypeToken<List<? extends rr4>> {
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(pr4 pr4, String str, String str2, String[] strArr, qn7 qn7) {
            super(2, qn7);
            this.this$0 = pr4;
            this.$serialNumber = str;
            this.$agent = str2;
            this.$flags = strArr;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$serialNumber, this.$agent, this.$flags, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends rr4>>> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object b;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                or4 or4 = this.this$0.c;
                String str = this.$serialNumber;
                String str2 = this.$agent;
                String[] strArr = this.$flags;
                this.L$0 = iv7;
                this.label = 1;
                b = or4.b(str, str2, strArr, this);
                if (b == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                b = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            iq5 iq5 = (iq5) b;
            if (iq5 instanceof kq5) {
                gj4 gj4 = (gj4) ((kq5) iq5).a();
                if (gj4 == null) {
                    return new kz4(new ServerError(600, "success with empty result"));
                }
                try {
                    bj4 q = gj4.q("flags");
                    Gson d2 = new zi4().d();
                    pq7.b(d2, "GsonBuilder().create()");
                    List<rr4> list = (List) d2.h(q, new C0193a().getType());
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str3 = this.this$0.f2860a;
                    local.e(str3, "result: " + list);
                    nr4 nr4 = this.this$0.b;
                    pq7.b(list, "flagItems");
                    nr4.b(list);
                    return new kz4(list, null, 2, null);
                } catch (Exception e) {
                    e.printStackTrace();
                    return new kz4(new ServerError(600, "success with empty result"));
                }
            } else if (iq5 instanceof hq5) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str4 = this.this$0.f2860a;
                StringBuilder sb = new StringBuilder();
                sb.append("fetchFlags - failed - ");
                hq5 hq5 = (hq5) iq5;
                sb.append(hq5.a());
                local2.e(str4, sb.toString());
                int a2 = hq5.a();
                ServerError c = hq5.c();
                return new kz4(new ServerError(a2, c != null ? c.getMessage() : null));
            } else {
                throw new al7();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.app_setting.flag.data.FlagRepository$isBcOn$2", f = "FlagRepository.kt", l = {}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(pr4 pr4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = pr4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            boolean z;
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                rr4 f = this.this$0.f(kr4.BUDDY_CHALLENGE.getStrType());
                if (f != null) {
                    String b = f.b();
                    if (b == null) {
                        b = "";
                    }
                    if (!wt7.v(b, "on", false, 2, null)) {
                        z = false;
                        return ao7.a(z);
                    }
                }
                z = true;
                return ao7.a(z);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public pr4(nr4 nr4, or4 or4) {
        pq7.c(nr4, "local");
        pq7.c(or4, "remote");
        this.b = nr4;
        this.c = or4;
        String simpleName = pr4.class.getSimpleName();
        pq7.b(simpleName, "FlagRepository::class.java.simpleName");
        this.f2860a = simpleName;
    }

    @DexIgnore
    public final Object e(String str, String str2, String[] strArr, qn7<? super kz4<List<rr4>>> qn7) {
        return eu7.g(bw7.b(), new a(this, str, str2, strArr, null), qn7);
    }

    @DexIgnore
    public final rr4 f(String str) {
        return this.b.a(str);
    }

    @DexIgnore
    public final Object g(qn7<? super Boolean> qn7) {
        return eu7.g(bw7.b(), new b(this, null), qn7);
    }
}
