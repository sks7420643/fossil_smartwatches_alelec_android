package com.fossil;

import androidx.lifecycle.LiveData;
import com.mapped.V3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ss0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Ls0<X> {
        @DexIgnore
        public /* final */ /* synthetic */ Js0 a;
        @DexIgnore
        public /* final */ /* synthetic */ V3 b;

        @DexIgnore
        public Ai(Js0 js0, V3 v3) {
            this.a = js0;
            this.b = v3;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public void onChanged(X x) {
            this.a.o(this.b.apply(x));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Ls0<X> {
        @DexIgnore
        public LiveData<Y> a;
        @DexIgnore
        public /* final */ /* synthetic */ V3 b;
        @DexIgnore
        public /* final */ /* synthetic */ Js0 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Ls0<Y> {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            @Override // com.fossil.Ls0
            public void onChanged(Y y) {
                Bi.this.c.o(y);
            }
        }

        @DexIgnore
        public Bi(V3 v3, Js0 js0) {
            this.b = v3;
            this.c = js0;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public void onChanged(X x) {
            LiveData<Y> liveData = (LiveData) this.b.apply(x);
            LiveData<Y> liveData2 = this.a;
            if (liveData2 != liveData) {
                if (liveData2 != null) {
                    this.c.q(liveData2);
                }
                this.a = liveData;
                if (liveData != null) {
                    this.c.p(liveData, new Aii());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Ls0<X> {
        @DexIgnore
        public boolean a; // = true;
        @DexIgnore
        public /* final */ /* synthetic */ Js0 b;

        @DexIgnore
        public Ci(Js0 js0) {
            this.b = js0;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public void onChanged(X x) {
            Object e = this.b.e();
            if (this.a || ((e == null && x != null) || (e != null && !e.equals(x)))) {
                this.a = false;
                this.b.o(x);
            }
        }
    }

    @DexIgnore
    public static <X> LiveData<X> a(LiveData<X> liveData) {
        Js0 js0 = new Js0();
        js0.p(liveData, new Ci(js0));
        return js0;
    }

    @DexIgnore
    public static <X, Y> LiveData<Y> b(LiveData<X> liveData, V3<X, Y> v3) {
        Js0 js0 = new Js0();
        js0.p(liveData, new Ai(js0, v3));
        return js0;
    }

    @DexIgnore
    public static <X, Y> LiveData<Y> c(LiveData<X> liveData, V3<X, LiveData<Y>> v3) {
        Js0 js0 = new Js0();
        js0.p(liveData, new Bi(v3, js0));
        return js0;
    }
}
