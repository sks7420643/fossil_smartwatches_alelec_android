package com.fossil;

import android.view.View;
import com.mapped.Cd6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nl5 {
    @DexIgnore
    public static final void a(View view, View.OnClickListener onClickListener) {
        Wg6.c(view, "$this$setOnSingleClickListener");
        if (onClickListener != null) {
            view.setOnClickListener(new El5(onClickListener));
            if (onClickListener != null) {
                return;
            }
        }
        view.setOnClickListener(null);
        Cd6 cd6 = Cd6.a;
    }
}
