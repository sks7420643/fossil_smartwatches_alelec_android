package com.fossil;

import android.animation.TypeEvaluator;
import android.graphics.drawable.Drawable;
import android.util.Property;
import com.fossil.Tx3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ux3 extends Tx3.Ai {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements TypeEvaluator<Ei> {
        @DexIgnore
        public static /* final */ TypeEvaluator<Ei> b; // = new Bi();
        @DexIgnore
        public /* final */ Ei a; // = new Ei();

        @DexIgnore
        public Ei a(float f, Ei ei, Ei ei2) {
            this.a.b(Lz3.d(ei.a, ei2.a, f), Lz3.d(ei.b, ei2.b, f), Lz3.d(ei.c, ei2.c, f));
            return this.a;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [float, java.lang.Object, java.lang.Object] */
        @Override // android.animation.TypeEvaluator
        public /* bridge */ /* synthetic */ Ei evaluate(float f, Ei ei, Ei ei2) {
            return a(f, ei, ei2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci extends Property<Ux3, Ei> {
        @DexIgnore
        public static /* final */ Property<Ux3, Ei> a; // = new Ci("circularReveal");

        @DexIgnore
        public Ci(String str) {
            super(Ei.class, str);
        }

        @DexIgnore
        public Ei a(Ux3 ux3) {
            return ux3.getRevealInfo();
        }

        @DexIgnore
        public void b(Ux3 ux3, Ei ei) {
            ux3.setRevealInfo(ei);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // android.util.Property
        public /* bridge */ /* synthetic */ Ei get(Ux3 ux3) {
            return a(ux3);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // android.util.Property
        public /* bridge */ /* synthetic */ void set(Ux3 ux3, Ei ei) {
            b(ux3, ei);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di extends Property<Ux3, Integer> {
        @DexIgnore
        public static /* final */ Property<Ux3, Integer> a; // = new Di("circularRevealScrimColor");

        @DexIgnore
        public Di(String str) {
            super(Integer.class, str);
        }

        @DexIgnore
        public Integer a(Ux3 ux3) {
            return Integer.valueOf(ux3.getCircularRevealScrimColor());
        }

        @DexIgnore
        public void b(Ux3 ux3, Integer num) {
            ux3.setCircularRevealScrimColor(num.intValue());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // android.util.Property
        public /* bridge */ /* synthetic */ Integer get(Ux3 ux3) {
            return a(ux3);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // android.util.Property
        public /* bridge */ /* synthetic */ void set(Ux3 ux3, Integer num) {
            b(ux3, num);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei {
        @DexIgnore
        public float a;
        @DexIgnore
        public float b;
        @DexIgnore
        public float c;

        @DexIgnore
        public Ei() {
        }

        @DexIgnore
        public Ei(float f, float f2, float f3) {
            this.a = f;
            this.b = f2;
            this.c = f3;
        }

        @DexIgnore
        public Ei(Ei ei) {
            this(ei.a, ei.b, ei.c);
        }

        @DexIgnore
        public boolean a() {
            return this.c == Float.MAX_VALUE;
        }

        @DexIgnore
        public void b(float f, float f2, float f3) {
            this.a = f;
            this.b = f2;
            this.c = f3;
        }

        @DexIgnore
        public void c(Ei ei) {
            b(ei.a, ei.b, ei.c);
        }
    }

    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration

    @DexIgnore
    int getCircularRevealScrimColor();

    @DexIgnore
    Ei getRevealInfo();

    @DexIgnore
    void setCircularRevealOverlayDrawable(Drawable drawable);

    @DexIgnore
    void setCircularRevealScrimColor(int i);

    @DexIgnore
    void setRevealInfo(Ei ei);
}
