package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kh extends Lp {
    @DexIgnore
    public /* final */ ArrayList<Ow> C; // = By1.a(this.i, Hm7.c(Ow.c));

    @DexIgnore
    public Kh(K5 k5, I60 i60, String str) {
        super(k5, i60, Yp.K, str, false, 16);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        Lp.i(this, new Zu(this.w), Vf.b, Jg.b, null, new Xg(this), null, 40, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.C;
    }
}
