package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P54 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements R54<List<String>> {
        @DexIgnore
        public /* final */ List<String> a; // = T34.g();

        @DexIgnore
        @Override // com.fossil.R54
        public boolean a(String str) {
            this.a.add(str);
            return true;
        }

        @DexIgnore
        public List<String> b() {
            return this.a;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // com.fossil.R54
        public /* bridge */ /* synthetic */ List<String> getResult() {
            return b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends K54 {
        @DexIgnore
        public /* final */ File a;

        @DexIgnore
        public Bi(File file) {
            I14.l(file);
            this.a = file;
        }

        @DexIgnore
        public /* synthetic */ Bi(File file, Ai ai) {
            this(file);
        }

        @DexIgnore
        @Override // com.fossil.K54
        public /* bridge */ /* synthetic */ InputStream b() throws IOException {
            return c();
        }

        @DexIgnore
        public FileInputStream c() throws IOException {
            return new FileInputStream(this.a);
        }

        @DexIgnore
        public String toString() {
            return "Files.asByteSource(" + this.a + ")";
        }
    }

    @DexIgnore
    public static K54 a(File file) {
        return new Bi(file, null);
    }

    @DexIgnore
    public static L54 b(File file, Charset charset) {
        return a(file).a(charset);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> T c(File file, Charset charset, R54<T> r54) throws IOException {
        return (T) b(file, charset).b(r54);
    }

    @DexIgnore
    public static List<String> d(File file, Charset charset) throws IOException {
        return (List) c(file, charset, new Ai());
    }
}
