package com.fossil;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xt4 {
    @DexIgnore
    public /* final */ Ys4 a;

    @DexIgnore
    public Xt4(Ys4 ys4) {
        Wg6.c(ys4, "dao");
        this.a = ys4;
    }

    @DexIgnore
    public final void a() {
        this.a.a();
    }

    @DexIgnore
    public final int b() {
        return this.a.e();
    }

    @DexIgnore
    public final int c(String str) {
        Wg6.c(str, "profileId");
        return this.a.b(str);
    }

    @DexIgnore
    public final void d() {
        this.a.j();
    }

    @DexIgnore
    public final void e() {
        this.a.g();
    }

    @DexIgnore
    public final void f() {
        this.a.h();
    }

    @DexIgnore
    public final int g(String[] strArr) {
        Wg6.c(strArr, "ids");
        return this.a.m(strArr);
    }

    @DexIgnore
    public final Xs4 h(String str) {
        Wg6.c(str, "id");
        return this.a.p(str);
    }

    @DexIgnore
    public final List<Xs4> i() {
        return this.a.n();
    }

    @DexIgnore
    public final List<Xs4> j() {
        return this.a.k();
    }

    @DexIgnore
    public final List<Xs4> k() {
        return this.a.l();
    }

    @DexIgnore
    public final LiveData<List<Xs4>> l() {
        return this.a.c();
    }

    @DexIgnore
    public final List<Xs4> m() {
        return this.a.i();
    }

    @DexIgnore
    public final List<Xs4> n() {
        return this.a.f();
    }

    @DexIgnore
    public final List<Xs4> o() {
        return this.a.d();
    }

    @DexIgnore
    public final long p(Xs4 xs4) {
        Wg6.c(xs4, "friend");
        return this.a.o(xs4);
    }

    @DexIgnore
    public final Long[] q(List<Xs4> list) {
        Wg6.c(list, NativeProtocol.AUDIENCE_FRIENDS);
        return this.a.insert(list);
    }
}
