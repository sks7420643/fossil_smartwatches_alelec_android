package com.fossil;

import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.M62;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ea2 {
    @DexIgnore
    public static /* final */ Status d; // = new Status(8, "The connection to Google Play services was lost");
    @DexIgnore
    public static /* final */ BasePendingResult<?>[] e; // = new BasePendingResult[0];
    @DexIgnore
    public /* final */ Set<BasePendingResult<?>> a; // = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
    @DexIgnore
    public /* final */ Ia2 b; // = new Ha2(this);
    @DexIgnore
    public /* final */ Map<M62.Ci<?>, M62.Fi> c;

    @DexIgnore
    public Ea2(Map<M62.Ci<?>, M62.Fi> map) {
        this.c = map;
    }

    @DexIgnore
    public final void a() {
        BasePendingResult[] basePendingResultArr = (BasePendingResult[]) this.a.toArray(e);
        for (BasePendingResult basePendingResult : basePendingResultArr) {
            basePendingResult.n(null);
            if (basePendingResult.r() != null) {
                basePendingResult.d(null);
                IBinder w = this.c.get(((I72) basePendingResult).w()).w();
                if (basePendingResult.i()) {
                    basePendingResult.n(new Ga2(basePendingResult, null, w, null));
                } else if (w == null || !w.isBinderAlive()) {
                    basePendingResult.n(null);
                    basePendingResult.e();
                    basePendingResult.r().intValue();
                    throw new NullPointerException();
                } else {
                    Ga2 ga2 = new Ga2(basePendingResult, null, w, null);
                    basePendingResult.n(ga2);
                    try {
                        w.linkToDeath(ga2, 0);
                    } catch (RemoteException e2) {
                        basePendingResult.e();
                        basePendingResult.r().intValue();
                        throw new NullPointerException();
                    }
                }
                this.a.remove(basePendingResult);
            } else if (basePendingResult.s()) {
                this.a.remove(basePendingResult);
            }
        }
    }

    @DexIgnore
    public final void b(BasePendingResult<? extends Z62> basePendingResult) {
        this.a.add(basePendingResult);
        basePendingResult.n(this.b);
    }

    @DexIgnore
    public final void c() {
        for (BasePendingResult basePendingResult : (BasePendingResult[]) this.a.toArray(e)) {
            basePendingResult.q(d);
        }
    }
}
