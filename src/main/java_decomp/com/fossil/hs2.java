package com.fossil;

import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.Rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hs2 extends As2 implements Fs2 {
    @DexIgnore
    public Hs2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
    }

    @DexIgnore
    @Override // com.fossil.Fs2
    public final Rg2 O1(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        Parcel e = e(5, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Fs2
    public final Rg2 g() throws RemoteException {
        Parcel e = e(4, d());
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Fs2
    public final Rg2 zza(Bitmap bitmap) throws RemoteException {
        Parcel d = d();
        Es2.d(d, bitmap);
        Parcel e = e(6, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Fs2
    public final Rg2 zza(String str) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        Parcel e = e(2, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }
}
