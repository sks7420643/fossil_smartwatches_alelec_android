package com.fossil;

import com.mapped.Wg6;
import java.util.regex.Pattern;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M extends Wk1 {
    @DexIgnore
    public Pattern b;

    @DexIgnore
    public M(String str) throws IllegalArgumentException {
        Pattern compile = Pattern.compile(str);
        Wg6.b(compile, "Pattern.compile(serialNumberRegex)");
        this.b = compile;
    }

    @DexIgnore
    @Override // com.fossil.Wk1
    public boolean a(E60 e60) {
        return this.b.matcher(e60.u.getSerialNumber()).matches();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(new JSONObject(), Jd0.H1, "serial_number_pattern"), Jd0.K1, this.b.toString());
    }
}
