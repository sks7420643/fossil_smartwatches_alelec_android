package com.fossil;

import java.security.MessageDigest;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ad1 implements Mb1 {
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ Class<?> e;
    @DexIgnore
    public /* final */ Class<?> f;
    @DexIgnore
    public /* final */ Mb1 g;
    @DexIgnore
    public /* final */ Map<Class<?>, Sb1<?>> h;
    @DexIgnore
    public /* final */ Ob1 i;
    @DexIgnore
    public int j;

    @DexIgnore
    public Ad1(Object obj, Mb1 mb1, int i2, int i3, Map<Class<?>, Sb1<?>> map, Class<?> cls, Class<?> cls2, Ob1 ob1) {
        Ik1.d(obj);
        this.b = obj;
        Ik1.e(mb1, "Signature must not be null");
        this.g = mb1;
        this.c = i2;
        this.d = i3;
        Ik1.d(map);
        this.h = map;
        Ik1.e(cls, "Resource class must not be null");
        this.e = cls;
        Ik1.e(cls2, "Transcode class must not be null");
        this.f = cls2;
        Ik1.d(ob1);
        this.i = ob1;
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public void a(MessageDigest messageDigest) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public boolean equals(Object obj) {
        if (!(obj instanceof Ad1)) {
            return false;
        }
        Ad1 ad1 = (Ad1) obj;
        return this.b.equals(ad1.b) && this.g.equals(ad1.g) && this.d == ad1.d && this.c == ad1.c && this.h.equals(ad1.h) && this.e.equals(ad1.e) && this.f.equals(ad1.f) && this.i.equals(ad1.i);
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public int hashCode() {
        if (this.j == 0) {
            int hashCode = this.b.hashCode();
            this.j = hashCode;
            int hashCode2 = (hashCode * 31) + this.g.hashCode();
            this.j = hashCode2;
            int i2 = (hashCode2 * 31) + this.c;
            this.j = i2;
            int i3 = (i2 * 31) + this.d;
            this.j = i3;
            int hashCode3 = (i3 * 31) + this.h.hashCode();
            this.j = hashCode3;
            int hashCode4 = (hashCode3 * 31) + this.e.hashCode();
            this.j = hashCode4;
            int hashCode5 = (hashCode4 * 31) + this.f.hashCode();
            this.j = hashCode5;
            this.j = (hashCode5 * 31) + this.i.hashCode();
        }
        return this.j;
    }

    @DexIgnore
    public String toString() {
        return "EngineKey{model=" + this.b + ", width=" + this.c + ", height=" + this.d + ", resourceClass=" + this.e + ", transcodeClass=" + this.f + ", signature=" + this.g + ", hashCode=" + this.j + ", transformations=" + this.h + ", options=" + this.i + '}';
    }
}
