package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fm0 extends Drawable.ConstantState {
    @DexIgnore
    public int a;
    @DexIgnore
    public Drawable.ConstantState b;
    @DexIgnore
    public ColorStateList c; // = null;
    @DexIgnore
    public PorterDuff.Mode d; // = Dm0.h;

    @DexIgnore
    public Fm0(Fm0 fm0) {
        if (fm0 != null) {
            this.a = fm0.a;
            this.b = fm0.b;
            this.c = fm0.c;
            this.d = fm0.d;
        }
    }

    @DexIgnore
    public boolean a() {
        return this.b != null;
    }

    @DexIgnore
    public int getChangingConfigurations() {
        int i = this.a;
        Drawable.ConstantState constantState = this.b;
        return (constantState != null ? constantState.getChangingConfigurations() : 0) | i;
    }

    @DexIgnore
    public Drawable newDrawable() {
        return newDrawable(null);
    }

    @DexIgnore
    public Drawable newDrawable(Resources resources) {
        return Build.VERSION.SDK_INT >= 21 ? new Em0(this, resources) : new Dm0(this, resources);
    }
}
