package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.view.FlexibleCheckBox;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rw5 extends RecyclerView.g<Ai> {
    @DexIgnore
    public List<WeatherLocationWrapper> a; // = new ArrayList();
    @DexIgnore
    public Bi b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public FlexibleCheckBox b;
        @DexIgnore
        public View c;
        @DexIgnore
        public /* final */ /* synthetic */ Rw5 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Aii(Ai ai) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                Bi bi;
                if (this.b.d.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && !((WeatherLocationWrapper) this.b.d.a.get(this.b.getAdapterPosition())).isUseCurrentLocation() && (bi = this.b.d.b) != null) {
                    bi.a(this.b.getAdapterPosition() - 1, !((WeatherLocationWrapper) this.b.d.a.get(this.b.getAdapterPosition())).isEnableLocation());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Rw5 rw5, View view) {
            super(view);
            Wg6.c(view, "view");
            this.d = rw5;
            View findViewById = view.findViewById(2131362056);
            if (findViewById != null) {
                this.c = findViewById;
                View findViewById2 = view.findViewById(2131362005);
                if (findViewById2 != null) {
                    this.b = (FlexibleCheckBox) findViewById2;
                    View findViewById3 = view.findViewById(2131363408);
                    if (findViewById3 != null) {
                        this.a = (TextView) findViewById3;
                        View view2 = this.c;
                        if (view2 != null) {
                            view2.setOnClickListener(new Aii(this));
                            return;
                        }
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        public final void a(WeatherLocationWrapper weatherLocationWrapper) {
            FlexibleCheckBox flexibleCheckBox;
            Wg6.c(weatherLocationWrapper, "locationWrapper");
            TextView textView = this.a;
            if (textView != null) {
                textView.setText(weatherLocationWrapper.getFullName());
            }
            FlexibleCheckBox flexibleCheckBox2 = this.b;
            if (flexibleCheckBox2 != null) {
                flexibleCheckBox2.setChecked(weatherLocationWrapper.isEnableLocation());
            }
            FlexibleCheckBox flexibleCheckBox3 = this.b;
            if (flexibleCheckBox3 != null) {
                flexibleCheckBox3.setEnabled(!weatherLocationWrapper.isUseCurrentLocation());
            }
            TextView textView2 = this.a;
            if (Wg6.a(textView2 != null ? textView2.getText() : null, Um5.c(PortfolioApp.get.instance(), 2131886395)) && (flexibleCheckBox = this.b) != null) {
                flexibleCheckBox.setEnableColor("nonBrandDisableCalendarDay");
            }
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(int i, boolean z);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public void i(Ai ai, int i) {
        Wg6.c(ai, "holder");
        if (getItemCount() > i && i != -1) {
            ai.a(this.a.get(i));
        }
    }

    @DexIgnore
    public Ai j(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558727, viewGroup, false);
        Wg6.b(inflate, "view");
        return new Ai(this, inflate);
    }

    @DexIgnore
    public final void k(List<WeatherLocationWrapper> list) {
        Wg6.c(list, "locationSearchList");
        this.a.clear();
        String c = Um5.c(PortfolioApp.get.instance(), 2131886395);
        Wg6.b(c, "LanguageHelper.getString\u2026on_Text__CurrentLocation)");
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886395);
        Wg6.b(c2, "LanguageHelper.getString\u2026on_Text__CurrentLocation)");
        this.a.add(0, new WeatherLocationWrapper("", 0.0d, 0.0d, c, c2, true, true, 6, null));
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void l(Bi bi) {
        Wg6.c(bi, "listener");
        this.b = bi;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        i(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return j(viewGroup, i);
    }
}
