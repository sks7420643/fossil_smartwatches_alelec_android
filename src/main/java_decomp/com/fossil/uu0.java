package com.fossil;

import com.mapped.Zf;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uu0<T> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ Zf.Di<T> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<T> {
        @DexIgnore
        public static /* final */ Object d; // = new Object();
        @DexIgnore
        public static Executor e;
        @DexIgnore
        public Executor a;
        @DexIgnore
        public Executor b;
        @DexIgnore
        public /* final */ Zf.Di<T> c;

        @DexIgnore
        public Ai(Zf.Di<T> di) {
            this.c = di;
        }

        @DexIgnore
        public Uu0<T> a() {
            if (this.b == null) {
                synchronized (d) {
                    if (e == null) {
                        e = Executors.newFixedThreadPool(2);
                    }
                }
                this.b = e;
            }
            return new Uu0<>(this.a, this.b, this.c);
        }
    }

    @DexIgnore
    public Uu0(Executor executor, Executor executor2, Zf.Di<T> di) {
        this.a = executor;
        this.b = executor2;
        this.c = di;
    }

    @DexIgnore
    public Executor a() {
        return this.b;
    }

    @DexIgnore
    public Zf.Di<T> b() {
        return this.c;
    }

    @DexIgnore
    public Executor c() {
        return this.a;
    }
}
