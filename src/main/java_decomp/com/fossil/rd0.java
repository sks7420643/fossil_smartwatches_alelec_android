package com.fossil;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Rd0 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai extends Binder implements Rd0 {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Aii implements Rd0 {
            @DexIgnore
            public IBinder b;

            @DexIgnore
            public Aii(IBinder iBinder) {
                this.b = iBinder;
            }

            @DexIgnore
            @Override // com.fossil.Rd0
            public void A2(String str, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsCallback");
                    obtain.writeString(str);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.b.transact(5, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            @Override // com.fossil.Rd0
            public void G2(Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.customtabs.ICustomTabsCallback");
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.b.transact(4, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            @DexIgnore
            public IBinder asBinder() {
                return this.b;
            }
        }

        @DexIgnore
        public static Rd0 d(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("android.support.customtabs.ICustomTabsCallback");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof Rd0)) ? new Aii(iBinder) : (Rd0) queryLocalInterface;
        }
    }

    @DexIgnore
    void A2(String str, Bundle bundle) throws RemoteException;

    @DexIgnore
    void G2(Bundle bundle) throws RemoteException;
}
