package com.fossil;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.appfilter.AppFilterProviderImpl;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ep5 extends AppFilterProviderImpl {
    @DexIgnore
    public static /* final */ String a; // = "ep5";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements UpgradeCommand {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
        public void execute(SQLiteDatabase sQLiteDatabase) {
            FLogger.INSTANCE.getLocal().d(Ep5.a, " ---- UPGRADE DB APPFILTER, table APPFILTER");
            sQLiteDatabase.execSQL("ALTER TABLE appfilter ADD COLUMN deviceFamily int");
            FLogger.INSTANCE.getLocal().d(Ep5.a, " ---- UPGRADE DB APPFILTER, table APPFILTER SUCCESS");
            StringBuilder sb = new StringBuilder();
            String J = PortfolioApp.d0.J();
            if (!TextUtils.isEmpty(J)) {
                sb.append("UPDATE ");
                sb.append("appfilter");
                sb.append(" SET deviceFamily = ");
                sb.append(DeviceIdentityUtils.getDeviceFamily(J).ordinal());
                sQLiteDatabase.execSQL(sb.toString());
                return;
            }
            Ep5.this.removeAllAppFilters();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements UpgradeCommand {
        @DexIgnore
        public Bi(Ep5 ep5) {
        }

        @DexIgnore
        @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
        public void execute(SQLiteDatabase sQLiteDatabase) {
            try {
                FLogger.INSTANCE.getLocal().d(Ep5.a, "Inside .doInBackground upgrade appfilter");
                Cursor query = sQLiteDatabase.query(true, "appfilter", new String[]{"id", "type", BaseFeatureModel.COLUMN_COLOR, "name", BaseFeatureModel.COLUMN_HAPTIC, "timestamp", "enabled", "deviceFamily"}, null, null, null, null, null, null);
                ArrayList<AppFilter> arrayList = new ArrayList();
                int i = -1;
                String J = PortfolioApp.d0.J();
                if (!TextUtils.isEmpty(J)) {
                    i = DeviceHelper.o.e(J).getValue();
                }
                String str = "deviceFamily";
                String str2 = "enabled";
                String str3 = "name";
                String str4 = "type";
                if (query == null || i <= 0 || !DeviceHelper.o.y(J)) {
                    str = "deviceFamily";
                    str2 = "enabled";
                    str3 = "name";
                    str4 = "type";
                } else {
                    query.moveToFirst();
                    while (!query.isAfterLast()) {
                        String string = query.getString(query.getColumnIndex(str4));
                        String string2 = query.getString(query.getColumnIndex(BaseFeatureModel.COLUMN_COLOR));
                        String string3 = query.getString(query.getColumnIndex(str3));
                        String string4 = query.getString(query.getColumnIndex(BaseFeatureModel.COLUMN_HAPTIC));
                        int i2 = query.getInt(query.getColumnIndex("timestamp"));
                        int i3 = query.getInt(query.getColumnIndex(str2));
                        int i4 = query.getInt(query.getColumnIndex(str));
                        int i5 = query.getInt(query.getColumnIndex("id"));
                        if (i4 == i) {
                            AppFilter appFilter = new AppFilter();
                            appFilter.setType(string);
                            appFilter.setColor(string2);
                            appFilter.setName(string3);
                            appFilter.setHaptic(string4);
                            appFilter.setTimestamp((long) i2);
                            boolean z = true;
                            if (i3 != 1) {
                                z = false;
                            }
                            appFilter.setEnabled(z);
                            appFilter.setDeviceFamily(i4);
                            appFilter.setDbRowId(i5);
                            Gp5 d = Mn5.p.a().h().d(string, MFDeviceFamily.fromInt(appFilter.getDeviceFamily()).toString());
                            if (d != null) {
                                appFilter.setHour(d.d());
                                appFilter.setVibrationOnly(d.f());
                            }
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String str5 = Ep5.a;
                            local.d(str5, "Add appfiler=" + appFilter);
                            if (!appFilter.isVibrationOnly()) {
                                arrayList.add(appFilter);
                            }
                        } else {
                            FLogger.INSTANCE.getLocal().d(Ep5.a, "Skip this app filter");
                        }
                        query.moveToNext();
                    }
                    query.close();
                }
                sQLiteDatabase.execSQL("CREATE TABLE appfilter_copy (id INTEGER PRIMARY KEY AUTOINCREMENT, type VARCHAR, color VARCHAR, haptic VARCHAR, timestamp BIGINT, enabled INTEGER, name VARCHAR, isVibrationOnly INTEGER, deviceFamily INTEGER, hour INTEGER);");
                if (!arrayList.isEmpty()) {
                    for (AppFilter appFilter2 : arrayList) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(BaseFeatureModel.COLUMN_COLOR, appFilter2.getColor());
                        contentValues.put(BaseFeatureModel.COLUMN_HAPTIC, appFilter2.getHaptic());
                        contentValues.put("timestamp", Long.valueOf(appFilter2.getTimestamp()));
                        contentValues.put(str3, appFilter2.getName());
                        contentValues.put(str2, Boolean.valueOf(appFilter2.isEnabled()));
                        contentValues.put(str4, appFilter2.getType());
                        contentValues.put("id", Integer.valueOf(appFilter2.getDbRowId()));
                        contentValues.put(AppFilter.COLUMN_IS_VIBRATION_ONLY, Boolean.valueOf(appFilter2.isVibrationOnly()));
                        contentValues.put(str, Integer.valueOf(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()));
                        contentValues.put(AppFilter.COLUMN_HOUR, Integer.valueOf(appFilter2.getHour()));
                        sQLiteDatabase.insert("appfilter_copy", null, contentValues);
                    }
                }
                sQLiteDatabase.execSQL("DROP TABLE appfilter;");
                sQLiteDatabase.execSQL("ALTER TABLE appfilter_copy RENAME TO appfilter;");
                FLogger.INSTANCE.getLocal().d(Ep5.a, "Migration complete");
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str6 = Ep5.a;
                local2.e(str6, "Error inside " + Ep5.a + ".upgrade - e=" + e);
            }
        }
    }

    @DexIgnore
    public Ep5(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider, com.fossil.wearables.fsl.appfilter.AppFilterProviderImpl, com.fossil.wearables.fsl.appfilter.AppFilterProvider
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.appfilter.AppFilterProviderImpl, com.fossil.wearables.fsl.shared.BaseDbProvider
    @SuppressLint({"UseSparseArrays"})
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        Map<Integer, UpgradeCommand> dbUpgrades = super.getDbUpgrades();
        if (dbUpgrades == null) {
            dbUpgrades = new HashMap<>();
        }
        dbUpgrades.put(2, new Ai());
        dbUpgrades.put(4, new Bi(this));
        return dbUpgrades;
    }
}
