package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewWeekPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xf6 implements MembersInjector<ActivityOverviewWeekPresenter> {
    @DexIgnore
    public static void a(ActivityOverviewWeekPresenter activityOverviewWeekPresenter) {
        activityOverviewWeekPresenter.y();
    }
}
