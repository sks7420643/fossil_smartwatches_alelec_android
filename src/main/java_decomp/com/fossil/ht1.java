package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ht1 extends Tc0 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Xs1[] d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ht1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ht1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(Dq1.class.getClassLoader());
            if (readParcelable != null) {
                return new Ht1((Dq1) readParcelable, (Nt1) parcel.readParcelable(Nt1.class.getClassLoader()), (Xs1[]) parcel.createTypedArray(Xs1.CREATOR));
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ht1[] newArray(int i) {
            return new Ht1[i];
        }
    }

    @DexIgnore
    public Ht1(Dq1 dq1, Nt1 nt1, Xs1[] xs1Arr) {
        super(dq1, nt1);
        this.d = xs1Arr;
    }

    @DexIgnore
    public Ht1(Dq1 dq1, Xs1[] xs1Arr) {
        super(dq1, null);
        this.d = xs1Arr;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public JSONObject a() {
        Object obj;
        JSONObject a2 = super.a();
        Jd0 jd0 = Jd0.e5;
        Xs1[] xs1Arr = this.d;
        if (xs1Arr == null || (obj = Px1.a(xs1Arr)) == null) {
            obj = JSONObject.NULL;
        }
        return G80.k(a2, jd0, obj);
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public byte[] a(short s, Ry1 ry1) {
        X90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.d != null) {
                JSONArray jSONArray = new JSONArray();
                for (Xs1 xs1 : this.d) {
                    jSONArray.put(xs1.a());
                }
                jSONObject.put("buddyChallengeApp._.config.challenge_list", jSONArray);
            } else {
                getDeviceMessage();
            }
        } catch (JSONException e) {
            D90.i.i(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        Wg6.b(jSONObject4, "deviceResponseJSONObject.toString()");
        Charset c = Hd0.y.c();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(c);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public boolean equals(Object obj) {
        Xs1[] xs1Arr;
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Ht1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Ht1 ht1 = (Ht1) obj;
            Xs1[] xs1Arr2 = this.d;
            if (xs1Arr2 == null || (xs1Arr = ht1.d) == null) {
                if (this.d == null || ht1.d == null) {
                    return false;
                }
            } else if (!Arrays.equals(xs1Arr2, xs1Arr)) {
                return false;
            }
            return true;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.BuddyChallengeWatchAppListChallengesData");
    }

    @DexIgnore
    public final Xs1[] getBuddyChallenges() {
        return this.d;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public int hashCode() {
        int hashCode = super.hashCode();
        Xs1[] xs1Arr = this.d;
        return (xs1Arr != null ? xs1Arr.hashCode() : 0) + (hashCode * 31);
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.d, i);
        }
    }
}
