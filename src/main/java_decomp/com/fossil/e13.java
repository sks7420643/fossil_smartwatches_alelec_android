package com.fossil;

import com.fossil.E13;
import com.fossil.E13.Ai;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class E13<MessageType extends E13<MessageType, BuilderType>, BuilderType extends Ai<MessageType, BuilderType>> extends Nz2<MessageType, BuilderType> {
    @DexIgnore
    public static Map<Object, E13<?, ?>> zzd; // = new ConcurrentHashMap();
    @DexIgnore
    public W33 zzb; // = W33.a();
    @DexIgnore
    public int zzc; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<MessageType extends E13<MessageType, BuilderType>, BuilderType extends Ai<MessageType, BuilderType>> extends Mz2<MessageType, BuilderType> {
        @DexIgnore
        public /* final */ MessageType b;
        @DexIgnore
        public MessageType c;
        @DexIgnore
        public boolean d; // = false;

        @DexIgnore
        public Ai(MessageType messagetype) {
            this.b = messagetype;
            this.c = (MessageType) ((E13) messagetype.r(Fi.d, null, null));
        }

        @DexIgnore
        public static void s(MessageType messagetype, MessageType messagetype2) {
            B33.a().c(messagetype).zzb(messagetype, messagetype2);
        }

        @DexIgnore
        @Override // com.fossil.P23
        public /* synthetic */ M23 b() {
            return v();
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: com.fossil.E13$Ai */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // java.lang.Object
        public /* synthetic */ Object clone() throws CloneNotSupportedException {
            Ai ai = (Ai) this.b.r(Fi.e, null, null);
            ai.r((E13) b());
            return ai;
        }

        @DexIgnore
        @Override // com.fossil.O23
        public final /* synthetic */ M23 d() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.P23
        public /* synthetic */ M23 h() {
            return w();
        }

        @DexIgnore
        @Override // com.fossil.O23
        public final boolean j() {
            return E13.v(this.c, false);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.E13$Ai<MessageType extends com.fossil.E13<MessageType, BuilderType>, BuilderType extends com.fossil.E13$Ai<MessageType, BuilderType>> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.Mz2
        public final /* synthetic */ Mz2 o(Nz2 nz2) {
            r((E13) nz2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Mz2
        public final /* synthetic */ Mz2 p(byte[] bArr, int i, int i2) throws L13 {
            t(bArr, 0, i2, Q03.a());
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Mz2
        public final /* synthetic */ Mz2 q(byte[] bArr, int i, int i2, Q03 q03) throws L13 {
            t(bArr, 0, i2, q03);
            return this;
        }

        @DexIgnore
        public final BuilderType r(MessageType messagetype) {
            if (this.d) {
                u();
                this.d = false;
            }
            s(this.c, messagetype);
            return this;
        }

        @DexIgnore
        public final BuilderType t(byte[] bArr, int i, int i2, Q03 q03) throws L13 {
            if (this.d) {
                u();
                this.d = false;
            }
            try {
                B33.a().c(this.c).a(this.c, bArr, 0, i2 + 0, new Sz2(q03));
                return this;
            } catch (L13 e) {
                throw e;
            } catch (IndexOutOfBoundsException e2) {
                throw L13.zza();
            } catch (IOException e3) {
                throw new RuntimeException("Reading from byte array should not throw IOException.", e3);
            }
        }

        @DexIgnore
        public void u() {
            MessageType messagetype = (MessageType) ((E13) this.c.r(Fi.d, null, null));
            s(messagetype, this.c);
            this.c = messagetype;
        }

        @DexIgnore
        public MessageType v() {
            if (this.d) {
                return this.c;
            }
            MessageType messagetype = this.c;
            B33.a().c(messagetype).zzc(messagetype);
            this.d = true;
            return this.c;
        }

        @DexIgnore
        public final MessageType w() {
            MessageType messagetype = (MessageType) ((E13) b());
            if (messagetype.j()) {
                return messagetype;
            }
            throw new U33(messagetype);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Bi<MessageType extends Bi<MessageType, BuilderType>, BuilderType> extends E13<MessageType, BuilderType> implements O23 {
        @DexIgnore
        public T03<Ei> zzc; // = T03.c();

        @DexIgnore
        public final T03<Ei> C() {
            if (this.zzc.o()) {
                this.zzc = (T03) this.zzc.clone();
            }
            return this.zzc;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T extends E13<T, ?>> extends Oz2<T> {
        @DexIgnore
        public Ci(T t) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<ContainingType extends M23, Type> extends O03<ContainingType, Type> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements V03<Ei> {
        @DexIgnore
        @Override // java.lang.Comparable
        public final /* synthetic */ int compareTo(Object obj) {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.V03
        public final P23 d(P23 p23, M23 m23) {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.V03
        public final V23 g(V23 v23, V23 v232) {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.V03
        public final int zza() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.V03
        public final L43 zzb() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.V03
        public final S43 zzc() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.V03
        public final boolean zzd() {
            throw new NoSuchMethodError();
        }

        @DexIgnore
        @Override // com.fossil.V03
        public final boolean zze() {
            throw new NoSuchMethodError();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class Fi {
        @DexIgnore
        public static /* final */ int a; // = 1;
        @DexIgnore
        public static /* final */ int b; // = 2;
        @DexIgnore
        public static /* final */ int c; // = 3;
        @DexIgnore
        public static /* final */ int d; // = 4;
        @DexIgnore
        public static /* final */ int e; // = 5;
        @DexIgnore
        public static /* final */ int f; // = 6;
        @DexIgnore
        public static /* final */ int g; // = 7;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] h; // = {1, 2, 3, 4, 5, 6, 7};
        @DexIgnore
        public static /* final */ int i; // = 1;
        @DexIgnore
        public static /* final */ int j; // = 2;
        @DexIgnore
        public static /* final */ int k; // = 1;
        @DexIgnore
        public static /* final */ int l; // = 2;

        @DexIgnore
        public static int[] a() {
            return (int[]) h.clone();
        }
    }

    @DexIgnore
    public static <E> M13<E> B() {
        return A33.d();
    }

    @DexIgnore
    public static <T extends E13<?, ?>> T o(Class<T> cls) {
        E13<?, ?> e13 = zzd.get(cls);
        if (e13 == null) {
            try {
                Class.forName(cls.getName(), true, cls.getClassLoader());
                e13 = (T) zzd.get(cls);
            } catch (ClassNotFoundException e) {
                throw new IllegalStateException("Class initialization cannot fail.", e);
            }
        }
        if (e13 == null) {
            e13 = (T) ((E13) ((E13) E43.c(cls)).r(Fi.f, null, null));
            if (e13 != null) {
                zzd.put(cls, e13);
            } else {
                throw new IllegalStateException();
            }
        }
        return (T) e13;
    }

    @DexIgnore
    public static J13 p(J13 j13) {
        int size = j13.size();
        return j13.C(size == 0 ? 10 : size << 1);
    }

    @DexIgnore
    public static <E> M13<E> q(M13<E> m13) {
        int size = m13.size();
        return m13.zza(size == 0 ? 10 : size << 1);
    }

    @DexIgnore
    public static Object s(M23 m23, String str, Object[] objArr) {
        return new D33(m23, str, objArr);
    }

    @DexIgnore
    public static Object t(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    @DexIgnore
    public static <T extends E13<?, ?>> void u(Class<T> cls, T t) {
        zzd.put(cls, t);
    }

    @DexIgnore
    public static final <T extends E13<T, ?>> boolean v(T t, boolean z) {
        byte byteValue = ((Byte) t.r(Fi.a, null, null)).byteValue();
        if (byteValue == 1) {
            return true;
        }
        if (byteValue == 0) {
            return false;
        }
        boolean zzd2 = B33.a().c(t).zzd(t);
        if (z) {
            t.r(Fi.b, zzd2 ? t : null, null);
        }
        return zzd2;
    }

    @DexIgnore
    public static K13 y() {
        return F13.c();
    }

    @DexIgnore
    public static J13 z() {
        return Z13.c();
    }

    @DexIgnore
    @Override // com.fossil.O23
    public final /* synthetic */ M23 d() {
        return (E13) r(Fi.f, null, null);
    }

    @DexIgnore
    @Override // com.fossil.M23
    public final /* synthetic */ P23 e() {
        Ai ai = (Ai) r(Fi.e, null, null);
        ai.r(this);
        return ai;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return B33.a().c(this).zza(this, (E13) obj);
    }

    @DexIgnore
    @Override // com.fossil.Nz2
    public final int f() {
        return this.zzc;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.zza;
        if (i != 0) {
            return i;
        }
        int zza = B33.a().c(this).zza(this);
        this.zza = zza;
        return zza;
    }

    @DexIgnore
    @Override // com.fossil.M23
    public final /* synthetic */ P23 i() {
        return (Ai) r(Fi.e, null, null);
    }

    @DexIgnore
    @Override // com.fossil.O23
    public final boolean j() {
        return v(this, true);
    }

    @DexIgnore
    @Override // com.fossil.M23
    public final int l() {
        if (this.zzc == -1) {
            this.zzc = B33.a().c(this).zzb(this);
        }
        return this.zzc;
    }

    @DexIgnore
    @Override // com.fossil.Nz2
    public final void m(int i) {
        this.zzc = i;
    }

    @DexIgnore
    @Override // com.fossil.M23
    public final void n(L03 l03) throws IOException {
        B33.a().c(this).b(this, N03.g(l03));
    }

    @DexIgnore
    public abstract Object r(int i, Object obj, Object obj2);

    @DexIgnore
    public String toString() {
        return R23.a(this, super.toString());
    }

    @DexIgnore
    public final <MessageType extends E13<MessageType, BuilderType>, BuilderType extends Ai<MessageType, BuilderType>> BuilderType w() {
        return (BuilderType) ((Ai) r(Fi.e, null, null));
    }

    @DexIgnore
    public final BuilderType x() {
        BuilderType buildertype = (BuilderType) ((Ai) r(Fi.e, null, null));
        buildertype.r(this);
        return buildertype;
    }
}
