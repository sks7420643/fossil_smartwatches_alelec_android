package com.fossil;

import android.database.Cursor;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T31 implements S31 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Hh<R31> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hh<R31> {
        @DexIgnore
        public Ai(T31 t31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, R31 r31) {
            String str = r31.a;
            if (str == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, str);
            }
            String str2 = r31.b;
            if (str2 == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, str2);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, R31 r31) {
            a(mi, r31);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkTag` (`tag`,`work_spec_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public T31(Oh oh) {
        this.a = oh;
        this.b = new Ai(this, oh);
    }

    @DexIgnore
    @Override // com.fossil.S31
    public void a(R31 r31) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert((Hh<R31>) r31);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.S31
    public List<String> b(String str) {
        Rh f = Rh.f("SELECT DISTINCT tag FROM worktag WHERE work_spec_id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f, false, null);
        try {
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(b2.getString(0));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }
}
