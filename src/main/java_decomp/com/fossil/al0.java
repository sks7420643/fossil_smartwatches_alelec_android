package com.fossil;

import android.app.Notification;
import android.app.RemoteInput;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.SparseArray;
import android.widget.RemoteViews;
import androidx.core.graphics.drawable.IconCompat;
import com.fossil.Zk0;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Al0 implements Yk0 {
    @DexIgnore
    public /* final */ Notification.Builder a;
    @DexIgnore
    public /* final */ Zk0.Ei b;
    @DexIgnore
    public RemoteViews c;
    @DexIgnore
    public RemoteViews d;
    @DexIgnore
    public /* final */ List<Bundle> e; // = new ArrayList();
    @DexIgnore
    public /* final */ Bundle f; // = new Bundle();
    @DexIgnore
    public int g;
    @DexIgnore
    public RemoteViews h;

    @DexIgnore
    public Al0(Zk0.Ei ei) {
        ArrayList<String> arrayList;
        this.b = ei;
        if (Build.VERSION.SDK_INT >= 26) {
            this.a = new Notification.Builder(ei.a, ei.I);
        } else {
            this.a = new Notification.Builder(ei.a);
        }
        Notification notification = ei.P;
        this.a.setWhen(notification.when).setSmallIcon(notification.icon, notification.iconLevel).setContent(notification.contentView).setTicker(notification.tickerText, ei.h).setVibrate(notification.vibrate).setLights(notification.ledARGB, notification.ledOnMS, notification.ledOffMS).setOngoing((notification.flags & 2) != 0).setOnlyAlertOnce((notification.flags & 8) != 0).setAutoCancel((notification.flags & 16) != 0).setDefaults(notification.defaults).setContentTitle(ei.d).setContentText(ei.e).setContentInfo(ei.j).setContentIntent(ei.f).setDeleteIntent(notification.deleteIntent).setFullScreenIntent(ei.g, (notification.flags & 128) != 0).setLargeIcon(ei.i).setNumber(ei.k).setProgress(ei.r, ei.s, ei.t);
        if (Build.VERSION.SDK_INT < 21) {
            this.a.setSound(notification.sound, notification.audioStreamType);
        }
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setSubText(ei.p).setUsesChronometer(ei.n).setPriority(ei.l);
            Iterator<Zk0.Ai> it = ei.b.iterator();
            while (it.hasNext()) {
                b(it.next());
            }
            Bundle bundle = ei.B;
            if (bundle != null) {
                this.f.putAll(bundle);
            }
            if (Build.VERSION.SDK_INT < 20) {
                if (ei.x) {
                    this.f.putBoolean("android.support.localOnly", true);
                }
                String str = ei.u;
                if (str != null) {
                    this.f.putString("android.support.groupKey", str);
                    if (ei.v) {
                        this.f.putBoolean("android.support.isGroupSummary", true);
                    } else {
                        this.f.putBoolean("android.support.useSideChannel", true);
                    }
                }
                String str2 = ei.w;
                if (str2 != null) {
                    this.f.putString("android.support.sortKey", str2);
                }
            }
            this.c = ei.F;
            this.d = ei.G;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setShowWhen(ei.m);
            if (Build.VERSION.SDK_INT < 21 && (arrayList = ei.R) != null && !arrayList.isEmpty()) {
                Bundle bundle2 = this.f;
                ArrayList<String> arrayList2 = ei.R;
                bundle2.putStringArray("android.people", (String[]) arrayList2.toArray(new String[arrayList2.size()]));
            }
        }
        if (Build.VERSION.SDK_INT >= 20) {
            this.a.setLocalOnly(ei.x).setGroup(ei.u).setGroupSummary(ei.v).setSortKey(ei.w);
            this.g = ei.M;
        }
        if (Build.VERSION.SDK_INT >= 21) {
            this.a.setCategory(ei.A).setColor(ei.C).setVisibility(ei.D).setPublicVersion(ei.E).setSound(notification.sound, notification.audioAttributes);
            Iterator<String> it2 = ei.R.iterator();
            while (it2.hasNext()) {
                this.a.addPerson(it2.next());
            }
            this.h = ei.H;
            if (ei.c.size() > 0) {
                Bundle bundle3 = ei.d().getBundle("android.car.EXTENSIONS");
                Bundle bundle4 = bundle3 == null ? new Bundle() : bundle3;
                Bundle bundle5 = new Bundle();
                for (int i = 0; i < ei.c.size(); i++) {
                    bundle5.putBundle(Integer.toString(i), Bl0.b(ei.c.get(i)));
                }
                bundle4.putBundle("invisible_actions", bundle5);
                ei.d().putBundle("android.car.EXTENSIONS", bundle4);
                this.f.putBundle("android.car.EXTENSIONS", bundle4);
            }
        }
        if (Build.VERSION.SDK_INT >= 24) {
            this.a.setExtras(ei.B).setRemoteInputHistory(ei.q);
            RemoteViews remoteViews = ei.F;
            if (remoteViews != null) {
                this.a.setCustomContentView(remoteViews);
            }
            RemoteViews remoteViews2 = ei.G;
            if (remoteViews2 != null) {
                this.a.setCustomBigContentView(remoteViews2);
            }
            RemoteViews remoteViews3 = ei.H;
            if (remoteViews3 != null) {
                this.a.setCustomHeadsUpContentView(remoteViews3);
            }
        }
        if (Build.VERSION.SDK_INT >= 26) {
            this.a.setBadgeIconType(ei.J).setShortcutId(ei.K).setTimeoutAfter(ei.L).setGroupAlertBehavior(ei.M);
            if (ei.z) {
                this.a.setColorized(ei.y);
            }
            if (!TextUtils.isEmpty(ei.I)) {
                this.a.setSound(null).setDefaults(0).setLights(0, 0, 0).setVibrate(null);
            }
        }
        if (Build.VERSION.SDK_INT >= 29) {
            this.a.setAllowSystemGeneratedContextualActions(ei.N);
            this.a.setBubbleMetadata(Zk0.Di.h(ei.O));
        }
        if (ei.Q) {
            if (this.b.v) {
                this.g = 2;
            } else {
                this.g = 1;
            }
            this.a.setVibrate(null);
            this.a.setSound(null);
            int i2 = notification.defaults & -2;
            notification.defaults = i2;
            int i3 = i2 & -3;
            notification.defaults = i3;
            this.a.setDefaults(i3);
            if (Build.VERSION.SDK_INT >= 26) {
                if (TextUtils.isEmpty(this.b.u)) {
                    this.a.setGroup("silent");
                }
                this.a.setGroupAlertBehavior(this.g);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Yk0
    public Notification.Builder a() {
        return this.a;
    }

    @DexIgnore
    public final void b(Zk0.Ai ai) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 20) {
            IconCompat e2 = ai.e();
            Notification.Action.Builder builder = Build.VERSION.SDK_INT >= 23 ? new Notification.Action.Builder(e2 != null ? e2.n() : null, ai.i(), ai.a()) : new Notification.Action.Builder(e2 != null ? e2.c() : 0, ai.i(), ai.a());
            if (ai.f() != null) {
                for (RemoteInput remoteInput : Dl0.b(ai.f())) {
                    builder.addRemoteInput(remoteInput);
                }
            }
            Bundle bundle = ai.d() != null ? new Bundle(ai.d()) : new Bundle();
            bundle.putBoolean("android.support.allowGeneratedReplies", ai.b());
            if (Build.VERSION.SDK_INT >= 24) {
                builder.setAllowGeneratedReplies(ai.b());
            }
            bundle.putInt("android.support.action.semanticAction", ai.g());
            if (Build.VERSION.SDK_INT >= 28) {
                builder.setSemanticAction(ai.g());
            }
            if (Build.VERSION.SDK_INT >= 29) {
                builder.setContextual(ai.j());
            }
            bundle.putBoolean("android.support.action.showsUserInterface", ai.h());
            builder.addExtras(bundle);
            this.a.addAction(builder.build());
        } else if (i >= 16) {
            this.e.add(Bl0.f(this.a, ai));
        }
    }

    @DexIgnore
    public Notification c() {
        Bundle a2;
        RemoteViews e2;
        RemoteViews c2;
        Zk0.Fi fi = this.b.o;
        if (fi != null) {
            fi.b(this);
        }
        RemoteViews d2 = fi != null ? fi.d(this) : null;
        Notification d3 = d();
        if (d2 != null) {
            d3.contentView = d2;
        } else {
            RemoteViews remoteViews = this.b.F;
            if (remoteViews != null) {
                d3.contentView = remoteViews;
            }
        }
        if (!(Build.VERSION.SDK_INT < 16 || fi == null || (c2 = fi.c(this)) == null)) {
            d3.bigContentView = c2;
        }
        if (!(Build.VERSION.SDK_INT < 21 || fi == null || (e2 = this.b.o.e(this)) == null)) {
            d3.headsUpContentView = e2;
        }
        if (!(Build.VERSION.SDK_INT < 16 || fi == null || (a2 = Zk0.a(d3)) == null)) {
            fi.a(a2);
        }
        return d3;
    }

    @DexIgnore
    public Notification d() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            return this.a.build();
        }
        if (i >= 24) {
            Notification build = this.a.build();
            if (this.g == 0) {
                return build;
            }
            if (!(build.getGroup() == null || (build.flags & 512) == 0 || this.g != 2)) {
                e(build);
            }
            if (!(build.getGroup() != null && (build.flags & 512) == 0 && this.g == 1)) {
                return build;
            }
            e(build);
            return build;
        } else if (i >= 21) {
            this.a.setExtras(this.f);
            Notification build2 = this.a.build();
            RemoteViews remoteViews = this.c;
            if (remoteViews != null) {
                build2.contentView = remoteViews;
            }
            RemoteViews remoteViews2 = this.d;
            if (remoteViews2 != null) {
                build2.bigContentView = remoteViews2;
            }
            RemoteViews remoteViews3 = this.h;
            if (remoteViews3 != null) {
                build2.headsUpContentView = remoteViews3;
            }
            if (this.g == 0) {
                return build2;
            }
            if (!(build2.getGroup() == null || (build2.flags & 512) == 0 || this.g != 2)) {
                e(build2);
            }
            if (!(build2.getGroup() != null && (build2.flags & 512) == 0 && this.g == 1)) {
                return build2;
            }
            e(build2);
            return build2;
        } else if (i >= 20) {
            this.a.setExtras(this.f);
            Notification build3 = this.a.build();
            RemoteViews remoteViews4 = this.c;
            if (remoteViews4 != null) {
                build3.contentView = remoteViews4;
            }
            RemoteViews remoteViews5 = this.d;
            if (remoteViews5 != null) {
                build3.bigContentView = remoteViews5;
            }
            if (this.g == 0) {
                return build3;
            }
            if (!(build3.getGroup() == null || (build3.flags & 512) == 0 || this.g != 2)) {
                e(build3);
            }
            if (!(build3.getGroup() != null && (build3.flags & 512) == 0 && this.g == 1)) {
                return build3;
            }
            e(build3);
            return build3;
        } else if (i >= 19) {
            SparseArray<Bundle> a2 = Bl0.a(this.e);
            if (a2 != null) {
                this.f.putSparseParcelableArray("android.support.actionExtras", a2);
            }
            this.a.setExtras(this.f);
            Notification build4 = this.a.build();
            RemoteViews remoteViews6 = this.c;
            if (remoteViews6 != null) {
                build4.contentView = remoteViews6;
            }
            RemoteViews remoteViews7 = this.d;
            if (remoteViews7 == null) {
                return build4;
            }
            build4.bigContentView = remoteViews7;
            return build4;
        } else if (i < 16) {
            return this.a.getNotification();
        } else {
            Notification build5 = this.a.build();
            Bundle a3 = Zk0.a(build5);
            Bundle bundle = new Bundle(this.f);
            for (String str : this.f.keySet()) {
                if (a3.containsKey(str)) {
                    bundle.remove(str);
                }
            }
            a3.putAll(bundle);
            SparseArray<Bundle> a4 = Bl0.a(this.e);
            if (a4 != null) {
                Zk0.a(build5).putSparseParcelableArray("android.support.actionExtras", a4);
            }
            RemoteViews remoteViews8 = this.c;
            if (remoteViews8 != null) {
                build5.contentView = remoteViews8;
            }
            RemoteViews remoteViews9 = this.d;
            if (remoteViews9 != null) {
                build5.bigContentView = remoteViews9;
            }
            return build5;
        }
    }

    @DexIgnore
    public final void e(Notification notification) {
        notification.sound = null;
        notification.vibrate = null;
        int i = notification.defaults & -2;
        notification.defaults = i;
        notification.defaults = i & -3;
    }
}
