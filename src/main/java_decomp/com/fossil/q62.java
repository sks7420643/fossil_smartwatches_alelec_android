package com.fossil;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.Ac2;
import com.fossil.L72;
import com.fossil.M62;
import com.fossil.M62.Di;
import com.fossil.P72;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.GoogleApiActivity;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Q62<O extends M62.Di> implements S62<O> {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ M62<O> b;
    @DexIgnore
    public /* final */ O c;
    @DexIgnore
    public /* final */ G72<O> d;
    @DexIgnore
    public /* final */ Looper e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ R62 g;
    @DexIgnore
    public /* final */ U72 h;
    @DexIgnore
    public /* final */ L72 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public static /* final */ Ai c; // = new Aii().a();
        @DexIgnore
        public /* final */ U72 a;
        @DexIgnore
        public /* final */ Looper b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Aii {
            @DexIgnore
            public U72 a;
            @DexIgnore
            public Looper b;

            @DexIgnore
            public Ai a() {
                if (this.a == null) {
                    this.a = new F72();
                }
                if (this.b == null) {
                    this.b = Looper.getMainLooper();
                }
                return new Ai(this.a, this.b);
            }

            @DexIgnore
            public Aii b(Looper looper) {
                Rc2.l(looper, "Looper must not be null.");
                this.b = looper;
                return this;
            }

            @DexIgnore
            public Aii c(U72 u72) {
                Rc2.l(u72, "StatusExceptionMapper must not be null.");
                this.a = u72;
                return this;
            }
        }

        @DexIgnore
        public Ai(U72 u72, Account account, Looper looper) {
            this.a = u72;
            this.b = looper;
        }
    }

    @DexIgnore
    public Q62(Activity activity, M62<O> m62, O o, Ai ai) {
        Rc2.l(activity, "Null activity is not permitted.");
        Rc2.l(m62, "Api must not be null.");
        Rc2.l(ai, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.a = activity.getApplicationContext();
        r(activity);
        this.b = m62;
        this.c = o;
        this.e = ai.b;
        this.d = G72.b(m62, o);
        this.g = new P92(this);
        L72 o2 = L72.o(this.a);
        this.i = o2;
        this.f = o2.r();
        this.h = ai.a;
        if (!(activity instanceof GoogleApiActivity)) {
            B82.q(activity, this.i, this.d);
        }
        this.i.i(this);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Q62(android.app.Activity r3, com.fossil.M62<O> r4, O r5, com.fossil.U72 r6) {
        /*
            r2 = this;
            com.fossil.Q62$Ai$Aii r0 = new com.fossil.Q62$Ai$Aii
            r0.<init>()
            r0.c(r6)
            android.os.Looper r1 = r3.getMainLooper()
            r0.b(r1)
            com.fossil.Q62$Ai r0 = r0.a()
            r2.<init>(r3, r4, r5, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Q62.<init>(android.app.Activity, com.fossil.M62, com.fossil.M62$Di, com.fossil.U72):void");
    }

    @DexIgnore
    public Q62(Context context, M62<O> m62, Looper looper) {
        Rc2.l(context, "Null context is not permitted.");
        Rc2.l(m62, "Api must not be null.");
        Rc2.l(looper, "Looper must not be null.");
        this.a = context.getApplicationContext();
        r(context);
        this.b = m62;
        this.c = null;
        this.e = looper;
        this.d = G72.c(m62);
        this.g = new P92(this);
        L72 o = L72.o(this.a);
        this.i = o;
        this.f = o.r();
        this.h = new F72();
    }

    @DexIgnore
    public Q62(Context context, M62<O> m62, O o, Ai ai) {
        Rc2.l(context, "Null context is not permitted.");
        Rc2.l(m62, "Api must not be null.");
        Rc2.l(ai, "Settings must not be null; use Settings.DEFAULT_SETTINGS instead.");
        this.a = context.getApplicationContext();
        r(context);
        this.b = m62;
        this.c = o;
        this.e = ai.b;
        this.d = G72.b(m62, o);
        this.g = new P92(this);
        L72 o2 = L72.o(this.a);
        this.i = o2;
        this.f = o2.r();
        this.h = ai.a;
        this.i.i(this);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Q62(android.content.Context r2, com.fossil.M62<O> r3, O r4, com.fossil.U72 r5) {
        /*
            r1 = this;
            com.fossil.Q62$Ai$Aii r0 = new com.fossil.Q62$Ai$Aii
            r0.<init>()
            r0.c(r5)
            com.fossil.Q62$Ai r0 = r0.a()
            r1.<init>(r2, r3, r4, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Q62.<init>(android.content.Context, com.fossil.M62, com.fossil.M62$Di, com.fossil.U72):void");
    }

    @DexIgnore
    public static String r(Object obj) {
        if (Mf2.m()) {
            try {
                return (String) Context.class.getMethod("getFeatureId", new Class[0]).invoke(obj, new Object[0]);
            } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e2) {
            }
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.S62
    public G72<O> a() {
        return this.d;
    }

    @DexIgnore
    public R62 b() {
        return this.g;
    }

    @DexIgnore
    public Ac2.Ai c() {
        Account m;
        GoogleSignInAccount b2;
        GoogleSignInAccount b3;
        Ac2.Ai ai = new Ac2.Ai();
        O o = this.c;
        if (!(o instanceof M62.Di.Bii) || (b3 = ((M62.Di.Bii) o).b()) == null) {
            O o2 = this.c;
            m = o2 instanceof M62.Di.Aii ? ((M62.Di.Aii) o2).m() : null;
        } else {
            m = b3.m();
        }
        ai.c(m);
        O o3 = this.c;
        ai.a((!(o3 instanceof M62.Di.Bii) || (b2 = ((M62.Di.Bii) o3).b()) == null) ? Collections.emptySet() : b2.o0());
        ai.d(this.a.getClass().getName());
        ai.e(this.a.getPackageName());
        return ai;
    }

    @DexIgnore
    public <A extends M62.Bi, T extends I72<? extends Z62, A>> T d(T t) {
        o(0, t);
        return t;
    }

    @DexIgnore
    public <TResult, A extends M62.Bi> Nt3<TResult> e(W72<A, TResult> w72) {
        return q(0, w72);
    }

    @DexIgnore
    @Deprecated
    public <A extends M62.Bi, T extends S72<A, ?>, U extends Y72<A, ?>> Nt3<Void> f(T t, U u) {
        Rc2.k(t);
        Rc2.k(u);
        Rc2.l(t.b(), "Listener has already been released.");
        Rc2.l(u.a(), "Listener has already been released.");
        Rc2.b(t.b().equals(u.a()), "Listener registration and unregistration methods must be constructed with the same ListenerHolder.");
        return this.i.f(this, t, u, Lb2.b);
    }

    @DexIgnore
    public Nt3<Boolean> g(P72.Ai<?> ai) {
        Rc2.l(ai, "Listener key cannot be null.");
        return this.i.e(this, ai);
    }

    @DexIgnore
    public <A extends M62.Bi, T extends I72<? extends Z62, A>> T h(T t) {
        o(1, t);
        return t;
    }

    @DexIgnore
    public final M62<O> i() {
        return this.b;
    }

    @DexIgnore
    public O j() {
        return this.c;
    }

    @DexIgnore
    public Context k() {
        return this.a;
    }

    @DexIgnore
    public final int l() {
        return this.f;
    }

    @DexIgnore
    public Looper m() {
        return this.e;
    }

    @DexIgnore
    public M62.Fi n(Looper looper, L72.Ai<O> ai) {
        return this.b.d().c(this.a, looper, c().b(), this.c, ai, ai);
    }

    @DexIgnore
    public final <A extends M62.Bi, T extends I72<? extends Z62, A>> T o(int i2, T t) {
        t.t();
        this.i.j(this, i2, t);
        return t;
    }

    @DexIgnore
    public X92 p(Context context, Handler handler) {
        return new X92(context, handler, c().b());
    }

    @DexIgnore
    public final <TResult, A extends M62.Bi> Nt3<TResult> q(int i2, W72<A, TResult> w72) {
        Ot3 ot3 = new Ot3();
        this.i.k(this, i2, w72, ot3, this.h);
        return ot3.a();
    }
}
