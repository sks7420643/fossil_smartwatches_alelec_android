package com.fossil;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.view.animation.Animation;
import android.widget.ImageView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vx0 extends ImageView {
    @DexIgnore
    public Animation.AnimationListener b;
    @DexIgnore
    public int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends OvalShape {
        @DexIgnore
        public RadialGradient b;
        @DexIgnore
        public Paint c; // = new Paint();

        @DexIgnore
        public Ai(int i) {
            Vx0.this.c = i;
            d((int) rect().width());
        }

        @DexIgnore
        public final void d(int i) {
            float f = (float) (i / 2);
            RadialGradient radialGradient = new RadialGradient(f, f, (float) Vx0.this.c, new int[]{1023410176, 0}, (float[]) null, Shader.TileMode.CLAMP);
            this.b = radialGradient;
            this.c.setShader(radialGradient);
        }

        @DexIgnore
        public void draw(Canvas canvas, Paint paint) {
            int width = Vx0.this.getWidth() / 2;
            float f = (float) width;
            float height = (float) (Vx0.this.getHeight() / 2);
            canvas.drawCircle(f, height, f, this.c);
            canvas.drawCircle(f, height, (float) (width - Vx0.this.c), paint);
        }

        @DexIgnore
        public void onResize(float f, float f2) {
            super.onResize(f, f2);
            d((int) f);
        }
    }

    @DexIgnore
    public Vx0(Context context, int i) {
        super(context);
        ShapeDrawable shapeDrawable;
        float f = getContext().getResources().getDisplayMetrics().density;
        int i2 = (int) (1.75f * f);
        int i3 = (int) (LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES * f);
        this.c = (int) (3.5f * f);
        if (a()) {
            shapeDrawable = new ShapeDrawable(new OvalShape());
            Mo0.s0(this, f * 4.0f);
        } else {
            shapeDrawable = new ShapeDrawable(new Ai(this.c));
            setLayerType(1, shapeDrawable.getPaint());
            shapeDrawable.getPaint().setShadowLayer((float) this.c, (float) i3, (float) i2, 503316480);
            int i4 = this.c;
            setPadding(i4, i4, i4, i4);
        }
        shapeDrawable.getPaint().setColor(i);
        Mo0.o0(this, shapeDrawable);
    }

    @DexIgnore
    public final boolean a() {
        return Build.VERSION.SDK_INT >= 21;
    }

    @DexIgnore
    public void b(Animation.AnimationListener animationListener) {
        this.b = animationListener;
    }

    @DexIgnore
    public void onAnimationEnd() {
        super.onAnimationEnd();
        Animation.AnimationListener animationListener = this.b;
        if (animationListener != null) {
            animationListener.onAnimationEnd(getAnimation());
        }
    }

    @DexIgnore
    public void onAnimationStart() {
        super.onAnimationStart();
        Animation.AnimationListener animationListener = this.b;
        if (animationListener != null) {
            animationListener.onAnimationStart(getAnimation());
        }
    }

    @DexIgnore
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (!a()) {
            setMeasuredDimension(getMeasuredWidth() + (this.c * 2), getMeasuredHeight() + (this.c * 2));
        }
    }

    @DexIgnore
    public void setBackgroundColor(int i) {
        if (getBackground() instanceof ShapeDrawable) {
            ((ShapeDrawable) getBackground()).getPaint().setColor(i);
        }
    }
}
