package com.fossil;

import com.fossil.S32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class A22 implements S32.Ai {
    @DexIgnore
    public /* final */ B22 a;
    @DexIgnore
    public /* final */ H02 b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public A22(B22 b22, H02 h02, int i) {
        this.a = b22;
        this.b = h02;
        this.c = i;
    }

    @DexIgnore
    public static S32.Ai b(B22 b22, H02 h02, int i) {
        return new A22(b22, h02, i);
    }

    @DexIgnore
    @Override // com.fossil.S32.Ai
    public Object a() {
        return this.a.d.a(this.b, this.c + 1);
    }
}
