package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Pe5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ImageButton r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ View t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;

    @DexIgnore
    public Pe5(Object obj, View view, int i, ConstraintLayout constraintLayout, ImageButton imageButton, ImageView imageView, View view2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = imageButton;
        this.s = imageView;
        this.t = view2;
        this.u = flexibleTextView;
        this.v = flexibleTextView2;
    }

    @DexIgnore
    @Deprecated
    public static Pe5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (Pe5) ViewDataBinding.p(layoutInflater, 2131558689, viewGroup, z, obj);
    }

    @DexIgnore
    public static Pe5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, Aq0.d());
    }
}
