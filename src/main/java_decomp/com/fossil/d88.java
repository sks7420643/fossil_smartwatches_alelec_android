package com.fossil;

import com.fossil.B88;
import com.mapped.Dx6;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.concurrent.CompletableFuture;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@IgnoreJRERequirement
public final class D88 extends B88.Ai {
    @DexIgnore
    public static /* final */ B88.Ai a; // = new D88();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @IgnoreJRERequirement
    public static final class Ai<R> implements B88<R, CompletableFuture<R>> {
        @DexIgnore
        public /* final */ Type a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends CompletableFuture<R> {
            @DexIgnore
            public /* final */ /* synthetic */ Call b;

            @DexIgnore
            public Aii(Ai ai, Call call) {
                this.b = call;
            }

            @DexIgnore
            public boolean cancel(boolean z) {
                if (z) {
                    this.b.cancel();
                }
                return super.cancel(z);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Bii implements Dx6<R> {
            @DexIgnore
            public /* final */ /* synthetic */ CompletableFuture a;

            @DexIgnore
            public Bii(Ai ai, CompletableFuture completableFuture) {
                this.a = completableFuture;
            }

            @DexIgnore
            @Override // com.mapped.Dx6
            public void onFailure(Call<R> call, Throwable th) {
                this.a.completeExceptionally(th);
            }

            @DexIgnore
            @Override // com.mapped.Dx6
            public void onResponse(Call<R> call, Q88<R> q88) {
                if (q88.e()) {
                    this.a.complete(q88.a());
                } else {
                    this.a.completeExceptionally(new G88(q88));
                }
            }
        }

        @DexIgnore
        public Ai(Type type) {
            this.a = type;
        }

        @DexIgnore
        @Override // com.fossil.B88
        public Type a() {
            return this.a;
        }

        @DexIgnore
        @Override // com.fossil.B88
        public /* bridge */ /* synthetic */ Object b(Call call) {
            return c(call);
        }

        @DexIgnore
        public CompletableFuture<R> c(Call<R> call) {
            Aii aii = new Aii(this, call);
            call.D(new Bii(this, aii));
            return aii;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @IgnoreJRERequirement
    public static final class Bi<R> implements B88<R, CompletableFuture<Q88<R>>> {
        @DexIgnore
        public /* final */ Type a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends CompletableFuture<Q88<R>> {
            @DexIgnore
            public /* final */ /* synthetic */ Call b;

            @DexIgnore
            public Aii(Bi bi, Call call) {
                this.b = call;
            }

            @DexIgnore
            public boolean cancel(boolean z) {
                if (z) {
                    this.b.cancel();
                }
                return super.cancel(z);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Bii implements Dx6<R> {
            @DexIgnore
            public /* final */ /* synthetic */ CompletableFuture a;

            @DexIgnore
            public Bii(Bi bi, CompletableFuture completableFuture) {
                this.a = completableFuture;
            }

            @DexIgnore
            @Override // com.mapped.Dx6
            public void onFailure(Call<R> call, Throwable th) {
                this.a.completeExceptionally(th);
            }

            @DexIgnore
            @Override // com.mapped.Dx6
            public void onResponse(Call<R> call, Q88<R> q88) {
                this.a.complete(q88);
            }
        }

        @DexIgnore
        public Bi(Type type) {
            this.a = type;
        }

        @DexIgnore
        @Override // com.fossil.B88
        public Type a() {
            return this.a;
        }

        @DexIgnore
        @Override // com.fossil.B88
        public /* bridge */ /* synthetic */ Object b(Call call) {
            return c(call);
        }

        @DexIgnore
        public CompletableFuture<Q88<R>> c(Call<R> call) {
            Aii aii = new Aii(this, call);
            call.D(new Bii(this, aii));
            return aii;
        }
    }

    @DexIgnore
    @Override // com.fossil.B88.Ai
    public B88<?, ?> a(Type type, Annotation[] annotationArr, Retrofit retrofit3) {
        if (B88.Ai.c(type) != CompletableFuture.class) {
            return null;
        }
        if (type instanceof ParameterizedType) {
            Type b = B88.Ai.b(0, (ParameterizedType) type);
            if (B88.Ai.c(b) != Q88.class) {
                return new Ai(b);
            }
            if (b instanceof ParameterizedType) {
                return new Bi(B88.Ai.b(0, (ParameterizedType) b));
            }
            throw new IllegalStateException("Response must be parameterized as Response<Foo> or Response<? extends Foo>");
        }
        throw new IllegalStateException("CompletableFuture return type must be parameterized as CompletableFuture<Foo> or CompletableFuture<? extends Foo>");
    }
}
