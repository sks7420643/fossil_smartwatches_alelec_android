package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ii7 {
    @DexIgnore
    public static SharedPreferences a;

    @DexIgnore
    public static int a(Context context, String str, int i) {
        return c(context).getInt(Ei7.i(context, "wxop_" + str), i);
    }

    @DexIgnore
    public static long b(Context context, String str, long j) {
        return c(context).getLong(Ei7.i(context, "wxop_" + str), j);
    }

    @DexIgnore
    public static SharedPreferences c(Context context) {
        SharedPreferences sharedPreferences;
        synchronized (Ii7.class) {
            try {
                SharedPreferences sharedPreferences2 = context.getSharedPreferences(".mta-wxop", 0);
                a = sharedPreferences2;
                if (sharedPreferences2 == null) {
                    a = PreferenceManager.getDefaultSharedPreferences(context);
                }
                sharedPreferences = a;
            } catch (Throwable th) {
                throw th;
            }
        }
        return sharedPreferences;
    }

    @DexIgnore
    public static String d(Context context, String str, String str2) {
        return c(context).getString(Ei7.i(context, "wxop_" + str), str2);
    }

    @DexIgnore
    public static void e(Context context, String str, int i) {
        String i2 = Ei7.i(context, "wxop_" + str);
        SharedPreferences.Editor edit = c(context).edit();
        edit.putInt(i2, i);
        edit.commit();
    }

    @DexIgnore
    public static void f(Context context, String str, long j) {
        String i = Ei7.i(context, "wxop_" + str);
        SharedPreferences.Editor edit = c(context).edit();
        edit.putLong(i, j);
        edit.commit();
    }

    @DexIgnore
    public static void g(Context context, String str, String str2) {
        String i = Ei7.i(context, "wxop_" + str);
        SharedPreferences.Editor edit = c(context).edit();
        edit.putString(i, str2);
        edit.commit();
    }
}
