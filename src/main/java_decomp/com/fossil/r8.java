package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum R8 {
    c((byte) 0),
    d((byte) 1),
    e((byte) 2);
    
    @DexIgnore
    public static /* final */ Q8 g; // = new Q8(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public R8(byte b2) {
        this.b = (byte) b2;
    }
}
