package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P38 extends IOException {
    @DexIgnore
    public /* final */ D38 errorCode;

    @DexIgnore
    public P38(D38 d38) {
        super("stream was reset: " + d38);
        this.errorCode = d38;
    }
}
