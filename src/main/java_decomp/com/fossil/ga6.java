package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Cw5;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ga6 extends BaseFragment {
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public CommuteTimeWatchAppSettingsViewModel g;
    @DexIgnore
    public Po4 h;
    @DexIgnore
    public Cw5 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Ga6 a(String str) {
            Wg6.c(str, MicroAppSetting.SETTING);
            Ga6 ga6 = new Ga6();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.USER_SETTING, str);
            ga6.setArguments(bundle);
            return ga6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Cw5.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Ga6 a;

        @DexIgnore
        public Bi(Ga6 ga6) {
            this.a = ga6;
        }

        @DexIgnore
        @Override // com.fossil.Cw5.Bi
        public void a(AddressWrapper addressWrapper) {
            Wg6.c(addressWrapper, "address");
            this.a.Q6(addressWrapper, false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends I67 {
        @DexIgnore
        public /* final */ /* synthetic */ Ga6 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(int i, int i2, Ga6 ga6) {
            super(i, i2);
            this.f = ga6;
        }

        @DexIgnore
        @Override // com.fossil.Cv0.Fi
        public void B(RecyclerView.ViewHolder viewHolder, int i) {
            Wg6.c(viewHolder, "viewHolder");
            int adapterPosition = viewHolder.getAdapterPosition();
            Cw5 cw5 = this.f.i;
            Ga6.M6(this.f).l(cw5 != null ? cw5.i(adapterPosition) : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ga6 b;

        @DexIgnore
        public Di(Ga6 ga6) {
            this.b = ga6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ga6 b;

        @DexIgnore
        public Ei(Ga6 ga6) {
            this.b = ga6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.P6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<CommuteTimeWatchAppSettingsViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ Ga6 a;

        @DexIgnore
        public Fi(Ga6 ga6) {
            this.a = ga6;
        }

        @DexIgnore
        public final void a(CommuteTimeWatchAppSettingsViewModel.Ai ai) {
            List<AddressWrapper> a2 = ai.a();
            if (a2 != null) {
                this.a.R6(a2);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(CommuteTimeWatchAppSettingsViewModel.Ai ai) {
            a(ai);
        }
    }

    /*
    static {
        Wg6.b(Ga6.class.getSimpleName(), "CommuteTimeWatchAppSetti\u2026nt::class.java.simpleName");
    }
    */

    @DexIgnore
    public static final /* synthetic */ CommuteTimeWatchAppSettingsViewModel M6(Ga6 ga6) {
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = ga6.g;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            return commuteTimeWatchAppSettingsViewModel;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        x0(true);
        return true;
    }

    @DexIgnore
    public final void P6() {
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.g;
        if (commuteTimeWatchAppSettingsViewModel == null) {
            Wg6.n("mViewModel");
            throw null;
        } else if (commuteTimeWatchAppSettingsViewModel.j()) {
            Q6(new AddressWrapper(), true);
        } else {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.L(childFragmentManager);
        }
    }

    @DexIgnore
    public final void Q6(AddressWrapper addressWrapper, boolean z) {
        ArrayList<String> arrayList = null;
        Bundle bundle = new Bundle();
        bundle.putParcelable("KEY_SELECTED_ADDRESS", addressWrapper);
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.g;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            CommuteTimeWatchAppSetting f = commuteTimeWatchAppSettingsViewModel.f();
            if (f != null) {
                arrayList = f.getListAddressNameExceptOf(addressWrapper);
            }
            bundle.putStringArrayList("KEY_LIST_ADDRESS", arrayList);
            bundle.putBoolean("KEY_HAVING_MAP_RESULT", !z);
            CommuteTimeSettingsDetailActivity.A.a(this, bundle, 113);
            return;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void R6(List<AddressWrapper> list) {
        Cw5 cw5 = this.i;
        if (cw5 != null) {
            cw5.l(Pm7.j0(list));
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (intent != null && i2 == 113) {
            AddressWrapper addressWrapper = (AddressWrapper) intent.getParcelableExtra("KEY_SELECTED_ADDRESS");
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.g;
            if (commuteTimeWatchAppSettingsViewModel != null) {
                commuteTimeWatchAppSettingsViewModel.k(addressWrapper);
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        String str = null;
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().J1().a(this);
        Po4 po4 = this.h;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(CommuteTimeWatchAppSettingsViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ngsViewModel::class.java)");
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = (CommuteTimeWatchAppSettingsViewModel) a2;
            this.g = commuteTimeWatchAppSettingsViewModel;
            if (commuteTimeWatchAppSettingsViewModel != null) {
                Bundle arguments = getArguments();
                if (arguments != null) {
                    str = arguments.getString(Constants.USER_SETTING);
                }
                commuteTimeWatchAppSettingsViewModel.i(str);
                return;
            }
            Wg6.n("mViewModel");
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        T35 t35 = (T35) Aq0.f(layoutInflater, 2131558519, viewGroup, false, A6());
        t35.s.setOnClickListener(new Di(this));
        t35.r.setOnClickListener(new Ei(this));
        Cw5 cw5 = new Cw5();
        cw5.m(new Bi(this));
        this.i = cw5;
        RecyclerView recyclerView = t35.v;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.i);
        new Cv0(new Ci(0, 4, this)).g(recyclerView);
        CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.g;
        if (commuteTimeWatchAppSettingsViewModel != null) {
            commuteTimeWatchAppSettingsViewModel.h().h(getViewLifecycleOwner(), new Fi(this));
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel2 = this.g;
            if (commuteTimeWatchAppSettingsViewModel2 != null) {
                commuteTimeWatchAppSettingsViewModel2.m();
                new G37(this, t35);
                Wg6.b(t35, "binding");
                return t35.n();
            }
            Wg6.n("mViewModel");
            throw null;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void x0(boolean z) {
        if (z) {
            Intent intent = new Intent();
            CommuteTimeWatchAppSettingsViewModel commuteTimeWatchAppSettingsViewModel = this.g;
            if (commuteTimeWatchAppSettingsViewModel != null) {
                intent.putExtra("COMMUTE_TIME_WATCH_APP_SETTING", commuteTimeWatchAppSettingsViewModel.g());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }
}
