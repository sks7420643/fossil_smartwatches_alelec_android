package com.fossil;

import com.fossil.Ng4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Yg4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public abstract Yg4 a();

        @DexIgnore
        public abstract Ai b(String str);

        @DexIgnore
        public abstract Ai c(long j);

        @DexIgnore
        public abstract Ai d(long j);
    }

    @DexIgnore
    public static Ai a() {
        return new Ng4.Bi();
    }

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public abstract long c();

    @DexIgnore
    public abstract long d();
}
