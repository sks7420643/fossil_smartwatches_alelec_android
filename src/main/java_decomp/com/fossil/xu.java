package com.fossil;

import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xu extends Mu {
    @DexIgnore
    public Se L; // = new Se(0, 0, 0);
    @DexIgnore
    public long M; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public Ve N;

    @DexIgnore
    public Xu(Ve ve, K5 k5) {
        super(Fu.i, Hs.t, k5, 0, 8);
        this.N = ve;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return Gy1.c(super.A(), this.L.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        Se se = new Se(Hy1.n(order.getShort(0)), Hy1.n(order.getShort(2)), Hy1.n(order.getShort(4)));
        this.L = se;
        return Gy1.c(jSONObject, se.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Mu, com.fossil.Ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.N.b).putShort((short) this.N.c).putShort((short) this.N.d).putShort((short) this.N.e).array();
        Wg6.b(array, "ByteBuffer.allocate(8).o\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final Se Q() {
        return this.L;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void f(long j) {
        this.M = j;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public long x() {
        return this.M;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return Gy1.c(super.z(), this.N.toJSONObject());
    }
}
