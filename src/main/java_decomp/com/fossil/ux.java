package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ux implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Tz b;
    @DexIgnore
    public /* final */ /* synthetic */ Object c;

    @DexIgnore
    public Ux(Tz tz, Object obj) {
        this.b = tz;
        this.c = obj;
    }

    @DexIgnore
    public final void run() {
        this.b.b.o(this.c);
    }
}
