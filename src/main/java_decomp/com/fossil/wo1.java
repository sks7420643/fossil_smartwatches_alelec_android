package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.O80;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wo1 extends O80 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Wo1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Wo1 a(Parcel parcel) {
            return new Wo1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Wo1 createFromParcel(Parcel parcel) {
            return new Wo1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Wo1[] newArray(int i) {
            return new Wo1[i];
        }
    }

    @DexIgnore
    public Wo1() {
        super(Ap1.STOP_WATCH, null, null, 6);
    }

    @DexIgnore
    public /* synthetic */ Wo1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public Wo1(Vw1 vw1) {
        super(Ap1.STOP_WATCH, vw1, null, 4);
    }
}
