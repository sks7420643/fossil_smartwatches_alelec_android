package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vo2 extends Un2 {
    @DexIgnore
    public /* final */ /* synthetic */ Wi2 s;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Vo2(Uo2 uo2, R62 r62, Wi2 wi2) {
        super(r62);
        this.s = wi2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.M62$Bi] */
    @Override // com.fossil.I72
    public final /* synthetic */ void u(On2 on2) throws RemoteException {
        ((Lo2) on2.I()).V2(new Wi2(this.s, new Wo2(this)));
    }
}
