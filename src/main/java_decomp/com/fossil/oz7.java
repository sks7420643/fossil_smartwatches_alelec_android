package com.fossil;

import java.util.Iterator;
import java.util.List;
import java.util.ServiceLoader;
import kotlinx.coroutines.internal.MainDispatcherFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oz7 {
    @DexIgnore
    public static /* final */ boolean a; // = Wz7.e("kotlinx.coroutines.fast.service.loader", true);
    @DexIgnore
    public static /* final */ Jx7 b;

    /*
    static {
        Oz7 oz7 = new Oz7();
        b = oz7.a();
    }
    */

    @DexIgnore
    public final Jx7 a() {
        Object next;
        Object obj;
        Jx7 d;
        try {
            List<MainDispatcherFactory> c = a ? Gz7.a.c() : At7.t(Ys7.b(ServiceLoader.load(MainDispatcherFactory.class, MainDispatcherFactory.class.getClassLoader()).iterator()));
            Iterator it = c.iterator();
            if (!it.hasNext()) {
                obj = null;
            } else {
                Object next2 = it.next();
                if (!it.hasNext()) {
                    obj = next2;
                } else {
                    int c2 = ((MainDispatcherFactory) next2).c();
                    Object obj2 = next2;
                    while (true) {
                        next = it.next();
                        int c3 = ((MainDispatcherFactory) next).c();
                        if (c2 >= c3) {
                            c3 = c2;
                            next = obj2;
                        }
                        if (!it.hasNext()) {
                            break;
                        }
                        obj2 = next;
                        c2 = c3;
                    }
                    obj = next;
                }
            }
            MainDispatcherFactory mainDispatcherFactory = (MainDispatcherFactory) obj;
            return (mainDispatcherFactory == null || (d = Pz7.d(mainDispatcherFactory, c)) == null) ? Pz7.b(null, null, 3, null) : d;
        } catch (Throwable th) {
            return Pz7.b(th, null, 2, null);
        }
    }
}
