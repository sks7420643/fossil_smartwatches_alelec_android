package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tn0 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public Tn0(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public static Tn0 a(Object obj) {
        if (obj == null) {
            return null;
        }
        return new Tn0(obj);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Tn0.class != obj.getClass()) {
            return false;
        }
        Tn0 tn0 = (Tn0) obj;
        Object obj2 = this.a;
        return obj2 == null ? tn0.a == null : obj2.equals(tn0.a);
    }

    @DexIgnore
    public int hashCode() {
        Object obj = this.a;
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "DisplayCutoutCompat{" + this.a + "}";
    }
}
