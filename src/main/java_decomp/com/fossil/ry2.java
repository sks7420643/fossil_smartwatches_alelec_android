package com.fossil;

import java.util.AbstractMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ry2 extends Sx2<Map.Entry<K, V>> {
    @DexIgnore
    public /* final */ /* synthetic */ Sy2 zza;

    @DexIgnore
    public Ry2(Sy2 sy2) {
        this.zza = sy2;
    }

    @DexIgnore
    @Override // java.util.List
    public final /* synthetic */ Object get(int i) {
        Sw2.a(i, this.zza.f);
        int i2 = i * 2;
        return new AbstractMap.SimpleImmutableEntry(this.zza.e[i2], this.zza.e[i2 + 1]);
    }

    @DexIgnore
    public final int size() {
        return this.zza.f;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final boolean zzh() {
        return true;
    }
}
