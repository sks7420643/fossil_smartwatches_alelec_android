package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.migration.MigrationActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C27 extends BaseFragment implements J07 {
    @DexIgnore
    public static /* final */ Ai i; // = new Ai(null);
    @DexIgnore
    public I07 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final C27 a() {
            return new C27();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    public void K6(I07 i07) {
        Wg6.c(i07, "presenter");
        this.g = i07;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(I07 i07) {
        K6(i07);
    }

    @DexIgnore
    @Override // com.fossil.J07
    public void a3() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WelcomeActivity.a aVar = WelcomeActivity.B;
            Wg6.b(activity, "it");
            aVar.a(activity);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.J07
    public void l() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.B;
            Wg6.b(activity, "it");
            HomeActivity.a.b(aVar, activity, null, 2, null);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.J07
    public void o3() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            MigrationActivity.a aVar = MigrationActivity.A;
            Wg6.b(activity, "it");
            aVar.a(activity);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(2131558628, viewGroup, false);
        ImageView imageView = (ImageView) inflate.findViewById(2131362731);
        ImageView imageView2 = (ImageView) inflate.findViewById(2131362672);
        if (Wr4.a.a().l()) {
            Wg6.b(imageView, "ivLogo");
            imageView.setVisibility(0);
            Wg6.b(imageView2, "ivBackground");
            imageView2.setVisibility(4);
        }
        return inflate;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        I07 i07 = this.g;
        if (i07 != null) {
            i07.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        I07 i07 = this.g;
        if (i07 != null) {
            i07.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
