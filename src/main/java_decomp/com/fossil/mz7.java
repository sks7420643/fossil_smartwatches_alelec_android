package com.fossil;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mz7<E> {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(Mz7.class, Object.class, "_cur");
    @DexIgnore
    public volatile Object _cur;

    @DexIgnore
    public Mz7(boolean z) {
        this._cur = new Nz7(8, z);
    }

    @DexIgnore
    public final boolean a(E e) {
        while (true) {
            Nz7 nz7 = (Nz7) this._cur;
            int a2 = nz7.a(e);
            if (a2 == 0) {
                return true;
            }
            if (a2 == 1) {
                a.compareAndSet(this, nz7, nz7.i());
            } else if (a2 == 2) {
                return false;
            }
        }
    }

    @DexIgnore
    public final void b() {
        while (true) {
            Nz7 nz7 = (Nz7) this._cur;
            if (!nz7.d()) {
                a.compareAndSet(this, nz7, nz7.i());
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final int c() {
        return ((Nz7) this._cur).f();
    }

    @DexIgnore
    public final E d() {
        while (true) {
            Nz7 nz7 = (Nz7) this._cur;
            E e = (E) nz7.j();
            if (e != Nz7.g) {
                return e;
            }
            a.compareAndSet(this, nz7, nz7.i());
        }
    }
}
