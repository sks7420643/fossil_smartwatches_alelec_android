package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bb7 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ Za7 d;

    @DexIgnore
    public Bb7(String str, String str2, String str3, Za7 za7) {
        Wg6.c(str, "complicationId");
        Wg6.c(str2, "data");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = za7;
    }

    @DexIgnore
    public final String a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final Za7 c() {
        return this.d;
    }

    @DexIgnore
    public final String d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Bb7) {
                Bb7 bb7 = (Bb7) obj;
                if (!Wg6.a(this.a, bb7.a) || !Wg6.a(this.b, bb7.b) || !Wg6.a(this.c, bb7.c) || !Wg6.a(this.d, bb7.d)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        Za7 za7 = this.d;
        if (za7 != null) {
            i = za7.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "ComplicationWrapper(complicationId=" + this.a + ", data=" + this.b + ", setting=" + this.c + ", dimension=" + this.d + ")";
    }
}
