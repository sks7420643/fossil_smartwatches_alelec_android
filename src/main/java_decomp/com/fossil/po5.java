package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Po5 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends TypeToken<List<Oo5>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends TypeToken<ArrayList<Oo5>> {
    }

    @DexIgnore
    public final String a(List<Oo5> list) {
        try {
            return new Gson().u(list, new Ai().getType());
        } catch (Exception e) {
            return null;
        }
    }

    @DexIgnore
    public final List<Oo5> b(String str) {
        List<Oo5> list;
        if (str == null || str.length() == 0) {
            return null;
        }
        try {
            list = (List) new Gson().l(str, new Bi().getType());
        } catch (Exception e) {
            list = null;
        }
        return list;
    }
}
