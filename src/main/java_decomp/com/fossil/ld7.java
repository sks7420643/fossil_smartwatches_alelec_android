package com.fossil;

import android.graphics.Bitmap;
import android.net.NetworkInfo;
import com.facebook.internal.Utility;
import com.fossil.Rd7;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ld7 extends Rd7 {
    @DexIgnore
    public /* final */ Downloader a;
    @DexIgnore
    public /* final */ Td7 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends IOException {
        @DexIgnore
        public Ai(String str) {
            super(str);
        }
    }

    @DexIgnore
    public Ld7(Downloader downloader, Td7 td7) {
        this.a = downloader;
        this.b = td7;
    }

    @DexIgnore
    @Override // com.fossil.Rd7
    public boolean c(Pd7 pd7) {
        String scheme = pd7.d.getScheme();
        return "http".equals(scheme) || Utility.URL_SCHEME.equals(scheme);
    }

    @DexIgnore
    @Override // com.fossil.Rd7
    public int e() {
        return 2;
    }

    @DexIgnore
    @Override // com.fossil.Rd7
    public Rd7.Ai f(Pd7 pd7, int i) throws IOException {
        Downloader.Response load = this.a.load(pd7.d, pd7.c);
        if (load == null) {
            return null;
        }
        Picasso.LoadedFrom loadedFrom = load.c ? Picasso.LoadedFrom.DISK : Picasso.LoadedFrom.NETWORK;
        Bitmap a2 = load.a();
        if (a2 != null) {
            return new Rd7.Ai(a2, loadedFrom);
        }
        InputStream c = load.c();
        if (c == null) {
            return null;
        }
        if (loadedFrom == Picasso.LoadedFrom.DISK && load.b() == 0) {
            Xd7.e(c);
            throw new Ai("Received response with 0 content-length header.");
        }
        if (loadedFrom == Picasso.LoadedFrom.NETWORK && load.b() > 0) {
            this.b.f(load.b());
        }
        return new Rd7.Ai(c, loadedFrom);
    }

    @DexIgnore
    @Override // com.fossil.Rd7
    public boolean h(boolean z, NetworkInfo networkInfo) {
        return networkInfo == null || networkInfo.isConnected();
    }

    @DexIgnore
    @Override // com.fossil.Rd7
    public boolean i() {
        return true;
    }
}
