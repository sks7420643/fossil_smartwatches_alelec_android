package com.fossil;

import android.content.Context;
import android.media.ExifInterface;
import android.net.Uri;
import com.fossil.Rd7;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ed7 extends Bd7 {
    @DexIgnore
    public Ed7(Context context) {
        super(context);
    }

    @DexIgnore
    public static int k(Uri uri) throws IOException {
        int attributeInt = new ExifInterface(uri.getPath()).getAttributeInt("Orientation", 1);
        if (attributeInt == 3) {
            return 180;
        }
        if (attributeInt != 6) {
            return attributeInt != 8 ? 0 : 270;
        }
        return 90;
    }

    @DexIgnore
    @Override // com.fossil.Bd7, com.fossil.Rd7
    public boolean c(Pd7 pd7) {
        return "file".equals(pd7.d.getScheme());
    }

    @DexIgnore
    @Override // com.fossil.Bd7, com.fossil.Rd7
    public Rd7.Ai f(Pd7 pd7, int i) throws IOException {
        return new Rd7.Ai(null, j(pd7), Picasso.LoadedFrom.DISK, k(pd7.d));
    }
}
