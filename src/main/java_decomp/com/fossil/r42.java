package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R42 implements Runnable {
    @DexIgnore
    public static /* final */ Se2 d; // = new Se2("RevokeAccessOperation", new String[0]);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ V72 c; // = new V72(null);

    @DexIgnore
    public R42(String str) {
        Rc2.g(str);
        this.b = str;
    }

    @DexIgnore
    public static T62<Status> a(String str) {
        if (str == null) {
            return U62.a(new Status(4), null);
        }
        R42 r42 = new R42(str);
        new Thread(r42).start();
        return r42.c;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0097  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ac  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r6 = this;
            r5 = 0
            com.google.android.gms.common.api.Status r1 = com.google.android.gms.common.api.Status.h
            java.net.URL r2 = new java.net.URL     // Catch:{ IOException -> 0x005a, Exception -> 0x0085 }
            java.lang.String r0 = r6.b     // Catch:{ IOException -> 0x005a, Exception -> 0x0085 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ IOException -> 0x005a, Exception -> 0x0085 }
            int r3 = r0.length()     // Catch:{ IOException -> 0x005a, Exception -> 0x0085 }
            if (r3 == 0) goto L_0x0052
            java.lang.String r3 = "https://accounts.google.com/o/oauth2/revoke?token="
            java.lang.String r0 = r3.concat(r0)     // Catch:{ IOException -> 0x005a, Exception -> 0x0085 }
        L_0x0017:
            r2.<init>(r0)     // Catch:{ IOException -> 0x005a, Exception -> 0x0085 }
            java.net.URLConnection r0 = r2.openConnection()     // Catch:{ IOException -> 0x005a, Exception -> 0x0085 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x005a, Exception -> 0x0085 }
            java.lang.String r2 = "Content-Type"
            java.lang.String r3 = "application/x-www-form-urlencoded"
            r0.setRequestProperty(r2, r3)     // Catch:{ IOException -> 0x005a, Exception -> 0x0085 }
            int r2 = r0.getResponseCode()     // Catch:{ IOException -> 0x005a, Exception -> 0x0085 }
            r0 = 200(0xc8, float:2.8E-43)
            if (r2 != r0) goto L_0x0079
            com.google.android.gms.common.api.Status r0 = com.google.android.gms.common.api.Status.f     // Catch:{ IOException -> 0x005a, Exception -> 0x0085 }
        L_0x0031:
            com.fossil.Se2 r1 = com.fossil.R42.d     // Catch:{ IOException -> 0x00b4, Exception -> 0x00b7 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00b4, Exception -> 0x00b7 }
            r4 = 26
            r3.<init>(r4)     // Catch:{ IOException -> 0x00b4, Exception -> 0x00b7 }
            java.lang.String r4 = "Response Code: "
            r3.append(r4)     // Catch:{ IOException -> 0x00b4, Exception -> 0x00b7 }
            r3.append(r2)     // Catch:{ IOException -> 0x00b4, Exception -> 0x00b7 }
            java.lang.String r2 = r3.toString()     // Catch:{ IOException -> 0x00b4, Exception -> 0x00b7 }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ IOException -> 0x00b4, Exception -> 0x00b7 }
            r1.a(r2, r3)     // Catch:{ IOException -> 0x00b4, Exception -> 0x00b7 }
        L_0x004c:
            com.fossil.V72 r1 = r6.c
            r1.j(r0)
            return
        L_0x0052:
            java.lang.String r0 = new java.lang.String
            java.lang.String r3 = "https://accounts.google.com/o/oauth2/revoke?token="
            r0.<init>(r3)
            goto L_0x0017
        L_0x005a:
            r0 = move-exception
            r2 = r0
        L_0x005c:
            com.fossil.Se2 r3 = com.fossil.R42.d
            java.lang.String r0 = r2.toString()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            int r2 = r0.length()
            if (r2 == 0) goto L_0x00ac
            java.lang.String r2 = "IOException when revoking access: "
            java.lang.String r0 = r2.concat(r0)
        L_0x0072:
            java.lang.Object[] r2 = new java.lang.Object[r5]
            r3.b(r0, r2)
            r0 = r1
            goto L_0x004c
        L_0x0079:
            com.fossil.Se2 r0 = com.fossil.R42.d
            java.lang.String r3 = "Unable to revoke access!"
            r4 = 0
            java.lang.Object[] r4 = new java.lang.Object[r4]
            r0.b(r3, r4)
            r0 = r1
            goto L_0x0031
        L_0x0085:
            r0 = move-exception
            r2 = r0
        L_0x0087:
            com.fossil.Se2 r3 = com.fossil.R42.d
            java.lang.String r0 = r2.toString()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            int r2 = r0.length()
            if (r2 == 0) goto L_0x00a4
            java.lang.String r2 = "Exception when revoking access: "
            java.lang.String r0 = r2.concat(r0)
        L_0x009d:
            java.lang.Object[] r2 = new java.lang.Object[r5]
            r3.b(r0, r2)
            r0 = r1
            goto L_0x004c
        L_0x00a4:
            java.lang.String r0 = new java.lang.String
            java.lang.String r2 = "Exception when revoking access: "
            r0.<init>(r2)
            goto L_0x009d
        L_0x00ac:
            java.lang.String r0 = new java.lang.String
            java.lang.String r2 = "IOException when revoking access: "
            r0.<init>(r2)
            goto L_0x0072
        L_0x00b4:
            r2 = move-exception
            r1 = r0
            goto L_0x005c
        L_0x00b7:
            r2 = move-exception
            r1 = r0
            goto L_0x0087
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.R42.run():void");
    }
}
