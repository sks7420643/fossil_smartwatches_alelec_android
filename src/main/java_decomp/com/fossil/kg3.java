package com.fossil;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.internal.BoltsMeasurementEventListener;
import com.fossil.E13;
import com.fossil.Gu2;
import com.fossil.Hu2;
import com.fossil.Iu2;
import com.fossil.Ku2;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kg3 extends Zq3 {
    @DexIgnore
    public static /* final */ String[] f; // = {"last_bundled_timestamp", "ALTER TABLE events ADD COLUMN last_bundled_timestamp INTEGER;", "last_bundled_day", "ALTER TABLE events ADD COLUMN last_bundled_day INTEGER;", "last_sampled_complex_event_id", "ALTER TABLE events ADD COLUMN last_sampled_complex_event_id INTEGER;", "last_sampling_rate", "ALTER TABLE events ADD COLUMN last_sampling_rate INTEGER;", "last_exempt_from_sampling", "ALTER TABLE events ADD COLUMN last_exempt_from_sampling INTEGER;", "current_session_count", "ALTER TABLE events ADD COLUMN current_session_count INTEGER;"};
    @DexIgnore
    public static /* final */ String[] g; // = {"origin", "ALTER TABLE user_attributes ADD COLUMN origin TEXT;"};
    @DexIgnore
    public static /* final */ String[] h; // = {"app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;", "app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;", "gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;", "dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;", "measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;", "last_bundle_start_timestamp", "ALTER TABLE apps ADD COLUMN last_bundle_start_timestamp INTEGER;", "day", "ALTER TABLE apps ADD COLUMN day INTEGER;", "daily_public_events_count", "ALTER TABLE apps ADD COLUMN daily_public_events_count INTEGER;", "daily_events_count", "ALTER TABLE apps ADD COLUMN daily_events_count INTEGER;", "daily_conversions_count", "ALTER TABLE apps ADD COLUMN daily_conversions_count INTEGER;", "remote_config", "ALTER TABLE apps ADD COLUMN remote_config BLOB;", "config_fetched_time", "ALTER TABLE apps ADD COLUMN config_fetched_time INTEGER;", "failed_config_fetch_time", "ALTER TABLE apps ADD COLUMN failed_config_fetch_time INTEGER;", "app_version_int", "ALTER TABLE apps ADD COLUMN app_version_int INTEGER;", "firebase_instance_id", "ALTER TABLE apps ADD COLUMN firebase_instance_id TEXT;", "daily_error_events_count", "ALTER TABLE apps ADD COLUMN daily_error_events_count INTEGER;", "daily_realtime_events_count", "ALTER TABLE apps ADD COLUMN daily_realtime_events_count INTEGER;", "health_monitor_sample", "ALTER TABLE apps ADD COLUMN health_monitor_sample TEXT;", "android_id", "ALTER TABLE apps ADD COLUMN android_id INTEGER;", "adid_reporting_enabled", "ALTER TABLE apps ADD COLUMN adid_reporting_enabled INTEGER;", "ssaid_reporting_enabled", "ALTER TABLE apps ADD COLUMN ssaid_reporting_enabled INTEGER;", "admob_app_id", "ALTER TABLE apps ADD COLUMN admob_app_id TEXT;", "linked_admob_app_id", "ALTER TABLE apps ADD COLUMN linked_admob_app_id TEXT;", "dynamite_version", "ALTER TABLE apps ADD COLUMN dynamite_version INTEGER;", "safelisted_events", "ALTER TABLE apps ADD COLUMN safelisted_events TEXT;", "ga_app_id", "ALTER TABLE apps ADD COLUMN ga_app_id TEXT;"};
    @DexIgnore
    public static /* final */ String[] i; // = {"realtime", "ALTER TABLE raw_events ADD COLUMN realtime INTEGER;"};
    @DexIgnore
    public static /* final */ String[] j; // = {"has_realtime", "ALTER TABLE queue ADD COLUMN has_realtime INTEGER;", "retry_count", "ALTER TABLE queue ADD COLUMN retry_count INTEGER;"};
    @DexIgnore
    public static /* final */ String[] k; // = {"session_scoped", "ALTER TABLE event_filters ADD COLUMN session_scoped BOOLEAN;"};
    @DexIgnore
    public static /* final */ String[] l; // = {"session_scoped", "ALTER TABLE property_filters ADD COLUMN session_scoped BOOLEAN;"};
    @DexIgnore
    public static /* final */ String[] m; // = {"previous_install_count", "ALTER TABLE app2 ADD COLUMN previous_install_count INTEGER;"};
    @DexIgnore
    public /* final */ Lg3 d; // = new Lg3(this, e(), "google_app_measurement.db");
    @DexIgnore
    public /* final */ Vq3 e; // = new Vq3(zzm());

    @DexIgnore
    public Kg3(Yq3 yq3) {
        super(yq3);
    }

    @DexIgnore
    public static void K(ContentValues contentValues, String str, Object obj) {
        Rc2.g(str);
        Rc2.k(obj);
        if (obj instanceof String) {
            contentValues.put(str, (String) obj);
        } else if (obj instanceof Long) {
            contentValues.put(str, (Long) obj);
        } else if (obj instanceof Double) {
            contentValues.put(str, (Double) obj);
        } else {
            throw new IllegalArgumentException("Invalid value type");
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0131  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.Jg3 A(long r13, java.lang.String r15, long r16, boolean r18, boolean r19, boolean r20, boolean r21, boolean r22) {
        /*
        // Method dump skipped, instructions count: 313
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.A(long, java.lang.String, long, boolean, boolean, boolean, boolean, boolean):com.fossil.Jg3");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:56:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.os.Bundle A0(java.lang.String r9) {
        /*
            r8 = this;
            r2 = 0
            r8.h()
            r8.r()
            android.database.sqlite.SQLiteDatabase r0 = r8.v()     // Catch:{ SQLiteException -> 0x00db, all -> 0x00de }
            java.lang.String r1 = "select parameters from default_event_params where app_id=?"
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x00db, all -> 0x00de }
            r4 = 0
            r3[r4] = r9     // Catch:{ SQLiteException -> 0x00db, all -> 0x00de }
            android.database.Cursor r1 = r0.rawQuery(r1, r3)     // Catch:{ SQLiteException -> 0x00db, all -> 0x00de }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0074 }
            if (r0 != 0) goto L_0x0030
            com.fossil.Kl3 r0 = r8.d()     // Catch:{ SQLiteException -> 0x0074 }
            com.fossil.Nl3 r0 = r0.N()     // Catch:{ SQLiteException -> 0x0074 }
            java.lang.String r3 = "Default event parameters not found"
            r0.a(r3)     // Catch:{ SQLiteException -> 0x0074 }
            if (r1 == 0) goto L_0x002f
            r1.close()
        L_0x002f:
            return r2
        L_0x0030:
            r0 = 0
            byte[] r3 = r1.getBlob(r0)
            com.fossil.Wu2$Ai r0 = com.fossil.Wu2.c0()     // Catch:{ IOException -> 0x00c2 }
            com.fossil.Gr3.z(r0, r3)     // Catch:{ IOException -> 0x00c2 }
            com.fossil.Wu2$Ai r0 = (com.fossil.Wu2.Ai) r0     // Catch:{ IOException -> 0x00c2 }
            com.fossil.M23 r0 = r0.h()     // Catch:{ IOException -> 0x00c2 }
            com.fossil.E13 r0 = (com.fossil.E13) r0     // Catch:{ IOException -> 0x00c2 }
            com.fossil.Wu2 r0 = (com.fossil.Wu2) r0     // Catch:{ IOException -> 0x00c2 }
            r8.n()
            java.util.List r0 = r0.D()
            android.os.Bundle r3 = new android.os.Bundle
            r3.<init>()
            java.util.Iterator r4 = r0.iterator()
        L_0x0056:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x00ba
            java.lang.Object r0 = r4.next()
            com.fossil.Yu2 r0 = (com.fossil.Yu2) r0
            java.lang.String r5 = r0.O()
            boolean r6 = r0.b0()
            if (r6 == 0) goto L_0x0088
            double r6 = r0.c0()
            r3.putDouble(r5, r6)
            goto L_0x0056
        L_0x0074:
            r0 = move-exception
        L_0x0075:
            com.fossil.Kl3 r3 = r8.d()     // Catch:{ all -> 0x0096 }
            com.fossil.Nl3 r3 = r3.F()     // Catch:{ all -> 0x0096 }
            java.lang.String r4 = "Error selecting default event parameters"
            r3.b(r4, r0)     // Catch:{ all -> 0x0096 }
            if (r1 == 0) goto L_0x002f
            r1.close()
            goto L_0x002f
        L_0x0088:
            boolean r6 = r0.Z()
            if (r6 == 0) goto L_0x009e
            float r0 = r0.a0()
            r3.putFloat(r5, r0)
            goto L_0x0056
        L_0x0096:
            r0 = move-exception
            r2 = r1
        L_0x0098:
            if (r2 == 0) goto L_0x009d
            r2.close()
        L_0x009d:
            throw r0
        L_0x009e:
            boolean r6 = r0.T()
            if (r6 == 0) goto L_0x00ac
            java.lang.String r0 = r0.U()
            r3.putString(r5, r0)
            goto L_0x0056
        L_0x00ac:
            boolean r6 = r0.X()
            if (r6 == 0) goto L_0x0056
            long r6 = r0.Y()
            r3.putLong(r5, r6)
            goto L_0x0056
        L_0x00ba:
            if (r1 == 0) goto L_0x00bf
            r1.close()
        L_0x00bf:
            r2 = r3
            goto L_0x002f
        L_0x00c2:
            r0 = move-exception
            com.fossil.Kl3 r3 = r8.d()
            com.fossil.Nl3 r3 = r3.F()
            java.lang.String r4 = "Failed to retrieve default event parameters. appId"
            java.lang.Object r5 = com.fossil.Kl3.w(r9)
            r3.c(r4, r5, r0)
            if (r1 == 0) goto L_0x002f
            r1.close()
            goto L_0x002f
        L_0x00db:
            r0 = move-exception
            r1 = r2
            goto L_0x0075
        L_0x00de:
            r0 = move-exception
            goto L_0x0098
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.A0(java.lang.String):android.os.Bundle");
    }

    @DexIgnore
    public final Jg3 B(long j2, String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        return A(j2, str, 1, false, false, z3, false, z5);
    }

    @DexIgnore
    public final boolean B0() {
        return h0("select count(1) > 0 from queue where has_realtime = 1", null) != 0;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0153  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0177  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.Rg3 C(java.lang.String r22, java.lang.String r23) {
        /*
        // Method dump skipped, instructions count: 392
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.C(java.lang.String, java.lang.String):com.fossil.Rg3");
    }

    @DexIgnore
    public final void C0() {
        int delete;
        h();
        r();
        if (g0()) {
            long a2 = l().h.a();
            long c = zzm().c();
            if (Math.abs(c - a2) > Xg3.y.a(null).longValue()) {
                l().h.b(c);
                h();
                r();
                if (g0() && (delete = v().delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", new String[]{String.valueOf(zzm().b()), String.valueOf(Zr3.M())})) > 0) {
                    d().N().b("Deleted stale rows. rowsDeleted", Integer.valueOf(delete));
                }
            }
        }
    }

    @DexIgnore
    public final long D0() {
        return y("select max(bundle_end_timestamp) from queue", null, 0);
    }

    @DexIgnore
    public final Object E(Cursor cursor, int i2) {
        int type = cursor.getType(i2);
        if (type == 0) {
            d().F().a("Loaded invalid null value from database");
            return null;
        } else if (type == 1) {
            return Long.valueOf(cursor.getLong(i2));
        } else {
            if (type == 2) {
                return Double.valueOf(cursor.getDouble(i2));
            }
            if (type == 3) {
                return cursor.getString(i2);
            }
            if (type != 4) {
                d().F().b("Loaded invalid unknown value type, ignoring it", Integer.valueOf(type));
                return null;
            }
            d().F().a("Loaded invalid blob type value, ignoring it");
            return null;
        }
    }

    @DexIgnore
    public final long E0() {
        return y("select max(timestamp) from raw_events", null, 0);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0059  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String F(long r8) {
        /*
            r7 = this;
            r1 = 0
            r7.h()
            r7.r()
            android.database.sqlite.SQLiteDatabase r0 = r7.v()     // Catch:{ SQLiteException -> 0x0048, all -> 0x003f }
            java.lang.String r2 = "select app_id from apps where app_id in (select distinct app_id from raw_events) and config_fetched_time < ? order by failed_config_fetch_time limit 1;"
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x0048, all -> 0x003f }
            r4 = 0
            java.lang.String r5 = java.lang.String.valueOf(r8)     // Catch:{ SQLiteException -> 0x0048, all -> 0x003f }
            r3[r4] = r5     // Catch:{ SQLiteException -> 0x0048, all -> 0x003f }
            android.database.Cursor r0 = r0.rawQuery(r2, r3)     // Catch:{ SQLiteException -> 0x0048, all -> 0x003f }
            boolean r2 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x0061 }
            if (r2 != 0) goto L_0x0034
            com.fossil.Kl3 r2 = r7.d()     // Catch:{ SQLiteException -> 0x0061 }
            com.fossil.Nl3 r2 = r2.N()     // Catch:{ SQLiteException -> 0x0061 }
            java.lang.String r3 = "No expired configs for apps with pending events"
            r2.a(r3)     // Catch:{ SQLiteException -> 0x0061 }
            if (r0 == 0) goto L_0x0033
            r0.close()
        L_0x0033:
            return r1
        L_0x0034:
            r2 = 0
            java.lang.String r1 = r0.getString(r2)
            if (r0 == 0) goto L_0x0033
            r0.close()
            goto L_0x0033
        L_0x003f:
            r0 = move-exception
            r2 = r0
            r3 = r1
        L_0x0042:
            if (r3 == 0) goto L_0x0047
            r3.close()
        L_0x0047:
            throw r2
        L_0x0048:
            r2 = move-exception
            r0 = r1
        L_0x004a:
            com.fossil.Kl3 r3 = r7.d()     // Catch:{ all -> 0x005d }
            com.fossil.Nl3 r3 = r3.F()     // Catch:{ all -> 0x005d }
            java.lang.String r4 = "Error selecting expired configs"
            r3.b(r4, r2)     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0033
            r0.close()
            goto L_0x0033
        L_0x005d:
            r1 = move-exception
            r2 = r1
            r3 = r0
            goto L_0x0042
        L_0x0061:
            r2 = move-exception
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.F(long):java.lang.String");
    }

    @DexIgnore
    public final boolean F0() {
        return h0("select count(1) > 0 from raw_events", null) != 0;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.Hr3> G(java.lang.String r12) {
        /*
            r11 = this;
            r9 = 0
            com.fossil.Rc2.g(r12)
            r11.h()
            r11.r()
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            android.database.sqlite.SQLiteDatabase r0 = r11.v()     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00d0 }
            java.lang.String r1 = "user_attributes"
            r2 = 4
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00d0 }
            r3 = 0
            java.lang.String r4 = "name"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00d0 }
            r3 = 1
            java.lang.String r4 = "origin"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00d0 }
            r3 = 2
            java.lang.String r4 = "set_timestamp"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00d0 }
            r3 = 3
            java.lang.String r4 = "value"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00d0 }
            java.lang.String r3 = "app_id=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00d0 }
            r5 = 0
            r4[r5] = r12     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00d0 }
            r5 = 0
            r6 = 0
            java.lang.String r7 = "rowid"
            java.lang.String r8 = "1000"
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00d0 }
            boolean r0 = r7.moveToFirst()     // Catch:{ SQLiteException -> 0x008d, all -> 0x00c5 }
            if (r0 != 0) goto L_0x004b
            if (r7 == 0) goto L_0x0049
            r7.close()
        L_0x0049:
            r9 = r10
        L_0x004a:
            return r9
        L_0x004b:
            r0 = 0
            java.lang.String r3 = r7.getString(r0)
            r0 = 1
            java.lang.String r2 = r7.getString(r0)
            if (r2 != 0) goto L_0x0059
            java.lang.String r2 = ""
        L_0x0059:
            r0 = 2
            long r4 = r7.getLong(r0)
            r0 = 3
            java.lang.Object r6 = r11.E(r7, r0)
            if (r6 != 0) goto L_0x0083
            com.fossil.Kl3 r0 = r11.d()
            com.fossil.Nl3 r0 = r0.F()
            java.lang.String r1 = "Read invalid user property value, ignoring it. appId"
            java.lang.Object r2 = com.fossil.Kl3.w(r12)
            r0.b(r1, r2)
        L_0x0076:
            boolean r0 = r7.moveToNext()
            if (r0 != 0) goto L_0x004b
            if (r7 == 0) goto L_0x0081
            r7.close()
        L_0x0081:
            r9 = r10
            goto L_0x004a
        L_0x0083:
            com.fossil.Hr3 r0 = new com.fossil.Hr3
            r1 = r12
            r0.<init>(r1, r2, r3, r4, r6)
            r10.add(r0)
            goto L_0x0076
        L_0x008d:
            r0 = move-exception
            r1 = r7
        L_0x008f:
            com.fossil.Kl3 r2 = r11.d()     // Catch:{ all -> 0x00cd }
            com.fossil.Nl3 r2 = r2.F()     // Catch:{ all -> 0x00cd }
            java.lang.String r3 = "Error querying user properties. appId"
            java.lang.Object r4 = com.fossil.Kl3.w(r12)     // Catch:{ all -> 0x00cd }
            r2.c(r3, r4, r0)     // Catch:{ all -> 0x00cd }
            boolean r0 = com.fossil.Q63.a()     // Catch:{ all -> 0x00cd }
            if (r0 == 0) goto L_0x00bf
            com.fossil.Zr3 r0 = r11.m()     // Catch:{ all -> 0x00cd }
            com.fossil.Zk3<java.lang.Boolean> r2 = com.fossil.Xg3.T0     // Catch:{ all -> 0x00cd }
            boolean r0 = r0.B(r12, r2)     // Catch:{ all -> 0x00cd }
            if (r0 == 0) goto L_0x00bf
            java.util.List r9 = java.util.Collections.emptyList()     // Catch:{ all -> 0x00cd }
            if (r1 == 0) goto L_0x004a
            r1.close()
            goto L_0x004a
        L_0x00bc:
            r0 = move-exception
            r1 = r9
            goto L_0x008f
        L_0x00bf:
            if (r1 == 0) goto L_0x004a
            r1.close()
            goto L_0x004a
        L_0x00c5:
            r0 = move-exception
        L_0x00c6:
            r9 = r7
        L_0x00c7:
            if (r9 == 0) goto L_0x00cc
            r9.close()
        L_0x00cc:
            throw r0
        L_0x00cd:
            r0 = move-exception
            r7 = r1
            goto L_0x00c6
        L_0x00d0:
            r0 = move-exception
            goto L_0x00c7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.G(java.lang.String):java.util.List");
    }

    @DexIgnore
    public final boolean G0() {
        return h0("select count(1) > 0 from raw_events where realtime = 1", null) != 0;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:62:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<android.util.Pair<com.fossil.Av2, java.lang.Long>> H(java.lang.String r12, int r13, int r14) {
        /*
        // Method dump skipped, instructions count: 264
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.H(java.lang.String, int, int):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00e4, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00e5, code lost:
        r1 = r7;
        r13 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x011b, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x011c, code lost:
        r7 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x012d, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0132, code lost:
        r0 = e;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fe A[Catch:{ all -> 0x012f }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x011b A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x012d A[ExcHandler: all (th java.lang.Throwable), Splitter:B:24:0x00b7] */
    /* JADX WARNING: Removed duplicated region for block: B:67:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.Hr3> I(java.lang.String r12, java.lang.String r13, java.lang.String r14) {
        /*
        // Method dump skipped, instructions count: 310
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.I(java.lang.String, java.lang.String, java.lang.String):java.util.List");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x015c  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0165  */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.Xr3> J(java.lang.String r24, java.lang.String[] r25) {
        /*
        // Method dump skipped, instructions count: 371
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.J(java.lang.String, java.lang.String[]):java.util.List");
    }

    @DexIgnore
    public final void L(Rg3 rg3) {
        long j2 = null;
        Rc2.k(rg3);
        h();
        r();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", rg3.a);
        contentValues.put("name", rg3.b);
        contentValues.put("lifetime_count", Long.valueOf(rg3.c));
        contentValues.put("current_bundle_count", Long.valueOf(rg3.d));
        contentValues.put("last_fire_timestamp", Long.valueOf(rg3.f));
        contentValues.put("last_bundled_timestamp", Long.valueOf(rg3.g));
        contentValues.put("last_bundled_day", rg3.h);
        contentValues.put("last_sampled_complex_event_id", rg3.i);
        contentValues.put("last_sampling_rate", rg3.j);
        contentValues.put("current_session_count", Long.valueOf(rg3.e));
        Boolean bool = rg3.k;
        if (bool != null && bool.booleanValue()) {
            j2 = 1L;
        }
        contentValues.put("last_exempt_from_sampling", j2);
        try {
            if (v().insertWithOnConflict("events", null, contentValues, 5) == -1) {
                d().F().b("Failed to insert/update event aggregates (got -1). appId", Kl3.w(rg3.a));
            }
        } catch (SQLiteException e2) {
            d().F().c("Error storing event aggregates. appId", Kl3.w(rg3.a), e2);
        }
    }

    @DexIgnore
    public final void M(Ll3 ll3) {
        Rc2.k(ll3);
        h();
        r();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", ll3.t());
        contentValues.put("app_instance_id", ll3.x());
        contentValues.put("gmp_app_id", ll3.A());
        contentValues.put("resettable_device_id_hash", ll3.J());
        contentValues.put("last_bundle_index", Long.valueOf(ll3.f0()));
        contentValues.put("last_bundle_start_timestamp", Long.valueOf(ll3.P()));
        contentValues.put("last_bundle_end_timestamp", Long.valueOf(ll3.R()));
        contentValues.put("app_version", ll3.T());
        contentValues.put("app_store", ll3.X());
        contentValues.put("gmp_version", Long.valueOf(ll3.Z()));
        contentValues.put("dev_cert_hash", Long.valueOf(ll3.b0()));
        contentValues.put("measurement_enabled", Boolean.valueOf(ll3.e0()));
        contentValues.put("day", Long.valueOf(ll3.j0()));
        contentValues.put("daily_public_events_count", Long.valueOf(ll3.k0()));
        contentValues.put("daily_events_count", Long.valueOf(ll3.l0()));
        contentValues.put("daily_conversions_count", Long.valueOf(ll3.m0()));
        contentValues.put("config_fetched_time", Long.valueOf(ll3.g0()));
        contentValues.put("failed_config_fetch_time", Long.valueOf(ll3.h0()));
        contentValues.put("app_version_int", Long.valueOf(ll3.V()));
        contentValues.put("firebase_instance_id", ll3.M());
        contentValues.put("daily_error_events_count", Long.valueOf(ll3.h()));
        contentValues.put("daily_realtime_events_count", Long.valueOf(ll3.g()));
        contentValues.put("health_monitor_sample", ll3.i());
        contentValues.put("android_id", Long.valueOf(ll3.k()));
        contentValues.put("adid_reporting_enabled", Boolean.valueOf(ll3.l()));
        contentValues.put("ssaid_reporting_enabled", Boolean.valueOf(ll3.m()));
        contentValues.put("admob_app_id", ll3.D());
        contentValues.put("dynamite_version", Long.valueOf(ll3.d0()));
        if (ll3.o() != null) {
            if (ll3.o().size() == 0) {
                d().I().b("Safelisted events should not be an empty list. appId", ll3.t());
            } else {
                contentValues.put("safelisted_events", TextUtils.join(",", ll3.o()));
            }
        }
        if (I73.a() && m().B(ll3.t(), Xg3.o0)) {
            contentValues.put("ga_app_id", ll3.G());
        }
        try {
            SQLiteDatabase v = v();
            if (((long) v.update("apps", contentValues, "app_id = ?", new String[]{ll3.t()})) == 0 && v.insertWithOnConflict("apps", null, contentValues, 5) == -1) {
                d().F().b("Failed to insert/update app (got -1). appId", Kl3.w(ll3.t()));
            }
        } catch (SQLiteException e2) {
            d().F().c("Error storing app. appId", Kl3.w(ll3.t()), e2);
        }
    }

    @DexIgnore
    public final void N(String str, List<Gu2> list) {
        boolean z;
        boolean z2;
        boolean z3;
        Rc2.k(list);
        for (int i2 = 0; i2 < list.size(); i2++) {
            Gu2.Ai ai = (Gu2.Ai) list.get(i2).x();
            if (ai.C() != 0) {
                for (int i3 = 0; i3 < ai.C(); i3++) {
                    Hu2.Ai ai2 = (Hu2.Ai) ai.E(i3).x();
                    Hu2.Ai ai3 = (Hu2.Ai) ((E13.Ai) ai2.clone());
                    String b = On3.b(ai2.B());
                    if (b != null) {
                        ai3.y(b);
                        z3 = true;
                    } else {
                        z3 = false;
                    }
                    boolean z4 = z3;
                    for (int i4 = 0; i4 < ai2.C(); i4++) {
                        Iu2 z5 = ai2.z(i4);
                        String a2 = Nn3.a(z5.M());
                        if (a2 != null) {
                            Iu2.Ai ai4 = (Iu2.Ai) z5.x();
                            ai4.x(a2);
                            ai3.x(i4, (Iu2) ((E13) ai4.h()));
                            z4 = true;
                        }
                    }
                    if (z4) {
                        ai.y(i3, ai3);
                        list.set(i2, (Gu2) ((E13) ai.h()));
                    }
                }
            }
            if (ai.x() != 0) {
                for (int i5 = 0; i5 < ai.x(); i5++) {
                    Ku2 B = ai.B(i5);
                    String a3 = Qn3.a(B.H());
                    if (a3 != null) {
                        Ku2.Ai ai5 = (Ku2.Ai) B.x();
                        ai5.x(a3);
                        ai.z(i5, ai5);
                        list.set(i2, (Gu2) ((E13) ai.h()));
                    }
                }
            }
        }
        r();
        h();
        Rc2.g(str);
        Rc2.k(list);
        SQLiteDatabase v = v();
        v.beginTransaction();
        try {
            r();
            h();
            Rc2.g(str);
            SQLiteDatabase v2 = v();
            v2.delete("property_filters", "app_id=?", new String[]{str});
            v2.delete("event_filters", "app_id=?", new String[]{str});
            for (Gu2 gu2 : list) {
                r();
                h();
                Rc2.g(str);
                Rc2.k(gu2);
                if (!gu2.I()) {
                    d().I().b("Audience with no ID. appId", Kl3.w(str));
                } else {
                    int J = gu2.J();
                    Iterator<Hu2> it = gu2.N().iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (!it.next().I()) {
                                d().I().c("Event filter with no ID. Audience definition ignored. appId, audienceId", Kl3.w(str), Integer.valueOf(J));
                                break;
                            }
                        } else {
                            Iterator<Ku2> it2 = gu2.L().iterator();
                            while (true) {
                                if (it2.hasNext()) {
                                    if (!it2.next().E()) {
                                        d().I().c("Property filter with no ID. Audience definition ignored. appId, audienceId", Kl3.w(str), Integer.valueOf(J));
                                        break;
                                    }
                                } else {
                                    Iterator<Hu2> it3 = gu2.N().iterator();
                                    while (true) {
                                        if (it3.hasNext()) {
                                            if (!T(str, J, it3.next())) {
                                                z = false;
                                                break;
                                            }
                                        } else {
                                            z = true;
                                            break;
                                        }
                                    }
                                    if (z) {
                                        Iterator<Ku2> it4 = gu2.L().iterator();
                                        while (true) {
                                            if (it4.hasNext()) {
                                                if (!U(str, J, it4.next())) {
                                                    z2 = false;
                                                    break;
                                                }
                                            } else {
                                                break;
                                            }
                                        }
                                    }
                                    z2 = z;
                                    if (!z2) {
                                        r();
                                        h();
                                        Rc2.g(str);
                                        SQLiteDatabase v3 = v();
                                        v3.delete("property_filters", "app_id=? and audience_id=?", new String[]{str, String.valueOf(J)});
                                        v3.delete("event_filters", "app_id=? and audience_id=?", new String[]{str, String.valueOf(J)});
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ArrayList arrayList = new ArrayList();
            for (Gu2 gu22 : list) {
                arrayList.add(gu22.I() ? Integer.valueOf(gu22.J()) : null);
            }
            l0(str, arrayList);
            v.setTransactionSuccessful();
        } finally {
            v.endTransaction();
        }
    }

    @DexIgnore
    public final void O(List<Long> list) {
        h();
        r();
        Rc2.k(list);
        Rc2.m(list.size());
        if (g0()) {
            String join = TextUtils.join(",", list);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(String.valueOf(sb2).length() + 80);
            sb3.append("SELECT COUNT(1) FROM queue WHERE rowid IN ");
            sb3.append(sb2);
            sb3.append(" AND retry_count =  2147483647 LIMIT 1");
            if (h0(sb3.toString(), null) > 0) {
                d().I().a("The number of upload retries exceeds the limit. Will remain unchanged.");
            }
            try {
                SQLiteDatabase v = v();
                StringBuilder sb4 = new StringBuilder(String.valueOf(sb2).length() + 127);
                sb4.append("UPDATE queue SET retry_count = IFNULL(retry_count, 0) + 1 WHERE rowid IN ");
                sb4.append(sb2);
                sb4.append(" AND (retry_count IS NULL OR retry_count < 2147483647)");
                v.execSQL(sb4.toString());
            } catch (SQLiteException e2) {
                d().F().b("Error incrementing retry count. error", e2);
            }
        }
    }

    @DexIgnore
    public final boolean P(Av2 av2, boolean z) {
        h();
        r();
        Rc2.k(av2);
        Rc2.g(av2.H2());
        Rc2.n(av2.i2());
        C0();
        long b = zzm().b();
        if (av2.j2() < b - Zr3.M() || av2.j2() > Zr3.M() + b) {
            d().I().d("Storing bundle outside of the max uploading time span. appId, now, timestamp", Kl3.w(av2.H2()), Long.valueOf(b), Long.valueOf(av2.j2()));
        }
        try {
            byte[] W = n().W(av2.c());
            d().N().b("Saving bundle, size", Integer.valueOf(W.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", av2.H2());
            contentValues.put("bundle_end_timestamp", Long.valueOf(av2.j2()));
            contentValues.put("data", W);
            contentValues.put("has_realtime", Integer.valueOf(z ? 1 : 0));
            if (av2.w0()) {
                contentValues.put("retry_count", Integer.valueOf(av2.H0()));
            }
            try {
                if (v().insert("queue", null, contentValues) != -1) {
                    return true;
                }
                d().F().b("Failed to insert bundle (got -1). appId", Kl3.w(av2.H2()));
                return false;
            } catch (SQLiteException e2) {
                d().F().c("Error storing bundle. appId", Kl3.w(av2.H2()), e2);
                return false;
            }
        } catch (IOException e3) {
            d().F().c("Data loss. Failed to serialize bundle. appId", Kl3.w(av2.H2()), e3);
            return false;
        }
    }

    @DexIgnore
    public final boolean Q(Sg3 sg3, long j2, boolean z) {
        h();
        r();
        Rc2.k(sg3);
        Rc2.g(sg3.a);
        byte[] c = n().x(sg3).c();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", sg3.a);
        contentValues.put("name", sg3.b);
        contentValues.put("timestamp", Long.valueOf(sg3.d));
        contentValues.put("metadata_fingerprint", Long.valueOf(j2));
        contentValues.put("data", c);
        contentValues.put("realtime", Integer.valueOf(z ? 1 : 0));
        try {
            if (v().insert("raw_events", null, contentValues) != -1) {
                return true;
            }
            d().F().b("Failed to insert raw event (got -1). appId", Kl3.w(sg3.a));
            return false;
        } catch (SQLiteException e2) {
            d().F().c("Error storing raw event. appId", Kl3.w(sg3.a), e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean R(Hr3 hr3) {
        Rc2.k(hr3);
        h();
        r();
        if (n0(hr3.a, hr3.c) == null) {
            if (Kr3.c0(hr3.c)) {
                if (h0("select count(1) from user_attributes where app_id=? and name not like '!_%' escape '!'", new String[]{hr3.a}) >= ((long) m().x(hr3.a))) {
                    return false;
                }
            } else if (!"_npa".equals(hr3.c)) {
                if (h0("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{hr3.a, hr3.b}) >= 25) {
                    return false;
                }
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", hr3.a);
        contentValues.put("origin", hr3.b);
        contentValues.put("name", hr3.c);
        contentValues.put("set_timestamp", Long.valueOf(hr3.d));
        K(contentValues, "value", hr3.e);
        try {
            if (v().insertWithOnConflict("user_attributes", null, contentValues, 5) == -1) {
                d().F().b("Failed to insert/update user property (got -1). appId", Kl3.w(hr3.a));
            }
        } catch (SQLiteException e2) {
            d().F().c("Error storing user property. appId", Kl3.w(hr3.a), e2);
        }
        return true;
    }

    @DexIgnore
    public final boolean S(Xr3 xr3) {
        Rc2.k(xr3);
        h();
        r();
        if (n0(xr3.b, xr3.d.c) == null) {
            if (h0("SELECT COUNT(1) FROM conditional_properties WHERE app_id=?", new String[]{xr3.b}) >= 1000) {
                return false;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", xr3.b);
        contentValues.put("origin", xr3.c);
        contentValues.put("name", xr3.d.c);
        K(contentValues, "value", xr3.d.c());
        contentValues.put("active", Boolean.valueOf(xr3.f));
        contentValues.put("trigger_event_name", xr3.g);
        contentValues.put("trigger_timeout", Long.valueOf(xr3.i));
        k();
        contentValues.put("timed_out_event", Kr3.m0(xr3.h));
        contentValues.put("creation_timestamp", Long.valueOf(xr3.e));
        k();
        contentValues.put("triggered_event", Kr3.m0(xr3.j));
        contentValues.put("triggered_timestamp", Long.valueOf(xr3.d.d));
        contentValues.put("time_to_live", Long.valueOf(xr3.k));
        k();
        contentValues.put("expired_event", Kr3.m0(xr3.l));
        try {
            if (v().insertWithOnConflict("conditional_properties", null, contentValues, 5) == -1) {
                d().F().b("Failed to insert/update conditional user property (got -1)", Kl3.w(xr3.b));
            }
        } catch (SQLiteException e2) {
            d().F().c("Error storing conditional user property", Kl3.w(xr3.b), e2);
        }
        return true;
    }

    @DexIgnore
    public final boolean T(String str, int i2, Hu2 hu2) {
        Boolean bool = null;
        Integer num = null;
        r();
        h();
        Rc2.g(str);
        Rc2.k(hu2);
        if (TextUtils.isEmpty(hu2.K())) {
            Nl3 I = d().I();
            Object w = Kl3.w(str);
            if (hu2.I()) {
                num = Integer.valueOf(hu2.J());
            }
            I.d("Event filter had no event name. Audience definition ignored. appId, audienceId, filterId", w, Integer.valueOf(i2), String.valueOf(num));
            return false;
        }
        byte[] c = hu2.c();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("audience_id", Integer.valueOf(i2));
        contentValues.put("filter_id", hu2.I() ? Integer.valueOf(hu2.J()) : null);
        contentValues.put(BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, hu2.K());
        if (hu2.R()) {
            bool = Boolean.valueOf(hu2.S());
        }
        contentValues.put("session_scoped", bool);
        contentValues.put("data", c);
        try {
            if (v().insertWithOnConflict("event_filters", null, contentValues, 5) == -1) {
                d().F().b("Failed to insert event filter (got -1). appId", Kl3.w(str));
            }
            return true;
        } catch (SQLiteException e2) {
            d().F().c("Error storing event filter. appId", Kl3.w(str), e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean U(String str, int i2, Ku2 ku2) {
        Boolean bool = null;
        Integer num = null;
        r();
        h();
        Rc2.g(str);
        Rc2.k(ku2);
        if (TextUtils.isEmpty(ku2.H())) {
            Nl3 I = d().I();
            Object w = Kl3.w(str);
            if (ku2.E()) {
                num = Integer.valueOf(ku2.G());
            }
            I.d("Property filter had no property name. Audience definition ignored. appId, audienceId, filterId", w, Integer.valueOf(i2), String.valueOf(num));
            return false;
        }
        byte[] c = ku2.c();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("audience_id", Integer.valueOf(i2));
        contentValues.put("filter_id", ku2.E() ? Integer.valueOf(ku2.G()) : null);
        contentValues.put("property_name", ku2.H());
        if (ku2.L()) {
            bool = Boolean.valueOf(ku2.M());
        }
        contentValues.put("session_scoped", bool);
        contentValues.put("data", c);
        try {
            if (v().insertWithOnConflict("property_filters", null, contentValues, 5) != -1) {
                return true;
            }
            d().F().b("Failed to insert property filter (got -1). appId", Kl3.w(str));
            return false;
        } catch (SQLiteException e2) {
            d().F().c("Error storing property filter. appId", Kl3.w(str), e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean V(String str, Bundle bundle) {
        h();
        r();
        byte[] c = n().x(new Sg3(this.a, "", str, "dep", 0, 0, bundle)).c();
        d().N().c("Saving default event parameters, appId, data size", j().v(str), Integer.valueOf(c.length));
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("parameters", c);
        try {
            if (v().insertWithOnConflict("default_event_params", null, contentValues, 5) != -1) {
                return true;
            }
            d().F().b("Failed to insert default event parameters (got -1). appId", Kl3.w(str));
            return false;
        } catch (SQLiteException e2) {
            d().F().c("Error storing default event parameters. appId", Kl3.w(str), e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean W(String str, Long l2, long j2, Wu2 wu2) {
        h();
        r();
        Rc2.k(wu2);
        Rc2.g(str);
        Rc2.k(l2);
        byte[] c = wu2.c();
        d().N().c("Saving complex main event, appId, data size", j().v(str), Integer.valueOf(c.length));
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put(LogBuilder.KEY_EVENT_ID, l2);
        contentValues.put("children_to_process", Long.valueOf(j2));
        contentValues.put("main_event", c);
        try {
            if (v().insertWithOnConflict("main_event_params", null, contentValues, 5) != -1) {
                return true;
            }
            d().F().b("Failed to insert complex main event (got -1). appId", Kl3.w(str));
            return false;
        } catch (SQLiteException e2) {
            d().F().c("Error storing complex main event. appId", Kl3.w(str), e2);
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0025, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        d().F().b("Error querying raw events", r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0033, code lost:
        if (0 != 0) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0035, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003d, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0041, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0042, code lost:
        r1 = null;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0025 A[ExcHandler: SQLiteException (r3v0 'e' android.database.sqlite.SQLiteException A[CUSTOM_DECLARE]), Splitter:B:1:0x0003] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final long X() {
        /*
            r6 = this;
            r0 = -1
            r2 = 0
            android.database.sqlite.SQLiteDatabase r3 = r6.v()     // Catch:{ SQLiteException -> 0x0025, all -> 0x0039 }
            java.lang.String r4 = "select rowid from raw_events order by rowid desc limit 1;"
            r5 = 0
            android.database.Cursor r2 = r3.rawQuery(r4, r5)     // Catch:{ SQLiteException -> 0x0025, all -> 0x0039 }
            boolean r3 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x0025 }
            if (r3 != 0) goto L_0x001a
            if (r2 == 0) goto L_0x0019
            r2.close()
        L_0x0019:
            return r0
        L_0x001a:
            r3 = 0
            long r0 = r2.getLong(r3)
            if (r2 == 0) goto L_0x0019
            r2.close()
            goto L_0x0019
        L_0x0025:
            r3 = move-exception
            com.fossil.Kl3 r4 = r6.d()     // Catch:{ all -> 0x0041 }
            com.fossil.Nl3 r4 = r4.F()     // Catch:{ all -> 0x0041 }
            java.lang.String r5 = "Error querying raw events"
            r4.b(r5, r3)     // Catch:{ all -> 0x0041 }
            if (r2 == 0) goto L_0x0019
            r2.close()
            goto L_0x0019
        L_0x0039:
            r0 = move-exception
            r1 = r2
        L_0x003b:
            if (r1 == 0) goto L_0x0040
            r1.close()
        L_0x0040:
            throw r0
        L_0x0041:
            r0 = move-exception
            r1 = r2
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.X():long");
    }

    @DexIgnore
    public final boolean g0() {
        return e().getDatabasePath("google_app_measurement.db").exists();
    }

    @DexIgnore
    public final long h0(String str, String[] strArr) {
        Cursor cursor = null;
        try {
            Cursor rawQuery = v().rawQuery(str, strArr);
            if (rawQuery.moveToFirst()) {
                long j2 = rawQuery.getLong(0);
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return j2;
            }
            throw new SQLiteException("Database returned empty set");
        } catch (SQLiteException e2) {
            d().F().c("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0296  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x02a2  */
    /* JADX WARNING: Removed duplicated region for block: B:78:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.Ll3 i0(java.lang.String r12) {
        /*
        // Method dump skipped, instructions count: 688
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.i0(java.lang.String):com.fossil.Ll3");
    }

    @DexIgnore
    public final List<Xr3> j0(String str, String str2, String str3) {
        Rc2.g(str);
        h();
        r();
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(str);
        StringBuilder sb = new StringBuilder("app_id=?");
        if (!TextUtils.isEmpty(str2)) {
            arrayList.add(str2);
            sb.append(" and origin=?");
        }
        if (!TextUtils.isEmpty(str3)) {
            arrayList.add(String.valueOf(str3).concat(G78.ANY_MARKER));
            sb.append(" and name glob ?");
        }
        return J(sb.toString(), (String[]) arrayList.toArray(new String[arrayList.size()]));
    }

    @DexIgnore
    public final void k0(String str, String str2) {
        Rc2.g(str);
        Rc2.g(str2);
        h();
        r();
        try {
            v().delete("user_attributes", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e2) {
            d().F().d("Error deleting user property. appId", Kl3.w(str), j().z(str2), e2);
        }
    }

    @DexIgnore
    public final boolean l0(String str, List<Integer> list) {
        Rc2.g(str);
        r();
        h();
        SQLiteDatabase v = v();
        try {
            long h0 = h0("select count(1) from audience_filter_values where app_id=?", new String[]{str});
            int max = Math.max(0, Math.min(2000, m().u(str, Xg3.F)));
            if (h0 <= ((long) max)) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < list.size(); i2++) {
                Integer num = list.get(i2);
                if (num == null || !(num instanceof Integer)) {
                    return false;
                }
                arrayList.add(Integer.toString(num.intValue()));
            }
            String join = TextUtils.join(",", arrayList);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(String.valueOf(sb2).length() + ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            sb3.append("audience_id in (select audience_id from audience_filter_values where app_id=? and audience_id not in ");
            sb3.append(sb2);
            sb3.append(" order by rowid desc limit -1 offset ?)");
            return v.delete("audience_filter_values", sb3.toString(), new String[]{str, Integer.toString(max)}) > 0;
        } catch (SQLiteException e2) {
            d().F().c("Database error querying filters. appId", Kl3.w(str), e2);
            return false;
        }
    }

    @DexIgnore
    public final long m0(String str) {
        Rc2.g(str);
        h();
        r();
        try {
            return (long) v().delete("raw_events", "rowid in (select rowid from raw_events where app_id=? order by rowid desc limit -1 offset ?)", new String[]{str, String.valueOf(Math.max(0, Math.min(1000000, m().u(str, Xg3.p))))});
        } catch (SQLiteException e2) {
            d().F().c("Error deleting over the limit events. appId", Kl3.w(str), e2);
            return 0;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.Hr3 n0(java.lang.String r10, java.lang.String r11) {
        /*
            r9 = this;
            r8 = 0
            com.fossil.Rc2.g(r10)
            com.fossil.Rc2.g(r11)
            r9.h()
            r9.r()
            android.database.sqlite.SQLiteDatabase r0 = r9.v()     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009b }
            java.lang.String r1 = "user_attributes"
            r2 = 3
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009b }
            r3 = 0
            java.lang.String r4 = "set_timestamp"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009b }
            r3 = 1
            java.lang.String r4 = "value"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009b }
            r3 = 2
            java.lang.String r4 = "origin"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009b }
            java.lang.String r3 = "app_id=? and name=?"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009b }
            r5 = 0
            r4[r5] = r10     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009b }
            r5 = 1
            r4[r5] = r11     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009b }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00a3, all -> 0x009b }
            boolean r0 = r7.moveToFirst()     // Catch:{ SQLiteException -> 0x0098, all -> 0x00ab }
            if (r0 != 0) goto L_0x0043
            if (r7 == 0) goto L_0x0042
            r7.close()
        L_0x0042:
            return r8
        L_0x0043:
            r0 = 0
            long r4 = r7.getLong(r0)
            r0 = 1
            java.lang.Object r6 = r9.E(r7, r0)     // Catch:{ SQLiteException -> 0x0077, all -> 0x00a6 }
            r0 = 2
            java.lang.String r2 = r7.getString(r0)     // Catch:{ SQLiteException -> 0x0077, all -> 0x00a6 }
            com.fossil.Hr3 r0 = new com.fossil.Hr3     // Catch:{ SQLiteException -> 0x0077, all -> 0x00a6 }
            r1 = r10
            r3 = r11
            r0.<init>(r1, r2, r3, r4, r6)     // Catch:{ SQLiteException -> 0x0077, all -> 0x00a6 }
            boolean r1 = r7.moveToNext()     // Catch:{ SQLiteException -> 0x0077, all -> 0x00a6 }
            if (r1 == 0) goto L_0x0070
            com.fossil.Kl3 r1 = r9.d()     // Catch:{ SQLiteException -> 0x0077, all -> 0x00a6 }
            com.fossil.Nl3 r1 = r1.F()     // Catch:{ SQLiteException -> 0x0077, all -> 0x00a6 }
            java.lang.String r2 = "Got multiple records for user property, expected one. appId"
            java.lang.Object r3 = com.fossil.Kl3.w(r10)     // Catch:{ SQLiteException -> 0x0077, all -> 0x00a6 }
            r1.b(r2, r3)     // Catch:{ SQLiteException -> 0x0077, all -> 0x00a6 }
        L_0x0070:
            if (r7 == 0) goto L_0x0075
            r7.close()
        L_0x0075:
            r8 = r0
            goto L_0x0042
        L_0x0077:
            r0 = move-exception
            r1 = r7
        L_0x0079:
            com.fossil.Kl3 r2 = r9.d()     // Catch:{ all -> 0x00a8 }
            com.fossil.Nl3 r2 = r2.F()     // Catch:{ all -> 0x00a8 }
            java.lang.String r3 = "Error querying user property. appId"
            java.lang.Object r4 = com.fossil.Kl3.w(r10)     // Catch:{ all -> 0x00a8 }
            com.fossil.Il3 r5 = r9.j()     // Catch:{ all -> 0x00a8 }
            java.lang.String r5 = r5.z(r11)     // Catch:{ all -> 0x00a8 }
            r2.d(r3, r4, r5, r0)     // Catch:{ all -> 0x00a8 }
            if (r1 == 0) goto L_0x0042
            r1.close()
            goto L_0x0042
        L_0x0098:
            r0 = move-exception
            r1 = r7
            goto L_0x0079
        L_0x009b:
            r0 = move-exception
            r7 = r8
        L_0x009d:
            if (r7 == 0) goto L_0x00a2
            r7.close()
        L_0x00a2:
            throw r0
        L_0x00a3:
            r0 = move-exception
            r1 = r8
            goto L_0x0079
        L_0x00a6:
            r0 = move-exception
            goto L_0x009d
        L_0x00a8:
            r0 = move-exception
            r7 = r1
            goto L_0x009d
        L_0x00ab:
            r0 = move-exception
            goto L_0x009d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.n0(java.lang.String, java.lang.String):com.fossil.Hr3");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0154  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.Xr3 o0(java.lang.String r22, java.lang.String r23) {
        /*
        // Method dump skipped, instructions count: 352
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.o0(java.lang.String, java.lang.String):com.fossil.Xr3");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:30:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final byte[] p0(java.lang.String r10) {
        /*
            r9 = this;
            r8 = 0
            com.fossil.Rc2.g(r10)
            r9.h()
            r9.r()
            android.database.sqlite.SQLiteDatabase r0 = r9.v()     // Catch:{ SQLiteException -> 0x0056, all -> 0x0077 }
            java.lang.String r1 = "apps"
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x0056, all -> 0x0077 }
            r3 = 0
            java.lang.String r4 = "remote_config"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x0056, all -> 0x0077 }
            java.lang.String r3 = "app_id=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0056, all -> 0x0077 }
            r5 = 0
            r4[r5] = r10     // Catch:{ SQLiteException -> 0x0056, all -> 0x0077 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0056, all -> 0x0077 }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0079 }
            if (r0 != 0) goto L_0x0033
            if (r1 == 0) goto L_0x0032
            r1.close()
        L_0x0032:
            return r8
        L_0x0033:
            r0 = 0
            byte[] r0 = r1.getBlob(r0)
            boolean r2 = r1.moveToNext()
            if (r2 == 0) goto L_0x004f
            com.fossil.Kl3 r2 = r9.d()
            com.fossil.Nl3 r2 = r2.F()
            java.lang.String r3 = "Got multiple records for app config, expected one. appId"
            java.lang.Object r4 = com.fossil.Kl3.w(r10)
            r2.b(r3, r4)
        L_0x004f:
            if (r1 == 0) goto L_0x0054
            r1.close()
        L_0x0054:
            r8 = r0
            goto L_0x0032
        L_0x0056:
            r0 = move-exception
            r1 = r8
        L_0x0058:
            com.fossil.Kl3 r2 = r9.d()     // Catch:{ all -> 0x006f }
            com.fossil.Nl3 r2 = r2.F()     // Catch:{ all -> 0x006f }
            java.lang.String r3 = "Error querying remote config. appId"
            java.lang.Object r4 = com.fossil.Kl3.w(r10)     // Catch:{ all -> 0x006f }
            r2.c(r3, r4, r0)     // Catch:{ all -> 0x006f }
            if (r1 == 0) goto L_0x0032
            r1.close()
            goto L_0x0032
        L_0x006f:
            r0 = move-exception
            r8 = r1
        L_0x0071:
            if (r8 == 0) goto L_0x0076
            r8.close()
        L_0x0076:
            throw r0
        L_0x0077:
            r0 = move-exception
            goto L_0x0071
        L_0x0079:
            r0 = move-exception
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.p0(java.lang.String):byte[]");
    }

    @DexIgnore
    public final int q0(String str, String str2) {
        Rc2.g(str);
        Rc2.g(str2);
        h();
        r();
        try {
            return v().delete("conditional_properties", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e2) {
            d().F().d("Error deleting conditional property", Kl3.w(str), j().z(str2), e2);
            return 0;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b0  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:45:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<com.fossil.Hu2>> r0(java.lang.String r11) {
        /*
            r10 = this;
            r9 = 0
            com.fossil.Rc2.g(r11)
            com.fossil.Zi0 r8 = new com.fossil.Zi0
            r8.<init>()
            android.database.sqlite.SQLiteDatabase r0 = r10.v()
            java.lang.String r1 = "event_filters"
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00b4 }
            r3 = 0
            java.lang.String r4 = "audience_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00b4 }
            r3 = 1
            java.lang.String r4 = "data"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00b4 }
            java.lang.String r3 = "app_id=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00b4 }
            r5 = 0
            r4[r5] = r11     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00b4 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00bc, all -> 0x00b4 }
            boolean r0 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x0097, all -> 0x00bf }
            if (r0 != 0) goto L_0x003b
            java.util.Map r0 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x0097, all -> 0x00bf }
            if (r2 == 0) goto L_0x003a
            r2.close()
        L_0x003a:
            return r0
        L_0x003b:
            r0 = 1
            byte[] r1 = r2.getBlob(r0)
            com.fossil.Hu2$Ai r0 = com.fossil.Hu2.T()     // Catch:{ IOException -> 0x0084 }
            com.fossil.Gr3.z(r0, r1)     // Catch:{ IOException -> 0x0084 }
            com.fossil.Hu2$Ai r0 = (com.fossil.Hu2.Ai) r0     // Catch:{ IOException -> 0x0084 }
            com.fossil.M23 r0 = r0.h()     // Catch:{ IOException -> 0x0084 }
            com.fossil.E13 r0 = (com.fossil.E13) r0     // Catch:{ IOException -> 0x0084 }
            com.fossil.Hu2 r0 = (com.fossil.Hu2) r0     // Catch:{ IOException -> 0x0084 }
            boolean r1 = r0.N()
            if (r1 == 0) goto L_0x0077
            r1 = 0
            int r3 = r2.getInt(r1)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r3)
            java.lang.Object r1 = r8.get(r1)
            java.util.List r1 = (java.util.List) r1
            if (r1 != 0) goto L_0x0074
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r8.put(r3, r1)
        L_0x0074:
            r1.add(r0)
        L_0x0077:
            boolean r0 = r2.moveToNext()
            if (r0 != 0) goto L_0x003b
            if (r2 == 0) goto L_0x0082
            r2.close()
        L_0x0082:
            r0 = r8
            goto L_0x003a
        L_0x0084:
            r0 = move-exception
            com.fossil.Kl3 r1 = r10.d()
            com.fossil.Nl3 r1 = r1.F()
            java.lang.String r3 = "Failed to merge filter. appId"
            java.lang.Object r4 = com.fossil.Kl3.w(r11)
            r1.c(r3, r4, r0)
            goto L_0x0077
        L_0x0097:
            r0 = move-exception
            r1 = r2
        L_0x0099:
            com.fossil.Kl3 r2 = r10.d()     // Catch:{ all -> 0x00c2 }
            com.fossil.Nl3 r2 = r2.F()     // Catch:{ all -> 0x00c2 }
            java.lang.String r3 = "Database error querying filters. appId"
            java.lang.Object r4 = com.fossil.Kl3.w(r11)     // Catch:{ all -> 0x00c2 }
            r2.c(r3, r4, r0)     // Catch:{ all -> 0x00c2 }
            java.util.Map r0 = java.util.Collections.emptyMap()     // Catch:{ all -> 0x00c2 }
            if (r1 == 0) goto L_0x003a
            r1.close()
            goto L_0x003a
        L_0x00b4:
            r0 = move-exception
            r1 = r9
        L_0x00b6:
            if (r1 == 0) goto L_0x00bb
            r1.close()
        L_0x00bb:
            throw r0
        L_0x00bc:
            r0 = move-exception
            r1 = r9
            goto L_0x0099
        L_0x00bf:
            r0 = move-exception
            r1 = r2
            goto L_0x00b6
        L_0x00c2:
            r0 = move-exception
            goto L_0x00b6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.r0(java.lang.String):java.util.Map");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<java.lang.Integer>> s0(java.lang.String r7) {
        /*
            r6 = this;
            r2 = 0
            r6.r()
            r6.h()
            com.fossil.Rc2.g(r7)
            com.fossil.Zi0 r3 = new com.fossil.Zi0
            r3.<init>()
            android.database.sqlite.SQLiteDatabase r0 = r6.v()
            java.lang.String r1 = "select audience_id, filter_id from event_filters where app_id = ? and session_scoped = 1 UNION select audience_id, filter_id from property_filters where app_id = ? and session_scoped = 1;"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0068, all -> 0x00a5 }
            r5 = 0
            r4[r5] = r7     // Catch:{ SQLiteException -> 0x0068, all -> 0x00a5 }
            r5 = 1
            r4[r5] = r7     // Catch:{ SQLiteException -> 0x0068, all -> 0x00a5 }
            android.database.Cursor r1 = r0.rawQuery(r1, r4)     // Catch:{ SQLiteException -> 0x0068, all -> 0x00a5 }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x00a7 }
            if (r0 != 0) goto L_0x0032
            java.util.Map r2 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x00a7 }
            if (r1 == 0) goto L_0x0031
            r1.close()
        L_0x0031:
            return r2
        L_0x0032:
            r0 = 0
            int r4 = r1.getInt(r0)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)
            java.lang.Object r0 = r3.get(r0)
            java.util.List r0 = (java.util.List) r0
            if (r0 != 0) goto L_0x004f
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r3.put(r4, r0)
        L_0x004f:
            r4 = 1
            int r4 = r1.getInt(r4)
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r0.add(r4)
            boolean r0 = r1.moveToNext()
            if (r0 != 0) goto L_0x0032
            if (r1 == 0) goto L_0x0066
            r1.close()
        L_0x0066:
            r2 = r3
            goto L_0x0031
        L_0x0068:
            r0 = move-exception
            r1 = r2
        L_0x006a:
            com.fossil.Kl3 r3 = r6.d()     // Catch:{ all -> 0x009d }
            com.fossil.Nl3 r3 = r3.F()     // Catch:{ all -> 0x009d }
            java.lang.String r4 = "Database error querying scoped filters. appId"
            java.lang.Object r5 = com.fossil.Kl3.w(r7)     // Catch:{ all -> 0x009d }
            r3.c(r4, r5, r0)     // Catch:{ all -> 0x009d }
            boolean r0 = com.fossil.Q63.a()     // Catch:{ all -> 0x009d }
            if (r0 == 0) goto L_0x0097
            com.fossil.Zr3 r0 = r6.m()     // Catch:{ all -> 0x009d }
            com.fossil.Zk3<java.lang.Boolean> r3 = com.fossil.Xg3.T0     // Catch:{ all -> 0x009d }
            boolean r0 = r0.B(r7, r3)     // Catch:{ all -> 0x009d }
            if (r0 == 0) goto L_0x0097
            java.util.Map r2 = java.util.Collections.emptyMap()     // Catch:{ all -> 0x009d }
            if (r1 == 0) goto L_0x0031
            r1.close()
            goto L_0x0031
        L_0x0097:
            if (r1 == 0) goto L_0x0031
            r1.close()
            goto L_0x0031
        L_0x009d:
            r0 = move-exception
            r2 = r1
        L_0x009f:
            if (r2 == 0) goto L_0x00a4
            r2.close()
        L_0x00a4:
            throw r0
        L_0x00a5:
            r0 = move-exception
            goto L_0x009f
        L_0x00a7:
            r0 = move-exception
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.s0(java.lang.String):java.util.Map");
    }

    @DexIgnore
    @Override // com.fossil.Zq3
    public final boolean t() {
        return false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<com.fossil.Hu2>> t0(java.lang.String r11, java.lang.String r12) {
        /*
            r10 = this;
            r8 = 0
            r10.r()
            r10.h()
            com.fossil.Rc2.g(r11)
            com.fossil.Rc2.g(r12)
            com.fossil.Zi0 r9 = new com.fossil.Zi0
            r9.<init>()
            android.database.sqlite.SQLiteDatabase r0 = r10.v()
            java.lang.String r1 = "event_filters"
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00e2 }
            r3 = 0
            java.lang.String r4 = "audience_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00e2 }
            r3 = 1
            java.lang.String r4 = "data"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00e2 }
            java.lang.String r3 = "app_id=? AND event_name=?"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00e2 }
            r5 = 0
            r4[r5] = r11     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00e2 }
            r5 = 1
            r4[r5] = r12     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00e2 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00e2 }
            boolean r0 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x009d, all -> 0x00d7 }
            if (r0 != 0) goto L_0x0047
            java.util.Map r8 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x009d, all -> 0x00d7 }
            if (r2 == 0) goto L_0x0046
            r2.close()
        L_0x0046:
            return r8
        L_0x0047:
            r0 = 1
            byte[] r1 = r2.getBlob(r0)
            com.fossil.Hu2$Ai r0 = com.fossil.Hu2.T()     // Catch:{ IOException -> 0x008a }
            com.fossil.Gr3.z(r0, r1)     // Catch:{ IOException -> 0x008a }
            com.fossil.Hu2$Ai r0 = (com.fossil.Hu2.Ai) r0     // Catch:{ IOException -> 0x008a }
            com.fossil.M23 r0 = r0.h()     // Catch:{ IOException -> 0x008a }
            com.fossil.E13 r0 = (com.fossil.E13) r0     // Catch:{ IOException -> 0x008a }
            com.fossil.Hu2 r0 = (com.fossil.Hu2) r0     // Catch:{ IOException -> 0x008a }
            r1 = 0
            int r3 = r2.getInt(r1)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r3)
            java.lang.Object r1 = r9.get(r1)
            java.util.List r1 = (java.util.List) r1
            if (r1 != 0) goto L_0x007a
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r9.put(r3, r1)
        L_0x007a:
            r1.add(r0)
        L_0x007d:
            boolean r0 = r2.moveToNext()
            if (r0 != 0) goto L_0x0047
            if (r2 == 0) goto L_0x0088
            r2.close()
        L_0x0088:
            r8 = r9
            goto L_0x0046
        L_0x008a:
            r0 = move-exception
            com.fossil.Kl3 r1 = r10.d()
            com.fossil.Nl3 r1 = r1.F()
            java.lang.String r3 = "Failed to merge filter. appId"
            java.lang.Object r4 = com.fossil.Kl3.w(r11)
            r1.c(r3, r4, r0)
            goto L_0x007d
        L_0x009d:
            r0 = move-exception
            r1 = r2
        L_0x009f:
            com.fossil.Kl3 r2 = r10.d()     // Catch:{ all -> 0x00df }
            com.fossil.Nl3 r2 = r2.F()     // Catch:{ all -> 0x00df }
            java.lang.String r3 = "Database error querying filters. appId"
            java.lang.Object r4 = com.fossil.Kl3.w(r11)     // Catch:{ all -> 0x00df }
            r2.c(r3, r4, r0)     // Catch:{ all -> 0x00df }
            boolean r0 = com.fossil.Q63.a()     // Catch:{ all -> 0x00df }
            if (r0 == 0) goto L_0x00d0
            com.fossil.Zr3 r0 = r10.m()     // Catch:{ all -> 0x00df }
            com.fossil.Zk3<java.lang.Boolean> r2 = com.fossil.Xg3.T0     // Catch:{ all -> 0x00df }
            boolean r0 = r0.B(r11, r2)     // Catch:{ all -> 0x00df }
            if (r0 == 0) goto L_0x00d0
            java.util.Map r8 = java.util.Collections.emptyMap()     // Catch:{ all -> 0x00df }
            if (r1 == 0) goto L_0x0046
            r1.close()
            goto L_0x0046
        L_0x00cd:
            r0 = move-exception
            r1 = r8
            goto L_0x009f
        L_0x00d0:
            if (r1 == 0) goto L_0x0046
            r1.close()
            goto L_0x0046
        L_0x00d7:
            r0 = move-exception
        L_0x00d8:
            r8 = r2
        L_0x00d9:
            if (r8 == 0) goto L_0x00de
            r8.close()
        L_0x00de:
            throw r0
        L_0x00df:
            r0 = move-exception
            r2 = r1
            goto L_0x00d8
        L_0x00e2:
            r0 = move-exception
            goto L_0x00d9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.t0(java.lang.String, java.lang.String):java.util.Map");
    }

    @DexIgnore
    public final void u() {
        r();
        v().setTransactionSuccessful();
    }

    @DexIgnore
    public final void u0() {
        r();
        v().beginTransaction();
    }

    @DexIgnore
    public final SQLiteDatabase v() {
        h();
        try {
            return this.d.getWritableDatabase();
        } catch (SQLiteException e2) {
            d().I().b("Error opening database", e2);
            throw e2;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:54:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, com.fossil.Cv2> v0(java.lang.String r10) {
        /*
            r9 = this;
            r8 = 0
            r9.r()
            r9.h()
            com.fossil.Rc2.g(r10)
            android.database.sqlite.SQLiteDatabase r0 = r9.v()
            java.lang.String r1 = "audience_filter_values"
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00df }
            r3 = 0
            java.lang.String r4 = "audience_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00df }
            r3 = 1
            java.lang.String r4 = "current_results"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00df }
            java.lang.String r3 = "app_id=?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00df }
            r5 = 0
            r4[r5] = r10     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00df }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00df }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x009f }
            if (r0 != 0) goto L_0x0054
            boolean r0 = com.fossil.Q63.a()     // Catch:{ SQLiteException -> 0x009f }
            if (r0 == 0) goto L_0x004e
            com.fossil.Zr3 r0 = r9.m()     // Catch:{ SQLiteException -> 0x009f }
            com.fossil.Zk3<java.lang.Boolean> r2 = com.fossil.Xg3.T0     // Catch:{ SQLiteException -> 0x009f }
            boolean r0 = r0.B(r10, r2)     // Catch:{ SQLiteException -> 0x009f }
            if (r0 == 0) goto L_0x004e
            java.util.Map r8 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x009f }
            if (r1 == 0) goto L_0x004d
            r1.close()
        L_0x004d:
            return r8
        L_0x004e:
            if (r1 == 0) goto L_0x004d
            r1.close()
            goto L_0x004d
        L_0x0054:
            com.fossil.Zi0 r2 = new com.fossil.Zi0
            r2.<init>()
        L_0x0059:
            r0 = 0
            int r3 = r1.getInt(r0)
            r0 = 1
            byte[] r4 = r1.getBlob(r0)
            com.fossil.Cv2$Ai r0 = com.fossil.Cv2.b0()     // Catch:{ IOException -> 0x0088 }
            com.fossil.Gr3.z(r0, r4)     // Catch:{ IOException -> 0x0088 }
            com.fossil.Cv2$Ai r0 = (com.fossil.Cv2.Ai) r0     // Catch:{ IOException -> 0x0088 }
            com.fossil.M23 r0 = r0.h()     // Catch:{ IOException -> 0x0088 }
            com.fossil.E13 r0 = (com.fossil.E13) r0     // Catch:{ IOException -> 0x0088 }
            com.fossil.Cv2 r0 = (com.fossil.Cv2) r0     // Catch:{ IOException -> 0x0088 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2.put(r3, r0)
        L_0x007b:
            boolean r0 = r1.moveToNext()
            if (r0 != 0) goto L_0x0059
            if (r1 == 0) goto L_0x0086
            r1.close()
        L_0x0086:
            r8 = r2
            goto L_0x004d
        L_0x0088:
            r0 = move-exception
            com.fossil.Kl3 r4 = r9.d()
            com.fossil.Nl3 r4 = r4.F()
            java.lang.String r5 = "Failed to merge filter results. appId, audienceId, error"
            java.lang.Object r6 = com.fossil.Kl3.w(r10)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r4.d(r5, r6, r3, r0)
            goto L_0x007b
        L_0x009f:
            r0 = move-exception
        L_0x00a0:
            com.fossil.Kl3 r2 = r9.d()     // Catch:{ all -> 0x00d7 }
            com.fossil.Nl3 r2 = r2.F()     // Catch:{ all -> 0x00d7 }
            java.lang.String r3 = "Database error querying filter results. appId"
            java.lang.Object r4 = com.fossil.Kl3.w(r10)     // Catch:{ all -> 0x00d7 }
            r2.c(r3, r4, r0)     // Catch:{ all -> 0x00d7 }
            boolean r0 = com.fossil.Q63.a()     // Catch:{ all -> 0x00d7 }
            if (r0 == 0) goto L_0x00d0
            com.fossil.Zr3 r0 = r9.m()     // Catch:{ all -> 0x00d7 }
            com.fossil.Zk3<java.lang.Boolean> r2 = com.fossil.Xg3.T0     // Catch:{ all -> 0x00d7 }
            boolean r0 = r0.B(r10, r2)     // Catch:{ all -> 0x00d7 }
            if (r0 == 0) goto L_0x00d0
            java.util.Map r8 = java.util.Collections.emptyMap()     // Catch:{ all -> 0x00d7 }
            if (r1 == 0) goto L_0x004d
            r1.close()
            goto L_0x004d
        L_0x00cd:
            r0 = move-exception
            r1 = r8
            goto L_0x00a0
        L_0x00d0:
            if (r1 == 0) goto L_0x004d
            r1.close()
            goto L_0x004d
        L_0x00d7:
            r0 = move-exception
            r8 = r1
        L_0x00d9:
            if (r8 == 0) goto L_0x00de
            r8.close()
        L_0x00de:
            throw r0
        L_0x00df:
            r0 = move-exception
            goto L_0x00d9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.v0(java.lang.String):java.util.Map");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String w() {
        /*
            r5 = this;
            r2 = 0
            android.database.sqlite.SQLiteDatabase r0 = r5.v()
            java.lang.String r1 = "select app_id from queue order by has_realtime desc, rowid asc limit 1;"
            r3 = 0
            android.database.Cursor r1 = r0.rawQuery(r1, r3)     // Catch:{ SQLiteException -> 0x0023, all -> 0x0040 }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0042 }
            if (r0 == 0) goto L_0x001d
            r0 = 0
            java.lang.String r2 = r1.getString(r0)     // Catch:{ SQLiteException -> 0x0042 }
            if (r1 == 0) goto L_0x001c
            r1.close()
        L_0x001c:
            return r2
        L_0x001d:
            if (r1 == 0) goto L_0x001c
            r1.close()
            goto L_0x001c
        L_0x0023:
            r0 = move-exception
            r1 = r2
        L_0x0025:
            com.fossil.Kl3 r3 = r5.d()     // Catch:{ all -> 0x0038 }
            com.fossil.Nl3 r3 = r3.F()     // Catch:{ all -> 0x0038 }
            java.lang.String r4 = "Database error getting next bundle app id"
            r3.b(r4, r0)     // Catch:{ all -> 0x0038 }
            if (r1 == 0) goto L_0x001c
            r1.close()
            goto L_0x001c
        L_0x0038:
            r0 = move-exception
            r2 = r1
        L_0x003a:
            if (r2 == 0) goto L_0x003f
            r2.close()
        L_0x003f:
            throw r0
        L_0x0040:
            r0 = move-exception
            goto L_0x003a
        L_0x0042:
            r0 = move-exception
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.w():java.lang.String");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<com.fossil.Ku2>> w0(java.lang.String r11, java.lang.String r12) {
        /*
            r10 = this;
            r8 = 0
            r10.r()
            r10.h()
            com.fossil.Rc2.g(r11)
            com.fossil.Rc2.g(r12)
            com.fossil.Zi0 r9 = new com.fossil.Zi0
            r9.<init>()
            android.database.sqlite.SQLiteDatabase r0 = r10.v()
            java.lang.String r1 = "property_filters"
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00e2 }
            r3 = 0
            java.lang.String r4 = "audience_id"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00e2 }
            r3 = 1
            java.lang.String r4 = "data"
            r2[r3] = r4     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00e2 }
            java.lang.String r3 = "app_id=? AND property_name=?"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00e2 }
            r5 = 0
            r4[r5] = r11     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00e2 }
            r5 = 1
            r4[r5] = r12     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00e2 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00cd, all -> 0x00e2 }
            boolean r0 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x009d, all -> 0x00d7 }
            if (r0 != 0) goto L_0x0047
            java.util.Map r8 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x009d, all -> 0x00d7 }
            if (r2 == 0) goto L_0x0046
            r2.close()
        L_0x0046:
            return r8
        L_0x0047:
            r0 = 1
            byte[] r1 = r2.getBlob(r0)
            com.fossil.Ku2$Ai r0 = com.fossil.Ku2.N()     // Catch:{ IOException -> 0x008a }
            com.fossil.Gr3.z(r0, r1)     // Catch:{ IOException -> 0x008a }
            com.fossil.Ku2$Ai r0 = (com.fossil.Ku2.Ai) r0     // Catch:{ IOException -> 0x008a }
            com.fossil.M23 r0 = r0.h()     // Catch:{ IOException -> 0x008a }
            com.fossil.E13 r0 = (com.fossil.E13) r0     // Catch:{ IOException -> 0x008a }
            com.fossil.Ku2 r0 = (com.fossil.Ku2) r0     // Catch:{ IOException -> 0x008a }
            r1 = 0
            int r3 = r2.getInt(r1)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r3)
            java.lang.Object r1 = r9.get(r1)
            java.util.List r1 = (java.util.List) r1
            if (r1 != 0) goto L_0x007a
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r9.put(r3, r1)
        L_0x007a:
            r1.add(r0)
        L_0x007d:
            boolean r0 = r2.moveToNext()
            if (r0 != 0) goto L_0x0047
            if (r2 == 0) goto L_0x0088
            r2.close()
        L_0x0088:
            r8 = r9
            goto L_0x0046
        L_0x008a:
            r0 = move-exception
            com.fossil.Kl3 r1 = r10.d()
            com.fossil.Nl3 r1 = r1.F()
            java.lang.String r3 = "Failed to merge filter"
            java.lang.Object r4 = com.fossil.Kl3.w(r11)
            r1.c(r3, r4, r0)
            goto L_0x007d
        L_0x009d:
            r0 = move-exception
            r1 = r2
        L_0x009f:
            com.fossil.Kl3 r2 = r10.d()     // Catch:{ all -> 0x00df }
            com.fossil.Nl3 r2 = r2.F()     // Catch:{ all -> 0x00df }
            java.lang.String r3 = "Database error querying filters. appId"
            java.lang.Object r4 = com.fossil.Kl3.w(r11)     // Catch:{ all -> 0x00df }
            r2.c(r3, r4, r0)     // Catch:{ all -> 0x00df }
            boolean r0 = com.fossil.Q63.a()     // Catch:{ all -> 0x00df }
            if (r0 == 0) goto L_0x00d0
            com.fossil.Zr3 r0 = r10.m()     // Catch:{ all -> 0x00df }
            com.fossil.Zk3<java.lang.Boolean> r2 = com.fossil.Xg3.T0     // Catch:{ all -> 0x00df }
            boolean r0 = r0.B(r11, r2)     // Catch:{ all -> 0x00df }
            if (r0 == 0) goto L_0x00d0
            java.util.Map r8 = java.util.Collections.emptyMap()     // Catch:{ all -> 0x00df }
            if (r1 == 0) goto L_0x0046
            r1.close()
            goto L_0x0046
        L_0x00cd:
            r0 = move-exception
            r1 = r8
            goto L_0x009f
        L_0x00d0:
            if (r1 == 0) goto L_0x0046
            r1.close()
            goto L_0x0046
        L_0x00d7:
            r0 = move-exception
        L_0x00d8:
            r8 = r2
        L_0x00d9:
            if (r8 == 0) goto L_0x00de
            r8.close()
        L_0x00de:
            throw r0
        L_0x00df:
            r0 = move-exception
            r2 = r1
            goto L_0x00d8
        L_0x00e2:
            r0 = move-exception
            goto L_0x00d9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.w0(java.lang.String, java.lang.String):java.util.Map");
    }

    @DexIgnore
    public final long x(Av2 av2) throws IOException {
        h();
        r();
        Rc2.k(av2);
        Rc2.g(av2.H2());
        byte[] c = av2.c();
        long v = n().v(c);
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", av2.H2());
        contentValues.put("metadata_fingerprint", Long.valueOf(v));
        contentValues.put("metadata", c);
        try {
            v().insertWithOnConflict("raw_events_metadata", null, contentValues, 4);
            return v;
        } catch (SQLiteException e2) {
            d().F().c("Error storing raw event metadata. appId", Kl3.w(av2.H2()), e2);
            throw e2;
        }
    }

    @DexIgnore
    public final long x0(String str) {
        Rc2.g(str);
        return y("select count(1) from events where app_id=? and name not like '!_%' escape '!'", new String[]{str}, 0);
    }

    @DexIgnore
    public final long y(String str, String[] strArr, long j2) {
        Cursor cursor = null;
        try {
            Cursor rawQuery = v().rawQuery(str, strArr);
            if (rawQuery.moveToFirst()) {
                j2 = rawQuery.getLong(0);
                if (rawQuery != null) {
                    rawQuery.close();
                }
            } else if (rawQuery != null) {
                rawQuery.close();
            }
            return j2;
        } catch (SQLiteException e2) {
            d().F().c("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final long y0(String str, String str2) {
        SQLiteException e2;
        long j2;
        Rc2.g(str);
        Rc2.g(str2);
        h();
        r();
        SQLiteDatabase v = v();
        v.beginTransaction();
        try {
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 32);
            sb.append("select ");
            sb.append(str2);
            sb.append(" from app2 where app_id=?");
            try {
                j2 = y(sb.toString(), new String[]{str}, -1);
                if (j2 == -1) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("app_id", str);
                    contentValues.put("first_open_count", (Integer) 0);
                    contentValues.put("previous_install_count", (Integer) 0);
                    if (v.insertWithOnConflict("app2", null, contentValues, 5) == -1) {
                        d().F().c("Failed to insert column (got -1). appId", Kl3.w(str), str2);
                        v.endTransaction();
                        return -1;
                    }
                    j2 = 0;
                }
                try {
                    ContentValues contentValues2 = new ContentValues();
                    contentValues2.put("app_id", str);
                    contentValues2.put(str2, Long.valueOf(1 + j2));
                    if (((long) v.update("app2", contentValues2, "app_id = ?", new String[]{str})) == 0) {
                        d().F().c("Failed to update column (got 0). appId", Kl3.w(str), str2);
                        v.endTransaction();
                        return -1;
                    }
                    v.setTransactionSuccessful();
                    v.endTransaction();
                    return j2;
                } catch (SQLiteException e3) {
                    e2 = e3;
                    try {
                        d().F().d("Error inserting column. appId", Kl3.w(str), str2, e2);
                        v.endTransaction();
                        return j2;
                    } catch (Throwable th) {
                        th = th;
                        v.endTransaction();
                        throw th;
                    }
                }
            } catch (SQLiteException e4) {
                e2 = e4;
                j2 = 0;
                d().F().d("Error inserting column. appId", Kl3.w(str), str2, e2);
                v.endTransaction();
                return j2;
            }
        } catch (SQLiteException e5) {
            e2 = e5;
            j2 = 0;
            d().F().d("Error inserting column. appId", Kl3.w(str), str2, e2);
            v.endTransaction();
            return j2;
        } catch (Throwable th2) {
            th = th2;
            v.endTransaction();
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0091  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.util.Pair<com.fossil.Wu2, java.lang.Long> z(java.lang.String r7, java.lang.Long r8) {
        /*
            r6 = this;
            r2 = 0
            r6.h()
            r6.r()
            android.database.sqlite.SQLiteDatabase r0 = r6.v()     // Catch:{ SQLiteException -> 0x0078, all -> 0x0095 }
            java.lang.String r1 = "select main_event, children_to_process from main_event_params where app_id=? and event_id=?"
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x0078, all -> 0x0095 }
            r4 = 0
            r3[r4] = r7     // Catch:{ SQLiteException -> 0x0078, all -> 0x0095 }
            r4 = 1
            java.lang.String r5 = java.lang.String.valueOf(r8)     // Catch:{ SQLiteException -> 0x0078, all -> 0x0095 }
            r3[r4] = r5     // Catch:{ SQLiteException -> 0x0078, all -> 0x0095 }
            android.database.Cursor r1 = r0.rawQuery(r1, r3)     // Catch:{ SQLiteException -> 0x0078, all -> 0x0095 }
            boolean r0 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0097 }
            if (r0 != 0) goto L_0x0037
            com.fossil.Kl3 r0 = r6.d()     // Catch:{ SQLiteException -> 0x0097 }
            com.fossil.Nl3 r0 = r0.N()     // Catch:{ SQLiteException -> 0x0097 }
            java.lang.String r3 = "Main event not found"
            r0.a(r3)     // Catch:{ SQLiteException -> 0x0097 }
            if (r1 == 0) goto L_0x0036
            r1.close()
        L_0x0036:
            return r2
        L_0x0037:
            r0 = 0
            byte[] r3 = r1.getBlob(r0)
            r0 = 1
            long r4 = r1.getLong(r0)
            com.fossil.Wu2$Ai r0 = com.fossil.Wu2.c0()     // Catch:{ IOException -> 0x0060 }
            com.fossil.Gr3.z(r0, r3)     // Catch:{ IOException -> 0x0060 }
            com.fossil.Wu2$Ai r0 = (com.fossil.Wu2.Ai) r0     // Catch:{ IOException -> 0x0060 }
            com.fossil.M23 r0 = r0.h()     // Catch:{ IOException -> 0x0060 }
            com.fossil.E13 r0 = (com.fossil.E13) r0     // Catch:{ IOException -> 0x0060 }
            com.fossil.Wu2 r0 = (com.fossil.Wu2) r0     // Catch:{ IOException -> 0x0060 }
            java.lang.Long r3 = java.lang.Long.valueOf(r4)
            android.util.Pair r2 = android.util.Pair.create(r0, r3)
            if (r1 == 0) goto L_0x0036
            r1.close()
            goto L_0x0036
        L_0x0060:
            r0 = move-exception
            com.fossil.Kl3 r3 = r6.d()
            com.fossil.Nl3 r3 = r3.F()
            java.lang.String r4 = "Failed to merge main event. appId, eventId"
            java.lang.Object r5 = com.fossil.Kl3.w(r7)
            r3.d(r4, r5, r8, r0)
            if (r1 == 0) goto L_0x0036
            r1.close()
            goto L_0x0036
        L_0x0078:
            r0 = move-exception
            r1 = r2
        L_0x007a:
            com.fossil.Kl3 r3 = r6.d()     // Catch:{ all -> 0x008d }
            com.fossil.Nl3 r3 = r3.F()     // Catch:{ all -> 0x008d }
            java.lang.String r4 = "Error selecting main event"
            r3.b(r4, r0)     // Catch:{ all -> 0x008d }
            if (r1 == 0) goto L_0x0036
            r1.close()
            goto L_0x0036
        L_0x008d:
            r0 = move-exception
            r2 = r1
        L_0x008f:
            if (r2 == 0) goto L_0x0094
            r2.close()
        L_0x0094:
            throw r0
        L_0x0095:
            r0 = move-exception
            goto L_0x008f
        L_0x0097:
            r0 = move-exception
            goto L_0x007a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kg3.z(java.lang.String, java.lang.Long):android.util.Pair");
    }

    @DexIgnore
    public final void z0() {
        r();
        v().endTransaction();
    }
}
