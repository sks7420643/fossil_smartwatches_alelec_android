package com.fossil;

import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nr4 {
    @DexIgnore
    public /* final */ Lr4 a;

    @DexIgnore
    public Nr4(Lr4 lr4) {
        Wg6.c(lr4, "dao");
        this.a = lr4;
    }

    @DexIgnore
    public final Rr4 a(String str) {
        Wg6.c(str, "id");
        return this.a.a(str);
    }

    @DexIgnore
    public final Long[] b(List<Rr4> list) {
        Wg6.c(list, "flag");
        return this.a.insert(list);
    }
}
