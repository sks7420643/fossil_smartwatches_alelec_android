package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.remote.SecureApiService2Dot1;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tp4 implements Factory<SecureApiService2Dot1> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<An4> b;

    @DexIgnore
    public Tp4(Uo4 uo4, Provider<An4> provider) {
        this.a = uo4;
        this.b = provider;
    }

    @DexIgnore
    public static Tp4 a(Uo4 uo4, Provider<An4> provider) {
        return new Tp4(uo4, provider);
    }

    @DexIgnore
    public static SecureApiService2Dot1 c(Uo4 uo4, An4 an4) {
        SecureApiService2Dot1 A = uo4.A(an4);
        Lk7.c(A, "Cannot return null from a non-@Nullable @Provides method");
        return A;
    }

    @DexIgnore
    public SecureApiService2Dot1 b() {
        return c(this.a, this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
