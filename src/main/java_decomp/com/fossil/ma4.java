package com.fossil;

import com.fossil.Ta4$d$d$a$b$d;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ma4 extends Ta4$d$d$a$b$d {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4$d$d$a$b$d.a {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Long c;

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$d.a
        public Ta4$d$d$a$b$d a() {
            String str = "";
            if (this.a == null) {
                str = " name";
            }
            if (this.b == null) {
                str = str + " code";
            }
            if (this.c == null) {
                str = str + " address";
            }
            if (str.isEmpty()) {
                return new Ma4(this.a, this.b, this.c.longValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$d.a
        public Ta4$d$d$a$b$d.a b(long j) {
            this.c = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$d.a
        public Ta4$d$d$a$b$d.a c(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null code");
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$d.a
        public Ta4$d$d$a$b$d.a d(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null name");
        }
    }

    @DexIgnore
    public Ma4(String str, String str2, long j) {
        this.a = str;
        this.b = str2;
        this.c = j;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$d
    public long b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$d
    public String c() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$d
    public String d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4$d$d$a$b$d)) {
            return false;
        }
        Ta4$d$d$a$b$d ta4$d$d$a$b$d = (Ta4$d$d$a$b$d) obj;
        return this.a.equals(ta4$d$d$a$b$d.d()) && this.b.equals(ta4$d$d$a$b$d.c()) && this.c == ta4$d$d$a$b$d.b();
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.a.hashCode();
        int hashCode2 = this.b.hashCode();
        long j = this.c;
        return ((((hashCode ^ 1000003) * 1000003) ^ hashCode2) * 1000003) ^ ((int) (j ^ (j >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "Signal{name=" + this.a + ", code=" + this.b + ", address=" + this.c + "}";
    }
}
