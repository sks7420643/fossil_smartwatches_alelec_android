package com.fossil;

import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Rc7 {
    @DexIgnore
    public static final Rc7 a = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Rc7 {
        @DexIgnore
        @Override // com.fossil.Rc7
        public Map<Class<?>, Set<Pc7>> a(Object obj) {
            return Mc7.b(obj);
        }

        @DexIgnore
        @Override // com.fossil.Rc7
        public Map<Class<?>, Qc7> b(Object obj) {
            return Mc7.a(obj);
        }
    }

    @DexIgnore
    Map<Class<?>, Set<Pc7>> a(Object obj);

    @DexIgnore
    Map<Class<?>, Qc7> b(Object obj);
}
