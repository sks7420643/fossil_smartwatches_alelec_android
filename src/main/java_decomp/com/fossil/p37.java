package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p37 {
    @DexIgnore
    public static p37 f;
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public DeviceRepository f2774a;
    @DexIgnore
    public on5 b;
    @DexIgnore
    public UserRepository c;
    @DexIgnore
    public GuestApiService d;
    @DexIgnore
    public FirmwareFileRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final p37 a() {
            p37 b;
            synchronized (this) {
                if (p37.h.b() == null) {
                    p37.h.c(new p37(null));
                }
                b = p37.h.b();
                if (b == null) {
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.util.DeviceUtils");
                }
            }
            return b;
        }

        @DexIgnore
        public final p37 b() {
            return p37.f;
        }

        @DexIgnore
        public final void c(p37 p37) {
            p37.f = p37;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.util.DeviceUtils", f = "DeviceUtils.kt", l = {57, 79, 84}, m = "downloadActiveDeviceFirmware")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ p37 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(p37 p37, qn7 qn7) {
            super(qn7);
            this.this$0 = p37;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2", f = "DeviceUtils.kt", l = {100}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ iq5 $repoResponse;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ p37 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends qq7 implements rp7<Firmware, rv7<? extends Boolean>> {
            @DexIgnore
            public /* final */ /* synthetic */ iv7 $this_withContext;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.p37$c$a$a")
            @eo7(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$2$2$1", f = "DeviceUtils.kt", l = {96}, m = "invokeSuspend")
            /* renamed from: com.fossil.p37$c$a$a  reason: collision with other inner class name */
            public static final class C0188a extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Firmware $it;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0188a(a aVar, Firmware firmware, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                    this.$it = firmware;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0188a aVar = new C0188a(this.this$0, this.$it, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
                    throw null;
                    //return ((C0188a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        el7.b(obj);
                        iv7 iv7 = this.p$;
                        p37 p37 = this.this$0.this$0.this$0;
                        Firmware firmware = this.$it;
                        this.L$0 = iv7;
                        this.label = 1;
                        Object e = p37.e(firmware, this);
                        return e == d ? d : e;
                    } else if (i == 1) {
                        iv7 iv72 = (iv7) this.L$0;
                        el7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, iv7 iv7) {
                super(1);
                this.this$0 = cVar;
                this.$this_withContext = iv7;
            }

            @DexIgnore
            public final rv7<Boolean> invoke(Firmware firmware) {
                pq7.c(firmware, "it");
                return gu7.b(this.$this_withContext, null, null, new C0188a(this, firmware, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(p37 p37, iq5 iq5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = p37;
            this.$repoResponse = iq5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$repoResponse, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0066  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x011c  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0120  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r18) {
            /*
            // Method dump skipped, instructions count: 307
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.p37.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.util.DeviceUtils$downloadActiveDeviceFirmware$repoResponse$1", f = "DeviceUtils.kt", l = {79}, m = "invokeSuspend")
    public static final class d extends ko7 implements rp7<qn7<? super q88<ApiResponse<Firmware>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $model;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ p37 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(p37 p37, String str, qn7 qn7) {
            super(1, qn7);
            this.this$0 = p37;
            this.$model = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(qn7<?> qn7) {
            pq7.c(qn7, "completion");
            return new d(this.this$0, this.$model, qn7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.rp7
        public final Object invoke(qn7<? super q88<ApiResponse<Firmware>>> qn7) {
            throw null;
            //return ((d) create(qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                GuestApiService g = this.this$0.g();
                String P = PortfolioApp.h0.c().P();
                String str = this.$model;
                pq7.b(str, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                this.label = 1;
                Object firmwares = g.getFirmwares(P, str, "android", false, this);
                return firmwares == d ? d : firmwares;
            } else if (i == 1) {
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.util.DeviceUtils", f = "DeviceUtils.kt", l = {133}, m = "downloadDetailFirmware")
    public static final class e extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ p37 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(p37 p37, qn7 qn7) {
            super(qn7);
            this.this$0 = p37;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.util.DeviceUtils$isDeviceDianaEV1Java$1", f = "DeviceUtils.kt", l = {}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $deviceSerial;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ p37 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(p37 p37, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = p37;
            this.$deviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, this.$deviceSerial, qn7);
            fVar.p$ = (iv7) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
            throw null;
            //return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return ao7.a(this.this$0.h(this.$deviceSerial));
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.util.DeviceUtils", f = "DeviceUtils.kt", l = {200}, m = "isLatestFirmware")
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ p37 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(p37 p37, qn7 qn7) {
            super(qn7);
            this.this$0 = p37;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.j(null, null, this);
        }
    }

    /*
    static {
        String simpleName = p37.class.getSimpleName();
        pq7.b(simpleName, "DeviceUtils::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public p37() {
        PortfolioApp.h0.c().M().Q(this);
    }

    @DexIgnore
    public /* synthetic */ p37(kq7 kq7) {
        this();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01cc  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x020d  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0211  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.fossil.qn7<? super com.fossil.tl7> r14) {
        /*
        // Method dump skipped, instructions count: 545
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.p37.d(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0181  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.portfolio.platform.data.model.Firmware r14, com.fossil.qn7<? super java.lang.Boolean> r15) {
        /*
        // Method dump skipped, instructions count: 397
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.p37.e(com.portfolio.platform.data.model.Firmware, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.cl7<java.lang.String, java.lang.String> f(java.lang.String r8, com.portfolio.platform.data.model.Device r9) {
        /*
            r7 = this;
            r1 = 0
            java.lang.String r0 = "deviceSerial"
            com.fossil.pq7.c(r8, r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.fossil.p37.g
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "fetchFirmwaresInfo - deviceSerial="
            r3.append(r4)
            r3.append(r8)
            java.lang.String r4 = ", activeDevice="
            r3.append(r4)
            r3.append(r9)
            java.lang.String r3 = r3.toString()
            r0.d(r2, r3)
            if (r9 != 0) goto L_0x0034
            com.portfolio.platform.data.source.DeviceRepository r0 = r7.f2774a
            if (r0 == 0) goto L_0x003c
            com.portfolio.platform.data.model.Device r9 = r0.getDeviceBySerial(r8)
        L_0x0034:
            if (r9 != 0) goto L_0x0042
            com.fossil.cl7 r0 = new com.fossil.cl7
            r0.<init>(r1, r1)
        L_0x003b:
            return r0
        L_0x003c:
            java.lang.String r0 = "mDeviceRepository"
            com.fossil.pq7.n(r0)
            throw r1
        L_0x0042:
            java.lang.String r2 = r9.getFirmwareRevision()
            java.lang.String r0 = "release"
            java.lang.String r3 = "release"
            r4 = 1
            boolean r0 = com.fossil.vt7.j(r0, r3, r4)
            if (r0 != 0) goto L_0x005b
            com.fossil.on5 r0 = r7.b
            if (r0 == 0) goto L_0x00bd
            boolean r0 = r0.Y()
            if (r0 != 0) goto L_0x0096
        L_0x005b:
            com.fossil.mn5$a r0 = com.fossil.mn5.p
            com.fossil.mn5 r0 = r0.a()
            com.fossil.jp5 r0 = r0.g()
            java.lang.String r3 = r9.getSku()
            com.portfolio.platform.data.model.Firmware r0 = r0.e(r3)
        L_0x006d:
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.fossil.p37.g
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "fetchFirmwaresInfo - latestFw="
            r5.append(r6)
            r5.append(r0)
            java.lang.String r5 = r5.toString()
            r3.d(r4, r5)
            if (r0 == 0) goto L_0x00c3
            java.lang.String r0 = r0.getVersionNumber()
        L_0x008f:
            com.fossil.cl7 r1 = new com.fossil.cl7
            r1.<init>(r2, r0)
            r0 = r1
            goto L_0x003b
        L_0x0096:
            com.fossil.on5 r0 = r7.b
            if (r0 == 0) goto L_0x00b7
            java.lang.String r3 = r9.getSku()
            com.portfolio.platform.data.model.Firmware r0 = r0.k(r3)
            if (r0 != 0) goto L_0x006d
            com.fossil.mn5$a r0 = com.fossil.mn5.p
            com.fossil.mn5 r0 = r0.a()
            com.fossil.jp5 r0 = r0.g()
            java.lang.String r3 = r9.getSku()
            com.portfolio.platform.data.model.Firmware r0 = r0.e(r3)
            goto L_0x006d
        L_0x00b7:
            java.lang.String r0 = "mSharePrefs"
            com.fossil.pq7.n(r0)
            throw r1
        L_0x00bd:
            java.lang.String r0 = "mSharePrefs"
            com.fossil.pq7.n(r0)
            throw r1
        L_0x00c3:
            r0 = r1
            goto L_0x008f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.p37.f(java.lang.String, com.portfolio.platform.data.model.Device):com.fossil.cl7");
    }

    @DexIgnore
    public final GuestApiService g() {
        GuestApiService guestApiService = this.d;
        if (guestApiService != null) {
            return guestApiService;
        }
        pq7.n("mGuestApiService");
        throw null;
    }

    @DexIgnore
    public final boolean h(String str) {
        String firmwareRevision;
        pq7.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        if (!FossilDeviceSerialPatternUtil.isDianaDevice(str)) {
            return false;
        }
        DeviceRepository deviceRepository = this.f2774a;
        if (deviceRepository != null) {
            Device deviceBySerial = deviceRepository.getDeviceBySerial(str);
            if (deviceBySerial == null || (firmwareRevision = deviceBySerial.getFirmwareRevision()) == null) {
                return false;
            }
            return vt7.r(firmwareRevision, "DN0.0.0.", true);
        }
        pq7.n("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final boolean i(String str) {
        pq7.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        return ((Boolean) fu7.b(null, new f(this, str, null), 1, null)).booleanValue();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0143  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01ac  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object j(java.lang.String r15, com.portfolio.platform.data.model.Device r16, com.fossil.qn7<? super java.lang.Boolean> r17) {
        /*
        // Method dump skipped, instructions count: 497
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.p37.j(java.lang.String, com.portfolio.platform.data.model.Device, com.fossil.qn7):java.lang.Object");
    }
}
