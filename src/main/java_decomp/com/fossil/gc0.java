package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gc0 {
    @DexIgnore
    public static /* final */ /* synthetic */ Gc0 a; // = new Gc0();

    @DexIgnore
    public final Hc0 a(byte[] bArr) {
        try {
            Iw1 iw1 = (Iw1) Ga.d.f(bArr);
            if (iw1 instanceof Ow1) {
                return new Pw1(iw1.h(), iw1.g(), iw1.f(), iw1.b(), iw1.d(), iw1.e(), iw1.c(), iw1.a(), iw1.i());
            }
            if (iw1 instanceof Tw1) {
                return new Uw1(iw1.h(), iw1.g(), iw1.f(), iw1.b(), iw1.d(), iw1.e(), iw1.c(), iw1.a(), iw1.i());
            }
            return null;
        } catch (IllegalArgumentException e) {
        }
    }
}
