package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gt1 extends Tc0 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Ws1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Gt1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Gt1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(Cq1.class.getClassLoader());
            if (readParcelable != null) {
                return new Gt1((Cq1) readParcelable, (Nt1) parcel.readParcelable(Nt1.class.getClassLoader()), (Ws1) parcel.readParcelable(Ws1.class.getClassLoader()));
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Gt1[] newArray(int i) {
            return new Gt1[i];
        }
    }

    @DexIgnore
    public Gt1(Cq1 cq1, Nt1 nt1, Ws1 ws1) {
        super(cq1, nt1);
        this.d = ws1;
    }

    @DexIgnore
    public Gt1(Cq1 cq1, Ws1 ws1) {
        super(cq1, null);
        this.d = ws1;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public JSONObject a() {
        JSONObject jSONObject;
        JSONObject a2 = super.a();
        Ws1 ws1 = this.d;
        if (ws1 == null || (jSONObject = ws1.toJSONObject()) == null) {
            jSONObject = new JSONObject();
        }
        return Gy1.c(a2, jSONObject);
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public byte[] a(short s, Ry1 ry1) {
        X90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.d != null) {
                jSONObject.put("buddyChallengeApp._.config.sync", this.d.a());
            } else {
                getDeviceMessage();
            }
        } catch (JSONException e) {
            D90.i.i(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        Wg6.b(jSONObject4, "deviceResponseJSONObject.toString()");
        Charset c = Hd0.y.c();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(c);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Gt1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(Wg6.a(this.d, ((Gt1) obj).d) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.BuddyChallengeWatchAppGetChallengeInfoData");
    }

    @DexIgnore
    public final Ws1 getBuddyChallenge() {
        return this.d;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public int hashCode() {
        int hashCode = super.hashCode();
        Ws1 ws1 = this.d;
        return (ws1 != null ? ws1.hashCode() : 0) + (hashCode * 31);
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }
}
