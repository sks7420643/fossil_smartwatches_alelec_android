package com.fossil;

import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pb7 implements Gb7 {
    @DexIgnore
    public byte[] a;
    @DexIgnore
    public String b;
    @DexIgnore
    public Nb7 c;
    @DexIgnore
    public Jb7 d;
    @DexIgnore
    public Lb7 e;
    @DexIgnore
    public int f;

    @DexIgnore
    public Pb7(byte[] bArr, String str, Nb7 nb7, Jb7 jb7, Lb7 lb7, int i) {
        Wg6.c(bArr, "tickerData");
        Wg6.c(str, "nameOfTicker");
        Wg6.c(nb7, "colour");
        Wg6.c(jb7, "metric");
        Wg6.c(lb7, "type");
        this.a = bArr;
        this.b = str;
        this.c = nb7;
        this.d = jb7;
        this.e = lb7;
        this.f = i;
    }

    @DexIgnore
    @Override // com.fossil.Gb7
    public int a() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.Gb7
    public Jb7 b() {
        return this.d;
    }

    @DexIgnore
    public final Nb7 c() {
        return this.c;
    }

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public final byte[] e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Pb7.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Pb7 pb7 = (Pb7) obj;
            if (!Wg6.a(this.b, pb7.b)) {
                return false;
            }
            if (this.c != pb7.c) {
                return false;
            }
            if (!Wg6.a(b(), pb7.b())) {
                return false;
            }
            if (getType() != pb7.getType()) {
                return false;
            }
            return a() == pb7.a();
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.watchface.model.theme.communication.WFTickerData");
    }

    @DexIgnore
    public final void f(byte[] bArr) {
        Wg6.c(bArr, "<set-?>");
        this.a = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Gb7
    public Lb7 getType() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((this.b.hashCode() * 31) + this.c.hashCode()) * 31) + b().hashCode()) * 31) + getType().hashCode()) * 31) + a();
    }

    @DexIgnore
    public String toString() {
        return "WFTickerData(tickerData=" + Arrays.toString(this.a) + ", nameOfTicker=" + this.b + ", colour=" + this.c + ", metric=" + b() + ", type=" + getType() + ", index=" + a() + ")";
    }
}
