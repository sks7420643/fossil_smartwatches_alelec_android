package com.fossil;

import com.fossil.Ka4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ta4$d$d$a$b$a {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract Ta4$d$d$a$b$a a();

        @DexIgnore
        public abstract a b(long j);

        @DexIgnore
        public abstract a c(String str);

        @DexIgnore
        public abstract a d(long j);

        @DexIgnore
        public abstract a e(String str);

        @DexIgnore
        public a f(byte[] bArr) {
            e(new String(bArr, Ta4.a));
            return this;
        }
    }

    @DexIgnore
    public static a a() {
        return new Ka4.Bi();
    }

    @DexIgnore
    public abstract long b();

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public abstract long d();

    @DexIgnore
    public abstract String e();

    @DexIgnore
    public byte[] f() {
        String e = e();
        if (e != null) {
            return e.getBytes(Ta4.a);
        }
        return null;
    }
}
