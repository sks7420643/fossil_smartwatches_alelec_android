package com.fossil;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ir4 extends RecyclerView.g<b> {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public CustomizeWidget f1654a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<WatchApp> c;
    @DexIgnore
    public a d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(WatchApp watchApp);

        @DexIgnore
        void b(WatchApp watchApp);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public CustomizeWidget f1655a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public View d;
        @DexIgnore
        public WatchApp e;
        @DexIgnore
        public /* final */ /* synthetic */ ir4 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public a(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a l;
                if (this.b.f.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && (l = this.b.f.l()) != null) {
                    Object obj = this.b.f.c.get(this.b.getAdapterPosition());
                    pq7.b(obj, "mData[adapterPosition]");
                    l.a((WatchApp) obj);
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ir4$b$b")
        /* renamed from: com.fossil.ir4$b$b  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0122b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public View$OnClickListenerC0122b(b bVar) {
                this.b = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                a l;
                if (this.b.f.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && (l = this.b.f.l()) != null) {
                    Object obj = this.b.f.c.get(this.b.getAdapterPosition());
                    pq7.b(obj, "mData[adapterPosition]");
                    l.b((WatchApp) obj);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements CustomizeWidget.b {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ b f1656a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public c(b bVar) {
                this.f1656a = bVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.view.CustomizeWidget.b
            public void a(CustomizeWidget customizeWidget) {
                pq7.c(customizeWidget, "view");
                this.f1656a.f.f1654a = customizeWidget;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ir4 ir4, View view) {
            super(view);
            pq7.c(view, "view");
            this.f = ir4;
            this.c = view.findViewById(2131362728);
            this.d = view.findViewById(2131362767);
            View findViewById = view.findViewById(2131363551);
            pq7.b(findViewById, "view.findViewById(R.id.wc_watch_app)");
            this.f1655a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363424);
            pq7.b(findViewById2, "view.findViewById(R.id.tv_watch_app_name)");
            this.b = (TextView) findViewById2;
            this.f1655a.setOnClickListener(new a(this));
            this.d.setOnClickListener(new View$OnClickListenerC0122b(this));
        }

        @DexIgnore
        public final void a(WatchApp watchApp) {
            String str;
            pq7.c(watchApp, "watchApp");
            this.e = watchApp;
            if (watchApp != null) {
                this.f1655a.b0(watchApp.getWatchappId());
                this.b.setText(um5.d(PortfolioApp.h0.c(), watchApp.getNameKey(), watchApp.getName()));
            }
            this.f1655a.setSelectedWc(pq7.a(watchApp.getWatchappId(), this.f.b));
            View view = this.d;
            pq7.b(view, "ivWarning");
            view.setVisibility(!ol5.c.e(watchApp.getWatchappId()) ? 0 : 8);
            if (pq7.a(watchApp.getWatchappId(), this.f.b)) {
                View view2 = this.c;
                pq7.b(view2, "ivIndicator");
                view2.setBackground(gl0.f(PortfolioApp.h0.c(), 2131230956));
            } else {
                View view3 = this.c;
                pq7.b(view3, "ivIndicator");
                view3.setBackground(gl0.f(PortfolioApp.h0.c(), 2131230957));
            }
            CustomizeWidget customizeWidget = this.f1655a;
            Intent intent = new Intent();
            WatchApp watchApp2 = this.e;
            if (watchApp2 == null || (str = watchApp2.getWatchappId()) == null) {
                str = "";
            }
            Intent putExtra = intent.putExtra("KEY_ID", str);
            pq7.b(putExtra, "Intent().putExtra(Custom\u2026                   ?: \"\")");
            CustomizeWidget.X(customizeWidget, "WATCH_APP", putExtra, null, new c(this), 4, null);
        }
    }

    @DexIgnore
    public ir4(ArrayList<WatchApp> arrayList, a aVar) {
        pq7.c(arrayList, "mData");
        this.c = arrayList;
        this.d = aVar;
        this.b = "empty";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ir4(ArrayList arrayList, a aVar, int i, kq7 kq7) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : aVar);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final void j() {
        FLogger.INSTANCE.getLocal().d("WatchAppsAdapter", "dragStopped");
        CustomizeWidget customizeWidget = this.f1654a;
        if (customizeWidget != null) {
            customizeWidget.setDragMode(false);
        }
    }

    @DexIgnore
    public final int k(String str) {
        T t;
        pq7.c(str, "watchAppId");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (pq7.a(next.getWatchappId(), str)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return this.c.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    public final a l() {
        return this.d;
    }

    @DexIgnore
    /* renamed from: m */
    public void onBindViewHolder(b bVar, int i) {
        pq7.c(bVar, "holder");
        if (getItemCount() > i && i != -1) {
            WatchApp watchApp = this.c.get(i);
            pq7.b(watchApp, "mData[position]");
            bVar.a(watchApp);
        }
    }

    @DexIgnore
    /* renamed from: n */
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        pq7.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558724, viewGroup, false);
        pq7.b(inflate, "LayoutInflater.from(pare\u2026watch_app, parent, false)");
        return new b(this, inflate);
    }

    @DexIgnore
    public final void o(List<WatchApp> list) {
        pq7.c(list, "data");
        this.c.clear();
        this.c.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void p(a aVar) {
        pq7.c(aVar, "listener");
        this.d = aVar;
    }

    @DexIgnore
    public final void q(String str) {
        pq7.c(str, "watchAppId");
        this.b = str;
        notifyDataSetChanged();
    }
}
