package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ry4 implements MembersInjector<BCNotificationActionReceiver> {
    @DexIgnore
    public static void a(BCNotificationActionReceiver bCNotificationActionReceiver, Tt4 tt4) {
        bCNotificationActionReceiver.b = tt4;
    }

    @DexIgnore
    public static void b(BCNotificationActionReceiver bCNotificationActionReceiver, FriendRepository friendRepository) {
        bCNotificationActionReceiver.a = friendRepository;
    }

    @DexIgnore
    public static void c(BCNotificationActionReceiver bCNotificationActionReceiver, An4 an4) {
        bCNotificationActionReceiver.c = an4;
    }
}
