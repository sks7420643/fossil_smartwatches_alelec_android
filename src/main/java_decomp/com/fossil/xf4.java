package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Xf4 implements Ft3 {
    @DexIgnore
    public /* final */ Bg4 a;
    @DexIgnore
    public /* final */ Bundle b;

    @DexIgnore
    public Xf4(Bg4 bg4, Bundle bundle) {
        this.a = bg4;
        this.b = bundle;
    }

    @DexIgnore
    @Override // com.fossil.Ft3
    public final Object then(Nt3 nt3) {
        return this.a.i(this.b, nt3);
    }
}
