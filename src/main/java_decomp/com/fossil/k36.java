package com.fossil;

import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K36 implements Factory<RemindTimePresenter> {
    @DexIgnore
    public static RemindTimePresenter a(H36 h36, RemindersSettingsDatabase remindersSettingsDatabase) {
        return new RemindTimePresenter(h36, remindersSettingsDatabase);
    }
}
