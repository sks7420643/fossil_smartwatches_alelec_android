package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveMinutesChartViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pr6 implements Factory<CustomizeActiveMinutesChartViewModel> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public Pr6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Pr6 a(Provider<ThemeRepository> provider) {
        return new Pr6(provider);
    }

    @DexIgnore
    public static CustomizeActiveMinutesChartViewModel c(ThemeRepository themeRepository) {
        return new CustomizeActiveMinutesChartViewModel(themeRepository);
    }

    @DexIgnore
    public CustomizeActiveMinutesChartViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
