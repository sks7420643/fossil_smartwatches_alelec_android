package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class X72 {
    @DexIgnore
    public static void a(Status status, Ot3<Void> ot3) {
        b(status, null, ot3);
    }

    @DexIgnore
    public static <TResult> void b(Status status, TResult tresult, Ot3<TResult> ot3) {
        if (status.D()) {
            ot3.c(tresult);
        } else {
            ot3.b(new N62(status));
        }
    }

    @DexIgnore
    @Deprecated
    public static Nt3<Void> c(Nt3<Boolean> nt3) {
        return nt3.h(new Ba2());
    }
}
