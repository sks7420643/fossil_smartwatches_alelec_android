package com.fossil;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z54 {
    @DexIgnore
    public static <T> T a(Class<T> cls, InvocationHandler invocationHandler) {
        I14.l(invocationHandler);
        I14.h(cls.isInterface(), "%s is not an interface", cls);
        return cls.cast(Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, invocationHandler));
    }
}
