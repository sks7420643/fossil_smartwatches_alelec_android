package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lv extends Dv {
    @DexIgnore
    public long M;
    @DexIgnore
    public long N;
    @DexIgnore
    public long O;
    @DexIgnore
    public /* final */ long P;
    @DexIgnore
    public /* final */ long Q;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Lv(long j, long j2, short s, K5 k5, int i, int i2) {
        super(Iu.h, s, Hs.p, k5, (i2 & 16) != 0 ? 3 : i);
        this.P = j;
        this.Q = j2;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(G80.k(G80.k(super.A(), Jd0.L0, Long.valueOf(this.M)), Jd0.M0, Long.valueOf(this.N)), Jd0.N0, Long.valueOf(this.O));
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 12) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            this.M = Hy1.o(order.getInt(0));
            this.N = Hy1.o(order.getInt(4));
            this.O = Hy1.o(order.getInt(8));
            G80.k(G80.k(G80.k(jSONObject, Jd0.L0, Long.valueOf(this.M)), Jd0.M0, Long.valueOf(this.N)), Jd0.N0, Long.valueOf(this.O));
        }
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.P).putInt((int) this.Q).array();
        Wg6.b(array, "ByteBuffer.allocate(8)\n \u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Dv, com.fossil.Fs
    public JSONObject z() {
        return G80.k(G80.k(super.z(), Jd0.c1, Long.valueOf(this.P)), Jd0.d1, Long.valueOf(this.Q));
    }
}
