package com.fossil;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class An0 {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public HandlerThread b;
    @DexIgnore
    public Handler c;
    @DexIgnore
    public int d;
    @DexIgnore
    public Handler.Callback e; // = new Ai();
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ String h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Handler.Callback {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                An0.this.a();
            } else if (i == 1) {
                An0.this.b((Runnable) message.obj);
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Callable b;
        @DexIgnore
        public /* final */ /* synthetic */ Handler c;
        @DexIgnore
        public /* final */ /* synthetic */ Di d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Object b;

            @DexIgnore
            public Aii(Object obj) {
                this.b = obj;
            }

            @DexIgnore
            public void run() {
                Bi.this.d.a(this.b);
            }
        }

        @DexIgnore
        public Bi(An0 an0, Callable callable, Handler handler, Di di) {
            this.b = callable;
            this.c = handler;
            this.d = di;
        }

        @DexIgnore
        public void run() {
            Object obj;
            try {
                obj = this.b.call();
            } catch (Exception e) {
                obj = null;
            }
            this.c.post(new Aii(obj));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ AtomicReference b;
        @DexIgnore
        public /* final */ /* synthetic */ Callable c;
        @DexIgnore
        public /* final */ /* synthetic */ ReentrantLock d;
        @DexIgnore
        public /* final */ /* synthetic */ AtomicBoolean e;
        @DexIgnore
        public /* final */ /* synthetic */ Condition f;

        @DexIgnore
        public Ci(An0 an0, AtomicReference atomicReference, Callable callable, ReentrantLock reentrantLock, AtomicBoolean atomicBoolean, Condition condition) {
            this.b = atomicReference;
            this.c = callable;
            this.d = reentrantLock;
            this.e = atomicBoolean;
            this.f = condition;
        }

        @DexIgnore
        public void run() {
            try {
                this.b.set(this.c.call());
            } catch (Exception e2) {
            }
            this.d.lock();
            try {
                this.e.set(false);
                this.f.signal();
            } finally {
                this.d.unlock();
            }
        }
    }

    @DexIgnore
    public interface Di<T> {
        @DexIgnore
        void a(T t);
    }

    @DexIgnore
    public An0(String str, int i, int i2) {
        this.h = str;
        this.g = i;
        this.f = i2;
        this.d = 0;
    }

    @DexIgnore
    public void a() {
        synchronized (this.a) {
            if (!this.c.hasMessages(1)) {
                this.b.quit();
                this.b = null;
                this.c = null;
            }
        }
    }

    @DexIgnore
    public void b(Runnable runnable) {
        runnable.run();
        synchronized (this.a) {
            this.c.removeMessages(0);
            this.c.sendMessageDelayed(this.c.obtainMessage(0), (long) this.f);
        }
    }

    @DexIgnore
    public final void c(Runnable runnable) {
        synchronized (this.a) {
            if (this.b == null) {
                HandlerThread handlerThread = new HandlerThread(this.h, this.g);
                this.b = handlerThread;
                handlerThread.start();
                this.c = new Handler(this.b.getLooper(), this.e);
                this.d++;
            }
            this.c.removeMessages(0);
            this.c.sendMessage(this.c.obtainMessage(1, runnable));
        }
    }

    @DexIgnore
    public <T> void d(Callable<T> callable, Di<T> di) {
        c(new Bi(this, callable, new Handler(), di));
    }

    @DexIgnore
    public <T> T e(Callable<T> callable, int i) throws InterruptedException {
        ReentrantLock reentrantLock = new ReentrantLock();
        Condition newCondition = reentrantLock.newCondition();
        AtomicReference atomicReference = new AtomicReference();
        AtomicBoolean atomicBoolean = new AtomicBoolean(true);
        c(new Ci(this, atomicReference, callable, reentrantLock, atomicBoolean, newCondition));
        reentrantLock.lock();
        try {
            if (!atomicBoolean.get()) {
                return (T) atomicReference.get();
            }
            long nanos = TimeUnit.MILLISECONDS.toNanos((long) i);
            do {
                try {
                    nanos = newCondition.awaitNanos(nanos);
                } catch (InterruptedException e2) {
                }
                if (!atomicBoolean.get()) {
                    T t = (T) atomicReference.get();
                    reentrantLock.unlock();
                    return t;
                }
            } while (nanos > 0);
            throw new InterruptedException("timeout");
        } finally {
            reentrantLock.unlock();
        }
    }
}
