package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.K70;
import com.mapped.Qg6;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ol1 extends Hl1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ol1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ol1 createFromParcel(Parcel parcel) {
            return (Ol1) Hl1.CREATOR.b(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ol1[] newArray(int i) {
            return new Ol1[i];
        }
    }

    @DexIgnore
    public Ol1(byte b, byte b2, Set<? extends K70> set) {
        this(b, b2, set, false, true);
    }

    @DexIgnore
    public Ol1(byte b, byte b2, Set<? extends K70> set, boolean z, boolean z2) {
        super(b, b2, set, true, z, z2);
        if (!(!c().isEmpty())) {
            throw new IllegalArgumentException("Day Of Week must not be empty.");
        }
    }

    @DexIgnore
    public final Set<K70> getDaysOfWeek() {
        return c();
    }
}
