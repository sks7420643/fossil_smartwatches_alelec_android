package com.fossil;

import android.text.TextUtils;
import com.google.gson.reflect.TypeToken;
import com.mapped.Cd6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.helper.GsonConvertDateTime;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H05 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends TypeToken<Resting> {
    }

    @DexIgnore
    public final Resting a(String str) {
        Resting resting;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        Zi4 zi4 = new Zi4();
        zi4.f(DateTime.class, new GsonConvertDateTime());
        try {
            resting = (Resting) zi4.d().l(str, new Ai().getType());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            e.printStackTrace();
            local.e("RestingConverter.toResting()", String.valueOf(Cd6.a));
            resting = null;
        }
        return resting;
    }

    @DexIgnore
    public final String b(Resting resting) {
        if (resting == null) {
            return null;
        }
        try {
            Zi4 zi4 = new Zi4();
            zi4.f(DateTime.class, new GsonConvertDateTime());
            return zi4.d().t(resting);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            e.printStackTrace();
            local.e("RestingConverter.toString()", String.valueOf(Cd6.a));
            return null;
        }
    }
}
