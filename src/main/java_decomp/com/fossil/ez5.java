package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.dk5;
import com.fossil.m47;
import com.fossil.ou6;
import com.fossil.t47;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.ota.UpdateFirmwareActivity;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ez5 extends qv5 implements zy5, t47.g, ou6.a {
    @DexIgnore
    public static /* final */ a A; // = new a(null);
    @DexIgnore
    public static /* final */ String z;
    @DexIgnore
    public md6 h;
    @DexIgnore
    public z66 i;
    @DexIgnore
    public mb6 j;
    @DexIgnore
    public bo6 k;
    @DexIgnore
    public f06 l;
    @DexIgnore
    public a46 m;
    @DexIgnore
    public un6 s;
    @DexIgnore
    public yy5 t;
    @DexIgnore
    public g37<r75> u;
    @DexIgnore
    public /* final */ ArrayList<Fragment> v; // = new ArrayList<>();
    @DexIgnore
    public String w; // = "";
    @DexIgnore
    public y67 x;
    @DexIgnore
    public HashMap y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ ez5 b(a aVar, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                str = "";
            }
            return aVar.a(str);
        }

        @DexIgnore
        public final ez5 a(String str) {
            pq7.c(str, "presetId");
            ez5 ez5 = new ez5();
            ez5.setArguments(nm0.a(new cl7("PRESET_ID_EXTRA", str)));
            return ez5;
        }

        @DexIgnore
        public final ez5 c() {
            ez5 ez5 = new ez5();
            ez5.setArguments(nm0.a(new cl7("KEY_DIANA_REQUIRE", Boolean.TRUE)));
            return ez5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements BottomNavigationView.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ br7 f1009a;
        @DexIgnore
        public /* final */ /* synthetic */ br7 b;
        @DexIgnore
        public /* final */ /* synthetic */ ez5 c;
        @DexIgnore
        public /* final */ /* synthetic */ Typeface d;

        @DexIgnore
        public b(br7 br7, br7 br72, ez5 ez5, String str, boolean z, Typeface typeface) {
            this.f1009a = br7;
            this.b = br72;
            this.c = ez5;
            this.d = typeface;
        }

        @DexIgnore
        @Override // com.google.android.material.bottomnavigation.BottomNavigationView.d
        public final boolean a(MenuItem menuItem) {
            int i;
            pq7.c(menuItem, "item");
            r75 r75 = (r75) ez5.K6(this.c).a();
            BottomNavigationView bottomNavigationView = r75 != null ? r75.q : null;
            if (bottomNavigationView != null) {
                pq7.b(bottomNavigationView, "mBinding.get()?.bottomNavigation!!");
                Menu menu = bottomNavigationView.getMenu();
                pq7.b(menu, "mBinding.get()?.bottomNavigation!!.menu");
                this.c.X6(menu, this.d, this.f1009a.element);
                menu.findItem(2131362185).setIcon(2131231088);
                Drawable f = gl0.f(this.c.requireContext(), 2131231013);
                if (f != null) {
                    f.setColorFilter(gl0.d(PortfolioApp.h0.c(), 2131099820), PorterDuff.Mode.SRC_ATOP);
                }
                menuItem.setIcon(f);
                MenuItem findItem = menu.findItem(2131361983);
                pq7.b(findItem, "menu.findItem(R.id.buddyChallengeFragment)");
                findItem.setIcon(f);
                menu.findItem(2131362173).setIcon(2131231063);
                menu.findItem(2131362967).setIcon(2131231145);
                menu.findItem(2131361886).setIcon(2131230990);
                MenuItem findItem2 = menu.findItem(2131361886);
                pq7.b(findItem2, "menu.findItem(R.id.alertsFragment)");
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(findItem2.getTitle());
                spannableStringBuilder.setSpan(new ForegroundColorSpan(this.f1009a.element), 0, spannableStringBuilder.length(), 0);
                menu.findItem(2131361886).setTitle(spannableStringBuilder);
                switch (menuItem.getItemId()) {
                    case 2131361886:
                        Drawable f2 = gl0.f(this.c.requireContext(), 2131230991);
                        int i2 = this.b.element;
                        if (f2 != null) {
                            f2.setColorFilter(i2, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(f2);
                        SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder2.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder2.length(), 0);
                        menu.findItem(2131361886).setTitle(spannableStringBuilder2);
                        i = 3;
                        break;
                    case 2131361983:
                        Drawable f3 = gl0.f(this.c.requireContext(), 2131231015);
                        int i3 = this.b.element;
                        if (f3 != null) {
                            f3.setColorFilter(i3, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(f3);
                        SpannableStringBuilder spannableStringBuilder3 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder3.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder3.length(), 0);
                        menu.findItem(2131361983).setTitle(spannableStringBuilder3);
                        i = 1;
                        break;
                    case 2131362173:
                        Drawable f4 = gl0.f(this.c.requireContext(), 2131231064);
                        int i4 = this.b.element;
                        if (f4 != null) {
                            f4.setColorFilter(i4, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(f4);
                        SpannableStringBuilder spannableStringBuilder4 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder4.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder4.length(), 0);
                        menu.findItem(2131362173).setTitle(spannableStringBuilder4);
                        i = 2;
                        break;
                    case 2131362185:
                        Drawable f5 = gl0.f(this.c.requireContext(), 2131231089);
                        int i5 = this.b.element;
                        if (f5 != null) {
                            f5.setColorFilter(i5, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(f5);
                        SpannableStringBuilder spannableStringBuilder5 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder5.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder5.length(), 0);
                        menu.findItem(2131362185).setTitle(spannableStringBuilder5);
                        i = 0;
                        break;
                    case 2131362967:
                        Drawable f6 = gl0.f(this.c.requireContext(), 2131231146);
                        int i6 = this.b.element;
                        if (f6 != null) {
                            f6.setColorFilter(i6, PorterDuff.Mode.SRC_ATOP);
                        }
                        menuItem.setIcon(f6);
                        SpannableStringBuilder spannableStringBuilder6 = new SpannableStringBuilder(menuItem.getTitle());
                        spannableStringBuilder6.setSpan(new ForegroundColorSpan(this.b.element), 0, spannableStringBuilder6.length(), 0);
                        menu.findItem(2131362967).setTitle(spannableStringBuilder6);
                        i = 4;
                        break;
                    default:
                        i = 0;
                        break;
                }
                if (this.c.j != null && nk5.o.y(PortfolioApp.h0.c().J())) {
                    this.c.S6().R(i);
                }
                if (this.c.i != null && nk5.o.x(PortfolioApp.h0.c().J())) {
                    this.c.R6().m0(i);
                }
                if (i != 2) {
                    ez5 ez5 = this.c;
                    if (ez5.i != null && ez5.R6().k0()) {
                        this.c.R6().g0();
                    }
                }
                FLogger.INSTANCE.getLocal().d(ez5.z, "show tab with tab=" + i);
                ez5.L6(this.c).t(i);
                this.c.c7(i);
                return true;
            }
            pq7.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnLongClickListener {
        @DexIgnore
        public static /* final */ c b; // = new c();

        @DexIgnore
        public final boolean onLongClick(View view) {
            pq7.b(view, "item");
            view.setHapticFeedbackEnabled(false);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ls0<Integer> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ez5 f1010a;

        @DexIgnore
        public d(ez5 ez5) {
            this.f1010a = ez5;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0071, code lost:
            if (((r0 == null || (r0 = r0.q) == null || (r0 = r0.getMenu()) == null || (r0 = r0.findItem(2131361983)) == null) ? false : r0.isVisible()) != false) goto L_0x0025;
         */
        @DexIgnore
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void onChanged(java.lang.Integer r6) {
            /*
                r5 = this;
                r1 = 0
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r2 = com.fossil.ez5.M6()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "dashboardTab: "
                r3.append(r4)
                r3.append(r6)
                java.lang.String r3 = r3.toString()
                r0.d(r2, r3)
                if (r6 != 0) goto L_0x0045
            L_0x0021:
                java.lang.Integer r6 = java.lang.Integer.valueOf(r1)
            L_0x0025:
                com.fossil.ez5 r0 = r5.f1010a
                int r1 = r6.intValue()
                com.fossil.ez5.O6(r0, r1)
                com.fossil.ez5 r0 = r5.f1010a
                com.fossil.yy5 r0 = com.fossil.ez5.L6(r0)
                int r1 = r6.intValue()
                r0.t(r1)
                com.fossil.ez5 r0 = r5.f1010a
                int r1 = r6.intValue()
                com.fossil.ez5.P6(r0, r1)
                return
            L_0x0045:
                int r0 = r6.intValue()
                r2 = 1
                if (r0 != r2) goto L_0x0025
                com.fossil.ez5 r0 = r5.f1010a
                com.fossil.g37 r0 = com.fossil.ez5.K6(r0)
                java.lang.Object r0 = r0.a()
                com.fossil.r75 r0 = (com.fossil.r75) r0
                if (r0 == 0) goto L_0x0074
                com.google.android.material.bottomnavigation.BottomNavigationView r0 = r0.q
                if (r0 == 0) goto L_0x0074
                android.view.Menu r0 = r0.getMenu()
                if (r0 == 0) goto L_0x0074
                r2 = 2131361983(0x7f0a00bf, float:1.8343734E38)
                android.view.MenuItem r0 = r0.findItem(r2)
                if (r0 == 0) goto L_0x0074
                boolean r0 = r0.isVisible()
            L_0x0071:
                if (r0 == 0) goto L_0x0021
                goto L_0x0025
            L_0x0074:
                r0 = r1
                goto L_0x0071
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ez5.d.onChanged(java.lang.Integer):void");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ ez5 f1011a;

        @DexIgnore
        public e(ez5 ez5) {
            this.f1011a = ez5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = ez5.z;
            local.d(str2, "activeDeviceSerialLiveData onChange " + str);
            yy5 L6 = ez5.L6(this.f1011a);
            if (str != null) {
                L6.r(str);
            } else {
                pq7.i();
                throw null;
            }
        }
    }

    /*
    static {
        String simpleName = ez5.class.getSimpleName();
        pq7.b(simpleName, "HomeFragment::class.java.simpleName");
        z = simpleName;
    }
    */

    @DexIgnore
    public ez5() {
        ru6.b.a();
    }

    @DexIgnore
    public static final /* synthetic */ g37 K6(ez5 ez5) {
        g37<r75> g37 = ez5.u;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ yy5 L6(ez5 ez5) {
        yy5 yy5 = ez5.t;
        if (yy5 != null) {
            return yy5;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zy5
    public void A(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = z;
        StringBuilder sb = new StringBuilder();
        sb.append("onUpdateFwComplete currentTab ");
        yy5 yy5 = this.t;
        if (yy5 != null) {
            sb.append(yy5.p());
            local.d(str, sb.toString());
            md6 md6 = this.h;
            if (md6 != null) {
                if (md6 != null) {
                    md6.U(z2);
                } else {
                    pq7.n("mHomeDashboardPresenter");
                    throw null;
                }
            }
            z66 z66 = this.i;
            if (z66 != null) {
                if (z66 != null) {
                    z66.n0();
                } else {
                    pq7.n("mHomeDianaCustomizePresenter");
                    throw null;
                }
            }
            yy5 yy52 = this.t;
            if (yy52 != null) {
                c7(yy52.p());
                if (!z2 && isActive()) {
                    s37 s37 = s37.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    pq7.b(childFragmentManager, "childFragmentManager");
                    s37.z0(childFragmentManager);
                    return;
                }
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zy5
    public void C1(Intent intent) {
        pq7.c(intent, "intent");
        md6 md6 = this.h;
        if (md6 != null) {
            if (md6 != null) {
                md6.T(intent);
            } else {
                pq7.n("mHomeDashboardPresenter");
                throw null;
            }
        }
        bo6 bo6 = this.k;
        if (bo6 == null) {
            return;
        }
        if (bo6 != null) {
            bo6.g0(intent);
        } else {
            pq7.n("mHomeProfilePresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        z66 z66 = this.i;
        if (z66 != null) {
            if (z66 == null) {
                pq7.n("mHomeDianaCustomizePresenter");
                throw null;
            } else if (z66.k0()) {
                z66 z662 = this.i;
                if (z662 != null) {
                    z662.g0();
                    return true;
                }
                pq7.n("mHomeDianaCustomizePresenter");
                throw null;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.zy5
    public void L0(FossilDeviceSerialPatternUtil.DEVICE device, int i2, boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = z;
        local.d(str, "updateFragmentList with deviceType " + device + " appMode " + i2);
        if (!z2) {
            cl5.c.c();
        }
        T6(z2);
        if (i2 != 1) {
            a7(device);
        } else {
            b7();
        }
        U6();
    }

    @DexIgnore
    @Override // com.fossil.zy5
    public void O3() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.I(childFragmentManager);
        }
    }

    @DexIgnore
    public final void Q6() {
        Resources resources = PortfolioApp.h0.c().getResources();
        pq7.b(resources, "PortfolioApp.instance.resources");
        int applyDimension = (int) TypedValue.applyDimension(1, 28.0f, resources.getDisplayMetrics());
        g37<r75> g37 = this.u;
        if (g37 != null) {
            r75 a2 = g37.a();
            if (a2 != null) {
                View childAt = a2.q.getChildAt(0);
                if (childAt != null) {
                    BottomNavigationMenuView bottomNavigationMenuView = (BottomNavigationMenuView) childAt;
                    int childCount = bottomNavigationMenuView.getChildCount();
                    for (int i2 = 0; i2 < childCount; i2++) {
                        View findViewById = bottomNavigationMenuView.getChildAt(i2).findViewById(2131362608);
                        pq7.b(findViewById, "icon");
                        findViewById.getLayoutParams().width = applyDimension;
                        findViewById.getLayoutParams().height = applyDimension;
                    }
                    return;
                }
                throw new il7("null cannot be cast to non-null type com.google.android.material.bottomnavigation.BottomNavigationMenuView");
            }
            pq7.i();
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        switch (str.hashCode()) {
            case -1944405936:
                if (str.equals("FIRMWARE_UPDATE_FAIL") && i2 == 2131363373) {
                    UpdateFirmwareActivity.a aVar = UpdateFirmwareActivity.C;
                    FragmentActivity requireActivity = requireActivity();
                    pq7.b(requireActivity, "requireActivity()");
                    aVar.b(requireActivity, PortfolioApp.h0.c().J(), false);
                    return;
                }
                return;
            case 986357734:
                if (!str.equals("FEEDBACK_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363291) {
                    FLogger.INSTANCE.getLocal().d(z, "Cancel Zendesk feedback");
                    return;
                } else if (i2 != 2131363373) {
                    return;
                } else {
                    if (!wr4.f3989a.a().n()) {
                        FLogger.INSTANCE.getLocal().d(z, "Go to Zendesk feedback");
                        yy5 yy5 = this.t;
                        if (yy5 != null) {
                            yy5.u("Feedback - From app [Fossil] - [Android]");
                            return;
                        } else {
                            pq7.n("mPresenter");
                            throw null;
                        }
                    } else {
                        String a2 = m47.a(m47.c.REPAIR_CENTER, null);
                        pq7.b(a2, "URLHelper.buildStaticPag\u2026Page.REPAIR_CENTER, null)");
                        d7(a2);
                        return;
                    }
                }
            case 1390226280:
                if (!str.equals("HAPPINESS_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363291) {
                    FLogger.INSTANCE.getLocal().d(z, "Send Zendesk feedback");
                    s37 s37 = s37.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    pq7.b(childFragmentManager, "childFragmentManager");
                    s37.A(childFragmentManager);
                    return;
                } else if (i2 == 2131363373) {
                    FLogger.INSTANCE.getLocal().d(z, "Open app rating dialog");
                    s37 s372 = s37.c;
                    FragmentManager childFragmentManager2 = getChildFragmentManager();
                    pq7.b(childFragmentManager2, "childFragmentManager");
                    s372.j(childFragmentManager2);
                    return;
                } else {
                    return;
                }
            case 1431920636:
                if (!str.equals("APP_RATING_CONFIRM")) {
                    return;
                }
                if (i2 == 2131363291) {
                    FLogger.INSTANCE.getLocal().d(z, "Stay in app");
                    return;
                } else if (i2 == 2131363373) {
                    FLogger.INSTANCE.getLocal().d(z, "Go to Play Store");
                    PortfolioApp c2 = PortfolioApp.h0.c();
                    dk5.a aVar2 = dk5.g;
                    String packageName = c2.getPackageName();
                    pq7.b(packageName, "packageName");
                    aVar2.k(c2, packageName);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    @DexIgnore
    public final z66 R6() {
        z66 z66 = this.i;
        if (z66 != null) {
            return z66;
        }
        pq7.n("mHomeDianaCustomizePresenter");
        throw null;
    }

    @DexIgnore
    public final mb6 S6() {
        mb6 mb6 = this.j;
        if (mb6 != null) {
            return mb6;
        }
        pq7.n("mHomeHybridCustomizePresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zy5
    public void T1() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = z;
        StringBuilder sb = new StringBuilder();
        sb.append("onStartUpdateFw currentTab ");
        yy5 yy5 = this.t;
        if (yy5 != null) {
            sb.append(yy5.p());
            local.d(str, sb.toString());
            yy5 yy52 = this.t;
            if (yy52 != null) {
                c7(yy52.p());
                md6 md6 = this.h;
                if (md6 == null) {
                    return;
                }
                if (md6 != null) {
                    md6.V();
                } else {
                    pq7.n("mHomeDashboardPresenter");
                    throw null;
                }
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void T6(boolean z2) {
        String d2 = qn5.l.a().d("nonBrandSurface");
        Typeface f = qn5.l.a().f("nonBrandTextStyle11");
        g37<r75> g37 = this.u;
        if (g37 != null) {
            r75 a2 = g37.a();
            if (a2 != null) {
                if (d2 != null) {
                    a2.q.setBackgroundColor(Color.parseColor(d2));
                }
                BottomNavigationView bottomNavigationView = a2.q;
                pq7.b(bottomNavigationView, "bottomNavigation");
                bottomNavigationView.getMenu().findItem(2131361983).setVisible(z2);
                BottomNavigationView bottomNavigationView2 = a2.q;
                pq7.b(bottomNavigationView2, "bottomNavigation");
                Menu menu = bottomNavigationView2.getMenu();
                pq7.b(menu, "bottomNavigation.menu");
                int size = menu.size();
                for (int i2 = 0; i2 < size; i2++) {
                    MenuItem item = menu.getItem(i2);
                    pq7.b(item, "getItem(index)");
                    g37<r75> g372 = this.u;
                    if (g372 != null) {
                        r75 a3 = g372.a();
                        BottomNavigationView bottomNavigationView3 = a3 != null ? a3.q : null;
                        if (bottomNavigationView3 != null) {
                            bottomNavigationView3.findViewById(item.getItemId()).setOnLongClickListener(c.b);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } else {
                        pq7.n("mBinding");
                        throw null;
                    }
                }
                br7 br7 = new br7();
                br7.element = gl0.d(requireContext(), 2131099938);
                br7 br72 = new br7();
                br72.element = gl0.d(requireContext(), 2131099967);
                String d3 = qn5.l.a().d("nonBrandDisableCalendarDay");
                String d4 = qn5.l.a().d("primaryColor");
                if (d3 != null) {
                    br7.element = Color.parseColor(d3);
                }
                if (d4 != null) {
                    br72.element = Color.parseColor(d4);
                }
                BottomNavigationView bottomNavigationView4 = a2.q;
                pq7.b(bottomNavigationView4, "bottomNavigation");
                bottomNavigationView4.setItemIconTintList(null);
                a2.q.setOnNavigationItemSelectedListener(new b(br7, br72, this, d2, z2, f));
            }
            Q6();
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void U6() {
        W6();
        yy5 yy5 = this.t;
        if (yy5 != null) {
            int p = yy5.p();
            if (p == 0) {
                g37<r75> g37 = this.u;
                if (g37 != null) {
                    r75 a2 = g37.a();
                    if (a2 != null) {
                        BottomNavigationView bottomNavigationView = a2.q;
                        pq7.b(bottomNavigationView, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView.setSelectedItemId(2131362185);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            } else if (p == 1) {
                g37<r75> g372 = this.u;
                if (g372 != null) {
                    r75 a3 = g372.a();
                    if (a3 != null) {
                        BottomNavigationView bottomNavigationView2 = a3.q;
                        pq7.b(bottomNavigationView2, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView2.setSelectedItemId(2131361983);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            } else if (p == 2) {
                g37<r75> g373 = this.u;
                if (g373 != null) {
                    r75 a4 = g373.a();
                    if (a4 != null) {
                        BottomNavigationView bottomNavigationView3 = a4.q;
                        pq7.b(bottomNavigationView3, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView3.setSelectedItemId(2131362173);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            } else if (p == 3) {
                g37<r75> g374 = this.u;
                if (g374 != null) {
                    r75 a5 = g374.a();
                    if (a5 != null) {
                        BottomNavigationView bottomNavigationView4 = a5.q;
                        pq7.b(bottomNavigationView4, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView4.setSelectedItemId(2131361886);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            } else if (p == 4) {
                g37<r75> g375 = this.u;
                if (g375 != null) {
                    r75 a6 = g375.a();
                    if (a6 != null) {
                        BottomNavigationView bottomNavigationView5 = a6.q;
                        pq7.b(bottomNavigationView5, "mBinding.get()!!.bottomNavigation");
                        bottomNavigationView5.setSelectedItemId(2131362967);
                        return;
                    }
                    pq7.i();
                    throw null;
                }
                pq7.n("mBinding");
                throw null;
            }
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void V6(String str) {
        pq7.c(str, "presetId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = z;
        StringBuilder sb = new StringBuilder();
        sb.append("navigate to preset id ");
        sb.append(str);
        sb.append(" isPresenterInitialize ");
        sb.append(this.i != null);
        local.d(str2, sb.toString());
        if (!TextUtils.isEmpty(str)) {
            z66 z66 = this.i;
            if (z66 == null) {
                this.w = str;
            } else if (z66 != null) {
                z66.r(str);
            } else {
                pq7.n("mHomeDianaCustomizePresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void W6() {
        yy5 yy5 = this.t;
        if (yy5 == null) {
            return;
        }
        if (yy5 != null) {
            int p = yy5.p();
            int size = this.v.size();
            int i2 = 0;
            while (i2 < size) {
                if (this.v.get(i2) instanceof aw5) {
                    Fragment fragment = this.v.get(i2);
                    if (fragment != null) {
                        ((aw5) fragment).b2(i2 == p);
                    } else {
                        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.VisibleChangeListener");
                    }
                }
                i2++;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ou6.a
    public void X5(InAppNotification inAppNotification) {
        yy5 yy5 = this.t;
        if (yy5 == null) {
            pq7.n("mPresenter");
            throw null;
        } else if (inAppNotification != null) {
            yy5.o(inAppNotification);
        } else {
            pq7.i();
            throw null;
        }
    }

    @DexIgnore
    public final void X6(Menu menu, Typeface typeface, int i2) {
        if (typeface != null) {
            int size = menu.size();
            for (int i3 = 0; i3 < size; i3++) {
                MenuItem item = menu.getItem(i3);
                pq7.b(item, "getItem(index)");
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(item.getTitle());
                spannableStringBuilder.setSpan(new z47(typeface), 0, spannableStringBuilder.length(), 0);
                spannableStringBuilder.setSpan(new ForegroundColorSpan(i2), 0, spannableStringBuilder.length(), 0);
                item.setTitle(spannableStringBuilder);
            }
        }
    }

    @DexIgnore
    /* renamed from: Y6 */
    public void M5(yy5 yy5) {
        pq7.c(yy5, "presenter");
        this.t = yy5;
    }

    @DexIgnore
    public final void Z6(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = z;
        local.d(str, "showBottomTab currentDashboardTab=" + i2);
        if (i2 == 0) {
            g37<r75> g37 = this.u;
            if (g37 != null) {
                r75 a2 = g37.a();
                if (a2 != null) {
                    BottomNavigationView bottomNavigationView = a2.q;
                    pq7.b(bottomNavigationView, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView.setSelectedItemId(2131362185);
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        } else if (i2 == 1) {
            g37<r75> g372 = this.u;
            if (g372 != null) {
                r75 a3 = g372.a();
                if (a3 != null) {
                    BottomNavigationView bottomNavigationView2 = a3.q;
                    pq7.b(bottomNavigationView2, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView2.setSelectedItemId(2131361983);
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        } else if (i2 == 2) {
            g37<r75> g373 = this.u;
            if (g373 != null) {
                r75 a4 = g373.a();
                if (a4 != null) {
                    BottomNavigationView bottomNavigationView3 = a4.q;
                    pq7.b(bottomNavigationView3, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView3.setSelectedItemId(2131362173);
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        } else if (i2 == 3) {
            g37<r75> g374 = this.u;
            if (g374 != null) {
                r75 a5 = g374.a();
                if (a5 != null) {
                    BottomNavigationView bottomNavigationView4 = a5.q;
                    pq7.b(bottomNavigationView4, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView4.setSelectedItemId(2131361886);
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        } else if (i2 == 4) {
            g37<r75> g375 = this.u;
            if (g375 != null) {
                r75 a6 = g375.a();
                if (a6 != null) {
                    BottomNavigationView bottomNavigationView5 = a6.q;
                    pq7.b(bottomNavigationView5, "mBinding.get()!!.bottomNavigation");
                    bottomNavigationView5.setSelectedItemId(2131362967);
                    return;
                }
                pq7.i();
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a7(FossilDeviceSerialPatternUtil.DEVICE device) {
        FLogger.INSTANCE.getLocal().d(z, "Inside .showMainFlow");
        az5 a2 = az5.Y.a();
        Fragment Z = getChildFragmentManager().Z(nv4.l.a());
        Fragment Z2 = getChildFragmentManager().Z("HomeDianaCustomizeFragment");
        Fragment Z3 = getChildFragmentManager().Z("HomeHybridCustomizeFragment");
        Fragment Z4 = getChildFragmentManager().Z("HomeAlertsFragment");
        Fragment Z5 = getChildFragmentManager().Z("HomeAlertsHybridFragment");
        Fragment Z6 = getChildFragmentManager().Z("HomeProfileFragment");
        Fragment Z7 = getChildFragmentManager().Z("HomeUpdateFirmwareFragment");
        if (Z2 == null) {
            Z2 = dz5.E.a(this.w);
            this.w = "";
        }
        if (Z == null) {
            Z = nv4.l.b();
        }
        if (Z3 == null) {
            Z3 = gz5.x.a();
        }
        if (Z4 == null) {
            Z4 = b06.s.a();
        }
        if (Z5 == null) {
            Z5 = z36.l.a();
        }
        if (Z6 == null) {
            Z6 = mz5.m.a();
        }
        if (Z7 == null) {
            Z7 = tn6.t.a();
        }
        this.v.clear();
        this.v.add(a2);
        this.v.add(Z);
        if (nk5.o.w(device)) {
            this.v.add(Z2);
            this.v.add(Z4);
        } else {
            this.v.add(Z3);
            this.v.add(Z5);
        }
        this.v.add(Z6);
        this.v.add(Z7);
        g37<r75> g37 = this.u;
        if (g37 != null) {
            r75 a3 = g37.a();
            if (a3 != null) {
                try {
                    ViewPager2 viewPager2 = a3.s;
                    pq7.b(viewPager2, "binding.rvTabs");
                    viewPager2.setAdapter(new g67(getChildFragmentManager(), this.v));
                    if (a3.s.getChildAt(0) != null) {
                        View childAt = a3.s.getChildAt(0);
                        if (childAt != null) {
                            ((RecyclerView) childAt).setItemViewCacheSize(4);
                        } else {
                            throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                        }
                    }
                    ViewPager2 viewPager22 = a3.s;
                    pq7.b(viewPager22, "binding.rvTabs");
                    viewPager22.setUserInputEnabled(false);
                    tl7 tl7 = tl7.f3441a;
                } catch (Exception e2) {
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.finish();
                        HomeActivity.a aVar = HomeActivity.B;
                        pq7.b(activity, "it");
                        yy5 yy5 = this.t;
                        if (yy5 != null) {
                            aVar.a(activity, Integer.valueOf(yy5.p()));
                            tl7 tl72 = tl7.f3441a;
                        } else {
                            pq7.n("mPresenter");
                            throw null;
                        }
                    }
                }
            }
            ro4 M = PortfolioApp.h0.c().M();
            if (Z2 != null) {
                dz5 dz5 = (dz5) Z2;
                if (Z3 != null) {
                    gz5 gz5 = (gz5) Z3;
                    if (Z6 != null) {
                        mz5 mz5 = (mz5) Z6;
                        if (Z4 != null) {
                            b06 b06 = (b06) Z4;
                            if (Z5 != null) {
                                z36 z36 = (z36) Z5;
                                if (Z7 != null) {
                                    M.p1(new oz5(a2, dz5, gz5, mz5, b06, z36, (tn6) Z7)).a(this);
                                    return;
                                }
                                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment");
                            }
                            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
                        }
                        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
                    }
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void b7() {
        FLogger.INSTANCE.getLocal().d(z, "Inside .showNoActiveDeviceFlow");
        az5 a2 = az5.Y.a();
        Fragment Z = getChildFragmentManager().Z("HomeDianaCustomizeFragment");
        Fragment Z2 = getChildFragmentManager().Z("HomeHybridCustomizeFragment");
        Fragment Z3 = getChildFragmentManager().Z("HomeAlertsFragment");
        Fragment Z4 = getChildFragmentManager().Z("HomeAlertsHybridFragment");
        Fragment Z5 = getChildFragmentManager().Z("HomeProfileFragment");
        Fragment Z6 = getChildFragmentManager().Z("HomeUpdateFirmwareFragment");
        Fragment Z7 = getChildFragmentManager().Z(nv4.l.a());
        if (Z == null) {
            Z = dz5.E.a(this.w);
            this.w = "";
        }
        if (Z7 == null) {
            Z7 = nv4.l.b();
        }
        if (Z2 == null) {
            Z2 = gz5.x.a();
        }
        if (Z3 == null) {
            Z3 = b06.s.a();
        }
        if (Z4 == null) {
            Z4 = z36.l.a();
        }
        if (Z5 == null) {
            Z5 = mz5.m.a();
        }
        if (Z6 == null) {
            Z6 = tn6.t.a();
        }
        this.v.clear();
        this.v.add(a2);
        this.v.add(Z7);
        this.v.add(Z2);
        this.v.add(Z3);
        this.v.add(Z5);
        this.v.add(Z6);
        g37<r75> g37 = this.u;
        if (g37 != null) {
            r75 a3 = g37.a();
            if (a3 != null) {
                ViewPager2 viewPager2 = a3.s;
                pq7.b(viewPager2, "binding.rvTabs");
                viewPager2.setAdapter(new g67(getChildFragmentManager(), this.v));
                if (a3.s.getChildAt(0) != null) {
                    View childAt = a3.s.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setItemViewCacheSize(4);
                    } else {
                        throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
                ViewPager2 viewPager22 = a3.s;
                pq7.b(viewPager22, "binding.rvTabs");
                viewPager22.setUserInputEnabled(false);
            }
            ro4 M = PortfolioApp.h0.c().M();
            if (Z != null) {
                dz5 dz5 = (dz5) Z;
                if (Z2 != null) {
                    gz5 gz5 = (gz5) Z2;
                    if (Z5 != null) {
                        mz5 mz5 = (mz5) Z5;
                        if (Z3 != null) {
                            b06 b06 = (b06) Z3;
                            if (Z4 != null) {
                                z36 z36 = (z36) Z4;
                                if (Z6 != null) {
                                    M.p1(new oz5(a2, dz5, gz5, mz5, b06, z36, (tn6) Z6)).a(this);
                                    return;
                                }
                                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.ota.HomeUpdateFirmwareFragment");
                            }
                            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
                        }
                        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsFragment");
                    }
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeProfileFragment");
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeHybridCustomizeFragment");
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDianaCustomizeFragment");
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void c7(int i2) {
        if (i2 == 1) {
            g37<r75> g37 = this.u;
            if (g37 != null) {
                r75 a2 = g37.a();
                if (a2 != null) {
                    ViewPager2 viewPager2 = a2.s;
                    pq7.b(viewPager2, "mBinding.get()!!.rvTabs");
                    viewPager2.setDescendantFocusability(262144);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.n("mBinding");
                throw null;
            }
        } else {
            g37<r75> g372 = this.u;
            if (g372 != null) {
                r75 a3 = g372.a();
                if (a3 != null) {
                    ViewPager2 viewPager22 = a3.s;
                    pq7.b(viewPager22, "mBinding.get()!!.rvTabs");
                    viewPager22.setDescendantFocusability(393216);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.n("mBinding");
                throw null;
            }
        }
        yy5 yy5 = this.t;
        if (yy5 != null) {
            if (!yy5.q()) {
                g37<r75> g373 = this.u;
                if (g373 != null) {
                    r75 a4 = g373.a();
                    if (a4 != null) {
                        a4.s.j(i2, false);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            } else if (i2 > 0) {
                g37<r75> g374 = this.u;
                if (g374 != null) {
                    r75 a5 = g374.a();
                    if (a5 != null) {
                        a5.s.j(5, false);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            } else {
                g37<r75> g375 = this.u;
                if (g375 != null) {
                    r75 a6 = g375.a();
                    if (a6 != null) {
                        a6.s.j(i2, false);
                    } else {
                        pq7.i();
                        throw null;
                    }
                } else {
                    pq7.n("mBinding");
                    throw null;
                }
            }
            W6();
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void d7(String str) {
        J6(new Intent("android.intent.action.VIEW", Uri.parse(str)), z);
    }

    @DexIgnore
    @Override // com.fossil.zy5
    public void o0(int i2) {
        md6 md6 = this.h;
        if (md6 == null) {
            return;
        }
        if (md6 != null) {
            md6.a0(i2);
        } else {
            pq7.n("mHomeDashboardPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zy5, androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = z;
        local.d(str, "onActivityResult " + i2 + ' ' + i2);
        z66 z66 = this.i;
        if (z66 != null) {
            if (z66 != null) {
                z66.l0(i2, i3, intent);
            } else {
                pq7.n("mHomeDianaCustomizePresenter");
                throw null;
            }
        }
        mb6 mb6 = this.j;
        if (mb6 == null) {
            return;
        }
        if (mb6 != null) {
            mb6.Q(i2, i3, intent);
        } else {
            pq7.n("mHomeHybridCustomizePresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        r75 r75 = (r75) aq0.f(layoutInflater, 2131558570, viewGroup, false, A6());
        this.u = new g37<>(this, r75);
        pq7.b(r75, "binding");
        return r75.n();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        yy5 yy5 = this.t;
        if (yy5 != null) {
            yy5.s();
            yy5 yy52 = this.t;
            if (yy52 != null) {
                yy52.n();
                super.onDestroy();
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        yy5 yy5 = this.t;
        if (yy5 != null) {
            yy5.m();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        yy5 yy5 = this.t;
        if (yy5 != null) {
            yy5.l();
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        Q6();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ts0 a2 = vs0.e(activity).a(y67.class);
            pq7.b(a2, "ViewModelProviders.of(ac\u2026ardViewModel::class.java)");
            y67 y67 = (y67) a2;
            this.x = y67;
            if (y67 != null) {
                y67.a().h(activity, new d(this));
            } else {
                pq7.n("mHomeDashboardViewModel");
                throw null;
            }
        }
        Bundle arguments = getArguments();
        if (arguments != null && arguments.getBoolean("KEY_DIANA_REQUIRE")) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.u(childFragmentManager);
        }
        PortfolioApp.h0.c().K().h(getViewLifecycleOwner(), new e(this));
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.zy5
    public void z3(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        pq7.c(zendeskFeedbackConfiguration, "configuration");
        Context context = getContext();
        if (context != null) {
            Intent intent = new Intent(context, ContactZendeskActivity.class);
            intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
            startActivityForResult(intent, 1007);
        }
    }
}
