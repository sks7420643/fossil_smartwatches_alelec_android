package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.Hq4;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.migration.MigrationViewModel;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jv6 extends BaseFragment {
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public Jg5 g;
    @DexIgnore
    public MigrationViewModel h;
    @DexIgnore
    public Po4 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Jv6 a() {
            return new Jv6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Jv6 b;

        @DexIgnore
        public Bi(Jv6 jv6) {
            this.b = jv6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Jv6.K6(this.b).r();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<Hq4.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ Jv6 a;

        @DexIgnore
        public Ci(Jv6 jv6) {
            this.a = jv6;
        }

        @DexIgnore
        public final void a(Hq4.Bi bi) {
            if (bi.a()) {
                this.a.H6("");
            } else {
                this.a.a();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Bi bi) {
            a(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Hq4.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ Jv6 a;

        @DexIgnore
        public Di(Jv6 jv6) {
            this.a = jv6;
        }

        @DexIgnore
        public final void a(Hq4.Ai ai) {
            int a2 = ai.a();
            if (a2 == 0) {
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    HomeActivity.a aVar = HomeActivity.B;
                    Wg6.b(activity, "it");
                    HomeActivity.a.b(aVar, activity, null, 2, null);
                    activity.finish();
                }
            } else if (a2 == 1) {
                String c = Um5.c(PortfolioApp.get.instance(), 2131886795);
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131886794);
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                Wg6.b(c, "title");
                Wg6.b(c2, "des");
                s37.E(childFragmentManager, c, c2);
            } else if (a2 == 2) {
                S37 s372 = S37.c;
                FragmentManager childFragmentManager2 = this.a.getChildFragmentManager();
                Wg6.b(childFragmentManager2, "childFragmentManager");
                s372.C(childFragmentManager2);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Hq4.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore
    public static final /* synthetic */ MigrationViewModel K6(Jv6 jv6) {
        MigrationViewModel migrationViewModel = jv6.h;
        if (migrationViewModel != null) {
            return migrationViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void L6() {
        Jg5 jg5 = this.g;
        if (jg5 != null) {
            jg5.q.setOnClickListener(new Bi(this));
        } else {
            Wg6.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public final void M6() {
        MigrationViewModel migrationViewModel = this.h;
        if (migrationViewModel != null) {
            migrationViewModel.j().h(getViewLifecycleOwner(), new Ci(this));
            MigrationViewModel migrationViewModel2 = this.h;
            if (migrationViewModel2 != null) {
                migrationViewModel2.h().h(getViewLifecycleOwner(), new Di(this));
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Jg5 z = Jg5.z(layoutInflater);
        Wg6.b(z, "MigrationFragmentBinding.inflate(inflater)");
        this.g = z;
        if (z != null) {
            return z.n();
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        PortfolioApp.get.instance().getIface().v1().a(this);
        Po4 po4 = this.i;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(MigrationViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ionViewModel::class.java)");
            this.h = (MigrationViewModel) a2;
            L6();
            M6();
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
