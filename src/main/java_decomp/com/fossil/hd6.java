package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hd6 implements Factory<Dh6> {
    @DexIgnore
    public static Dh6 a(Dd6 dd6) {
        Dh6 d = dd6.d();
        Lk7.c(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
