package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U57 {
    @DexIgnore
    public short a;
    @DexIgnore
    public short b;

    @DexIgnore
    public final short a() {
        return this.a;
    }

    @DexIgnore
    public final short b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof U57) {
                U57 u57 = (U57) obj;
                if (!(this.a == u57.a && this.b == u57.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a * 31) + this.b;
    }

    @DexIgnore
    public String toString() {
        return "HeartRateDWMModel(minValue=" + ((int) this.a) + ", maxValue=" + ((int) this.b) + ")";
    }
}
