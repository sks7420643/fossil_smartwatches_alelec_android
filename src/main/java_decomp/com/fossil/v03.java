package com.fossil;

import com.fossil.V03;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface V03<T extends V03<T>> extends Comparable<T> {
    @DexIgnore
    P23 d(P23 p23, M23 m23);

    @DexIgnore
    V23 g(V23 v23, V23 v232);

    @DexIgnore
    int zza();

    @DexIgnore
    L43 zzb();

    @DexIgnore
    S43 zzc();

    @DexIgnore
    boolean zzd();

    @DexIgnore
    boolean zze();
}
