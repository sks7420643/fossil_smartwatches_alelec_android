package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C42 {
    @DexIgnore
    public SharedPreferences a;

    @DexIgnore
    public C42(Context context) {
        try {
            Context d = H62.d(context);
            this.a = d == null ? null : d.getSharedPreferences("google_ads_flags", 0);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while getting SharedPreferences ", th);
            this.a = null;
        }
    }

    @DexIgnore
    public final boolean a(String str, boolean z) {
        try {
            if (this.a == null) {
                return false;
            }
            return this.a.getBoolean(str, false);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th);
            return false;
        }
    }

    @DexIgnore
    public final float b(String str, float f) {
        try {
            return this.a == null ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : this.a.getFloat(str, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th);
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
    }

    @DexIgnore
    public final String c(String str, String str2) {
        try {
            return this.a == null ? str2 : this.a.getString(str, str2);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th);
            return str2;
        }
    }
}
