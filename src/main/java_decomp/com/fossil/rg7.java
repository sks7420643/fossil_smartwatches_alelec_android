package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rg7 extends Ng7 {
    @DexIgnore
    public Long m; // = null;
    @DexIgnore
    public String n;
    @DexIgnore
    public String o;

    @DexIgnore
    public Rg7(Context context, String str, String str2, int i, Long l, Jg7 jg7) {
        super(context, i, jg7);
        this.o = str;
        this.n = str2;
        this.m = l;
    }

    @DexIgnore
    @Override // com.fossil.Ng7
    public Og7 a() {
        return Og7.a;
    }

    @DexIgnore
    @Override // com.fossil.Ng7
    public boolean b(JSONObject jSONObject) {
        Ji7.d(jSONObject, "pi", this.n);
        Ji7.d(jSONObject, "rf", this.o);
        Long l = this.m;
        if (l == null) {
            return true;
        }
        jSONObject.put("du", l);
        return true;
    }
}
