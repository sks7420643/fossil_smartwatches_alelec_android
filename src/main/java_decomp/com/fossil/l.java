package com.fossil;

import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L extends Wk1 {
    @DexIgnore
    public /* final */ Al1[] b;

    @DexIgnore
    public L(Al1[] al1Arr) {
        this.b = al1Arr;
    }

    @DexIgnore
    @Override // com.fossil.Wk1
    public boolean a(E60 e60) {
        if (this.b.length == 0) {
            return true;
        }
        for (Al1 al1 : this.b) {
            if (al1 == e60.u.getDeviceType()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject k = G80.k(new JSONObject(), Jd0.H1, "device_type");
        Jd0 jd0 = Jd0.J1;
        Al1[] al1Arr = this.b;
        JSONArray jSONArray = new JSONArray();
        for (Al1 al1 : al1Arr) {
            jSONArray.put(Ey1.a(al1));
        }
        return G80.k(k, jd0, jSONArray);
    }
}
