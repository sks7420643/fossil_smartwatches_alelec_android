package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O34 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends K24<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable c;
        @DexIgnore
        public /* final */ /* synthetic */ J14 d;

        @DexIgnore
        public Ai(Iterable iterable, J14 j14) {
            this.c = iterable;
            this.d = j14;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return P34.k(this.c.iterator(), this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends K24<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable c;
        @DexIgnore
        public /* final */ /* synthetic */ B14 d;

        @DexIgnore
        public Bi(Iterable iterable, B14 b14) {
            this.c = iterable;
            this.d = b14;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            return P34.w(this.c.iterator(), this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends K24<T> {
        @DexIgnore
        public /* final */ /* synthetic */ List c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;

        @DexIgnore
        public Ci(List list, int i) {
            this.c = list;
            this.d = i;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            int min = Math.min(this.c.size(), this.d);
            List list = this.c;
            return list.subList(min, list.size()).iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends K24<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Iterator<T> {
            @DexIgnore
            public boolean b; // = true;
            @DexIgnore
            public /* final */ /* synthetic */ Iterator c;

            @DexIgnore
            public Aii(Di di, Iterator it) {
                this.c = it;
            }

            @DexIgnore
            public boolean hasNext() {
                return this.c.hasNext();
            }

            @DexIgnore
            @Override // java.util.Iterator
            public T next() {
                T t = (T) this.c.next();
                this.b = false;
                return t;
            }

            @DexIgnore
            public void remove() {
                A24.c(!this.b);
                this.c.remove();
            }
        }

        @DexIgnore
        public Di(Iterable iterable, int i) {
            this.c = iterable;
            this.d = i;
        }

        @DexIgnore
        @Override // java.lang.Iterable
        public Iterator<T> iterator() {
            Iterator it = this.c.iterator();
            P34.b(it, this.d);
            return new Aii(this, it);
        }
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> boolean a(Collection<T> collection, Iterable<? extends T> iterable) {
        if (iterable instanceof Collection) {
            return collection.addAll(B24.a(iterable));
        }
        I14.l(iterable);
        return P34.a(collection, iterable.iterator());
    }

    @DexIgnore
    public static <T> boolean b(Iterable<T> iterable, J14<? super T> j14) {
        return P34.c(iterable.iterator(), j14);
    }

    @DexIgnore
    public static <E> Collection<E> c(Iterable<E> iterable) {
        return iterable instanceof Collection ? (Collection) iterable : T34.i(iterable.iterator());
    }

    @DexIgnore
    public static <T> Iterable<T> d(Iterable<T> iterable, J14<? super T> j14) {
        I14.l(iterable);
        I14.l(j14);
        return new Ai(iterable, j14);
    }

    @DexIgnore
    public static <T> T e(Iterable<? extends T> iterable, T t) {
        return (T) P34.n(iterable.iterator(), t);
    }

    @DexIgnore
    public static <T> T f(Iterable<T> iterable) {
        return (T) P34.o(iterable.iterator());
    }

    @DexIgnore
    public static <T> Iterable<T> g(Iterable<T> iterable, int i) {
        I14.l(iterable);
        I14.e(i >= 0, "number to skip cannot be negative");
        return iterable instanceof List ? new Ci((List) iterable, i) : new Di(iterable, i);
    }

    @DexIgnore
    public static Object[] h(Iterable<?> iterable) {
        return c(iterable).toArray();
    }

    @DexIgnore
    public static <T> T[] i(Iterable<? extends T> iterable, T[] tArr) {
        return (T[]) c(iterable).toArray(tArr);
    }

    @DexIgnore
    public static String j(Iterable<?> iterable) {
        return P34.v(iterable.iterator());
    }

    @DexIgnore
    public static <F, T> Iterable<T> k(Iterable<F> iterable, B14<? super F, ? extends T> b14) {
        I14.l(iterable);
        I14.l(b14);
        return new Bi(iterable, b14);
    }
}
