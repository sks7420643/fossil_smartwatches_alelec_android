package com.fossil;

import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Lk6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vv7<T> extends Yv7<T> implements Do7, Xe6<T> {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater j; // = AtomicReferenceFieldUpdater.newUpdater(Vv7.class, Object.class, "_reusableCancellableContinuation");
    @DexIgnore
    public volatile Object _reusableCancellableContinuation;
    @DexIgnore
    public Object e; // = Wv7.a;
    @DexIgnore
    public /* final */ Do7 f;
    @DexIgnore
    public /* final */ Object g;
    @DexIgnore
    public /* final */ Dv7 h;
    @DexIgnore
    public /* final */ Xe6<T> i;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: com.mapped.Xe6<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public Vv7(Dv7 dv7, Xe6<? super T> xe6) {
        super(0);
        this.h = dv7;
        this.i = xe6;
        Xe6<T> xe62 = this.i;
        this.f = (Do7) (!(xe62 instanceof Do7) ? null : xe62);
        this.g = Zz7.b(getContext());
        this._reusableCancellableContinuation = null;
    }

    @DexIgnore
    @Override // com.fossil.Yv7
    public Xe6<T> c() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Do7
    public Do7 getCallerFrame() {
        return this.f;
    }

    @DexIgnore
    @Override // com.mapped.Xe6
    public Af6 getContext() {
        return this.i.getContext();
    }

    @DexIgnore
    @Override // com.fossil.Do7
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Yv7
    public Object j() {
        Object obj = this.e;
        if (Nv7.a()) {
            if (!(obj != Wv7.a)) {
                throw new AssertionError();
            }
        }
        this.e = Wv7.a;
        return obj;
    }

    @DexIgnore
    public final Throwable k(Lk6<?> lk6) {
        Vz7 vz7;
        do {
            Object obj = this._reusableCancellableContinuation;
            vz7 = Wv7.b;
            if (obj != vz7) {
                if (obj == null) {
                    return null;
                }
                if (!(obj instanceof Throwable)) {
                    throw new IllegalStateException(("Inconsistent state " + obj).toString());
                } else if (j.compareAndSet(this, obj, null)) {
                    return (Throwable) obj;
                } else {
                    throw new IllegalArgumentException("Failed requirement.".toString());
                }
            }
        } while (!j.compareAndSet(this, vz7, lk6));
        return null;
    }

    @DexIgnore
    public final Lu7<T> l() {
        Object obj;
        do {
            obj = this._reusableCancellableContinuation;
            if (obj == null) {
                this._reusableCancellableContinuation = Wv7.b;
                return null;
            } else if (!(obj instanceof Lu7)) {
                throw new IllegalStateException(("Inconsistent state " + obj).toString());
            }
        } while (!j.compareAndSet(this, obj, Wv7.b));
        return (Lu7) obj;
    }

    @DexIgnore
    public final void m(Af6 af6, T t) {
        this.e = t;
        this.d = 1;
        this.h.P(af6, this);
    }

    @DexIgnore
    public final Lu7<?> n() {
        Object obj = this._reusableCancellableContinuation;
        if (!(obj instanceof Lu7)) {
            obj = null;
        }
        return (Lu7) obj;
    }

    @DexIgnore
    public final boolean o() {
        return this._reusableCancellableContinuation != null;
    }

    @DexIgnore
    public final boolean p(Throwable th) {
        while (true) {
            Object obj = this._reusableCancellableContinuation;
            if (Wg6.a(obj, Wv7.b)) {
                if (j.compareAndSet(this, Wv7.b, th)) {
                    return true;
                }
            } else if (obj instanceof Throwable) {
                return true;
            } else {
                if (j.compareAndSet(this, obj, null)) {
                    return false;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Xe6
    public void resumeWith(Object obj) {
        Af6 context = this.i.getContext();
        Object b = Wu7.b(obj);
        if (this.h.Q(context)) {
            this.e = b;
            this.d = 0;
            this.h.M(context, this);
            return;
        }
        Hw7 b2 = Wx7.b.b();
        if (b2.o0()) {
            this.e = b;
            this.d = 0;
            b2.X(this);
            return;
        }
        b2.g0(true);
        try {
            Af6 context2 = getContext();
            Object c = Zz7.c(context2, this.g);
            try {
                this.i.resumeWith(obj);
                Cd6 cd6 = Cd6.a;
                do {
                } while (b2.r0());
            } finally {
                Zz7.a(context2, c);
            }
        } catch (Throwable th) {
            b2.S(true);
            throw th;
        }
        b2.S(true);
    }

    @DexIgnore
    public String toString() {
        return "DispatchedContinuation[" + this.h + ", " + Ov7.c(this.i) + ']';
    }
}
