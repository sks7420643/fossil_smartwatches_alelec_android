package com.fossil;

import java.io.Serializable;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ww2 {
    @DexIgnore
    public static <T> Xw2<T> a(Xw2<T> xw2) {
        return ((xw2 instanceof Yw2) || (xw2 instanceof Zw2)) ? xw2 : xw2 instanceof Serializable ? new Zw2(xw2) : new Yw2(xw2);
    }

    @DexIgnore
    public static <T> Xw2<T> b(@NullableDecl T t) {
        return new Bx2(t);
    }
}
