package com.fossil;

import android.content.Context;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.view.ActionMode;
import androidx.collection.SimpleArrayMap;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sf0 extends ActionMode {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ androidx.appcompat.view.ActionMode b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements ActionMode.Callback {
        @DexIgnore
        public /* final */ ActionMode.Callback a;
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public /* final */ ArrayList<Sf0> c; // = new ArrayList<>();
        @DexIgnore
        public /* final */ SimpleArrayMap<Menu, Menu> d; // = new SimpleArrayMap<>();

        @DexIgnore
        public Ai(Context context, ActionMode.Callback callback) {
            this.b = context;
            this.a = callback;
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public void a(androidx.appcompat.view.ActionMode actionMode) {
            this.a.onDestroyActionMode(e(actionMode));
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean b(androidx.appcompat.view.ActionMode actionMode, Menu menu) {
            return this.a.onCreateActionMode(e(actionMode), f(menu));
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean c(androidx.appcompat.view.ActionMode actionMode, Menu menu) {
            return this.a.onPrepareActionMode(e(actionMode), f(menu));
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean d(androidx.appcompat.view.ActionMode actionMode, MenuItem menuItem) {
            return this.a.onActionItemClicked(e(actionMode), new Fg0(this.b, (Hm0) menuItem));
        }

        @DexIgnore
        public android.view.ActionMode e(androidx.appcompat.view.ActionMode actionMode) {
            int size = this.c.size();
            for (int i = 0; i < size; i++) {
                Sf0 sf0 = this.c.get(i);
                if (sf0 != null && sf0.b == actionMode) {
                    return sf0;
                }
            }
            Sf0 sf02 = new Sf0(this.b, actionMode);
            this.c.add(sf02);
            return sf02;
        }

        @DexIgnore
        public final Menu f(Menu menu) {
            Menu menu2 = this.d.get(menu);
            if (menu2 != null) {
                return menu2;
            }
            Kg0 kg0 = new Kg0(this.b, (Gm0) menu);
            this.d.put(menu, kg0);
            return kg0;
        }
    }

    @DexIgnore
    public Sf0(Context context, androidx.appcompat.view.ActionMode actionMode) {
        this.a = context;
        this.b = actionMode;
    }

    @DexIgnore
    public void finish() {
        this.b.c();
    }

    @DexIgnore
    public View getCustomView() {
        return this.b.d();
    }

    @DexIgnore
    public Menu getMenu() {
        return new Kg0(this.a, (Gm0) this.b.e());
    }

    @DexIgnore
    public MenuInflater getMenuInflater() {
        return this.b.f();
    }

    @DexIgnore
    public CharSequence getSubtitle() {
        return this.b.g();
    }

    @DexIgnore
    public Object getTag() {
        return this.b.h();
    }

    @DexIgnore
    public CharSequence getTitle() {
        return this.b.i();
    }

    @DexIgnore
    public boolean getTitleOptionalHint() {
        return this.b.j();
    }

    @DexIgnore
    public void invalidate() {
        this.b.k();
    }

    @DexIgnore
    public boolean isTitleOptional() {
        return this.b.l();
    }

    @DexIgnore
    public void setCustomView(View view) {
        this.b.m(view);
    }

    @DexIgnore
    @Override // android.view.ActionMode
    public void setSubtitle(int i) {
        this.b.n(i);
    }

    @DexIgnore
    @Override // android.view.ActionMode
    public void setSubtitle(CharSequence charSequence) {
        this.b.o(charSequence);
    }

    @DexIgnore
    public void setTag(Object obj) {
        this.b.p(obj);
    }

    @DexIgnore
    @Override // android.view.ActionMode
    public void setTitle(int i) {
        this.b.q(i);
    }

    @DexIgnore
    @Override // android.view.ActionMode
    public void setTitle(CharSequence charSequence) {
        this.b.r(charSequence);
    }

    @DexIgnore
    public void setTitleOptionalHint(boolean z) {
        this.b.s(z);
    }
}
