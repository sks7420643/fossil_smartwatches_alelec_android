package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zb2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Zb2> CREATOR; // = new Qd2();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public Zb2(int i, String str) {
        this.b = i;
        this.c = str;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && (obj instanceof Zb2)) {
            Zb2 zb2 = (Zb2) obj;
            return zb2.b == this.b && Pc2.a(zb2.c, this.c);
        }
    }

    @DexIgnore
    public int hashCode() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        int i = this.b;
        String str = this.c;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 12);
        sb.append(i);
        sb.append(":");
        sb.append(str);
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.n(parcel, 1, this.b);
        Bd2.u(parcel, 2, this.c, false);
        Bd2.b(parcel, a2);
    }
}
