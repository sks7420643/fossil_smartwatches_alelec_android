package com.fossil;

import com.mapped.Cd6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.BaseActivity;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ev5 extends CoroutineUseCase<Bi, Di, Ci> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ Ai f; // = new Ai(null);
    @DexIgnore
    public /* final */ Vn5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Ev5.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ WeakReference<BaseActivity> a;

        @DexIgnore
        public Bi(WeakReference<BaseActivity> weakReference) {
            Wg6.c(weakReference, "activityContext");
            this.a = weakReference;
        }

        @DexIgnore
        public final WeakReference<BaseActivity> a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ Z52 b;

        @DexIgnore
        public Ci(int i, Z52 z52) {
            this.a = i;
            this.b = z52;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ SignUpSocialAuth a;

        @DexIgnore
        public Di(SignUpSocialAuth signUpSocialAuth) {
            Wg6.c(signUpSocialAuth, "auth");
            this.a = signUpSocialAuth;
        }

        @DexIgnore
        public final SignUpSocialAuth a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements Yn5 {
        @DexIgnore
        public /* final */ /* synthetic */ Ev5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei(Ev5 ev5) {
            this.a = ev5;
        }

        @DexIgnore
        @Override // com.fossil.Yn5
        public void a(SignUpSocialAuth signUpSocialAuth) {
            Wg6.c(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Ev5.f.a();
            local.d(a2, "Inside .onLoginSuccess with result=" + signUpSocialAuth);
            this.a.j(new Di(signUpSocialAuth));
        }

        @DexIgnore
        @Override // com.fossil.Yn5
        public void b(int i, Z52 z52, String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Ev5.f.a();
            local.d(a2, "Inside .onLoginFailed with errorCode=" + i + ", connectionResult=" + z52);
            this.a.i(new Ci(i, z52));
        }
    }

    /*
    static {
        String simpleName = Ev5.class.getSimpleName();
        Wg6.b(simpleName, "LoginGoogleUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public Ev5(Vn5 vn5) {
        Wg6.c(vn5, "mLoginGoogleManager");
        this.d = vn5;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return e;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Bi bi, Xe6 xe6) {
        return n(bi, xe6);
    }

    @DexIgnore
    public Object n(Bi bi, Xe6<Object> xe6) {
        try {
            FLogger.INSTANCE.getLocal().d(e, "running UseCase");
            if (!PortfolioApp.get.instance().p0()) {
                return new Ci(601, null);
            }
            Vn5 vn5 = this.d;
            WeakReference<BaseActivity> a2 = bi != null ? bi.a() : null;
            if (a2 != null) {
                vn5.g(a2, new Ei(this));
                return Cd6.a;
            }
            Wg6.i();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = e;
            local.d(str, "Inside .run failed with exception=" + e2);
            return new Ci(600, null);
        }
    }
}
