package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H6 extends U5 {
    @DexIgnore
    public int k; // = 23;
    @DexIgnore
    public /* final */ int l;

    @DexIgnore
    public H6(int i, N4 n4) {
        super(V5.g, n4);
        this.l = i;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void d(K5 k5) {
        k5.b(this.l);
    }

    @DexIgnore
    @Override // com.fossil.U5
    public boolean i(H7 h7) {
        return h7 instanceof K7;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public Fd0<H7> j() {
        return this.j.c;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void k(H7 h7) {
        this.k = ((K7) h7).b;
    }
}
