package com.fossil;

import com.fossil.P18;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B38 implements S28 {
    @DexIgnore
    public /* final */ OkHttpClient a;
    @DexIgnore
    public /* final */ P28 b;
    @DexIgnore
    public /* final */ K48 c;
    @DexIgnore
    public /* final */ J48 d;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public long f; // = 262144;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class Bi implements C58 {
        @DexIgnore
        public /* final */ O48 b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public long d;

        @DexIgnore
        public Bi() {
            this.b = new O48(B38.this.c.e());
            this.d = 0;
        }

        @DexIgnore
        public final void a(boolean z, IOException iOException) throws IOException {
            B38 b38 = B38.this;
            int i = b38.e;
            if (i != 6) {
                if (i == 5) {
                    b38.g(this.b);
                    B38 b382 = B38.this;
                    b382.e = 6;
                    P28 p28 = b382.b;
                    if (p28 != null) {
                        p28.r(!z, b382, this.d, iOException);
                        return;
                    }
                    return;
                }
                throw new IllegalStateException("state: " + B38.this.e);
            }
        }

        @DexIgnore
        @Override // com.fossil.C58
        public long d0(I48 i48, long j) throws IOException {
            try {
                long d0 = B38.this.c.d0(i48, j);
                if (d0 > 0) {
                    this.d += d0;
                }
                return d0;
            } catch (IOException e2) {
                a(false, e2);
                throw e2;
            }
        }

        @DexIgnore
        @Override // com.fossil.C58
        public D58 e() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci implements A58 {
        @DexIgnore
        public /* final */ O48 b; // = new O48(B38.this.d.e());
        @DexIgnore
        public boolean c;

        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        @Override // com.fossil.A58
        public void K(I48 i48, long j) throws IOException {
            if (this.c) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                B38.this.d.O(j);
                B38.this.d.E("\r\n");
                B38.this.d.K(i48, j);
                B38.this.d.E("\r\n");
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.A58, java.lang.AutoCloseable
        public void close() throws IOException {
            synchronized (this) {
                if (!this.c) {
                    this.c = true;
                    B38.this.d.E("0\r\n\r\n");
                    B38.this.g(this.b);
                    B38.this.e = 3;
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.A58
        public D58 e() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.A58, java.io.Flushable
        public void flush() throws IOException {
            synchronized (this) {
                if (!this.c) {
                    B38.this.d.flush();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends Bi {
        @DexIgnore
        public /* final */ Q18 f;
        @DexIgnore
        public long g; // = -1;
        @DexIgnore
        public boolean h; // = true;

        @DexIgnore
        public Di(Q18 q18) {
            super();
            this.f = q18;
        }

        @DexIgnore
        public final void b() throws IOException {
            if (this.g != -1) {
                B38.this.c.U();
            }
            try {
                this.g = B38.this.c.m0();
                String trim = B38.this.c.U().trim();
                if (this.g < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                    throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.g + trim + "\"");
                } else if (this.g == 0) {
                    this.h = false;
                    U28.g(B38.this.a.o(), this.f, B38.this.n());
                    a(true, null);
                }
            } catch (NumberFormatException e) {
                throw new ProtocolException(e.getMessage());
            }
        }

        @DexIgnore
        @Override // com.fossil.C58, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!this.c) {
                if (this.h && !B28.p(this, 100, TimeUnit.MILLISECONDS)) {
                    a(false, null);
                }
                this.c = true;
            }
        }

        @DexIgnore
        @Override // com.fossil.C58, com.fossil.B38.Bi
        public long d0(I48 i48, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.c) {
                throw new IllegalStateException("closed");
            } else if (!this.h) {
                return -1;
            } else {
                long j2 = this.g;
                if (j2 == 0 || j2 == -1) {
                    b();
                    if (!this.h) {
                        return -1;
                    }
                }
                long d0 = super.d0(i48, Math.min(j, this.g));
                if (d0 != -1) {
                    this.g -= d0;
                    return d0;
                }
                ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                a(false, protocolException);
                throw protocolException;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ei implements A58 {
        @DexIgnore
        public /* final */ O48 b; // = new O48(B38.this.d.e());
        @DexIgnore
        public boolean c;
        @DexIgnore
        public long d;

        @DexIgnore
        public Ei(long j) {
            this.d = j;
        }

        @DexIgnore
        @Override // com.fossil.A58
        public void K(I48 i48, long j) throws IOException {
            if (!this.c) {
                B28.f(i48.p0(), 0, j);
                if (j <= this.d) {
                    B38.this.d.K(i48, j);
                    this.d -= j;
                    return;
                }
                throw new ProtocolException("expected " + this.d + " bytes but received " + j);
            }
            throw new IllegalStateException("closed");
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.A58, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!this.c) {
                this.c = true;
                if (this.d <= 0) {
                    B38.this.g(this.b);
                    B38.this.e = 3;
                    return;
                }
                throw new ProtocolException("unexpected end of stream");
            }
        }

        @DexIgnore
        @Override // com.fossil.A58
        public D58 e() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.A58, java.io.Flushable
        public void flush() throws IOException {
            if (!this.c) {
                B38.this.d.flush();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi extends Bi {
        @DexIgnore
        public long f;

        @DexIgnore
        public Fi(B38 b38, long j) throws IOException {
            super();
            this.f = j;
            if (j == 0) {
                a(true, null);
            }
        }

        @DexIgnore
        @Override // com.fossil.C58, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!this.c) {
                if (this.f != 0 && !B28.p(this, 100, TimeUnit.MILLISECONDS)) {
                    a(false, null);
                }
                this.c = true;
            }
        }

        @DexIgnore
        @Override // com.fossil.C58, com.fossil.B38.Bi
        public long d0(I48 i48, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (!this.c) {
                long j2 = this.f;
                if (j2 == 0) {
                    return -1;
                }
                long d0 = super.d0(i48, Math.min(j2, j));
                if (d0 != -1) {
                    long j3 = this.f - d0;
                    this.f = j3;
                    if (j3 == 0) {
                        a(true, null);
                    }
                    return d0;
                }
                ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                a(false, protocolException);
                throw protocolException;
            } else {
                throw new IllegalStateException("closed");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi extends Bi {
        @DexIgnore
        public boolean f;

        @DexIgnore
        public Gi(B38 b38) {
            super();
        }

        @DexIgnore
        @Override // com.fossil.C58, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!this.c) {
                if (!this.f) {
                    a(false, null);
                }
                this.c = true;
            }
        }

        @DexIgnore
        @Override // com.fossil.C58, com.fossil.B38.Bi
        public long d0(I48 i48, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.c) {
                throw new IllegalStateException("closed");
            } else if (this.f) {
                return -1;
            } else {
                long d0 = super.d0(i48, j);
                if (d0 != -1) {
                    return d0;
                }
                this.f = true;
                a(true, null);
                return -1;
            }
        }
    }

    @DexIgnore
    public B38(OkHttpClient okHttpClient, P28 p28, K48 k48, J48 j48) {
        this.a = okHttpClient;
        this.b = p28;
        this.c = k48;
        this.d = j48;
    }

    @DexIgnore
    @Override // com.fossil.S28
    public void a() throws IOException {
        this.d.flush();
    }

    @DexIgnore
    @Override // com.fossil.S28
    public void b(V18 v18) throws IOException {
        o(v18.e(), Y28.a(v18, this.b.d().q().b().type()));
    }

    @DexIgnore
    @Override // com.fossil.S28
    public W18 c(Response response) throws IOException {
        P28 p28 = this.b;
        p28.f.q(p28.e);
        String j = response.j("Content-Type");
        if (!U28.c(response)) {
            return new X28(j, 0, S48.d(k(0)));
        }
        if ("chunked".equalsIgnoreCase(response.j("Transfer-Encoding"))) {
            return new X28(j, -1, S48.d(i(response.G().j())));
        }
        long b2 = U28.b(response);
        return b2 != -1 ? new X28(j, b2, S48.d(k(b2))) : new X28(j, -1, S48.d(l()));
    }

    @DexIgnore
    @Override // com.fossil.S28
    public void cancel() {
        L28 d2 = this.b.d();
        if (d2 != null) {
            d2.d();
        }
    }

    @DexIgnore
    @Override // com.fossil.S28
    public Response.a d(boolean z) throws IOException {
        int i = this.e;
        if (i == 1 || i == 3) {
            try {
                A38 a2 = A38.a(m());
                Response.a aVar = new Response.a();
                aVar.n(a2.a);
                aVar.g(a2.b);
                aVar.k(a2.c);
                aVar.j(n());
                if (z && a2.b == 100) {
                    return null;
                }
                if (a2.b == 100) {
                    this.e = 3;
                    return aVar;
                }
                this.e = 4;
                return aVar;
            } catch (EOFException e2) {
                IOException iOException = new IOException("unexpected end of stream on " + this.b);
                iOException.initCause(e2);
                throw iOException;
            }
        } else {
            throw new IllegalStateException("state: " + this.e);
        }
    }

    @DexIgnore
    @Override // com.fossil.S28
    public void e() throws IOException {
        this.d.flush();
    }

    @DexIgnore
    @Override // com.fossil.S28
    public A58 f(V18 v18, long j) {
        if ("chunked".equalsIgnoreCase(v18.c("Transfer-Encoding"))) {
            return h();
        }
        if (j != -1) {
            return j(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    @DexIgnore
    public void g(O48 o48) {
        D58 i = o48.i();
        o48.j(D58.d);
        i.a();
        i.b();
    }

    @DexIgnore
    public A58 h() {
        if (this.e == 1) {
            this.e = 2;
            return new Ci();
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public C58 i(Q18 q18) throws IOException {
        if (this.e == 4) {
            this.e = 5;
            return new Di(q18);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public A58 j(long j) {
        if (this.e == 1) {
            this.e = 2;
            return new Ei(j);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public C58 k(long j) throws IOException {
        if (this.e == 4) {
            this.e = 5;
            return new Fi(this, j);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public C58 l() throws IOException {
        if (this.e == 4) {
            P28 p28 = this.b;
            if (p28 != null) {
                this.e = 5;
                p28.j();
                return new Gi(this);
            }
            throw new IllegalStateException("streamAllocation == null");
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public final String m() throws IOException {
        String z = this.c.z(this.f);
        this.f -= (long) z.length();
        return z;
    }

    @DexIgnore
    public P18 n() throws IOException {
        P18.Ai ai = new P18.Ai();
        while (true) {
            String m = m();
            if (m.length() == 0) {
                return ai.e();
            }
            Z18.a.a(ai, m);
        }
    }

    @DexIgnore
    public void o(P18 p18, String str) throws IOException {
        if (this.e == 0) {
            this.d.E(str).E("\r\n");
            int h = p18.h();
            for (int i = 0; i < h; i++) {
                this.d.E(p18.e(i)).E(": ").E(p18.i(i)).E("\r\n");
            }
            this.d.E("\r\n");
            this.e = 1;
            return;
        }
        throw new IllegalStateException("state: " + this.e);
    }
}
