package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rl1 extends Nu1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public Vs1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Rl1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Rl1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                byte[] createByteArray = parcel.createByteArray();
                if (createByteArray != null) {
                    Wg6.b(createByteArray, "parcel.createByteArray()!!");
                    Parcelable readParcelable = parcel.readParcelable(Vs1.class.getClassLoader());
                    if (readParcelable != null) {
                        return new Rl1(readString, createByteArray, (Vs1) readParcelable);
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Rl1[] newArray(int i) {
            return new Rl1[i];
        }
    }

    @DexIgnore
    public Rl1(String str, byte[] bArr, Vs1 vs1) {
        super(str, bArr);
        this.e = vs1;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Rl1(String str, byte[] bArr, Vs1 vs1, int i, Qg6 qg6) {
        this(str, bArr, (i & 4) != 0 ? new Vs1(0, 62, 0) : vs1);
    }

    @DexIgnore
    public final void a(Vs1 vs1) {
        this.e = vs1;
    }

    @DexIgnore
    public final JSONObject e() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("image_name", c()).put("pos", this.e.toJSONObject());
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final Vs1 getPositionConfig() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Ox1, com.fossil.Nu1
    public JSONObject toJSONObject() {
        JSONObject put = super.toJSONObject().put("pos", this.e.toJSONObject());
        Wg6.b(put, "super.toJSONObject()\n   \u2026ionConfig.toJSONObject())");
        return put;
    }

    @DexIgnore
    @Override // com.fossil.Nu1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
    }
}
