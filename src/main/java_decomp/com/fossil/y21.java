package com.fossil;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import androidx.work.impl.WorkDatabase;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Y21 implements G21, K11 {
    @DexIgnore
    public static /* final */ String m; // = X01.f("SystemFgDispatcher");
    @DexIgnore
    public Context b;
    @DexIgnore
    public S11 c;
    @DexIgnore
    public /* final */ K41 d;
    @DexIgnore
    public /* final */ Object e; // = new Object();
    @DexIgnore
    public String f;
    @DexIgnore
    public T01 g;
    @DexIgnore
    public /* final */ Map<String, T01> h;
    @DexIgnore
    public /* final */ Map<String, O31> i;
    @DexIgnore
    public /* final */ Set<O31> j;
    @DexIgnore
    public /* final */ H21 k;
    @DexIgnore
    public Bi l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ WorkDatabase b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore
        public Ai(WorkDatabase workDatabase, String str) {
            this.b = workDatabase;
            this.c = str;
        }

        @DexIgnore
        public void run() {
            O31 o = this.b.j().o(this.c);
            if (o != null && o.b()) {
                synchronized (Y21.this.e) {
                    Y21.this.i.put(this.c, o);
                    Y21.this.j.add(o);
                    Y21.this.k.d(Y21.this.j);
                }
            }
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void b(int i, int i2, Notification notification);

        @DexIgnore
        void c(int i, Notification notification);

        @DexIgnore
        void d(int i);

        @DexIgnore
        Object stop();  // void declaration
    }

    @DexIgnore
    public Y21(Context context) {
        this.b = context;
        S11 l2 = S11.l(this.b);
        this.c = l2;
        this.d = l2.q();
        this.f = null;
        this.g = null;
        this.h = new LinkedHashMap();
        this.j = new HashSet();
        this.i = new HashMap();
        this.k = new H21(this.b, this.d, this);
        this.c.n().b(this);
    }

    @DexIgnore
    public final void a(Intent intent) {
        X01.c().d(m, String.format("Stopping foreground work for %s", intent), new Throwable[0]);
        String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        if (stringExtra != null && !TextUtils.isEmpty(stringExtra)) {
            this.c.g(UUID.fromString(stringExtra));
        }
    }

    @DexIgnore
    @Override // com.fossil.G21
    public void b(List<String> list) {
        if (!list.isEmpty()) {
            for (String str : list) {
                X01.c().a(m, String.format("Constraints unmet for WorkSpec %s", str), new Throwable[0]);
                this.c.x(str);
            }
        }
    }

    @DexIgnore
    public final void c(Intent intent) {
        int i2 = 0;
        int intExtra = intent.getIntExtra("KEY_NOTIFICATION_ID", 0);
        int intExtra2 = intent.getIntExtra("KEY_FOREGROUND_SERVICE_TYPE", 0);
        String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        Notification notification = (Notification) intent.getParcelableExtra("KEY_NOTIFICATION");
        X01.c().a(m, String.format("Notifying with (id: %s, workSpecId: %s, notificationType: %s)", Integer.valueOf(intExtra), stringExtra, Integer.valueOf(intExtra2)), new Throwable[0]);
        if (!(notification == null || this.l == null)) {
            this.h.put(stringExtra, new T01(intExtra, notification, intExtra2));
            if (TextUtils.isEmpty(this.f)) {
                this.f = stringExtra;
                this.l.b(intExtra, intExtra2, notification);
                return;
            }
            this.l.c(intExtra, notification);
            if (intExtra2 != 0 && Build.VERSION.SDK_INT >= 29) {
                for (Map.Entry<String, T01> entry : this.h.entrySet()) {
                    i2 = entry.getValue().a() | i2;
                }
                T01 t01 = this.h.get(this.f);
                if (t01 != null) {
                    this.l.b(t01.c(), i2, t01.b());
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.K11
    public void d(String str, boolean z) {
        Bi bi;
        Map.Entry<String, T01> entry;
        synchronized (this.e) {
            O31 remove = this.i.remove(str);
            if (remove != null ? this.j.remove(remove) : false) {
                this.k.d(this.j);
            }
        }
        this.g = this.h.remove(str);
        if (!str.equals(this.f)) {
            T01 t01 = this.g;
            if (t01 != null && (bi = this.l) != null) {
                bi.d(t01.c());
            }
        } else if (this.h.size() > 0) {
            Iterator<Map.Entry<String, T01>> it = this.h.entrySet().iterator();
            Map.Entry<String, T01> next = it.next();
            while (true) {
                entry = next;
                if (!it.hasNext()) {
                    break;
                }
                next = it.next();
            }
            this.f = entry.getKey();
            if (this.l != null) {
                T01 value = entry.getValue();
                this.l.b(value.c(), value.a(), value.b());
                this.l.d(value.c());
            }
        }
    }

    @DexIgnore
    public final void e(Intent intent) {
        X01.c().d(m, String.format("Started foreground service %s", intent), new Throwable[0]);
        String stringExtra = intent.getStringExtra("KEY_WORKSPEC_ID");
        this.d.b(new Ai(this.c.p(), stringExtra));
    }

    @DexIgnore
    @Override // com.fossil.G21
    public void f(List<String> list) {
    }

    @DexIgnore
    public void g() {
        X01.c().d(m, "Stopping foreground service", new Throwable[0]);
        Bi bi = this.l;
        if (bi != null) {
            T01 t01 = this.g;
            if (t01 != null) {
                bi.d(t01.c());
                this.g = null;
            }
            this.l.stop();
        }
    }

    @DexIgnore
    public void h() {
        this.l = null;
        synchronized (this.e) {
            this.k.e();
        }
        this.c.n().h(this);
    }

    @DexIgnore
    public void i(Intent intent) {
        String action = intent.getAction();
        if ("ACTION_START_FOREGROUND".equals(action)) {
            e(intent);
            c(intent);
        } else if ("ACTION_NOTIFY".equals(action)) {
            c(intent);
        } else if ("ACTION_CANCEL_WORK".equals(action)) {
            a(intent);
        }
    }

    @DexIgnore
    public void j(Bi bi) {
        if (this.l != null) {
            X01.c().b(m, "A callback already exists.", new Throwable[0]);
        } else {
            this.l = bi;
        }
    }
}
