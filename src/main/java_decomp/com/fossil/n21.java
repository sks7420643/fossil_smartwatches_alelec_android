package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class N21 extends K21<F21> {
    @DexIgnore
    public static /* final */ String e; // = X01.f("NetworkNotRoamingCtrlr");

    @DexIgnore
    public N21(Context context, K41 k41) {
        super(W21.c(context, k41).d());
    }

    @DexIgnore
    @Override // com.fossil.K21
    public boolean b(O31 o31) {
        return o31.j.b() == Y01.NOT_ROAMING;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.K21
    public /* bridge */ /* synthetic */ boolean c(F21 f21) {
        return i(f21);
    }

    @DexIgnore
    public boolean i(F21 f21) {
        if (Build.VERSION.SDK_INT >= 24) {
            return !f21.a() || !f21.c();
        }
        X01.c().a(e, "Not-roaming network constraint is not supported before API 24, only checking for connected state.", new Throwable[0]);
        return !f21.a();
    }
}
