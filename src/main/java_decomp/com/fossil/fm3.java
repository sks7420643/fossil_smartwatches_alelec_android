package com.fossil;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fm3 implements ServiceConnection {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ Cm3 b;

    @DexIgnore
    public Fm3(Cm3 cm3, String str) {
        this.b = cm3;
        this.a = str;
    }

    @DexIgnore
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (iBinder == null) {
            this.b.a.d().I().a("Install Referrer connection returned with null binder");
            return;
        }
        try {
            Bw2 e = Dz2.e(iBinder);
            if (e == null) {
                this.b.a.d().I().a("Install Referrer Service implementation was not found");
                return;
            }
            this.b.a.d().N().a("Install Referrer Service connected");
            this.b.a.c().y(new Em3(this, e, this));
        } catch (Exception e2) {
            this.b.a.d().I().b("Exception occurred while calling Install Referrer API", e2);
        }
    }

    @DexIgnore
    public final void onServiceDisconnected(ComponentName componentName) {
        this.b.a.d().N().a("Install Referrer Service disconnected");
    }
}
