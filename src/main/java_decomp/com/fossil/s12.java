package com.fossil;

import com.fossil.V12;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S12 extends V12.Bi {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ Set<V12.Ci> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends V12.Bi.Aii {
        @DexIgnore
        public Long a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public Set<V12.Ci> c;

        @DexIgnore
        @Override // com.fossil.V12.Bi.Aii
        public V12.Bi a() {
            String str = "";
            if (this.a == null) {
                str = " delta";
            }
            if (this.b == null) {
                str = str + " maxAllowedDelay";
            }
            if (this.c == null) {
                str = str + " flags";
            }
            if (str.isEmpty()) {
                return new S12(this.a.longValue(), this.b.longValue(), this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.V12.Bi.Aii
        public V12.Bi.Aii b(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.V12.Bi.Aii
        public V12.Bi.Aii c(Set<V12.Ci> set) {
            if (set != null) {
                this.c = set;
                return this;
            }
            throw new NullPointerException("Null flags");
        }

        @DexIgnore
        @Override // com.fossil.V12.Bi.Aii
        public V12.Bi.Aii d(long j) {
            this.b = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public S12(long j, long j2, Set<V12.Ci> set) {
        this.a = j;
        this.b = j2;
        this.c = set;
    }

    @DexIgnore
    @Override // com.fossil.V12.Bi
    public long b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.V12.Bi
    public Set<V12.Ci> c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.V12.Bi
    public long d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof V12.Bi)) {
            return false;
        }
        V12.Bi bi = (V12.Bi) obj;
        return this.a == bi.b() && this.b == bi.d() && this.c.equals(bi.c());
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        long j2 = this.b;
        return ((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ConfigValue{delta=" + this.a + ", maxAllowedDelay=" + this.b + ", flags=" + this.c + "}";
    }
}
