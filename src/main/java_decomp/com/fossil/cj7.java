package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cj7 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Cj7> CREATOR; // = new Ai();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Intent c;
    @DexIgnore
    public /* final */ Fj7 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Cj7> {
        @DexIgnore
        public Cj7 a(Parcel parcel) {
            return new Cj7(parcel, null);
        }

        @DexIgnore
        public Cj7[] b(int i) {
            return new Cj7[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Cj7 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Cj7[] newArray(int i) {
            return b(i);
        }
    }

    @DexIgnore
    public Cj7(Intent intent, int i, Fj7 fj7) {
        this.c = intent;
        this.b = i;
        this.d = fj7;
    }

    @DexIgnore
    public Cj7(Parcel parcel) {
        this.b = parcel.readInt();
        this.c = (Intent) parcel.readParcelable(Cj7.class.getClassLoader());
        this.d = (Fj7) parcel.readSerializable();
    }

    @DexIgnore
    public /* synthetic */ Cj7(Parcel parcel, Ai ai) {
        this(parcel);
    }

    @DexIgnore
    public Fj7 a() {
        return this.d;
    }

    @DexIgnore
    public void b(Activity activity) {
        activity.startActivityForResult(this.c, this.b);
    }

    @DexIgnore
    public void c(Fragment fragment) {
        fragment.startActivityForResult(this.c, this.b);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b);
        parcel.writeParcelable(this.c, i);
        parcel.writeSerializable(this.d);
    }
}
