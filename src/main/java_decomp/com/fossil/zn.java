package com.fossil;

import com.mapped.Cd6;
import com.mapped.Gg6;
import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zn extends Qq7 implements Hg6<Fs, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Lp b;
    @DexIgnore
    public /* final */ /* synthetic */ Gg6 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Zn(Lp lp, Gg6 gg6) {
        super(1);
        this.b = lp;
        this.c = gg6;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Fs fs) {
        Fs fs2 = fs;
        Lp lp = this.b;
        lp.m++;
        lp.v = Nr.a(lp.v, null, Zq.I.a(fs2.v), fs2.v, null, 9);
        Mw mw = fs2.v;
        Lw lw = mw.d;
        if (lw == Lw.g || lw == Lw.o || mw.e.d.b == F7.c) {
            this.b.o(fs2.v);
        } else {
            Short valueOf = fs2 instanceof Dv ? Short.valueOf(((Dv) fs2).L) : fs2 instanceof Zs ? Short.valueOf(((Zs) fs2).F) : null;
            if (valueOf != null) {
                Lp.i(this.b, new Bv(valueOf.shortValue(), this.b.w, 0, 4), new Bn(this, fs2), new Nn(this), null, null, null, 56, null);
            } else {
                this.c.invoke();
            }
        }
        return Cd6.a;
    }
}
