package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ej4 extends Hj4 {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;

    @DexIgnore
    public Ej4(String str) {
        super(str);
    }

    @DexIgnore
    public Ej4(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public Ej4(Throwable th) {
        super(th);
    }
}
