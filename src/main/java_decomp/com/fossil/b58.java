package com.fossil;

import com.mapped.Wg6;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B58 extends G48 {
    @DexIgnore
    public /* final */ Logger l; // = Logger.getLogger("okio.Okio");
    @DexIgnore
    public /* final */ Socket m;

    @DexIgnore
    public B58(Socket socket) {
        Wg6.c(socket, "socket");
        this.m = socket;
    }

    @DexIgnore
    @Override // com.fossil.G48
    public IOException t(IOException iOException) {
        SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
        if (iOException != null) {
            socketTimeoutException.initCause(iOException);
        }
        return socketTimeoutException;
    }

    @DexIgnore
    @Override // com.fossil.G48
    public void x() {
        try {
            this.m.close();
        } catch (Exception e) {
            Logger logger = this.l;
            Level level = Level.WARNING;
            logger.log(level, "Failed to close timed out socket " + this.m, (Throwable) e);
        } catch (AssertionError e2) {
            if (S48.e(e2)) {
                Logger logger2 = this.l;
                Level level2 = Level.WARNING;
                logger2.log(level2, "Failed to close timed out socket " + this.m, (Throwable) e2);
                return;
            }
            throw e2;
        }
    }
}
