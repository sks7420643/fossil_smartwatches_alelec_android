package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager2.widget.ViewPager2;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardActivity;
import com.portfolio.platform.buddy_challenge.screens.tab.history.BCHistoryViewModel;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.customview.LimitSlopeSwipeRefresh;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ky4 extends BaseFragment implements Ny5 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public G37<L75> g;
    @DexIgnore
    public BCHistoryViewModel h;
    @DexIgnore
    public Po4 i;
    @DexIgnore
    public Oy4 j;
    @DexIgnore
    public int k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Ky4.m;
        }

        @DexIgnore
        public final Ky4 b() {
            return new Ky4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ L75 a;
        @DexIgnore
        public /* final */ /* synthetic */ Ky4 b;

        @DexIgnore
        public Bi(L75 l75, Ky4 ky4) {
            this.a = l75;
            this.b = ky4;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void c(int i) {
            super.c(i);
            Hr7 hr7 = Hr7.a;
            String c = Um5.c(PortfolioApp.get.instance(), 2131886239);
            Wg6.b(c, "LanguageHelper.getString\u2026story_Text__OfChallenges)");
            String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i + 1), Integer.valueOf(this.b.P6().getItemCount())}, 2));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            FlexibleTextView flexibleTextView = this.a.s;
            Wg6.b(flexibleTextView, "ftvIndicator");
            flexibleTextView.setText(format);
            Ky4.N6(this.b).q(i, this.b.P6().getItemCount());
            this.b.k = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ L75 a;
        @DexIgnore
        public /* final */ /* synthetic */ Ky4 b;

        @DexIgnore
        public Ci(L75 l75, Ky4 ky4) {
            this.a = l75;
            this.b = ky4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.a.r;
            Wg6.b(flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            Ky4.N6(this.b).r(this.b.k, this.b.P6().getItemCount());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<List<? extends Qt4>> {
        @DexIgnore
        public /* final */ /* synthetic */ Ky4 a;

        @DexIgnore
        public Di(Ky4 ky4) {
            this.a = ky4;
        }

        @DexIgnore
        public final void a(List<Qt4> list) {
            Oy4 P6 = this.a.P6();
            Wg6.b(list, "it");
            P6.i(list);
            Hr7 hr7 = Hr7.a;
            String c = Um5.c(PortfolioApp.get.instance(), 2131886239);
            Wg6.b(c, "LanguageHelper.getString\u2026story_Text__OfChallenges)");
            String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(this.a.k + 1), Integer.valueOf(list.size())}, 2));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            L75 l75 = (L75) Ky4.K6(this.a).a();
            if (l75 != null) {
                FlexibleTextView flexibleTextView = l75.s;
                Wg6.b(flexibleTextView, "ftvIndicator");
                if (format != null) {
                    String upperCase = format.toUpperCase();
                    Wg6.b(upperCase, "(this as java.lang.String).toUpperCase()");
                    flexibleTextView.setText(upperCase);
                    if (!list.isEmpty()) {
                        FlexibleTextView flexibleTextView2 = l75.r;
                        Wg6.b(flexibleTextView2, "ftvError");
                        flexibleTextView2.setVisibility(8);
                        FlexibleTextView flexibleTextView3 = l75.s;
                        Wg6.b(flexibleTextView3, "ftvIndicator");
                        flexibleTextView3.setVisibility(0);
                        FlexibleTextView flexibleTextView4 = l75.t;
                        Wg6.b(flexibleTextView4, "ftvNotice");
                        flexibleTextView4.setVisibility(0);
                        return;
                    }
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends Qt4> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Ky4 a;

        @DexIgnore
        public Ei(Ky4 ky4) {
            this.a = ky4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            L75 l75 = (L75) Ky4.K6(this.a).a();
            if (l75 != null) {
                Wg6.b(bool, "it");
                if (bool.booleanValue()) {
                    FlexibleTextView flexibleTextView = l75.q;
                    Wg6.b(flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(0);
                    ViewPager2 viewPager2 = l75.v;
                    Wg6.b(viewPager2, "subTabs");
                    viewPager2.setVisibility(8);
                    FlexibleTextView flexibleTextView2 = l75.s;
                    Wg6.b(flexibleTextView2, "ftvIndicator");
                    flexibleTextView2.setVisibility(8);
                    FlexibleTextView flexibleTextView3 = l75.t;
                    Wg6.b(flexibleTextView3, "ftvNotice");
                    flexibleTextView3.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView4 = l75.q;
                Wg6.b(flexibleTextView4, "ftvEmpty");
                flexibleTextView4.setVisibility(8);
                ViewPager2 viewPager22 = l75.v;
                Wg6.b(viewPager22, "subTabs");
                viewPager22.setVisibility(0);
                FlexibleTextView flexibleTextView5 = l75.s;
                Wg6.b(flexibleTextView5, "ftvIndicator");
                flexibleTextView5.setVisibility(0);
                FlexibleTextView flexibleTextView6 = l75.t;
                Wg6.b(flexibleTextView6, "ftvNotice");
                flexibleTextView6.setVisibility(0);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Ky4 a;

        @DexIgnore
        public Fi(Ky4 ky4) {
            this.a = ky4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            L75 l75 = (L75) Ky4.K6(this.a).a();
            if (l75 != null) {
                LimitSlopeSwipeRefresh limitSlopeSwipeRefresh = l75.w;
                Wg6.b(limitSlopeSwipeRefresh, "swipe");
                Wg6.b(bool, "it");
                limitSlopeSwipeRefresh.setRefreshing(bool.booleanValue());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> implements Ls0<Lc6<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ Ky4 a;

        @DexIgnore
        public Gi(Ky4 ky4) {
            this.a = ky4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, ? extends ServerError> lc6) {
            if (lc6 != null) {
                boolean booleanValue = lc6.getFirst().booleanValue();
                ServerError serverError = (ServerError) lc6.getSecond();
                L75 l75 = (L75) Ky4.K6(this.a).a();
                if (l75 != null) {
                    FlexibleTextView flexibleTextView = l75.q;
                    Wg6.b(flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(8);
                    if (booleanValue) {
                        FlexibleTextView flexibleTextView2 = l75.t;
                        Wg6.b(flexibleTextView2, "ftvNotice");
                        flexibleTextView2.setVisibility(8);
                        FlexibleTextView flexibleTextView3 = l75.s;
                        Wg6.b(flexibleTextView3, "ftvIndicator");
                        flexibleTextView3.setVisibility(8);
                        FlexibleTextView flexibleTextView4 = l75.r;
                        Wg6.b(flexibleTextView4, "ftvError");
                        flexibleTextView4.setVisibility(0);
                        return;
                    }
                    FlexibleTextView flexibleTextView5 = l75.r;
                    Wg6.b(flexibleTextView5, "ftvError");
                    flexibleTextView5.setVisibility(8);
                    if (this.a.isResumed()) {
                        FlexibleTextView flexibleTextView6 = l75.r;
                        Wg6.b(flexibleTextView6, "ftvError");
                        String c = Um5.c(flexibleTextView6.getContext(), 2131886231);
                        FragmentActivity activity = this.a.getActivity();
                        if (activity != null) {
                            Toast.makeText(activity, c, 1).show();
                        }
                    }
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends ServerError> lc6) {
            a(lc6);
        }
    }

    /*
    static {
        String simpleName = Ky4.class.getSimpleName();
        Wg6.b(simpleName, "BCHistoryFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 K6(Ky4 ky4) {
        G37<L75> g37 = ky4.g;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ BCHistoryViewModel N6(Ky4 ky4) {
        BCHistoryViewModel bCHistoryViewModel = ky4.h;
        if (bCHistoryViewModel != null) {
            return bCHistoryViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final Oy4 P6() {
        Oy4 oy4 = this.j;
        if (oy4 != null) {
            return oy4;
        }
        Wg6.n("historyAdapter");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        G37<L75> g37 = this.g;
        if (g37 != null) {
            L75 a2 = g37.a();
            if (a2 != null) {
                this.j = new Oy4(this);
                ViewPager2 viewPager2 = a2.v;
                Wg6.b(viewPager2, "this");
                Oy4 oy4 = this.j;
                if (oy4 != null) {
                    viewPager2.setAdapter(oy4);
                    viewPager2.setOffscreenPageLimit(5);
                    a2.v.g(new Bi(a2, this));
                    a2.w.setOnRefreshListener(new Ci(a2, this));
                    return;
                }
                Wg6.n("historyAdapter");
                throw null;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final void R6() {
        BCHistoryViewModel bCHistoryViewModel = this.h;
        if (bCHistoryViewModel != null) {
            bCHistoryViewModel.m().h(getViewLifecycleOwner(), new Di(this));
            BCHistoryViewModel bCHistoryViewModel2 = this.h;
            if (bCHistoryViewModel2 != null) {
                bCHistoryViewModel2.k().h(getViewLifecycleOwner(), new Ei(this));
                BCHistoryViewModel bCHistoryViewModel3 = this.h;
                if (bCHistoryViewModel3 != null) {
                    bCHistoryViewModel3.n().h(getViewLifecycleOwner(), new Fi(this));
                    BCHistoryViewModel bCHistoryViewModel4 = this.h;
                    if (bCHistoryViewModel4 != null) {
                        bCHistoryViewModel4.l().h(getViewLifecycleOwner(), new Gi(this));
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ny5
    public void e6(Ps4 ps4) {
        Wg6.c(ps4, "challenge");
        BCOverviewLeaderBoardActivity.A.a(this, ps4, ps4.f());
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().N1().a(this);
        Po4 po4 = this.i;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCHistoryViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026oryViewModel::class.java)");
            this.h = (BCHistoryViewModel) a2;
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        L75 l75 = (L75) Aq0.f(layoutInflater, 2131558569, viewGroup, false, A6());
        this.g = new G37<>(this, l75);
        Wg6.b(l75, "binding");
        return l75.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        Q6();
        R6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
