package com.fossil;

import com.mapped.Ku3;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Oq5 {
    @DexIgnore
    public static /* final */ String b; // = "com.fossil.oq5";
    @DexIgnore
    public Weather a;

    @DexIgnore
    public Weather a() {
        return this.a;
    }

    @DexIgnore
    public void b(Ku3 ku3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "Inside " + b + ".parse - json=" + ku3);
        try {
            Zi4 zi4 = new Zi4();
            zi4.f(DateTime.class, new GsonConvertDateTime());
            zi4.f(Date.class, new GsonConverterShortDate());
            this.a = (Weather) zi4.d().k(ku3.toString(), Weather.class);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = b;
            local2.d(str2, "parse mWeather=" + this.a);
        } catch (Exception e) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = b;
            local3.e(str3, "parse error=" + e.toString());
        }
    }
}
