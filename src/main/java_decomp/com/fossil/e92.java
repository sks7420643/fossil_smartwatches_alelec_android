package com.fossil;

import android.os.Looper;
import android.os.Message;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E92 extends Ol2 {
    @DexIgnore
    public /* final */ /* synthetic */ C92 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public E92(C92 c92, Looper looper) {
        super(looper);
        this.a = c92;
    }

    @DexIgnore
    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            ((F92) message.obj).b(this.a);
        } else if (i != 2) {
            StringBuilder sb = new StringBuilder(31);
            sb.append("Unknown message id: ");
            sb.append(i);
            Log.w("GACStateManager", sb.toString());
        } else {
            throw ((RuntimeException) message.obj);
        }
    }
}
