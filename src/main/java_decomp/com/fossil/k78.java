package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class K78 implements J78 {
    @DexIgnore
    public P78 a;
    @DexIgnore
    public Object[] b;

    @DexIgnore
    public P78 a() {
        return this.a;
    }

    @DexIgnore
    public void b(Object[] objArr) {
        this.b = objArr;
    }

    @DexIgnore
    public void c(I78 i78) {
    }

    @DexIgnore
    public void d(P78 p78) {
        this.a = p78;
    }

    @DexIgnore
    public void e(String str) {
    }

    @DexIgnore
    public void f(G78 g78) {
    }

    @DexIgnore
    public void g(String str) {
    }

    @DexIgnore
    public void h(String str) {
    }

    @DexIgnore
    public void i(Throwable th) {
    }

    @DexIgnore
    public void j(long j) {
    }
}
