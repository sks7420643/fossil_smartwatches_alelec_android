package com.fossil;

import com.google.gson.Gson;
import com.mapped.Fu3;
import com.mapped.Hp4;
import com.mapped.Ku3;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.portfolio.platform.data.legacy.threedotzero.DeclarationFile;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant;
import com.portfolio.platform.data.model.Range;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.format.ISODateTimeFormat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Nq5 {
    @DexIgnore
    public List<Hp4> a; // = new ArrayList();
    @DexIgnore
    public Range b;

    @DexIgnore
    public List<Hp4> a() {
        return this.a;
    }

    @DexIgnore
    public Range b() {
        return this.b;
    }

    @DexIgnore
    public void c(Ku3 ku3) {
        this.a = new ArrayList();
        if (ku3.s(CloudLogWriter.ITEMS_PARAM)) {
            try {
                Fu3 q = ku3.q(CloudLogWriter.ITEMS_PARAM);
                if (q.size() > 0) {
                    for (int i = 0; i < q.size(); i++) {
                        Ku3 d = q.m(i).d();
                        Hp4 hp4 = new Hp4();
                        if (d.s("appId")) {
                            hp4.f(d.p("appId").f());
                        }
                        if (d.s("name")) {
                            hp4.l(d.p("name").f());
                        }
                        if (d.s("description")) {
                            hp4.i(d.p("description").f());
                        }
                        if (d.s(MicroAppVariant.COLUMN_MAJOR_NUMBER)) {
                            hp4.j(d.p(MicroAppVariant.COLUMN_MAJOR_NUMBER).b());
                        }
                        if (d.s(MicroAppVariant.COLUMN_MINOR_NUMBER)) {
                            hp4.k(d.p(MicroAppVariant.COLUMN_MINOR_NUMBER).b());
                        }
                        if (d.s("updatedAt")) {
                            hp4.n(ISODateTimeFormat.dateTimeNoMillis().parseDateTime(d.p("updatedAt").f()).getMillis());
                        }
                        if (d.s("createdAt")) {
                            hp4.g(ISODateTimeFormat.dateTimeNoMillis().parseDateTime(d.p("createdAt").f()).getMillis());
                        }
                        if (d.s(MicroAppVariant.COLUMN_DECLARATION_FILES)) {
                            Fu3 q2 = d.q(MicroAppVariant.COLUMN_DECLARATION_FILES);
                            ArrayList arrayList = new ArrayList();
                            if (q2.size() > 0) {
                                for (int i2 = 0; i2 < q2.size(); i2++) {
                                    Ku3 d2 = q2.m(i2).d();
                                    DeclarationFile declarationFile = new DeclarationFile();
                                    if (d2.s(DeclarationFile.COLUMN_FILE_ID)) {
                                        declarationFile.setFileId(d2.p(DeclarationFile.COLUMN_FILE_ID).f());
                                    }
                                    if (d2.s("description")) {
                                        declarationFile.setDescription(d2.p("description").f());
                                    }
                                    if (d2.s("content")) {
                                        declarationFile.setContent(d2.p("content").f());
                                    }
                                    arrayList.add(declarationFile);
                                }
                            }
                            hp4.h(arrayList);
                        }
                        this.a.add(hp4);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (ku3.s("_range")) {
            try {
                this.b = (Range) new Gson().k(ku3.r("_range").toString(), Range.class);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
