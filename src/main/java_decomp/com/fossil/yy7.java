package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yy7<T> {
    @DexIgnore
    public static /* final */ Bi b; // = new Bi(null);
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ Throwable a;

        @DexIgnore
        public Ai(Throwable th) {
            this.a = th;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return (obj instanceof Ai) && Wg6.a(this.a, ((Ai) obj).a);
        }

        @DexIgnore
        public int hashCode() {
            Throwable th = this.a;
            if (th != null) {
                return th.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "Closed(" + this.a + ')';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public /* synthetic */ Bi(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public /* synthetic */ Yy7(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public static final /* synthetic */ Yy7 a(Object obj) {
        return new Yy7(obj);
    }

    @DexIgnore
    public static Object b(Object obj) {
        return obj;
    }

    @DexIgnore
    public static boolean c(Object obj, Object obj2) {
        return (obj2 instanceof Yy7) && Wg6.a(obj, ((Yy7) obj2).f());
    }

    @DexIgnore
    public static int d(Object obj) {
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public static String e(Object obj) {
        if (obj instanceof Ai) {
            return obj.toString();
        }
        return "Value(" + obj + ')';
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return c(this.a, obj);
    }

    @DexIgnore
    public final /* synthetic */ Object f() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return d(this.a);
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }
}
