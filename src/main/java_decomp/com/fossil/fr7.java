package com.fossil;

import com.mapped.Bh6;
import com.mapped.Ch6;
import com.mapped.Hi6;
import com.mapped.Ni6;
import com.mapped.Yg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fr7 {
    @DexIgnore
    public Gs7 a(Nq7 nq7) {
        return nq7;
    }

    @DexIgnore
    public Es7 b(Class cls) {
        return new Iq7(cls);
    }

    @DexIgnore
    public Hi6 c(Class cls, String str) {
        return new Vq7(cls, str);
    }

    @DexIgnore
    public Is7 d(Yg6 yg6) {
        return yg6;
    }

    @DexIgnore
    public Js7 e(Sq7 sq7) {
        return sq7;
    }

    @DexIgnore
    public Ls7 f(Bh6 bh6) {
        return bh6;
    }

    @DexIgnore
    public Ni6 g(Ch6 ch6) {
        return ch6;
    }

    @DexIgnore
    public String h(Mq7 mq7) {
        String obj = mq7.getClass().getGenericInterfaces()[0].toString();
        return obj.startsWith("kotlin.jvm.functions.") ? obj.substring(21) : obj;
    }

    @DexIgnore
    public String i(Qq7 qq7) {
        return h(qq7);
    }
}
