package com.fossil;

import android.graphics.Color;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pl0 {
    @DexIgnore
    public static /* final */ ThreadLocal<double[]> a; // = new ThreadLocal<>();

    @DexIgnore
    public static void a(int i, int i2, int i3, double[] dArr) {
        if (dArr.length == 3) {
            double d = ((double) i) / 255.0d;
            double pow = d < 0.04045d ? d / 12.92d : Math.pow((d + 0.055d) / 1.055d, 2.4d);
            double d2 = ((double) i2) / 255.0d;
            double pow2 = d2 < 0.04045d ? d2 / 12.92d : Math.pow((d2 + 0.055d) / 1.055d, 2.4d);
            double d3 = ((double) i3) / 255.0d;
            double pow3 = d3 < 0.04045d ? d3 / 12.92d : Math.pow((d3 + 0.055d) / 1.055d, 2.4d);
            dArr[0] = ((0.4124d * pow) + (0.3576d * pow2) + (0.1805d * pow3)) * 100.0d;
            dArr[1] = ((0.2126d * pow) + (0.7152d * pow2) + (0.0722d * pow3)) * 100.0d;
            dArr[2] = ((pow3 * 0.9505d) + (pow2 * 0.1192d) + (pow * 0.0193d)) * 100.0d;
            return;
        }
        throw new IllegalArgumentException("outXyz must have a length of 3.");
    }

    @DexIgnore
    public static double b(int i) {
        double[] g = g();
        c(i, g);
        return g[1] / 100.0d;
    }

    @DexIgnore
    public static void c(int i, double[] dArr) {
        a(Color.red(i), Color.green(i), Color.blue(i), dArr);
    }

    @DexIgnore
    public static int d(int i, int i2) {
        return 255 - (((255 - i2) * (255 - i)) / 255);
    }

    @DexIgnore
    public static int e(int i, int i2) {
        int alpha = Color.alpha(i2);
        int alpha2 = Color.alpha(i);
        int d = d(alpha2, alpha);
        return Color.argb(d, f(Color.red(i), alpha2, Color.red(i2), alpha, d), f(Color.green(i), alpha2, Color.green(i2), alpha, d), f(Color.blue(i), alpha2, Color.blue(i2), alpha, d));
    }

    @DexIgnore
    public static int f(int i, int i2, int i3, int i4, int i5) {
        if (i5 == 0) {
            return 0;
        }
        return (((i * 255) * i2) + ((i3 * i4) * (255 - i2))) / (i5 * 255);
    }

    @DexIgnore
    public static double[] g() {
        double[] dArr = a.get();
        if (dArr != null) {
            return dArr;
        }
        double[] dArr2 = new double[3];
        a.set(dArr2);
        return dArr2;
    }

    @DexIgnore
    public static int h(int i, int i2) {
        if (i2 >= 0 && i2 <= 255) {
            return (16777215 & i) | (i2 << 24);
        }
        throw new IllegalArgumentException("alpha must be between 0 and 255.");
    }
}
