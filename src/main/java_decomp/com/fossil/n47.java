package com.fossil;

import com.mapped.An4;
import com.mapped.Gg6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N47 {
    @DexIgnore
    public static /* final */ Yk7 b; // = Zk7.a(Ai.INSTANCE);
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ Bi d; // = new Bi(null);
    @DexIgnore
    public An4 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Qq7 implements Gg6<N47> {
        @DexIgnore
        public static /* final */ Ai INSTANCE; // = new Ai();

        @DexIgnore
        public Ai() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final N47 invoke() {
            throw null;
            //return Ci.b.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public /* synthetic */ Bi(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final N47 a() {
            Yk7 yk7 = N47.b;
            Bi bi = N47.d;
            return (N47) yk7.getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public static /* final */ N47 a; // = new N47();
        @DexIgnore
        public static /* final */ Ci b; // = new Ci();

        @DexIgnore
        public final N47 a() {
            return a;
        }
    }

    /*
    static {
        String simpleName = N47.class.getSimpleName();
        Wg6.b(simpleName, "UserUtils::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public N47() {
        PortfolioApp.get.instance().getIface().I(this);
    }

    @DexIgnore
    public final boolean b(int i, int i2) {
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        long currentTimeMillis = System.currentTimeMillis();
        An4 an4 = this.a;
        if (an4 != null) {
            int days = (int) timeUnit.toDays(currentTimeMillis - an4.y());
            FLogger.INSTANCE.getLocal().d(c, "Inside .isHappyUser");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = c;
            local.d(str, "crashThreshold = " + i + ", successfulSyncThreshold = " + i2 + ", delta in Day = " + days);
            if (days >= i) {
                An4 an42 = this.a;
                if (an42 == null) {
                    Wg6.n("mSharePrefs");
                    throw null;
                } else if (an42.m() >= i2) {
                    return true;
                }
            }
            return false;
        }
        Wg6.n("mSharePrefs");
        throw null;
    }

    @DexIgnore
    public final boolean c() {
        An4 an4 = this.a;
        if (an4 != null) {
            return an4.n0();
        }
        Wg6.n("mSharePrefs");
        throw null;
    }
}
