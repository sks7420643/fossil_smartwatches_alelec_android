package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.LinkedHashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ej0<K, V> {
    @DexIgnore
    public /* final */ LinkedHashMap<K, V> a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;

    @DexIgnore
    public Ej0(int i) {
        if (i > 0) {
            this.c = i;
            this.a = new LinkedHashMap<>(0, 0.75f, true);
            return;
        }
        throw new IllegalArgumentException("maxSize <= 0");
    }

    @DexIgnore
    public V a(K k) {
        return null;
    }

    @DexIgnore
    public void b(boolean z, K k, V v, V v2) {
    }

    @DexIgnore
    public final void c() {
        k(-1);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001e, code lost:
        if (r1 != null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0020, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0022, code lost:
        monitor-enter(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r4.e++;
        r0 = r4.a.put(r5, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002f, code lost:
        if (r0 == null) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0031, code lost:
        r4.a.put(r5, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0036, code lost:
        monitor-exit(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0037, code lost:
        if (r0 == null) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0039, code lost:
        b(false, r5, r1, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003e, code lost:
        r4.b += h(r5, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004b, code lost:
        k(r4.c);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        r1 = a(r5);
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final V d(K r5) {
        /*
            r4 = this;
            if (r5 == 0) goto L_0x0055
            monitor-enter(r4)
            java.util.LinkedHashMap<K, V> r0 = r4.a     // Catch:{ all -> 0x0052 }
            java.lang.Object r0 = r0.get(r5)     // Catch:{ all -> 0x0052 }
            if (r0 == 0) goto L_0x0013
            int r1 = r4.g     // Catch:{ all -> 0x0052 }
            int r1 = r1 + 1
            r4.g = r1     // Catch:{ all -> 0x0052 }
            monitor-exit(r4)     // Catch:{ all -> 0x0052 }
        L_0x0012:
            return r0
        L_0x0013:
            int r0 = r4.h     // Catch:{ all -> 0x0052 }
            int r0 = r0 + 1
            r4.h = r0     // Catch:{ all -> 0x0052 }
            monitor-exit(r4)     // Catch:{ all -> 0x0052 }
            java.lang.Object r1 = r4.a(r5)
            if (r1 != 0) goto L_0x0022
            r0 = 0
            goto L_0x0012
        L_0x0022:
            monitor-enter(r4)
            int r0 = r4.e     // Catch:{ all -> 0x0048 }
            int r0 = r0 + 1
            r4.e = r0     // Catch:{ all -> 0x0048 }
            java.util.LinkedHashMap<K, V> r0 = r4.a     // Catch:{ all -> 0x0048 }
            java.lang.Object r0 = r0.put(r5, r1)     // Catch:{ all -> 0x0048 }
            if (r0 == 0) goto L_0x003e
            java.util.LinkedHashMap<K, V> r2 = r4.a     // Catch:{ all -> 0x0048 }
            r2.put(r5, r0)     // Catch:{ all -> 0x0048 }
        L_0x0036:
            monitor-exit(r4)     // Catch:{ all -> 0x0048 }
            if (r0 == 0) goto L_0x004b
            r2 = 0
            r4.b(r2, r5, r1, r0)
            goto L_0x0012
        L_0x003e:
            int r2 = r4.b
            int r3 = r4.h(r5, r1)
            int r2 = r2 + r3
            r4.b = r2
            goto L_0x0036
        L_0x0048:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x004b:
            int r0 = r4.c
            r4.k(r0)
            r0 = r1
            goto L_0x0012
        L_0x0052:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0055:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            java.lang.String r1 = "key == null"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ej0.d(java.lang.Object):java.lang.Object");
    }

    @DexIgnore
    public final int e() {
        int i;
        synchronized (this) {
            i = this.c;
        }
        return i;
    }

    @DexIgnore
    public final V f(K k, V v) {
        V put;
        if (k == null || v == null) {
            throw new NullPointerException("key == null || value == null");
        }
        synchronized (this) {
            this.d++;
            this.b += h(k, v);
            put = this.a.put(k, v);
            if (put != null) {
                this.b -= h(k, put);
            }
        }
        if (put != null) {
            b(false, k, put, v);
        }
        k(this.c);
        return put;
    }

    @DexIgnore
    public final V g(K k) {
        V remove;
        if (k != null) {
            synchronized (this) {
                remove = this.a.remove(k);
                if (remove != null) {
                    this.b -= h(k, remove);
                }
            }
            if (remove != null) {
                b(false, k, remove, null);
            }
            return remove;
        }
        throw new NullPointerException("key == null");
    }

    @DexIgnore
    public final int h(K k, V v) {
        int j = j(k, v);
        if (j >= 0) {
            return j;
        }
        throw new IllegalStateException("Negative size: " + ((Object) k) + SimpleComparison.EQUAL_TO_OPERATION + ((Object) v));
    }

    @DexIgnore
    public final int i() {
        int i;
        synchronized (this) {
            i = this.b;
        }
        return i;
    }

    @DexIgnore
    public int j(K k, V v) {
        return 1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0070, code lost:
        throw new java.lang.IllegalStateException(getClass().getName() + ".sizeOf() is reporting inconsistent results!");
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void k(int r5) {
        /*
            r4 = this;
        L_0x0000:
            monitor-enter(r4)
            int r0 = r4.b     // Catch:{ all -> 0x0071 }
            if (r0 < 0) goto L_0x0052
            java.util.LinkedHashMap<K, V> r0 = r4.a     // Catch:{ all -> 0x0071 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0071 }
            if (r0 == 0) goto L_0x0011
            int r0 = r4.b     // Catch:{ all -> 0x0071 }
            if (r0 != 0) goto L_0x0052
        L_0x0011:
            int r0 = r4.b     // Catch:{ all -> 0x0071 }
            if (r0 <= r5) goto L_0x001d
            java.util.LinkedHashMap<K, V> r0 = r4.a     // Catch:{ all -> 0x0071 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0071 }
            if (r0 == 0) goto L_0x001f
        L_0x001d:
            monitor-exit(r4)     // Catch:{ all -> 0x0071 }
            return
        L_0x001f:
            java.util.LinkedHashMap<K, V> r0 = r4.a     // Catch:{ all -> 0x0071 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x0071 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0071 }
            java.lang.Object r0 = r0.next()     // Catch:{ all -> 0x0071 }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x0071 }
            java.lang.Object r1 = r0.getKey()     // Catch:{ all -> 0x0071 }
            java.lang.Object r0 = r0.getValue()     // Catch:{ all -> 0x0071 }
            java.util.LinkedHashMap<K, V> r2 = r4.a     // Catch:{ all -> 0x0071 }
            r2.remove(r1)     // Catch:{ all -> 0x0071 }
            int r2 = r4.b     // Catch:{ all -> 0x0071 }
            int r3 = r4.h(r1, r0)     // Catch:{ all -> 0x0071 }
            int r2 = r2 - r3
            r4.b = r2     // Catch:{ all -> 0x0071 }
            int r2 = r4.f     // Catch:{ all -> 0x0071 }
            int r2 = r2 + 1
            r4.f = r2     // Catch:{ all -> 0x0071 }
            monitor-exit(r4)     // Catch:{ all -> 0x0071 }
            r2 = 1
            r3 = 0
            r4.b(r2, r1, r0, r3)
            goto L_0x0000
        L_0x0052:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.Class r2 = r4.getClass()
            java.lang.String r2 = r2.getName()
            r1.append(r2)
            java.lang.String r2 = ".sizeOf() is reporting inconsistent results!"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0071:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ej0.k(int):void");
    }

    @DexIgnore
    public final String toString() {
        String format;
        int i = 0;
        synchronized (this) {
            int i2 = this.g + this.h;
            if (i2 != 0) {
                i = (this.g * 100) / i2;
            }
            format = String.format(Locale.US, "LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]", Integer.valueOf(this.c), Integer.valueOf(this.g), Integer.valueOf(this.h), Integer.valueOf(i));
        }
        return format;
    }
}
