package com.fossil;

import android.os.SystemClock;
import android.view.View;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ez4 implements View.OnClickListener {
    @DexIgnore
    public long b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ Hg6<View, Cd6> d;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.mapped.Hg6<? super android.view.View, com.mapped.Cd6> */
    /* JADX WARN: Multi-variable type inference failed */
    public Ez4(int i, Hg6<? super View, Cd6> hg6) {
        Wg6.c(hg6, "onSafeClick");
        this.c = i;
        this.d = hg6;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Ez4(int i, Hg6 hg6, int i2, Qg6 qg6) {
        this((i2 & 1) != 0 ? 1000 : i, hg6);
    }

    @DexIgnore
    public void onClick(View view) {
        if (SystemClock.elapsedRealtime() - this.b >= ((long) this.c)) {
            this.b = SystemClock.elapsedRealtime();
            this.d.invoke(view);
        }
    }
}
