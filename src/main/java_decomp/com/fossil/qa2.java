package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qa2 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ Z52 b;

    @DexIgnore
    public Qa2(Z52 z52, int i) {
        Rc2.k(z52);
        this.b = z52;
        this.a = i;
    }

    @DexIgnore
    public final Z52 a() {
        return this.b;
    }

    @DexIgnore
    public final int b() {
        return this.a;
    }
}
