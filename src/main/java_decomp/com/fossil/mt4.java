package com.fossil;

import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mt4 {
    @DexIgnore
    @Vu3("challenge")
    public Ps4 a;
    @DexIgnore
    @Vu3("category")
    public String b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final Ps4 b() {
        return this.a;
    }

    @DexIgnore
    public final boolean c() {
        return this.c;
    }

    @DexIgnore
    public final void d(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Mt4) {
                Mt4 mt4 = (Mt4) obj;
                if (!Wg6.a(this.a, mt4.a) || !Wg6.a(this.b, mt4.b) || this.c != mt4.c) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Ps4 ps4 = this.a;
        int hashCode = ps4 != null ? ps4.hashCode() : 0;
        String str = this.b;
        if (str != null) {
            i = str.hashCode();
        }
        boolean z = this.c;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((hashCode * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "Recommended(challenge=" + this.a + ", category=" + this.b + ", isNew=" + this.c + ")";
    }
}
