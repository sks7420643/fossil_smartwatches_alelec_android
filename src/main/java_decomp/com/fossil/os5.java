package com.fossil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.mapped.AlertDialogFragment;
import com.mapped.Kc6;
import com.mapped.PermissionUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.Comparator;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Os5 extends BaseFragment implements AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public BlockingQueue<Uh5> g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<T> implements Comparator<Uh5> {
        @DexIgnore
        public static /* final */ Ai b; // = new Ai();

        @DexIgnore
        public final int a(Uh5 uh5, Uh5 uh52) {
            return uh5.ordinal() - uh52.ordinal();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // java.util.Comparator
        public /* bridge */ /* synthetic */ int compare(Uh5 uh5, Uh5 uh52) {
            return a(uh5, uh52);
        }
    }

    /*
    static {
        String simpleName = Os5.class.getSimpleName();
        Wg6.b(simpleName, "BasePermissionFragment::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public final void K6(Uh5 uh5) {
        BlockingQueue<Uh5> blockingQueue = this.g;
        if (blockingQueue == null) {
            Wg6.n("mPermissionQueue");
            throw null;
        } else if (!blockingQueue.contains(uh5)) {
            BlockingQueue<Uh5> blockingQueue2 = this.g;
            if (blockingQueue2 != null) {
                blockingQueue2.offer(uh5);
            } else {
                Wg6.n("mPermissionQueue");
                throw null;
            }
        }
    }

    @DexIgnore
    public final boolean L6() {
        boolean c = PermissionUtils.a.c(PortfolioApp.get.instance());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "forceOpenBackgroundLocationPermission() - isBackgroundLocationPermissionGranted = " + c);
        if (!c) {
            X6();
        }
        return !c;
    }

    @DexIgnore
    public final void M(Uh5... uh5Arr) {
        boolean M6;
        Wg6.c(uh5Arr, "permissionCodes");
        for (Uh5 uh5 : uh5Arr) {
            K6(uh5);
        }
        BlockingQueue<Uh5> blockingQueue = this.g;
        if (blockingQueue != null) {
            Uh5 peek = blockingQueue.peek();
            FLogger.INSTANCE.getLocal().d(i, "processPermissionPopups() - permissionErrorCode = " + peek);
            if (peek == null) {
                W6();
                return;
            }
            switch (Ns5.a[peek.ordinal()]) {
                case 1:
                    M6 = M6();
                    break;
                case 2:
                case 3:
                    M6 = N6();
                    break;
                case 4:
                case 5:
                    M6 = O6();
                    break;
                case 6:
                    M6 = L6();
                    break;
                default:
                    throw new Kc6();
            }
            if (!M6) {
                BlockingQueue<Uh5> blockingQueue2 = this.g;
                if (blockingQueue2 != null) {
                    blockingQueue2.remove(peek);
                    M(new Uh5[0]);
                    return;
                }
                Wg6.n("mPermissionQueue");
                throw null;
            }
            return;
        }
        Wg6.n("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public final boolean M6() {
        boolean e = PermissionUtils.a.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "forceOpenBluetoothPermission() - isBluetoothEnabled = " + e);
        if (!e) {
            Y6();
        }
        return !e;
    }

    @DexIgnore
    public final boolean N6() {
        boolean d = PermissionUtils.a.d(PortfolioApp.get.instance());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "forceOpenLocationPermission() - isLocationPermissionGranted = " + d);
        if (!d) {
            Z6();
        }
        return !d;
    }

    @DexIgnore
    public final boolean O6() {
        boolean g2 = PermissionUtils.a.g();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "forceOpenLocationService() - isLocationOpen = " + g2);
        if (!g2) {
            a7();
        }
        return !g2;
    }

    @DexIgnore
    public final void P6() {
        startActivity(new Intent("android.settings.BLUETOOTH_SETTINGS"));
    }

    @DexIgnore
    public final void Q6() {
        startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        if (Wg6.a(str, "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i2 == 2131362442) {
                V6();
                R6();
            } else if (i2 != 2131363373) {
                U6();
            } else {
                PermissionUtils.Ai ai = PermissionUtils.a;
                FragmentActivity requireActivity = requireActivity();
                Wg6.b(requireActivity, "requireActivity()");
                ai.r(requireActivity, 0);
            }
        } else if (Wg6.a(str, S37.c.f())) {
            if (i2 == 2131362442) {
                V6();
                R6();
            } else if (i2 != 2131363373) {
                U6();
            } else {
                PermissionUtils.Ai ai2 = PermissionUtils.a;
                FragmentActivity requireActivity2 = requireActivity();
                Wg6.b(requireActivity2, "requireActivity()");
                ai2.q(requireActivity2, 1);
            }
        } else if (Wg6.a(str, "REQUEST_OPEN_LOCATION_SERVICE")) {
            if (i2 != 2131362442) {
                T6();
                return;
            }
            V6();
            Q6();
        } else if (!Wg6.a(str, "BLUETOOTH_OFF")) {
        } else {
            if (i2 != 2131362442) {
                S6();
                return;
            }
            V6();
            P6();
        }
    }

    @DexIgnore
    public final void R6() {
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, PortfolioApp.get.instance().getPackageName(), null));
        startActivity(intent);
    }

    @DexIgnore
    public abstract void S6();

    @DexIgnore
    public abstract void T6();

    @DexIgnore
    public abstract void U6();

    @DexIgnore
    public abstract void V6();

    @DexIgnore
    public abstract void W6();

    @DexIgnore
    public final void X6() {
        BlockingQueue<Uh5> blockingQueue = this.g;
        if (blockingQueue != null) {
            Uh5 peek = blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "requestLocationPermission() - permissionErrorCode = " + peek);
            if (!isActive()) {
                return;
            }
            if (peek == null || peek != Uh5.BACKGROUND_LOCATION_PERMISSION_OFF) {
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                s37.k0(childFragmentManager);
                return;
            }
            S37 s372 = S37.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            Wg6.b(childFragmentManager2, "childFragmentManager");
            s372.i0(childFragmentManager2);
            return;
        }
        Wg6.n("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public final void Y6() {
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.k(childFragmentManager);
        }
    }

    @DexIgnore
    public final void Z6() {
        BlockingQueue<Uh5> blockingQueue = this.g;
        if (blockingQueue != null) {
            Uh5 peek = blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "requestLocationPermission() - permissionErrorCode = " + peek);
            if (!isActive()) {
                return;
            }
            if (peek == null || peek != Uh5.LOCATION_PERMISSION_FEATURE_OFF) {
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                s37.k0(childFragmentManager);
                return;
            }
            S37 s372 = S37.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            Wg6.b(childFragmentManager2, "childFragmentManager");
            s372.j0(childFragmentManager2);
            return;
        }
        Wg6.n("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public final void a7() {
        BlockingQueue<Uh5> blockingQueue = this.g;
        if (blockingQueue != null) {
            Uh5 peek = blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "requestLocationService() - permissionErrorCode = " + peek);
            if (!isActive()) {
                return;
            }
            if (peek == null || peek != Uh5.LOCATION_SERVICE_FEATURE_OFF) {
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                s37.T(childFragmentManager);
                return;
            }
            S37 s372 = S37.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            Wg6.b(childFragmentManager2, "childFragmentManager");
            s372.S(childFragmentManager2);
            return;
        }
        Wg6.n("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.g = new PriorityBlockingQueue(5, Ai.b);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.fossil.Rk0.Bi, com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        boolean z = true;
        Wg6.c(strArr, "permissions");
        Wg6.c(iArr, "grantResults");
        if (i2 != 0) {
            super.onRequestPermissionsResult(i2, strArr, iArr);
            return;
        }
        if (!(!(iArr.length == 0)) || iArr[0] != 0) {
            z = false;
        }
        if (z) {
            M(new Uh5[0]);
        } else {
            U6();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
