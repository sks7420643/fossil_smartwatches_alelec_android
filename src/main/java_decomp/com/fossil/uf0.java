package com.fossil;

import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uf0 {
    @DexIgnore
    public /* final */ ArrayList<Ro0> a; // = new ArrayList<>();
    @DexIgnore
    public long b; // = -1;
    @DexIgnore
    public Interpolator c;
    @DexIgnore
    public So0 d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ To0 f; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends To0 {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public int b; // = 0;

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.So0
        public void b(View view) {
            int i = this.b + 1;
            this.b = i;
            if (i == Uf0.this.a.size()) {
                So0 so0 = Uf0.this.d;
                if (so0 != null) {
                    so0.b(null);
                }
                d();
            }
        }

        @DexIgnore
        @Override // com.fossil.So0, com.fossil.To0
        public void c(View view) {
            if (!this.a) {
                this.a = true;
                So0 so0 = Uf0.this.d;
                if (so0 != null) {
                    so0.c(null);
                }
            }
        }

        @DexIgnore
        public void d() {
            this.b = 0;
            this.a = false;
            Uf0.this.b();
        }
    }

    @DexIgnore
    public void a() {
        if (this.e) {
            Iterator<Ro0> it = this.a.iterator();
            while (it.hasNext()) {
                it.next().b();
            }
            this.e = false;
        }
    }

    @DexIgnore
    public void b() {
        this.e = false;
    }

    @DexIgnore
    public Uf0 c(Ro0 ro0) {
        if (!this.e) {
            this.a.add(ro0);
        }
        return this;
    }

    @DexIgnore
    public Uf0 d(Ro0 ro0, Ro0 ro02) {
        this.a.add(ro0);
        ro02.h(ro0.c());
        this.a.add(ro02);
        return this;
    }

    @DexIgnore
    public Uf0 e(long j) {
        if (!this.e) {
            this.b = j;
        }
        return this;
    }

    @DexIgnore
    public Uf0 f(Interpolator interpolator) {
        if (!this.e) {
            this.c = interpolator;
        }
        return this;
    }

    @DexIgnore
    public Uf0 g(So0 so0) {
        if (!this.e) {
            this.d = so0;
        }
        return this;
    }

    @DexIgnore
    public void h() {
        if (!this.e) {
            Iterator<Ro0> it = this.a.iterator();
            while (it.hasNext()) {
                Ro0 next = it.next();
                long j = this.b;
                if (j >= 0) {
                    next.d(j);
                }
                Interpolator interpolator = this.c;
                if (interpolator != null) {
                    next.e(interpolator);
                }
                if (this.d != null) {
                    next.f(this.f);
                }
                next.j();
            }
            this.e = true;
        }
    }
}
