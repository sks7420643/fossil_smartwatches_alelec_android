package com.fossil;

import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vc1<DataType, ResourceType, Transcode> {
    @DexIgnore
    public /* final */ Class<DataType> a;
    @DexIgnore
    public /* final */ List<? extends Qb1<DataType, ResourceType>> b;
    @DexIgnore
    public /* final */ Th1<ResourceType, Transcode> c;
    @DexIgnore
    public /* final */ Mn0<List<Throwable>> d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public interface Ai<ResourceType> {
        @DexIgnore
        Id1<ResourceType> a(Id1<ResourceType> id1);
    }

    @DexIgnore
    public Vc1(Class<DataType> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<? extends Qb1<DataType, ResourceType>> list, Th1<ResourceType, Transcode> th1, Mn0<List<Throwable>> mn0) {
        this.a = cls;
        this.b = list;
        this.c = th1;
        this.d = mn0;
        this.e = "Failed DecodePath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    @DexIgnore
    public Id1<Transcode> a(Xb1<DataType> xb1, int i, int i2, Ob1 ob1, Ai<ResourceType> ai) throws Dd1 {
        return this.c.a(ai.a(b(xb1, i, i2, ob1)), ob1);
    }

    @DexIgnore
    public final Id1<ResourceType> b(Xb1<DataType> xb1, int i, int i2, Ob1 ob1) throws Dd1 {
        List<Throwable> b2 = this.d.b();
        Ik1.d(b2);
        List<Throwable> list = b2;
        try {
            return c(xb1, i, i2, ob1, list);
        } finally {
            this.d.a(list);
        }
    }

    @DexIgnore
    public final Id1<ResourceType> c(Xb1<DataType> xb1, int i, int i2, Ob1 ob1, List<Throwable> list) throws Dd1 {
        Id1<ResourceType> id1;
        int size = this.b.size();
        Id1<ResourceType> id12 = null;
        int i3 = 0;
        while (true) {
            if (i3 >= size) {
                id1 = id12;
                break;
            }
            Qb1 qb1 = (Qb1) this.b.get(i3);
            try {
                id1 = qb1.a(xb1.b(), ob1) ? qb1.b(xb1.b(), i, i2, ob1) : id12;
            } catch (IOException | OutOfMemoryError | RuntimeException e2) {
                if (Log.isLoggable("DecodePath", 2)) {
                    Log.v("DecodePath", "Failed to decode data for " + qb1, e2);
                }
                list.add(e2);
                id1 = id12;
            }
            if (id1 != null) {
                break;
            }
            i3++;
            id12 = id1;
        }
        if (id1 != null) {
            return id1;
        }
        throw new Dd1(this.e, new ArrayList(list));
    }

    @DexIgnore
    public String toString() {
        return "DecodePath{ dataClass=" + this.a + ", decoders=" + this.b + ", transcoder=" + this.c + '}';
    }
}
