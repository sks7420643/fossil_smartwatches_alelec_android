package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import com.fossil.E61;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z51 implements E61<Bitmap> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public Z51(Context context) {
        Wg6.c(context, "context");
        this.a = context;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ boolean a(Bitmap bitmap) {
        return e(bitmap);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ String b(Bitmap bitmap) {
        return f(bitmap);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.G51, java.lang.Object, com.fossil.F81, com.fossil.X51, com.mapped.Xe6] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ Object c(G51 g51, Bitmap bitmap, F81 f81, X51 x51, Xe6 xe6) {
        return d(g51, bitmap, f81, x51, xe6);
    }

    @DexIgnore
    public Object d(G51 g51, Bitmap bitmap, F81 f81, X51 x51, Xe6<? super D61> xe6) {
        Resources resources = this.a.getResources();
        Wg6.b(resources, "context.resources");
        return new C61(new BitmapDrawable(resources, bitmap), false, Q51.MEMORY);
    }

    @DexIgnore
    public boolean e(Bitmap bitmap) {
        Wg6.c(bitmap, "data");
        return E61.Ai.a(this, bitmap);
    }

    @DexIgnore
    public String f(Bitmap bitmap) {
        Wg6.c(bitmap, "data");
        return null;
    }
}
