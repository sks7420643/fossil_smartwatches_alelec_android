package com.fossil;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import com.fossil.A18;
import com.fossil.C71;
import com.fossil.P18;
import com.mapped.Gg6;
import com.mapped.Kc6;
import com.mapped.Wg6;
import java.io.Closeable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W81 {
    @DexIgnore
    public static /* final */ P18 a; // = new P18.Ai().e();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements A18.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ Yk7 b;

        @DexIgnore
        public Ai(Yk7 yk7) {
            this.b = yk7;
        }

        @DexIgnore
        @Override // com.fossil.A18.Ai
        public final A18 d(V18 v18) {
            return ((A18.Ai) this.b.getValue()).d(v18);
        }
    }

    @DexIgnore
    public static final void a(Closeable closeable) {
        Wg6.c(closeable, "$this$closeQuietly");
        try {
            closeable.close();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e2) {
        }
    }

    @DexIgnore
    public static final int b(Bitmap bitmap) {
        Wg6.c(bitmap, "$this$getAllocationByteCountCompat");
        if (!bitmap.isRecycled()) {
            try {
                return Build.VERSION.SDK_INT >= 19 ? bitmap.getAllocationByteCount() : bitmap.getRowBytes() * bitmap.getHeight();
            } catch (Exception e) {
                return Y81.a.a(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
            }
        } else {
            throw new IllegalStateException(("Cannot obtain size for recycled Bitmap: " + bitmap + " [" + bitmap.getWidth() + " x " + bitmap.getHeight() + "] + " + bitmap.getConfig()).toString());
        }
    }

    @DexIgnore
    public static final int c(Bitmap.Config config) {
        if (config == Bitmap.Config.ALPHA_8) {
            return 1;
        }
        if (config == Bitmap.Config.RGB_565 || config == Bitmap.Config.ARGB_4444) {
            return 2;
        }
        return (Build.VERSION.SDK_INT < 26 || config != Bitmap.Config.RGBA_F16) ? 4 : 8;
    }

    @DexIgnore
    public static final Drawable d(Resources resources, int i, Resources.Theme theme) {
        Wg6.c(resources, "$this$getDrawableCompat");
        Drawable a2 = Nl0.a(resources, i, theme);
        if (a2 != null) {
            return a2;
        }
        throw new IllegalStateException("Required value was null.".toString());
    }

    @DexIgnore
    public static final String e(Q51 q51) {
        Wg6.c(q51, "$this$emoji");
        int i = V81.a[q51.ordinal()];
        if (i == 1) {
            return "\ud83e\udde0 ";
        }
        if (i == 2) {
            return "\ud83d\udcbe";
        }
        if (i == 3) {
            return "\u2601\ufe0f ";
        }
        throw new Kc6();
    }

    @DexIgnore
    public static final String f(Uri uri) {
        Wg6.c(uri, "$this$firstPathSegment");
        List<String> pathSegments = uri.getPathSegments();
        Wg6.b(pathSegments, "pathSegments");
        return (String) Pm7.H(pathSegments);
    }

    @DexIgnore
    public static final String g(MimeTypeMap mimeTypeMap, String str) {
        Wg6.c(mimeTypeMap, "$this$getMimeTypeFromUrl");
        if (str == null || Vt7.l(str)) {
            return null;
        }
        return mimeTypeMap.getMimeTypeFromExtension(Wt7.k0(Wt7.l0(Wt7.s0(Wt7.s0(str, '#', null, 2, null), '?', null, 2, null), '/', null, 2, null), '.', ""));
    }

    @DexIgnore
    public static final int h(Configuration configuration) {
        Wg6.c(configuration, "$this$nightMode");
        return configuration.uiMode & 48;
    }

    @DexIgnore
    public static final K71 i(View view) {
        Wg6.c(view, "$this$requestManager");
        Object tag = view.getTag(F51.coil_request_manager);
        if (!(tag instanceof K71)) {
            tag = null;
        }
        K71 k71 = (K71) tag;
        if (k71 != null) {
            return k71;
        }
        K71 k712 = new K71();
        view.addOnAttachStateChangeListener(k712);
        view.setTag(F51.coil_request_manager, k712);
        return k712;
    }

    @DexIgnore
    public static final E81 j(ImageView imageView) {
        int i;
        Wg6.c(imageView, "$this$scale");
        ImageView.ScaleType scaleType = imageView.getScaleType();
        return (scaleType != null && ((i = V81.b[scaleType.ordinal()]) == 1 || i == 2 || i == 3 || i == 4)) ? E81.FIT : E81.FILL;
    }

    @DexIgnore
    public static final C71.Bi k(C71 c71, String str) {
        Wg6.c(c71, "$this$getValue");
        if (str != null) {
            return c71.b(str);
        }
        return null;
    }

    @DexIgnore
    public static final boolean l(X71 x71) {
        Wg6.c(x71, "$this$isDiskPreload");
        return (x71 instanceof T71) && x71.u() == null && !x71.n().getWriteEnabled();
    }

    @DexIgnore
    public static final boolean m(Bitmap.Config config) {
        Wg6.c(config, "$this$isHardware");
        return Build.VERSION.SDK_INT >= 26 && config == Bitmap.Config.HARDWARE;
    }

    @DexIgnore
    public static final boolean n() {
        return Wg6.a(Looper.myLooper(), Looper.getMainLooper());
    }

    @DexIgnore
    public static final boolean o(Drawable drawable) {
        Wg6.c(drawable, "$this$isVector");
        return (drawable instanceof A01) || (Build.VERSION.SDK_INT > 21 && (drawable instanceof VectorDrawable));
    }

    @DexIgnore
    public static final A18.Ai p(Gg6<? extends A18.Ai> gg6) {
        Wg6.c(gg6, "initializer");
        return new Ai(Zk7.a(gg6));
    }

    @DexIgnore
    public static final Bitmap.Config q(Bitmap.Config config) {
        return (config == null || m(config)) ? Bitmap.Config.ARGB_8888 : config;
    }

    @DexIgnore
    public static final W71 r(W71 w71) {
        return w71 != null ? w71 : W71.c;
    }

    @DexIgnore
    public static final P18 s(P18 p18) {
        return p18 != null ? p18 : a;
    }

    @DexIgnore
    public static final void t(C71 c71, String str, Drawable drawable, boolean z) {
        Bitmap bitmap = null;
        Wg6.c(c71, "$this$putValue");
        Wg6.c(drawable, "value");
        if (str != null) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) (!(drawable instanceof BitmapDrawable) ? null : drawable);
            if (bitmapDrawable != null) {
                bitmap = bitmapDrawable.getBitmap();
            }
            if (bitmap != null) {
                c71.c(str, bitmap, z);
            }
        }
    }
}
