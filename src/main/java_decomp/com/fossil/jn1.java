package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jn1 extends R60 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Kn1[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Jn1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Jn1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length >= 6) {
                ArrayList arrayList = new ArrayList();
                Ur7 l = Bs7.l(Em7.G(bArr), 6);
                int a2 = l.a();
                int b = l.b();
                int c = l.c();
                if (c < 0 ? a2 >= b : a2 <= b) {
                    while (true) {
                        arrayList.add(Kn1.CREATOR.a(Dm7.k(bArr, a2, a2 + 6)));
                        if (a2 == b) {
                            break;
                        }
                        a2 += c;
                    }
                }
                Object[] array = arrayList.toArray(new Kn1[0]);
                if (array != null) {
                    return new Jn1((Kn1[]) array);
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
            throw new IllegalArgumentException(E.b(E.e("Invalid data size: "), bArr.length, ", require at least: 6").toString());
        }

        @DexIgnore
        public Jn1 b(Parcel parcel) {
            return new Jn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Jn1 createFromParcel(Parcel parcel) {
            return new Jn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Jn1[] newArray(int i) {
            return new Jn1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Jn1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        Kn1[] kn1Arr = (Kn1[]) parcel.createTypedArray(Kn1.CREATOR);
        this.c = kn1Arr == null ? new Kn1[0] : kn1Arr;
    }

    @DexIgnore
    public Jn1(Kn1[] kn1Arr) {
        super(Zm1.AUTO_WORKOUT_DETECTION);
        this.c = kn1Arr;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] bArr = new byte[0];
        for (Kn1 kn1 : this.c) {
            bArr = Dm7.q(bArr, kn1.a());
        }
        return bArr;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public JSONArray c() {
        JSONArray jSONArray = new JSONArray();
        try {
            for (Kn1 kn1 : this.c) {
                jSONArray.put(kn1.toJSONObject());
            }
        } catch (JSONException e) {
            D90.i.i(e);
        }
        return jSONArray;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Jn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.c, ((Jn1) obj).c);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.autoworkoutdectection.AutoWorkoutDetectionConfig");
    }

    @DexIgnore
    public final Kn1[] getAutoWorkoutDetectionItems() {
        return this.c;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        return Arrays.hashCode(this.c);
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.c, i);
        }
    }
}
