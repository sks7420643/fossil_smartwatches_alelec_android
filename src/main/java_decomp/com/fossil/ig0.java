package com.fossil;

import android.content.Context;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ig0 {

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void b(Cg0 cg0, boolean z);

        @DexIgnore
        boolean c(Cg0 cg0);
    }

    @DexIgnore
    void b(Cg0 cg0, boolean z);

    @DexIgnore
    void c(boolean z);

    @DexIgnore
    boolean d();

    @DexIgnore
    boolean e(Cg0 cg0, Eg0 eg0);

    @DexIgnore
    boolean f(Cg0 cg0, Eg0 eg0);

    @DexIgnore
    void g(Ai ai);

    @DexIgnore
    int getId();

    @DexIgnore
    void h(Context context, Cg0 cg0);

    @DexIgnore
    void i(Parcelable parcelable);

    @DexIgnore
    boolean k(Ng0 ng0);

    @DexIgnore
    Parcelable l();
}
