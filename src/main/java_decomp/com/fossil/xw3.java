package com.fossil;

import android.util.Property;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xw3 extends Property<ViewGroup, Float> {
    @DexIgnore
    public static /* final */ Property<ViewGroup, Float> a; // = new Xw3("childrenAlpha");

    @DexIgnore
    public Xw3(String str) {
        super(Float.class, str);
    }

    @DexIgnore
    public Float a(ViewGroup viewGroup) {
        Float f = (Float) viewGroup.getTag(Nw3.mtrl_internal_children_alpha_tag);
        return f != null ? f : Float.valueOf(1.0f);
    }

    @DexIgnore
    public void b(ViewGroup viewGroup, Float f) {
        float floatValue = f.floatValue();
        viewGroup.setTag(Nw3.mtrl_internal_children_alpha_tag, Float.valueOf(floatValue));
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            viewGroup.getChildAt(i).setAlpha(floatValue);
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // android.util.Property
    public /* bridge */ /* synthetic */ Float get(ViewGroup viewGroup) {
        return a(viewGroup);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // android.util.Property
    public /* bridge */ /* synthetic */ void set(ViewGroup viewGroup, Float f) {
        b(viewGroup, f);
    }
}
