package com.fossil;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.fossil.Af1;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ff1<Data> implements Af1<Integer, Data> {
    @DexIgnore
    public /* final */ Af1<Uri, Data> a;
    @DexIgnore
    public /* final */ Resources b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Bf1<Integer, AssetFileDescriptor> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public Ai(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Integer, AssetFileDescriptor> b(Ef1 ef1) {
            return new Ff1(this.a, ef1.d(Uri.class, AssetFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Bf1<Integer, ParcelFileDescriptor> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public Bi(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Integer, ParcelFileDescriptor> b(Ef1 ef1) {
            return new Ff1(this.a, ef1.d(Uri.class, ParcelFileDescriptor.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci implements Bf1<Integer, InputStream> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public Ci(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Integer, InputStream> b(Ef1 ef1) {
            return new Ff1(this.a, ef1.d(Uri.class, InputStream.class));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di implements Bf1<Integer, Uri> {
        @DexIgnore
        public /* final */ Resources a;

        @DexIgnore
        public Di(Resources resources) {
            this.a = resources;
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Integer, Uri> b(Ef1 ef1) {
            return new Ff1(this.a, If1.c());
        }
    }

    @DexIgnore
    public Ff1(Resources resources, Af1<Uri, Data> af1) {
        this.b = resources;
        this.a = af1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(Integer num) {
        return e(num);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai b(Integer num, int i, int i2, Ob1 ob1) {
        return c(num, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<Data> c(Integer num, int i, int i2, Ob1 ob1) {
        Uri d = d(num);
        if (d == null) {
            return null;
        }
        return this.a.b(d, i, i2, ob1);
    }

    @DexIgnore
    public final Uri d(Integer num) {
        try {
            return Uri.parse("android.resource://" + this.b.getResourcePackageName(num.intValue()) + '/' + this.b.getResourceTypeName(num.intValue()) + '/' + this.b.getResourceEntryName(num.intValue()));
        } catch (Resources.NotFoundException e) {
            if (Log.isLoggable("ResourceLoader", 5)) {
                Log.w("ResourceLoader", "Received invalid resource id: " + num, e);
            }
            return null;
        }
    }

    @DexIgnore
    public boolean e(Integer num) {
        return true;
    }
}
