package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.fossil.R16;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A26 extends U16 {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public boolean e; // = true;
    @DexIgnore
    public R16 f;
    @DexIgnore
    public /* final */ V16 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<T> implements Ls0<R16.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ A26 a;

        @DexIgnore
        public Ai(A26 a26) {
            this.a = a26;
        }

        @DexIgnore
        public final void a(R16.Ai ai) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = A26.h;
            local.d(str, "NotificationSettingChanged value = " + ai);
            this.a.e = ai.b();
            this.a.g.e2(ai.a());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(R16.Ai ai) {
            a(ai);
        }
    }

    /*
    static {
        String simpleName = A26.class.getSimpleName();
        Wg6.b(simpleName, "NotificationSettingsType\u2026er::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public A26(V16 v16) {
        Wg6.c(v16, "mView");
        this.g = v16;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "start: isCall = " + this.e);
        R16 r16 = this.f;
        if (r16 != null) {
            MutableLiveData<R16.Ai> a2 = r16.a();
            V16 v16 = this.g;
            if (v16 != null) {
                a2.h((W16) v16, new Ai(this));
                this.g.t(s(this.e));
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment");
        }
        Wg6.n("mNotificationSettingViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(h, "stop");
        R16 r16 = this.f;
        if (r16 != null) {
            MutableLiveData<R16.Ai> a2 = r16.a();
            V16 v16 = this.g;
            if (v16 != null) {
                a2.n((W16) v16);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeFragment");
        }
        Wg6.n("mNotificationSettingViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.U16
    public void n(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "changeNotificationSettingsTypeTo: settingsType = " + i);
        R16.Ai ai = new R16.Ai(i, this.e);
        R16 r16 = this.f;
        if (r16 != null) {
            r16.a().l(ai);
        } else {
            Wg6.n("mNotificationSettingViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.U16
    public void o(R16 r16) {
        Wg6.c(r16, "viewModel");
        this.f = r16;
    }

    @DexIgnore
    public final String s(boolean z) {
        if (z) {
            String c = Um5.c(PortfolioApp.get.instance(), 2131886094);
            Wg6.b(c, "LanguageHelper.getString\u2026ngs_Text__AllowCallsFrom)");
            return c;
        }
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886095);
        Wg6.b(c2, "LanguageHelper.getString\u2026_Text__AllowMessagesFrom)");
        return c2;
    }

    @DexIgnore
    public void t() {
        this.g.M5(this);
    }
}
