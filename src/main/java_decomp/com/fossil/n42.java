package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class N42 {
    @DexIgnore
    public static int b; // = 31;
    @DexIgnore
    public int a; // = 1;

    @DexIgnore
    public N42 a(Object obj) {
        this.a = (obj == null ? 0 : obj.hashCode()) + (b * this.a);
        return this;
    }

    @DexIgnore
    public int b() {
        return this.a;
    }

    @DexIgnore
    public final N42 c(boolean z) {
        this.a = (b * this.a) + (z ? 1 : 0);
        return this;
    }
}
