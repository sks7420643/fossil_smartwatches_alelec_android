package com.fossil;

import androidx.work.ListenableWorker;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class H11 {
    @DexIgnore
    public UUID a;
    @DexIgnore
    public O31 b;
    @DexIgnore
    public Set<String> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai<B extends Ai<?, ?>, W extends H11> {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public UUID b; // = UUID.randomUUID();
        @DexIgnore
        public O31 c;
        @DexIgnore
        public Set<String> d; // = new HashSet();

        @DexIgnore
        public Ai(Class<? extends ListenableWorker> cls) {
            this.c = new O31(this.b.toString(), cls.getName());
            a(cls.getName());
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.H11$Ai<B extends com.fossil.H11$Ai<?, ?>, W extends com.fossil.H11> */
        /* JADX WARN: Multi-variable type inference failed */
        public final B a(String str) {
            this.d.add(str);
            d();
            return this;
        }

        @DexIgnore
        public final W b() {
            W c2 = c();
            this.b = UUID.randomUUID();
            O31 o31 = new O31(this.c);
            this.c = o31;
            o31.a = this.b.toString();
            return c2;
        }

        @DexIgnore
        public abstract W c();

        @DexIgnore
        public abstract B d();

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.H11$Ai<B extends com.fossil.H11$Ai<?, ?>, W extends com.fossil.H11> */
        /* JADX WARN: Multi-variable type inference failed */
        public final B e(P01 p01) {
            this.c.j = p01;
            d();
            return this;
        }
    }

    @DexIgnore
    public H11(UUID uuid, O31 o31, Set<String> set) {
        this.a = uuid;
        this.b = o31;
        this.c = set;
    }

    @DexIgnore
    public UUID a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return this.a.toString();
    }

    @DexIgnore
    public Set<String> c() {
        return this.c;
    }

    @DexIgnore
    public O31 d() {
        return this.b;
    }
}
