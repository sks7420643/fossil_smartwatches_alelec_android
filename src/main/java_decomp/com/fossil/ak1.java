package com.fossil;

import androidx.collection.SimpleArrayMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ak1<K, V> extends Zi0<K, V> {
    @DexIgnore
    public int j;

    @DexIgnore
    @Override // androidx.collection.SimpleArrayMap
    public void clear() {
        this.j = 0;
        super.clear();
    }

    @DexIgnore
    @Override // androidx.collection.SimpleArrayMap
    public int hashCode() {
        if (this.j == 0) {
            this.j = super.hashCode();
        }
        return this.j;
    }

    @DexIgnore
    @Override // androidx.collection.SimpleArrayMap
    public void k(SimpleArrayMap<? extends K, ? extends V> simpleArrayMap) {
        this.j = 0;
        super.k(simpleArrayMap);
    }

    @DexIgnore
    @Override // androidx.collection.SimpleArrayMap
    public V l(int i) {
        this.j = 0;
        return (V) super.l(i);
    }

    @DexIgnore
    @Override // androidx.collection.SimpleArrayMap
    public V m(int i, V v) {
        this.j = 0;
        return (V) super.m(i, v);
    }

    @DexIgnore
    @Override // java.util.Map, androidx.collection.SimpleArrayMap
    public V put(K k, V v) {
        this.j = 0;
        return (V) super.put(k, v);
    }
}
