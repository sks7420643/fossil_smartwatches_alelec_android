package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import com.fossil.Zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Et2 extends Zs2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle h;
    @DexIgnore
    public /* final */ /* synthetic */ Zs2 i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Et2(Zs2 zs2, String str, String str2, Bundle bundle) {
        super(zs2);
        this.i = zs2;
        this.f = str;
        this.g = str2;
        this.h = bundle;
    }

    @DexIgnore
    @Override // com.fossil.Zs2.Ai
    public final void a() throws RemoteException {
        this.i.h.clearConditionalUserProperty(this.f, this.g, this.h);
    }
}
