package com.fossil;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ah1 extends Zg1<Drawable> {
    @DexIgnore
    public Ah1(Drawable drawable) {
        super(drawable);
    }

    @DexIgnore
    public static Id1<Drawable> f(Drawable drawable) {
        if (drawable != null) {
            return new Ah1(drawable);
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public void b() {
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public int c() {
        return Math.max(1, this.b.getIntrinsicWidth() * this.b.getIntrinsicHeight() * 4);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v1. Raw type applied. Possible types: java.lang.Class<?>, java.lang.Class<android.graphics.drawable.Drawable> */
    @Override // com.fossil.Id1
    public Class<Drawable> d() {
        return this.b.getClass();
    }
}
