package com.fossil;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ja1 extends ByteArrayOutputStream {
    @DexIgnore
    public /* final */ Y91 b;

    @DexIgnore
    public Ja1(Y91 y91, int i) {
        this.b = y91;
        ((ByteArrayOutputStream) this).buf = y91.a(Math.max(i, 256));
    }

    @DexIgnore
    public final void a(int i) {
        int i2 = ((ByteArrayOutputStream) this).count;
        if (i2 + i > ((ByteArrayOutputStream) this).buf.length) {
            byte[] a2 = this.b.a((i2 + i) * 2);
            System.arraycopy(((ByteArrayOutputStream) this).buf, 0, a2, 0, ((ByteArrayOutputStream) this).count);
            this.b.b(((ByteArrayOutputStream) this).buf);
            ((ByteArrayOutputStream) this).buf = a2;
        }
    }

    @DexIgnore
    @Override // java.io.OutputStream, java.io.ByteArrayOutputStream, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.b.b(((ByteArrayOutputStream) this).buf);
        ((ByteArrayOutputStream) this).buf = null;
        super.close();
    }

    @DexIgnore
    @Override // java.lang.Object
    public void finalize() {
        this.b.b(((ByteArrayOutputStream) this).buf);
    }

    @DexIgnore
    @Override // java.io.OutputStream, java.io.ByteArrayOutputStream
    public void write(int i) {
        synchronized (this) {
            a(1);
            super.write(i);
        }
    }

    @DexIgnore
    @Override // java.io.OutputStream
    public void write(byte[] bArr, int i, int i2) {
        synchronized (this) {
            a(i2);
            super.write(bArr, i, i2);
        }
    }
}
