package com.fossil;

import com.portfolio.platform.manager.login.MFLoginWechatManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Aq4 implements Factory<MFLoginWechatManager> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Aq4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Aq4 a(Uo4 uo4) {
        return new Aq4(uo4);
    }

    @DexIgnore
    public static MFLoginWechatManager c(Uo4 uo4) {
        MFLoginWechatManager H = uo4.H();
        Lk7.c(H, "Cannot return null from a non-@Nullable @Provides method");
        return H;
    }

    @DexIgnore
    public MFLoginWechatManager b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
