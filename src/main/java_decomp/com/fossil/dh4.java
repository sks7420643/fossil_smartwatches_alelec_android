package com.fossil;

import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import org.joda.time.DateTimeFieldType;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dh4 {
    @DexIgnore
    public static /* final */ String[] c; // = {G78.ANY_MARKER, "FCM", "GCM", ""};
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public Dh4(J64 j64) {
        this.a = j64.g().getSharedPreferences("com.google.android.gms.appid", 0);
        this.b = b(j64);
    }

    @DexIgnore
    public static String b(J64 j64) {
        String d = j64.j().d();
        if (d != null) {
            return d;
        }
        String c2 = j64.j().c();
        if (!c2.startsWith("1:") && !c2.startsWith("2:")) {
            return c2;
        }
        String[] split = c2.split(":");
        if (split.length != 4) {
            return null;
        }
        String str = split[1];
        if (str.isEmpty()) {
            return null;
        }
        return str;
    }

    @DexIgnore
    public static String c(PublicKey publicKey) {
        try {
            byte[] digest = MessageDigest.getInstance("SHA1").digest(publicKey.getEncoded());
            digest[0] = (byte) ((byte) (((digest[0] & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY) + 112) & 255));
            return Base64.encodeToString(digest, 0, 8, 11);
        } catch (NoSuchAlgorithmException e) {
            Log.w("ContentValues", "Unexpected error, device missing required algorithms");
            return null;
        }
    }

    @DexIgnore
    public final String a(String str, String str2) {
        return "|T|" + str + "|" + str2;
    }

    @DexIgnore
    public final String d(String str) {
        try {
            return new JSONObject(str).getString("token");
        } catch (JSONException e) {
            return null;
        }
    }

    @DexIgnore
    public final PublicKey e(String str) {
        try {
            return KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decode(str, 8)));
        } catch (IllegalArgumentException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            Log.w("ContentValues", "Invalid key stored " + e);
            return null;
        }
    }

    @DexIgnore
    public String f() {
        String g;
        synchronized (this.a) {
            g = g();
            if (g == null) {
                g = h();
            }
        }
        return g;
    }

    @DexIgnore
    public final String g() {
        String string;
        synchronized (this.a) {
            string = this.a.getString("|S|id", null);
        }
        return string;
    }

    @DexIgnore
    public final String h() {
        String str = null;
        synchronized (this.a) {
            String string = this.a.getString("|S||P|", null);
            if (string != null) {
                PublicKey e = e(string);
                if (e != null) {
                    str = c(e);
                }
            }
        }
        return str;
    }

    @DexIgnore
    public String i() {
        String str = null;
        synchronized (this.a) {
            String[] strArr = c;
            int length = strArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                String string = this.a.getString(a(this.b, strArr[i]), null);
                if (string == null || string.isEmpty()) {
                    i++;
                } else {
                    str = string.startsWith("{") ? d(string) : string;
                }
            }
        }
        return str;
    }
}
