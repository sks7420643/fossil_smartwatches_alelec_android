package com.fossil;

import com.mapped.Wg6;
import com.mapped.Zf;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nq4 extends Zf.Di<GoalTrackingData> {
    @DexIgnore
    public boolean a(GoalTrackingData goalTrackingData, GoalTrackingData goalTrackingData2) {
        Wg6.c(goalTrackingData, "oldItem");
        Wg6.c(goalTrackingData2, "newItem");
        return Wg6.a(goalTrackingData, goalTrackingData2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Zf.Di
    public /* bridge */ /* synthetic */ boolean areContentsTheSame(GoalTrackingData goalTrackingData, GoalTrackingData goalTrackingData2) {
        return a(goalTrackingData, goalTrackingData2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Zf.Di
    public /* bridge */ /* synthetic */ boolean areItemsTheSame(GoalTrackingData goalTrackingData, GoalTrackingData goalTrackingData2) {
        return b(goalTrackingData, goalTrackingData2);
    }

    @DexIgnore
    public boolean b(GoalTrackingData goalTrackingData, GoalTrackingData goalTrackingData2) {
        Wg6.c(goalTrackingData, "oldItem");
        Wg6.c(goalTrackingData2, "newItem");
        return Wg6.a(goalTrackingData.getId(), goalTrackingData2.getId());
    }
}
