package com.fossil;

import com.google.gson.JsonElement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fj4 extends JsonElement {
    @DexIgnore
    public static /* final */ Fj4 a; // = new Fj4();

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || (obj instanceof Fj4);
    }

    @DexIgnore
    public int hashCode() {
        return Fj4.class.hashCode();
    }
}
