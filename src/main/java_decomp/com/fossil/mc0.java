package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.fossil.blesdk.model.uiframework.packages.theme.ThemeTemplateFactory$getThemeTemplate$4", f = "ThemeTemplateFactory.kt", l = {48}, m = "invokeSuspend")
public final class mc0 extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
    @DexIgnore
    public iv7 b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public int d;
    @DexIgnore
    public /* final */ /* synthetic */ ry1 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mc0(ry1 ry1, qn7 qn7) {
        super(2, qn7);
        this.e = ry1;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        mc0 mc0 = new mc0(this.e, qn7);
        mc0.b = (iv7) obj;
        throw null;
        //return mc0;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
        mc0 mc0 = new mc0(this.e, qn7);
        mc0.b = iv7;
        return mc0.invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d2 = yn7.d();
        int i = this.d;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.b;
            nc0 nc0 = nc0.f2498a;
            ry1 ry1 = this.e;
            this.c = iv7;
            this.d = 1;
            if (nc0.b(ry1, this) == d2) {
                return d2;
            }
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.c;
            el7.b(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return tl7.f3441a;
    }
}
