package com.fossil;

import com.fossil.M62;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N82 extends R82 {
    @DexIgnore
    public /* final */ ArrayList<M62.Fi> c;
    @DexIgnore
    public /* final */ /* synthetic */ H82 d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public N82(H82 h82, ArrayList<M62.Fi> arrayList) {
        super(h82, null);
        this.d = h82;
        this.c = arrayList;
    }

    @DexIgnore
    @Override // com.fossil.R82
    public final void a() {
        this.d.a.t.q = this.d.t();
        ArrayList<M62.Fi> arrayList = this.c;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            M62.Fi fi = arrayList.get(i);
            i++;
            fi.i(this.d.o, this.d.a.t.q);
        }
    }
}
