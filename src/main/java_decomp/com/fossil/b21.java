package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class B21 implements N11 {
    @DexIgnore
    public static /* final */ String c; // = X01.f("SystemAlarmScheduler");
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore
    public B21(Context context) {
        this.b = context.getApplicationContext();
    }

    @DexIgnore
    @Override // com.fossil.N11
    public void a(O31... o31Arr) {
        for (O31 o31 : o31Arr) {
            b(o31);
        }
    }

    @DexIgnore
    public final void b(O31 o31) {
        X01.c().a(c, String.format("Scheduling work with workSpecId %s", o31.a), new Throwable[0]);
        this.b.startService(X11.f(this.b, o31.a));
    }

    @DexIgnore
    @Override // com.fossil.N11
    public boolean c() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.N11
    public void e(String str) {
        this.b.startService(X11.g(this.b, str));
    }
}
