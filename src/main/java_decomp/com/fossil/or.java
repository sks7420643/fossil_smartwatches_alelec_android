package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Or extends Zj {
    @DexIgnore
    public /* final */ Pu1 T;

    @DexIgnore
    public Or(K5 k5, I60 i60, Pu1 pu1) {
        super(k5, i60, Yp.f0, true, pu1.a(), pu1.getData(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.T = pu1;
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro, com.fossil.Mj
    public JSONObject C() {
        return G80.k(super.C(), Jd0.n3, this.T.toJSONObject());
    }

    @DexIgnore
    public final Pu1 R() {
        return this.T;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        return this.T.getLocaleString();
    }
}
