package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kv2 extends Gw2 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Xw2<Tw2<Tv2>> b;

    @DexIgnore
    public Kv2(Context context, Xw2<Tw2<Tv2>> xw2) {
        if (context != null) {
            this.a = context;
            this.b = xw2;
            return;
        }
        throw new NullPointerException("Null context");
    }

    @DexIgnore
    @Override // com.fossil.Gw2
    public final Context a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Gw2
    public final Xw2<Tw2<Tv2>> b() {
        return this.b;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Gw2) {
            Gw2 gw2 = (Gw2) obj;
            if (this.a.equals(gw2.a())) {
                Xw2<Tw2<Tv2>> xw2 = this.b;
                if (xw2 == null) {
                    return gw2.b() == null;
                }
                if (xw2.equals(gw2.b())) {
                    return true;
                }
            }
        }
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = this.a.hashCode();
        Xw2<Tw2<Tv2>> xw2 = this.b;
        return (xw2 == null ? 0 : xw2.hashCode()) ^ ((hashCode ^ 1000003) * 1000003);
    }

    @DexIgnore
    public final String toString() {
        String valueOf = String.valueOf(this.a);
        String valueOf2 = String.valueOf(this.b);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 46 + String.valueOf(valueOf2).length());
        sb.append("FlagsContext{context=");
        sb.append(valueOf);
        sb.append(", hermeticFileOverrides=");
        sb.append(valueOf2);
        sb.append("}");
        return sb.toString();
    }
}
