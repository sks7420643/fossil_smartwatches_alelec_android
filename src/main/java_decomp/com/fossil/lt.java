package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Lt {
    c((byte) 1),
    d((byte) 2),
    e((byte) 3),
    f((byte) 4),
    g((byte) 5),
    h((byte) 6),
    i((byte) 7),
    j((byte) 8),
    k((byte) 9),
    l((byte) 11),
    m((byte) 12),
    n((byte) 13);
    
    @DexIgnore
    public static /* final */ Jt p; // = new Jt(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public Lt(byte b2) {
        this.b = (byte) b2;
    }
}
