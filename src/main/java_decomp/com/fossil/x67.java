package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.portfolio.platform.data.model.DebugFirmwareData;
import com.portfolio.platform.data.model.Firmware;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x67 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MutableLiveData<List<DebugFirmwareData>> f4045a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Firmware> b; // = new MutableLiveData<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.viewmodel.FirmwareDebugViewModel$loadFirmware$1", f = "FirmwareDebugViewModel.kt", l = {16}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ rp7 $block;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ x67 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(x67 x67, rp7 rp7, qn7 qn7) {
            super(2, qn7);
            this.this$0 = x67;
            this.$block = rp7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$block, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object invoke;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                rp7 rp7 = this.$block;
                this.L$0 = iv7;
                this.label = 1;
                invoke = rp7.invoke(this);
                if (invoke == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                invoke = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.a().l((List) invoke);
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public final MutableLiveData<List<DebugFirmwareData>> a() {
        return this.f4045a;
    }

    @DexIgnore
    public final MutableLiveData<Firmware> b() {
        return this.b;
    }

    @DexIgnore
    public final boolean c() {
        if (this.f4045a.e() != null) {
            List<DebugFirmwareData> e = this.f4045a.e();
            if (e != null) {
                pq7.b(e, "firmwares.value!!");
                if (!e.isEmpty()) {
                    return true;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
        return false;
    }

    @DexIgnore
    public final xw7 d(rp7<? super qn7<? super List<DebugFirmwareData>>, ? extends Object> rp7) {
        pq7.c(rp7, "block");
        return gu7.d(jv7.a(bw7.a()), null, null, new a(this, rp7, null), 3, null);
    }

    @DexIgnore
    public final void e() {
        MutableLiveData<List<DebugFirmwareData>> mutableLiveData = this.f4045a;
        mutableLiveData.l(mutableLiveData.e());
        MutableLiveData<Firmware> mutableLiveData2 = this.b;
        mutableLiveData2.l(mutableLiveData2.e());
    }

    @DexIgnore
    public final void f(Firmware firmware) {
        pq7.c(firmware, "firmware");
        this.b.l(firmware);
    }
}
