package com.fossil;

import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nt4 {
    @DexIgnore
    @Vu3("step")
    public Integer a;

    @DexIgnore
    public Nt4() {
        this(null, 1, null);
    }

    @DexIgnore
    public Nt4(Integer num) {
        this.a = num;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Nt4(Integer num, int i, Qg6 qg6) {
        this((i & 1) != 0 ? null : num);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof Nt4) && Wg6.a(this.a, ((Nt4) obj).a));
    }

    @DexIgnore
    public int hashCode() {
        Integer num = this.a;
        if (num != null) {
            return num.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "StepResult(step=" + this.a + ")";
    }
}
