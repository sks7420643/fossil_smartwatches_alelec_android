package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cn3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Fr3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Or3 c;
    @DexIgnore
    public /* final */ /* synthetic */ Qm3 d;

    @DexIgnore
    public Cn3(Qm3 qm3, Fr3 fr3, Or3 or3) {
        this.d = qm3;
        this.b = fr3;
        this.c = or3;
    }

    @DexIgnore
    public final void run() {
        this.d.b.d0();
        if (this.b.c() == null) {
            this.d.b.L(this.b, this.c);
        } else {
            this.d.b.u(this.b, this.c);
        }
    }
}
