package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pd2 extends Hl2 implements Nd2 {
    @DexIgnore
    public Pd2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.service.ICommonService");
    }

    @DexIgnore
    @Override // com.fossil.Nd2
    public final void N(Ld2 ld2) throws RemoteException {
        Parcel d = d();
        Il2.c(d, ld2);
        n(1, d);
    }
}
