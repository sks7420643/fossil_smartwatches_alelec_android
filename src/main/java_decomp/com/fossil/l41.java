package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class L41 implements K41 {
    @DexIgnore
    public /* final */ A41 a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public /* final */ Executor c; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Executor {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            L41.this.d(runnable);
        }
    }

    @DexIgnore
    public L41(Executor executor) {
        this.a = new A41(executor);
    }

    @DexIgnore
    @Override // com.fossil.K41
    public Executor a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.K41
    public void b(Runnable runnable) {
        this.a.execute(runnable);
    }

    @DexIgnore
    @Override // com.fossil.K41
    public A41 c() {
        return this.a;
    }

    @DexIgnore
    public void d(Runnable runnable) {
        this.b.post(runnable);
    }
}
