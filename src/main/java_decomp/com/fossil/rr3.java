package com.fossil;

import com.fossil.Cv2;
import com.fossil.Dv2;
import com.fossil.Uu2;
import com.fossil.Vu2;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rr3 {
    @DexIgnore
    public String a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public Cv2 c;
    @DexIgnore
    public BitSet d;
    @DexIgnore
    public BitSet e;
    @DexIgnore
    public Map<Integer, Long> f;
    @DexIgnore
    public Map<Integer, List<Long>> g;
    @DexIgnore
    public /* final */ /* synthetic */ Pr3 h;

    @DexIgnore
    public Rr3(Pr3 pr3, String str) {
        this.h = pr3;
        this.a = str;
        this.b = true;
        this.d = new BitSet();
        this.e = new BitSet();
        this.f = new Zi0();
        this.g = new Zi0();
    }

    @DexIgnore
    public Rr3(Pr3 pr3, String str, Cv2 cv2, BitSet bitSet, BitSet bitSet2, Map<Integer, Long> map, Map<Integer, Long> map2) {
        this.h = pr3;
        this.a = str;
        this.d = bitSet;
        this.e = bitSet2;
        this.f = map;
        this.g = new Zi0();
        if (map2 != null) {
            for (Integer num : map2.keySet()) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(map2.get(num));
                this.g.put(num, arrayList);
            }
        }
        this.b = false;
        this.c = cv2;
    }

    @DexIgnore
    public /* synthetic */ Rr3(Pr3 pr3, String str, Cv2 cv2, BitSet bitSet, BitSet bitSet2, Map map, Map map2, Sr3 sr3) {
        this(pr3, str, cv2, bitSet, bitSet2, map, map2);
    }

    @DexIgnore
    public /* synthetic */ Rr3(Pr3 pr3, String str, Sr3 sr3) {
        this(pr3, str);
    }

    @DexIgnore
    public final Uu2 a(int i) {
        ArrayList arrayList;
        List list;
        Uu2.Ai S = Uu2.S();
        S.x(i);
        S.B(this.b);
        Cv2 cv2 = this.c;
        if (cv2 != null) {
            S.z(cv2);
        }
        Cv2.Ai b0 = Cv2.b0();
        b0.E(Gr3.E(this.d));
        b0.z(Gr3.E(this.e));
        if (this.f == null) {
            arrayList = null;
        } else {
            ArrayList arrayList2 = new ArrayList(this.f.size());
            for (Integer num : this.f.keySet()) {
                int intValue = num.intValue();
                Vu2.Ai L = Vu2.L();
                L.x(intValue);
                L.y(this.f.get(Integer.valueOf(intValue)).longValue());
                arrayList2.add((Vu2) ((E13) L.h()));
            }
            arrayList = arrayList2;
        }
        b0.G(arrayList);
        if (this.g == null) {
            list = Collections.emptyList();
        } else {
            ArrayList arrayList3 = new ArrayList(this.g.size());
            for (Integer num2 : this.g.keySet()) {
                Dv2.Ai M = Dv2.M();
                M.x(num2.intValue());
                List<Long> list2 = this.g.get(num2);
                if (list2 != null) {
                    Collections.sort(list2);
                    M.y(list2);
                }
                arrayList3.add((Dv2) ((E13) M.h()));
            }
            list = arrayList3;
        }
        b0.H(list);
        S.y(b0);
        return (Uu2) ((E13) S.h());
    }

    @DexIgnore
    public final void c(Wr3 wr3) {
        int a2 = wr3.a();
        Boolean bool = wr3.c;
        if (bool != null) {
            this.e.set(a2, bool.booleanValue());
        }
        Boolean bool2 = wr3.d;
        if (bool2 != null) {
            this.d.set(a2, bool2.booleanValue());
        }
        if (wr3.e != null) {
            Long l = this.f.get(Integer.valueOf(a2));
            long longValue = wr3.e.longValue() / 1000;
            if (l == null || longValue > l.longValue()) {
                this.f.put(Integer.valueOf(a2), Long.valueOf(longValue));
            }
        }
        if (wr3.f != null) {
            List<Long> list = this.g.get(Integer.valueOf(a2));
            if (list == null) {
                list = new ArrayList<>();
                this.g.put(Integer.valueOf(a2), list);
            }
            if (wr3.i()) {
                list.clear();
            }
            if (E63.a() && this.h.m().y(this.a, Xg3.g0) && wr3.j()) {
                list.clear();
            }
            if (!E63.a() || !this.h.m().y(this.a, Xg3.g0)) {
                list.add(Long.valueOf(wr3.f.longValue() / 1000));
                return;
            }
            long longValue2 = wr3.f.longValue() / 1000;
            if (!list.contains(Long.valueOf(longValue2))) {
                list.add(Long.valueOf(longValue2));
            }
        }
    }
}
