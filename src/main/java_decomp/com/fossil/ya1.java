package com.fossil;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.StrictMode;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ya1 implements Closeable {
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ File d;
    @DexIgnore
    public /* final */ File e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public long g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public long i; // = 0;
    @DexIgnore
    public Writer j;
    @DexIgnore
    public /* final */ LinkedHashMap<String, Di> k; // = new LinkedHashMap<>(0, 0.75f, true);
    @DexIgnore
    public int l;
    @DexIgnore
    public long m; // = 0;
    @DexIgnore
    public /* final */ ThreadPoolExecutor s; // = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new Bi(null));
    @DexIgnore
    public /* final */ Callable<Void> t; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Callable<Void> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public Void a() throws Exception {
            synchronized (Ya1.this) {
                if (Ya1.this.j != null) {
                    Ya1.this.g0();
                    if (Ya1.this.M()) {
                        Ya1.this.V();
                        Ya1.this.l = 0;
                    }
                }
            }
            return null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Void call() throws Exception {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements ThreadFactory {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public /* synthetic */ Bi(Ai ai) {
            this();
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread thread;
            synchronized (this) {
                thread = new Thread(runnable, "glide-disk-lru-cache-thread");
                thread.setPriority(1);
            }
            return thread;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci {
        @DexIgnore
        public /* final */ Di a;
        @DexIgnore
        public /* final */ boolean[] b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public Ci(Di di) {
            this.a = di;
            this.b = di.e ? null : new boolean[Ya1.this.h];
        }

        @DexIgnore
        public /* synthetic */ Ci(Ya1 ya1, Di di, Ai ai) {
            this(di);
        }

        @DexIgnore
        public void a() throws IOException {
            Ya1.this.A(this, false);
        }

        @DexIgnore
        public void b() {
            if (!this.c) {
                try {
                    a();
                } catch (IOException e) {
                }
            }
        }

        @DexIgnore
        public void e() throws IOException {
            Ya1.this.A(this, true);
            this.c = true;
        }

        @DexIgnore
        public File f(int i) throws IOException {
            File k;
            synchronized (Ya1.this) {
                if (this.a.f == this) {
                    if (!this.a.e) {
                        this.b[i] = true;
                    }
                    k = this.a.k(i);
                    if (!Ya1.this.b.exists()) {
                        Ya1.this.b.mkdirs();
                    }
                } else {
                    throw new IllegalStateException();
                }
            }
            return k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ long[] b;
        @DexIgnore
        public File[] c;
        @DexIgnore
        public File[] d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public Ci f;
        @DexIgnore
        public long g;

        @DexIgnore
        public Di(String str) {
            this.a = str;
            this.b = new long[Ya1.this.h];
            this.c = new File[Ya1.this.h];
            this.d = new File[Ya1.this.h];
            StringBuilder sb = new StringBuilder(str);
            sb.append('.');
            int length = sb.length();
            for (int i = 0; i < Ya1.this.h; i++) {
                sb.append(i);
                this.c[i] = new File(Ya1.this.b, sb.toString());
                sb.append(".tmp");
                this.d[i] = new File(Ya1.this.b, sb.toString());
                sb.setLength(length);
            }
        }

        @DexIgnore
        public /* synthetic */ Di(Ya1 ya1, String str, Ai ai) {
            this(str);
        }

        @DexIgnore
        public File j(int i) {
            return this.c[i];
        }

        @DexIgnore
        public File k(int i) {
            return this.d[i];
        }

        @DexIgnore
        public String l() throws IOException {
            StringBuilder sb = new StringBuilder();
            long[] jArr = this.b;
            for (long j : jArr) {
                sb.append(' ');
                sb.append(j);
            }
            return sb.toString();
        }

        @DexIgnore
        public final IOException m(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        @DexIgnore
        public final void n(String[] strArr) throws IOException {
            if (strArr.length == Ya1.this.h) {
                for (int i = 0; i < strArr.length; i++) {
                    try {
                        this.b[i] = Long.parseLong(strArr[i]);
                    } catch (NumberFormatException e2) {
                        m(strArr);
                        throw null;
                    }
                }
                return;
            }
            m(strArr);
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ei {
        @DexIgnore
        public /* final */ File[] a;

        @DexIgnore
        public Ei(String str, long j, File[] fileArr, long[] jArr) {
            this.a = fileArr;
        }

        @DexIgnore
        public /* synthetic */ Ei(Ya1 ya1, String str, long j, File[] fileArr, long[] jArr, Ai ai) {
            this(str, j, fileArr, jArr);
        }

        @DexIgnore
        public File a(int i) {
            return this.a[i];
        }
    }

    @DexIgnore
    public Ya1(File file, int i2, int i3, long j2) {
        this.b = file;
        this.f = i2;
        this.c = new File(file, "journal");
        this.d = new File(file, "journal.tmp");
        this.e = new File(file, "journal.bkp");
        this.h = i3;
        this.g = j2;
    }

    @DexIgnore
    public static void C(File file) throws IOException {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    @DexIgnore
    @TargetApi(26)
    public static void G(Writer writer) throws IOException {
        if (Build.VERSION.SDK_INT < 26) {
            writer.flush();
            return;
        }
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitUnbufferedIo().build());
        try {
            writer.flush();
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    @DexIgnore
    public static Ya1 P(File file, int i2, int i3, long j2) throws IOException {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 > 0) {
            File file2 = new File(file, "journal.bkp");
            if (file2.exists()) {
                File file3 = new File(file, "journal");
                if (file3.exists()) {
                    file2.delete();
                } else {
                    b0(file2, file3, false);
                }
            }
            Ya1 ya1 = new Ya1(file, i2, i3, j2);
            if (ya1.c.exists()) {
                try {
                    ya1.S();
                    ya1.Q();
                    return ya1;
                } catch (IOException e2) {
                    PrintStream printStream = System.out;
                    printStream.println("DiskLruCache " + file + " is corrupt: " + e2.getMessage() + ", removing");
                    ya1.B();
                }
            }
            file.mkdirs();
            Ya1 ya12 = new Ya1(file, i2, i3, j2);
            ya12.V();
            return ya12;
        } else {
            throw new IllegalArgumentException("valueCount <= 0");
        }
    }

    @DexIgnore
    public static void b0(File file, File file2, boolean z) throws IOException {
        if (z) {
            C(file2);
        }
        if (!file.renameTo(file2)) {
            throw new IOException();
        }
    }

    @DexIgnore
    @TargetApi(26)
    public static void o(Writer writer) throws IOException {
        if (Build.VERSION.SDK_INT < 26) {
            writer.close();
            return;
        }
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitUnbufferedIo().build());
        try {
            writer.close();
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    @DexIgnore
    public final void A(Ci ci, boolean z) throws IOException {
        synchronized (this) {
            Di di = ci.a;
            if (di.f == ci) {
                if (z && !di.e) {
                    for (int i2 = 0; i2 < this.h; i2++) {
                        if (!ci.b[i2]) {
                            ci.a();
                            throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                        } else if (!di.k(i2).exists()) {
                            ci.a();
                            return;
                        }
                    }
                }
                for (int i3 = 0; i3 < this.h; i3++) {
                    File k2 = di.k(i3);
                    if (!z) {
                        C(k2);
                    } else if (k2.exists()) {
                        File j2 = di.j(i3);
                        k2.renameTo(j2);
                        long j3 = di.b[i3];
                        long length = j2.length();
                        di.b[i3] = length;
                        this.i = (this.i - j3) + length;
                    }
                }
                this.l++;
                di.f = null;
                if (di.e || z) {
                    di.e = true;
                    this.j.append((CharSequence) "CLEAN");
                    this.j.append(' ');
                    this.j.append((CharSequence) di.a);
                    this.j.append((CharSequence) di.l());
                    this.j.append('\n');
                    if (z) {
                        long j4 = this.m;
                        this.m = 1 + j4;
                        di.g = j4;
                    }
                } else {
                    this.k.remove(di.a);
                    this.j.append((CharSequence) "REMOVE");
                    this.j.append(' ');
                    this.j.append((CharSequence) di.a);
                    this.j.append('\n');
                }
                G(this.j);
                if (this.i > this.g || M()) {
                    this.s.submit(this.t);
                }
                return;
            }
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    public void B() throws IOException {
        close();
        Ab1.b(this.b);
    }

    @DexIgnore
    public Ci D(String str) throws IOException {
        return F(str, -1);
    }

    @DexIgnore
    public final Ci F(String str, long j2) throws IOException {
        Di di;
        synchronized (this) {
            m();
            Di di2 = this.k.get(str);
            if (j2 != -1 && (di2 == null || di2.g != j2)) {
                return null;
            }
            if (di2 == null) {
                Di di3 = new Di(this, str, null);
                this.k.put(str, di3);
                di = di3;
            } else if (di2.f != null) {
                return null;
            } else {
                di = di2;
            }
            Ci ci = new Ci(this, di, null);
            di.f = ci;
            this.j.append((CharSequence) "DIRTY");
            this.j.append(' ');
            this.j.append((CharSequence) str);
            this.j.append('\n');
            G(this.j);
            return ci;
        }
    }

    @DexIgnore
    public Ei L(String str) throws IOException {
        Ei ei = null;
        synchronized (this) {
            m();
            Di di = this.k.get(str);
            if (di != null) {
                if (di.e) {
                    File[] fileArr = di.c;
                    int length = fileArr.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            this.l++;
                            this.j.append((CharSequence) "READ");
                            this.j.append(' ');
                            this.j.append((CharSequence) str);
                            this.j.append('\n');
                            if (M()) {
                                this.s.submit(this.t);
                            }
                            ei = new Ei(this, str, di.g, di.c, di.b, null);
                        } else if (!fileArr[i2].exists()) {
                            break;
                        } else {
                            i2++;
                        }
                    }
                }
            }
        }
        return ei;
    }

    @DexIgnore
    public final boolean M() {
        int i2 = this.l;
        return i2 >= 2000 && i2 >= this.k.size();
    }

    @DexIgnore
    public final void Q() throws IOException {
        C(this.d);
        Iterator<Di> it = this.k.values().iterator();
        while (it.hasNext()) {
            Di next = it.next();
            if (next.f == null) {
                for (int i2 = 0; i2 < this.h; i2++) {
                    this.i += next.b[i2];
                }
            } else {
                next.f = null;
                for (int i3 = 0; i3 < this.h; i3++) {
                    C(next.j(i3));
                    C(next.k(i3));
                }
                it.remove();
            }
        }
    }

    @DexIgnore
    public final void S() throws IOException {
        Za1 za1 = new Za1(new FileInputStream(this.c), Ab1.a);
        try {
            String f2 = za1.f();
            String f3 = za1.f();
            String f4 = za1.f();
            String f5 = za1.f();
            String f6 = za1.f();
            if (!"libcore.io.DiskLruCache".equals(f2) || !"1".equals(f3) || !Integer.toString(this.f).equals(f4) || !Integer.toString(this.h).equals(f5) || !"".equals(f6)) {
                throw new IOException("unexpected journal header: [" + f2 + ", " + f3 + ", " + f5 + ", " + f6 + "]");
            }
            int i2 = 0;
            while (true) {
                try {
                    T(za1.f());
                    i2++;
                } catch (EOFException e2) {
                    this.l = i2 - this.k.size();
                    if (za1.c()) {
                        V();
                    } else {
                        this.j = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.c, true), Ab1.a));
                    }
                    return;
                }
            }
        } finally {
            Ab1.a(za1);
        }
    }

    @DexIgnore
    public final void T(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i2 = indexOf + 1;
            int indexOf2 = str.indexOf(32, i2);
            if (indexOf2 == -1) {
                String substring = str.substring(i2);
                if (indexOf != 6 || !str.startsWith("REMOVE")) {
                    str2 = substring;
                } else {
                    this.k.remove(substring);
                    return;
                }
            } else {
                str2 = str.substring(i2, indexOf2);
            }
            Di di = this.k.get(str2);
            if (di == null) {
                di = new Di(this, str2, null);
                this.k.put(str2, di);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(" ");
                di.e = true;
                di.f = null;
                di.n(split);
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                di.f = new Ci(this, di, null);
            } else if (indexOf2 != -1 || indexOf != 4 || !str.startsWith("READ")) {
                throw new IOException("unexpected journal line: " + str);
            }
        } else {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void V() throws IOException {
        synchronized (this) {
            if (this.j != null) {
                o(this.j);
            }
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.d), Ab1.a));
            try {
                bufferedWriter.write("libcore.io.DiskLruCache");
                bufferedWriter.write("\n");
                bufferedWriter.write("1");
                bufferedWriter.write("\n");
                bufferedWriter.write(Integer.toString(this.f));
                bufferedWriter.write("\n");
                bufferedWriter.write(Integer.toString(this.h));
                bufferedWriter.write("\n");
                bufferedWriter.write("\n");
                for (Di di : this.k.values()) {
                    if (di.f != null) {
                        bufferedWriter.write("DIRTY " + di.a + '\n');
                    } else {
                        bufferedWriter.write("CLEAN " + di.a + di.l() + '\n');
                    }
                }
                o(bufferedWriter);
                if (this.c.exists()) {
                    b0(this.c, this.e, true);
                }
                b0(this.d, this.c, false);
                this.e.delete();
                this.j = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.c, true), Ab1.a));
            } catch (Throwable th) {
                o(bufferedWriter);
                throw th;
            }
        }
    }

    @DexIgnore
    public boolean X(String str) throws IOException {
        synchronized (this) {
            m();
            Di di = this.k.get(str);
            if (di == null || di.f != null) {
                return false;
            }
            for (int i2 = 0; i2 < this.h; i2++) {
                File j2 = di.j(i2);
                if (!j2.exists() || j2.delete()) {
                    this.i -= di.b[i2];
                    di.b[i2] = 0;
                } else {
                    throw new IOException("failed to delete " + j2);
                }
            }
            this.l++;
            this.j.append((CharSequence) "REMOVE");
            this.j.append(' ');
            this.j.append((CharSequence) str);
            this.j.append('\n');
            this.k.remove(str);
            if (M()) {
                this.s.submit(this.t);
            }
            return true;
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        synchronized (this) {
            if (this.j != null) {
                Iterator it = new ArrayList(this.k.values()).iterator();
                while (it.hasNext()) {
                    Di di = (Di) it.next();
                    if (di.f != null) {
                        di.f.a();
                    }
                }
                g0();
                o(this.j);
                this.j = null;
            }
        }
    }

    @DexIgnore
    public final void g0() throws IOException {
        while (this.i > this.g) {
            X(this.k.entrySet().iterator().next().getKey());
        }
    }

    @DexIgnore
    public final void m() {
        if (this.j == null) {
            throw new IllegalStateException("cache is closed");
        }
    }
}
