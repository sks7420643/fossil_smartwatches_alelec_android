package com.fossil;

import androidx.lifecycle.LiveData;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cz4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Ls0<T> {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public T b;
        @DexIgnore
        public /* final */ /* synthetic */ Js0 c;

        @DexIgnore
        public Ai(Js0 js0) {
            this.c = js0;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public void onChanged(T t) {
            if (!this.a) {
                this.a = true;
                this.b = t;
                this.c.l(t);
            } else if ((t == null && this.b != null) || (!Wg6.a(t, this.b))) {
                this.b = t;
                this.c.l(t);
            }
        }
    }

    @DexIgnore
    public static final <T> LiveData<T> a(LiveData<T> liveData) {
        Wg6.c(liveData, "$this$distinct");
        Js0 js0 = new Js0();
        js0.p(liveData, new Ai(js0));
        return js0;
    }
}
