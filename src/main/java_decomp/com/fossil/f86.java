package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F86 implements Factory<CommuteTimeSettingsPresenter> {
    @DexIgnore
    public static CommuteTimeSettingsPresenter a(S76 s76, An4 an4, UserRepository userRepository) {
        return new CommuteTimeSettingsPresenter(s76, an4, userRepository);
    }
}
