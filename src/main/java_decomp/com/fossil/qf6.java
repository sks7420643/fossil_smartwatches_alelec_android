package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qf6 implements Factory<ActivityOverviewMonthPresenter> {
    @DexIgnore
    public static ActivityOverviewMonthPresenter a(Nf6 nf6, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        return new ActivityOverviewMonthPresenter(nf6, userRepository, summariesRepository, portfolioApp);
    }
}
