package com.fossil;

import com.mapped.Cf;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import java.util.ArrayList;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Zl6 extends Gq4<Yl6> {
    @DexIgnore
    void X0(GoalTrackingSummary goalTrackingSummary);

    @DexIgnore
    Object d();  // void declaration

    @DexIgnore
    void j(Date date, boolean z, boolean z2, boolean z3);

    @DexIgnore
    void n(Mv5 mv5, ArrayList<String> arrayList);

    @DexIgnore
    void n5(Cf<GoalTrackingData> cf);
}
