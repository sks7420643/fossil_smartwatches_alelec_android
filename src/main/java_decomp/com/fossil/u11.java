package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class U11 {
    @DexIgnore
    public static /* final */ String d; // = X01.f("DelayedWorkTracker");
    @DexIgnore
    public /* final */ V11 a;
    @DexIgnore
    public /* final */ D11 b;
    @DexIgnore
    public /* final */ Map<String, Runnable> c; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ O31 b;

        @DexIgnore
        public Ai(O31 o31) {
            this.b = o31;
        }

        @DexIgnore
        public void run() {
            X01.c().a(U11.d, String.format("Scheduling work %s", this.b.a), new Throwable[0]);
            U11.this.a.a(this.b);
        }
    }

    @DexIgnore
    public U11(V11 v11, D11 d11) {
        this.a = v11;
        this.b = d11;
    }

    @DexIgnore
    public void a(O31 o31) {
        Runnable remove = this.c.remove(o31.a);
        if (remove != null) {
            this.b.b(remove);
        }
        Ai ai = new Ai(o31);
        this.c.put(o31.a, ai);
        long currentTimeMillis = System.currentTimeMillis();
        this.b.a(o31.a() - currentTimeMillis, ai);
    }

    @DexIgnore
    public void b(String str) {
        Runnable remove = this.c.remove(str);
        if (remove != null) {
            this.b.b(remove);
        }
    }
}
