package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.F57;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iy4 {
    @DexIgnore
    public /* final */ F57.Bi a;
    @DexIgnore
    public /* final */ Uy4 b; // = Uy4.d.b();
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ Oy5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Sf5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Qq7 implements Hg6<View, Cd6> {
            @DexIgnore
            public /* final */ /* synthetic */ Uy4 $colorGenerator$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ F57.Bi $drawableBuilder$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ Xs4 $friend$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ Oy5 $listener$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xs4 xs4, F57.Bi bi, Uy4 uy4, Oy5 oy5) {
                super(1);
                this.this$0 = ai;
                this.$friend$inlined = xs4;
                this.$drawableBuilder$inlined = bi;
                this.$colorGenerator$inlined = uy4;
                this.$listener$inlined = oy5;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Hg6
            public /* bridge */ /* synthetic */ Cd6 invoke(View view) {
                invoke(view);
                return Cd6.a;
            }

            @DexIgnore
            public final void invoke(View view) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("SentViewHolder", "accept: " + this.this$0.getAdapterPosition());
                this.$listener$inlined.e4(this.$friend$inlined);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii extends Qq7 implements Hg6<View, Cd6> {
            @DexIgnore
            public /* final */ /* synthetic */ Uy4 $colorGenerator$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ F57.Bi $drawableBuilder$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ Xs4 $friend$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ Oy5 $listener$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ai ai, Xs4 xs4, F57.Bi bi, Uy4 uy4, Oy5 oy5) {
                super(1);
                this.this$0 = ai;
                this.$friend$inlined = xs4;
                this.$drawableBuilder$inlined = bi;
                this.$colorGenerator$inlined = uy4;
                this.$listener$inlined = oy5;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Hg6
            public /* bridge */ /* synthetic */ Cd6 invoke(View view) {
                invoke(view);
                return Cd6.a;
            }

            @DexIgnore
            public final void invoke(View view) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("SentViewHolder", "reject: " + this.this$0.getAdapterPosition());
                this.$listener$inlined.R3(this.$friend$inlined);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Sf5 sf5) {
            super(sf5.n());
            Wg6.c(sf5, "binding");
            this.a = sf5;
        }

        @DexIgnore
        public final void a(Xs4 xs4, Oy5 oy5, F57.Bi bi, Uy4 uy4) {
            Wg6.c(xs4, "friend");
            Wg6.c(oy5, "listener");
            Wg6.c(bi, "drawableBuilder");
            Wg6.c(uy4, "colorGenerator");
            Sf5 sf5 = this.a;
            String a2 = Hz4.a.a(xs4.b(), xs4.e(), xs4.i());
            Hr7 hr7 = Hr7.a;
            FlexibleTextView flexibleTextView = sf5.v;
            Wg6.b(flexibleTextView, "tvFriendRequested");
            String c = Um5.c(flexibleTextView.getContext(), 2131886296);
            Wg6.b(c, "LanguageHelper.getString\u2026rnameWantsToBeYourFriend)");
            String format = String.format(c, Arrays.copyOf(new Object[]{a2}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            FlexibleTextView flexibleTextView2 = sf5.v;
            Wg6.b(flexibleTextView2, "tvFriendRequested");
            flexibleTextView2.setText(Jl5.b(Jl5.b, a2, format, 0, 4, null));
            ImageView imageView = sf5.s;
            Wg6.b(imageView, "ivAvatar");
            Ty4.b(imageView, xs4.h(), a2, bi, uy4);
            FlexibleButton flexibleButton = sf5.q;
            Wg6.b(flexibleButton, "btnAccept");
            Fz4.a(flexibleButton, new Aii(this, xs4, bi, uy4, oy5));
            FlexibleButton flexibleButton2 = sf5.r;
            Wg6.b(flexibleButton2, "btnDecline");
            Fz4.a(flexibleButton2, new Bii(this, xs4, bi, uy4, oy5));
        }
    }

    @DexIgnore
    public Iy4(int i, Oy5 oy5) {
        Wg6.c(oy5, "listener");
        this.c = i;
        this.d = oy5;
        F57.Bi f = F57.a().f();
        Wg6.b(f, "TextDrawable.builder().round()");
        this.a = f;
    }

    @DexIgnore
    public final int a() {
        return this.c;
    }

    @DexIgnore
    public boolean b(List<? extends Object> list, int i) {
        Wg6.c(list, "items");
        Object obj = list.get(i);
        return (obj instanceof Xs4) && ((Xs4) obj).c() == 2;
    }

    @DexIgnore
    public void c(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        Ai ai = null;
        Wg6.c(list, "items");
        Wg6.c(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof Xs4)) {
            obj = null;
        }
        Xs4 xs4 = (Xs4) obj;
        if (viewHolder instanceof Ai) {
            ai = viewHolder;
        }
        Ai ai2 = ai;
        if (xs4 != null && ai2 != null) {
            ai2.a(xs4, this.d, this.a, this.b);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder d(ViewGroup viewGroup) {
        Wg6.c(viewGroup, "parent");
        Sf5 z = Sf5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemSentRequestFriendBin\u2026.context), parent, false)");
        return new Ai(z);
    }
}
