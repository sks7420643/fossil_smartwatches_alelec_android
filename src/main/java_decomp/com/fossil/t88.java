package com.fossil;

import java.lang.annotation.Annotation;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T88 implements S88 {
    @DexIgnore
    public static /* final */ S88 a; // = new T88();

    @DexIgnore
    public static Annotation[] a(Annotation[] annotationArr) {
        if (U88.m(annotationArr, S88.class)) {
            return annotationArr;
        }
        Annotation[] annotationArr2 = new Annotation[(annotationArr.length + 1)];
        annotationArr2[0] = a;
        System.arraycopy(annotationArr, 0, annotationArr2, 1, annotationArr.length);
        return annotationArr2;
    }

    @DexIgnore
    @Override // java.lang.annotation.Annotation
    public Class<? extends Annotation> annotationType() {
        return S88.class;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj instanceof S88;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "@" + S88.class.getName() + "()";
    }
}
