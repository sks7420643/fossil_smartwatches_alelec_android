package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import com.mapped.W6;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"RestrictedAPI"})
public final class Gf0 {
    @DexIgnore
    public static /* final */ ThreadLocal<TypedValue> a; // = new ThreadLocal<>();
    @DexIgnore
    public static /* final */ WeakHashMap<Context, SparseArray<Ai>> b; // = new WeakHashMap<>(0);
    @DexIgnore
    public static /* final */ Object c; // = new Object();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public /* final */ ColorStateList a;
        @DexIgnore
        public /* final */ Configuration b;

        @DexIgnore
        public Ai(ColorStateList colorStateList, Configuration configuration) {
            this.a = colorStateList;
            this.b = configuration;
        }
    }

    @DexIgnore
    public static void a(Context context, int i, ColorStateList colorStateList) {
        synchronized (c) {
            SparseArray<Ai> sparseArray = b.get(context);
            if (sparseArray == null) {
                sparseArray = new SparseArray<>();
                b.put(context, sparseArray);
            }
            sparseArray.append(i, new Ai(colorStateList, context.getResources().getConfiguration()));
        }
    }

    @DexIgnore
    public static ColorStateList b(Context context, int i) {
        Ai ai;
        synchronized (c) {
            SparseArray<Ai> sparseArray = b.get(context);
            if (!(sparseArray == null || sparseArray.size() <= 0 || (ai = sparseArray.get(i)) == null)) {
                if (ai.b.equals(context.getResources().getConfiguration())) {
                    return ai.a;
                }
                sparseArray.remove(i);
            }
            return null;
        }
    }

    @DexIgnore
    public static ColorStateList c(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            return context.getColorStateList(i);
        }
        ColorStateList b2 = b(context, i);
        if (b2 != null) {
            return b2;
        }
        ColorStateList f = f(context, i);
        if (f == null) {
            return W6.e(context, i);
        }
        a(context, i, f);
        return f;
    }

    @DexIgnore
    public static Drawable d(Context context, int i) {
        return Jh0.h().j(context, i);
    }

    @DexIgnore
    public static TypedValue e() {
        TypedValue typedValue = a.get();
        if (typedValue != null) {
            return typedValue;
        }
        TypedValue typedValue2 = new TypedValue();
        a.set(typedValue2);
        return typedValue2;
    }

    @DexIgnore
    public static ColorStateList f(Context context, int i) {
        if (g(context, i)) {
            return null;
        }
        Resources resources = context.getResources();
        try {
            return Il0.a(resources, resources.getXml(i), context.getTheme());
        } catch (Exception e) {
            Log.e("AppCompatResources", "Failed to inflate ColorStateList, leaving it to the framework", e);
            return null;
        }
    }

    @DexIgnore
    public static boolean g(Context context, int i) {
        Resources resources = context.getResources();
        TypedValue e = e();
        resources.getValue(i, e, true);
        int i2 = e.type;
        return i2 >= 28 && i2 <= 31;
    }
}
