package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Pc2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ue3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Ue3> CREATOR; // = new If3();
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ float c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public float a;
        @DexIgnore
        public float b;

        @DexIgnore
        public final Ai a(float f) {
            this.a = f;
            return this;
        }

        @DexIgnore
        public final Ue3 b() {
            return new Ue3(this.b, this.a);
        }

        @DexIgnore
        public final Ai c(float f) {
            this.b = f;
            return this;
        }
    }

    @DexIgnore
    public Ue3(float f, float f2) {
        boolean z = -90.0f <= f && f <= 90.0f;
        StringBuilder sb = new StringBuilder(62);
        sb.append("Tilt needs to be between -90 and 90 inclusive: ");
        sb.append(f);
        Rc2.b(z, sb.toString());
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES + f;
        this.c = (((double) f2) <= 0.0d ? (f2 % 360.0f) + 360.0f : f2) % 360.0f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Ue3)) {
            return false;
        }
        Ue3 ue3 = (Ue3) obj;
        return Float.floatToIntBits(this.b) == Float.floatToIntBits(ue3.b) && Float.floatToIntBits(this.c) == Float.floatToIntBits(ue3.c);
    }

    @DexIgnore
    public int hashCode() {
        return Pc2.b(Float.valueOf(this.b), Float.valueOf(this.c));
    }

    @DexIgnore
    public String toString() {
        Pc2.Ai c2 = Pc2.c(this);
        c2.a("tilt", Float.valueOf(this.b));
        c2.a("bearing", Float.valueOf(this.c));
        return c2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.j(parcel, 2, this.b);
        Bd2.j(parcel, 3, this.c);
        Bd2.b(parcel, a2);
    }
}
