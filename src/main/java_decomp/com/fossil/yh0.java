package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yh0 extends Resources {
    @DexIgnore
    public static boolean b;
    @DexIgnore
    public /* final */ WeakReference<Context> a;

    @DexIgnore
    public Yh0(Context context, Resources resources) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.a = new WeakReference<>(context);
    }

    @DexIgnore
    public static boolean a() {
        return b;
    }

    @DexIgnore
    public static boolean b() {
        return a() && Build.VERSION.SDK_INT <= 20;
    }

    @DexIgnore
    public final Drawable c(int i) {
        return super.getDrawable(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public Drawable getDrawable(int i) throws Resources.NotFoundException {
        Context context = this.a.get();
        return context != null ? Jh0.h().t(context, this, i) : super.getDrawable(i);
    }
}
