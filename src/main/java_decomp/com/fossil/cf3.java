package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cf3 implements Parcelable.Creator<Je3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Je3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        String str = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            if (Ad2.l(t) != 2) {
                Ad2.B(parcel, t);
            } else {
                str = Ad2.f(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Je3(str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Je3[] newArray(int i) {
        return new Je3[i];
    }
}
