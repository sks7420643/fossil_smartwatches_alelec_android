package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yg3 implements Parcelable.Creator<Vg3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Vg3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        long j = 0;
        String str = null;
        Ug3 ug3 = null;
        String str2 = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 2) {
                str2 = Ad2.f(parcel, t);
            } else if (l == 3) {
                ug3 = (Ug3) Ad2.e(parcel, t, Ug3.CREATOR);
            } else if (l == 4) {
                str = Ad2.f(parcel, t);
            } else if (l != 5) {
                Ad2.B(parcel, t);
            } else {
                j = Ad2.y(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Vg3(str2, ug3, str, j);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Vg3[] newArray(int i) {
        return new Vg3[i];
    }
}
