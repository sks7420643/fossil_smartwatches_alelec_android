package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.remote.SecureApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sp4 implements Factory<SecureApiService> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<An4> b;

    @DexIgnore
    public Sp4(Uo4 uo4, Provider<An4> provider) {
        this.a = uo4;
        this.b = provider;
    }

    @DexIgnore
    public static Sp4 a(Uo4 uo4, Provider<An4> provider) {
        return new Sp4(uo4, provider);
    }

    @DexIgnore
    public static SecureApiService c(Uo4 uo4, An4 an4) {
        SecureApiService z = uo4.z(an4);
        Lk7.c(z, "Cannot return null from a non-@Nullable @Provides method");
        return z;
    }

    @DexIgnore
    public SecureApiService b() {
        return c(this.a, this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
