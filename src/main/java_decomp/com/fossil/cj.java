package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Rc6;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cj extends Bi {
    @DexIgnore
    public Cj(K5 k5, I60 i60, Yp yp, HashMap<Hu1, Object> hashMap, String str) {
        super(k5, i60, yp, Ke.b.a(k5.x, Ob.f), true, hashMap, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, str, 64);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Cj(K5 k5, I60 i60, HashMap hashMap, String str, int i) {
        this(k5, i60, Yp.o, (i & 4) != 0 ? new HashMap() : hashMap, (i & 8) != 0 ? E.a("UUID.randomUUID().toString()") : str);
    }

    @DexIgnore
    public final byte[][] a0() {
        ArrayList<J0> arrayList = this.I;
        ArrayList arrayList2 = new ArrayList(Im7.m(arrayList, 10));
        Iterator<T> it = arrayList.iterator();
        while (it.hasNext()) {
            arrayList2.add(it.next().f);
        }
        Object[] array = arrayList2.toArray(new byte[0][]);
        if (array != null) {
            return (byte[][]) array;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.Bi, com.fossil.Lp
    public Object x() {
        return a0();
    }
}
