package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Dz4;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.RecommendationChallengeAdapter;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.customview.InterceptSwipe;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ux4 extends BaseFragment {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public G37<Ta5> h;
    @DexIgnore
    public RecommendationChallengeAdapter i;
    @DexIgnore
    public BCRecommendationViewModel j;
    @DexIgnore
    public TimerViewObserver k; // = new TimerViewObserver();
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Ux4.m;
        }

        @DexIgnore
        public final Ux4 b() {
            return new Ux4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements InterceptSwipe.j {
        @DexIgnore
        public /* final */ /* synthetic */ Ta5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Ux4 b;

        @DexIgnore
        public Bi(Ta5 ta5, Ux4 ux4) {
            this.a = ta5;
            this.b = ux4;
        }

        @DexIgnore
        @Override // com.portfolio.platform.uirenew.customview.InterceptSwipe.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.a.r;
            Wg6.b(flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            FlexibleTextView flexibleTextView2 = this.a.q;
            Wg6.b(flexibleTextView2, "ftvEmpty");
            flexibleTextView2.setVisibility(8);
            Ux4.O6(this.b).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Dz4.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Ux4 a;

        @DexIgnore
        public Ci(Ux4 ux4) {
            this.a = ux4;
        }

        @DexIgnore
        @Override // com.fossil.Dz4.Bi
        public void a(View view, int i) {
            Wg6.c(view, "view");
            Ux4.L6(this.a).g(i);
            Object h = Ux4.L6(this.a).h(i);
            if (!(h instanceof Mt4)) {
                h = null;
            }
            Mt4 mt4 = (Mt4) h;
            if (mt4 != null) {
                Ux4.O6(this.a).l(mt4, i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Ux4 a;

        @DexIgnore
        public Di(Ux4 ux4) {
            this.a = ux4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            Ta5 ta5 = (Ta5) Ux4.K6(this.a).a();
            if (ta5 != null) {
                InterceptSwipe interceptSwipe = ta5.u;
                Wg6.b(interceptSwipe, "swipeRefresh");
                Wg6.b(bool, "it");
                interceptSwipe.setRefreshing(bool.booleanValue());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Ux4 a;

        @DexIgnore
        public Ei(Ux4 ux4) {
            this.a = ux4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            Ux4 ux4 = this.a;
            Wg6.b(bool, "it");
            ux4.V6(bool.booleanValue());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<List<? extends Object>> {
        @DexIgnore
        public /* final */ /* synthetic */ Ux4 a;

        @DexIgnore
        public Fi(Ux4 ux4) {
            this.a = ux4;
        }

        @DexIgnore
        public final void a(List<? extends Object> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Ux4.s.a();
            local.e(a2, "recommendedLive: " + list);
            this.a.k.a();
            RecommendationChallengeAdapter L6 = Ux4.L6(this.a);
            Wg6.b(list, "it");
            L6.j(list);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends Object> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> implements Ls0<Lc6<? extends Mt4, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ Ux4 a;

        @DexIgnore
        public Gi(Ux4 ux4) {
            this.a = ux4;
        }

        @DexIgnore
        public final void a(Lc6<Mt4, Integer> lc6) {
            this.a.T6(lc6.getFirst().b(), lc6.getFirst().a(), lc6.getSecond().intValue());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Mt4, ? extends Integer> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> implements Ls0<Lc6<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ Ux4 a;

        @DexIgnore
        public Hi(Ux4 ux4) {
            this.a = ux4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, ? extends ServerError> lc6) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Ux4.s.a();
            local.e(a2, "errorLive: - pair: " + lc6);
            Ta5 ta5 = (Ta5) Ux4.K6(this.a).a();
            if (ta5 != null) {
                boolean booleanValue = lc6.getFirst().booleanValue();
                ServerError serverError = (ServerError) lc6.getSecond();
                if (booleanValue) {
                    FlexibleTextView flexibleTextView = ta5.r;
                    Wg6.b(flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView2 = ta5.r;
                Wg6.b(flexibleTextView2, "ftvError");
                String c = Um5.c(flexibleTextView2.getContext(), 2131886231);
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    Toast.makeText(activity, c, 1).show();
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends ServerError> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii<T> implements Ls0<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ Ux4 a;

        @DexIgnore
        public Ii(Ux4 ux4) {
            this.a = ux4;
        }

        @DexIgnore
        public final void a(Integer num) {
            if (Ux4.L6(this.a).getItemCount() == 2) {
                this.a.V6(true);
            }
            RecommendationChallengeAdapter L6 = Ux4.L6(this.a);
            Wg6.b(num, "it");
            L6.i(num.intValue());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Integer num) {
            a(num);
        }
    }

    /*
    static {
        String name = Ux4.class.getName();
        Wg6.b(name, "BCRecommendationSubTabFragment::class.java.name");
        m = name;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 K6(Ux4 ux4) {
        G37<Ta5> g37 = ux4.h;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ RecommendationChallengeAdapter L6(Ux4 ux4) {
        RecommendationChallengeAdapter recommendationChallengeAdapter = ux4.i;
        if (recommendationChallengeAdapter != null) {
            return recommendationChallengeAdapter;
        }
        Wg6.n("recommendedAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ BCRecommendationViewModel O6(Ux4 ux4) {
        BCRecommendationViewModel bCRecommendationViewModel = ux4.j;
        if (bCRecommendationViewModel != null) {
            return bCRecommendationViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void R6() {
        RecommendationChallengeAdapter recommendationChallengeAdapter = new RecommendationChallengeAdapter();
        this.i = recommendationChallengeAdapter;
        if (recommendationChallengeAdapter != null) {
            recommendationChallengeAdapter.k(this.k);
            G37<Ta5> g37 = this.h;
            if (g37 != null) {
                Ta5 a2 = g37.a();
                if (a2 != null) {
                    RecyclerView recyclerView = a2.s;
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                    RecommendationChallengeAdapter recommendationChallengeAdapter2 = this.i;
                    if (recommendationChallengeAdapter2 != null) {
                        recyclerView.setAdapter(recommendationChallengeAdapter2);
                        a2.u.setOnRefreshListener(new Bi(a2, this));
                        RecyclerView recyclerView2 = a2.s;
                        Context requireContext = requireContext();
                        Wg6.b(requireContext, "requireContext()");
                        recyclerView2.addOnItemTouchListener(new Dz4(requireContext, new Ci(this)));
                        return;
                    }
                    Wg6.n("recommendedAdapter");
                    throw null;
                }
                return;
            }
            Wg6.n("binding");
            throw null;
        }
        Wg6.n("recommendedAdapter");
        throw null;
    }

    @DexIgnore
    public final void S6() {
        BCRecommendationViewModel bCRecommendationViewModel = this.j;
        if (bCRecommendationViewModel != null) {
            bCRecommendationViewModel.e().h(getViewLifecycleOwner(), new Di(this));
            BCRecommendationViewModel bCRecommendationViewModel2 = this.j;
            if (bCRecommendationViewModel2 != null) {
                bCRecommendationViewModel2.c().h(getViewLifecycleOwner(), new Ei(this));
                BCRecommendationViewModel bCRecommendationViewModel3 = this.j;
                if (bCRecommendationViewModel3 != null) {
                    bCRecommendationViewModel3.f().h(getViewLifecycleOwner(), new Fi(this));
                    BCRecommendationViewModel bCRecommendationViewModel4 = this.j;
                    if (bCRecommendationViewModel4 != null) {
                        bCRecommendationViewModel4.g().h(getViewLifecycleOwner(), new Gi(this));
                        BCRecommendationViewModel bCRecommendationViewModel5 = this.j;
                        if (bCRecommendationViewModel5 != null) {
                            bCRecommendationViewModel5.d().h(getViewLifecycleOwner(), new Hi(this));
                            BCRecommendationViewModel bCRecommendationViewModel6 = this.j;
                            if (bCRecommendationViewModel6 != null) {
                                bCRecommendationViewModel6.h().h(getViewLifecycleOwner(), new Ii(this));
                            } else {
                                Wg6.n("viewModel");
                                throw null;
                            }
                        } else {
                            Wg6.n("viewModel");
                            throw null;
                        }
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void T6(Ps4 ps4, String str, int i2) {
        BCWaitingChallengeDetailActivity.a aVar = BCWaitingChallengeDetailActivity.B;
        Fragment requireParentFragment = requireParentFragment();
        Wg6.b(requireParentFragment, "requireParentFragment()");
        aVar.a(requireParentFragment, ps4, str, i2, false);
    }

    @DexIgnore
    public final void U6(int i2) {
        BCRecommendationViewModel bCRecommendationViewModel = this.j;
        if (bCRecommendationViewModel != null) {
            bCRecommendationViewModel.m(i2);
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void V6(boolean z) {
        G37<Ta5> g37 = this.h;
        if (g37 != null) {
            Ta5 a2 = g37.a();
            if (a2 != null) {
                InterceptSwipe interceptSwipe = a2.u;
                Wg6.b(interceptSwipe, "swipeRefresh");
                interceptSwipe.setRefreshing(false);
                if (z) {
                    FlexibleTextView flexibleTextView = a2.q;
                    Wg6.b(flexibleTextView, "ftvEmpty");
                    flexibleTextView.setVisibility(0);
                    RecyclerView recyclerView = a2.s;
                    Wg6.b(recyclerView, "rcvRecommended");
                    recyclerView.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView2 = a2.q;
                Wg6.b(flexibleTextView2, "ftvEmpty");
                flexibleTextView2.setVisibility(8);
                RecyclerView recyclerView2 = a2.s;
                Wg6.b(recyclerView2, "rcvRecommended");
                recyclerView2.setVisibility(0);
                return;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().b1().a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCRecommendationViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ionViewModel::class.java)");
            this.j = (BCRecommendationViewModel) a2;
            getLifecycle().c(this.k);
            getLifecycle().a(this.k);
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Ta5 ta5 = (Ta5) Aq0.f(layoutInflater, 2131558614, viewGroup, false, A6());
        this.h = new G37<>(this, ta5);
        Wg6.b(ta5, "bd");
        return ta5.n();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        getLifecycle().c(this.k);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        BCRecommendationViewModel bCRecommendationViewModel = this.j;
        if (bCRecommendationViewModel != null) {
            bCRecommendationViewModel.i();
            R6();
            S6();
            return;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
