package com.fossil;

import android.os.Binder;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Bt0<Params, Progress, Result> {
    @DexIgnore
    public static /* final */ ThreadFactory g; // = new Ai();
    @DexIgnore
    public static /* final */ BlockingQueue<Runnable> h; // = new LinkedBlockingQueue(10);
    @DexIgnore
    public static /* final */ Executor i; // = new ThreadPoolExecutor(5, 128, 1, TimeUnit.SECONDS, h, g);
    @DexIgnore
    public static Fi j;
    @DexIgnore
    public /* final */ Hi<Params, Result> b; // = new Bi();
    @DexIgnore
    public /* final */ FutureTask<Result> c; // = new Ci(this.b);
    @DexIgnore
    public volatile Gi d; // = Gi.PENDING;
    @DexIgnore
    public /* final */ AtomicBoolean e; // = new AtomicBoolean();
    @DexIgnore
    public /* final */ AtomicBoolean f; // = new AtomicBoolean();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements ThreadFactory {
        @DexIgnore
        public /* final */ AtomicInteger a; // = new AtomicInteger(1);

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "ModernAsyncTask #" + this.a.getAndIncrement());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Hi<Params, Result> {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Result call() throws Exception {
            Bt0.this.f.set(true);
            Result result = null;
            try {
                Process.setThreadPriority(10);
                result = (Result) Bt0.this.b(this.a);
                Binder.flushPendingCommands();
                Bt0.this.l(result);
                return result;
            } catch (Throwable th) {
                Bt0.this.l(result);
                throw th;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends FutureTask<Result> {
        @DexIgnore
        public Ci(Callable callable) {
            super(callable);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v4, resolved type: com.fossil.Bt0 */
        /* JADX WARN: Multi-variable type inference failed */
        public void done() {
            try {
                Bt0.this.m(get());
            } catch (InterruptedException e) {
                Log.w("AsyncTask", e);
            } catch (ExecutionException e2) {
                throw new RuntimeException("An error occurred while executing doInBackground()", e2.getCause());
            } catch (CancellationException e3) {
                Bt0.this.m(null);
            } catch (Throwable th) {
                throw new RuntimeException("An error occurred while executing doInBackground()", th);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Di {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;

        /*
        static {
            int[] iArr = new int[Gi.values().length];
            a = iArr;
            try {
                iArr[Gi.RUNNING.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Gi.FINISHED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei<Data> {
        @DexIgnore
        public /* final */ Bt0 a;
        @DexIgnore
        public /* final */ Data[] b;

        @DexIgnore
        public Ei(Bt0 bt0, Data... dataArr) {
            this.a = bt0;
            this.b = dataArr;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Fi extends Handler {
        @DexIgnore
        public Fi() {
            super(Looper.getMainLooper());
        }

        @DexIgnore
        public void handleMessage(Message message) {
            Ei ei = (Ei) message.obj;
            int i = message.what;
            if (i == 1) {
                ei.a.d(ei.b[0]);
            } else if (i == 2) {
                ei.a.k(ei.b);
            }
        }
    }

    @DexIgnore
    public enum Gi {
        PENDING,
        RUNNING,
        FINISHED
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Hi<Params, Result> implements Callable<Result> {
        @DexIgnore
        public Params[] a;
    }

    @DexIgnore
    public static Handler e() {
        Fi fi;
        synchronized (Bt0.class) {
            try {
                if (j == null) {
                    j = new Fi();
                }
                fi = j;
            } catch (Throwable th) {
                throw th;
            }
        }
        return fi;
    }

    @DexIgnore
    public final boolean a(boolean z) {
        this.e.set(true);
        return this.c.cancel(z);
    }

    @DexIgnore
    public abstract Result b(Params... paramsArr);

    @DexIgnore
    public final Bt0<Params, Progress, Result> c(Executor executor, Params... paramsArr) {
        if (this.d != Gi.PENDING) {
            int i2 = Di.a[this.d.ordinal()];
            if (i2 == 1) {
                throw new IllegalStateException("Cannot execute task: the task is already running.");
            } else if (i2 != 2) {
                throw new IllegalStateException("We should never reach this state");
            } else {
                throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        } else {
            this.d = Gi.RUNNING;
            j();
            this.b.a = paramsArr;
            executor.execute(this.c);
            return this;
        }
    }

    @DexIgnore
    public void d(Result result) {
        if (f()) {
            h(result);
        } else {
            i(result);
        }
        this.d = Gi.FINISHED;
    }

    @DexIgnore
    public final boolean f() {
        return this.e.get();
    }

    @DexIgnore
    public void g() {
    }

    @DexIgnore
    public void h(Result result) {
        g();
    }

    @DexIgnore
    public void i(Result result) {
    }

    @DexIgnore
    public void j() {
    }

    @DexIgnore
    public void k(Progress... progressArr) {
    }

    @DexIgnore
    public Result l(Result result) {
        e().obtainMessage(1, new Ei(this, result)).sendToTarget();
        return result;
    }

    @DexIgnore
    public void m(Result result) {
        if (!this.f.get()) {
            l(result);
        }
    }
}
