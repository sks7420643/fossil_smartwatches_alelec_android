package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vh2 extends zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<vh2> CREATOR; // = new pi2();
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore
    public vh2(String str, String str2, String str3, int i) {
        this(str, str2, str3, i, 0);
    }

    @DexIgnore
    public vh2(String str, String str2, String str3, int i, int i2) {
        rc2.k(str);
        this.b = str;
        rc2.k(str2);
        this.c = str2;
        if (str3 != null) {
            this.d = str3;
            this.e = i;
            this.f = i2;
            return;
        }
        throw new IllegalStateException("Device UID is null.");
    }

    @DexIgnore
    public final String A() {
        return this.d;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof vh2)) {
            return false;
        }
        vh2 vh2 = (vh2) obj;
        return pc2.a(this.b, vh2.b) && pc2.a(this.c, vh2.c) && pc2.a(this.d, vh2.d) && this.e == vh2.e && this.f == vh2.f;
    }

    @DexIgnore
    public final String f() {
        return this.c;
    }

    @DexIgnore
    public final String h() {
        return String.format("%s:%s:%s", this.b, this.c, this.d);
    }

    @DexIgnore
    public final int hashCode() {
        return pc2.b(this.b, this.c, this.d, Integer.valueOf(this.e));
    }

    @DexIgnore
    public final int k() {
        return this.e;
    }

    @DexIgnore
    public final String toString() {
        return String.format("Device{%s:%s:%s}", h(), Integer.valueOf(this.e), Integer.valueOf(this.f));
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = bd2.a(parcel);
        bd2.u(parcel, 1, c(), false);
        bd2.u(parcel, 2, f(), false);
        bd2.u(parcel, 4, A(), false);
        bd2.n(parcel, 5, k());
        bd2.n(parcel, 6, this.f);
        bd2.b(parcel, a2);
    }
}
