package com.fossil;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M01 extends RecyclerView.q {
    @DexIgnore
    public ViewPager2.i a;
    @DexIgnore
    public /* final */ ViewPager2 b;
    @DexIgnore
    public /* final */ RecyclerView c;
    @DexIgnore
    public /* final */ LinearLayoutManager d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Ai g; // = new Ai();
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public int a;
        @DexIgnore
        public float b;
        @DexIgnore
        public int c;

        @DexIgnore
        public void a() {
            this.a = -1;
            this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.c = 0;
        }
    }

    @DexIgnore
    public M01(ViewPager2 viewPager2) {
        this.b = viewPager2;
        RecyclerView recyclerView = viewPager2.k;
        this.c = recyclerView;
        this.d = (LinearLayoutManager) recyclerView.getLayoutManager();
        l();
    }

    @DexIgnore
    public final void a(int i2, float f2, int i3) {
        ViewPager2.i iVar = this.a;
        if (iVar != null) {
            iVar.b(i2, f2, i3);
        }
    }

    @DexIgnore
    public final void b(int i2) {
        ViewPager2.i iVar = this.a;
        if (iVar != null) {
            iVar.c(i2);
        }
    }

    @DexIgnore
    public final void c(int i2) {
        if ((this.e != 3 || this.f != 0) && this.f != i2) {
            this.f = i2;
            ViewPager2.i iVar = this.a;
            if (iVar != null) {
                iVar.a(i2);
            }
        }
    }

    @DexIgnore
    public final int d() {
        return this.d.a2();
    }

    @DexIgnore
    public double e() {
        o();
        Ai ai = this.g;
        return ((double) ai.b) + ((double) ai.a);
    }

    @DexIgnore
    public int f() {
        return this.f;
    }

    @DexIgnore
    public boolean g() {
        return this.m;
    }

    @DexIgnore
    public boolean h() {
        return this.f == 0;
    }

    @DexIgnore
    public final boolean i() {
        int i2 = this.e;
        return i2 == 1 || i2 == 4;
    }

    @DexIgnore
    public void j() {
        this.l = true;
    }

    @DexIgnore
    public void k(int i2, boolean z) {
        this.e = z ? 2 : 3;
        this.m = false;
        boolean z2 = this.i != i2;
        this.i = i2;
        c(2);
        if (z2) {
            b(i2);
        }
    }

    @DexIgnore
    public final void l() {
        this.e = 0;
        this.f = 0;
        this.g.a();
        this.h = -1;
        this.i = -1;
        this.j = false;
        this.k = false;
        this.m = false;
        this.l = false;
    }

    @DexIgnore
    public void m(ViewPager2.i iVar) {
        this.a = iVar;
    }

    @DexIgnore
    public final void n(boolean z) {
        this.m = z;
        this.e = z ? 4 : 1;
        int i2 = this.i;
        if (i2 != -1) {
            this.h = i2;
            this.i = -1;
        } else if (this.h == -1) {
            this.h = d();
        }
        c(1);
    }

    @DexIgnore
    public final void o() {
        int i2;
        int top;
        Ai ai = this.g;
        int a2 = this.d.a2();
        ai.a = a2;
        if (a2 == -1) {
            ai.a();
            return;
        }
        View D = this.d.D(a2);
        if (D == null) {
            ai.a();
            return;
        }
        int b0 = this.d.b0(D);
        int k0 = this.d.k0(D);
        int n0 = this.d.n0(D);
        int I = this.d.I(D);
        ViewGroup.LayoutParams layoutParams = D.getLayoutParams();
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
            b0 += marginLayoutParams.leftMargin;
            k0 += marginLayoutParams.rightMargin;
            n0 += marginLayoutParams.topMargin;
            i2 = marginLayoutParams.bottomMargin + I;
        } else {
            i2 = I;
        }
        int height = D.getHeight() + n0 + i2;
        int width = D.getWidth();
        if (this.d.q2() == 0) {
            top = (D.getLeft() - b0) - this.c.getPaddingLeft();
            if (this.b.d()) {
                top = -top;
            }
            height = width + b0 + k0;
        } else {
            top = (D.getTop() - n0) - this.c.getPaddingTop();
        }
        int i3 = -top;
        ai.c = i3;
        if (i3 >= 0) {
            ai.b = height == 0 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : ((float) i3) / ((float) height);
        } else if (new I01(this.d).d()) {
            throw new IllegalStateException("Page(s) contain a ViewGroup with a LayoutTransition (or animateLayoutChanges=\"true\"), which interferes with the scrolling animation. Make sure to call getLayoutTransition().setAnimateParentHierarchy(false) on all ViewGroups with a LayoutTransition before an animation is started.");
        } else {
            throw new IllegalStateException(String.format(Locale.US, "Page can only be offset by a positive amount, not by %d", Integer.valueOf(ai.c)));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.q
    public void onScrollStateChanged(RecyclerView recyclerView, int i2) {
        boolean z = true;
        if (!(this.e == 1 && this.f == 1) && i2 == 1) {
            n(false);
        } else if (!i() || i2 != 2) {
            if (i() && i2 == 0) {
                o();
                if (!this.k) {
                    int i3 = this.g.a;
                    if (i3 != -1) {
                        a(i3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
                    }
                } else {
                    Ai ai = this.g;
                    if (ai.c == 0) {
                        int i4 = this.h;
                        int i5 = ai.a;
                        if (i4 != i5) {
                            b(i5);
                        }
                    } else {
                        z = false;
                    }
                }
                if (z) {
                    c(0);
                    l();
                }
            }
            if (this.e == 2 && i2 == 0 && this.l) {
                o();
                Ai ai2 = this.g;
                if (ai2.c == 0) {
                    int i6 = this.i;
                    int i7 = ai2.a;
                    if (i6 != i7) {
                        if (i7 == -1) {
                            i7 = 0;
                        }
                        b(i7);
                    }
                    c(0);
                    l();
                }
            }
        } else if (this.k) {
            c(2);
            this.j = true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001b, code lost:
        if ((r8 < 0) == r6.b.d()) goto L_0x001d;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0020  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0030  */
    @Override // androidx.recyclerview.widget.RecyclerView.q
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onScrolled(androidx.recyclerview.widget.RecyclerView r7, int r8, int r9) {
        /*
            r6 = this;
            r5 = -1
            r1 = 1
            r2 = 0
            r6.k = r1
            r6.o()
            boolean r0 = r6.j
            if (r0 == 0) goto L_0x0067
            r6.j = r2
            if (r9 > 0) goto L_0x001d
            if (r9 != 0) goto L_0x0060
            if (r8 >= 0) goto L_0x005e
            r0 = r1
        L_0x0015:
            androidx.viewpager2.widget.ViewPager2 r3 = r6.b
            boolean r3 = r3.d()
            if (r0 != r3) goto L_0x0060
        L_0x001d:
            r0 = r1
        L_0x001e:
            if (r0 == 0) goto L_0x0062
            com.fossil.M01$Ai r0 = r6.g
            int r3 = r0.c
            if (r3 == 0) goto L_0x0062
            int r0 = r0.a
            int r0 = r0 + 1
        L_0x002a:
            r6.i = r0
            int r3 = r6.h
            if (r3 == r0) goto L_0x0033
            r6.b(r0)
        L_0x0033:
            com.fossil.M01$Ai r0 = r6.g
            int r0 = r0.a
            if (r0 != r5) goto L_0x003a
            r0 = r2
        L_0x003a:
            com.fossil.M01$Ai r3 = r6.g
            float r4 = r3.b
            int r3 = r3.c
            r6.a(r0, r4, r3)
            com.fossil.M01$Ai r0 = r6.g
            int r0 = r0.a
            int r3 = r6.i
            if (r0 == r3) goto L_0x004d
            if (r3 != r5) goto L_0x005d
        L_0x004d:
            com.fossil.M01$Ai r0 = r6.g
            int r0 = r0.c
            if (r0 != 0) goto L_0x005d
            int r0 = r6.f
            if (r0 == r1) goto L_0x005d
            r6.c(r2)
            r6.l()
        L_0x005d:
            return
        L_0x005e:
            r0 = r2
            goto L_0x0015
        L_0x0060:
            r0 = r2
            goto L_0x001e
        L_0x0062:
            com.fossil.M01$Ai r0 = r6.g
            int r0 = r0.a
            goto L_0x002a
        L_0x0067:
            int r0 = r6.e
            if (r0 != 0) goto L_0x0033
            com.fossil.M01$Ai r0 = r6.g
            int r0 = r0.a
            if (r0 != r5) goto L_0x0072
            r0 = r2
        L_0x0072:
            r6.b(r0)
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.M01.onScrolled(androidx.recyclerview.widget.RecyclerView, int, int):void");
    }
}
