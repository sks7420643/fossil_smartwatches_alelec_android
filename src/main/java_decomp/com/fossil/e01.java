package com.fossil;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class E01 {
    @DexIgnore
    public /* final */ DataSetObservable a; // = new DataSetObservable();

    @DexIgnore
    @Deprecated
    public void a(View view, int i, Object obj) {
        throw new UnsupportedOperationException("Required method destroyItem was not overridden");
    }

    @DexIgnore
    public void b(ViewGroup viewGroup, int i, Object obj) {
        a(viewGroup, i, obj);
        throw null;
    }

    @DexIgnore
    @Deprecated
    public void c(View view) {
    }

    @DexIgnore
    public void d(ViewGroup viewGroup) {
        c(viewGroup);
    }

    @DexIgnore
    public abstract int e();

    @DexIgnore
    public int f(Object obj) {
        return -1;
    }

    @DexIgnore
    public CharSequence g(int i) {
        return null;
    }

    @DexIgnore
    public float h(int i) {
        return 1.0f;
    }

    @DexIgnore
    @Deprecated
    public Object i(View view, int i) {
        throw new UnsupportedOperationException("Required method instantiateItem was not overridden");
    }

    @DexIgnore
    public Object j(ViewGroup viewGroup, int i) {
        i(viewGroup, i);
        throw null;
    }

    @DexIgnore
    public abstract boolean k(View view, Object obj);

    @DexIgnore
    public void l(DataSetObserver dataSetObserver) {
        this.a.registerObserver(dataSetObserver);
    }

    @DexIgnore
    public void m(Parcelable parcelable, ClassLoader classLoader) {
    }

    @DexIgnore
    public Parcelable n() {
        return null;
    }

    @DexIgnore
    @Deprecated
    public void o(View view, int i, Object obj) {
    }

    @DexIgnore
    public void p(ViewGroup viewGroup, int i, Object obj) {
        o(viewGroup, i, obj);
    }

    @DexIgnore
    public void q(DataSetObserver dataSetObserver) {
        synchronized (this) {
        }
    }

    @DexIgnore
    @Deprecated
    public void r(View view) {
    }

    @DexIgnore
    public void s(ViewGroup viewGroup) {
        r(viewGroup);
    }

    @DexIgnore
    public void t(DataSetObserver dataSetObserver) {
        this.a.unregisterObserver(dataSetObserver);
    }
}
