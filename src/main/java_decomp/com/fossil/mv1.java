package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Wg6;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Mv1 extends Ox1 implements Parcelable, Serializable, Nx1 {
    @DexIgnore
    public String b;
    @DexIgnore
    public Jv1 c;
    @DexIgnore
    public Kv1 d;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Mv1(android.os.Parcel r5) {
        /*
            r4 = this;
            r3 = 0
            java.lang.String r2 = r5.readString()
            if (r2 == 0) goto L_0x003e
            java.lang.String r0 = "parcel.readString()!!"
            com.mapped.Wg6.b(r2, r0)
            java.lang.Class<com.fossil.Jv1> r0 = com.fossil.Jv1.class
            java.lang.ClassLoader r0 = r0.getClassLoader()
            android.os.Parcelable r0 = r5.readParcelable(r0)
            if (r0 == 0) goto L_0x003a
            java.lang.String r1 = "parcel.readParcelable<Sc\u2026class.java.classLoader)!!"
            com.mapped.Wg6.b(r0, r1)
            com.fossil.Jv1 r0 = (com.fossil.Jv1) r0
            java.lang.Class<com.fossil.Kv1> r1 = com.fossil.Kv1.class
            java.lang.ClassLoader r1 = r1.getClassLoader()
            android.os.Parcelable r1 = r5.readParcelable(r1)
            if (r1 == 0) goto L_0x0036
            java.lang.String r3 = "parcel.readParcelable<Sc\u2026class.java.classLoader)!!"
            com.mapped.Wg6.b(r1, r3)
            com.fossil.Kv1 r1 = (com.fossil.Kv1) r1
            r4.<init>(r2, r0, r1)
            return
        L_0x0036:
            com.mapped.Wg6.i()
            throw r3
        L_0x003a:
            com.mapped.Wg6.i()
            throw r3
        L_0x003e:
            com.mapped.Wg6.i()
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Mv1.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public Mv1(String str, Jv1 jv1, Kv1 kv1) {
        this.c = new Jv1(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.d = new Kv1(1.0f, 1.0f);
        this.b = str;
        this.c = jv1;
        this.d = kv1;
    }

    @DexIgnore
    public Mv1(JSONObject jSONObject, String str) throws IllegalArgumentException {
        this.c = new Jv1(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.d = new Kv1(1.0f, 1.0f);
        if (str == null || Vt7.l(str)) {
            str = jSONObject.optString("name", G80.e(0, 1));
            Wg6.b(str, "jsonObject.optString(UIS\u2026AME, generateShortUUID())");
        }
        this.b = str;
        try {
            JSONObject jSONObject2 = jSONObject.getJSONObject("size");
            Wg6.b(jSONObject2, "jsonObject.getJSONObject(UIScriptConstant.SIZE)");
            Lv1 lv1 = new Lv1(jSONObject2.getInt("w"), jSONObject2.getInt("h"));
            this.d = lv1.toScaledSize(240, 240);
            JSONObject jSONObject3 = jSONObject.getJSONObject("pos");
            Wg6.b(jSONObject3, "jsonObject.getJSONObject(UIScriptConstant.POS)");
            Iv1 iv1 = new Iv1(jSONObject3.getInt("x"), jSONObject3.getInt("y"));
            this.c = new Iv1(iv1.getX() - (lv1.getWidth() / 2), iv1.getY() - (lv1.getHeight() / 2)).toScaledPosition(240, 240);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    @DexIgnore
    public abstract Vb0 a();

    @DexIgnore
    public final Jv1 b() {
        return this.c;
    }

    @DexIgnore
    public final Kv1 c() {
        return this.d;
    }

    @DexIgnore
    @Override // java.lang.Object
    public abstract Mv1 clone();

    @DexIgnore
    @Override // java.lang.Object
    public abstract /* synthetic */ Nx1 clone();

    @DexIgnore
    public JSONObject d() {
        Iv1 actualPosition = this.c.toActualPosition(240, 240);
        Lv1 actualSize = this.d.toActualSize(240, 240);
        JSONObject put = new JSONObject().put("size", actualSize.a()).put("pos", new Iv1(actualPosition.getX() + (actualSize.getWidth() / 2), actualPosition.getY() + (actualSize.getHeight() / 2)).a()).put("type", Ey1.a(a())).put("name", this.b);
        Wg6.b(put, "JSONObject()\n           \u2026   name\n                )");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getName() {
        return this.b;
    }

    @DexIgnore
    public final float getScaledHeight() {
        return getScaledSize().getScaledHeight();
    }

    @DexIgnore
    public final Jv1 getScaledPosition() {
        return this.c;
    }

    @DexIgnore
    public final Kv1 getScaledSize() {
        return this.d;
    }

    @DexIgnore
    public final float getScaledWidth() {
        return getScaledSize().getScaledWidth();
    }

    @DexIgnore
    public final float getScaledX() {
        return getScaledPosition().getScaledX();
    }

    @DexIgnore
    public final float getScaledY() {
        return getScaledPosition().getScaledY();
    }

    @DexIgnore
    public Mv1 setScaledHeight(float f) {
        this.d.a(f);
        return this;
    }

    @DexIgnore
    public Mv1 setScaledPosition(Jv1 jv1) {
        this.c = jv1;
        return this;
    }

    @DexIgnore
    public Mv1 setScaledSize(Kv1 kv1) {
        this.d = kv1;
        return this;
    }

    @DexIgnore
    public Mv1 setScaledWidth(float f) {
        this.d.b(f);
        return this;
    }

    @DexIgnore
    public Mv1 setScaledX(float f) {
        this.c.a(f);
        return this;
    }

    @DexIgnore
    public Mv1 setScaledY(float f) {
        this.c.b(f);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return d();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }
}
