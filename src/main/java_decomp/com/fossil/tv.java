package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Tv extends Ps {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ N6 I;
    @DexIgnore
    public /* final */ N6 J;
    @DexIgnore
    public /* final */ short K;

    @DexIgnore
    public Tv(Sv sv, short s, Hs hs, K5 k5, int i) {
        super(hs, k5, i);
        this.K = (short) s;
        byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(sv.b).putShort(this.K).array();
        Wg6.b(array, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.G = array;
        byte[] array2 = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(sv.a()).putShort(this.K).array();
        Wg6.b(array2, "ByteBuffer.allocate(1 + \u2026dle)\n            .array()");
        this.H = array2;
        N6 n6 = N6.j;
        this.I = n6;
        this.J = n6;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final Mt E(byte b) {
        return Kt.k.a(b);
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final N6 K() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] M() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final N6 N() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] P() {
        return this.H;
    }

    @DexIgnore
    public void Q(byte[] bArr) {
        this.H = bArr;
    }

    @DexIgnore
    public final short R() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(super.z(), Jd0.A0, Hy1.l(this.K, null, 1, null));
    }
}
