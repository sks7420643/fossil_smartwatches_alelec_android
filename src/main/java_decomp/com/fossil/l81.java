package com.fossil;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L81 implements M81 {
    @DexIgnore
    public static /* final */ PorterDuffXfermode a; // = new PorterDuffXfermode(PorterDuff.Mode.SRC_IN);

    @DexIgnore
    @Override // com.fossil.M81
    public Object a(G51 g51, Bitmap bitmap, F81 f81, Xe6<? super Bitmap> xe6) {
        Paint paint = new Paint(3);
        int min = Math.min(bitmap.getWidth(), bitmap.getHeight());
        float f = ((float) min) / 2.0f;
        Bitmap.Config config = bitmap.getConfig();
        Wg6.b(config, "input.config");
        Bitmap c = g51.c(min, min, config);
        Canvas canvas = new Canvas(c);
        canvas.drawCircle(f, f, f, paint);
        paint.setXfermode(a);
        canvas.drawBitmap(bitmap, f - (((float) bitmap.getWidth()) / 2.0f), f - (((float) bitmap.getHeight()) / 2.0f), paint);
        g51.b(bitmap);
        return c;
    }

    @DexIgnore
    @Override // com.fossil.M81
    public String key() {
        String name = L81.class.getName();
        Wg6.b(name, "CircleCropTransformation::class.java.name");
        return name;
    }
}
