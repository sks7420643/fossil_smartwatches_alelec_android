package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O53 implements L53 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a;
    @DexIgnore
    public static /* final */ Xv2<Boolean> b;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        a = hw2.d("measurement.client.consent_state_v1.dev", false);
        b = hw2.d("measurement.service.consent_state_v1", false);
    }
    */

    @DexIgnore
    @Override // com.fossil.L53
    public final boolean zza() {
        return a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.L53
    public final boolean zzb() {
        return b.o().booleanValue();
    }
}
