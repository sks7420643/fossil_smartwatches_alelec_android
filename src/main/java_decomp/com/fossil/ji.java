package com.fossil;

import com.fossil.Ix1;
import com.fossil.Lx1;
import com.mapped.Wg6;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ji extends Lp {
    @DexIgnore
    public static /* final */ Lx1.Bi I; // = Lx1.Bi.CBC_NO_PADDING;
    @DexIgnore
    public static /* final */ byte[] J; // = new byte[16];
    @DexIgnore
    public static /* final */ Ff K; // = new Ff(null);
    @DexIgnore
    public byte[] C; // = new byte[8];
    @DexIgnore
    public byte[] D; // = new byte[8];
    @DexIgnore
    public byte[] E; // = new byte[0];
    @DexIgnore
    public /* final */ ArrayList<Ow> F; // = By1.a(this.i, Hm7.c(Ow.f));
    @DexIgnore
    public /* final */ Rt G;
    @DexIgnore
    public /* final */ byte[] H;

    @DexIgnore
    public Ji(K5 k5, I60 i60, Rt rt, byte[] bArr, String str) {
        super(k5, i60, Yp.S, str, false, 16);
        this.G = rt;
        this.H = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        new SecureRandom().nextBytes(this.C);
        byte[] bArr = this.H;
        if (bArr == null) {
            k(Zq.A);
        } else if (bArr.length < 16) {
            k(Zq.y);
        } else {
            byte[] copyOf = Arrays.copyOf(bArr, 16);
            Wg6.b(copyOf, "java.util.Arrays.copyOf(this, newSize)");
            this.E = copyOf;
            K5 k5 = this.w;
            Rt rt = this.G;
            Lp.i(this, new Ft(k5, rt, Rt.g.a(rt, this.C)), new Ih(this), new Vh(this), null, null, null, 56, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        JSONObject k = G80.k(super.C(), Jd0.u2, Ey1.a(this.G));
        Jd0 jd0 = Jd0.t2;
        byte[] bArr = this.H;
        return G80.k(k, jd0, bArr != null ? Long.valueOf(Ix1.a.b(bArr, Ix1.Ai.CRC32)) : JSONObject.NULL);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        return G80.k(G80.k(super.E(), Jd0.o2, Dy1.e(this.C, null, 1, null)), Jd0.p2, Dy1.e(this.D, null, 1, null));
    }

    @DexIgnore
    public final void H(byte[] bArr) {
        if (bArr.length != 16) {
            l(Nr.a(this.v, null, Zq.p, null, null, 13));
            return;
        }
        byte[] a2 = Rt.g.a(this.G, Lx1.a.a(I, this.E, J, bArr));
        if (a2.length != 16) {
            l(Nr.a(this.v, null, Zq.p, null, null, 13));
            return;
        }
        this.D = Dm7.k(a2, 0, 8);
        if (Arrays.equals(this.C, Dm7.k(a2, 8, 16))) {
            Lp.i(this, new Et(this.w, this.G, Rt.g.a(this.G, Lx1.a.b(I, this.E, J, Dm7.q(this.C, this.D)))), Tf.b, Hg.b, null, new Vg(this), null, 40, null);
            return;
        }
        l(Nr.a(this.v, null, Zq.z, null, null, 13));
    }

    @DexIgnore
    public final byte[] I() {
        return this.D;
    }

    @DexIgnore
    public final byte[] J() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.F;
    }
}
