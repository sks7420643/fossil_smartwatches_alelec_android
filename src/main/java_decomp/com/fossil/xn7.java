package com.fossil;

import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Jf6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xn7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Io7 {
        @DexIgnore
        public /* final */ /* synthetic */ Xe6 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ Hg6 $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Xe6 xe6, Xe6 xe62, Hg6 hg6) {
            super(xe62);
            this.$completion = xe6;
            this.$this_createCoroutineUnintercepted$inlined = hg6;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                El7.b(obj);
                Hg6 hg6 = this.$this_createCoroutineUnintercepted$inlined;
                if (hg6 != null) {
                    Ir7.d(hg6, 1);
                    return hg6.invoke(this);
                }
                throw new Rc6("null cannot be cast to non-null type (kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Jf6 {
        @DexIgnore
        public /* final */ /* synthetic */ Xe6 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ Af6 $context;
        @DexIgnore
        public /* final */ /* synthetic */ Hg6 $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Xe6 xe6, Af6 af6, Xe6 xe62, Af6 af62, Hg6 hg6) {
            super(xe62, af62);
            this.$completion = xe6;
            this.$context = af6;
            this.$this_createCoroutineUnintercepted$inlined = hg6;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                El7.b(obj);
                Hg6 hg6 = this.$this_createCoroutineUnintercepted$inlined;
                if (hg6 != null) {
                    Ir7.d(hg6, 1);
                    return hg6.invoke(this);
                }
                throw new Rc6("null cannot be cast to non-null type (kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Io7 {
        @DexIgnore
        public /* final */ /* synthetic */ Xe6 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ Object $receiver$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Coroutine $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Xe6 xe6, Xe6 xe62, Coroutine coroutine, Object obj) {
            super(xe62);
            this.$completion = xe6;
            this.$this_createCoroutineUnintercepted$inlined = coroutine;
            this.$receiver$inlined = obj;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                El7.b(obj);
                Coroutine coroutine = this.$this_createCoroutineUnintercepted$inlined;
                if (coroutine != null) {
                    Ir7.d(coroutine, 2);
                    return coroutine.invoke(this.$receiver$inlined, this);
                }
                throw new Rc6("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Jf6 {
        @DexIgnore
        public /* final */ /* synthetic */ Xe6 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ Af6 $context;
        @DexIgnore
        public /* final */ /* synthetic */ Object $receiver$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Coroutine $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(Xe6 xe6, Af6 af6, Xe6 xe62, Af6 af62, Coroutine coroutine, Object obj) {
            super(xe62, af62);
            this.$completion = xe6;
            this.$context = af6;
            this.$this_createCoroutineUnintercepted$inlined = coroutine;
            this.$receiver$inlined = obj;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                El7.b(obj);
                Coroutine coroutine = this.$this_createCoroutineUnintercepted$inlined;
                if (coroutine != null) {
                    Ir7.d(coroutine, 2);
                    return coroutine.invoke(this.$receiver$inlined, this);
                }
                throw new Rc6("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore
    public static final <T> Xe6<Cd6> a(Hg6<? super Xe6<? super T>, ? extends Object> hg6, Xe6<? super T> xe6) {
        Wg6.c(hg6, "$this$createCoroutineUnintercepted");
        Wg6.c(xe6, "completion");
        Go7.a(xe6);
        if (hg6 instanceof Zn7) {
            return ((Zn7) hg6).create(xe6);
        }
        Af6 context = xe6.getContext();
        if (context == Un7.INSTANCE) {
            if (xe6 != null) {
                return new Ai(xe6, xe6, hg6);
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        } else if (xe6 != null) {
            return new Bi(xe6, context, xe6, context, hg6);
        } else {
            throw new Rc6("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        }
    }

    @DexIgnore
    public static final <R, T> Xe6<Cd6> b(Coroutine<? super R, ? super Xe6<? super T>, ? extends Object> coroutine, R r, Xe6<? super T> xe6) {
        Wg6.c(coroutine, "$this$createCoroutineUnintercepted");
        Wg6.c(xe6, "completion");
        Go7.a(xe6);
        if (coroutine instanceof Zn7) {
            return ((Zn7) coroutine).create(r, xe6);
        }
        Af6 context = xe6.getContext();
        if (context == Un7.INSTANCE) {
            if (xe6 != null) {
                return new Ci(xe6, xe6, coroutine, r);
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        } else if (xe6 != null) {
            return new Di(xe6, context, xe6, context, coroutine, r);
        } else {
            throw new Rc6("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.mapped.Xe6<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> Xe6<T> c(Xe6<? super T> xe6) {
        Xe6<T> xe62;
        Wg6.c(xe6, "$this$intercepted");
        Jf6 jf6 = !(xe6 instanceof Jf6) ? null : xe6;
        return (jf6 == null || (xe62 = (Xe6<T>) jf6.intercepted()) == null) ? xe6 : xe62;
    }
}
