package com.fossil;

import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tz4 {
    @DexIgnore
    public final String a(DateTime dateTime) {
        String s0 = TimeUtils.s0(DateTimeZone.UTC, dateTime);
        Wg6.b(s0, "DateHelper.printServerDa\u2026t(DateTimeZone.UTC, date)");
        return Vt7.o(s0, "Z", "+0000", true);
    }

    @DexIgnore
    public final DateTime b(String str) {
        try {
            return TimeUtils.R(DateTimeZone.UTC, str);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("DateTimeUTCStringConverter", "toOffsetDateTime - e=" + e);
            return null;
        }
    }
}
