package com.fossil.wearables.fsl.goaltracking;

import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class GoalTracking {
    @DexIgnore
    public static /* final */ String COLUMN_CREATED_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String COLUMN_FREQUENCY; // = "frequency";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_NAME; // = "name";
    @DexIgnore
    public static /* final */ String COLUMN_PERIOD_TYPE; // = "periodType";
    @DexIgnore
    public static /* final */ String COLUMN_PERIOD_VALUE; // = "periodValue";
    @DexIgnore
    public static /* final */ String COLUMN_PIN_TYPE; // = "pinType";
    @DexIgnore
    public static /* final */ String COLUMN_SERVER_ID; // = "serverId";
    @DexIgnore
    public static /* final */ String COLUMN_STATUS; // = "status";
    @DexIgnore
    public static /* final */ String COLUMN_TARGET; // = "target";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATED_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ String COLUMN_URI; // = "uri";
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    public long createdAt;
    @DexIgnore
    @DatabaseField(columnName = "frequency")
    public int frequency;
    @DexIgnore
    @DatabaseField(columnName = "id", generatedId = true)
    public long id;
    @DexIgnore
    @DatabaseField(columnName = "name")
    public String name;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_PERIOD_TYPE)
    public int periodType;
    @DexIgnore
    @DatabaseField(columnName = COLUMN_PERIOD_VALUE)
    public int periodValue;
    @DexIgnore
    @DatabaseField(columnName = "pinType")
    public int pinType;
    @DexIgnore
    @DatabaseField(columnName = "serverId")
    public String serverId;
    @DexIgnore
    @DatabaseField(columnName = "status")
    public int status;
    @DexIgnore
    @DatabaseField(columnName = "target")
    public int target;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    public long updatedAt;
    @DexIgnore
    @DatabaseField(columnName = "uri")
    public String uri;

    @DexIgnore
    public GoalTracking() {
        this.pinType = 0;
    }

    @DexIgnore
    public GoalTracking(GoalTracking goalTracking) {
        this.name = goalTracking.name;
        this.frequency = goalTracking.frequency;
        this.target = goalTracking.target;
        this.periodType = goalTracking.periodType;
        this.periodValue = goalTracking.periodValue;
        this.status = goalTracking.status;
        this.serverId = goalTracking.serverId;
        this.createdAt = goalTracking.createdAt;
        this.updatedAt = goalTracking.updatedAt;
        this.uri = goalTracking.uri;
        this.id = goalTracking.id;
        this.pinType = goalTracking.getPinType();
    }

    @DexIgnore
    public GoalTracking(String str, Frequency frequency2, int i, PeriodType periodType2, int i2, GoalStatus goalStatus) {
        this.name = str.trim();
        this.frequency = frequency2.getValue();
        this.target = i;
        this.periodType = periodType2.getValue();
        this.periodValue = i2;
        this.status = goalStatus.getValue();
        this.uri = GoalTrackingURI.generateURI(this).toASCIIString();
        this.pinType = 0;
    }

    @DexIgnore
    public long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public Frequency getFrequency() {
        return Frequency.fromInt(this.frequency);
    }

    @DexIgnore
    public long getId() {
        return this.id;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public PeriodType getPeriodType() {
        return PeriodType.fromInt(this.periodType);
    }

    @DexIgnore
    public int getPeriodValue() {
        return this.periodValue;
    }

    @DexIgnore
    public int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public String getServerId() {
        return this.serverId;
    }

    @DexIgnore
    public GoalStatus getStatus() {
        return GoalStatus.fromInt(this.status);
    }

    @DexIgnore
    public int getTarget() {
        return this.target;
    }

    @DexIgnore
    public long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public String getUri() {
        return this.uri;
    }

    @DexIgnore
    public void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public void setFrequency(Frequency frequency2) {
        this.frequency = frequency2.getValue();
    }

    @DexIgnore
    public void setId(long j) {
        this.id = j;
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public void setPeriodType(PeriodType periodType2) {
        this.periodType = periodType2.getValue();
    }

    @DexIgnore
    public void setPeriodValue(int i) {
        this.periodValue = i;
    }

    @DexIgnore
    public void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public void setServerId(String str) {
        this.serverId = str;
    }

    @DexIgnore
    public void setStatus(GoalStatus goalStatus) {
        this.status = goalStatus.getValue();
    }

    @DexIgnore
    public void setTarget(int i) {
        this.target = i;
    }

    @DexIgnore
    public void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public void setUri(String str) {
        this.uri = str;
    }
}
