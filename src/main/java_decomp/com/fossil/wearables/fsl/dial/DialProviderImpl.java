package com.fossil.wearables.fsl.dial;

import android.content.Context;
import android.util.Log;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DialProviderImpl extends BaseDbProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "dial.db";
    @DexIgnore
    public static DialProviderImpl sDialProvider;
    @DexIgnore
    public /* final */ String TAG; // = DialProviderImpl.class.getSimpleName();

    @DexIgnore
    public DialProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    public static DialProviderImpl getInstance(Context context) {
        if (sDialProvider == null) {
            sDialProvider = new DialProviderImpl(context, DB_NAME);
        }
        return sDialProvider;
    }

    @DexIgnore
    public void create(SavedDial savedDial) {
        if (savedDial != null) {
            try {
                this.databaseHelper.getDao(SavedDial.class).create((Dao) savedDial);
                Dao dao = this.databaseHelper.getDao(ConfigItem.class);
                if (savedDial.getConfigItems() != null) {
                    Iterator<E> it = savedDial.getConfigItems().iterator();
                    while (it.hasNext()) {
                        dao.create((Dao) it.next());
                    }
                } else if (savedDial.initialConfigItems != null) {
                    Iterator<ConfigItem> it2 = savedDial.initialConfigItems.iterator();
                    while (it2.hasNext()) {
                        dao.create((Dao) it2.next());
                    }
                }
                String str = this.TAG;
                Log.d(str, "create " + savedDial.getDisplayName());
            } catch (SQLException e) {
                String str2 = this.TAG;
                Log.e(str2, "create '" + savedDial.getDisplayName() + "' failed.\n" + e.getLocalizedMessage());
            }
        }
    }

    @DexIgnore
    public void delete(int i) {
        try {
            Dao dao = this.databaseHelper.getDao(SavedDial.class);
            SavedDial savedDial = (SavedDial) dao.queryForId(Integer.valueOf(i));
            dao.delete((Dao) savedDial);
            String str = this.TAG;
            Log.d(str, "delete " + savedDial.getDisplayName());
        } catch (SQLException e) {
            String str2 = this.TAG;
            Log.e(str2, "delete '" + i + "' failed.\n" + e.getLocalizedMessage());
        }
    }

    @DexIgnore
    public List<SavedDial> getAllSavedDials() {
        List<SavedDial> list = null;
        try {
            list = this.databaseHelper.getDao(SavedDial.class).queryForAll();
            String str = this.TAG;
            Log.d(str, "getAllSavedDials() = " + list.size());
            return list;
        } catch (SQLException e) {
            String str2 = this.TAG;
            Log.e(str2, "getAllSavedDials failed.\n" + e.getLocalizedMessage());
            return list;
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{SavedDial.class, ConfigItem.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 3;
    }

    @DexIgnore
    public SavedDial getSavedDial(int i) {
        SavedDial savedDial;
        SQLException e;
        try {
            SavedDial savedDial2 = (SavedDial) this.databaseHelper.getDao(SavedDial.class).queryForId(Integer.valueOf(i));
            try {
                String str = this.TAG;
                Log.d(str, "getSavedDial(" + i + ") = " + savedDial2.getDisplayName());
                return savedDial2;
            } catch (SQLException e2) {
                e = e2;
                savedDial = savedDial2;
            }
        } catch (SQLException e3) {
            savedDial = null;
            e = e3;
            String str2 = this.TAG;
            Log.e(str2, "getSavedDial '" + i + "' failed.\n" + e.getLocalizedMessage());
            return savedDial;
        }
    }

    @DexIgnore
    public void update(SavedDial savedDial) {
        if (savedDial != null) {
            try {
                Dao dao = this.databaseHelper.getDao(SavedDial.class);
                SavedDial savedDial2 = (SavedDial) dao.queryForSameId(savedDial);
                if (savedDial2 != null) {
                    savedDial.setDbRowId(savedDial2.getDbRowId());
                }
                dao.update((Dao) savedDial);
                Dao dao2 = this.databaseHelper.getDao(ConfigItem.class);
                for (E e : savedDial.getConfigItems()) {
                    ConfigItem configItem = (ConfigItem) dao2.queryForSameId(e);
                    if (configItem != null) {
                        e.setDbRowId(configItem.getDbRowId());
                    }
                    dao2.update((Dao) e);
                }
                String str = this.TAG;
                Log.d(str, "updated " + savedDial.getDisplayName());
            } catch (SQLException e2) {
                String str2 = this.TAG;
                Log.e(str2, "update '" + savedDial.getDisplayName() + "' failed.\n" + e2.getLocalizedMessage());
            }
        }
    }

    @DexIgnore
    public void updateNoConfig(SavedDial savedDial) {
        if (savedDial != null) {
            try {
                Dao dao = this.databaseHelper.getDao(SavedDial.class);
                SavedDial savedDial2 = (SavedDial) dao.queryForSameId(savedDial);
                if (savedDial2 != null) {
                    savedDial.setDbRowId(savedDial2.getDbRowId());
                }
                dao.update((Dao) savedDial);
                String str = this.TAG;
                Log.d(str, "updated (no config) " + savedDial.getDisplayName());
            } catch (SQLException e) {
                String str2 = this.TAG;
                Log.e(str2, "update '" + savedDial.getDisplayName() + "' failed.\n" + e.getLocalizedMessage());
            }
        }
    }
}
