package com.fossil.wearables.fsl.dial;

import com.fossil.wearables.fsl.shared.BaseModel;
import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ConfigItem extends BaseModel {
    @DexIgnore
    public static /* final */ String KEY_24_HOUR; // = "24-hour";
    @DexIgnore
    public static /* final */ String KEY_DATE; // = "Date";
    @DexIgnore
    public static /* final */ String KEY_DAY_COLOR; // = "Day color";
    @DexIgnore
    public static /* final */ String KEY_DAY_DATE; // = "Day & Date";
    @DexIgnore
    public static /* final */ String KEY_DIAL_COLOR; // = "Dial color";
    @DexIgnore
    public static /* final */ String KEY_DIGITS_COLOR; // = "Digits color";
    @DexIgnore
    public static /* final */ String KEY_HAND_COLOR; // = "Hand color";
    @DexIgnore
    public static /* final */ String KEY_ICONS; // = "icon";
    @DexIgnore
    public static /* final */ String KEY_MOON_PHASE; // = "Moon phase";
    @DexIgnore
    public static /* final */ String KEY_STYLE_ID; // = "Style";
    @DexIgnore
    public static /* final */ String KEY_SUBEYES; // = "Subeye";
    @DexIgnore
    public static /* final */ String KEY_TEMPERATURE; // = "Temperature";
    @DexIgnore
    public static /* final */ String VALUE_KEY_ALARM; // = "Alarm";
    @DexIgnore
    public static /* final */ String VALUE_KEY_DAY; // = "Day";
    @DexIgnore
    public static /* final */ String VALUE_KEY_MISSED_CALLS; // = "Missed call";
    @DexIgnore
    public static /* final */ String VALUE_KEY_NEXT_MEETING; // = "Next meeting";
    @DexIgnore
    public static /* final */ String VALUE_KEY_PHONE_BATTERY; // = "Phone battery";
    @DexIgnore
    public static /* final */ String VALUE_KEY_SECOND_TIME_ZONE; // = "Timezone";
    @DexIgnore
    public static /* final */ String VALUE_KEY_STEP_COUNT; // = "Step count";
    @DexIgnore
    public static /* final */ String VALUE_KEY_UNREAD_TEXT; // = "Unread text";
    @DexIgnore
    public static /* final */ String VALUE_KEY_WATCH_BATTERY; // = "Watch battery";
    @DexIgnore
    public static /* final */ String VALUE_KEY_WEATHER; // = "Weather";
    @DexIgnore
    public static /* final */ String VALUE_OFF; // = "Off";
    @DexIgnore
    public static /* final */ String VALUE_ON; // = "On";
    @DexIgnore
    public Float floatValue;
    @DexIgnore
    @DatabaseField(canBeNull = false)
    public String mName;
    @DexIgnore
    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    public SavedDial mSavedDial;
    @DexIgnore
    @DatabaseField(canBeNull = false)
    public String value;

    @DexIgnore
    public ConfigItem() {
    }

    @DexIgnore
    public ConfigItem(SavedDial savedDial, String str, String str2) {
        this.mSavedDial = savedDial;
        this.mName = str;
        this.value = str2;
    }

    @DexIgnore
    public String getName() {
        return this.mName;
    }

    @DexIgnore
    public int getSubvalue(String str) {
        String[] split;
        String str2 = this.value;
        if (str2 == null || str2.length() == 0 || (split = this.value.split("/")) == null || split.length < 2 || !isType(str)) {
            return -1;
        }
        return Integer.valueOf(split[1]).intValue();
    }

    @DexIgnore
    public boolean isType(String str) {
        if (this.mName.equalsIgnoreCase(str)) {
            return true;
        }
        return (this.mName.contains(KEY_SUBEYES) || this.mName.contains("icon")) && this.value.contains(str);
    }

    @DexIgnore
    public void setSubvalue(String str, int i) {
        this.value = str + "/" + i;
    }

    @DexIgnore
    public String toString() {
        return "id=" + getDbRowId() + ", name=" + this.mName + ", value=" + this.value;
    }
}
