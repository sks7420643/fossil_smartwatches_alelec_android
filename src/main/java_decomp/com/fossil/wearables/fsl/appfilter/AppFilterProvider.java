package com.fossil.wearables.fsl.appfilter;

import com.fossil.wearables.fsl.BaseProvider;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface AppFilterProvider extends BaseProvider {
    @DexIgnore
    List<AppFilter> getAllAppFilterVibration(int i);

    @DexIgnore
    List<AppFilter> getAllAppFilters();

    @DexIgnore
    List<AppFilter> getAllAppFilters(int i);

    @DexIgnore
    List<AppFilter> getAllAppFiltersWithHour(int i, int i2);

    @DexIgnore
    AppFilter getAppFilterMatchingType(String str, int i);

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider
    /* synthetic */ String getDbPath();

    @DexIgnore
    Object removeAllAppFilters();  // void declaration

    @DexIgnore
    void removeAppFilter(AppFilter appFilter);

    @DexIgnore
    void saveAppFilter(AppFilter appFilter);
}
