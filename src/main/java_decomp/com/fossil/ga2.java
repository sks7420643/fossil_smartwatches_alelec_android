package com.fossil;

import android.os.IBinder;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.lang.ref.WeakReference;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ga2 implements IBinder.DeathRecipient, Ia2 {
    @DexIgnore
    public /* final */ WeakReference<BasePendingResult<?>> a;
    @DexIgnore
    public /* final */ WeakReference<Mb2> b;
    @DexIgnore
    public /* final */ WeakReference<IBinder> c;

    @DexIgnore
    public Ga2(BasePendingResult<?> basePendingResult, Mb2 mb2, IBinder iBinder) {
        this.b = new WeakReference<>(mb2);
        this.a = new WeakReference<>(basePendingResult);
        this.c = new WeakReference<>(iBinder);
    }

    @DexIgnore
    public /* synthetic */ Ga2(BasePendingResult basePendingResult, Mb2 mb2, IBinder iBinder, Ha2 ha2) {
        this(basePendingResult, null, iBinder);
    }

    @DexIgnore
    @Override // com.fossil.Ia2
    public final void a(BasePendingResult<?> basePendingResult) {
        b();
    }

    @DexIgnore
    public final void b() {
        BasePendingResult<?> basePendingResult = this.a.get();
        Mb2 mb2 = this.b.get();
        if (!(mb2 == null || basePendingResult == null)) {
            mb2.a(basePendingResult.r().intValue());
        }
        IBinder iBinder = this.c.get();
        if (iBinder != null) {
            try {
                iBinder.unlinkToDeath(this, 0);
            } catch (NoSuchElementException e) {
            }
        }
    }

    @DexIgnore
    public final void binderDied() {
        b();
    }
}
