package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum D94 {
    NONE,
    JAVA_ONLY,
    ALL;
    
    @DexIgnore
    public static /* final */ int REPORT_UPLOAD_VARIANT_DATATRANSPORT; // = 2;
    @DexIgnore
    public static /* final */ int REPORT_UPLOAD_VARIANT_LEGACY; // = 1;

    @DexIgnore
    public static D94 getState(Wc4 wc4) {
        boolean z = true;
        boolean z2 = wc4.g == 2;
        if (wc4.h != 2) {
            z = false;
        }
        return getState(z2, z);
    }

    @DexIgnore
    public static D94 getState(boolean z, boolean z2) {
        return !z ? NONE : !z2 ? JAVA_ONLY : ALL;
    }
}
