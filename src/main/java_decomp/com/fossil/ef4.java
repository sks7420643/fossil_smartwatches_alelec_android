package com.fossil;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.facebook.internal.Utility;
import com.fossil.Je4;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ef4 {
    @DexIgnore
    public /* final */ J64 a;
    @DexIgnore
    public /* final */ Rf4 b;
    @DexIgnore
    public /* final */ Bg4 c;
    @DexIgnore
    public /* final */ Ti4 d;
    @DexIgnore
    public /* final */ Je4 e;
    @DexIgnore
    public /* final */ Ug4 f;

    @DexIgnore
    public Ef4(J64 j64, Rf4 rf4, Bg4 bg4, Ti4 ti4, Je4 je4, Ug4 ug4) {
        this.a = j64;
        this.b = rf4;
        this.c = bg4;
        this.d = ti4;
        this.e = je4;
        this.f = ug4;
    }

    @DexIgnore
    public Ef4(J64 j64, Rf4 rf4, Ti4 ti4, Je4 je4, Ug4 ug4) {
        this(j64, rf4, new Bg4(j64.g(), rf4), ti4, je4, ug4);
    }

    @DexIgnore
    public static String a(byte[] bArr) {
        return Base64.encodeToString(bArr, 11);
    }

    @DexIgnore
    public static boolean f(String str) {
        return "SERVICE_NOT_AVAILABLE".equals(str) || "INTERNAL_SERVER_ERROR".equals(str) || "InternalServerError".equals(str);
    }

    @DexIgnore
    public final Nt3<String> b(Nt3<Bundle> nt3) {
        return nt3.i(Se4.a(), new Df4(this));
    }

    @DexIgnore
    public final String c() {
        try {
            return a(MessageDigest.getInstance(Utility.HASH_ALGORITHM_SHA1).digest(this.a.i().getBytes()));
        } catch (NoSuchAlgorithmException e2) {
            return "[HASH-ERROR]";
        }
    }

    @DexIgnore
    public Nt3<String> d(String str, String str2, String str3) {
        return b(i(str, str2, str3, new Bundle()));
    }

    @DexIgnore
    public final String e(Bundle bundle) throws IOException {
        if (bundle != null) {
            String string = bundle.getString("registration_id");
            if (string != null || (string = bundle.getString("unregistered")) != null) {
                return string;
            }
            String string2 = bundle.getString("error");
            if ("RST".equals(string2)) {
                throw new IOException("INSTANCE_ID_RESET");
            } else if (string2 != null) {
                throw new IOException(string2);
            } else {
                String valueOf = String.valueOf(bundle);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 21);
                sb.append("Unexpected response: ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString(), new Throwable());
                throw new IOException("SERVICE_NOT_AVAILABLE");
            }
        } else {
            throw new IOException("SERVICE_NOT_AVAILABLE");
        }
    }

    @DexIgnore
    public final /* synthetic */ String g(Nt3 nt3) throws Exception {
        return e((Bundle) nt3.n(IOException.class));
    }

    @DexIgnore
    public final Bundle h(String str, String str2, String str3, Bundle bundle) {
        bundle.putString("scope", str3);
        bundle.putString(RemoteFLogger.MESSAGE_SENDER_KEY, str2);
        bundle.putString("subtype", str2);
        bundle.putString("appid", str);
        bundle.putString("gmp_app_id", this.a.j().c());
        bundle.putString("gmsv", Integer.toString(this.b.d()));
        bundle.putString("osv", Integer.toString(Build.VERSION.SDK_INT));
        bundle.putString("app_ver", this.b.a());
        bundle.putString("app_ver_name", this.b.b());
        bundle.putString("firebase-app-name-hash", c());
        try {
            String b2 = ((Yg4) Qt3.a(this.f.a(false))).b();
            if (!TextUtils.isEmpty(b2)) {
                bundle.putString("Goog-Firebase-Installations-Auth", b2);
            } else {
                Log.w("FirebaseInstanceId", "FIS auth token is empty");
            }
        } catch (InterruptedException | ExecutionException e2) {
            Log.e("FirebaseInstanceId", "Failed to get FIS auth token", e2);
        }
        bundle.putString("cliv", "20.2.3".length() != 0 ? "fiid-".concat("20.2.3") : new String("fiid-"));
        Je4.Ai a2 = this.e.a("fire-iid");
        if (a2 != Je4.Ai.NONE) {
            bundle.putString("Firebase-Client-Log-Type", Integer.toString(a2.getCode()));
            bundle.putString("Firebase-Client", this.d.a());
        }
        return bundle;
    }

    @DexIgnore
    public final Nt3<Bundle> i(String str, String str2, String str3, Bundle bundle) {
        h(str, str2, str3, bundle);
        return this.c.l(bundle);
    }

    @DexIgnore
    public Nt3<?> j(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        String valueOf2 = String.valueOf(str3);
        return b(i(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle));
    }

    @DexIgnore
    public Nt3<?> k(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        bundle.putString("delete", "1");
        String valueOf2 = String.valueOf(str3);
        return b(i(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle));
    }
}
