package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.N04;
import com.fossil.X37;
import com.google.android.material.tabs.TabLayout;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.HomeActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tv5 extends BaseFragment implements Qv6 {
    @DexIgnore
    public static /* final */ Ai t; // = new Ai(null);
    @DexIgnore
    public Pv6 g;
    @DexIgnore
    public Ar4 h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public /* final */ String k; // = ThemeManager.l.a().d("disabledButton");
    @DexIgnore
    public /* final */ String l; // = ThemeManager.l.a().d("primaryColor");
    @DexIgnore
    public G37<H65> m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Tv5 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            Tv5 tv5 = new Tv5();
            tv5.setArguments(bundle);
            return tv5;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements N04.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Tv5 a;

        @DexIgnore
        public Bi(Tv5 tv5) {
            this.a = tv5;
        }

        @DexIgnore
        @Override // com.fossil.N04.Bi
        public final void a(TabLayout.g gVar, int i) {
            Wg6.c(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.k) && !TextUtils.isEmpty(this.a.l)) {
                int parseColor = Color.parseColor(this.a.k);
                int parseColor2 = Color.parseColor(this.a.l);
                gVar.o(2131230966);
                if (i == this.a.j) {
                    Drawable e = gVar.e();
                    if (e != null) {
                        e.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable e2 = gVar.e();
                if (e2 != null) {
                    e2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ H65 a;
        @DexIgnore
        public /* final */ /* synthetic */ Tv5 b;

        @DexIgnore
        public Ci(H65 h65, Tv5 tv5) {
            this.a = h65;
            this.b = tv5;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i, float f, int i2) {
            Drawable e;
            Drawable e2;
            super.b(i, f, i2);
            if (!TextUtils.isEmpty(this.b.l)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ExploreWatchFragment", "set icon color " + this.b.l);
                int parseColor = Color.parseColor(this.b.l);
                TabLayout.g v = this.a.s.v(i);
                if (!(v == null || (e2 = v.e()) == null)) {
                    e2.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.k) && this.b.j != i) {
                int parseColor2 = Color.parseColor(this.b.k);
                TabLayout.g v2 = this.a.s.v(this.b.j);
                if (!(v2 == null || (e = v2.e()) == null)) {
                    e.setTint(parseColor2);
                }
            }
            this.b.j = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Tv5 b;

        @DexIgnore
        public Di(Tv5 tv5) {
            this.b = tv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Tv5.M6(this.b).n();
        }
    }

    @DexIgnore
    public static final /* synthetic */ Pv6 M6(Tv5 tv5) {
        Pv6 pv6 = tv5.g;
        if (pv6 != null) {
            return pv6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "ExploreWatchFragment";
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Pv6 pv6) {
        P6(pv6);
    }

    @DexIgnore
    public void P6(Pv6 pv6) {
        Wg6.c(pv6, "presenter");
        this.g = pv6;
    }

    @DexIgnore
    @Override // com.fossil.Qv6
    public void f() {
        DashBar dashBar;
        G37<H65> g37 = this.m;
        if (g37 != null) {
            H65 a2 = g37.a();
            if (a2 != null && (dashBar = a2.t) != null) {
                X37.Ai ai = X37.a;
                Wg6.b(dashBar, "this");
                ai.a(dashBar, this.i, 500);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Qv6
    public void j3() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.B;
            Wg6.b(activity, "it");
            HomeActivity.a.b(aVar, activity, null, 2, null);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        G37<H65> g37 = new G37<>(this, (H65) Aq0.f(layoutInflater, 2131558554, viewGroup, false, A6()));
        this.m = g37;
        if (g37 != null) {
            H65 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Ar4 ar4 = this.h;
        if (ar4 != null) {
            ar4.i();
            Pv6 pv6 = this.g;
            if (pv6 != null) {
                pv6.m();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        } else {
            Wg6.n("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Pv6 pv6 = this.g;
        if (pv6 != null) {
            pv6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        this.h = new Ar4();
        G37<H65> g37 = this.m;
        if (g37 != null) {
            H65 a2 = g37.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.u;
                Wg6.b(viewPager2, "binding.vpExplore");
                Ar4 ar4 = this.h;
                if (ar4 != null) {
                    viewPager2.setAdapter(ar4);
                    if (a2.u.getChildAt(0) != null) {
                        View childAt = a2.u.getChildAt(0);
                        if (childAt != null) {
                            ((RecyclerView) childAt).setOverScrollMode(2);
                        } else {
                            throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                        }
                    }
                    TabLayout tabLayout = a2.s;
                    ViewPager2 viewPager22 = a2.u;
                    if (viewPager22 != null) {
                        new N04(tabLayout, viewPager22, new Bi(this)).a();
                        a2.u.g(new Ci(a2, this));
                        a2.q.setOnClickListener(new Di(this));
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.n("mAdapter");
                    throw null;
                }
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                this.i = z;
                Pv6 pv6 = this.g;
                if (pv6 != null) {
                    pv6.o(z);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qv6
    public void z5(List<? extends Explore> list) {
        Wg6.c(list, "data");
        Ar4 ar4 = this.h;
        if (ar4 != null) {
            ar4.l(list);
        } else {
            Wg6.n("mAdapter");
            throw null;
        }
    }
}
