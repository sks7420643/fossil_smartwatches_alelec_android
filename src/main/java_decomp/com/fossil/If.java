package com.fossil;

import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Xe6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.fossil.blesdk.device.logic.phase.SyncPhase$onAllFileRead$1", f = "SyncPhase.kt", l = {}, m = "invokeSuspend")
public final class If extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public Il6 b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ /* synthetic */ Mi d;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public If(Mi mi, ArrayList arrayList, Xe6 xe6) {
        super(2, xe6);
        this.d = mi;
        this.e = arrayList;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        If r0 = new If(this.d, this.e, xe6);
        r0.b = (Il6) obj;
        throw null;
        //return r0;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        If r0 = new If(this.d, this.e, xe6);
        r0.b = il6;
        return r0.invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Yn7.d();
        if (this.c == 0) {
            El7.b(obj);
            U.a.b(this.d.x.a().getMacAddress(), Ob.e.c);
            this.d.b0(this.e);
            Mi mi = this.d;
            mi.l(Nr.a(mi.v, null, Zq.b, null, null, 13));
            return Cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
