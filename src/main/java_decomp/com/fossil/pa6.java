package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingActivity;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pa6 implements MembersInjector<WeatherSettingActivity> {
    @DexIgnore
    public static void a(WeatherSettingActivity weatherSettingActivity, WeatherSettingPresenter weatherSettingPresenter) {
        weatherSettingActivity.A = weatherSettingPresenter;
    }
}
