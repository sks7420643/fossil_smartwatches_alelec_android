package com.fossil;

import com.mapped.Af6;
import com.mapped.Rm6;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Dx7 {
    @DexIgnore
    public static final Uu7 a(Rm6 rm6) {
        return new Ax7(rm6);
    }

    @DexIgnore
    public static /* synthetic */ Uu7 b(Rm6 rm6, int i, Object obj) {
        if ((i & 1) != 0) {
            rm6 = null;
        }
        return Bx7.a(rm6);
    }

    @DexIgnore
    public static final void c(Af6 af6, CancellationException cancellationException) {
        Rm6 rm6 = (Rm6) af6.get(Rm6.r);
        if (rm6 != null) {
            rm6.D(cancellationException);
        }
    }

    @DexIgnore
    public static /* synthetic */ void d(Af6 af6, CancellationException cancellationException, int i, Object obj) {
        if ((i & 1) != 0) {
            cancellationException = null;
        }
        Bx7.c(af6, cancellationException);
    }

    @DexIgnore
    public static final Dw7 e(Rm6 rm6, Dw7 dw7) {
        return rm6.A(new Fw7(rm6, dw7));
    }

    @DexIgnore
    public static final void f(Af6 af6) {
        Rm6 rm6 = (Rm6) af6.get(Rm6.r);
        if (rm6 != null) {
            Bx7.h(rm6);
            return;
        }
        throw new IllegalStateException(("Context cannot be checked for liveness because it does not have a job: " + af6).toString());
    }

    @DexIgnore
    public static final void g(Rm6 rm6) {
        if (!rm6.isActive()) {
            throw rm6.k();
        }
    }
}
