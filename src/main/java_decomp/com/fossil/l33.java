package com.fossil;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L33 extends R33 {
    @DexIgnore
    public /* final */ /* synthetic */ G33 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public L33(G33 g33) {
        super(g33, null);
        this.c = g33;
    }

    @DexIgnore
    public /* synthetic */ L33(G33 g33, J33 j33) {
        this(g33);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, com.fossil.R33, java.util.Collection, java.util.Set, java.lang.Iterable
    public final Iterator<Map.Entry<K, V>> iterator() {
        return new I33(this.c, null);
    }
}
