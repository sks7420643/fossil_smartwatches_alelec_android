package com.fossil;

import com.fossil.E38;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.SocketTimeoutException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K38 {
    @DexIgnore
    public long a; // = 0;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ I38 d;
    @DexIgnore
    public /* final */ Deque<P18> e; // = new ArrayDeque();
    @DexIgnore
    public E38.Ai f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ Bi h;
    @DexIgnore
    public /* final */ Ai i;
    @DexIgnore
    public /* final */ Ci j; // = new Ci();
    @DexIgnore
    public /* final */ Ci k; // = new Ci();
    @DexIgnore
    public D38 l; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai implements A58 {
        @DexIgnore
        public /* final */ I48 b; // = new I48();
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.A58
        public void K(I48 i48, long j) throws IOException {
            this.b.K(i48, j);
            while (this.b.p0() >= 16384) {
                a(false);
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public final void a(boolean z) throws IOException {
            long min;
            synchronized (K38.this) {
                K38.this.k.r();
                while (K38.this.b <= 0 && !this.d && !this.c && K38.this.l == null) {
                    try {
                        K38.this.t();
                    } catch (Throwable th) {
                        K38.this.k.y();
                        throw th;
                    }
                }
                K38.this.k.y();
                K38.this.e();
                min = Math.min(K38.this.b, this.b.p0());
                K38.this.b -= min;
            }
            K38.this.k.r();
            try {
                K38.this.d.r0(K38.this.c, z && min == this.b.p0(), this.b, min);
            } finally {
                K38.this.k.y();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x001d, code lost:
            if (r6.b.p0() <= 0) goto L_0x002d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
            if (r6.b.p0() <= 0) goto L_0x0037;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0029, code lost:
            a(true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:14:0x002d, code lost:
            r1 = r6.e;
            r1.d.r0(r1.c, true, null, 0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0037, code lost:
            r1 = r6.e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0039, code lost:
            monitor-enter(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            r6.c = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x003d, code lost:
            monitor-exit(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x003e, code lost:
            r6.e.d.flush();
            r6.e.d();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0013, code lost:
            if (r6.e.i.d != false) goto L_0x0037;
         */
        @DexIgnore
        @Override // java.io.Closeable, com.fossil.A58, java.lang.AutoCloseable
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void close() throws java.io.IOException {
            /*
                r6 = this;
                r4 = 0
                r2 = 1
                com.fossil.K38 r1 = com.fossil.K38.this
                monitor-enter(r1)
                boolean r0 = r6.c     // Catch:{ all -> 0x004e }
                if (r0 == 0) goto L_0x000c
                monitor-exit(r1)     // Catch:{ all -> 0x004e }
            L_0x000b:
                return
            L_0x000c:
                monitor-exit(r1)     // Catch:{ all -> 0x004e }
                com.fossil.K38 r0 = com.fossil.K38.this
                com.fossil.K38$Ai r0 = r0.i
                boolean r0 = r0.d
                if (r0 != 0) goto L_0x0037
                com.fossil.I48 r0 = r6.b
                long r0 = r0.p0()
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x002d
            L_0x001f:
                com.fossil.I48 r0 = r6.b
                long r0 = r0.p0()
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 <= 0) goto L_0x0037
                r6.a(r2)
                goto L_0x001f
            L_0x002d:
                com.fossil.K38 r1 = com.fossil.K38.this
                com.fossil.I38 r0 = r1.d
                int r1 = r1.c
                r3 = 0
                r0.r0(r1, r2, r3, r4)
            L_0x0037:
                com.fossil.K38 r1 = com.fossil.K38.this
                monitor-enter(r1)
                r0 = 1
                r6.c = r0     // Catch:{ all -> 0x004b }
                monitor-exit(r1)     // Catch:{ all -> 0x004b }
                com.fossil.K38 r0 = com.fossil.K38.this
                com.fossil.I38 r0 = r0.d
                r0.flush()
                com.fossil.K38 r0 = com.fossil.K38.this
                r0.d()
                goto L_0x000b
            L_0x004b:
                r0 = move-exception
                monitor-exit(r1)
                throw r0
            L_0x004e:
                r0 = move-exception
                monitor-exit(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.K38.Ai.close():void");
        }

        @DexIgnore
        @Override // com.fossil.A58
        public D58 e() {
            return K38.this.k;
        }

        @DexIgnore
        @Override // com.fossil.A58, java.io.Flushable
        public void flush() throws IOException {
            synchronized (K38.this) {
                K38.this.e();
            }
            while (this.b.p0() > 0) {
                a(false);
                K38.this.d.flush();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi implements C58 {
        @DexIgnore
        public /* final */ I48 b; // = new I48();
        @DexIgnore
        public /* final */ I48 c; // = new I48();
        @DexIgnore
        public /* final */ long d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public Bi(long j) {
            this.d = j;
        }

        @DexIgnore
        public void a(K48 k48, long j) throws IOException {
            boolean z;
            boolean z2;
            long j2;
            while (j > 0) {
                synchronized (K38.this) {
                    z = this.f;
                    z2 = this.c.p0() + j > this.d;
                }
                if (z2) {
                    k48.skip(j);
                    K38.this.h(D38.FLOW_CONTROL_ERROR);
                    return;
                } else if (z) {
                    k48.skip(j);
                    return;
                } else {
                    long d0 = k48.d0(this.b, j);
                    if (d0 != -1) {
                        j -= d0;
                        synchronized (K38.this) {
                            if (this.e) {
                                j2 = this.b.p0();
                                this.b.j();
                            } else {
                                boolean z3 = this.c.p0() == 0;
                                this.c.N(this.b);
                                if (z3) {
                                    K38.this.notifyAll();
                                }
                                j2 = 0;
                            }
                        }
                        if (j2 > 0) {
                            b(j2);
                        }
                    } else {
                        throw new EOFException();
                    }
                }
            }
        }

        @DexIgnore
        public final void b(long j) {
            K38.this.d.q0(j);
        }

        @DexIgnore
        @Override // com.fossil.C58, java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            long p0;
            ArrayList<P18> arrayList;
            E38.Ai ai;
            synchronized (K38.this) {
                this.e = true;
                p0 = this.c.p0();
                this.c.j();
                if (K38.this.e.isEmpty() || K38.this.f == null) {
                    arrayList = null;
                    ai = null;
                } else {
                    arrayList = new ArrayList(K38.this.e);
                    K38.this.e.clear();
                    ai = K38.this.f;
                }
                K38.this.notifyAll();
            }
            if (p0 > 0) {
                b(p0);
            }
            K38.this.d();
            if (ai != null) {
                for (P18 p18 : arrayList) {
                    ai.a(p18);
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.C58
        public long d0(I48 i48, long j) throws IOException {
            D38 d38;
            long j2;
            P18 p18;
            E38.Ai ai;
            long j3;
            if (j >= 0) {
                while (true) {
                    synchronized (K38.this) {
                        K38.this.j.r();
                        try {
                            d38 = K38.this.l != null ? K38.this.l : null;
                            if (!this.e) {
                                if (K38.this.e.isEmpty() || K38.this.f == null) {
                                    if (this.c.p0() > 0) {
                                        j2 = this.c.d0(i48, Math.min(j, this.c.p0()));
                                        K38.this.a += j2;
                                        if (d38 == null && K38.this.a >= ((long) (K38.this.d.y.d() / 2))) {
                                            K38.this.d.v0(K38.this.c, K38.this.a);
                                            K38.this.a = 0;
                                        }
                                    } else if (this.f || d38 != null) {
                                        j2 = -1;
                                    } else {
                                        K38.this.t();
                                        K38.this.j.y();
                                    }
                                    p18 = null;
                                    ai = null;
                                    j3 = j2;
                                } else {
                                    P18 p182 = (P18) K38.this.e.removeFirst();
                                    j3 = -1;
                                    ai = K38.this.f;
                                    p18 = p182;
                                }
                                if (p18 != null && ai != null) {
                                    ai.a(p18);
                                }
                            } else {
                                throw new IOException("stream closed");
                            }
                        } finally {
                            K38.this.j.y();
                        }
                    }
                }
                if (j3 != -1) {
                    b(j3);
                    return j3;
                } else if (d38 == null) {
                    return -1;
                } else {
                    throw new P38(d38);
                }
            } else {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            }
        }

        @DexIgnore
        @Override // com.fossil.C58
        public D58 e() {
            return K38.this.j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends G48 {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        @Override // com.fossil.G48
        public IOException t(IOException iOException) {
            SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
            if (iOException != null) {
                socketTimeoutException.initCause(iOException);
            }
            return socketTimeoutException;
        }

        @DexIgnore
        @Override // com.fossil.G48
        public void x() {
            K38.this.h(D38.CANCEL);
            K38.this.d.g0();
        }

        @DexIgnore
        public void y() throws IOException {
            if (s()) {
                throw t(null);
            }
        }
    }

    @DexIgnore
    public K38(int i2, I38 i38, boolean z, boolean z2, P18 p18) {
        if (i38 != null) {
            this.c = i2;
            this.d = i38;
            this.b = (long) i38.z.d();
            this.h = new Bi((long) i38.y.d());
            Ai ai = new Ai();
            this.i = ai;
            this.h.f = z2;
            ai.d = z;
            if (p18 != null) {
                this.e.add(p18);
            }
            if (l() && p18 != null) {
                throw new IllegalStateException("locally-initiated streams shouldn't have headers yet");
            } else if (!l() && p18 == null) {
                throw new IllegalStateException("remotely-initiated streams should have headers");
            }
        } else {
            throw new NullPointerException("connection == null");
        }
    }

    @DexIgnore
    public void c(long j2) {
        this.b += j2;
        if (j2 > 0) {
            notifyAll();
        }
    }

    @DexIgnore
    public void d() throws IOException {
        boolean z;
        boolean m;
        synchronized (this) {
            z = !this.h.f && this.h.e && (this.i.d || this.i.c);
            m = m();
        }
        if (z) {
            f(D38.CANCEL);
        } else if (!m) {
            this.d.b0(this.c);
        }
    }

    @DexIgnore
    public void e() throws IOException {
        Ai ai = this.i;
        if (ai.c) {
            throw new IOException("stream closed");
        } else if (ai.d) {
            throw new IOException("stream finished");
        } else if (this.l != null) {
            throw new P38(this.l);
        }
    }

    @DexIgnore
    public void f(D38 d38) throws IOException {
        if (g(d38)) {
            this.d.t0(this.c, d38);
        }
    }

    @DexIgnore
    public final boolean g(D38 d38) {
        synchronized (this) {
            if (this.l != null) {
                return false;
            }
            if (this.h.f && this.i.d) {
                return false;
            }
            this.l = d38;
            notifyAll();
            this.d.b0(this.c);
            return true;
        }
    }

    @DexIgnore
    public void h(D38 d38) {
        if (g(d38)) {
            this.d.u0(this.c, d38);
        }
    }

    @DexIgnore
    public int i() {
        return this.c;
    }

    @DexIgnore
    public A58 j() {
        synchronized (this) {
            if (!this.g && !l()) {
                throw new IllegalStateException("reply before requesting the sink");
            }
        }
        return this.i;
    }

    @DexIgnore
    public C58 k() {
        return this.h;
    }

    @DexIgnore
    public boolean l() {
        return this.d.b == ((this.c & 1) == 1);
    }

    @DexIgnore
    public boolean m() {
        synchronized (this) {
            if (this.l != null) {
                return false;
            }
            return (!this.h.f && !this.h.e) || (!this.i.d && !this.i.c) || !this.g;
        }
    }

    @DexIgnore
    public D58 n() {
        return this.j;
    }

    @DexIgnore
    public void o(K48 k48, int i2) throws IOException {
        this.h.a(k48, (long) i2);
    }

    @DexIgnore
    public void p() {
        boolean m;
        synchronized (this) {
            this.h.f = true;
            m = m();
            notifyAll();
        }
        if (!m) {
            this.d.b0(this.c);
        }
    }

    @DexIgnore
    public void q(List<E38> list) {
        boolean m;
        synchronized (this) {
            this.g = true;
            this.e.add(B28.H(list));
            m = m();
            notifyAll();
        }
        if (!m) {
            this.d.b0(this.c);
        }
    }

    @DexIgnore
    public void r(D38 d38) {
        synchronized (this) {
            if (this.l == null) {
                this.l = d38;
                notifyAll();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public P18 s() throws IOException {
        P18 removeFirst;
        synchronized (this) {
            this.j.r();
            while (this.e.isEmpty() && this.l == null) {
                try {
                    t();
                } catch (Throwable th) {
                    this.j.y();
                    throw th;
                }
            }
            this.j.y();
            if (!this.e.isEmpty()) {
                removeFirst = this.e.removeFirst();
            } else {
                throw new P38(this.l);
            }
        }
        return removeFirst;
    }

    @DexIgnore
    public void t() throws InterruptedIOException {
        try {
            wait();
        } catch (InterruptedException e2) {
            Thread.currentThread().interrupt();
            throw new InterruptedIOException();
        }
    }

    @DexIgnore
    public D58 u() {
        return this.k;
    }
}
