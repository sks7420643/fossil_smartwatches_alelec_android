package com.fossil;

import android.app.Activity;
import android.os.RemoteException;
import com.fossil.Zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ft2 extends Zs2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ Activity f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ String h;
    @DexIgnore
    public /* final */ /* synthetic */ Zs2 i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Ft2(Zs2 zs2, Activity activity, String str, String str2) {
        super(zs2);
        this.i = zs2;
        this.f = activity;
        this.g = str;
        this.h = str2;
    }

    @DexIgnore
    @Override // com.fossil.Zs2.Ai
    public final void a() throws RemoteException {
        this.i.h.setCurrentScreen(Tg2.n(this.f), this.g, this.h, this.b);
    }
}
