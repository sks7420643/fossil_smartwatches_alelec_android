package com.fossil;

import com.mapped.Wg6;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gl7<A, B, C> implements Serializable {
    @DexIgnore
    public /* final */ A first;
    @DexIgnore
    public /* final */ B second;
    @DexIgnore
    public /* final */ C third;

    @DexIgnore
    public Gl7(A a2, B b, C c) {
        this.first = a2;
        this.second = b;
        this.third = c;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.Gl7 */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Gl7 copy$default(Gl7 gl7, Object obj, Object obj2, Object obj3, int i, Object obj4) {
        if ((i & 1) != 0) {
            obj = gl7.first;
        }
        if ((i & 2) != 0) {
            obj2 = gl7.second;
        }
        if ((i & 4) != 0) {
            obj3 = gl7.third;
        }
        return gl7.copy(obj, obj2, obj3);
    }

    @DexIgnore
    public final A component1() {
        return this.first;
    }

    @DexIgnore
    public final B component2() {
        return this.second;
    }

    @DexIgnore
    public final C component3() {
        return this.third;
    }

    @DexIgnore
    public final Gl7<A, B, C> copy(A a2, B b, C c) {
        return new Gl7<>(a2, b, c);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Gl7) {
                Gl7 gl7 = (Gl7) obj;
                if (!Wg6.a(this.first, gl7.first) || !Wg6.a(this.second, gl7.second) || !Wg6.a(this.third, gl7.third)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final A getFirst() {
        return this.first;
    }

    @DexIgnore
    public final B getSecond() {
        return this.second;
    }

    @DexIgnore
    public final C getThird() {
        return this.third;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        A a2 = this.first;
        int hashCode = a2 != null ? a2.hashCode() : 0;
        B b = this.second;
        int hashCode2 = b != null ? b.hashCode() : 0;
        C c = this.third;
        if (c != null) {
            i = c.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return '(' + ((Object) this.first) + ", " + ((Object) this.second) + ", " + ((Object) this.third) + ')';
    }
}
