package com.fossil;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q34<T> extends I44<Iterable<T>> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ Comparator<? super T> elementOrder;

    @DexIgnore
    public Q34(Comparator<? super T> comparator) {
        this.elementOrder = comparator;
    }

    @DexIgnore
    public int compare(Iterable<T> iterable, Iterable<T> iterable2) {
        Iterator<T> it = iterable2.iterator();
        for (T t : iterable) {
            if (!it.hasNext()) {
                return 1;
            }
            int compare = this.elementOrder.compare(t, it.next());
            if (compare != 0) {
                return compare;
            }
        }
        return it.hasNext() ? -1 : 0;
    }

    @DexIgnore
    @Override // com.fossil.I44, java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        return compare((Iterable) ((Iterable) obj), (Iterable) ((Iterable) obj2));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Q34) {
            return this.elementOrder.equals(((Q34) obj).elementOrder);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.elementOrder.hashCode() ^ 2075626741;
    }

    @DexIgnore
    public String toString() {
        return this.elementOrder + ".lexicographical()";
    }
}
