package com.fossil;

import com.portfolio.platform.ui.user.information.domain.usecase.GetUser;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cp6 implements Factory<ProfileEditViewModel> {
    @DexIgnore
    public /* final */ Provider<UpdateUser> a;
    @DexIgnore
    public /* final */ Provider<GetUser> b;

    @DexIgnore
    public Cp6(Provider<UpdateUser> provider, Provider<GetUser> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Cp6 a(Provider<UpdateUser> provider, Provider<GetUser> provider2) {
        return new Cp6(provider, provider2);
    }

    @DexIgnore
    public static ProfileEditViewModel c(UpdateUser updateUser, GetUser getUser) {
        return new ProfileEditViewModel(updateUser, getUser);
    }

    @DexIgnore
    public ProfileEditViewModel b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
