package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public interface Da3 {
    @DexIgnore
    T62<Status> a(R62 r62, Ga3 ga3);

    @DexIgnore
    T62<Status> b(R62 r62, LocationRequest locationRequest, Ga3 ga3);
}
