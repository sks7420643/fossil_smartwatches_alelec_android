package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.mapped.AlertDialogFragment;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.create_intro.BCCreateChallengeIntroActivity;
import com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsActivity;
import com.portfolio.platform.buddy_challenge.screens.tab.challenge.create.BCCreateSubTabViewModel;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleButton;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ix4 extends BaseFragment implements AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public G37<F45> g;
    @DexIgnore
    public BCCreateSubTabViewModel h;
    @DexIgnore
    public Po4 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Ix4.k;
        }

        @DexIgnore
        public final Ix4 b() {
            return new Ix4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Qq7 implements Hg6<View, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Ix4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Ix4 ix4) {
            super(1);
            this.this$0 = ix4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(View view) {
            invoke(view);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            Ix4.L6(this.this$0).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<Iz4> {
        @DexIgnore
        public /* final */ /* synthetic */ Ix4 a;

        @DexIgnore
        public Ci(Ix4 ix4) {
            this.a = ix4;
        }

        @DexIgnore
        public final void a(Iz4 iz4) {
            boolean z = true;
            if (iz4 != null) {
                int i = Jx4.a[iz4.ordinal()];
                if (i == 1) {
                    S37 s37 = S37.c;
                    FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                    Wg6.b(childFragmentManager, "childFragmentManager");
                    String c = Um5.c(PortfolioApp.get.instance(), 2131886235);
                    Wg6.b(c, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    String c2 = Um5.c(PortfolioApp.get.instance(), 2131886233);
                    Wg6.b(c2, "LanguageHelper.getString\u2026uCanOnlyJoinOneChallenge)");
                    s37.E(childFragmentManager, c, c2);
                } else if (i == 2) {
                    if (PortfolioApp.get.instance().J().length() <= 0) {
                        z = false;
                    }
                    if (z) {
                        S37 s372 = S37.c;
                        FragmentManager childFragmentManager2 = this.a.getChildFragmentManager();
                        Wg6.b(childFragmentManager2, "childFragmentManager");
                        String c3 = Um5.c(PortfolioApp.get.instance(), 2131886235);
                        Wg6.b(c3, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                        String c4 = Um5.c(PortfolioApp.get.instance(), 2131886234);
                        Wg6.b(c4, "LanguageHelper.getString\u2026IsDisconnectedPleaseSync)");
                        s372.E(childFragmentManager2, c3, c4);
                        return;
                    }
                    S37 s373 = S37.c;
                    FragmentManager childFragmentManager3 = this.a.getChildFragmentManager();
                    Wg6.b(childFragmentManager3, "childFragmentManager");
                    String c5 = Um5.c(PortfolioApp.get.instance(), 2131886235);
                    Wg6.b(c5, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    String c6 = Um5.c(PortfolioApp.get.instance(), 2131886226);
                    Wg6.b(c6, "LanguageHelper.getString\u2026ctivateYourDeviceToJoinA)");
                    s373.E(childFragmentManager3, c5, c6);
                } else if (i == 3) {
                    S37 s374 = S37.c;
                    FragmentManager childFragmentManager4 = this.a.getChildFragmentManager();
                    Wg6.b(childFragmentManager4, "childFragmentManager");
                    s374.C0(childFragmentManager4);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Iz4 iz4) {
            a(iz4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ix4 a;

        @DexIgnore
        public Di(Ix4 ix4) {
            this.a = ix4;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(Object obj) {
            this.a.P6();
        }
    }

    /*
    static {
        String name = Ix4.class.getName();
        Wg6.b(name, "BCCreateSubTabFragment::class.java.name");
        k = name;
    }
    */

    @DexIgnore
    public static final /* synthetic */ BCCreateSubTabViewModel L6(Ix4 ix4) {
        BCCreateSubTabViewModel bCCreateSubTabViewModel = ix4.h;
        if (bCCreateSubTabViewModel != null) {
            return bCCreateSubTabViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void N6() {
        FlexibleButton flexibleButton;
        G37<F45> g37 = this.g;
        if (g37 != null) {
            F45 a2 = g37.a();
            if (a2 != null && (flexibleButton = a2.q) != null) {
                Fz4.a(flexibleButton, new Bi(this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void O6() {
        BCCreateSubTabViewModel bCCreateSubTabViewModel = this.h;
        if (bCCreateSubTabViewModel != null) {
            bCCreateSubTabViewModel.g().h(getViewLifecycleOwner(), new Ci(this));
            BCCreateSubTabViewModel bCCreateSubTabViewModel2 = this.h;
            if (bCCreateSubTabViewModel2 != null) {
                bCCreateSubTabViewModel2.f().h(getViewLifecycleOwner(), new Di(this));
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void P6() {
        BCCreateChallengeIntroActivity.a aVar = BCCreateChallengeIntroActivity.A;
        Fragment requireParentFragment = requireParentFragment();
        Wg6.b(requireParentFragment, "requireParentFragment()");
        aVar.a(requireParentFragment);
    }

    @DexIgnore
    public final void Q6() {
        BCFindFriendsActivity.A.a(this, null);
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        if (str.hashCode() != 1839815732 || !str.equals("FINDING_FRIEND")) {
            return;
        }
        if (i2 == 2131363291) {
            Q6();
        } else if (i2 == 2131363373) {
            P6();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        N6();
        O6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        F45 f45 = (F45) Aq0.f(layoutInflater, 2131558527, viewGroup, false, A6());
        this.g = new G37<>(this, f45);
        PortfolioApp.get.instance().getIface().B1().a(this);
        Po4 po4 = this.i;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCCreateSubTabViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026TabViewModel::class.java)");
            this.h = (BCCreateSubTabViewModel) a2;
            Wg6.b(f45, "binding");
            return f45.n();
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
