package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lz2 extends Gz2 {
    @DexIgnore
    @Override // com.fossil.Gz2
    public final void a(Throwable th, Throwable th2) {
        th.addSuppressed(th2);
    }
}
