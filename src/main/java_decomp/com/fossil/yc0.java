package com.fossil;

import com.fossil.V18;
import com.mapped.Hg6;
import com.mapped.Wg6;
import java.util.HashMap;
import java.util.Map;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yc0 implements Interceptor {
    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) {
        V18.Ai h = chain.c().h();
        Hg6<V18, HashMap<String, String>> j = Id0.i.j();
        if (j != null) {
            V18 c = chain.c();
            Wg6.b(c, "chain.request()");
            for (Map.Entry<String, String> entry : j.invoke(c).entrySet()) {
                h.a(entry.getKey(), entry.getValue());
            }
        }
        Response d = chain.d(h.b());
        Wg6.b(d, "chain.proceed(requestBuilder.build())");
        return d;
    }
}
