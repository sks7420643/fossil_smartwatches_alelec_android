package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Fm1 {
    WEATHER("weatherSSE"),
    HEART_RATE("hrSSE"),
    STEPS("stepsSSE"),
    DATE("dateSSE"),
    CHANCE_OF_RAIN("chanceOfRainSSE"),
    SECOND_TIMEZONE("timeZone2SSE"),
    ACTIVE_MINUTES("activeMinutesSSE"),
    CALORIES("caloriesSSE"),
    BATTERY("batterySSE"),
    EMPTY("empty");
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }
    }

    @DexIgnore
    public Fm1(String str) {
        this.b = str;
    }

    @DexIgnore
    public final Object a() {
        Object obj = X8.a[ordinal()] != 1 ? this.b : JSONObject.NULL;
        Wg6.b(obj, "when (this) {\n          \u2026 -> rawName\n            }");
        return obj;
    }
}
