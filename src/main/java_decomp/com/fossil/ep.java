package com.fossil;

import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.charset.Charset;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ep {
    @DexIgnore
    public /* synthetic */ Ep(Qg6 qg6) {
    }

    @DexIgnore
    public final byte[] a(JSONObject jSONObject) {
        String jSONObject2 = jSONObject.toString();
        Wg6.b(jSONObject2, "jsonFileContent.toString()");
        Charset c = Hd0.y.c();
        if (jSONObject2 != null) {
            byte[] bytes = jSONObject2.getBytes(c);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }
}
