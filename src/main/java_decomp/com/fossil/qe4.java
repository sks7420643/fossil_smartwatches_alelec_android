package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;
import java.util.concurrent.ExecutorService;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qe4 implements Le4 {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public static Kg4 d;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ExecutorService b;

    @DexIgnore
    public Qe4(Context context, ExecutorService executorService) {
        this.a = context;
        this.b = executorService;
    }

    @DexIgnore
    public static Nt3<Integer> b(Context context, Intent intent) {
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            Log.d("FirebaseInstanceId", "Binding to service");
        }
        return c(context, "com.google.firebase.MESSAGING_EVENT").c(intent).i(Se4.a(), Oe4.a);
    }

    @DexIgnore
    public static Kg4 c(Context context, String str) {
        Kg4 kg4;
        synchronized (c) {
            if (d == null) {
                d = new Kg4(context, str);
            }
            kg4 = d;
        }
        return kg4;
    }

    @DexIgnore
    public static final /* synthetic */ Integer d(Nt3 nt3) throws Exception {
        return -1;
    }

    @DexIgnore
    public static final /* synthetic */ Nt3 g(Context context, Intent intent, Nt3 nt3) throws Exception {
        return (!Mf2.j() || ((Integer) nt3.m()).intValue() != 402) ? nt3 : b(context, intent).i(Se4.a(), Pe4.a);
    }

    @DexIgnore
    @Override // com.fossil.Le4
    public Nt3<Integer> a(Intent intent) {
        String stringExtra = intent.getStringExtra("gcm.rawData64");
        if (stringExtra != null) {
            intent.putExtra("rawData", Base64.decode(stringExtra, 0));
            intent.removeExtra("gcm.rawData64");
        }
        return h(this.a, intent);
    }

    @DexIgnore
    @SuppressLint({"InlinedApi"})
    public Nt3<Integer> h(Context context, Intent intent) {
        boolean z = true;
        boolean z2 = Mf2.j() && context.getApplicationInfo().targetSdkVersion >= 26;
        if ((intent.getFlags() & SQLiteDatabase.CREATE_IF_NECESSARY) == 0) {
            z = false;
        }
        return (!z2 || z) ? Qt3.c(this.b, new Me4(context, intent)).k(this.b, new Ne4(context, intent)) : b(context, intent);
    }
}
