package com.fossil;

import android.content.Context;
import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.Ta4;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Q94 {
    @DexIgnore
    public /* final */ Y84 a;
    @DexIgnore
    public /* final */ Sb4 b;
    @DexIgnore
    public /* final */ Mc4 c;
    @DexIgnore
    public /* final */ V94 d;
    @DexIgnore
    public /* final */ S94 e;
    @DexIgnore
    public String f;

    @DexIgnore
    public Q94(Y84 y84, Sb4 sb4, Mc4 mc4, V94 v94, S94 s94) {
        this.a = y84;
        this.b = sb4;
        this.c = mc4;
        this.d = v94;
        this.e = s94;
    }

    @DexIgnore
    public static Q94 b(Context context, H94 h94, Tb4 tb4, L84 l84, V94 v94, S94 s94, Kd4 kd4, Rc4 rc4) {
        return new Q94(new Y84(context, h94, l84, kd4), new Sb4(new File(tb4.a()), rc4), Mc4.a(context), v94, s94);
    }

    @DexIgnore
    public static List<Ta4.Bi> e(Map<String, String> map) {
        ArrayList arrayList = new ArrayList();
        arrayList.ensureCapacity(map.size());
        for (Map.Entry<String, String> entry : map.entrySet()) {
            Ta4.Bi.Aii a2 = Ta4.Bi.a();
            a2.b(entry.getKey());
            a2.c(entry.getValue());
            arrayList.add(a2.a());
        }
        Collections.sort(arrayList, P94.a());
        return arrayList;
    }

    @DexIgnore
    public void c(String str, List<L94> list) {
        ArrayList arrayList = new ArrayList();
        for (L94 l94 : list) {
            Ta4.Ci.Bii c2 = l94.c();
            if (c2 != null) {
                arrayList.add(c2);
            }
        }
        Sb4 sb4 = this.b;
        Ta4.Ci.Aii a2 = Ta4.Ci.a();
        a2.b(Ua4.a(arrayList));
        sb4.j(str, a2.a());
    }

    @DexIgnore
    public void d(long j) {
        this.b.i(this.f, j);
    }

    @DexIgnore
    public void g(String str, long j) {
        this.f = str;
        this.b.B(this.a.c(str, j));
    }

    @DexIgnore
    public void h() {
        this.f = null;
    }

    @DexIgnore
    public final boolean i(Nt3<Z84> nt3) {
        if (nt3.q()) {
            Z84 m = nt3.m();
            X74 f2 = X74.f();
            f2.b("Crashlytics report successfully enqueued to DataTransport: " + m.c());
            this.b.h(m.c());
            return true;
        }
        X74.f().c("Crashlytics report could not be enqueued to DataTransport", nt3.l());
        return false;
    }

    @DexIgnore
    public final void j(Throwable th, Thread thread, String str, long j, boolean z) {
        String str2 = this.f;
        if (str2 == null) {
            X74.f().b("Cannot persist event, no currently open session");
            return;
        }
        boolean equals = str.equals(CrashDumperPlugin.NAME);
        Ta4.Di.Dii b2 = this.a.b(th, thread, str, j, 4, 8, z);
        Ta4.Di.Dii.Biii g = b2.g();
        String d2 = this.d.d();
        if (d2 != null) {
            Ta4.Di.Dii.Diii.Aiiii a2 = Ta4.Di.Dii.Diii.a();
            a2.b(d2);
            g.d(a2.a());
        } else {
            X74.f().b("No log data to include with this event.");
        }
        List<Ta4.Bi> e2 = e(this.e.a());
        if (!e2.isEmpty()) {
            Ta4.Di.Dii.Aiii.Aiiii f2 = b2.b().f();
            f2.c(Ua4.a(e2));
            g.b(f2.a());
        }
        this.b.A(g.a(), str2, equals);
    }

    @DexIgnore
    public void k(Throwable th, Thread thread, long j) {
        j(th, thread, CrashDumperPlugin.NAME, j, true);
    }

    @DexIgnore
    public void l() {
        String str = this.f;
        if (str == null) {
            X74.f().b("Could not persist user ID; no current session");
            return;
        }
        String b2 = this.e.b();
        if (b2 == null) {
            X74.f().b("Could not persist user ID; no user ID available");
        } else {
            this.b.C(b2, str);
        }
    }

    @DexIgnore
    public void m() {
        this.b.g();
    }

    @DexIgnore
    public Nt3<Void> n(Executor executor, D94 d94) {
        if (d94 == D94.NONE) {
            X74.f().b("Send via DataTransport disabled. Removing DataTransport reports.");
            this.b.g();
            return Qt3.f(null);
        }
        List<Z84> x = this.b.x();
        ArrayList arrayList = new ArrayList();
        for (Z84 z84 : x) {
            if (z84.b().k() != Ta4.Ei.NATIVE || d94 == D94.ALL) {
                arrayList.add(this.c.e(z84).i(executor, O94.a(this)));
            } else {
                X74.f().b("Send native reports via DataTransport disabled. Removing DataTransport reports.");
                this.b.h(z84.c());
            }
        }
        return Qt3.g(arrayList);
    }
}
