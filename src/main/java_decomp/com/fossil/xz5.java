package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.mapped.Jh6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xz5 extends U47 implements R36 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ Ai u; // = new Ai(null);
    @DexIgnore
    public /* final */ Zp0 k; // = new Sr4(this);
    @DexIgnore
    public G37<Z55> l;
    @DexIgnore
    public Q36 m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Xz5.t;
        }

        @DexIgnore
        public final Xz5 b() {
            return new Xz5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ Xz5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Z55 b;

        @DexIgnore
        public Bi(Xz5 xz5, Z55 z55) {
            this.a = xz5;
            this.b = z55;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            Q36 A6 = Xz5.A6(this.a);
            NumberPicker numberPicker2 = this.b.v;
            Wg6.b(numberPicker2, "binding.numberPickerTwo");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.b.u;
            Wg6.b(numberPicker3, "binding.numberPickerThree");
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            A6.p(String.valueOf(i2), String.valueOf(value), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ Xz5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Z55 b;

        @DexIgnore
        public Ci(Xz5 xz5, Z55 z55) {
            this.a = xz5;
            this.b = z55;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            Q36 A6 = Xz5.A6(this.a);
            NumberPicker numberPicker2 = this.b.t;
            Wg6.b(numberPicker2, "binding.numberPickerOne");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.b.u;
            Wg6.b(numberPicker3, "binding.numberPickerThree");
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            A6.p(String.valueOf(value), String.valueOf(i2), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ Xz5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Z55 b;

        @DexIgnore
        public Di(Xz5 xz5, Z55 z55) {
            this.a = xz5;
            this.b = z55;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            Q36 A6 = Xz5.A6(this.a);
            NumberPicker numberPicker2 = this.b.t;
            Wg6.b(numberPicker2, "binding.numberPickerOne");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.b.v;
            Wg6.b(numberPicker3, "binding.numberPickerTwo");
            int value2 = numberPicker3.getValue();
            if (i2 != 1) {
                z = false;
            }
            A6.p(String.valueOf(value), String.valueOf(value2), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Xz5 b;

        @DexIgnore
        public Ei(Xz5 xz5) {
            this.b = xz5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Xz5.A6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ Z55 b;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 c;

        @DexIgnore
        public Fi(Z55 z55, Jh6 jh6) {
            this.b = z55;
            this.c = jh6;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.b.w;
            Wg6.b(constraintLayout, "it.rootBackground");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                ViewGroup.LayoutParams layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) ((CoordinatorLayout.e) layoutParams).f();
                    if (bottomSheetBehavior != null) {
                        bottomSheetBehavior.E(3);
                        Z55 z55 = this.b;
                        Wg6.b(z55, "it");
                        View n = z55.n();
                        Wg6.b(n, "it.root");
                        n.getViewTreeObserver().removeOnGlobalLayoutListener(this.c.element);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                throw new Rc6("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new Rc6("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = Xz5.class.getSimpleName();
        Wg6.b(simpleName, "DoNotDisturbScheduledTim\u2026nt::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ Q36 A6(Xz5 xz5) {
        Q36 q36 = xz5.m;
        if (q36 != null) {
            return q36;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void C6(Z55 z55) {
        String d = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(d)) {
            z55.w.setBackgroundColor(Color.parseColor(d));
        }
        NumberPicker numberPicker = z55.t;
        Wg6.b(numberPicker, "binding.numberPickerOne");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = z55.t;
        Wg6.b(numberPicker2, "binding.numberPickerOne");
        numberPicker2.setMaxValue(12);
        z55.t.setOnValueChangedListener(new Bi(this, z55));
        NumberPicker numberPicker3 = z55.v;
        Wg6.b(numberPicker3, "binding.numberPickerTwo");
        numberPicker3.setMinValue(0);
        NumberPicker numberPicker4 = z55.v;
        Wg6.b(numberPicker4, "binding.numberPickerTwo");
        numberPicker4.setMaxValue(59);
        z55.v.setOnValueChangedListener(new Ci(this, z55));
        String c = Um5.c(PortfolioApp.get.instance(), 2131886102);
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886104);
        NumberPicker numberPicker5 = z55.u;
        Wg6.b(numberPicker5, "binding.numberPickerThree");
        numberPicker5.setMinValue(0);
        NumberPicker numberPicker6 = z55.u;
        Wg6.b(numberPicker6, "binding.numberPickerThree");
        numberPicker6.setMaxValue(1);
        z55.u.setDisplayedValues(new String[]{c, c2});
        z55.u.setOnValueChangedListener(new Di(this, z55));
    }

    @DexIgnore
    public final void D6(int i) {
        Q36 q36 = this.m;
        if (q36 != null) {
            q36.o(i);
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.R36
    public void E(int i) {
        int i2;
        int i3 = i / 60;
        if (i3 >= 12) {
            i2 = 1;
            i3 -= 12;
        } else {
            i2 = 0;
        }
        if (i3 == 0) {
            i3 = 12;
        }
        G37<Z55> g37 = this.l;
        if (g37 != null) {
            Z55 a2 = g37.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.t;
                Wg6.b(numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i3);
                NumberPicker numberPicker2 = a2.v;
                Wg6.b(numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i % 60);
                NumberPicker numberPicker3 = a2.u;
                Wg6.b(numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i2);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void E6(Q36 q36) {
        Wg6.c(q36, "presenter");
        this.m = q36;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Q36 q36) {
        E6(q36);
    }

    @DexIgnore
    @Override // com.fossil.R36
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Z55 z55 = (Z55) Aq0.f(layoutInflater, 2131558550, viewGroup, false, this.k);
        z55.q.setOnClickListener(new Ei(this));
        Wg6.b(z55, "binding");
        C6(z55);
        this.l = new G37<>(this, z55);
        return z55.n();
    }

    @DexIgnore
    @Override // com.fossil.U47, androidx.fragment.app.Fragment, com.fossil.Kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        Q36 q36 = this.m;
        if (q36 != null) {
            q36.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Q36 q36 = this.m;
        if (q36 != null) {
            q36.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<Z55> g37 = this.l;
        if (g37 != null) {
            Z55 a2 = g37.a();
            if (a2 != null) {
                Jh6 jh6 = new Jh6();
                jh6.element = null;
                jh6.element = (T) new Fi(a2, jh6);
                Wg6.b(a2, "it");
                View n = a2.n();
                Wg6.b(n, "it.root");
                n.getViewTreeObserver().addOnGlobalLayoutListener(jh6.element);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.R36
    public void t(String str) {
        FlexibleTextView flexibleTextView;
        Wg6.c(str, "title");
        G37<Z55> g37 = this.l;
        if (g37 != null) {
            Z55 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.r) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.U47
    public void z6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
