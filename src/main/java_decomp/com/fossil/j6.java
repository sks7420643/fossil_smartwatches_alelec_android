package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J6 extends M5 {
    @DexIgnore
    public /* final */ byte[] m;

    @DexIgnore
    public J6(N6 n6, byte[] bArr, N4 n4) {
        super(V5.i, n6, n4);
        this.m = bArr;
    }

    @DexIgnore
    @Override // com.fossil.M5, com.fossil.U5
    public JSONObject b(boolean z) {
        JSONObject b = super.b(z);
        if (z) {
            byte[] bArr = this.m;
            if (bArr.length < 100) {
                G80.k(b, Jd0.S0, Dy1.e(bArr, null, 1, null));
                return b;
            }
        }
        G80.k(b, Jd0.T0, Integer.valueOf(this.m.length));
        return b;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public void d(K5 k5) {
        k5.y(this.l, this.m);
        this.k = true;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public boolean i(H7 h7) {
        return (h7 instanceof L7) && ((L7) h7).b == this.l;
    }

    @DexIgnore
    @Override // com.fossil.U5
    public Fd0<H7> j() {
        return this.j.a;
    }
}
