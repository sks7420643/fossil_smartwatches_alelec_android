package com.fossil;

import com.fossil.Ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ea4 extends Ta4.Di.Aii {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ Ta4.Di.Aii.Biii d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4.Di.Aii.Aiii {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Ta4.Di.Aii.Biii d;
        @DexIgnore
        public String e;

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Aii.Aiii
        public Ta4.Di.Aii a() {
            String str = "";
            if (this.a == null) {
                str = " identifier";
            }
            if (this.b == null) {
                str = str + " version";
            }
            if (str.isEmpty()) {
                return new Ea4(this.a, this.b, this.c, this.d, this.e);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Aii.Aiii
        public Ta4.Di.Aii.Aiii b(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Aii.Aiii
        public Ta4.Di.Aii.Aiii c(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null identifier");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Aii.Aiii
        public Ta4.Di.Aii.Aiii d(String str) {
            this.e = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Di.Aii.Aiii
        public Ta4.Di.Aii.Aiii e(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null version");
        }
    }

    @DexIgnore
    public Ea4(String str, String str2, String str3, Ta4.Di.Aii.Biii biii, String str4) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = biii;
        this.e = str4;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Aii
    public String b() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Aii
    public String c() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Aii
    public String d() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Aii
    public Ta4.Di.Aii.Biii e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        String str;
        Ta4.Di.Aii.Biii biii;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4.Di.Aii)) {
            return false;
        }
        Ta4.Di.Aii aii = (Ta4.Di.Aii) obj;
        if (this.a.equals(aii.c()) && this.b.equals(aii.f()) && ((str = this.c) != null ? str.equals(aii.b()) : aii.b() == null) && ((biii = this.d) != null ? biii.equals(aii.e()) : aii.e() == null)) {
            String str2 = this.e;
            if (str2 == null) {
                if (aii.d() == null) {
                    return true;
                }
            } else if (str2.equals(aii.d())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Aii
    public String f() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int hashCode = this.a.hashCode();
        int hashCode2 = this.b.hashCode();
        String str = this.c;
        int hashCode3 = str == null ? 0 : str.hashCode();
        Ta4.Di.Aii.Biii biii = this.d;
        int hashCode4 = biii == null ? 0 : biii.hashCode();
        String str2 = this.e;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return ((((hashCode3 ^ ((((hashCode ^ 1000003) * 1000003) ^ hashCode2) * 1000003)) * 1000003) ^ hashCode4) * 1000003) ^ i;
    }

    @DexIgnore
    public String toString() {
        return "Application{identifier=" + this.a + ", version=" + this.b + ", displayVersion=" + this.c + ", organization=" + this.d + ", installationUuid=" + this.e + "}";
    }
}
