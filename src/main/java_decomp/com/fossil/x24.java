package com.fossil;

import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.Serializable;
import java.lang.Enum;
import java.util.Collection;
import java.util.EnumSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X24<E extends Enum<E>> extends H34<E> {
    @DexIgnore
    public /* final */ transient EnumSet<E> c;
    @DexIgnore
    @LazyInit
    public transient int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<E extends Enum<E>> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ EnumSet<E> delegate;

        @DexIgnore
        public Bi(EnumSet<E> enumSet) {
            this.delegate = enumSet;
        }

        @DexIgnore
        public Object readResolve() {
            return new X24(this.delegate.clone());
        }
    }

    @DexIgnore
    public X24(EnumSet<E> enumSet) {
        this.c = enumSet;
    }

    @DexIgnore
    public static H34 asImmutable(EnumSet enumSet) {
        int size = enumSet.size();
        return size != 0 ? size != 1 ? new X24(enumSet) : H34.of(O34.f(enumSet)) : H34.of();
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean contains(Object obj) {
        return this.c.contains(obj);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
    public boolean containsAll(Collection<?> collection) {
        if (collection instanceof X24) {
            collection = ((X24) collection).c;
        }
        return this.c.containsAll(collection);
    }

    @DexIgnore
    @Override // com.fossil.H34
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof X24) {
            obj = ((X24) obj).c;
        }
        return this.c.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.H34
    public int hashCode() {
        int i = this.d;
        if (i != 0) {
            return i;
        }
        int hashCode = this.c.hashCode();
        this.d = hashCode;
        return hashCode;
    }

    @DexIgnore
    public boolean isEmpty() {
        return this.c.isEmpty();
    }

    @DexIgnore
    @Override // com.fossil.H34
    public boolean isHashCodeFast() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.U24, com.fossil.U24, com.fossil.H34, com.fossil.H34, java.lang.Iterable
    public H54<E> iterator() {
        return P34.x(this.c.iterator());
    }

    @DexIgnore
    public int size() {
        return this.c.size();
    }

    @DexIgnore
    public String toString() {
        return this.c.toString();
    }

    @DexIgnore
    @Override // com.fossil.U24, com.fossil.H34
    public Object writeReplace() {
        return new Bi(this.c);
    }
}
