package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T70 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ D3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Object c;

    @DexIgnore
    public T70(D3 d3, Object obj) {
        this.b = d3;
        this.c = obj;
    }

    @DexIgnore
    public final void run() {
        this.b.b.o(this.c);
    }
}
