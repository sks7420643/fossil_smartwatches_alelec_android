package com.fossil;

import android.app.Dialog;
import com.google.android.gms.common.api.GoogleApiActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ta2 implements Runnable {
    @DexIgnore
    public /* final */ Qa2 b;
    @DexIgnore
    public /* final */ /* synthetic */ Ra2 c;

    @DexIgnore
    public Ta2(Ra2 ra2, Qa2 qa2) {
        this.c = ra2;
        this.b = qa2;
    }

    @DexIgnore
    public final void run() {
        if (this.c.c) {
            Z52 a2 = this.b.a();
            if (a2.k()) {
                Ra2 ra2 = this.c;
                ra2.b.startActivityForResult(GoogleApiActivity.b(ra2.b(), a2.h(), this.b.b(), false), 1);
            } else if (this.c.f.m(a2.c())) {
                Ra2 ra22 = this.c;
                ra22.f.A(ra22.b(), this.c.b, a2.c(), 2, this.c);
            } else if (a2.c() == 18) {
                Dialog u = C62.u(this.c.b(), this.c);
                Ra2 ra23 = this.c;
                ra23.f.w(ra23.b().getApplicationContext(), new Sa2(this, u));
            } else {
                this.c.m(a2, this.b.b());
            }
        }
    }
}
