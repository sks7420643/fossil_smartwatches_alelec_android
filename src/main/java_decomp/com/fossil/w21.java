package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class W21 {
    @DexIgnore
    public static W21 e;
    @DexIgnore
    public Q21 a;
    @DexIgnore
    public R21 b;
    @DexIgnore
    public U21 c;
    @DexIgnore
    public V21 d;

    @DexIgnore
    public W21(Context context, K41 k41) {
        Context applicationContext = context.getApplicationContext();
        this.a = new Q21(applicationContext, k41);
        this.b = new R21(applicationContext, k41);
        this.c = new U21(applicationContext, k41);
        this.d = new V21(applicationContext, k41);
    }

    @DexIgnore
    public static W21 c(Context context, K41 k41) {
        W21 w21;
        synchronized (W21.class) {
            try {
                if (e == null) {
                    e = new W21(context, k41);
                }
                w21 = e;
            } catch (Throwable th) {
                throw th;
            }
        }
        return w21;
    }

    @DexIgnore
    public Q21 a() {
        return this.a;
    }

    @DexIgnore
    public R21 b() {
        return this.b;
    }

    @DexIgnore
    public U21 d() {
        return this.c;
    }

    @DexIgnore
    public V21 e() {
        return this.d;
    }
}
