package com.fossil;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Build;
import java.lang.ref.WeakReference;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Qh0 extends ContextWrapper {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public static ArrayList<WeakReference<Qh0>> d;
    @DexIgnore
    public /* final */ Resources a;
    @DexIgnore
    public /* final */ Resources.Theme b;

    @DexIgnore
    public Qh0(Context context) {
        super(context);
        if (Yh0.b()) {
            Yh0 yh0 = new Yh0(this, context.getResources());
            this.a = yh0;
            Resources.Theme newTheme = yh0.newTheme();
            this.b = newTheme;
            newTheme.setTo(context.getTheme());
            return;
        }
        this.a = new Sh0(this, context.getResources());
        this.b = null;
    }

    @DexIgnore
    public static boolean a(Context context) {
        if ((context instanceof Qh0) || (context.getResources() instanceof Sh0) || (context.getResources() instanceof Yh0)) {
            return false;
        }
        return Build.VERSION.SDK_INT < 21 || Yh0.b();
    }

    @DexIgnore
    public static Context b(Context context) {
        if (!a(context)) {
            return context;
        }
        synchronized (c) {
            if (d == null) {
                d = new ArrayList<>();
            } else {
                for (int size = d.size() - 1; size >= 0; size--) {
                    WeakReference<Qh0> weakReference = d.get(size);
                    if (weakReference == null || weakReference.get() == null) {
                        d.remove(size);
                    }
                }
                for (int size2 = d.size() - 1; size2 >= 0; size2--) {
                    WeakReference<Qh0> weakReference2 = d.get(size2);
                    Qh0 qh0 = weakReference2 != null ? weakReference2.get() : null;
                    if (qh0 != null && qh0.getBaseContext() == context) {
                        return qh0;
                    }
                }
            }
            Qh0 qh02 = new Qh0(context);
            d.add(new WeakReference<>(qh02));
            return qh02;
        }
    }

    @DexIgnore
    public AssetManager getAssets() {
        return this.a.getAssets();
    }

    @DexIgnore
    public Resources getResources() {
        return this.a;
    }

    @DexIgnore
    public Resources.Theme getTheme() {
        Resources.Theme theme = this.b;
        return theme == null ? super.getTheme() : theme;
    }

    @DexIgnore
    public void setTheme(int i) {
        Resources.Theme theme = this.b;
        if (theme == null) {
            super.setTheme(i);
        } else {
            theme.applyStyle(i, true);
        }
    }
}
