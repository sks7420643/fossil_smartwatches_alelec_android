package com.fossil;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Li1 implements Ei1 {
    @DexIgnore
    public /* final */ Set<Qj1<?>> b; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    public void c() {
        this.b.clear();
    }

    @DexIgnore
    public List<Qj1<?>> e() {
        return Jk1.j(this.b);
    }

    @DexIgnore
    public void g(Qj1<?> qj1) {
        this.b.add(qj1);
    }

    @DexIgnore
    public void l(Qj1<?> qj1) {
        this.b.remove(qj1);
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onDestroy() {
        for (Qj1 qj1 : Jk1.j(this.b)) {
            qj1.onDestroy();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onStart() {
        for (Qj1 qj1 : Jk1.j(this.b)) {
            qj1.onStart();
        }
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onStop() {
        for (Qj1 qj1 : Jk1.j(this.b)) {
            qj1.onStop();
        }
    }
}
