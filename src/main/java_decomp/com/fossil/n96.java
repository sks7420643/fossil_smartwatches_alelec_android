package com.fossil;

import com.mapped.EditPhotoFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N96 implements MembersInjector<EditPhotoFragment> {
    @DexIgnore
    public static void a(EditPhotoFragment editPhotoFragment, Po4 po4) {
        editPhotoFragment.s = po4;
    }
}
