package com.fossil;

import android.content.ComponentName;
import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ki0 {
    @DexIgnore
    public abstract IBinder a();

    @DexIgnore
    public abstract ComponentName b();
}
