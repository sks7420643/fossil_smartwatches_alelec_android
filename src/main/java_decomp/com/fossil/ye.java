package com.fossil;

import com.mapped.Cd6;
import com.mapped.Coroutine;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ye extends Qq7 implements Coroutine<Fs, Float, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Bi b;
    @DexIgnore
    public /* final */ /* synthetic */ J0 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Ye(Bi bi, J0 j0) {
        super(2);
        this.b = bi;
        this.c = j0;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public Cd6 invoke(Fs fs, Float f) {
        float floatValue = f.floatValue();
        if (this.b.J > 0) {
            Bi bi = this.b;
            floatValue = ((floatValue * ((float) (this.c.g - this.b.N))) + ((float) (bi.K + bi.N))) / ((float) this.b.J);
        }
        if (Math.abs(floatValue - this.b.L) > this.b.U || floatValue == 1.0f) {
            Bi bi2 = this.b;
            bi2.L = floatValue;
            bi2.d(floatValue);
        }
        return Cd6.a;
    }
}
