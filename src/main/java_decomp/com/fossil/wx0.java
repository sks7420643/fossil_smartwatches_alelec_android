package com.fossil;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wx0 extends Drawable implements Animatable {
    @DexIgnore
    public static /* final */ Interpolator h; // = new LinearInterpolator();
    @DexIgnore
    public static /* final */ Interpolator i; // = new Er0();
    @DexIgnore
    public static /* final */ int[] j; // = {-16777216};
    @DexIgnore
    public /* final */ Ci b;
    @DexIgnore
    public float c;
    @DexIgnore
    public Resources d;
    @DexIgnore
    public Animator e;
    @DexIgnore
    public float f;
    @DexIgnore
    public boolean g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ci a;

        @DexIgnore
        public Ai(Ci ci) {
            this.a = ci;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            Wx0.this.n(floatValue, this.a);
            Wx0.this.b(floatValue, this.a, false);
            Wx0.this.invalidateSelf();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Animator.AnimatorListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ci a;

        @DexIgnore
        public Bi(Ci ci) {
            this.a = ci;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            Wx0.this.b(1.0f, this.a, true);
            this.a.A();
            this.a.l();
            Wx0 wx0 = Wx0.this;
            if (wx0.g) {
                wx0.g = false;
                animator.cancel();
                animator.setDuration(1332);
                animator.start();
                this.a.x(false);
                return;
            }
            wx0.f += 1.0f;
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            Wx0.this.f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public /* final */ RectF a; // = new RectF();
        @DexIgnore
        public /* final */ Paint b; // = new Paint();
        @DexIgnore
        public /* final */ Paint c; // = new Paint();
        @DexIgnore
        public /* final */ Paint d; // = new Paint();
        @DexIgnore
        public float e; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float f; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float g; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        @DexIgnore
        public float h; // = 5.0f;
        @DexIgnore
        public int[] i;
        @DexIgnore
        public int j;
        @DexIgnore
        public float k;
        @DexIgnore
        public float l;
        @DexIgnore
        public float m;
        @DexIgnore
        public boolean n;
        @DexIgnore
        public Path o;
        @DexIgnore
        public float p; // = 1.0f;
        @DexIgnore
        public float q;
        @DexIgnore
        public int r;
        @DexIgnore
        public int s;
        @DexIgnore
        public int t; // = 255;
        @DexIgnore
        public int u;

        @DexIgnore
        public Ci() {
            this.b.setStrokeCap(Paint.Cap.SQUARE);
            this.b.setAntiAlias(true);
            this.b.setStyle(Paint.Style.STROKE);
            this.c.setStyle(Paint.Style.FILL);
            this.c.setAntiAlias(true);
            this.d.setColor(0);
        }

        @DexIgnore
        public void A() {
            this.k = this.e;
            this.l = this.f;
            this.m = this.g;
        }

        @DexIgnore
        public void a(Canvas canvas, Rect rect) {
            RectF rectF = this.a;
            float f2 = this.q;
            float f3 = (this.h / 2.0f) + f2;
            if (f2 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                f3 = (((float) Math.min(rect.width(), rect.height())) / 2.0f) - Math.max((((float) this.r) * this.p) / 2.0f, this.h / 2.0f);
            }
            rectF.set(((float) rect.centerX()) - f3, ((float) rect.centerY()) - f3, ((float) rect.centerX()) + f3, f3 + ((float) rect.centerY()));
            float f4 = this.e;
            float f5 = this.g;
            float f6 = (f4 + f5) * 360.0f;
            float f7 = ((this.f + f5) * 360.0f) - f6;
            this.b.setColor(this.u);
            this.b.setAlpha(this.t);
            float f8 = this.h / 2.0f;
            rectF.inset(f8, f8);
            canvas.drawCircle(rectF.centerX(), rectF.centerY(), rectF.width() / 2.0f, this.d);
            float f9 = -f8;
            rectF.inset(f9, f9);
            canvas.drawArc(rectF, f6, f7, false, this.b);
            b(canvas, f6, f7, rectF);
        }

        @DexIgnore
        public void b(Canvas canvas, float f2, float f3, RectF rectF) {
            if (this.n) {
                Path path = this.o;
                if (path == null) {
                    Path path2 = new Path();
                    this.o = path2;
                    path2.setFillType(Path.FillType.EVEN_ODD);
                } else {
                    path.reset();
                }
                this.o.moveTo(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.o.lineTo(((float) this.r) * this.p, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                Path path3 = this.o;
                float f4 = this.p;
                path3.lineTo((((float) this.r) * f4) / 2.0f, f4 * ((float) this.s));
                this.o.offset(((Math.min(rectF.width(), rectF.height()) / 2.0f) + rectF.centerX()) - ((((float) this.r) * this.p) / 2.0f), rectF.centerY() + (this.h / 2.0f));
                this.o.close();
                this.c.setColor(this.u);
                this.c.setAlpha(this.t);
                canvas.save();
                canvas.rotate(f2 + f3, rectF.centerX(), rectF.centerY());
                canvas.drawPath(this.o, this.c);
                canvas.restore();
            }
        }

        @DexIgnore
        public int c() {
            return this.t;
        }

        @DexIgnore
        public float d() {
            return this.f;
        }

        @DexIgnore
        public int e() {
            return this.i[f()];
        }

        @DexIgnore
        public int f() {
            return (this.j + 1) % this.i.length;
        }

        @DexIgnore
        public float g() {
            return this.e;
        }

        @DexIgnore
        public int h() {
            return this.i[this.j];
        }

        @DexIgnore
        public float i() {
            return this.l;
        }

        @DexIgnore
        public float j() {
            return this.m;
        }

        @DexIgnore
        public float k() {
            return this.k;
        }

        @DexIgnore
        public void l() {
            t(f());
        }

        @DexIgnore
        public void m() {
            this.k = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.l = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            this.m = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            y(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            v(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            w(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }

        @DexIgnore
        public void n(int i2) {
            this.t = i2;
        }

        @DexIgnore
        public void o(float f2, float f3) {
            this.r = (int) f2;
            this.s = (int) f3;
        }

        @DexIgnore
        public void p(float f2) {
            if (f2 != this.p) {
                this.p = f2;
            }
        }

        @DexIgnore
        public void q(float f2) {
            this.q = f2;
        }

        @DexIgnore
        public void r(int i2) {
            this.u = i2;
        }

        @DexIgnore
        public void s(ColorFilter colorFilter) {
            this.b.setColorFilter(colorFilter);
        }

        @DexIgnore
        public void t(int i2) {
            this.j = i2;
            this.u = this.i[i2];
        }

        @DexIgnore
        public void u(int[] iArr) {
            this.i = iArr;
            t(0);
        }

        @DexIgnore
        public void v(float f2) {
            this.f = f2;
        }

        @DexIgnore
        public void w(float f2) {
            this.g = f2;
        }

        @DexIgnore
        public void x(boolean z) {
            if (this.n != z) {
                this.n = z;
            }
        }

        @DexIgnore
        public void y(float f2) {
            this.e = f2;
        }

        @DexIgnore
        public void z(float f2) {
            this.h = f2;
            this.b.setStrokeWidth(f2);
        }
    }

    @DexIgnore
    public Wx0(Context context) {
        Pn0.d(context);
        this.d = context.getResources();
        Ci ci = new Ci();
        this.b = ci;
        ci.u(j);
        k(2.5f);
        m();
    }

    @DexIgnore
    public final void a(float f2, Ci ci) {
        n(f2, ci);
        ci.y(ci.k() + (((ci.i() - 0.01f) - ci.k()) * f2));
        ci.v(ci.i());
        ci.w(((((float) (Math.floor((double) (ci.j() / 0.8f)) + 1.0d)) - ci.j()) * f2) + ci.j());
    }

    @DexIgnore
    public void b(float f2, Ci ci, boolean z) {
        float k;
        float interpolation;
        if (this.g) {
            a(f2, ci);
        } else if (f2 != 1.0f || z) {
            float j2 = ci.j();
            if (f2 < 0.5f) {
                interpolation = ci.k();
                k = (i.getInterpolation(f2 / 0.5f) * 0.79f) + 0.01f + interpolation;
            } else {
                k = ci.k() + 0.79f;
                interpolation = k - (((1.0f - i.getInterpolation((f2 - 0.5f) / 0.5f)) * 0.79f) + 0.01f);
            }
            float f3 = this.f;
            ci.y(interpolation);
            ci.v(k);
            ci.w((0.20999998f * f2) + j2);
            h((f2 + f3) * 216.0f);
        }
    }

    @DexIgnore
    public final int c(float f2, int i2, int i3) {
        int i4 = (i2 >> 24) & 255;
        int i5 = (i2 >> 16) & 255;
        int i6 = (i2 >> 8) & 255;
        int i7 = i2 & 255;
        return ((i4 + ((int) (((float) (((i3 >> 24) & 255) - i4)) * f2))) << 24) | ((i5 + ((int) (((float) (((i3 >> 16) & 255) - i5)) * f2))) << 16) | ((((int) (((float) (((i3 >> 8) & 255) - i6)) * f2)) + i6) << 8) | (((int) (((float) ((i3 & 255) - i7)) * f2)) + i7);
    }

    @DexIgnore
    public void d(boolean z) {
        this.b.x(z);
        invalidateSelf();
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        Rect bounds = getBounds();
        canvas.save();
        canvas.rotate(this.c, bounds.exactCenterX(), bounds.exactCenterY());
        this.b.a(canvas, bounds);
        canvas.restore();
    }

    @DexIgnore
    public void e(float f2) {
        this.b.p(f2);
        invalidateSelf();
    }

    @DexIgnore
    public void f(int... iArr) {
        this.b.u(iArr);
        this.b.t(0);
        invalidateSelf();
    }

    @DexIgnore
    public void g(float f2) {
        this.b.w(f2);
        invalidateSelf();
    }

    @DexIgnore
    public int getAlpha() {
        return this.b.c();
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public final void h(float f2) {
        this.c = f2;
    }

    @DexIgnore
    public final void i(float f2, float f3, float f4, float f5) {
        Ci ci = this.b;
        float f6 = this.d.getDisplayMetrics().density;
        ci.z(f3 * f6);
        ci.q(f2 * f6);
        ci.t(0);
        ci.o(f4 * f6, f6 * f5);
    }

    @DexIgnore
    public boolean isRunning() {
        return this.e.isRunning();
    }

    @DexIgnore
    public void j(float f2, float f3) {
        this.b.y(f2);
        this.b.v(f3);
        invalidateSelf();
    }

    @DexIgnore
    public void k(float f2) {
        this.b.z(f2);
        invalidateSelf();
    }

    @DexIgnore
    public void l(int i2) {
        if (i2 == 0) {
            i(11.0f, 3.0f, 12.0f, 6.0f);
        } else {
            i(7.5f, 2.5f, 10.0f, 5.0f);
        }
        invalidateSelf();
    }

    @DexIgnore
    public final void m() {
        Ci ci = this.b;
        ValueAnimator ofFloat = ValueAnimator.ofFloat(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
        ofFloat.addUpdateListener(new Ai(ci));
        ofFloat.setRepeatCount(-1);
        ofFloat.setRepeatMode(1);
        ofFloat.setInterpolator(h);
        ofFloat.addListener(new Bi(ci));
        this.e = ofFloat;
    }

    @DexIgnore
    public void n(float f2, Ci ci) {
        if (f2 > 0.75f) {
            ci.r(c((f2 - 0.75f) / 0.25f, ci.h(), ci.e()));
        } else {
            ci.r(ci.h());
        }
    }

    @DexIgnore
    public void setAlpha(int i2) {
        this.b.n(i2);
        invalidateSelf();
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.b.s(colorFilter);
        invalidateSelf();
    }

    @DexIgnore
    public void start() {
        this.e.cancel();
        this.b.A();
        if (this.b.d() != this.b.g()) {
            this.g = true;
            this.e.setDuration(666);
            this.e.start();
            return;
        }
        this.b.t(0);
        this.b.m();
        this.e.setDuration(1332);
        this.e.start();
    }

    @DexIgnore
    public void stop() {
        this.e.cancel();
        h(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.b.x(false);
        this.b.t(0);
        this.b.m();
        invalidateSelf();
    }
}
