package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.mapped.AlertDialogFragment;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.TimeUtils;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardActivity;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardViewModel;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fw4 extends BaseFragment implements AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ Ai t; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public BCOverviewLeaderBoardViewModel h;
    @DexIgnore
    public G37<T95> i;
    @DexIgnore
    public Ps4 j;
    @DexIgnore
    public String k;
    @DexIgnore
    public Kv4 l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Fw4.s;
        }

        @DexIgnore
        public final Fw4 b(Ps4 ps4, String str) {
            Fw4 fw4 = new Fw4();
            Bundle bundle = new Bundle();
            bundle.putParcelable("challenge_extra", ps4);
            bundle.putString("challenge_history_id_extra", str);
            fw4.setArguments(bundle);
            return fw4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Fw4 b;

        @DexIgnore
        public Bi(Fw4 fw4) {
            this.b = fw4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.onBackPressed();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Qq7 implements Hg6<View, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Fw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Fw4 fw4) {
            super(1);
            this.this$0 = fw4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(View view) {
            invoke(view);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            this.this$0.T6().E();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ Fw4 a;

        @DexIgnore
        public Di(Fw4 fw4) {
            this.a = fw4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView;
            T95 a2 = this.a.S6().a();
            if (!(a2 == null || (flexibleTextView = a2.t) == null)) {
                flexibleTextView.setVisibility(8);
            }
            this.a.T6().G();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<Object> {
        @DexIgnore
        public static /* final */ Ei a; // = new Ei();

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(Object obj) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<Lc6<? extends Boolean, ? extends Boolean>> {
        @DexIgnore
        public /* final */ /* synthetic */ Fw4 a;

        @DexIgnore
        public Fi(Fw4 fw4) {
            this.a = fw4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, Boolean> lc6) {
            SwipeRefreshLayout swipeRefreshLayout;
            Boolean first = lc6.getFirst();
            if (first != null) {
                boolean booleanValue = first.booleanValue();
                T95 a2 = this.a.S6().a();
                if (!(a2 == null || (swipeRefreshLayout = a2.z) == null)) {
                    swipeRefreshLayout.setRefreshing(booleanValue);
                }
            }
            Boolean second = lc6.getSecond();
            if (second == null) {
                return;
            }
            if (second.booleanValue()) {
                this.a.b();
            } else {
                this.a.a();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends Boolean> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> implements Ls0<Ps4> {
        @DexIgnore
        public /* final */ /* synthetic */ Fw4 a;

        @DexIgnore
        public Gi(Fw4 fw4) {
            this.a = fw4;
        }

        @DexIgnore
        public final void a(Ps4 ps4) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Fw4.t.a();
            local.e(a2, "challengeLive - oldChallenge: " + this.a.j + " - new : " + ps4);
            this.a.j = ps4;
            Fw4 fw4 = this.a;
            Wg6.b(ps4, "it");
            fw4.Z6(ps4);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Ps4 ps4) {
            a(ps4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> implements Ls0<List<? extends Object>> {
        @DexIgnore
        public /* final */ /* synthetic */ Fw4 a;

        @DexIgnore
        public Hi(Fw4 fw4) {
            this.a = fw4;
        }

        @DexIgnore
        public final void a(List<? extends Object> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Fw4.t.a();
            local.e(a2, "highlightPlayersLive - " + list);
            Kv4 L6 = Fw4.L6(this.a);
            Wg6.b(list, "it");
            L6.g(list);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends Object> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii<T> implements Ls0<Lc6<? extends Integer, ? extends Ms4>> {
        @DexIgnore
        public /* final */ /* synthetic */ Fw4 a;

        @DexIgnore
        public Ii(Fw4 fw4) {
            this.a = fw4;
        }

        @DexIgnore
        public final void a(Lc6<Integer, Ms4> lc6) {
            Integer q;
            String str = null;
            int intValue = lc6.getFirst().intValue();
            Integer h = lc6.getSecond().h();
            int intValue2 = h != null ? h.intValue() : 0;
            Hr7 hr7 = Hr7.a;
            String c = Um5.c(PortfolioApp.get.instance(), 2131886316);
            Wg6.b(c, "LanguageHelper.getString\u2026urrentRankNumberInNumber)");
            String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(intValue2), Integer.valueOf(intValue)}, 2));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Fw4.t.a();
            local.e(a2, "current - rank: " + intValue2 + " - numberOfPlayers: " + intValue);
            T95 a3 = this.a.S6().a();
            if (a3 != null) {
                FlexibleTextView flexibleTextView = a3.v;
                Wg6.b(flexibleTextView, "ftvRank");
                flexibleTextView.setText(Jl5.b(Jl5.b, String.valueOf(intValue2), format, 0, 4, null));
                Ps4 ps4 = this.a.j;
                if (ps4 != null) {
                    str = ps4.r();
                }
                if (Wg6.a("activity_reach_goal", str)) {
                    Ps4 ps42 = this.a.j;
                    int intValue3 = (ps42 == null || (q = ps42.q()) == null) ? 0 : q.intValue();
                    Integer n = lc6.getSecond().n();
                    int intValue4 = n != null ? n.intValue() : 0;
                    Hr7 hr72 = Hr7.a;
                    String c2 = Um5.c(PortfolioApp.get.instance(), 2131887313);
                    Wg6.b(c2, "LanguageHelper.getString\u2026y_challenge_slash_format)");
                    String format2 = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf(intValue3 - intValue4), Integer.valueOf(intValue3)}, 2));
                    Wg6.b(format2, "java.lang.String.format(format, *args)");
                    TimerTextView timerTextView = a3.r;
                    Wg6.b(timerTextView, "ftvCountDown");
                    timerTextView.setTag(Integer.valueOf(intValue3));
                    TimerTextView timerTextView2 = a3.r;
                    Wg6.b(timerTextView2, "ftvCountDown");
                    timerTextView2.setText(format2);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Integer, ? extends Ms4> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji<T> implements Ls0<Lc6<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ Fw4 a;

        @DexIgnore
        public Ji(Fw4 fw4) {
            this.a = fw4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, ? extends ServerError> lc6) {
            String str = null;
            if (lc6.getFirst().booleanValue()) {
                Cl5.c.e();
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            S37 s37 = S37.c;
            ServerError serverError = (ServerError) lc6.getSecond();
            Integer code = serverError != null ? serverError.getCode() : null;
            ServerError serverError2 = (ServerError) lc6.getSecond();
            if (serverError2 != null) {
                str = serverError2.getMessage();
            }
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.g(code, str, childFragmentManager);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends ServerError> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki<T> implements Ls0<Gl7<? extends Boolean, ? extends Boolean, ? extends Boolean>> {
        @DexIgnore
        public /* final */ /* synthetic */ Fw4 a;

        @DexIgnore
        public Ki(Fw4 fw4) {
            this.a = fw4;
        }

        @DexIgnore
        public final void a(Gl7<Boolean, Boolean, Boolean> gl7) {
            boolean booleanValue = gl7.getFirst().booleanValue();
            boolean booleanValue2 = gl7.getSecond().booleanValue();
            boolean booleanValue3 = gl7.getThird().booleanValue();
            if (booleanValue) {
                this.a.X6();
            } else if (booleanValue2) {
                Fw4 fw4 = this.a;
                Ps4 ps4 = fw4.j;
                String f = ps4 != null ? ps4.f() : null;
                if (f != null) {
                    fw4.Y6(f);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else if (booleanValue3) {
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                s37.h(childFragmentManager);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Gl7<? extends Boolean, ? extends Boolean, ? extends Boolean> gl7) {
            a(gl7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li<T> implements Ls0<List<? extends Gs4>> {
        @DexIgnore
        public /* final */ /* synthetic */ Fw4 a;

        @DexIgnore
        public Li(Fw4 fw4) {
            this.a = fw4;
        }

        @DexIgnore
        public final void a(List<Gs4> list) {
            Fw4 fw4 = this.a;
            Wg6.b(list, "it");
            fw4.W6(list);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends Gs4> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi<T> implements Ls0<Lc6<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ Fw4 a;

        @DexIgnore
        public Mi(Fw4 fw4) {
            this.a = fw4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, ? extends ServerError> lc6) {
            T95 a2 = this.a.S6().a();
            if (a2 == null) {
                return;
            }
            if (lc6.getFirst().booleanValue()) {
                FlexibleTextView flexibleTextView = a2.t;
                Wg6.b(flexibleTextView, "ftvError");
                flexibleTextView.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView2 = a2.t;
            Wg6.b(flexibleTextView2, "ftvError");
            String c = Um5.c(flexibleTextView2.getContext(), 2131886231);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                Toast.makeText(activity, c, 0).show();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends ServerError> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni<T> implements Ls0<Gl7<? extends String, ? extends String, ? extends Integer>> {
        @DexIgnore
        public static /* final */ Ni a; // = new Ni();

        @DexIgnore
        public final void a(Gl7<String, String, Integer> gl7) {
            Xr4.a.h("bc_left_challenge_after_start", gl7.getFirst(), gl7.getSecond(), gl7.getThird().intValue(), PortfolioApp.get.instance());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Gl7<? extends String, ? extends String, ? extends Integer> gl7) {
            a(gl7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi implements My5 {
        @DexIgnore
        public /* final */ /* synthetic */ Fw4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Oi(Fw4 fw4) {
            this.a = fw4;
        }

        @DexIgnore
        @Override // com.fossil.My5
        public void a(Gs4 gs4) {
            Wg6.c(gs4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            Object a2 = gs4.a();
            if (!(a2 instanceof Vy4)) {
                a2 = null;
            }
            Vy4 vy4 = (Vy4) a2;
            if (vy4 != null) {
                this.a.T6().F(vy4);
            }
        }
    }

    /*
    static {
        String simpleName = Fw4.class.getSimpleName();
        Wg6.b(simpleName, "BCOverviewLeaderBoardFra\u2026nt::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ Kv4 L6(Fw4 fw4) {
        Kv4 kv4 = fw4.l;
        if (kv4 != null) {
            return kv4;
        }
        Wg6.n("leaderBoardAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        if (str.hashCode() == 1970588827 && str.equals("LEAVE_CHALLENGE") && i2 == 2131363373) {
            BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel = this.h;
            if (bCOverviewLeaderBoardViewModel != null) {
                bCOverviewLeaderBoardViewModel.D();
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final G37<T95> S6() {
        G37<T95> g37 = this.i;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final BCOverviewLeaderBoardViewModel T6() {
        BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel = this.h;
        if (bCOverviewLeaderBoardViewModel != null) {
            return bCOverviewLeaderBoardViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void U6() {
        Date date;
        int i2;
        Integer q;
        G37<T95> g37 = this.i;
        if (g37 != null) {
            T95 a2 = g37.a();
            if (a2 != null) {
                a2.x.setOnClickListener(new Bi(this));
                ImageView imageView = a2.w;
                Wg6.b(imageView, "imgOption");
                Fz4.a(imageView, new Ci(this));
                a2.z.setOnRefreshListener(new Di(this));
                Ps4 ps4 = this.j;
                if (ps4 == null || (date = ps4.m()) == null) {
                    date = new Date();
                }
                Ps4 ps42 = this.j;
                if (Wg6.a("activity_reach_goal", ps42 != null ? ps42.r() : null)) {
                    Ps4 ps43 = this.j;
                    i2 = (ps43 == null || (q = ps43.q()) == null) ? 15000 : q.intValue();
                } else {
                    i2 = -1;
                }
                this.l = new Kv4(date.getTime(), i2);
                RecyclerView recyclerView = a2.y;
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                Kv4 kv4 = this.l;
                if (kv4 != null) {
                    recyclerView.setAdapter(kv4);
                } else {
                    Wg6.n("leaderBoardAdapter");
                    throw null;
                }
            }
        } else {
            Wg6.n("binding");
            throw null;
        }
    }

    @DexIgnore
    public final void V6() {
        BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel = this.h;
        if (bCOverviewLeaderBoardViewModel != null) {
            bCOverviewLeaderBoardViewModel.y().h(getViewLifecycleOwner(), new Fi(this));
            BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel2 = this.h;
            if (bCOverviewLeaderBoardViewModel2 != null) {
                bCOverviewLeaderBoardViewModel2.r().h(getViewLifecycleOwner(), new Gi(this));
                BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel3 = this.h;
                if (bCOverviewLeaderBoardViewModel3 != null) {
                    bCOverviewLeaderBoardViewModel3.u().h(getViewLifecycleOwner(), new Hi(this));
                    BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel4 = this.h;
                    if (bCOverviewLeaderBoardViewModel4 != null) {
                        bCOverviewLeaderBoardViewModel4.s().h(getViewLifecycleOwner(), new Ii(this));
                        BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel5 = this.h;
                        if (bCOverviewLeaderBoardViewModel5 != null) {
                            bCOverviewLeaderBoardViewModel5.x().h(getViewLifecycleOwner(), new Ji(this));
                            BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel6 = this.h;
                            if (bCOverviewLeaderBoardViewModel6 != null) {
                                bCOverviewLeaderBoardViewModel6.A().h(getViewLifecycleOwner(), new Ki(this));
                                BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel7 = this.h;
                                if (bCOverviewLeaderBoardViewModel7 != null) {
                                    bCOverviewLeaderBoardViewModel7.z().h(getViewLifecycleOwner(), new Li(this));
                                    BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel8 = this.h;
                                    if (bCOverviewLeaderBoardViewModel8 != null) {
                                        bCOverviewLeaderBoardViewModel8.t().h(getViewLifecycleOwner(), new Mi(this));
                                        BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel9 = this.h;
                                        if (bCOverviewLeaderBoardViewModel9 != null) {
                                            bCOverviewLeaderBoardViewModel9.w().h(getViewLifecycleOwner(), Ni.a);
                                            BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel10 = this.h;
                                            if (bCOverviewLeaderBoardViewModel10 != null) {
                                                LiveData<Object> v = bCOverviewLeaderBoardViewModel10.v();
                                                if (v != null) {
                                                    v.h(getViewLifecycleOwner(), Ei.a);
                                                    return;
                                                }
                                                return;
                                            }
                                            Wg6.n("viewModel");
                                            throw null;
                                        }
                                        Wg6.n("viewModel");
                                        throw null;
                                    }
                                    Wg6.n("viewModel");
                                    throw null;
                                }
                                Wg6.n("viewModel");
                                throw null;
                            }
                            Wg6.n("viewModel");
                            throw null;
                        }
                        Wg6.n("viewModel");
                        throw null;
                    }
                    Wg6.n("viewModel");
                    throw null;
                }
                Wg6.n("viewModel");
                throw null;
            }
            Wg6.n("viewModel");
            throw null;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void W6(List<Gs4> list) {
        Es4 b = Es4.A.b();
        String c = Um5.c(requireContext(), 2131886322);
        Wg6.b(c, "LanguageHelper.getString\u2026rBoard_Menu_Title__Allow)");
        b.setTitle(c);
        b.E6(list);
        b.G6(new Oi(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        b.show(childFragmentManager, Es4.A.a());
    }

    @DexIgnore
    public final void X6() {
        BCWaitingChallengeDetailActivity.B.a(this, this.j, "joined_challenge", -1, true);
    }

    @DexIgnore
    public final void Y6(String str) {
        Date date;
        int i2;
        Integer q;
        Ps4 ps4 = this.j;
        if (ps4 == null || (date = ps4.m()) == null) {
            date = new Date();
        }
        Ps4 ps42 = this.j;
        if (Wg6.a("activity_reach_goal", ps42 != null ? ps42.r() : null)) {
            Ps4 ps43 = this.j;
            i2 = (ps43 == null || (q = ps43.q()) == null) ? 15000 : q.intValue();
        } else {
            i2 = -1;
        }
        Ps4 ps44 = this.j;
        BCLeaderBoardActivity.A.a(this, str, date.getTime(), i2, ps44 != null ? ps44.n() : null);
    }

    @DexIgnore
    public final void Z6(Ps4 ps4) {
        String str;
        String a2;
        FLogger.INSTANCE.getLocal().e(s, "updateView - challenge: " + ps4);
        G37<T95> g37 = this.i;
        if (g37 != null) {
            T95 a3 = g37.a();
            if (a3 != null) {
                FlexibleTextView flexibleTextView = a3.A;
                Wg6.b(flexibleTextView, "tvTitle");
                flexibleTextView.setText(ps4.g());
                String r = ps4.r();
                if (r != null) {
                    int hashCode = r.hashCode();
                    if (hashCode != -1348781656) {
                        if (hashCode == -637042289 && r.equals("activity_reach_goal")) {
                            a3.r.setTextColor(W6.d(PortfolioApp.get.instance(), 2131099689));
                            Integer q = ps4.q();
                            int intValue = q != null ? q.intValue() : 0;
                            TimerTextView timerTextView = a3.r;
                            Wg6.b(timerTextView, "ftvCountDown");
                            Object tag = timerTextView.getTag();
                            if (!(tag instanceof Integer)) {
                                tag = null;
                            }
                            if (((Integer) tag) == null) {
                                TimerTextView timerTextView2 = a3.r;
                                Wg6.b(timerTextView2, "ftvCountDown");
                                timerTextView2.setTag(ps4.q());
                                Hr7 hr7 = Hr7.a;
                                String c = Um5.c(PortfolioApp.get.instance(), 2131887313);
                                Wg6.b(c, "LanguageHelper.getString\u2026y_challenge_slash_format)");
                                String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(intValue), Integer.valueOf(intValue)}, 2));
                                Wg6.b(format, "java.lang.String.format(format, *args)");
                                TimerTextView timerTextView3 = a3.r;
                                Wg6.b(timerTextView3, "ftvCountDown");
                                timerTextView3.setText(format);
                            }
                            TimerTextView timerTextView4 = a3.u;
                            Wg6.b(timerTextView4, "ftvLeft");
                            timerTextView4.setVisibility(0);
                        }
                    } else if (r.equals("activity_best_result")) {
                        a3.r.setTextColor(W6.d(PortfolioApp.get.instance(), 2131099677));
                        if (Wg6.a("completed", ps4.n())) {
                            TimerTextView timerTextView5 = a3.r;
                            Wg6.b(timerTextView5, "ftvCountDown");
                            timerTextView5.setText(Um5.c(PortfolioApp.get.instance(), 2131887316));
                        } else {
                            TimerTextView timerTextView6 = a3.r;
                            Date e = ps4.e();
                            timerTextView6.setTime(e != null ? e.getTime() : 0);
                            a3.r.setDisplayType(Wy4.HOUR_MIN_SEC);
                        }
                    }
                }
                String l0 = PortfolioApp.get.instance().l0();
                Ht4 i2 = ps4.i();
                if (Wg6.a(l0, i2 != null ? i2.b() : null)) {
                    a2 = Um5.c(PortfolioApp.get.instance(), 2131886250);
                } else {
                    Hz4 hz4 = Hz4.a;
                    Ht4 i3 = ps4.i();
                    String a4 = i3 != null ? i3.a() : null;
                    Ht4 i4 = ps4.i();
                    String c2 = i4 != null ? i4.c() : null;
                    Ht4 i5 = ps4.i();
                    if (i5 == null || (str = i5.d()) == null) {
                        str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                    }
                    a2 = hz4.a(a4, c2, str);
                }
                Date b = ps4.b();
                if (b != null) {
                    String c3 = TimeUtils.c(b);
                    Hr7 hr72 = Hr7.a;
                    String c4 = Um5.c(PortfolioApp.get.instance(), 2131886312);
                    Wg6.b(c4, "LanguageHelper.getString\u2026bel__CreatedByNameOnDate)");
                    String format2 = String.format(c4, Arrays.copyOf(new Object[]{a2, c3}, 2));
                    Wg6.b(format2, "java.lang.String.format(format, *args)");
                    FlexibleTextView flexibleTextView2 = a3.s;
                    Wg6.b(flexibleTextView2, "ftvCreated");
                    Jl5 jl5 = Jl5.b;
                    Wg6.b(a2, Constants.PROFILE_KEY_LAST_NAME);
                    flexibleTextView2.setText(Jl5.b(jl5, a2, format2, 0, 4, null));
                    return;
                }
                Wg6.i();
                throw null;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        FragmentActivity activity;
        super.onActivityResult(i2, i3, intent);
        if (i2 == 15 && i3 == -1 && (activity = getActivity()) != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        this.j = arguments != null ? (Ps4) arguments.getParcelable("challenge_extra") : null;
        Bundle arguments2 = getArguments();
        this.k = arguments2 != null ? arguments2.getString("challenge_history_id_extra") : null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.e(str, "onCreate - challenge: " + this.j + " - historyId: " + this.k);
        PortfolioApp.get.instance().getIface().E0().a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCOverviewLeaderBoardViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
            this.h = (BCOverviewLeaderBoardViewModel) a2;
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        T95 t95 = (T95) Aq0.f(layoutInflater, 2131558600, viewGroup, false, A6());
        this.i = new G37<>(this, t95);
        Wg6.b(t95, "binding");
        return t95.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        if (this.k == null) {
            BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel = this.h;
            if (bCOverviewLeaderBoardViewModel != null) {
                bCOverviewLeaderBoardViewModel.G();
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        }
        AnalyticsHelper g2 = AnalyticsHelper.f.g();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            g2.m("bc_leader_board", activity);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        BCOverviewLeaderBoardViewModel bCOverviewLeaderBoardViewModel = this.h;
        if (bCOverviewLeaderBoardViewModel != null) {
            bCOverviewLeaderBoardViewModel.B(this.j, this.k);
            U6();
            V6();
            Ps4 ps4 = this.j;
            if (ps4 != null) {
                Z6(ps4);
                return;
            }
            return;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
