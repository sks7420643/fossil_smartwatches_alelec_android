package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L36 implements MembersInjector<RemindTimePresenter> {
    @DexIgnore
    public static void a(RemindTimePresenter remindTimePresenter) {
        remindTimePresenter.u();
    }
}
