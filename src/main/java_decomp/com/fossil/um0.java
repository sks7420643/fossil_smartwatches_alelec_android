package com.fossil;

import android.os.LocaleList;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Um0 implements Tm0 {
    @DexIgnore
    public /* final */ LocaleList a;

    @DexIgnore
    public Um0(LocaleList localeList) {
        this.a = localeList;
    }

    @DexIgnore
    @Override // com.fossil.Tm0
    public Object a() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this.a.equals(((Tm0) obj).a());
    }

    @DexIgnore
    @Override // com.fossil.Tm0
    public Locale get(int i) {
        return this.a.get(i);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }
}
