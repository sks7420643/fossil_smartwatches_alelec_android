package com.fossil;

import com.fossil.Tq4;
import com.mapped.Cd6;
import com.mapped.Hx5;
import com.mapped.Ku3;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import java.net.SocketTimeoutException;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nu5 extends Tq4<Bi, Di, Ci> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ Ai f; // = new Ai(null);
    @DexIgnore
    public /* final */ AuthApiUserService d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Nu5.e;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Tq4.Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public Bi(String str, String str2) {
            Wg6.c(str, "oldPass");
            Wg6.c(str2, "newPass");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Tq4.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public Ci(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Tq4.Ci {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends Sq5<Ku3> {
        @DexIgnore
        public /* final */ /* synthetic */ Nu5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei(Nu5 nu5) {
            this.a = nu5;
        }

        @DexIgnore
        @Override // com.fossil.Sq5
        public void a(Call<Ku3> call, ServerError serverError) {
            Wg6.c(call, "call");
            Wg6.c(serverError, "response");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Nu5.f.a();
            local.d(a2, "changePassword onErrorResponse response=" + serverError.getUserMessage());
            Tq4.Di b = this.a.b();
            Integer code = serverError.getCode();
            Wg6.b(code, "response.code");
            b.a(new Ci(code.intValue(), serverError.getUserMessage()));
        }

        @DexIgnore
        @Override // com.fossil.Sq5
        public void b(Call<Ku3> call, Throwable th) {
            Wg6.c(call, "call");
            Wg6.c(th, "t");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Nu5.f.a();
            StringBuilder sb = new StringBuilder();
            sb.append("changePassword onFail throwable=");
            th.printStackTrace();
            sb.append(Cd6.a);
            local.d(a2, sb.toString());
            if (th instanceof SocketTimeoutException) {
                this.a.b().a(new Ci(MFNetworkReturnCode.CLIENT_TIMEOUT, null));
            } else {
                this.a.b().a(new Ci(601, null));
            }
        }

        @DexIgnore
        @Override // com.fossil.Sq5
        public void c(Call<Ku3> call, Q88<Ku3> q88) {
            Wg6.c(call, "call");
            Wg6.c(q88, "response");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Nu5.f.a();
            local.d(a2, "changePassword onSuccessResponse response=" + q88);
            this.a.b().onSuccess(new Di());
        }
    }

    /*
    static {
        String simpleName = Nu5.class.getSimpleName();
        Wg6.b(simpleName, "ChangePasswordUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public Nu5(AuthApiUserService authApiUserService) {
        Wg6.c(authApiUserService, "mAuthApiUserService");
        this.d = authApiUserService;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.Tq4$Bi] */
    @Override // com.fossil.Tq4
    public /* bridge */ /* synthetic */ void a(Bi bi) {
        h(bi);
    }

    @DexIgnore
    public void h(Bi bi) {
        Wg6.c(bi, "requestValues");
        if (!Hx5.b(PortfolioApp.get.instance())) {
            b().a(new Ci(601, ""));
            return;
        }
        Ku3 ku3 = new Ku3();
        try {
            ku3.n("oldPassword", bi.b());
            ku3.n("newPassword", bi.a());
        } catch (Exception e2) {
        }
        this.d.changePassword(ku3).D(new Ei(this));
    }
}
