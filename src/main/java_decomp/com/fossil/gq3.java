package com.fossil;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.fossil.Kq3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gq3<T extends Context & Kq3> {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public Gq3(T t) {
        Rc2.k(t);
        this.a = t;
    }

    @DexIgnore
    public final int a(Intent intent, int i, int i2) {
        Pm3 a2 = Pm3.a(this.a, null, null);
        Kl3 d = a2.d();
        if (intent == null) {
            d.I().a("AppMeasurementService started with null intent");
        } else {
            String action = intent.getAction();
            a2.b();
            d.N().c("Local AppMeasurementService called. startId, action", Integer.valueOf(i2), action);
            if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
                f(new Fq3(this, i2, d, intent));
            }
        }
        return 2;
    }

    @DexIgnore
    public final IBinder b(Intent intent) {
        if (intent == null) {
            j().F().a("onBind called with null intent");
            return null;
        }
        String action = intent.getAction();
        if ("com.google.android.gms.measurement.START".equals(action)) {
            return new Qm3(Yq3.g(this.a));
        }
        j().I().b("onBind received unknown action", action);
        return null;
    }

    @DexIgnore
    public final void c() {
        Pm3 a2 = Pm3.a(this.a, null, null);
        Kl3 d = a2.d();
        a2.b();
        d.N().a("Local AppMeasurementService is starting up");
    }

    @DexIgnore
    public final /* synthetic */ void d(int i, Kl3 kl3, Intent intent) {
        if (this.a.zza(i)) {
            kl3.N().b("Local AppMeasurementService processed last upload request. StartId", Integer.valueOf(i));
            j().N().a("Completed wakeful intent.");
            this.a.a(intent);
        }
    }

    @DexIgnore
    public final /* synthetic */ void e(Kl3 kl3, JobParameters jobParameters) {
        kl3.N().a("AppMeasurementJobService processed last upload request.");
        this.a.b(jobParameters, false);
    }

    @DexIgnore
    public final void f(Runnable runnable) {
        Yq3 g = Yq3.g(this.a);
        g.c().y(new Hq3(this, g, runnable));
    }

    @DexIgnore
    @TargetApi(24)
    public final boolean g(JobParameters jobParameters) {
        Pm3 a2 = Pm3.a(this.a, null, null);
        Kl3 d = a2.d();
        String string = jobParameters.getExtras().getString("action");
        a2.b();
        d.N().b("Local AppMeasurementJobService called. action", string);
        if (!"com.google.android.gms.measurement.UPLOAD".equals(string)) {
            return true;
        }
        f(new Iq3(this, d, jobParameters));
        return true;
    }

    @DexIgnore
    public final void h() {
        Pm3 a2 = Pm3.a(this.a, null, null);
        Kl3 d = a2.d();
        a2.b();
        d.N().a("Local AppMeasurementService is shutting down");
    }

    @DexIgnore
    public final boolean i(Intent intent) {
        if (intent == null) {
            j().F().a("onUnbind called with null intent");
        } else {
            j().N().b("onUnbind called for intent. action", intent.getAction());
        }
        return true;
    }

    @DexIgnore
    public final Kl3 j() {
        return Pm3.a(this.a, null, null).d();
    }

    @DexIgnore
    public final void k(Intent intent) {
        if (intent == null) {
            j().F().a("onRebind called with null intent");
            return;
        }
        j().N().b("onRebind called. action", intent.getAction());
    }
}
