package com.fossil;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.VideoUploader;
import com.mapped.Cd6;
import com.mapped.Gg6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ga8 implements MethodChannel.MethodCallHandler {
    @DexIgnore
    public static /* final */ ThreadPoolExecutor f; // = new ThreadPoolExecutor(11, 1000, 200, TimeUnit.MINUTES, new ArrayBlockingQueue(11));
    @DexIgnore
    public static boolean g; // = true;
    @DexIgnore
    public static /* final */ Ci h; // = new Ci(null);
    @DexIgnore
    public /* final */ Sa8 b; // = new Sa8();
    @DexIgnore
    public /* final */ Fa8 c; // = new Fa8(this.e, new Handler());
    @DexIgnore
    public /* final */ Ea8 d;
    @DexIgnore
    public /* final */ PluginRegistry.Registrar e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements PluginRegistry.RequestPermissionsResultListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ga8 b;

        @DexIgnore
        public Ai(Ga8 ga8) {
            this.b = ga8;
        }

        @DexIgnore
        @Override // io.flutter.plugin.common.PluginRegistry.RequestPermissionsResultListener
        public final boolean onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
            this.b.b.c(i, strArr, iArr);
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Ra8 {
        @DexIgnore
        @Override // com.fossil.Ra8
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.Ra8
        public void b(List<String> list, List<String> list2) {
            Wg6.c(list, "deniedPermissions");
            Wg6.c(list2, "grantedPermissions");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public /* synthetic */ Ci(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final boolean a() {
            return Ga8.g;
        }

        @DexIgnore
        public final void b(Gg6<Cd6> gg6) {
            Wg6.c(gg6, "runnable");
            Ga8.f.execute(new Ha8(gg6));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ Ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ Ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(Ga8 ga8, MethodCall methodCall, Ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            Object argument = this.$call.argument("ids");
            if (argument != null) {
                Wg6.b(argument, "call.argument<List<String>>(\"ids\")!!");
                this.$resultHandler.c(this.this$0.d.c((List) argument));
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ Ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ Ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(Ga8 ga8, MethodCall methodCall, Ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            try {
                Object argument = this.$call.argument("image");
                if (argument != null) {
                    Wg6.b(argument, "call.argument<ByteArray>(\"image\")!!");
                    byte[] bArr = (byte[]) argument;
                    String str = (String) this.$call.argument("title");
                    String str2 = str != null ? str : "";
                    Wg6.b(str2, "call.argument<String>(\"title\") ?: \"\"");
                    String str3 = (String) this.$call.argument(Constants.DESC);
                    if (str3 == null) {
                        str3 = "";
                    }
                    Wg6.b(str3, "call.argument<String>(\"desc\") ?: \"\"");
                    Ka8 n = this.this$0.d.n(bArr, str2, str3);
                    if (n == null) {
                        this.$resultHandler.c(null);
                    } else {
                        this.$resultHandler.c(Oa8.a.c(n));
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } catch (Exception e) {
                Xa8.a("save image error", e);
                this.$resultHandler.c(null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ Ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ Ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(Ga8 ga8, MethodCall methodCall, Ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            try {
                Object argument = this.$call.argument("path");
                if (argument != null) {
                    Wg6.b(argument, "call.argument<String>(\"path\")!!");
                    String str = (String) argument;
                    Object argument2 = this.$call.argument("title");
                    if (argument2 != null) {
                        Wg6.b(argument2, "call.argument<String>(\"title\")!!");
                        String str2 = (String) argument2;
                        String str3 = (String) this.$call.argument(Constants.DESC);
                        if (str3 == null) {
                            str3 = "";
                        }
                        Wg6.b(str3, "call.argument<String>(\"desc\") ?: \"\"");
                        Ka8 o = this.this$0.d.o(str, str2, str3);
                        if (o == null) {
                            this.$resultHandler.c(null);
                            return;
                        }
                        this.$resultHandler.c(Oa8.a.c(o));
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            } catch (Exception e) {
                Xa8.a("save video error", e);
                this.$resultHandler.c(null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ Ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ Ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(Ga8 ga8, MethodCall methodCall, Ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            Object argument = this.$call.argument("type");
            if (argument != null) {
                Wg6.b(argument, "call.argument<Int>(\"type\")!!");
                int intValue = ((Number) argument).intValue();
                long k = this.this$0.k(this.$call);
                Object argument2 = this.$call.argument("hasAll");
                if (argument2 != null) {
                    Wg6.b(argument2, "call.argument<Boolean>(\"hasAll\")!!");
                    this.$resultHandler.c(Oa8.a.d(this.this$0.d.i(intValue, k, ((Boolean) argument2).booleanValue(), this.this$0.i(this.$call))));
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ Ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ Ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(Ga8 ga8, MethodCall methodCall, Ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                Wg6.b(argument, "call.argument<String>(\"id\")!!");
                String str = (String) argument;
                Object argument2 = this.$call.argument(PlaceFields.PAGE);
                if (argument2 != null) {
                    Wg6.b(argument2, "call.argument<Int>(\"page\")!!");
                    int intValue = ((Number) argument2).intValue();
                    Object argument3 = this.$call.argument("pageCount");
                    if (argument3 != null) {
                        Wg6.b(argument3, "call.argument<Int>(\"pageCount\")!!");
                        int intValue2 = ((Number) argument3).intValue();
                        Object argument4 = this.$call.argument("type");
                        if (argument4 != null) {
                            Wg6.b(argument4, "call.argument<Int>(\"type\")!!");
                            this.$resultHandler.c(Oa8.a.b(this.this$0.d.e(str, intValue, intValue2, ((Number) argument4).intValue(), this.this$0.k(this.$call), this.this$0.i(this.$call))));
                            return;
                        }
                        Wg6.i();
                        throw null;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ Ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ Ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(Ga8 ga8, MethodCall methodCall, Ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            this.$resultHandler.c(Oa8.a.b(this.this$0.d.f(this.this$0.j(this.$call, "galleryId"), this.this$0.h(this.$call, "type"), this.this$0.h(this.$call, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE), this.this$0.h(this.$call, "end"), this.this$0.k(this.$call), this.this$0.i(this.$call))));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ Ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ Ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(Ga8 ga8, MethodCall methodCall, Ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                Wg6.b(argument, "call.argument<String>(\"id\")!!");
                this.this$0.d.a((String) argument, this.$resultHandler);
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $haveLocationPermission;
        @DexIgnore
        public /* final */ /* synthetic */ Ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ Ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(Ga8 ga8, MethodCall methodCall, boolean z, Ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$haveLocationPermission = z;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            boolean booleanValue;
            Object argument = this.$call.argument("id");
            if (argument != null) {
                Wg6.b(argument, "call.argument<String>(\"id\")!!");
                String str = (String) argument;
                if (!this.$haveLocationPermission) {
                    booleanValue = false;
                } else {
                    Object argument2 = this.$call.argument("isOrigin");
                    if (argument2 != null) {
                        Wg6.b(argument2, "call.argument<Boolean>(\"isOrigin\")!!");
                        booleanValue = ((Boolean) argument2).booleanValue();
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                this.this$0.d.h(str, booleanValue, this.$resultHandler);
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $haveLocationPermission;
        @DexIgnore
        public /* final */ /* synthetic */ Ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ Ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Li(Ga8 ga8, MethodCall methodCall, boolean z, Ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$haveLocationPermission = z;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                Wg6.b(argument, "call.argument<String>(\"id\")!!");
                this.this$0.d.k((String) argument, Ga8.h.a(), this.$haveLocationPermission, this.$resultHandler);
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ Ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ Ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Mi(Ga8 ga8, MethodCall methodCall, Ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                Wg6.b(argument, "call.argument<String>(\"id\")!!");
                String str = (String) argument;
                Object argument2 = this.$call.argument("type");
                if (argument2 != null) {
                    Wg6.b(argument2, "call.argument<Int>(\"type\")!!");
                    Ma8 l = this.this$0.d.l(str, ((Number) argument2).intValue(), this.this$0.k(this.$call), this.this$0.i(this.$call));
                    if (l != null) {
                        this.$resultHandler.c(Oa8.a.d(Gm7.b(l)));
                        return;
                    }
                    this.$resultHandler.c(null);
                    return;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ Ya8 $resultHandler;
        @DexIgnore
        public /* final */ /* synthetic */ Ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ni(Ga8 ga8, MethodCall methodCall, Ya8 ya8) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
            this.$resultHandler = ya8;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            Object argument = this.$call.argument("id");
            if (argument != null) {
                Wg6.b(argument, "call.argument<String>(\"id\")!!");
                this.$resultHandler.c(this.this$0.d.j((String) argument));
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall $call;
        @DexIgnore
        public /* final */ /* synthetic */ Ga8 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Oi(Ga8 ga8, MethodCall methodCall) {
            super(0);
            this.this$0 = ga8;
            this.$call = methodCall;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            if (Wg6.a((Boolean) this.$call.argument("notify"), Boolean.TRUE)) {
                this.this$0.c.d();
            } else {
                this.this$0.c.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi implements Ra8 {
        @DexIgnore
        public /* final */ /* synthetic */ Ga8 a;
        @DexIgnore
        public /* final */ /* synthetic */ MethodCall b;
        @DexIgnore
        public /* final */ /* synthetic */ Ya8 c;

        @DexIgnore
        public Pi(Ga8 ga8, MethodCall methodCall, Ya8 ya8) {
            this.a = ga8;
            this.b = methodCall;
            this.c = ya8;
        }

        @DexIgnore
        @Override // com.fossil.Ra8
        public void a() {
            this.a.l(this.b, this.c, true);
        }

        @DexIgnore
        @Override // com.fossil.Ra8
        public void b(List<String> list, List<String> list2) {
            Wg6.c(list, "deniedPermissions");
            Wg6.c(list2, "grantedPermissions");
            Xa8.b("onDenied call.method = " + this.b.method);
            if (Wg6.a(this.b.method, "requestPermission")) {
                this.c.c(0);
                return;
            }
            if (list2.containsAll(Hm7.c("android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"))) {
                this.a.l(this.b, this.c, false);
            } else {
                this.a.m(this.c);
            }
        }
    }

    @DexIgnore
    public Ga8(PluginRegistry.Registrar registrar) {
        Wg6.c(registrar, "registrar");
        this.e = registrar;
        this.e.addRequestPermissionsResultListener(new Ai(this));
        this.b.i(new Bi());
        Context context = this.e.context();
        Wg6.b(context, "registrar.context()");
        Context applicationContext = context.getApplicationContext();
        Wg6.b(applicationContext, "registrar.context().applicationContext");
        this.d = new Ea8(applicationContext);
    }

    @DexIgnore
    public final int h(MethodCall methodCall, String str) {
        Wg6.c(methodCall, "$this$getInt");
        Wg6.c(str, "key");
        Object argument = methodCall.argument(str);
        if (argument != null) {
            return ((Number) argument).intValue();
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final La8 i(MethodCall methodCall) {
        Wg6.c(methodCall, "$this$getOption");
        Object argument = methodCall.argument("option");
        if (argument != null) {
            Wg6.b(argument, "argument<Map<*, *>>(\"option\")!!");
            return Oa8.a.a((Map) argument);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final String j(MethodCall methodCall, String str) {
        Wg6.c(methodCall, "$this$getString");
        Wg6.c(str, "key");
        Object argument = methodCall.argument(str);
        if (argument != null) {
            return (String) argument;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final long k(MethodCall methodCall) {
        Wg6.c(methodCall, "$this$getTimeStamp");
        Object argument = methodCall.argument("timestamp");
        if (argument != null) {
            return ((Number) argument).longValue();
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void l(MethodCall methodCall, Ya8 ya8, boolean z) {
        Xa8.b("onGranted call.method = " + methodCall.method);
        String str = methodCall.method;
        if (str != null) {
            switch (str.hashCode()) {
                case -1283288098:
                    if (str.equals("getLatLngAndroidQ")) {
                        h.b(new Ni(this, methodCall, ya8));
                        return;
                    }
                    break;
                case -1039689911:
                    if (str.equals("notify")) {
                        h.b(new Oi(this, methodCall));
                        return;
                    }
                    break;
                case -886445535:
                    if (str.equals("getFullFile")) {
                        h.b(new Ki(this, methodCall, z, ya8));
                        return;
                    }
                    break;
                case -151967598:
                    if (str.equals("fetchPathProperties")) {
                        h.b(new Mi(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 163601886:
                    if (str.equals("saveImage")) {
                        h.b(new Ei(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 175491326:
                    if (str.equals("saveVideo")) {
                        h.b(new Fi(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 594039295:
                    if (str.equals("getAssetListWithRange")) {
                        h.b(new Ii(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 746581438:
                    if (str.equals("requestPermission")) {
                        ya8.c(1);
                        return;
                    }
                    break;
                case 857200492:
                    if (str.equals("assetExists")) {
                        h.b(new Ji(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 1063055279:
                    if (str.equals("getOriginBytes")) {
                        h.b(new Li(this, methodCall, z, ya8));
                        return;
                    }
                    break;
                case 1150344167:
                    if (str.equals("deleteWithIds")) {
                        h.b(new Di(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 1505159642:
                    if (str.equals("getGalleryList")) {
                        if (Build.VERSION.SDK_INT >= 29) {
                            this.c.c(true);
                        }
                        h.b(new Gi(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 1642188493:
                    if (str.equals("getAssetWithGalleryId")) {
                        h.b(new Hi(this, methodCall, ya8));
                        return;
                    }
                    break;
                case 1966168096:
                    if (str.equals("getThumb")) {
                        Object argument = methodCall.argument("id");
                        if (argument != null) {
                            Wg6.b(argument, "call.argument<String>(\"id\")!!");
                            String str2 = (String) argument;
                            Object argument2 = methodCall.argument("width");
                            if (argument2 != null) {
                                Wg6.b(argument2, "call.argument<Int>(\"width\")!!");
                                int intValue = ((Number) argument2).intValue();
                                Object argument3 = methodCall.argument("height");
                                if (argument3 != null) {
                                    Wg6.b(argument3, "call.argument<Int>(\"height\")!!");
                                    int intValue2 = ((Number) argument3).intValue();
                                    Object argument4 = methodCall.argument("format");
                                    if (argument4 != null) {
                                        Wg6.b(argument4, "call.argument<Int>(\"format\")!!");
                                        this.d.m(str2, intValue, intValue2, ((Number) argument4).intValue(), ya8);
                                        return;
                                    }
                                    Wg6.i();
                                    throw null;
                                }
                                Wg6.i();
                                throw null;
                            }
                            Wg6.i();
                            throw null;
                        }
                        Wg6.i();
                        throw null;
                    }
                    break;
            }
        }
        ya8.b();
    }

    @DexIgnore
    public final void m(Ya8 ya8) {
        ya8.d("Request for permission failed.", "User denied permission.", null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    @Override // io.flutter.plugin.common.MethodChannel.MethodCallHandler
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMethodCall(io.flutter.plugin.common.MethodCall r8, io.flutter.plugin.common.MethodChannel.Result r9) {
        /*
        // Method dump skipped, instructions count: 332
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ga8.onMethodCall(io.flutter.plugin.common.MethodCall, io.flutter.plugin.common.MethodChannel$Result):void");
    }
}
