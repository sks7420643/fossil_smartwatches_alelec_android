package com.fossil;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class S84 {
    @DexIgnore
    public /* final */ ExecutorService a;
    @DexIgnore
    public Nt3<Void> b; // = Qt3.f(null);
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public ThreadLocal<Boolean> d; // = new ThreadLocal<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void run() {
            S84.this.d.set(Boolean.TRUE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ Runnable a;

        @DexIgnore
        public Bi(S84 s84, Runnable runnable) {
            this.a = runnable;
        }

        @DexIgnore
        public Void a() throws Exception {
            this.a.run();
            return null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Void call() throws Exception {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Ft3<Void, T> {
        @DexIgnore
        public /* final */ /* synthetic */ Callable a;

        @DexIgnore
        public Ci(S84 s84, Callable callable) {
            this.a = callable;
        }

        @DexIgnore
        @Override // com.fossil.Ft3
        public T then(Nt3<Void> nt3) throws Exception {
            return (T) this.a.call();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements Ft3<T, Void> {
        @DexIgnore
        public Di(S84 s84) {
        }

        @DexIgnore
        public Void a(Nt3<T> nt3) throws Exception {
            return null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // com.fossil.Ft3
        public /* bridge */ /* synthetic */ Void then(Nt3 nt3) throws Exception {
            return a(nt3);
        }
    }

    @DexIgnore
    public S84(ExecutorService executorService) {
        this.a = executorService;
        executorService.submit(new Ai());
    }

    @DexIgnore
    public void b() {
        if (!e()) {
            throw new IllegalStateException("Not running on background worker thread as intended.");
        }
    }

    @DexIgnore
    public Executor c() {
        return this.a;
    }

    @DexIgnore
    public final <T> Nt3<Void> d(Nt3<T> nt3) {
        return nt3.i(this.a, new Di(this));
    }

    @DexIgnore
    public final boolean e() {
        return Boolean.TRUE.equals(this.d.get());
    }

    @DexIgnore
    public final <T> Ft3<Void, T> f(Callable<T> callable) {
        return new Ci(this, callable);
    }

    @DexIgnore
    public Nt3<Void> g(Runnable runnable) {
        return h(new Bi(this, runnable));
    }

    @DexIgnore
    public <T> Nt3<T> h(Callable<T> callable) {
        Nt3<T> i;
        synchronized (this.c) {
            i = this.b.i(this.a, f(callable));
            this.b = d(i);
        }
        return i;
    }

    @DexIgnore
    public <T> Nt3<T> i(Callable<Nt3<T>> callable) {
        Nt3<T> k;
        synchronized (this.c) {
            k = this.b.k(this.a, f(callable));
            this.b = d(k);
        }
        return k;
    }
}
