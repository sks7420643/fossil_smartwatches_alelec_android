package com.fossil;

import com.fossil.Ta4$d$d$a$b$a;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ka4 extends Ta4$d$d$a$b$a {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4$d$d$a$b$a.a {
        @DexIgnore
        public Long a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$a.a
        public Ta4$d$d$a$b$a a() {
            String str = "";
            if (this.a == null) {
                str = " baseAddress";
            }
            if (this.b == null) {
                str = str + " size";
            }
            if (this.c == null) {
                str = str + " name";
            }
            if (str.isEmpty()) {
                return new Ka4(this.a.longValue(), this.b.longValue(), this.c, this.d);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$a.a
        public Ta4$d$d$a$b$a.a b(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$a.a
        public Ta4$d$d$a$b$a.a c(String str) {
            if (str != null) {
                this.c = str;
                return this;
            }
            throw new NullPointerException("Null name");
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$a.a
        public Ta4$d$d$a$b$a.a d(long j) {
            this.b = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$a.a
        public Ta4$d$d$a$b$a.a e(String str) {
            this.d = str;
            return this;
        }
    }

    @DexIgnore
    public Ka4(long j, long j2, String str, String str2) {
        this.a = j;
        this.b = j2;
        this.c = str;
        this.d = str2;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$a
    public long b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$a
    public String c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$a
    public long d() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$a
    public String e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4$d$d$a$b$a)) {
            return false;
        }
        Ta4$d$d$a$b$a ta4$d$d$a$b$a = (Ta4$d$d$a$b$a) obj;
        if (this.a == ta4$d$d$a$b$a.b() && this.b == ta4$d$d$a$b$a.d() && this.c.equals(ta4$d$d$a$b$a.c())) {
            String str = this.d;
            if (str == null) {
                if (ta4$d$d$a$b$a.e() == null) {
                    return true;
                }
            } else if (str.equals(ta4$d$d$a$b$a.e())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        int i = (int) (j ^ (j >>> 32));
        long j2 = this.b;
        int i2 = (int) (j2 ^ (j2 >>> 32));
        int hashCode = this.c.hashCode();
        String str = this.d;
        return (str == null ? 0 : str.hashCode()) ^ ((((((i ^ 1000003) * 1000003) ^ i2) * 1000003) ^ hashCode) * 1000003);
    }

    @DexIgnore
    public String toString() {
        return "BinaryImage{baseAddress=" + this.a + ", size=" + this.b + ", name=" + this.c + ", uuid=" + this.d + "}";
    }
}
