package com.fossil;

import android.content.Intent;
import com.fossil.iq4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.setting.SpecialSkuSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lt5 extends iq4<a, c, b> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ d e; // = new d();
    @DexIgnore
    public /* final */ NotificationsRepository f;
    @DexIgnore
    public /* final */ DeviceRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ List<j06> f2243a;
        @DexIgnore
        public /* final */ List<i06> b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public a(List<j06> list, List<i06> list2, int i) {
            pq7.c(list, "contactWrapperList");
            pq7.c(list2, "appWrapperList");
            this.f2243a = list;
            this.b = list2;
            this.c = i;
        }

        @DexIgnore
        public final List<i06> a() {
            return this.b;
        }

        @DexIgnore
        public final List<j06> b() {
            return this.f2243a;
        }

        @DexIgnore
        public final int c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f2244a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public b(int i, int i2, ArrayList<Integer> arrayList) {
            pq7.c(arrayList, "errorCodes");
            this.f2244a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.f2244a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements wq5.b {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            boolean z = false;
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetNotificationFiltersUserCase", "Inside .bleReceiver communicateMode= " + communicateMode);
            if (communicateMode == CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS && lt5.this.p()) {
                lt5.this.s(false);
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    lt5.this.j(new c());
                    return;
                }
                FLogger.INSTANCE.getLocal().d("SetNotificationFiltersUserCase", "onReceive failed");
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                lt5.this.i(new b(FailureCode.FAILED_TO_CONNECT, intExtra, integerArrayListExtra));
            }
        }
    }

    @DexIgnore
    public lt5(NotificationsRepository notificationsRepository, DeviceRepository deviceRepository) {
        pq7.c(notificationsRepository, "mNotificationsRepository");
        pq7.c(deviceRepository, "mDeviceRepository");
        this.f = notificationsRepository;
        this.g = deviceRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "SetNotificationFiltersUserCase";
    }

    @DexIgnore
    public final List<AppNotificationFilter> m(List<i06> list, short s, boolean z) {
        ArrayList arrayList = new ArrayList();
        short angleForApp = !z ? -1 : (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForApp();
        for (i06 i06 : list) {
            InstalledApp installedApp = i06.getInstalledApp();
            if (installedApp != null) {
                Boolean isSelected = installedApp.isSelected();
                pq7.b(isSelected, "it.isSelected");
                if (isSelected.booleanValue()) {
                    String title = installedApp.getTitle() == null ? "" : installedApp.getTitle();
                    lo1 lo1 = new lo1(s, s, angleForApp, 10000);
                    pq7.b(title, "appName");
                    String identifier = installedApp.getIdentifier();
                    pq7.b(identifier, "it.identifier");
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(title, identifier, "", NotificationBaseObj.ANotificationType.NOTIFICATION));
                    appNotificationFilter.setHandMovingConfig(lo1);
                    appNotificationFilter.setVibePattern(no1.DEFAULT_OTHER_APPS);
                    arrayList.add(appNotificationFilter);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<AppNotificationFilter> n(List<j06> list, short s, boolean z) {
        Contact contact;
        short s2;
        ArrayList arrayList = new ArrayList();
        for (j06 j06 : list) {
            if (j06.isAdded() && (contact = j06.getContact()) != null) {
                if (contact.isUseCall()) {
                    s2 = z ? (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForCall() : -1;
                    lo1 lo1 = new lo1(s, s, s2, 10000);
                    DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), "", phone_incoming_call.getNotificationType()));
                    appNotificationFilter.setSender(contact.getDisplayName());
                    appNotificationFilter.setHandMovingConfig(lo1);
                    appNotificationFilter.setVibePattern(no1.CALL);
                    arrayList.add(appNotificationFilter);
                } else {
                    s2 = -1;
                }
                if (contact.isUseSms()) {
                    if (z) {
                        s2 = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForSms();
                    }
                    lo1 lo12 = new lo1(s, s, s2, 10000);
                    DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType()));
                    appNotificationFilter2.setSender(contact.getDisplayName());
                    appNotificationFilter2.setHandMovingConfig(lo12);
                    appNotificationFilter2.setVibePattern(no1.TEXT);
                    arrayList.add(appNotificationFilter2);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0243  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0083 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x01a8 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x01c8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter> o(android.util.SparseArray<java.util.List<com.fossil.wearables.fsl.shared.BaseFeatureModel>> r22, java.util.List<com.fossil.j06> r23, java.util.List<com.fossil.i06> r24, short r25, boolean r26) {
        /*
        // Method dump skipped, instructions count: 585
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.lt5.o(android.util.SparseArray, java.util.List, java.util.List, short, boolean):java.util.List");
    }

    @DexIgnore
    public final boolean p() {
        return this.d;
    }

    @DexIgnore
    public final void q() {
        wq5.d.e(this.e, CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    /* renamed from: r */
    public Object k(a aVar, qn7<Object> qn7) {
        Integer num = null;
        try {
            FLogger.INSTANCE.getLocal().d("SetNotificationFiltersUserCase", "running UseCase");
            this.d = true;
            List<j06> b2 = aVar != null ? aVar.b() : null;
            List<i06> a2 = aVar != null ? aVar.a() : null;
            if (aVar != null) {
                num = ao7.e(aVar.c());
            }
            if (b2 == null || a2 == null || num == null) {
                i(new b(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG, -1, new ArrayList()));
            } else {
                String J = PortfolioApp.h0.c().J();
                boolean j = e47.b.j(this.g.getSkuModelBySerialPrefix(nk5.o.m(J)), J);
                short e2 = (short) ll5.e(num.intValue());
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(o(this.f.getAllNotificationsByHour(J, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()), b2, a2, e2, j));
                arrayList.addAll(n(b2, e2, j));
                arrayList.addAll(m(a2, e2, j));
                AppNotificationFilterSettings appNotificationFilterSettings = new AppNotificationFilterSettings(arrayList, System.currentTimeMillis());
                PortfolioApp.h0.c().s1(appNotificationFilterSettings, J);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetNotificationFiltersUserCase", "saveNotificationSettingToDevice, total: " + appNotificationFilterSettings.getNotificationFilters().size() + " items");
            }
            return new Object();
        } catch (Exception e3) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.e("SetNotificationFiltersUserCase", "Error inside SetNotificationFiltersUserCase.connectDevice - e=" + e3);
            return new b(600, -1, new ArrayList());
        }
    }

    @DexIgnore
    public final void s(boolean z) {
        this.d = z;
    }
}
