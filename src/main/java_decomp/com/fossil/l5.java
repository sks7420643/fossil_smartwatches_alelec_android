package com.fossil;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import com.mapped.Hg6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L5 extends Qq7 implements Hg6<BluetoothGattService, String> {
    @DexIgnore
    public static /* final */ L5 b; // = new L5();

    @DexIgnore
    public L5() {
        super(1);
    }

    @DexIgnore
    public final String a(BluetoothGattService bluetoothGattService) {
        StringBuilder sb = new StringBuilder();
        Wg6.b(bluetoothGattService, Constants.SERVICE);
        sb.append(bluetoothGattService.getUuid().toString());
        sb.append("\n");
        List<BluetoothGattCharacteristic> characteristics = bluetoothGattService.getCharacteristics();
        Wg6.b(characteristics, "service.characteristics");
        sb.append(Pm7.N(characteristics, "\n", null, null, 0, null, J5.b, 30, null));
        return sb.toString();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ String invoke(BluetoothGattService bluetoothGattService) {
        return a(bluetoothGattService);
    }
}
