package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V83 implements W83 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        a = hw2.d("measurement.service.ssaid_removal", true);
        hw2.b("measurement.id.ssaid_removal", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.W83
    public final boolean zza() {
        return a.o().booleanValue();
    }
}
