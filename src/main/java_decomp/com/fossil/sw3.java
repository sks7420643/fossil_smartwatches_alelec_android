package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sw3 {
    @DexIgnore
    public static /* final */ int AlertDialog_AppCompat; // = 2131951617;
    @DexIgnore
    public static /* final */ int AlertDialog_AppCompat_Light; // = 2131951618;
    @DexIgnore
    public static /* final */ int Animation_AppCompat_Dialog; // = 2131951622;
    @DexIgnore
    public static /* final */ int Animation_AppCompat_DropDownUp; // = 2131951623;
    @DexIgnore
    public static /* final */ int Animation_AppCompat_Tooltip; // = 2131951624;
    @DexIgnore
    public static /* final */ int Animation_Design_BottomSheetDialog; // = 2131951625;
    @DexIgnore
    public static /* final */ int Animation_MaterialComponents_BottomSheetDialog; // = 2131951626;
    @DexIgnore
    public static /* final */ int Base_AlertDialog_AppCompat; // = 2131951681;
    @DexIgnore
    public static /* final */ int Base_AlertDialog_AppCompat_Light; // = 2131951682;
    @DexIgnore
    public static /* final */ int Base_Animation_AppCompat_Dialog; // = 2131951683;
    @DexIgnore
    public static /* final */ int Base_Animation_AppCompat_DropDownUp; // = 2131951684;
    @DexIgnore
    public static /* final */ int Base_Animation_AppCompat_Tooltip; // = 2131951685;
    @DexIgnore
    public static /* final */ int Base_CardView; // = 2131951686;
    @DexIgnore
    public static /* final */ int Base_DialogWindowTitleBackground_AppCompat; // = 2131951688;
    @DexIgnore
    public static /* final */ int Base_DialogWindowTitle_AppCompat; // = 2131951687;
    @DexIgnore
    public static /* final */ int Base_MaterialAlertDialog_MaterialComponents_Title_Icon; // = 2131951689;
    @DexIgnore
    public static /* final */ int Base_MaterialAlertDialog_MaterialComponents_Title_Panel; // = 2131951690;
    @DexIgnore
    public static /* final */ int Base_MaterialAlertDialog_MaterialComponents_Title_Text; // = 2131951691;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat; // = 2131951692;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Body1; // = 2131951693;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Body2; // = 2131951694;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Button; // = 2131951695;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Caption; // = 2131951696;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display1; // = 2131951697;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display2; // = 2131951698;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display3; // = 2131951699;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display4; // = 2131951700;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Headline; // = 2131951701;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Inverse; // = 2131951702;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Large; // = 2131951703;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Large_Inverse; // = 2131951704;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large; // = 2131951705;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small; // = 2131951706;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Medium; // = 2131951707;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Medium_Inverse; // = 2131951708;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Menu; // = 2131951709;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_SearchResult; // = 2131951710;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_SearchResult_Subtitle; // = 2131951711;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_SearchResult_Title; // = 2131951712;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Small; // = 2131951713;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Small_Inverse; // = 2131951714;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Subhead; // = 2131951715;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Subhead_Inverse; // = 2131951716;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Title; // = 2131951717;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Title_Inverse; // = 2131951718;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Tooltip; // = 2131951719;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Menu; // = 2131951720;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle; // = 2131951721;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse; // = 2131951722;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Title; // = 2131951723;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse; // = 2131951724;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle; // = 2131951725;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionMode_Title; // = 2131951726;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button; // = 2131951727;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button_Borderless_Colored; // = 2131951728;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button_Colored; // = 2131951729;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button_Inverse; // = 2131951730;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_DropDownItem; // = 2131951731;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_PopupMenu_Header; // = 2131951732;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_PopupMenu_Large; // = 2131951733;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_PopupMenu_Small; // = 2131951734;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Switch; // = 2131951735;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_TextView_SpinnerItem; // = 2131951736;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_MaterialComponents_Badge; // = 2131951737;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_MaterialComponents_Button; // = 2131951738;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_MaterialComponents_Headline6; // = 2131951739;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_MaterialComponents_Subtitle2; // = 2131951740;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item; // = 2131951741;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle; // = 2131951742;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_Widget_AppCompat_Toolbar_Title; // = 2131951743;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat; // = 2131951777;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_ActionBar; // = 2131951778;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dark; // = 2131951779;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dark_ActionBar; // = 2131951780;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dialog; // = 2131951781;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dialog_Alert; // = 2131951782;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Light; // = 2131951783;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_MaterialComponents_Dialog; // = 2131951784;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_MaterialComponents_Dialog_Alert; // = 2131951785;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_MaterialComponents_MaterialAlertDialog; // = 2131951786;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat; // = 2131951744;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_CompactMenu; // = 2131951745;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog; // = 2131951746;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_DialogWhenLarge; // = 2131951750;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog_Alert; // = 2131951747;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog_FixedSize; // = 2131951748;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog_MinWidth; // = 2131951749;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light; // = 2131951751;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_DarkActionBar; // = 2131951752;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog; // = 2131951753;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_DialogWhenLarge; // = 2131951757;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog_Alert; // = 2131951754;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog_FixedSize; // = 2131951755;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog_MinWidth; // = 2131951756;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents; // = 2131951758;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Bridge; // = 2131951759;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_CompactMenu; // = 2131951760;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Dialog; // = 2131951761;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_DialogWhenLarge; // = 2131951766;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Dialog_Alert; // = 2131951762;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Dialog_Bridge; // = 2131951763;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Dialog_FixedSize; // = 2131951764;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Dialog_MinWidth; // = 2131951765;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Light; // = 2131951767;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Light_Bridge; // = 2131951768;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Light_DarkActionBar; // = 2131951769;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Light_DarkActionBar_Bridge; // = 2131951770;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Light_Dialog; // = 2131951771;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Light_DialogWhenLarge; // = 2131951776;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Light_Dialog_Alert; // = 2131951772;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Light_Dialog_Bridge; // = 2131951773;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Light_Dialog_FixedSize; // = 2131951774;
    @DexIgnore
    public static /* final */ int Base_Theme_MaterialComponents_Light_Dialog_MinWidth; // = 2131951775;
    @DexIgnore
    public static /* final */ int Base_V14_ThemeOverlay_MaterialComponents_Dialog; // = 2131951796;
    @DexIgnore
    public static /* final */ int Base_V14_ThemeOverlay_MaterialComponents_Dialog_Alert; // = 2131951797;
    @DexIgnore
    public static /* final */ int Base_V14_ThemeOverlay_MaterialComponents_MaterialAlertDialog; // = 2131951798;
    @DexIgnore
    public static /* final */ int Base_V14_Theme_MaterialComponents; // = 2131951787;
    @DexIgnore
    public static /* final */ int Base_V14_Theme_MaterialComponents_Bridge; // = 2131951788;
    @DexIgnore
    public static /* final */ int Base_V14_Theme_MaterialComponents_Dialog; // = 2131951789;
    @DexIgnore
    public static /* final */ int Base_V14_Theme_MaterialComponents_Dialog_Bridge; // = 2131951790;
    @DexIgnore
    public static /* final */ int Base_V14_Theme_MaterialComponents_Light; // = 2131951791;
    @DexIgnore
    public static /* final */ int Base_V14_Theme_MaterialComponents_Light_Bridge; // = 2131951792;
    @DexIgnore
    public static /* final */ int Base_V14_Theme_MaterialComponents_Light_DarkActionBar_Bridge; // = 2131951793;
    @DexIgnore
    public static /* final */ int Base_V14_Theme_MaterialComponents_Light_Dialog; // = 2131951794;
    @DexIgnore
    public static /* final */ int Base_V14_Theme_MaterialComponents_Light_Dialog_Bridge; // = 2131951795;
    @DexIgnore
    public static /* final */ int Base_V21_ThemeOverlay_AppCompat_Dialog; // = 2131951803;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat; // = 2131951799;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat_Dialog; // = 2131951800;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat_Light; // = 2131951801;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat_Light_Dialog; // = 2131951802;
    @DexIgnore
    public static /* final */ int Base_V22_Theme_AppCompat; // = 2131951804;
    @DexIgnore
    public static /* final */ int Base_V22_Theme_AppCompat_Light; // = 2131951805;
    @DexIgnore
    public static /* final */ int Base_V23_Theme_AppCompat; // = 2131951806;
    @DexIgnore
    public static /* final */ int Base_V23_Theme_AppCompat_Light; // = 2131951807;
    @DexIgnore
    public static /* final */ int Base_V26_Theme_AppCompat; // = 2131951808;
    @DexIgnore
    public static /* final */ int Base_V26_Theme_AppCompat_Light; // = 2131951809;
    @DexIgnore
    public static /* final */ int Base_V26_Widget_AppCompat_Toolbar; // = 2131951810;
    @DexIgnore
    public static /* final */ int Base_V28_Theme_AppCompat; // = 2131951811;
    @DexIgnore
    public static /* final */ int Base_V28_Theme_AppCompat_Light; // = 2131951812;
    @DexIgnore
    public static /* final */ int Base_V7_ThemeOverlay_AppCompat_Dialog; // = 2131951817;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat; // = 2131951813;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat_Dialog; // = 2131951814;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat_Light; // = 2131951815;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat_Light_Dialog; // = 2131951816;
    @DexIgnore
    public static /* final */ int Base_V7_Widget_AppCompat_AutoCompleteTextView; // = 2131951818;
    @DexIgnore
    public static /* final */ int Base_V7_Widget_AppCompat_EditText; // = 2131951819;
    @DexIgnore
    public static /* final */ int Base_V7_Widget_AppCompat_Toolbar; // = 2131951820;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar; // = 2131951821;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_Solid; // = 2131951822;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_TabBar; // = 2131951823;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_TabText; // = 2131951824;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_TabView; // = 2131951825;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionButton; // = 2131951826;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionButton_CloseMode; // = 2131951827;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionButton_Overflow; // = 2131951828;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionMode; // = 2131951829;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActivityChooserView; // = 2131951830;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_AutoCompleteTextView; // = 2131951831;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button; // = 2131951832;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ButtonBar; // = 2131951838;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ButtonBar_AlertDialog; // = 2131951839;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Borderless; // = 2131951833;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Borderless_Colored; // = 2131951834;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_ButtonBar_AlertDialog; // = 2131951835;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Colored; // = 2131951836;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Small; // = 2131951837;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_CompoundButton_CheckBox; // = 2131951840;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_CompoundButton_RadioButton; // = 2131951841;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_CompoundButton_Switch; // = 2131951842;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_DrawerArrowToggle; // = 2131951843;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_DrawerArrowToggle_Common; // = 2131951844;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_DropDownItem_Spinner; // = 2131951845;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_EditText; // = 2131951846;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ImageButton; // = 2131951847;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar; // = 2131951848;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_Solid; // = 2131951849;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabBar; // = 2131951850;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabText; // = 2131951851;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse; // = 2131951852;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabView; // = 2131951853;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_PopupMenu; // = 2131951854;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_PopupMenu_Overflow; // = 2131951855;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListMenuView; // = 2131951856;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListPopupWindow; // = 2131951857;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListView; // = 2131951858;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListView_DropDown; // = 2131951859;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListView_Menu; // = 2131951860;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_PopupMenu; // = 2131951861;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_PopupMenu_Overflow; // = 2131951862;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_PopupWindow; // = 2131951863;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ProgressBar; // = 2131951864;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ProgressBar_Horizontal; // = 2131951865;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_RatingBar; // = 2131951866;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_RatingBar_Indicator; // = 2131951867;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_RatingBar_Small; // = 2131951868;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SearchView; // = 2131951869;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SearchView_ActionBar; // = 2131951870;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SeekBar; // = 2131951871;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SeekBar_Discrete; // = 2131951872;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Spinner; // = 2131951873;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Spinner_Underlined; // = 2131951874;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_TextView; // = 2131951875;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_TextView_SpinnerItem; // = 2131951876;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Toolbar; // = 2131951877;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Toolbar_Button_Navigation; // = 2131951878;
    @DexIgnore
    public static /* final */ int Base_Widget_Design_TabLayout; // = 2131951879;
    @DexIgnore
    public static /* final */ int Base_Widget_MaterialComponents_AutoCompleteTextView; // = 2131951880;
    @DexIgnore
    public static /* final */ int Base_Widget_MaterialComponents_CheckedTextView; // = 2131951881;
    @DexIgnore
    public static /* final */ int Base_Widget_MaterialComponents_Chip; // = 2131951882;
    @DexIgnore
    public static /* final */ int Base_Widget_MaterialComponents_PopupMenu; // = 2131951883;
    @DexIgnore
    public static /* final */ int Base_Widget_MaterialComponents_PopupMenu_ContextMenu; // = 2131951884;
    @DexIgnore
    public static /* final */ int Base_Widget_MaterialComponents_PopupMenu_ListPopupWindow; // = 2131951885;
    @DexIgnore
    public static /* final */ int Base_Widget_MaterialComponents_PopupMenu_Overflow; // = 2131951886;
    @DexIgnore
    public static /* final */ int Base_Widget_MaterialComponents_TextInputEditText; // = 2131951887;
    @DexIgnore
    public static /* final */ int Base_Widget_MaterialComponents_TextInputLayout; // = 2131951888;
    @DexIgnore
    public static /* final */ int Base_Widget_MaterialComponents_TextView; // = 2131951889;
    @DexIgnore
    public static /* final */ int CardView; // = 2131951903;
    @DexIgnore
    public static /* final */ int CardView_Dark; // = 2131951904;
    @DexIgnore
    public static /* final */ int CardView_Light; // = 2131951905;
    @DexIgnore
    public static /* final */ int EmptyTheme; // = 2131951937;
    @DexIgnore
    public static /* final */ int MaterialAlertDialog_MaterialComponents; // = 2131951952;
    @DexIgnore
    public static /* final */ int MaterialAlertDialog_MaterialComponents_Body_Text; // = 2131951953;
    @DexIgnore
    public static /* final */ int MaterialAlertDialog_MaterialComponents_Picker_Date_Calendar; // = 2131951954;
    @DexIgnore
    public static /* final */ int MaterialAlertDialog_MaterialComponents_Picker_Date_Spinner; // = 2131951955;
    @DexIgnore
    public static /* final */ int MaterialAlertDialog_MaterialComponents_Title_Icon; // = 2131951956;
    @DexIgnore
    public static /* final */ int MaterialAlertDialog_MaterialComponents_Title_Icon_CenterStacked; // = 2131951957;
    @DexIgnore
    public static /* final */ int MaterialAlertDialog_MaterialComponents_Title_Panel; // = 2131951958;
    @DexIgnore
    public static /* final */ int MaterialAlertDialog_MaterialComponents_Title_Panel_CenterStacked; // = 2131951959;
    @DexIgnore
    public static /* final */ int MaterialAlertDialog_MaterialComponents_Title_Text; // = 2131951960;
    @DexIgnore
    public static /* final */ int MaterialAlertDialog_MaterialComponents_Title_Text_CenterStacked; // = 2131951961;
    @DexIgnore
    public static /* final */ int Platform_AppCompat; // = 2131951998;
    @DexIgnore
    public static /* final */ int Platform_AppCompat_Light; // = 2131951999;
    @DexIgnore
    public static /* final */ int Platform_MaterialComponents; // = 2131952000;
    @DexIgnore
    public static /* final */ int Platform_MaterialComponents_Dialog; // = 2131952001;
    @DexIgnore
    public static /* final */ int Platform_MaterialComponents_Light; // = 2131952002;
    @DexIgnore
    public static /* final */ int Platform_MaterialComponents_Light_Dialog; // = 2131952003;
    @DexIgnore
    public static /* final */ int Platform_ThemeOverlay_AppCompat; // = 2131952004;
    @DexIgnore
    public static /* final */ int Platform_ThemeOverlay_AppCompat_Dark; // = 2131952005;
    @DexIgnore
    public static /* final */ int Platform_ThemeOverlay_AppCompat_Light; // = 2131952006;
    @DexIgnore
    public static /* final */ int Platform_V21_AppCompat; // = 2131952007;
    @DexIgnore
    public static /* final */ int Platform_V21_AppCompat_Light; // = 2131952008;
    @DexIgnore
    public static /* final */ int Platform_V25_AppCompat; // = 2131952009;
    @DexIgnore
    public static /* final */ int Platform_V25_AppCompat_Light; // = 2131952010;
    @DexIgnore
    public static /* final */ int Platform_Widget_AppCompat_Spinner; // = 2131952011;
    @DexIgnore
    public static /* final */ int RtlOverlay_DialogWindowTitle_AppCompat; // = 2131952019;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_ActionBar_TitleItem; // = 2131952020;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_DialogTitle_Icon; // = 2131952021;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem; // = 2131952022;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup; // = 2131952023;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_Shortcut; // = 2131952024;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_SubmenuArrow; // = 2131952025;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_Text; // = 2131952026;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_Title; // = 2131952027;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_SearchView_MagIcon; // = 2131952033;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown; // = 2131952028;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1; // = 2131952029;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2; // = 2131952030;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Query; // = 2131952031;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Text; // = 2131952032;
    @DexIgnore
    public static /* final */ int RtlUnderlay_Widget_AppCompat_ActionButton; // = 2131952034;
    @DexIgnore
    public static /* final */ int RtlUnderlay_Widget_AppCompat_ActionButton_Overflow; // = 2131952035;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay; // = 2131952044;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay_BottomLeftDifferentCornerSize; // = 2131952045;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay_BottomRightCut; // = 2131952046;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay_Cut; // = 2131952047;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay_DifferentCornerSize; // = 2131952048;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay_MaterialComponents_BottomSheet; // = 2131952049;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay_MaterialComponents_Chip; // = 2131952050;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay_MaterialComponents_ExtendedFloatingActionButton; // = 2131952051;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay_MaterialComponents_FloatingActionButton; // = 2131952052;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Day; // = 2131952053;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Window_Fullscreen; // = 2131952054;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Year; // = 2131952055;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay_MaterialComponents_TextInputLayout_FilledBox; // = 2131952056;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay_TopLeftCut; // = 2131952057;
    @DexIgnore
    public static /* final */ int ShapeAppearanceOverlay_TopRightDifferentCornerSize; // = 2131952058;
    @DexIgnore
    public static /* final */ int ShapeAppearance_MaterialComponents; // = 2131952039;
    @DexIgnore
    public static /* final */ int ShapeAppearance_MaterialComponents_LargeComponent; // = 2131952040;
    @DexIgnore
    public static /* final */ int ShapeAppearance_MaterialComponents_MediumComponent; // = 2131952041;
    @DexIgnore
    public static /* final */ int ShapeAppearance_MaterialComponents_SmallComponent; // = 2131952042;
    @DexIgnore
    public static /* final */ int ShapeAppearance_MaterialComponents_Test; // = 2131952043;
    @DexIgnore
    public static /* final */ int TestStyleWithLineHeight; // = 2131952069;
    @DexIgnore
    public static /* final */ int TestStyleWithLineHeightAppearance; // = 2131952070;
    @DexIgnore
    public static /* final */ int TestStyleWithThemeLineHeightAttribute; // = 2131952071;
    @DexIgnore
    public static /* final */ int TestStyleWithoutLineHeight; // = 2131952072;
    @DexIgnore
    public static /* final */ int TestThemeWithLineHeight; // = 2131952073;
    @DexIgnore
    public static /* final */ int TestThemeWithLineHeightDisabled; // = 2131952074;
    @DexIgnore
    public static /* final */ int Test_ShapeAppearanceOverlay_MaterialComponents_MaterialCalendar_Day; // = 2131952064;
    @DexIgnore
    public static /* final */ int Test_Theme_MaterialComponents_MaterialCalendar; // = 2131952065;
    @DexIgnore
    public static /* final */ int Test_Widget_MaterialComponents_MaterialCalendar; // = 2131952066;
    @DexIgnore
    public static /* final */ int Test_Widget_MaterialComponents_MaterialCalendar_Day; // = 2131952067;
    @DexIgnore
    public static /* final */ int Test_Widget_MaterialComponents_MaterialCalendar_Day_Selected; // = 2131952068;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat; // = 2131952075;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Body1; // = 2131952076;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Body2; // = 2131952077;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Button; // = 2131952078;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Caption; // = 2131952079;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display1; // = 2131952080;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display2; // = 2131952081;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display3; // = 2131952082;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display4; // = 2131952083;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Headline; // = 2131952084;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Inverse; // = 2131952085;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Large; // = 2131952086;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Large_Inverse; // = 2131952087;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_SearchResult_Subtitle; // = 2131952088;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_SearchResult_Title; // = 2131952089;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_Widget_PopupMenu_Large; // = 2131952090;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_Widget_PopupMenu_Small; // = 2131952091;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Medium; // = 2131952092;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Medium_Inverse; // = 2131952093;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Menu; // = 2131952094;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_SearchResult_Subtitle; // = 2131952095;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_SearchResult_Title; // = 2131952096;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Small; // = 2131952097;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Small_Inverse; // = 2131952098;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Subhead; // = 2131952099;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Subhead_Inverse; // = 2131952100;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Title; // = 2131952101;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Title_Inverse; // = 2131952102;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Tooltip; // = 2131952103;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Menu; // = 2131952104;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Subtitle; // = 2131952105;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse; // = 2131952106;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Title; // = 2131952107;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse; // = 2131952108;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Subtitle; // = 2131952109;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse; // = 2131952110;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Title; // = 2131952111;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse; // = 2131952112;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button; // = 2131952113;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button_Borderless_Colored; // = 2131952114;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button_Colored; // = 2131952115;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button_Inverse; // = 2131952116;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_DropDownItem; // = 2131952117;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_PopupMenu_Header; // = 2131952118;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_PopupMenu_Large; // = 2131952119;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_PopupMenu_Small; // = 2131952120;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Switch; // = 2131952121;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_TextView_SpinnerItem; // = 2131952122;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification; // = 2131952123;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Info; // = 2131952124;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Line2; // = 2131952126;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Time; // = 2131952129;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Title; // = 2131952131;
    @DexIgnore
    public static /* final */ int TextAppearance_Design_CollapsingToolbar_Expanded; // = 2131952133;
    @DexIgnore
    public static /* final */ int TextAppearance_Design_Counter; // = 2131952134;
    @DexIgnore
    public static /* final */ int TextAppearance_Design_Counter_Overflow; // = 2131952135;
    @DexIgnore
    public static /* final */ int TextAppearance_Design_Error; // = 2131952136;
    @DexIgnore
    public static /* final */ int TextAppearance_Design_HelperText; // = 2131952137;
    @DexIgnore
    public static /* final */ int TextAppearance_Design_Hint; // = 2131952138;
    @DexIgnore
    public static /* final */ int TextAppearance_Design_Snackbar_Message; // = 2131952139;
    @DexIgnore
    public static /* final */ int TextAppearance_Design_Tab; // = 2131952140;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Badge; // = 2131952141;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Body1; // = 2131952142;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Body2; // = 2131952143;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Button; // = 2131952144;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Caption; // = 2131952145;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Chip; // = 2131952146;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Headline1; // = 2131952147;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Headline2; // = 2131952148;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Headline3; // = 2131952149;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Headline4; // = 2131952150;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Headline5; // = 2131952151;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Headline6; // = 2131952152;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Overline; // = 2131952153;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Subtitle1; // = 2131952154;
    @DexIgnore
    public static /* final */ int TextAppearance_MaterialComponents_Subtitle2; // = 2131952155;
    @DexIgnore
    public static /* final */ int TextAppearance_Widget_AppCompat_ExpandedMenu_Item; // = 2131952156;
    @DexIgnore
    public static /* final */ int TextAppearance_Widget_AppCompat_Toolbar_Subtitle; // = 2131952157;
    @DexIgnore
    public static /* final */ int TextAppearance_Widget_AppCompat_Toolbar_Title; // = 2131952158;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat; // = 2131952245;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_ActionBar; // = 2131952246;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dark; // = 2131952247;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dark_ActionBar; // = 2131952248;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_DayNight; // = 2131952249;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_DayNight_ActionBar; // = 2131952250;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dialog; // = 2131952251;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dialog_Alert; // = 2131952252;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Light; // = 2131952253;
    @DexIgnore
    public static /* final */ int ThemeOverlay_Design_TextInputEditText; // = 2131952254;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents; // = 2131952255;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_ActionBar; // = 2131952256;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_ActionBar_Primary; // = 2131952257;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_ActionBar_Surface; // = 2131952258;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_AutoCompleteTextView; // = 2131952259;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_AutoCompleteTextView_FilledBox; // = 2131952260;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_AutoCompleteTextView_FilledBox_Dense; // = 2131952261;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_AutoCompleteTextView_OutlinedBox; // = 2131952262;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_AutoCompleteTextView_OutlinedBox_Dense; // = 2131952263;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_BottomAppBar_Primary; // = 2131952264;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_BottomAppBar_Surface; // = 2131952265;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_BottomSheetDialog; // = 2131952266;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_Dark; // = 2131952267;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_Dark_ActionBar; // = 2131952268;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_DayNight_BottomSheetDialog; // = 2131952269;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_Dialog; // = 2131952270;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_Dialog_Alert; // = 2131952271;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_Light; // = 2131952272;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_Light_BottomSheetDialog; // = 2131952273;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_MaterialAlertDialog; // = 2131952274;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_MaterialAlertDialog_Centered; // = 2131952275;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date; // = 2131952276;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Calendar; // = 2131952277;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Header_Text; // = 2131952278;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Header_Text_Day; // = 2131952279;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_MaterialAlertDialog_Picker_Date_Spinner; // = 2131952280;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_MaterialCalendar; // = 2131952281;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_MaterialCalendar_Fullscreen; // = 2131952282;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_TextInputEditText; // = 2131952283;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_TextInputEditText_FilledBox; // = 2131952284;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_TextInputEditText_FilledBox_Dense; // = 2131952285;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_TextInputEditText_OutlinedBox; // = 2131952286;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_TextInputEditText_OutlinedBox_Dense; // = 2131952287;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_Toolbar_Primary; // = 2131952288;
    @DexIgnore
    public static /* final */ int ThemeOverlay_MaterialComponents_Toolbar_Surface; // = 2131952289;
    @DexIgnore
    public static /* final */ int Theme_AppCompat; // = 2131952168;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_CompactMenu; // = 2131952169;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight; // = 2131952170;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_DarkActionBar; // = 2131952171;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_Dialog; // = 2131952172;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_DialogWhenLarge; // = 2131952175;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_Dialog_Alert; // = 2131952173;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_Dialog_MinWidth; // = 2131952174;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_NoActionBar; // = 2131952176;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Dialog; // = 2131952177;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DialogWhenLarge; // = 2131952180;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Dialog_Alert; // = 2131952178;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Dialog_MinWidth; // = 2131952179;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light; // = 2131952182;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_DarkActionBar; // = 2131952183;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_Dialog; // = 2131952184;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_DialogWhenLarge; // = 2131952187;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_Dialog_Alert; // = 2131952185;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_Dialog_MinWidth; // = 2131952186;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_NoActionBar; // = 2131952188;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_NoActionBar; // = 2131952189;
    @DexIgnore
    public static /* final */ int Theme_Design; // = 2131952190;
    @DexIgnore
    public static /* final */ int Theme_Design_BottomSheetDialog; // = 2131952191;
    @DexIgnore
    public static /* final */ int Theme_Design_Light; // = 2131952192;
    @DexIgnore
    public static /* final */ int Theme_Design_Light_BottomSheetDialog; // = 2131952193;
    @DexIgnore
    public static /* final */ int Theme_Design_Light_NoActionBar; // = 2131952194;
    @DexIgnore
    public static /* final */ int Theme_Design_NoActionBar; // = 2131952195;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents; // = 2131952196;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_BottomSheetDialog; // = 2131952197;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Bridge; // = 2131952198;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_CompactMenu; // = 2131952199;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight; // = 2131952200;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_BottomSheetDialog; // = 2131952201;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_Bridge; // = 2131952202;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_DarkActionBar; // = 2131952203;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_DarkActionBar_Bridge; // = 2131952204;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_Dialog; // = 2131952205;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_DialogWhenLarge; // = 2131952213;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_Dialog_Alert; // = 2131952206;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_Dialog_Alert_Bridge; // = 2131952207;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_Dialog_Bridge; // = 2131952208;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_Dialog_FixedSize; // = 2131952209;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_Dialog_FixedSize_Bridge; // = 2131952210;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_Dialog_MinWidth; // = 2131952211;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_Dialog_MinWidth_Bridge; // = 2131952212;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_NoActionBar; // = 2131952214;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DayNight_NoActionBar_Bridge; // = 2131952215;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Dialog; // = 2131952216;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_DialogWhenLarge; // = 2131952224;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Dialog_Alert; // = 2131952217;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Dialog_Alert_Bridge; // = 2131952218;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Dialog_Bridge; // = 2131952219;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Dialog_FixedSize; // = 2131952220;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Dialog_FixedSize_Bridge; // = 2131952221;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Dialog_MinWidth; // = 2131952222;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Dialog_MinWidth_Bridge; // = 2131952223;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light; // = 2131952225;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_BarSize; // = 2131952226;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_BottomSheetDialog; // = 2131952227;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_Bridge; // = 2131952228;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_DarkActionBar; // = 2131952229;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_DarkActionBar_Bridge; // = 2131952230;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_Dialog; // = 2131952231;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_DialogWhenLarge; // = 2131952239;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_Dialog_Alert; // = 2131952232;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_Dialog_Alert_Bridge; // = 2131952233;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_Dialog_Bridge; // = 2131952234;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_Dialog_FixedSize; // = 2131952235;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_Dialog_FixedSize_Bridge; // = 2131952236;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_Dialog_MinWidth; // = 2131952237;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_Dialog_MinWidth_Bridge; // = 2131952238;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_LargeTouch; // = 2131952240;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_NoActionBar; // = 2131952241;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_Light_NoActionBar_Bridge; // = 2131952242;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_NoActionBar; // = 2131952243;
    @DexIgnore
    public static /* final */ int Theme_MaterialComponents_NoActionBar_Bridge; // = 2131952244;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar; // = 2131952302;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_Solid; // = 2131952303;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_TabBar; // = 2131952304;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_TabText; // = 2131952305;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_TabView; // = 2131952306;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionButton; // = 2131952307;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionButton_CloseMode; // = 2131952308;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionButton_Overflow; // = 2131952309;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionMode; // = 2131952310;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActivityChooserView; // = 2131952311;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_AutoCompleteTextView; // = 2131952312;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button; // = 2131952313;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ButtonBar; // = 2131952319;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ButtonBar_AlertDialog; // = 2131952320;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Borderless; // = 2131952314;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Borderless_Colored; // = 2131952315;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_ButtonBar_AlertDialog; // = 2131952316;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Colored; // = 2131952317;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Small; // = 2131952318;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_CompoundButton_CheckBox; // = 2131952321;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_CompoundButton_RadioButton; // = 2131952322;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_CompoundButton_Switch; // = 2131952323;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_DrawerArrowToggle; // = 2131952324;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_DropDownItem_Spinner; // = 2131952325;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_EditText; // = 2131952326;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ImageButton; // = 2131952327;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar; // = 2131952328;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_Solid; // = 2131952329;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_Solid_Inverse; // = 2131952330;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabBar; // = 2131952331;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabBar_Inverse; // = 2131952332;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabText; // = 2131952333;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabText_Inverse; // = 2131952334;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabView; // = 2131952335;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabView_Inverse; // = 2131952336;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionButton; // = 2131952337;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionButton_CloseMode; // = 2131952338;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionButton_Overflow; // = 2131952339;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionMode_Inverse; // = 2131952340;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActivityChooserView; // = 2131952341;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_AutoCompleteTextView; // = 2131952342;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_DropDownItem_Spinner; // = 2131952343;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ListPopupWindow; // = 2131952344;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ListView_DropDown; // = 2131952345;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_PopupMenu; // = 2131952346;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_PopupMenu_Overflow; // = 2131952347;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_SearchView; // = 2131952348;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_Spinner_DropDown_ActionBar; // = 2131952349;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListMenuView; // = 2131952350;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListPopupWindow; // = 2131952351;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListView; // = 2131952352;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListView_DropDown; // = 2131952353;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListView_Menu; // = 2131952354;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_PopupMenu; // = 2131952355;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_PopupMenu_Overflow; // = 2131952356;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_PopupWindow; // = 2131952357;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ProgressBar; // = 2131952358;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ProgressBar_Horizontal; // = 2131952359;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_RatingBar; // = 2131952360;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_RatingBar_Indicator; // = 2131952361;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_RatingBar_Small; // = 2131952362;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SearchView; // = 2131952363;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SearchView_ActionBar; // = 2131952364;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SeekBar; // = 2131952365;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SeekBar_Discrete; // = 2131952366;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner; // = 2131952367;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner_DropDown; // = 2131952368;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner_DropDown_ActionBar; // = 2131952369;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner_Underlined; // = 2131952370;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_TextView; // = 2131952371;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_TextView_SpinnerItem; // = 2131952372;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Toolbar; // = 2131952373;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Toolbar_Button_Navigation; // = 2131952374;
    @DexIgnore
    public static /* final */ int Widget_Compat_NotificationActionContainer; // = 2131952375;
    @DexIgnore
    public static /* final */ int Widget_Compat_NotificationActionText; // = 2131952376;
    @DexIgnore
    public static /* final */ int Widget_Design_AppBarLayout; // = 2131952377;
    @DexIgnore
    public static /* final */ int Widget_Design_BottomNavigationView; // = 2131952378;
    @DexIgnore
    public static /* final */ int Widget_Design_BottomSheet_Modal; // = 2131952379;
    @DexIgnore
    public static /* final */ int Widget_Design_CollapsingToolbar; // = 2131952380;
    @DexIgnore
    public static /* final */ int Widget_Design_FloatingActionButton; // = 2131952381;
    @DexIgnore
    public static /* final */ int Widget_Design_NavigationView; // = 2131952382;
    @DexIgnore
    public static /* final */ int Widget_Design_ScrimInsetsFrameLayout; // = 2131952383;
    @DexIgnore
    public static /* final */ int Widget_Design_Snackbar; // = 2131952384;
    @DexIgnore
    public static /* final */ int Widget_Design_TabLayout; // = 2131952385;
    @DexIgnore
    public static /* final */ int Widget_Design_TextInputLayout; // = 2131952386;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_ActionBar_Primary; // = 2131952387;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_ActionBar_PrimarySurface; // = 2131952388;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_ActionBar_Solid; // = 2131952389;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_ActionBar_Surface; // = 2131952390;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_AppBarLayout_Primary; // = 2131952391;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_AppBarLayout_PrimarySurface; // = 2131952392;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_AppBarLayout_Surface; // = 2131952393;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_AutoCompleteTextView_FilledBox; // = 2131952394;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_AutoCompleteTextView_FilledBox_Dense; // = 2131952395;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_AutoCompleteTextView_OutlinedBox; // = 2131952396;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_AutoCompleteTextView_OutlinedBox_Dense; // = 2131952397;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Badge; // = 2131952398;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_BottomAppBar; // = 2131952399;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_BottomAppBar_Colored; // = 2131952400;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_BottomAppBar_PrimarySurface; // = 2131952401;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_BottomNavigationView; // = 2131952402;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_BottomNavigationView_Colored; // = 2131952403;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_BottomNavigationView_PrimarySurface; // = 2131952404;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_BottomSheet; // = 2131952405;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_BottomSheet_Modal; // = 2131952406;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Button; // = 2131952407;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Button_Icon; // = 2131952408;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Button_OutlinedButton; // = 2131952409;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Button_OutlinedButton_Icon; // = 2131952410;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Button_TextButton; // = 2131952411;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Button_TextButton_Dialog; // = 2131952412;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Button_TextButton_Dialog_Flush; // = 2131952413;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Button_TextButton_Dialog_Icon; // = 2131952414;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Button_TextButton_Icon; // = 2131952415;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Button_TextButton_Snackbar; // = 2131952416;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Button_UnelevatedButton; // = 2131952417;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Button_UnelevatedButton_Icon; // = 2131952418;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_CardView; // = 2131952419;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_CheckedTextView; // = 2131952420;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_ChipGroup; // = 2131952425;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Chip_Action; // = 2131952421;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Chip_Choice; // = 2131952422;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Chip_Entry; // = 2131952423;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Chip_Filter; // = 2131952424;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_CompoundButton_CheckBox; // = 2131952426;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_CompoundButton_RadioButton; // = 2131952427;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_CompoundButton_Switch; // = 2131952428;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_ExtendedFloatingActionButton; // = 2131952429;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_ExtendedFloatingActionButton_Icon; // = 2131952430;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_FloatingActionButton; // = 2131952431;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Light_ActionBar_Solid; // = 2131952432;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialButtonToggleGroup; // = 2131952433;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar; // = 2131952434;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_Day; // = 2131952435;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_DayTextView; // = 2131952439;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_Day_Invalid; // = 2131952436;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_Day_Selected; // = 2131952437;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_Day_Today; // = 2131952438;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_Fullscreen; // = 2131952440;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_HeaderConfirmButton; // = 2131952441;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_HeaderDivider; // = 2131952442;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_HeaderLayout; // = 2131952443;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_HeaderSelection; // = 2131952444;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_HeaderSelection_Fullscreen; // = 2131952445;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_HeaderTitle; // = 2131952446;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_HeaderToggleButton; // = 2131952447;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_Item; // = 2131952448;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_Year; // = 2131952449;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_Year_Selected; // = 2131952450;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_MaterialCalendar_Year_Today; // = 2131952451;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_NavigationView; // = 2131952452;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_PopupMenu; // = 2131952453;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_PopupMenu_ContextMenu; // = 2131952454;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_PopupMenu_ListPopupWindow; // = 2131952455;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_PopupMenu_Overflow; // = 2131952456;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Snackbar; // = 2131952457;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Snackbar_FullWidth; // = 2131952458;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TabLayout; // = 2131952459;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TabLayout_Colored; // = 2131952460;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TabLayout_PrimarySurface; // = 2131952461;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TextInputEditText_FilledBox; // = 2131952462;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TextInputEditText_FilledBox_Dense; // = 2131952463;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TextInputEditText_OutlinedBox; // = 2131952464;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TextInputEditText_OutlinedBox_Dense; // = 2131952465;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TextInputLayout_FilledBox; // = 2131952466;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TextInputLayout_FilledBox_Dense; // = 2131952467;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TextInputLayout_FilledBox_Dense_ExposedDropdownMenu; // = 2131952468;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TextInputLayout_FilledBox_ExposedDropdownMenu; // = 2131952469;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TextInputLayout_OutlinedBox; // = 2131952470;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TextInputLayout_OutlinedBox_Dense; // = 2131952471;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TextInputLayout_OutlinedBox_Dense_ExposedDropdownMenu; // = 2131952472;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TextInputLayout_OutlinedBox_ExposedDropdownMenu; // = 2131952473;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_TextView; // = 2131952474;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Toolbar; // = 2131952475;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Toolbar_Primary; // = 2131952476;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Toolbar_PrimarySurface; // = 2131952477;
    @DexIgnore
    public static /* final */ int Widget_MaterialComponents_Toolbar_Surface; // = 2131952478;
    @DexIgnore
    public static /* final */ int Widget_Support_CoordinatorLayout; // = 2131952479;
}
