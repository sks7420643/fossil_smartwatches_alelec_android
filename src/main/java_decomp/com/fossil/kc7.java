package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.workers.PushPendingDataWorker;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kc7 implements Factory<PushPendingDataWorker.b> {
    @DexIgnore
    public /* final */ Provider<ActivitiesRepository> a;
    @DexIgnore
    public /* final */ Provider<SummariesRepository> b;
    @DexIgnore
    public /* final */ Provider<SleepSessionsRepository> c;
    @DexIgnore
    public /* final */ Provider<SleepSummariesRepository> d;
    @DexIgnore
    public /* final */ Provider<GoalTrackingRepository> e;
    @DexIgnore
    public /* final */ Provider<HeartRateSampleRepository> f;
    @DexIgnore
    public /* final */ Provider<HeartRateSummaryRepository> g;
    @DexIgnore
    public /* final */ Provider<FitnessDataRepository> h;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> i;
    @DexIgnore
    public /* final */ Provider<An4> j;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> k;
    @DexIgnore
    public /* final */ Provider<ThirdPartyRepository> l;
    @DexIgnore
    public /* final */ Provider<Tt4> m;
    @DexIgnore
    public /* final */ Provider<FriendRepository> n;
    @DexIgnore
    public /* final */ Provider<FileRepository> o;
    @DexIgnore
    public /* final */ Provider<UserRepository> p;
    @DexIgnore
    public /* final */ Provider<WorkoutSettingRepository> q;
    @DexIgnore
    public /* final */ Provider<FCMRepository> r;
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> s;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> t;

    @DexIgnore
    public Kc7(Provider<ActivitiesRepository> provider, Provider<SummariesRepository> provider2, Provider<SleepSessionsRepository> provider3, Provider<SleepSummariesRepository> provider4, Provider<GoalTrackingRepository> provider5, Provider<HeartRateSampleRepository> provider6, Provider<HeartRateSummaryRepository> provider7, Provider<FitnessDataRepository> provider8, Provider<AlarmsRepository> provider9, Provider<An4> provider10, Provider<HybridPresetRepository> provider11, Provider<ThirdPartyRepository> provider12, Provider<Tt4> provider13, Provider<FriendRepository> provider14, Provider<FileRepository> provider15, Provider<UserRepository> provider16, Provider<WorkoutSettingRepository> provider17, Provider<FCMRepository> provider18, Provider<DianaWatchFaceRepository> provider19, Provider<PortfolioApp> provider20) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
        this.i = provider9;
        this.j = provider10;
        this.k = provider11;
        this.l = provider12;
        this.m = provider13;
        this.n = provider14;
        this.o = provider15;
        this.p = provider16;
        this.q = provider17;
        this.r = provider18;
        this.s = provider19;
        this.t = provider20;
    }

    @DexIgnore
    public static Kc7 a(Provider<ActivitiesRepository> provider, Provider<SummariesRepository> provider2, Provider<SleepSessionsRepository> provider3, Provider<SleepSummariesRepository> provider4, Provider<GoalTrackingRepository> provider5, Provider<HeartRateSampleRepository> provider6, Provider<HeartRateSummaryRepository> provider7, Provider<FitnessDataRepository> provider8, Provider<AlarmsRepository> provider9, Provider<An4> provider10, Provider<HybridPresetRepository> provider11, Provider<ThirdPartyRepository> provider12, Provider<Tt4> provider13, Provider<FriendRepository> provider14, Provider<FileRepository> provider15, Provider<UserRepository> provider16, Provider<WorkoutSettingRepository> provider17, Provider<FCMRepository> provider18, Provider<DianaWatchFaceRepository> provider19, Provider<PortfolioApp> provider20) {
        return new Kc7(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14, provider15, provider16, provider17, provider18, provider19, provider20);
    }

    @DexIgnore
    public static PushPendingDataWorker.b c(Provider<ActivitiesRepository> provider, Provider<SummariesRepository> provider2, Provider<SleepSessionsRepository> provider3, Provider<SleepSummariesRepository> provider4, Provider<GoalTrackingRepository> provider5, Provider<HeartRateSampleRepository> provider6, Provider<HeartRateSummaryRepository> provider7, Provider<FitnessDataRepository> provider8, Provider<AlarmsRepository> provider9, Provider<An4> provider10, Provider<HybridPresetRepository> provider11, Provider<ThirdPartyRepository> provider12, Provider<Tt4> provider13, Provider<FriendRepository> provider14, Provider<FileRepository> provider15, Provider<UserRepository> provider16, Provider<WorkoutSettingRepository> provider17, Provider<FCMRepository> provider18, Provider<DianaWatchFaceRepository> provider19, Provider<PortfolioApp> provider20) {
        return new PushPendingDataWorker.b(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14, provider15, provider16, provider17, provider18, provider19, provider20);
    }

    @DexIgnore
    public PushPendingDataWorker.b b() {
        return c(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p, this.q, this.r, this.s, this.t);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
