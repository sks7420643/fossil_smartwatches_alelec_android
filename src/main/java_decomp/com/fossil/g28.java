package com.fossil;

import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Flushable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G28 implements Closeable, Flushable {
    @DexIgnore
    public static /* final */ Pattern A; // = Pattern.compile("[a-z0-9_-]{1,120}");
    @DexIgnore
    public /* final */ Q38 b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ File d;
    @DexIgnore
    public /* final */ File e;
    @DexIgnore
    public /* final */ File f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public long h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public long j; // = 0;
    @DexIgnore
    public J48 k;
    @DexIgnore
    public /* final */ LinkedHashMap<String, Di> l; // = new LinkedHashMap<>(0, 0.75f, true);
    @DexIgnore
    public int m;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public long x; // = 0;
    @DexIgnore
    public /* final */ Executor y;
    @DexIgnore
    public /* final */ Runnable z; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void run() {
            boolean z = true;
            synchronized (G28.this) {
                if (G28.this.t) {
                    z = false;
                }
                if (!z && !G28.this.u) {
                    try {
                        G28.this.M();
                    } catch (IOException e) {
                        G28.this.v = true;
                    }
                    try {
                        if (G28.this.o()) {
                            G28.this.F();
                            G28.this.m = 0;
                        }
                    } catch (IOException e2) {
                        G28.this.w = true;
                        G28.this.k = S48.c(S48.b());
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends H28 {
        @DexIgnore
        public Bi(A58 a58) {
            super(a58);
        }

        @DexIgnore
        @Override // com.fossil.H28
        public void a(IOException iOException) {
            G28.this.s = true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ci {
        @DexIgnore
        public /* final */ Di a;
        @DexIgnore
        public /* final */ boolean[] b;
        @DexIgnore
        public boolean c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends H28 {
            @DexIgnore
            public Aii(A58 a58) {
                super(a58);
            }

            @DexIgnore
            @Override // com.fossil.H28
            public void a(IOException iOException) {
                synchronized (G28.this) {
                    Ci.this.c();
                }
            }
        }

        @DexIgnore
        public Ci(Di di) {
            this.a = di;
            this.b = di.e ? null : new boolean[G28.this.i];
        }

        @DexIgnore
        public void a() throws IOException {
            synchronized (G28.this) {
                if (!this.c) {
                    if (this.a.f == this) {
                        G28.this.b(this, false);
                    }
                    this.c = true;
                } else {
                    throw new IllegalStateException();
                }
            }
        }

        @DexIgnore
        public void b() throws IOException {
            synchronized (G28.this) {
                if (!this.c) {
                    if (this.a.f == this) {
                        G28.this.b(this, true);
                    }
                    this.c = true;
                } else {
                    throw new IllegalStateException();
                }
            }
        }

        @DexIgnore
        public void c() {
            if (this.a.f == this) {
                int i = 0;
                while (true) {
                    G28 g28 = G28.this;
                    if (i < g28.i) {
                        try {
                            g28.b.f(this.a.d[i]);
                        } catch (IOException e) {
                        }
                        i++;
                    } else {
                        this.a.f = null;
                        return;
                    }
                }
            }
        }

        @DexIgnore
        public A58 d(int i) {
            A58 b2;
            synchronized (G28.this) {
                if (this.c) {
                    throw new IllegalStateException();
                } else if (this.a.f != this) {
                    b2 = S48.b();
                } else {
                    if (!this.a.e) {
                        this.b[i] = true;
                    }
                    try {
                        b2 = new Aii(G28.this.b.b(this.a.d[i]));
                    } catch (FileNotFoundException e) {
                        b2 = S48.b();
                    }
                }
            }
            return b2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ long[] b;
        @DexIgnore
        public /* final */ File[] c;
        @DexIgnore
        public /* final */ File[] d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public Ci f;
        @DexIgnore
        public long g;

        @DexIgnore
        public Di(String str) {
            this.a = str;
            int i = G28.this.i;
            this.b = new long[i];
            this.c = new File[i];
            this.d = new File[i];
            StringBuilder sb = new StringBuilder(str);
            sb.append('.');
            int length = sb.length();
            for (int i2 = 0; i2 < G28.this.i; i2++) {
                sb.append(i2);
                this.c[i2] = new File(G28.this.c, sb.toString());
                sb.append(".tmp");
                this.d[i2] = new File(G28.this.c, sb.toString());
                sb.setLength(length);
            }
        }

        @DexIgnore
        public final IOException a(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        @DexIgnore
        public void b(String[] strArr) throws IOException {
            if (strArr.length == G28.this.i) {
                for (int i = 0; i < strArr.length; i++) {
                    try {
                        this.b[i] = Long.parseLong(strArr[i]);
                    } catch (NumberFormatException e2) {
                        a(strArr);
                        throw null;
                    }
                }
                return;
            }
            a(strArr);
            throw null;
        }

        @DexIgnore
        public Ei c() {
            int i = 0;
            if (Thread.holdsLock(G28.this)) {
                C58[] c58Arr = new C58[G28.this.i];
                long[] jArr = (long[]) this.b.clone();
                for (int i2 = 0; i2 < G28.this.i; i2++) {
                    try {
                        c58Arr[i2] = G28.this.b.a(this.c[i2]);
                    } catch (FileNotFoundException e2) {
                        while (i < G28.this.i && c58Arr[i] != null) {
                            B28.g(c58Arr[i]);
                            i++;
                        }
                        try {
                            G28.this.L(this);
                        } catch (IOException e3) {
                        }
                        return null;
                    }
                }
                return new Ei(this.a, this.g, c58Arr, jArr);
            }
            throw new AssertionError();
        }

        @DexIgnore
        public void d(J48 j48) throws IOException {
            for (long j : this.b) {
                j48.v(32).k0(j);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ei implements Closeable {
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ long c;
        @DexIgnore
        public /* final */ C58[] d;

        @DexIgnore
        public Ei(String str, long j, C58[] c58Arr, long[] jArr) {
            this.b = str;
            this.c = j;
            this.d = c58Arr;
        }

        @DexIgnore
        public Ci a() throws IOException {
            return G28.this.j(this.b, this.c);
        }

        @DexIgnore
        public C58 b(int i) {
            return this.d[i];
        }

        @DexIgnore
        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            for (C58 c58 : this.d) {
                B28.g(c58);
            }
        }
    }

    @DexIgnore
    public G28(Q38 q38, File file, int i2, int i3, long j2, Executor executor) {
        this.b = q38;
        this.c = file;
        this.g = i2;
        this.d = new File(file, "journal");
        this.e = new File(file, "journal.tmp");
        this.f = new File(file, "journal.bkp");
        this.i = i3;
        this.h = j2;
        this.y = executor;
    }

    @DexIgnore
    public static G28 c(Q38 q38, File file, int i2, int i3, long j2) {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 > 0) {
            return new G28(q38, file, i2, i3, j2, new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), B28.G("OkHttp DiskLruCache", true)));
        } else {
            throw new IllegalArgumentException("valueCount <= 0");
        }
    }

    @DexIgnore
    public final J48 A() throws FileNotFoundException {
        return S48.c(new Bi(this.b.g(this.d)));
    }

    @DexIgnore
    public final void B() throws IOException {
        this.b.f(this.e);
        Iterator<Di> it = this.l.values().iterator();
        while (it.hasNext()) {
            Di next = it.next();
            if (next.f == null) {
                for (int i2 = 0; i2 < this.i; i2++) {
                    this.j += next.b[i2];
                }
            } else {
                next.f = null;
                for (int i3 = 0; i3 < this.i; i3++) {
                    this.b.f(next.c[i3]);
                    this.b.f(next.d[i3]);
                }
                it.remove();
            }
        }
    }

    @DexIgnore
    public final void C() throws IOException {
        K48 d2 = S48.d(this.b.a(this.d));
        try {
            String U = d2.U();
            String U2 = d2.U();
            String U3 = d2.U();
            String U4 = d2.U();
            String U5 = d2.U();
            if (!"libcore.io.DiskLruCache".equals(U) || !"1".equals(U2) || !Integer.toString(this.g).equals(U3) || !Integer.toString(this.i).equals(U4) || !"".equals(U5)) {
                throw new IOException("unexpected journal header: [" + U + ", " + U2 + ", " + U4 + ", " + U5 + "]");
            }
            int i2 = 0;
            while (true) {
                try {
                    D(d2.U());
                    i2++;
                } catch (EOFException e2) {
                    this.m = i2 - this.l.size();
                    if (!d2.u()) {
                        F();
                    } else {
                        this.k = A();
                    }
                    return;
                }
            }
        } finally {
            B28.g(d2);
        }
    }

    @DexIgnore
    public final void D(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i2 = indexOf + 1;
            int indexOf2 = str.indexOf(32, i2);
            if (indexOf2 == -1) {
                String substring = str.substring(i2);
                if (indexOf != 6 || !str.startsWith("REMOVE")) {
                    str2 = substring;
                } else {
                    this.l.remove(substring);
                    return;
                }
            } else {
                str2 = str.substring(i2, indexOf2);
            }
            Di di = this.l.get(str2);
            if (di == null) {
                di = new Di(str2);
                this.l.put(str2, di);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(" ");
                di.e = true;
                di.f = null;
                di.b(split);
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                di.f = new Ci(di);
            } else if (indexOf2 != -1 || indexOf != 4 || !str.startsWith("READ")) {
                throw new IOException("unexpected journal line: " + str);
            }
        } else {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public void F() throws IOException {
        synchronized (this) {
            if (this.k != null) {
                this.k.close();
            }
            J48 c2 = S48.c(this.b.b(this.e));
            try {
                c2.E("libcore.io.DiskLruCache").v(10);
                c2.E("1").v(10);
                c2.k0((long) this.g).v(10);
                c2.k0((long) this.i).v(10);
                c2.v(10);
                for (Di di : this.l.values()) {
                    if (di.f != null) {
                        c2.E("DIRTY").v(32);
                        c2.E(di.a);
                        c2.v(10);
                    } else {
                        c2.E("CLEAN").v(32);
                        c2.E(di.a);
                        di.d(c2);
                        c2.v(10);
                    }
                }
                c2.close();
                if (this.b.d(this.d)) {
                    this.b.e(this.d, this.f);
                }
                this.b.e(this.e, this.d);
                this.b.f(this.f);
                this.k = A();
                this.s = false;
                this.w = false;
            } catch (Throwable th) {
                c2.close();
                throw th;
            }
        }
    }

    @DexIgnore
    public boolean G(String str) throws IOException {
        synchronized (this) {
            m();
            a();
            P(str);
            Di di = this.l.get(str);
            if (di == null) {
                return false;
            }
            boolean L = L(di);
            if (L && this.j <= this.h) {
                this.v = false;
            }
            return L;
        }
    }

    @DexIgnore
    public boolean L(Di di) throws IOException {
        Ci ci = di.f;
        if (ci != null) {
            ci.c();
        }
        for (int i2 = 0; i2 < this.i; i2++) {
            this.b.f(di.c[i2]);
            long j2 = this.j;
            long[] jArr = di.b;
            this.j = j2 - jArr[i2];
            jArr[i2] = 0;
        }
        this.m++;
        this.k.E("REMOVE").v(32).E(di.a).v(10);
        this.l.remove(di.a);
        if (!o()) {
            return true;
        }
        this.y.execute(this.z);
        return true;
    }

    @DexIgnore
    public void M() throws IOException {
        while (this.j > this.h) {
            L(this.l.values().iterator().next());
        }
        this.v = false;
    }

    @DexIgnore
    public final void P(String str) {
        if (!A.matcher(str).matches()) {
            throw new IllegalArgumentException("keys must match regex [a-z0-9_-]{1,120}: \"" + str + "\"");
        }
    }

    @DexIgnore
    public final void a() {
        synchronized (this) {
            if (isClosed()) {
                throw new IllegalStateException("cache is closed");
            }
        }
    }

    @DexIgnore
    public void b(Ci ci, boolean z2) throws IOException {
        synchronized (this) {
            Di di = ci.a;
            if (di.f == ci) {
                if (z2 && !di.e) {
                    for (int i2 = 0; i2 < this.i; i2++) {
                        if (!ci.b[i2]) {
                            ci.a();
                            throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                        } else if (!this.b.d(di.d[i2])) {
                            ci.a();
                            return;
                        }
                    }
                }
                for (int i3 = 0; i3 < this.i; i3++) {
                    File file = di.d[i3];
                    if (!z2) {
                        this.b.f(file);
                    } else if (this.b.d(file)) {
                        File file2 = di.c[i3];
                        this.b.e(file, file2);
                        long j2 = di.b[i3];
                        long h2 = this.b.h(file2);
                        di.b[i3] = h2;
                        this.j = (this.j - j2) + h2;
                    }
                }
                this.m++;
                di.f = null;
                if (di.e || z2) {
                    di.e = true;
                    this.k.E("CLEAN").v(32);
                    this.k.E(di.a);
                    di.d(this.k);
                    this.k.v(10);
                    if (z2) {
                        long j3 = this.x;
                        this.x = 1 + j3;
                        di.g = j3;
                    }
                } else {
                    this.l.remove(di.a);
                    this.k.E("REMOVE").v(32);
                    this.k.E(di.a);
                    this.k.v(10);
                }
                this.k.flush();
                if (this.j > this.h || o()) {
                    this.y.execute(this.z);
                }
                return;
            }
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        synchronized (this) {
            if (!this.t || this.u) {
                this.u = true;
                return;
            }
            Di[] diArr = (Di[]) this.l.values().toArray(new Di[this.l.size()]);
            for (Di di : diArr) {
                if (di.f != null) {
                    di.f.a();
                }
            }
            M();
            this.k.close();
            this.k = null;
            this.u = true;
        }
    }

    @DexIgnore
    public void f() throws IOException {
        close();
        this.b.c(this.c);
    }

    @DexIgnore
    @Override // java.io.Flushable
    public void flush() throws IOException {
        synchronized (this) {
            if (this.t) {
                a();
                M();
                this.k.flush();
            }
        }
    }

    @DexIgnore
    public Ci h(String str) throws IOException {
        return j(str, -1);
    }

    @DexIgnore
    public boolean isClosed() {
        boolean z2;
        synchronized (this) {
            z2 = this.u;
        }
        return z2;
    }

    @DexIgnore
    public Ci j(String str, long j2) throws IOException {
        Di di;
        synchronized (this) {
            m();
            a();
            P(str);
            Di di2 = this.l.get(str);
            if (j2 != -1 && (di2 == null || di2.g != j2)) {
                return null;
            }
            if (di2 != null && di2.f != null) {
                return null;
            }
            if (this.v || this.w) {
                this.y.execute(this.z);
                return null;
            }
            this.k.E("DIRTY").v(32).E(str).v(10);
            this.k.flush();
            if (this.s) {
                return null;
            }
            if (di2 == null) {
                Di di3 = new Di(str);
                this.l.put(str, di3);
                di = di3;
            } else {
                di = di2;
            }
            Ci ci = new Ci(di);
            di.f = ci;
            return ci;
        }
    }

    @DexIgnore
    public void k() throws IOException {
        synchronized (this) {
            m();
            for (Di di : (Di[]) this.l.values().toArray(new Di[this.l.size()])) {
                L(di);
            }
            this.v = false;
        }
    }

    @DexIgnore
    public Ei l(String str) throws IOException {
        synchronized (this) {
            m();
            a();
            P(str);
            Di di = this.l.get(str);
            if (di == null || !di.e) {
                return null;
            }
            Ei c2 = di.c();
            if (c2 == null) {
                return null;
            }
            this.m++;
            this.k.E("READ").v(32).E(str).v(10);
            if (o()) {
                this.y.execute(this.z);
            }
            return c2;
        }
    }

    @DexIgnore
    public void m() throws IOException {
        synchronized (this) {
            if (!this.t) {
                if (this.b.d(this.f)) {
                    if (this.b.d(this.d)) {
                        this.b.f(this.f);
                    } else {
                        this.b.e(this.f, this.d);
                    }
                }
                if (this.b.d(this.d)) {
                    try {
                        C();
                        B();
                        this.t = true;
                        return;
                    } catch (IOException e2) {
                        W38 j2 = W38.j();
                        j2.q(5, "DiskLruCache " + this.c + " is corrupt: " + e2.getMessage() + ", removing", e2);
                        f();
                        this.u = false;
                    } catch (Throwable th) {
                        this.u = false;
                        throw th;
                    }
                }
                F();
                this.t = true;
            }
        }
    }

    @DexIgnore
    public boolean o() {
        int i2 = this.m;
        return i2 >= 2000 && i2 >= this.l.size();
    }
}
