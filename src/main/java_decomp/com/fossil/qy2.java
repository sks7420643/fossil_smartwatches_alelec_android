package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qy2<E> extends Sx2<E> {
    @DexIgnore
    public static /* final */ Sx2<Object> zza; // = new Qy2(new Object[0], 0);
    @DexIgnore
    public /* final */ transient Object[] d;
    @DexIgnore
    public /* final */ transient int e;

    @DexIgnore
    public Qy2(Object[] objArr, int i) {
        this.d = objArr;
        this.e = i;
    }

    @DexIgnore
    @Override // java.util.List
    public final E get(int i) {
        Sw2.a(i, this.e);
        return (E) this.d[i];
    }

    @DexIgnore
    public final int size() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Sx2, com.fossil.Tx2
    public final int zzb(Object[] objArr, int i) {
        System.arraycopy(this.d, 0, objArr, i, this.e);
        return this.e + i;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final Object[] zze() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final int zzf() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final int zzg() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final boolean zzh() {
        return false;
    }
}
