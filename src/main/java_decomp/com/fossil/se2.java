package com.fossil;

import android.util.Log;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Se2 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public Se2(String str, String str2) {
        this.b = str2;
        this.a = str;
        new Hc2(str);
        int i = 2;
        while (7 >= i && !Log.isLoggable(this.a, i)) {
            i++;
        }
        this.c = i;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Se2(java.lang.String r7, java.lang.String... r8) {
        /*
            r6 = this;
            int r0 = r8.length
            if (r0 != 0) goto L_0x0009
            java.lang.String r0 = ""
        L_0x0005:
            r6.<init>(r7, r0)
            return
        L_0x0009:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r0 = 91
            r1.append(r0)
            int r2 = r8.length
            r0 = 0
        L_0x0015:
            if (r0 >= r2) goto L_0x002b
            r3 = r8[r0]
            int r4 = r1.length()
            r5 = 1
            if (r4 <= r5) goto L_0x0025
            java.lang.String r4 = ","
            r1.append(r4)
        L_0x0025:
            r1.append(r3)
            int r0 = r0 + 1
            goto L_0x0015
        L_0x002b:
            r0 = 93
            r1.append(r0)
            r0 = 32
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Se2.<init>(java.lang.String, java.lang.String[]):void");
    }

    @DexIgnore
    public void a(String str, Object... objArr) {
        if (d(3)) {
            Log.d(this.a, c(str, objArr));
        }
    }

    @DexIgnore
    public void b(String str, Object... objArr) {
        Log.e(this.a, c(str, objArr));
    }

    @DexIgnore
    public String c(String str, Object... objArr) {
        if (objArr != null && objArr.length > 0) {
            str = String.format(Locale.US, str, objArr);
        }
        return this.b.concat(str);
    }

    @DexIgnore
    public boolean d(int i) {
        return this.c <= i;
    }
}
