package com.fossil;

import com.mapped.Wg6;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S48 {
    @DexIgnore
    public static final A58 a(File file) throws FileNotFoundException {
        Wg6.c(file, "$this$appendingSink");
        return h(new FileOutputStream(file, true));
    }

    @DexIgnore
    public static final A58 b() {
        return new H48();
    }

    @DexIgnore
    public static final J48 c(A58 a58) {
        Wg6.c(a58, "$this$buffer");
        return new V48(a58);
    }

    @DexIgnore
    public static final K48 d(C58 c58) {
        Wg6.c(c58, "$this$buffer");
        return new W48(c58);
    }

    @DexIgnore
    public static final boolean e(AssertionError assertionError) {
        Wg6.c(assertionError, "$this$isAndroidGetsocknameError");
        if (assertionError.getCause() == null) {
            return false;
        }
        String message = assertionError.getMessage();
        return message != null ? Wt7.v(message, "getsockname failed", false, 2, null) : false;
    }

    @DexIgnore
    public static final A58 f(File file) throws FileNotFoundException {
        return j(file, false, 1, null);
    }

    @DexIgnore
    public static final A58 g(File file, boolean z) throws FileNotFoundException {
        Wg6.c(file, "$this$sink");
        return h(new FileOutputStream(file, z));
    }

    @DexIgnore
    public static final A58 h(OutputStream outputStream) {
        Wg6.c(outputStream, "$this$sink");
        return new T48(outputStream, new D58());
    }

    @DexIgnore
    public static final A58 i(Socket socket) throws IOException {
        Wg6.c(socket, "$this$sink");
        B58 b58 = new B58(socket);
        OutputStream outputStream = socket.getOutputStream();
        Wg6.b(outputStream, "getOutputStream()");
        return b58.v(new T48(outputStream, b58));
    }

    @DexIgnore
    public static /* synthetic */ A58 j(File file, boolean z, int i, Object obj) throws FileNotFoundException {
        if ((i & 1) != 0) {
            z = false;
        }
        return g(file, z);
    }

    @DexIgnore
    public static final C58 k(File file) throws FileNotFoundException {
        Wg6.c(file, "$this$source");
        return l(new FileInputStream(file));
    }

    @DexIgnore
    public static final C58 l(InputStream inputStream) {
        Wg6.c(inputStream, "$this$source");
        return new R48(inputStream, new D58());
    }

    @DexIgnore
    public static final C58 m(Socket socket) throws IOException {
        Wg6.c(socket, "$this$source");
        B58 b58 = new B58(socket);
        InputStream inputStream = socket.getInputStream();
        Wg6.b(inputStream, "getInputStream()");
        return b58.w(new R48(inputStream, b58));
    }
}
