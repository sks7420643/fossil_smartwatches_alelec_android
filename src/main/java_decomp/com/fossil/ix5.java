package com.fossil;

import android.database.Cursor;
import android.widget.Filter;
import com.mapped.Rc6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ix5 extends Filter {
    @DexIgnore
    public Ai a;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(Cursor cursor);

        @DexIgnore
        Cursor b();

        @DexIgnore
        CharSequence d(Cursor cursor);

        @DexIgnore
        Cursor e(CharSequence charSequence);
    }

    @DexIgnore
    public Ix5(Ai ai) {
        Wg6.c(ai, "mClient");
        this.a = ai;
    }

    @DexIgnore
    public CharSequence convertResultToString(Object obj) {
        Wg6.c(obj, "resultValue");
        return this.a.d((Cursor) obj);
    }

    @DexIgnore
    public Filter.FilterResults performFiltering(CharSequence charSequence) {
        Wg6.c(charSequence, "constraint");
        Cursor e = this.a.e(charSequence);
        Filter.FilterResults filterResults = new Filter.FilterResults();
        if (e != null) {
            filterResults.count = e.getCount();
            filterResults.values = e;
        } else {
            filterResults.count = 0;
            filterResults.values = null;
        }
        return filterResults;
    }

    @DexIgnore
    public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
        Wg6.c(charSequence, "constraint");
        Wg6.c(filterResults, "results");
        Cursor b = this.a.b();
        Object obj = filterResults.values;
        if (obj != null && (!Wg6.a(obj, b))) {
            Ai ai = this.a;
            Object obj2 = filterResults.values;
            if (obj2 != null) {
                ai.a((Cursor) obj2);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type android.database.Cursor");
        }
    }
}
