package com.fossil;

import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mz6 implements MembersInjector<SignUpActivity> {
    @DexIgnore
    public static void a(SignUpActivity signUpActivity, Un5 un5) {
        signUpActivity.B = un5;
    }

    @DexIgnore
    public static void b(SignUpActivity signUpActivity, Vn5 vn5) {
        signUpActivity.C = vn5;
    }

    @DexIgnore
    public static void c(SignUpActivity signUpActivity, MFLoginWechatManager mFLoginWechatManager) {
        signUpActivity.E = mFLoginWechatManager;
    }

    @DexIgnore
    public static void d(SignUpActivity signUpActivity, Xn5 xn5) {
        signUpActivity.D = xn5;
    }

    @DexIgnore
    public static void e(SignUpActivity signUpActivity, SignUpPresenter signUpPresenter) {
        signUpActivity.A = signUpPresenter;
    }
}
