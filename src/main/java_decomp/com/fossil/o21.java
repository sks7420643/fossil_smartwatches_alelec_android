package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class O21 extends K21<F21> {
    @DexIgnore
    public O21(Context context, K41 k41) {
        super(W21.c(context, k41).d());
    }

    @DexIgnore
    @Override // com.fossil.K21
    public boolean b(O31 o31) {
        return o31.j.b() == Y01.UNMETERED;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.K21
    public /* bridge */ /* synthetic */ boolean c(F21 f21) {
        return i(f21);
    }

    @DexIgnore
    public boolean i(F21 f21) {
        return !f21.a() || f21.b();
    }
}
