package com.fossil;

import androidx.lifecycle.LiveData;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fu4 {
    @DexIgnore
    public /* final */ Jt4 a;

    @DexIgnore
    public Fu4(Jt4 jt4) {
        Wg6.c(jt4, "dao");
        this.a = jt4;
    }

    @DexIgnore
    public final void a() {
        this.a.a();
    }

    @DexIgnore
    public final long b(It4 it4) {
        Wg6.c(it4, "profile");
        return this.a.c(it4);
    }

    @DexIgnore
    public final It4 c() {
        return this.a.d();
    }

    @DexIgnore
    public final LiveData<It4> d() {
        return this.a.b();
    }
}
