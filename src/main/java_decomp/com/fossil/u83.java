package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U83 implements R83 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a; // = new Hw2(Yv2.a("com.google.android.gms.measurement")).d("measurement.scheduler.task_thread.cleanup_on_exit", false);

    @DexIgnore
    @Override // com.fossil.R83
    public final boolean zza() {
        return a.o().booleanValue();
    }
}
