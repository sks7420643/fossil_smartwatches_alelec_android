package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z42 extends Q42 {
    @DexIgnore
    public /* final */ /* synthetic */ Y42 b;

    @DexIgnore
    public Z42(Y42 y42) {
        this.b = y42;
    }

    @DexIgnore
    @Override // com.fossil.Q42, com.fossil.E52
    public final void W(Status status) throws RemoteException {
        this.b.j(status);
    }
}
