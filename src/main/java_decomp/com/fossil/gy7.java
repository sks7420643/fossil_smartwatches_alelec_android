package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lk6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gy7 extends Hy7 implements Tv7 {
    @DexIgnore
    public volatile Gy7 _immediate;
    @DexIgnore
    public /* final */ Gy7 c;
    @DexIgnore
    public /* final */ Handler d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Dw7 {
        @DexIgnore
        public /* final */ /* synthetic */ Gy7 b;
        @DexIgnore
        public /* final */ /* synthetic */ Runnable c;

        @DexIgnore
        public Ai(Gy7 gy7, Runnable runnable) {
            this.b = gy7;
            this.c = runnable;
        }

        @DexIgnore
        @Override // com.fossil.Dw7
        public void dispose() {
            this.b.d.removeCallbacks(this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Gy7 b;
        @DexIgnore
        public /* final */ /* synthetic */ Lk6 c;

        @DexIgnore
        public Bi(Gy7 gy7, Lk6 lk6) {
            this.b = gy7;
            this.c = lk6;
        }

        @DexIgnore
        public final void run() {
            this.c.f(this.b, Cd6.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Qq7 implements Hg6<Throwable, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Runnable $block;
        @DexIgnore
        public /* final */ /* synthetic */ Gy7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Gy7 gy7, Runnable runnable) {
            super(1);
            this.this$0 = gy7;
            this.$block = runnable;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
            invoke(th);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(Throwable th) {
            this.this$0.d.removeCallbacks(this.$block);
        }
    }

    @DexIgnore
    public Gy7(Handler handler, String str) {
        this(handler, str, false);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Gy7(Handler handler, String str, boolean z) {
        super(null);
        Gy7 gy7 = null;
        this.d = handler;
        this.e = str;
        this.f = z;
        this._immediate = z ? this : gy7;
        Gy7 gy72 = this._immediate;
        if (gy72 == null) {
            gy72 = new Gy7(this.d, this.e, true);
            this._immediate = gy72;
        }
        this.c = gy72;
    }

    @DexIgnore
    @Override // com.fossil.Tv7, com.fossil.Hy7
    public Dw7 G(long j, Runnable runnable) {
        this.d.postDelayed(runnable, Bs7.h(j, 4611686018427387903L));
        return new Ai(this, runnable);
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public void M(Af6 af6, Runnable runnable) {
        this.d.post(runnable);
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public boolean Q(Af6 af6) {
        return !this.f || (Wg6.a(Looper.myLooper(), this.d.getLooper()) ^ true);
    }

    @DexIgnore
    @Override // com.fossil.Jx7
    public /* bridge */ /* synthetic */ Jx7 S() {
        return V();
    }

    @DexIgnore
    public Gy7 V() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof Gy7) && ((Gy7) obj).d == this.d;
    }

    @DexIgnore
    @Override // com.fossil.Tv7
    public void f(long j, Lk6<? super Cd6> lk6) {
        Bi bi = new Bi(this, lk6);
        this.d.postDelayed(bi, Bs7.h(j, 4611686018427387903L));
        lk6.e(new Ci(this, bi));
    }

    @DexIgnore
    public int hashCode() {
        return System.identityHashCode(this.d);
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public String toString() {
        String str = this.e;
        if (str == null) {
            return this.d.toString();
        }
        if (!this.f) {
            return str;
        }
        return this.e + " [immediate]";
    }
}
