package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E37 implements Factory<VerifySecretKeyUseCase> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;
    @DexIgnore
    public /* final */ Provider<Q27> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;

    @DexIgnore
    public E37(Provider<DeviceRepository> provider, Provider<UserRepository> provider2, Provider<Q27> provider3, Provider<PortfolioApp> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static E37 a(Provider<DeviceRepository> provider, Provider<UserRepository> provider2, Provider<Q27> provider3, Provider<PortfolioApp> provider4) {
        return new E37(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static VerifySecretKeyUseCase c(DeviceRepository deviceRepository, UserRepository userRepository, Q27 q27, PortfolioApp portfolioApp) {
        return new VerifySecretKeyUseCase(deviceRepository, userRepository, q27, portfolioApp);
    }

    @DexIgnore
    public VerifySecretKeyUseCase b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
