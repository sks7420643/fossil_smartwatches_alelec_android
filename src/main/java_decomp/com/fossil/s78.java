package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class S78 {
    @DexIgnore
    public static /* final */ S78 b; // = new S78();
    @DexIgnore
    public static String c; // = "1.6.99";
    @DexIgnore
    public static /* final */ String d; // = N78.class.getName();
    @DexIgnore
    public /* final */ D78 a; // = new N78();

    @DexIgnore
    public static final S78 c() {
        return b;
    }

    @DexIgnore
    public D78 a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return d;
    }
}
