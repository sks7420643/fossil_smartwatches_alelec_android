package com.fossil;

import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Al5 {
    @DexIgnore
    public static Al5 b;
    @DexIgnore
    public /* final */ int a;

    @DexIgnore
    public Al5() {
        WindowManager windowManager = (WindowManager) PortfolioApp.d0.getSystemService("window");
        if (windowManager != null) {
            Display defaultDisplay = windowManager.getDefaultDisplay();
            Point point = new Point();
            defaultDisplay.getSize(point);
            this.a = point.x;
            return;
        }
        this.a = 0;
    }

    @DexIgnore
    public static Al5 a() {
        if (b == null) {
            b = new Al5();
        }
        return b;
    }

    @DexIgnore
    public int b() {
        return this.a;
    }
}
