package com.fossil;

import com.facebook.internal.Utility;
import com.fossil.Q18;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X08 {
    @DexIgnore
    public /* final */ Q18 a;
    @DexIgnore
    public /* final */ L18 b;
    @DexIgnore
    public /* final */ SocketFactory c;
    @DexIgnore
    public /* final */ Y08 d;
    @DexIgnore
    public /* final */ List<T18> e;
    @DexIgnore
    public /* final */ List<G18> f;
    @DexIgnore
    public /* final */ ProxySelector g;
    @DexIgnore
    public /* final */ Proxy h;
    @DexIgnore
    public /* final */ SSLSocketFactory i;
    @DexIgnore
    public /* final */ HostnameVerifier j;
    @DexIgnore
    public /* final */ C18 k;

    @DexIgnore
    public X08(String str, int i2, L18 l18, SocketFactory socketFactory, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier, C18 c18, Y08 y08, Proxy proxy, List<T18> list, List<G18> list2, ProxySelector proxySelector) {
        Q18.Ai ai = new Q18.Ai();
        ai.s(sSLSocketFactory != null ? Utility.URL_SCHEME : "http");
        ai.g(str);
        ai.n(i2);
        this.a = ai.c();
        if (l18 != null) {
            this.b = l18;
            if (socketFactory != null) {
                this.c = socketFactory;
                if (y08 != null) {
                    this.d = y08;
                    if (list != null) {
                        this.e = B28.t(list);
                        if (list2 != null) {
                            this.f = B28.t(list2);
                            if (proxySelector != null) {
                                this.g = proxySelector;
                                this.h = proxy;
                                this.i = sSLSocketFactory;
                                this.j = hostnameVerifier;
                                this.k = c18;
                                return;
                            }
                            throw new NullPointerException("proxySelector == null");
                        }
                        throw new NullPointerException("connectionSpecs == null");
                    }
                    throw new NullPointerException("protocols == null");
                }
                throw new NullPointerException("proxyAuthenticator == null");
            }
            throw new NullPointerException("socketFactory == null");
        }
        throw new NullPointerException("dns == null");
    }

    @DexIgnore
    public C18 a() {
        return this.k;
    }

    @DexIgnore
    public List<G18> b() {
        return this.f;
    }

    @DexIgnore
    public L18 c() {
        return this.b;
    }

    @DexIgnore
    public boolean d(X08 x08) {
        return this.b.equals(x08.b) && this.d.equals(x08.d) && this.e.equals(x08.e) && this.f.equals(x08.f) && this.g.equals(x08.g) && B28.q(this.h, x08.h) && B28.q(this.i, x08.i) && B28.q(this.j, x08.j) && B28.q(this.k, x08.k) && l().z() == x08.l().z();
    }

    @DexIgnore
    public HostnameVerifier e() {
        return this.j;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof X08) {
            X08 x08 = (X08) obj;
            return this.a.equals(x08.a) && d(x08);
        }
    }

    @DexIgnore
    public List<T18> f() {
        return this.e;
    }

    @DexIgnore
    public Proxy g() {
        return this.h;
    }

    @DexIgnore
    public Y08 h() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        int hashCode = this.a.hashCode();
        int hashCode2 = this.b.hashCode();
        int hashCode3 = this.d.hashCode();
        int hashCode4 = this.e.hashCode();
        int hashCode5 = this.f.hashCode();
        int hashCode6 = this.g.hashCode();
        Proxy proxy = this.h;
        int hashCode7 = proxy != null ? proxy.hashCode() : 0;
        SSLSocketFactory sSLSocketFactory = this.i;
        int hashCode8 = sSLSocketFactory != null ? sSLSocketFactory.hashCode() : 0;
        HostnameVerifier hostnameVerifier = this.j;
        int hashCode9 = hostnameVerifier != null ? hostnameVerifier.hashCode() : 0;
        C18 c18 = this.k;
        if (c18 != null) {
            i2 = c18.hashCode();
        }
        return ((((((hashCode7 + ((((((((((((hashCode + 527) * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31)) * 31) + hashCode8) * 31) + hashCode9) * 31) + i2;
    }

    @DexIgnore
    public ProxySelector i() {
        return this.g;
    }

    @DexIgnore
    public SocketFactory j() {
        return this.c;
    }

    @DexIgnore
    public SSLSocketFactory k() {
        return this.i;
    }

    @DexIgnore
    public Q18 l() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Address{");
        sb.append(this.a.m());
        sb.append(":");
        sb.append(this.a.z());
        if (this.h != null) {
            sb.append(", proxy=");
            sb.append(this.h);
        } else {
            sb.append(", proxySelector=");
            sb.append(this.g);
        }
        sb.append("}");
        return sb.toString();
    }
}
