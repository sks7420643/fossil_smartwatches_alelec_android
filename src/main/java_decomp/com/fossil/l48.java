package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.facebook.internal.FileLruCache;
import com.facebook.internal.Utility;
import com.google.maps.internal.UrlSigner;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.joda.time.DateTimeFieldType;

public class L48 implements Serializable, Comparable<L48> {
    public static final Ai Companion = new Ai(null);
    public static final L48 EMPTY = new L48(new byte[0]);
    public static final long serialVersionUID = 1;
    public transient int b;
    public transient String c;
    public final byte[] data;

    public static final class Ai {
        public Ai() {
        }

        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        public final L48 a(String str) {
            Wg6.c(str, "$this$decodeBase64");
            byte[] a2 = D48.a(str);
            if (a2 != null) {
                return new L48(a2);
            }
            return null;
        }

        public final L48 b(String str) {
            Wg6.c(str, "$this$decodeHex");
            if (str.length() % 2 == 0) {
                int length = str.length() / 2;
                byte[] bArr = new byte[length];
                for (int i = 0; i < length; i++) {
                    int i2 = i * 2;
                    bArr[i] = (byte) ((byte) (F58.b(str.charAt(i2 + 1)) + (F58.b(str.charAt(i2)) << 4)));
                }
                return new L48(bArr);
            }
            throw new IllegalArgumentException(("Unexpected hex string: " + str).toString());
        }

        public final L48 c(String str, Charset charset) {
            Wg6.c(str, "$this$encode");
            Wg6.c(charset, "charset");
            byte[] bytes = str.getBytes(charset);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return new L48(bytes);
        }

        public final L48 d(String str) {
            Wg6.c(str, "$this$encodeUtf8");
            L48 l48 = new L48(E48.a(str));
            l48.setUtf8$okio(str);
            return l48;
        }

        public final L48 e(ByteBuffer byteBuffer) {
            Wg6.c(byteBuffer, "$this$toByteString");
            byte[] bArr = new byte[byteBuffer.remaining()];
            byteBuffer.get(bArr);
            return new L48(bArr);
        }

        public final L48 f(byte... bArr) {
            Wg6.c(bArr, "data");
            byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
            Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
            return new L48(copyOf);
        }

        public final L48 g(byte[] bArr, int i, int i2) {
            Wg6.c(bArr, "$this$toByteString");
            F48.b((long) bArr.length, (long) i, (long) i2);
            return new L48(Dm7.k(bArr, i, i2 + i));
        }

        public final L48 h(InputStream inputStream, int i) throws IOException {
            int i2 = 0;
            Wg6.c(inputStream, "$this$readByteString");
            if (i >= 0) {
                byte[] bArr = new byte[i];
                while (i2 < i) {
                    int read = inputStream.read(bArr, i2, i - i2);
                    if (read != -1) {
                        i2 += read;
                    } else {
                        throw new EOFException();
                    }
                }
                return new L48(bArr);
            }
            throw new IllegalArgumentException(("byteCount < 0: " + i).toString());
        }
    }

    public L48(byte[] bArr) {
        Wg6.c(bArr, "data");
        this.data = bArr;
    }

    public static final L48 decodeBase64(String str) {
        return Companion.a(str);
    }

    public static final L48 decodeHex(String str) {
        return Companion.b(str);
    }

    public static final L48 encodeString(String str, Charset charset) {
        return Companion.c(str, charset);
    }

    public static final L48 encodeUtf8(String str) {
        return Companion.d(str);
    }

    public static /* synthetic */ int indexOf$default(L48 l48, L48 l482, int i, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                i = 0;
            }
            return l48.indexOf(l482, i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: indexOf");
    }

    public static /* synthetic */ int indexOf$default(L48 l48, byte[] bArr, int i, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                i = 0;
            }
            return l48.indexOf(bArr, i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: indexOf");
    }

    public static /* synthetic */ int lastIndexOf$default(L48 l48, L48 l482, int i, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                i = l48.size();
            }
            return l48.lastIndexOf(l482, i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: lastIndexOf");
    }

    public static /* synthetic */ int lastIndexOf$default(L48 l48, byte[] bArr, int i, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                i = l48.size();
            }
            return l48.lastIndexOf(bArr, i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: lastIndexOf");
    }

    public static final L48 of(ByteBuffer byteBuffer) {
        return Companion.e(byteBuffer);
    }

    public static final L48 of(byte... bArr) {
        return Companion.f(bArr);
    }

    public static final L48 of(byte[] bArr, int i, int i2) {
        return Companion.g(bArr, i, i2);
    }

    public static final L48 read(InputStream inputStream, int i) throws IOException {
        return Companion.h(inputStream, i);
    }

    private final void readObject(ObjectInputStream objectInputStream) throws IOException {
        L48 h = Companion.h(objectInputStream, objectInputStream.readInt());
        Field declaredField = L48.class.getDeclaredField("data");
        Wg6.b(declaredField, "field");
        declaredField.setAccessible(true);
        declaredField.set(this, h.data);
    }

    public static /* synthetic */ L48 substring$default(L48 l48, int i, int i2, int i3, Object obj) {
        if (obj == null) {
            if ((i3 & 1) != 0) {
                i = 0;
            }
            if ((i3 & 2) != 0) {
                i2 = l48.size();
            }
            return l48.substring(i, i2);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: substring");
    }

    private final void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(this.data.length);
        objectOutputStream.write(this.data);
    }

    public final byte -deprecated_getByte(int i) {
        return getByte(i);
    }

    public final int -deprecated_size() {
        return size();
    }

    public ByteBuffer asByteBuffer() {
        ByteBuffer asReadOnlyBuffer = ByteBuffer.wrap(this.data).asReadOnlyBuffer();
        Wg6.b(asReadOnlyBuffer, "ByteBuffer.wrap(data).asReadOnlyBuffer()");
        return asReadOnlyBuffer;
    }

    public String base64() {
        return D48.c(getData$okio(), null, 1, null);
    }

    public String base64Url() {
        return D48.b(getData$okio(), D48.d());
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x0028 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002e A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int compareTo(com.fossil.L48 r8) {
        /*
            r7 = this;
            r0 = 0
            java.lang.String r1 = "other"
            com.mapped.Wg6.c(r8, r1)
            int r2 = r7.size()
            int r3 = r8.size()
            int r4 = java.lang.Math.min(r2, r3)
            r1 = r0
        L_0x0013:
            if (r1 >= r4) goto L_0x002a
            byte r5 = r7.getByte(r1)
            r5 = r5 & 255(0xff, float:3.57E-43)
            byte r6 = r8.getByte(r1)
            r6 = r6 & 255(0xff, float:3.57E-43)
            if (r5 != r6) goto L_0x0026
            int r1 = r1 + 1
            goto L_0x0013
        L_0x0026:
            if (r5 >= r6) goto L_0x002e
        L_0x0028:
            r0 = -1
        L_0x0029:
            return r0
        L_0x002a:
            if (r2 == r3) goto L_0x0029
            if (r2 < r3) goto L_0x0028
        L_0x002e:
            r0 = 1
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.L48.compareTo(com.fossil.L48):int");
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(L48 l48) {
        return compareTo(l48);
    }

    public L48 digest$okio(String str) {
        Wg6.c(str, "algorithm");
        byte[] digest = MessageDigest.getInstance(str).digest(this.data);
        Wg6.b(digest, "MessageDigest.getInstance(algorithm).digest(data)");
        return new L48(digest);
    }

    public final boolean endsWith(L48 l48) {
        Wg6.c(l48, "suffix");
        return rangeEquals(size() - l48.size(), l48, 0, l48.size());
    }

    public final boolean endsWith(byte[] bArr) {
        Wg6.c(bArr, "suffix");
        return rangeEquals(size() - bArr.length, bArr, 0, bArr.length);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof L48) {
            L48 l48 = (L48) obj;
            if (l48.size() == getData$okio().length && l48.rangeEquals(0, getData$okio(), 0, getData$okio().length)) {
                return true;
            }
        }
        return false;
    }

    public final byte getByte(int i) {
        return internalGet$okio(i);
    }

    public final byte[] getData$okio() {
        return this.data;
    }

    public final int getHashCode$okio() {
        return this.b;
    }

    public int getSize$okio() {
        return getData$okio().length;
    }

    public final String getUtf8$okio() {
        return this.c;
    }

    public int hashCode() {
        int hashCode$okio = getHashCode$okio();
        if (hashCode$okio != 0) {
            return hashCode$okio;
        }
        int hashCode = Arrays.hashCode(getData$okio());
        setHashCode$okio(hashCode);
        return hashCode;
    }

    public String hex() {
        char[] cArr = new char[(getData$okio().length * 2)];
        byte[] data$okio = getData$okio();
        int i = 0;
        for (byte b2 : data$okio) {
            int i2 = i + 1;
            cArr[i] = (char) F58.f()[(b2 >> 4) & 15];
            i = i2 + 1;
            cArr[i2] = (char) F58.f()[b2 & DateTimeFieldType.CLOCKHOUR_OF_HALFDAY];
        }
        return new String(cArr);
    }

    public L48 hmac$okio(String str, L48 l48) {
        Wg6.c(str, "algorithm");
        Wg6.c(l48, "key");
        try {
            Mac instance = Mac.getInstance(str);
            instance.init(new SecretKeySpec(l48.toByteArray(), str));
            byte[] doFinal = instance.doFinal(this.data);
            Wg6.b(doFinal, "mac.doFinal(data)");
            return new L48(doFinal);
        } catch (InvalidKeyException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public L48 hmacSha1(L48 l48) {
        Wg6.c(l48, "key");
        return hmac$okio(UrlSigner.ALGORITHM_HMAC_SHA1, l48);
    }

    public L48 hmacSha256(L48 l48) {
        Wg6.c(l48, "key");
        return hmac$okio("HmacSHA256", l48);
    }

    public L48 hmacSha512(L48 l48) {
        Wg6.c(l48, "key");
        return hmac$okio("HmacSHA512", l48);
    }

    public final int indexOf(L48 l48) {
        return indexOf$default(this, l48, 0, 2, (Object) null);
    }

    public final int indexOf(L48 l48, int i) {
        Wg6.c(l48, FacebookRequestErrorClassification.KEY_OTHER);
        return indexOf(l48.internalArray$okio(), i);
    }

    public int indexOf(byte[] bArr) {
        return indexOf$default(this, bArr, 0, 2, (Object) null);
    }

    public int indexOf(byte[] bArr, int i) {
        Wg6.c(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        int length = getData$okio().length - bArr.length;
        int max = Math.max(i, 0);
        if (max <= length) {
            while (!F48.a(getData$okio(), max, bArr, 0, bArr.length)) {
                if (max != length) {
                    max++;
                }
            }
            return max;
        }
        return -1;
    }

    public byte[] internalArray$okio() {
        return getData$okio();
    }

    public byte internalGet$okio(int i) {
        return getData$okio()[i];
    }

    public final int lastIndexOf(L48 l48) {
        return lastIndexOf$default(this, l48, 0, 2, (Object) null);
    }

    public final int lastIndexOf(L48 l48, int i) {
        Wg6.c(l48, FacebookRequestErrorClassification.KEY_OTHER);
        return lastIndexOf(l48.internalArray$okio(), i);
    }

    public int lastIndexOf(byte[] bArr) {
        return lastIndexOf$default(this, bArr, 0, 2, (Object) null);
    }

    public int lastIndexOf(byte[] bArr, int i) {
        Wg6.c(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        for (int min = Math.min(i, getData$okio().length - bArr.length); min >= 0; min--) {
            if (F48.a(getData$okio(), min, bArr, 0, bArr.length)) {
                return min;
            }
        }
        return -1;
    }

    public L48 md5() {
        return digest$okio(Utility.HASH_ALGORITHM_MD5);
    }

    public boolean rangeEquals(int i, L48 l48, int i2, int i3) {
        Wg6.c(l48, FacebookRequestErrorClassification.KEY_OTHER);
        return l48.rangeEquals(i2, getData$okio(), i, i3);
    }

    public boolean rangeEquals(int i, byte[] bArr, int i2, int i3) {
        Wg6.c(bArr, FacebookRequestErrorClassification.KEY_OTHER);
        return i >= 0 && i <= getData$okio().length - i3 && i2 >= 0 && i2 <= bArr.length - i3 && F48.a(getData$okio(), i, bArr, i2, i3);
    }

    public final void setHashCode$okio(int i) {
        this.b = i;
    }

    public final void setUtf8$okio(String str) {
        this.c = str;
    }

    public L48 sha1() {
        return digest$okio(Utility.HASH_ALGORITHM_SHA1);
    }

    public L48 sha256() {
        return digest$okio("SHA-256");
    }

    public L48 sha512() {
        return digest$okio("SHA-512");
    }

    public final int size() {
        return getSize$okio();
    }

    public final boolean startsWith(L48 l48) {
        Wg6.c(l48, "prefix");
        return rangeEquals(0, l48, 0, l48.size());
    }

    public final boolean startsWith(byte[] bArr) {
        Wg6.c(bArr, "prefix");
        return rangeEquals(0, bArr, 0, bArr.length);
    }

    public String string(Charset charset) {
        Wg6.c(charset, "charset");
        return new String(this.data, charset);
    }

    public L48 substring() {
        return substring$default(this, 0, 0, 3, null);
    }

    public L48 substring(int i) {
        return substring$default(this, i, 0, 2, null);
    }

    public L48 substring(int i, int i2) {
        boolean z = true;
        if (i >= 0) {
            if (i2 <= getData$okio().length) {
                if (i2 - i < 0) {
                    z = false;
                }
                if (z) {
                    return (i == 0 && i2 == getData$okio().length) ? this : new L48(Dm7.k(getData$okio(), i, i2));
                }
                throw new IllegalArgumentException("endIndex < beginIndex".toString());
            }
            throw new IllegalArgumentException(("endIndex > length(" + getData$okio().length + ')').toString());
        }
        throw new IllegalArgumentException("beginIndex < 0".toString());
    }

    public L48 toAsciiLowercase() {
        byte b2;
        for (int i = 0; i < getData$okio().length; i++) {
            byte b3 = getData$okio()[i];
            byte b4 = (byte) 65;
            if (b3 >= b4 && b3 <= (b2 = (byte) 90)) {
                byte[] data$okio = getData$okio();
                byte[] copyOf = Arrays.copyOf(data$okio, data$okio.length);
                Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
                copyOf[i] = (byte) ((byte) (b3 + 32));
                for (int i2 = i + 1; i2 < copyOf.length; i2++) {
                    byte b5 = copyOf[i2];
                    if (b5 >= b4 && b5 <= b2) {
                        copyOf[i2] = (byte) ((byte) (b5 + 32));
                    }
                }
                return new L48(copyOf);
            }
        }
        return this;
    }

    public L48 toAsciiUppercase() {
        byte b2;
        for (int i = 0; i < getData$okio().length; i++) {
            byte b3 = getData$okio()[i];
            byte b4 = (byte) 97;
            if (b3 >= b4 && b3 <= (b2 = (byte) 122)) {
                byte[] data$okio = getData$okio();
                byte[] copyOf = Arrays.copyOf(data$okio, data$okio.length);
                Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
                copyOf[i] = (byte) ((byte) (b3 - 32));
                for (int i2 = i + 1; i2 < copyOf.length; i2++) {
                    byte b5 = copyOf[i2];
                    if (b5 >= b4 && b5 <= b2) {
                        copyOf[i2] = (byte) ((byte) (b5 - 32));
                    }
                }
                return new L48(copyOf);
            }
        }
        return this;
    }

    public byte[] toByteArray() {
        byte[] data$okio = getData$okio();
        byte[] copyOf = Arrays.copyOf(data$okio, data$okio.length);
        Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return copyOf;
    }

    public String toString() {
        boolean z = true;
        if (getData$okio().length == 0) {
            return "[size=0]";
        }
        int a2 = F58.a(getData$okio(), 64);
        if (a2 != -1) {
            String utf8 = utf8();
            if (utf8 != null) {
                String substring = utf8.substring(0, a2);
                Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                String q = Vt7.q(Vt7.q(Vt7.q(substring, "\\", "\\\\", false, 4, null), "\n", "\\n", false, 4, null), "\r", "\\r", false, 4, null);
                if (a2 < utf8.length()) {
                    return "[size=" + getData$okio().length + " text=" + q + "\u2026]";
                }
                return "[text=" + q + ']';
            }
            throw new Rc6("null cannot be cast to non-null type java.lang.String");
        } else if (getData$okio().length <= 64) {
            return "[hex=" + hex() + ']';
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("[size=");
            sb.append(getData$okio().length);
            sb.append(" hex=");
            if (64 > getData$okio().length) {
                z = false;
            }
            if (z) {
                if (64 != getData$okio().length) {
                    this = new L48(Dm7.k(getData$okio(), 0, 64));
                }
                sb.append(this.hex());
                sb.append("\u2026]");
                return sb.toString();
            }
            throw new IllegalArgumentException(("endIndex > length(" + getData$okio().length + ')').toString());
        }
    }

    public String utf8() {
        String utf8$okio = getUtf8$okio();
        if (utf8$okio != null) {
            return utf8$okio;
        }
        String b2 = E48.b(internalArray$okio());
        setUtf8$okio(b2);
        return b2;
    }

    public void write(OutputStream outputStream) throws IOException {
        Wg6.c(outputStream, "out");
        outputStream.write(this.data);
    }

    public void write$okio(I48 i48, int i, int i2) {
        Wg6.c(i48, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        F58.d(this, i48, i, i2);
    }
}
