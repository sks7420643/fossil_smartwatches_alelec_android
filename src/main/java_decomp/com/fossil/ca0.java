package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Ca0 extends Enum<Ca0> {
    @DexIgnore
    public static /* final */ Ca0 c;
    @DexIgnore
    public static /* final */ Ca0 d;
    @DexIgnore
    public static /* final */ /* synthetic */ Ca0[] e;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        Ca0 ca0 = new Ca0("HOUR", 0, (byte) 1);
        c = ca0;
        Ca0 ca02 = new Ca0("MINUTE", 1, (byte) 2);
        d = ca02;
        e = new Ca0[]{ca0, ca02, new Ca0("SUB_EYE", 2, (byte) 4)};
    }
    */

    @DexIgnore
    public Ca0(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static Ca0 valueOf(String str) {
        return (Ca0) Enum.valueOf(Ca0.class, str);
    }

    @DexIgnore
    public static Ca0[] values() {
        return (Ca0[]) e.clone();
    }
}
