package com.fossil;

import com.fossil.Lx1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jx {
    @DexIgnore
    public static /* final */ Lx1.Bi a; // = Lx1.Bi.CTR_NO_PADDING;
    @DexIgnore
    public static /* final */ Jx b; // = new Jx();

    @DexIgnore
    public final byte[] a(String str, N6 n6, Lx1.Ai ai, byte[] bArr) {
        boolean z = true;
        Hx a2 = Ix.b.a(str);
        byte[] c = a2.c();
        byte[] a3 = a2.a(n6).a();
        if (c == null) {
            return bArr;
        }
        if (true != (!(c.length == 0))) {
            return bArr;
        }
        if (a3.length != 0) {
            z = false;
        }
        if (!(!z)) {
            return bArr;
        }
        byte[] d = Lx1.a.d(ai, a, c, a3, bArr);
        int ceil = (int) ((float) Math.ceil((double) (((float) bArr.length) / ((float) 16))));
        Kx kx = a2.d.get(n6);
        if (kx != null) {
            kx.e = ceil + kx.e;
        }
        return d;
    }

    @DexIgnore
    public final byte[] b(String str, N6 n6, byte[] bArr) {
        return a(str, n6, Lx1.Ai.DECRYPT, bArr);
    }

    @DexIgnore
    public final byte[] c(String str, N6 n6, byte[] bArr) {
        return a(str, n6, Lx1.Ai.ENCRYPT, bArr);
    }
}
