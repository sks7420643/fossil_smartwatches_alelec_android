package com.fossil;

import android.media.MediaMetadata;
import android.media.session.MediaController;
import android.media.session.PlaybackState;
import android.os.Bundle;
import android.view.KeyEvent;
import com.facebook.internal.AnalyticsEvents;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Sr5 extends Qr5 {
    @DexIgnore
    public /* final */ String c; // = "NewMusicController";
    @DexIgnore
    public /* final */ MediaController.Callback d;
    @DexIgnore
    public int e;
    @DexIgnore
    public Rr5 f;
    @DexIgnore
    public /* final */ MediaController g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends MediaController.Callback {
        @DexIgnore
        public /* final */ /* synthetic */ Sr5 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public Ai(Sr5 sr5, String str) {
            this.a = sr5;
            this.b = str;
        }

        @DexIgnore
        public void onMetadataChanged(MediaMetadata mediaMetadata) {
            super.onMetadataChanged(mediaMetadata);
            if (mediaMetadata != null) {
                String b2 = Ij5.b(mediaMetadata.getString("android.media.metadata.TITLE"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                String b3 = Ij5.b(mediaMetadata.getString("android.media.metadata.ARTIST"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                String b4 = Ij5.b(mediaMetadata.getString("android.media.metadata.ALBUM"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.a.c;
                local.d(str, "SystemCallback of " + this.b + " - onMetadataChanged title=" + b2 + ", artist=" + b3);
                Rr5 rr5 = new Rr5(this.a.d(), this.b, b2, b3, b4);
                if (!Wg6.a(rr5, this.a.g())) {
                    Rr5 g = this.a.g();
                    this.a.l(rr5);
                    this.a.i(g, rr5);
                }
            }
        }

        @DexIgnore
        public void onPlaybackStateChanged(PlaybackState playbackState) {
            super.onPlaybackStateChanged(playbackState);
            int state = playbackState != null ? playbackState.getState() : 0;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a.c;
            StringBuilder sb = new StringBuilder();
            sb.append("SystemCallback of ");
            sb.append(this.b);
            sb.append(" - onPlaybackStateChanged , state = ");
            sb.append(playbackState != null ? Integer.valueOf(playbackState.getState()) : null);
            sb.append(" lastState ");
            sb.append(this.a.h());
            local.d(str, sb.toString());
            if (state != this.a.h()) {
                int h = this.a.h();
                this.a.m(state);
                Sr5 sr5 = this.a;
                sr5.j(h, state, sr5);
            }
        }

        @DexIgnore
        public void onSessionDestroyed() {
            super.onSessionDestroyed();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a.c;
            local.d(str, "SystemCallback of " + this.b + " onSessionDestroyed");
            Sr5 sr5 = this.a;
            sr5.k(sr5);
        }

        @DexIgnore
        public void onSessionEvent(String str, Bundle bundle) {
            Wg6.c(str, Constants.EVENT);
            super.onSessionEvent(str, bundle);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.a.c;
            local.d(str2, "SystemCallback of " + this.b + " .onSessionEvent, event=" + str + ", extras=" + bundle);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Sr5(android.media.session.MediaController r7, java.lang.String r8) {
        /*
            r6 = this;
            java.lang.String r0 = "mSystemMediaController"
            com.mapped.Wg6.c(r7, r0)
            java.lang.String r0 = "appName"
            com.mapped.Wg6.c(r8, r0)
            java.lang.String r0 = r7.getPackageName()
            java.lang.String r1 = "mSystemMediaController.packageName"
            com.mapped.Wg6.b(r0, r1)
            r6.<init>(r8, r0)
            r6.g = r7
            java.lang.String r0 = "NewMusicController"
            r6.c = r0
            com.fossil.Sr5$Ai r0 = new com.fossil.Sr5$Ai
            r0.<init>(r6, r8)
            r6.d = r0
            com.fossil.Rr5 r0 = new com.fossil.Rr5
            java.lang.String r1 = r6.d()
            java.lang.String r3 = "Unknown"
            java.lang.String r4 = "Unknown"
            java.lang.String r5 = "Unknown"
            r2 = r8
            r0.<init>(r1, r2, r3, r4, r5)
            r6.f = r0
            android.media.session.MediaController r0 = r6.g
            android.media.session.MediaController$Callback r1 = r6.d
            r0.registerCallback(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Sr5.<init>(android.media.session.MediaController, java.lang.String):void");
    }

    @DexIgnore
    @Override // com.fossil.Qr5
    public boolean a(KeyEvent keyEvent) {
        Wg6.c(keyEvent, "keyEvent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.c;
        local.d(str, "dispatchMediaButtonEvent of " + d() + " keyEvent=" + keyEvent);
        return this.g.dispatchMediaButtonEvent(keyEvent);
    }

    @DexIgnore
    @Override // com.fossil.Qr5
    public Rr5 c() {
        String str;
        String str2;
        String str3;
        MediaMetadata metadata = this.g.getMetadata();
        if (metadata != null) {
            str2 = Ij5.b(metadata.getString("android.media.metadata.TITLE"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            str = Ij5.b(metadata.getString("android.media.metadata.ARTIST"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            str3 = Ij5.b(metadata.getString("android.media.metadata.ALBUM"), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
        } else {
            str = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
            str2 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
            str3 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        Rr5 rr5 = new Rr5(d(), b(), str2, str, str3);
        if (!Wg6.a(rr5, this.f)) {
            this.f = rr5;
        }
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.Qr5
    public int e() {
        PlaybackState playbackState = this.g.getPlaybackState();
        Integer valueOf = playbackState != null ? Integer.valueOf(playbackState.getState()) : null;
        int i = this.e;
        if ((valueOf == null || valueOf.intValue() != i) && valueOf != null) {
            this.e = valueOf.intValue();
        }
        return this.e;
    }

    @DexIgnore
    public final Rr5 g() {
        return this.f;
    }

    @DexIgnore
    public final int h() {
        return this.e;
    }

    @DexIgnore
    public void i(Rr5 rr5, Rr5 rr52) {
        Wg6.c(rr5, "oldMetadata");
        Wg6.c(rr52, "newMetadata");
    }

    @DexIgnore
    public void j(int i, int i2, Qr5 qr5) {
        Wg6.c(qr5, "controller");
    }

    @DexIgnore
    public void k(Qr5 qr5) {
        Wg6.c(qr5, "controller");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.c;
        local.d(str, ".onSessionDestroyed of " + d());
        this.g.unregisterCallback(this.d);
    }

    @DexIgnore
    public final void l(Rr5 rr5) {
        Wg6.c(rr5, "<set-?>");
        this.f = rr5;
    }

    @DexIgnore
    public final void m(int i) {
        this.e = i;
    }
}
