package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewPropertyAnimator;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yu0 extends Pv0 {
    @DexIgnore
    public static /* final */ boolean DEBUG; // = false;
    @DexIgnore
    public static TimeInterpolator sDefaultInterpolator;
    @DexIgnore
    public ArrayList<RecyclerView.ViewHolder> mAddAnimations; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<ArrayList<RecyclerView.ViewHolder>> mAdditionsList; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<RecyclerView.ViewHolder> mChangeAnimations; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<ArrayList<Ii>> mChangesList; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<RecyclerView.ViewHolder> mMoveAnimations; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<ArrayList<Ji>> mMovesList; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<RecyclerView.ViewHolder> mPendingAdditions; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<Ii> mPendingChanges; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<Ji> mPendingMoves; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<RecyclerView.ViewHolder> mPendingRemovals; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<RecyclerView.ViewHolder> mRemoveAnimations; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;

        @DexIgnore
        public Ai(ArrayList arrayList) {
            this.b = arrayList;
        }

        @DexIgnore
        public void run() {
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                Ji ji = (Ji) it.next();
                Yu0.this.animateMoveImpl(ji.a, ji.b, ji.c, ji.d, ji.e);
            }
            this.b.clear();
            Yu0.this.mMovesList.remove(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;

        @DexIgnore
        public Bi(ArrayList arrayList) {
            this.b = arrayList;
        }

        @DexIgnore
        public void run() {
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                Yu0.this.animateChangeImpl((Ii) it.next());
            }
            this.b.clear();
            Yu0.this.mChangesList.remove(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;

        @DexIgnore
        public Ci(ArrayList arrayList) {
            this.b = arrayList;
        }

        @DexIgnore
        public void run() {
            Iterator it = this.b.iterator();
            while (it.hasNext()) {
                Yu0.this.animateAddImpl((RecyclerView.ViewHolder) it.next());
            }
            this.b.clear();
            Yu0.this.mAdditionsList.remove(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder a;
        @DexIgnore
        public /* final */ /* synthetic */ ViewPropertyAnimator b;
        @DexIgnore
        public /* final */ /* synthetic */ View c;

        @DexIgnore
        public Di(RecyclerView.ViewHolder viewHolder, ViewPropertyAnimator viewPropertyAnimator, View view) {
            this.a = viewHolder;
            this.b = viewPropertyAnimator;
            this.c = view;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.b.setListener(null);
            this.c.setAlpha(1.0f);
            Yu0.this.dispatchRemoveFinished(this.a);
            Yu0.this.mRemoveAnimations.remove(this.a);
            Yu0.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            Yu0.this.dispatchRemoveStarting(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ ViewPropertyAnimator c;

        @DexIgnore
        public Ei(RecyclerView.ViewHolder viewHolder, View view, ViewPropertyAnimator viewPropertyAnimator) {
            this.a = viewHolder;
            this.b = view;
            this.c = viewPropertyAnimator;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            this.b.setAlpha(1.0f);
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.c.setListener(null);
            Yu0.this.dispatchAddFinished(this.a);
            Yu0.this.mAddAnimations.remove(this.a);
            Yu0.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            Yu0.this.dispatchAddStarting(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;
        @DexIgnore
        public /* final */ /* synthetic */ ViewPropertyAnimator e;

        @DexIgnore
        public Fi(RecyclerView.ViewHolder viewHolder, int i, View view, int i2, ViewPropertyAnimator viewPropertyAnimator) {
            this.a = viewHolder;
            this.b = i;
            this.c = view;
            this.d = i2;
            this.e = viewPropertyAnimator;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            if (this.b != 0) {
                this.c.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            if (this.d != 0) {
                this.c.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.e.setListener(null);
            Yu0.this.dispatchMoveFinished(this.a);
            Yu0.this.mMoveAnimations.remove(this.a);
            Yu0.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            Yu0.this.dispatchMoveStarting(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ Ii a;
        @DexIgnore
        public /* final */ /* synthetic */ ViewPropertyAnimator b;
        @DexIgnore
        public /* final */ /* synthetic */ View c;

        @DexIgnore
        public Gi(Ii ii, ViewPropertyAnimator viewPropertyAnimator, View view) {
            this.a = ii;
            this.b = viewPropertyAnimator;
            this.c = view;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.b.setListener(null);
            this.c.setAlpha(1.0f);
            this.c.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.c.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            Yu0.this.dispatchChangeFinished(this.a.a, true);
            Yu0.this.mChangeAnimations.remove(this.a.a);
            Yu0.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            Yu0.this.dispatchChangeStarting(this.a.a, true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Hi extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ Ii a;
        @DexIgnore
        public /* final */ /* synthetic */ ViewPropertyAnimator b;
        @DexIgnore
        public /* final */ /* synthetic */ View c;

        @DexIgnore
        public Hi(Ii ii, ViewPropertyAnimator viewPropertyAnimator, View view) {
            this.a = ii;
            this.b = viewPropertyAnimator;
            this.c = view;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.b.setListener(null);
            this.c.setAlpha(1.0f);
            this.c.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.c.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            Yu0.this.dispatchChangeFinished(this.a.b, false);
            Yu0.this.mChangeAnimations.remove(this.a.b);
            Yu0.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            Yu0.this.dispatchChangeStarting(this.a.b, false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ii {
        @DexIgnore
        public RecyclerView.ViewHolder a;
        @DexIgnore
        public RecyclerView.ViewHolder b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;

        @DexIgnore
        public Ii(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
            this.a = viewHolder;
            this.b = viewHolder2;
        }

        @DexIgnore
        public Ii(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2, int i, int i2, int i3, int i4) {
            this(viewHolder, viewHolder2);
            this.c = i;
            this.d = i2;
            this.e = i3;
            this.f = i4;
        }

        @DexIgnore
        public String toString() {
            return "ChangeInfo{oldHolder=" + this.a + ", newHolder=" + this.b + ", fromX=" + this.c + ", fromY=" + this.d + ", toX=" + this.e + ", toY=" + this.f + '}';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ji {
        @DexIgnore
        public RecyclerView.ViewHolder a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;

        @DexIgnore
        public Ji(RecyclerView.ViewHolder viewHolder, int i, int i2, int i3, int i4) {
            this.a = viewHolder;
            this.b = i;
            this.c = i2;
            this.d = i3;
            this.e = i4;
        }
    }

    @DexIgnore
    private void animateRemoveImpl(RecyclerView.ViewHolder viewHolder) {
        View view = viewHolder.itemView;
        ViewPropertyAnimator animate = view.animate();
        this.mRemoveAnimations.add(viewHolder);
        animate.setDuration(getRemoveDuration()).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).setListener(new Di(viewHolder, animate, view)).start();
    }

    @DexIgnore
    private void endChangeAnimation(List<Ii> list, RecyclerView.ViewHolder viewHolder) {
        for (int size = list.size() - 1; size >= 0; size--) {
            Ii ii = list.get(size);
            if (endChangeAnimationIfNecessary(ii, viewHolder) && ii.a == null && ii.b == null) {
                list.remove(ii);
            }
        }
    }

    @DexIgnore
    private void endChangeAnimationIfNecessary(Ii ii) {
        RecyclerView.ViewHolder viewHolder = ii.a;
        if (viewHolder != null) {
            endChangeAnimationIfNecessary(ii, viewHolder);
        }
        RecyclerView.ViewHolder viewHolder2 = ii.b;
        if (viewHolder2 != null) {
            endChangeAnimationIfNecessary(ii, viewHolder2);
        }
    }

    @DexIgnore
    private boolean endChangeAnimationIfNecessary(Ii ii, RecyclerView.ViewHolder viewHolder) {
        boolean z = false;
        if (ii.b == viewHolder) {
            ii.b = null;
        } else if (ii.a != viewHolder) {
            return false;
        } else {
            ii.a = null;
            z = true;
        }
        viewHolder.itemView.setAlpha(1.0f);
        viewHolder.itemView.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        viewHolder.itemView.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        dispatchChangeFinished(viewHolder, z);
        return true;
    }

    @DexIgnore
    private void resetAnimation(RecyclerView.ViewHolder viewHolder) {
        if (sDefaultInterpolator == null) {
            sDefaultInterpolator = new ValueAnimator().getInterpolator();
        }
        viewHolder.itemView.animate().setInterpolator(sDefaultInterpolator);
        endAnimation(viewHolder);
    }

    @DexIgnore
    @Override // com.fossil.Pv0
    public boolean animateAdd(RecyclerView.ViewHolder viewHolder) {
        resetAnimation(viewHolder);
        viewHolder.itemView.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.mPendingAdditions.add(viewHolder);
        return true;
    }

    @DexIgnore
    public void animateAddImpl(RecyclerView.ViewHolder viewHolder) {
        View view = viewHolder.itemView;
        ViewPropertyAnimator animate = view.animate();
        this.mAddAnimations.add(viewHolder);
        animate.alpha(1.0f).setDuration(getAddDuration()).setListener(new Ei(viewHolder, view, animate)).start();
    }

    @DexIgnore
    @Override // com.fossil.Pv0
    public boolean animateChange(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2, int i, int i2, int i3, int i4) {
        if (viewHolder == viewHolder2) {
            return animateMove(viewHolder, i, i2, i3, i4);
        }
        float translationX = viewHolder.itemView.getTranslationX();
        float translationY = viewHolder.itemView.getTranslationY();
        float alpha = viewHolder.itemView.getAlpha();
        resetAnimation(viewHolder);
        int i5 = (int) (((float) (i3 - i)) - translationX);
        int i6 = (int) (((float) (i4 - i2)) - translationY);
        viewHolder.itemView.setTranslationX(translationX);
        viewHolder.itemView.setTranslationY(translationY);
        viewHolder.itemView.setAlpha(alpha);
        if (viewHolder2 != null) {
            resetAnimation(viewHolder2);
            viewHolder2.itemView.setTranslationX((float) (-i5));
            viewHolder2.itemView.setTranslationY((float) (-i6));
            viewHolder2.itemView.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        this.mPendingChanges.add(new Ii(viewHolder, viewHolder2, i, i2, i3, i4));
        return true;
    }

    @DexIgnore
    public void animateChangeImpl(Ii ii) {
        View view = null;
        RecyclerView.ViewHolder viewHolder = ii.a;
        View view2 = viewHolder == null ? null : viewHolder.itemView;
        RecyclerView.ViewHolder viewHolder2 = ii.b;
        if (viewHolder2 != null) {
            view = viewHolder2.itemView;
        }
        if (view2 != null) {
            ViewPropertyAnimator duration = view2.animate().setDuration(getChangeDuration());
            this.mChangeAnimations.add(ii.a);
            duration.translationX((float) (ii.e - ii.c));
            duration.translationY((float) (ii.f - ii.d));
            duration.alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).setListener(new Gi(ii, duration, view2)).start();
        }
        if (view != null) {
            ViewPropertyAnimator animate = view.animate();
            this.mChangeAnimations.add(ii.b);
            animate.translationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).setDuration(getChangeDuration()).alpha(1.0f).setListener(new Hi(ii, animate, view)).start();
        }
    }

    @DexIgnore
    @Override // com.fossil.Pv0
    public boolean animateMove(RecyclerView.ViewHolder viewHolder, int i, int i2, int i3, int i4) {
        View view = viewHolder.itemView;
        int translationX = i + ((int) view.getTranslationX());
        int translationY = i2 + ((int) viewHolder.itemView.getTranslationY());
        resetAnimation(viewHolder);
        int i5 = i3 - translationX;
        int i6 = i4 - translationY;
        if (i5 == 0 && i6 == 0) {
            dispatchMoveFinished(viewHolder);
            return false;
        }
        if (i5 != 0) {
            view.setTranslationX((float) (-i5));
        }
        if (i6 != 0) {
            view.setTranslationY((float) (-i6));
        }
        this.mPendingMoves.add(new Ji(viewHolder, translationX, translationY, i3, i4));
        return true;
    }

    @DexIgnore
    public void animateMoveImpl(RecyclerView.ViewHolder viewHolder, int i, int i2, int i3, int i4) {
        View view = viewHolder.itemView;
        int i5 = i3 - i;
        int i6 = i4 - i2;
        if (i5 != 0) {
            view.animate().translationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        if (i6 != 0) {
            view.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        ViewPropertyAnimator animate = view.animate();
        this.mMoveAnimations.add(viewHolder);
        animate.setDuration(getMoveDuration()).setListener(new Fi(viewHolder, i5, view, i6, animate)).start();
    }

    @DexIgnore
    @Override // com.fossil.Pv0
    public boolean animateRemove(RecyclerView.ViewHolder viewHolder) {
        resetAnimation(viewHolder);
        this.mPendingRemovals.add(viewHolder);
        return true;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.j
    public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder, List<Object> list) {
        return !list.isEmpty() || super.canReuseUpdatedViewHolder(viewHolder, list);
    }

    @DexIgnore
    public void cancelAll(List<RecyclerView.ViewHolder> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            list.get(size).itemView.animate().cancel();
        }
    }

    @DexIgnore
    public void dispatchFinishedWhenDone() {
        if (!isRunning()) {
            dispatchAnimationsFinished();
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.j
    public void endAnimation(RecyclerView.ViewHolder viewHolder) {
        View view = viewHolder.itemView;
        view.animate().cancel();
        for (int size = this.mPendingMoves.size() - 1; size >= 0; size--) {
            if (this.mPendingMoves.get(size).a == viewHolder) {
                view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                view.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                dispatchMoveFinished(viewHolder);
                this.mPendingMoves.remove(size);
            }
        }
        endChangeAnimation(this.mPendingChanges, viewHolder);
        if (this.mPendingRemovals.remove(viewHolder)) {
            view.setAlpha(1.0f);
            dispatchRemoveFinished(viewHolder);
        }
        if (this.mPendingAdditions.remove(viewHolder)) {
            view.setAlpha(1.0f);
            dispatchAddFinished(viewHolder);
        }
        for (int size2 = this.mChangesList.size() - 1; size2 >= 0; size2--) {
            ArrayList<Ii> arrayList = this.mChangesList.get(size2);
            endChangeAnimation(arrayList, viewHolder);
            if (arrayList.isEmpty()) {
                this.mChangesList.remove(size2);
            }
        }
        for (int size3 = this.mMovesList.size() - 1; size3 >= 0; size3--) {
            ArrayList<Ji> arrayList2 = this.mMovesList.get(size3);
            int size4 = arrayList2.size() - 1;
            while (true) {
                if (size4 < 0) {
                    break;
                } else if (arrayList2.get(size4).a == viewHolder) {
                    view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    view.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    dispatchMoveFinished(viewHolder);
                    arrayList2.remove(size4);
                    if (arrayList2.isEmpty()) {
                        this.mMovesList.remove(size3);
                    }
                } else {
                    size4--;
                }
            }
        }
        for (int size5 = this.mAdditionsList.size() - 1; size5 >= 0; size5--) {
            ArrayList<RecyclerView.ViewHolder> arrayList3 = this.mAdditionsList.get(size5);
            if (arrayList3.remove(viewHolder)) {
                view.setAlpha(1.0f);
                dispatchAddFinished(viewHolder);
                if (arrayList3.isEmpty()) {
                    this.mAdditionsList.remove(size5);
                }
            }
        }
        this.mRemoveAnimations.remove(viewHolder);
        this.mAddAnimations.remove(viewHolder);
        this.mChangeAnimations.remove(viewHolder);
        this.mMoveAnimations.remove(viewHolder);
        dispatchFinishedWhenDone();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.j
    public void endAnimations() {
        for (int size = this.mPendingMoves.size() - 1; size >= 0; size--) {
            Ji ji = this.mPendingMoves.get(size);
            View view = ji.a.itemView;
            view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            view.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            dispatchMoveFinished(ji.a);
            this.mPendingMoves.remove(size);
        }
        for (int size2 = this.mPendingRemovals.size() - 1; size2 >= 0; size2--) {
            dispatchRemoveFinished(this.mPendingRemovals.get(size2));
            this.mPendingRemovals.remove(size2);
        }
        for (int size3 = this.mPendingAdditions.size() - 1; size3 >= 0; size3--) {
            RecyclerView.ViewHolder viewHolder = this.mPendingAdditions.get(size3);
            viewHolder.itemView.setAlpha(1.0f);
            dispatchAddFinished(viewHolder);
            this.mPendingAdditions.remove(size3);
        }
        for (int size4 = this.mPendingChanges.size() - 1; size4 >= 0; size4--) {
            endChangeAnimationIfNecessary(this.mPendingChanges.get(size4));
        }
        this.mPendingChanges.clear();
        if (isRunning()) {
            for (int size5 = this.mMovesList.size() - 1; size5 >= 0; size5--) {
                ArrayList<Ji> arrayList = this.mMovesList.get(size5);
                for (int size6 = arrayList.size() - 1; size6 >= 0; size6--) {
                    Ji ji2 = arrayList.get(size6);
                    View view2 = ji2.a.itemView;
                    view2.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    view2.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    dispatchMoveFinished(ji2.a);
                    arrayList.remove(size6);
                    if (arrayList.isEmpty()) {
                        this.mMovesList.remove(arrayList);
                    }
                }
            }
            for (int size7 = this.mAdditionsList.size() - 1; size7 >= 0; size7--) {
                ArrayList<RecyclerView.ViewHolder> arrayList2 = this.mAdditionsList.get(size7);
                for (int size8 = arrayList2.size() - 1; size8 >= 0; size8--) {
                    RecyclerView.ViewHolder viewHolder2 = arrayList2.get(size8);
                    viewHolder2.itemView.setAlpha(1.0f);
                    dispatchAddFinished(viewHolder2);
                    arrayList2.remove(size8);
                    if (arrayList2.isEmpty()) {
                        this.mAdditionsList.remove(arrayList2);
                    }
                }
            }
            for (int size9 = this.mChangesList.size() - 1; size9 >= 0; size9--) {
                ArrayList<Ii> arrayList3 = this.mChangesList.get(size9);
                for (int size10 = arrayList3.size() - 1; size10 >= 0; size10--) {
                    endChangeAnimationIfNecessary(arrayList3.get(size10));
                    if (arrayList3.isEmpty()) {
                        this.mChangesList.remove(arrayList3);
                    }
                }
            }
            cancelAll(this.mRemoveAnimations);
            cancelAll(this.mMoveAnimations);
            cancelAll(this.mAddAnimations);
            cancelAll(this.mChangeAnimations);
            dispatchAnimationsFinished();
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.j
    public boolean isRunning() {
        return !this.mPendingAdditions.isEmpty() || !this.mPendingChanges.isEmpty() || !this.mPendingMoves.isEmpty() || !this.mPendingRemovals.isEmpty() || !this.mMoveAnimations.isEmpty() || !this.mRemoveAnimations.isEmpty() || !this.mAddAnimations.isEmpty() || !this.mChangeAnimations.isEmpty() || !this.mMovesList.isEmpty() || !this.mAdditionsList.isEmpty() || !this.mChangesList.isEmpty();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.j
    public void runPendingAnimations() {
        boolean z = !this.mPendingRemovals.isEmpty();
        boolean z2 = !this.mPendingMoves.isEmpty();
        boolean z3 = !this.mPendingChanges.isEmpty();
        boolean z4 = !this.mPendingAdditions.isEmpty();
        if (z || z2 || z4 || z3) {
            Iterator<RecyclerView.ViewHolder> it = this.mPendingRemovals.iterator();
            while (it.hasNext()) {
                animateRemoveImpl(it.next());
            }
            this.mPendingRemovals.clear();
            if (z2) {
                ArrayList<Ji> arrayList = new ArrayList<>();
                arrayList.addAll(this.mPendingMoves);
                this.mMovesList.add(arrayList);
                this.mPendingMoves.clear();
                Ai ai = new Ai(arrayList);
                if (z) {
                    Mo0.e0(arrayList.get(0).a.itemView, ai, getRemoveDuration());
                } else {
                    ai.run();
                }
            }
            if (z3) {
                ArrayList<Ii> arrayList2 = new ArrayList<>();
                arrayList2.addAll(this.mPendingChanges);
                this.mChangesList.add(arrayList2);
                this.mPendingChanges.clear();
                Bi bi = new Bi(arrayList2);
                if (z) {
                    Mo0.e0(arrayList2.get(0).a.itemView, bi, getRemoveDuration());
                } else {
                    bi.run();
                }
            }
            if (z4) {
                ArrayList<RecyclerView.ViewHolder> arrayList3 = new ArrayList<>();
                arrayList3.addAll(this.mPendingAdditions);
                this.mAdditionsList.add(arrayList3);
                this.mPendingAdditions.clear();
                Ci ci = new Ci(arrayList3);
                if (z || z2 || z3) {
                    Mo0.e0(arrayList3.get(0).itemView, ci, Math.max(z2 ? getMoveDuration() : 0, z3 ? getChangeDuration() : 0) + (z ? getRemoveDuration() : 0));
                } else {
                    ci.run();
                }
            }
        }
    }
}
