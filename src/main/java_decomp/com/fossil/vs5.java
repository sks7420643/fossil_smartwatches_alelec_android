package com.fossil;

import com.mapped.Er4;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vs5 extends Er4 {
    @DexIgnore
    public /* final */ List<String> d;
    @DexIgnore
    public String e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Vs5(String str, String str2, List<String> list, String str3) {
        super(str, str2);
        Wg6.c(str, "tagName");
        Wg6.c(str2, "title");
        Wg6.c(list, "values");
        Wg6.c(str3, "btnText");
        this.d = list;
        this.e = str3;
    }

    @DexIgnore
    public final String f() {
        return this.e;
    }

    @DexIgnore
    public final List<String> g() {
        return this.d;
    }
}
