package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y86 implements Factory<SearchSecondTimezonePresenter> {
    @DexIgnore
    public static SearchSecondTimezonePresenter a(U86 u86) {
        return new SearchSecondTimezonePresenter(u86);
    }
}
