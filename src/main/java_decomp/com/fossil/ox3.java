package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import com.google.android.material.button.MaterialButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ox3 {
    @DexIgnore
    public static /* final */ boolean s; // = (Build.VERSION.SDK_INT >= 21);
    @DexIgnore
    public /* final */ MaterialButton a;
    @DexIgnore
    public G04 b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public PorterDuff.Mode i;
    @DexIgnore
    public ColorStateList j;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public ColorStateList l;
    @DexIgnore
    public Drawable m;
    @DexIgnore
    public boolean n; // = false;
    @DexIgnore
    public boolean o; // = false;
    @DexIgnore
    public boolean p; // = false;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public LayerDrawable r;

    @DexIgnore
    public Ox3(MaterialButton materialButton, G04 g04) {
        this.a = materialButton;
        this.b = g04;
    }

    @DexIgnore
    public final void A(G04 g04) {
        if (d() != null) {
            d().setShapeAppearanceModel(g04);
        }
        if (l() != null) {
            l().setShapeAppearanceModel(g04);
        }
        if (c() != null) {
            c().setShapeAppearanceModel(g04);
        }
    }

    @DexIgnore
    public void B(int i2, int i3) {
        Drawable drawable = this.m;
        if (drawable != null) {
            drawable.setBounds(this.c, this.e, i3 - this.d, i2 - this.f);
        }
    }

    @DexIgnore
    public final void C() {
        C04 d2 = d();
        C04 l2 = l();
        if (d2 != null) {
            d2.e0((float) this.h, this.k);
            if (l2 != null) {
                l2.d0((float) this.h, this.n ? Vx3.c(this.a, Jw3.colorSurface) : 0);
            }
        }
    }

    @DexIgnore
    public final InsetDrawable D(Drawable drawable) {
        return new InsetDrawable(drawable, this.c, this.e, this.d, this.f);
    }

    @DexIgnore
    public final Drawable a() {
        C04 c04 = new C04(this.b);
        c04.M(this.a.getContext());
        Am0.o(c04, this.j);
        PorterDuff.Mode mode = this.i;
        if (mode != null) {
            Am0.p(c04, mode);
        }
        c04.e0((float) this.h, this.k);
        C04 c042 = new C04(this.b);
        c042.setTint(0);
        c042.d0((float) this.h, this.n ? Vx3.c(this.a, Jw3.colorSurface) : 0);
        if (s) {
            C04 c043 = new C04(this.b);
            this.m = c043;
            Am0.n(c043, -1);
            RippleDrawable rippleDrawable = new RippleDrawable(Tz3.d(this.l), D(new LayerDrawable(new Drawable[]{c042, c04})), this.m);
            this.r = rippleDrawable;
            return rippleDrawable;
        }
        Sz3 sz3 = new Sz3(this.b);
        this.m = sz3;
        Am0.o(sz3, Tz3.d(this.l));
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{c042, c04, this.m});
        this.r = layerDrawable;
        return D(layerDrawable);
    }

    @DexIgnore
    public int b() {
        return this.g;
    }

    @DexIgnore
    public J04 c() {
        LayerDrawable layerDrawable = this.r;
        if (layerDrawable == null || layerDrawable.getNumberOfLayers() <= 1) {
            return null;
        }
        return this.r.getNumberOfLayers() > 2 ? (J04) this.r.getDrawable(2) : (J04) this.r.getDrawable(1);
    }

    @DexIgnore
    public C04 d() {
        return e(false);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    public final C04 e(boolean z) {
        LayerDrawable layerDrawable = this.r;
        if (layerDrawable == null || layerDrawable.getNumberOfLayers() <= 0) {
            return null;
        }
        return s ? (C04) ((LayerDrawable) ((InsetDrawable) this.r.getDrawable(0)).getDrawable()).getDrawable(!z) : (C04) this.r.getDrawable(!z ? 1 : 0);
    }

    @DexIgnore
    public ColorStateList f() {
        return this.l;
    }

    @DexIgnore
    public G04 g() {
        return this.b;
    }

    @DexIgnore
    public ColorStateList h() {
        return this.k;
    }

    @DexIgnore
    public int i() {
        return this.h;
    }

    @DexIgnore
    public ColorStateList j() {
        return this.j;
    }

    @DexIgnore
    public PorterDuff.Mode k() {
        return this.i;
    }

    @DexIgnore
    public final C04 l() {
        return e(true);
    }

    @DexIgnore
    public boolean m() {
        return this.o;
    }

    @DexIgnore
    public boolean n() {
        return this.q;
    }

    @DexIgnore
    public void o(TypedArray typedArray) {
        this.c = typedArray.getDimensionPixelOffset(Tw3.MaterialButton_android_insetLeft, 0);
        this.d = typedArray.getDimensionPixelOffset(Tw3.MaterialButton_android_insetRight, 0);
        this.e = typedArray.getDimensionPixelOffset(Tw3.MaterialButton_android_insetTop, 0);
        this.f = typedArray.getDimensionPixelOffset(Tw3.MaterialButton_android_insetBottom, 0);
        if (typedArray.hasValue(Tw3.MaterialButton_cornerRadius)) {
            int dimensionPixelSize = typedArray.getDimensionPixelSize(Tw3.MaterialButton_cornerRadius, -1);
            this.g = dimensionPixelSize;
            u(this.b.w((float) dimensionPixelSize));
            this.p = true;
        }
        this.h = typedArray.getDimensionPixelSize(Tw3.MaterialButton_strokeWidth, 0);
        this.i = Kz3.e(typedArray.getInt(Tw3.MaterialButton_backgroundTintMode, -1), PorterDuff.Mode.SRC_IN);
        this.j = Oz3.a(this.a.getContext(), typedArray, Tw3.MaterialButton_backgroundTint);
        this.k = Oz3.a(this.a.getContext(), typedArray, Tw3.MaterialButton_strokeColor);
        this.l = Oz3.a(this.a.getContext(), typedArray, Tw3.MaterialButton_rippleColor);
        this.q = typedArray.getBoolean(Tw3.MaterialButton_android_checkable, false);
        int dimensionPixelSize2 = typedArray.getDimensionPixelSize(Tw3.MaterialButton_elevation, 0);
        int E = Mo0.E(this.a);
        int paddingTop = this.a.getPaddingTop();
        int D = Mo0.D(this.a);
        int paddingBottom = this.a.getPaddingBottom();
        this.a.setInternalBackground(a());
        C04 d2 = d();
        if (d2 != null) {
            d2.U((float) dimensionPixelSize2);
        }
        Mo0.A0(this.a, E + this.c, paddingTop + this.e, D + this.d, paddingBottom + this.f);
    }

    @DexIgnore
    public void p(int i2) {
        if (d() != null) {
            d().setTint(i2);
        }
    }

    @DexIgnore
    public void q() {
        this.o = true;
        this.a.setSupportBackgroundTintList(this.j);
        this.a.setSupportBackgroundTintMode(this.i);
    }

    @DexIgnore
    public void r(boolean z) {
        this.q = z;
    }

    @DexIgnore
    public void s(int i2) {
        if (!this.p || this.g != i2) {
            this.g = i2;
            this.p = true;
            u(this.b.w((float) i2));
        }
    }

    @DexIgnore
    public void t(ColorStateList colorStateList) {
        if (this.l != colorStateList) {
            this.l = colorStateList;
            if (s && (this.a.getBackground() instanceof RippleDrawable)) {
                ((RippleDrawable) this.a.getBackground()).setColor(Tz3.d(colorStateList));
            } else if (!s && (this.a.getBackground() instanceof Sz3)) {
                ((Sz3) this.a.getBackground()).setTintList(Tz3.d(colorStateList));
            }
        }
    }

    @DexIgnore
    public void u(G04 g04) {
        this.b = g04;
        A(g04);
    }

    @DexIgnore
    public void v(boolean z) {
        this.n = z;
        C();
    }

    @DexIgnore
    public void w(ColorStateList colorStateList) {
        if (this.k != colorStateList) {
            this.k = colorStateList;
            C();
        }
    }

    @DexIgnore
    public void x(int i2) {
        if (this.h != i2) {
            this.h = i2;
            C();
        }
    }

    @DexIgnore
    public void y(ColorStateList colorStateList) {
        if (this.j != colorStateList) {
            this.j = colorStateList;
            if (d() != null) {
                Am0.o(d(), this.j);
            }
        }
    }

    @DexIgnore
    public void z(PorterDuff.Mode mode) {
        if (this.i != mode) {
            this.i = mode;
            if (d() != null && this.i != null) {
                Am0.p(d(), this.i);
            }
        }
    }
}
