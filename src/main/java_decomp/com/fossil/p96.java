package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P96 implements Factory<EditPhotoViewModel> {
    @DexIgnore
    public /* final */ Provider<WFBackgroundPhotoRepository> a;

    @DexIgnore
    public P96(Provider<WFBackgroundPhotoRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static P96 a(Provider<WFBackgroundPhotoRepository> provider) {
        return new P96(provider);
    }

    @DexIgnore
    public static EditPhotoViewModel c(WFBackgroundPhotoRepository wFBackgroundPhotoRepository) {
        return new EditPhotoViewModel(wFBackgroundPhotoRepository);
    }

    @DexIgnore
    public EditPhotoViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
