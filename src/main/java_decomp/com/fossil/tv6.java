package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.DeviceRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tv6 extends pv6 {
    @DexIgnore
    public /* final */ qv6 e;
    @DexIgnore
    public /* final */ mj5 f;
    @DexIgnore
    public /* final */ DeviceRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp $portfolioApp;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ tv6 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.tv6$a$a")
        /* renamed from: com.fossil.tv6$a$a  reason: collision with other inner class name */
        public static final class C0245a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0245a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0245a aVar = new C0245a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((C0245a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                String str;
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    String J = this.this$0.$portfolioApp.J();
                    String deviceNameBySerial = this.this$0.this$0.g.getDeviceNameBySerial(J);
                    MisfitDeviceProfile m = nk5.o.j().m(J);
                    if (m == null || (str = m.getFirmwareVersion()) == null) {
                        str = "";
                    }
                    ck5.f.g().h(nk5.o.m(J), deviceNameBySerial, str);
                    FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, J, "ExploreWatchPresenter", "[Sync Start] AUTO SYNC after pair new device");
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(PortfolioApp portfolioApp, qn7 qn7, tv6 tv6) {
            super(2, qn7);
            this.$portfolioApp = portfolioApp;
            this.this$0 = tv6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.$portfolioApp, qn7, this.this$0);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 b = bw7.b();
                C0245a aVar = new C0245a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(b, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.$portfolioApp.S1(this.this$0.f, false, 13);
            this.this$0.u().j3();
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter$start$1", f = "ExploreWatchPresenter.kt", l = {40}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ tv6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter$start$1$data$1", f = "ExploreWatchPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<? extends Explore>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends Explore>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.t();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(tv6 tv6, qn7 qn7) {
            super(2, qn7);
            this.this$0 = tv6;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 h = this.this$0.h();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                g = eu7.g(h, aVar, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.u().z5((List) g);
            this.this$0.u().f();
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public tv6(qv6 qv6, mj5 mj5, DeviceRepository deviceRepository) {
        pq7.c(qv6, "mView");
        pq7.c(mj5, "mDeviceSettingFactory");
        pq7.c(deviceRepository, "mDeviceRepository");
        this.e = qv6;
        this.f = mj5;
        this.g = deviceRepository;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        xw7 unused = gu7.d(k(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.pv6
    public void n() {
        xw7 unused = gu7.d(k(), null, null, new a(PortfolioApp.h0.c(), null, this), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.pv6
    public void o(boolean z) {
    }

    @DexIgnore
    public final List<Explore> t() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        explore.setTitle(um5.c(PortfolioApp.h0.c(), 2131887042));
        explore.setDescription(um5.c(PortfolioApp.h0.c(), 2131887039));
        explore.setExploreType(Explore.ExploreType.WRIST_FLICK);
        Explore explore2 = new Explore();
        explore2.setTitle(um5.c(PortfolioApp.h0.c(), 2131887040));
        explore2.setDescription(um5.c(PortfolioApp.h0.c(), 2131887037));
        explore2.setExploreType(Explore.ExploreType.DOUBLE_TAP);
        Explore explore3 = new Explore();
        explore3.setTitle(um5.c(PortfolioApp.h0.c(), 2131887043));
        explore3.setDescription(um5.c(PortfolioApp.h0.c(), 2131887038));
        explore3.setExploreType(Explore.ExploreType.PRESS_AND_HOLD);
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        return arrayList;
    }

    @DexIgnore
    public final qv6 u() {
        return this.e;
    }

    @DexIgnore
    public void v() {
        this.e.M5(this);
    }
}
