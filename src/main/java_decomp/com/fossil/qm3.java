package com.fossil;

import android.os.Binder;
import android.os.Bundle;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qm3 extends Bl3 {
    @DexIgnore
    public /* final */ Yq3 b;
    @DexIgnore
    public Boolean c;
    @DexIgnore
    public String d;

    @DexIgnore
    public Qm3(Yq3 yq3) {
        this(yq3, null);
    }

    @DexIgnore
    public Qm3(Yq3 yq3, String str) {
        Rc2.k(yq3);
        this.b = yq3;
        this.d = null;
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void D1(Xr3 xr3) {
        Rc2.k(xr3);
        Rc2.k(xr3.d);
        X2(xr3.b, true);
        n(new Vm3(this, new Xr3(xr3)));
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void E1(Vg3 vg3, Or3 or3) {
        Rc2.k(vg3);
        Z2(or3, false);
        n(new Bn3(this, vg3, or3));
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void K1(Vg3 vg3, String str, String str2) {
        Rc2.k(vg3);
        Rc2.g(str);
        X2(str, true);
        n(new An3(this, vg3, str));
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void N0(long j, String str, String str2, String str3) {
        n(new Hn3(this, str2, str3, str, j));
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void P1(Or3 or3) {
        Z2(or3, false);
        n(new Sm3(this, or3));
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final List<Fr3> S(String str, String str2, String str3, boolean z) {
        X2(str, true);
        try {
            List<Hr3> list = (List) this.b.c().v(new Xm3(this, str, str2, str3)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (Hr3 hr3 : list) {
                if (z || !Kr3.B0(hr3.c)) {
                    arrayList.add(new Fr3(hr3));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.b.d().F().c("Failed to get user properties as. appId", Kl3.w(str), e);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void S0(Or3 or3) {
        X2(or3.b, false);
        n(new Ym3(this, or3));
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final List<Xr3> T0(String str, String str2, String str3) {
        X2(str, true);
        try {
            return (List) this.b.c().v(new Zm3(this, str, str2, str3)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.b.d().F().b("Failed to get conditional user properties as", e);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final List<Xr3> V0(String str, String str2, Or3 or3) {
        Z2(or3, false);
        try {
            return (List) this.b.c().v(new Wm3(this, or3, str, str2)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.b.d().F().b("Failed to get conditional user properties", e);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    public final void X2(String str, boolean z) {
        boolean z2 = false;
        if (!TextUtils.isEmpty(str)) {
            if (z) {
                try {
                    if (this.c == null) {
                        if ("com.google.android.gms".equals(this.d) || Pf2.a(this.b.e(), Binder.getCallingUid()) || I62.a(this.b.e()).c(Binder.getCallingUid())) {
                            z2 = true;
                        }
                        this.c = Boolean.valueOf(z2);
                    }
                    if (this.c.booleanValue()) {
                        return;
                    }
                } catch (SecurityException e) {
                    this.b.d().F().b("Measurement Service called with invalid calling package. appId", Kl3.w(str));
                    throw e;
                }
            }
            if (this.d == null && H62.n(this.b.e(), Binder.getCallingUid(), str)) {
                this.d = str;
            }
            if (!str.equals(this.d)) {
                throw new SecurityException(String.format("Unknown calling package name '%s'.", str));
            }
            return;
        }
        this.b.d().F().a("Measurement Service called without app package");
        throw new SecurityException("Measurement Service called without app package");
    }

    @DexIgnore
    public final Vg3 Y2(Vg3 vg3, Or3 or3) {
        Ug3 ug3;
        boolean z = false;
        if (!(!"_cmp".equals(vg3.b) || (ug3 = vg3.c) == null || ug3.c() == 0)) {
            String F = vg3.c.F("_cis");
            if (!TextUtils.isEmpty(F) && (("referrer broadcast".equals(F) || "referrer API".equals(F)) && this.b.G().B(or3.b, Xg3.S))) {
                z = true;
            }
        }
        if (!z) {
            return vg3;
        }
        this.b.d().L().b("Event has been filtered ", vg3.toString());
        return new Vg3("_cmpx", vg3.c, vg3.d, vg3.e);
    }

    @DexIgnore
    public final void Z2(Or3 or3, boolean z) {
        Rc2.k(or3);
        X2(or3.b, false);
        this.b.a0().h0(or3.c, or3.x, or3.B);
    }

    @DexIgnore
    public final /* synthetic */ void i(Or3 or3, Bundle bundle) {
        this.b.U().V(or3.b, bundle);
    }

    @DexIgnore
    public final void n(Runnable runnable) {
        Rc2.k(runnable);
        if (this.b.c().G()) {
            runnable.run();
        } else {
            this.b.c().y(runnable);
        }
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final String n0(Or3 or3) {
        Z2(or3, false);
        return this.b.T(or3);
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void n2(Bundle bundle, Or3 or3) {
        if (J73.a() && this.b.G().s(Xg3.O0)) {
            Z2(or3, false);
            n(new Tm3(this, or3, bundle));
        }
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final List<Fr3> o1(String str, String str2, boolean z, Or3 or3) {
        Z2(or3, false);
        try {
            List<Hr3> list = (List) this.b.c().v(new Um3(this, or3, str, str2)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (Hr3 hr3 : list) {
                if (z || !Kr3.B0(hr3.c)) {
                    arrayList.add(new Fr3(hr3));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.b.d().F().c("Failed to query user properties. appId", Kl3.w(or3.b), e);
            return Collections.emptyList();
        }
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final List<Fr3> p1(Or3 or3, boolean z) {
        Z2(or3, false);
        try {
            List<Hr3> list = (List) this.b.c().v(new Fn3(this, or3)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (Hr3 hr3 : list) {
                if (z || !Kr3.B0(hr3.c)) {
                    arrayList.add(new Fr3(hr3));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            this.b.d().F().c("Failed to get user properties. appId", Kl3.w(or3.b), e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void r1(Or3 or3) {
        Z2(or3, false);
        n(new En3(this, or3));
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void s(Xr3 xr3, Or3 or3) {
        Rc2.k(xr3);
        Rc2.k(xr3.d);
        Z2(or3, false);
        Xr3 xr32 = new Xr3(xr3);
        xr32.b = or3.b;
        n(new Gn3(this, xr32, or3));
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void u2(Fr3 fr3, Or3 or3) {
        Rc2.k(fr3);
        Z2(or3, false);
        n(new Cn3(this, fr3, or3));
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final byte[] w2(Vg3 vg3, String str) {
        Rc2.g(str);
        Rc2.k(vg3);
        X2(str, true);
        this.b.d().M().b("Log and bundle. event", this.b.Z().v(vg3.b));
        long a2 = this.b.zzm().a() / 1000000;
        try {
            byte[] bArr = (byte[]) this.b.c().A(new Dn3(this, vg3, str)).get();
            if (bArr == null) {
                this.b.d().F().b("Log and bundle returned null. appId", Kl3.w(str));
                bArr = new byte[0];
            }
            this.b.d().M().d("Log and bundle processed. event, size, time_ms", this.b.Z().v(vg3.b), Integer.valueOf(bArr.length), Long.valueOf((this.b.zzm().a() / 1000000) - a2));
            return bArr;
        } catch (InterruptedException | ExecutionException e) {
            this.b.d().F().d("Failed to log and bundle. appId, event, error", Kl3.w(str), this.b.Z().v(vg3.b), e);
            return null;
        }
    }
}
