package com.fossil;

import com.mapped.Af6;
import com.mapped.Qg6;
import com.mapped.Ve6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hv7 extends Ve6 {
    @DexIgnore
    public static /* final */ Ai c; // = new Ai(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Af6.Ci<Hv7> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public final String M() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof Hv7) && Wg6.a(this.b, ((Hv7) obj).b));
    }

    @DexIgnore
    public int hashCode() {
        String str = this.b;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "CoroutineName(" + this.b + ')';
    }
}
