package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Zl7<E> extends Wl7<E> implements List<E>, Jr7 {
    @DexIgnore
    public static /* final */ Ai b; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(int i, int i2) {
            if (i < 0 || i >= i2) {
                throw new IndexOutOfBoundsException("index: " + i + ", size: " + i2);
            }
        }

        @DexIgnore
        public final void b(int i, int i2) {
            if (i < 0 || i > i2) {
                throw new IndexOutOfBoundsException("index: " + i + ", size: " + i2);
            }
        }

        @DexIgnore
        public final void c(int i, int i2, int i3) {
            if (i < 0 || i2 > i3) {
                throw new IndexOutOfBoundsException("fromIndex: " + i + ", toIndex: " + i2 + ", size: " + i3);
            } else if (i > i2) {
                throw new IllegalArgumentException("fromIndex: " + i + " > toIndex: " + i2);
            }
        }

        @DexIgnore
        public final boolean d(Collection<?> collection, Collection<?> collection2) {
            Wg6.c(collection, "c");
            Wg6.c(collection2, FacebookRequestErrorClassification.KEY_OTHER);
            if (collection.size() != collection2.size()) {
                return false;
            }
            Iterator<?> it = collection2.iterator();
            Iterator<?> it2 = collection.iterator();
            while (it2.hasNext()) {
                if (!Wg6.a(it2.next(), it.next())) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public final int e(Collection<?> collection) {
            Wg6.c(collection, "c");
            Iterator<?> it = collection.iterator();
            int i = 1;
            while (it.hasNext()) {
                Object next = it.next();
                i = (next != null ? next.hashCode() : 0) + (i * 31);
            }
            return i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Iterator<E>, Jr7 {
        @DexIgnore
        public int b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi() {
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final void b(int i) {
            this.b = i;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b < Zl7.this.size();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public E next() {
            if (hasNext()) {
                Zl7 zl7 = Zl7.this;
                int i = this.b;
                this.b = i + 1;
                return (E) zl7.get(i);
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Zl7<E>.b implements ListIterator<E>, Jr7 {
        @DexIgnore
        public Ci(int i) {
            super();
            Zl7.b.b(i, Zl7.this.size());
            b(i);
        }

        @DexIgnore
        @Override // java.util.ListIterator
        public void add(E e) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @DexIgnore
        public boolean hasPrevious() {
            return a() > 0;
        }

        @DexIgnore
        public int nextIndex() {
            return a();
        }

        @DexIgnore
        @Override // java.util.ListIterator
        public E previous() {
            if (hasPrevious()) {
                Zl7 zl7 = Zl7.this;
                b(a() - 1);
                return (E) zl7.get(a());
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public int previousIndex() {
            return a() - 1;
        }

        @DexIgnore
        @Override // java.util.ListIterator
        public void set(E e) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<E> extends Zl7<E> implements RandomAccess {
        @DexIgnore
        public int c;
        @DexIgnore
        public /* final */ Zl7<E> d;
        @DexIgnore
        public /* final */ int e;

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.Zl7<? extends E> */
        /* JADX WARN: Multi-variable type inference failed */
        public Di(Zl7<? extends E> zl7, int i, int i2) {
            Wg6.c(zl7, "list");
            this.d = zl7;
            this.e = i;
            Zl7.b.c(i, i2, zl7.size());
            this.c = i2 - this.e;
        }

        @DexIgnore
        @Override // com.fossil.Wl7
        public int a() {
            return this.c;
        }

        @DexIgnore
        @Override // java.util.List, com.fossil.Zl7
        public E get(int i) {
            Zl7.b.a(i, this.c);
            return this.d.get(this.e + i);
        }
    }

    @DexIgnore
    @Override // java.util.List
    public void add(int i, E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.List
    public boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        return b.d(this, (Collection) obj);
    }

    @DexIgnore
    @Override // java.util.List
    public abstract E get(int i);

    @DexIgnore
    public int hashCode() {
        return b.e(this);
    }

    @DexIgnore
    public int indexOf(Object obj) {
        int i = 0;
        for (E e : this) {
            if (Wg6.a(e, obj)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection, java.lang.Iterable
    public Iterator<E> iterator() {
        return new Bi();
    }

    @DexIgnore
    public int lastIndexOf(Object obj) {
        ListIterator<E> listIterator = listIterator(size());
        while (listIterator.hasPrevious()) {
            if (Wg6.a(listIterator.previous(), obj)) {
                return listIterator.nextIndex();
            }
        }
        return -1;
    }

    @DexIgnore
    @Override // java.util.List
    public ListIterator<E> listIterator() {
        return new Ci(0);
    }

    @DexIgnore
    @Override // java.util.List
    public ListIterator<E> listIterator(int i) {
        return new Ci(i);
    }

    @DexIgnore
    @Override // java.util.List
    public E remove(int i) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.List
    public E set(int i, E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.List
    public List<E> subList(int i, int i2) {
        return new Di(this, i, i2);
    }
}
