package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ae5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleCheckBox q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ LinearLayout u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ View x;

    @DexIgnore
    public Ae5(Object obj, View view, int i, FlexibleCheckBox flexibleCheckBox, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, LinearLayout linearLayout, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, View view2) {
        super(obj, view, i);
        this.q = flexibleCheckBox;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = flexibleTextView;
        this.u = linearLayout;
        this.v = flexibleTextView2;
        this.w = flexibleTextView3;
        this.x = view2;
    }

    @DexIgnore
    @Deprecated
    public static Ae5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (Ae5) ViewDataBinding.p(layoutInflater, 2131558672, viewGroup, z, obj);
    }

    @DexIgnore
    public static Ae5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, Aq0.d());
    }
}
