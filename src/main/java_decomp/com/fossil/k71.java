package com.fossil;

import android.view.View;
import coil.memory.ViewTargetRequestDelegate;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K71 implements View.OnAttachStateChangeListener {
    @DexIgnore
    public ViewTargetRequestDelegate b;
    @DexIgnore
    public volatile UUID c;
    @DexIgnore
    public volatile Rm6 d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f; // = true;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "coil.memory.ViewTargetRequestManager$clearCurrentRequest$1", f = "ViewTargetRequestManager.kt", l = {}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ K71 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(K71 k71, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = k71;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.c(null);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public final void a() {
        this.c = null;
        Rm6 rm6 = this.d;
        if (rm6 != null) {
            Rm6.Ai.a(rm6, null, 1, null);
        }
        this.d = Gu7.d(Jv7.a(Bw7.c().S()), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    public final UUID b() {
        UUID uuid = this.c;
        if (uuid != null && W81.n() && this.e) {
            return uuid;
        }
        UUID randomUUID = UUID.randomUUID();
        Wg6.b(randomUUID, "UUID.randomUUID()");
        return randomUUID;
    }

    @DexIgnore
    public final void c(ViewTargetRequestDelegate viewTargetRequestDelegate) {
        if (this.e) {
            this.e = false;
        } else {
            Rm6 rm6 = this.d;
            if (rm6 != null) {
                Rm6.Ai.a(rm6, null, 1, null);
            }
            this.d = null;
        }
        ViewTargetRequestDelegate viewTargetRequestDelegate2 = this.b;
        if (viewTargetRequestDelegate2 != null) {
            viewTargetRequestDelegate2.c();
        }
        this.b = viewTargetRequestDelegate;
        this.f = true;
    }

    @DexIgnore
    public final UUID d(Rm6 rm6) {
        Wg6.c(rm6, "job");
        UUID b2 = b();
        this.c = b2;
        return b2;
    }

    @DexIgnore
    public void onViewAttachedToWindow(View view) {
        Wg6.c(view, "v");
        if (this.f) {
            this.f = false;
            return;
        }
        ViewTargetRequestDelegate viewTargetRequestDelegate = this.b;
        if (viewTargetRequestDelegate != null) {
            this.e = true;
            viewTargetRequestDelegate.d();
        }
    }

    @DexIgnore
    public void onViewDetachedFromWindow(View view) {
        Wg6.c(view, "v");
        this.f = false;
        ViewTargetRequestDelegate viewTargetRequestDelegate = this.b;
        if (viewTargetRequestDelegate != null) {
            viewTargetRequestDelegate.c();
        }
    }
}
