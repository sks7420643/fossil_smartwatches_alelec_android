package com.fossil;

import com.mapped.U04;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.calories.DashboardCaloriesPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cg6 implements Factory<DashboardCaloriesPresenter> {
    @DexIgnore
    public static DashboardCaloriesPresenter a(Zf6 zf6, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, U04 u04) {
        return new DashboardCaloriesPresenter(zf6, summariesRepository, fitnessDataRepository, userRepository, u04);
    }
}
