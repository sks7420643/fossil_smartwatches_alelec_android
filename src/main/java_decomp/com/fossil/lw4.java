package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.F57;
import com.mapped.AlertDialogFragment;
import com.mapped.Cd6;
import com.mapped.Gg6;
import com.mapped.Hg6;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputActivity;
import com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendActivity;
import com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeActivity;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.customview.FriendsInView;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lw4 extends BaseFragment implements AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ int A; // = W6.d(PortfolioApp.get.instance(), 2131099689);
    @DexIgnore
    public static /* final */ int B; // = W6.d(PortfolioApp.get.instance(), 2131099677);
    @DexIgnore
    public static /* final */ String C; // = ThemeManager.l.a().d("nonBrandPlaceholderBackground");
    @DexIgnore
    public static /* final */ Ai D; // = new Ai(null);
    @DexIgnore
    public static /* final */ String z;
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public BCWaitingChallengeDetailViewModel h;
    @DexIgnore
    public Ps4 i;
    @DexIgnore
    public String j;
    @DexIgnore
    public int k; // = -1;
    @DexIgnore
    public String l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public G37<Yb5> s;
    @DexIgnore
    public Gg6<Cd6> t; // = Bi.INSTANCE;
    @DexIgnore
    public Gg6<Cd6> u; // = Ci.INSTANCE;
    @DexIgnore
    public /* final */ TimerViewObserver v; // = new TimerViewObserver();
    @DexIgnore
    public /* final */ F57.Bi w;
    @DexIgnore
    public /* final */ Uy4 x;
    @DexIgnore
    public HashMap y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Lw4.z;
        }

        @DexIgnore
        public final Lw4 b(Ps4 ps4, String str, int i, String str2, boolean z) {
            Lw4 lw4 = new Lw4();
            Bundle bundle = new Bundle();
            bundle.putParcelable("challenge_extra", ps4);
            bundle.putString("category_extra", str);
            bundle.putInt("index_extra", i);
            bundle.putString("challenge_id_extra", str2);
            bundle.putBoolean("about_extra", z);
            lw4.setArguments(bundle);
            return lw4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public static /* final */ Bi INSTANCE; // = new Bi();

        @DexIgnore
        public Bi() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public static /* final */ Ci INSTANCE; // = new Ci();

        @DexIgnore
        public Ci() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Gl7<? extends String, ? extends String, ? extends String>> {
        @DexIgnore
        public static /* final */ Di a; // = new Di();

        @DexIgnore
        public final void a(Gl7<String, String, String> gl7) {
            if (Wg6.a(gl7.getFirst(), "bc_left_challenge_before_start")) {
                Xr4.a.h(gl7.getFirst(), gl7.getSecond(), gl7.getThird(), 0, PortfolioApp.get.instance());
            } else {
                Xr4.a.a(gl7.getFirst(), gl7.getSecond(), gl7.getThird(), PortfolioApp.get.instance());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Gl7<? extends String, ? extends String, ? extends String> gl7) {
            a(gl7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<Ps4> {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 a;

        @DexIgnore
        public Ei(Lw4 lw4) {
            this.a = lw4;
        }

        @DexIgnore
        public final void a(Ps4 ps4) {
            if (ps4 != null) {
                this.a.d7(ps4);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Ps4 ps4) {
            a(ps4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<Ps4> {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 a;

        @DexIgnore
        public Fi(Lw4 lw4) {
            this.a = lw4;
        }

        @DexIgnore
        public final void a(Ps4 ps4) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Lw4.D.a();
            local.e(a2, "pendingChallenge - data: " + ps4);
            if (ps4 != null) {
                this.a.d7(ps4);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Ps4 ps4) {
            a(ps4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> implements Ls0<Lc6<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 a;

        @DexIgnore
        public Gi(Lw4 lw4) {
            this.a = lw4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, ? extends ServerError> lc6) {
            Yb5 yb5 = (Yb5) Lw4.N6(this.a).a();
            if (yb5 != null) {
                boolean booleanValue = lc6.getFirst().booleanValue();
                ServerError serverError = (ServerError) lc6.getSecond();
                if (booleanValue) {
                    FlexibleTextView flexibleTextView = yb5.y;
                    Wg6.b(flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView2 = yb5.y;
                Wg6.b(flexibleTextView2, "ftvError");
                String c = Um5.c(flexibleTextView2.getContext(), 2131886231);
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    Toast.makeText(activity, c, 1).show();
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends ServerError> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> implements Ls0<Lc6<? extends Boolean, ? extends Boolean>> {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 a;

        @DexIgnore
        public Hi(Lw4 lw4) {
            this.a = lw4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, Boolean> lc6) {
            Yb5 yb5;
            Boolean first = lc6.getFirst();
            Boolean second = lc6.getSecond();
            if (!(first == null || (yb5 = (Yb5) Lw4.N6(this.a).a()) == null)) {
                SwipeRefreshLayout swipeRefreshLayout = yb5.G;
                Wg6.b(swipeRefreshLayout, "swipeRefresh");
                swipeRefreshLayout.setRefreshing(first.booleanValue());
            }
            if (second == null) {
                return;
            }
            if (Wg6.a(second, Boolean.TRUE)) {
                this.a.b();
            } else if (Wg6.a(second, Boolean.FALSE)) {
                this.a.a();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends Boolean> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii<T> implements Ls0<Lc6<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 a;

        @DexIgnore
        public Ii(Lw4 lw4) {
            this.a = lw4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, ? extends ServerError> lc6) {
            Intent intent;
            String str = null;
            if (lc6.getFirst().booleanValue()) {
                if (this.a.k != -1) {
                    intent = new Intent();
                    intent.putExtra("index_extra", this.a.k);
                } else {
                    intent = null;
                }
                this.a.b7(intent);
                return;
            }
            ServerError serverError = (ServerError) lc6.getSecond();
            this.a.h7(serverError != null ? serverError.getCode() : null, serverError != null ? serverError.getMessage() : null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Lw4.D.a();
            StringBuilder sb = new StringBuilder();
            sb.append("code: ");
            sb.append(serverError != null ? serverError.getCode() : null);
            sb.append(" - message: ");
            if (serverError != null) {
                str = serverError.getMessage();
            }
            sb.append(str);
            local.e(a2, sb.toString());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends ServerError> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji<T> implements Ls0<Gl7<? extends List<? extends At4>, ? extends ServerError, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 a;

        @DexIgnore
        public Ji(Lw4 lw4) {
            this.a = lw4;
        }

        @DexIgnore
        public final void a(Gl7<? extends List<At4>, ? extends ServerError, Integer> gl7) {
            Yb5 yb5 = (Yb5) Lw4.N6(this.a).a();
            if (yb5 != null) {
                List<At4> list = (List) gl7.getFirst();
                if (list != null && (!list.isEmpty())) {
                    FriendsInView friendsInView = yb5.t;
                    Wg6.b(friendsInView, "friendsInView");
                    friendsInView.setVisibility(0);
                    yb5.t.setData(list);
                }
                Integer third = gl7.getThird();
                if (third != null) {
                    Hr7 hr7 = Hr7.a;
                    String c = Um5.c(PortfolioApp.get.instance(), 2131886215);
                    Wg6.b(c, "LanguageHelper.getString\u2026_Subtitle__NumberMembers)");
                    String format = String.format(c, Arrays.copyOf(new Object[]{third}, 1));
                    Wg6.b(format, "java.lang.String.format(format, *args)");
                    FlexibleTextView flexibleTextView = yb5.w;
                    Wg6.b(flexibleTextView, "ftvCountMember");
                    flexibleTextView.setText(Jl5.b(Jl5.b, String.valueOf(third.intValue()), format, 0, 4, null));
                    FlexibleTextView flexibleTextView2 = yb5.w;
                    Wg6.b(flexibleTextView2, "ftvCountMember");
                    flexibleTextView2.setTag(third);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Gl7<? extends List<? extends At4>, ? extends ServerError, ? extends Integer> gl7) {
            a(gl7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki<T> implements Ls0<List<? extends Gs4>> {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 a;

        @DexIgnore
        public Ki(Lw4 lw4) {
            this.a = lw4;
        }

        @DexIgnore
        public final void a(List<Gs4> list) {
            Lw4 lw4 = this.a;
            Wg6.b(list, "it");
            lw4.f7(list);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends Gs4> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li<T> implements Ls0<Lc6<? extends Vy4, ? extends Ps4>> {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 a;

        @DexIgnore
        public Li(Lw4 lw4) {
            this.a = lw4;
        }

        @DexIgnore
        public final void a(Lc6<? extends Vy4, Ps4> lc6) {
            int i = Mw4.a[((Vy4) lc6.getFirst()).ordinal()];
            if (i == 1) {
                this.a.i7(lc6.getSecond());
            } else if (i == 2) {
                this.a.j7(lc6.getSecond());
            } else if (i != 3) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = Lw4.D.a();
                local.e(a2, "optionActionLive - wrong option: " + lc6);
            } else {
                this.a.g7();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Vy4, ? extends Ps4> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi<T> implements Ls0<Iz4> {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 a;

        @DexIgnore
        public Mi(Lw4 lw4) {
            this.a = lw4;
        }

        @DexIgnore
        public final void a(Iz4 iz4) {
            boolean z = true;
            if (iz4 != null) {
                int i = Mw4.b[iz4.ordinal()];
                if (i == 1) {
                    S37 s37 = S37.c;
                    FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                    Wg6.b(childFragmentManager, "childFragmentManager");
                    String c = Um5.c(PortfolioApp.get.instance(), 2131886235);
                    Wg6.b(c, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    String c2 = Um5.c(PortfolioApp.get.instance(), 2131886233);
                    Wg6.b(c2, "LanguageHelper.getString\u2026uCanOnlyJoinOneChallenge)");
                    s37.E(childFragmentManager, c, c2);
                    return;
                } else if (i == 2) {
                    if (PortfolioApp.get.instance().J().length() <= 0) {
                        z = false;
                    }
                    if (z) {
                        S37 s372 = S37.c;
                        FragmentManager childFragmentManager2 = this.a.getChildFragmentManager();
                        Wg6.b(childFragmentManager2, "childFragmentManager");
                        String c3 = Um5.c(PortfolioApp.get.instance(), 2131886235);
                        Wg6.b(c3, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                        String c4 = Um5.c(PortfolioApp.get.instance(), 2131886234);
                        Wg6.b(c4, "LanguageHelper.getString\u2026IsDisconnectedPleaseSync)");
                        s372.E(childFragmentManager2, c3, c4);
                        return;
                    }
                    S37 s373 = S37.c;
                    FragmentManager childFragmentManager3 = this.a.getChildFragmentManager();
                    Wg6.b(childFragmentManager3, "childFragmentManager");
                    String c5 = Um5.c(PortfolioApp.get.instance(), 2131886235);
                    Wg6.b(c5, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    String c6 = Um5.c(PortfolioApp.get.instance(), 2131886226);
                    Wg6.b(c6, "LanguageHelper.getString\u2026ctivateYourDeviceToJoinA)");
                    s373.E(childFragmentManager3, c5, c6);
                    return;
                } else if (i == 3) {
                    S37 s374 = S37.c;
                    FragmentManager childFragmentManager4 = this.a.getChildFragmentManager();
                    Wg6.b(childFragmentManager4, "childFragmentManager");
                    s374.C0(childFragmentManager4);
                    return;
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Lw4.D.a();
            local.e(a2, "warningTypeDialogLive - wrong type: " + iz4);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Iz4 iz4) {
            a(iz4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Ps4 $challenge$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ String $subject$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Yb5 $this_run$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ni(Yb5 yb5, String str, Lw4 lw4, Ps4 ps4) {
            super(0);
            this.$this_run$inlined = yb5;
            this.$subject$inlined = str;
            this.this$0 = lw4;
            this.$challenge$inlined = ps4;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            Lw4.S6(this.this$0).a(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Ps4 $challenge$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ String $subject$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Yb5 $this_run$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Oi(Yb5 yb5, String str, Lw4 lw4, Ps4 ps4) {
            super(0);
            this.$this_run$inlined = yb5;
            this.$subject$inlined = str;
            this.this$0 = lw4;
            this.$challenge$inlined = ps4;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            Lw4.S6(this.this$0).a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Ps4 $challenge$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Pi(Lw4 lw4, Ps4 ps4) {
            super(0);
            this.this$0 = lw4;
            this.$challenge$inlined = ps4;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            Date m = this.$challenge$inlined.m();
            this.this$0.k7(this.$challenge$inlined.f(), (m != null ? m.getTime() : 0) > Xy4.a.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Qi extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public static /* final */ Qi INSTANCE; // = new Qi();

        @DexIgnore
        public Qi() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ri implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 b;

        @DexIgnore
        public Ri(Lw4 lw4) {
            this.b = lw4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.B0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Si extends Qq7 implements Hg6<View, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Si(Lw4 lw4) {
            super(1);
            this.this$0 = lw4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(View view) {
            invoke(view);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            this.this$0.t.invoke();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ti extends Qq7 implements Hg6<View, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ti(Lw4 lw4) {
            super(1);
            this.this$0 = lw4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(View view) {
            invoke(view);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            Lw4.S6(this.this$0).D();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ui implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ Yb5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 b;

        @DexIgnore
        public Ui(Yb5 yb5, Lw4 lw4) {
            this.a = yb5;
            this.b = lw4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.a.y;
            Wg6.b(flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            Lw4.S6(this.b).G();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Vi extends Qq7 implements Hg6<View, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Vi(Lw4 lw4) {
            super(1);
            this.this$0 = lw4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(View view) {
            invoke(view);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            this.this$0.u.invoke();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Wi extends Qq7 implements Hg6<View, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Wi(Lw4 lw4) {
            super(1);
            this.this$0 = lw4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(View view) {
            invoke(view);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            Ps4 ps4 = this.this$0.i;
            if (ps4 != null) {
                Date m = ps4.m();
                this.this$0.k7(ps4.f(), (m != null ? m.getTime() : 0) > Xy4.a.b());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Xi implements My5 {
        @DexIgnore
        public /* final */ /* synthetic */ Lw4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Xi(Lw4 lw4) {
            this.a = lw4;
        }

        @DexIgnore
        @Override // com.fossil.My5
        public void a(Gs4 gs4) {
            Wg6.c(gs4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            BCWaitingChallengeDetailViewModel S6 = Lw4.S6(this.a);
            Object a2 = gs4.a();
            if (a2 != null) {
                S6.E((Vy4) a2);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.buddy_challenge.util.DetailOptionDialog");
        }
    }

    /*
    static {
        String simpleName = Lw4.class.getSimpleName();
        Wg6.b(simpleName, "BCWaitingChallengeDetail\u2026nt::class.java.simpleName");
        z = simpleName;
    }
    */

    @DexIgnore
    public Lw4() {
        F57.Bi f = F57.a().f();
        Wg6.b(f, "TextDrawable.builder().round()");
        this.w = f;
        this.x = Uy4.d.b();
    }

    @DexIgnore
    public static final /* synthetic */ G37 N6(Lw4 lw4) {
        G37<Yb5> g37 = lw4.s;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ BCWaitingChallengeDetailViewModel S6(Lw4 lw4) {
        BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel = lw4.h;
        if (bCWaitingChallengeDetailViewModel != null) {
            return bCWaitingChallengeDetailViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void B0() {
        if (this.l != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finishAffinity();
            }
            Context context = getContext();
            if (context != null) {
                HomeActivity.a aVar = HomeActivity.B;
                Wg6.b(context, "it");
                aVar.a(context, 1);
                return;
            }
            return;
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        if (str.hashCode() == 1970588827 && str.equals("LEAVE_CHALLENGE") && i2 == 2131363373) {
            BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel = this.h;
            if (bCWaitingChallengeDetailViewModel != null) {
                bCWaitingChallengeDetailViewModel.z();
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void a7() {
        Ps4 ps4 = this.i;
        if (ps4 != null) {
            Cl5.c.d(PortfolioApp.get.instance(), ps4.f().hashCode());
        }
        String str = this.l;
        if (str != null) {
            Cl5.c.d(PortfolioApp.get.instance(), str.hashCode());
        }
    }

    @DexIgnore
    public final void b7(Intent intent) {
        if (this.l != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finishAffinity();
            }
            Context context = getContext();
            if (context != null) {
                HomeActivity.a aVar = HomeActivity.B;
                Wg6.b(context, "it");
                aVar.a(context, 1);
                return;
            }
            return;
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.setResult(-1, intent);
        }
        FragmentActivity activity3 = getActivity();
        if (activity3 != null) {
            activity3.finish();
        }
    }

    @DexIgnore
    public final void c7() {
        BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel = this.h;
        if (bCWaitingChallengeDetailViewModel != null) {
            bCWaitingChallengeDetailViewModel.p().h(getViewLifecycleOwner(), new Ei(this));
            BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel2 = this.h;
            if (bCWaitingChallengeDetailViewModel2 != null) {
                LiveData<Ps4> s2 = bCWaitingChallengeDetailViewModel2.s();
                if (s2 != null) {
                    s2.h(getViewLifecycleOwner(), new Fi(this));
                }
                BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel3 = this.h;
                if (bCWaitingChallengeDetailViewModel3 != null) {
                    bCWaitingChallengeDetailViewModel3.q().h(getViewLifecycleOwner(), new Gi(this));
                    BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel4 = this.h;
                    if (bCWaitingChallengeDetailViewModel4 != null) {
                        bCWaitingChallengeDetailViewModel4.t().h(getViewLifecycleOwner(), new Hi(this));
                        BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel5 = this.h;
                        if (bCWaitingChallengeDetailViewModel5 != null) {
                            bCWaitingChallengeDetailViewModel5.u().h(getViewLifecycleOwner(), new Ii(this));
                            BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel6 = this.h;
                            if (bCWaitingChallengeDetailViewModel6 != null) {
                                bCWaitingChallengeDetailViewModel6.r().h(getViewLifecycleOwner(), new Ji(this));
                                BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel7 = this.h;
                                if (bCWaitingChallengeDetailViewModel7 != null) {
                                    bCWaitingChallengeDetailViewModel7.w().h(getViewLifecycleOwner(), new Ki(this));
                                    BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel8 = this.h;
                                    if (bCWaitingChallengeDetailViewModel8 != null) {
                                        bCWaitingChallengeDetailViewModel8.v().h(getViewLifecycleOwner(), new Li(this));
                                        BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel9 = this.h;
                                        if (bCWaitingChallengeDetailViewModel9 != null) {
                                            bCWaitingChallengeDetailViewModel9.x().h(getViewLifecycleOwner(), new Mi(this));
                                            BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel10 = this.h;
                                            if (bCWaitingChallengeDetailViewModel10 != null) {
                                                bCWaitingChallengeDetailViewModel10.o().h(getViewLifecycleOwner(), Di.a);
                                            } else {
                                                Wg6.n("viewModel");
                                                throw null;
                                            }
                                        } else {
                                            Wg6.n("viewModel");
                                            throw null;
                                        }
                                    } else {
                                        Wg6.n("viewModel");
                                        throw null;
                                    }
                                } else {
                                    Wg6.n("viewModel");
                                    throw null;
                                }
                            } else {
                                Wg6.n("viewModel");
                                throw null;
                            }
                        } else {
                            Wg6.n("viewModel");
                            throw null;
                        }
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0534  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void d7(com.fossil.Ps4 r12) {
        /*
        // Method dump skipped, instructions count: 1342
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Lw4.d7(com.fossil.Ps4):void");
    }

    @DexIgnore
    public final void e7() {
        G37<Yb5> g37 = this.s;
        if (g37 != null) {
            Yb5 a2 = g37.a();
            if (a2 != null) {
                a2.C.setOnClickListener(new Ri(this));
                String str = C;
                if (str != null) {
                    a2.w.setBackgroundColor(Color.parseColor(str));
                }
                FlexibleButton flexibleButton = a2.q;
                Wg6.b(flexibleButton, "btnAction");
                Fz4.a(flexibleButton, new Si(this));
                RTLImageView rTLImageView = a2.B;
                Wg6.b(rTLImageView, "imgOption");
                Fz4.a(rTLImageView, new Ti(this));
                a2.G.setOnRefreshListener(new Ui(a2, this));
                FlexibleTextView flexibleTextView = a2.w;
                Wg6.b(flexibleTextView, "ftvCountMember");
                Fz4.a(flexibleTextView, new Vi(this));
                FriendsInView friendsInView = a2.t;
                Wg6.b(friendsInView, "friendsInView");
                Fz4.a(friendsInView, new Wi(this));
                return;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final void f7(List<Gs4> list) {
        Es4 b = Es4.A.b();
        String c = Um5.c(requireContext(), 2131886322);
        Wg6.b(c, "LanguageHelper.getString\u2026rBoard_Menu_Title__Allow)");
        b.setTitle(c);
        b.E6(list);
        b.G6(new Xi(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        b.show(childFragmentManager, Es4.A.a());
    }

    @DexIgnore
    public final void g7() {
        S37 s37 = S37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        s37.h(childFragmentManager);
    }

    @DexIgnore
    public final void h7(Integer num, String str) {
        if (num == null) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.C(childFragmentManager);
            return;
        }
        S37 s372 = S37.c;
        int intValue = num.intValue();
        FragmentManager childFragmentManager2 = getChildFragmentManager();
        Wg6.b(childFragmentManager2, "childFragmentManager");
        s372.n0(intValue, str, childFragmentManager2);
    }

    @DexIgnore
    public final void i7(Ps4 ps4) {
        BCCreateChallengeInputActivity.A.a(this, Py4.g(ps4));
    }

    @DexIgnore
    public final void j7(Ps4 ps4) {
        BCInviteFriendActivity.A.a(this, Py4.g(ps4));
    }

    @DexIgnore
    public final void k7(String str, boolean z2) {
        BCMemberInChallengeActivity.A.a(this, str, z2);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        this.i = arguments != null ? (Ps4) arguments.getParcelable("challenge_extra") : null;
        Bundle arguments2 = getArguments();
        this.j = arguments2 != null ? arguments2.getString("category_extra") : null;
        Bundle arguments3 = getArguments();
        this.k = arguments3 != null ? arguments3.getInt("index_extra") : -1;
        Bundle arguments4 = getArguments();
        this.l = arguments4 != null ? arguments4.getString("challenge_id_extra") : null;
        Bundle arguments5 = getArguments();
        this.m = arguments5 != null ? arguments5.getBoolean("about_extra") : false;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = z;
        local.e(str, "onCreate - challenge: " + this.i + " - category: " + this.j + " - index: " + this.k + " - visitId: " + this.l + " - about: " + this.m);
        a7();
        PortfolioApp.get.instance().getIface().Y0().a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCWaitingChallengeDetailViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ailViewModel::class.java)");
            this.h = (BCWaitingChallengeDetailViewModel) a2;
            getLifecycle().a(this.v);
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Yb5 yb5 = (Yb5) Aq0.f(layoutInflater, 2131558635, viewGroup, false, A6());
        this.s = new G37<>(this, yb5);
        Wg6.b(yb5, "binding");
        return yb5.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel = this.h;
        if (bCWaitingChallengeDetailViewModel != null) {
            bCWaitingChallengeDetailViewModel.F();
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel = this.h;
        if (bCWaitingChallengeDetailViewModel != null) {
            bCWaitingChallengeDetailViewModel.H();
            AnalyticsHelper g2 = AnalyticsHelper.f.g();
            FragmentActivity activity = getActivity();
            if (activity != null) {
                g2.m("bc_challenge_detail", activity);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type android.app.Activity");
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        BCWaitingChallengeDetailViewModel bCWaitingChallengeDetailViewModel = this.h;
        if (bCWaitingChallengeDetailViewModel != null) {
            bCWaitingChallengeDetailViewModel.y(this.i, this.l);
            e7();
            c7();
            Ps4 ps4 = this.i;
            if (ps4 != null) {
                d7(ps4);
                return;
            }
            return;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.y;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
