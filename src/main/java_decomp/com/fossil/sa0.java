package com.fossil;

import android.os.Parcel;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sa0 extends Va0 {
    @DexIgnore
    public static /* final */ Ra0 CREATOR; // = new Ra0(null);
    @DexIgnore
    public /* final */ Y90 c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore
    public /* synthetic */ Sa0(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = Y90.values()[parcel.readInt()];
        this.d = parcel.readInt() != 0;
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public byte[] a() {
        ByteBuffer order = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN);
        Wg6.b(order, "ByteBuffer.allocate(3)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.putShort(this.c.b);
        order.put((byte) ((this.c.c.b << 7) | (this.d ? 1 : 0)));
        byte[] array = order.array();
        Wg6.b(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Sa0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Sa0 sa0 = (Sa0) obj;
            if (this.c != sa0.c) {
                return false;
            }
            return this.d == sa0.d;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.HIDInstr");
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public int hashCode() {
        return (this.c.hashCode() * 31) + Boolean.valueOf(this.d).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Va0, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(super.toJSONObject(), Jd0.X3, Ey1.a(this.c)), Jd0.Y3, Boolean.valueOf(this.d));
    }

    @DexIgnore
    @Override // com.fossil.Va0
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.c.ordinal());
        }
        if (parcel != null) {
            parcel.writeInt(this.d ? 1 : 0);
        }
    }
}
