package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yj0 extends Uj0 {
    @DexIgnore
    public Uj0[] k0; // = new Uj0[4];
    @DexIgnore
    public int l0; // = 0;

    @DexIgnore
    public void I0(Uj0 uj0) {
        int i = this.l0;
        Uj0[] uj0Arr = this.k0;
        if (i + 1 > uj0Arr.length) {
            this.k0 = (Uj0[]) Arrays.copyOf(uj0Arr, uj0Arr.length * 2);
        }
        Uj0[] uj0Arr2 = this.k0;
        int i2 = this.l0;
        uj0Arr2[i2] = uj0;
        this.l0 = i2 + 1;
    }

    @DexIgnore
    public void J0() {
        this.l0 = 0;
    }
}
