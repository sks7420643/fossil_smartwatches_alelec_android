package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Q80 implements Runnable {
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ U80 c;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public Q80(U80 u80) {
        this.c = u80;
    }

    @DexIgnore
    public void run() {
        if (!this.b) {
            this.c.a();
            U80 u80 = this.c;
            Q80 q80 = u80.d;
            if (q80 != null) {
                u80.j.postDelayed(q80, u80.f);
            }
        }
    }
}
