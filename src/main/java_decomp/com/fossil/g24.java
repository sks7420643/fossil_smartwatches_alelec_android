package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class G24 extends Z24<Object, Object> {
    @DexIgnore
    public static /* final */ G24 INSTANCE; // = new G24();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    public G24() {
        super(A34.of(), 0);
    }

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }
}
