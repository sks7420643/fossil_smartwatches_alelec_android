package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class P14<T> extends H54<T> {
    @DexIgnore
    public Bi b; // = Bi.NOT_READY;
    @DexIgnore
    public T c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;

        /*
        static {
            int[] iArr = new int[Bi.values().length];
            a = iArr;
            try {
                iArr[Bi.DONE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Bi.READY.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
        */
    }

    @DexIgnore
    public enum Bi {
        READY,
        NOT_READY,
        DONE,
        FAILED
    }

    @DexIgnore
    public abstract T a();

    @DexIgnore
    @CanIgnoreReturnValue
    public final T b() {
        this.b = Bi.DONE;
        return null;
    }

    @DexIgnore
    public final boolean c() {
        this.b = Bi.FAILED;
        this.c = a();
        if (this.b == Bi.DONE) {
            return false;
        }
        this.b = Bi.READY;
        return true;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public final boolean hasNext() {
        I14.s(this.b != Bi.FAILED);
        int i = Ai.a[this.b.ordinal()];
        if (i == 1) {
            return false;
        }
        if (i != 2) {
            return c();
        }
        return true;
    }

    @DexIgnore
    @Override // java.util.Iterator
    @CanIgnoreReturnValue
    public final T next() {
        if (hasNext()) {
            this.b = Bi.NOT_READY;
            T t = this.c;
            this.c = null;
            return t;
        }
        throw new NoSuchElementException();
    }
}
