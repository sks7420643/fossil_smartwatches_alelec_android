package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Kv7 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[Lv7.values().length];
        a = iArr;
        iArr[Lv7.DEFAULT.ordinal()] = 1;
        a[Lv7.ATOMIC.ordinal()] = 2;
        a[Lv7.UNDISPATCHED.ordinal()] = 3;
        a[Lv7.LAZY.ordinal()] = 4;
        int[] iArr2 = new int[Lv7.values().length];
        b = iArr2;
        iArr2[Lv7.DEFAULT.ordinal()] = 1;
        b[Lv7.ATOMIC.ordinal()] = 2;
        b[Lv7.UNDISPATCHED.ordinal()] = 3;
        b[Lv7.LAZY.ordinal()] = 4;
    }
    */
}
