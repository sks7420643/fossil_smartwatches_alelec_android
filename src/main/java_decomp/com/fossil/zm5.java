package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zm5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ Gson f4501a; // = new Gson();
    @DexIgnore
    public /* final */ PortfolioApp b;
    @DexIgnore
    public /* final */ DeviceRepository c;
    @DexIgnore
    public /* final */ DianaAppSettingRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.CustomizeRealDataManager", f = "CustomizeRealDataManager.kt", l = {93, 121}, m = "getComplicationRealData")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zm5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(zm5 zm5, qn7 qn7) {
            super(qn7);
            this.this$0 = zm5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.CustomizeRealDataManager", f = "CustomizeRealDataManager.kt", l = {154}, m = "getComplicationRealData")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ zm5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(zm5 zm5, qn7 qn7) {
            super(qn7);
            this.this$0 = zm5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.CustomizeRealDataManager$getComplicationRealData$deviceInDb$1", f = "CustomizeRealDataManager.kt", l = {}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super Device>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ zm5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(zm5 zm5, qn7 qn7) {
            super(2, qn7);
            this.this$0 = zm5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Device> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return this.this$0.c.getDeviceBySerial(this.this$0.b.J());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.CustomizeRealDataManager$getComplicationRealData$secondTzComplication$2", f = "CustomizeRealDataManager.kt", l = {94}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super DianaAppSetting>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $complicationId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ zm5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(zm5 zm5, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = zm5;
            this.$complicationId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, this.$complicationId, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super DianaAppSetting> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                DianaAppSettingRepository dianaAppSettingRepository = this.this$0.d;
                String str = this.$complicationId;
                this.L$0 = iv7;
                this.label = 1;
                Object dianaAppSetting = dianaAppSettingRepository.getDianaAppSetting("complication", str, this);
                return dianaAppSetting == d ? d : dianaAppSetting;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    public zm5(PortfolioApp portfolioApp, DeviceRepository deviceRepository, DianaAppSettingRepository dianaAppSettingRepository) {
        pq7.c(portfolioApp, "mApp");
        pq7.c(deviceRepository, "mDeviceRepository");
        pq7.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        this.b = portfolioApp;
        this.c = deviceRepository;
        this.d = dianaAppSettingRepository;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r2v11 double), ('h' char)] */
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [('+' char), (r2v11 double), ('h' char)] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0084 A[Catch:{ Exception -> 0x0121 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.portfolio.platform.data.model.MFUser r9, java.util.List<com.portfolio.platform.data.model.CustomizeRealData> r10, java.lang.String r11, com.fossil.qn7<? super java.lang.String> r12) {
        /*
        // Method dump skipped, instructions count: 335
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zm5.d(com.portfolio.platform.data.model.MFUser, java.util.List, java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r0v92 int), ('%' char)] */
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0103: INVOKE  (r0v69 int) = (r0v68 float) type: STATIC call: com.fossil.lr7.b(float):int), (8451 char)] */
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x014d: INVOKE  (r1v22 int) = (wrap: float : 0x0149: INVOKE  (r1v21 float) = (r4v8 float) type: STATIC call: com.fossil.jk5.a(float):float) type: STATIC call: com.fossil.lr7.b(float):int), (8457 char)] */
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0270: INVOKE  (r1v9 int) = (r1v7 com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile) type: VIRTUAL call: com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile.getBatteryLevel():int), ('%' char)] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object e(java.util.List<com.portfolio.platform.data.model.CustomizeRealData> r10, java.lang.String r11, com.portfolio.platform.data.model.MFUser r12, com.fossil.qn7<? super java.lang.String> r13) {
        /*
        // Method dump skipped, instructions count: 728
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zm5.e(java.util.List, java.lang.String, com.portfolio.platform.data.model.MFUser, com.fossil.qn7):java.lang.Object");
    }
}
