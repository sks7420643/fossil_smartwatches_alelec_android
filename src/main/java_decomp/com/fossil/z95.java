package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Z95 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RelativeLayout A;
    @DexIgnore
    public /* final */ ConstraintLayout B;
    @DexIgnore
    public /* final */ Barrier q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ ImageView w;
    @DexIgnore
    public /* final */ ImageView x;
    @DexIgnore
    public /* final */ View y;
    @DexIgnore
    public /* final */ DashBar z;

    @DexIgnore
    public Z95(Object obj, View view, int i, Barrier barrier, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, ImageView imageView, ImageView imageView2, View view2, DashBar dashBar, RelativeLayout relativeLayout, ConstraintLayout constraintLayout) {
        super(obj, view, i);
        this.q = barrier;
        this.r = flexibleButton;
        this.s = flexibleButton2;
        this.t = flexibleTextView;
        this.u = flexibleTextView2;
        this.v = flexibleTextView3;
        this.w = imageView;
        this.x = imageView2;
        this.y = view2;
        this.z = dashBar;
        this.A = relativeLayout;
        this.B = constraintLayout;
    }
}
