package com.fossil;

import com.portfolio.platform.data.SignUpEmailAuth;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Zz6 extends Gq4<Yz6> {
    @DexIgnore
    void B1(SignUpEmailAuth signUpEmailAuth);

    @DexIgnore
    Object D5();  // void declaration

    @DexIgnore
    void M1(boolean z);

    @DexIgnore
    void T0(String str);

    @DexIgnore
    Object U1();  // void declaration

    @DexIgnore
    void c5(boolean z);

    @DexIgnore
    Object h();  // void declaration

    @DexIgnore
    Object i();  // void declaration

    @DexIgnore
    void l5(int i, String str);

    @DexIgnore
    void z1(String str, int i, int i2);
}
