package com.fossil;

import com.fossil.A18;
import com.fossil.U88;
import com.mapped.Xe6;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class H88<ResponseT, ReturnT> extends R88<ReturnT> {
    @DexIgnore
    public /* final */ P88 a;
    @DexIgnore
    public /* final */ A18.Ai b;
    @DexIgnore
    public /* final */ E88<W18, ResponseT> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<ResponseT, ReturnT> extends H88<ResponseT, ReturnT> {
        @DexIgnore
        public /* final */ B88<ResponseT, ReturnT> d;

        @DexIgnore
        public Ai(P88 p88, A18.Ai ai, E88<W18, ResponseT> e88, B88<ResponseT, ReturnT> b88) {
            super(p88, ai, e88);
            this.d = b88;
        }

        @DexIgnore
        @Override // com.fossil.H88
        public ReturnT c(Call<ResponseT> call, Object[] objArr) {
            return this.d.b(call);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<ResponseT> extends H88<ResponseT, Object> {
        @DexIgnore
        public /* final */ B88<ResponseT, Call<ResponseT>> d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public Bi(P88 p88, A18.Ai ai, E88<W18, ResponseT> e88, B88<ResponseT, Call<ResponseT>> b88, boolean z) {
            super(p88, ai, e88);
            this.d = b88;
            this.e = z;
        }

        @DexIgnore
        @Override // com.fossil.H88
        public Object c(Call<ResponseT> call, Object[] objArr) {
            Call<ResponseT> b = this.d.b(call);
            Xe6 xe6 = (Xe6) objArr[objArr.length - 1];
            try {
                return this.e ? J88.b(b, xe6) : J88.a(b, xe6);
            } catch (Exception e2) {
                return J88.d(e2, xe6);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<ResponseT> extends H88<ResponseT, Object> {
        @DexIgnore
        public /* final */ B88<ResponseT, Call<ResponseT>> d;

        @DexIgnore
        public Ci(P88 p88, A18.Ai ai, E88<W18, ResponseT> e88, B88<ResponseT, Call<ResponseT>> b88) {
            super(p88, ai, e88);
            this.d = b88;
        }

        @DexIgnore
        @Override // com.fossil.H88
        public Object c(Call<ResponseT> call, Object[] objArr) {
            Call<ResponseT> b = this.d.b(call);
            Xe6 xe6 = (Xe6) objArr[objArr.length - 1];
            try {
                return J88.c(b, xe6);
            } catch (Exception e) {
                return J88.d(e, xe6);
            }
        }
    }

    @DexIgnore
    public H88(P88 p88, A18.Ai ai, E88<W18, ResponseT> e88) {
        this.a = p88;
        this.b = ai;
        this.c = e88;
    }

    @DexIgnore
    public static <ResponseT, ReturnT> B88<ResponseT, ReturnT> d(Retrofit retrofit3, Method method, Type type, Annotation[] annotationArr) {
        try {
            return (B88<ResponseT, ReturnT>) retrofit3.a(type, annotationArr);
        } catch (RuntimeException e) {
            throw U88.o(method, e, "Unable to create call adapter for %s", type);
        }
    }

    @DexIgnore
    public static <ResponseT> E88<W18, ResponseT> e(Retrofit retrofit3, Method method, Type type) {
        try {
            return retrofit3.i(type, method.getAnnotations());
        } catch (RuntimeException e) {
            throw U88.o(method, e, "Unable to create converter for %s", type);
        }
    }

    @DexIgnore
    public static <ResponseT, ReturnT> H88<ResponseT, ReturnT> f(Retrofit retrofit3, Method method, P88 p88) {
        Type genericReturnType;
        boolean z;
        Annotation[] annotationArr;
        boolean z2 = p88.k;
        Annotation[] annotations = method.getAnnotations();
        if (z2) {
            Type[] genericParameterTypes = method.getGenericParameterTypes();
            Type g = U88.g(0, (ParameterizedType) genericParameterTypes[genericParameterTypes.length - 1]);
            if (U88.i(g) != Q88.class || !(g instanceof ParameterizedType)) {
                z = false;
            } else {
                g = U88.h(0, (ParameterizedType) g);
                z = true;
            }
            U88.Bi bi = new U88.Bi(null, Call.class, g);
            annotationArr = T88.a(annotations);
            genericReturnType = bi;
        } else {
            genericReturnType = method.getGenericReturnType();
            z = false;
            annotationArr = annotations;
        }
        B88 d = d(retrofit3, method, genericReturnType, annotationArr);
        Type a2 = d.a();
        if (a2 == Response.class) {
            throw U88.n(method, "'" + U88.i(a2).getName() + "' is not a valid response body type. Did you mean ResponseBody?", new Object[0]);
        } else if (a2 == Q88.class) {
            throw U88.n(method, "Response must include generic type (e.g., Response<String>)", new Object[0]);
        } else if (!p88.c.equals("HEAD") || Void.class.equals(a2)) {
            E88 e = e(retrofit3, method, a2);
            A18.Ai ai = retrofit3.b;
            return !z2 ? new Ai(p88, ai, e, d) : z ? new Ci(p88, ai, e, d) : new Bi(p88, ai, e, d, false);
        } else {
            throw U88.n(method, "HEAD method must use Void as response type.", new Object[0]);
        }
    }

    @DexIgnore
    @Override // com.fossil.R88
    public final ReturnT a(Object[] objArr) {
        return c(new K88(this.a, objArr, this.b, this.c), objArr);
    }

    @DexIgnore
    public abstract ReturnT c(Call<ResponseT> call, Object[] objArr);
}
