package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E38 {
    @DexIgnore
    public static /* final */ L48 d; // = L48.encodeUtf8(":");
    @DexIgnore
    public static /* final */ L48 e; // = L48.encodeUtf8(":status");
    @DexIgnore
    public static /* final */ L48 f; // = L48.encodeUtf8(":method");
    @DexIgnore
    public static /* final */ L48 g; // = L48.encodeUtf8(":path");
    @DexIgnore
    public static /* final */ L48 h; // = L48.encodeUtf8(":scheme");
    @DexIgnore
    public static /* final */ L48 i; // = L48.encodeUtf8(":authority");
    @DexIgnore
    public /* final */ L48 a;
    @DexIgnore
    public /* final */ L48 b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(P18 p18);
    }

    @DexIgnore
    public E38(L48 l48, L48 l482) {
        this.a = l48;
        this.b = l482;
        this.c = l48.size() + 32 + l482.size();
    }

    @DexIgnore
    public E38(L48 l48, String str) {
        this(l48, L48.encodeUtf8(str));
    }

    @DexIgnore
    public E38(String str, String str2) {
        this(L48.encodeUtf8(str), L48.encodeUtf8(str2));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof E38)) {
            return false;
        }
        E38 e38 = (E38) obj;
        return this.a.equals(e38.a) && this.b.equals(e38.b);
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() + 527) * 31) + this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return B28.r("%s: %s", this.a.utf8(), this.b.utf8());
    }
}
