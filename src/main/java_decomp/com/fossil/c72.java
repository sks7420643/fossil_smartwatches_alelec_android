package com.fossil;

import com.fossil.Z62;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class C72<R extends Z62, S extends Z62> {
    @DexIgnore
    public abstract Status a(Status status);

    @DexIgnore
    public abstract T62<S> b(R r);
}
