package com.fossil;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.F57;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hy4 {
    @DexIgnore
    public /* final */ F57.Bi a;
    @DexIgnore
    public /* final */ Uy4 b; // = Uy4.d.b();
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Ne5 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Ne5 ne5) {
            super(ne5.n());
            Wg6.c(ne5, "binding");
            this.a = ne5;
        }

        @DexIgnore
        public final void a(Xs4 xs4, F57.Bi bi, Uy4 uy4) {
            int i = 8;
            Wg6.c(xs4, "friend");
            Wg6.c(bi, "drawableBuilder");
            Wg6.c(uy4, "colorGenerator");
            Ne5 ne5 = this.a;
            String b = Hz4.a.b(xs4.b(), xs4.e(), xs4.i());
            FlexibleTextView flexibleTextView = ne5.v;
            Wg6.b(flexibleTextView, "tvFullName");
            flexibleTextView.setText(b);
            ImageView imageView = ne5.r;
            Wg6.b(imageView, "ivAvatar");
            Ty4.b(imageView, xs4.h(), b, bi, uy4);
            int c = xs4.c();
            if (c == -1) {
                ImageView imageView2 = ne5.s;
                Wg6.b(imageView2, "ivPending");
                imageView2.setVisibility(0);
                ImageView imageView3 = ne5.s;
                Wg6.b(imageView3, "ivPending");
                imageView3.setImageDrawable(W6.f(imageView3.getContext(), 2131231019));
                ProgressBar progressBar = ne5.u;
                Wg6.b(progressBar, "prg");
                progressBar.setVisibility(8);
            } else if (c == 0) {
                ImageView imageView4 = ne5.s;
                Wg6.b(imageView4, "ivPending");
                imageView4.setVisibility(8);
                ProgressBar progressBar2 = ne5.u;
                Wg6.b(progressBar2, "prg");
                if (xs4.f() != 0) {
                    i = 0;
                }
                progressBar2.setVisibility(i);
            } else if (c == 1) {
                ImageView imageView5 = ne5.s;
                Wg6.b(imageView5, "ivPending");
                imageView5.setVisibility(0);
                ImageView imageView6 = ne5.s;
                Wg6.b(imageView6, "ivPending");
                imageView6.setImageDrawable(W6.f(imageView6.getContext(), 2131231121));
                ProgressBar progressBar3 = ne5.u;
                Wg6.b(progressBar3, "prg");
                progressBar3.setVisibility(8);
            }
        }
    }

    @DexIgnore
    public Hy4(int i) {
        this.c = i;
        F57.Bi f = F57.a().f();
        Wg6.b(f, "TextDrawable.builder().round()");
        this.a = f;
    }

    @DexIgnore
    public final int a() {
        return this.c;
    }

    @DexIgnore
    public boolean b(List<? extends Object> list, int i) {
        Wg6.c(list, "items");
        Object obj = list.get(i);
        if (obj instanceof Xs4) {
            Xs4 xs4 = (Xs4) obj;
            if (xs4.c() == 0 || xs4.c() == 1) {
                return true;
            }
            if (xs4.c() == -1) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public void c(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        Ai ai = null;
        Wg6.c(list, "items");
        Wg6.c(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof Xs4)) {
            obj = null;
        }
        Xs4 xs4 = (Xs4) obj;
        if (viewHolder instanceof Ai) {
            ai = viewHolder;
        }
        Ai ai2 = ai;
        if (xs4 != null && ai2 != null) {
            ai2.a(xs4, this.a, this.b);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder d(ViewGroup viewGroup) {
        Wg6.c(viewGroup, "parent");
        Ne5 z = Ne5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemFriendListBinding.in\u2026.context), parent, false)");
        return new Ai(z);
    }
}
