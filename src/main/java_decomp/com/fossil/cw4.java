package com.fossil;

import com.portfolio.platform.buddy_challenge.domain.NotificationRepository;
import com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cw4 implements Factory<BCNotificationViewModel> {
    @DexIgnore
    public /* final */ Provider<NotificationRepository> a;

    @DexIgnore
    public Cw4(Provider<NotificationRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Cw4 a(Provider<NotificationRepository> provider) {
        return new Cw4(provider);
    }

    @DexIgnore
    public static BCNotificationViewModel c(NotificationRepository notificationRepository) {
        return new BCNotificationViewModel(notificationRepository);
    }

    @DexIgnore
    public BCNotificationViewModel b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
