package com.fossil;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class D62 {
    @DexIgnore
    public static /* final */ int a; // = H62.a;
    @DexIgnore
    public static /* final */ D62 b; // = new D62();

    @DexIgnore
    public static D62 h() {
        return b;
    }

    @DexIgnore
    public static String n(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("gcore_");
        sb.append(a);
        sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
        }
        sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
        if (context != null) {
            sb.append(context.getPackageName());
        }
        sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
        if (context != null) {
            try {
                sb.append(Ag2.a(context).e(context.getPackageName(), 0).versionCode);
            } catch (PackageManager.NameNotFoundException e) {
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public void a(Context context) {
        H62.a(context);
    }

    @DexIgnore
    public int b(Context context) {
        return H62.b(context);
    }

    @DexIgnore
    @Deprecated
    public Intent c(int i) {
        return d(null, i, null);
    }

    @DexIgnore
    public Intent d(Context context, int i, String str) {
        if (i == 1 || i == 2) {
            return (context == null || !If2.d(context)) ? Ie2.a("com.google.android.gms", n(context, str)) : Ie2.c();
        }
        if (i != 3) {
            return null;
        }
        return Ie2.b("com.google.android.gms");
    }

    @DexIgnore
    public PendingIntent e(Context context, int i, int i2) {
        return f(context, i, i2, null);
    }

    @DexIgnore
    public PendingIntent f(Context context, int i, int i2, String str) {
        Intent d = d(context, i, str);
        if (d == null) {
            return null;
        }
        return PendingIntent.getActivity(context, i2, d, 134217728);
    }

    @DexIgnore
    public String g(int i) {
        return H62.c(i);
    }

    @DexIgnore
    public int i(Context context) {
        return j(context, a);
    }

    @DexIgnore
    public int j(Context context, int i) {
        int h = H62.h(context, i);
        if (H62.j(context, h)) {
            return 18;
        }
        return h;
    }

    @DexIgnore
    public boolean k(Context context, int i) {
        return H62.j(context, i);
    }

    @DexIgnore
    public boolean l(Context context, String str) {
        return H62.l(context, str);
    }

    @DexIgnore
    public boolean m(int i) {
        return H62.m(i);
    }
}
