package com.fossil;

import com.mapped.Wg6;
import java.lang.reflect.Method;

public class Lo7 {

    public static final class Ai {
        public static final Method a;

        /* JADX WARNING: Removed duplicated region for block: B:12:0x0043 A[LOOP:0: B:1:0x000e->B:12:0x0043, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x003d A[SYNTHETIC] */
        /*
        static {
            /*
                r2 = 0
                java.lang.Class<java.lang.Throwable> r0 = java.lang.Throwable.class
                java.lang.reflect.Method[] r4 = r0.getMethods()
                java.lang.String r0 = "throwableClass.methods"
                com.mapped.Wg6.b(r4, r0)
                int r5 = r4.length
                r3 = r2
            L_0x000e:
                if (r3 >= r5) goto L_0x0047
                r1 = r4[r3]
                java.lang.String r0 = "it"
                com.mapped.Wg6.b(r1, r0)
                java.lang.String r0 = r1.getName()
                java.lang.String r6 = "addSuppressed"
                boolean r0 = com.mapped.Wg6.a(r0, r6)
                if (r0 == 0) goto L_0x0041
                java.lang.Class[] r0 = r1.getParameterTypes()
                java.lang.String r6 = "it.parameterTypes"
                com.mapped.Wg6.b(r0, r6)
                java.lang.Object r0 = com.fossil.Em7.X(r0)
                java.lang.Class r0 = (java.lang.Class) r0
                java.lang.Class<java.lang.Throwable> r6 = java.lang.Throwable.class
                boolean r0 = com.mapped.Wg6.a(r0, r6)
                if (r0 == 0) goto L_0x0041
                r0 = 1
            L_0x003b:
                if (r0 == 0) goto L_0x0043
                r0 = r1
            L_0x003e:
                com.fossil.Lo7.Ai.a = r0
                return
            L_0x0041:
                r0 = r2
                goto L_0x003b
            L_0x0043:
                int r0 = r3 + 1
                r3 = r0
                goto L_0x000e
            L_0x0047:
                r0 = 0
                goto L_0x003e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Lo7.Ai.<clinit>():void");
        }
        */
    }

    public void a(Throwable th, Throwable th2) {
        Wg6.c(th, "cause");
        Wg6.c(th2, "exception");
        Method method = Ai.a;
        if (method != null) {
            method.invoke(th, th2);
        }
    }

    public Rr7 b() {
        return new Qr7();
    }
}
