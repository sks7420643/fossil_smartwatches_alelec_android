package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.http.AndroidHttpClient;
import android.os.Build;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class La1 {
    @DexIgnore
    public static N91 a(Context context) {
        return c(context, null);
    }

    @DexIgnore
    public static N91 b(Context context, G91 g91) {
        N91 n91 = new N91(new Z91(new File(context.getCacheDir(), "volley")), g91);
        n91.d();
        return n91;
    }

    @DexIgnore
    public static N91 c(Context context, W91 w91) {
        X91 x91;
        String str;
        if (w91 != null) {
            x91 = new X91(w91);
        } else if (Build.VERSION.SDK_INT >= 9) {
            x91 = new X91((W91) new Ea1());
        } else {
            try {
                String packageName = context.getPackageName();
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageName, 0);
                str = packageName + "/" + packageInfo.versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                str = "volley/0";
            }
            x91 = new X91(new Aa1(AndroidHttpClient.newInstance(str)));
        }
        return b(context, x91);
    }
}
