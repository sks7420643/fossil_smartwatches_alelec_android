package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import com.fossil.Zs2;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ct2 extends Zs2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ Context h;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle i;
    @DexIgnore
    public /* final */ /* synthetic */ Zs2 j;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Ct2(Zs2 zs2, String str, String str2, Context context, Bundle bundle) {
        super(zs2);
        this.j = zs2;
        this.f = str;
        this.g = str2;
        this.h = context;
        this.i = bundle;
    }

    @DexIgnore
    @Override // com.fossil.Zs2.Ai
    public final void a() {
        String str;
        String str2;
        String str3;
        int i2;
        boolean z;
        try {
            this.j.e = new ArrayList();
            Zs2 zs2 = this.j;
            if (Zs2.K(this.f, this.g)) {
                str3 = this.g;
                str = this.f;
                str2 = this.j.a;
            } else {
                str = null;
                str2 = null;
                str3 = null;
            }
            Zs2.V(this.h);
            boolean z2 = Zs2.j.booleanValue() || str != null;
            this.j.h = this.j.c(this.h, z2);
            if (this.j.h == null) {
                Log.w(this.j.a, "Failed to connect to measurement client.");
                return;
            }
            int i3 = Zs2.T(this.h);
            int i4 = Zs2.R(this.h);
            if (z2) {
                i2 = Math.max(i3, i4);
                z = i4 < i3;
            } else {
                if (i3 > 0) {
                    i4 = i3;
                }
                if (i3 > 0) {
                    i2 = i4;
                    z = true;
                } else {
                    i2 = i4;
                    z = false;
                }
            }
            this.j.h.initialize(Tg2.n(this.h), new Xs2(31000, (long) i2, z, str2, str, str3, this.i), this.b);
        } catch (Exception e) {
            this.j.o(e, true, false);
        }
    }
}
