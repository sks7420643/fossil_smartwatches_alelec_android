package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Da0 {
    c((byte) 0),
    d((byte) 1);
    
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public Da0(byte b2) {
        this.b = (byte) b2;
    }
}
