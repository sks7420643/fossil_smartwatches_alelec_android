package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class S95 extends R95 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d B; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray C;
    @DexIgnore
    public long A;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        C = sparseIntArray;
        sparseIntArray.put(2131362172, 1);
        C.put(2131362666, 2);
        C.put(2131362113, 3);
        C.put(2131362535, 4);
        C.put(2131363075, 5);
        C.put(2131362412, 6);
        C.put(2131362107, 7);
        C.put(2131363217, 8);
        C.put(2131361889, 9);
    }
    */

    @DexIgnore
    public S95(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 10, B, C));
    }

    @DexIgnore
    public S95(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleSwitchCompat) objArr[9], (ConstraintLayout) objArr[7], (ConstraintLayout) objArr[3], (ConstraintLayout) objArr[1], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[4], (RTLImageView) objArr[2], (ConstraintLayout) objArr[0], (FlexibleSwitchCompat) objArr[5], (FlexibleTextView) objArr[8]);
        this.A = -1;
        this.x.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.A = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.A != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.A = 1;
        }
        w();
    }
}
