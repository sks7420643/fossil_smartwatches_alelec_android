package com.fossil;

import com.fossil.Dl7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lk6;
import com.mapped.Wg6;
import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U81 implements B18, Hg6<Throwable, Cd6> {
    @DexIgnore
    public /* final */ A18 b;
    @DexIgnore
    public /* final */ Lk6<Response> c;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.mapped.Lk6<? super okhttp3.Response> */
    /* JADX WARN: Multi-variable type inference failed */
    public U81(A18 a18, Lk6<? super Response> lk6) {
        Wg6.c(a18, "call");
        Wg6.c(lk6, "continuation");
        this.b = a18;
        this.c = lk6;
    }

    @DexIgnore
    public void a(Throwable th) {
        try {
            this.b.cancel();
        } catch (Throwable th2) {
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
        a(th);
        return Cd6.a;
    }

    @DexIgnore
    @Override // com.fossil.B18
    public void onFailure(A18 a18, IOException iOException) {
        Wg6.c(a18, "call");
        Wg6.c(iOException, "e");
        if (!a18.f()) {
            Lk6<Response> lk6 = this.c;
            Dl7.Ai ai = Dl7.Companion;
            lk6.resumeWith(Dl7.constructor-impl(El7.a(iOException)));
        }
    }

    @DexIgnore
    @Override // com.fossil.B18
    public void onResponse(A18 a18, Response response) {
        Wg6.c(a18, "call");
        Wg6.c(response, "response");
        Lk6<Response> lk6 = this.c;
        Dl7.Ai ai = Dl7.Companion;
        lk6.resumeWith(Dl7.constructor-impl(response));
    }
}
