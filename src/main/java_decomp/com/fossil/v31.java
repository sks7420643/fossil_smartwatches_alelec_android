package com.fossil;

import androidx.work.impl.WorkDatabase;
import com.fossil.A11;
import java.util.LinkedList;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class V31 implements Runnable {
    @DexIgnore
    public /* final */ L11 b; // = new L11();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends V31 {
        @DexIgnore
        public /* final */ /* synthetic */ S11 c;
        @DexIgnore
        public /* final */ /* synthetic */ UUID d;

        @DexIgnore
        public Ai(S11 s11, UUID uuid) {
            this.c = s11;
            this.d = uuid;
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.V31
        public void g() {
            WorkDatabase p = this.c.p();
            p.beginTransaction();
            try {
                a(this.c, this.d.toString());
                p.setTransactionSuccessful();
                p.endTransaction();
                f(this.c);
            } catch (Throwable th) {
                p.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends V31 {
        @DexIgnore
        public /* final */ /* synthetic */ S11 c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ boolean e;

        @DexIgnore
        public Bi(S11 s11, String str, boolean z) {
            this.c = s11;
            this.d = str;
            this.e = z;
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.V31
        public void g() {
            WorkDatabase p = this.c.p();
            p.beginTransaction();
            try {
                for (String str : p.j().m(this.d)) {
                    a(this.c, str);
                }
                p.setTransactionSuccessful();
                p.endTransaction();
                if (this.e) {
                    f(this.c);
                }
            } catch (Throwable th) {
                p.endTransaction();
                throw th;
            }
        }
    }

    @DexIgnore
    public static V31 b(UUID uuid, S11 s11) {
        return new Ai(s11, uuid);
    }

    @DexIgnore
    public static V31 c(String str, S11 s11, boolean z) {
        return new Bi(s11, str, z);
    }

    @DexIgnore
    public void a(S11 s11, String str) {
        e(s11.p(), str);
        s11.n().k(str);
        for (N11 n11 : s11.o()) {
            n11.e(str);
        }
    }

    @DexIgnore
    public A11 d() {
        return this.b;
    }

    @DexIgnore
    public final void e(WorkDatabase workDatabase, String str) {
        P31 j = workDatabase.j();
        A31 b2 = workDatabase.b();
        LinkedList linkedList = new LinkedList();
        linkedList.add(str);
        while (!linkedList.isEmpty()) {
            String str2 = (String) linkedList.remove();
            F11 n = j.n(str2);
            if (!(n == F11.SUCCEEDED || n == F11.FAILED)) {
                j.a(F11.CANCELLED, str2);
            }
            linkedList.addAll(b2.b(str2));
        }
    }

    @DexIgnore
    public void f(S11 s11) {
        O11.b(s11.j(), s11.p(), s11.o());
    }

    @DexIgnore
    public abstract void g();

    @DexIgnore
    public void run() {
        try {
            g();
            this.b.a(A11.a);
        } catch (Throwable th) {
            this.b.a(new A11.Bi.Aii(th));
        }
    }
}
