package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.WorkoutState;
import com.fossil.fitness.WorkoutType;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lp1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ WorkoutType c;
    @DexIgnore
    public /* final */ WorkoutState d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ long h;
    @DexIgnore
    public /* final */ long i;
    @DexIgnore
    public /* final */ short j;
    @DexIgnore
    public /* final */ short k;
    @DexIgnore
    public /* final */ short l;
    @DexIgnore
    public /* final */ boolean m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<lp1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public lp1 createFromParcel(Parcel parcel) {
            long readLong = parcel.readLong();
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                WorkoutType valueOf = WorkoutType.valueOf(readString);
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    pq7.b(readString2, "parcel.readString()!!");
                    return new lp1(readLong, valueOf, WorkoutState.valueOf(readString2), parcel.readLong(), parcel.readLong(), parcel.readLong(), parcel.readLong(), parcel.readLong(), (short) parcel.readInt(), (short) parcel.readInt(), (short) parcel.readInt(), parcel.readInt() == 1);
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public lp1[] newArray(int i) {
            return new lp1[i];
        }
    }

    @DexIgnore
    public lp1(long j2, WorkoutType workoutType, WorkoutState workoutState, long j3, long j4, long j5, long j6, long j7, short s, short s2, short s3, boolean z) {
        this.b = j2;
        this.c = workoutType;
        this.d = workoutState;
        this.e = j3;
        this.f = j4;
        this.g = j5;
        this.h = j6;
        this.i = j7;
        this.j = (short) s;
        this.k = (short) s2;
        this.l = (short) s3;
        this.m = z;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(lp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            lp1 lp1 = (lp1) obj;
            if (this.b != lp1.b) {
                return false;
            }
            if (this.c != lp1.c) {
                return false;
            }
            if (this.d != lp1.d) {
                return false;
            }
            if (this.e != lp1.e) {
                return false;
            }
            if (this.f != lp1.f) {
                return false;
            }
            if (this.g != lp1.g) {
                return false;
            }
            if (this.h != lp1.h) {
                return false;
            }
            if (this.i != lp1.i) {
                return false;
            }
            if (this.j != lp1.j) {
                return false;
            }
            if (this.k != lp1.k) {
                return false;
            }
            if (this.l != lp1.l) {
                return false;
            }
            return this.m == lp1.m;
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.device.data.workoutsession.WorkoutSession");
    }

    @DexIgnore
    public final long getActiveCaloriesInCalorie() {
        return this.h;
    }

    @DexIgnore
    public final short getAverageHeartRate() {
        return this.k;
    }

    @DexIgnore
    public final short getCurrentHeartRate() {
        return this.j;
    }

    @DexIgnore
    public final long getDistanceInMeter() {
        return this.g;
    }

    @DexIgnore
    public final long getDurationInSecond() {
        return this.e;
    }

    @DexIgnore
    public final short getMaximumHeartRate() {
        return this.l;
    }

    @DexIgnore
    public final long getNumberOfStep() {
        return this.f;
    }

    @DexIgnore
    public final long getSessionId() {
        return this.b;
    }

    @DexIgnore
    public final long getTotalCaloriesInCalorie() {
        return this.i;
    }

    @DexIgnore
    public final WorkoutState getWorkoutState() {
        return this.d;
    }

    @DexIgnore
    public final WorkoutType getWorkoutType() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Long.valueOf(this.b).hashCode();
        int hashCode2 = this.c.hashCode();
        int hashCode3 = this.d.hashCode();
        int hashCode4 = Long.valueOf(this.e).hashCode();
        int hashCode5 = Long.valueOf(this.f).hashCode();
        int hashCode6 = Long.valueOf(this.g).hashCode();
        int hashCode7 = Long.valueOf(this.h).hashCode();
        int hashCode8 = Long.valueOf(this.i).hashCode();
        short s = this.j;
        short s2 = this.k;
        return (((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + s) * 31) + s2) * 31) + this.l) * 31) + Boolean.valueOf(this.m).hashCode();
    }

    @DexIgnore
    public final boolean isRequiredGPS() {
        return this.m;
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put(Constants.SESSION_ID, this.b).put("type", ey1.a(this.c)).put("state", ey1.a(this.d)).put("duration_in_second", this.e).put("number_of_step", this.f).put("distance_in_meter", this.g).put("active_calorie", this.h).put("total_calorie", this.i).put("current_heart_rate", Short.valueOf(this.j)).put("average_heart_rate", Short.valueOf(this.k)).put("maximum_heart_rate", Short.valueOf(this.l)).put("gps", this.m);
        pq7.b(put, "JSONObject()\n           \u2026put(\"gps\", isRequiredGPS)");
        return put;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeLong(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeLong(this.e);
        }
        if (parcel != null) {
            parcel.writeLong(this.f);
        }
        if (parcel != null) {
            parcel.writeLong(this.g);
        }
        if (parcel != null) {
            parcel.writeLong(this.h);
        }
        if (parcel != null) {
            parcel.writeLong(this.i);
        }
        if (parcel != null) {
            parcel.writeInt(hy1.n(this.j));
        }
        if (parcel != null) {
            parcel.writeInt(hy1.n(this.k));
        }
        if (parcel != null) {
            parcel.writeInt(hy1.n(this.l));
        }
        if (parcel != null) {
            parcel.writeInt(this.m ? 1 : 0);
        }
    }
}
