package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Ob {
    d((byte) 0),
    e((byte) 1),
    f((byte) 2),
    g((byte) 3),
    h((byte) 4),
    i((byte) 5),
    j((byte) 6),
    k((byte) 7),
    l((byte) 8),
    m((byte) 9),
    n((byte) 10),
    o((byte) 11),
    p((byte) 12),
    q((byte) 13),
    r(DateTimeFieldType.HOUR_OF_HALFDAY),
    s(DateTimeFieldType.CLOCKHOUR_OF_HALFDAY),
    t(DateTimeFieldType.CLOCKHOUR_OF_DAY),
    u(DateTimeFieldType.HOUR_OF_DAY),
    v(DateTimeFieldType.MINUTE_OF_HOUR),
    w(DateTimeFieldType.SECOND_OF_DAY),
    x(DateTimeFieldType.SECOND_OF_MINUTE),
    y((byte) 255);
    
    @DexIgnore
    public static /* final */ Mb A; // = new Mb(null);
    @DexIgnore
    public /* final */ short b;
    @DexIgnore
    public /* final */ byte c;

    @DexIgnore
    public Ob(byte b2) {
        this.c = b2;
        ByteBuffer wrap = ByteBuffer.wrap(new byte[]{b2, (byte) 255});
        Wg6.b(wrap, "ByteBuffer.wrap(byteArrayOf(id, (0xFF).toByte()))");
        this.b = wrap.getShort();
    }

    @DexIgnore
    public final short a(byte b2) {
        ByteBuffer wrap = ByteBuffer.wrap(new byte[]{this.c, b2});
        Wg6.b(wrap, "ByteBuffer.wrap(byteArrayOf(id, index))");
        return wrap.getShort();
    }
}
