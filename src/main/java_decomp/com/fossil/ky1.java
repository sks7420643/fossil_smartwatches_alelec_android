package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Ky1 {
    VERBOSE(2, 'V'),
    DEBUG(3, 'D'),
    INFO(4, 'I'),
    WARN(5, 'W'),
    ERROR(6, 'E'),
    ASSERT(7, 'A');
    
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public /* final */ int priority;
    @DexIgnore
    public /* final */ char symbol;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Ky1 a(int i) {
            Ky1[] values = Ky1.values();
            for (Ky1 ky1 : values) {
                if (ky1.getPriority() == i) {
                    return ky1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public Ky1(int i, char c) {
        this.priority = i;
        this.symbol = (char) c;
    }

    @DexIgnore
    public final int getPriority() {
        return this.priority;
    }

    @DexIgnore
    public final char getSymbol() {
        return this.symbol;
    }
}
