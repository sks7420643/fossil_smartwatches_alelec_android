package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vj6 implements Factory<SleepOverviewDayPresenter> {
    @DexIgnore
    public static SleepOverviewDayPresenter a(Sj6 sj6, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, PortfolioApp portfolioApp) {
        return new SleepOverviewDayPresenter(sj6, sleepSummariesRepository, sleepSessionsRepository, portfolioApp);
    }
}
