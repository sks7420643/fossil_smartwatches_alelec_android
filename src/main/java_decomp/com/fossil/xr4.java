package com.fossil;

import android.content.Context;
import android.content.Intent;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xr4 {
    @DexIgnore
    public static /* final */ Xr4 a; // = new Xr4();

    @DexIgnore
    public final void a(String str, String str2, String str3, Context context) {
        Wg6.c(str, "type");
        Wg6.c(str2, "challengeId");
        Wg6.c(str3, "profileId");
        Wg6.c(context, "context");
        Intent intent = new Intent(str);
        intent.putExtra("challenge_id_extra", str2);
        intent.putExtra("current_user_id_extra", str3);
        Ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void b(String str, String str2, String str3, Context context) {
        Wg6.c(str, "type");
        Wg6.c(str2, "currentId");
        Wg6.c(str3, "friendId");
        Wg6.c(context, "context");
        Intent intent = new Intent(str);
        intent.putExtra("current_user_id_extra", str2);
        intent.putExtra("friend_id_extra", str3);
        Ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void c(String str, String str2, int i, int i2, boolean z, Context context) {
        Wg6.c(str, "challengeId");
        Wg6.c(str2, "currentId");
        Wg6.c(context, "context");
        Intent intent = new Intent("bc_complete_challenge");
        intent.putExtra("challenge_id_extra", str);
        intent.putExtra("current_user_id_extra", str2);
        intent.putExtra("current_steps_extra", i2);
        intent.putExtra("rank_extra", i);
        intent.putExtra("reach_goal_or_top", z);
        Ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void d(Ps4 ps4, long j, String str, Context context) {
        Wg6.c(ps4, "challenge");
        Wg6.c(str, "startType");
        Wg6.c(context, "context");
        Intent intent = new Intent("bc_create_challenge");
        intent.putExtra("challenge_extra", ps4);
        intent.putExtra("start_tracking_time_extra", j);
        intent.putExtra("start_type_extra", str);
        Ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void e(String str, Context context) {
        Wg6.c(str, "socialProfileId");
        Wg6.c(context, "context");
        Intent intent = new Intent("bc_create_social_profile");
        intent.putExtra("current_user_id_extra", str);
        Ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void f(Context context) {
        Wg6.c(context, "context");
        Intent intent = new Intent("bc_look_for_friend");
        intent.putExtra("current_user_id_extra", PortfolioApp.get.instance().l0());
        Ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void g(Context context) {
        Wg6.c(context, "context");
        Intent intent = new Intent("bc_look_for_friend_challenge");
        intent.putExtra("current_user_id_extra", PortfolioApp.get.instance().l0());
        Ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void h(String str, String str2, String str3, int i, Context context) {
        Wg6.c(str, "type");
        Wg6.c(str2, "challengeId");
        Wg6.c(str3, "profileId");
        Wg6.c(context, "context");
        Intent intent = new Intent(str);
        intent.putExtra("challenge_id_extra", str2);
        intent.putExtra("current_user_id_extra", str3);
        intent.putExtra("current_steps_extra", i);
        Ct0.b(context).d(intent);
    }

    @DexIgnore
    public final void i(String str, String str2, Context context) {
        Wg6.c(str, "currentId");
        Wg6.c(str2, "friendId");
        Wg6.c(context, "context");
        Intent intent = new Intent("bc_send_friend_request");
        intent.putExtra("current_user_id_extra", str);
        intent.putExtra("friend_id_extra", str2);
        Ct0.b(context).d(intent);
    }
}
