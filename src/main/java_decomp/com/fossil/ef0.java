package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import com.facebook.places.model.PlaceFields;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ef0 {
    @DexIgnore
    public static Ef0 d;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ LocationManager b;
    @DexIgnore
    public /* final */ Ai c; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public long b;
        @DexIgnore
        public long c;
        @DexIgnore
        public long d;
        @DexIgnore
        public long e;
        @DexIgnore
        public long f;
    }

    @DexIgnore
    public Ef0(Context context, LocationManager locationManager) {
        this.a = context;
        this.b = locationManager;
    }

    @DexIgnore
    public static Ef0 a(Context context) {
        if (d == null) {
            Context applicationContext = context.getApplicationContext();
            d = new Ef0(applicationContext, (LocationManager) applicationContext.getSystemService(PlaceFields.LOCATION));
        }
        return d;
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public final Location b() {
        Location location = null;
        Location c2 = Hl0.b(this.a, "android.permission.ACCESS_COARSE_LOCATION") == 0 ? c("network") : null;
        if (Hl0.b(this.a, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            location = c("gps");
        }
        if (location != null && c2 != null) {
            return location.getTime() > c2.getTime() ? location : c2;
        }
        if (location == null) {
            location = c2;
        }
        return location;
    }

    @DexIgnore
    public final Location c(String str) {
        try {
            if (this.b.isProviderEnabled(str)) {
                return this.b.getLastKnownLocation(str);
            }
        } catch (Exception e) {
            Log.d("TwilightManager", "Failed to get last known location", e);
        }
        return null;
    }

    @DexIgnore
    public boolean d() {
        Ai ai = this.c;
        if (e()) {
            return ai.a;
        }
        Location b2 = b();
        if (b2 != null) {
            f(b2);
            return ai.a;
        }
        Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        int i = Calendar.getInstance().get(11);
        return i < 6 || i >= 22;
    }

    @DexIgnore
    public final boolean e() {
        return this.c.f > System.currentTimeMillis();
    }

    @DexIgnore
    public final void f(Location location) {
        long j;
        Ai ai = this.c;
        long currentTimeMillis = System.currentTimeMillis();
        Df0 b2 = Df0.b();
        b2.a(currentTimeMillis - LogBuilder.MAX_INTERVAL, location.getLatitude(), location.getLongitude());
        long j2 = b2.a;
        b2.a(currentTimeMillis, location.getLatitude(), location.getLongitude());
        boolean z = b2.c == 1;
        long j3 = b2.b;
        long j4 = b2.a;
        b2.a(LogBuilder.MAX_INTERVAL + currentTimeMillis, location.getLatitude(), location.getLongitude());
        long j5 = b2.b;
        if (j3 == -1 || j4 == -1) {
            j = 43200000 + currentTimeMillis;
        } else {
            j = (currentTimeMillis > j4 ? 0 + j5 : currentTimeMillis > j3 ? 0 + j4 : 0 + j3) + 60000;
        }
        ai.a = z;
        ai.b = j2;
        ai.c = j3;
        ai.d = j4;
        ai.e = j5;
        ai.f = j;
    }
}
