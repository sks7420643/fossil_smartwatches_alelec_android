package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.O80;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zo1 extends Hv1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public static /* final */ Vw1 c; // = Vw1.TOP_SHORT_PRESS_RELEASE;
    @DexIgnore
    public static /* final */ Vw1 d; // = Vw1.MIDDLE_SHORT_PRESS_RELEASE;
    @DexIgnore
    public static /* final */ Vw1 e; // = Vw1.BOTTOM_SHORT_PRESS_RELEASE;
    @DexIgnore
    public /* final */ HashSet<O80> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Zo1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Zo1 a(Parcel parcel) {
            return new Zo1(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Zo1 createFromParcel(Parcel parcel) {
            return new Zo1(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Zo1[] newArray(int i) {
            return new Zo1[i];
        }
    }

    @DexIgnore
    public Zo1(Parcel parcel) {
        super(parcel);
        HashSet<O80> hashSet = new HashSet<>();
        this.b = hashSet;
        Parcelable[] readParcelableArray = parcel.readParcelableArray(O80.class.getClassLoader());
        if (readParcelableArray != null) {
            Mm7.t(hashSet, (O80[]) readParcelableArray);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.device.data.watchapp.WatchApp>");
    }

    @DexIgnore
    public Zo1(O80 o80, O80 o802, O80 o803) {
        this.b = new HashSet<>();
        o80.a(c);
        o802.a(d);
        o803.a(e);
        this.b.add(o80);
        this.b.add(o802);
        this.b.add(o803);
    }

    @DexIgnore
    public Zo1(O80[] o80Arr) {
        HashSet<O80> hashSet = new HashSet<>();
        this.b = hashSet;
        Mm7.t(hashSet, o80Arr);
    }

    @DexIgnore
    @Override // com.fossil.Hv1
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        for (T t : this.b) {
            jSONArray.put(t.toJSONObject());
            Gy1.b(jSONObject, t.a(), false, 2, null);
        }
        jSONObject.put("master._.config.buttons", jSONArray);
        return jSONObject;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Zo1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(Wg6.a(this.b, ((Zo1) obj).b) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.watchapp.WatchAppConfig");
    }

    @DexIgnore
    public final O80 getBottomButton() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (next.getButtonEvent() == e) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        if (t != null) {
            return t;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final O80 getMiddleButton() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (next.getButtonEvent() == d) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        if (t != null) {
            return t;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final O80 getTopButton() {
        T t;
        boolean z;
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (next.getButtonEvent() == c) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        if (t != null) {
            return t;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return a();
    }

    @DexIgnore
    @Override // com.fossil.Hv1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            Object[] array = this.b.toArray(new O80[0]);
            if (array != null) {
                parcel.writeParcelableArray((Parcelable[]) array, i);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }
}
