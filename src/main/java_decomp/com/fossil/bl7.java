package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bl7 extends Error {
    @DexIgnore
    public Bl7() {
        this(null, 1, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Bl7(String str) {
        super(str);
        Wg6.c(str, "message");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Bl7(String str, int i, Qg6 qg6) {
        this((i & 1) != 0 ? "An operation is not implemented." : str);
    }
}
