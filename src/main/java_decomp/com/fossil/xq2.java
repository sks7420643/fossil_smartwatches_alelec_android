package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Xq2 extends Gr2 implements Wq2 {
    @DexIgnore
    public Xq2() {
        super("com.google.android.gms.location.internal.ISettingsCallbacks");
    }

    @DexIgnore
    @Override // com.fossil.Gr2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        E0((Ka3) Qr2.a(parcel, Ka3.CREATOR));
        return true;
    }
}
