package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.t47;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px5 extends qv5 implements ox5, t47.g {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public g37<n25> h;
    @DexIgnore
    public nx5 i;
    @DexIgnore
    public /* final */ s j; // = new s(this);
    @DexIgnore
    public /* final */ p k; // = new p(this);
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return px5.m;
        }

        @DexIgnore
        public final px5 b() {
            return new px5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ px5 b;
        @DexIgnore
        public /* final */ /* synthetic */ n25 c;

        @DexIgnore
        public b(px5 px5, n25 n25) {
            this.b = px5;
            this.c = n25;
        }

        @DexIgnore
        public final void onClick(View view) {
            pq7.b(view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            px5 px5 = this.b;
            FlexibleTextView flexibleTextView = this.c.r;
            pq7.b(flexibleTextView, "binding.dayFriday");
            px5.O6(flexibleTextView);
            this.b.R6(6, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ px5 b;
        @DexIgnore
        public /* final */ /* synthetic */ n25 c;

        @DexIgnore
        public c(px5 px5, n25 n25) {
            this.b = px5;
            this.c = n25;
        }

        @DexIgnore
        public final void onClick(View view) {
            pq7.b(view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            px5 px5 = this.b;
            FlexibleTextView flexibleTextView = this.c.t;
            pq7.b(flexibleTextView, "binding.daySaturday");
            px5.O6(flexibleTextView);
            this.b.R6(7, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ px5 f2883a;
        @DexIgnore
        public /* final */ /* synthetic */ n25 b;

        @DexIgnore
        public d(px5 px5, n25 n25) {
            this.f2883a = px5;
            this.b = n25;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            nx5 L6 = px5.L6(this.f2883a);
            NumberPicker numberPicker2 = this.b.J;
            pq7.b(numberPicker2, "binding.numberPickerTwo");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.b.I;
            pq7.b(numberPicker3, "binding.numberPickerThree");
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            L6.t(String.valueOf(i2), String.valueOf(value), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ px5 f2884a;
        @DexIgnore
        public /* final */ /* synthetic */ n25 b;

        @DexIgnore
        public e(px5 px5, n25 n25) {
            this.f2884a = px5;
            this.b = n25;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            nx5 L6 = px5.L6(this.f2884a);
            NumberPicker numberPicker2 = this.b.H;
            pq7.b(numberPicker2, "binding.numberPickerOne");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.b.I;
            pq7.b(numberPicker3, "binding.numberPickerThree");
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            L6.t(String.valueOf(value), String.valueOf(i2), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements NumberPicker.g {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ px5 f2885a;
        @DexIgnore
        public /* final */ /* synthetic */ n25 b;

        @DexIgnore
        public f(px5 px5, n25 n25) {
            this.f2885a = px5;
            this.b = n25;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            boolean z = true;
            nx5 L6 = px5.L6(this.f2885a);
            NumberPicker numberPicker2 = this.b.H;
            pq7.b(numberPicker2, "binding.numberPickerOne");
            int value = numberPicker2.getValue();
            NumberPicker numberPicker3 = this.b.J;
            pq7.b(numberPicker3, "binding.numberPickerTwo");
            int value2 = numberPicker3.getValue();
            if (i2 != 1) {
                z = false;
            }
            L6.t(String.valueOf(value), String.valueOf(value2), z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ px5 b;

        @DexIgnore
        public g(px5 px5) {
            this.b = px5;
        }

        @DexIgnore
        public final void onClick(View view) {
            px5.L6(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ px5 b;

        @DexIgnore
        public h(px5 px5) {
            this.b = px5;
        }

        @DexIgnore
        public final void onClick(View view) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.s(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements CompoundButton.OnCheckedChangeListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ px5 f2886a;
        @DexIgnore
        public /* final */ /* synthetic */ n25 b;

        @DexIgnore
        public i(px5 px5, n25 n25) {
            this.f2886a = px5;
            this.b = n25;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            px5.L6(this.f2886a).s(z);
            ConstraintLayout constraintLayout = this.b.q;
            pq7.b(constraintLayout, "binding.clDaysRepeat");
            constraintLayout.setVisibility(z ? 0 : 8);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ px5 b;

        @DexIgnore
        public j(px5 px5) {
            this.b = px5;
        }

        @DexIgnore
        public final void onClick(View view) {
            px5.L6(this.b).p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ px5 b;
        @DexIgnore
        public /* final */ /* synthetic */ n25 c;

        @DexIgnore
        public k(px5 px5, n25 n25) {
            this.b = px5;
            this.c = n25;
        }

        @DexIgnore
        public final void onClick(View view) {
            pq7.b(view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            px5 px5 = this.b;
            FlexibleTextView flexibleTextView = this.c.u;
            pq7.b(flexibleTextView, "binding.daySunday");
            px5.O6(flexibleTextView);
            this.b.R6(1, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ px5 b;
        @DexIgnore
        public /* final */ /* synthetic */ n25 c;

        @DexIgnore
        public l(px5 px5, n25 n25) {
            this.b = px5;
            this.c = n25;
        }

        @DexIgnore
        public final void onClick(View view) {
            pq7.b(view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            px5 px5 = this.b;
            FlexibleTextView flexibleTextView = this.c.s;
            pq7.b(flexibleTextView, "binding.dayMonday");
            px5.O6(flexibleTextView);
            this.b.R6(2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ px5 b;
        @DexIgnore
        public /* final */ /* synthetic */ n25 c;

        @DexIgnore
        public m(px5 px5, n25 n25) {
            this.b = px5;
            this.c = n25;
        }

        @DexIgnore
        public final void onClick(View view) {
            pq7.b(view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            px5 px5 = this.b;
            FlexibleTextView flexibleTextView = this.c.w;
            pq7.b(flexibleTextView, "binding.dayTuesday");
            px5.O6(flexibleTextView);
            this.b.R6(3, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ px5 b;
        @DexIgnore
        public /* final */ /* synthetic */ n25 c;

        @DexIgnore
        public n(px5 px5, n25 n25) {
            this.b = px5;
            this.c = n25;
        }

        @DexIgnore
        public final void onClick(View view) {
            pq7.b(view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            px5 px5 = this.b;
            FlexibleTextView flexibleTextView = this.c.x;
            pq7.b(flexibleTextView, "binding.dayWednesday");
            px5.O6(flexibleTextView);
            this.b.R6(4, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ px5 b;
        @DexIgnore
        public /* final */ /* synthetic */ n25 c;

        @DexIgnore
        public o(px5 px5, n25 n25) {
            this.b = px5;
            this.c = n25;
        }

        @DexIgnore
        public final void onClick(View view) {
            pq7.b(view, "view");
            boolean z = !view.isSelected();
            view.setSelected(z);
            px5 px5 = this.b;
            FlexibleTextView flexibleTextView = this.c.v;
            pq7.b(flexibleTextView, "binding.dayThursday");
            px5.O6(flexibleTextView);
            this.b.R6(5, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ px5 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public p(px5 px5) {
            this.b = px5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FlexibleTextView flexibleTextView;
            FlexibleEditText flexibleEditText;
            n25 n25 = (n25) px5.K6(this.b).a();
            if (!(n25 == null || (flexibleEditText = n25.z) == null)) {
                flexibleEditText.requestLayout();
            }
            px5.L6(this.b).r(String.valueOf(editable));
            if (editable != null) {
                String str = editable.length() + " / 50";
                n25 n252 = (n25) px5.K6(this.b).a();
                if (n252 != null && (flexibleTextView = n252.B) != null) {
                    flexibleTextView.setText(str);
                }
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleEditText b;
        @DexIgnore
        public /* final */ /* synthetic */ px5 c;

        @DexIgnore
        public q(FlexibleEditText flexibleEditText, px5 px5) {
            this.b = flexibleEditText;
            this.c = px5;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            FlexibleTextView flexibleTextView;
            View n;
            int length;
            n25 n25 = (n25) px5.K6(this.c).a();
            if (n25 != null && (flexibleTextView = n25.D) != null) {
                if (z) {
                    FlexibleEditText flexibleEditText = this.b;
                    pq7.b(flexibleEditText, "it");
                    Editable text = flexibleEditText.getText();
                    if (text == null || text.length() == 0) {
                        length = 0;
                    } else {
                        FlexibleEditText flexibleEditText2 = this.b;
                        pq7.b(flexibleEditText2, "it");
                        Editable text2 = flexibleEditText2.getText();
                        if (text2 != null) {
                            length = text2.length();
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                    pq7.b(flexibleTextView, "counterTextView");
                    flexibleTextView.setText(length + " / 15");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                n25 n252 = (n25) px5.K6(this.c).a();
                if (!(n252 == null || (n = n252.n()) == null)) {
                    n.requestFocus();
                }
                pq7.b(flexibleTextView, "counterTextView");
                flexibleTextView.setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleEditText b;
        @DexIgnore
        public /* final */ /* synthetic */ px5 c;

        @DexIgnore
        public r(FlexibleEditText flexibleEditText, px5 px5) {
            this.b = flexibleEditText;
            this.c = px5;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            FlexibleTextView flexibleTextView;
            View n;
            int length;
            n25 n25 = (n25) px5.K6(this.c).a();
            if (n25 != null && (flexibleTextView = n25.B) != null) {
                if (z) {
                    FlexibleEditText flexibleEditText = this.b;
                    pq7.b(flexibleEditText, "it");
                    Editable text = flexibleEditText.getText();
                    if (text == null || text.length() == 0) {
                        length = 0;
                    } else {
                        FlexibleEditText flexibleEditText2 = this.b;
                        pq7.b(flexibleEditText2, "it");
                        Editable text2 = flexibleEditText2.getText();
                        if (text2 != null) {
                            length = text2.length();
                        } else {
                            pq7.i();
                            throw null;
                        }
                    }
                    pq7.b(flexibleTextView, "counterTextView");
                    flexibleTextView.setText(length + " / 50");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                n25 n252 = (n25) px5.K6(this.c).a();
                if (!(n252 == null || (n = n252.n()) == null)) {
                    n.requestFocus();
                }
                pq7.b(flexibleTextView, "counterTextView");
                flexibleTextView.setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ px5 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public s(px5 px5) {
            this.b = px5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FlexibleTextView flexibleTextView;
            FlexibleEditText flexibleEditText;
            n25 n25 = (n25) px5.K6(this.b).a();
            if (!(n25 == null || (flexibleEditText = n25.A) == null)) {
                flexibleEditText.requestLayout();
            }
            px5.L6(this.b).u(String.valueOf(editable));
            if (editable != null) {
                String str = editable.length() + " / 15";
                n25 n252 = (n25) px5.K6(this.b).a();
                if (n252 != null && (flexibleTextView = n252.D) != null) {
                    flexibleTextView.setText(str);
                }
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    /*
    static {
        String simpleName = px5.class.getSimpleName();
        pq7.b(simpleName, "AlarmFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ g37 K6(px5 px5) {
        g37<n25> g37 = px5.h;
        if (g37 != null) {
            return g37;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ nx5 L6(px5 px5) {
        nx5 nx5 = px5.i;
        if (nx5 != null) {
            return nx5;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ox5
    public void B0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return m;
    }

    @DexIgnore
    @Override // com.fossil.ox5
    public void E(int i2) {
        int i3;
        int i4 = i2 / 60;
        if (i4 >= 12) {
            i3 = 1;
            i4 -= 12;
        } else {
            i3 = 0;
        }
        if (i4 == 0) {
            i4 = 12;
        }
        g37<n25> g37 = this.h;
        if (g37 != null) {
            n25 a2 = g37.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.H;
                pq7.b(numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i4);
                NumberPicker numberPicker2 = a2.J;
                pq7.b(numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i2 % 60);
                NumberPicker numberPicker3 = a2.I;
                pq7.b(numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i3);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        nx5 nx5 = this.i;
        if (nx5 != null) {
            nx5.o();
            return false;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void O6(TextView textView) {
        pq7.c(textView, "textView");
        FlexibleTextView flexibleTextView = (FlexibleTextView) textView;
        if (flexibleTextView.isSelected()) {
            flexibleTextView.m("flexible_tv_selected");
        } else {
            flexibleTextView.m("flexible_tv_unselected");
        }
    }

    @DexIgnore
    public final void P6(boolean z) {
        if (!z) {
            E6("add_alarm_view");
        } else {
            E6("edit_alarm_view");
        }
    }

    @DexIgnore
    public final void Q6(n25 n25) {
        FlexibleTextView flexibleTextView = n25.u;
        pq7.b(flexibleTextView, "binding.daySunday");
        if (!flexibleTextView.isSelected()) {
            n25.u.m("flexible_tv_selected");
        } else {
            n25.u.m("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView2 = n25.s;
        pq7.b(flexibleTextView2, "binding.dayMonday");
        if (!flexibleTextView2.isSelected()) {
            n25.s.m("flexible_tv_selected");
        } else {
            n25.s.m("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView3 = n25.w;
        pq7.b(flexibleTextView3, "binding.dayTuesday");
        if (!flexibleTextView3.isSelected()) {
            n25.w.m("flexible_tv_selected");
        } else {
            n25.w.m("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView4 = n25.x;
        pq7.b(flexibleTextView4, "binding.dayWednesday");
        if (!flexibleTextView4.isSelected()) {
            n25.x.m("flexible_tv_selected");
        } else {
            n25.x.m("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView5 = n25.v;
        pq7.b(flexibleTextView5, "binding.dayThursday");
        if (!flexibleTextView5.isSelected()) {
            n25.v.m("flexible_tv_selected");
        } else {
            n25.v.m("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView6 = n25.r;
        pq7.b(flexibleTextView6, "binding.dayFriday");
        if (!flexibleTextView6.isSelected()) {
            n25.r.m("flexible_tv_selected");
        } else {
            n25.r.m("flexible_tv_unselected");
        }
        FlexibleTextView flexibleTextView7 = n25.t;
        pq7.b(flexibleTextView7, "binding.daySaturday");
        if (!flexibleTextView7.isSelected()) {
            n25.t.m("flexible_tv_selected");
        } else {
            n25.t.m("flexible_tv_unselected");
        }
        n25.E.setOnClickListener(new g(this));
        n25.F.setOnClickListener(new h(this));
        n25.L.setOnCheckedChangeListener(new i(this, n25));
        n25.y.setOnClickListener(new j(this));
        n25.u.setOnClickListener(new k(this, n25));
        n25.s.setOnClickListener(new l(this, n25));
        n25.w.setOnClickListener(new m(this, n25));
        n25.x.setOnClickListener(new n(this, n25));
        n25.v.setOnClickListener(new o(this, n25));
        n25.r.setOnClickListener(new b(this, n25));
        n25.t.setOnClickListener(new c(this, n25));
        NumberPicker numberPicker = n25.H;
        pq7.b(numberPicker, "binding.numberPickerOne");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = n25.H;
        pq7.b(numberPicker2, "binding.numberPickerOne");
        numberPicker2.setMaxValue(12);
        n25.H.setOnValueChangedListener(new d(this, n25));
        NumberPicker numberPicker3 = n25.J;
        pq7.b(numberPicker3, "binding.numberPickerTwo");
        numberPicker3.setMinValue(0);
        NumberPicker numberPicker4 = n25.J;
        pq7.b(numberPicker4, "binding.numberPickerTwo");
        numberPicker4.setMaxValue(59);
        n25.J.setOnValueChangedListener(new e(this, n25));
        String c2 = um5.c(PortfolioApp.h0.c(), 2131886102);
        pq7.b(c2, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Am)");
        if (c2 != null) {
            String upperCase = c2.toUpperCase();
            pq7.b(upperCase, "(this as java.lang.String).toUpperCase()");
            String c3 = um5.c(PortfolioApp.h0.c(), 2131886104);
            pq7.b(c3, "LanguageHelper.getString\u2026larm_EditAlarm_Title__Pm)");
            if (c3 != null) {
                String upperCase2 = c3.toUpperCase();
                pq7.b(upperCase2, "(this as java.lang.String).toUpperCase()");
                NumberPicker numberPicker5 = n25.I;
                pq7.b(numberPicker5, "binding.numberPickerThree");
                numberPicker5.setMinValue(0);
                NumberPicker numberPicker6 = n25.I;
                pq7.b(numberPicker6, "binding.numberPickerThree");
                numberPicker6.setMaxValue(1);
                n25.I.setDisplayedValues(new String[]{upperCase, upperCase2});
                n25.I.setOnValueChangedListener(new f(this, n25));
                return;
            }
            throw new il7("null cannot be cast to non-null type java.lang.String");
        }
        throw new il7("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        FragmentActivity activity;
        pq7.c(str, "tag");
        super.R5(str, i2, intent);
        int hashCode = str.hashCode();
        if (hashCode != -1375614559) {
            if (hashCode != 1038249436) {
                if (hashCode == 1185284775 && str.equals("CONFIRM_SET_ALARM_FAILED") && i2 == 2131362279 && (activity = getActivity()) != null) {
                    activity.finish();
                }
            } else if (str.equals("CONFIRM_DELETE_ALARM") && i2 == 2131363373) {
                nx5 nx5 = this.i;
                if (nx5 != null) {
                    nx5.n();
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        } else if (!str.equals("UNSAVED_CHANGE")) {
        } else {
            if (i2 != 2131363373) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    activity2.finish();
                    return;
                }
                return;
            }
            nx5 nx52 = this.i;
            if (nx52 != null) {
                nx52.p();
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void R6(int i2, boolean z) {
        nx5 nx5 = this.i;
        if (nx5 != null) {
            nx5.q(z, i2);
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* renamed from: S6 */
    public void M5(nx5 nx5) {
        pq7.c(nx5, "presenter");
        this.i = nx5;
    }

    @DexIgnore
    public final void T6(int i2, boolean z) {
        g37<n25> g37 = this.h;
        if (g37 != null) {
            n25 a2 = g37.a();
            if (a2 != null) {
                switch (i2) {
                    case 1:
                        FlexibleTextView flexibleTextView = a2.u;
                        pq7.b(flexibleTextView, "binding.daySunday");
                        flexibleTextView.setSelected(z);
                        FlexibleTextView flexibleTextView2 = a2.u;
                        pq7.b(flexibleTextView2, "binding.daySunday");
                        O6(flexibleTextView2);
                        return;
                    case 2:
                        FlexibleTextView flexibleTextView3 = a2.s;
                        pq7.b(flexibleTextView3, "binding.dayMonday");
                        flexibleTextView3.setSelected(z);
                        FlexibleTextView flexibleTextView4 = a2.s;
                        pq7.b(flexibleTextView4, "binding.dayMonday");
                        O6(flexibleTextView4);
                        return;
                    case 3:
                        FlexibleTextView flexibleTextView5 = a2.w;
                        pq7.b(flexibleTextView5, "binding.dayTuesday");
                        flexibleTextView5.setSelected(z);
                        FlexibleTextView flexibleTextView6 = a2.w;
                        pq7.b(flexibleTextView6, "binding.dayTuesday");
                        O6(flexibleTextView6);
                        return;
                    case 4:
                        FlexibleTextView flexibleTextView7 = a2.x;
                        pq7.b(flexibleTextView7, "binding.dayWednesday");
                        flexibleTextView7.setSelected(z);
                        FlexibleTextView flexibleTextView8 = a2.x;
                        pq7.b(flexibleTextView8, "binding.dayWednesday");
                        O6(flexibleTextView8);
                        return;
                    case 5:
                        FlexibleTextView flexibleTextView9 = a2.v;
                        pq7.b(flexibleTextView9, "binding.dayThursday");
                        flexibleTextView9.setSelected(z);
                        FlexibleTextView flexibleTextView10 = a2.v;
                        pq7.b(flexibleTextView10, "binding.dayThursday");
                        O6(flexibleTextView10);
                        return;
                    case 6:
                        FlexibleTextView flexibleTextView11 = a2.r;
                        pq7.b(flexibleTextView11, "binding.dayFriday");
                        flexibleTextView11.setSelected(z);
                        FlexibleTextView flexibleTextView12 = a2.r;
                        pq7.b(flexibleTextView12, "binding.dayFriday");
                        O6(flexibleTextView12);
                        return;
                    case 7:
                        FlexibleTextView flexibleTextView13 = a2.t;
                        pq7.b(flexibleTextView13, "binding.daySaturday");
                        flexibleTextView13.setSelected(z);
                        FlexibleTextView flexibleTextView14 = a2.t;
                        pq7.b(flexibleTextView14, "binding.daySaturday");
                        O6(flexibleTextView14);
                        return;
                    default:
                        return;
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ox5
    public void W2() {
        RTLImageView rTLImageView;
        g37<n25> g37 = this.h;
        if (g37 != null) {
            n25 a2 = g37.a();
            if (a2 != null && (rTLImageView = a2.F) != null) {
                rTLImageView.setVisibility(0);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ox5
    public void Y(boolean z) {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        FlexibleButton flexibleButton3;
        g37<n25> g37 = this.h;
        if (g37 != null) {
            n25 a2 = g37.a();
            if (!(a2 == null || (flexibleButton3 = a2.y) == null)) {
                flexibleButton3.setEnabled(z);
            }
            if (z) {
                g37<n25> g372 = this.h;
                if (g372 != null) {
                    n25 a3 = g372.a();
                    if (a3 != null && (flexibleButton2 = a3.y) != null) {
                        flexibleButton2.d("flexible_button_primary");
                        return;
                    }
                    return;
                }
                pq7.n("mBinding");
                throw null;
            }
            g37<n25> g373 = this.h;
            if (g373 != null) {
                n25 a4 = g373.a();
                if (a4 != null && (flexibleButton = a4.y) != null) {
                    flexibleButton.d("flexible_button_disabled");
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ox5
    public void Y1(Alarm alarm, boolean z) {
        pq7.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "showSetAlarmComplete: alarm = " + alarm + ", isSuccess = " + z);
        if (z) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
            }
        } else if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.q0(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.ox5
    public void Z4(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "startFireBaseTracer(), isAddNewAlarm = " + z);
        P6(z);
    }

    @DexIgnore
    @Override // com.fossil.ox5
    public void b5(SparseIntArray sparseIntArray) {
        pq7.c(sparseIntArray, "daysRepeat");
        int size = sparseIntArray.size();
        int i2 = 1;
        while (i2 <= 7) {
            if (size == 0) {
                T6(i2, false);
            } else {
                T6(i2, sparseIntArray.get(i2) == i2);
            }
            i2++;
        }
    }

    @DexIgnore
    @Override // com.fossil.ox5
    public void c() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.y(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.ox5
    public void l2(String str) {
        FlexibleEditText flexibleEditText;
        pq7.c(str, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, "showMessage, value = " + str);
        if (!vt7.l(str)) {
            g37<n25> g37 = this.h;
            if (g37 != null) {
                n25 a2 = g37.a();
                if (a2 != null && (flexibleEditText = a2.z) != null) {
                    flexibleEditText.setText(str);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ox5
    public void m2(boolean z) {
        ConstraintLayout constraintLayout;
        FlexibleSwitchCompat flexibleSwitchCompat;
        g37<n25> g37 = this.h;
        if (g37 != null) {
            n25 a2 = g37.a();
            if (!(a2 == null || (flexibleSwitchCompat = a2.L) == null)) {
                flexibleSwitchCompat.setChecked(z);
            }
            g37<n25> g372 = this.h;
            if (g372 != null) {
                n25 a3 = g372.a();
                if (a3 != null && (constraintLayout = a3.q) != null) {
                    constraintLayout.setVisibility(z ? 0 : 8);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        FlexibleEditText flexibleEditText;
        FlexibleEditText flexibleEditText2;
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        n25 n25 = (n25) aq0.f(layoutInflater, 2131558503, viewGroup, false, A6());
        pq7.b(n25, "binding");
        Q6(n25);
        g37<n25> g37 = new g37<>(this, n25);
        this.h = g37;
        n25 a2 = g37.a();
        if (!(a2 == null || (flexibleEditText2 = a2.A) == null)) {
            flexibleEditText2.addTextChangedListener(this.j);
            flexibleEditText2.setOnFocusChangeListener(new q(flexibleEditText2, this));
        }
        g37<n25> g372 = this.h;
        if (g372 != null) {
            n25 a3 = g372.a();
            if (!(a3 == null || (flexibleEditText = a3.z) == null)) {
                flexibleEditText.addTextChangedListener(this.k);
                flexibleEditText.setOnFocusChangeListener(new r(flexibleEditText, this));
            }
            g37<n25> g373 = this.h;
            if (g373 != null) {
                n25 a4 = g373.a();
                if (a4 != null) {
                    pq7.b(a4, "mBinding.get()!!");
                    return a4.n();
                }
                pq7.i();
                throw null;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        g37<n25> g37 = this.h;
        if (g37 != null) {
            n25 a2 = g37.a();
            if (a2 != null) {
                FlexibleEditText flexibleEditText = a2.A;
                if (flexibleEditText != null) {
                    flexibleEditText.removeTextChangedListener(this.j);
                }
                a2.z.removeTextChangedListener(this.k);
            }
            super.onDestroyView();
            v6();
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        nx5 nx5 = this.i;
        if (nx5 != null) {
            nx5.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        nx5 nx5 = this.i;
        if (nx5 != null) {
            nx5.l();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.i();
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ox5
    public void q4() {
        s37 s37 = s37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        s37.y0(childFragmentManager);
    }

    @DexIgnore
    @Override // com.fossil.ox5
    public void s5() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.r0(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.ox5
    public void t(String str) {
        FlexibleEditText flexibleEditText;
        pq7.c(str, "title");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = m;
        local.d(str2, "showTitle, value = " + str);
        if (!vt7.l(str)) {
            g37<n25> g37 = this.h;
            if (g37 != null) {
                n25 a2 = g37.a();
                if (a2 != null && (flexibleEditText = a2.A) != null) {
                    flexibleEditText.setText(str);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
