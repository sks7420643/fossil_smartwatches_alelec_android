package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.TimeZone;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ V1 CREATOR; // = new V1(null);
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ int d; // = ((TimeZone.getDefault().getOffset(this.e) / 1000) / 60);
    @DexIgnore
    public /* final */ long e;

    @DexIgnore
    public X1(long j) {
        this.e = j;
        long j2 = (long) 1000;
        long j3 = j / j2;
        this.b = j3;
        this.c = j - (j2 * j3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(new JSONObject(), Jd0.G2, Long.valueOf(this.b)), Jd0.H2, Long.valueOf(this.c)), Jd0.w2, Integer.valueOf(this.d));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.e);
    }
}
