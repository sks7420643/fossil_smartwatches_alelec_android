package com.fossil;

import android.database.Cursor;
import com.fossil.J32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class C32 implements J32.Bi {
    @DexIgnore
    public static /* final */ C32 a; // = new C32();

    @DexIgnore
    public static J32.Bi a() {
        return a;
    }

    @DexIgnore
    @Override // com.fossil.J32.Bi
    public Object apply(Object obj) {
        return Boolean.valueOf(((Cursor) obj).moveToNext());
    }
}
