package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import com.fossil.Lh1;
import java.nio.ByteBuffer;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hh1 extends Drawable implements Lh1.Bi, Animatable {
    @DexIgnore
    public /* final */ Ai b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public Paint j;
    @DexIgnore
    public Rect k;
    @DexIgnore
    public List<Tz0> l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Drawable.ConstantState {
        @DexIgnore
        public /* final */ Lh1 a;

        @DexIgnore
        public Ai(Lh1 lh1) {
            this.a = lh1;
        }

        @DexIgnore
        public int getChangingConfigurations() {
            return 0;
        }

        @DexIgnore
        public Drawable newDrawable() {
            return new Hh1(this);
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            return newDrawable();
        }
    }

    @DexIgnore
    public Hh1(Context context, Bb1 bb1, Sb1<Bitmap> sb1, int i2, int i3, Bitmap bitmap) {
        this(new Ai(new Lh1(Oa1.c(context), bb1, i2, i3, sb1, bitmap)));
    }

    @DexIgnore
    public Hh1(Ai ai) {
        this.f = true;
        this.h = -1;
        Ik1.d(ai);
        this.b = ai;
    }

    @DexIgnore
    @Override // com.fossil.Lh1.Bi
    public void a() {
        if (b() == null) {
            stop();
            invalidateSelf();
            return;
        }
        invalidateSelf();
        if (g() == f() - 1) {
            this.g++;
        }
        int i2 = this.h;
        if (i2 != -1 && this.g >= i2) {
            j();
            stop();
        }
    }

    @DexIgnore
    public final Drawable.Callback b() {
        Drawable.Callback callback = getCallback();
        while (callback instanceof Drawable) {
            callback = ((Drawable) callback).getCallback();
        }
        return callback;
    }

    @DexIgnore
    public ByteBuffer c() {
        return this.b.a.b();
    }

    @DexIgnore
    public final Rect d() {
        if (this.k == null) {
            this.k = new Rect();
        }
        return this.k;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        if (!this.e) {
            if (this.i) {
                Gravity.apply(119, getIntrinsicWidth(), getIntrinsicHeight(), getBounds(), d());
                this.i = false;
            }
            canvas.drawBitmap(this.b.a.c(), (Rect) null, d(), h());
        }
    }

    @DexIgnore
    public Bitmap e() {
        return this.b.a.e();
    }

    @DexIgnore
    public int f() {
        return this.b.a.f();
    }

    @DexIgnore
    public int g() {
        return this.b.a.d();
    }

    @DexIgnore
    public Drawable.ConstantState getConstantState() {
        return this.b;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.b.a.h();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.b.a.k();
    }

    @DexIgnore
    public int getOpacity() {
        return -2;
    }

    @DexIgnore
    public final Paint h() {
        if (this.j == null) {
            this.j = new Paint(2);
        }
        return this.j;
    }

    @DexIgnore
    public int i() {
        return this.b.a.j();
    }

    @DexIgnore
    public boolean isRunning() {
        return this.c;
    }

    @DexIgnore
    public final void j() {
        List<Tz0> list = this.l;
        if (list != null) {
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.l.get(i2).a(this);
            }
        }
    }

    @DexIgnore
    public void k() {
        this.e = true;
        this.b.a.a();
    }

    @DexIgnore
    public final void l() {
        this.g = 0;
    }

    @DexIgnore
    public void m(Sb1<Bitmap> sb1, Bitmap bitmap) {
        this.b.a.o(sb1, bitmap);
    }

    @DexIgnore
    public final void n() {
        Ik1.a(!this.e, "You cannot start a recycled Drawable. Ensure thatyou clear any references to the Drawable when clearing the corresponding request.");
        if (this.b.a.f() == 1) {
            invalidateSelf();
        } else if (!this.c) {
            this.c = true;
            this.b.a.r(this);
            invalidateSelf();
        }
    }

    @DexIgnore
    public final void o() {
        this.c = false;
        this.b.a.s(this);
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.i = true;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        h().setAlpha(i2);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        h().setColorFilter(colorFilter);
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        Ik1.a(!this.e, "Cannot change the visibility of a recycled resource. Ensure that you unset the Drawable from your View before changing the View's visibility.");
        this.f = z;
        if (!z) {
            o();
        } else if (this.d) {
            n();
        }
        return super.setVisible(z, z2);
    }

    @DexIgnore
    public void start() {
        this.d = true;
        l();
        if (this.f) {
            n();
        }
    }

    @DexIgnore
    public void stop() {
        this.d = false;
        o();
    }
}
