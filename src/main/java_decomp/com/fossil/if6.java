package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class If6 {
    @DexIgnore
    public /* final */ Bf6 a;
    @DexIgnore
    public /* final */ Tf6 b;
    @DexIgnore
    public /* final */ Nf6 c;

    @DexIgnore
    public If6(Bf6 bf6, Tf6 tf6, Nf6 nf6) {
        Wg6.c(bf6, "mActivityOverviewDayView");
        Wg6.c(tf6, "mActivityOverviewWeekView");
        Wg6.c(nf6, "mActivityOverviewMonthView");
        this.a = bf6;
        this.b = tf6;
        this.c = nf6;
    }

    @DexIgnore
    public final Bf6 a() {
        return this.a;
    }

    @DexIgnore
    public final Nf6 b() {
        return this.c;
    }

    @DexIgnore
    public final Tf6 c() {
        return this.b;
    }
}
