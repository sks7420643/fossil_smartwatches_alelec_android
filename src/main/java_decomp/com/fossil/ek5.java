package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AppHelper;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ek5 implements MembersInjector<AppHelper> {
    @DexIgnore
    public static void a(AppHelper appHelper, Q27 q27) {
        appHelper.d = q27;
    }

    @DexIgnore
    public static void b(AppHelper appHelper, DeviceRepository deviceRepository) {
        appHelper.b = deviceRepository;
    }

    @DexIgnore
    public static void c(AppHelper appHelper, An4 an4) {
        appHelper.a = an4;
    }

    @DexIgnore
    public static void d(AppHelper appHelper, UserRepository userRepository) {
        appHelper.c = userRepository;
    }
}
