package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B53 implements Xw2<E53> {
    @DexIgnore
    public static B53 c; // = new B53();
    @DexIgnore
    public /* final */ Xw2<E53> b;

    @DexIgnore
    public B53() {
        this(Ww2.b(new D53()));
    }

    @DexIgnore
    public B53(Xw2<E53> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((E53) c.zza()).zza();
    }

    @DexIgnore
    public static long b() {
        return ((E53) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ E53 zza() {
        return this.b.zza();
    }
}
