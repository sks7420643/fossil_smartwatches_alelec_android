package com.fossil;

import com.fossil.C44;
import com.fossil.D44;
import com.fossil.H34;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Q44<E> extends G34<E> {
    @DexIgnore
    public static /* final */ Q44<Object> EMPTY; // = new Q44<>(Y24.of());
    @DexIgnore
    public /* final */ transient D44.Ei<E>[] d;
    @DexIgnore
    public /* final */ transient D44.Ei<E>[] e;
    @DexIgnore
    public /* final */ transient int f;
    @DexIgnore
    public /* final */ transient int g;
    @DexIgnore
    @LazyInit
    public transient H34<E> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends H34.Bi<E> {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // com.fossil.U24
        public boolean contains(Object obj) {
            return Q44.this.contains(obj);
        }

        @DexIgnore
        @Override // com.fossil.H34.Bi
        public E get(int i) {
            return (E) Q44.this.d[i].getElement();
        }

        @DexIgnore
        @Override // com.fossil.U24
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return Q44.this.d.length;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<E> extends D44.Ei<E> {
        @DexIgnore
        public /* final */ D44.Ei<E> nextInBucket;

        @DexIgnore
        public Ci(E e, int i, D44.Ei<E> ei) {
            super(e, i);
            this.nextInBucket = ei;
        }

        @DexIgnore
        @Override // com.fossil.D44.Ei
        public D44.Ei<E> nextInBucket() {
            return this.nextInBucket;
        }
    }

    @DexIgnore
    public Q44(Collection<? extends C44.Ai<? extends E>> collection) {
        D44.Ei<E> ci;
        int size = collection.size();
        D44.Ei<E>[] eiArr = new D44.Ei[size];
        if (size == 0) {
            this.d = eiArr;
            this.e = null;
            this.f = 0;
            this.g = 0;
            this.h = H34.of();
            return;
        }
        int a2 = R24.a(size, 1.0d);
        D44.Ei<E>[] eiArr2 = new D44.Ei[a2];
        long j = 0;
        Iterator<? extends C44.Ai<? extends E>> it = collection.iterator();
        int i = 0;
        int i2 = 0;
        while (it.hasNext()) {
            C44.Ai ai = (C44.Ai) it.next();
            Object element = ai.getElement();
            I14.l(element);
            int count = ai.getCount();
            int hashCode = element.hashCode();
            int b = (a2 - 1) & R24.b(hashCode);
            D44.Ei<E> ei = eiArr2[b];
            if (ei == null) {
                ci = (ai instanceof D44.Ei) && !(ai instanceof Ci) ? (D44.Ei) ai : new D44.Ei<>(element, count);
            } else {
                ci = new Ci<>(element, count, ei);
            }
            i2 += hashCode ^ count;
            eiArr[i] = ci;
            eiArr2[b] = ci;
            j += (long) count;
            i++;
        }
        this.d = eiArr;
        this.e = eiArr2;
        this.f = V54.b(j);
        this.g = i2;
    }

    @DexIgnore
    @Override // com.fossil.C44, com.fossil.G34
    public int count(Object obj) {
        D44.Ei<E>[] eiArr = this.e;
        if (!(obj == null || eiArr == null)) {
            for (D44.Ei<E> ei = eiArr[R24.c(obj) & (eiArr.length - 1)]; ei != null; ei = ei.nextInBucket()) {
                if (F14.a(obj, ei.getElement())) {
                    return ei.getCount();
                }
            }
        }
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.C44, com.fossil.G34
    public H34<E> elementSet() {
        H34<E> h34 = this.h;
        if (h34 != null) {
            return h34;
        }
        Bi bi = new Bi();
        this.h = bi;
        return bi;
    }

    @DexIgnore
    @Override // com.fossil.G34
    public C44.Ai<E> getEntry(int i) {
        return this.d[i];
    }

    @DexIgnore
    @Override // com.fossil.G34
    public int hashCode() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.f;
    }
}
