package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class U13 {
    @DexIgnore
    public volatile M23 a;
    @DexIgnore
    public volatile Xz2 b;

    /*
    static {
        Q03.a();
    }
    */

    @DexIgnore
    public final M23 a(M23 m23) {
        M23 m232 = this.a;
        this.b = null;
        this.a = m23;
        return m232;
    }

    @DexIgnore
    public final int b() {
        if (this.b != null) {
            return this.b.zza();
        }
        if (this.a != null) {
            return this.a.l();
        }
        return 0;
    }

    @DexIgnore
    public final M23 c(M23 m23) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    try {
                        this.a = m23;
                        this.b = Xz2.zza;
                    } catch (L13 e) {
                        this.a = m23;
                        this.b = Xz2.zza;
                    }
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public final Xz2 d() {
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b != null) {
                return this.b;
            }
            if (this.a == null) {
                this.b = Xz2.zza;
            } else {
                this.b = this.a.g();
            }
            return this.b;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof U13)) {
            return false;
        }
        U13 u13 = (U13) obj;
        M23 m23 = this.a;
        M23 m232 = u13.a;
        return (m23 == null && m232 == null) ? d().equals(u13.d()) : (m23 == null || m232 == null) ? m23 != null ? m23.equals(u13.c(m23.d())) : c(m232.d()).equals(m232) : m23.equals(m232);
    }

    @DexIgnore
    public int hashCode() {
        return 1;
    }
}
