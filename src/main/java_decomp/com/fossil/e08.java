package com.fossil;

import com.fossil.Dl7;
import com.mapped.Af6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Rc6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E08 {
    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static final <T> void a(Hg6<? super Xe6<? super T>, ? extends Object> hg6, Xe6<? super T> xe6) {
        Go7.a(xe6);
        try {
            Af6 context = xe6.getContext();
            Object c = Zz7.c(context, null);
            if (hg6 != null) {
                try {
                    Ir7.d(hg6, 1);
                    Object invoke = hg6.invoke(xe6);
                    Zz7.a(context, c);
                    if (invoke != Yn7.d()) {
                        Dl7.Ai ai = Dl7.Companion;
                        xe6.resumeWith(Dl7.constructor-impl(invoke));
                    }
                } catch (Throwable th) {
                    Zz7.a(context, c);
                    throw th;
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type (kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            }
        } catch (Throwable th2) {
            Dl7.Ai ai2 = Dl7.Companion;
            xe6.resumeWith(Dl7.constructor-impl(El7.a(th2)));
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static final <R, T> void b(Coroutine<? super R, ? super Xe6<? super T>, ? extends Object> coroutine, R r, Xe6<? super T> xe6) {
        Go7.a(xe6);
        try {
            Af6 context = xe6.getContext();
            Object c = Zz7.c(context, null);
            if (coroutine != null) {
                try {
                    Ir7.d(coroutine, 2);
                    Object invoke = coroutine.invoke(r, xe6);
                    Zz7.a(context, c);
                    if (invoke != Yn7.d()) {
                        Dl7.Ai ai = Dl7.Companion;
                        xe6.resumeWith(Dl7.constructor-impl(invoke));
                    }
                } catch (Throwable th) {
                    Zz7.a(context, c);
                    throw th;
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            }
        } catch (Throwable th2) {
            Dl7.Ai ai2 = Dl7.Companion;
            xe6.resumeWith(Dl7.constructor-impl(El7.a(th2)));
        }
    }

    @DexIgnore
    public static final <T, R> Object c(Tz7<? super T> tz7, R r, Coroutine<? super R, ? super Xe6<? super T>, ? extends Object> coroutine) {
        Object vu7;
        tz7.v0();
        if (coroutine != null) {
            try {
                Ir7.d(coroutine, 2);
                vu7 = coroutine.invoke(r, tz7);
            } catch (Throwable th) {
                vu7 = new Vu7(th, false, 2, null);
            }
            if (vu7 == Yn7.d()) {
                return Yn7.d();
            }
            Object X = tz7.X(vu7);
            if (X == Gx7.b) {
                return Yn7.d();
            }
            if (!(X instanceof Vu7)) {
                return Gx7.h(X);
            }
            Throwable th2 = ((Vu7) X).a;
            Xe6<T> xe6 = tz7.e;
            if (!Nv7.d()) {
                throw th2;
            } else if (!(xe6 instanceof Do7)) {
                throw th2;
            } else {
                throw Uz7.a(th2, (Do7) xe6);
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
        }
    }

    @DexIgnore
    public static final <T, R> Object d(Tz7<? super T> tz7, R r, Coroutine<? super R, ? super Xe6<? super T>, ? extends Object> coroutine) {
        Object vu7;
        tz7.v0();
        if (coroutine != null) {
            try {
                Ir7.d(coroutine, 2);
                vu7 = coroutine.invoke(r, tz7);
            } catch (Throwable th) {
                vu7 = new Vu7(th, false, 2, null);
            }
            if (vu7 == Yn7.d()) {
                return Yn7.d();
            }
            Object X = tz7.X(vu7);
            if (X == Gx7.b) {
                return Yn7.d();
            }
            if (!(X instanceof Vu7)) {
                return Gx7.h(X);
            }
            Vu7 vu72 = (Vu7) X;
            Throwable th2 = vu72.a;
            if (!(th2 instanceof Zx7) || ((Zx7) th2).coroutine != tz7) {
                Throwable th3 = vu72.a;
                Xe6<T> xe6 = tz7.e;
                if (!Nv7.d()) {
                    throw th3;
                } else if (!(xe6 instanceof Do7)) {
                    throw th3;
                } else {
                    throw Uz7.a(th3, (Do7) xe6);
                }
            } else if (!(vu7 instanceof Vu7)) {
                return vu7;
            } else {
                Throwable th4 = ((Vu7) vu7).a;
                Xe6<T> xe62 = tz7.e;
                if (!Nv7.d()) {
                    throw th4;
                } else if (!(xe62 instanceof Do7)) {
                    throw th4;
                } else {
                    throw Uz7.a(th4, (Do7) xe62);
                }
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
        }
    }
}
