package com.fossil;

import java.lang.reflect.AccessibleObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Lk4 {
    @DexIgnore
    public static /* final */ Lk4 a; // = (Xj4.c() < 9 ? new Kk4() : new Mk4());

    @DexIgnore
    public static Lk4 a() {
        return a;
    }

    @DexIgnore
    public abstract void b(AccessibleObject accessibleObject);
}
