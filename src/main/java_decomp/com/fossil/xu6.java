package com.fossil;

import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xu6 {
    @DexIgnore
    public /* final */ BaseActivity a;
    @DexIgnore
    public /* final */ Vu6 b;

    @DexIgnore
    public Xu6(BaseActivity baseActivity, Vu6 vu6) {
        Wg6.c(baseActivity, "baseActivity");
        Wg6.c(vu6, "mView");
        this.a = baseActivity;
        this.b = vu6;
    }

    @DexIgnore
    public final BaseActivity a() {
        return this.a;
    }

    @DexIgnore
    public final Vu6 b() {
        return this.b;
    }
}
