package com.fossil;

import android.os.Looper;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xr2 implements Da3 {
    @DexIgnore
    @Override // com.fossil.Da3
    public final T62<Status> a(R62 r62, Ga3 ga3) {
        return r62.j(new Zr2(this, r62, ga3));
    }

    @DexIgnore
    @Override // com.fossil.Da3
    public final T62<Status> b(R62 r62, LocationRequest locationRequest, Ga3 ga3) {
        Rc2.l(Looper.myLooper(), "Calling thread must be a prepared Looper thread.");
        return r62.j(new Yr2(this, r62, locationRequest, ga3));
    }
}
