package com.fossil;

import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hj5 {
    @DexIgnore
    public static final Xs4 a(Xs4 xs4) {
        Wg6.c(xs4, "$this$clone");
        return new Xs4(xs4.d(), xs4.i(), xs4.b(), xs4.e(), xs4.g(), xs4.h(), xs4.a(), xs4.f(), xs4.c());
    }

    @DexIgnore
    public static final List<Ot4> b(List<Xs4> list) {
        Wg6.c(list, "$this$toSuggestedFriends");
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            arrayList.add(new Ot4(t.d(), t.i(), t.b(), t.e(), t.h(), false, 32, null));
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<Object> c(List<Xs4> list) {
        int i;
        int i2;
        int i3 = 0;
        Wg6.c(list, "$this$unSectionedFriendToSectionedFriends");
        ArrayList arrayList = new ArrayList();
        if (((Xs4) Pm7.F(list)).c() == 2) {
            if (!(list instanceof Collection) || !list.isEmpty()) {
                Iterator<T> it = list.iterator();
                i2 = 0;
                while (it.hasNext()) {
                    if (it.next().c() == 2) {
                        int i4 = i2 + 1;
                        if (i4 >= 0) {
                            i2 = i4;
                        } else {
                            Hm7.k();
                            throw null;
                        }
                    }
                }
            } else {
                i2 = 0;
            }
            Hr7 hr7 = Hr7.a;
            String c = Um5.c(PortfolioApp.get.instance(), 2131886298);
            Wg6.b(c, "LanguageHelper.getString\u2026t_Title__PendingRequests)");
            String format = String.format(c, Arrays.copyOf(new Object[]{Integer.valueOf(i2)}, 1));
            Wg6.b(format, "java.lang.String.format(format, *args)");
            arrayList.add(format);
        }
        if (((Xs4) Pm7.P(list)).c() == 0 || ((Xs4) Pm7.P(list)).c() == 1 || ((Xs4) Pm7.P(list)).c() == -1) {
            if (!(list instanceof Collection) || !list.isEmpty()) {
                i = 0;
                for (T t : list) {
                    if (t.c() == 0 || t.c() == 1 || t.c() == -1) {
                        int i5 = i + 1;
                        if (i5 >= 0) {
                            i = i5;
                        } else {
                            Hm7.k();
                            throw null;
                        }
                    }
                }
            } else {
                i = 0;
            }
            Hr7 hr72 = Hr7.a;
            String c2 = Um5.c(PortfolioApp.get.instance(), 2131886297);
            Wg6.b(c2, "LanguageHelper.getString\u2026ends_List_Title__Friends)");
            String format2 = String.format(c2, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            Wg6.b(format2, "java.lang.String.format(format, *args)");
            arrayList.add(format2);
        }
        for (T t2 : list) {
            if (t2.c() == 2) {
                i3++;
                arrayList.add(i3, t2);
            } else {
                arrayList.add(t2);
            }
        }
        return arrayList;
    }
}
