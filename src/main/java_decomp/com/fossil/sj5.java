package com.fossil;

import android.net.Uri;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.charset.Charset;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sj5 implements Mb1 {
    @DexIgnore
    public String b;
    @DexIgnore
    public Uri c;
    @DexIgnore
    public String d;

    @DexIgnore
    public Sj5(Uri uri, String str) {
        this.c = uri;
        this.d = str;
    }

    @DexIgnore
    public Sj5(String str, String str2) {
        this.b = str;
        this.d = str2;
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public void a(MessageDigest messageDigest) {
        Wg6.c(messageDigest, "messageDigest");
        String str = this.b + this.c + this.d;
        Charset charset = Mb1.a;
        Wg6.b(charset, "Key.CHARSET");
        if (str != null) {
            byte[] bytes = str.getBytes(charset);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            messageDigest.update(bytes);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final String c() {
        return this.d;
    }

    @DexIgnore
    public final Uri d() {
        return this.c;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }
}
