package com.fossil;

import java.util.Comparator;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D54 {

    @DexIgnore
    public enum Bi {
        NEXT_LOWER {
            @DexIgnore
            @Override // com.fossil.D54.Bi
            public int resultIndex(int i) {
                return i - 1;
            }
        },
        NEXT_HIGHER {
            @DexIgnore
            @Override // com.fossil.D54.Bi
            public int resultIndex(int i) {
                return i;
            }
        },
        INVERTED_INSERTION_INDEX {
            @DexIgnore
            @Override // com.fossil.D54.Bi
            public int resultIndex(int i) {
                return i;
            }
        };

        @DexIgnore
        public abstract int resultIndex(int i);
    }

    @DexIgnore
    public enum Ci {
        ANY_PRESENT {
            @DexIgnore
            @Override // com.fossil.D54.Ci
            public <E> int resultIndex(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                return i;
            }
        },
        LAST_PRESENT {
            @DexIgnore
            @Override // com.fossil.D54.Ci
            public <E> int resultIndex(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                int size = list.size() - 1;
                while (i < size) {
                    int i2 = ((i + size) + 1) >>> 1;
                    if (comparator.compare((Object) list.get(i2), e) > 0) {
                        size = i2 - 1;
                    } else {
                        i = i2;
                    }
                }
                return i;
            }
        },
        FIRST_PRESENT {
            @DexIgnore
            @Override // com.fossil.D54.Ci
            public <E> int resultIndex(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                int i2 = 0;
                while (i2 < i) {
                    int i3 = (i2 + i) >>> 1;
                    if (comparator.compare((Object) list.get(i3), e) < 0) {
                        i2 = i3 + 1;
                    } else {
                        i = i3;
                    }
                }
                return i2;
            }
        },
        FIRST_AFTER {
            @DexIgnore
            @Override // com.fossil.D54.Ci
            public <E> int resultIndex(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                return Ci.LAST_PRESENT.resultIndex(comparator, e, list, i) + 1;
            }
        },
        LAST_BEFORE {
            @DexIgnore
            @Override // com.fossil.D54.Ci
            public <E> int resultIndex(Comparator<? super E> comparator, E e, List<? extends E> list, int i) {
                return Ci.FIRST_PRESENT.resultIndex(comparator, e, list, i) - 1;
            }
        };

        @DexIgnore
        public abstract <E> int resultIndex(Comparator<? super E> comparator, E e, List<? extends E> list, int i);
    }

    @DexIgnore
    public static <E> int a(List<? extends E> list, E e, Comparator<? super E> comparator, Ci ci, Bi bi) {
        I14.l(comparator);
        I14.l(list);
        I14.l(ci);
        I14.l(bi);
        if (!(list instanceof RandomAccess)) {
            list = T34.h(list);
        }
        int i = 0;
        int size = list.size() - 1;
        while (i <= size) {
            int i2 = (i + size) >>> 1;
            int compare = comparator.compare(e, (Object) list.get(i2));
            if (compare < 0) {
                size = i2 - 1;
            } else if (compare <= 0) {
                return ci.resultIndex(comparator, e, list.subList(i, size + 1), i2 - i) + i;
            } else {
                i = i2 + 1;
            }
        }
        return bi.resultIndex(i);
    }
}
