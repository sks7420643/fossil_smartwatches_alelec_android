package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D9 extends Tx1<El1[]> {
    @DexIgnore
    public D9() {
        super(null, 1, null);
    }

    @DexIgnore
    @Override // com.fossil.Qx1
    public byte[] b(Object obj) {
        return V9.d.h((El1[]) obj);
    }
}
