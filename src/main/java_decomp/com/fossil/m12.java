package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M12 implements Factory<V12> {
    @DexIgnore
    public /* final */ Provider<T32> a;

    @DexIgnore
    public M12(Provider<T32> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static V12 a(T32 t32) {
        V12 a2 = L12.a(t32);
        Lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public static M12 b(Provider<T32> provider) {
        return new M12(provider);
    }

    @DexIgnore
    public V12 c() {
        return a(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return c();
    }
}
