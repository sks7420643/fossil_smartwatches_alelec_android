package com.fossil;

import android.content.Context;
import android.util.Log;
import com.mapped.Yd;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S42 extends Yd<Void> implements T72 {
    @DexIgnore
    public Semaphore a; // = new Semaphore(0);
    @DexIgnore
    public Set<R62> b;

    @DexIgnore
    public S42(Context context, Set<R62> set) {
        super(context);
        this.b = set;
    }

    @DexIgnore
    public final Void a() {
        int i = 0;
        for (R62 r62 : this.b) {
            if (r62.o(this)) {
                i++;
            }
        }
        try {
            this.a.tryAcquire(i, 5, TimeUnit.SECONDS);
            return null;
        } catch (InterruptedException e) {
            Log.i("GACSignInLoader", "Unexpected InterruptedException", e);
            Thread.currentThread().interrupt();
            return null;
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Yd
    public final /* synthetic */ Void loadInBackground() {
        return a();
    }

    @DexIgnore
    @Override // com.fossil.T72
    public final void onComplete() {
        this.a.release();
    }

    @DexIgnore
    @Override // com.fossil.At0
    public final void onStartLoading() {
        this.a.drainPermits();
        forceLoad();
    }
}
