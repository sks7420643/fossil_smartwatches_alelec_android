package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L93 implements Xw2<K93> {
    @DexIgnore
    public static L93 c; // = new L93();
    @DexIgnore
    public /* final */ Xw2<K93> b;

    @DexIgnore
    public L93() {
        this(Ww2.b(new N93()));
    }

    @DexIgnore
    public L93(Xw2<K93> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((K93) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ K93 zza() {
        return this.b.zza();
    }
}
