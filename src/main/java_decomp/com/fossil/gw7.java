package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gw7 implements Sw7 {
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public Gw7(boolean z) {
        this.b = z;
    }

    @DexIgnore
    @Override // com.fossil.Sw7
    public Kx7 b() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Sw7
    public boolean isActive() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Empty{");
        sb.append(isActive() ? "Active" : "New");
        sb.append('}');
        return sb.toString();
    }
}
