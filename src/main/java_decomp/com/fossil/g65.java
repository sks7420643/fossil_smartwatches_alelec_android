package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class G65 extends F65 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d H; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray I;
    @DexIgnore
    public long G;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        I = sparseIntArray;
        sparseIntArray.put(2131362666, 1);
        I.put(2131363410, 2);
        I.put(2131363076, 3);
        I.put(2131362712, 4);
        I.put(2131362424, 5);
        I.put(2131362423, 6);
        I.put(2131362123, 7);
        I.put(2131362234, 8);
        I.put(2131362245, 9);
        I.put(2131362247, 10);
        I.put(2131362236, 11);
        I.put(2131362465, 12);
        I.put(2131362427, 13);
        I.put(2131362296, 14);
        I.put(2131361941, 15);
    }
    */

    @DexIgnore
    public G65(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 16, H, I));
    }

    @DexIgnore
    public G65(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[15], (ConstraintLayout) objArr[7], (FlexibleEditText) objArr[8], (FlexibleEditText) objArr[11], (FlexibleEditText) objArr[9], (FlexibleEditText) objArr[10], (FlexibleButton) objArr[14], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[12], (RTLImageView) objArr[1], (RTLImageView) objArr[4], (ConstraintLayout) objArr[0], (ScrollView) objArr[3], (FlexibleTextView) objArr[2]);
        this.G = -1;
        this.D.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.G = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.G != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.G = 1;
        }
        w();
    }
}
