package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.G72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pi2 implements Parcelable.Creator<G72> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ G72 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 0;
        int i2 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                str3 = Ad2.f(parcel, t);
            } else if (l == 2) {
                str2 = Ad2.f(parcel, t);
            } else if (l == 4) {
                str = Ad2.f(parcel, t);
            } else if (l == 5) {
                i2 = Ad2.v(parcel, t);
            } else if (l != 6) {
                Ad2.B(parcel, t);
            } else {
                i = Ad2.v(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new G72(str3, str2, str, i2, i);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ G72[] newArray(int i) {
        return new G72[i];
    }
}
