package com.fossil;

import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Un0 {
    @DexIgnore
    public /* final */ View a;
    @DexIgnore
    public /* final */ Ci b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ View.OnLongClickListener f; // = new Ai();
    @DexIgnore
    public /* final */ View.OnTouchListener g; // = new Bi();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements View.OnLongClickListener {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            return Un0.this.c(view);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements View.OnTouchListener {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            return Un0.this.d(view, motionEvent);
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        boolean a(View view, Un0 un0);
    }

    @DexIgnore
    public Un0(View view, Ci ci) {
        this.a = view;
        this.b = ci;
    }

    @DexIgnore
    public void a() {
        this.a.setOnLongClickListener(this.f);
        this.a.setOnTouchListener(this.g);
    }

    @DexIgnore
    public void b(Point point) {
        point.set(this.c, this.d);
    }

    @DexIgnore
    public boolean c(View view) {
        return this.b.a(view, this);
    }

    @DexIgnore
    public boolean d(View view, MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                if (action != 2) {
                    if (action != 3) {
                        return false;
                    }
                } else if (!Bo0.b(motionEvent, 8194) || (motionEvent.getButtonState() & 1) == 0 || this.e) {
                    return false;
                } else {
                    if (this.c == x && this.d == y) {
                        return false;
                    }
                    this.c = x;
                    this.d = y;
                    boolean a2 = this.b.a(view, this);
                    this.e = a2;
                    return a2;
                }
            }
            this.e = false;
            return false;
        }
        this.c = x;
        this.d = y;
        return false;
    }
}
