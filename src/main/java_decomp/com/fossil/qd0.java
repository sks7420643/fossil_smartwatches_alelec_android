package com.fossil;

import com.mapped.Cd6;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qd0 {
    @DexIgnore
    public static /* final */ HashMap<String, Pd0> a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ Qd0 b; // = new Qd0();

    @DexIgnore
    public final long a(String str) {
        long min;
        synchronized (a) {
            Pd0 pd0 = a.get(str);
            if (pd0 == null) {
                pd0 = new Pd0(0, 128);
            }
            min = Math.min(128L, Lr7.c(Math.pow((double) 2, (double) (pd0.a + 1))));
            if (min < pd0.b) {
                pd0.a++;
            }
            a.put(str, pd0);
            M80.c.a("ExponentialBackOff", "getNextRate: key=%s, newRate=%d.", str, Long.valueOf(min));
        }
        return 1000 * min;
    }

    @DexIgnore
    public final void b(String str) {
        synchronized (a) {
            Pd0 pd0 = a.get(str);
            if (pd0 == null) {
                pd0 = new Pd0(0, 128);
            }
            a.put(str, pd0.a(0, pd0.b));
            Cd6 cd6 = Cd6.a;
        }
    }
}
