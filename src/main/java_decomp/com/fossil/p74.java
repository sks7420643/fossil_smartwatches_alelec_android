package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class P74<T> implements Mg4<T> {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Object a; // = c;
    @DexIgnore
    public volatile Mg4<T> b;

    @DexIgnore
    public P74(Mg4<T> mg4) {
        this.b = mg4;
    }

    @DexIgnore
    @Override // com.fossil.Mg4
    public T get() {
        T t = (T) this.a;
        if (t == c) {
            synchronized (this) {
                t = this.a;
                if (t == c) {
                    t = this.b.get();
                    this.a = t;
                    this.b = null;
                }
            }
        }
        return t;
    }
}
