package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class G88 extends RuntimeException {
    @DexIgnore
    public /* final */ transient Q88<?> b;
    @DexIgnore
    public /* final */ int code;
    @DexIgnore
    public /* final */ String message;

    @DexIgnore
    public G88(Q88<?> q88) {
        super(a(q88));
        this.code = q88.b();
        this.message = q88.f();
        this.b = q88;
    }

    @DexIgnore
    public static String a(Q88<?> q88) {
        U88.b(q88, "response == null");
        return "HTTP " + q88.b() + " " + q88.f();
    }

    @DexIgnore
    public int code() {
        return this.code;
    }

    @DexIgnore
    public String message() {
        return this.message;
    }

    @DexIgnore
    public Q88<?> response() {
        return this.b;
    }
}
