package com.fossil;

import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class X84 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Tb4 b;

    @DexIgnore
    public X84(String str, Tb4 tb4) {
        this.a = str;
        this.b = tb4;
    }

    @DexIgnore
    public boolean a() {
        try {
            return b().createNewFile();
        } catch (IOException e) {
            X74 f = X74.f();
            f.e("Error creating marker: " + this.a, e);
            return false;
        }
    }

    @DexIgnore
    public final File b() {
        return new File(this.b.b(), this.a);
    }

    @DexIgnore
    public boolean c() {
        return b().exists();
    }

    @DexIgnore
    public boolean d() {
        return b().delete();
    }
}
