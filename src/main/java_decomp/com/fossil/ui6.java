package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ui6 {
    @DexIgnore
    public /* final */ Oi6 a;
    @DexIgnore
    public /* final */ Fj6 b;
    @DexIgnore
    public /* final */ Zi6 c;

    @DexIgnore
    public Ui6(Oi6 oi6, Fj6 fj6, Zi6 zi6) {
        Wg6.c(oi6, "mHeartRateOverviewDayView");
        Wg6.c(fj6, "mHeartRateOverviewWeekView");
        Wg6.c(zi6, "mHeartRateOverviewMonthView");
        this.a = oi6;
        this.b = fj6;
        this.c = zi6;
    }

    @DexIgnore
    public final Oi6 a() {
        return this.a;
    }

    @DexIgnore
    public final Zi6 b() {
        return this.c;
    }

    @DexIgnore
    public final Fj6 c() {
        return this.b;
    }
}
