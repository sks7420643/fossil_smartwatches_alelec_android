package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ks3 extends IInterface {
    @DexIgnore
    void M1(Os3 os3) throws RemoteException;

    @DexIgnore
    void Q2(Us3 us3) throws RemoteException;

    @DexIgnore
    void f1(Status status) throws RemoteException;

    @DexIgnore
    void o2(Z52 z52, Js3 js3) throws RemoteException;

    @DexIgnore
    void s1(Status status) throws RemoteException;

    @DexIgnore
    void v(Status status, GoogleSignInAccount googleSignInAccount) throws RemoteException;
}
