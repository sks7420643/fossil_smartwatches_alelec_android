package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.viewpager2.widget.ViewPager2;
import com.mapped.WatchFaceComplicationFragment;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleButton;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D87 extends BaseFragment {
    @DexIgnore
    public /* final */ ArrayList<Fragment> g; // = new ArrayList<>();
    @DexIgnore
    public Pg5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ D87 b;

        @DexIgnore
        public Ai(D87 d87) {
            this.b = d87;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.P6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ D87 b;

        @DexIgnore
        public Bi(D87 d87) {
            this.b = d87;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.Q6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<T> {
        @DexIgnore
        public /* final */ /* synthetic */ D87 a;

        @DexIgnore
        public Ci(D87 d87) {
            this.a = d87;
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public final void onChanged(T t) {
            T t2 = t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceComplicationContainerFragment", "watchFaceComplicationsLive, value = " + ((Object) t2));
            if (!t2.b().b().isEmpty()) {
                String a2 = t2.b().a();
                if (!(a2 == null || Vt7.l(a2))) {
                    FlexibleButton flexibleButton = D87.K6(this.a).c;
                    Wg6.b(flexibleButton, "mBinding.fbRing");
                    flexibleButton.setVisibility(0);
                    return;
                }
            }
            this.a.P6();
            FlexibleButton flexibleButton2 = D87.K6(this.a).c;
            Wg6.b(flexibleButton2, "mBinding.fbRing");
            flexibleButton2.setVisibility(4);
        }
    }

    @DexIgnore
    public static final /* synthetic */ Pg5 K6(D87 d87) {
        Pg5 pg5 = d87.h;
        if (pg5 != null) {
            return pg5;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return "WatchFaceComplicationContainerFragment";
    }

    @DexIgnore
    public final void N6() {
        this.g.clear();
        WatchFaceComplicationFragment watchFaceComplicationFragment = (WatchFaceComplicationFragment) getChildFragmentManager().Z("WatchFaceComplicationFragment");
        K87 k87 = (K87) getChildFragmentManager().Z("WatchFaceRingFragment");
        if (watchFaceComplicationFragment == null) {
            watchFaceComplicationFragment = WatchFaceComplicationFragment.l.a();
        }
        if (k87 == null) {
            k87 = K87.l.a();
        }
        this.g.add(watchFaceComplicationFragment);
        this.g.add(k87);
        Pg5 pg5 = this.h;
        if (pg5 != null) {
            ViewPager2 viewPager2 = pg5.e;
            Wg6.b(viewPager2, "it");
            viewPager2.setAdapter(new G67(getChildFragmentManager(), this.g));
            viewPager2.setUserInputEnabled(false);
            viewPager2.setOffscreenPageLimit(2);
            Hc7 hc7 = Hc7.c;
            Fragment requireParentFragment = requireParentFragment();
            Wg6.b(requireParentFragment, "requireParentFragment()");
            hc7.b(requireParentFragment, watchFaceComplicationFragment);
            Hc7 hc72 = Hc7.c;
            Fragment requireParentFragment2 = requireParentFragment();
            Wg6.b(requireParentFragment2, "requireParentFragment()");
            hc72.b(requireParentFragment2, k87);
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void O6() {
        N6();
        P6();
        Pg5 pg5 = this.h;
        if (pg5 != null) {
            pg5.b.setOnClickListener(new Ai(this));
            pg5.c.setOnClickListener(new Bi(this));
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void P6() {
        Pg5 pg5 = this.h;
        if (pg5 != null) {
            pg5.c.d("flexible_button_secondary");
            pg5.b.d("flexible_button_primary");
            pg5.e.j(0, false);
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        Pg5 pg5 = this.h;
        if (pg5 != null) {
            pg5.b.d("flexible_button_secondary");
            pg5.c.d("flexible_button_primary");
            pg5.e.j(1, false);
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        LiveData<Eb7> q;
        super.onActivityCreated(bundle);
        String d = ThemeManager.l.a().d("nonBrandSurface");
        if (!TextUtils.isEmpty(d)) {
            Pg5 pg5 = this.h;
            if (pg5 != null) {
                pg5.d.setBackgroundColor(Color.parseColor(d));
            } else {
                Wg6.n("mBinding");
                throw null;
            }
        }
        Hc7 hc7 = Hc7.c;
        Fragment requireParentFragment = requireParentFragment();
        Wg6.b(requireParentFragment, "requireParentFragment()");
        Gc7 f = hc7.f(requireParentFragment);
        if (f != null && (q = f.q()) != null) {
            LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
            Wg6.b(viewLifecycleOwner, "viewLifecycleOwner");
            q.h(viewLifecycleOwner, new Ci(this));
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Pg5 c = Pg5.c(layoutInflater);
        Wg6.b(c, "WatchFaceComplicationCon\u2026Binding.inflate(inflater)");
        this.h = c;
        O6();
        Pg5 pg5 = this.h;
        if (pg5 != null) {
            ConstraintLayout b = pg5.b();
            Wg6.b(b, "mBinding.root");
            return b;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
