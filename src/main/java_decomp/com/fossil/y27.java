package com.fossil;

import com.portfolio.platform.data.source.ActivitiesRepository;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y27 implements MembersInjector<X27> {
    @DexIgnore
    public static void a(X27 x27, ActivitiesRepository activitiesRepository) {
        x27.a = activitiesRepository;
    }
}
