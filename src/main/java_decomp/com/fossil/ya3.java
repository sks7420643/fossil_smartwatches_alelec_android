package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ya3 implements Parcelable.Creator<Xa3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Xa3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 1;
        int i2 = 1;
        long j = -1;
        long j2 = -1;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                i2 = Ad2.v(parcel, t);
            } else if (l == 2) {
                i = Ad2.v(parcel, t);
            } else if (l == 3) {
                j2 = Ad2.y(parcel, t);
            } else if (l != 4) {
                Ad2.B(parcel, t);
            } else {
                j = Ad2.y(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Xa3(i2, i, j2, j);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Xa3[] newArray(int i) {
        return new Xa3[i];
    }
}
