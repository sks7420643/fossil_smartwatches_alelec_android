package com.fossil;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@TargetApi(14)
public final class Qo3 implements Application.ActivityLifecycleCallbacks {
    @DexIgnore
    public /* final */ /* synthetic */ Un3 b;

    @DexIgnore
    public Qo3(Un3 un3) {
        this.b = un3;
    }

    @DexIgnore
    public /* synthetic */ Qo3(Un3 un3, Vn3 vn3) {
        this(un3);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00a0 A[Catch:{ Exception -> 0x011d }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0159  */
    /* JADX WARNING: Removed duplicated region for block: B:82:? A[Catch:{ Exception -> 0x011d }, ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(boolean r10, android.net.Uri r11, java.lang.String r12, java.lang.String r13) {
        /*
        // Method dump skipped, instructions count: 519
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Qo3.b(boolean, android.net.Uri, java.lang.String, java.lang.String):void");
    }

    @DexIgnore
    public final void onActivityCreated(Activity activity, Bundle bundle) {
        try {
            this.b.d().N().a("onActivityCreated");
            Intent intent = activity.getIntent();
            if (intent != null) {
                Uri data = intent.getData();
                if (data == null || !data.isHierarchical()) {
                    this.b.s().G(activity, bundle);
                    return;
                }
                this.b.k();
                this.b.c().y(new Po3(this, bundle == null, data, Kr3.Y(intent) ? "gs" : "auto", data.getQueryParameter("referrer")));
                this.b.s().G(activity, bundle);
            }
        } catch (Exception e) {
            this.b.d().F().b("Throwable caught in onActivityCreated", e);
        } finally {
            this.b.s().G(activity, bundle);
        }
    }

    @DexIgnore
    public final void onActivityDestroyed(Activity activity) {
        this.b.s().V(activity);
    }

    @DexIgnore
    public final void onActivityPaused(Activity activity) {
        this.b.s().T(activity);
        Jq3 u = this.b.u();
        u.c().y(new Lq3(u, u.zzm().c()));
    }

    @DexIgnore
    public final void onActivityResumed(Activity activity) {
        Jq3 u = this.b.u();
        u.c().y(new Mq3(u, u.zzm().c()));
        this.b.s().F(activity);
    }

    @DexIgnore
    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        this.b.s().U(activity, bundle);
    }

    @DexIgnore
    public final void onActivityStarted(Activity activity) {
    }

    @DexIgnore
    public final void onActivityStopped(Activity activity) {
    }
}
