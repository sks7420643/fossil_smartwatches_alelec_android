package com.fossil;

import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.nk5;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.ActivePreset;
import com.portfolio.platform.data.legacy.threedotzero.DeviceModel;
import com.portfolio.platform.data.legacy.threedotzero.LegacyGoalTrackingSettings;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.model.room.microapp.ButtonMapping;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oq4 {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ a v; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ List<DianaAppSetting> f2708a; // = new ArrayList();
    @DexIgnore
    public /* final */ on5 b;
    @DexIgnore
    public /* final */ UserRepository c;
    @DexIgnore
    public /* final */ NotificationsRepository d;
    @DexIgnore
    public /* final */ yy6 e;
    @DexIgnore
    public /* final */ PortfolioApp f;
    @DexIgnore
    public /* final */ GoalTrackingRepository g;
    @DexIgnore
    public /* final */ DeviceDao h;
    @DexIgnore
    public /* final */ HybridCustomizeDatabase i;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository j;
    @DexIgnore
    public /* final */ pr4 k;
    @DexIgnore
    public /* final */ DianaPresetRepository l;
    @DexIgnore
    public /* final */ WatchFaceRepository m;
    @DexIgnore
    public /* final */ FileRepository n;
    @DexIgnore
    public /* final */ k97 o;
    @DexIgnore
    public /* final */ uo5 p;
    @DexIgnore
    public /* final */ s77 q;
    @DexIgnore
    public /* final */ DeviceRepository r;
    @DexIgnore
    public /* final */ DianaAppSettingRepository s;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return oq4.u;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.MigrationManager", f = "MigrationManager.kt", l = {567, 568, 595}, m = "migrateComplicationWatchAppSetting46")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ oq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(oq4 oq4, qn7 qn7) {
            super(qn7);
            this.this$0 = oq4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.MigrationManager", f = "MigrationManager.kt", l = {626, 672, 677, 727}, m = "migrateFor2Dot0")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ oq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(oq4 oq4, qn7 qn7) {
            super(qn7);
            this.this$0 = oq4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.p(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.MigrationManager$migrateFor2Dot0$4", f = "MigrationManager.kt", l = {763}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ oq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(oq4 oq4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = oq4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                yy6 yy6 = this.this$0.e;
                ty6 ty6 = new ty6(this.this$0.f.J(), ry6.MIGRATION);
                this.L$0 = iv7;
                this.label = 1;
                if (jq4.a(yy6, ty6, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ LegacyGoalTrackingSettings $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ oq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(LegacyGoalTrackingSettings legacyGoalTrackingSettings, qn7 qn7, oq4 oq4) {
            super(2, qn7);
            this.$it = legacyGoalTrackingSettings;
            this.this$0 = oq4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.$it, qn7, this.this$0);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0050 A[LOOP:0: B:12:0x004e->B:13:0x0050, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x010b  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 273
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.oq4.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.MigrationManager$migrateFor4Dot0$8", f = "MigrationManager.kt", l = {286}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ oq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(oq4 oq4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = oq4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, qn7);
            fVar.p$ = (iv7) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                yy6 yy6 = this.this$0.e;
                ty6 ty6 = new ty6(this.this$0.f.J(), ry6.MIGRATION);
                this.L$0 = iv7;
                this.label = 1;
                if (jq4.a(yy6, ty6, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ oq4 b;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList c;
        @DexIgnore
        public /* final */ /* synthetic */ List d;

        @DexIgnore
        public g(oq4 oq4, ArrayList arrayList, List list) {
            this.b = oq4;
            this.c = arrayList;
            this.d = list;
        }

        @DexIgnore
        public final void run() {
            this.b.i.microAppDao().upsertListMicroApp(this.c);
            this.b.i.presetDao().upsertPresetList(this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.MigrationManager", f = "MigrationManager.kt", l = {606, 621}, m = "migratePhotoBackgrounds46")
    public static final class h extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ oq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(oq4 oq4, qn7 qn7) {
            super(qn7);
            this.this$0 = oq4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.r(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.MigrationManager", f = "MigrationManager.kt", l = {351, 361, 362, 363, 364, 365}, m = "migratePresetToWatchFace")
    public static final class i extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ oq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(oq4 oq4, qn7 qn7) {
            super(qn7);
            this.this$0 = oq4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.MigrationManager", f = "MigrationManager.kt", l = {384, 401, 534, 557}, m = "migratePresets46")
    public static final class j extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$13;
        @DexIgnore
        public Object L$14;
        @DexIgnore
        public Object L$15;
        @DexIgnore
        public Object L$16;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ oq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(oq4 oq4, qn7 qn7) {
            super(qn7);
            this.this$0 = oq4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.t(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.MigrationManager", f = "MigrationManager.kt", l = {147, 150, 154}, m = "processMigration")
    public static final class k extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ oq4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(oq4 oq4, qn7 qn7) {
            super(qn7);
            this.this$0 = oq4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.MigrationManager$upgrade$2", f = "MigrationManager.kt", l = {105, 110, 114, 122, 124, 125}, m = "invokeSuspend")
    public static final class l extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ oq4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.MigrationManager$upgrade$2$1", f = "MigrationManager.kt", l = {136}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            public a(qn7 qn7) {
                super(2, qn7);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    p37 a2 = p37.h.a();
                    this.L$0 = iv7;
                    this.label = 1;
                    if (a2.d(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(oq4 oq4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = oq4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            l lVar = new l(this.this$0, qn7);
            lVar.p$ = (iv7) obj;
            throw null;
            //return lVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
            throw null;
            //return ((l) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:10:0x00e5  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x00fe  */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x0155  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x01f4  */
        /* JADX WARNING: Removed duplicated region for block: B:60:0x027e  */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x02c2  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x02c5  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r16) {
            /*
            // Method dump skipped, instructions count: 736
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.oq4.l.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = oq4.class.getSimpleName();
        pq7.b(simpleName, "MigrationManager::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public oq4(on5 on5, UserRepository userRepository, NotificationsRepository notificationsRepository, yy6 yy6, PortfolioApp portfolioApp, GoalTrackingRepository goalTrackingRepository, DeviceDao deviceDao, HybridCustomizeDatabase hybridCustomizeDatabase, MicroAppLastSettingRepository microAppLastSettingRepository, mj5 mj5, pr4 pr4, DianaPresetRepository dianaPresetRepository, WatchFaceRepository watchFaceRepository, FileRepository fileRepository, k97 k97, uo5 uo5, s77 s77, DeviceRepository deviceRepository, DianaAppSettingRepository dianaAppSettingRepository, DianaWatchFaceRepository dianaWatchFaceRepository) {
        pq7.c(on5, "mSharedPrefs");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(notificationsRepository, "mNotificationRepository");
        pq7.c(yy6, "mGetHybridDeviceSettingUseCase");
        pq7.c(portfolioApp, "mApp");
        pq7.c(goalTrackingRepository, "mGoalTrackingRepository");
        pq7.c(deviceDao, "mDeviceDao");
        pq7.c(hybridCustomizeDatabase, "mHybridCustomizeDatabase");
        pq7.c(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        pq7.c(mj5, "mDeviceSettingFactory");
        pq7.c(pr4, "flagRepository");
        pq7.c(dianaPresetRepository, "oldPresetRepository");
        pq7.c(watchFaceRepository, "oldWatchFaceRepository");
        pq7.c(fileRepository, "fileRepository");
        pq7.c(k97, "newPhotoWFBackgroundPhotoRepository");
        pq7.c(uo5, "newPresetRepository");
        pq7.c(s77, "wfAssetRepository");
        pq7.c(deviceRepository, "deviceRepository");
        pq7.c(dianaAppSettingRepository, "dianaAppSettingRepository");
        pq7.c(dianaWatchFaceRepository, "dianaWatchFaceRepository");
        this.b = on5;
        this.c = userRepository;
        this.d = notificationsRepository;
        this.e = yy6;
        this.f = portfolioApp;
        this.g = goalTrackingRepository;
        this.h = deviceDao;
        this.i = hybridCustomizeDatabase;
        this.j = microAppLastSettingRepository;
        this.k = pr4;
        this.l = dianaPresetRepository;
        this.m = watchFaceRepository;
        this.n = fileRepository;
        this.o = k97;
        this.p = uo5;
        this.q = s77;
        this.r = deviceRepository;
        this.s = dianaAppSettingRepository;
        this.t = dianaWatchFaceRepository;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0135, code lost:
        if (r0 == null) goto L_0x0137;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x014a, code lost:
        if (r0 != null) goto L_0x00ce;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.portfolio.platform.data.model.room.microapp.HybridPreset j(java.lang.String r13, java.lang.String r14, com.portfolio.platform.data.legacy.threedotzero.MappingSet r15, com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting r16) {
        /*
        // Method dump skipped, instructions count: 408
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.oq4.j(java.lang.String, java.lang.String, com.portfolio.platform.data.legacy.threedotzero.MappingSet, com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting):com.portfolio.platform.data.model.room.microapp.HybridPreset");
    }

    @DexIgnore
    public final HybridPreset k(List<? extends MicroApp> list, String str, String str2, ActivePreset activePreset, HashMap<String, String> hashMap) {
        T t2;
        FLogger.INSTANCE.getLocal().d(u, "convert " + activePreset);
        ArrayList arrayList = new ArrayList();
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "Calendar.getInstance()");
        String w0 = lk5.w0(instance.getTime());
        for (ButtonMapping buttonMapping : activePreset.getButtonMappingList()) {
            String component1 = buttonMapping.component1();
            String component2 = buttonMapping.component2();
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                T next = it.next();
                if (pq7.a(next.getAppId(), component2)) {
                    t2 = next;
                    break;
                }
            }
            if (t2 == null) {
                FLogger.INSTANCE.getLocal().d(u, "this preset is not support for " + str2 + " due to " + component2);
                return null;
            }
            String str3 = hashMap.get(component2);
            pq7.b(w0, "localUpdatedAt");
            HybridPresetAppSetting hybridPresetAppSetting = new HybridPresetAppSetting(component1, component2, w0);
            if (str3 != null) {
                hybridPresetAppSetting.setSettings(str3);
            }
            arrayList.add(hybridPresetAppSetting);
        }
        HybridPreset hybridPreset = new HybridPreset(str + ':' + str2, "Active Preset", str2, arrayList, true);
        FLogger.INSTANCE.getLocal().d(u, "legacy active createdAt " + activePreset.getCreateAt() + " updatedAt " + activePreset.getUpdateAt());
        hybridPreset.setPinType(activePreset.getPinType());
        long j2 = (long) 1000;
        hybridPreset.setCreatedAt(lk5.w0(new Date(activePreset.getCreateAt() * j2)));
        hybridPreset.setUpdatedAt(lk5.w0(new Date(activePreset.getUpdateAt() * j2)));
        FLogger.INSTANCE.getLocal().d(u, "active preset createdAt " + hybridPreset.getCreatedAt() + " updatedAt " + hybridPreset.getUpdatedAt());
        return hybridPreset;
    }

    @DexIgnore
    public final HybridPreset l(String str, RecommendedPreset recommendedPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = u;
        StringBuilder sb = new StringBuilder();
        sb.append("convertRecommendPresetToHybridPreset ");
        sb.append(recommendedPreset != null ? recommendedPreset.getName() : null);
        local.d(str2, sb.toString());
        if (recommendedPreset == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "Calendar.getInstance()");
        String w0 = lk5.w0(instance.getTime());
        List<ButtonMapping> buttonMappingList = recommendedPreset.getButtonMappingList();
        pq7.b(buttonMappingList, "recommendedPreset.buttonMappingList");
        for (T t2 : buttonMappingList) {
            String component1 = t2.component1();
            String component2 = t2.component2();
            pq7.b(w0, "localUpdatedAt");
            arrayList.add(new HybridPresetAppSetting(component1, component2, w0));
        }
        HybridPreset hybridPreset = new HybridPreset(str + ':' + recommendedPreset.getId(), recommendedPreset.getName(), str, new ArrayList(), false);
        hybridPreset.setPinType(0);
        long j2 = (long) 1000;
        hybridPreset.setCreatedAt(lk5.w0(new Date(recommendedPreset.getCreateAt() * j2)));
        hybridPreset.setUpdatedAt(lk5.w0(new Date(recommendedPreset.getUpdateAt() * j2)));
        return hybridPreset;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x015c, code lost:
        if (r0 == null) goto L_0x015e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0192, code lost:
        if (r0 != null) goto L_0x00d5;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.portfolio.platform.data.model.room.microapp.HybridPreset m(java.lang.String r13, com.portfolio.platform.data.legacy.threedotzero.MappingSet r14, com.portfolio.platform.data.legacy.threedotzero.DeviceProvider r15, com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting r16) {
        /*
        // Method dump skipped, instructions count: 487
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.oq4.m(java.lang.String, com.portfolio.platform.data.legacy.threedotzero.MappingSet, com.portfolio.platform.data.legacy.threedotzero.DeviceProvider, com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting):com.portfolio.platform.data.model.room.microapp.HybridPreset");
    }

    @DexIgnore
    public final HybridPreset n(List<? extends MicroApp> list, String str, SavedPreset savedPreset, HashMap<String, String> hashMap) {
        T t2;
        FLogger.INSTANCE.getLocal().d(u, "convert " + savedPreset + " microAppList " + list.size());
        ArrayList arrayList = new ArrayList();
        Calendar instance = Calendar.getInstance();
        pq7.b(instance, "Calendar.getInstance()");
        String w0 = lk5.w0(instance.getTime());
        for (ButtonMapping buttonMapping : savedPreset.getButtonMappingList()) {
            String component1 = buttonMapping.component1();
            String component2 = buttonMapping.component2();
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                T next = it.next();
                if (pq7.a(next.getAppId(), component2)) {
                    t2 = next;
                    break;
                }
            }
            if (t2 == null) {
                FLogger.INSTANCE.getLocal().d(u, "this preset is not support for " + str + " due to " + component2);
                return null;
            }
            String str2 = hashMap.get(component2);
            pq7.b(w0, "localUpdatedAt");
            HybridPresetAppSetting hybridPresetAppSetting = new HybridPresetAppSetting(component1, component2, w0);
            if (str2 != null) {
                hybridPresetAppSetting.setSettings(str2);
            }
            arrayList.add(hybridPresetAppSetting);
        }
        HybridPreset hybridPreset = new HybridPreset(str + ':' + savedPreset.getId(), savedPreset.getName(), str, arrayList, false);
        FLogger.INSTANCE.getLocal().d(u, "legacy createdAt " + savedPreset.getCreateAt() + " updatedAt " + savedPreset.getUpdateAt());
        long j2 = (long) 1000;
        hybridPreset.setCreatedAt(lk5.w0(new Date(savedPreset.getCreateAt() * j2)));
        hybridPreset.setUpdatedAt(lk5.w0(new Date(savedPreset.getUpdateAt() * j2)));
        FLogger.INSTANCE.getLocal().d(u, "preset createdAt " + hybridPreset.getCreatedAt() + " updatedAt " + hybridPreset.getUpdatedAt());
        hybridPreset.setPinType(savedPreset.getPinType());
        return hybridPreset;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x013a  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x015a  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x016d  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01c9  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x01da  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x01f3  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object o(com.fossil.qn7<? super com.fossil.tl7> r14) {
        /*
        // Method dump skipped, instructions count: 513
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.oq4.o(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:128:0x05d4  */
    /* JADX WARNING: Removed duplicated region for block: B:179:0x072b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x07be  */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x081e  */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x0928  */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x092d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01d2  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0219  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x038b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object p(com.fossil.qn7<? super com.fossil.tl7> r23) {
        /*
        // Method dump skipped, instructions count: 2360
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.oq4.p(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x022c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object q(java.lang.String r23, com.fossil.qn7<? super com.fossil.tl7> r24) {
        /*
        // Method dump skipped, instructions count: 1422
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.oq4.q(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object r(java.lang.String r19, com.fossil.qn7<? super com.fossil.tl7> r20) {
        /*
        // Method dump skipped, instructions count: 357
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.oq4.r(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0110  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0131  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0141  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0148  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x015f  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01fa  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object s(com.fossil.qn7<? super java.lang.Integer> r16) {
        /*
        // Method dump skipped, instructions count: 528
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.oq4.s(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v169, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r6v19, types: [java.util.List] */
    /* JADX WARN: Type inference failed for: r7v14, types: [java.util.List] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x037e  */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x043f  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x044a  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0461  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x047e  */
    /* JADX WARNING: Removed duplicated region for block: B:141:0x04a5  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x04b0  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x0510  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x051f  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x0529  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x0535  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0541  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x054c  */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x0557  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0562  */
    /* JADX WARNING: Removed duplicated region for block: B:181:0x057d  */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x0598  */
    /* JADX WARNING: Removed duplicated region for block: B:187:0x05b3  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:190:0x05ce  */
    /* JADX WARNING: Removed duplicated region for block: B:227:0x0708  */
    /* JADX WARNING: Removed duplicated region for block: B:235:0x05d2 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:242:0x033b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:258:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0198  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0250  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x02bb  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0302  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x030f  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0318  */
    /* JADX WARNING: Unknown variable types count: 3 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object t(java.lang.String r34, com.fossil.qn7<? super com.fossil.tl7> r35) {
        /*
        // Method dump skipped, instructions count: 1868
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.oq4.t(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object u(java.lang.String r11, com.fossil.qn7<? super com.fossil.tl7> r12) {
        /*
        // Method dump skipped, instructions count: 312
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.oq4.u(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void v() {
        this.b.w1(System.currentTimeMillis());
        this.b.U0(0);
        this.b.H1(true);
    }

    @DexIgnore
    public void w() {
        List<DeviceModel> allDevice = mn5.p.a().e().getAllDevice();
        String J = this.f.J();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "migrationDeviceData - allDevices=" + allDevice + ", activeSerial=" + J);
        try {
            boolean z = !TextUtils.isEmpty(J) && !nk5.o.v(J);
            if (z) {
                this.b.E0("");
                this.b.p1(true);
            }
            if (!(allDevice == null || allDevice.isEmpty())) {
                for (DeviceModel deviceModel : allDevice) {
                    if (deviceModel != null && !TextUtils.isEmpty(deviceModel.getDeviceId())) {
                        String deviceId = deviceModel.getDeviceId();
                        String macAddress = deviceModel.getMacAddress();
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = u;
                        local2.d(str2, "migrationDeviceData - step1: deviceId=" + deviceId + ", deviceAddress=" + macAddress);
                        nk5.a aVar = nk5.o;
                        String deviceId2 = deviceModel.getDeviceId();
                        if (deviceId2 == null) {
                            pq7.i();
                            throw null;
                        } else if (!aVar.v(deviceId2)) {
                            mn5.p.a().e().removeDevice(deviceId);
                        }
                    }
                }
                if (z) {
                    try {
                        IButtonConnectivity b2 = PortfolioApp.h0.b();
                        if (b2 != null) {
                            b2.removeActiveSerial(J);
                        } else {
                            pq7.i();
                            throw null;
                        }
                    } catch (Exception e2) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str3 = u;
                        local3.d(str3, "Exception when remove legacy device in button service ,keep going e=" + e2);
                    }
                }
                for (DeviceModel deviceModel2 : allDevice) {
                    if (deviceModel2 != null && !TextUtils.isEmpty(deviceModel2.getDeviceId())) {
                        String deviceId3 = deviceModel2.getDeviceId();
                        String macAddress2 = deviceModel2.getMacAddress();
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String str4 = u;
                        local4.d(str4, "migrationDeviceData - step2: - deviceId=" + deviceId3 + ", deviceAddress=" + macAddress2);
                        nk5.a aVar2 = nk5.o;
                        String deviceId4 = deviceModel2.getDeviceId();
                        if (deviceId4 == null) {
                            pq7.i();
                            throw null;
                        } else if (aVar2.v(deviceId4)) {
                            if (vt7.j(deviceModel2.getDeviceId(), J, true)) {
                                PortfolioApp portfolioApp = this.f;
                                String deviceId5 = deviceModel2.getDeviceId();
                                if (deviceId5 != null) {
                                    portfolioApp.n1(deviceId5, deviceModel2.getMacAddress());
                                } else {
                                    pq7.i();
                                    throw null;
                                }
                            }
                            IButtonConnectivity b3 = PortfolioApp.h0.b();
                            if (b3 != null) {
                                b3.setPairedSerial(deviceModel2.getDeviceId(), deviceModel2.getMacAddress());
                            } else {
                                pq7.i();
                                throw null;
                            }
                        } else {
                            IButtonConnectivity b4 = PortfolioApp.h0.b();
                            if (b4 != null) {
                                b4.removePairedSerial(deviceModel2.getDeviceId());
                            } else {
                                pq7.i();
                                throw null;
                            }
                        }
                    }
                }
                this.b.q1(false);
            }
        } catch (Exception e3) {
            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            String str5 = u;
            local5.e(str5, "migrationDeviceData - Exception when upgrade to 1.14.0 version e=" + e3);
            e3.printStackTrace();
            this.b.q1(true);
        }
    }

    @DexIgnore
    public Object x(qn7<? super Boolean> qn7) {
        return eu7.g(bw7.b(), new l(this, null), qn7);
    }
}
