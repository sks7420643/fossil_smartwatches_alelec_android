package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.J32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class G32 implements J32.Bi {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public G32(String str) {
        this.a = str;
    }

    @DexIgnore
    public static J32.Bi a(String str) {
        return new G32(str);
    }

    @DexIgnore
    @Override // com.fossil.J32.Bi
    public Object apply(Object obj) {
        return J32.b0(this.a, (SQLiteDatabase) obj);
    }
}
