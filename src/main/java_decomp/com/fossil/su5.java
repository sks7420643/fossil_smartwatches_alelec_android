package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class su5 extends iq4<a, iq4.d, iq4.a> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ SummariesRepository d;
    @DexIgnore
    public /* final */ FitnessDataRepository e;
    @DexIgnore
    public /* final */ UserRepository f;
    @DexIgnore
    public /* final */ ActivitiesRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Date f3309a;

        @DexIgnore
        public a(Date date) {
            pq7.c(date, "date");
            this.f3309a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.f3309a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries", f = "FetchSummaries.kt", l = {29, 57, 58, 61}, m = "run")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ su5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(su5 su5, qn7 qn7) {
            super(qn7);
            this.this$0 = su5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    /*
    static {
        String simpleName = su5.class.getSimpleName();
        pq7.b(simpleName, "FetchSummaries::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public su5(SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, ActivitiesRepository activitiesRepository) {
        pq7.c(summariesRepository, "mSummariesRepository");
        pq7.c(fitnessDataRepository, "mFitnessDataRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(activitiesRepository, "mActivitiesRepository");
        this.d = summariesRepository;
        this.e = fitnessDataRepository;
        this.f = userRepository;
        this.g = activitiesRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return h;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x025f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.su5.a r23, com.fossil.qn7<? super com.fossil.tl7> r24) {
        /*
        // Method dump skipped, instructions count: 627
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.su5.k(com.fossil.su5$a, com.fossil.qn7):java.lang.Object");
    }
}
