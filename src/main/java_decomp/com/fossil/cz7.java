package com.fossil;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Cz7<T> extends Rz7 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(Cz7.class, Object.class, "_consensus");
    @DexIgnore
    public volatile Object _consensus; // = Bz7.a;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.Cz7<T> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.Rz7
    public Cz7<?> a() {
        return this;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.Rz7
    public final Object c(Object obj) {
        Object obj2 = this._consensus;
        if (obj2 == Bz7.a) {
            obj2 = e(g(obj));
        }
        d(obj, obj2);
        return obj2;
    }

    @DexIgnore
    public abstract void d(T t, Object obj);

    @DexIgnore
    public final Object e(Object obj) {
        if (Nv7.a()) {
            if (!(obj != Bz7.a)) {
                throw new AssertionError();
            }
        }
        Object obj2 = this._consensus;
        return obj2 != Bz7.a ? obj2 : !a.compareAndSet(this, Bz7.a, obj) ? this._consensus : obj;
    }

    @DexIgnore
    public long f() {
        return 0;
    }

    @DexIgnore
    public abstract Object g(T t);
}
