package com.fossil;

import com.mapped.An4;
import com.mapped.Cj4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.ui.profile.usecase.GetZendeskInformation;
import com.portfolio.platform.uirenew.home.HomePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kz5 implements Factory<HomePresenter> {
    @DexIgnore
    public static HomePresenter a(Zy5 zy5, An4 an4, DeviceRepository deviceRepository, PortfolioApp portfolioApp, UpdateFirmwareUsecase updateFirmwareUsecase, Uq4 uq4, Cj4 cj4, Bv5 bv5, N47 n47, ServerSettingRepository serverSettingRepository, GetZendeskInformation getZendeskInformation, FlagRepository flagRepository) {
        return new HomePresenter(zy5, an4, deviceRepository, portfolioApp, updateFirmwareUsecase, uq4, cj4, bv5, n47, serverSettingRepository, getZendeskInformation, flagRepository);
    }
}
