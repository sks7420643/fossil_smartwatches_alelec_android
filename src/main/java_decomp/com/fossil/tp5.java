package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.receiver.AlarmReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tp5 implements MembersInjector<AlarmReceiver> {
    @DexIgnore
    public static void a(AlarmReceiver alarmReceiver, AlarmHelper alarmHelper) {
        alarmReceiver.d = alarmHelper;
    }

    @DexIgnore
    public static void b(AlarmReceiver alarmReceiver, AlarmsRepository alarmsRepository) {
        alarmReceiver.e = alarmsRepository;
    }

    @DexIgnore
    public static void c(AlarmReceiver alarmReceiver, DeviceRepository deviceRepository) {
        alarmReceiver.c = deviceRepository;
    }

    @DexIgnore
    public static void d(AlarmReceiver alarmReceiver, An4 an4) {
        alarmReceiver.b = an4;
    }

    @DexIgnore
    public static void e(AlarmReceiver alarmReceiver, UserRepository userRepository) {
        alarmReceiver.a = userRepository;
    }
}
