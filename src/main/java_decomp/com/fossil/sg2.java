package com.fossil;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Sg2 {
    @DexIgnore
    Object o();  // void declaration

    @DexIgnore
    void onCreate(Bundle bundle);

    @DexIgnore
    Object onDestroy();  // void declaration

    @DexIgnore
    Object onLowMemory();  // void declaration

    @DexIgnore
    Object onPause();  // void declaration

    @DexIgnore
    Object onResume();  // void declaration

    @DexIgnore
    void onSaveInstanceState(Bundle bundle);

    @DexIgnore
    Object onStart();  // void declaration

    @DexIgnore
    Object onStop();  // void declaration

    @DexIgnore
    void p(Activity activity, Bundle bundle, Bundle bundle2);

    @DexIgnore
    View q(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle);
}
