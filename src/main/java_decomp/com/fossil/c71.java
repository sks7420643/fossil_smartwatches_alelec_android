package com.fossil;

import android.graphics.Bitmap;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface C71 {
    @DexIgnore
    public static final Ai a = Ai.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ Ai a; // = new Ai();

        @DexIgnore
        public final C71 a(S61 s61, int i) {
            Wg6.c(s61, "referenceCounter");
            return i > 0 ? new F71(s61, i) : U61.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public Bi(Bitmap bitmap, boolean z, int i) {
            Wg6.c(bitmap, "bitmap");
            this.a = bitmap;
            this.b = z;
            this.c = i;
        }

        @DexIgnore
        public final Bitmap a() {
            return this.a;
        }

        @DexIgnore
        public final int b() {
            return this.c;
        }

        @DexIgnore
        public final boolean c() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Bi) {
                    Bi bi = (Bi) obj;
                    if (!(Wg6.a(this.a, bi.a) && this.b == bi.b && this.c == bi.c)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            Bitmap bitmap = this.a;
            int hashCode = bitmap != null ? bitmap.hashCode() : 0;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return (((hashCode * 31) + i) * 31) + this.c;
        }

        @DexIgnore
        public String toString() {
            return "Value(bitmap=" + this.a + ", isSampled=" + this.b + ", size=" + this.c + ")";
        }
    }

    @DexIgnore
    void a(int i);

    @DexIgnore
    Bi b(String str);

    @DexIgnore
    void c(String str, Bitmap bitmap, boolean z);
}
