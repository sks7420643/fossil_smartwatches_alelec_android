package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sc4 {
    @DexIgnore
    public /* final */ B94 a;

    @DexIgnore
    public Sc4(B94 b94) {
        this.a = b94;
    }

    @DexIgnore
    public static Tc4 a(int i) {
        return i != 3 ? new Oc4() : new Uc4();
    }

    @DexIgnore
    public Ad4 b(JSONObject jSONObject) throws JSONException {
        return a(jSONObject.getInt("settings_version")).a(this.a, jSONObject);
    }
}
