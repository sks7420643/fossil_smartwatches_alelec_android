package com.fossil;

import com.mapped.Hg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Nt7 {
    @DexIgnore
    public static final <T> void a(Appendable appendable, T t, Hg6<? super T, ? extends CharSequence> hg6) {
        Wg6.c(appendable, "$this$appendElement");
        if (hg6 != null) {
            appendable.append((CharSequence) hg6.invoke(t));
            return;
        }
        if (t != null ? t instanceof CharSequence : true) {
            appendable.append(t);
        } else if (t instanceof Character) {
            appendable.append(t.charValue());
        } else {
            appendable.append(String.valueOf(t));
        }
    }
}
