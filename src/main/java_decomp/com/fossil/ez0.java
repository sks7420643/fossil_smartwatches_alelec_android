package com.fossil;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ez0 implements Gz0 {
    @DexIgnore
    public Ai a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends ViewGroup {
        @DexIgnore
        public ViewGroup b;
        @DexIgnore
        public View c;
        @DexIgnore
        public ArrayList<Drawable> d; // = null;
        @DexIgnore
        public Ez0 e;
        @DexIgnore
        public boolean f;

        /*
        static {
            try {
                ViewGroup.class.getDeclaredMethod("invalidateChildInParentFast", Integer.TYPE, Integer.TYPE, Rect.class);
            } catch (NoSuchMethodException e2) {
            }
        }
        */

        @DexIgnore
        public Ai(Context context, ViewGroup viewGroup, View view, Ez0 ez0) {
            super(context);
            this.b = viewGroup;
            this.c = view;
            setRight(viewGroup.getWidth());
            setBottom(viewGroup.getHeight());
            viewGroup.addView(this);
            this.e = ez0;
        }

        @DexIgnore
        public void a(Drawable drawable) {
            c();
            if (this.d == null) {
                this.d = new ArrayList<>();
            }
            if (!this.d.contains(drawable)) {
                this.d.add(drawable);
                invalidate(drawable.getBounds());
                drawable.setCallback(this);
            }
        }

        @DexIgnore
        public void b(View view) {
            c();
            if (view.getParent() instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view.getParent();
                if (!(viewGroup == this.b || viewGroup.getParent() == null || !Mo0.P(viewGroup))) {
                    int[] iArr = new int[2];
                    int[] iArr2 = new int[2];
                    viewGroup.getLocationOnScreen(iArr);
                    this.b.getLocationOnScreen(iArr2);
                    Mo0.V(view, iArr[0] - iArr2[0]);
                    Mo0.W(view, iArr[1] - iArr2[1]);
                }
                viewGroup.removeView(view);
                if (view.getParent() != null) {
                    viewGroup.removeView(view);
                }
            }
            super.addView(view);
        }

        @DexIgnore
        public final void c() {
            if (this.f) {
                throw new IllegalStateException("This overlay was disposed already. Please use a new one via ViewGroupUtils.getOverlay()");
            }
        }

        @DexIgnore
        public final void d() {
            if (getChildCount() == 0) {
                ArrayList<Drawable> arrayList = this.d;
                if (arrayList == null || arrayList.size() == 0) {
                    this.f = true;
                    this.b.removeView(this);
                }
            }
        }

        @DexIgnore
        public void dispatchDraw(Canvas canvas) {
            int i;
            int i2;
            int[] iArr = new int[2];
            int[] iArr2 = new int[2];
            this.b.getLocationOnScreen(iArr);
            this.c.getLocationOnScreen(iArr2);
            canvas.translate((float) (iArr2[0] - iArr[0]), (float) (iArr2[1] - iArr[1]));
            canvas.clipRect(new Rect(0, 0, this.c.getWidth(), this.c.getHeight()));
            super.dispatchDraw(canvas);
            ArrayList<Drawable> arrayList = this.d;
            if (arrayList == null) {
                i2 = 0;
                i = 0;
            } else {
                i2 = arrayList.size();
                i = 0;
            }
            while (i < i2) {
                this.d.get(i).draw(canvas);
                i++;
            }
        }

        @DexIgnore
        public boolean dispatchTouchEvent(MotionEvent motionEvent) {
            return false;
        }

        @DexIgnore
        public final void e(int[] iArr) {
            int[] iArr2 = new int[2];
            int[] iArr3 = new int[2];
            this.b.getLocationOnScreen(iArr2);
            this.c.getLocationOnScreen(iArr3);
            iArr[0] = iArr3[0] - iArr2[0];
            iArr[1] = iArr3[1] - iArr2[1];
        }

        @DexIgnore
        public void f(Drawable drawable) {
            ArrayList<Drawable> arrayList = this.d;
            if (arrayList != null) {
                arrayList.remove(drawable);
                invalidate(drawable.getBounds());
                drawable.setCallback(null);
                d();
            }
        }

        @DexIgnore
        public void g(View view) {
            super.removeView(view);
            d();
        }

        @DexIgnore
        public ViewParent invalidateChildInParent(int[] iArr, Rect rect) {
            if (this.b != null) {
                rect.offset(iArr[0], iArr[1]);
                if (this.b instanceof ViewGroup) {
                    iArr[0] = 0;
                    iArr[1] = 0;
                    int[] iArr2 = new int[2];
                    e(iArr2);
                    rect.offset(iArr2[0], iArr2[1]);
                    return super.invalidateChildInParent(iArr, rect);
                }
                invalidate(rect);
            }
            return null;
        }

        @DexIgnore
        public void invalidateDrawable(Drawable drawable) {
            invalidate(drawable.getBounds());
        }

        @DexIgnore
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        }

        @DexIgnore
        public boolean verifyDrawable(Drawable drawable) {
            ArrayList<Drawable> arrayList;
            return super.verifyDrawable(drawable) || ((arrayList = this.d) != null && arrayList.contains(drawable));
        }
    }

    @DexIgnore
    public Ez0(Context context, ViewGroup viewGroup, View view) {
        this.a = new Ai(context, viewGroup, view, this);
    }

    @DexIgnore
    public static Ez0 e(View view) {
        ViewGroup f = f(view);
        if (f == null) {
            return null;
        }
        int childCount = f.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = f.getChildAt(i);
            if (childAt instanceof Ai) {
                return ((Ai) childAt).e;
            }
        }
        return new Zy0(f.getContext(), f, view);
    }

    @DexIgnore
    public static ViewGroup f(View view) {
        View view2 = view;
        while (view2 != null) {
            if (view2.getId() == 16908290 && (view2 instanceof ViewGroup)) {
                return (ViewGroup) view2;
            }
            if (view2.getParent() instanceof ViewGroup) {
                view2 = (ViewGroup) view2.getParent();
            }
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Gz0
    public void a(Drawable drawable) {
        this.a.a(drawable);
    }

    @DexIgnore
    @Override // com.fossil.Gz0
    public void b(Drawable drawable) {
        this.a.f(drawable);
    }
}
