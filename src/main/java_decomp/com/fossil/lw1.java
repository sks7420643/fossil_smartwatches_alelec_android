package com.fossil;

import android.graphics.Bitmap;
import android.os.Parcel;
import com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor;
import com.fossil.imagefilters.EInkImageFactory;
import com.fossil.imagefilters.Format;
import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Lw1 extends Iw1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public byte[] l;
    @DexIgnore
    public /* final */ Ec0 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Lw1 a(byte[] bArr) {
            try {
                Iw1 iw1 = (Iw1) Ga.d.f(bArr);
                if (iw1 instanceof Lw1) {
                    return (Lw1) iw1;
                }
            } catch (IllegalArgumentException e) {
            }
            return null;
        }
    }

    @DexIgnore
    public Lw1(Parcel parcel) {
        super(parcel);
        Cc0 cc0;
        byte[] bArr;
        Cc0[] c = c();
        int length = c.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                cc0 = null;
                break;
            }
            cc0 = c[i];
            if (Wg6.a(cc0.b, "theme_class")) {
                break;
            }
            i++;
        }
        if (!(cc0 == null || (bArr = cc0.c) == null)) {
            try {
                Ec0 a2 = Ec0.g.a(new String(Dm7.k(bArr, 0, bArr.length - 1), Hd0.y.c()));
                if (a2 != null) {
                    this.m = a2;
                    k();
                    return;
                }
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        }
        throw new IllegalArgumentException("Invalid Theme Classifier");
    }

    @DexIgnore
    public Lw1(Ry1 ry1, Yb0 yb0, Cc0[] cc0Arr, Cc0[] cc0Arr2, Cc0[] cc0Arr3, Cc0[] cc0Arr4, Cc0[] cc0Arr5, Cc0[] cc0Arr6, Cc0[] cc0Arr7) throws IllegalArgumentException {
        super(ry1, yb0, cc0Arr, cc0Arr2, cc0Arr3, cc0Arr4, cc0Arr5, cc0Arr6, cc0Arr7);
        Cc0 cc0;
        byte[] bArr;
        Cc0[] c = c();
        int length = c.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                cc0 = null;
                break;
            }
            cc0 = c[i];
            if (Wg6.a(cc0.b, "theme_class")) {
                break;
            }
            i++;
        }
        if (!(cc0 == null || (bArr = cc0.c) == null)) {
            try {
                Ec0 a2 = Ec0.g.a(new String(Dm7.k(bArr, 0, bArr.length - 1), Hd0.y.c()));
                if (a2 != null) {
                    this.m = a2;
                    k();
                    return;
                }
            } catch (Exception e) {
                throw new IllegalArgumentException(e);
            }
        }
        throw new IllegalArgumentException("Invalid Theme Classifier");
    }

    @DexIgnore
    @Override // com.fossil.Iw1, com.fossil.Iw1, java.lang.Object
    public abstract /* synthetic */ Nx1 clone();

    @DexIgnore
    public ThemeEditor edit() {
        return null;
    }

    @DexIgnore
    public final byte[] getPreviewImageData() {
        Cc0 cc0;
        byte[] bArr = null;
        if (this.l == null) {
            Cc0[] b = b();
            int length = b.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    cc0 = null;
                    break;
                }
                cc0 = b[i];
                if (Wg6.a(cc0.b, "!preview.rle")) {
                    break;
                }
                i++;
            }
            if (cc0 != null) {
                Bitmap decode = EInkImageFactory.decode(cc0.c, Format.RLE);
                Wg6.b(decode, "EInkImageFactory.decode(\u2026ageNode.data, Format.RLE)");
                bArr = Cy1.b(decode, null, 1, null);
                decode.recycle();
            }
            this.l = bArr;
        }
        return this.l;
    }

    @DexIgnore
    public final Ec0 getThemeClassifier() {
        return this.m;
    }

    @DexIgnore
    public final Lw1 j() {
        Cc0[] f = f();
        Cc0 cc0 = (Cc0) Em7.L(f, 0);
        if (cc0 != null) {
            f[0] = new Cc0("previewWatchFace", cc0.c);
        }
        int i = Fc0.a[this.m.ordinal()];
        if (i == 1) {
            return new Qw1(h(), g(), f, b(), d(), e(), c(), a(), i());
        }
        if (i == 2) {
            return new Ow1(h(), g(), f, b(), d(), e(), c(), a(), i());
        }
        if (i == 3) {
            return new Tw1(h(), g(), f, b(), d(), e(), c(), a(), i());
        }
        throw new Kc6();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0016, code lost:
        if ((!(f().length == 0)) != false) goto L_0x0018;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void k() {
        /*
            r4 = this;
            r0 = 1
            r1 = 0
            com.fossil.Yb0 r2 = r4.g()
            com.fossil.Jw1 r2 = r2.b
            com.fossil.Jw1 r3 = com.fossil.Jw1.THEME
            if (r2 != r3) goto L_0x0029
            com.fossil.Cc0[] r2 = r4.f()
            int r2 = r2.length
            if (r2 != 0) goto L_0x001b
            r2 = r0
        L_0x0014:
            r2 = r2 ^ 1
            if (r2 == 0) goto L_0x0029
        L_0x0018:
            if (r0 == 0) goto L_0x001d
            return
        L_0x001b:
            r2 = r1
            goto L_0x0014
        L_0x001d:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Incorrect package type."
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0029:
            r0 = r1
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Lw1.k():void");
    }

    @DexIgnore
    @Override // com.fossil.Iw1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(new JSONObject(), Jd0.U0, Hy1.k((int) getPackageCrc(), null, 1, null)), Jd0.M4, getBundleId());
    }
}
