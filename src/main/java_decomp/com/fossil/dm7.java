package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import com.facebook.share.internal.ShareConstants;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dm7 extends Cm7 {
    @DexIgnore
    public static final <T> List<T> d(T[] tArr) {
        Wg6.c(tArr, "$this$asList");
        List<T> a2 = Fm7.a(tArr);
        Wg6.b(a2, "ArraysUtilJVM.asList(this)");
        return a2;
    }

    @DexIgnore
    public static final int e(int[] iArr, int i, int i2, int i3) {
        Wg6.c(iArr, "$this$binarySearch");
        return Arrays.binarySearch(iArr, i2, i3, i);
    }

    @DexIgnore
    public static /* synthetic */ int f(int[] iArr, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i2 = 0;
        }
        if ((i4 & 4) != 0) {
            i3 = iArr.length;
        }
        return e(iArr, i, i2, i3);
    }

    @DexIgnore
    public static final byte[] g(byte[] bArr, byte[] bArr2, int i, int i2, int i3) {
        Wg6.c(bArr, "$this$copyInto");
        Wg6.c(bArr2, ShareConstants.DESTINATION);
        System.arraycopy(bArr, i2, bArr2, i, i3 - i2);
        return bArr2;
    }

    @DexIgnore
    public static final <T> T[] h(T[] tArr, T[] tArr2, int i, int i2, int i3) {
        Wg6.c(tArr, "$this$copyInto");
        Wg6.c(tArr2, ShareConstants.DESTINATION);
        System.arraycopy(tArr, i2, tArr2, i, i3 - i2);
        return tArr2;
    }

    @DexIgnore
    public static /* synthetic */ byte[] i(byte[] bArr, byte[] bArr2, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i = 0;
        }
        if ((i4 & 4) != 0) {
            i2 = 0;
        }
        if ((i4 & 8) != 0) {
            i3 = bArr.length;
        }
        g(bArr, bArr2, i, i2, i3);
        return bArr2;
    }

    @DexIgnore
    public static /* synthetic */ Object[] j(Object[] objArr, Object[] objArr2, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i = 0;
        }
        if ((i4 & 4) != 0) {
            i2 = 0;
        }
        if ((i4 & 8) != 0) {
            i3 = objArr.length;
        }
        h(objArr, objArr2, i, i2, i3);
        return objArr2;
    }

    @DexIgnore
    public static final byte[] k(byte[] bArr, int i, int i2) {
        Wg6.c(bArr, "$this$copyOfRangeImpl");
        Bm7.b(i2, bArr.length);
        byte[] copyOfRange = Arrays.copyOfRange(bArr, i, i2);
        Wg6.b(copyOfRange, "java.util.Arrays.copyOfR\u2026this, fromIndex, toIndex)");
        return copyOfRange;
    }

    @DexIgnore
    public static final <T> T[] l(T[] tArr, int i, int i2) {
        Wg6.c(tArr, "$this$copyOfRangeImpl");
        Bm7.b(i2, tArr.length);
        T[] tArr2 = (T[]) Arrays.copyOfRange(tArr, i, i2);
        Wg6.b(tArr2, "java.util.Arrays.copyOfR\u2026this, fromIndex, toIndex)");
        return tArr2;
    }

    @DexIgnore
    public static final <T> void m(T[] tArr, T t, int i, int i2) {
        Wg6.c(tArr, "$this$fill");
        Arrays.fill(tArr, i, i2, t);
    }

    @DexIgnore
    public static final <R> List<R> n(Object[] objArr, Class<R> cls) {
        Wg6.c(objArr, "$this$filterIsInstance");
        Wg6.c(cls, "klass");
        ArrayList arrayList = new ArrayList();
        o(objArr, arrayList, cls);
        return arrayList;
    }

    @DexIgnore
    public static final <C extends Collection<? super R>, R> C o(Object[] objArr, C c, Class<R> cls) {
        Wg6.c(objArr, "$this$filterIsInstanceTo");
        Wg6.c(c, ShareConstants.DESTINATION);
        Wg6.c(cls, "klass");
        for (Object obj : objArr) {
            if (cls.isInstance(obj)) {
                c.add(obj);
            }
        }
        return c;
    }

    @DexIgnore
    public static final byte[] p(byte[] bArr, byte b) {
        Wg6.c(bArr, "$this$plus");
        int length = bArr.length;
        byte[] copyOf = Arrays.copyOf(bArr, length + 1);
        copyOf[length] = (byte) b;
        Wg6.b(copyOf, Constants.RESULT);
        return copyOf;
    }

    @DexIgnore
    public static final byte[] q(byte[] bArr, byte[] bArr2) {
        Wg6.c(bArr, "$this$plus");
        Wg6.c(bArr2, MessengerShareContentUtility.ELEMENTS);
        int length = bArr.length;
        int length2 = bArr2.length;
        byte[] copyOf = Arrays.copyOf(bArr, length + length2);
        System.arraycopy(bArr2, 0, copyOf, length, length2);
        Wg6.b(copyOf, Constants.RESULT);
        return copyOf;
    }

    @DexIgnore
    public static final <T> T[] r(T[] tArr, T t) {
        Wg6.c(tArr, "$this$plus");
        int length = tArr.length;
        T[] tArr2 = (T[]) Arrays.copyOf(tArr, length + 1);
        tArr2[length] = t;
        Wg6.b(tArr2, Constants.RESULT);
        return tArr2;
    }

    @DexIgnore
    public static final <T> T[] s(T[] tArr, T[] tArr2) {
        Wg6.c(tArr, "$this$plus");
        Wg6.c(tArr2, MessengerShareContentUtility.ELEMENTS);
        int length = tArr.length;
        int length2 = tArr2.length;
        T[] tArr3 = (T[]) Arrays.copyOf(tArr, length + length2);
        System.arraycopy(tArr2, 0, tArr3, length, length2);
        Wg6.b(tArr3, Constants.RESULT);
        return tArr3;
    }

    @DexIgnore
    public static final void t(int[] iArr) {
        Wg6.c(iArr, "$this$sort");
        if (iArr.length > 1) {
            Arrays.sort(iArr);
        }
    }

    @DexIgnore
    public static final <T> void u(T[] tArr) {
        Wg6.c(tArr, "$this$sort");
        if (tArr.length > 1) {
            Arrays.sort(tArr);
        }
    }

    @DexIgnore
    public static final <T> void v(T[] tArr, Comparator<? super T> comparator) {
        Wg6.c(tArr, "$this$sortWith");
        Wg6.c(comparator, "comparator");
        if (tArr.length > 1) {
            Arrays.sort(tArr, comparator);
        }
    }

    @DexIgnore
    public static final Integer[] w(int[] iArr) {
        Wg6.c(iArr, "$this$toTypedArray");
        Integer[] numArr = new Integer[iArr.length];
        int length = iArr.length;
        for (int i = 0; i < length; i++) {
            numArr[i] = Integer.valueOf(iArr[i]);
        }
        return numArr;
    }
}
