package com.fossil;

import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class D58 {
    @DexIgnore
    public static /* final */ D58 d; // = new Ai();
    @DexIgnore
    public boolean a;
    @DexIgnore
    public long b;
    @DexIgnore
    public long c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends D58 {
        @DexIgnore
        @Override // com.fossil.D58
        public D58 d(long j) {
            return this;
        }

        @DexIgnore
        @Override // com.fossil.D58
        public void f() {
        }

        @DexIgnore
        @Override // com.fossil.D58
        public D58 g(long j, TimeUnit timeUnit) {
            Wg6.c(timeUnit, Constants.PROFILE_KEY_UNIT);
            return this;
        }
    }

    @DexIgnore
    public D58 a() {
        this.a = false;
        return this;
    }

    @DexIgnore
    public D58 b() {
        this.c = 0;
        return this;
    }

    @DexIgnore
    public long c() {
        if (this.a) {
            return this.b;
        }
        throw new IllegalStateException("No deadline".toString());
    }

    @DexIgnore
    public D58 d(long j) {
        this.a = true;
        this.b = j;
        return this;
    }

    @DexIgnore
    public boolean e() {
        return this.a;
    }

    @DexIgnore
    public void f() throws IOException {
        if (Thread.interrupted()) {
            Thread.currentThread().interrupt();
            throw new InterruptedIOException("interrupted");
        } else if (this.a && this.b - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }

    @DexIgnore
    public D58 g(long j, TimeUnit timeUnit) {
        Wg6.c(timeUnit, Constants.PROFILE_KEY_UNIT);
        if (j >= 0) {
            this.c = timeUnit.toNanos(j);
            return this;
        }
        throw new IllegalArgumentException(("timeout < 0: " + j).toString());
    }

    @DexIgnore
    public long h() {
        return this.c;
    }
}
