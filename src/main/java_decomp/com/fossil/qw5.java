package com.fossil;

import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Lc6;
import com.mapped.Mj6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qw5 extends RecyclerView.g<Ai> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public List<Lc6<WatchApp, String>> a;
    @DexIgnore
    public List<Lc6<WatchApp, String>> b; // = new ArrayList();
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public Di d;
    @DexIgnore
    public Ci e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Qw5 qw5, View view) {
            super(view);
            Wg6.c(view, "itemView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends Ai {
        @DexIgnore
        public /* final */ FlexibleTextView a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Qw5 qw5, View view) {
            super(qw5, view);
            Wg6.c(view, "itemView");
            View findViewById = view.findViewById(2131363383);
            if (findViewById != null) {
                this.a = (FlexibleTextView) findViewById;
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        void a(String str);
    }

    @DexIgnore
    public interface Di {
        @DexIgnore
        void a(WatchApp watchApp);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ei extends Ai {
        @DexIgnore
        public CustomizeWidget a;
        @DexIgnore
        public FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public WatchApp d;
        @DexIgnore
        public /* final */ /* synthetic */ Qw5 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ei b;

            @DexIgnore
            public Aii(Ei ei) {
                this.b = ei;
            }

            @DexIgnore
            public final void onClick(View view) {
                WatchApp a2 = this.b.a();
                if (a2 != null) {
                    Di di = this.b.e.d;
                    if (di != null) {
                        di.a(a2);
                    } else {
                        FLogger.INSTANCE.getLocal().d(Qw5.f, "itemClick(), no listener.");
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(Qw5.f, "itemClick(), watch app tag null.");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(Qw5 qw5, View view) {
            super(qw5, view);
            Wg6.c(view, "itemView");
            this.e = qw5;
            View findViewById = view.findViewById(2131363547);
            Wg6.b(findViewById, "itemView.findViewById(R.id.wc_icon)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363370);
            Wg6.b(findViewById2, "itemView.findViewById(R.id.tv_name)");
            this.b = (FlexibleTextView) findViewById2;
            this.c = (FlexibleTextView) view.findViewById(2131363263);
            view.setOnClickListener(new Aii(this));
        }

        @DexIgnore
        public final WatchApp a() {
            return this.d;
        }

        @DexIgnore
        public final FlexibleTextView b() {
            return this.c;
        }

        @DexIgnore
        public final FlexibleTextView c() {
            return this.b;
        }

        @DexIgnore
        public final CustomizeWidget d() {
            return this.a;
        }

        @DexIgnore
        public final void e(WatchApp watchApp) {
            this.d = watchApp;
        }
    }

    /*
    static {
        String name = Qw5.class.getName();
        Wg6.b(name, "WatchAppSearchAdapter::class.java.name");
        f = name;
    }
    */

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<Lc6<WatchApp, String>> list = this.a;
        return list != null ? list.size() : this.b.size() + 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        return (this.a == null && i == 0) ? 1 : 2;
    }

    @DexIgnore
    public final SpannableString i(String str, String str2) {
        if (!(str.length() == 0)) {
            if (!(str2.length() == 0)) {
                Mj6 mj6 = new Mj6(str2);
                if (str != null) {
                    String lowerCase = str.toLowerCase();
                    Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    Ts7<Ht7> findAll$default = Mj6.findAll$default(mj6, lowerCase, 0, 2, null);
                    SpannableString spannableString = new SpannableString(str);
                    for (Ht7 ht7 : findAll$default) {
                        spannableString.setSpan(new StyleSpan(1), ht7.a().h().intValue(), ht7.a().h().intValue() + str2.length(), 0);
                    }
                    return spannableString;
                }
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        return new SpannableString(str);
    }

    @DexIgnore
    public void j(Ai ai, int i) {
        Wg6.c(ai, "holder");
        if (!(ai instanceof Bi)) {
            Ei ei = (Ei) ai;
            List<Lc6<WatchApp, String>> list = this.a;
            if (list == null) {
                int i2 = i - 1;
                if (i2 < this.b.size()) {
                    if (!TextUtils.isEmpty(this.b.get(i2).getSecond())) {
                        FlexibleTextView b2 = ei.b();
                        Wg6.b(b2, "resultSearchViewHolder.tvAssignedTo");
                        b2.setVisibility(0);
                        FlexibleTextView b3 = ei.b();
                        Wg6.b(b3, "resultSearchViewHolder.tvAssignedTo");
                        b3.setText(Um5.c(PortfolioApp.get.instance(), 2131886516));
                    } else {
                        FlexibleTextView b4 = ei.b();
                        Wg6.b(b4, "resultSearchViewHolder.tvAssignedTo");
                        b4.setVisibility(8);
                    }
                    ei.d().b0(this.b.get(i2).getFirst().getWatchappId());
                    ei.c().setText(Um5.d(PortfolioApp.get.instance(), this.b.get(i2).getFirst().getNameKey(), this.b.get(i2).getFirst().getName()));
                    ei.e(this.b.get(i2).getFirst());
                    return;
                }
                ei.e(null);
            } else if (i - 1 < list.size()) {
                if (!TextUtils.isEmpty(list.get(i).getSecond())) {
                    FlexibleTextView b5 = ei.b();
                    Wg6.b(b5, "resultSearchViewHolder.tvAssignedTo");
                    b5.setVisibility(0);
                    FlexibleTextView b6 = ei.b();
                    Wg6.b(b6, "resultSearchViewHolder.tvAssignedTo");
                    b6.setText(Um5.c(PortfolioApp.get.instance(), 2131886516));
                } else {
                    FlexibleTextView b7 = ei.b();
                    Wg6.b(b7, "resultSearchViewHolder.tvAssignedTo");
                    b7.setVisibility(8);
                }
                ei.d().b0(list.get(i).getFirst().getWatchappId());
                String d2 = Um5.d(PortfolioApp.get.instance(), list.get(i).getFirst().getNameKey(), list.get(i).getFirst().getName());
                FlexibleTextView c2 = ei.c();
                Wg6.b(d2, "name");
                c2.setText(i(d2, this.c));
                ei.e(list.get(i).getFirst());
            } else {
                ei.e(null);
            }
        } else if (this.b.isEmpty()) {
            ((Bi) ai).a().setVisibility(4);
        } else {
            ((Bi) ai).a().setVisibility(0);
        }
    }

    @DexIgnore
    public Ai k(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        if (i == 1) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558671, viewGroup, false);
            Wg6.b(inflate, "view");
            return new Bi(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558725, viewGroup, false);
        Wg6.b(inflate2, "view");
        return new Ei(this, inflate2);
    }

    @DexIgnore
    public final void l(List<Lc6<WatchApp, String>> list) {
        Wg6.c(list, "value");
        this.b = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void m(List<Lc6<WatchApp, String>> list) {
        Ci ci;
        this.a = list;
        if (!(list == null || !list.isEmpty() || (ci = this.e) == null)) {
            ci.a(this.c);
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void n(String str) {
        Wg6.c(str, "<set-?>");
        this.c = str;
    }

    @DexIgnore
    public final void o(Ci ci) {
        Wg6.c(ci, "listener");
        this.e = ci;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        j(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return k(viewGroup, i);
    }

    @DexIgnore
    public final void p(Di di) {
        Wg6.c(di, "listener");
        this.d = di;
    }
}
