package com.fossil;

import com.fossil.En1;
import com.mapped.Hg6;
import com.mapped.Hi6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Z9 extends Nq7 implements Hg6<byte[], En1> {
    @DexIgnore
    public Z9(En1.Bi bi) {
        super(1, bi);
    }

    @DexIgnore
    @Override // com.fossil.Gq7, com.fossil.Ds7
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public final Hi6 getOwner() {
        return Er7.b(En1.Bi.class);
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/InactiveNudgeConfig;";
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public En1 invoke(byte[] bArr) {
        return ((En1.Bi) this.receiver).a(bArr);
    }
}
