package com.fossil;

import com.fossil.Zd1;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Qd1<T extends Zd1> {
    @DexIgnore
    public /* final */ Queue<T> a; // = Jk1.f(20);

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public T b() {
        T poll = this.a.poll();
        return poll == null ? a() : poll;
    }

    @DexIgnore
    public void c(T t) {
        if (this.a.size() < 20) {
            this.a.offer(t);
        }
    }
}
