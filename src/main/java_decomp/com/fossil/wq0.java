package com.fossil;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wq0 {
    @DexIgnore
    public /* final */ ArrayList<Fragment> a; // = new ArrayList<>();
    @DexIgnore
    public /* final */ HashMap<String, Vq0> b; // = new HashMap<>();

    @DexIgnore
    public void a(Fragment fragment) {
        if (!this.a.contains(fragment)) {
            synchronized (this.a) {
                this.a.add(fragment);
            }
            fragment.mAdded = true;
            return;
        }
        throw new IllegalStateException("Fragment already added: " + fragment);
    }

    @DexIgnore
    public void b() {
        this.b.values().removeAll(Collections.singleton(null));
    }

    @DexIgnore
    public boolean c(String str) {
        return this.b.containsKey(str);
    }

    @DexIgnore
    public void d(int i) {
        Iterator<Fragment> it = this.a.iterator();
        while (it.hasNext()) {
            Vq0 vq0 = this.b.get(it.next().mWho);
            if (vq0 != null) {
                vq0.r(i);
            }
        }
        for (Vq0 vq02 : this.b.values()) {
            if (vq02 != null) {
                vq02.r(i);
            }
        }
    }

    @DexIgnore
    public void e(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String str2 = str + "    ";
        if (!this.b.isEmpty()) {
            printWriter.print(str);
            printWriter.print("Active Fragments:");
            for (Vq0 vq0 : this.b.values()) {
                printWriter.print(str);
                if (vq0 != null) {
                    Fragment i = vq0.i();
                    printWriter.println(i);
                    i.dump(str2, fileDescriptor, printWriter, strArr);
                } else {
                    printWriter.println("null");
                }
            }
        }
        int size = this.a.size();
        if (size > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i2 = 0; i2 < size; i2++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i2);
                printWriter.print(": ");
                printWriter.println(this.a.get(i2).toString());
            }
        }
    }

    @DexIgnore
    public Fragment f(String str) {
        Vq0 vq0 = this.b.get(str);
        if (vq0 != null) {
            return vq0.i();
        }
        return null;
    }

    @DexIgnore
    public Fragment g(int i) {
        for (int size = this.a.size() - 1; size >= 0; size--) {
            Fragment fragment = this.a.get(size);
            if (fragment != null && fragment.mFragmentId == i) {
                return fragment;
            }
        }
        for (Vq0 vq0 : this.b.values()) {
            if (vq0 != null) {
                Fragment i2 = vq0.i();
                if (i2.mFragmentId == i) {
                    return i2;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public Fragment h(String str) {
        if (str != null) {
            for (int size = this.a.size() - 1; size >= 0; size--) {
                Fragment fragment = this.a.get(size);
                if (fragment != null && str.equals(fragment.mTag)) {
                    return fragment;
                }
            }
        }
        if (str != null) {
            for (Vq0 vq0 : this.b.values()) {
                if (vq0 != null) {
                    Fragment i = vq0.i();
                    if (str.equals(i.mTag)) {
                        return i;
                    }
                }
            }
        }
        return null;
    }

    @DexIgnore
    public Fragment i(String str) {
        Fragment findFragmentByWho;
        for (Vq0 vq0 : this.b.values()) {
            if (!(vq0 == null || (findFragmentByWho = vq0.i().findFragmentByWho(str)) == null)) {
                return findFragmentByWho;
            }
        }
        return null;
    }

    @DexIgnore
    public Fragment j(Fragment fragment) {
        ViewGroup viewGroup = fragment.mContainer;
        View view = fragment.mView;
        if (!(viewGroup == null || view == null)) {
            for (int indexOf = this.a.indexOf(fragment) - 1; indexOf >= 0; indexOf--) {
                Fragment fragment2 = this.a.get(indexOf);
                if (fragment2.mContainer == viewGroup && fragment2.mView != null) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public List<Fragment> k() {
        ArrayList arrayList = new ArrayList();
        for (Vq0 vq0 : this.b.values()) {
            if (vq0 != null) {
                arrayList.add(vq0.i());
            } else {
                arrayList.add(null);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public Vq0 l(String str) {
        return this.b.get(str);
    }

    @DexIgnore
    public List<Fragment> m() {
        ArrayList arrayList;
        if (this.a.isEmpty()) {
            return Collections.emptyList();
        }
        synchronized (this.a) {
            arrayList = new ArrayList(this.a);
        }
        return arrayList;
    }

    @DexIgnore
    public void n(Vq0 vq0) {
        this.b.put(vq0.i().mWho, vq0);
    }

    @DexIgnore
    public void o(Vq0 vq0) {
        Fragment i = vq0.i();
        for (Vq0 vq02 : this.b.values()) {
            if (vq02 != null) {
                Fragment i2 = vq02.i();
                if (i.mWho.equals(i2.mTargetWho)) {
                    i2.mTarget = i;
                    i2.mTargetWho = null;
                }
            }
        }
        this.b.put(i.mWho, null);
        String str = i.mTargetWho;
        if (str != null) {
            i.mTarget = f(str);
        }
    }

    @DexIgnore
    public void p(Fragment fragment) {
        synchronized (this.a) {
            this.a.remove(fragment);
        }
        fragment.mAdded = false;
    }

    @DexIgnore
    public void q() {
        this.b.clear();
    }

    @DexIgnore
    public void r(List<String> list) {
        this.a.clear();
        if (list != null) {
            for (String str : list) {
                Fragment f = f(str);
                if (f != null) {
                    if (FragmentManager.s0(2)) {
                        Log.v("FragmentManager", "restoreSaveState: added (" + str + "): " + f);
                    }
                    a(f);
                } else {
                    throw new IllegalStateException("No instantiated fragment for (" + str + ")");
                }
            }
        }
    }

    @DexIgnore
    public ArrayList<Uq0> s() {
        ArrayList<Uq0> arrayList = new ArrayList<>(this.b.size());
        for (Vq0 vq0 : this.b.values()) {
            if (vq0 != null) {
                Fragment i = vq0.i();
                Uq0 p = vq0.p();
                arrayList.add(p);
                if (FragmentManager.s0(2)) {
                    Log.v("FragmentManager", "Saved state of " + i + ": " + p.s);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public ArrayList<String> t() {
        synchronized (this.a) {
            if (this.a.isEmpty()) {
                return null;
            }
            ArrayList<String> arrayList = new ArrayList<>(this.a.size());
            Iterator<Fragment> it = this.a.iterator();
            while (it.hasNext()) {
                Fragment next = it.next();
                arrayList.add(next.mWho);
                if (FragmentManager.s0(2)) {
                    Log.v("FragmentManager", "saveAllState: adding fragment (" + next.mWho + "): " + next);
                }
            }
            return arrayList;
        }
    }
}
