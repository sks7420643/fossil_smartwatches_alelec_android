package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleImageButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class U25 extends T25 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362116, 1);
        E.put(2131361851, 2);
        E.put(2131362968, 3);
        E.put(2131362546, 4);
        E.put(2131362489, 5);
        E.put(2131362406, 6);
        E.put(2131362076, 7);
        E.put(2131361854, 8);
        E.put(2131362102, 9);
        E.put(2131361855, 10);
        E.put(2131361852, 11);
    }
    */

    @DexIgnore
    public U25(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 12, D, E));
    }

    @DexIgnore
    public U25(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (RTLImageView) objArr[2], (AppCompatImageView) objArr[11], (FlexibleImageButton) objArr[8], (FlexibleImageButton) objArr[10], (ConstraintLayout) objArr[7], (ConstraintLayout) objArr[9], (ConstraintLayout) objArr[1], (FlexibleTextView) objArr[6], (FlexibleButton) objArr[5], (FlexibleTextView) objArr[4], (DashBar) objArr[3], (ConstraintLayout) objArr[0]);
        this.C = -1;
        this.B.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.C != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.C = 1;
        }
        w();
    }
}
