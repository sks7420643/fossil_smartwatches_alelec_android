package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;
import com.mapped.G72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mi2 implements Parcelable.Creator<Uh2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Uh2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 0;
        int[] iArr = null;
        String str = null;
        Ii2 ii2 = null;
        G72 g72 = null;
        String str2 = null;
        DataType dataType = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 1:
                    dataType = (DataType) Ad2.e(parcel, t, DataType.CREATOR);
                    break;
                case 2:
                    str2 = Ad2.f(parcel, t);
                    break;
                case 3:
                    i = Ad2.v(parcel, t);
                    break;
                case 4:
                    g72 = (G72) Ad2.e(parcel, t, G72.CREATOR);
                    break;
                case 5:
                    ii2 = (Ii2) Ad2.e(parcel, t, Ii2.CREATOR);
                    break;
                case 6:
                    str = Ad2.f(parcel, t);
                    break;
                case 7:
                default:
                    Ad2.B(parcel, t);
                    break;
                case 8:
                    iArr = Ad2.d(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new Uh2(dataType, str2, i, g72, ii2, str, iArr);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Uh2[] newArray(int i) {
        return new Uh2[i];
    }
}
