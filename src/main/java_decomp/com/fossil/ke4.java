package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import com.sina.weibo.sdk.statistic.LogBuilder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ke4 {
    @DexIgnore
    public static Ke4 b;
    @DexIgnore
    public /* final */ SharedPreferences a;

    @DexIgnore
    public Ke4(Context context) {
        this.a = context.getSharedPreferences("FirebaseAppHeartBeat", 0);
    }

    @DexIgnore
    public static Ke4 a(Context context) {
        Ke4 ke4;
        synchronized (Ke4.class) {
            try {
                if (b == null) {
                    b = new Ke4(context);
                }
                ke4 = b;
            } catch (Throwable th) {
                throw th;
            }
        }
        return ke4;
    }

    @DexIgnore
    public boolean b(long j) {
        boolean c;
        synchronized (this) {
            c = c("fire-global", j);
        }
        return c;
    }

    @DexIgnore
    public boolean c(String str, long j) {
        synchronized (this) {
            if (!this.a.contains(str)) {
                this.a.edit().putLong(str, j).apply();
                return true;
            } else if (j - this.a.getLong(str, -1) < LogBuilder.MAX_INTERVAL) {
                return false;
            } else {
                this.a.edit().putLong(str, j).apply();
                return true;
            }
        }
    }
}
