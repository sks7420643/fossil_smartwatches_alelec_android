package com.fossil;

import android.content.Context;
import com.fossil.M62;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kj2 implements M62.Di.Bii {
    @DexIgnore
    public /* final */ GoogleSignInAccount b;

    @DexIgnore
    public Kj2(Context context, GoogleSignInAccount googleSignInAccount) {
        if ("<<default account>>".equals(googleSignInAccount.f())) {
            if (Mf2.h() && context.getPackageManager().hasSystemFeature("cn.google")) {
                this.b = null;
                return;
            }
        }
        this.b = googleSignInAccount;
    }

    @DexIgnore
    @Override // com.fossil.M62.Di.Bii
    public final GoogleSignInAccount b() {
        return this.b;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        return obj == this || ((obj instanceof Kj2) && Pc2.a(((Kj2) obj).b, this.b));
    }

    @DexIgnore
    public final int hashCode() {
        GoogleSignInAccount googleSignInAccount = this.b;
        if (googleSignInAccount != null) {
            return googleSignInAccount.hashCode();
        }
        return 0;
    }
}
