package com.fossil;

import dagger.internal.Factory;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Gk7<K, V, V2> implements Factory<Map<K, V2>> {
    @DexIgnore
    public /* final */ Map<K, Provider<V>> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai<K, V, V2> {
        @DexIgnore
        public /* final */ LinkedHashMap<K, Provider<V>> a;

        @DexIgnore
        public Ai(int i) {
            this.a = Hk7.b(i);
        }

        @DexIgnore
        public Ai<K, V, V2> a(K k, Provider<V> provider) {
            LinkedHashMap<K, Provider<V>> linkedHashMap = this.a;
            Lk7.c(k, "key");
            Lk7.c(provider, "provider");
            linkedHashMap.put(k, provider);
            return this;
        }
    }

    @DexIgnore
    public Gk7(Map<K, Provider<V>> map) {
        this.a = Collections.unmodifiableMap(map);
    }

    @DexIgnore
    public final Map<K, Provider<V>> a() {
        return this.a;
    }
}
