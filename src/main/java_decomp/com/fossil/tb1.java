package com.fossil;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import java.io.FileNotFoundException;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tb1 extends Ec1<AssetFileDescriptor> {
    @DexIgnore
    public Tb1(ContentResolver contentResolver, Uri uri) {
        super(contentResolver, uri);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Ec1
    public /* bridge */ /* synthetic */ void b(AssetFileDescriptor assetFileDescriptor) throws IOException {
        f(assetFileDescriptor);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Ec1
    public /* bridge */ /* synthetic */ AssetFileDescriptor e(Uri uri, ContentResolver contentResolver) throws FileNotFoundException {
        return g(uri, contentResolver);
    }

    @DexIgnore
    public void f(AssetFileDescriptor assetFileDescriptor) throws IOException {
        assetFileDescriptor.close();
    }

    @DexIgnore
    public AssetFileDescriptor g(Uri uri, ContentResolver contentResolver) throws FileNotFoundException {
        AssetFileDescriptor openAssetFileDescriptor = contentResolver.openAssetFileDescriptor(uri, "r");
        if (openAssetFileDescriptor != null) {
            return openAssetFileDescriptor;
        }
        throw new FileNotFoundException("FileDescriptor is null for: " + uri);
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public Class<AssetFileDescriptor> getDataClass() {
        return AssetFileDescriptor.class;
    }
}
