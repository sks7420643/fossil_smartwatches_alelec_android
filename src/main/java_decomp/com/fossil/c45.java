package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class C45 extends B45 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d P; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Q;
    @DexIgnore
    public long O;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        Q = sparseIntArray;
        sparseIntArray.put(2131363143, 1);
        Q.put(2131362056, 2);
        Q.put(2131362758, 3);
        Q.put(2131362172, 4);
        Q.put(2131362666, 5);
        Q.put(2131362641, 6);
        Q.put(2131362221, 7);
        Q.put(2131362637, 8);
        Q.put(2131362218, 9);
        Q.put(2131362645, 10);
        Q.put(2131362222, 11);
        Q.put(2131363322, 12);
        Q.put(2131362463, 13);
        Q.put(2131362805, 14);
        Q.put(2131362219, 15);
        Q.put(2131363342, 16);
        Q.put(2131362220, 17);
        Q.put(2131363366, 18);
        Q.put(2131362828, 19);
        Q.put(2131362223, 20);
        Q.put(2131363486, 21);
        Q.put(2131361975, 22);
        Q.put(2131361972, 23);
    }
    */

    @DexIgnore
    public C45(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 24, P, Q));
    }

    @DexIgnore
    public C45(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[23], (FlexibleButton) objArr[22], (ConstraintLayout) objArr[2], (ConstraintLayout) objArr[4], (FlexibleTextInputEditText) objArr[9], (FlexibleEditText) objArr[15], (FlexibleEditText) objArr[17], (FlexibleTextInputEditText) objArr[7], (FlexibleTextInputEditText) objArr[11], (FlexibleEditText) objArr[20], (FlexibleTextView) objArr[13], (FlexibleTextInputLayout) objArr[8], (FlexibleTextInputLayout) objArr[6], (FlexibleTextInputLayout) objArr[10], (RTLImageView) objArr[5], (ImageView) objArr[3], (LinearLayout) objArr[14], (LinearLayout) objArr[19], (ConstraintLayout) objArr[0], (ScrollView) objArr[1], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[18], (View) objArr[21]);
        this.O = -1;
        this.I.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.O = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.O != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.O = 1;
        }
        w();
    }
}
