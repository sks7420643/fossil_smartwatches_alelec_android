package com.fossil;

import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRemote;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L97 implements Factory<WFBackgroundPhotoRepository> {
    @DexIgnore
    public /* final */ Provider<WFBackgroundPhotoRemote> a;

    @DexIgnore
    public L97(Provider<WFBackgroundPhotoRemote> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static L97 a(Provider<WFBackgroundPhotoRemote> provider) {
        return new L97(provider);
    }

    @DexIgnore
    public static WFBackgroundPhotoRepository c(WFBackgroundPhotoRemote wFBackgroundPhotoRemote) {
        return new WFBackgroundPhotoRepository(wFBackgroundPhotoRemote);
    }

    @DexIgnore
    public WFBackgroundPhotoRepository b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
