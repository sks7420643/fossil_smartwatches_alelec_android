package com.fossil;

import com.mapped.U04;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Om6 implements Factory<HeartRateDetailPresenter> {
    @DexIgnore
    public static HeartRateDetailPresenter a(Jm6 jm6, HeartRateSummaryRepository heartRateSummaryRepository, HeartRateSampleRepository heartRateSampleRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FileRepository fileRepository, U04 u04) {
        return new HeartRateDetailPresenter(jm6, heartRateSummaryRepository, heartRateSampleRepository, userRepository, workoutSessionRepository, fileRepository, u04);
    }
}
