package com.fossil;

import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.retrofit.AuthenticationInterceptor;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ip4 implements Factory<GoogleApiService> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<AuthenticationInterceptor> b;
    @DexIgnore
    public /* final */ Provider<Uq5> c;

    @DexIgnore
    public Ip4(Uo4 uo4, Provider<AuthenticationInterceptor> provider, Provider<Uq5> provider2) {
        this.a = uo4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static Ip4 a(Uo4 uo4, Provider<AuthenticationInterceptor> provider, Provider<Uq5> provider2) {
        return new Ip4(uo4, provider, provider2);
    }

    @DexIgnore
    public static GoogleApiService c(Uo4 uo4, AuthenticationInterceptor authenticationInterceptor, Uq5 uq5) {
        GoogleApiService p = uo4.p(authenticationInterceptor, uq5);
        Lk7.c(p, "Cannot return null from a non-@Nullable @Provides method");
        return p;
    }

    @DexIgnore
    public GoogleApiService b() {
        return c(this.a, this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
