package com.fossil;

import android.content.Context;
import com.mapped.Ji;
import com.mapped.Oh;
import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hw0 {
    @DexIgnore
    public /* final */ Ji.Ci a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ Oh.Di d;
    @DexIgnore
    public /* final */ List<Oh.Bi> e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ Oh.Ci g;
    @DexIgnore
    public /* final */ Executor h;
    @DexIgnore
    public /* final */ Executor i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public /* final */ boolean k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ Set<Integer> m;

    @DexIgnore
    public Hw0(Context context, String str, Ji.Ci ci, Oh.Di di, List<Oh.Bi> list, boolean z, Oh.Ci ci2, Executor executor, Executor executor2, boolean z2, boolean z3, boolean z4, Set<Integer> set, String str2, File file) {
        this.a = ci;
        this.b = context;
        this.c = str;
        this.d = di;
        this.e = list;
        this.f = z;
        this.g = ci2;
        this.h = executor;
        this.i = executor2;
        this.j = z2;
        this.k = z3;
        this.l = z4;
        this.m = set;
    }

    @DexIgnore
    public boolean a(int i2, int i3) {
        Set<Integer> set;
        boolean z = true;
        if ((i2 > i3) && this.l) {
            return false;
        }
        if (!this.k || ((set = this.m) != null && set.contains(Integer.valueOf(i2)))) {
            z = false;
        }
        return z;
    }
}
