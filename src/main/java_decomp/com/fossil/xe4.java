package com.fossil;

import java.util.concurrent.CountDownLatch;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Xe4 implements Ht3 {
    @DexIgnore
    public /* final */ CountDownLatch a;

    @DexIgnore
    public Xe4(CountDownLatch countDownLatch) {
        this.a = countDownLatch;
    }

    @DexIgnore
    @Override // com.fossil.Ht3
    public final void onComplete(Nt3 nt3) {
        this.a.countDown();
    }
}
