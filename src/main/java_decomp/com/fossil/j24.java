package com.fossil;

import com.fossil.I44;
import java.io.Serializable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J24<T> extends I44<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ A34<T, Integer> rankMap;

    @DexIgnore
    public J24(A34<T, Integer> a34) {
        this.rankMap = a34;
    }

    @DexIgnore
    public J24(List<T> list) {
        this(X34.f(list));
    }

    @DexIgnore
    public final int a(T t) {
        Integer num = this.rankMap.get(t);
        if (num != null) {
            return num.intValue();
        }
        throw new I44.Ci(t);
    }

    @DexIgnore
    @Override // com.fossil.I44, java.util.Comparator
    public int compare(T t, T t2) {
        return a(t) - a(t2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof J24) {
            return this.rankMap.equals(((J24) obj).rankMap);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.rankMap.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Ordering.explicit(" + this.rankMap.keySet() + ")";
    }
}
