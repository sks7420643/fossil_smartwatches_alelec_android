package com.fossil;

import android.animation.LayoutTransition;
import android.util.Log;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dz0 {
    @DexIgnore
    public static LayoutTransition a;
    @DexIgnore
    public static Field b;
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public static Method d;
    @DexIgnore
    public static boolean e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends LayoutTransition {
        @DexIgnore
        public boolean isChangingLayout() {
            return true;
        }
    }

    @DexIgnore
    public static void a(LayoutTransition layoutTransition) {
        if (!e) {
            try {
                Method declaredMethod = LayoutTransition.class.getDeclaredMethod("cancel", new Class[0]);
                d = declaredMethod;
                declaredMethod.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("ViewGroupUtilsApi14", "Failed to access cancel method by reflection");
            }
            e = true;
        }
        Method method = d;
        if (method != null) {
            try {
                method.invoke(layoutTransition, new Object[0]);
            } catch (IllegalAccessException e3) {
                Log.i("ViewGroupUtilsApi14", "Failed to access cancel method by reflection");
            } catch (InvocationTargetException e4) {
                Log.i("ViewGroupUtilsApi14", "Failed to invoke cancel method by reflection");
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:37:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b(android.view.ViewGroup r5, boolean r6) {
        /*
            r4 = 1
            r0 = 0
            r3 = 0
            android.animation.LayoutTransition r1 = com.fossil.Dz0.a
            if (r1 != 0) goto L_0x0028
            com.fossil.Dz0$Ai r1 = new com.fossil.Dz0$Ai
            r1.<init>()
            com.fossil.Dz0.a = r1
            r2 = 2
            r1.setAnimator(r2, r3)
            android.animation.LayoutTransition r1 = com.fossil.Dz0.a
            r1.setAnimator(r0, r3)
            android.animation.LayoutTransition r1 = com.fossil.Dz0.a
            r1.setAnimator(r4, r3)
            android.animation.LayoutTransition r1 = com.fossil.Dz0.a
            r2 = 3
            r1.setAnimator(r2, r3)
            android.animation.LayoutTransition r1 = com.fossil.Dz0.a
            r2 = 4
            r1.setAnimator(r2, r3)
        L_0x0028:
            if (r6 == 0) goto L_0x0048
            android.animation.LayoutTransition r0 = r5.getLayoutTransition()
            if (r0 == 0) goto L_0x0042
            boolean r1 = r0.isRunning()
            if (r1 == 0) goto L_0x0039
            a(r0)
        L_0x0039:
            android.animation.LayoutTransition r1 = com.fossil.Dz0.a
            if (r0 == r1) goto L_0x0042
            int r1 = com.fossil.Ny0.transition_layout_save
            r5.setTag(r1, r0)
        L_0x0042:
            android.animation.LayoutTransition r0 = com.fossil.Dz0.a
            r5.setLayoutTransition(r0)
        L_0x0047:
            return
        L_0x0048:
            r5.setLayoutTransition(r3)
            boolean r1 = com.fossil.Dz0.c
            if (r1 != 0) goto L_0x005f
            java.lang.Class<android.view.ViewGroup> r1 = android.view.ViewGroup.class
            java.lang.String r2 = "mLayoutSuppressed"
            java.lang.reflect.Field r1 = r1.getDeclaredField(r2)     // Catch:{ NoSuchFieldException -> 0x0087 }
            com.fossil.Dz0.b = r1     // Catch:{ NoSuchFieldException -> 0x0087 }
            r2 = 1
            r1.setAccessible(r2)     // Catch:{ NoSuchFieldException -> 0x0087 }
        L_0x005d:
            com.fossil.Dz0.c = r4
        L_0x005f:
            java.lang.reflect.Field r1 = com.fossil.Dz0.b
            if (r1 == 0) goto L_0x006f
            boolean r0 = r1.getBoolean(r5)     // Catch:{ IllegalAccessException -> 0x0090 }
            if (r0 == 0) goto L_0x006f
            java.lang.reflect.Field r1 = com.fossil.Dz0.b     // Catch:{ IllegalAccessException -> 0x0099 }
            r2 = 0
            r1.setBoolean(r5, r2)     // Catch:{ IllegalAccessException -> 0x0099 }
        L_0x006f:
            if (r0 == 0) goto L_0x0074
            r5.requestLayout()
        L_0x0074:
            int r0 = com.fossil.Ny0.transition_layout_save
            java.lang.Object r0 = r5.getTag(r0)
            android.animation.LayoutTransition r0 = (android.animation.LayoutTransition) r0
            if (r0 == 0) goto L_0x0047
            int r1 = com.fossil.Ny0.transition_layout_save
            r5.setTag(r1, r3)
            r5.setLayoutTransition(r0)
            goto L_0x0047
        L_0x0087:
            r1 = move-exception
            java.lang.String r1 = "ViewGroupUtilsApi14"
            java.lang.String r2 = "Failed to access mLayoutSuppressed field by reflection"
            android.util.Log.i(r1, r2)
            goto L_0x005d
        L_0x0090:
            r1 = move-exception
        L_0x0091:
            java.lang.String r1 = "ViewGroupUtilsApi14"
            java.lang.String r2 = "Failed to get mLayoutSuppressed field by reflection"
            android.util.Log.i(r1, r2)
            goto L_0x006f
        L_0x0099:
            r1 = move-exception
            goto L_0x0091
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Dz0.b(android.view.ViewGroup, boolean):void");
    }
}
