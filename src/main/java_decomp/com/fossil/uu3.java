package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uu3 extends Tb2<Object> implements Z62 {
    @DexIgnore
    public /* final */ Status e;

    @DexIgnore
    public Uu3(DataHolder dataHolder) {
        super(dataHolder);
        this.e = new Status(dataHolder.k());
    }

    @DexIgnore
    @Override // com.fossil.Z62
    public Status a() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Tb2
    public /* synthetic */ Object c(int i, int i2) {
        return new Iv3(this.b, i, i2);
    }

    @DexIgnore
    @Override // com.fossil.Tb2
    public String f() {
        return "path";
    }
}
