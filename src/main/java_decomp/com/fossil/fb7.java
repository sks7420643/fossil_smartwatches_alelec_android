package com.fossil;

import android.graphics.drawable.Drawable;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fb7 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public L77 e;
    @DexIgnore
    public String f;
    @DexIgnore
    public Drawable g;
    @DexIgnore
    public K77 h;
    @DexIgnore
    public String i;

    @DexIgnore
    public Fb7(String str, String str2, String str3, String str4, L77 l77, String str5, Drawable drawable, K77 k77, String str6) {
        Wg6.c(str, "id");
        Wg6.c(str2, "name");
        Wg6.c(l77, "assetType");
        Wg6.c(str5, "checkSum");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = l77;
        this.f = str5;
        this.g = drawable;
        this.h = k77;
        this.i = str6;
    }

    @DexIgnore
    public final L77 a() {
        return this.e;
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final String c() {
        return this.i;
    }

    @DexIgnore
    public final String d() {
        return this.b;
    }

    @DexIgnore
    public final Drawable e() {
        return this.g;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Fb7) {
                Fb7 fb7 = (Fb7) obj;
                if (!Wg6.a(this.a, fb7.a) || !Wg6.a(this.b, fb7.b) || !Wg6.a(this.c, fb7.c) || !Wg6.a(this.d, fb7.d) || !Wg6.a(this.e, fb7.e) || !Wg6.a(this.f, fb7.f) || !Wg6.a(this.g, fb7.g) || !Wg6.a(this.h, fb7.h) || !Wg6.a(this.i, fb7.i)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.d;
    }

    @DexIgnore
    public final String g() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.d;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        L77 l77 = this.e;
        int hashCode5 = l77 != null ? l77.hashCode() : 0;
        String str5 = this.f;
        int hashCode6 = str5 != null ? str5.hashCode() : 0;
        Drawable drawable = this.g;
        int hashCode7 = drawable != null ? drawable.hashCode() : 0;
        K77 k77 = this.h;
        int hashCode8 = k77 != null ? k77.hashCode() : 0;
        String str6 = this.i;
        if (str6 != null) {
            i2 = str6.hashCode();
        }
        return (((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "UIWFAsset(id=" + this.a + ", name=" + this.b + ", urlData=" + this.c + ", previewUrl=" + this.d + ", assetType=" + this.e + ", checkSum=" + this.f + ", previewDrawable=" + this.g + ", colour=" + this.h + ", localDataPath=" + this.i + ")";
    }
}
