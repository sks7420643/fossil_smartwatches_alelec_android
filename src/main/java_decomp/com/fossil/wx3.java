package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wx3 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Wx3> CREATOR; // = new Ai();
    @DexIgnore
    public /* final */ Hy3 b;
    @DexIgnore
    public /* final */ Hy3 c;
    @DexIgnore
    public /* final */ Hy3 d;
    @DexIgnore
    public /* final */ Ci e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Wx3> {
        @DexIgnore
        public Wx3 a(Parcel parcel) {
            return new Wx3((Hy3) parcel.readParcelable(Hy3.class.getClassLoader()), (Hy3) parcel.readParcelable(Hy3.class.getClassLoader()), (Hy3) parcel.readParcelable(Hy3.class.getClassLoader()), (Ci) parcel.readParcelable(Ci.class.getClassLoader()), null);
        }

        @DexIgnore
        public Wx3[] b(int i) {
            return new Wx3[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Wx3 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Wx3[] newArray(int i) {
            return b(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public static /* final */ long e; // = Ny3.a(Hy3.b(1900, 0).h);
        @DexIgnore
        public static /* final */ long f; // = Ny3.a(Hy3.b(2100, 11).h);
        @DexIgnore
        public long a; // = e;
        @DexIgnore
        public long b; // = f;
        @DexIgnore
        public Long c;
        @DexIgnore
        public Ci d; // = By3.a(Long.MIN_VALUE);

        @DexIgnore
        public Bi(Wx3 wx3) {
            this.a = wx3.b.h;
            this.b = wx3.c.h;
            this.c = Long.valueOf(wx3.d.h);
            this.d = wx3.e;
        }

        @DexIgnore
        public Wx3 a() {
            if (this.c == null) {
                long M6 = Ey3.M6();
                if (this.a > M6 || M6 > this.b) {
                    M6 = this.a;
                }
                this.c = Long.valueOf(M6);
            }
            Bundle bundle = new Bundle();
            bundle.putParcelable("DEEP_COPY_VALIDATOR_KEY", this.d);
            return new Wx3(Hy3.c(this.a), Hy3.c(this.b), Hy3.c(this.c.longValue()), (Ci) bundle.getParcelable("DEEP_COPY_VALIDATOR_KEY"), null);
        }

        @DexIgnore
        public Bi b(long j) {
            this.c = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public interface Ci extends Parcelable {
        @DexIgnore
        boolean S(long j);
    }

    @DexIgnore
    public Wx3(Hy3 hy3, Hy3 hy32, Hy3 hy33, Ci ci) {
        this.b = hy3;
        this.c = hy32;
        this.d = hy33;
        this.e = ci;
        if (hy3.a(hy33) > 0) {
            throw new IllegalArgumentException("start Month cannot be after current Month");
        } else if (hy33.a(hy32) <= 0) {
            this.g = hy3.m(hy32) + 1;
            this.f = (hy32.e - hy3.e) + 1;
        } else {
            throw new IllegalArgumentException("current Month cannot be after end Month");
        }
    }

    @DexIgnore
    public /* synthetic */ Wx3(Hy3 hy3, Hy3 hy32, Hy3 hy33, Ci ci, Ai ai) {
        this(hy3, hy32, hy33, ci);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public Ci e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Wx3)) {
            return false;
        }
        Wx3 wx3 = (Wx3) obj;
        return this.b.equals(wx3.b) && this.c.equals(wx3.c) && this.d.equals(wx3.d) && this.e.equals(wx3.e);
    }

    @DexIgnore
    public Hy3 f() {
        return this.c;
    }

    @DexIgnore
    public int g() {
        return this.g;
    }

    @DexIgnore
    public Hy3 h() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.b, this.c, this.d, this.e});
    }

    @DexIgnore
    public Hy3 i() {
        return this.b;
    }

    @DexIgnore
    public int k() {
        return this.f;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.b, 0);
        parcel.writeParcelable(this.c, 0);
        parcel.writeParcelable(this.d, 0);
        parcel.writeParcelable(this.e, 0);
    }
}
