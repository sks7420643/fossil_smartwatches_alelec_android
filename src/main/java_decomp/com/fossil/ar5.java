package com.fossil;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ar5 implements SensorEventListener {
    @DexIgnore
    public /* final */ Di a; // = new Di();
    @DexIgnore
    public /* final */ Ai b;
    @DexIgnore
    public SensorManager c;
    @DexIgnore
    public Sensor d;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public long a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public Bi c;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public Bi a;

        @DexIgnore
        public Bi a() {
            Bi bi = this.a;
            if (bi == null) {
                return new Bi();
            }
            this.a = bi.c;
            return bi;
        }

        @DexIgnore
        public void b(Bi bi) {
            bi.c = this.a;
            this.a = bi;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di {
        @DexIgnore
        public /* final */ Ci a; // = new Ci();
        @DexIgnore
        public Bi b;
        @DexIgnore
        public Bi c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;

        @DexIgnore
        public void a(long j, boolean z) {
            d(j - 500000000);
            Bi a2 = this.a.a();
            a2.a = j;
            a2.b = z;
            a2.c = null;
            Bi bi = this.c;
            if (bi != null) {
                bi.c = a2;
            }
            this.c = a2;
            if (this.b == null) {
                this.b = a2;
            }
            this.d++;
            if (z) {
                this.e++;
            }
        }

        @DexIgnore
        public void b() {
            while (true) {
                Bi bi = this.b;
                if (bi != null) {
                    this.b = bi.c;
                    this.a.b(bi);
                } else {
                    this.c = null;
                    this.d = 0;
                    this.e = 0;
                    return;
                }
            }
        }

        @DexIgnore
        public boolean c() {
            Bi bi;
            Bi bi2 = this.c;
            if (!(bi2 == null || (bi = this.b) == null || bi2.a - bi.a < 250000000)) {
                int i = this.e;
                int i2 = this.d;
                if (i >= (i2 >> 2) + (i2 >> 1)) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public void d(long j) {
            Bi bi;
            while (this.d >= 4 && (bi = this.b) != null && j - bi.a > 0) {
                if (bi.b) {
                    this.e--;
                }
                this.d--;
                Bi bi2 = bi.c;
                this.b = bi2;
                if (bi2 == null) {
                    this.c = null;
                }
                this.a.b(bi);
            }
        }
    }

    @DexIgnore
    public Ar5(Ai ai) {
        this.b = ai;
    }

    @DexIgnore
    public final boolean a(SensorEvent sensorEvent) {
        float[] fArr = sensorEvent.values;
        float f = fArr[0];
        float f2 = fArr[1];
        float f3 = fArr[2];
        return Math.sqrt((double) ((f3 * f3) + ((f * f) + (f2 * f2)))) > 13.0d;
    }

    @DexIgnore
    public boolean b(SensorManager sensorManager) {
        if (this.d != null) {
            return true;
        }
        Sensor defaultSensor = sensorManager.getDefaultSensor(1);
        this.d = defaultSensor;
        if (defaultSensor != null) {
            this.c = sensorManager;
            sensorManager.registerListener(this, defaultSensor, 0);
        }
        return this.d != null;
    }

    @DexIgnore
    public void c() {
        Sensor sensor = this.d;
        if (sensor != null) {
            this.c.unregisterListener(this, sensor);
            this.c = null;
            this.d = null;
        }
    }

    @DexIgnore
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    @DexIgnore
    public void onSensorChanged(SensorEvent sensorEvent) {
        boolean a2 = a(sensorEvent);
        this.a.a(sensorEvent.timestamp, a2);
        if (this.a.c()) {
            this.a.b();
            this.b.a();
        }
    }
}
