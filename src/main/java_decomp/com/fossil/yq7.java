package com.fossil;

import com.fossil.Ks7;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Yq7 extends Gq7 implements Ks7 {
    @DexIgnore
    public Yq7() {
    }

    @DexIgnore
    public Yq7(Object obj) {
        super(obj);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Yq7) {
            Yq7 yq7 = (Yq7) obj;
            return getOwner().equals(yq7.getOwner()) && getName().equals(yq7.getName()) && getSignature().equals(yq7.getSignature()) && Wg6.a(getBoundReceiver(), yq7.getBoundReceiver());
        } else if (obj instanceof Ks7) {
            return obj.equals(compute());
        } else {
            return false;
        }
    }

    @DexIgnore
    public abstract /* synthetic */ Ks7.Ai<R> getGetter();

    @DexIgnore
    @Override // com.fossil.Gq7
    public Ks7 getReflected() {
        return (Ks7) super.getReflected();
    }

    @DexIgnore
    public int hashCode() {
        return (((getOwner().hashCode() * 31) + getName().hashCode()) * 31) + getSignature().hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ks7
    public boolean isConst() {
        return getReflected().isConst();
    }

    @DexIgnore
    @Override // com.fossil.Ks7
    public boolean isLateinit() {
        return getReflected().isLateinit();
    }

    @DexIgnore
    public String toString() {
        Ds7 compute = compute();
        if (compute != this) {
            return compute.toString();
        }
        return "property " + getName() + " (Kotlin reflection is not available)";
    }
}
