package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Cj1 {

    @DexIgnore
    public enum Ai {
        RUNNING(false),
        PAUSED(false),
        CLEARED(false),
        SUCCESS(true),
        FAILED(true);
        
        @DexIgnore
        public /* final */ boolean isComplete;

        @DexIgnore
        public Ai(boolean z) {
            this.isComplete = z;
        }

        @DexIgnore
        public boolean isComplete() {
            return this.isComplete;
        }
    }

    @DexIgnore
    void a(Bj1 bj1);

    @DexIgnore
    boolean b();

    @DexIgnore
    Cj1 c();

    @DexIgnore
    boolean d(Bj1 bj1);

    @DexIgnore
    boolean e(Bj1 bj1);

    @DexIgnore
    void i(Bj1 bj1);

    @DexIgnore
    boolean k(Bj1 bj1);
}
