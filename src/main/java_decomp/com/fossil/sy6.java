package com.fossil;

import com.mapped.Wg6;
import com.portfolio.platform.CoroutineUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sy6 implements CoroutineUseCase.Ai {
    @DexIgnore
    public int a;
    @DexIgnore
    public String b;

    @DexIgnore
    public Sy6(int i, String str) {
        Wg6.c(str, "errorMessage");
        this.a = i;
        this.b = str;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }
}
