package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.O80;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ro1 extends O80 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ro1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Ro1 a(Parcel parcel) {
            return new Ro1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ro1 createFromParcel(Parcel parcel) {
            return new Ro1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ro1[] newArray(int i) {
            return new Ro1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Ro1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public Ro1(Ww1 ww1) {
        super(Ap1.COMMUTE, null, ww1, 2);
    }

    @DexIgnore
    public Ro1(Ww1 ww1, Vw1 vw1) {
        super(Ap1.COMMUTE, vw1, ww1);
    }

    @DexIgnore
    @Override // com.mapped.O80
    public JSONObject a() {
        JSONObject put = super.a().put("commuteApp._.config.destinations", Ay1.b(getDataConfig().getDestinations()));
        Wg6.b(put, "super.getDataConfigJSONO\u2026stinations.toJSONArray())");
        return put;
    }

    @DexIgnore
    public final Ww1 getDataConfig() {
        Xw1 xw1 = this.d;
        if (xw1 != null) {
            return (Ww1) xw1;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.CommuteTimeWatchAppDataConfig");
    }
}
