package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.model.PlaceFields;
import com.fossil.Cr4;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel;
import com.portfolio.platform.uirenew.mappicker.MapPickerActivity;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Da6 extends BaseFragment implements Cr4.Ai {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public G37<R35> g;
    @DexIgnore
    public CommuteTimeSettingsDetailViewModel h;
    @DexIgnore
    public Po4 i;
    @DexIgnore
    public Cr4 j;
    @DexIgnore
    public Boolean k; // = Boolean.FALSE;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Da6.m;
        }

        @DexIgnore
        public final Da6 b(AddressWrapper addressWrapper, ArrayList<String> arrayList, boolean z) {
            Da6 da6 = new Da6();
            Bundle bundle = new Bundle();
            bundle.putParcelable("KEY_BUNDLE_SETTING_ADDRESS", addressWrapper);
            bundle.putStringArrayList("KEY_BUNDLE_LIST_ADDRESS", arrayList);
            bundle.putBoolean("KEY_BUNDLE_IS_MAP_RESULT", z);
            da6.setArguments(bundle);
            return da6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ R35 c;

        @DexIgnore
        public Bi(View view, R35 r35) {
            this.b = view;
            this.c = r35;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.b.getWindowVisibleDisplayFrame(rect);
            int height = this.b.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.c.z.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                R35 r35 = this.c;
                Wg6.b(r35, "binding");
                r35.n().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = Da6.s.a();
                local.d(a2, "observeKeyboard - isOpen=true - dropDownHeight=" + i);
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = this.c.q;
                Wg6.b(flexibleAutoCompleteTextView, "binding.autocompletePlaces");
                flexibleAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Da6 b;

        @DexIgnore
        public Ci(Da6 da6, R35 r35) {
            this.b = da6;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction f;
            Cr4 cr4 = this.b.j;
            if (cr4 != null && (f = cr4.f(i)) != null) {
                SpannableString fullText = f.getFullText(null);
                Wg6.b(fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = Da6.s.a();
                local.i(a2, "Autocomplete item selected: " + ((Object) fullText));
                CommuteTimeSettingsDetailViewModel L6 = Da6.L6(this.b);
                String spannableString = fullText.toString();
                Wg6.b(spannableString, "primaryText.toString()");
                if (spannableString != null) {
                    String obj = Wt7.u0(spannableString).toString();
                    String placeId = f.getPlaceId();
                    Cr4 cr42 = this.b.j;
                    if (cr42 != null) {
                        L6.m(obj, placeId, cr42.g());
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ R35 b;

        @DexIgnore
        public Di(Da6 da6, R35 r35) {
            this.b = r35;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.b.t;
            Wg6.b(imageView, "binding.closeIv");
            imageView.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ Da6 b;

        @DexIgnore
        public Ei(Da6 da6, R35 r35) {
            this.b = da6;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            Wg6.b(keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.b.F6();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Da6 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Fi(Da6 da6) {
            this.b = da6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            CommuteTimeSettingsDetailViewModel L6 = Da6.L6(this.b);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                String obj = Wt7.u0(valueOf).toString();
                if (obj != null) {
                    String upperCase = obj.toUpperCase();
                    Wg6.b(upperCase, "(this as java.lang.String).toUpperCase()");
                    L6.q(upperCase);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Da6 b;

        @DexIgnore
        public Gi(Da6 da6) {
            this.b = da6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.x0(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Da6 b;

        @DexIgnore
        public Hi(Da6 da6) {
            this.b = da6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (Da6.L6(this.b).k()) {
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = this.b.getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                s37.M(childFragmentManager);
            } else if (Da6.L6(this.b).l()) {
                this.b.x0(true);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Da6 b;
        @DexIgnore
        public /* final */ /* synthetic */ R35 c;

        @DexIgnore
        public Ii(Da6 da6, R35 r35) {
            this.b = da6;
            this.c = r35;
        }

        @DexIgnore
        public final void onClick(View view) {
            CommuteTimeSettingsDetailViewModel.n(Da6.L6(this.b), null, null, null, 7, null);
            this.c.q.setText("");
            this.b.P6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Da6 a;

        @DexIgnore
        public Ji(Da6 da6) {
            this.a = da6;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            Da6.L6(this.a).p(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Da6 b;

        @DexIgnore
        public Ki(Da6 da6) {
            this.b = da6;
        }

        @DexIgnore
        public final void onClick(View view) {
            String str;
            double d = 0.0d;
            MapPickerActivity.a aVar = MapPickerActivity.A;
            Da6 da6 = this.b;
            AddressWrapper g = Da6.L6(da6).g();
            double lat = g != null ? g.getLat() : 0.0d;
            AddressWrapper g2 = Da6.L6(this.b).g();
            if (g2 != null) {
                d = g2.getLng();
            }
            AddressWrapper g3 = Da6.L6(this.b).g();
            if (g3 == null || (str = g3.getAddress()) == null) {
                str = "";
            }
            aVar.a(da6, lat, d, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li<T> implements Ls0<CommuteTimeSettingsDetailViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ Da6 a;
        @DexIgnore
        public /* final */ /* synthetic */ R35 b;

        @DexIgnore
        public Li(Da6 da6, R35 r35) {
            this.a = da6;
            this.b = r35;
        }

        @DexIgnore
        public final void a(CommuteTimeSettingsDetailViewModel.Ai ai) {
            AddressWrapper.AddressType e;
            PlacesClient d = ai.d();
            if (d != null) {
                this.a.H(d);
            }
            String c = ai.c();
            if (!(c == null || (e = ai.e()) == null)) {
                int i = Ea6.a[e.ordinal()];
                if (i == 1) {
                    this.b.u.setText(Um5.c(PortfolioApp.get.instance(), 2131886354));
                } else if (i == 2) {
                    this.b.u.setText(Um5.c(PortfolioApp.get.instance(), 2131886356));
                } else if (i == 3) {
                    this.b.u.setText(c);
                }
            }
            String a2 = ai.a();
            if (a2 != null) {
                this.b.q.setText(a2);
            }
            Boolean b2 = ai.b();
            if (b2 != null) {
                boolean booleanValue = b2.booleanValue();
                FlexibleSwitchCompat flexibleSwitchCompat = this.b.D;
                Wg6.b(flexibleSwitchCompat, "binding.scAvoidTolls");
                flexibleSwitchCompat.setChecked(booleanValue);
            }
            Boolean h = ai.h();
            if (h != null) {
                boolean booleanValue2 = h.booleanValue();
                FlexibleEditText flexibleEditText = this.b.u;
                Wg6.b(flexibleEditText, "binding.fetAddressName");
                flexibleEditText.setEnabled(booleanValue2);
                if (booleanValue2) {
                    this.b.u.requestFocus();
                }
            }
            Boolean i2 = ai.i();
            if (i2 != null) {
                if (i2.booleanValue()) {
                    this.b.s.d("flexible_button_primary");
                } else {
                    this.b.s.d("flexible_button_disabled");
                }
            }
            Boolean g = ai.g();
            if (g != null && g.booleanValue()) {
                this.a.E4();
            }
            Boolean f = ai.f();
            if (f == null) {
                return;
            }
            if (f.booleanValue()) {
                this.a.b();
            } else {
                this.a.a();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(CommuteTimeSettingsDetailViewModel.Ai ai) {
            a(ai);
        }
    }

    /*
    static {
        String simpleName = Da6.class.getSimpleName();
        Wg6.b(simpleName, "CommuteTimeSettingsDetai\u2026nt::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ CommuteTimeSettingsDetailViewModel L6(Da6 da6) {
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = da6.h;
        if (commuteTimeSettingsDetailViewModel != null) {
            return commuteTimeSettingsDetailViewModel;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Cr4.Ai
    public void D1(boolean z) {
        G37<R35> g37 = this.g;
        if (g37 != null) {
            R35 a2 = g37.a();
            if (a2 != null) {
                if (z) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    Wg6.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(flexibleAutoCompleteTextView.getText())) {
                        Boolean bool = this.k;
                        if (bool == null) {
                            Wg6.i();
                            throw null;
                        } else if (!bool.booleanValue()) {
                            Q6();
                            return;
                        }
                    }
                }
                P6();
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void E4() {
        FLogger.INSTANCE.getLocal().d(m, "showLocationError");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.J(childFragmentManager);
        }
    }

    @DexIgnore
    public final void H(PlacesClient placesClient) {
        if (isActive()) {
            Context requireContext = requireContext();
            Wg6.b(requireContext, "requireContext()");
            Cr4 cr4 = new Cr4(requireContext, placesClient);
            this.j = cr4;
            if (cr4 != null) {
                cr4.h(this);
            }
            G37<R35> g37 = this.g;
            if (g37 != null) {
                R35 a2 = g37.a();
                if (a2 != null) {
                    a2.q.setAdapter(this.j);
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    Wg6.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    Wg6.b(text, "it.autocompletePlaces.text");
                    CharSequence u0 = Wt7.u0(text);
                    if (u0.length() > 0) {
                        a2.q.setText(u0);
                        a2.q.setSelection(u0.length());
                        return;
                    }
                    P6();
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void P6() {
        G37<R35> g37 = this.g;
        if (g37 != null) {
            R35 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                Wg6.b(flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        G37<R35> g37 = this.g;
        if (g37 != null) {
            R35 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                Wg6.b(flexibleTextView, "it.ftvAddressError");
                PortfolioApp instance = PortfolioApp.get.instance();
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                Wg6.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(instance.getString(2131886360, new Object[]{flexibleAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.v;
                Wg6.b(flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (intent != null && i2 == 100) {
            Bundle bundleExtra = intent.getBundleExtra(Constants.RESULT);
            String string = bundleExtra != null ? bundleExtra.getString("address") : null;
            Location location = bundleExtra != null ? (Location) bundleExtra.getParcelable(PlaceFields.LOCATION) : null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = m;
            StringBuilder sb = new StringBuilder();
            sb.append("addressResult: ");
            sb.append(string);
            sb.append(", lat: ");
            sb.append(location != null ? Double.valueOf(location.getLatitude()) : null);
            sb.append(", lng: ");
            sb.append(location != null ? Double.valueOf(location.getLongitude()) : null);
            local.d(str, sb.toString());
            if (string != null && location != null) {
                CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.h;
                if (commuteTimeSettingsDetailViewModel != null) {
                    commuteTimeSettingsDetailViewModel.o(string, location);
                    this.k = Boolean.TRUE;
                    return;
                }
                Wg6.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        Boolean bool = null;
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().M1().a(this);
        Po4 po4 = this.i;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(CommuteTimeSettingsDetailViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ailViewModel::class.java)");
            CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = (CommuteTimeSettingsDetailViewModel) a2;
            this.h = commuteTimeSettingsDetailViewModel;
            if (commuteTimeSettingsDetailViewModel != null) {
                Bundle arguments = getArguments();
                AddressWrapper addressWrapper = arguments != null ? (AddressWrapper) arguments.getParcelable("KEY_BUNDLE_SETTING_ADDRESS") : null;
                Bundle arguments2 = getArguments();
                commuteTimeSettingsDetailViewModel.j(addressWrapper, arguments2 != null ? arguments2.getStringArrayList("KEY_BUNDLE_LIST_ADDRESS") : null);
                Bundle arguments3 = getArguments();
                if (arguments3 != null) {
                    bool = Boolean.valueOf(arguments3.getBoolean("KEY_BUNDLE_IS_MAP_RESULT"));
                }
                this.k = bool;
                return;
            }
            Wg6.n("mViewModel");
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        R35 r35 = (R35) Aq0.f(layoutInflater, 2131558518, viewGroup, false, A6());
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = r35.q;
        flexibleAutoCompleteTextView.setDropDownBackgroundDrawable(W6.f(flexibleAutoCompleteTextView.getContext(), 2131230821));
        flexibleAutoCompleteTextView.setOnItemClickListener(new Ci(this, r35));
        flexibleAutoCompleteTextView.addTextChangedListener(new Di(this, r35));
        flexibleAutoCompleteTextView.setOnKeyListener(new Ei(this, r35));
        r35.u.addTextChangedListener(new Fi(this));
        r35.w.setOnClickListener(new Gi(this));
        r35.s.setOnClickListener(new Hi(this));
        r35.t.setOnClickListener(new Ii(this, r35));
        r35.D.setOnCheckedChangeListener(new Ji(this));
        Wg6.b(r35, "binding");
        View n = r35.n();
        Wg6.b(n, "binding.root");
        n.getViewTreeObserver().addOnGlobalLayoutListener(new Bi(n, r35));
        r35.x.setOnClickListener(new Ki(this));
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.h;
        if (commuteTimeSettingsDetailViewModel != null) {
            commuteTimeSettingsDetailViewModel.i().h(getViewLifecycleOwner(), new Li(this, r35));
            this.g = new G37<>(this, r35);
            return r35.n();
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.h;
        if (commuteTimeSettingsDetailViewModel != null) {
            commuteTimeSettingsDetailViewModel.s();
            super.onPause();
            return;
        }
        Wg6.n("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.h;
        if (commuteTimeSettingsDetailViewModel != null) {
            commuteTimeSettingsDetailViewModel.r();
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void x0(boolean z) {
        if (z) {
            CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.h;
            if (commuteTimeSettingsDetailViewModel != null) {
                AddressWrapper g2 = commuteTimeSettingsDetailViewModel.g();
                if (g2 != null) {
                    Intent intent = new Intent();
                    intent.putExtra("KEY_SELECTED_ADDRESS", g2);
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.setResult(-1, intent);
                    }
                }
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }
}
