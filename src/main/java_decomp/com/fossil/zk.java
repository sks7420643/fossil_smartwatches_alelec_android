package com.fossil;

import com.fossil.fitness.WorkoutState;
import com.fossil.fitness.WorkoutType;
import com.mapped.C90;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zk extends Cq {
    @DexIgnore
    public /* final */ ArrayList<Ow> E; // = By1.a(this.C, Hm7.c(Ow.c));
    @DexIgnore
    public C90 F;

    @DexIgnore
    public Zk(K5 k5, I60 i60) {
        super(k5, i60, Yp.J, new Tu(k5));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        JSONObject E2 = super.E();
        Jd0 jd0 = Jd0.f0;
        C90 c90 = this.F;
        return G80.k(E2, jd0, c90 != null ? c90.toJSONObject() : null);
    }

    @DexIgnore
    @Override // com.fossil.Cq
    public void G(Fs fs) {
        this.F = ((Tu) fs).M;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        C90 c90 = this.F;
        return c90 != null ? c90 : new C90(0, WorkoutType.UNKNOWN, WorkoutState.END, 0, 0, 0, 0, 0, 0, 0, 0, false);
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Cq
    public ArrayList<Ow> z() {
        return this.E;
    }
}
