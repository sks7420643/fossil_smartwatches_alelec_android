package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pc2 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ List<String> a;
        @DexIgnore
        public /* final */ Object b;

        @DexIgnore
        public Ai(Object obj) {
            Rc2.k(obj);
            this.b = obj;
            this.a = new ArrayList();
        }

        @DexIgnore
        public final Ai a(String str, Object obj) {
            List<String> list = this.a;
            Rc2.k(str);
            String str2 = str;
            String valueOf = String.valueOf(obj);
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 1 + String.valueOf(valueOf).length());
            sb.append(str2);
            sb.append(SimpleComparison.EQUAL_TO_OPERATION);
            sb.append(valueOf);
            list.add(sb.toString());
            return this;
        }

        @DexIgnore
        public final String toString() {
            StringBuilder sb = new StringBuilder(100);
            sb.append(this.b.getClass().getSimpleName());
            sb.append('{');
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                sb.append(this.a.get(i));
                if (i < size - 1) {
                    sb.append(", ");
                }
            }
            sb.append('}');
            return sb.toString();
        }
    }

    @DexIgnore
    public static boolean a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    @DexIgnore
    public static int b(Object... objArr) {
        return Arrays.hashCode(objArr);
    }

    @DexIgnore
    public static Ai c(Object obj) {
        return new Ai(obj);
    }
}
