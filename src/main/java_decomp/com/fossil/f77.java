package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F77 {
    @DexIgnore
    public int a; // = -1;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ D77 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Ge5 a;
        @DexIgnore
        public /* final */ D77 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;
            @DexIgnore
            public /* final */ /* synthetic */ Fb7 c;

            @DexIgnore
            public Aii(Ai ai, Fb7 fb7, boolean z, int i) {
                this.b = ai;
                this.c = fb7;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.b.b(this.c, this.b.getAdapterPosition());
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnLongClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Bii(Ai ai, Fb7 fb7, boolean z, int i) {
                this.b = ai;
            }

            @DexIgnore
            public final boolean onLongClick(View view) {
                this.b.b.a();
                return true;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;
            @DexIgnore
            public /* final */ /* synthetic */ Fb7 c;

            @DexIgnore
            public Cii(Ai ai, Fb7 fb7, boolean z, int i) {
                this.b = ai;
                this.c = fb7;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.b.d(this.c);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Dii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai b;

            @DexIgnore
            public Dii(Ai ai, Fb7 fb7, boolean z, int i) {
                this.b = ai;
            }

            @DexIgnore
            public final void onClick(View view) {
                this.b.b.onCancel();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Ge5 ge5, D77 d77) {
            super(ge5.n());
            Wg6.c(ge5, "binding");
            Wg6.c(d77, "photoListener");
            this.a = ge5;
            this.b = d77;
        }

        @DexIgnore
        public final void b(Fb7 fb7, int i, boolean z) {
            Wg6.c(fb7, "wfWrapper");
            Ge5 ge5 = this.a;
            ge5.q.setOnClickListener(new Aii(this, fb7, z, i));
            ge5.q.setOnLongClickListener(new Bii(this, fb7, z, i));
            ge5.r.setOnClickListener(new Cii(this, fb7, z, i));
            ge5.n().setOnClickListener(new Dii(this, fb7, z, i));
            ImageView imageView = ge5.r;
            Wg6.b(imageView, "ivRemove");
            imageView.setVisibility(z ? 0 : 8);
            Drawable e = fb7.e();
            if (e != null) {
                ImageView imageView2 = ge5.q;
                Wg6.b(imageView2, "ivBackgroundPreview");
                E51.a(imageView2);
                ge5.q.setImageDrawable(e);
            } else {
                ImageView imageView3 = ge5.q;
                Wg6.b(imageView3, "ivBackgroundPreview");
                Ty4.a(imageView3, fb7.f(), W6.f(PortfolioApp.get.instance(), 2131231268));
            }
            if (i == getAdapterPosition()) {
                View view = ge5.t;
                Wg6.b(view, "vBackgroundSelected");
                view.setVisibility(0);
                return;
            }
            View view2 = ge5.t;
            Wg6.b(view2, "vBackgroundSelected");
            view2.setVisibility(8);
        }
    }

    @DexIgnore
    public F77(int i, D77 d77) {
        Wg6.c(d77, "photoListener");
        this.c = i;
        this.d = d77;
    }

    @DexIgnore
    public final void a(int i) {
        this.a = i;
    }

    @DexIgnore
    public final int b() {
        return this.c;
    }

    @DexIgnore
    public final void c(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public boolean d(List<? extends Object> list, int i) {
        Wg6.c(list, "items");
        Object obj = list.get(i);
        return (obj instanceof Fb7) && ((Fb7) obj).a() == L77.BACKGROUND_PHOTO;
    }

    @DexIgnore
    public void e(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        Ai ai = null;
        Wg6.c(list, "items");
        Wg6.c(viewHolder, "holder");
        Object obj = list.get(i);
        if (!(obj instanceof Fb7)) {
            obj = null;
        }
        Fb7 fb7 = (Fb7) obj;
        if (fb7 != null) {
            if (viewHolder instanceof Ai) {
                ai = viewHolder;
            }
            Ai ai2 = ai;
            if (ai2 != null) {
                ai2.b(fb7, this.a, this.b);
            }
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder f(ViewGroup viewGroup) {
        Wg6.c(viewGroup, "parent");
        Ge5 z = Ge5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemDianaBackgroundBindi\u2026(inflater, parent, false)");
        return new Ai(z, this.d);
    }
}
