package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iv4 extends ts0 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ String f1674a;
    @DexIgnore
    public /* final */ MutableLiveData<List<Object>> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Boolean, ServerError>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public Timer f;
    @DexIgnore
    public /* final */ tt4 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel", f = "BCLeaderBoardViewModel.kt", l = {94}, m = "initPlayers")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ iv4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(iv4 iv4, qn7 qn7) {
            super(qn7);
            this.this$0 = iv4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.j(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$initPlayers$historyChallenge$1", f = "BCLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super bt4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iv4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(iv4 iv4, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iv4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$challengeId, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super bt4> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return this.this$0.g.z(this.$challengeId);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$loadPlayers$1", f = "BCLeaderBoardViewModel.kt", l = {51, 56, 58}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ String $status;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iv4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(iv4 iv4, String str, String str2, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iv4;
            this.$challengeId = str;
            this.$status = str2;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$challengeId, this.$status, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0046  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0059  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0072  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 3
                r5 = 2
                r4 = 1
                java.lang.Object r1 = com.fossil.yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0074
                if (r0 == r4) goto L_0x0048
                if (r0 == r5) goto L_0x0023
                if (r0 != r6) goto L_0x001b
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
            L_0x0018:
                com.fossil.tl7 r0 = com.fossil.tl7.f3441a
            L_0x001a:
                return r0
            L_0x001b:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0023:
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
            L_0x002a:
                com.fossil.iv4 r2 = r7.this$0
                androidx.lifecycle.MutableLiveData r2 = com.fossil.iv4.d(r2)
                r3 = 0
                java.lang.Boolean r3 = com.fossil.ao7.a(r3)
                r2.l(r3)
                com.fossil.iv4 r2 = r7.this$0
                java.lang.String r3 = r7.$challengeId
                r7.L$0 = r0
                r7.label = r6
                java.lang.Object r0 = r2.o(r3, r7)
                if (r0 != r1) goto L_0x0018
                r0 = r1
                goto L_0x001a
            L_0x0048:
                java.lang.Object r0 = r7.L$0
                com.fossil.iv7 r0 = (com.fossil.iv7) r0
                com.fossil.el7.b(r8)
            L_0x004f:
                java.lang.String r2 = r7.$status
                java.lang.String r3 = "completed"
                boolean r2 = com.fossil.pq7.a(r2, r3)
                if (r2 == 0) goto L_0x0064
                com.fossil.iv4 r2 = r7.this$0
                androidx.lifecycle.MutableLiveData r2 = com.fossil.iv4.c(r2)
                java.lang.String r3 = "completed"
                r2.l(r3)
            L_0x0064:
                com.fossil.iv4 r2 = r7.this$0
                java.lang.String r3 = r7.$challengeId
                r7.L$0 = r0
                r7.label = r5
                java.lang.Object r2 = r2.l(r3, r7)
                if (r2 != r1) goto L_0x002a
                r0 = r1
                goto L_0x001a
            L_0x0074:
                com.fossil.el7.b(r8)
                com.fossil.iv7 r0 = r7.p$
                com.fossil.iv4 r2 = r7.this$0
                androidx.lifecycle.MutableLiveData r2 = com.fossil.iv4.d(r2)
                java.lang.Boolean r3 = com.fossil.ao7.a(r4)
                r2.l(r3)
                com.fossil.iv4 r2 = r7.this$0
                java.lang.String r3 = r7.$challengeId
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r2 = r2.j(r3, r7)
                if (r2 != r1) goto L_0x004f
                r0 = r1
                goto L_0x001a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.iv4.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel", f = "BCLeaderBoardViewModel.kt", l = {103, 116, 122}, m = "loadPlayersFromServer")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ iv4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(iv4 iv4, qn7 qn7) {
            super(qn7);
            this.this$0 = iv4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.l(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$loadPlayersFromServer$2", f = "BCLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $players;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iv4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(iv4 iv4, List list, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iv4;
            this.$players = list;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, this.$players, qn7);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                this.this$0.g.C(this.$players);
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$loadPlayersFromServer$result$1", f = "BCLeaderBoardViewModel.kt", l = {104}, m = "invokeSuspend")
    public static final class f extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends ms4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iv4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(iv4 iv4, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iv4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(this.this$0, this.$challengeId, qn7);
            fVar.p$ = (iv7) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends ms4>>> qn7) {
            throw null;
            //return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                tt4 tt4 = this.this$0.g;
                String str = this.$challengeId;
                this.L$0 = iv7;
                this.label = 1;
                Object t = tt4.t(str, new String[]{"running", "completed", "left_after_start"}, this);
                return t == d ? d : t;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$refresh$1", f = "BCLeaderBoardViewModel.kt", l = {65}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iv4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(iv4 iv4, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iv4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, this.$challengeId, qn7);
            gVar.p$ = (iv7) obj;
            throw null;
            //return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                iv4 iv4 = this.this$0;
                String str = this.$challengeId;
                this.L$0 = iv7;
                this.label = 1;
                if (iv4.l(str, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.c.l(ao7.a(false));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$updateHistoryChallenge$2", f = "BCLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ List $players;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iv4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(iv4 iv4, String str, List list, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iv4;
            this.$challengeId = str;
            this.$players = list;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, this.$challengeId, this.$players, qn7);
            hVar.p$ = (iv7) obj;
            throw null;
            //return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                bt4 z = this.this$0.g.z(this.$challengeId);
                if (z != null) {
                    List list = this.$players;
                    if (!(list == null || list.isEmpty())) {
                        this.this$0.g.K(py4.b(z, this.$players));
                    }
                }
                return tl7.f3441a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends TimerTask {
        @DexIgnore
        public /* final */ /* synthetic */ iv4 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(qn7 qn7, i iVar) {
                super(2, qn7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    FLogger.INSTANCE.getLocal().e(this.this$0.b.f1674a, "waitingChallengeEnd - end challenge");
                    Object e = this.this$0.b.b.e();
                    if (!(e instanceof List)) {
                        e = null;
                    }
                    List<ms4> list = (List) e;
                    if (list != null) {
                        this.this$0.b.g.C(list);
                    }
                    this.this$0.b.e.l("completed");
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public i(iv4 iv4) {
            this.b = iv4;
        }

        @DexIgnore
        public void run() {
            xw7 unused = gu7.d(us0.a(this.b), bw7.b(), null, new a(null, this), 2, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel", f = "BCLeaderBoardViewModel.kt", l = {72}, m = "waitingChallengeEnd")
    public static final class j extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ iv4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(iv4 iv4, qn7 qn7) {
            super(qn7);
            this.this$0 = iv4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.leaderboard.BCLeaderBoardViewModel$waitingChallengeEnd$challenge$1", f = "BCLeaderBoardViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class k extends ko7 implements vp7<iv7, qn7<? super ps4>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ iv4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(iv4 iv4, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = iv4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            k kVar = new k(this.this$0, this.$challengeId, qn7);
            kVar.p$ = (iv7) obj;
            throw null;
            //return kVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super ps4> qn7) {
            throw null;
            //return ((k) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return this.this$0.g.a(this.$challengeId);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public iv4(tt4 tt4) {
        pq7.c(tt4, "challengeRepository");
        this.g = tt4;
        String simpleName = iv4.class.getSimpleName();
        pq7.b(simpleName, "BCLeaderBoardViewModel::class.java.simpleName");
        this.f1674a = simpleName;
    }

    @DexIgnore
    public final LiveData<String> f() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, ServerError>> g() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Boolean> h() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<List<Object>> i() {
        return this.b;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object j(java.lang.String r7, com.fossil.qn7<? super com.fossil.tl7> r8) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.fossil.iv4.a
            if (r0 == 0) goto L_0x005a
            r0 = r8
            com.fossil.iv4$a r0 = (com.fossil.iv4.a) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x005a
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0069
            if (r3 != r5) goto L_0x0061
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.iv4 r0 = (com.fossil.iv4) r0
            com.fossil.el7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.fossil.bt4 r0 = (com.fossil.bt4) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = r6.f1674a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "initPlayers - historyChallenge: "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.e(r2, r3)
            if (r0 == 0) goto L_0x0057
            androidx.lifecycle.MutableLiveData<java.util.List<java.lang.Object>> r1 = r6.b
            java.util.List r0 = r0.k()
            r1.l(r0)
        L_0x0057:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0059:
            return r0
        L_0x005a:
            com.fossil.iv4$a r0 = new com.fossil.iv4$a
            r0.<init>(r6, r8)
            r1 = r0
            goto L_0x0014
        L_0x0061:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0069:
            com.fossil.el7.b(r2)
            com.fossil.dv7 r2 = com.fossil.bw7.b()
            com.fossil.iv4$b r3 = new com.fossil.iv4$b
            r4 = 0
            r3.<init>(r6, r7, r4)
            r1.L$0 = r6
            r1.L$1 = r7
            r1.label = r5
            java.lang.Object r1 = com.fossil.eu7.g(r2, r3, r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.iv4.j(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void k(String str, String str2) {
        if (str != null) {
            xw7 unused = gu7.d(us0.a(this), null, null, new c(this, str, str2, null), 3, null);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0141  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0165  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object l(java.lang.String r13, com.fossil.qn7<? super com.fossil.tl7> r14) {
        /*
        // Method dump skipped, instructions count: 404
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.iv4.l(java.lang.String, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void m(String str) {
        pq7.c(str, "challengeId");
        xw7 unused = gu7.d(us0.a(this), null, null, new g(this, str, null), 3, null);
    }

    @DexIgnore
    public final /* synthetic */ Object n(String str, List<ms4> list, qn7<? super tl7> qn7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.f1674a;
        local.e(str2, "updateHistoryChallenge - challengeId: " + str);
        Object g2 = eu7.g(bw7.b(), new h(this, str, list, null), qn7);
        return g2 == yn7.d() ? g2 : tl7.f3441a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object o(java.lang.String r9, com.fossil.qn7<? super com.fossil.tl7> r10) {
        /*
            r8 = this;
            r7 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.fossil.iv4.j
            if (r0 == 0) goto L_0x0061
            r0 = r10
            com.fossil.iv4$j r0 = (com.fossil.iv4.j) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0061
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0016:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0070
            if (r3 != r7) goto L_0x0068
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.fossil.iv4 r0 = (com.fossil.iv4) r0
            com.fossil.el7.b(r2)
            r1 = r2
            r8 = r0
        L_0x002f:
            r0 = r1
            com.fossil.ps4 r0 = (com.fossil.ps4) r0
            if (r0 == 0) goto L_0x005e
            java.util.Date r0 = r0.e()
            if (r0 == 0) goto L_0x008a
            long r0 = r0.getTime()
            java.lang.Long r0 = com.fossil.ao7.f(r0)
            if (r0 == 0) goto L_0x008a
            long r0 = r0.longValue()
        L_0x0048:
            int r2 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r2 >= 0) goto L_0x004d
            r0 = r4
        L_0x004d:
            java.util.Timer r2 = new java.util.Timer
            r2.<init>()
            r8.f = r2
            if (r2 == 0) goto L_0x005e
            com.fossil.iv4$i r3 = new com.fossil.iv4$i
            r3.<init>(r8)
            r2.schedule(r3, r0)
        L_0x005e:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x0060:
            return r0
        L_0x0061:
            com.fossil.iv4$j r0 = new com.fossil.iv4$j
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0016
        L_0x0068:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0070:
            com.fossil.el7.b(r2)
            com.fossil.dv7 r2 = com.fossil.bw7.b()
            com.fossil.iv4$k r3 = new com.fossil.iv4$k
            r6 = 0
            r3.<init>(r8, r9, r6)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.label = r7
            java.lang.Object r1 = com.fossil.eu7.g(r2, r3, r1)
            if (r1 != r0) goto L_0x002f
            goto L_0x0060
        L_0x008a:
            com.fossil.xy4 r0 = com.fossil.xy4.f4212a
            long r0 = r0.b()
            long r0 = r4 - r0
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.iv4.o(java.lang.String, com.fossil.qn7):java.lang.Object");
    }
}
