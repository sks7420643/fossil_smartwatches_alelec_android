package com.fossil;

import com.zendesk.service.ErrorResponse;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pj7 implements ErrorResponse {
    @DexIgnore
    public Throwable a;
    @DexIgnore
    public Q88 b;

    @DexIgnore
    public Pj7(Q88 q88) {
        this.b = q88;
    }

    @DexIgnore
    public Pj7(Throwable th) {
        this.a = th;
    }

    @DexIgnore
    public static Pj7 d(Q88 q88) {
        return new Pj7(q88);
    }

    @DexIgnore
    public static Pj7 e(Throwable th) {
        return new Pj7(th);
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public int a() {
        Q88 q88 = this.b;
        if (q88 != null) {
            return q88.b();
        }
        return -1;
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public String b() {
        Throwable th = this.a;
        if (th != null) {
            return th.getMessage();
        }
        StringBuilder sb = new StringBuilder();
        Q88 q88 = this.b;
        if (q88 != null) {
            if (Tj7.b(q88.f())) {
                sb.append(this.b.f());
            } else {
                sb.append(this.b.b());
            }
        }
        return sb.toString();
    }

    @DexIgnore
    @Override // com.zendesk.service.ErrorResponse
    public boolean c() {
        Throwable th = this.a;
        return th != null && (th instanceof IOException);
    }
}
