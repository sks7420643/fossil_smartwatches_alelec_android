package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class R87 {
    @DexIgnore
    public static final O87 a(int i) {
        return i != 0 ? i != 1 ? i != 2 ? i != 3 ? O87.Companion.a() : O87.DARK_GRAY : O87.LIGHT_GRAY : O87.WHITE : O87.BLACK;
    }

    @DexIgnore
    public static final O87 b(Nb7 nb7) {
        Wg6.c(nb7, "$this$toColorSpace");
        int i = Q87.a[nb7.ordinal()];
        return i != 1 ? i != 2 ? i != 3 ? i != 4 ? O87.Companion.a() : O87.DARK_GRAY : O87.LIGHT_GRAY : O87.WHITE : O87.BLACK;
    }
}
