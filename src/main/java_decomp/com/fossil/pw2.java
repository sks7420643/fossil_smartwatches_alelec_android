package com.fossil;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pw2<T> extends Tw2<T> {
    @DexIgnore
    public static /* final */ Pw2<Object> zza; // = new Pw2<>();

    @DexIgnore
    public final boolean equals(@NullableDecl Object obj) {
        return obj == this;
    }

    @DexIgnore
    public final int hashCode() {
        return 2040732332;
    }

    @DexIgnore
    public final String toString() {
        return "Optional.absent()";
    }

    @DexIgnore
    @Override // com.fossil.Tw2
    public final boolean zza() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Tw2
    public final T zzb() {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }
}
