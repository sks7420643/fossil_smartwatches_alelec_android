package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ln0<F, S> {
    @DexIgnore
    public /* final */ F a;
    @DexIgnore
    public /* final */ S b;

    @DexIgnore
    public Ln0(F f, S s) {
        this.a = f;
        this.b = s;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof Ln0)) {
            return false;
        }
        Ln0 ln0 = (Ln0) obj;
        return Kn0.a(ln0.a, this.a) && Kn0.a(ln0.b, this.b);
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        F f = this.a;
        int hashCode = f == null ? 0 : f.hashCode();
        S s = this.b;
        if (s != null) {
            i = s.hashCode();
        }
        return hashCode ^ i;
    }

    @DexIgnore
    public String toString() {
        return "Pair{" + String.valueOf(this.a) + " " + String.valueOf(this.b) + "}";
    }
}
