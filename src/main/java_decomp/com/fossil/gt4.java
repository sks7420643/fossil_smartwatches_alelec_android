package com.fossil;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gt4 implements Ft4 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Hh<Dt4> b;
    @DexIgnore
    public /* final */ Et4 c; // = new Et4();
    @DexIgnore
    public /* final */ Vh d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hh<Dt4> {
        @DexIgnore
        public Ai(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, Dt4 dt4) {
            if (dt4.e() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, dt4.e());
            }
            if (dt4.i() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, dt4.i());
            }
            if (dt4.a() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, dt4.a());
            }
            String b = Gt4.this.c.b(dt4.d());
            if (b == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, b);
            }
            String a2 = Gt4.this.c.a(dt4.b());
            if (a2 == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, a2);
            }
            String c = Gt4.this.c.c(dt4.f());
            if (c == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, c);
            }
            mi.bindLong(7, dt4.c() ? 1 : 0);
            mi.bindLong(8, (long) dt4.g());
            String b2 = Gt4.this.c.b(dt4.h());
            if (b2 == null) {
                mi.bindNull(9);
            } else {
                mi.bindString(9, b2);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, Dt4 dt4) {
            a(mi, dt4);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `notification` (`id`,`titleKey`,`bodyKey`,`createdAt`,`challengeData`,`profileData`,`confirmChallenge`,`rank`,`reachGoalAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Vh {
        @DexIgnore
        public Bi(Gt4 gt4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM notification WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Vh {
        @DexIgnore
        public Ci(Gt4 gt4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM notification";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements Callable<List<Dt4>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh a;

        @DexIgnore
        public Di(Rh rh) {
            this.a = rh;
        }

        @DexIgnore
        public List<Dt4> a() throws Exception {
            Cursor b2 = Ex0.b(Gt4.this.a, this.a, false, null);
            try {
                int c = Dx0.c(b2, "id");
                int c2 = Dx0.c(b2, "titleKey");
                int c3 = Dx0.c(b2, "bodyKey");
                int c4 = Dx0.c(b2, "createdAt");
                int c5 = Dx0.c(b2, "challengeData");
                int c6 = Dx0.c(b2, "profileData");
                int c7 = Dx0.c(b2, "confirmChallenge");
                int c8 = Dx0.c(b2, "rank");
                int c9 = Dx0.c(b2, "reachGoalAt");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    arrayList.add(new Dt4(b2.getString(c), b2.getString(c2), b2.getString(c3), Gt4.this.c.f(b2.getString(c4)), Gt4.this.c.d(b2.getString(c5)), Gt4.this.c.e(b2.getString(c6)), b2.getInt(c7) != 0, b2.getInt(c8), Gt4.this.c.f(b2.getString(c9))));
                }
                return arrayList;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ List<Dt4> call() throws Exception {
            return a();
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.m();
        }
    }

    @DexIgnore
    public Gt4(Oh oh) {
        this.a = oh;
        this.b = new Ai(oh);
        new Bi(this, oh);
        this.d = new Ci(this, oh);
    }

    @DexIgnore
    @Override // com.fossil.Ft4
    public void a() {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.d.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ft4
    public LiveData<List<Dt4>> b() {
        Rh f = Rh.f("SELECT * FROM notification ORDER BY createdAt DESC", 0);
        Nw0 invalidationTracker = this.a.getInvalidationTracker();
        Di di = new Di(f);
        return invalidationTracker.d(new String[]{"notification"}, false, di);
    }

    @DexIgnore
    @Override // com.fossil.Ft4
    public Long[] insert(List<Dt4> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }
}
