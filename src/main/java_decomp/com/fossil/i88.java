package com.fossil;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I88 {
    @DexIgnore
    public /* final */ Method a;
    @DexIgnore
    public /* final */ List<?> b;

    @DexIgnore
    public I88(Method method, List<?> list) {
        this.a = method;
        this.b = Collections.unmodifiableList(list);
    }

    @DexIgnore
    public Method a() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return String.format("%s.%s() %s", this.a.getDeclaringClass().getName(), this.a.getName(), this.b);
    }
}
