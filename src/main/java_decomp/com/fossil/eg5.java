package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Eg5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView A;
    @DexIgnore
    public /* final */ RTLImageView B;
    @DexIgnore
    public /* final */ RTLImageView C;
    @DexIgnore
    public /* final */ RTLImageView D;
    @DexIgnore
    public /* final */ ImageView E;
    @DexIgnore
    public /* final */ ImageView F;
    @DexIgnore
    public /* final */ RTLImageView G;
    @DexIgnore
    public /* final */ ImageView H;
    @DexIgnore
    public /* final */ FlexibleProgressBar I;
    @DexIgnore
    public /* final */ View J;
    @DexIgnore
    public /* final */ View K;
    @DexIgnore
    public /* final */ View L;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ RTLImageView z;

    @DexIgnore
    public Eg5(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, RTLImageView rTLImageView4, RTLImageView rTLImageView5, ImageView imageView, ImageView imageView2, RTLImageView rTLImageView6, ImageView imageView3, FlexibleProgressBar flexibleProgressBar, View view2, View view3, View view4) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = flexibleTextView3;
        this.u = flexibleTextView4;
        this.v = flexibleTextView5;
        this.w = flexibleTextView6;
        this.x = flexibleTextView7;
        this.y = flexibleTextView8;
        this.z = rTLImageView;
        this.A = rTLImageView2;
        this.B = rTLImageView3;
        this.C = rTLImageView4;
        this.D = rTLImageView5;
        this.E = imageView;
        this.F = imageView2;
        this.G = rTLImageView6;
        this.H = imageView3;
        this.I = flexibleProgressBar;
        this.J = view2;
        this.K = view3;
        this.L = view4;
    }

    @DexIgnore
    @Deprecated
    public static Eg5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z2, Object obj) {
        return (Eg5) ViewDataBinding.p(layoutInflater, 2131558728, viewGroup, z2, obj);
    }

    @DexIgnore
    public static Eg5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z2) {
        return A(layoutInflater, viewGroup, z2, Aq0.d());
    }
}
