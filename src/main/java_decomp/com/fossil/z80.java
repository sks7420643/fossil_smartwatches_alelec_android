package com.fossil;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import com.fossil.common.cipher.TextEncryption;
import com.mapped.Cd6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Z80 extends HandlerThread {
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public /* final */ P80 c;
    @DexIgnore
    public /* final */ U80 d;
    @DexIgnore
    public /* final */ Cd6 e; // = Cd6.a;
    @DexIgnore
    public /* final */ Ld0 f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public Z80(String str, int i, long j, String str2, String str3, Zw1 zw1, int i2, B90 b90, Ld0 ld0, boolean z) {
        super(Z80.class.getSimpleName(), 10);
        this.f = ld0;
        this.g = z;
        super.start();
        Handler handler = new Handler(getLooper());
        this.b = handler;
        P80 p80 = new P80(str, i, j, handler, str2, str3);
        this.c = p80;
        U80 u80 = new U80(p80, zw1, b90, this.b, this.g);
        this.d = u80;
        if (i2 > 0) {
            long j2 = ((long) i2) * ((long) 1000);
            Q80 q80 = u80.d;
            if (q80 != null) {
                q80.b = true;
                u80.j.removeCallbacks(q80);
            }
            if (j2 > 0) {
                u80.f = j2;
            }
            Q80 q802 = new Q80(u80);
            u80.d = q802;
            if (q802 != null) {
                u80.j.post(q802);
            }
        }
    }

    @DexIgnore
    public static /* synthetic */ void c(Z80 z80, long j, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                j = 0;
            }
            z80.b(j);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: startUploadLog");
    }

    @DexIgnore
    public final /* bridge */ /* synthetic */ long a() {
        return h();
    }

    @DexIgnore
    public final void b(long j) {
        U80 u80 = this.d;
        Md0 md0 = u80.e;
        if (md0 != null) {
            md0.b = true;
            u80.j.removeCallbacks(md0);
        }
        Md0 md02 = new Md0(new R80(u80));
        u80.e = md02;
        if (md02 != null) {
            u80.j.postDelayed(md02, j);
        }
    }

    @DexIgnore
    public boolean d(A90 a90) {
        boolean f2 = f(a90);
        if (e() > 0) {
            b(e());
        }
        return f2;
    }

    @DexIgnore
    public abstract long e();

    @DexIgnore
    public final boolean f(A90 a90) {
        try {
            a90.c = a();
            a90.m = C90.e.a();
            Zk1 b2 = C90.e.b(a90.a());
            if (b2 != null) {
                a90.l = b2;
            }
            a90.k = C90.e.f(a90.a());
            String o = TextEncryption.j.o(a90.toJSONString(0));
            if (o != null) {
                return this.c.c(o);
            }
            return false;
        } catch (OutOfMemoryError e2) {
            return false;
        }
    }

    @DexIgnore
    public final long g() {
        return this.c.h();
    }

    @DexIgnore
    @SuppressLint({"ApplySharedPref"})
    public final long h() {
        long longValue;
        SharedPreferences.Editor edit;
        SharedPreferences.Editor putLong;
        synchronized (this.e) {
            SharedPreferences b2 = G80.b(this.f);
            Long valueOf = b2 != null ? Long.valueOf(b2.getLong("log_line_number", Long.MAX_VALUE)) : null;
            Long valueOf2 = (valueOf == null || valueOf.longValue() == Long.MAX_VALUE) ? 0L : Long.valueOf(valueOf.longValue() + 1);
            if (!(b2 == null || (edit = b2.edit()) == null || (putLong = edit.putLong("log_line_number", valueOf2.longValue())) == null)) {
                putLong.commit();
            }
            longValue = valueOf2.longValue();
        }
        return longValue;
    }

    @DexIgnore
    public final void start() {
        super.start();
    }
}
