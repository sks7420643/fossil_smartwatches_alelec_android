package com.fossil;

import android.database.Cursor;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K31 implements J31 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Hh<I31> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hh<I31> {
        @DexIgnore
        public Ai(K31 k31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, I31 i31) {
            String str = i31.a;
            if (str == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, str);
            }
            String str2 = i31.b;
            if (str2 == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, str2);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, I31 i31) {
            a(mi, i31);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkName` (`name`,`work_spec_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public K31(Oh oh) {
        this.a = oh;
        this.b = new Ai(this, oh);
    }

    @DexIgnore
    @Override // com.fossil.J31
    public void a(I31 i31) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert((Hh<I31>) i31);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.J31
    public List<String> b(String str) {
        Rh f = Rh.f("SELECT name FROM workname WHERE work_spec_id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f, false, null);
        try {
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                arrayList.add(b2.getString(0));
            }
            return arrayList;
        } finally {
            b2.close();
            f.m();
        }
    }
}
