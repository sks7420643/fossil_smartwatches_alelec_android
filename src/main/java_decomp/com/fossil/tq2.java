package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tq2 extends Mq2 implements Rq2 {
    @DexIgnore
    public Tq2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IFusedLocationProviderCallback");
    }

    @DexIgnore
    @Override // com.fossil.Rq2
    public final void M(Pq2 pq2) throws RemoteException {
        Parcel d = d();
        Qr2.c(d, pq2);
        n(1, d);
    }
}
