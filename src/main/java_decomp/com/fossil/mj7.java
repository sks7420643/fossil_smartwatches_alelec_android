package com.fossil;

import android.os.Build;
import android.util.Log;
import com.zendesk.service.ErrorResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import retrofit.android.AndroidLog;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mj7 {
    @DexIgnore
    public static /* final */ TimeZone a; // = TimeZone.getTimeZone("UTC");
    @DexIgnore
    public static /* final */ List<Ci> b; // = new ArrayList();
    @DexIgnore
    public static Ci c;
    @DexIgnore
    public static boolean d; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Ci {
        @DexIgnore
        @Override // com.fossil.Mj7.Ci
        public void a(Di di, String str, String str2, Throwable th) {
            String a2 = Nj7.a(str);
            if (b(str) && Di.ERROR == di) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
                simpleDateFormat.setTimeZone(Mj7.a);
                Log.println(Di.ERROR.priority, a2, "Time in UTC: " + simpleDateFormat.format(new Date()));
            }
            if (th != null) {
                str2 = str2 + Tj7.b + Log.getStackTraceString(th);
            }
            for (String str3 : Nj7.c(str2, AndroidLog.LOG_CHUNK_SIZE)) {
                Log.println(di == null ? Di.INFO.priority : di.priority, a2, str3);
            }
        }

        @DexIgnore
        public final boolean b(String str) {
            return Tj7.b(str) && (str.endsWith("Provider") || str.endsWith("Service"));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements Ci {
        @DexIgnore
        @Override // com.fossil.Mj7.Ci
        public void a(Di di, String str, String str2, Throwable th) {
            StringBuilder sb = new StringBuilder(100);
            sb.append("[");
            sb.append(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US).format(new Date()));
            sb.append("]");
            sb.append(" ");
            if (di == null) {
                di = Di.INFO;
            }
            sb.append(Nj7.b(di.priority));
            sb.append("/");
            if (!Tj7.b(str)) {
                str = "UNKNOWN";
            }
            sb.append(str);
            sb.append(": ");
            sb.append(str2);
            System.out.println(sb.toString());
            if (th != null) {
                th.printStackTrace(System.out);
            }
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        void a(Di di, String str, String str2, Throwable th);
    }

    @DexIgnore
    public enum Di {
        VERBOSE(2),
        DEBUG(3),
        INFO(4),
        WARN(5),
        ERROR(6);
        
        @DexIgnore
        public /* final */ int priority;

        @DexIgnore
        public Di(int i) {
            this.priority = i;
        }
    }

    /*
    static {
        Bi bi;
        try {
            Class.forName("android.os.Build");
            if (Build.VERSION.SDK_INT != 0) {
                c = new Ai();
            }
            if (c == null) {
                bi = new Bi();
                c = bi;
            }
        } catch (ClassNotFoundException e) {
            if (c == null) {
                bi = new Bi();
            }
        } catch (Throwable th) {
            if (c == null) {
                c = new Bi();
            }
            throw th;
        }
    }
    */

    @DexIgnore
    public static void b(String str, String str2, Throwable th, Object... objArr) {
        i(Di.DEBUG, str, str2, th, objArr);
    }

    @DexIgnore
    public static void c(String str, String str2, Object... objArr) {
        i(Di.DEBUG, str, str2, null, objArr);
    }

    @DexIgnore
    public static void d(String str, ErrorResponse errorResponse) {
        StringBuilder sb = new StringBuilder();
        if (errorResponse != null) {
            sb.append("Network Error: ");
            sb.append(errorResponse.c());
            sb.append(", Status Code: ");
            sb.append(errorResponse.a());
            if (Tj7.b(errorResponse.b())) {
                sb.append(", Reason: ");
                sb.append(errorResponse.b());
            }
        }
        String sb2 = sb.toString();
        Di di = Di.ERROR;
        if (!Tj7.b(sb2)) {
            sb2 = "Unknown error";
        }
        i(di, str, sb2, null, new Object[0]);
    }

    @DexIgnore
    public static void e(String str, String str2, Throwable th, Object... objArr) {
        i(Di.ERROR, str, str2, th, objArr);
    }

    @DexIgnore
    public static void f(String str, String str2, Object... objArr) {
        i(Di.ERROR, str, str2, null, objArr);
    }

    @DexIgnore
    public static void g(String str, String str2, Object... objArr) {
        i(Di.INFO, str, str2, null, objArr);
    }

    @DexIgnore
    public static boolean h() {
        return d;
    }

    @DexIgnore
    public static void i(Di di, String str, String str2, Throwable th, Object... objArr) {
        if (objArr != null && objArr.length > 0) {
            str2 = String.format(Locale.US, str2, objArr);
        }
        if (d) {
            c.a(di, str, str2, th);
            for (Ci ci : b) {
                ci.a(di, str, str2, th);
            }
        }
    }

    @DexIgnore
    public static void j(boolean z) {
        d = z;
    }

    @DexIgnore
    public static void k(String str, String str2, Throwable th, Object... objArr) {
        i(Di.WARN, str, str2, th, objArr);
    }

    @DexIgnore
    public static void l(String str, String str2, Object... objArr) {
        i(Di.WARN, str, str2, null, objArr);
    }
}
