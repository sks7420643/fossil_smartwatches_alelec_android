package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sa3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Sa3> CREATOR; // = new Ta3();
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public Sa3(String str, String str2, String str3) {
        this.d = str;
        this.b = str2;
        this.c = str3;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.u(parcel, 1, this.b, false);
        Bd2.u(parcel, 2, this.c, false);
        Bd2.u(parcel, 5, this.d, false);
        Bd2.b(parcel, a2);
    }
}
