package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F03 {
    @DexIgnore
    public /* final */ L03 a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore
    public F03(int i) {
        byte[] bArr = new byte[i];
        this.b = bArr;
        this.a = L03.f(bArr);
    }

    @DexIgnore
    public /* synthetic */ F03(int i, Wz2 wz2) {
        this(i);
    }

    @DexIgnore
    public final Xz2 a() {
        this.a.N();
        return new H03(this.b);
    }

    @DexIgnore
    public final L03 b() {
        return this.a;
    }
}
