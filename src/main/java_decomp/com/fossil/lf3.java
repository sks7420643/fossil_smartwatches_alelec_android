package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lf3 implements Parcelable.Creator<GoogleMapOptions> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ GoogleMapOptions createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        byte b = -1;
        byte b2 = -1;
        int i = 0;
        byte b3 = -1;
        byte b4 = -1;
        byte b5 = -1;
        byte b6 = -1;
        byte b7 = -1;
        byte b8 = -1;
        byte b9 = -1;
        byte b10 = -1;
        byte b11 = -1;
        byte b12 = -1;
        LatLngBounds latLngBounds = null;
        Float f = null;
        Float f2 = null;
        CameraPosition cameraPosition = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 2:
                    b = Ad2.o(parcel, t);
                    break;
                case 3:
                    b2 = Ad2.o(parcel, t);
                    break;
                case 4:
                    i = Ad2.v(parcel, t);
                    break;
                case 5:
                    cameraPosition = (CameraPosition) Ad2.e(parcel, t, CameraPosition.CREATOR);
                    break;
                case 6:
                    b3 = Ad2.o(parcel, t);
                    break;
                case 7:
                    b4 = Ad2.o(parcel, t);
                    break;
                case 8:
                    b5 = Ad2.o(parcel, t);
                    break;
                case 9:
                    b6 = Ad2.o(parcel, t);
                    break;
                case 10:
                    b7 = Ad2.o(parcel, t);
                    break;
                case 11:
                    b8 = Ad2.o(parcel, t);
                    break;
                case 12:
                    b9 = Ad2.o(parcel, t);
                    break;
                case 13:
                default:
                    Ad2.B(parcel, t);
                    break;
                case 14:
                    b10 = Ad2.o(parcel, t);
                    break;
                case 15:
                    b11 = Ad2.o(parcel, t);
                    break;
                case 16:
                    f2 = Ad2.s(parcel, t);
                    break;
                case 17:
                    f = Ad2.s(parcel, t);
                    break;
                case 18:
                    latLngBounds = (LatLngBounds) Ad2.e(parcel, t, LatLngBounds.CREATOR);
                    break;
                case 19:
                    b12 = Ad2.o(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new GoogleMapOptions(b, b2, i, cameraPosition, b3, b4, b5, b6, b7, b8, b9, b10, b11, f2, f, latLngBounds, b12);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ GoogleMapOptions[] newArray(int i) {
        return new GoogleMapOptions[i];
    }
}
