package com.fossil;

import com.mapped.Lc6;
import com.mapped.Td0;
import com.mapped.Ud0;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P90 {
    @DexIgnore
    public static /* final */ HashMap<Integer, Lc6<Td0, Ud0>> a; // = Zm7.i(Hl7.a(3073, new Lc6(Td0.RING_PHONE, Ud0.STANDARD)), Hl7.a(6657, new Lc6(Td0.ALARM, Ud0.STANDARD)), Hl7.a(6658, new Lc6(Td0.ALARM, Ud0.SEQUENCED)), Hl7.a(7169, new Lc6(Td0.PROGRESS, Ud0.STANDARD)), Hl7.a(7170, new Lc6(Td0.PROGRESS, Ud0.SWEEP)), Hl7.a(7681, new Lc6(Td0.TWENTY_FOUR_HOUR, Ud0.STANDARD)), Hl7.a(7682, new Lc6(Td0.TWENTY_FOUR_HOUR, Ud0.SEQUENCED)), Hl7.a(1025, new Lc6(Td0.GOAL_TRACKING, Ud0.STANDARD)), Hl7.a(4097, new Lc6(Td0.SELFIE, Ud0.STANDARD)), Hl7.a(4609, new Lc6(Td0.MUSIC_CONTROL, Ud0.PLAY_PAUSE)), Hl7.a(4610, new Lc6(Td0.MUSIC_CONTROL, Ud0.NEXT)), Hl7.a(4611, new Lc6(Td0.MUSIC_CONTROL, Ud0.PREVIOUS)), Hl7.a(4612, new Lc6(Td0.MUSIC_CONTROL, Ud0.VOLUME_UP)), Hl7.a(4613, new Lc6(Td0.MUSIC_CONTROL, Ud0.VOLUME_DOWN)), Hl7.a(4614, new Lc6(Td0.MUSIC_CONTROL, Ud0.STANDARD)), Hl7.a(5121, new Lc6(Td0.DATE, Ud0.STANDARD)), Hl7.a(5122, new Lc6(Td0.DATE, Ud0.SEQUENCED)), Hl7.a(5633, new Lc6(Td0.TIME2, Ud0.STANDARD)), Hl7.a(5634, new Lc6(Td0.TIME2, Ud0.SEQUENCED)), Hl7.a(6145, new Lc6(Td0.ALERT, Ud0.STANDARD)), Hl7.a(6146, new Lc6(Td0.ALERT, Ud0.SEQUENCED)), Hl7.a(8193, new Lc6(Td0.STOPWATCH, Ud0.STANDARD)), Hl7.a(9217, new Lc6(Td0.COMMUTE_TIME, Ud0.TRAVEL)), Hl7.a(9218, new Lc6(Td0.COMMUTE_TIME, Ud0.ETA)));
    @DexIgnore
    public static /* final */ P90 b; // = new P90();

    @DexIgnore
    public final Td0 a(short s) {
        Lc6<Td0, Ud0> lc6 = a.get(Integer.valueOf(s));
        return lc6 != null ? lc6.getFirst() : Td0.UNDEFINED;
    }

    @DexIgnore
    public final Ud0 b(short s) {
        Lc6<Td0, Ud0> lc6 = a.get(Integer.valueOf(s));
        return lc6 != null ? lc6.getSecond() : Ud0.UNDEFINED;
    }
}
