package com.fossil;

import com.fossil.Bu0;
import com.mapped.V3;
import com.mapped.Xe;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Gu0<T> extends Xe<Integer, T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<Value> extends Vt0<Integer, Value> {
        @DexIgnore
        public /* final */ Gu0<Value> a;

        @DexIgnore
        public Ai(Gu0<Value> gu0) {
            this.a = gu0;
        }

        @DexIgnore
        public void a(Integer num, int i, int i2, boolean z, Executor executor, Bu0.Ai<Value> ai) {
            int max;
            Integer valueOf;
            if (num == null) {
                max = i;
                valueOf = 0;
            } else {
                max = Math.max(i / i2, 2) * i2;
                valueOf = Integer.valueOf(Math.max(0, ((num.intValue() - (max / 2)) / i2) * i2));
            }
            this.a.dispatchLoadInitial(false, valueOf.intValue(), max, i2, executor, ai);
        }

        @DexIgnore
        @Override // com.mapped.Xe
        public void addInvalidatedCallback(Xe.Ci ci) {
            this.a.addInvalidatedCallback(ci);
        }

        @DexIgnore
        public Integer b(int i, Value value) {
            return Integer.valueOf(i);
        }

        @DexIgnore
        @Override // com.fossil.Vt0
        public void dispatchLoadAfter(int i, Value value, int i2, Executor executor, Bu0.Ai<Value> ai) {
            this.a.dispatchLoadRange(1, i + 1, i2, executor, ai);
        }

        @DexIgnore
        @Override // com.fossil.Vt0
        public void dispatchLoadBefore(int i, Value value, int i2, Executor executor, Bu0.Ai<Value> ai) {
            int i3 = i - 1;
            if (i3 < 0) {
                this.a.dispatchLoadRange(2, i3, 0, executor, ai);
                return;
            }
            int min = Math.min(i2, i3 + 1);
            this.a.dispatchLoadRange(2, (i3 - min) + 1, min, executor, ai);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, boolean, java.util.concurrent.Executor, com.fossil.Bu0$Ai] */
        @Override // com.fossil.Vt0
        public /* bridge */ /* synthetic */ void dispatchLoadInitial(Integer num, int i, int i2, boolean z, Executor executor, Bu0.Ai ai) {
            a(num, i, i2, z, executor, ai);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.Vt0
        public /* bridge */ /* synthetic */ Integer getKey(int i, Object obj) {
            return b(i, obj);
        }

        @DexIgnore
        @Override // com.mapped.Xe
        public void invalidate() {
            this.a.invalidate();
        }

        @DexIgnore
        @Override // com.mapped.Xe
        public boolean isInvalid() {
            return this.a.isInvalid();
        }

        @DexIgnore
        @Override // com.mapped.Xe
        public <ToValue> Xe<Integer, ToValue> map(V3<Value, ToValue> v3) {
            throw new UnsupportedOperationException("Inaccessible inner type doesn't support map op");
        }

        @DexIgnore
        @Override // com.mapped.Xe
        public <ToValue> Xe<Integer, ToValue> mapByPage(V3<List<Value>, List<ToValue>> v3) {
            throw new UnsupportedOperationException("Inaccessible inner type doesn't support map op");
        }

        @DexIgnore
        @Override // com.mapped.Xe
        public void removeInvalidatedCallback(Xe.Ci ci) {
            this.a.removeInvalidatedCallback(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Bi<T> {
        @DexIgnore
        public abstract void a(List<T> list, int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci<T> extends Bi<T> {
        @DexIgnore
        public /* final */ Xe.Di<T> a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public Ci(Gu0 gu0, boolean z, int i, Bu0.Ai<T> ai) {
            this.a = new Xe.Di<>(gu0, 0, null, ai);
            this.b = z;
            this.c = i;
            if (i < 1) {
                throw new IllegalArgumentException("Page size must be non-negative");
            }
        }

        @DexIgnore
        @Override // com.fossil.Gu0.Bi
        public void a(List<T> list, int i, int i2) {
            if (!this.a.a()) {
                Xe.Di.d(list, i, i2);
                if (list.size() + i != i2 && list.size() % this.c != 0) {
                    throw new IllegalArgumentException("PositionalDataSource requires initial load size to be a multiple of page size to support internal tiling. loadSize " + list.size() + ", position " + i + ", totalCount " + i2 + ", pageSize " + this.c);
                } else if (this.b) {
                    this.a.b(new Bu0<>(list, i, (i2 - i) - list.size(), 0));
                } else {
                    this.a.b(new Bu0<>(list, i));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public Di(int i, int i2, int i3, boolean z) {
            this.a = i;
            this.b = i2;
            this.c = i3;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ei<T> {
        @DexIgnore
        public abstract void a(List<T> list);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Fi<T> extends Ei<T> {
        @DexIgnore
        public Xe.Di<T> a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Fi(Gu0 gu0, int i, int i2, Executor executor, Bu0.Ai<T> ai) {
            this.a = new Xe.Di<>(gu0, i, executor, ai);
            this.b = i2;
        }

        @DexIgnore
        @Override // com.fossil.Gu0.Ei
        public void a(List<T> list) {
            if (!this.a.a()) {
                this.a.b(new Bu0<>(list, 0, 0, this.b));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Gi {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Gi(int i, int i2) {
            this.a = i;
            this.b = i2;
        }
    }

    @DexIgnore
    public static int computeInitialLoadPosition(Di di, int i) {
        int i2 = di.a;
        int i3 = di.b;
        int i4 = di.c;
        return Math.max(0, Math.min(((((i - i3) + i4) - 1) / i4) * i4, (i2 / i4) * i4));
    }

    @DexIgnore
    public static int computeInitialLoadSize(Di di, int i, int i2) {
        return Math.min(i2 - i, di.b);
    }

    @DexIgnore
    public final void dispatchLoadInitial(boolean z, int i, int i2, int i3, Executor executor, Bu0.Ai<T> ai) {
        Ci ci = new Ci(this, z, i3, ai);
        loadInitial(new Di(i, i2, i3, z), ci);
        ci.a.c(executor);
    }

    @DexIgnore
    public final void dispatchLoadRange(int i, int i2, int i3, Executor executor, Bu0.Ai<T> ai) {
        Fi fi = new Fi(this, i, i2, executor, ai);
        if (i3 == 0) {
            fi.a(Collections.emptyList());
        } else {
            loadRange(new Gi(i2, i3), fi);
        }
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public boolean isContiguous() {
        return false;
    }

    @DexIgnore
    public abstract void loadInitial(Di di, Bi<T> bi);

    @DexIgnore
    public abstract void loadRange(Gi gi, Ei<T> ei);

    @DexIgnore
    @Override // com.mapped.Xe
    public final <V> Gu0<V> map(V3<T, V> v3) {
        return mapByPage((V3) Xe.createListFunction(v3));
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public final <V> Gu0<V> mapByPage(V3<List<T>, List<V>> v3) {
        return new Lu0(this, v3);
    }

    @DexIgnore
    public Vt0<Integer, T> wrapAsContiguousWithoutPlaceholders() {
        return new Ai(this);
    }
}
