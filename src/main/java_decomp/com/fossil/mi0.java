package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.Td0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mi0 extends Service {
    @DexIgnore
    public Td0.Ai b; // = new Ai(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Td0.Ai {
        @DexIgnore
        public Ai(Mi0 mi0) {
        }

        @DexIgnore
        @Override // com.fossil.Td0
        public void G0(Rd0 rd0, Bundle bundle) throws RemoteException {
            rd0.G2(bundle);
        }

        @DexIgnore
        @Override // com.fossil.Td0
        public void g1(Rd0 rd0, String str, Bundle bundle) throws RemoteException {
            rd0.A2(str, bundle);
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return this.b;
    }
}
