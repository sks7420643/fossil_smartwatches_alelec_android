package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O51 {
    @DexIgnore
    public int[] a;
    @DexIgnore
    public int b;

    @DexIgnore
    public O51(int i) {
        this.a = new int[i];
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ O51(int i, int i2, Qg6 qg6) {
        this((i2 & 1) != 0 ? 10 : i);
    }

    @DexIgnore
    public final boolean a(int i) {
        boolean z = false;
        int f = Dm7.f(this.a, i, 0, this.b, 2, null);
        if (f < 0) {
            z = true;
        }
        if (z) {
            this.a = M51.a.b(this.a, this.b, f, i);
            this.b++;
        }
        return z;
    }

    @DexIgnore
    public final boolean b(int i) {
        boolean z = false;
        int f = Dm7.f(this.a, i, 0, this.b, 2, null);
        if (f >= 0) {
            z = true;
        }
        if (z) {
            c(f);
        }
        return z;
    }

    @DexIgnore
    public final void c(int i) {
        int[] iArr = this.a;
        int i2 = i + 1;
        System.arraycopy(iArr, i2, iArr, i, this.b - i2);
        this.b--;
    }
}
