package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ot4 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public boolean f;

    @DexIgnore
    public Ot4(String str, String str2, String str3, String str4, String str5, boolean z) {
        Wg6.c(str, "id");
        Wg6.c(str2, "socialId");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
        this.f = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Ot4(String str, String str2, String str3, String str4, String str5, boolean z, int i, Qg6 qg6) {
        this(str, str2, str3, str4, str5, (i & 32) != 0 ? false : z);
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final String c() {
        return this.d;
    }

    @DexIgnore
    public final String d() {
        return this.e;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Ot4) {
                Ot4 ot4 = (Ot4) obj;
                if (!Wg6.a(this.a, ot4.a) || !Wg6.a(this.b, ot4.b) || !Wg6.a(this.c, ot4.c) || !Wg6.a(this.d, ot4.d) || !Wg6.a(this.e, ot4.e) || this.f != ot4.f) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean f() {
        return this.f;
    }

    @DexIgnore
    public final void g(boolean z) {
        this.f = z;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.d;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.e;
        if (str5 != null) {
            i = str5.hashCode();
        }
        boolean z = this.f;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "SuggestedFriend(id=" + this.a + ", socialId=" + this.b + ", first=" + this.c + ", last=" + this.d + ", picture=" + this.e + ", isChecked=" + this.f + ")";
    }
}
