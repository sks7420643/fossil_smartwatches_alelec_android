package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xi6 implements Factory<Fj6> {
    @DexIgnore
    public static Fj6 a(Ui6 ui6) {
        Fj6 c = ui6.c();
        Lk7.c(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }
}
