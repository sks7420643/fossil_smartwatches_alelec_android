package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.field.types.BooleanCharType;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ww1 extends Xw1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ String[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ww1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Ww1 a(Parcel parcel) {
            return new Ww1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ww1 createFromParcel(Parcel parcel) {
            return new Ww1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Ww1[] newArray(int i) {
            return new Ww1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Ww1(Parcel parcel, Qg6 qg6) {
        super(Yw1.values()[parcel.readInt()]);
        String[] createStringArray = parcel.createStringArray();
        if (createStringArray != null) {
            this.c = createStringArray;
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Ww1(String[] strArr) throws IllegalArgumentException {
        super(Yw1.COMMUTE);
        this.c = strArr;
        if (strArr.length > 10) {
            StringBuilder e = E.e("Destinations(");
            e.append(this.c);
            e.append(") must be less than or equal to ");
            e.append(BooleanCharType.DEFAULT_TRUE_FALSE_FORMAT);
            throw new IllegalArgumentException(e.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.Xw1
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Xw1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Ww1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return Arrays.equals(this.c, ((Ww1) obj).c);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.watchapp.config.data.CommuteTimeWatchAppDataConfig");
    }

    @DexIgnore
    public final String[] getDestinations() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Xw1
    public int hashCode() {
        return (super.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put("commuteApp._.config.destinations", Ay1.b(this.c));
        Wg6.b(put, "JSONObject()\n           \u2026stinations.toJSONArray())");
        return put;
    }

    @DexIgnore
    @Override // com.fossil.Xw1
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeStringArray(this.c);
        }
    }
}
