package com.fossil;

import com.fossil.A91;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class X91 implements G91 {
    @DexIgnore
    public static /* final */ boolean c; // = U91.b;
    @DexIgnore
    public /* final */ W91 a;
    @DexIgnore
    public /* final */ Y91 b;

    @DexIgnore
    @Deprecated
    public X91(Da1 da1) {
        this(da1, new Y91(4096));
    }

    @DexIgnore
    @Deprecated
    public X91(Da1 da1, Y91 y91) {
        this.a = new V91(da1);
        this.b = y91;
    }

    @DexIgnore
    public X91(W91 w91) {
        this(w91, new Y91(4096));
    }

    @DexIgnore
    public X91(W91 w91, Y91 y91) {
        this.a = w91;
        this.b = y91;
    }

    @DexIgnore
    public static void b(String str, M91<?> m91, T91 t91) throws T91 {
        Q91 retryPolicy = m91.getRetryPolicy();
        int timeoutMs = m91.getTimeoutMs();
        try {
            retryPolicy.a(t91);
            m91.addMarker(String.format("%s-retry [timeout=%s]", str, Integer.valueOf(timeoutMs)));
        } catch (T91 e) {
            m91.addMarker(String.format("%s-timeout-giveup [timeout=%s]", str, Integer.valueOf(timeoutMs)));
            throw e;
        }
    }

    @DexIgnore
    public static List<F91> c(List<F91> list, A91.Ai ai) {
        TreeSet treeSet = new TreeSet(String.CASE_INSENSITIVE_ORDER);
        if (!list.isEmpty()) {
            for (F91 f91 : list) {
                treeSet.add(f91.a());
            }
        }
        ArrayList arrayList = new ArrayList(list);
        List<F91> list2 = ai.h;
        if (list2 != null) {
            if (!list2.isEmpty()) {
                for (F91 f912 : ai.h) {
                    if (!treeSet.contains(f912.a())) {
                        arrayList.add(f912);
                    }
                }
            }
        } else if (!ai.g.isEmpty()) {
            for (Map.Entry<String, String> entry : ai.g.entrySet()) {
                if (!treeSet.contains(entry.getKey())) {
                    arrayList.add(new F91(entry.getKey(), entry.getValue()));
                }
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0055, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0056, code lost:
        r4 = null;
        r3 = r17;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005b, code lost:
        r3 = r3.d();
        com.fossil.U91.c("Unexpected response code %d for %s", java.lang.Integer.valueOf(r3), r21.getUrl());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0075, code lost:
        if (r4 != null) goto L_0x0077;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0077, code lost:
        r2 = new com.fossil.J91(r3, r4, false, android.os.SystemClock.elapsedRealtime() - r18, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0085, code lost:
        if (r3 == 401) goto L_0x008b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008b, code lost:
        b("auth", r21, new com.fossil.Z81(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00cf, code lost:
        b("socket", r21, new com.fossil.S91());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00e7, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0102, code lost:
        throw new java.lang.RuntimeException("Bad URL " + r21.getUrl(), r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0109, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x010a, code lost:
        r4 = null;
        r3 = r17;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x010f, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0110, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0115, code lost:
        if (r3 < 400) goto L_0x011b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x011d, code lost:
        if (r3 < 500) goto L_0x0143;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0127, code lost:
        if (r21.shouldRetryServerErrors() != false) goto L_0x0129;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0129, code lost:
        b("server", r21, new com.fossil.R91(r2));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x013c, code lost:
        throw new com.fossil.C91(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0142, code lost:
        throw new com.fossil.R91(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0148, code lost:
        throw new com.fossil.R91(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0149, code lost:
        b("network", r21, new com.fossil.I91());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x015c, code lost:
        throw new com.fossil.K91(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x015d, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x015f, code lost:
        r2 = e;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ce A[ExcHandler: SocketTimeoutException (e java.net.SocketTimeoutException), Splitter:B:2:0x0009] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00e7 A[ExcHandler: MalformedURLException (r2v1 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), Splitter:B:2:0x0009] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0157 A[SYNTHETIC] */
    @Override // com.fossil.G91
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.J91 a(com.fossil.M91<?> r21) throws com.fossil.T91 {
        /*
        // Method dump skipped, instructions count: 353
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.X91.a(com.fossil.M91):com.fossil.J91");
    }

    @DexIgnore
    public final Map<String, String> d(A91.Ai ai) {
        if (ai == null) {
            return Collections.emptyMap();
        }
        HashMap hashMap = new HashMap();
        String str = ai.b;
        if (str != null) {
            hashMap.put("If-None-Match", str);
        }
        long j = ai.d;
        if (j <= 0) {
            return hashMap;
        }
        hashMap.put("If-Modified-Since", Ba1.a(j));
        return hashMap;
    }

    @DexIgnore
    public final byte[] e(InputStream inputStream, int i) throws IOException, R91 {
        Ja1 ja1 = new Ja1(this.b, i);
        byte[] bArr = null;
        if (inputStream != null) {
            try {
                bArr = this.b.a(1024);
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    ja1.write(bArr, 0, read);
                }
                byte[] byteArray = ja1.toByteArray();
                if (inputStream != null) {
                    try {
                    } catch (IOException e) {
                        U91.e("Error occurred when closing InputStream", new Object[0]);
                    }
                }
                return byteArray;
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e2) {
                        U91.e("Error occurred when closing InputStream", new Object[0]);
                    }
                }
                this.b.b(bArr);
                ja1.close();
            }
        } else {
            throw new R91();
        }
    }

    @DexIgnore
    public final void f(long j, M91<?> m91, byte[] bArr, int i) {
        if (c || j > 3000) {
            U91.b("HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]", m91, Long.valueOf(j), bArr != null ? Integer.valueOf(bArr.length) : "null", Integer.valueOf(i), Integer.valueOf(m91.getRetryPolicy().c()));
        }
    }
}
