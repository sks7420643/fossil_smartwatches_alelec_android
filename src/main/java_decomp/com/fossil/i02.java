package com.fossil;

import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I02 implements Yy1 {
    @DexIgnore
    public /* final */ Set<Ty1> a;
    @DexIgnore
    public /* final */ H02 b;
    @DexIgnore
    public /* final */ L02 c;

    @DexIgnore
    public I02(Set<Ty1> set, H02 h02, L02 l02) {
        this.a = set;
        this.b = h02;
        this.c = l02;
    }

    @DexIgnore
    @Override // com.fossil.Yy1
    public <T> Xy1<T> a(String str, Class<T> cls, Wy1<T, byte[]> wy1) {
        return b(str, cls, Ty1.b("proto"), wy1);
    }

    @DexIgnore
    @Override // com.fossil.Yy1
    public <T> Xy1<T> b(String str, Class<T> cls, Ty1 ty1, Wy1<T, byte[]> wy1) {
        if (this.a.contains(ty1)) {
            return new K02(this.b, str, ty1, wy1, this.c);
        }
        throw new IllegalArgumentException(String.format("%s is not supported byt this factory. Supported encodings are: %s.", ty1, this.a));
    }
}
