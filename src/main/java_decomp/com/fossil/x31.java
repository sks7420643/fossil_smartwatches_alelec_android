package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.work.impl.WorkDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class X31 {
    @DexIgnore
    public /* final */ WorkDatabase a;

    @DexIgnore
    public X31(WorkDatabase workDatabase) {
        this.a = workDatabase;
    }

    @DexIgnore
    public static void a(Context context, Lx0 lx0) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("androidx.work.util.id", 0);
        if (sharedPreferences.contains("next_job_scheduler_id") || sharedPreferences.contains("next_job_scheduler_id")) {
            int i = sharedPreferences.getInt("next_job_scheduler_id", 0);
            int i2 = sharedPreferences.getInt("next_alarm_manager_id", 0);
            lx0.beginTransaction();
            try {
                lx0.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"next_job_scheduler_id", Integer.valueOf(i)});
                lx0.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"next_alarm_manager_id", Integer.valueOf(i2)});
                sharedPreferences.edit().clear().apply();
                lx0.setTransactionSuccessful();
            } finally {
                lx0.endTransaction();
            }
        }
    }

    @DexIgnore
    public int b() {
        int c;
        synchronized (X31.class) {
            try {
                c = c("next_alarm_manager_id");
            } catch (Throwable th) {
                throw th;
            }
        }
        return c;
    }

    @DexIgnore
    public final int c(String str) {
        int i = 0;
        this.a.beginTransaction();
        try {
            Long a2 = this.a.f().a(str);
            int intValue = a2 != null ? a2.intValue() : 0;
            if (intValue != Integer.MAX_VALUE) {
                i = intValue + 1;
            }
            e(str, i);
            this.a.setTransactionSuccessful();
            return intValue;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public int d(int i, int i2) {
        synchronized (X31.class) {
            try {
                int c = c("next_job_scheduler_id");
                if (c < i || c > i2) {
                    e("next_job_scheduler_id", i + 1);
                } else {
                    i = c;
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return i;
    }

    @DexIgnore
    public final void e(String str, int i) {
        this.a.f().b(new C31(str, (long) i));
    }
}
