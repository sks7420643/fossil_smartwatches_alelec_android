package com.fossil;

import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Vh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N31 implements M31 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Vh b;
    @DexIgnore
    public /* final */ Vh c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Hh<L31> {
        @DexIgnore
        public Ai(N31 n31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(Mi mi, L31 l31) {
            String str = l31.a;
            if (str == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, str);
            }
            byte[] k = R01.k(l31.b);
            if (k == null) {
                mi.bindNull(2);
            } else {
                mi.bindBlob(2, k);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, L31 l31) {
            a(mi, l31);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `WorkProgress` (`work_spec_id`,`progress`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Vh {
        @DexIgnore
        public Bi(N31 n31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE from WorkProgress where work_spec_id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Vh {
        @DexIgnore
        public Ci(N31 n31, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM WorkProgress";
        }
    }

    @DexIgnore
    public N31(Oh oh) {
        this.a = oh;
        new Ai(this, oh);
        this.b = new Bi(this, oh);
        this.c = new Ci(this, oh);
    }

    @DexIgnore
    @Override // com.fossil.M31
    public void b(String str) {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.b.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.b.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.M31
    public void deleteAll() {
        this.a.assertNotSuspendingTransaction();
        Mi acquire = this.c.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }
}
