package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Tc0;
import com.mapped.Wg6;
import com.mapped.X90;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bu1 extends Tc0 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Kp1 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Bu1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Bu1 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(Uq1.class.getClassLoader());
            if (readParcelable != null) {
                Wg6.b(readParcelable, "parcel.readParcelable<Wo\u2026class.java.classLoader)!!");
                return new Bu1((Uq1) readParcelable, (Nt1) parcel.readParcelable(Nt1.class.getClassLoader()), (Kp1) parcel.readParcelable(Kp1.class.getClassLoader()));
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Bu1[] newArray(int i) {
            return new Bu1[i];
        }
    }

    @DexIgnore
    public Bu1(Uq1 uq1, Kp1 kp1) {
        super(uq1, null);
        this.d = kp1;
    }

    @DexIgnore
    public Bu1(Uq1 uq1, Nt1 nt1) {
        super(uq1, nt1);
        this.d = null;
    }

    @DexIgnore
    public Bu1(Uq1 uq1, Nt1 nt1, Kp1 kp1) {
        super(uq1, nt1);
        this.d = kp1;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public JSONObject a() {
        JSONObject jSONObject;
        JSONObject a2 = super.a();
        Kp1 kp1 = this.d;
        if (kp1 == null || (jSONObject = kp1.toJSONObject()) == null) {
            jSONObject = new JSONObject();
        }
        return Gy1.c(a2, jSONObject);
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public byte[] a(short s, Ry1 ry1) {
        X90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.b()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            Kp1 kp1 = this.d;
            if (kp1 != null) {
                JSONObject k = G80.k(new JSONObject(), Jd0.H, kp1.getFileName());
                JSONObject jSONObject2 = new JSONObject();
                Jd0 jd0 = Jd0.H5;
                X90 deviceRequest2 = getDeviceRequest();
                if (deviceRequest2 != null) {
                    jSONObject.put("workoutApp._.config.images", G80.k(G80.k(G80.k(jSONObject2, jd0, Long.valueOf(((Uq1) deviceRequest2).getSessionId())), Jd0.K5, k), Jd0.O5, kp1.e().b));
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.WorkoutRouteImageRequest");
                }
            }
            if (getDeviceMessage() != null) {
                jSONObject.put("workoutApp._.config.response", getDeviceMessage().toJSONObject());
            }
        } catch (JSONException e) {
            D90.i.i(e);
        }
        JSONObject jSONObject3 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("id", valueOf);
            jSONObject4.put("set", jSONObject);
            jSONObject3.put(str, jSONObject4);
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        String jSONObject5 = jSONObject3.toString();
        Wg6.b(jSONObject5, "deviceResponseJSONObject.toString()");
        Charset c = Hd0.y.c();
        if (jSONObject5 != null) {
            byte[] bytes = jSONObject5.getBytes(c);
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final Kp1 b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Bu1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(Wg6.a(this.d, ((Bu1) obj).d) ^ true);
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.WorkoutRouteImageData");
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public int hashCode() {
        int hashCode = super.hashCode();
        Kp1 kp1 = this.d;
        return (kp1 != null ? kp1.hashCode() : 0) + (hashCode * 31);
    }

    @DexIgnore
    @Override // com.mapped.Tc0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }
}
