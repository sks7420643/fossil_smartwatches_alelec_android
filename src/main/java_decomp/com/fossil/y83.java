package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y83 implements Xw2<X83> {
    @DexIgnore
    public static Y83 c; // = new Y83();
    @DexIgnore
    public /* final */ Xw2<X83> b;

    @DexIgnore
    public Y83() {
        this(Ww2.b(new A93()));
    }

    @DexIgnore
    public Y83(Xw2<X83> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((X83) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((X83) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ X83 zza() {
        return this.b.zza();
    }
}
