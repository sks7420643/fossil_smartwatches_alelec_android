package com.fossil;

import dagger.internal.Factory;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uz5 implements Factory<WeakReference<Ao6>> {
    @DexIgnore
    public static WeakReference<Ao6> a(Oz5 oz5) {
        WeakReference<Ao6> f = oz5.f();
        Lk7.c(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }
}
