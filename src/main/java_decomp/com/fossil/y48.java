package com.fossil;

import com.mapped.Cd6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y48 {
    @DexIgnore
    public static X48 a;
    @DexIgnore
    public static long b;
    @DexIgnore
    public static /* final */ Y48 c; // = new Y48();

    @DexIgnore
    public final void a(X48 x48) {
        boolean z = false;
        Wg6.c(x48, "segment");
        if (x48.f == null && x48.g == null) {
            z = true;
        }
        if (!z) {
            throw new IllegalArgumentException("Failed requirement.".toString());
        } else if (!x48.d) {
            synchronized (this) {
                long j = (long) 8192;
                if (b + j <= 65536) {
                    b += j;
                    x48.f = a;
                    x48.c = 0;
                    x48.b = 0;
                    a = x48;
                    Cd6 cd6 = Cd6.a;
                }
            }
        }
    }

    @DexIgnore
    public final X48 b() {
        synchronized (this) {
            X48 x48 = a;
            if (x48 == null) {
                return new X48();
            }
            a = x48.f;
            x48.f = null;
            b -= (long) 8192;
            return x48;
        }
    }
}
