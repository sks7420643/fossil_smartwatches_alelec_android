package com.fossil;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.imagefilters.EInkImageFactory;
import com.fossil.imagefilters.Format;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nv1 extends Mv1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ Vb0 e;
    @DexIgnore
    public Hw1 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Nv1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Nv1 createFromParcel(Parcel parcel) {
            return new Nv1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Nv1[] newArray(int i) {
            return new Nv1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Nv1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.e = Vb0.b;
        Parcelable readParcelable = parcel.readParcelable(Hw1.class.getClassLoader());
        if (readParcelable != null) {
            this.f = (Hw1) readParcelable;
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Nv1(String str, byte[] bArr) {
        super(str, new Jv1(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES), new Kv1(1.0f, 1.0f));
        this.e = Vb0.b;
        this.f = new Hw1(str, bArr);
    }

    @DexIgnore
    public Nv1(JSONObject jSONObject, Cc0[] cc0Arr, String str) throws IllegalArgumentException {
        super(jSONObject, str);
        Cc0 cc0;
        this.e = Vb0.b;
        String optString = jSONObject.optString("name");
        int length = cc0Arr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                cc0 = null;
                break;
            }
            cc0 = cc0Arr[i];
            if (Wg6.a(optString, cc0.b)) {
                break;
            }
            i++;
        }
        byte[] bArr = cc0 != null ? cc0.c : null;
        if (bArr != null) {
            Bitmap decode = EInkImageFactory.decode(bArr, Format.RAW);
            Wg6.b(decode, "EInkImageFactory.decode(\u2026iceImageData, Format.RAW)");
            this.f = new Hw1(getName(), Cy1.b(decode, null, 1, null));
            decode.recycle();
            return;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    @DexIgnore
    public Nv1(byte[] bArr) {
        this(G80.e(0, 1), bArr);
    }

    @DexIgnore
    public final Cc0 a(String str) {
        return this.f.a(new Lv1(240, 240), str);
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Vb0 a() {
        return this.e;
    }

    @DexIgnore
    public final JSONObject b(String str) {
        JSONObject put = d().put("name", str);
        Wg6.b(put, "getUIScript()\n          \u2026onstant.NAME, customName)");
        return put;
    }

    @DexIgnore
    @Override // java.lang.Object, com.fossil.Mv1, com.fossil.Mv1
    public Nv1 clone() {
        String name = getName();
        byte[] bitmapImageData = this.f.getBitmapImageData();
        byte[] copyOf = Arrays.copyOf(bitmapImageData, bitmapImageData.length);
        Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new Nv1(name, copyOf);
    }

    @DexIgnore
    public final Hw1 e() {
        return this.f;
    }

    @DexIgnore
    public Nv1 f() {
        return this;
    }

    @DexIgnore
    public Nv1 g() {
        return this;
    }

    @DexIgnore
    public final byte[] getBitmapImageData() {
        return this.f.getBitmapImageData();
    }

    @DexIgnore
    public Nv1 h() {
        return this;
    }

    @DexIgnore
    public Nv1 i() {
        return this;
    }

    @DexIgnore
    public final Nv1 setImageData(byte[] bArr) {
        this.f = new Hw1(getName(), bArr);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public /* bridge */ /* synthetic */ Mv1 setScaledHeight(float f2) {
        return f();
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Nv1 setScaledPosition(Jv1 jv1) {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Nv1 setScaledSize(Kv1 kv1) {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public /* bridge */ /* synthetic */ Mv1 setScaledWidth(float f2) {
        return g();
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public /* bridge */ /* synthetic */ Mv1 setScaledX(float f2) {
        return h();
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public /* bridge */ /* synthetic */ Mv1 setScaledY(float f2) {
        return i();
    }
}
