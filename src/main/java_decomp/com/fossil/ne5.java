package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ne5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ View t;
    @DexIgnore
    public /* final */ ProgressBar u;
    @DexIgnore
    public /* final */ FlexibleTextView v;

    @DexIgnore
    public Ne5(Object obj, View view, int i, ConstraintLayout constraintLayout, ImageView imageView, ImageView imageView2, View view2, ProgressBar progressBar, FlexibleTextView flexibleTextView) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = imageView;
        this.s = imageView2;
        this.t = view2;
        this.u = progressBar;
        this.v = flexibleTextView;
    }

    @DexIgnore
    @Deprecated
    public static Ne5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (Ne5) ViewDataBinding.p(layoutInflater, 2131558688, viewGroup, z, obj);
    }

    @DexIgnore
    public static Ne5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, Aq0.d());
    }
}
