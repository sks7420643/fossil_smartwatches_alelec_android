package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Build;
import android.view.ViewConfiguration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Of0 {
    @DexIgnore
    public Context a;

    @DexIgnore
    public Of0(Context context) {
        this.a = context;
    }

    @DexIgnore
    public static Of0 b(Context context) {
        return new Of0(context);
    }

    @DexIgnore
    public boolean a() {
        return this.a.getApplicationInfo().targetSdkVersion < 14;
    }

    @DexIgnore
    public int c() {
        return this.a.getResources().getDisplayMetrics().widthPixels / 2;
    }

    @DexIgnore
    public int d() {
        Configuration configuration = this.a.getResources().getConfiguration();
        int i = configuration.screenWidthDp;
        int i2 = configuration.screenHeightDp;
        if (configuration.smallestScreenWidthDp > 600 || i > 600 || ((i > 960 && i2 > 720) || (i > 720 && i2 > 960))) {
            return 5;
        }
        if (i >= 500 || ((i > 640 && i2 > 480) || (i > 480 && i2 > 640))) {
            return 4;
        }
        return i >= 360 ? 3 : 2;
    }

    @DexIgnore
    public int e() {
        return this.a.getResources().getDimensionPixelSize(Oe0.abc_action_bar_stacked_tab_max_width);
    }

    @DexIgnore
    public int f() {
        TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes(null, Ue0.ActionBar, Le0.actionBarStyle, 0);
        int layoutDimension = obtainStyledAttributes.getLayoutDimension(Ue0.ActionBar_height, 0);
        Resources resources = this.a.getResources();
        if (!g()) {
            layoutDimension = Math.min(layoutDimension, resources.getDimensionPixelSize(Oe0.abc_action_bar_stacked_max_height));
        }
        obtainStyledAttributes.recycle();
        return layoutDimension;
    }

    @DexIgnore
    public boolean g() {
        return this.a.getResources().getBoolean(Me0.abc_action_bar_embed_tabs);
    }

    @DexIgnore
    public boolean h() {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return !ViewConfiguration.get(this.a).hasPermanentMenuKey();
    }
}
