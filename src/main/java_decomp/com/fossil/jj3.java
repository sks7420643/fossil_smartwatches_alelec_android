package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Jj3 extends Jn3 implements Ln3 {
    @DexIgnore
    public Jj3(Pm3 pm3) {
        super(pm3);
        Rc2.k(pm3);
    }

    @DexIgnore
    @Override // com.fossil.Jn3
    public void f() {
        this.a.q();
    }

    @DexIgnore
    @Override // com.fossil.Jn3
    public void g() {
        this.a.c().g();
    }

    @DexIgnore
    @Override // com.fossil.Jn3
    public void h() {
        this.a.c().h();
    }

    @DexIgnore
    public void n() {
        this.a.r();
        throw null;
    }

    @DexIgnore
    public Gg3 o() {
        return this.a.R();
    }

    @DexIgnore
    public Un3 p() {
        return this.a.E();
    }

    @DexIgnore
    public Dl3 q() {
        return this.a.Q();
    }

    @DexIgnore
    public Fp3 r() {
        return this.a.O();
    }

    @DexIgnore
    public Ap3 s() {
        return this.a.N();
    }

    @DexIgnore
    public Gl3 t() {
        return this.a.H();
    }

    @DexIgnore
    public Jq3 u() {
        return this.a.B();
    }
}
