package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Qg4 implements Callable {
    @DexIgnore
    public /* final */ Tg4 a;

    @DexIgnore
    public Qg4(Tg4 tg4) {
        this.a = tg4;
    }

    @DexIgnore
    public static Callable a(Tg4 tg4) {
        return new Qg4(tg4);
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public Object call() {
        return this.a.d();
    }
}
