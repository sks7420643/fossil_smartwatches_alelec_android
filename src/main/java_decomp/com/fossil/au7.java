package com.fossil;

import com.mapped.Af6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Rm6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Au7<T> extends Fx7 implements Rm6, Xe6<T>, Il6 {
    @DexIgnore
    public /* final */ Af6 c;
    @DexIgnore
    public /* final */ Af6 d;

    @DexIgnore
    public Au7(Af6 af6, boolean z) {
        super(z);
        this.d = af6;
        this.c = af6.plus(this);
    }

    @DexIgnore
    @Override // com.fossil.Fx7
    public final void S(Throwable th) {
        Fv7.a(this.c, th);
    }

    @DexIgnore
    @Override // com.fossil.Fx7
    public String Z() {
        String b = Cv7.b(this.c);
        if (b == null) {
            return super.Z();
        }
        return '\"' + b + "\":" + super.Z();
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.Fx7
    public final void e0(Object obj) {
        if (obj instanceof Vu7) {
            Vu7 vu7 = (Vu7) obj;
            w0(vu7.a, vu7.a());
            return;
        }
        x0(obj);
    }

    @DexIgnore
    @Override // com.fossil.Fx7
    public final void f0() {
        y0();
    }

    @DexIgnore
    @Override // com.mapped.Xe6
    public final Af6 getContext() {
        return this.c;
    }

    @DexIgnore
    @Override // com.mapped.Il6
    public Af6 h() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Fx7, com.mapped.Rm6
    public boolean isActive() {
        return super.isActive();
    }

    @DexIgnore
    @Override // com.mapped.Xe6
    public final void resumeWith(Object obj) {
        Object X = X(Wu7.b(obj));
        if (X != Gx7.b) {
            u0(X);
        }
    }

    @DexIgnore
    public void u0(Object obj) {
        p(obj);
    }

    @DexIgnore
    public final void v0() {
        T((Rm6) this.d.get(Rm6.r));
    }

    @DexIgnore
    public void w0(Throwable th, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.Fx7
    public String x() {
        return Ov7.a(this) + " was cancelled";
    }

    @DexIgnore
    public void x0(T t) {
    }

    @DexIgnore
    public void y0() {
    }

    @DexIgnore
    public final <R> void z0(Lv7 lv7, R r, Coroutine<? super R, ? super Xe6<? super T>, ? extends Object> coroutine) {
        v0();
        lv7.invoke(coroutine, r, this);
    }
}
