package com.fossil;

import android.os.RemoteException;
import com.mapped.Wv2;
import com.mapped.Yv2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cb3 extends S72<Fr2, Yv2> {
    @DexIgnore
    public /* final */ /* synthetic */ Ir2 d;
    @DexIgnore
    public /* final */ /* synthetic */ P72 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Cb3(Wv2 wv2, P72 p72, Ir2 ir2, P72 p722) {
        super(p72);
        this.d = ir2;
        this.e = p722;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.M62$Bi, com.fossil.Ot3] */
    @Override // com.fossil.S72
    public final /* synthetic */ void d(Fr2 fr2, Ot3 ot3) throws RemoteException {
        fr2.w0(this.d, this.e, new Wv2.Ai(ot3));
    }
}
