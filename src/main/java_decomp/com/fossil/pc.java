package com.fossil;

import com.fossil.fitness.FitnessData;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Q40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pc extends Qq7 implements Hg6<FitnessData[], Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Qc b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Pc(Qc qc) {
        super(1);
        this.b = qc;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(FitnessData[] fitnessDataArr) {
        FitnessData[] fitnessDataArr2 = fitnessDataArr;
        Q40.Bi bi = this.b.c.w;
        if (bi != null) {
            bi.onAutoSyncFitnessDataReceived(fitnessDataArr2);
        }
        return Cd6.a;
    }
}
