package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class F22 {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ K22 b;
    @DexIgnore
    public /* final */ H22 c;
    @DexIgnore
    public /* final */ S32 d;

    @DexIgnore
    public F22(Executor executor, K22 k22, H22 h22, S32 s32) {
        this.a = executor;
        this.b = k22;
        this.c = h22;
        this.d = s32;
    }

    @DexIgnore
    public static /* synthetic */ Object b(F22 f22) {
        for (H02 h02 : f22.b.w()) {
            f22.c.a(h02, 1);
        }
        return null;
    }

    @DexIgnore
    public void a() {
        this.a.execute(D22.a(this));
    }
}
