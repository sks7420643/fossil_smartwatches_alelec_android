package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ie3 extends Me3 {
    @DexIgnore
    public /* final */ float d;

    @DexIgnore
    public Ie3(float f) {
        super(2, Float.valueOf(Math.max(f, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
        this.d = Math.max(f, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    @Override // com.fossil.Me3
    public final String toString() {
        float f = this.d;
        StringBuilder sb = new StringBuilder(29);
        sb.append("[Gap: length=");
        sb.append(f);
        sb.append("]");
        return sb.toString();
    }
}
