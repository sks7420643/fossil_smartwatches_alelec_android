package com.fossil;

import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Rl6;
import com.mapped.Rm6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eu7 {
    @DexIgnore
    public static final <T> Rl6<T> a(Il6 il6, Af6 af6, Lv7 lv7, Coroutine<? super Il6, ? super Xe6<? super T>, ? extends Object> coroutine) {
        return Gu7.a(il6, af6, lv7, coroutine);
    }

    @DexIgnore
    public static final Rm6 c(Il6 il6, Af6 af6, Lv7 lv7, Coroutine<? super Il6, ? super Xe6<? super Cd6>, ? extends Object> coroutine) {
        return Gu7.c(il6, af6, lv7, coroutine);
    }

    @DexIgnore
    public static final <T> T e(Af6 af6, Coroutine<? super Il6, ? super Xe6<? super T>, ? extends Object> coroutine) throws InterruptedException {
        return (T) Fu7.a(af6, coroutine);
    }

    @DexIgnore
    public static final <T> Object g(Af6 af6, Coroutine<? super Il6, ? super Xe6<? super T>, ? extends Object> coroutine, Xe6<? super T> xe6) {
        return Gu7.e(af6, coroutine, xe6);
    }
}
