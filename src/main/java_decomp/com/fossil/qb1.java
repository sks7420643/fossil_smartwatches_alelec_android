package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Qb1<T, Z> {
    @DexIgnore
    boolean a(T t, Ob1 ob1) throws IOException;

    @DexIgnore
    Id1<Z> b(T t, int i, int i2, Ob1 ob1) throws IOException;
}
