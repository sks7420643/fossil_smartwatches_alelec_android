package com.fossil;

import android.accounts.Account;
import android.view.View;
import com.google.android.gms.common.api.Scope;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ac2 {
    @DexIgnore
    public /* final */ Account a;
    @DexIgnore
    public /* final */ Set<Scope> b;
    @DexIgnore
    public /* final */ Set<Scope> c;
    @DexIgnore
    public /* final */ Map<M62<?>, Bi> d;
    @DexIgnore
    public /* final */ View e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ Gs3 h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public Integer j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Account a;
        @DexIgnore
        public Aj0<Scope> b;
        @DexIgnore
        public Map<M62<?>, Bi> c;
        @DexIgnore
        public int d; // = 0;
        @DexIgnore
        public View e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public Gs3 h; // = Gs3.k;
        @DexIgnore
        public boolean i;

        @DexIgnore
        public final Ai a(Collection<Scope> collection) {
            if (this.b == null) {
                this.b = new Aj0<>();
            }
            this.b.addAll(collection);
            return this;
        }

        @DexIgnore
        public final Ac2 b() {
            return new Ac2(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i);
        }

        @DexIgnore
        public final Ai c(Account account) {
            this.a = account;
            return this;
        }

        @DexIgnore
        public final Ai d(String str) {
            this.g = str;
            return this;
        }

        @DexIgnore
        public final Ai e(String str) {
            this.f = str;
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ Set<Scope> a;

        @DexIgnore
        public Bi(Set<Scope> set) {
            Rc2.k(set);
            this.a = Collections.unmodifiableSet(set);
        }
    }

    @DexIgnore
    public Ac2(Account account, Set<Scope> set, Map<M62<?>, Bi> map, int i2, View view, String str, String str2, Gs3 gs3, boolean z) {
        this.a = account;
        this.b = set == null ? Collections.emptySet() : Collections.unmodifiableSet(set);
        this.d = map == null ? Collections.emptyMap() : map;
        this.e = view;
        this.f = str;
        this.g = str2;
        this.h = gs3;
        this.i = z;
        HashSet hashSet = new HashSet(this.b);
        for (Bi bi : this.d.values()) {
            hashSet.addAll(bi.a);
        }
        this.c = Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    public final Account a() {
        return this.a;
    }

    @DexIgnore
    @Deprecated
    public final String b() {
        Account account = this.a;
        if (account != null) {
            return account.name;
        }
        return null;
    }

    @DexIgnore
    public final Account c() {
        Account account = this.a;
        return account != null ? account : new Account("<<default account>>", "com.google");
    }

    @DexIgnore
    public final Set<Scope> d() {
        return this.c;
    }

    @DexIgnore
    public final Set<Scope> e(M62<?> m62) {
        Bi bi = this.d.get(m62);
        if (bi == null || bi.a.isEmpty()) {
            return this.b;
        }
        HashSet hashSet = new HashSet(this.b);
        hashSet.addAll(bi.a);
        return hashSet;
    }

    @DexIgnore
    public final Integer f() {
        return this.j;
    }

    @DexIgnore
    public final Map<M62<?>, Bi> g() {
        return this.d;
    }

    @DexIgnore
    public final String h() {
        return this.g;
    }

    @DexIgnore
    public final String i() {
        return this.f;
    }

    @DexIgnore
    public final Set<Scope> j() {
        return this.b;
    }

    @DexIgnore
    public final Gs3 k() {
        return this.h;
    }

    @DexIgnore
    public final boolean l() {
        return this.i;
    }

    @DexIgnore
    public final void m(Integer num) {
        this.j = num;
    }
}
