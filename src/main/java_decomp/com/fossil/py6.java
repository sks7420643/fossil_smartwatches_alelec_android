package com.fossil;

import android.os.CountDownTimer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Py6 {
    @DexIgnore
    public CountDownTimer a; // = new Ai(this, 30000, 1000);
    @DexIgnore
    public Px6 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends CountDownTimer {
        @DexIgnore
        public /* final */ /* synthetic */ Py6 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Py6 py6, long j, long j2) {
            super(j, j2);
            this.a = py6;
        }

        @DexIgnore
        public void onFinish() {
        }

        @DexIgnore
        public void onTick(long j) {
            Px6 px6 = this.a.b;
            if (px6 != null) {
                px6.Y6((int) (j / ((long) 1000)));
            }
        }
    }

    @DexIgnore
    public Py6(Px6 px6) {
        this.b = px6;
    }

    @DexIgnore
    public final void b() {
        this.a.cancel();
        this.b = null;
    }

    @DexIgnore
    public final void c(boolean z) {
        this.a.cancel();
        if (z) {
            this.a.start();
        }
    }
}
