package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U7 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ K5 a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public U7(K5 k5) {
        this.a = k5;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action;
        if (intent != null && (action = intent.getAction()) != null && action.hashCode() == -1099894406 && action.equals("com.fossil.blesdk.hid.HIDProfile.action.CONNECTION_STATE_CHANGED")) {
            B5 a2 = B5.g.a(intent.getIntExtra("com.fossil.blesdk.hid.HIDProfile.extra.PREVIOUS_STATE", 0));
            B5 a3 = B5.g.a(intent.getIntExtra("com.fossil.blesdk.hid.HIDProfile.extra.NEW_STATE", 0));
            if (Wg6.a((BluetoothDevice) intent.getParcelableExtra("com.fossil.blesdk.hid.HIDProfile.extra.BLUETOOTH_DEVICE"), this.a.A)) {
                D90.i.d(new A90("hid_connection_state_changed", V80.h, this.a.x, "", "", true, null, null, null, G80.k(G80.k(new JSONObject(), Jd0.C0, Ey1.a(a2)), Jd0.B0, Ey1.a(a3)), 448));
                this.a.f(a2, a3);
            }
        }
    }
}
