package com.fossil;

import android.graphics.Rect;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N67 {
    @DexIgnore
    public /* final */ Q67 a;
    @DexIgnore
    public /* final */ float b;

    @DexIgnore
    public N67(float f, float f2, float f3) {
        this.b = f3;
        this.a = new Q67(f, f2);
    }

    @DexIgnore
    public final boolean a(Rect rect) {
        Wg6.c(rect, "rect");
        return !(b(new Q67((float) rect.left, (float) rect.top)) || b(new Q67((float) rect.right, (float) rect.top)) || b(new Q67((float) rect.left, (float) rect.bottom)) || b(new Q67((float) rect.right, (float) rect.bottom)));
    }

    @DexIgnore
    public final boolean b(Q67 q67) {
        Wg6.c(q67, "point");
        return this.a.a(q67) <= this.b;
    }

    @DexIgnore
    public String toString() {
        return "Circle(center=" + this.a + ", radius=" + this.b + ')';
    }
}
