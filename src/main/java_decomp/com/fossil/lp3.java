package com.fossil;

import android.os.RemoteException;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ AtomicReference b;
    @DexIgnore
    public /* final */ /* synthetic */ Or3 c;
    @DexIgnore
    public /* final */ /* synthetic */ Fp3 d;

    @DexIgnore
    public Lp3(Fp3 fp3, AtomicReference atomicReference, Or3 or3) {
        this.d = fp3;
        this.b = atomicReference;
        this.c = or3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.b) {
            try {
                Cl3 cl3 = this.d.d;
                if (cl3 == null) {
                    this.d.d().F().a("Failed to get app instance id");
                    return;
                }
                this.b.set(cl3.n0(this.c));
                String str = (String) this.b.get();
                if (str != null) {
                    this.d.p().M(str);
                    this.d.l().l.b(str);
                }
                this.d.e0();
                this.b.notify();
            } catch (RemoteException e) {
                this.d.d().F().b("Failed to get app instance id", e);
            } finally {
                this.b.notify();
            }
        }
    }
}
