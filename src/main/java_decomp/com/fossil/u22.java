package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.J32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class U22 implements J32.Bi {
    @DexIgnore
    public /* final */ long a;

    @DexIgnore
    public U22(long j) {
        this.a = j;
    }

    @DexIgnore
    public static J32.Bi a(long j) {
        return new U22(j);
    }

    @DexIgnore
    @Override // com.fossil.J32.Bi
    public Object apply(Object obj) {
        return Integer.valueOf(((SQLiteDatabase) obj).delete("events", "timestamp_ms < ?", new String[]{String.valueOf(this.a)}));
    }
}
