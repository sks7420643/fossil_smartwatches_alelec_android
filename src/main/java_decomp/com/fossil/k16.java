package com.fossil;

import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Hx5;
import com.mapped.Qg6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K16 extends BaseFragment implements J16 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public G37<B95> g;
    @DexIgnore
    public I16 h;
    @DexIgnore
    public Hx5 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return K16.k;
        }

        @DexIgnore
        public final K16 b() {
            return new K16();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Hx5.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ K16 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(K16 k16) {
            this.a = k16;
        }

        @DexIgnore
        @Override // com.fossil.Hx5.Ai
        public void a() {
            K16.L6(this.a).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ K16 b;

        @DexIgnore
        public Ci(K16 k16) {
            this.b = k16;
        }

        @DexIgnore
        public final void onClick(View view) {
            K16.L6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ K16 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di(K16 k16) {
            this.b = k16;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Hx5 hx5 = this.b.i;
            if (hx5 != null) {
                hx5.v(String.valueOf(charSequence));
            }
            this.b.P6(!TextUtils.isEmpty(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ B95 b;

        @DexIgnore
        public Ei(B95 b95) {
            this.b = b95;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                B95 b95 = this.b;
                Wg6.b(b95, "binding");
                b95.n().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ K16 b;

        @DexIgnore
        public Fi(K16 k16) {
            this.b = k16;
        }

        @DexIgnore
        public final void onClick(View view) {
            K16.L6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ B95 b;

        @DexIgnore
        public Gi(B95 b95) {
            this.b = b95;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.r.setText("");
        }
    }

    /*
    static {
        String simpleName = K16.class.getSimpleName();
        Wg6.b(simpleName, "NotificationContactsFrag\u2026nt::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ I16 L6(K16 k16) {
        I16 i16 = k16.h;
        if (i16 != null) {
            return i16;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        I16 i16 = this.h;
        if (i16 != null) {
            i16.n();
            return true;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(I16 i16) {
        O6(i16);
    }

    @DexIgnore
    public void O6(I16 i16) {
        Wg6.c(i16, "presenter");
        this.h = i16;
    }

    @DexIgnore
    public final void P6(boolean z) {
        G37<B95> g37 = this.g;
        if (g37 != null) {
            B95 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                ImageView imageView = a2.t;
                Wg6.b(imageView, "ivClearSearch");
                imageView.setVisibility(0);
                return;
            }
            ImageView imageView2 = a2.t;
            Wg6.b(imageView2, "ivClearSearch");
            imageView2.setVisibility(8);
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.J16
    public void T(Cursor cursor) {
        Hx5 hx5 = this.i;
        if (hx5 != null) {
            hx5.l(cursor);
        }
    }

    @DexIgnore
    @Override // com.fossil.J16
    public void V() {
    }

    @DexIgnore
    @Override // com.fossil.J16
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.J16
    public void f6(boolean z) {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        G37<B95> g37 = this.g;
        if (g37 != null) {
            B95 a2 = g37.a();
            if (!(a2 == null || (flexibleButton2 = a2.q) == null)) {
                flexibleButton2.setEnabled(z);
            }
            G37<B95> g372 = this.g;
            if (g372 != null) {
                B95 a3 = g372.a();
                if (a3 != null && (flexibleButton = a3.q) != null) {
                    flexibleButton.setBackgroundColor(z ? W6.d(PortfolioApp.get.instance(), 2131099967) : W6.d(PortfolioApp.get.instance(), 2131099827));
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.J16
    public void j5(List<J06> list, FilterQueryProvider filterQueryProvider) {
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView;
        Wg6.c(list, "listContactWrapper");
        Wg6.c(filterQueryProvider, "filterQueryProvider");
        Hx5 hx5 = new Hx5(null, list);
        this.i = hx5;
        if (hx5 != null) {
            hx5.y(new Bi(this));
            Hx5 hx52 = this.i;
            if (hx52 != null) {
                hx52.k(filterQueryProvider);
                G37<B95> g37 = this.g;
                if (g37 != null) {
                    B95 a2 = g37.a();
                    if (a2 != null && (alphabetFastScrollRecyclerView = a2.w) != null) {
                        alphabetFastScrollRecyclerView.setAdapter(this.i);
                        return;
                    }
                    return;
                }
                Wg6.n("mBinding");
                throw null;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        B95 b95 = (B95) Aq0.f(layoutInflater, 2131558590, viewGroup, false, A6());
        b95.u.setOnClickListener(new Ci(this));
        b95.r.addTextChangedListener(new Di(this));
        b95.r.setOnFocusChangeListener(new Ei(b95));
        b95.q.setOnClickListener(new Fi(this));
        b95.t.setOnClickListener(new Gi(b95));
        b95.w.setIndexBarVisibility(true);
        b95.w.setIndexBarHighLateTextVisibility(true);
        b95.w.setIndexbarHighLateTextColor(2131099703);
        b95.w.setIndexBarTransparentValue(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        b95.w.setIndexTextSize(9);
        b95.w.setIndexBarTextColor(2131099703);
        Typeface b = Nl0.b(requireContext(), 2131296256);
        String d = ThemeManager.l.a().d("primaryText");
        Typeface f = ThemeManager.l.a().f("nonBrandTextStyle5");
        if (f == null) {
            f = b;
        }
        if (!TextUtils.isEmpty(d)) {
            int parseColor = Color.parseColor(d);
            b95.w.setIndexBarTextColor(parseColor);
            b95.w.setIndexbarHighLateTextColor(parseColor);
        }
        if (f != null) {
            b95.w.setTypeface(f);
        }
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = b95.w;
        alphabetFastScrollRecyclerView.setLayoutManager(new LinearLayoutManager(alphabetFastScrollRecyclerView.getContext(), 1, false));
        this.g = new G37<>(this, b95);
        Wg6.b(b95, "binding");
        return b95.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        I16 i16 = this.h;
        if (i16 != null) {
            i16.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        I16 i16 = this.h;
        if (i16 != null) {
            i16.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
