package com.fossil;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lz0 extends Kz0 {
    @DexIgnore
    public static boolean k; // = true;

    @DexIgnore
    @Override // com.fossil.Nz0
    @SuppressLint({"NewApi"})
    public void h(View view, int i) {
        if (Build.VERSION.SDK_INT == 28) {
            super.h(view, i);
        } else if (k) {
            try {
                view.setTransitionVisibility(i);
            } catch (NoSuchMethodError e) {
                k = false;
            }
        }
    }
}
