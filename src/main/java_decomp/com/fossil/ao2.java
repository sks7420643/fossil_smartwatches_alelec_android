package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ao2 extends Tn2 implements Bo2 {
    @DexIgnore
    public Ao2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitConfigApi");
    }

    @DexIgnore
    @Override // com.fossil.Bo2
    public final void Y1(Yi2 yi2) throws RemoteException {
        Parcel d = d();
        Qo2.b(d, yi2);
        e(22, d);
    }
}
