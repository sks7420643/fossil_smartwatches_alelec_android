package com.fossil;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A61 implements E61<Uri> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public A61(Context context) {
        Wg6.c(context, "context");
        this.a = context;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ boolean a(Uri uri) {
        return e(uri);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ String b(Uri uri) {
        return g(uri);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.G51, java.lang.Object, com.fossil.F81, com.fossil.X51, com.mapped.Xe6] */
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ Object c(G51 g51, Uri uri, F81 f81, X51 x51, Xe6 xe6) {
        return d(g51, uri, f81, x51, xe6);
    }

    @DexIgnore
    public Object d(G51 g51, Uri uri, F81 f81, X51 x51, Xe6<? super D61> xe6) {
        InputStream openInputStream;
        if (f(uri)) {
            AssetFileDescriptor openAssetFileDescriptor = this.a.getContentResolver().openAssetFileDescriptor(uri, "r");
            openInputStream = openAssetFileDescriptor != null ? openAssetFileDescriptor.createInputStream() : null;
            if (openInputStream == null) {
                throw new IllegalStateException(("Unable to find a contact photo associated with '" + uri + "'.").toString());
            }
        } else {
            openInputStream = this.a.getContentResolver().openInputStream(uri);
            if (openInputStream == null) {
                throw new IllegalStateException(("Unable to open '" + uri + "'.").toString());
            }
        }
        return new K61(S48.d(S48.l(openInputStream)), this.a.getContentResolver().getType(uri), Q51.DISK);
    }

    @DexIgnore
    public boolean e(Uri uri) {
        Wg6.c(uri, "data");
        return Wg6.a(uri.getScheme(), "content");
    }

    @DexIgnore
    public final boolean f(Uri uri) {
        Wg6.c(uri, "data");
        return Wg6.a(uri.getAuthority(), "com.android.contacts") && Wg6.a(uri.getLastPathSegment(), "display_photo");
    }

    @DexIgnore
    public String g(Uri uri) {
        Wg6.c(uri, "data");
        String uri2 = uri.toString();
        Wg6.b(uri2, "data.toString()");
        return uri2;
    }
}
