package com.fossil;

import com.mapped.Af6;
import com.mapped.Coroutine;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yw0 implements Af6.Bi {
    @DexIgnore
    public static /* final */ Ai c; // = new Ai(null);
    @DexIgnore
    public /* final */ Rn7 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Af6.Ci<Yw0> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public final Rn7 b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.mapped.Af6
    public <R> R fold(R r, Coroutine<? super R, ? super Af6.Bi, ? extends R> coroutine) {
        Wg6.c(coroutine, "operation");
        return (R) Af6.Bi.Aii.a(this, r, coroutine);
    }

    @DexIgnore
    @Override // com.mapped.Af6, com.mapped.Af6.Bi
    public <E extends Af6.Bi> E get(Af6.Ci<E> ci) {
        Wg6.c(ci, "key");
        return (E) Af6.Bi.Aii.b(this, ci);
    }

    @DexIgnore
    @Override // com.mapped.Af6.Bi
    public Af6.Ci<Yw0> getKey() {
        return c;
    }

    @DexIgnore
    @Override // com.mapped.Af6
    public Af6 minusKey(Af6.Ci<?> ci) {
        Wg6.c(ci, "key");
        return Af6.Bi.Aii.c(this, ci);
    }

    @DexIgnore
    @Override // com.mapped.Af6
    public Af6 plus(Af6 af6) {
        Wg6.c(af6, "context");
        return Af6.Bi.Aii.d(this, af6);
    }
}
