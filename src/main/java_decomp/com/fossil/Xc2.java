package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import com.facebook.LegacyTokenHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xc2 {
    @DexIgnore
    public /* final */ Resources a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public Xc2(Context context) {
        Rc2.k(context);
        Resources resources = context.getResources();
        this.a = resources;
        this.b = resources.getResourcePackageName(J62.common_google_play_services_unknown_issue);
    }

    @DexIgnore
    public String a(String str) {
        int identifier = this.a.getIdentifier(str, LegacyTokenHelper.TYPE_STRING, this.b);
        if (identifier == 0) {
            return null;
        }
        return this.a.getString(identifier);
    }
}
