package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sh6 implements Factory<Vh6> {
    @DexIgnore
    public static Vh6 a(Qh6 qh6) {
        Vh6 b = qh6.b();
        Lk7.c(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
