package com.fossil;

import com.fossil.Bz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dz1 extends Bz1 {
    @DexIgnore
    public /* final */ Integer a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ String h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Bz1.Ai {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;

        @DexIgnore
        @Override // com.fossil.Bz1.Ai
        public Bz1.Ai a(Integer num) {
            this.a = num;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Bz1.Ai
        public Bz1.Ai b(String str) {
            this.d = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Bz1.Ai
        public Bz1 c() {
            return new Dz1(this.a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, null);
        }

        @DexIgnore
        @Override // com.fossil.Bz1.Ai
        public Bz1.Ai d(String str) {
            this.h = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Bz1.Ai
        public Bz1.Ai e(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Bz1.Ai
        public Bz1.Ai f(String str) {
            this.g = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Bz1.Ai
        public Bz1.Ai g(String str) {
            this.b = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Bz1.Ai
        public Bz1.Ai h(String str) {
            this.f = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Bz1.Ai
        public Bz1.Ai i(String str) {
            this.e = str;
            return this;
        }
    }

    @DexIgnore
    public /* synthetic */ Dz1(Integer num, String str, String str2, String str3, String str4, String str5, String str6, String str7, Ai ai) {
        this.a = num;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = str7;
    }

    @DexIgnore
    @Override // com.fossil.Bz1
    public String b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Bz1
    public String c() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.Bz1
    public String d() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Bz1
    public String e() {
        return this.g;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Bz1)) {
            return false;
        }
        Integer num = this.a;
        if (num != null ? num.equals(((Dz1) obj).a) : ((Dz1) obj).a == null) {
            String str = this.b;
            if (str != null ? str.equals(((Dz1) obj).b) : ((Dz1) obj).b == null) {
                String str2 = this.c;
                if (str2 != null ? str2.equals(((Dz1) obj).c) : ((Dz1) obj).c == null) {
                    String str3 = this.d;
                    if (str3 != null ? str3.equals(((Dz1) obj).d) : ((Dz1) obj).d == null) {
                        String str4 = this.e;
                        if (str4 != null ? str4.equals(((Dz1) obj).e) : ((Dz1) obj).e == null) {
                            String str5 = this.f;
                            if (str5 != null ? str5.equals(((Dz1) obj).f) : ((Dz1) obj).f == null) {
                                String str6 = this.g;
                                if (str6 != null ? str6.equals(((Dz1) obj).g) : ((Dz1) obj).g == null) {
                                    String str7 = this.h;
                                    if (str7 == null) {
                                        if (((Dz1) obj).h == null) {
                                            z = true;
                                            return z;
                                        }
                                    } else if (str7.equals(((Dz1) obj).h)) {
                                        z = true;
                                        return z;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Bz1
    public String f() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Bz1
    public String g() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.Bz1
    public String h() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Integer num = this.a;
        int hashCode = num == null ? 0 : num.hashCode();
        String str = this.b;
        int hashCode2 = str == null ? 0 : str.hashCode();
        String str2 = this.c;
        int hashCode3 = str2 == null ? 0 : str2.hashCode();
        String str3 = this.d;
        int hashCode4 = str3 == null ? 0 : str3.hashCode();
        String str4 = this.e;
        int hashCode5 = str4 == null ? 0 : str4.hashCode();
        String str5 = this.f;
        int hashCode6 = str5 == null ? 0 : str5.hashCode();
        String str6 = this.g;
        int hashCode7 = str6 == null ? 0 : str6.hashCode();
        String str7 = this.h;
        if (str7 != null) {
            i = str7.hashCode();
        }
        return ((((((((((((((hashCode ^ 1000003) * 1000003) ^ hashCode2) * 1000003) ^ hashCode3) * 1000003) ^ hashCode4) * 1000003) ^ hashCode5) * 1000003) ^ hashCode6) * 1000003) ^ hashCode7) * 1000003) ^ i;
    }

    @DexIgnore
    @Override // com.fossil.Bz1
    public Integer i() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return "AndroidClientInfo{sdkVersion=" + this.a + ", model=" + this.b + ", hardware=" + this.c + ", device=" + this.d + ", product=" + this.e + ", osBuild=" + this.f + ", manufacturer=" + this.g + ", fingerprint=" + this.h + "}";
    }
}
