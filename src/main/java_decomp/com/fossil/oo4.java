package com.fossil;

import com.mapped.U04;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oo4 implements Factory<U04> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static /* final */ Oo4 a; // = new Oo4();
    }

    @DexIgnore
    public static Oo4 a() {
        return Ai.a;
    }

    @DexIgnore
    public static U04 c() {
        return new U04();
    }

    @DexIgnore
    public U04 b() {
        return c();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
