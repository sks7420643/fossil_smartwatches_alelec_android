package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H63 implements I63 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a; // = new Hw2(Yv2.a("com.google.android.gms.measurement")).d("measurement.service.audience.invalidate_config_cache_after_app_unisntall", true);

    @DexIgnore
    @Override // com.fossil.I63
    public final boolean zza() {
        return a.o().booleanValue();
    }
}
