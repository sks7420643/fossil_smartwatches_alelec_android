package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yf2 {
    @DexIgnore
    public static Context a;
    @DexIgnore
    public static Boolean b;

    @DexIgnore
    public static boolean a(Context context) {
        boolean booleanValue;
        synchronized (Yf2.class) {
            try {
                Context applicationContext = context.getApplicationContext();
                if (a == null || b == null || a != applicationContext) {
                    b = null;
                    if (Mf2.j()) {
                        b = Boolean.valueOf(applicationContext.getPackageManager().isInstantApp());
                    } else {
                        try {
                            context.getClassLoader().loadClass("com.google.android.instantapps.supervisor.InstantAppsRuntime");
                            b = Boolean.TRUE;
                        } catch (ClassNotFoundException e) {
                            b = Boolean.FALSE;
                        }
                    }
                    a = applicationContext;
                    booleanValue = b.booleanValue();
                } else {
                    booleanValue = b.booleanValue();
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        return booleanValue;
    }
}
