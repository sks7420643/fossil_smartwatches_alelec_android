package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T76 implements MembersInjector<CommuteTimeSettingsDefaultAddressActivity> {
    @DexIgnore
    public static void a(CommuteTimeSettingsDefaultAddressActivity commuteTimeSettingsDefaultAddressActivity, CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter) {
        commuteTimeSettingsDefaultAddressActivity.A = commuteTimeSettingsDefaultAddressPresenter;
    }
}
