package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.preset.data.source.DianaRecommendedPresetRepository;
import com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xy6 implements Factory<GetDianaDeviceSettingUseCase> {
    @DexIgnore
    public /* final */ Provider<WatchAppRepository> a;
    @DexIgnore
    public /* final */ Provider<ComplicationRepository> b;
    @DexIgnore
    public /* final */ Provider<DianaAppSettingRepository> c;
    @DexIgnore
    public /* final */ Provider<CategoryRepository> d;
    @DexIgnore
    public /* final */ Provider<DianaWatchFaceRepository> e;
    @DexIgnore
    public /* final */ Provider<RingStyleRepository> f;
    @DexIgnore
    public /* final */ Provider<V36> g;
    @DexIgnore
    public /* final */ Provider<D26> h;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> i;
    @DexIgnore
    public /* final */ Provider<An4> j;
    @DexIgnore
    public /* final */ Provider<WatchLocalizationRepository> k;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> l;
    @DexIgnore
    public /* final */ Provider<WFAssetRepository> m;
    @DexIgnore
    public /* final */ Provider<FileRepository> n;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> o;
    @DexIgnore
    public /* final */ Provider<DianaRecommendedPresetRepository> p;
    @DexIgnore
    public /* final */ Provider<UserRepository> q;
    @DexIgnore
    public /* final */ Provider<WFBackgroundPhotoRepository> r;

    @DexIgnore
    public Xy6(Provider<WatchAppRepository> provider, Provider<ComplicationRepository> provider2, Provider<DianaAppSettingRepository> provider3, Provider<CategoryRepository> provider4, Provider<DianaWatchFaceRepository> provider5, Provider<RingStyleRepository> provider6, Provider<V36> provider7, Provider<D26> provider8, Provider<NotificationSettingsDatabase> provider9, Provider<An4> provider10, Provider<WatchLocalizationRepository> provider11, Provider<AlarmsRepository> provider12, Provider<WFAssetRepository> provider13, Provider<FileRepository> provider14, Provider<DianaPresetRepository> provider15, Provider<DianaRecommendedPresetRepository> provider16, Provider<UserRepository> provider17, Provider<WFBackgroundPhotoRepository> provider18) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
        this.i = provider9;
        this.j = provider10;
        this.k = provider11;
        this.l = provider12;
        this.m = provider13;
        this.n = provider14;
        this.o = provider15;
        this.p = provider16;
        this.q = provider17;
        this.r = provider18;
    }

    @DexIgnore
    public static Xy6 a(Provider<WatchAppRepository> provider, Provider<ComplicationRepository> provider2, Provider<DianaAppSettingRepository> provider3, Provider<CategoryRepository> provider4, Provider<DianaWatchFaceRepository> provider5, Provider<RingStyleRepository> provider6, Provider<V36> provider7, Provider<D26> provider8, Provider<NotificationSettingsDatabase> provider9, Provider<An4> provider10, Provider<WatchLocalizationRepository> provider11, Provider<AlarmsRepository> provider12, Provider<WFAssetRepository> provider13, Provider<FileRepository> provider14, Provider<DianaPresetRepository> provider15, Provider<DianaRecommendedPresetRepository> provider16, Provider<UserRepository> provider17, Provider<WFBackgroundPhotoRepository> provider18) {
        return new Xy6(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14, provider15, provider16, provider17, provider18);
    }

    @DexIgnore
    public static GetDianaDeviceSettingUseCase c(WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, DianaAppSettingRepository dianaAppSettingRepository, CategoryRepository categoryRepository, DianaWatchFaceRepository dianaWatchFaceRepository, RingStyleRepository ringStyleRepository, V36 v36, D26 d26, NotificationSettingsDatabase notificationSettingsDatabase, An4 an4, WatchLocalizationRepository watchLocalizationRepository, AlarmsRepository alarmsRepository, WFAssetRepository wFAssetRepository, FileRepository fileRepository, DianaPresetRepository dianaPresetRepository, DianaRecommendedPresetRepository dianaRecommendedPresetRepository, UserRepository userRepository, WFBackgroundPhotoRepository wFBackgroundPhotoRepository) {
        return new GetDianaDeviceSettingUseCase(watchAppRepository, complicationRepository, dianaAppSettingRepository, categoryRepository, dianaWatchFaceRepository, ringStyleRepository, v36, d26, notificationSettingsDatabase, an4, watchLocalizationRepository, alarmsRepository, wFAssetRepository, fileRepository, dianaPresetRepository, dianaRecommendedPresetRepository, userRepository, wFBackgroundPhotoRepository);
    }

    @DexIgnore
    public GetDianaDeviceSettingUseCase b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get(), this.j.get(), this.k.get(), this.l.get(), this.m.get(), this.n.get(), this.o.get(), this.p.get(), this.q.get(), this.r.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
