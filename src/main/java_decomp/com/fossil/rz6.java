package com.fossil;

import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rz6 {
    @DexIgnore
    public /* final */ BaseActivity a;
    @DexIgnore
    public /* final */ Pz6 b;

    @DexIgnore
    public Rz6(BaseActivity baseActivity, Pz6 pz6) {
        Wg6.c(baseActivity, "mContext");
        Wg6.c(pz6, "mView");
        this.a = baseActivity;
        this.b = pz6;
    }

    @DexIgnore
    public final BaseActivity a() {
        return this.a;
    }

    @DexIgnore
    public final Pz6 b() {
        return this.b;
    }
}
