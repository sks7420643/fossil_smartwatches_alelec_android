package com.fossil;

import android.annotation.SuppressLint;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Iz0 extends Nz0 {
    @DexIgnore
    public static boolean f; // = true;

    @DexIgnore
    @Override // com.fossil.Nz0
    public void a(View view) {
    }

    @DexIgnore
    @Override // com.fossil.Nz0
    @SuppressLint({"NewApi"})
    public float c(View view) {
        if (f) {
            try {
                return view.getTransitionAlpha();
            } catch (NoSuchMethodError e) {
                f = false;
            }
        }
        return view.getAlpha();
    }

    @DexIgnore
    @Override // com.fossil.Nz0
    public void d(View view) {
    }

    @DexIgnore
    @Override // com.fossil.Nz0
    @SuppressLint({"NewApi"})
    public void g(View view, float f2) {
        if (f) {
            try {
                view.setTransitionAlpha(f2);
                return;
            } catch (NoSuchMethodError e) {
                f = false;
            }
        }
        view.setAlpha(f2);
    }
}
