package com.fossil;

import com.mapped.HandMovingConfig;
import com.mapped.R50;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ou extends Mu {
    @DexIgnore
    public static /* final */ Nu N; // = new Nu(null);
    @DexIgnore
    public /* final */ R50 L;
    @DexIgnore
    public /* final */ HandMovingConfig[] M;

    @DexIgnore
    public Ou(K5 k5, R50 r50, HandMovingConfig[] handMovingConfigArr) {
        super(Fu.n, Hs.A, k5, 0, 8);
        this.L = r50;
        this.M = handMovingConfigArr;
        this.E = true;
    }

    @DexIgnore
    @Override // com.fossil.Mu, com.fossil.Ps
    public byte[] L() {
        byte[] array = ByteBuffer.allocate((this.M.length * 5) + 2).order(ByteOrder.LITTLE_ENDIAN).put(this.L.a()).put((byte) this.M.length).put(N.a(this.M)).array();
        Wg6.b(array, "ByteBuffer.allocate(HEAD\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(G80.k(super.z(), Jd0.W0, Ey1.a(this.L)), Jd0.b1, Px1.a(this.M));
    }
}
