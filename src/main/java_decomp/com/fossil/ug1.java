package com.fossil;

import android.graphics.Bitmap;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ug1 implements Qb1<Bitmap, Bitmap> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Id1<Bitmap> {
        @DexIgnore
        public /* final */ Bitmap b;

        @DexIgnore
        public Ai(Bitmap bitmap) {
            this.b = bitmap;
        }

        @DexIgnore
        public Bitmap a() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.Id1
        public void b() {
        }

        @DexIgnore
        @Override // com.fossil.Id1
        public int c() {
            return Jk1.h(this.b);
        }

        @DexIgnore
        @Override // com.fossil.Id1
        public Class<Bitmap> d() {
            return Bitmap.class;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // com.fossil.Id1
        public /* bridge */ /* synthetic */ Bitmap get() {
            return a();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ boolean a(Bitmap bitmap, Ob1 ob1) throws IOException {
        return d(bitmap, ob1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Id1' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ Id1<Bitmap> b(Bitmap bitmap, int i, int i2, Ob1 ob1) throws IOException {
        return c(bitmap, i, i2, ob1);
    }

    @DexIgnore
    public Id1<Bitmap> c(Bitmap bitmap, int i, int i2, Ob1 ob1) {
        return new Ai(bitmap);
    }

    @DexIgnore
    public boolean d(Bitmap bitmap, Ob1 ob1) {
        return true;
    }
}
