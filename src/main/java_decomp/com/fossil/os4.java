package com.fossil;

import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Os4 {
    @DexIgnore
    @Vu3("profileID")
    public String a;
    @DexIgnore
    @Vu3("status")
    public String b;

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Os4) {
                Os4 os4 = (Os4) obj;
                if (!Wg6.a(this.a, os4.a) || !Wg6.a(this.b, os4.b)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "BlockResult(id=" + this.a + ", status=" + this.b + ")";
    }
}
