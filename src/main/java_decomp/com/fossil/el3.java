package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class El3 extends Ss2 implements Cl3 {
    @DexIgnore
    public El3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.internal.IMeasurementService");
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void D1(Xr3 xr3) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, xr3);
        i(13, d);
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void E1(Vg3 vg3, Or3 or3) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, vg3);
        Qt2.c(d, or3);
        i(1, d);
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void K1(Vg3 vg3, String str, String str2) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, vg3);
        d.writeString(str);
        d.writeString(str2);
        i(5, d);
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void N0(long j, String str, String str2, String str3) throws RemoteException {
        Parcel d = d();
        d.writeLong(j);
        d.writeString(str);
        d.writeString(str2);
        d.writeString(str3);
        i(10, d);
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void P1(Or3 or3) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, or3);
        i(6, d);
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final List<Fr3> S(String str, String str2, String str3, boolean z) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        d.writeString(str3);
        Qt2.d(d, z);
        Parcel e = e(15, d);
        ArrayList createTypedArrayList = e.createTypedArrayList(Fr3.CREATOR);
        e.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void S0(Or3 or3) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, or3);
        i(18, d);
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final List<Xr3> T0(String str, String str2, String str3) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        d.writeString(str3);
        Parcel e = e(17, d);
        ArrayList createTypedArrayList = e.createTypedArrayList(Xr3.CREATOR);
        e.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final List<Xr3> V0(String str, String str2, Or3 or3) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        Qt2.c(d, or3);
        Parcel e = e(16, d);
        ArrayList createTypedArrayList = e.createTypedArrayList(Xr3.CREATOR);
        e.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final String n0(Or3 or3) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, or3);
        Parcel e = e(11, d);
        String readString = e.readString();
        e.recycle();
        return readString;
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void n2(Bundle bundle, Or3 or3) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, bundle);
        Qt2.c(d, or3);
        i(19, d);
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final List<Fr3> o1(String str, String str2, boolean z, Or3 or3) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        Qt2.d(d, z);
        Qt2.c(d, or3);
        Parcel e = e(14, d);
        ArrayList createTypedArrayList = e.createTypedArrayList(Fr3.CREATOR);
        e.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final List<Fr3> p1(Or3 or3, boolean z) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, or3);
        Qt2.d(d, z);
        Parcel e = e(7, d);
        ArrayList createTypedArrayList = e.createTypedArrayList(Fr3.CREATOR);
        e.recycle();
        return createTypedArrayList;
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void r1(Or3 or3) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, or3);
        i(4, d);
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void s(Xr3 xr3, Or3 or3) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, xr3);
        Qt2.c(d, or3);
        i(12, d);
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final void u2(Fr3 fr3, Or3 or3) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, fr3);
        Qt2.c(d, or3);
        i(2, d);
    }

    @DexIgnore
    @Override // com.fossil.Cl3
    public final byte[] w2(Vg3 vg3, String str) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, vg3);
        d.writeString(str);
        Parcel e = e(9, d);
        byte[] createByteArray = e.createByteArray();
        e.recycle();
        return createByteArray;
    }
}
