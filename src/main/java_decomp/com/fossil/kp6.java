package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.M47;
import com.mapped.AlertDialogFragment;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.feedback.WrappedZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kp6 extends BaseFragment implements Yp6, View.OnClickListener, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public G37<V55> g;
    @DexIgnore
    public Xp6 h;
    @DexIgnore
    public Integer i;
    @DexIgnore
    public /* final */ String j; // = ThemeManager.l.a().d("primaryColor");
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Kp6 a() {
            return new Kp6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Kp6 b;

        @DexIgnore
        public Bi(Kp6 kp6) {
            this.b = kp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.e0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Kp6 b;

        @DexIgnore
        public Ci(Kp6 kp6) {
            this.b = kp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.r(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Kp6 b;

        @DexIgnore
        public Di(Kp6 kp6) {
            this.b = kp6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (Wr4.a.a().n()) {
                String a2 = M47.a(M47.Ci.REPAIR_CENTER, null);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = Kp6.l;
                local.d(str, "Support mail = " + a2);
                Kp6 kp6 = this.b;
                Wg6.b(a2, "url");
                kp6.O6(a2);
                return;
            }
            this.b.M6().p();
            this.b.M6().q("Delete Account - Contact Us - From app [Fossil] - [Android]");
        }
    }

    /*
    static {
        String simpleName = Kp6.class.getSimpleName();
        Wg6.b(simpleName, "DeleteAccountFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Xp6 xp6) {
        N6(xp6);
    }

    @DexIgnore
    public final Xp6 M6() {
        Xp6 xp6 = this.h;
        if (xp6 != null) {
            return xp6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public void N6(Xp6 xp6) {
        Wg6.c(xp6, "presenter");
        I14.l(xp6);
        Wg6.b(xp6, "Preconditions.checkNotNull(presenter)");
        this.h = xp6;
    }

    @DexIgnore
    public final void O6(String str) {
        J6(new Intent("android.intent.action.VIEW", Uri.parse(str)), l);
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = l;
        local.d(str2, "Inside .onDialogFragmentResult with TAG=" + str);
        if (!(str.length() == 0)) {
            if (str.hashCode() != 1069400824 || !str.equals("CONFIRM_DELETE_ACCOUNT")) {
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    ((BaseActivity) activity).R5(str, i2, intent);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
            } else if (i2 == 2131363373) {
                Xp6 xp6 = this.h;
                if (xp6 != null) {
                    xp6.n();
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Yp6
    public void a6() {
        B6().t();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            Wg6.b(activity, "it");
            if (!activity.isDestroyed() && !activity.isFinishing()) {
                FLogger.INSTANCE.getLocal().d(l, "deleteUser - successfully");
                WelcomeActivity.B.b(activity);
                activity.finish();
            }
        }
    }

    @DexIgnore
    public void e0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.Yp6
    public void j0(ZendeskFeedbackConfiguration zendeskFeedbackConfiguration) {
        Wg6.c(zendeskFeedbackConfiguration, "configuration");
        if (getContext() == null) {
            FLogger.INSTANCE.getLocal().e(ContactZendeskActivity.LOG_TAG, "Context is null, cannot start the context.");
            return;
        }
        Intent intent = new Intent(getContext(), ContactZendeskActivity.class);
        intent.putExtra(ContactZendeskActivity.EXTRA_CONTACT_CONFIGURATION, new WrappedZendeskFeedbackConfiguration(zendeskFeedbackConfiguration));
        startActivityForResult(intent, 1000);
    }

    @DexIgnore
    @Override // com.fossil.Yp6
    public void k() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.Yp6
    public void m() {
        b();
    }

    @DexIgnore
    @Override // com.fossil.Yp6
    public void o(int i2, String str) {
        Wg6.c(str, "message");
        if (isActive()) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.n0(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 != 1000) {
            super.onActivityResult(i2, i3, intent);
        } else if (i3 == -1) {
            Xp6 xp6 = this.h;
            if (xp6 != null) {
                xp6.o();
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onClick(View view) {
        Wg6.c(view, "v");
        if (view.getId() == 2131361851) {
            e0();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        V55 v55 = (V55) Aq0.f(LayoutInflater.from(getContext()), 2131558548, null, false, A6());
        this.g = new G37<>(this, v55);
        Wg6.b(v55, "binding");
        return v55.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Xp6 xp6 = this.h;
        if (xp6 != null) {
            xp6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Xp6 xp6 = this.h;
        if (xp6 != null) {
            xp6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        if (!TextUtils.isEmpty(this.j)) {
            this.i = Integer.valueOf(Color.parseColor(this.j));
        }
        G37<V55> g37 = this.g;
        if (g37 != null) {
            V55 a2 = g37.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new Bi(this));
                a2.s.setOnClickListener(new Ci(this));
                String q = Vt7.q(Um5.c(PortfolioApp.get.instance(), 2131887094).toString(), "contact_our_support_team", "", false, 4, null);
                FlexibleTextView flexibleTextView = a2.u;
                Wg6.b(flexibleTextView, "binding.tvDescription");
                flexibleTextView.setText(Html.fromHtml(q));
                Integer num = this.i;
                if (num != null) {
                    a2.u.setLinkTextColor(num.intValue());
                }
                a2.u.setOnClickListener(new Di(this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
