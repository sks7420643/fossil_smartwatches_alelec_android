package com.fossil;

import com.facebook.LegacyTokenHelper;
import com.mapped.Rc6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ep7 {
    @DexIgnore
    public static final <T> Class<T> a(T t) {
        Wg6.c(t, "$this$javaClass");
        Class<T> cls = (Class<T>) t.getClass();
        if (cls != null) {
            return cls;
        }
        throw new Rc6("null cannot be cast to non-null type java.lang.Class<T>");
    }

    @DexIgnore
    public static final <T> Class<T> b(Es7<T> es7) {
        Wg6.c(es7, "$this$javaObjectType");
        Class<T> cls = (Class<T>) ((Hq7) es7).b();
        if (cls.isPrimitive()) {
            String name = cls.getName();
            if (name != null) {
                switch (name.hashCode()) {
                    case -1325958191:
                        if (name.equals(LegacyTokenHelper.TYPE_DOUBLE)) {
                            cls = (Class<T>) Double.class;
                            break;
                        }
                        break;
                    case 104431:
                        if (name.equals(LegacyTokenHelper.TYPE_INTEGER)) {
                            cls = (Class<T>) Integer.class;
                            break;
                        }
                        break;
                    case 3039496:
                        if (name.equals(LegacyTokenHelper.TYPE_BYTE)) {
                            cls = (Class<T>) Byte.class;
                            break;
                        }
                        break;
                    case 3052374:
                        if (name.equals(LegacyTokenHelper.TYPE_CHAR)) {
                            cls = (Class<T>) Character.class;
                            break;
                        }
                        break;
                    case 3327612:
                        if (name.equals(LegacyTokenHelper.TYPE_LONG)) {
                            cls = (Class<T>) Long.class;
                            break;
                        }
                        break;
                    case 3625364:
                        if (name.equals("void")) {
                            cls = (Class<T>) Void.class;
                            break;
                        }
                        break;
                    case 64711720:
                        if (name.equals("boolean")) {
                            cls = (Class<T>) Boolean.class;
                            break;
                        }
                        break;
                    case 97526364:
                        if (name.equals(LegacyTokenHelper.TYPE_FLOAT)) {
                            cls = (Class<T>) Float.class;
                            break;
                        }
                        break;
                    case 109413500:
                        if (name.equals(LegacyTokenHelper.TYPE_SHORT)) {
                            cls = (Class<T>) Short.class;
                            break;
                        }
                        break;
                }
            }
            if (cls == null) {
                throw new Rc6("null cannot be cast to non-null type java.lang.Class<T>");
            }
        } else if (cls == null) {
            throw new Rc6("null cannot be cast to non-null type java.lang.Class<T>");
        }
        return cls;
    }

    @DexIgnore
    public static final <T> Es7<T> c(Class<T> cls) {
        Wg6.c(cls, "$this$kotlin");
        return Er7.b(cls);
    }
}
