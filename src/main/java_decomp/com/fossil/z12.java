package com.fossil;

import com.fossil.S32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Z12 implements S32.Ai {
    @DexIgnore
    public /* final */ K22 a;

    @DexIgnore
    public Z12(K22 k22) {
        this.a = k22;
    }

    @DexIgnore
    public static S32.Ai b(K22 k22) {
        return new Z12(k22);
    }

    @DexIgnore
    @Override // com.fossil.S32.Ai
    public Object a() {
        return Integer.valueOf(this.a.cleanUp());
    }
}
