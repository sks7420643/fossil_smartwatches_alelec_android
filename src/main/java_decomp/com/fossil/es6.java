package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Es6 implements MembersInjector<Ds6> {
    @DexIgnore
    public static void a(Ds6 ds6, Po4 po4) {
        ds6.g = po4;
    }
}
