package com.fossil;

import android.accounts.Account;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Jc2 extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai extends Ql2 implements Jc2 {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Rl2 implements Jc2 {
            @DexIgnore
            public Aii(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.common.internal.IAccountAccessor");
            }

            @DexIgnore
            @Override // com.fossil.Jc2
            public final Account m() throws RemoteException {
                Parcel e = e(2, d());
                Account account = (Account) Sl2.b(e, Account.CREATOR);
                e.recycle();
                return account;
            }
        }

        @DexIgnore
        public static Jc2 e(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IAccountAccessor");
            return queryLocalInterface instanceof Jc2 ? (Jc2) queryLocalInterface : new Aii(iBinder);
        }
    }

    @DexIgnore
    Account m() throws RemoteException;
}
