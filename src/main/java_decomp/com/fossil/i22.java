package com.fossil;

import com.fossil.L22;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I22 extends L22 {
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends L22.Ai {
        @DexIgnore
        public Long a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Long d;
        @DexIgnore
        public Integer e;

        @DexIgnore
        @Override // com.fossil.L22.Ai
        public L22 a() {
            String str = "";
            if (this.a == null) {
                str = " maxStorageSizeInBytes";
            }
            if (this.b == null) {
                str = str + " loadBatchSize";
            }
            if (this.c == null) {
                str = str + " criticalSectionEnterTimeoutMs";
            }
            if (this.d == null) {
                str = str + " eventCleanUpAge";
            }
            if (this.e == null) {
                str = str + " maxBlobByteSizePerRow";
            }
            if (str.isEmpty()) {
                return new I22(this.a.longValue(), this.b.intValue(), this.c.intValue(), this.d.longValue(), this.e.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.L22.Ai
        public L22.Ai b(int i) {
            this.c = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.L22.Ai
        public L22.Ai c(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.L22.Ai
        public L22.Ai d(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.L22.Ai
        public L22.Ai e(int i) {
            this.e = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.L22.Ai
        public L22.Ai f(long j) {
            this.a = Long.valueOf(j);
            return this;
        }
    }

    @DexIgnore
    public I22(long j, int i, int i2, long j2, int i3) {
        this.b = j;
        this.c = i;
        this.d = i2;
        this.e = j2;
        this.f = i3;
    }

    @DexIgnore
    @Override // com.fossil.L22
    public int b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.L22
    public long c() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.L22
    public int d() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.L22
    public int e() {
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof L22)) {
            return false;
        }
        L22 l22 = (L22) obj;
        return this.b == l22.f() && this.c == l22.d() && this.d == l22.b() && this.e == l22.c() && this.f == l22.e();
    }

    @DexIgnore
    @Override // com.fossil.L22
    public long f() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.b;
        int i = this.c;
        int i2 = this.d;
        long j2 = this.e;
        return ((((((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ i) * 1000003) ^ i2) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ this.f;
    }

    @DexIgnore
    public String toString() {
        return "EventStoreConfig{maxStorageSizeInBytes=" + this.b + ", loadBatchSize=" + this.c + ", criticalSectionEnterTimeoutMs=" + this.d + ", eventCleanUpAge=" + this.e + ", maxBlobByteSizePerRow=" + this.f + "}";
    }
}
