package com.fossil;

import android.animation.TimeInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uw3 {
    @DexIgnore
    public static /* final */ TimeInterpolator a; // = new LinearInterpolator();
    @DexIgnore
    public static /* final */ TimeInterpolator b; // = new Er0();
    @DexIgnore
    public static /* final */ TimeInterpolator c; // = new Dr0();
    @DexIgnore
    public static /* final */ TimeInterpolator d; // = new Fr0();
    @DexIgnore
    public static /* final */ TimeInterpolator e; // = new DecelerateInterpolator();

    @DexIgnore
    public static float a(float f, float f2, float f3) {
        return ((f2 - f) * f3) + f;
    }

    @DexIgnore
    public static int b(int i, int i2, float f) {
        return Math.round(((float) (i2 - i)) * f) + i;
    }
}
