package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h96 extends c96 {
    @DexIgnore
    public String e; // = "empty";
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h; // = "empty";
    @DexIgnore
    public String i;
    @DexIgnore
    public /* final */ ArrayList<Complication> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Complication> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ d96 l;
    @DexIgnore
    public /* final */ ComplicationRepository m;
    @DexIgnore
    public /* final */ on5 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1", f = "ComplicationSearchPresenter.kt", l = {75}, m = "invokeSuspend")
    public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $query;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $results;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ h96 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.h96$a$a")
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1$1", f = "ComplicationSearchPresenter.kt", l = {75}, m = "invokeSuspend")
        /* renamed from: com.fossil.h96$a$a  reason: collision with other inner class name */
        public static final class C0108a extends ko7 implements vp7<iv7, qn7<? super List<Complication>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0108a(a aVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0108a aVar = new C0108a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<Complication>> qn7) {
                throw null;
                //return ((C0108a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object queryComplicationByName;
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    ComplicationRepository complicationRepository = this.this$0.this$0.m;
                    String str = this.this$0.$query;
                    this.L$0 = iv7;
                    this.label = 1;
                    queryComplicationByName = complicationRepository.queryComplicationByName(str, this);
                    if (queryComplicationByName == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    queryComplicationByName = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return pm7.j0((Collection) queryComplicationByName);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(h96 h96, String str, dr7 dr7, qn7 qn7) {
            super(2, qn7);
            this.this$0 = h96;
            this.$query = str;
            this.$results = dr7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            a aVar = new a(this.this$0, this.$query, this.$results, qn7);
            aVar.p$ = (iv7) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            dr7 dr7;
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                if (this.$query.length() > 0) {
                    dr7 dr72 = this.$results;
                    dv7 i2 = this.this$0.i();
                    C0108a aVar = new C0108a(this, null);
                    this.L$0 = iv7;
                    this.L$1 = dr72;
                    this.label = 1;
                    g = eu7.g(i2, aVar, this);
                    if (g == d) {
                        return d;
                    }
                    dr7 = dr72;
                }
                this.this$0.y().B(this.this$0.C(this.$results.element));
                this.this$0.i = this.$query;
                return tl7.f3441a;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                g = obj;
                dr7 = (dr7) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            dr7.element = (T) ((List) g);
            this.this$0.y().B(this.this$0.C(this.$results.element));
            this.this$0.i = this.$query;
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$start$1", f = "ComplicationSearchPresenter.kt", l = {41}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ h96 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$start$1$1", f = "ComplicationSearchPresenter.kt", l = {42, 43}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x006f  */
            /* JADX WARNING: Removed duplicated region for block: B:15:0x0094  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r3 = 1
                    java.lang.Object r4 = com.fossil.yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x0071
                    if (r0 == r3) goto L_0x002f
                    if (r0 != r6) goto L_0x0027
                    java.lang.Object r0 = r7.L$1
                    java.util.ArrayList r0 = (java.util.ArrayList) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.iv7 r1 = (com.fossil.iv7) r1
                    com.fossil.el7.b(r8)
                    r1 = r8
                    r2 = r0
                L_0x001b:
                    r0 = r1
                    java.util.Collection r0 = (java.util.Collection) r0
                    boolean r0 = r2.addAll(r0)
                    java.lang.Boolean r0 = com.fossil.ao7.a(r0)
                L_0x0026:
                    return r0
                L_0x0027:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x002f:
                    java.lang.Object r0 = r7.L$1
                    java.util.ArrayList r0 = (java.util.ArrayList) r0
                    java.lang.Object r1 = r7.L$0
                    com.fossil.iv7 r1 = (com.fossil.iv7) r1
                    com.fossil.el7.b(r8)
                    r3 = r0
                    r2 = r8
                L_0x003c:
                    r0 = r2
                    java.util.Collection r0 = (java.util.Collection) r0
                    r3.addAll(r0)
                    com.fossil.h96$b r0 = r7.this$0
                    com.fossil.h96 r0 = r0.this$0
                    java.util.ArrayList r0 = com.fossil.h96.t(r0)
                    com.fossil.h96$b r2 = r7.this$0
                    com.fossil.h96 r2 = r2.this$0
                    com.portfolio.platform.data.source.ComplicationRepository r2 = com.fossil.h96.s(r2)
                    com.fossil.h96$b r3 = r7.this$0
                    com.fossil.h96 r3 = r3.this$0
                    com.fossil.on5 r3 = com.fossil.h96.u(r3)
                    java.util.List r3 = r3.l()
                    java.lang.String r5 = "sharedPreferencesManager\u2026licationSearchedIdsRecent"
                    com.fossil.pq7.b(r3, r5)
                    r7.L$0 = r1
                    r7.L$1 = r0
                    r7.label = r6
                    java.lang.Object r1 = r2.getComplicationByIds(r3, r7)
                    if (r1 != r4) goto L_0x0094
                    r0 = r4
                    goto L_0x0026
                L_0x0071:
                    com.fossil.el7.b(r8)
                    com.fossil.iv7 r1 = r7.p$
                    com.fossil.h96$b r0 = r7.this$0
                    com.fossil.h96 r0 = r0.this$0
                    java.util.ArrayList r0 = com.fossil.h96.r(r0)
                    com.fossil.h96$b r2 = r7.this$0
                    com.fossil.h96 r2 = r2.this$0
                    com.portfolio.platform.data.source.ComplicationRepository r2 = com.fossil.h96.s(r2)
                    r7.L$0 = r1
                    r7.L$1 = r0
                    r7.label = r3
                    java.lang.Object r2 = r2.getAllComplicationRaw(r7)
                    if (r2 != r4) goto L_0x0096
                    r0 = r4
                    goto L_0x0026
                L_0x0094:
                    r2 = r0
                    goto L_0x001b
                L_0x0096:
                    r3 = r0
                    goto L_0x003c
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.h96.b.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(h96 h96, qn7 qn7) {
            super(2, qn7);
            this.this$0 = h96;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                dv7 i2 = this.this$0.i();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(i2, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.z();
            return tl7.f3441a;
        }
    }

    @DexIgnore
    public h96(d96 d96, ComplicationRepository complicationRepository, on5 on5) {
        pq7.c(d96, "mView");
        pq7.c(complicationRepository, "mComplicationRepository");
        pq7.c(on5, "sharedPreferencesManager");
        this.l = d96;
        this.m = complicationRepository;
        this.n = on5;
    }

    @DexIgnore
    public Bundle A(Bundle bundle) {
        if (bundle != null) {
            bundle.putString(ViewHierarchy.DIMENSION_TOP_KEY, this.e);
            bundle.putString("bottom", this.f);
            bundle.putString(ViewHierarchy.DIMENSION_LEFT_KEY, this.g);
            bundle.putString("right", this.h);
        }
        return bundle;
    }

    @DexIgnore
    public void B() {
        this.l.M5(this);
    }

    @DexIgnore
    public final List<cl7<Complication, String>> C(List<Complication> list) {
        ArrayList arrayList = new ArrayList();
        for (Complication complication : list) {
            String complicationId = complication.getComplicationId();
            if (pq7.a(complicationId, this.e)) {
                arrayList.add(new cl7(complication, ViewHierarchy.DIMENSION_TOP_KEY));
            } else if (pq7.a(complicationId, this.f)) {
                arrayList.add(new cl7(complication, "bottom"));
            } else if (pq7.a(complicationId, this.h)) {
                arrayList.add(new cl7(complication, "right"));
            } else if (pq7.a(complicationId, this.g)) {
                arrayList.add(new cl7(complication, ViewHierarchy.DIMENSION_LEFT_KEY));
            } else {
                arrayList.add(new cl7(complication, ""));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final void D(String str, String str2, String str3, String str4) {
        pq7.c(str, "topComplication");
        pq7.c(str2, "bottomComplication");
        pq7.c(str3, "rightComplication");
        pq7.c(str4, "leftComplication");
        this.e = str;
        this.f = str2;
        this.h = str3;
        this.g = str4;
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void l() {
        xw7 unused = gu7.d(k(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.c96
    public void n() {
        this.i = "";
        this.l.D();
        z();
    }

    @DexIgnore
    @Override // com.fossil.c96
    public void o(Complication complication) {
        pq7.c(complication, "selectedComplication");
        List<String> l2 = this.n.l();
        pq7.b(l2, "sharedPreferencesManager\u2026licationSearchedIdsRecent");
        if (!l2.contains(complication.getComplicationId())) {
            l2.add(0, complication.getComplicationId());
            if (l2.size() > 5) {
                l2 = l2.subList(0, 5);
            }
            this.n.R0(l2);
        }
        this.l.b3(complication);
    }

    @DexIgnore
    @Override // com.fossil.c96
    public void p(String str) {
        pq7.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        dr7 dr7 = new dr7();
        dr7.element = (T) new ArrayList();
        xw7 unused = gu7.d(k(), null, null, new a(this, str, dr7, null), 3, null);
    }

    @DexIgnore
    public final d96 y() {
        return this.l;
    }

    @DexIgnore
    public final void z() {
        if (this.k.isEmpty()) {
            this.l.B(C(pm7.j0(this.j)));
        } else {
            this.l.K(C(pm7.j0(this.k)));
        }
        if (!TextUtils.isEmpty(this.i)) {
            d96 d96 = this.l;
            String str = this.i;
            if (str != null) {
                d96.z(str);
                String str2 = this.i;
                if (str2 != null) {
                    p(str2);
                } else {
                    pq7.i();
                    throw null;
                }
            } else {
                pq7.i();
                throw null;
            }
        }
    }
}
