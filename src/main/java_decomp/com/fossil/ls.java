package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ls extends Rs {
    @DexIgnore
    public /* final */ byte[] A;
    @DexIgnore
    public /* final */ C2 B;

    @DexIgnore
    public Ls(K5 k5, C2 c2) {
        super(Hs.G, k5);
        this.B = c2;
        byte[] a2 = c2.a();
        ByteBuffer order = ByteBuffer.allocate(a2.length + 3).order(ByteOrder.LITTLE_ENDIAN);
        Wg6.b(order, "ByteBuffer\n             \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(Ot.c.b);
        order.put(c2.b.b);
        order.put(c2.c);
        order.put(a2);
        byte[] array = order.array();
        Wg6.b(array, "ackData.array()");
        this.A = array;
    }

    @DexIgnore
    @Override // com.fossil.Ns
    public U5 D() {
        return new J6(N6.l, this.A, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(super.z(), Jd0.l1, this.B.toJSONObject());
    }
}
