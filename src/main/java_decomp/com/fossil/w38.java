package com.fossil;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.X509TrustManager;
import okhttp3.OkHttpClient;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class W38 {
    @DexIgnore
    public static /* final */ W38 a; // = i();
    @DexIgnore
    public static /* final */ Logger b; // = Logger.getLogger(OkHttpClient.class.getName());

    @DexIgnore
    public static List<String> b(List<T18> list) {
        ArrayList arrayList = new ArrayList(list.size());
        int size = list.size();
        for (int i = 0; i < size; i++) {
            T18 t18 = list.get(i);
            if (t18 != T18.HTTP_1_0) {
                arrayList.add(t18.toString());
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static byte[] e(List<T18> list) {
        I48 i48 = new I48();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            T18 t18 = list.get(i);
            if (t18 != T18.HTTP_1_0) {
                i48.w0(t18.toString().length());
                i48.D0(t18.toString());
            }
        }
        return i48.r();
    }

    @DexIgnore
    public static W38 i() {
        S38 s;
        W38 u = R38.u();
        if (u != null) {
            return u;
        }
        if (p() && (s = S38.s()) != null) {
            return s;
        }
        T38 s2 = T38.s();
        if (s2 != null) {
            return s2;
        }
        W38 s3 = U38.s();
        return s3 == null ? new W38() : s3;
    }

    @DexIgnore
    public static W38 j() {
        return a;
    }

    @DexIgnore
    public static boolean p() {
        if ("conscrypt".equals(System.getProperty("okhttp.platform"))) {
            return true;
        }
        return "Conscrypt".equals(Security.getProviders()[0].getName());
    }

    @DexIgnore
    public void a(SSLSocket sSLSocket) {
    }

    @DexIgnore
    public A48 c(X509TrustManager x509TrustManager) {
        return new Y38(d(x509TrustManager));
    }

    @DexIgnore
    public C48 d(X509TrustManager x509TrustManager) {
        return new Z38(x509TrustManager.getAcceptedIssuers());
    }

    @DexIgnore
    public void f(SSLSocketFactory sSLSocketFactory) {
    }

    @DexIgnore
    public void g(SSLSocket sSLSocket, String str, List<T18> list) {
    }

    @DexIgnore
    public void h(Socket socket, InetSocketAddress inetSocketAddress, int i) throws IOException {
        socket.connect(inetSocketAddress, i);
    }

    @DexIgnore
    public String k() {
        return "OkHttp";
    }

    @DexIgnore
    public SSLContext l() {
        if ("1.7".equals(System.getProperty("java.specification.version"))) {
            try {
                return SSLContext.getInstance("TLSv1.2");
            } catch (NoSuchAlgorithmException e) {
            }
        }
        try {
            return SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException e2) {
            throw new IllegalStateException("No TLS provider", e2);
        }
    }

    @DexIgnore
    public String m(SSLSocket sSLSocket) {
        return null;
    }

    @DexIgnore
    public Object n(String str) {
        if (b.isLoggable(Level.FINE)) {
            return new Throwable(str);
        }
        return null;
    }

    @DexIgnore
    public boolean o(String str) {
        return true;
    }

    @DexIgnore
    public void q(int i, String str, Throwable th) {
        b.log(i == 5 ? Level.WARNING : Level.INFO, str, th);
    }

    @DexIgnore
    public void r(String str, Object obj) {
        if (obj == null) {
            str = str + " To see where this was allocated, set the OkHttpClient logger level to FINE: Logger.getLogger(OkHttpClient.class.getName()).setLevel(Level.FINE);";
        }
        q(5, str, (Throwable) obj);
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName();
    }
}
