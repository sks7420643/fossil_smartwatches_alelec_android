package com.fossil;

import com.fossil.Dl7;
import com.mapped.Af6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vn7<T> implements Xe6<T>, Do7 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater<Vn7<?>, Object> c; // = AtomicReferenceFieldUpdater.newUpdater(Vn7.class, Object.class, Constants.RESULT);
    @DexIgnore
    public /* final */ Xe6<T> b;
    @DexIgnore
    public volatile Object result;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Vn7(Xe6<? super T> xe6) {
        this(xe6, Wn7.UNDECIDED);
        Wg6.c(xe6, "delegate");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.mapped.Xe6<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public Vn7(Xe6<? super T> xe6, Object obj) {
        Wg6.c(xe6, "delegate");
        this.b = xe6;
        this.result = obj;
    }

    @DexIgnore
    public final Object a() {
        Object obj = this.result;
        Wn7 wn7 = Wn7.UNDECIDED;
        if (obj == wn7) {
            if (c.compareAndSet(this, wn7, Yn7.d())) {
                return Yn7.d();
            }
            obj = this.result;
        }
        if (obj == Wn7.RESUMED) {
            return Yn7.d();
        }
        if (!(obj instanceof Dl7.Bi)) {
            return obj;
        }
        throw ((Dl7.Bi) obj).exception;
    }

    @DexIgnore
    @Override // com.fossil.Do7
    public Do7 getCallerFrame() {
        Xe6<T> xe6 = this.b;
        if (!(xe6 instanceof Do7)) {
            xe6 = null;
        }
        return (Do7) xe6;
    }

    @DexIgnore
    @Override // com.mapped.Xe6
    public Af6 getContext() {
        return this.b.getContext();
    }

    @DexIgnore
    @Override // com.fossil.Do7
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    @Override // com.mapped.Xe6
    public void resumeWith(Object obj) {
        while (true) {
            Object obj2 = this.result;
            Wn7 wn7 = Wn7.UNDECIDED;
            if (obj2 == wn7) {
                if (c.compareAndSet(this, wn7, obj)) {
                    return;
                }
            } else if (obj2 != Yn7.d()) {
                throw new IllegalStateException("Already resumed");
            } else if (c.compareAndSet(this, Yn7.d(), Wn7.RESUMED)) {
                this.b.resumeWith(obj);
                return;
            }
        }
    }

    @DexIgnore
    public String toString() {
        return "SafeContinuation for " + this.b;
    }
}
