package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hm1 extends Dm1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Hm1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public Hm1 a(Parcel parcel) {
            return new Hm1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Hm1 createFromParcel(Parcel parcel) {
            return new Hm1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Hm1[] newArray(int i) {
            return new Hm1[i];
        }
    }

    @DexIgnore
    public Hm1() {
        super(Fm1.EMPTY, null, null, null, 14);
    }

    @DexIgnore
    public /* synthetic */ Hm1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    public Hm1(Dt1 dt1, Et1 et1) {
        super(Fm1.EMPTY, null, dt1, et1, 2);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Hm1(Dt1 dt1, Et1 et1, int i, Qg6 qg6) {
        this(dt1, (i & 2) != 0 ? new Et1(Et1.CREATOR.a()) : et1);
    }
}
