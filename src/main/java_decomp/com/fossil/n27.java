package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n27 extends iq4<a, c, b> {
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f2455a;

        @DexIgnore
        public a(String str) {
            pq7.c(str, Constants.EMAIL);
            this.f2455a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f2455a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f2456a;

        @DexIgnore
        public b(int i, String str) {
            pq7.c(str, "errorMesagge");
            this.f2456a = i;
        }

        @DexIgnore
        public final int a() {
            return this.f2456a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ boolean f2457a;

        @DexIgnore
        public c(boolean z) {
            this.f2457a = z;
        }

        @DexIgnore
        public final boolean a() {
            return this.f2457a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.usecase.CheckAuthenticationEmailExisting", f = "CheckAuthenticationEmailExisting.kt", l = {27}, m = "run")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ n27 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(n27 n27, qn7 qn7) {
            super(qn7);
            this.this$0 = n27;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore
    public n27(UserRepository userRepository) {
        pq7.c(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "CheckAuthenticationEmailExisting";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.n27.a r8, com.fossil.qn7<java.lang.Object> r9) {
        /*
        // Method dump skipped, instructions count: 255
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.n27.k(com.fossil.n27$a, com.fossil.qn7):java.lang.Object");
    }
}
