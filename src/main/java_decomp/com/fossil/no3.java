package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class No3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ long b;
    @DexIgnore
    public /* final */ /* synthetic */ Un3 c;

    @DexIgnore
    public No3(Un3 un3, long j) {
        this.c = un3;
        this.b = j;
    }

    @DexIgnore
    public final void run() {
        this.c.l().p.b(this.b);
        this.c.d().M().b("Minimum session duration set", Long.valueOf(this.b));
    }
}
