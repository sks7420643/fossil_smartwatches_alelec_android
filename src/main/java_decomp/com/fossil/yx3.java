package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Paint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yx3 {
    @DexIgnore
    public /* final */ Xx3 a;
    @DexIgnore
    public /* final */ Xx3 b;
    @DexIgnore
    public /* final */ Xx3 c;
    @DexIgnore
    public /* final */ Xx3 d;
    @DexIgnore
    public /* final */ Xx3 e;
    @DexIgnore
    public /* final */ Xx3 f;
    @DexIgnore
    public /* final */ Xx3 g;
    @DexIgnore
    public /* final */ Paint h;

    @DexIgnore
    public Yx3(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(Nz3.c(context, Jw3.materialCalendarStyle, Dy3.class.getCanonicalName()), Tw3.MaterialCalendar);
        this.a = Xx3.a(context, obtainStyledAttributes.getResourceId(Tw3.MaterialCalendar_dayStyle, 0));
        this.g = Xx3.a(context, obtainStyledAttributes.getResourceId(Tw3.MaterialCalendar_dayInvalidStyle, 0));
        this.b = Xx3.a(context, obtainStyledAttributes.getResourceId(Tw3.MaterialCalendar_daySelectedStyle, 0));
        this.c = Xx3.a(context, obtainStyledAttributes.getResourceId(Tw3.MaterialCalendar_dayTodayStyle, 0));
        ColorStateList a2 = Oz3.a(context, obtainStyledAttributes, Tw3.MaterialCalendar_rangeFillColor);
        this.d = Xx3.a(context, obtainStyledAttributes.getResourceId(Tw3.MaterialCalendar_yearStyle, 0));
        this.e = Xx3.a(context, obtainStyledAttributes.getResourceId(Tw3.MaterialCalendar_yearSelectedStyle, 0));
        this.f = Xx3.a(context, obtainStyledAttributes.getResourceId(Tw3.MaterialCalendar_yearTodayStyle, 0));
        Paint paint = new Paint();
        this.h = paint;
        paint.setColor(a2.getDefaultColor());
        obtainStyledAttributes.recycle();
    }
}
