package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zo3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle b;
    @DexIgnore
    public /* final */ /* synthetic */ Xo3 c;
    @DexIgnore
    public /* final */ /* synthetic */ Xo3 d;
    @DexIgnore
    public /* final */ /* synthetic */ long e;
    @DexIgnore
    public /* final */ /* synthetic */ Ap3 f;

    @DexIgnore
    public Zo3(Ap3 ap3, Bundle bundle, Xo3 xo3, Xo3 xo32, long j) {
        this.f = ap3;
        this.b = bundle;
        this.c = xo3;
        this.d = xo32;
        this.e = j;
    }

    @DexIgnore
    public final void run() {
        Ap3.O(this.f, this.b, this.c, this.d, this.e);
    }
}
