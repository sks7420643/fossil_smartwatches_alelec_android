package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pp4 implements Factory<Lo4> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Pp4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Pp4 a(Uo4 uo4) {
        return new Pp4(uo4);
    }

    @DexIgnore
    public static Lo4 c(Uo4 uo4) {
        Lo4 w = uo4.w();
        Lk7.c(w, "Cannot return null from a non-@Nullable @Provides method");
        return w;
    }

    @DexIgnore
    public Lo4 b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
