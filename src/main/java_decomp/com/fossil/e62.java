package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E62 extends Exception {
    @DexIgnore
    public /* final */ int errorCode;

    @DexIgnore
    public E62(int i) {
        this.errorCode = i;
    }
}
