package com.fossil;

import android.text.TextUtils;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bo3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ long b;
    @DexIgnore
    public /* final */ /* synthetic */ Un3 c;

    @DexIgnore
    public Bo3(Un3 un3, long j) {
        this.c = un3;
        this.b = j;
    }

    @DexIgnore
    public final void run() {
        Un3 un3 = this.c;
        long j = this.b;
        un3.h();
        un3.f();
        un3.x();
        un3.d().M().a("Resetting analytics data (FE)");
        Jq3 u = un3.u();
        u.h();
        u.e.a();
        boolean o = un3.a.o();
        Xl3 l = un3.l();
        l.j.b(j);
        if (!TextUtils.isEmpty(l.l().z.a())) {
            l.z.b(null);
        }
        if (W63.a() && l.m().s(Xg3.w0)) {
            l.u.b(0);
        }
        if (!l.m().G()) {
            l.z(!o);
        }
        l.A.b(null);
        l.B.b(0);
        l.C.b(null);
        un3.r().X();
        if (W63.a() && un3.m().s(Xg3.w0)) {
            un3.u().d.a();
        }
        un3.i = !o;
        this.c.r().S(new AtomicReference<>());
    }
}
