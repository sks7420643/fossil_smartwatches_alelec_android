package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Parcel;
import com.facebook.GraphRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gl3 extends Lm3 {
    @DexIgnore
    public /* final */ Fl3 c; // = new Fl3(this, e(), "google_app_measurement_local.db");
    @DexIgnore
    public boolean d;

    @DexIgnore
    public Gl3(Pm3 pm3) {
        super(pm3);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long B(android.database.sqlite.SQLiteDatabase r10) {
        /*
            r9 = 0
            java.lang.String r1 = "messages"
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ all -> 0x0039 }
            r0 = 0
            java.lang.String r3 = "rowid"
            r2[r0] = r3     // Catch:{ all -> 0x0039 }
            java.lang.String r3 = "type=?"
            r0 = 1
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ all -> 0x0039 }
            r0 = 0
            java.lang.String r5 = "3"
            r4[r0] = r5     // Catch:{ all -> 0x0039 }
            r5 = 0
            r6 = 0
            java.lang.String r7 = "rowid desc"
            java.lang.String r8 = "1"
            r0 = r10
            android.database.Cursor r2 = r0.query(r1, r2, r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x0039 }
            boolean r0 = r2.moveToFirst()     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x0031
            r0 = 0
            long r0 = r2.getLong(r0)     // Catch:{ all -> 0x0041 }
            if (r2 == 0) goto L_0x0030
            r2.close()
        L_0x0030:
            return r0
        L_0x0031:
            if (r2 == 0) goto L_0x0036
            r2.close()
        L_0x0036:
            r0 = -1
            goto L_0x0030
        L_0x0039:
            r0 = move-exception
            r1 = r9
        L_0x003b:
            if (r1 == 0) goto L_0x0040
            r1.close()
        L_0x0040:
            throw r0
        L_0x0041:
            r0 = move-exception
            r1 = r2
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Gl3.B(android.database.sqlite.SQLiteDatabase):long");
    }

    @DexIgnore
    @Override // com.fossil.Lm3
    public final boolean A() {
        return false;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x020f, code lost:
        r3 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x0210, code lost:
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x0253, code lost:
        r3 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0257, code lost:
        r3 = e;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x020f A[ExcHandler: all (th java.lang.Throwable), Splitter:B:13:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x0255 A[ExcHandler: SQLiteDatabaseLockedException (e android.database.sqlite.SQLiteDatabaseLockedException), Splitter:B:13:0x002c] */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x00c9 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0125  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0154  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.Zc2> C(int r19) {
        /*
        // Method dump skipped, instructions count: 604
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Gl3.C(int):java.util.List");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00bd A[SYNTHETIC, Splitter:B:36:0x00bd] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00fb  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0110  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0125  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x00e1 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean D(int r14, byte[] r15) {
        /*
        // Method dump skipped, instructions count: 342
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Gl3.D(int, byte[]):boolean");
    }

    @DexIgnore
    public final boolean E(Vg3 vg3) {
        Parcel obtain = Parcel.obtain();
        vg3.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return D(0, marshall);
        }
        d().G().a("Event is too long for local database. Sending event directly to service");
        return false;
    }

    @DexIgnore
    public final boolean F(Fr3 fr3) {
        Parcel obtain = Parcel.obtain();
        fr3.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return D(1, marshall);
        }
        d().G().a("User property too long for local database. Sending directly to service");
        return false;
    }

    @DexIgnore
    public final boolean G(Xr3 xr3) {
        k();
        byte[] m0 = Kr3.m0(xr3);
        if (m0.length <= 131072) {
            return D(2, m0);
        }
        d().G().a("Conditional user property too long for local database. Sending directly to service");
        return false;
    }

    @DexIgnore
    public final void H() {
        f();
        h();
        try {
            int delete = K().delete(GraphRequest.DEBUG_MESSAGES_KEY, null, null) + 0;
            if (delete > 0) {
                d().N().b("Reset local analytics data. records", Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            d().F().b("Error resetting local analytics data. error", e);
        }
    }

    @DexIgnore
    public final boolean I() {
        return D(3, new byte[0]);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0052 A[SYNTHETIC, Splitter:B:24:0x0052] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0070 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0070 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean J() {
        /*
            r12 = this;
            r6 = 5
            r4 = 0
            r2 = 1
            r1 = 0
            r12.h()
            r12.f()
            boolean r0 = r12.d
            if (r0 == 0) goto L_0x0010
            r0 = r1
        L_0x000f:
            return r0
        L_0x0010:
            boolean r0 = r12.L()
            if (r0 != 0) goto L_0x0018
            r0 = r1
            goto L_0x000f
        L_0x0018:
            r0 = r6
            r7 = r1
        L_0x001a:
            if (r7 >= r6) goto L_0x00a2
            android.database.sqlite.SQLiteDatabase r3 = r12.K()     // Catch:{ SQLiteFullException -> 0x0082, SQLiteDatabaseLockedException -> 0x0074, SQLiteException -> 0x004e, all -> 0x009a }
            if (r3 != 0) goto L_0x002c
            r5 = 1
            r12.d = r5     // Catch:{ SQLiteFullException -> 0x00b2, SQLiteDatabaseLockedException -> 0x00b4, SQLiteException -> 0x00b6 }
            if (r3 == 0) goto L_0x002a
            r3.close()
        L_0x002a:
            r0 = r1
            goto L_0x000f
        L_0x002c:
            r3.beginTransaction()
            java.lang.String r5 = "messages"
            java.lang.String r8 = "type == ?"
            r9 = 1
            java.lang.String[] r9 = new java.lang.String[r9]
            r10 = 0
            r11 = 3
            java.lang.String r11 = java.lang.Integer.toString(r11)
            r9[r10] = r11
            r3.delete(r5, r8, r9)
            r3.setTransactionSuccessful()
            r3.endTransaction()
            if (r3 == 0) goto L_0x004c
            r3.close()
        L_0x004c:
            r0 = r2
            goto L_0x000f
        L_0x004e:
            r5 = move-exception
            r3 = r4
        L_0x0050:
            if (r3 == 0) goto L_0x005b
            boolean r8 = r3.inTransaction()     // Catch:{ all -> 0x00b8 }
            if (r8 == 0) goto L_0x005b
            r3.endTransaction()     // Catch:{ all -> 0x00b8 }
        L_0x005b:
            com.fossil.Kl3 r8 = r12.d()     // Catch:{ all -> 0x00b8 }
            com.fossil.Nl3 r8 = r8.F()     // Catch:{ all -> 0x00b8 }
            java.lang.String r9 = "Error deleting app launch break from local database"
            r8.b(r9, r5)     // Catch:{ all -> 0x00b8 }
            r5 = 1
            r12.d = r5     // Catch:{ all -> 0x00b8 }
            if (r3 == 0) goto L_0x0070
            r3.close()
        L_0x0070:
            int r3 = r7 + 1
            r7 = r3
            goto L_0x001a
        L_0x0074:
            r3 = move-exception
            r3 = r4
        L_0x0076:
            long r8 = (long) r0
            android.os.SystemClock.sleep(r8)
            int r0 = r0 + 20
            if (r3 == 0) goto L_0x0070
            r3.close()
            goto L_0x0070
        L_0x0082:
            r5 = move-exception
            r3 = r4
        L_0x0084:
            com.fossil.Kl3 r8 = r12.d()
            com.fossil.Nl3 r8 = r8.F()
            java.lang.String r9 = "Error deleting app launch break from local database"
            r8.b(r9, r5)
            r5 = 1
            r12.d = r5
            if (r3 == 0) goto L_0x0070
            r3.close()
            goto L_0x0070
        L_0x009a:
            r0 = move-exception
            r1 = r4
        L_0x009c:
            if (r1 == 0) goto L_0x00a1
            r1.close()
        L_0x00a1:
            throw r0
        L_0x00a2:
            com.fossil.Kl3 r0 = r12.d()
            com.fossil.Nl3 r0 = r0.I()
            java.lang.String r2 = "Error deleting app launch break from local database in reasonable time"
            r0.a(r2)
            r0 = r1
            goto L_0x000f
        L_0x00b2:
            r5 = move-exception
            goto L_0x0084
        L_0x00b4:
            r5 = move-exception
            goto L_0x0076
        L_0x00b6:
            r5 = move-exception
            goto L_0x0050
        L_0x00b8:
            r0 = move-exception
            r1 = r3
            goto L_0x009c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Gl3.J():boolean");
    }

    @DexIgnore
    public final SQLiteDatabase K() throws SQLiteException {
        if (this.d) {
            return null;
        }
        SQLiteDatabase writableDatabase = this.c.getWritableDatabase();
        if (writableDatabase != null) {
            return writableDatabase;
        }
        this.d = true;
        return null;
    }

    @DexIgnore
    public final boolean L() {
        return e().getDatabasePath("google_app_measurement_local.db").exists();
    }
}
