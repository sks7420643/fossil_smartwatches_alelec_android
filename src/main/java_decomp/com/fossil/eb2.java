package com.fossil;

import android.util.Log;
import com.fossil.M62;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eb2 implements Ht3<Map<G72<?>, String>> {
    @DexIgnore
    public T72 a;
    @DexIgnore
    public /* final */ /* synthetic */ Db2 b;

    @DexIgnore
    public Eb2(Db2 db2, T72 t72) {
        this.b = db2;
        this.a = t72;
    }

    @DexIgnore
    public final void a() {
        this.a.onComplete();
    }

    @DexIgnore
    @Override // com.fossil.Ht3
    public final void onComplete(Nt3<Map<G72<?>, String>> nt3) {
        Db2.p(this.b).lock();
        try {
            if (!Db2.z(this.b)) {
                this.a.onComplete();
                return;
            }
            if (nt3.q()) {
                Db2.x(this.b, new Zi0(Db2.K(this.b).size()));
                for (Bb2 bb2 : Db2.K(this.b).values()) {
                    Db2.E(this.b).put(bb2.a(), Z52.f);
                }
            } else if (nt3.l() instanceof O62) {
                O62 o62 = (O62) nt3.l();
                if (Db2.C(this.b)) {
                    Db2.x(this.b, new Zi0(Db2.K(this.b).size()));
                    for (Bb2 bb22 : Db2.K(this.b).values()) {
                        G72 a2 = bb22.a();
                        Z52 connectionResult = o62.getConnectionResult((Q62<? extends M62.Di>) bb22);
                        if (Db2.r(this.b, bb22, connectionResult)) {
                            Db2.E(this.b).put(a2, new Z52(16));
                        } else {
                            Db2.E(this.b).put(a2, connectionResult);
                        }
                    }
                } else {
                    Db2.x(this.b, o62.zaj());
                }
            } else {
                Log.e("ConnectionlessGAC", "Unexpected availability exception", nt3.l());
                Db2.x(this.b, Collections.emptyMap());
            }
            if (this.b.c()) {
                Db2.B(this.b).putAll(Db2.E(this.b));
                if (Db2.D(this.b) == null) {
                    Db2.G(this.b);
                    Db2.H(this.b);
                    Db2.J(this.b).signalAll();
                }
            }
            this.a.onComplete();
            Db2.p(this.b).unlock();
        } finally {
            Db2.p(this.b).unlock();
        }
    }
}
