package com.fossil;

import com.fossil.W94;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class X94 implements U94 {
    @DexIgnore
    public static /* final */ Charset d; // = Charset.forName("UTF-8");
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public W94 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements W94.Di {
        @DexIgnore
        public /* final */ /* synthetic */ byte[] a;
        @DexIgnore
        public /* final */ /* synthetic */ int[] b;

        @DexIgnore
        public Ai(X94 x94, byte[] bArr, int[] iArr) {
            this.a = bArr;
            this.b = iArr;
        }

        @DexIgnore
        @Override // com.fossil.W94.Di
        public void a(InputStream inputStream, int i) throws IOException {
            try {
                inputStream.read(this.a, this.b[0], i);
                int[] iArr = this.b;
                iArr[0] = iArr[0] + i;
            } finally {
                inputStream.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi {
        @DexIgnore
        public /* final */ byte[] a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Bi(X94 x94, byte[] bArr, int i) {
            this.a = bArr;
            this.b = i;
        }
    }

    @DexIgnore
    public X94(File file, int i) {
        this.a = file;
        this.b = i;
    }

    @DexIgnore
    @Override // com.fossil.U94
    public void a() {
        R84.e(this.c, "There was a problem closing the Crashlytics log file.");
        this.c = null;
    }

    @DexIgnore
    @Override // com.fossil.U94
    public String b() {
        byte[] c2 = c();
        if (c2 != null) {
            return new String(c2, d);
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.U94
    public byte[] c() {
        Bi g = g();
        if (g == null) {
            return null;
        }
        int i = g.b;
        byte[] bArr = new byte[i];
        System.arraycopy(g.a, 0, bArr, 0, i);
        return bArr;
    }

    @DexIgnore
    @Override // com.fossil.U94
    public void d() {
        a();
        this.a.delete();
    }

    @DexIgnore
    @Override // com.fossil.U94
    public void e(long j, String str) {
        h();
        f(j, str);
    }

    @DexIgnore
    public final void f(long j, String str) {
        if (this.c != null) {
            String str2 = str == null ? "null" : str;
            try {
                int i = this.b / 4;
                if (str2.length() > i) {
                    str2 = "..." + str2.substring(str2.length() - i);
                }
                this.c.h(String.format(Locale.US, "%d %s%n", Long.valueOf(j), str2.replaceAll("\r", " ").replaceAll("\n", " ")).getBytes(d));
                while (!this.c.A() && this.c.T() > this.b) {
                    this.c.M();
                }
            } catch (IOException e) {
                X74.f().e("There was a problem writing to the Crashlytics log.", e);
            }
        }
    }

    @DexIgnore
    public final Bi g() {
        if (!this.a.exists()) {
            return null;
        }
        h();
        W94 w94 = this.c;
        if (w94 == null) {
            return null;
        }
        int[] iArr = {0};
        byte[] bArr = new byte[w94.T()];
        try {
            this.c.m(new Ai(this, bArr, iArr));
        } catch (IOException e) {
            X74.f().e("A problem occurred while reading the Crashlytics log file.", e);
        }
        return new Bi(this, bArr, iArr[0]);
    }

    @DexIgnore
    public final void h() {
        if (this.c == null) {
            try {
                this.c = new W94(this.a);
            } catch (IOException e) {
                X74 f = X74.f();
                f.e("Could not open log file: " + this.a, e);
            }
        }
    }
}
