package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class P21 extends K21<Boolean> {
    @DexIgnore
    public P21(Context context, K41 k41) {
        super(W21.c(context, k41).e());
    }

    @DexIgnore
    @Override // com.fossil.K21
    public boolean b(O31 o31) {
        return o31.j.i();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.K21
    public /* bridge */ /* synthetic */ boolean c(Boolean bool) {
        return i(bool);
    }

    @DexIgnore
    public boolean i(Boolean bool) {
        return !bool.booleanValue();
    }
}
