package com.fossil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.Handler;
import androidx.collection.SimpleArrayMap;
import com.fossil.An0;
import com.fossil.Nl0;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zm0 {
    @DexIgnore
    public static /* final */ Ej0<String, Typeface> a; // = new Ej0<>(16);
    @DexIgnore
    public static /* final */ An0 b; // = new An0("fonts", 10, 10000);
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public static /* final */ SimpleArrayMap<String, ArrayList<An0.Di<Gi>>> d; // = new SimpleArrayMap<>();
    @DexIgnore
    public static /* final */ Comparator<byte[]> e; // = new Di();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Callable<Gi> {
        @DexIgnore
        public /* final */ /* synthetic */ Context a;
        @DexIgnore
        public /* final */ /* synthetic */ Ym0 b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;

        @DexIgnore
        public Ai(Context context, Ym0 ym0, int i, String str) {
            this.a = context;
            this.b = ym0;
            this.c = i;
            this.d = str;
        }

        @DexIgnore
        public Gi a() throws Exception {
            Gi f = Zm0.f(this.a, this.b, this.c);
            Typeface typeface = f.a;
            if (typeface != null) {
                Zm0.a.f(this.d, typeface);
            }
            return f;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Gi call() throws Exception {
            return a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements An0.Di<Gi> {
        @DexIgnore
        public /* final */ /* synthetic */ Nl0.Ai a;
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;

        @DexIgnore
        public Bi(Nl0.Ai ai, Handler handler) {
            this.a = ai;
            this.b = handler;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.An0.Di
        public /* bridge */ /* synthetic */ void a(Gi gi) {
            b(gi);
        }

        @DexIgnore
        public void b(Gi gi) {
            if (gi == null) {
                this.a.a(1, this.b);
                return;
            }
            int i = gi.b;
            if (i == 0) {
                this.a.b(gi.a, this.b);
            } else {
                this.a.a(i, this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements An0.Di<Gi> {
        @DexIgnore
        public /* final */ /* synthetic */ String a;

        @DexIgnore
        public Ci(String str) {
            this.a = str;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.An0.Di
        public /* bridge */ /* synthetic */ void a(Gi gi) {
            b(gi);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x001f, code lost:
            if (r1 >= r0.size()) goto L_0x0010;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0021, code lost:
            r0.get(r1).a(r5);
            r1 = r1 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0019, code lost:
            r1 = 0;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void b(com.fossil.Zm0.Gi r5) {
            /*
                r4 = this;
                java.lang.Object r1 = com.fossil.Zm0.c
                monitor-enter(r1)
                androidx.collection.SimpleArrayMap<java.lang.String, java.util.ArrayList<com.fossil.An0$Di<com.fossil.Zm0$Gi>>> r0 = com.fossil.Zm0.d     // Catch:{ all -> 0x002e }
                java.lang.String r2 = r4.a     // Catch:{ all -> 0x002e }
                java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x002e }
                java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ all -> 0x002e }
                if (r0 != 0) goto L_0x0011
                monitor-exit(r1)     // Catch:{ all -> 0x002e }
            L_0x0010:
                return
            L_0x0011:
                androidx.collection.SimpleArrayMap<java.lang.String, java.util.ArrayList<com.fossil.An0$Di<com.fossil.Zm0$Gi>>> r2 = com.fossil.Zm0.d     // Catch:{ all -> 0x002e }
                java.lang.String r3 = r4.a     // Catch:{ all -> 0x002e }
                r2.remove(r3)     // Catch:{ all -> 0x002e }
                monitor-exit(r1)     // Catch:{ all -> 0x002e }
                r1 = 0
                r2 = r1
            L_0x001b:
                int r1 = r0.size()
                if (r2 >= r1) goto L_0x0010
                java.lang.Object r1 = r0.get(r2)
                com.fossil.An0$Di r1 = (com.fossil.An0.Di) r1
                r1.a(r5)
                int r1 = r2 + 1
                r2 = r1
                goto L_0x001b
            L_0x002e:
                r0 = move-exception
                monitor-exit(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Zm0.Ci.b(com.fossil.Zm0$Gi):void");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements Comparator<byte[]> {
        @DexIgnore
        public int a(byte[] bArr, byte[] bArr2) {
            int i;
            int i2;
            if (bArr.length != bArr2.length) {
                int length = bArr.length;
                i2 = bArr2.length;
                i = length;
            } else {
                for (int i3 = 0; i3 < bArr.length; i3++) {
                    if (bArr[i3] != bArr2[i3]) {
                        byte b = bArr[i3];
                        i2 = bArr2[i3];
                        i = b;
                    }
                }
                return 0;
            }
            return (i == 1 ? 1 : 0) - (i2 == 1 ? 1 : 0);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // java.util.Comparator
        public /* bridge */ /* synthetic */ int compare(byte[] bArr, byte[] bArr2) {
            return a(bArr, bArr2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ Fi[] b;

        @DexIgnore
        public Ei(int i, Fi[] fiArr) {
            this.a = i;
            this.b = fiArr;
        }

        @DexIgnore
        public Fi[] a() {
            return this.b;
        }

        @DexIgnore
        public int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Fi {
        @DexIgnore
        public /* final */ Uri a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ int e;

        @DexIgnore
        public Fi(Uri uri, int i, int i2, boolean z, int i3) {
            Pn0.d(uri);
            this.a = uri;
            this.b = i;
            this.c = i2;
            this.d = z;
            this.e = i3;
        }

        @DexIgnore
        public int a() {
            return this.e;
        }

        @DexIgnore
        public int b() {
            return this.b;
        }

        @DexIgnore
        public Uri c() {
            return this.a;
        }

        @DexIgnore
        public int d() {
            return this.c;
        }

        @DexIgnore
        public boolean e() {
            return this.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi {
        @DexIgnore
        public /* final */ Typeface a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Gi(Typeface typeface, int i) {
            this.a = typeface;
            this.b = i;
        }
    }

    @DexIgnore
    public static List<byte[]> a(Signature[] signatureArr) {
        ArrayList arrayList = new ArrayList();
        for (Signature signature : signatureArr) {
            arrayList.add(signature.toByteArray());
        }
        return arrayList;
    }

    @DexIgnore
    public static boolean b(List<byte[]> list, List<byte[]> list2) {
        if (list.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list.size(); i++) {
            if (!Arrays.equals(list.get(i), list2.get(i))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static Ei c(Context context, CancellationSignal cancellationSignal, Ym0 ym0) throws PackageManager.NameNotFoundException {
        ProviderInfo h = h(context.getPackageManager(), ym0, context.getResources());
        return h == null ? new Ei(1, null) : new Ei(0, e(context, ym0, h.authority, cancellationSignal));
    }

    @DexIgnore
    public static List<List<byte[]>> d(Ym0 ym0, Resources resources) {
        return ym0.a() != null ? ym0.a() : Kl0.c(resources, ym0.b());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0146  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.Zm0.Fi[] e(android.content.Context r18, com.fossil.Ym0 r19, java.lang.String r20, android.os.CancellationSignal r21) {
        /*
        // Method dump skipped, instructions count: 342
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Zm0.e(android.content.Context, com.fossil.Ym0, java.lang.String, android.os.CancellationSignal):com.fossil.Zm0$Fi[]");
    }

    @DexIgnore
    public static Gi f(Context context, Ym0 ym0, int i) {
        try {
            Ei c2 = c(context, null, ym0);
            int i2 = -3;
            if (c2.b() == 0) {
                Typeface b2 = Sl0.b(context, null, c2.a(), i);
                if (b2 != null) {
                    i2 = 0;
                }
                return new Gi(b2, i2);
            }
            if (c2.b() == 1) {
                i2 = -2;
            }
            return new Gi(null, i2);
        } catch (PackageManager.NameNotFoundException e2) {
            return new Gi(null, -1);
        }
    }

    @DexIgnore
    public static Typeface g(Context context, Ym0 ym0, Nl0.Ai ai, Handler handler, boolean z, int i, int i2) {
        String str = ym0.c() + ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR + i2;
        Typeface d2 = a.d(str);
        if (d2 != null) {
            if (ai != null) {
                ai.d(d2);
            }
            return d2;
        } else if (!z || i != -1) {
            Ai ai2 = new Ai(context, ym0, i2, str);
            if (z) {
                try {
                    return ((Gi) b.e(ai2, i)).a;
                } catch (InterruptedException e2) {
                    return null;
                }
            } else {
                Bi bi = ai == null ? null : new Bi(ai, handler);
                synchronized (c) {
                    ArrayList<An0.Di<Gi>> arrayList = d.get(str);
                    if (arrayList != null) {
                        if (bi != null) {
                            arrayList.add(bi);
                        }
                        return null;
                    }
                    if (bi != null) {
                        ArrayList<An0.Di<Gi>> arrayList2 = new ArrayList<>();
                        arrayList2.add(bi);
                        d.put(str, arrayList2);
                    }
                    b.d(ai2, new Ci(str));
                    return null;
                }
            }
        } else {
            Gi f = f(context, ym0, i2);
            if (ai != null) {
                int i3 = f.b;
                if (i3 == 0) {
                    ai.b(f.a, handler);
                } else {
                    ai.a(i3, handler);
                }
            }
            return f.a;
        }
    }

    @DexIgnore
    public static ProviderInfo h(PackageManager packageManager, Ym0 ym0, Resources resources) throws PackageManager.NameNotFoundException {
        String d2 = ym0.d();
        ProviderInfo resolveContentProvider = packageManager.resolveContentProvider(d2, 0);
        if (resolveContentProvider == null) {
            throw new PackageManager.NameNotFoundException("No package found for authority: " + d2);
        } else if (resolveContentProvider.packageName.equals(ym0.e())) {
            List<byte[]> a2 = a(packageManager.getPackageInfo(resolveContentProvider.packageName, 64).signatures);
            Collections.sort(a2, e);
            List<List<byte[]>> d3 = d(ym0, resources);
            for (int i = 0; i < d3.size(); i++) {
                ArrayList arrayList = new ArrayList(d3.get(i));
                Collections.sort(arrayList, e);
                if (b(a2, arrayList)) {
                    return resolveContentProvider;
                }
            }
            return null;
        } else {
            throw new PackageManager.NameNotFoundException("Found content provider " + d2 + ", but package was not " + ym0.e());
        }
    }

    @DexIgnore
    public static Map<Uri, ByteBuffer> i(Context context, Fi[] fiArr, CancellationSignal cancellationSignal) {
        HashMap hashMap = new HashMap();
        for (Fi fi : fiArr) {
            if (fi.a() == 0) {
                Uri c2 = fi.c();
                if (!hashMap.containsKey(c2)) {
                    hashMap.put(c2, Zl0.f(context, cancellationSignal, c2));
                }
            }
        }
        return Collections.unmodifiableMap(hashMap);
    }
}
