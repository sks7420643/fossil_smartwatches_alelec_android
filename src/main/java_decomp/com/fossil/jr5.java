package com.fossil;

import com.mapped.An4;
import com.mapped.InAppNotificationManager;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.service.buddychallenge.BuddyChallengeManager;
import com.portfolio.platform.service.fcm.FossilFirebaseMessagingService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jr5 implements MembersInjector<FossilFirebaseMessagingService> {
    @DexIgnore
    public static void a(FossilFirebaseMessagingService fossilFirebaseMessagingService, FCMRepository fCMRepository) {
        fossilFirebaseMessagingService.k = fCMRepository;
    }

    @DexIgnore
    public static void b(FossilFirebaseMessagingService fossilFirebaseMessagingService, FlagRepository flagRepository) {
        fossilFirebaseMessagingService.l = flagRepository;
    }

    @DexIgnore
    public static void c(FossilFirebaseMessagingService fossilFirebaseMessagingService, BuddyChallengeManager buddyChallengeManager) {
        fossilFirebaseMessagingService.i = buddyChallengeManager;
    }

    @DexIgnore
    public static void d(FossilFirebaseMessagingService fossilFirebaseMessagingService, InAppNotificationManager inAppNotificationManager) {
        fossilFirebaseMessagingService.h = inAppNotificationManager;
    }

    @DexIgnore
    public static void e(FossilFirebaseMessagingService fossilFirebaseMessagingService, An4 an4) {
        fossilFirebaseMessagingService.j = an4;
    }
}
