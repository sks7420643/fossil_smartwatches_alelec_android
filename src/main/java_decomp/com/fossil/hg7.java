package com.fossil;

import android.content.Context;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hg7 {
    @DexIgnore
    public static void a(Context context) {
        Ig7.D(context, null);
    }

    @DexIgnore
    public static void b(Context context) {
        Ig7.E(context, null);
    }

    @DexIgnore
    public static boolean c(Context context, String str, String str2) {
        return Ig7.F(context, str, str2, null);
    }

    @DexIgnore
    public static void d(Context context, String str, Properties properties) {
        Ig7.I(context, str, properties, null);
    }
}
