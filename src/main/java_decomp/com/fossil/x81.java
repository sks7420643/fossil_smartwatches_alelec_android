package com.fossil;

import android.util.Log;
import com.mapped.Wg6;
import java.io.PrintWriter;
import java.io.StringWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class X81 {
    @DexIgnore
    public static final void a(String str, Throwable th) {
        Wg6.c(str, "tag");
        Wg6.c(th, "throwable");
        if (Q81.c.a() && Q81.c.b() <= 6) {
            StringWriter stringWriter = new StringWriter();
            th.printStackTrace(new PrintWriter(stringWriter));
            Log.println(6, str, stringWriter.toString());
        }
    }
}
