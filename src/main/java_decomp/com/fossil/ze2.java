package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.common.stats.WakeLockEvent;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ze2 implements Parcelable.Creator<WakeLockEvent> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ WakeLockEvent createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        ArrayList<String> arrayList = null;
        String str5 = null;
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 1:
                    i = Ad2.v(parcel, t);
                    break;
                case 2:
                    j3 = Ad2.y(parcel, t);
                    break;
                case 3:
                case 7:
                case 9:
                default:
                    Ad2.B(parcel, t);
                    break;
                case 4:
                    str5 = Ad2.f(parcel, t);
                    break;
                case 5:
                    i3 = Ad2.v(parcel, t);
                    break;
                case 6:
                    arrayList = Ad2.h(parcel, t);
                    break;
                case 8:
                    j2 = Ad2.y(parcel, t);
                    break;
                case 10:
                    str3 = Ad2.f(parcel, t);
                    break;
                case 11:
                    i2 = Ad2.v(parcel, t);
                    break;
                case 12:
                    str4 = Ad2.f(parcel, t);
                    break;
                case 13:
                    str2 = Ad2.f(parcel, t);
                    break;
                case 14:
                    i4 = Ad2.v(parcel, t);
                    break;
                case 15:
                    f = Ad2.r(parcel, t);
                    break;
                case 16:
                    j = Ad2.y(parcel, t);
                    break;
                case 17:
                    str = Ad2.f(parcel, t);
                    break;
                case 18:
                    z = Ad2.m(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new WakeLockEvent(i, j3, i2, str5, i3, arrayList, str4, j2, i4, str3, str2, f, j, str, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ WakeLockEvent[] newArray(int i) {
        return new WakeLockEvent[i];
    }
}
