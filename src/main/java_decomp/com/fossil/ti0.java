package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ti0 implements Wi0 {
    @DexIgnore
    @Override // com.fossil.Wi0
    public void a(Vi0 vi0, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        vi0.c(new Xi0(colorStateList, f));
        View g = vi0.g();
        g.setClipToOutline(true);
        g.setElevation(f2);
        o(vi0, f3);
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void b(Vi0 vi0, float f) {
        p(vi0).h(f);
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public float c(Vi0 vi0) {
        return vi0.g().getElevation();
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public float d(Vi0 vi0) {
        return p(vi0).d();
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void e(Vi0 vi0) {
        o(vi0, g(vi0));
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void f(Vi0 vi0, float f) {
        vi0.g().setElevation(f);
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public float g(Vi0 vi0) {
        return p(vi0).c();
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public ColorStateList h(Vi0 vi0) {
        return p(vi0).b();
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void i(Vi0 vi0) {
        if (!vi0.e()) {
            vi0.a(0, 0, 0, 0);
            return;
        }
        float g = g(vi0);
        float d = d(vi0);
        int ceil = (int) Math.ceil((double) Yi0.c(g, d, vi0.d()));
        int ceil2 = (int) Math.ceil((double) Yi0.d(g, d, vi0.d()));
        vi0.a(ceil, ceil2, ceil, ceil2);
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void j() {
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public float k(Vi0 vi0) {
        return d(vi0) * 2.0f;
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public float l(Vi0 vi0) {
        return d(vi0) * 2.0f;
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void m(Vi0 vi0) {
        o(vi0, g(vi0));
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void n(Vi0 vi0, ColorStateList colorStateList) {
        p(vi0).f(colorStateList);
    }

    @DexIgnore
    @Override // com.fossil.Wi0
    public void o(Vi0 vi0, float f) {
        p(vi0).g(f, vi0.e(), vi0.d());
        i(vi0);
    }

    @DexIgnore
    public final Xi0 p(Vi0 vi0) {
        return (Xi0) vi0.f();
    }
}
