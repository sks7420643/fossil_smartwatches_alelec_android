package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class U21 extends T21<F21> {
    @DexIgnore
    public static /* final */ String j; // = X01.f("NetworkStateTracker");
    @DexIgnore
    public /* final */ ConnectivityManager g; // = ((ConnectivityManager) this.b.getSystemService("connectivity"));
    @DexIgnore
    public Bi h;
    @DexIgnore
    public Ai i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends BroadcastReceiver {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null && intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                X01.c().a(U21.j, "Network broadcast received", new Throwable[0]);
                U21 u21 = U21.this;
                u21.d(u21.g());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends ConnectivityManager.NetworkCallback {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
            X01.c().a(U21.j, String.format("Network capabilities changed: %s", networkCapabilities), new Throwable[0]);
            U21 u21 = U21.this;
            u21.d(u21.g());
        }

        @DexIgnore
        public void onLost(Network network) {
            X01.c().a(U21.j, "Network connection lost", new Throwable[0]);
            U21 u21 = U21.this;
            u21.d(u21.g());
        }
    }

    @DexIgnore
    public U21(Context context, K41 k41) {
        super(context, k41);
        if (j()) {
            this.h = new Bi();
        } else {
            this.i = new Ai();
        }
    }

    @DexIgnore
    public static boolean j() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.T21
    public /* bridge */ /* synthetic */ F21 b() {
        return h();
    }

    @DexIgnore
    @Override // com.fossil.T21
    public void e() {
        if (j()) {
            try {
                X01.c().a(j, "Registering network callback", new Throwable[0]);
                this.g.registerDefaultNetworkCallback(this.h);
            } catch (IllegalArgumentException | SecurityException e) {
                X01.c().b(j, "Received exception while registering network callback", e);
            }
        } else {
            X01.c().a(j, "Registering broadcast receiver", new Throwable[0]);
            this.b.registerReceiver(this.i, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    @DexIgnore
    @Override // com.fossil.T21
    public void f() {
        if (j()) {
            try {
                X01.c().a(j, "Unregistering network callback", new Throwable[0]);
                this.g.unregisterNetworkCallback(this.h);
            } catch (IllegalArgumentException | SecurityException e) {
                X01.c().b(j, "Received exception while unregistering network callback", e);
            }
        } else {
            X01.c().a(j, "Unregistering broadcast receiver", new Throwable[0]);
            this.b.unregisterReceiver(this.i);
        }
    }

    @DexIgnore
    public F21 g() {
        boolean z = true;
        NetworkInfo activeNetworkInfo = this.g.getActiveNetworkInfo();
        boolean z2 = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        boolean i2 = i();
        boolean a2 = Km0.a(this.g);
        if (activeNetworkInfo == null || activeNetworkInfo.isRoaming()) {
            z = false;
        }
        return new F21(z2, i2, a2, z);
    }

    @DexIgnore
    public F21 h() {
        return g();
    }

    @DexIgnore
    public final boolean i() {
        NetworkCapabilities networkCapabilities;
        return Build.VERSION.SDK_INT >= 23 && (networkCapabilities = this.g.getNetworkCapabilities(this.g.getActiveNetwork())) != null && networkCapabilities.hasCapability(16);
    }
}
