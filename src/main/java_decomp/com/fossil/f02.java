package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class F02 implements Executor {
    @DexIgnore
    public /* final */ Executor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Runnable {
        @DexIgnore
        public /* final */ Runnable b;

        @DexIgnore
        public Ai(Runnable runnable) {
            this.b = runnable;
        }

        @DexIgnore
        public void run() {
            try {
                this.b.run();
            } catch (Exception e) {
                C12.c("Executor", "Background execution failure.", e);
            }
        }
    }

    @DexIgnore
    public F02(Executor executor) {
        this.b = executor;
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        this.b.execute(new Ai(runnable));
    }
}
