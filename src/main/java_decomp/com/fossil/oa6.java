package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oa6 implements Factory<CommuteTimeWatchAppSettingsViewModel> {
    @DexIgnore
    public /* final */ Provider<An4> a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;

    @DexIgnore
    public Oa6(Provider<An4> provider, Provider<UserRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Oa6 a(Provider<An4> provider, Provider<UserRepository> provider2) {
        return new Oa6(provider, provider2);
    }

    @DexIgnore
    public static CommuteTimeWatchAppSettingsViewModel c(An4 an4, UserRepository userRepository) {
        return new CommuteTimeWatchAppSettingsViewModel(an4, userRepository);
    }

    @DexIgnore
    public CommuteTimeWatchAppSettingsViewModel b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
