package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rd4 extends RuntimeException {
    @DexIgnore
    public Rd4(String str) {
        super(str);
    }

    @DexIgnore
    public Rd4(String str, Exception exc) {
        super(str, exc);
    }
}
