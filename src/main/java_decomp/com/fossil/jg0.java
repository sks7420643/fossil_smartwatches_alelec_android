package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Jg0 {

    @DexIgnore
    public interface Ai {
        @DexIgnore
        boolean e();

        @DexIgnore
        void f(Eg0 eg0, int i);

        @DexIgnore
        Eg0 getItemData();
    }

    @DexIgnore
    void b(Cg0 cg0);
}
