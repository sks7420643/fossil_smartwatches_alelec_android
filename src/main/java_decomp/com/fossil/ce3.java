package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ce3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Ce3> CREATOR; // = new Ye3();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Zd3 c;
    @DexIgnore
    public /* final */ Float d;

    @DexIgnore
    public Ce3(int i) {
        this(i, (Zd3) null, (Float) null);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Ce3(int i, IBinder iBinder, Float f) {
        this(i, iBinder == null ? null : new Zd3(Rg2.Ai.e(iBinder)), f);
    }

    @DexIgnore
    public Ce3(int i, Zd3 zd3, Float f) {
        Rc2.b(i != 3 || (zd3 != null && (f != null && (f.floatValue() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : (f.floatValue() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 0 : -1)) > 0)), String.format("Invalid Cap: type=%s bitmapDescriptor=%s bitmapRefWidth=%s", Integer.valueOf(i), zd3, f));
        this.b = i;
        this.c = zd3;
        this.d = f;
    }

    @DexIgnore
    public Ce3(Zd3 zd3, float f) {
        this(3, zd3, Float.valueOf(f));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Ce3)) {
            return false;
        }
        Ce3 ce3 = (Ce3) obj;
        return this.b == ce3.b && Pc2.a(this.c, ce3.c) && Pc2.a(this.d, ce3.d);
    }

    @DexIgnore
    public int hashCode() {
        return Pc2.b(Integer.valueOf(this.b), this.c, this.d);
    }

    @DexIgnore
    public String toString() {
        int i = this.b;
        StringBuilder sb = new StringBuilder(23);
        sb.append("[Cap: type=");
        sb.append(i);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.n(parcel, 2, this.b);
        Zd3 zd3 = this.c;
        Bd2.m(parcel, 3, zd3 == null ? null : zd3.a().asBinder(), false);
        Bd2.l(parcel, 4, this.d, false);
        Bd2.b(parcel, a2);
    }
}
