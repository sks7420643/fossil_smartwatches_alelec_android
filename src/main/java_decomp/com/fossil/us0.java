package com.fossil;

import com.mapped.Il6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Us0 {
    @DexIgnore
    public static final Il6 a(Ts0 ts0) {
        Wg6.c(ts0, "$this$viewModelScope");
        Il6 il6 = (Il6) ts0.getTag("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY");
        if (il6 != null) {
            return il6;
        }
        Object tagIfAbsent = ts0.setTagIfAbsent("androidx.lifecycle.ViewModelCoroutineScope.JOB_KEY", new Lr0(Ux7.b(null, 1, null).plus(Bw7.c().S())));
        Wg6.b(tagIfAbsent, "setTagIfAbsent(JOB_KEY,\n\u2026patchers.Main.immediate))");
        return (Il6) tagIfAbsent;
    }
}
