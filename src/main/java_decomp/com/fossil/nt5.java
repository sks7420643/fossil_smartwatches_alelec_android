package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nt5 implements Factory<SetReplyMessageMappingUseCase> {
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> a;

    @DexIgnore
    public Nt5(Provider<QuickResponseRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Nt5 a(Provider<QuickResponseRepository> provider) {
        return new Nt5(provider);
    }

    @DexIgnore
    public static SetReplyMessageMappingUseCase c(QuickResponseRepository quickResponseRepository) {
        return new SetReplyMessageMappingUseCase(quickResponseRepository);
    }

    @DexIgnore
    public SetReplyMessageMappingUseCase b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
