package com.fossil;

import com.fossil.A34;
import com.fossil.Y24;
import java.io.Serializable;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Proxy;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.security.AccessControlException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F64 {
    @DexIgnore
    public static /* final */ B14<Type, String> a; // = new Ai();
    @DexIgnore
    public static /* final */ D14 b; // = D14.i(", ").k("null");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements B14<Type, String> {
        @DexIgnore
        public String a(Type type) {
            return Ei.CURRENT.typeName(type);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.B14
        public /* bridge */ /* synthetic */ String apply(Type type) {
            return a(type);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends E64 {
        @DexIgnore
        public /* final */ /* synthetic */ AtomicReference b;

        @DexIgnore
        public Bi(AtomicReference atomicReference) {
            this.b = atomicReference;
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void b(Class<?> cls) {
            this.b.set(cls.getComponentType());
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void c(GenericArrayType genericArrayType) {
            this.b.set(genericArrayType.getGenericComponentType());
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void e(TypeVariable<?> typeVariable) {
            this.b.set(F64.q(typeVariable.getBounds()));
        }

        @DexIgnore
        @Override // com.fossil.E64
        public void f(WildcardType wildcardType) {
            this.b.set(F64.q(wildcardType.getUpperBounds()));
        }
    }

    @DexIgnore
    public enum Ci {
        OWNED_BY_ENCLOSING_CLASS {
            @DexIgnore
            @Override // com.fossil.F64.Ci
            public Class<?> getOwnerType(Class<?> cls) {
                return cls.getEnclosingClass();
            }
        },
        LOCAL_CLASS_HAS_NO_OWNER {
            @DexIgnore
            @Override // com.fossil.F64.Ci
            public Class<?> getOwnerType(Class<?> cls) {
                if (cls.isLocalClass()) {
                    return null;
                }
                return cls.getEnclosingClass();
            }
        };
        
        @DexIgnore
        public static /* final */ Ci JVM_BEHAVIOR; // = a();

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Bii<T> {
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Dii extends Bii<String> {
        }

        @DexIgnore
        public /* synthetic */ Ci(Ai ai) {
            this();
        }

        @DexIgnore
        public static Ci a() {
            ParameterizedType parameterizedType = (ParameterizedType) Dii.class.getGenericSuperclass();
            Ci[] values = values();
            for (Ci ci : values) {
                if (ci.getOwnerType(Bii.class) == parameterizedType.getOwnerType()) {
                    return ci;
                }
            }
            throw new AssertionError();
        }

        @DexIgnore
        public abstract Class<?> getOwnerType(Class<?> cls);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements GenericArrayType, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Type componentType;

        @DexIgnore
        public Di(Type type) {
            this.componentType = Ei.CURRENT.usedInGenericType(type);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj instanceof GenericArrayType) {
                return F14.a(getGenericComponentType(), ((GenericArrayType) obj).getGenericComponentType());
            }
            return false;
        }

        @DexIgnore
        public Type getGenericComponentType() {
            return this.componentType;
        }

        @DexIgnore
        public int hashCode() {
            return this.componentType.hashCode();
        }

        @DexIgnore
        public String toString() {
            return F64.t(this.componentType) + "[]";
        }
    }

    @DexIgnore
    public enum Ei {
        JAVA6 {
            @DexIgnore
            @Override // com.fossil.F64.Ei
            public GenericArrayType newArrayType(Type type) {
                return new Di(type);
            }

            @DexIgnore
            @Override // com.fossil.F64.Ei
            public Type usedInGenericType(Type type) {
                I14.l(type);
                if (!(type instanceof Class)) {
                    return type;
                }
                Class cls = (Class) type;
                return cls.isArray() ? new Di(cls.getComponentType()) : type;
            }
        },
        JAVA7 {
            @DexIgnore
            @Override // com.fossil.F64.Ei
            public Type newArrayType(Type type) {
                return type instanceof Class ? F64.i((Class) type) : new Di(type);
            }

            @DexIgnore
            @Override // com.fossil.F64.Ei
            public Type usedInGenericType(Type type) {
                I14.l(type);
                return type;
            }
        },
        JAVA8 {
            @DexIgnore
            @Override // com.fossil.F64.Ei
            public Type newArrayType(Type type) {
                return Ei.JAVA7.newArrayType(type);
            }

            @DexIgnore
            @Override // com.fossil.F64.Ei
            public String typeName(Type type) {
                try {
                    return (String) Type.class.getMethod("getTypeName", new Class[0]).invoke(type, new Object[0]);
                } catch (NoSuchMethodException e) {
                    throw new AssertionError("Type.getTypeName should be available in Java 8");
                } catch (InvocationTargetException e2) {
                    throw new RuntimeException(e2);
                } catch (IllegalAccessException e3) {
                    throw new RuntimeException(e3);
                }
            }

            @DexIgnore
            @Override // com.fossil.F64.Ei
            public Type usedInGenericType(Type type) {
                return Ei.JAVA7.usedInGenericType(type);
            }
        };
        
        @DexIgnore
        public static /* final */ Ei CURRENT;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Dii extends A64<int[]> {
        }

        /*
        static {
            if (AnnotatedElement.class.isAssignableFrom(TypeVariable.class)) {
                CURRENT = JAVA8;
            } else if (new Dii().capture() instanceof Class) {
                CURRENT = JAVA7;
            } else {
                CURRENT = JAVA6;
            }
        }
        */

        @DexIgnore
        public /* synthetic */ Ei(Ai ai) {
            this();
        }

        @DexIgnore
        public abstract Type newArrayType(Type type);

        @DexIgnore
        public String typeName(Type type) {
            return F64.t(type);
        }

        @DexIgnore
        public final Y24<Type> usedInGenericType(Type[] typeArr) {
            Y24.Bi builder = Y24.builder();
            for (Type type : typeArr) {
                builder.g(usedInGenericType(type));
            }
            return builder.i();
        }

        @DexIgnore
        public abstract Type usedInGenericType(Type type);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<X> {
        @DexIgnore
        public static /* final */ boolean a; // = (!Fi.class.getTypeParameters()[0].equals(F64.l(Fi.class, "X", new Type[0])));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements ParameterizedType, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Y24<Type> argumentsList;
        @DexIgnore
        public /* final */ Type ownerType;
        @DexIgnore
        public /* final */ Class<?> rawType;

        @DexIgnore
        public Gi(Type type, Class<?> cls, Type[] typeArr) {
            I14.l(cls);
            I14.d(typeArr.length == cls.getTypeParameters().length);
            F64.g(typeArr, "type parameter");
            this.ownerType = type;
            this.rawType = cls;
            this.argumentsList = Ei.CURRENT.usedInGenericType(typeArr);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType parameterizedType = (ParameterizedType) obj;
            return getRawType().equals(parameterizedType.getRawType()) && F14.a(getOwnerType(), parameterizedType.getOwnerType()) && Arrays.equals(getActualTypeArguments(), parameterizedType.getActualTypeArguments());
        }

        @DexIgnore
        public Type[] getActualTypeArguments() {
            return F64.s(this.argumentsList);
        }

        @DexIgnore
        public Type getOwnerType() {
            return this.ownerType;
        }

        @DexIgnore
        public Type getRawType() {
            return this.rawType;
        }

        @DexIgnore
        public int hashCode() {
            Type type = this.ownerType;
            return ((type == null ? 0 : type.hashCode()) ^ this.argumentsList.hashCode()) ^ this.rawType.hashCode();
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder();
            Type type = this.ownerType;
            if (type != null) {
                sb.append(Ei.CURRENT.typeName(type));
                sb.append('.');
            }
            sb.append(this.rawType.getName());
            sb.append('<');
            sb.append(F64.b.e(O34.k(this.argumentsList, F64.a)));
            sb.append('>');
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<D extends GenericDeclaration> {
        @DexIgnore
        public /* final */ D a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ Y24<Type> c;

        @DexIgnore
        public Hi(D d, String str, Type[] typeArr) {
            F64.g(typeArr, "bound for type variable");
            I14.l(d);
            this.a = d;
            I14.l(str);
            this.b = str;
            this.c = Y24.copyOf(typeArr);
        }

        @DexIgnore
        public D a() {
            return this.a;
        }

        @DexIgnore
        public String b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            boolean z = true;
            if (Fi.a) {
                if (obj == null || !Proxy.isProxyClass(obj.getClass()) || !(Proxy.getInvocationHandler(obj) instanceof Ii)) {
                    return false;
                }
                Hi hi = ((Ii) Proxy.getInvocationHandler(obj)).a;
                return this.b.equals(hi.b()) && this.a.equals(hi.a()) && this.c.equals(hi.c);
            } else if (!(obj instanceof TypeVariable)) {
                return false;
            } else {
                TypeVariable typeVariable = (TypeVariable) obj;
                if (!this.b.equals(typeVariable.getName()) || !this.a.equals(typeVariable.getGenericDeclaration())) {
                    z = false;
                }
                return z;
            }
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode() ^ this.b.hashCode();
        }

        @DexIgnore
        public String toString() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements InvocationHandler {
        @DexIgnore
        public static /* final */ A34<String, Method> b;
        @DexIgnore
        public /* final */ Hi<?> a;

        /*
        static {
            A34.Bi builder = A34.builder();
            Method[] methods = Hi.class.getMethods();
            for (Method method : methods) {
                if (method.getDeclaringClass().equals(Hi.class)) {
                    try {
                        method.setAccessible(true);
                    } catch (AccessControlException e) {
                    }
                    builder.c(method.getName(), method);
                }
            }
            b = builder.a();
        }
        */

        @DexIgnore
        public Ii(Hi<?> hi) {
            this.a = hi;
        }

        @DexIgnore
        @Override // java.lang.reflect.InvocationHandler
        public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
            String name = method.getName();
            Method method2 = b.get(name);
            if (method2 != null) {
                try {
                    return method2.invoke(this.a, objArr);
                } catch (InvocationTargetException e) {
                    throw e.getCause();
                }
            } else {
                throw new UnsupportedOperationException(name);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements WildcardType, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Y24<Type> lowerBounds;
        @DexIgnore
        public /* final */ Y24<Type> upperBounds;

        @DexIgnore
        public Ji(Type[] typeArr, Type[] typeArr2) {
            F64.g(typeArr, "lower bound for wildcard");
            F64.g(typeArr2, "upper bound for wildcard");
            this.lowerBounds = Ei.CURRENT.usedInGenericType(typeArr);
            this.upperBounds = Ei.CURRENT.usedInGenericType(typeArr2);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof WildcardType)) {
                return false;
            }
            WildcardType wildcardType = (WildcardType) obj;
            return this.lowerBounds.equals(Arrays.asList(wildcardType.getLowerBounds())) && this.upperBounds.equals(Arrays.asList(wildcardType.getUpperBounds()));
        }

        @DexIgnore
        public Type[] getLowerBounds() {
            return F64.s(this.lowerBounds);
        }

        @DexIgnore
        public Type[] getUpperBounds() {
            return F64.s(this.upperBounds);
        }

        @DexIgnore
        public int hashCode() {
            return this.lowerBounds.hashCode() ^ this.upperBounds.hashCode();
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder("?");
            Iterator it = this.lowerBounds.iterator();
            while (it.hasNext()) {
                sb.append(" super ");
                sb.append(Ei.CURRENT.typeName((Type) it.next()));
            }
            for (Type type : F64.h(this.upperBounds)) {
                sb.append(" extends ");
                sb.append(Ei.CURRENT.typeName(type));
            }
            return sb.toString();
        }
    }

    @DexIgnore
    public static void g(Type[] typeArr, String str) {
        for (Type type : typeArr) {
            if (type instanceof Class) {
                Class cls = (Class) type;
                I14.i(!cls.isPrimitive(), "Primitive type '%s' used as %s", cls, str);
            }
        }
    }

    @DexIgnore
    public static Iterable<Type> h(Iterable<Type> iterable) {
        return O34.d(iterable, K14.d(K14.a(Object.class)));
    }

    @DexIgnore
    public static Class<?> i(Class<?> cls) {
        return Array.newInstance(cls, 0).getClass();
    }

    @DexIgnore
    public static Type j(Type type) {
        I14.l(type);
        AtomicReference atomicReference = new AtomicReference();
        new Bi(atomicReference).a(type);
        return (Type) atomicReference.get();
    }

    @DexIgnore
    public static Type k(Type type) {
        boolean z = true;
        if (!(type instanceof WildcardType)) {
            return Ei.CURRENT.newArrayType(type);
        }
        WildcardType wildcardType = (WildcardType) type;
        Type[] lowerBounds = wildcardType.getLowerBounds();
        I14.e(lowerBounds.length <= 1, "Wildcard cannot have more than one lower bounds.");
        if (lowerBounds.length == 1) {
            return r(k(lowerBounds[0]));
        }
        Type[] upperBounds = wildcardType.getUpperBounds();
        if (upperBounds.length != 1) {
            z = false;
        }
        I14.e(z, "Wildcard should have only one upper bound.");
        return p(k(upperBounds[0]));
    }

    @DexIgnore
    public static <D extends GenericDeclaration> TypeVariable<D> l(D d, String str, Type... typeArr) {
        if (typeArr.length == 0) {
            typeArr = new Type[]{Object.class};
        }
        return o(d, str, typeArr);
    }

    @DexIgnore
    public static ParameterizedType m(Class<?> cls, Type... typeArr) {
        return new Gi(Ci.JVM_BEHAVIOR.getOwnerType(cls), cls, typeArr);
    }

    @DexIgnore
    public static ParameterizedType n(Type type, Class<?> cls, Type... typeArr) {
        if (type == null) {
            return m(cls, typeArr);
        }
        I14.l(typeArr);
        I14.h(cls.getEnclosingClass() != null, "Owner type for unenclosed %s", cls);
        return new Gi(type, cls, typeArr);
    }

    @DexIgnore
    public static <D extends GenericDeclaration> TypeVariable<D> o(D d, String str, Type[] typeArr) {
        return (TypeVariable) Z54.a(TypeVariable.class, new Ii(new Hi(d, str, typeArr)));
    }

    @DexIgnore
    public static WildcardType p(Type type) {
        return new Ji(new Type[0], new Type[]{type});
    }

    @DexIgnore
    public static Type q(Type[] typeArr) {
        for (Type type : typeArr) {
            Type j = j(type);
            if (j != null) {
                if (j instanceof Class) {
                    Class cls = (Class) j;
                    if (cls.isPrimitive()) {
                        return cls;
                    }
                }
                return p(j);
            }
        }
        return null;
    }

    @DexIgnore
    public static WildcardType r(Type type) {
        return new Ji(new Type[]{type}, new Type[]{Object.class});
    }

    @DexIgnore
    public static Type[] s(Collection<Type> collection) {
        return (Type[]) collection.toArray(new Type[collection.size()]);
    }

    @DexIgnore
    public static String t(Type type) {
        return type instanceof Class ? ((Class) type).getName() : type.toString();
    }
}
