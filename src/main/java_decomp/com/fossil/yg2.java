package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.fossil.Qg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yg2 implements Qg2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ FrameLayout a;
    @DexIgnore
    public /* final */ /* synthetic */ LayoutInflater b;
    @DexIgnore
    public /* final */ /* synthetic */ ViewGroup c;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle d;
    @DexIgnore
    public /* final */ /* synthetic */ Qg2 e;

    @DexIgnore
    public Yg2(Qg2 qg2, FrameLayout frameLayout, LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.e = qg2;
        this.a = frameLayout;
        this.b = layoutInflater;
        this.c = viewGroup;
        this.d = bundle;
    }

    @DexIgnore
    @Override // com.fossil.Qg2.Ai
    public final void a(Sg2 sg2) {
        this.a.removeAllViews();
        this.a.addView(this.e.a.q(this.b, this.c, this.d));
    }

    @DexIgnore
    @Override // com.fossil.Qg2.Ai
    public final int getState() {
        return 2;
    }
}
