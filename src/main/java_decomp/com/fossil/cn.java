package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Tc0;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cn extends Ro {
    @DexIgnore
    public /* final */ Tc0 S;
    @DexIgnore
    public /* final */ Ob T;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Cn(K5 k5, I60 i60, Tc0 tc0, Ob ob, String str, int i) {
        super(k5, i60, Yp.V, true, Ke.b.b(k5.x, ob), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 16) != 0 ? E.a("UUID.randomUUID().toString()") : str, true, 32);
        boolean z = tc0 instanceof Zt1;
        this.S = tc0;
        this.T = ob;
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro, com.fossil.Mj
    public JSONObject C() {
        return G80.k(super.C(), Jd0.Z2, this.S.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Ro
    public byte[] M() {
        Tc0 tc0 = this.S;
        short b = Ke.b.b(this.w.x, this.T);
        Ry1 ry1 = this.x.a().h().get(Short.valueOf(this.T.b));
        if (ry1 == null) {
            ry1 = Hd0.y.d();
        }
        return tc0.a(b, ry1);
    }
}
