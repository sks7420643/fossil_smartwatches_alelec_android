package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ff6 implements Factory<ActivityOverviewDayPresenter> {
    @DexIgnore
    public static ActivityOverviewDayPresenter a(Bf6 bf6, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, WorkoutSessionRepository workoutSessionRepository, PortfolioApp portfolioApp) {
        return new ActivityOverviewDayPresenter(bf6, summariesRepository, activitiesRepository, workoutSessionRepository, portfolioApp);
    }
}
