package com.fossil;

import android.text.TextUtils;
import android.util.Log;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hi4 {
    @DexIgnore
    public static /* final */ Pattern d; // = Pattern.compile("[a-zA-Z0-9-_.~%]{1,900}");
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public Hi4(String str, String str2) {
        this.a = d(str2, str);
        this.b = str;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 1 + String.valueOf(str2).length());
        sb.append(str);
        sb.append("!");
        sb.append(str2);
        this.c = sb.toString();
    }

    @DexIgnore
    public static Hi4 a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String[] split = str.split("!", -1);
        if (split.length == 2) {
            return new Hi4(split[0], split[1]);
        }
        return null;
    }

    @DexIgnore
    public static String d(String str, String str2) {
        if (str != null && str.startsWith("/topics/")) {
            Log.w("FirebaseMessaging", String.format("Format /topics/topic-name is deprecated. Only 'topic-name' should be used in %s.", str2));
            str = str.substring(8);
        }
        if (str != null && d.matcher(str).matches()) {
            return str;
        }
        throw new IllegalArgumentException(String.format("Invalid topic name: %s does not match the allowed format %s.", str, "[a-zA-Z0-9-_.~%]{1,900}"));
    }

    @DexIgnore
    public final String b() {
        return this.b;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final String e() {
        return this.c;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof Hi4)) {
            return false;
        }
        Hi4 hi4 = (Hi4) obj;
        return this.a.equals(hi4.a) && this.b.equals(hi4.b);
    }

    @DexIgnore
    public final int hashCode() {
        return Pc2.b(this.b, this.a);
    }
}
