package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bw5;
import com.fossil.Cr4;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.mapped.Qg6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity;
import com.portfolio.platform.uirenew.mappicker.MapPickerActivity;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N76 extends BaseFragment implements S76, Cr4.Ai {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public G37<N35> g;
    @DexIgnore
    public R76 h;
    @DexIgnore
    public Bw5 i;
    @DexIgnore
    public Cr4 j;
    @DexIgnore
    public String k;
    @DexIgnore
    public HashMap l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final N76 a() {
            return new N76();
        }

        @DexIgnore
        public final String b() {
            return N76.m;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Bw5.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ N76 a;
        @DexIgnore
        public /* final */ /* synthetic */ N35 b;

        @DexIgnore
        public Bi(N76 n76, N35 n35) {
            this.a = n76;
            this.b = n35;
        }

        @DexIgnore
        @Override // com.fossil.Bw5.Bi
        public void a(String str) {
            Wg6.c(str, "address");
            this.b.q.setText((CharSequence) str, false);
            this.a.P6(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ N76 b;

        @DexIgnore
        public Ci(N76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            N76.L6(this.b).p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ N76 b;

        @DexIgnore
        public Di(N76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            N76.L6(this.b).q();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ N76 b;

        @DexIgnore
        public Ei(N76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            N76.L6(this.b).s();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ N76 b;

        @DexIgnore
        public Fi(N76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            N76.L6(this.b).t();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ N35 c;

        @DexIgnore
        public Gi(View view, N35 n35) {
            this.b = view;
            this.c = n35;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.b.getWindowVisibleDisplayFrame(rect);
            int height = this.b.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.c.D.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                N35 n35 = this.c;
                Wg6.b(n35, "binding");
                n35.n().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = N76.s.b();
                local.d(b2, "observeKeyboard - isOpen=true - dropDownHeight=" + i);
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = this.c.q;
                Wg6.b(flexibleAutoCompleteTextView, "binding.autocompletePlaces");
                flexibleAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ N76 b;
        @DexIgnore
        public /* final */ /* synthetic */ N35 c;

        @DexIgnore
        public Hi(N76 n76, N35 n35) {
            this.b = n76;
            this.c = n35;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction f;
            Cr4 cr4 = this.b.j;
            if (cr4 != null && (f = cr4.f(i)) != null) {
                SpannableString fullText = f.getFullText(null);
                Wg6.b(fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = N76.s.b();
                local.i(b2, "Autocomplete item selected: " + ((Object) fullText));
                String spannableString = fullText.toString();
                Wg6.b(spannableString, "primaryText.toString()");
                if (!TextUtils.isEmpty(spannableString)) {
                    this.b.P6(spannableString);
                    this.c.q.setText((CharSequence) spannableString, false);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ N76 b;
        @DexIgnore
        public /* final */ /* synthetic */ N35 c;

        @DexIgnore
        public Ii(N76 n76, N35 n35) {
            this.b = n76;
            this.c = n35;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            RTLImageView rTLImageView = this.c.t;
            if (!Wg6.a(this.b.N6(), String.valueOf(editable))) {
                this.b.P6(null);
            }
            RTLImageView rTLImageView2 = this.c.t;
            Wg6.b(rTLImageView2, "binding.closeIv");
            rTLImageView2.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ N76 b;

        @DexIgnore
        public Ji(N76 n76, N35 n35) {
            this.b = n76;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            Wg6.b(keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.b.F6();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ N76 b;

        @DexIgnore
        public Ki(N76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            N76.L6(this.b).v("travel");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ N76 b;

        @DexIgnore
        public Li(N76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            String str;
            MapPickerActivity.a aVar = MapPickerActivity.A;
            N76 n76 = this.b;
            CommuteTimeSetting o = N76.L6(n76).o();
            if (o == null || (str = o.getAddress()) == null) {
                str = "";
            }
            aVar.a(n76, 0.0d, 0.0d, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ N76 b;

        @DexIgnore
        public Mi(N76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ N76 b;
        @DexIgnore
        public /* final */ /* synthetic */ N35 c;

        @DexIgnore
        public Ni(N76 n76, N35 n35) {
            this.b = n76;
            this.c = n35;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.c.q.setText((CharSequence) "", false);
            N76.L6(this.b).n();
            this.b.O6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ N76 b;

        @DexIgnore
        public Oi(N76 n76) {
            this.b = n76;
        }

        @DexIgnore
        public final void onClick(View view) {
            N76.L6(this.b).v("eta");
        }
    }

    /*
    static {
        String simpleName = N76.class.getSimpleName();
        Wg6.b(simpleName, "CommuteTimeSettingsFragment::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ R76 L6(N76 n76) {
        R76 r76 = n76.h;
        if (r76 != null) {
            return r76;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Cr4.Ai
    public void D1(boolean z) {
        G37<N35> g37 = this.g;
        if (g37 != null) {
            N35 a2 = g37.a();
            if (a2 != null) {
                if (z) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    Wg6.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(flexibleAutoCompleteTextView.getText())) {
                        this.k = null;
                        S6();
                        return;
                    }
                }
                O6();
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001c, code lost:
        if (r1 != null) goto L_0x001e;
     */
    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean F6() {
        /*
            r7 = this;
            r3 = 0
            com.fossil.G37<com.fossil.N35> r0 = r7.g
            if (r0 == 0) goto L_0x007c
            java.lang.Object r0 = r0.a()
            r4 = r0
            com.fossil.N35 r4 = (com.fossil.N35) r4
            if (r4 == 0) goto L_0x005e
            java.lang.String r0 = r7.k
            if (r0 == 0) goto L_0x0068
            if (r0 == 0) goto L_0x0060
            java.lang.CharSequence r0 = com.fossil.Wt7.u0(r0)
            java.lang.String r1 = r0.toString()
            if (r1 == 0) goto L_0x0068
        L_0x001e:
            com.portfolio.platform.view.RTLImageView r0 = r4.z
            java.lang.String r2 = "it.ivArrivalTime"
            com.mapped.Wg6.b(r0, r2)
            float r0 = r0.getAlpha()
            r2 = 1065353216(0x3f800000, float:1.0)
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 != 0) goto L_0x006b
            java.lang.String r5 = "eta"
        L_0x0031:
            com.fossil.R76 r0 = r7.h
            if (r0 == 0) goto L_0x0076
            com.portfolio.platform.view.FlexibleAutoCompleteTextView r2 = r4.q
            java.lang.String r3 = "it.autocompletePlaces"
            com.mapped.Wg6.b(r2, r3)
            android.text.Editable r2 = r2.getText()
            java.lang.String r2 = r2.toString()
            if (r2 == 0) goto L_0x006e
            java.lang.CharSequence r2 = com.fossil.Wt7.u0(r2)
            java.lang.String r2 = r2.toString()
            com.fossil.Mh5 r3 = com.fossil.Mh5.CAR
            com.portfolio.platform.view.FlexibleSwitchCompat r4 = r4.J
            java.lang.String r6 = "it.scAvoidTolls"
            com.mapped.Wg6.b(r4, r6)
            boolean r4 = r4.isChecked()
            r0.u(r1, r2, r3, r4, r5)
        L_0x005e:
            r0 = 1
            return r0
        L_0x0060:
            com.mapped.Rc6 r0 = new com.mapped.Rc6
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.CharSequence"
            r0.<init>(r1)
            throw r0
        L_0x0068:
            java.lang.String r1 = ""
            goto L_0x001e
        L_0x006b:
            java.lang.String r5 = "travel"
            goto L_0x0031
        L_0x006e:
            com.mapped.Rc6 r0 = new com.mapped.Rc6
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.CharSequence"
            r0.<init>(r1)
            throw r0
        L_0x0076:
            java.lang.String r0 = "mPresenter"
            com.mapped.Wg6.n(r0)
            throw r3
        L_0x007c:
            java.lang.String r0 = "mBinding"
            com.mapped.Wg6.n(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.N76.F6():boolean");
    }

    @DexIgnore
    @Override // com.fossil.S76
    public void H(PlacesClient placesClient) {
        if (isActive()) {
            Context requireContext = requireContext();
            Wg6.b(requireContext, "requireContext()");
            Cr4 cr4 = new Cr4(requireContext, placesClient);
            this.j = cr4;
            if (cr4 != null) {
                cr4.h(this);
            }
            G37<N35> g37 = this.g;
            if (g37 != null) {
                N35 a2 = g37.a();
                if (a2 != null) {
                    a2.q.setAdapter(this.j);
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    Wg6.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    Wg6.b(text, "it.autocompletePlaces.text");
                    CharSequence u0 = Wt7.u0(text);
                    if (u0.length() > 0) {
                        a2.q.setText(u0, false);
                        a2.q.setSelection(u0.length());
                        return;
                    }
                    O6();
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(R76 r76) {
        Q6(r76);
    }

    @DexIgnore
    public final String N6() {
        return this.k;
    }

    @DexIgnore
    public void O6() {
        G37<N35> g37 = this.g;
        if (g37 != null) {
            N35 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                Wg6.b(flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                LinearLayout linearLayout = a2.G;
                Wg6.b(linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(0);
                Ee5 ee5 = a2.x;
                Wg6.b(ee5, "it.icHome");
                View n = ee5.n();
                Wg6.b(n, "it.icHome.root");
                n.setVisibility(0);
                Ee5 ee52 = a2.y;
                Wg6.b(ee52, "it.icWork");
                View n2 = ee52.n();
                Wg6.b(n2, "it.icWork.root");
                n2.setVisibility(0);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void P6(String str) {
        this.k = str;
    }

    @DexIgnore
    @Override // com.fossil.S76
    public void Q4(String str) {
        Ee5 ee5;
        Wg6.c(str, "userWorkDefaultAddress");
        G37<N35> g37 = this.g;
        if (g37 != null) {
            N35 a2 = g37.a();
            if (a2 != null && (ee5 = a2.y) != null) {
                FlexibleTextView flexibleTextView = ee5.r;
                Wg6.b(flexibleTextView, "workButton.ftvContent");
                flexibleTextView.setText(str);
                int a3 = (int) P47.a(12, requireContext());
                ee5.t.setPadding(a3, a3, a3, a3);
                if (!TextUtils.isEmpty(str)) {
                    ImageButton imageButton = ee5.t;
                    Wg6.b(imageButton, "workButton.ivEditButton");
                    imageButton.setImageTintList(ColorStateList.valueOf(W6.d(PortfolioApp.get.instance(), 2131099968)));
                    ee5.t.setImageResource(2131231049);
                    return;
                }
                ImageButton imageButton2 = ee5.t;
                Wg6.b(imageButton2, "workButton.ivEditButton");
                imageButton2.setImageTintList(ColorStateList.valueOf(W6.d(PortfolioApp.get.instance(), 2131099809)));
                ee5.t.setImageResource(2131231049);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void Q6(R76 r76) {
        Wg6.c(r76, "presenter");
        this.h = r76;
    }

    @DexIgnore
    public void R6(String str) {
        Wg6.c(str, "address");
        G37<N35> g37 = this.g;
        if (g37 != null) {
            N35 a2 = g37.a();
            if (a2 != null) {
                if (str.length() > 0) {
                    a2.q.setText((CharSequence) str, false);
                    this.k = str;
                    return;
                }
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public void S6() {
        G37<N35> g37 = this.g;
        if (g37 != null) {
            N35 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                Wg6.b(flexibleTextView, "it.ftvAddressError");
                PortfolioApp instance = PortfolioApp.get.instance();
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                Wg6.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(instance.getString(2131886360, new Object[]{flexibleAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.u;
                Wg6.b(flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                LinearLayout linearLayout = a2.G;
                Wg6.b(linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(8);
                Ee5 ee5 = a2.x;
                Wg6.b(ee5, "it.icHome");
                View n = ee5.n();
                Wg6.b(n, "it.icHome.root");
                n.setVisibility(8);
                Ee5 ee52 = a2.y;
                Wg6.b(ee52, "it.icWork");
                View n2 = ee52.n();
                Wg6.b(n2, "it.icWork.root");
                n2.setVisibility(8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.S76
    public void c2(String str, String str2) {
        Wg6.c(str, "addressType");
        Wg6.c(str2, "address");
        Bundle bundle = new Bundle();
        bundle.putString("KEY_DEFAULT_PLACE", str2);
        bundle.putString("AddressType", str);
        int hashCode = str.hashCode();
        if (hashCode != 2255103) {
            if (hashCode == 76517104 && str.equals("Other")) {
                Cr4 cr4 = this.j;
                if (cr4 != null) {
                    cr4.h(null);
                }
                CommuteTimeSettingsDefaultAddressActivity.B.b(this, bundle);
            }
        } else if (str.equals("Home")) {
            Cr4 cr42 = this.j;
            if (cr42 != null) {
                cr42.h(null);
            }
            CommuteTimeSettingsDefaultAddressActivity.B.a(this, bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.S76
    public void h0(List<String> list) {
        LinearLayout linearLayout;
        LinearLayout linearLayout2;
        Wg6.c(list, "recentSearchedList");
        O6();
        if (!list.isEmpty()) {
            Bw5 bw5 = this.i;
            if (bw5 != null) {
                bw5.k(Pm7.j0(list));
            }
            G37<N35> g37 = this.g;
            if (g37 != null) {
                N35 a2 = g37.a();
                if (a2 != null && (linearLayout2 = a2.G) != null) {
                    linearLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
        G37<N35> g372 = this.g;
        if (g372 != null) {
            N35 a3 = g372.a();
            if (a3 != null && (linearLayout = a3.G) != null) {
                linearLayout.setVisibility(4);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.S76
    public void k2(String str) {
        Ee5 ee5;
        Wg6.c(str, "userHomeDefaultAddress");
        G37<N35> g37 = this.g;
        if (g37 != null) {
            N35 a2 = g37.a();
            if (a2 != null && (ee5 = a2.x) != null) {
                FlexibleTextView flexibleTextView = ee5.r;
                Wg6.b(flexibleTextView, "homeButton.ftvContent");
                flexibleTextView.setText(str);
                int a3 = (int) P47.a(12, requireContext());
                ee5.t.setPadding(a3, a3, a3, a3);
                if (!TextUtils.isEmpty(str)) {
                    ImageButton imageButton = ee5.t;
                    Wg6.b(imageButton, "homeButton.ivEditButton");
                    imageButton.setImageTintList(ColorStateList.valueOf(W6.d(PortfolioApp.get.instance(), 2131099967)));
                    ee5.t.setImageResource(2131231049);
                    return;
                }
                ImageButton imageButton2 = ee5.t;
                Wg6.b(imageButton2, "homeButton.ivEditButton");
                imageButton2.setImageTintList(ColorStateList.valueOf(W6.d(PortfolioApp.get.instance(), 2131099809)));
                ee5.t.setImageResource(2131231049);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (intent == null) {
            return;
        }
        if (i2 == 100) {
            Bundle bundleExtra = intent.getBundleExtra(Constants.RESULT);
            String string = bundleExtra != null ? bundleExtra.getString("address") : null;
            if (string != null) {
                R6(string);
            }
        } else if (i2 == 111) {
            String stringExtra = intent.getStringExtra("KEY_DEFAULT_PLACE");
            k2(stringExtra != null ? stringExtra : "");
            R76 r76 = this.h;
            if (r76 != null) {
                if (stringExtra == null) {
                    stringExtra = "";
                }
                r76.r(stringExtra);
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        } else if (i2 == 112) {
            String stringExtra2 = intent.getStringExtra("KEY_DEFAULT_PLACE");
            Q4(stringExtra2 != null ? stringExtra2 : "");
            R76 r762 = this.h;
            if (r762 != null) {
                if (stringExtra2 == null) {
                    stringExtra2 = "";
                }
                r762.r(stringExtra2);
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        N35 n35 = (N35) Aq0.f(layoutInflater, 2131558516, viewGroup, false, A6());
        String d = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(d)) {
            n35.F.setBackgroundColor(Color.parseColor(d));
        }
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = n35.q;
        flexibleAutoCompleteTextView.setDropDownBackgroundDrawable(W6.f(flexibleAutoCompleteTextView.getContext(), 2131230821));
        flexibleAutoCompleteTextView.setOnItemClickListener(new Hi(this, n35));
        flexibleAutoCompleteTextView.addTextChangedListener(new Ii(this, n35));
        flexibleAutoCompleteTextView.setOnKeyListener(new Ji(this, n35));
        n35.w.setOnClickListener(new Mi(this));
        Ee5 ee5 = n35.x;
        ee5.u.setImageResource(2131231073);
        FlexibleTextView flexibleTextView = ee5.s;
        Wg6.b(flexibleTextView, "it.ftvTitle");
        flexibleTextView.setText(Um5.c(PortfolioApp.get.instance(), 2131886354));
        ee5.q.setOnClickListener(new Ci(this));
        ee5.t.setOnClickListener(new Di(this));
        Ee5 ee52 = n35.y;
        ee52.u.setImageResource(2131231074);
        FlexibleTextView flexibleTextView2 = ee52.s;
        Wg6.b(flexibleTextView2, "it.ftvTitle");
        flexibleTextView2.setText(Um5.c(PortfolioApp.get.instance(), 2131886355));
        ee52.q.setOnClickListener(new Ei(this));
        ee52.t.setOnClickListener(new Fi(this));
        n35.t.setOnClickListener(new Ni(this, n35));
        Bw5 bw5 = new Bw5();
        bw5.l(new Bi(this, n35));
        this.i = bw5;
        RecyclerView recyclerView = n35.I;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.i);
        Wg6.b(n35, "binding");
        View n = n35.n();
        Wg6.b(n, "binding.root");
        n.getViewTreeObserver().addOnGlobalLayoutListener(new Gi(n, n35));
        n35.z.setOnClickListener(new Oi(this));
        n35.B.setOnClickListener(new Ki(this));
        n35.A.setOnClickListener(new Li(this));
        this.g = new G37<>(this, n35);
        return n35.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        R76 r76 = this.h;
        if (r76 != null) {
            r76.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        R76 r76 = this.h;
        if (r76 != null) {
            r76.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.S76
    public void p6(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        G37<N35> g37 = this.g;
        if (g37 != null) {
            N35 a2 = g37.a();
            if (a2 != null && (flexibleSwitchCompat = a2.J) != null) {
                Wg6.b(flexibleSwitchCompat, "it");
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.S76
    public void s4(CommuteTimeSetting commuteTimeSetting) {
        FLogger.INSTANCE.getLocal().d(m, "showCommuteTimeSettings");
        String format = commuteTimeSetting != null ? commuteTimeSetting.getFormat() : null;
        if (format != null) {
            int hashCode = format.hashCode();
            if (hashCode != -865698022) {
                if (hashCode == 100754 && format.equals("eta")) {
                    G37<N35> g37 = this.g;
                    if (g37 != null) {
                        N35 a2 = g37.a();
                        if (a2 != null) {
                            RTLImageView rTLImageView = a2.z;
                            Wg6.b(rTLImageView, "it.ivArrivalTime");
                            rTLImageView.setAlpha(1.0f);
                            RTLImageView rTLImageView2 = a2.B;
                            Wg6.b(rTLImageView2, "it.ivTravelTime");
                            rTLImageView2.setAlpha(0.5f);
                            return;
                        }
                        return;
                    }
                    Wg6.n("mBinding");
                    throw null;
                }
            } else if (format.equals("travel")) {
                G37<N35> g372 = this.g;
                if (g372 != null) {
                    N35 a3 = g372.a();
                    if (a3 != null) {
                        RTLImageView rTLImageView3 = a3.B;
                        Wg6.b(rTLImageView3, "it.ivTravelTime");
                        rTLImageView3.setAlpha(1.0f);
                        RTLImageView rTLImageView4 = a3.z;
                        Wg6.b(rTLImageView4, "it.ivArrivalTime");
                        rTLImageView4.setAlpha(0.5f);
                        return;
                    }
                    return;
                }
                Wg6.n("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.l;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.S76
    public void w2(String str) {
        boolean z = true;
        Wg6.c(str, "userCurrentAddress");
        if (isActive()) {
            G37<N35> g37 = this.g;
            if (g37 != null) {
                N35 a2 = g37.a();
                if (a2 != null) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    Wg6.b(flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    Wg6.b(text, "it.autocompletePlaces.text");
                    if (Wt7.u0(text).length() == 0) {
                        if (str.length() <= 0) {
                            z = false;
                        }
                        if (z) {
                            a2.q.setText((CharSequence) str, false);
                            this.k = str;
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.S76
    public void x0(boolean z) {
        if (z) {
            Intent intent = new Intent();
            R76 r76 = this.h;
            if (r76 != null) {
                intent.putExtra("COMMUTE_TIME_SETTING", r76.o());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }
}
