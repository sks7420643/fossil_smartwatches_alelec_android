package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Va3 implements Parcelable.Creator<Ka3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ka3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        La3 la3 = null;
        Status status = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                status = (Status) Ad2.e(parcel, t, Status.CREATOR);
            } else if (l != 2) {
                Ad2.B(parcel, t);
            } else {
                la3 = (La3) Ad2.e(parcel, t, La3.CREATOR);
            }
        }
        Ad2.k(parcel, C);
        return new Ka3(status, la3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ka3[] newArray(int i) {
        return new Ka3[i];
    }
}
