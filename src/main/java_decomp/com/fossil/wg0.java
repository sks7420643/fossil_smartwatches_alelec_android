package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wg0 {
    @DexIgnore
    public /* final */ ImageView a;
    @DexIgnore
    public Rh0 b;
    @DexIgnore
    public Rh0 c;
    @DexIgnore
    public Rh0 d;

    @DexIgnore
    public Wg0(ImageView imageView) {
        this.a = imageView;
    }

    @DexIgnore
    public final boolean a(Drawable drawable) {
        if (this.d == null) {
            this.d = new Rh0();
        }
        Rh0 rh0 = this.d;
        rh0.a();
        ColorStateList a2 = Gp0.a(this.a);
        if (a2 != null) {
            rh0.d = true;
            rh0.a = a2;
        }
        PorterDuff.Mode b2 = Gp0.b(this.a);
        if (b2 != null) {
            rh0.c = true;
            rh0.b = b2;
        }
        if (!rh0.d && !rh0.c) {
            return false;
        }
        Ug0.i(drawable, rh0, this.a.getDrawableState());
        return true;
    }

    @DexIgnore
    public void b() {
        Drawable drawable = this.a.getDrawable();
        if (drawable != null) {
            Dh0.b(drawable);
        }
        if (drawable == null) {
            return;
        }
        if (!j() || !a(drawable)) {
            Rh0 rh0 = this.c;
            if (rh0 != null) {
                Ug0.i(drawable, rh0, this.a.getDrawableState());
                return;
            }
            Rh0 rh02 = this.b;
            if (rh02 != null) {
                Ug0.i(drawable, rh02, this.a.getDrawableState());
            }
        }
    }

    @DexIgnore
    public ColorStateList c() {
        Rh0 rh0 = this.c;
        if (rh0 != null) {
            return rh0.a;
        }
        return null;
    }

    @DexIgnore
    public PorterDuff.Mode d() {
        Rh0 rh0 = this.c;
        if (rh0 != null) {
            return rh0.b;
        }
        return null;
    }

    @DexIgnore
    public boolean e() {
        return Build.VERSION.SDK_INT < 21 || !(this.a.getBackground() instanceof RippleDrawable);
    }

    @DexIgnore
    public void f(AttributeSet attributeSet, int i) {
        int n;
        Th0 v = Th0.v(this.a.getContext(), attributeSet, Ue0.AppCompatImageView, i, 0);
        ImageView imageView = this.a;
        Mo0.j0(imageView, imageView.getContext(), Ue0.AppCompatImageView, attributeSet, v.r(), i, 0);
        try {
            Drawable drawable = this.a.getDrawable();
            if (!(drawable != null || (n = v.n(Ue0.AppCompatImageView_srcCompat, -1)) == -1 || (drawable = Gf0.d(this.a.getContext(), n)) == null)) {
                this.a.setImageDrawable(drawable);
            }
            if (drawable != null) {
                Dh0.b(drawable);
            }
            if (v.s(Ue0.AppCompatImageView_tint)) {
                Gp0.c(this.a, v.c(Ue0.AppCompatImageView_tint));
            }
            if (v.s(Ue0.AppCompatImageView_tintMode)) {
                Gp0.d(this.a, Dh0.e(v.k(Ue0.AppCompatImageView_tintMode, -1), null));
            }
        } finally {
            v.w();
        }
    }

    @DexIgnore
    public void g(int i) {
        if (i != 0) {
            Drawable d2 = Gf0.d(this.a.getContext(), i);
            if (d2 != null) {
                Dh0.b(d2);
            }
            this.a.setImageDrawable(d2);
        } else {
            this.a.setImageDrawable(null);
        }
        b();
    }

    @DexIgnore
    public void h(ColorStateList colorStateList) {
        if (this.c == null) {
            this.c = new Rh0();
        }
        Rh0 rh0 = this.c;
        rh0.a = colorStateList;
        rh0.d = true;
        b();
    }

    @DexIgnore
    public void i(PorterDuff.Mode mode) {
        if (this.c == null) {
            this.c = new Rh0();
        }
        Rh0 rh0 = this.c;
        rh0.b = mode;
        rh0.c = true;
        b();
    }

    @DexIgnore
    public final boolean j() {
        int i = Build.VERSION.SDK_INT;
        return i > 21 ? this.b != null : i == 21;
    }
}
