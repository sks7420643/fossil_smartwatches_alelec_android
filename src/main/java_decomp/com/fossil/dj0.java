package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dj0<E> implements Cloneable {
    @DexIgnore
    public static /* final */ Object f; // = new Object();
    @DexIgnore
    public boolean b;
    @DexIgnore
    public long[] c;
    @DexIgnore
    public Object[] d;
    @DexIgnore
    public int e;

    @DexIgnore
    public Dj0() {
        this(10);
    }

    @DexIgnore
    public Dj0(int i) {
        this.b = false;
        if (i == 0) {
            this.c = Cj0.b;
            this.d = Cj0.c;
            return;
        }
        int f2 = Cj0.f(i);
        this.c = new long[f2];
        this.d = new Object[f2];
    }

    @DexIgnore
    @Override // java.lang.Object
    public /* bridge */ /* synthetic */ Object clone() throws CloneNotSupportedException {
        return g();
    }

    @DexIgnore
    public void d(long j, E e2) {
        int i = this.e;
        if (i == 0 || j > this.c[i - 1]) {
            if (this.b && this.e >= this.c.length) {
                j();
            }
            int i2 = this.e;
            if (i2 >= this.c.length) {
                int f2 = Cj0.f(i2 + 1);
                long[] jArr = new long[f2];
                Object[] objArr = new Object[f2];
                long[] jArr2 = this.c;
                System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
                Object[] objArr2 = this.d;
                System.arraycopy(objArr2, 0, objArr, 0, objArr2.length);
                this.c = jArr;
                this.d = objArr;
            }
            this.c[i2] = j;
            this.d[i2] = e2;
            this.e = i2 + 1;
            return;
        }
        r(j, e2);
    }

    @DexIgnore
    public void e() {
        int i = this.e;
        Object[] objArr = this.d;
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = null;
        }
        this.e = 0;
        this.b = false;
    }

    @DexIgnore
    public Dj0<E> g() {
        try {
            Dj0<E> dj0 = (Dj0) super.clone();
            dj0.c = (long[]) this.c.clone();
            dj0.d = (Object[]) this.d.clone();
            return dj0;
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public boolean i(long j) {
        return o(j) >= 0;
    }

    @DexIgnore
    public final void j() {
        int i = this.e;
        long[] jArr = this.c;
        Object[] objArr = this.d;
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3++) {
            Object obj = objArr[i3];
            if (obj != f) {
                if (i3 != i2) {
                    jArr[i2] = jArr[i3];
                    objArr[i2] = obj;
                    objArr[i3] = null;
                }
                i2++;
            }
        }
        this.b = false;
        this.e = i2;
    }

    @DexIgnore
    public E l(long j) {
        return n(j, null);
    }

    @DexIgnore
    public E n(long j, E e2) {
        int b2 = Cj0.b(this.c, this.e, j);
        if (b2 < 0) {
            return e2;
        }
        Object[] objArr = this.d;
        return objArr[b2] == f ? e2 : (E) objArr[b2];
    }

    @DexIgnore
    public int o(long j) {
        if (this.b) {
            j();
        }
        return Cj0.b(this.c, this.e, j);
    }

    @DexIgnore
    public boolean p() {
        return u() == 0;
    }

    @DexIgnore
    public long q(int i) {
        if (this.b) {
            j();
        }
        return this.c[i];
    }

    @DexIgnore
    public void r(long j, E e2) {
        int b2 = Cj0.b(this.c, this.e, j);
        if (b2 >= 0) {
            this.d[b2] = e2;
            return;
        }
        if (b2 < this.e) {
            Object[] objArr = this.d;
            if (objArr[b2] == f) {
                this.c[b2] = j;
                objArr[b2] = e2;
                return;
            }
        }
        if (this.b && this.e >= this.c.length) {
            j();
            b2 = Cj0.b(this.c, this.e, j);
        }
        int i = this.e;
        if (i >= this.c.length) {
            int f2 = Cj0.f(i + 1);
            long[] jArr = new long[f2];
            Object[] objArr2 = new Object[f2];
            long[] jArr2 = this.c;
            System.arraycopy(jArr2, 0, jArr, 0, jArr2.length);
            Object[] objArr3 = this.d;
            System.arraycopy(objArr3, 0, objArr2, 0, objArr3.length);
            this.c = jArr;
            this.d = objArr2;
        }
        int i2 = this.e;
        if (i2 - b2 != 0) {
            long[] jArr3 = this.c;
            int i3 = b2 + 1;
            System.arraycopy(jArr3, b2, jArr3, i3, i2 - b2);
            Object[] objArr4 = this.d;
            System.arraycopy(objArr4, b2, objArr4, i3, this.e - b2);
        }
        this.c[b2] = j;
        this.d[b2] = e2;
        this.e++;
    }

    @DexIgnore
    public void s(long j) {
        Object[] objArr;
        Object obj;
        int b2 = Cj0.b(this.c, this.e, j);
        if (b2 >= 0 && (objArr = this.d)[b2] != (obj = f)) {
            objArr[b2] = obj;
            this.b = true;
        }
    }

    @DexIgnore
    public void t(int i) {
        Object[] objArr = this.d;
        Object obj = objArr[i];
        Object obj2 = f;
        if (obj != obj2) {
            objArr[i] = obj2;
            this.b = true;
        }
    }

    @DexIgnore
    public String toString() {
        if (u() <= 0) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.e * 28);
        sb.append('{');
        for (int i = 0; i < this.e; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(q(i));
            sb.append('=');
            E v = v(i);
            if (v != this) {
                sb.append((Object) v);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public int u() {
        if (this.b) {
            j();
        }
        return this.e;
    }

    @DexIgnore
    public E v(int i) {
        if (this.b) {
            j();
        }
        return (E) this.d[i];
    }
}
