package com.fossil;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.NumberPicker;
import java.text.DateFormatSymbols;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sv5 extends U47 implements Hx6 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ Ai u; // = new Ai(null);
    @DexIgnore
    public Gx6 k;
    @DexIgnore
    public G37<P25> l;
    @DexIgnore
    public Z67 m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Sv5.t;
        }

        @DexIgnore
        public final Sv5 b() {
            return new Sv5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ Sv5 a;

        @DexIgnore
        public Bi(Sv5 sv5) {
            this.a = sv5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Sv5.u.a();
            local.d(a2, "Month was changed from " + i + " to " + i2);
            Sv5.A6(this.a).p(i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ Sv5 a;

        @DexIgnore
        public Ci(Sv5 sv5) {
            this.a = sv5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Sv5.u.a();
            local.d(a2, "Day was changed from " + i + " to " + i2);
            Sv5.A6(this.a).o(i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ Sv5 a;

        @DexIgnore
        public Di(Sv5 sv5) {
            this.a = sv5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Sv5.u.a();
            local.d(a2, "Year was changed from " + i + " to " + i2);
            Sv5.A6(this.a).q(i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Sv5 b;

        @DexIgnore
        public Ei(Sv5 sv5) {
            this.b = sv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Sv5.A6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Sv5 b;

        @DexIgnore
        public Fi(Sv5 sv5) {
            this.b = sv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.dismiss();
        }
    }

    /*
    static {
        String simpleName = Sv5.class.getSimpleName();
        Wg6.b(simpleName, "BirthdayFragment::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ Gx6 A6(Sv5 sv5) {
        Gx6 gx6 = sv5.k;
        if (gx6 != null) {
            return gx6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Hx6
    public void A4(Lc6<Integer, Integer> lc6, Lc6<Integer, Integer> lc62, Lc6<Integer, Integer> lc63) {
        NumberPicker numberPicker;
        NumberPicker numberPicker2;
        NumberPicker numberPicker3;
        NumberPicker numberPicker4;
        NumberPicker numberPicker5;
        NumberPicker numberPicker6;
        Wg6.c(lc6, "dayRange");
        Wg6.c(lc62, "monthRange");
        Wg6.c(lc63, "yearRange");
        P25 C6 = C6();
        if (!(C6 == null || (numberPicker6 = C6.w) == null)) {
            Wg6.b(numberPicker6, "npYear");
            numberPicker6.setMinValue(lc63.getFirst().intValue());
            numberPicker6.setMaxValue(lc63.getSecond().intValue());
        }
        P25 C62 = C6();
        if (!(C62 == null || (numberPicker5 = C62.v) == null)) {
            Wg6.b(numberPicker5, "npMonth");
            numberPicker5.setMinValue(lc62.getFirst().intValue());
            numberPicker5.setMaxValue(lc62.getSecond().intValue());
        }
        P25 C63 = C6();
        if (!(C63 == null || (numberPicker4 = C63.u) == null)) {
            Wg6.b(numberPicker4, "npDay");
            numberPicker4.setMinValue(lc6.getFirst().intValue());
            numberPicker4.setMaxValue(lc6.getSecond().intValue());
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            int i = arguments.getInt("DAY");
            int i2 = arguments.getInt("MONTH");
            int i3 = arguments.getInt("YEAR");
            P25 C64 = C6();
            if (!(C64 == null || (numberPicker3 = C64.u) == null)) {
                numberPicker3.setValue(i);
            }
            P25 C65 = C6();
            if (!(C65 == null || (numberPicker2 = C65.v) == null)) {
                numberPicker2.setValue(i2);
            }
            P25 C66 = C6();
            if (!(C66 == null || (numberPicker = C66.w) == null)) {
                numberPicker.setValue(i3);
            }
            Gx6 gx6 = this.k;
            if (gx6 != null) {
                gx6.o(i);
                Gx6 gx62 = this.k;
                if (gx62 != null) {
                    gx62.p(i2);
                    Gx6 gx63 = this.k;
                    if (gx63 != null) {
                        gx63.q(i3);
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final P25 C6() {
        G37<P25> g37 = this.l;
        if (g37 != null) {
            return g37.a();
        }
        return null;
    }

    @DexIgnore
    public void D6(Gx6 gx6) {
        Wg6.c(gx6, "presenter");
        this.k = gx6;
    }

    @DexIgnore
    @Override // com.fossil.Hx6
    public void M2(int i, int i2) {
        NumberPicker numberPicker;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = t;
        local.d(str, "Update day range: from " + i + " to " + i2);
        P25 C6 = C6();
        if (C6 != null && (numberPicker = C6.u) != null) {
            Wg6.b(numberPicker, "npDay");
            numberPicker.setMinValue(i);
            numberPicker.setMaxValue(i2);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Gx6 gx6) {
        D6(gx6);
    }

    @DexIgnore
    @Override // com.fossil.Hx6
    public void o6(Date date) {
        Wg6.c(date, Constants.PROFILE_KEY_BIRTHDAY);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ProfileSetupFragment", "birthDay: " + date);
        Z67 z67 = this.m;
        if (z67 != null) {
            z67.a().l(date);
            dismiss();
            return;
        }
        Wg6.n("mUserBirthDayViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.U47, androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, 2131951897);
    }

    @DexIgnore
    @Override // com.fossil.Ze0, com.fossil.Nx3, com.fossil.Kq0
    public Dialog onCreateDialog(Bundle bundle) {
        FLogger.INSTANCE.getLocal().d(t, "onCreateDialog");
        View inflate = View.inflate(getContext(), 2131558504, null);
        Wg6.b(inflate, "view");
        inflate.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        Mx3 mx3 = new Mx3(requireContext(), getTheme());
        mx3.setContentView(inflate);
        mx3.setCanceledOnTouchOutside(true);
        return mx3;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        ViewDataBinding e = Aq0.e(layoutInflater, 2131558504, viewGroup, false);
        Wg6.b(e, "DataBindingUtil.inflate(\u2026rthday, container, false)");
        P25 p25 = (P25) e;
        this.l = new G37<>(this, p25);
        return p25.n();
    }

    @DexIgnore
    @Override // com.fossil.U47, androidx.fragment.app.Fragment, com.fossil.Kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Gx6 gx6 = this.k;
        if (gx6 != null) {
            gx6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onStop() {
        super.onStop();
        Gx6 gx6 = this.k;
        if (gx6 != null) {
            gx6.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        NumberPicker numberPicker;
        NumberPicker numberPicker2;
        NumberPicker numberPicker3;
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        Ts0 a2 = Vs0.e(requireActivity()).a(Z67.class);
        Wg6.b(a2, "ViewModelProviders.of(re\u2026DayViewModel::class.java)");
        this.m = (Z67) a2;
        P25 C6 = C6();
        if (!(C6 == null || (numberPicker3 = C6.v) == null)) {
            DateFormatSymbols instance = DateFormatSymbols.getInstance(Locale.getDefault());
            Wg6.b(instance, "DateFormatSymbols.getInstance(Locale.getDefault())");
            numberPicker3.setDisplayedValues(instance.getShortMonths());
            numberPicker3.setOnValueChangedListener(new Bi(this));
        }
        P25 C62 = C6();
        if (!(C62 == null || (numberPicker2 = C62.u) == null)) {
            numberPicker2.setOnValueChangedListener(new Ci(this));
        }
        P25 C63 = C6();
        if (!(C63 == null || (numberPicker = C63.w) == null)) {
            numberPicker.setOnValueChangedListener(new Di(this));
        }
        P25 C64 = C6();
        if (!(C64 == null || (flexibleButton2 = C64.s) == null)) {
            flexibleButton2.setOnClickListener(new Ei(this));
        }
        P25 C65 = C6();
        if (C65 != null && (flexibleButton = C65.r) != null) {
            flexibleButton.setOnClickListener(new Fi(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.U47
    public void z6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
