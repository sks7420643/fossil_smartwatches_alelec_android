package com.fossil;

import com.mapped.Af6;
import com.mapped.Coroutine;
import com.mapped.Wg6;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Un7 implements Af6, Serializable {
    @DexIgnore
    public static /* final */ Un7 INSTANCE; // = new Un7();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private final Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    @Override // com.mapped.Af6
    public <R> R fold(R r, Coroutine<? super R, ? super Af6.Bi, ? extends R> coroutine) {
        Wg6.c(coroutine, "operation");
        return r;
    }

    @DexIgnore
    @Override // com.mapped.Af6
    public <E extends Af6.Bi> E get(Af6.Ci<E> ci) {
        Wg6.c(ci, "key");
        return null;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    @Override // com.mapped.Af6
    public Af6 minusKey(Af6.Ci<?> ci) {
        Wg6.c(ci, "key");
        return this;
    }

    @DexIgnore
    @Override // com.mapped.Af6
    public Af6 plus(Af6 af6) {
        Wg6.c(af6, "context");
        return af6;
    }

    @DexIgnore
    public String toString() {
        return "EmptyCoroutineContext";
    }
}
