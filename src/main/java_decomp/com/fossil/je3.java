package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Je3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Je3> CREATOR; // = new Cf3();
    @DexIgnore
    public String b;

    @DexIgnore
    public Je3(String str) {
        this.b = str;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.u(parcel, 2, this.b, false);
        Bd2.b(parcel, a2);
    }
}
