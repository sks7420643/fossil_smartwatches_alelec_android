package com.fossil;

import android.util.Base64;
import android.util.JsonReader;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.stetho.dumpapp.plugins.FilesDumperPlugin;
import com.fossil.Ta4;
import com.fossil.Ta4$d$d$a$b$a;
import com.fossil.Ta4$d$d$a$b$c;
import com.fossil.Ta4$d$d$a$b$d;
import com.fossil.Ta4$d$d$a$b$e;
import com.fossil.wearables.fsl.countdown.CountDown;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.Explore;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cb4 {
    @DexIgnore
    public static /* final */ Pd4 a;

    @DexIgnore
    public interface Ai<T> {
        @DexIgnore
        T a(JsonReader jsonReader) throws IOException;
    }

    /*
    static {
        Be4 be4 = new Be4();
        be4.g(Y94.a);
        be4.h(true);
        a = be4.f();
    }
    */

    @DexIgnore
    public static Ta4 A(JsonReader jsonReader) throws IOException {
        Ta4.Ai b = Ta4.b();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -2118372775:
                    if (nextName.equals("ndkPayload")) {
                        c = 7;
                        break;
                    }
                    break;
                case -1962630338:
                    if (nextName.equals("sdkVersion")) {
                        c = 0;
                        break;
                    }
                    break;
                case -911706486:
                    if (nextName.equals("buildVersion")) {
                        c = 4;
                        break;
                    }
                    break;
                case 344431858:
                    if (nextName.equals("gmpAppId")) {
                        c = 1;
                        break;
                    }
                    break;
                case 719853845:
                    if (nextName.equals("installationUuid")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1874684019:
                    if (nextName.equals("platform")) {
                        c = 2;
                        break;
                    }
                    break;
                case 1975623094:
                    if (nextName.equals("displayVersion")) {
                        c = 5;
                        break;
                    }
                    break;
                case 1984987798:
                    if (nextName.equals(Constants.SESSION)) {
                        c = 6;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    b.h(jsonReader.nextString());
                    break;
                case 1:
                    b.d(jsonReader.nextString());
                    break;
                case 2:
                    b.g(jsonReader.nextInt());
                    break;
                case 3:
                    b.e(jsonReader.nextString());
                    break;
                case 4:
                    b.b(jsonReader.nextString());
                    break;
                case 5:
                    b.c(jsonReader.nextString());
                    break;
                case 6:
                    b.i(B(jsonReader));
                    break;
                case 7:
                    b.f(y(jsonReader));
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
        return b.a();
    }

    @DexIgnore
    public static Ta4.Di B(JsonReader jsonReader) throws IOException {
        Ta4.Di.Bii a2 = Ta4.Di.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -2128794476:
                    if (nextName.equals("startedAt")) {
                        c = 2;
                        break;
                    }
                    break;
                case -1618432855:
                    if (nextName.equals("identifier")) {
                        c = 1;
                        break;
                    }
                    break;
                case -1606742899:
                    if (nextName.equals(CountDown.COLUMN_ENDED_AT)) {
                        c = 3;
                        break;
                    }
                    break;
                case -1335157162:
                    if (nextName.equals("device")) {
                        c = '\b';
                        break;
                    }
                    break;
                case -1291329255:
                    if (nextName.equals("events")) {
                        c = '\t';
                        break;
                    }
                    break;
                case 3556:
                    if (nextName.equals("os")) {
                        c = 7;
                        break;
                    }
                    break;
                case 96801:
                    if (nextName.equals("app")) {
                        c = 6;
                        break;
                    }
                    break;
                case 3599307:
                    if (nextName.equals("user")) {
                        c = 5;
                        break;
                    }
                    break;
                case 286956243:
                    if (nextName.equals("generator")) {
                        c = 0;
                        break;
                    }
                    break;
                case 1025385094:
                    if (nextName.equals("crashed")) {
                        c = 4;
                        break;
                    }
                    break;
                case 2047016109:
                    if (nextName.equals("generatorType")) {
                        c = '\n';
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    a2.g(jsonReader.nextString());
                    break;
                case 1:
                    a2.j(Base64.decode(jsonReader.nextString(), 2));
                    break;
                case 2:
                    a2.l(jsonReader.nextLong());
                    break;
                case 3:
                    a2.e(Long.valueOf(jsonReader.nextLong()));
                    break;
                case 4:
                    a2.c(jsonReader.nextBoolean());
                    break;
                case 5:
                    a2.m(C(jsonReader));
                    break;
                case 6:
                    a2.b(j(jsonReader));
                    break;
                case 7:
                    a2.k(z(jsonReader));
                    break;
                case '\b':
                    a2.d(m(jsonReader));
                    break;
                case '\t':
                    a2.f(k(jsonReader, Va4.b()));
                    break;
                case '\n':
                    a2.h(jsonReader.nextInt());
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4.Di.Fii C(JsonReader jsonReader) throws IOException {
        Ta4.Di.Fii.Aiii a2 = Ta4.Di.Fii.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            if (nextName.hashCode() == -1618432855 && nextName.equals("identifier")) {
                c = 0;
            }
            if (c != 0) {
                jsonReader.skipValue();
            } else {
                a2.b(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4.Di.Aii j(JsonReader jsonReader) throws IOException {
        Ta4.Di.Aii.Aiii a2 = Ta4.Di.Aii.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1618432855:
                    if (nextName.equals("identifier")) {
                        c = 0;
                        break;
                    }
                    break;
                case 351608024:
                    if (nextName.equals("version")) {
                        c = 1;
                        break;
                    }
                    break;
                case 719853845:
                    if (nextName.equals("installationUuid")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1975623094:
                    if (nextName.equals("displayVersion")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                a2.c(jsonReader.nextString());
            } else if (c == 1) {
                a2.e(jsonReader.nextString());
            } else if (c == 2) {
                a2.b(jsonReader.nextString());
            } else if (c != 3) {
                jsonReader.skipValue();
            } else {
                a2.d(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static <T> Ua4<T> k(JsonReader jsonReader, Ai<T> ai) throws IOException {
        ArrayList arrayList = new ArrayList();
        jsonReader.beginArray();
        while (jsonReader.hasNext()) {
            arrayList.add(ai.a(jsonReader));
        }
        jsonReader.endArray();
        return Ua4.a(arrayList);
    }

    @DexIgnore
    public static Ta4.Bi l(JsonReader jsonReader) throws IOException {
        Ta4.Bi.Aii a2 = Ta4.Bi.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            int hashCode = nextName.hashCode();
            if (hashCode != 106079) {
                if (hashCode == 111972721 && nextName.equals("value")) {
                    c = 1;
                }
            } else if (nextName.equals("key")) {
                c = 0;
            }
            if (c == 0) {
                a2.b(jsonReader.nextString());
            } else if (c != 1) {
                jsonReader.skipValue();
            } else {
                a2.c(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4.Di.Cii m(JsonReader jsonReader) throws IOException {
        Ta4.Di.Cii.Aiii a2 = Ta4.Di.Cii.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1981332476:
                    if (nextName.equals("simulator")) {
                        c = 5;
                        break;
                    }
                    break;
                case -1969347631:
                    if (nextName.equals("manufacturer")) {
                        c = 7;
                        break;
                    }
                    break;
                case 112670:
                    if (nextName.equals("ram")) {
                        c = 3;
                        break;
                    }
                    break;
                case 3002454:
                    if (nextName.equals("arch")) {
                        c = 0;
                        break;
                    }
                    break;
                case 81784169:
                    if (nextName.equals("diskSpace")) {
                        c = 4;
                        break;
                    }
                    break;
                case 94848180:
                    if (nextName.equals("cores")) {
                        c = 2;
                        break;
                    }
                    break;
                case 104069929:
                    if (nextName.equals(DeviceRequestsHelper.DEVICE_INFO_MODEL)) {
                        c = 1;
                        break;
                    }
                    break;
                case 109757585:
                    if (nextName.equals("state")) {
                        c = 6;
                        break;
                    }
                    break;
                case 2078953423:
                    if (nextName.equals("modelClass")) {
                        c = '\b';
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    a2.b(jsonReader.nextInt());
                    break;
                case 1:
                    a2.f(jsonReader.nextString());
                    break;
                case 2:
                    a2.c(jsonReader.nextInt());
                    break;
                case 3:
                    a2.h(jsonReader.nextLong());
                    break;
                case 4:
                    a2.d(jsonReader.nextLong());
                    break;
                case 5:
                    a2.i(jsonReader.nextBoolean());
                    break;
                case 6:
                    a2.j(jsonReader.nextInt());
                    break;
                case 7:
                    a2.e(jsonReader.nextString());
                    break;
                case '\b':
                    a2.g(jsonReader.nextString());
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4.Di.Dii n(JsonReader jsonReader) throws IOException {
        Ta4.Di.Dii.Biii a2 = Ta4.Di.Dii.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1335157162:
                    if (nextName.equals("device")) {
                        c = 3;
                        break;
                    }
                    break;
                case 96801:
                    if (nextName.equals("app")) {
                        c = 2;
                        break;
                    }
                    break;
                case 107332:
                    if (nextName.equals("log")) {
                        c = 4;
                        break;
                    }
                    break;
                case 3575610:
                    if (nextName.equals("type")) {
                        c = 1;
                        break;
                    }
                    break;
                case 55126294:
                    if (nextName.equals("timestamp")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                a2.e(jsonReader.nextLong());
            } else if (c == 1) {
                a2.f(jsonReader.nextString());
            } else if (c == 2) {
                a2.b(o(jsonReader));
            } else if (c == 3) {
                a2.c(q(jsonReader));
            } else if (c != 4) {
                jsonReader.skipValue();
            } else {
                a2.d(u(jsonReader));
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4.Di.Dii.Aiii o(JsonReader jsonReader) throws IOException {
        Ta4.Di.Dii.Aiii.Aiiii a2 = Ta4.Di.Dii.Aiii.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1332194002:
                    if (nextName.equals(Explore.COLUMN_BACKGROUND)) {
                        c = 0;
                        break;
                    }
                    break;
                case -1090974952:
                    if (nextName.equals("execution")) {
                        c = 2;
                        break;
                    }
                    break;
                case 555169704:
                    if (nextName.equals("customAttributes")) {
                        c = 3;
                        break;
                    }
                    break;
                case 928737948:
                    if (nextName.equals("uiOrientation")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                a2.b(Boolean.valueOf(jsonReader.nextBoolean()));
            } else if (c == 1) {
                a2.e(jsonReader.nextInt());
            } else if (c == 2) {
                a2.d(r(jsonReader));
            } else if (c != 3) {
                jsonReader.skipValue();
            } else {
                a2.c(k(jsonReader, Xa4.b()));
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4$d$d$a$b$a p(JsonReader jsonReader) throws IOException {
        Ta4$d$d$a$b$a.a a2 = Ta4$d$d$a$b$a.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case 3373707:
                    if (nextName.equals("name")) {
                        c = 0;
                        break;
                    }
                    break;
                case 3530753:
                    if (nextName.equals("size")) {
                        c = 2;
                        break;
                    }
                    break;
                case 3601339:
                    if (nextName.equals("uuid")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1153765347:
                    if (nextName.equals("baseAddress")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                a2.c(jsonReader.nextString());
            } else if (c == 1) {
                a2.b(jsonReader.nextLong());
            } else if (c == 2) {
                a2.d(jsonReader.nextLong());
            } else if (c != 3) {
                jsonReader.skipValue();
            } else {
                a2.f(Base64.decode(jsonReader.nextString(), 2));
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4.Di.Dii.Ciii q(JsonReader jsonReader) throws IOException {
        Ta4.Di.Dii.Ciii.Aiiii a2 = Ta4.Di.Dii.Ciii.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1708606089:
                    if (nextName.equals(LegacyDeviceModel.COLUMN_BATTERY_LEVEL)) {
                        c = 0;
                        break;
                    }
                    break;
                case -1455558134:
                    if (nextName.equals("batteryVelocity")) {
                        c = 1;
                        break;
                    }
                    break;
                case -1439500848:
                    if (nextName.equals("orientation")) {
                        c = 4;
                        break;
                    }
                    break;
                case 279795450:
                    if (nextName.equals("diskUsed")) {
                        c = 2;
                        break;
                    }
                    break;
                case 976541947:
                    if (nextName.equals("ramUsed")) {
                        c = 5;
                        break;
                    }
                    break;
                case 1516795582:
                    if (nextName.equals("proximityOn")) {
                        c = 3;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                a2.b(Double.valueOf(jsonReader.nextDouble()));
            } else if (c == 1) {
                a2.c(jsonReader.nextInt());
            } else if (c == 2) {
                a2.d(jsonReader.nextLong());
            } else if (c == 3) {
                a2.f(jsonReader.nextBoolean());
            } else if (c == 4) {
                a2.e(jsonReader.nextInt());
            } else if (c != 5) {
                jsonReader.skipValue();
            } else {
                a2.g(jsonReader.nextLong());
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4.Di.Dii.Aiii.Biiii r(JsonReader jsonReader) throws IOException {
        Ta4$d$d$a$b$b a2 = Ta4.Di.Dii.Aiii.Biiii.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1337936983:
                    if (nextName.equals("threads")) {
                        c = 0;
                        break;
                    }
                    break;
                case -902467928:
                    if (nextName.equals("signal")) {
                        c = 2;
                        break;
                    }
                    break;
                case 937615455:
                    if (nextName.equals("binaries")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1481625679:
                    if (nextName.equals("exception")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                a2.e(k(jsonReader, Ya4.b()));
            } else if (c == 1) {
                a2.c(s(jsonReader));
            } else if (c == 2) {
                a2.d(v(jsonReader));
            } else if (c != 3) {
                jsonReader.skipValue();
            } else {
                a2.b(k(jsonReader, Za4.b()));
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4$d$d$a$b$c s(JsonReader jsonReader) throws IOException {
        Ta4$d$d$a$b$c.a a2 = Ta4$d$d$a$b$c.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1266514778:
                    if (nextName.equals("frames")) {
                        c = 1;
                        break;
                    }
                    break;
                case -934964668:
                    if (nextName.equals("reason")) {
                        c = 4;
                        break;
                    }
                    break;
                case 3575610:
                    if (nextName.equals("type")) {
                        c = 3;
                        break;
                    }
                    break;
                case 91997906:
                    if (nextName.equals("causedBy")) {
                        c = 0;
                        break;
                    }
                    break;
                case 581754413:
                    if (nextName.equals("overflowCount")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                a2.b(s(jsonReader));
            } else if (c == 1) {
                a2.c(k(jsonReader, Ab4.b()));
            } else if (c == 2) {
                a2.d(jsonReader.nextInt());
            } else if (c == 3) {
                a2.f(jsonReader.nextString());
            } else if (c != 4) {
                jsonReader.skipValue();
            } else {
                a2.e(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4$d$d$a$b$e.b t(JsonReader jsonReader) throws IOException {
        Ta4$d$d$a$b$e.b.a a2 = Ta4$d$d$a$b$e.b.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -1019779949:
                    if (nextName.equals(Constants.JSON_KEY_OFFSET)) {
                        c = 2;
                        break;
                    }
                    break;
                case -887523944:
                    if (nextName.equals("symbol")) {
                        c = 4;
                        break;
                    }
                    break;
                case 3571:
                    if (nextName.equals("pc")) {
                        c = 3;
                        break;
                    }
                    break;
                case 3143036:
                    if (nextName.equals("file")) {
                        c = 1;
                        break;
                    }
                    break;
                case 2125650548:
                    if (nextName.equals("importance")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                a2.c(jsonReader.nextInt());
            } else if (c == 1) {
                a2.b(jsonReader.nextString());
            } else if (c == 2) {
                a2.d(jsonReader.nextLong());
            } else if (c == 3) {
                a2.e(jsonReader.nextLong());
            } else if (c != 4) {
                jsonReader.skipValue();
            } else {
                a2.f(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4.Di.Dii.Diii u(JsonReader jsonReader) throws IOException {
        Ta4.Di.Dii.Diii.Aiiii a2 = Ta4.Di.Dii.Diii.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            if (nextName.hashCode() == 951530617 && nextName.equals("content")) {
                c = 0;
            }
            if (c != 0) {
                jsonReader.skipValue();
            } else {
                a2.b(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4$d$d$a$b$d v(JsonReader jsonReader) throws IOException {
        Ta4$d$d$a$b$d.a a2 = Ta4$d$d$a$b$d.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            int hashCode = nextName.hashCode();
            if (hashCode != -1147692044) {
                if (hashCode != 3059181) {
                    if (hashCode == 3373707 && nextName.equals("name")) {
                        c = 0;
                    }
                } else if (nextName.equals("code")) {
                    c = 1;
                }
            } else if (nextName.equals("address")) {
                c = 2;
            }
            if (c == 0) {
                a2.d(jsonReader.nextString());
            } else if (c == 1) {
                a2.c(jsonReader.nextString());
            } else if (c != 2) {
                jsonReader.skipValue();
            } else {
                a2.b(jsonReader.nextLong());
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4$d$d$a$b$e w(JsonReader jsonReader) throws IOException {
        Ta4$d$d$a$b$e.a a2 = Ta4$d$d$a$b$e.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            int hashCode = nextName.hashCode();
            if (hashCode != -1266514778) {
                if (hashCode != 3373707) {
                    if (hashCode == 2125650548 && nextName.equals("importance")) {
                        c = 0;
                    }
                } else if (nextName.equals("name")) {
                    c = 1;
                }
            } else if (nextName.equals("frames")) {
                c = 2;
            }
            if (c == 0) {
                a2.c(jsonReader.nextInt());
            } else if (c == 1) {
                a2.d(jsonReader.nextString());
            } else if (c != 2) {
                jsonReader.skipValue();
            } else {
                a2.b(k(jsonReader, Bb4.b()));
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4.Ci.Bii x(JsonReader jsonReader) throws IOException {
        Ta4.Ci.Bii.Aiii a2 = Ta4.Ci.Bii.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            int hashCode = nextName.hashCode();
            if (hashCode != -734768633) {
                if (hashCode == -567321830 && nextName.equals("contents")) {
                    c = 1;
                }
            } else if (nextName.equals("filename")) {
                c = 0;
            }
            if (c == 0) {
                a2.c(jsonReader.nextString());
            } else if (c != 1) {
                jsonReader.skipValue();
            } else {
                a2.b(Base64.decode(jsonReader.nextString(), 2));
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4.Ci y(JsonReader jsonReader) throws IOException {
        Ta4.Ci.Aii a2 = Ta4.Ci.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            int hashCode = nextName.hashCode();
            if (hashCode != 97434231) {
                if (hashCode == 106008351 && nextName.equals("orgId")) {
                    c = 1;
                }
            } else if (nextName.equals(FilesDumperPlugin.NAME)) {
                c = 0;
            }
            if (c == 0) {
                a2.b(k(jsonReader, Wa4.b()));
            } else if (c != 1) {
                jsonReader.skipValue();
            } else {
                a2.c(jsonReader.nextString());
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public static Ta4.Di.Eii z(JsonReader jsonReader) throws IOException {
        Ta4.Di.Eii.Aiii a2 = Ta4.Di.Eii.a();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String nextName = jsonReader.nextName();
            char c = '\uffff';
            switch (nextName.hashCode()) {
                case -911706486:
                    if (nextName.equals("buildVersion")) {
                        c = 2;
                        break;
                    }
                    break;
                case -293026577:
                    if (nextName.equals("jailbroken")) {
                        c = 3;
                        break;
                    }
                    break;
                case 351608024:
                    if (nextName.equals("version")) {
                        c = 1;
                        break;
                    }
                    break;
                case 1874684019:
                    if (nextName.equals("platform")) {
                        c = 0;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                a2.d(jsonReader.nextInt());
            } else if (c == 1) {
                a2.e(jsonReader.nextString());
            } else if (c == 2) {
                a2.b(jsonReader.nextString());
            } else if (c != 3) {
                jsonReader.skipValue();
            } else {
                a2.c(jsonReader.nextBoolean());
            }
        }
        jsonReader.endObject();
        return a2.a();
    }

    @DexIgnore
    public Ta4 D(String str) throws IOException {
        try {
            JsonReader jsonReader = new JsonReader(new StringReader(str));
            try {
                Ta4 A = A(jsonReader);
                jsonReader.close();
                return A;
            } catch (Throwable th) {
            }
            throw th;
        } catch (IllegalStateException e) {
            throw new IOException(e);
        }
    }

    @DexIgnore
    public String E(Ta4 ta4) {
        return a.b(ta4);
    }

    @DexIgnore
    public Ta4.Di.Dii h(String str) throws IOException {
        try {
            JsonReader jsonReader = new JsonReader(new StringReader(str));
            try {
                Ta4.Di.Dii n = n(jsonReader);
                jsonReader.close();
                return n;
            } catch (Throwable th) {
            }
            throw th;
        } catch (IllegalStateException e) {
            throw new IOException(e);
        }
    }

    @DexIgnore
    public String i(Ta4.Di.Dii dii) {
        return a.b(dii);
    }
}
