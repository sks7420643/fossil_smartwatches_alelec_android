package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cadence implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Cadence> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ Integer mAverage;
    @DexIgnore
    public /* final */ Integer mMaximum;
    @DexIgnore
    public /* final */ CadenceUnit mUnit;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<Cadence> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Cadence createFromParcel(Parcel parcel) {
            return new Cadence(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Cadence[] newArray(int i) {
            return new Cadence[i];
        }
    }

    @DexIgnore
    public Cadence(Parcel parcel) {
        if (parcel.readByte() == 0) {
            this.mAverage = null;
        } else {
            this.mAverage = Integer.valueOf(parcel.readInt());
        }
        if (parcel.readByte() == 0) {
            this.mMaximum = null;
        } else {
            this.mMaximum = Integer.valueOf(parcel.readInt());
        }
        this.mUnit = CadenceUnit.values()[parcel.readInt()];
    }

    @DexIgnore
    public Cadence(Integer num, Integer num2, CadenceUnit cadenceUnit) {
        this.mAverage = num;
        this.mMaximum = num2;
        this.mUnit = cadenceUnit;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Integer num;
        Integer num2;
        if (!(obj instanceof Cadence)) {
            return false;
        }
        Cadence cadence = (Cadence) obj;
        if ((this.mAverage != null || cadence.mAverage != null) && ((num2 = this.mAverage) == null || !num2.equals(cadence.mAverage))) {
            return false;
        }
        return ((this.mMaximum == null && cadence.mMaximum == null) || ((num = this.mMaximum) != null && num.equals(cadence.mMaximum))) && this.mUnit == cadence.mUnit;
    }

    @DexIgnore
    public Integer getAverage() {
        return this.mAverage;
    }

    @DexIgnore
    public Integer getMaximum() {
        return this.mMaximum;
    }

    @DexIgnore
    public CadenceUnit getUnit() {
        return this.mUnit;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Integer num = this.mAverage;
        int hashCode = num == null ? 0 : num.hashCode();
        Integer num2 = this.mMaximum;
        if (num2 != null) {
            i = num2.hashCode();
        }
        return ((((hashCode + 527) * 31) + i) * 31) + this.mUnit.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Cadence{mAverage=" + this.mAverage + ",mMaximum=" + this.mMaximum + ",mUnit=" + this.mUnit + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (this.mAverage != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mAverage.intValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mMaximum != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mMaximum.intValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeInt(this.mUnit.ordinal());
    }
}
