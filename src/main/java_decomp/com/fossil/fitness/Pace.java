package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pace implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Pace> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ Float mAverage;
    @DexIgnore
    public /* final */ Float mBest;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<Pace> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Pace createFromParcel(Parcel parcel) {
            return new Pace(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Pace[] newArray(int i) {
            return new Pace[i];
        }
    }

    @DexIgnore
    public Pace(Parcel parcel) {
        if (parcel.readByte() == 0) {
            this.mAverage = null;
        } else {
            this.mAverage = Float.valueOf(parcel.readFloat());
        }
        if (parcel.readByte() == 0) {
            this.mBest = null;
        } else {
            this.mBest = Float.valueOf(parcel.readFloat());
        }
    }

    @DexIgnore
    public Pace(Float f, Float f2) {
        this.mAverage = f;
        this.mBest = f2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Float f;
        Float f2;
        if (!(obj instanceof Pace)) {
            return false;
        }
        Pace pace = (Pace) obj;
        if ((this.mAverage != null || pace.mAverage != null) && ((f2 = this.mAverage) == null || !f2.equals(pace.mAverage))) {
            return false;
        }
        return (this.mBest == null && pace.mBest == null) || ((f = this.mBest) != null && f.equals(pace.mBest));
    }

    @DexIgnore
    public Float getAverage() {
        return this.mAverage;
    }

    @DexIgnore
    public Float getBest() {
        return this.mBest;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Float f = this.mAverage;
        int hashCode = f == null ? 0 : f.hashCode();
        Float f2 = this.mBest;
        if (f2 != null) {
            i = f2.hashCode();
        }
        return ((hashCode + 527) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "Pace{mAverage=" + this.mAverage + ",mBest=" + this.mBest + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (this.mAverage != null) {
            parcel.writeByte((byte) 1);
            parcel.writeFloat(this.mAverage.floatValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mBest != null) {
            parcel.writeByte((byte) 1);
            parcel.writeFloat(this.mBest.floatValue());
            return;
        }
        parcel.writeByte((byte) 0);
    }
}
