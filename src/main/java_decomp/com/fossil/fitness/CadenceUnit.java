package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum CadenceUnit {
    SPM(0),
    RPM(1);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public CadenceUnit(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
