package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SleepStateChange implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<SleepStateChange> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ int mIndexInMinute;
    @DexIgnore
    public /* final */ SleepState mState;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<SleepStateChange> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SleepStateChange createFromParcel(Parcel parcel) {
            return new SleepStateChange(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public SleepStateChange[] newArray(int i) {
            return new SleepStateChange[i];
        }
    }

    @DexIgnore
    public SleepStateChange(Parcel parcel) {
        this.mState = SleepState.values()[parcel.readInt()];
        this.mIndexInMinute = parcel.readInt();
    }

    @DexIgnore
    public SleepStateChange(SleepState sleepState, int i) {
        this.mState = sleepState;
        this.mIndexInMinute = i;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof SleepStateChange)) {
            return false;
        }
        SleepStateChange sleepStateChange = (SleepStateChange) obj;
        return this.mState == sleepStateChange.mState && this.mIndexInMinute == sleepStateChange.mIndexInMinute;
    }

    @DexIgnore
    public int getIndexInMinute() {
        return this.mIndexInMinute;
    }

    @DexIgnore
    public SleepState getState() {
        return this.mState;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.mState.hashCode() + 527) * 31) + this.mIndexInMinute;
    }

    @DexIgnore
    public String toString() {
        return "SleepStateChange{mState=" + this.mState + ",mIndexInMinute=" + this.mIndexInMinute + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mState.ordinal());
        parcel.writeInt(this.mIndexInMinute);
    }
}
