package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ActivitySample implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ActivitySample> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ boolean mActive;
    @DexIgnore
    public /* final */ Short mAvgHeartRate;
    @DexIgnore
    public /* final */ BodyState mBodyState;
    @DexIgnore
    public /* final */ byte mCalories;
    @DexIgnore
    public /* final */ ChargeState mChargeState;
    @DexIgnore
    public /* final */ ArrayList<DebugEntry> mDebugEntries;
    @DexIgnore
    public /* final */ float mDistance;
    @DexIgnore
    public /* final */ ArrayList<GpsDataPoint> mGpsDataPoint;
    @DexIgnore
    public /* final */ Short mHeartrate;
    @DexIgnore
    public /* final */ Byte mHeartrateQuality;
    @DexIgnore
    public /* final */ ArrayList<HighResolutionHeartRate> mHighResolutionHeartRate;
    @DexIgnore
    public /* final */ int mId;
    @DexIgnore
    public /* final */ HashMap<InformationCode, InfomationCodeEntry> mInfomationCodes;
    @DexIgnore
    public /* final */ boolean mIsPaddingEntry;
    @DexIgnore
    public /* final */ Short mMaxHeartRate;
    @DexIgnore
    public /* final */ int mMaxVariance;
    @DexIgnore
    public /* final */ Float mMinuteDistance;
    @DexIgnore
    public /* final */ short mPoint;
    @DexIgnore
    public /* final */ BodyState mRawBodyState;
    @DexIgnore
    public /* final */ ChargeState mRawChargeState;
    @DexIgnore
    public /* final */ Integer mRawHeartRate;
    @DexIgnore
    public /* final */ Float mRecoveryDistance;
    @DexIgnore
    public /* final */ Integer mResting;
    @DexIgnore
    public /* final */ Byte mRestingQuality;
    @DexIgnore
    public /* final */ SkinProximity mSkinProximity;
    @DexIgnore
    public /* final */ short mSteps;
    @DexIgnore
    public /* final */ ArrayList<Integer> mTaggedGoals;
    @DexIgnore
    public /* final */ ArrayList<TaggedWorkoutEntry> mTaggedWorkoutEntries;
    @DexIgnore
    public /* final */ int mTimestamp;
    @DexIgnore
    public /* final */ int mTimezoneOffsetInSecond;
    @DexIgnore
    public /* final */ int mVariance;
    @DexIgnore
    public /* final */ ArrayList<WorkoutSummary> mWorkoutSummary;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<ActivitySample> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActivitySample createFromParcel(Parcel parcel) {
            return new ActivitySample(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActivitySample[] newArray(int i) {
            return new ActivitySample[i];
        }
    }

    @DexIgnore
    public ActivitySample(int i, int i2, int i3, short s, short s2, byte b, int i4, int i5, boolean z, Short sh, Short sh2, Short sh3, float f, Integer num, HashMap<InformationCode, InfomationCodeEntry> hashMap, ArrayList<TaggedWorkoutEntry> arrayList, ArrayList<Integer> arrayList2, SkinProximity skinProximity, Byte b2, Byte b3, ArrayList<DebugEntry> arrayList3, boolean z2, BodyState bodyState, ChargeState chargeState, Integer num2, ArrayList<WorkoutSummary> arrayList4, ArrayList<GpsDataPoint> arrayList5, BodyState bodyState2, ChargeState chargeState2, Float f2, ArrayList<HighResolutionHeartRate> arrayList6, Float f3) {
        this.mId = i;
        this.mTimestamp = i2;
        this.mTimezoneOffsetInSecond = i3;
        this.mSteps = (short) s;
        this.mPoint = (short) s2;
        this.mCalories = (byte) b;
        this.mVariance = i4;
        this.mMaxVariance = i5;
        this.mActive = z;
        this.mHeartrate = sh;
        this.mMaxHeartRate = sh2;
        this.mAvgHeartRate = sh3;
        this.mDistance = f;
        this.mResting = num;
        this.mInfomationCodes = hashMap;
        this.mTaggedWorkoutEntries = arrayList;
        this.mTaggedGoals = arrayList2;
        this.mSkinProximity = skinProximity;
        this.mHeartrateQuality = b2;
        this.mRestingQuality = b3;
        this.mDebugEntries = arrayList3;
        this.mIsPaddingEntry = z2;
        this.mBodyState = bodyState;
        this.mChargeState = chargeState;
        this.mRawHeartRate = num2;
        this.mWorkoutSummary = arrayList4;
        this.mGpsDataPoint = arrayList5;
        this.mRawBodyState = bodyState2;
        this.mRawChargeState = chargeState2;
        this.mMinuteDistance = f2;
        this.mHighResolutionHeartRate = arrayList6;
        this.mRecoveryDistance = f3;
    }

    @DexIgnore
    public ActivitySample(Parcel parcel) {
        boolean z = true;
        this.mId = parcel.readInt();
        this.mTimestamp = parcel.readInt();
        this.mTimezoneOffsetInSecond = parcel.readInt();
        this.mSteps = (short) ((short) parcel.readInt());
        this.mPoint = (short) ((short) parcel.readInt());
        this.mCalories = parcel.readByte();
        this.mVariance = parcel.readInt();
        this.mMaxVariance = parcel.readInt();
        this.mActive = parcel.readByte() != 0;
        if (parcel.readByte() == 0) {
            this.mHeartrate = null;
        } else {
            this.mHeartrate = Short.valueOf((short) parcel.readInt());
        }
        if (parcel.readByte() == 0) {
            this.mMaxHeartRate = null;
        } else {
            this.mMaxHeartRate = Short.valueOf((short) parcel.readInt());
        }
        if (parcel.readByte() == 0) {
            this.mAvgHeartRate = null;
        } else {
            this.mAvgHeartRate = Short.valueOf((short) parcel.readInt());
        }
        this.mDistance = parcel.readFloat();
        if (parcel.readByte() == 0) {
            this.mResting = null;
        } else {
            this.mResting = Integer.valueOf(parcel.readInt());
        }
        HashMap<InformationCode, InfomationCodeEntry> hashMap = new HashMap<>();
        this.mInfomationCodes = hashMap;
        parcel.readMap(hashMap, ActivitySample.class.getClassLoader());
        ArrayList<TaggedWorkoutEntry> arrayList = new ArrayList<>();
        this.mTaggedWorkoutEntries = arrayList;
        parcel.readList(arrayList, ActivitySample.class.getClassLoader());
        ArrayList<Integer> arrayList2 = new ArrayList<>();
        this.mTaggedGoals = arrayList2;
        parcel.readList(arrayList2, ActivitySample.class.getClassLoader());
        if (parcel.readByte() == 0) {
            this.mSkinProximity = null;
        } else {
            this.mSkinProximity = SkinProximity.values()[parcel.readInt()];
        }
        if (parcel.readByte() == 0) {
            this.mHeartrateQuality = null;
        } else {
            this.mHeartrateQuality = Byte.valueOf(parcel.readByte());
        }
        if (parcel.readByte() == 0) {
            this.mRestingQuality = null;
        } else {
            this.mRestingQuality = Byte.valueOf(parcel.readByte());
        }
        ArrayList<DebugEntry> arrayList3 = new ArrayList<>();
        this.mDebugEntries = arrayList3;
        parcel.readList(arrayList3, ActivitySample.class.getClassLoader());
        this.mIsPaddingEntry = parcel.readByte() == 0 ? false : z;
        if (parcel.readByte() == 0) {
            this.mBodyState = null;
        } else {
            this.mBodyState = BodyState.values()[parcel.readInt()];
        }
        if (parcel.readByte() == 0) {
            this.mChargeState = null;
        } else {
            this.mChargeState = ChargeState.values()[parcel.readInt()];
        }
        if (parcel.readByte() == 0) {
            this.mRawHeartRate = null;
        } else {
            this.mRawHeartRate = Integer.valueOf(parcel.readInt());
        }
        ArrayList<WorkoutSummary> arrayList4 = new ArrayList<>();
        this.mWorkoutSummary = arrayList4;
        parcel.readList(arrayList4, ActivitySample.class.getClassLoader());
        ArrayList<GpsDataPoint> arrayList5 = new ArrayList<>();
        this.mGpsDataPoint = arrayList5;
        parcel.readList(arrayList5, ActivitySample.class.getClassLoader());
        if (parcel.readByte() == 0) {
            this.mRawBodyState = null;
        } else {
            this.mRawBodyState = BodyState.values()[parcel.readInt()];
        }
        if (parcel.readByte() == 0) {
            this.mRawChargeState = null;
        } else {
            this.mRawChargeState = ChargeState.values()[parcel.readInt()];
        }
        if (parcel.readByte() == 0) {
            this.mMinuteDistance = null;
        } else {
            this.mMinuteDistance = Float.valueOf(parcel.readFloat());
        }
        ArrayList<HighResolutionHeartRate> arrayList6 = new ArrayList<>();
        this.mHighResolutionHeartRate = arrayList6;
        parcel.readList(arrayList6, ActivitySample.class.getClassLoader());
        if (parcel.readByte() == 0) {
            this.mRecoveryDistance = null;
        } else {
            this.mRecoveryDistance = Float.valueOf(parcel.readFloat());
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Float f;
        Float f2;
        ChargeState chargeState;
        BodyState bodyState;
        Integer num;
        ChargeState chargeState2;
        BodyState bodyState2;
        Byte b;
        Byte b2;
        SkinProximity skinProximity;
        Integer num2;
        Short sh;
        Short sh2;
        Short sh3;
        if (!(obj instanceof ActivitySample)) {
            return false;
        }
        ActivitySample activitySample = (ActivitySample) obj;
        if (this.mId != activitySample.mId || this.mTimestamp != activitySample.mTimestamp || this.mTimezoneOffsetInSecond != activitySample.mTimezoneOffsetInSecond || this.mSteps != activitySample.mSteps || this.mPoint != activitySample.mPoint || this.mCalories != activitySample.mCalories || this.mVariance != activitySample.mVariance || this.mMaxVariance != activitySample.mMaxVariance || this.mActive != activitySample.mActive) {
            return false;
        }
        if ((this.mHeartrate != null || activitySample.mHeartrate != null) && ((sh3 = this.mHeartrate) == null || !sh3.equals(activitySample.mHeartrate))) {
            return false;
        }
        if ((this.mMaxHeartRate != null || activitySample.mMaxHeartRate != null) && ((sh2 = this.mMaxHeartRate) == null || !sh2.equals(activitySample.mMaxHeartRate))) {
            return false;
        }
        if (((this.mAvgHeartRate != null || activitySample.mAvgHeartRate != null) && ((sh = this.mAvgHeartRate) == null || !sh.equals(activitySample.mAvgHeartRate))) || this.mDistance != activitySample.mDistance) {
            return false;
        }
        if (((this.mResting != null || activitySample.mResting != null) && ((num2 = this.mResting) == null || !num2.equals(activitySample.mResting))) || !this.mInfomationCodes.equals(activitySample.mInfomationCodes) || !this.mTaggedWorkoutEntries.equals(activitySample.mTaggedWorkoutEntries) || !this.mTaggedGoals.equals(activitySample.mTaggedGoals)) {
            return false;
        }
        if ((this.mSkinProximity != null || activitySample.mSkinProximity != null) && ((skinProximity = this.mSkinProximity) == null || !skinProximity.equals(activitySample.mSkinProximity))) {
            return false;
        }
        if ((this.mHeartrateQuality != null || activitySample.mHeartrateQuality != null) && ((b2 = this.mHeartrateQuality) == null || !b2.equals(activitySample.mHeartrateQuality))) {
            return false;
        }
        if (((this.mRestingQuality != null || activitySample.mRestingQuality != null) && ((b = this.mRestingQuality) == null || !b.equals(activitySample.mRestingQuality))) || !this.mDebugEntries.equals(activitySample.mDebugEntries) || this.mIsPaddingEntry != activitySample.mIsPaddingEntry) {
            return false;
        }
        if ((this.mBodyState != null || activitySample.mBodyState != null) && ((bodyState2 = this.mBodyState) == null || !bodyState2.equals(activitySample.mBodyState))) {
            return false;
        }
        if ((this.mChargeState != null || activitySample.mChargeState != null) && ((chargeState2 = this.mChargeState) == null || !chargeState2.equals(activitySample.mChargeState))) {
            return false;
        }
        if (((this.mRawHeartRate != null || activitySample.mRawHeartRate != null) && ((num = this.mRawHeartRate) == null || !num.equals(activitySample.mRawHeartRate))) || !this.mWorkoutSummary.equals(activitySample.mWorkoutSummary) || !this.mGpsDataPoint.equals(activitySample.mGpsDataPoint)) {
            return false;
        }
        if ((this.mRawBodyState != null || activitySample.mRawBodyState != null) && ((bodyState = this.mRawBodyState) == null || !bodyState.equals(activitySample.mRawBodyState))) {
            return false;
        }
        if ((this.mRawChargeState != null || activitySample.mRawChargeState != null) && ((chargeState = this.mRawChargeState) == null || !chargeState.equals(activitySample.mRawChargeState))) {
            return false;
        }
        if (((this.mMinuteDistance != null || activitySample.mMinuteDistance != null) && ((f2 = this.mMinuteDistance) == null || !f2.equals(activitySample.mMinuteDistance))) || !this.mHighResolutionHeartRate.equals(activitySample.mHighResolutionHeartRate)) {
            return false;
        }
        return (this.mRecoveryDistance == null && activitySample.mRecoveryDistance == null) || ((f = this.mRecoveryDistance) != null && f.equals(activitySample.mRecoveryDistance));
    }

    @DexIgnore
    public boolean getActive() {
        return this.mActive;
    }

    @DexIgnore
    public Short getAvgHeartRate() {
        return this.mAvgHeartRate;
    }

    @DexIgnore
    public BodyState getBodyState() {
        return this.mBodyState;
    }

    @DexIgnore
    public byte getCalories() {
        return this.mCalories;
    }

    @DexIgnore
    public ChargeState getChargeState() {
        return this.mChargeState;
    }

    @DexIgnore
    public ArrayList<DebugEntry> getDebugEntries() {
        return this.mDebugEntries;
    }

    @DexIgnore
    public float getDistance() {
        return this.mDistance;
    }

    @DexIgnore
    public ArrayList<GpsDataPoint> getGpsDataPoint() {
        return this.mGpsDataPoint;
    }

    @DexIgnore
    public Short getHeartrate() {
        return this.mHeartrate;
    }

    @DexIgnore
    public Byte getHeartrateQuality() {
        return this.mHeartrateQuality;
    }

    @DexIgnore
    public ArrayList<HighResolutionHeartRate> getHighResolutionHeartRate() {
        return this.mHighResolutionHeartRate;
    }

    @DexIgnore
    public int getId() {
        return this.mId;
    }

    @DexIgnore
    public HashMap<InformationCode, InfomationCodeEntry> getInfomationCodes() {
        return this.mInfomationCodes;
    }

    @DexIgnore
    public boolean getIsPaddingEntry() {
        return this.mIsPaddingEntry;
    }

    @DexIgnore
    public Short getMaxHeartRate() {
        return this.mMaxHeartRate;
    }

    @DexIgnore
    public int getMaxVariance() {
        return this.mMaxVariance;
    }

    @DexIgnore
    public Float getMinuteDistance() {
        return this.mMinuteDistance;
    }

    @DexIgnore
    public short getPoint() {
        return this.mPoint;
    }

    @DexIgnore
    public BodyState getRawBodyState() {
        return this.mRawBodyState;
    }

    @DexIgnore
    public ChargeState getRawChargeState() {
        return this.mRawChargeState;
    }

    @DexIgnore
    public Integer getRawHeartRate() {
        return this.mRawHeartRate;
    }

    @DexIgnore
    public Float getRecoveryDistance() {
        return this.mRecoveryDistance;
    }

    @DexIgnore
    public Integer getResting() {
        return this.mResting;
    }

    @DexIgnore
    public Byte getRestingQuality() {
        return this.mRestingQuality;
    }

    @DexIgnore
    public SkinProximity getSkinProximity() {
        return this.mSkinProximity;
    }

    @DexIgnore
    public short getSteps() {
        return this.mSteps;
    }

    @DexIgnore
    public ArrayList<Integer> getTaggedGoals() {
        return this.mTaggedGoals;
    }

    @DexIgnore
    public ArrayList<TaggedWorkoutEntry> getTaggedWorkoutEntries() {
        return this.mTaggedWorkoutEntries;
    }

    @DexIgnore
    public int getTimestamp() {
        return this.mTimestamp;
    }

    @DexIgnore
    public int getTimezoneOffsetInSecond() {
        return this.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public int getVariance() {
        return this.mVariance;
    }

    @DexIgnore
    public ArrayList<WorkoutSummary> getWorkoutSummary() {
        return this.mWorkoutSummary;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.mId;
        int i2 = this.mTimestamp;
        int i3 = this.mTimezoneOffsetInSecond;
        short s = this.mSteps;
        short s2 = this.mPoint;
        byte b = this.mCalories;
        int i4 = this.mVariance;
        int i5 = this.mMaxVariance;
        boolean z = this.mActive;
        Short sh = this.mHeartrate;
        int i6 = 0;
        int hashCode = sh == null ? 0 : sh.hashCode();
        Short sh2 = this.mMaxHeartRate;
        int hashCode2 = sh2 == null ? 0 : sh2.hashCode();
        Short sh3 = this.mAvgHeartRate;
        int hashCode3 = sh3 == null ? 0 : sh3.hashCode();
        int floatToIntBits = Float.floatToIntBits(this.mDistance);
        Integer num = this.mResting;
        int hashCode4 = num == null ? 0 : num.hashCode();
        int hashCode5 = this.mInfomationCodes.hashCode();
        int hashCode6 = this.mTaggedWorkoutEntries.hashCode();
        int hashCode7 = this.mTaggedGoals.hashCode();
        SkinProximity skinProximity = this.mSkinProximity;
        int hashCode8 = skinProximity == null ? 0 : skinProximity.hashCode();
        Byte b2 = this.mHeartrateQuality;
        int hashCode9 = b2 == null ? 0 : b2.hashCode();
        Byte b3 = this.mRestingQuality;
        int hashCode10 = b3 == null ? 0 : b3.hashCode();
        int hashCode11 = this.mDebugEntries.hashCode();
        boolean z2 = this.mIsPaddingEntry;
        BodyState bodyState = this.mBodyState;
        int hashCode12 = bodyState == null ? 0 : bodyState.hashCode();
        ChargeState chargeState = this.mChargeState;
        int hashCode13 = chargeState == null ? 0 : chargeState.hashCode();
        Integer num2 = this.mRawHeartRate;
        int hashCode14 = num2 == null ? 0 : num2.hashCode();
        int hashCode15 = this.mWorkoutSummary.hashCode();
        int hashCode16 = this.mGpsDataPoint.hashCode();
        BodyState bodyState2 = this.mRawBodyState;
        int hashCode17 = bodyState2 == null ? 0 : bodyState2.hashCode();
        ChargeState chargeState2 = this.mRawChargeState;
        int hashCode18 = chargeState2 == null ? 0 : chargeState2.hashCode();
        Float f = this.mMinuteDistance;
        int hashCode19 = f == null ? 0 : f.hashCode();
        int hashCode20 = this.mHighResolutionHeartRate.hashCode();
        Float f2 = this.mRecoveryDistance;
        if (f2 != null) {
            i6 = f2.hashCode();
        }
        return ((((((((((((((((((((((((((((((((((((((((((((hashCode + ((((((((((((((((((i + 527) * 31) + i2) * 31) + i3) * 31) + s) * 31) + s2) * 31) + b) * 31) + i4) * 31) + i5) * 31) + (z ? 1 : 0)) * 31)) * 31) + hashCode2) * 31) + hashCode3) * 31) + floatToIntBits) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + hashCode10) * 31) + hashCode11) * 31) + (z2 ? 1 : 0)) * 31) + hashCode12) * 31) + hashCode13) * 31) + hashCode14) * 31) + hashCode15) * 31) + hashCode16) * 31) + hashCode17) * 31) + hashCode18) * 31) + hashCode19) * 31) + hashCode20) * 31) + i6;
    }

    @DexIgnore
    public String toString() {
        return "ActivitySample{mId=" + this.mId + ",mTimestamp=" + this.mTimestamp + ",mTimezoneOffsetInSecond=" + this.mTimezoneOffsetInSecond + ",mSteps=" + ((int) this.mSteps) + ",mPoint=" + ((int) this.mPoint) + ",mCalories=" + ((int) this.mCalories) + ",mVariance=" + this.mVariance + ",mMaxVariance=" + this.mMaxVariance + ",mActive=" + this.mActive + ",mHeartrate=" + this.mHeartrate + ",mMaxHeartRate=" + this.mMaxHeartRate + ",mAvgHeartRate=" + this.mAvgHeartRate + ",mDistance=" + this.mDistance + ",mResting=" + this.mResting + ",mInfomationCodes=" + this.mInfomationCodes + ",mTaggedWorkoutEntries=" + this.mTaggedWorkoutEntries + ",mTaggedGoals=" + this.mTaggedGoals + ",mSkinProximity=" + this.mSkinProximity + ",mHeartrateQuality=" + this.mHeartrateQuality + ",mRestingQuality=" + this.mRestingQuality + ",mDebugEntries=" + this.mDebugEntries + ",mIsPaddingEntry=" + this.mIsPaddingEntry + ",mBodyState=" + this.mBodyState + ",mChargeState=" + this.mChargeState + ",mRawHeartRate=" + this.mRawHeartRate + ",mWorkoutSummary=" + this.mWorkoutSummary + ",mGpsDataPoint=" + this.mGpsDataPoint + ",mRawBodyState=" + this.mRawBodyState + ",mRawChargeState=" + this.mRawChargeState + ",mMinuteDistance=" + this.mMinuteDistance + ",mHighResolutionHeartRate=" + this.mHighResolutionHeartRate + ",mRecoveryDistance=" + this.mRecoveryDistance + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mId);
        parcel.writeInt(this.mTimestamp);
        parcel.writeInt(this.mTimezoneOffsetInSecond);
        parcel.writeInt(this.mSteps);
        parcel.writeInt(this.mPoint);
        parcel.writeByte(this.mCalories);
        parcel.writeInt(this.mVariance);
        parcel.writeInt(this.mMaxVariance);
        parcel.writeByte(this.mActive ? (byte) 1 : 0);
        if (this.mHeartrate != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mHeartrate.shortValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mMaxHeartRate != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mMaxHeartRate.shortValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mAvgHeartRate != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mAvgHeartRate.shortValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeFloat(this.mDistance);
        if (this.mResting != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mResting.intValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeMap(this.mInfomationCodes);
        parcel.writeList(this.mTaggedWorkoutEntries);
        parcel.writeList(this.mTaggedGoals);
        if (this.mSkinProximity != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mSkinProximity.ordinal());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mHeartrateQuality != null) {
            parcel.writeByte((byte) 1);
            parcel.writeByte(this.mHeartrateQuality.byteValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mRestingQuality != null) {
            parcel.writeByte((byte) 1);
            parcel.writeByte(this.mRestingQuality.byteValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeList(this.mDebugEntries);
        parcel.writeByte(this.mIsPaddingEntry ? (byte) 1 : 0);
        if (this.mBodyState != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mBodyState.ordinal());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mChargeState != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mChargeState.ordinal());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mRawHeartRate != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mRawHeartRate.intValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeList(this.mWorkoutSummary);
        parcel.writeList(this.mGpsDataPoint);
        if (this.mRawBodyState != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mRawBodyState.ordinal());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mRawChargeState != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mRawChargeState.ordinal());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mMinuteDistance != null) {
            parcel.writeByte((byte) 1);
            parcel.writeFloat(this.mMinuteDistance.floatValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeList(this.mHighResolutionHeartRate);
        if (this.mRecoveryDistance != null) {
            parcel.writeByte((byte) 1);
            parcel.writeFloat(this.mRecoveryDistance.floatValue());
            return;
        }
        parcel.writeByte((byte) 0);
    }
}
