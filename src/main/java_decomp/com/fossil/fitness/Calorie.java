package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Calorie implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Calorie> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ int mResolutionInSecond;
    @DexIgnore
    public /* final */ int mTotal;
    @DexIgnore
    public /* final */ ArrayList<Byte> mValues;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<Calorie> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Calorie createFromParcel(Parcel parcel) {
            return new Calorie(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public Calorie[] newArray(int i) {
            return new Calorie[i];
        }
    }

    @DexIgnore
    public Calorie(int i, ArrayList<Byte> arrayList, int i2) {
        this.mResolutionInSecond = i;
        this.mValues = arrayList;
        this.mTotal = i2;
    }

    @DexIgnore
    public Calorie(Parcel parcel) {
        this.mResolutionInSecond = parcel.readInt();
        ArrayList<Byte> arrayList = new ArrayList<>();
        this.mValues = arrayList;
        parcel.readList(arrayList, Calorie.class.getClassLoader());
        this.mTotal = parcel.readInt();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof Calorie)) {
            return false;
        }
        Calorie calorie = (Calorie) obj;
        return this.mResolutionInSecond == calorie.mResolutionInSecond && this.mValues.equals(calorie.mValues) && this.mTotal == calorie.mTotal;
    }

    @DexIgnore
    public int getResolutionInSecond() {
        return this.mResolutionInSecond;
    }

    @DexIgnore
    public int getTotal() {
        return this.mTotal;
    }

    @DexIgnore
    public ArrayList<Byte> getValues() {
        return this.mValues;
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.mResolutionInSecond + 527) * 31) + this.mValues.hashCode()) * 31) + this.mTotal;
    }

    @DexIgnore
    public String toString() {
        return "Calorie{mResolutionInSecond=" + this.mResolutionInSecond + ",mValues=" + this.mValues + ",mTotal=" + this.mTotal + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mResolutionInSecond);
        parcel.writeList(this.mValues);
        parcel.writeInt(this.mTotal);
    }
}
