package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class WorkoutSession implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<WorkoutSession> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ Cadence mCadence;
    @DexIgnore
    public /* final */ Calorie mCalorie;
    @DexIgnore
    public /* final */ Distance mDistance;
    @DexIgnore
    public /* final */ int mDuration;
    @DexIgnore
    public /* final */ int mEndTime;
    @DexIgnore
    public /* final */ ArrayList<GpsDataPoint> mGpsDataPoints;
    @DexIgnore
    public /* final */ HeartRate mHeartrate;
    @DexIgnore
    public /* final */ long mId;
    @DexIgnore
    public /* final */ WorkoutMode mMode;
    @DexIgnore
    public /* final */ Pace mPace;
    @DexIgnore
    public /* final */ int mStartTime;
    @DexIgnore
    public /* final */ ArrayList<WorkoutStateChange> mStateChanges;
    @DexIgnore
    public /* final */ Step mStep;
    @DexIgnore
    public /* final */ int mTimezoneOffsetInSecond;
    @DexIgnore
    public /* final */ WorkoutType mType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<WorkoutSession> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutSession createFromParcel(Parcel parcel) {
            return new WorkoutSession(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutSession[] newArray(int i) {
            return new WorkoutSession[i];
        }
    }

    @DexIgnore
    public WorkoutSession(long j, int i, int i2, int i3, int i4, WorkoutType workoutType, WorkoutMode workoutMode, ArrayList<WorkoutStateChange> arrayList, Step step, Calorie calorie, Distance distance, HeartRate heartRate, ArrayList<GpsDataPoint> arrayList2, Pace pace, Cadence cadence) {
        this.mId = j;
        this.mStartTime = i;
        this.mEndTime = i2;
        this.mTimezoneOffsetInSecond = i3;
        this.mDuration = i4;
        this.mType = workoutType;
        this.mMode = workoutMode;
        this.mStateChanges = arrayList;
        this.mStep = step;
        this.mCalorie = calorie;
        this.mDistance = distance;
        this.mHeartrate = heartRate;
        this.mGpsDataPoints = arrayList2;
        this.mPace = pace;
        this.mCadence = cadence;
    }

    @DexIgnore
    public WorkoutSession(Parcel parcel) {
        this.mId = parcel.readLong();
        this.mStartTime = parcel.readInt();
        this.mEndTime = parcel.readInt();
        this.mTimezoneOffsetInSecond = parcel.readInt();
        this.mDuration = parcel.readInt();
        this.mType = WorkoutType.values()[parcel.readInt()];
        this.mMode = WorkoutMode.values()[parcel.readInt()];
        ArrayList<WorkoutStateChange> arrayList = new ArrayList<>();
        this.mStateChanges = arrayList;
        parcel.readList(arrayList, WorkoutSession.class.getClassLoader());
        this.mStep = new Step(parcel);
        this.mCalorie = new Calorie(parcel);
        if (parcel.readByte() == 0) {
            this.mDistance = null;
        } else {
            this.mDistance = new Distance(parcel);
        }
        if (parcel.readByte() == 0) {
            this.mHeartrate = null;
        } else {
            this.mHeartrate = new HeartRate(parcel);
        }
        ArrayList<GpsDataPoint> arrayList2 = new ArrayList<>();
        this.mGpsDataPoints = arrayList2;
        parcel.readList(arrayList2, WorkoutSession.class.getClassLoader());
        if (parcel.readByte() == 0) {
            this.mPace = null;
        } else {
            this.mPace = new Pace(parcel);
        }
        if (parcel.readByte() == 0) {
            this.mCadence = null;
        } else {
            this.mCadence = new Cadence(parcel);
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Cadence cadence;
        Pace pace;
        HeartRate heartRate;
        Distance distance;
        if (!(obj instanceof WorkoutSession)) {
            return false;
        }
        WorkoutSession workoutSession = (WorkoutSession) obj;
        if (this.mId != workoutSession.mId || this.mStartTime != workoutSession.mStartTime || this.mEndTime != workoutSession.mEndTime || this.mTimezoneOffsetInSecond != workoutSession.mTimezoneOffsetInSecond || this.mDuration != workoutSession.mDuration || this.mType != workoutSession.mType || this.mMode != workoutSession.mMode || !this.mStateChanges.equals(workoutSession.mStateChanges) || !this.mStep.equals(workoutSession.mStep) || !this.mCalorie.equals(workoutSession.mCalorie)) {
            return false;
        }
        if ((this.mDistance != null || workoutSession.mDistance != null) && ((distance = this.mDistance) == null || !distance.equals(workoutSession.mDistance))) {
            return false;
        }
        if (((this.mHeartrate != null || workoutSession.mHeartrate != null) && ((heartRate = this.mHeartrate) == null || !heartRate.equals(workoutSession.mHeartrate))) || !this.mGpsDataPoints.equals(workoutSession.mGpsDataPoints)) {
            return false;
        }
        if ((this.mPace != null || workoutSession.mPace != null) && ((pace = this.mPace) == null || !pace.equals(workoutSession.mPace))) {
            return false;
        }
        return (this.mCadence == null && workoutSession.mCadence == null) || ((cadence = this.mCadence) != null && cadence.equals(workoutSession.mCadence));
    }

    @DexIgnore
    public Cadence getCadence() {
        return this.mCadence;
    }

    @DexIgnore
    public Calorie getCalorie() {
        return this.mCalorie;
    }

    @DexIgnore
    public Distance getDistance() {
        return this.mDistance;
    }

    @DexIgnore
    public int getDuration() {
        return this.mDuration;
    }

    @DexIgnore
    public int getEndTime() {
        return this.mEndTime;
    }

    @DexIgnore
    public ArrayList<GpsDataPoint> getGpsDataPoints() {
        return this.mGpsDataPoints;
    }

    @DexIgnore
    public HeartRate getHeartrate() {
        return this.mHeartrate;
    }

    @DexIgnore
    public long getId() {
        return this.mId;
    }

    @DexIgnore
    public WorkoutMode getMode() {
        return this.mMode;
    }

    @DexIgnore
    public Pace getPace() {
        return this.mPace;
    }

    @DexIgnore
    public int getStartTime() {
        return this.mStartTime;
    }

    @DexIgnore
    public ArrayList<WorkoutStateChange> getStateChanges() {
        return this.mStateChanges;
    }

    @DexIgnore
    public Step getStep() {
        return this.mStep;
    }

    @DexIgnore
    public int getTimezoneOffsetInSecond() {
        return this.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public WorkoutType getType() {
        return this.mType;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.mId;
        int i = (int) (j ^ (j >>> 32));
        int i2 = this.mStartTime;
        int i3 = this.mEndTime;
        int i4 = this.mTimezoneOffsetInSecond;
        int i5 = this.mDuration;
        int hashCode = this.mType.hashCode();
        int hashCode2 = this.mMode.hashCode();
        int hashCode3 = this.mStateChanges.hashCode();
        int hashCode4 = this.mStep.hashCode();
        int hashCode5 = this.mCalorie.hashCode();
        Distance distance = this.mDistance;
        int i6 = 0;
        int hashCode6 = distance == null ? 0 : distance.hashCode();
        HeartRate heartRate = this.mHeartrate;
        int hashCode7 = heartRate == null ? 0 : heartRate.hashCode();
        int hashCode8 = this.mGpsDataPoints.hashCode();
        Pace pace = this.mPace;
        int hashCode9 = pace == null ? 0 : pace.hashCode();
        Cadence cadence = this.mCadence;
        if (cadence != null) {
            i6 = cadence.hashCode();
        }
        return ((((((((hashCode6 + ((((((((((((((((((((i + 527) * 31) + i2) * 31) + i3) * 31) + i4) * 31) + i5) * 31) + hashCode) * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31)) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + i6;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSession{mId=" + this.mId + ",mStartTime=" + this.mStartTime + ",mEndTime=" + this.mEndTime + ",mTimezoneOffsetInSecond=" + this.mTimezoneOffsetInSecond + ",mDuration=" + this.mDuration + ",mType=" + this.mType + ",mMode=" + this.mMode + ",mStateChanges=" + this.mStateChanges + ",mStep=" + this.mStep + ",mCalorie=" + this.mCalorie + ",mDistance=" + this.mDistance + ",mHeartrate=" + this.mHeartrate + ",mGpsDataPoints=" + this.mGpsDataPoints + ",mPace=" + this.mPace + ",mCadence=" + this.mCadence + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.mId);
        parcel.writeInt(this.mStartTime);
        parcel.writeInt(this.mEndTime);
        parcel.writeInt(this.mTimezoneOffsetInSecond);
        parcel.writeInt(this.mDuration);
        parcel.writeInt(this.mType.ordinal());
        parcel.writeInt(this.mMode.ordinal());
        parcel.writeList(this.mStateChanges);
        this.mStep.writeToParcel(parcel, i);
        this.mCalorie.writeToParcel(parcel, i);
        if (this.mDistance != null) {
            parcel.writeByte((byte) 1);
            this.mDistance.writeToParcel(parcel, i);
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mHeartrate != null) {
            parcel.writeByte((byte) 1);
            this.mHeartrate.writeToParcel(parcel, i);
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeList(this.mGpsDataPoints);
        if (this.mPace != null) {
            parcel.writeByte((byte) 1);
            this.mPace.writeToParcel(parcel, i);
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mCadence != null) {
            parcel.writeByte((byte) 1);
            this.mCadence.writeToParcel(parcel, i);
            return;
        }
        parcel.writeByte((byte) 0);
    }
}
