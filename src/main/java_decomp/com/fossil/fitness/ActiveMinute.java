package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ActiveMinute implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ActiveMinute> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ int mResolutionInSecond;
    @DexIgnore
    public /* final */ int mTotal;
    @DexIgnore
    public /* final */ ArrayList<Boolean> mValues;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<ActiveMinute> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActiveMinute createFromParcel(Parcel parcel) {
            return new ActiveMinute(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ActiveMinute[] newArray(int i) {
            return new ActiveMinute[i];
        }
    }

    @DexIgnore
    public ActiveMinute(int i, ArrayList<Boolean> arrayList, int i2) {
        this.mResolutionInSecond = i;
        this.mValues = arrayList;
        this.mTotal = i2;
    }

    @DexIgnore
    public ActiveMinute(Parcel parcel) {
        this.mResolutionInSecond = parcel.readInt();
        ArrayList<Boolean> arrayList = new ArrayList<>();
        this.mValues = arrayList;
        parcel.readList(arrayList, ActiveMinute.class.getClassLoader());
        this.mTotal = parcel.readInt();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof ActiveMinute)) {
            return false;
        }
        ActiveMinute activeMinute = (ActiveMinute) obj;
        return this.mResolutionInSecond == activeMinute.mResolutionInSecond && this.mValues.equals(activeMinute.mValues) && this.mTotal == activeMinute.mTotal;
    }

    @DexIgnore
    public int getResolutionInSecond() {
        return this.mResolutionInSecond;
    }

    @DexIgnore
    public int getTotal() {
        return this.mTotal;
    }

    @DexIgnore
    public ArrayList<Boolean> getValues() {
        return this.mValues;
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.mResolutionInSecond + 527) * 31) + this.mValues.hashCode()) * 31) + this.mTotal;
    }

    @DexIgnore
    public String toString() {
        return "ActiveMinute{mResolutionInSecond=" + this.mResolutionInSecond + ",mValues=" + this.mValues + ",mTotal=" + this.mTotal + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mResolutionInSecond);
        parcel.writeList(this.mValues);
        parcel.writeInt(this.mTotal);
    }
}
