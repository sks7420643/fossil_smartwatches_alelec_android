package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class HeartRate implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<HeartRate> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ short mAverage;
    @DexIgnore
    public /* final */ short mMaximum;
    @DexIgnore
    public /* final */ int mResolutionInSecond;
    @DexIgnore
    public /* final */ ArrayList<Integer> mSampleIndexInSeconds;
    @DexIgnore
    public /* final */ ArrayList<Short> mValues;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<HeartRate> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public HeartRate createFromParcel(Parcel parcel) {
            return new HeartRate(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public HeartRate[] newArray(int i) {
            return new HeartRate[i];
        }
    }

    @DexIgnore
    public HeartRate(int i, ArrayList<Short> arrayList, ArrayList<Integer> arrayList2, short s, short s2) {
        this.mResolutionInSecond = i;
        this.mValues = arrayList;
        this.mSampleIndexInSeconds = arrayList2;
        this.mAverage = (short) s;
        this.mMaximum = (short) s2;
    }

    @DexIgnore
    public HeartRate(Parcel parcel) {
        this.mResolutionInSecond = parcel.readInt();
        ArrayList<Short> arrayList = new ArrayList<>();
        this.mValues = arrayList;
        parcel.readList(arrayList, HeartRate.class.getClassLoader());
        ArrayList<Integer> arrayList2 = new ArrayList<>();
        this.mSampleIndexInSeconds = arrayList2;
        parcel.readList(arrayList2, HeartRate.class.getClassLoader());
        this.mAverage = (short) ((short) parcel.readInt());
        this.mMaximum = (short) ((short) parcel.readInt());
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof HeartRate)) {
            return false;
        }
        HeartRate heartRate = (HeartRate) obj;
        return this.mResolutionInSecond == heartRate.mResolutionInSecond && this.mValues.equals(heartRate.mValues) && this.mSampleIndexInSeconds.equals(heartRate.mSampleIndexInSeconds) && this.mAverage == heartRate.mAverage && this.mMaximum == heartRate.mMaximum;
    }

    @DexIgnore
    public short getAverage() {
        return this.mAverage;
    }

    @DexIgnore
    public short getMaximum() {
        return this.mMaximum;
    }

    @DexIgnore
    public int getResolutionInSecond() {
        return this.mResolutionInSecond;
    }

    @DexIgnore
    public ArrayList<Integer> getSampleIndexInSeconds() {
        return this.mSampleIndexInSeconds;
    }

    @DexIgnore
    public ArrayList<Short> getValues() {
        return this.mValues;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((((this.mResolutionInSecond + 527) * 31) + this.mValues.hashCode()) * 31) + this.mSampleIndexInSeconds.hashCode()) * 31) + this.mAverage) * 31) + this.mMaximum;
    }

    @DexIgnore
    public String toString() {
        return "HeartRate{mResolutionInSecond=" + this.mResolutionInSecond + ",mValues=" + this.mValues + ",mSampleIndexInSeconds=" + this.mSampleIndexInSeconds + ",mAverage=" + ((int) this.mAverage) + ",mMaximum=" + ((int) this.mMaximum) + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mResolutionInSecond);
        parcel.writeList(this.mValues);
        parcel.writeList(this.mSampleIndexInSeconds);
        parcel.writeInt(this.mAverage);
        parcel.writeInt(this.mMaximum);
    }
}
