package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class TaggedWorkoutEntry implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<TaggedWorkoutEntry> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ Byte mAceLatencyInMinute;
    @DexIgnore
    public /* final */ byte mCaloriesBeforeTagged;
    @DexIgnore
    public /* final */ Integer mDistanceBeforeTagged;
    @DexIgnore
    public /* final */ Short mHeartrate;
    @DexIgnore
    public /* final */ long mId;
    @DexIgnore
    public /* final */ WorkoutMode mMode;
    @DexIgnore
    public /* final */ byte mSecondsInMinute;
    @DexIgnore
    public /* final */ WorkoutState mState;
    @DexIgnore
    public /* final */ int mStateChangeIndexInSeconds;
    @DexIgnore
    public /* final */ short mStepsBeforeTagged;
    @DexIgnore
    public /* final */ WorkoutType mType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<TaggedWorkoutEntry> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public TaggedWorkoutEntry createFromParcel(Parcel parcel) {
            return new TaggedWorkoutEntry(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public TaggedWorkoutEntry[] newArray(int i) {
            return new TaggedWorkoutEntry[i];
        }
    }

    @DexIgnore
    public TaggedWorkoutEntry(long j, byte b, short s, byte b2, Short sh, WorkoutState workoutState, WorkoutType workoutType, int i, Byte b3, Integer num, WorkoutMode workoutMode) {
        this.mId = j;
        this.mSecondsInMinute = (byte) b;
        this.mStepsBeforeTagged = (short) s;
        this.mCaloriesBeforeTagged = (byte) b2;
        this.mHeartrate = sh;
        this.mState = workoutState;
        this.mType = workoutType;
        this.mStateChangeIndexInSeconds = i;
        this.mAceLatencyInMinute = b3;
        this.mDistanceBeforeTagged = num;
        this.mMode = workoutMode;
    }

    @DexIgnore
    public TaggedWorkoutEntry(Parcel parcel) {
        this.mId = parcel.readLong();
        this.mSecondsInMinute = parcel.readByte();
        this.mStepsBeforeTagged = (short) ((short) parcel.readInt());
        this.mCaloriesBeforeTagged = parcel.readByte();
        if (parcel.readByte() == 0) {
            this.mHeartrate = null;
        } else {
            this.mHeartrate = Short.valueOf((short) parcel.readInt());
        }
        this.mState = WorkoutState.values()[parcel.readInt()];
        this.mType = WorkoutType.values()[parcel.readInt()];
        this.mStateChangeIndexInSeconds = parcel.readInt();
        if (parcel.readByte() == 0) {
            this.mAceLatencyInMinute = null;
        } else {
            this.mAceLatencyInMinute = Byte.valueOf(parcel.readByte());
        }
        if (parcel.readByte() == 0) {
            this.mDistanceBeforeTagged = null;
        } else {
            this.mDistanceBeforeTagged = Integer.valueOf(parcel.readInt());
        }
        this.mMode = WorkoutMode.values()[parcel.readInt()];
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Integer num;
        Byte b;
        Short sh;
        if (!(obj instanceof TaggedWorkoutEntry)) {
            return false;
        }
        TaggedWorkoutEntry taggedWorkoutEntry = (TaggedWorkoutEntry) obj;
        if (this.mId != taggedWorkoutEntry.mId || this.mSecondsInMinute != taggedWorkoutEntry.mSecondsInMinute || this.mStepsBeforeTagged != taggedWorkoutEntry.mStepsBeforeTagged || this.mCaloriesBeforeTagged != taggedWorkoutEntry.mCaloriesBeforeTagged) {
            return false;
        }
        if (((this.mHeartrate != null || taggedWorkoutEntry.mHeartrate != null) && ((sh = this.mHeartrate) == null || !sh.equals(taggedWorkoutEntry.mHeartrate))) || this.mState != taggedWorkoutEntry.mState || this.mType != taggedWorkoutEntry.mType || this.mStateChangeIndexInSeconds != taggedWorkoutEntry.mStateChangeIndexInSeconds) {
            return false;
        }
        if ((this.mAceLatencyInMinute != null || taggedWorkoutEntry.mAceLatencyInMinute != null) && ((b = this.mAceLatencyInMinute) == null || !b.equals(taggedWorkoutEntry.mAceLatencyInMinute))) {
            return false;
        }
        return ((this.mDistanceBeforeTagged == null && taggedWorkoutEntry.mDistanceBeforeTagged == null) || ((num = this.mDistanceBeforeTagged) != null && num.equals(taggedWorkoutEntry.mDistanceBeforeTagged))) && this.mMode == taggedWorkoutEntry.mMode;
    }

    @DexIgnore
    public Byte getAceLatencyInMinute() {
        return this.mAceLatencyInMinute;
    }

    @DexIgnore
    public byte getCaloriesBeforeTagged() {
        return this.mCaloriesBeforeTagged;
    }

    @DexIgnore
    public Integer getDistanceBeforeTagged() {
        return this.mDistanceBeforeTagged;
    }

    @DexIgnore
    public Short getHeartrate() {
        return this.mHeartrate;
    }

    @DexIgnore
    public long getId() {
        return this.mId;
    }

    @DexIgnore
    public WorkoutMode getMode() {
        return this.mMode;
    }

    @DexIgnore
    public byte getSecondsInMinute() {
        return this.mSecondsInMinute;
    }

    @DexIgnore
    public WorkoutState getState() {
        return this.mState;
    }

    @DexIgnore
    public int getStateChangeIndexInSeconds() {
        return this.mStateChangeIndexInSeconds;
    }

    @DexIgnore
    public short getStepsBeforeTagged() {
        return this.mStepsBeforeTagged;
    }

    @DexIgnore
    public WorkoutType getType() {
        return this.mType;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        long j = this.mId;
        int i2 = (int) (j ^ (j >>> 32));
        byte b = this.mSecondsInMinute;
        short s = this.mStepsBeforeTagged;
        byte b2 = this.mCaloriesBeforeTagged;
        Short sh = this.mHeartrate;
        int hashCode = sh == null ? 0 : sh.hashCode();
        int hashCode2 = this.mState.hashCode();
        int hashCode3 = this.mType.hashCode();
        int i3 = this.mStateChangeIndexInSeconds;
        Byte b3 = this.mAceLatencyInMinute;
        int hashCode4 = b3 == null ? 0 : b3.hashCode();
        Integer num = this.mDistanceBeforeTagged;
        if (num != null) {
            i = num.hashCode();
        }
        return ((((((((((((hashCode + ((((((((i2 + 527) * 31) + b) * 31) + s) * 31) + b2) * 31)) * 31) + hashCode2) * 31) + hashCode3) * 31) + i3) * 31) + hashCode4) * 31) + i) * 31) + this.mMode.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "TaggedWorkoutEntry{mId=" + this.mId + ",mSecondsInMinute=" + ((int) this.mSecondsInMinute) + ",mStepsBeforeTagged=" + ((int) this.mStepsBeforeTagged) + ",mCaloriesBeforeTagged=" + ((int) this.mCaloriesBeforeTagged) + ",mHeartrate=" + this.mHeartrate + ",mState=" + this.mState + ",mType=" + this.mType + ",mStateChangeIndexInSeconds=" + this.mStateChangeIndexInSeconds + ",mAceLatencyInMinute=" + this.mAceLatencyInMinute + ",mDistanceBeforeTagged=" + this.mDistanceBeforeTagged + ",mMode=" + this.mMode + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.mId);
        parcel.writeByte(this.mSecondsInMinute);
        parcel.writeInt(this.mStepsBeforeTagged);
        parcel.writeByte(this.mCaloriesBeforeTagged);
        if (this.mHeartrate != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mHeartrate.shortValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeInt(this.mState.ordinal());
        parcel.writeInt(this.mType.ordinal());
        parcel.writeInt(this.mStateChangeIndexInSeconds);
        if (this.mAceLatencyInMinute != null) {
            parcel.writeByte((byte) 1);
            parcel.writeByte(this.mAceLatencyInMinute.byteValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        if (this.mDistanceBeforeTagged != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mDistanceBeforeTagged.intValue());
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeInt(this.mMode.ordinal());
    }
}
