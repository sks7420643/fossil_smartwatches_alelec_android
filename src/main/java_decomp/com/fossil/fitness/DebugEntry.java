package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DebugEntry implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<DebugEntry> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ short mCode;
    @DexIgnore
    public /* final */ byte[] mData;
    @DexIgnore
    public /* final */ short mFileMajor;
    @DexIgnore
    public /* final */ byte mFileMinor;
    @DexIgnore
    public /* final */ short mLength;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 implements Parcelable.Creator<DebugEntry> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public DebugEntry createFromParcel(Parcel parcel) {
            return new DebugEntry(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public DebugEntry[] newArray(int i) {
            return new DebugEntry[i];
        }
    }

    @DexIgnore
    public DebugEntry(Parcel parcel) {
        this.mFileMajor = (short) ((short) parcel.readInt());
        this.mFileMinor = parcel.readByte();
        this.mCode = (short) ((short) parcel.readInt());
        this.mLength = (short) ((short) parcel.readInt());
        this.mData = parcel.createByteArray();
    }

    @DexIgnore
    public DebugEntry(short s, byte b, short s2, short s3, byte[] bArr) {
        this.mFileMajor = (short) s;
        this.mFileMinor = (byte) b;
        this.mCode = (short) s2;
        this.mLength = (short) s3;
        this.mData = bArr;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof DebugEntry)) {
            return false;
        }
        DebugEntry debugEntry = (DebugEntry) obj;
        return this.mFileMajor == debugEntry.mFileMajor && this.mFileMinor == debugEntry.mFileMinor && this.mCode == debugEntry.mCode && this.mLength == debugEntry.mLength && Arrays.equals(this.mData, debugEntry.mData);
    }

    @DexIgnore
    public short getCode() {
        return this.mCode;
    }

    @DexIgnore
    public byte[] getData() {
        return this.mData;
    }

    @DexIgnore
    public short getFileMajor() {
        return this.mFileMajor;
    }

    @DexIgnore
    public byte getFileMinor() {
        return this.mFileMinor;
    }

    @DexIgnore
    public short getLength() {
        return this.mLength;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((((this.mFileMajor + 527) * 31) + this.mFileMinor) * 31) + this.mCode) * 31) + this.mLength) * 31) + Arrays.hashCode(this.mData);
    }

    @DexIgnore
    public String toString() {
        return "DebugEntry{mFileMajor=" + ((int) this.mFileMajor) + ",mFileMinor=" + ((int) this.mFileMinor) + ",mCode=" + ((int) this.mCode) + ",mLength=" + ((int) this.mLength) + ",mData=" + this.mData + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mFileMajor);
        parcel.writeByte(this.mFileMinor);
        parcel.writeInt(this.mCode);
        parcel.writeInt(this.mLength);
        parcel.writeByteArray(this.mData);
    }
}
