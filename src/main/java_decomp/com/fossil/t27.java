package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t27 extends iq4<a, b, Object> {
    @DexIgnore
    public /* final */ s27 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f3353a;

        @DexIgnore
        public b(String str) {
            pq7.c(str, "passphrase");
            this.f3353a = str;
        }

        @DexIgnore
        public final String a() {
            return this.f3353a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.usecase.GeneratePassphraseUseCase", f = "GeneratePassphraseUseCase.kt", l = {26}, m = "run")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ t27 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(t27 t27, qn7 qn7) {
            super(qn7);
            this.this$0 = t27;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore
    public t27(s27 s27) {
        pq7.c(s27, "mEncryptValueKeyStoreUseCase");
        this.d = s27;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "GeneratePassphraseUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.t27.a r19, com.fossil.qn7<java.lang.Object> r20) {
        /*
        // Method dump skipped, instructions count: 244
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.t27.k(com.fossil.t27$a, com.fossil.qn7):java.lang.Object");
    }
}
