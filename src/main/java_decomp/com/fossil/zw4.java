package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.buddy_challenge.screens.notification.BCNotificationActivity;
import com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsActivity;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zw4 extends BaseFragment {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public G37<T75> g;
    @DexIgnore
    public /* final */ List<Fragment> h; // = new ArrayList();
    @DexIgnore
    public int i;
    @DexIgnore
    public String j; // = "";
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Zw4.l;
        }

        @DexIgnore
        public final Zw4 b() {
            return new Zw4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements TabLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ Zw4 a;

        @DexIgnore
        public Bi(Zw4 zw4) {
            this.a = zw4;
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void a(TabLayout.g gVar) {
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void b(TabLayout.g gVar) {
            if (gVar != null) {
                this.a.O6(gVar.f());
            }
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void c(TabLayout.g gVar) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Qq7 implements Hg6<View, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Zw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Zw4 zw4) {
            super(1);
            this.this$0 = zw4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(View view) {
            invoke(view);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            BCFindFriendsActivity.A.a(this.this$0, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Qq7 implements Hg6<View, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Zw4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(Zw4 zw4) {
            super(1);
            this.this$0 = zw4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(View view) {
            invoke(view);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            BCNotificationActivity.A.a(this.this$0);
        }
    }

    /*
    static {
        String simpleName = Zw4.class.getSimpleName();
        Wg6.b(simpleName, "BCTabsFragment::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public final void M6() {
        G37<T75> g37 = this.g;
        if (g37 != null) {
            T75 a2 = g37.a();
            if (a2 != null) {
                a2.u.c(new Bi(this));
                RTLImageView rTLImageView = a2.r;
                Wg6.b(rTLImageView, "imgSearch");
                Fz4.a(rTLImageView, new Ci(this));
                RTLImageView rTLImageView2 = a2.q;
                Wg6.b(rTLImageView2, "imgNotification");
                Fz4.a(rTLImageView2, new Di(this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void N6() {
        FLogger.INSTANCE.getLocal().d(l, "Inside .initTabs");
        Fragment Z = getChildFragmentManager().Z(Dx4.s.a());
        Fragment Z2 = getChildFragmentManager().Z(By4.m.a());
        Fragment Z3 = getChildFragmentManager().Z(Ky4.s.a());
        if (Z == null) {
            Z = Dx4.s.b();
        }
        if (Z2 == null) {
            Z2 = By4.m.b();
        }
        if (Z3 == null) {
            Z3 = Ky4.s.b();
        }
        this.h.clear();
        this.h.add(Z);
        this.h.add(Z2);
        this.h.add(Z3);
        Yw4 yw4 = new Yw4(this.h, this);
        G37<T75> g37 = this.g;
        if (g37 != null) {
            T75 a2 = g37.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.t;
                Wg6.b(viewPager2, "this");
                viewPager2.setAdapter(yw4);
                viewPager2.setUserInputEnabled(false);
                viewPager2.setOffscreenPageLimit(this.h.size());
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void O6(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.e(str, "scroll to position=" + i2 + " fragment " + this.h.get(i2));
        this.i = i2;
        G37<T75> g37 = this.g;
        if (g37 != null) {
            T75 a2 = g37.a();
            if (a2 != null) {
                a2.t.j(this.i, true);
                Fragment fragment = this.h.get(i2);
                if (fragment instanceof By4) {
                    RTLImageView rTLImageView = a2.r;
                    Wg6.b(rTLImageView, "imgSearch");
                    rTLImageView.setVisibility(0);
                    this.j = "bc_friend_tab";
                } else if (fragment instanceof Ky4) {
                    RTLImageView rTLImageView2 = a2.r;
                    Wg6.b(rTLImageView2, "imgSearch");
                    rTLImageView2.setVisibility(4);
                    this.j = "bc_history_tab";
                } else {
                    this.j = "";
                    RTLImageView rTLImageView3 = a2.r;
                    Wg6.b(rTLImageView3, "imgSearch");
                    rTLImageView3.setVisibility(4);
                }
                AnalyticsHelper g2 = AnalyticsHelper.f.g();
                String str2 = this.j;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    g2.m(str2, activity);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type android.app.Activity");
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        T75 t75 = (T75) Aq0.f(layoutInflater, 2131558573, viewGroup, false, A6());
        this.g = new G37<>(this, t75);
        Wg6.b(t75, "binding");
        return t75.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        AnalyticsHelper g2 = AnalyticsHelper.f.g();
        String str = this.j;
        FragmentActivity activity = getActivity();
        if (activity != null) {
            g2.m(str, activity);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        M6();
        N6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
