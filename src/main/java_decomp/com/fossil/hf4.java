package com.fossil;

import android.content.Intent;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hf4 implements Le4 {
    @DexIgnore
    public /* final */ ExecutorService a;

    @DexIgnore
    public Hf4(ExecutorService executorService) {
        this.a = executorService;
    }

    @DexIgnore
    public static int c(Intent intent) {
        String stringExtra = intent.getStringExtra("CMD");
        if (stringExtra == null) {
            return -1;
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf = String.valueOf(intent.getExtras());
            StringBuilder sb = new StringBuilder(String.valueOf(stringExtra).length() + 21 + String.valueOf(valueOf).length());
            sb.append("Received command: ");
            sb.append(stringExtra);
            sb.append(" - ");
            sb.append(valueOf);
            Log.d("FirebaseInstanceId", sb.toString());
        }
        if ("RST".equals(stringExtra) || "RST_FULL".equals(stringExtra)) {
            FirebaseInstanceId.m().F();
            return -1;
        } else if (!"SYNC".equals(stringExtra)) {
            return -1;
        } else {
            FirebaseInstanceId.m().i();
            return -1;
        }
    }

    @DexIgnore
    @Override // com.fossil.Le4
    public Nt3<Integer> a(Intent intent) {
        return Qt3.c(this.a, new Gf4(intent));
    }
}
