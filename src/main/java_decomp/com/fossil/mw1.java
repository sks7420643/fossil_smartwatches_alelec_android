package com.fossil;

import com.mapped.Wg6;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mw1 extends Gw1 {
    @DexIgnore
    public Mw1(byte[] bArr) {
        super("!preview.rle", bArr);
    }

    @DexIgnore
    @Override // com.fossil.Fw1, com.fossil.Fw1, com.fossil.Gw1, com.fossil.Gw1, com.fossil.Gw1, java.lang.Object
    public Mw1 clone() {
        byte[] bitmapImageData = getBitmapImageData();
        byte[] copyOf = Arrays.copyOf(bitmapImageData, bitmapImageData.length);
        Wg6.b(copyOf, "java.util.Arrays.copyOf(this, size)");
        return new Mw1(copyOf);
    }
}
