package com.fossil;

import android.content.Context;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B52 {
    @DexIgnore
    public static B52 d;
    @DexIgnore
    public O42 a;
    @DexIgnore
    public GoogleSignInAccount b;
    @DexIgnore
    public GoogleSignInOptions c; // = this.a.d();

    @DexIgnore
    public B52(Context context) {
        O42 b2 = O42.b(context);
        this.a = b2;
        this.b = b2.c();
    }

    @DexIgnore
    public static B52 c(Context context) {
        B52 d2;
        synchronized (B52.class) {
            try {
                d2 = d(context.getApplicationContext());
            } catch (Throwable th) {
                throw th;
            }
        }
        return d2;
    }

    @DexIgnore
    public static B52 d(Context context) {
        B52 b52;
        synchronized (B52.class) {
            try {
                if (d == null) {
                    d = new B52(context);
                }
                b52 = d;
            } catch (Throwable th) {
                throw th;
            }
        }
        return b52;
    }

    @DexIgnore
    public final void a() {
        synchronized (this) {
            this.a.a();
            this.b = null;
        }
    }

    @DexIgnore
    public final void b(GoogleSignInOptions googleSignInOptions, GoogleSignInAccount googleSignInAccount) {
        synchronized (this) {
            this.a.f(googleSignInAccount, googleSignInOptions);
            this.b = googleSignInAccount;
            this.c = googleSignInOptions;
        }
    }

    @DexIgnore
    public final GoogleSignInAccount e() {
        GoogleSignInAccount googleSignInAccount;
        synchronized (this) {
            googleSignInAccount = this.b;
        }
        return googleSignInAccount;
    }
}
