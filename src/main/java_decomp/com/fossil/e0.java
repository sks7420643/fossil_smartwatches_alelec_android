package com.fossil;

import android.database.Cursor;
import com.mapped.Rh;
import com.portfolio.platform.data.model.Firmware;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class E0 implements Callable<K0> {
    @DexIgnore
    public /* final */ /* synthetic */ Rh a;
    @DexIgnore
    public /* final */ /* synthetic */ G0 b;

    @DexIgnore
    public E0(G0 g0, Rh rh) {
        this.b = g0;
        this.a = rh;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.concurrent.Callable
    public K0 call() throws Exception {
        K0 k0 = null;
        Cursor b2 = Ex0.b(this.b.a, this.a, false, null);
        try {
            int c = Dx0.c(b2, "id");
            int c2 = Dx0.c(b2, "classifier");
            int c3 = Dx0.c(b2, "packageOSVersion");
            int c4 = Dx0.c(b2, "checksum");
            int c5 = Dx0.c(b2, Firmware.COLUMN_DOWNLOAD_URL);
            int c6 = Dx0.c(b2, "updatedAt");
            int c7 = Dx0.c(b2, "createdAt");
            int c8 = Dx0.c(b2, "data");
            if (b2.moveToFirst()) {
                k0 = new K0(b2.getString(c), this.b.c.b(b2.getInt(c2)), this.b.d.a(b2.getString(c3)), b2.getString(c4), b2.getString(c5), this.b.e.b(b2.getLong(c6)), this.b.e.b(b2.getLong(c7)), b2.getBlob(c8));
            }
            return k0;
        } finally {
            b2.close();
            this.a.m();
        }
    }
}
