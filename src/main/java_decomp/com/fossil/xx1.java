package com.fossil;

import com.fossil.Ix1;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Xx1<T> extends Wx1<T> {
    @DexIgnore
    public /* final */ Ix1.Ai e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Xx1(byte b, byte b2, Ry1 ry1) {
        super(b, b2, ry1);
        Wg6.c(ry1, "version");
        this.e = Ix1.Ai.CRC32C;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Xx1(byte b, byte b2, Ry1 ry1, int i, Qg6 qg6) {
        this(b, (i & 2) != 0 ? (byte) 255 : b2, (i & 4) != 0 ? new Ry1(2, 0) : ry1);
    }

    @DexIgnore
    @Override // com.fossil.Wx1
    public Ix1.Ai c() {
        return this.e;
    }
}
