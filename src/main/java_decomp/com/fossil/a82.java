package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A82 {
    @DexIgnore
    public /* final */ Map<BasePendingResult<?>, Boolean> a; // = Collections.synchronizedMap(new WeakHashMap());
    @DexIgnore
    public /* final */ Map<Ot3<?>, Boolean> b; // = Collections.synchronizedMap(new WeakHashMap());

    @DexIgnore
    public final void b(BasePendingResult<? extends Z62> basePendingResult, boolean z) {
        this.a.put(basePendingResult, Boolean.valueOf(z));
        basePendingResult.b(new Z72(this, basePendingResult));
    }

    @DexIgnore
    public final <TResult> void c(Ot3<TResult> ot3, boolean z) {
        this.b.put(ot3, Boolean.valueOf(z));
        ot3.a().b(new C82(this, ot3));
    }

    @DexIgnore
    public final void d(boolean z, Status status) {
        HashMap hashMap;
        HashMap hashMap2;
        synchronized (this.a) {
            hashMap = new HashMap(this.a);
        }
        synchronized (this.b) {
            hashMap2 = new HashMap(this.b);
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            if (z || ((Boolean) entry.getValue()).booleanValue()) {
                ((BasePendingResult) entry.getKey()).q(status);
            }
        }
        for (Map.Entry entry2 : hashMap2.entrySet()) {
            if (z || ((Boolean) entry2.getValue()).booleanValue()) {
                ((Ot3) entry2.getKey()).d(new N62(status));
            }
        }
    }

    @DexIgnore
    public final boolean e() {
        return !this.a.isEmpty() || !this.b.isEmpty();
    }

    @DexIgnore
    public final void f() {
        d(false, L72.n);
    }

    @DexIgnore
    public final void g() {
        d(true, Ea2.d);
    }
}
