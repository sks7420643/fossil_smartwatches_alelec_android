package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.fossil.M47;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Av4 extends BaseFragment {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ int m; // = 110;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public /* final */ String g; // = ThemeManager.l.a().d("primaryText");
    @DexIgnore
    public G37<D85> h;
    @DexIgnore
    public BCCreateSocialProfileViewModel i;
    @DexIgnore
    public Po4 j;
    @DexIgnore
    public HashMap k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final int a() {
            return Av4.m;
        }

        @DexIgnore
        public final String b() {
            return Av4.l;
        }

        @DexIgnore
        public final Av4 c() {
            return new Av4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ D85 b;
        @DexIgnore
        public /* final */ /* synthetic */ Av4 c;

        @DexIgnore
        public Bi(D85 d85, Av4 av4) {
            this.b = d85;
            this.c = av4;
        }

        @DexIgnore
        public final void onClick(View view) {
            BCCreateSocialProfileViewModel N6 = Av4.N6(this.c);
            FlexibleTextInputEditText flexibleTextInputEditText = this.b.t;
            Wg6.b(flexibleTextInputEditText, "etSocialId");
            String valueOf = String.valueOf(flexibleTextInputEditText.getText());
            if (valueOf != null) {
                N6.h(Wt7.u0(valueOf).toString());
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ D85 b;
        @DexIgnore
        public /* final */ /* synthetic */ Av4 c;

        @DexIgnore
        public Ci(D85 d85, Av4 av4) {
            this.b = d85;
            this.c = av4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            FlexibleTextInputLayout flexibleTextInputLayout = this.b.z;
            Wg6.b(flexibleTextInputLayout, "inputSocialId");
            flexibleTextInputLayout.setErrorEnabled(false);
            BCCreateSocialProfileViewModel N6 = Av4.N6(this.c);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                N6.q(Wt7.u0(valueOf).toString());
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Av4 a;

        @DexIgnore
        public Di(Av4 av4) {
            this.a = av4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            Wg6.b(bool, "loading");
            if (bool.booleanValue()) {
                Av4 av4 = this.a;
                String c = Um5.c(PortfolioApp.get.instance(), 2131886259);
                Wg6.b(c, "LanguageHelper.getString\u2026reating_Text__PleaseWait)");
                av4.H6(c);
                return;
            }
            this.a.a();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<BCCreateSocialProfileViewModel.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ Av4 a;

        @DexIgnore
        public Ei(Av4 av4) {
            this.a = av4;
        }

        @DexIgnore
        public final void a(BCCreateSocialProfileViewModel.Bi bi) {
            D85 d85 = (D85) Av4.K6(this.a).a();
            if (d85 != null) {
                if (bi.b()) {
                    d85.B.setCompoundDrawablesWithIntrinsicBounds(W6.f(PortfolioApp.get.instance(), 2131231114), (Drawable) null, (Drawable) null, (Drawable) null);
                } else {
                    d85.B.setCompoundDrawablesWithIntrinsicBounds(W6.f(PortfolioApp.get.instance(), 2131230967), (Drawable) null, (Drawable) null, (Drawable) null);
                }
                if (bi.a()) {
                    d85.C.setCompoundDrawablesWithIntrinsicBounds(W6.f(PortfolioApp.get.instance(), 2131231114), (Drawable) null, (Drawable) null, (Drawable) null);
                } else {
                    d85.C.setCompoundDrawablesWithIntrinsicBounds(W6.f(PortfolioApp.get.instance(), 2131230967), (Drawable) null, (Drawable) null, (Drawable) null);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(BCCreateSocialProfileViewModel.Bi bi) {
            a(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Av4 a;

        @DexIgnore
        public Fi(Av4 av4) {
            this.a = av4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            Wg6.b(bool, "it");
            if (bool.booleanValue()) {
                Xr4.a.e(PortfolioApp.get.instance().l0(), PortfolioApp.get.instance());
                this.a.Q6();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> implements Ls0<BCCreateSocialProfileViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ Av4 a;

        @DexIgnore
        public Gi(Av4 av4) {
            this.a = av4;
        }

        @DexIgnore
        public final void a(BCCreateSocialProfileViewModel.Ai ai) {
            if (ai.a() != 409) {
                this.a.o(ai.a(), ai.b());
                return;
            }
            String c = Um5.c(PortfolioApp.get.instance(), 2131886262);
            D85 d85 = (D85) Av4.K6(this.a).a();
            if (d85 != null) {
                FlexibleButton flexibleButton = d85.q;
                Wg6.b(flexibleButton, "btnCreateSocialProfile");
                flexibleButton.setEnabled(false);
                FlexibleTextInputLayout flexibleTextInputLayout = d85.z;
                Wg6.b(flexibleTextInputLayout, "inputSocialId");
                flexibleTextInputLayout.setErrorEnabled(true);
                FlexibleTextInputLayout flexibleTextInputLayout2 = d85.z;
                Wg6.b(flexibleTextInputLayout2, "inputSocialId");
                flexibleTextInputLayout2.setError(c);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(BCCreateSocialProfileViewModel.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Av4 a;

        @DexIgnore
        public Hi(Av4 av4) {
            this.a = av4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            FlexibleButton flexibleButton;
            D85 d85 = (D85) Av4.K6(this.a).a();
            if (d85 != null && (flexibleButton = d85.q) != null) {
                Wg6.b(bool, "it");
                flexibleButton.setEnabled(bool.booleanValue());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    /*
    static {
        String simpleName = Av4.class.getSimpleName();
        Wg6.b(simpleName, "BCCreateSocialProfileFra\u2026nt::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 K6(Av4 av4) {
        G37<D85> g37 = av4.h;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ BCCreateSocialProfileViewModel N6(Av4 av4) {
        BCCreateSocialProfileViewModel bCCreateSocialProfileViewModel = av4.i;
        if (bCCreateSocialProfileViewModel != null) {
            return bCCreateSocialProfileViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final String P6() {
        String a2 = M47.a(M47.Ci.TERMS, null);
        String a3 = M47.a(M47.Ci.PRIVACY, null);
        String str = Um5.c(PortfolioApp.get.instance(), 2131886977).toString();
        Wg6.b(a2, "termOfUseUrl");
        String q = Vt7.q(str, "term_of_use_url", a2, false, 4, null);
        Wg6.b(a3, "privacyPolicyUrl");
        return Vt7.q(q, "privacy_policy", a3, false, 4, null);
    }

    @DexIgnore
    public final void Q6() {
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            int i2 = m;
            parentFragment.onActivityResult(i2, i2, null);
        }
    }

    @DexIgnore
    public final void R6() {
        G37<D85> g37 = this.h;
        if (g37 != null) {
            D85 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.x;
                Wg6.b(flexibleTextView, "ftvTermsService");
                flexibleTextView.setMovementMethod(new LinkMovementMethod());
                String str = this.g;
                if (str != null) {
                    a2.x.setLinkTextColor(Color.parseColor(str));
                }
                a2.q.setOnClickListener(new Bi(a2, this));
                String c = Um5.c(PortfolioApp.get.instance(), 2131886266);
                FlexibleTextView flexibleTextView2 = a2.w;
                Wg6.b(flexibleTextView2, "ftvDescriptionBold");
                Jl5 jl5 = Jl5.b;
                Wg6.b(c, "strBold");
                flexibleTextView2.setText(Jl5.b(jl5, c, c, 0, 4, null));
                FlexibleTextView flexibleTextView3 = a2.x;
                Wg6.b(flexibleTextView3, "ftvTermsService");
                flexibleTextView3.setText(Cn0.a(P6(), 0));
                a2.t.addTextChangedListener(new Ci(a2, this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void S6() {
        BCCreateSocialProfileViewModel bCCreateSocialProfileViewModel = this.i;
        if (bCCreateSocialProfileViewModel != null) {
            bCCreateSocialProfileViewModel.n().h(getViewLifecycleOwner(), new Di(this));
            BCCreateSocialProfileViewModel bCCreateSocialProfileViewModel2 = this.i;
            if (bCCreateSocialProfileViewModel2 != null) {
                bCCreateSocialProfileViewModel2.p().h(getViewLifecycleOwner(), new Ei(this));
                BCCreateSocialProfileViewModel bCCreateSocialProfileViewModel3 = this.i;
                if (bCCreateSocialProfileViewModel3 != null) {
                    bCCreateSocialProfileViewModel3.o().h(getViewLifecycleOwner(), new Fi(this));
                    BCCreateSocialProfileViewModel bCCreateSocialProfileViewModel4 = this.i;
                    if (bCCreateSocialProfileViewModel4 != null) {
                        bCCreateSocialProfileViewModel4.m().h(getViewLifecycleOwner(), new Gi(this));
                        BCCreateSocialProfileViewModel bCCreateSocialProfileViewModel5 = this.i;
                        if (bCCreateSocialProfileViewModel5 != null) {
                            bCCreateSocialProfileViewModel5.l().h(getViewLifecycleOwner(), new Hi(this));
                        } else {
                            Wg6.n("viewModel");
                            throw null;
                        }
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void o(int i2, String str) {
        S37 s37 = S37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        s37.n0(i2, str, childFragmentManager);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().o0().a(this);
        Po4 po4 = this.j;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCCreateSocialProfileViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ileViewModel::class.java)");
            this.i = (BCCreateSocialProfileViewModel) a2;
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        D85 d85 = (D85) Aq0.f(layoutInflater, 2131558578, viewGroup, false, A6());
        this.h = new G37<>(this, d85);
        Wg6.b(d85, "binding");
        return d85.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        R6();
        S6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.k;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
