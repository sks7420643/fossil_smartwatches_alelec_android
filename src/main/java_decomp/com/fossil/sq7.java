package com.fossil;

import com.fossil.Js7;
import com.mapped.Ni6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Sq7 extends Uq7 implements Js7 {
    @DexIgnore
    public Sq7() {
    }

    @DexIgnore
    public Sq7(Object obj) {
        super(obj);
    }

    @DexIgnore
    @Override // com.fossil.Gq7
    public Ds7 computeReflected() {
        Er7.e(this);
        return this;
    }

    @DexIgnore
    public abstract /* synthetic */ R get(T t);

    @DexIgnore
    @Override // com.mapped.Ni6
    public Object getDelegate(Object obj) {
        return ((Js7) getReflected()).getDelegate(obj);
    }

    @DexIgnore
    @Override // com.fossil.Yq7, com.mapped.Ni6, com.fossil.Uq7
    public Ni6.Ai getGetter() {
        return ((Js7) getReflected()).getGetter();
    }

    @DexIgnore
    @Override // com.fossil.Js7, com.fossil.Uq7
    public Js7.Ai getSetter() {
        return ((Js7) getReflected()).getSetter();
    }

    @DexIgnore
    @Override // com.mapped.Hg6
    public Object invoke(Object obj) {
        return get(obj);
    }

    @DexIgnore
    public abstract /* synthetic */ void set(T t, R r);
}
