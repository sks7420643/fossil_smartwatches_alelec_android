package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import com.facebook.internal.AnalyticsEvents;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class St0 implements Closeable {
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ File d;
    @DexIgnore
    public /* final */ RandomAccessFile e;
    @DexIgnore
    public /* final */ FileChannel f;
    @DexIgnore
    public /* final */ FileLock g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements FileFilter {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public boolean accept(File file) {
            return !file.getName().equals("MultiDex.lock");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi extends File {
        @DexIgnore
        public long crc; // = -1;

        @DexIgnore
        public Bi(File file, String str) {
            super(file, str);
        }
    }

    @DexIgnore
    public St0(File file, File file2) throws IOException {
        Log.i("MultiDex", "MultiDexExtractor(" + file.getPath() + ", " + file2.getPath() + ")");
        this.b = file;
        this.d = file2;
        this.c = j(file);
        File file3 = new File(file2, "MultiDex.lock");
        RandomAccessFile randomAccessFile = new RandomAccessFile(file3, "rw");
        this.e = randomAccessFile;
        try {
            this.f = randomAccessFile.getChannel();
            try {
                Log.i("MultiDex", "Blocking on lock " + file3.getPath());
                this.g = this.f.lock();
                Log.i("MultiDex", file3.getPath() + " locked");
            } catch (IOException | Error | RuntimeException e2) {
                b(this.f);
                throw e2;
            }
        } catch (IOException | Error | RuntimeException e3) {
            b(this.e);
            throw e3;
        }
    }

    @DexIgnore
    public static void A(Context context, String str, long j, long j2, List<Bi> list) {
        SharedPreferences.Editor edit = f(context).edit();
        edit.putLong(str + "timestamp", j);
        edit.putLong(str + "crc", j2);
        edit.putInt(str + "dex.number", list.size() + 1);
        int i = 2;
        for (Bi bi : list) {
            edit.putLong(str + "dex.crc." + i, bi.crc);
            edit.putLong(str + "dex.time." + i, bi.lastModified());
            i++;
        }
        edit.commit();
    }

    @DexIgnore
    public static void b(Closeable closeable) {
        try {
            closeable.close();
        } catch (IOException e2) {
            Log.w("MultiDex", "Failed to close resource", e2);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static void c(ZipFile zipFile, ZipEntry zipEntry, File file, String str) throws IOException, FileNotFoundException {
        InputStream inputStream = zipFile.getInputStream(zipEntry);
        File createTempFile = File.createTempFile("tmp-" + str, ".zip", file.getParentFile());
        Log.i("MultiDex", "Extracting " + createTempFile.getPath());
        try {
            ZipOutputStream zipOutputStream = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(createTempFile)));
            try {
                ZipEntry zipEntry2 = new ZipEntry("classes.dex");
                zipEntry2.setTime(zipEntry.getTime());
                zipOutputStream.putNextEntry(zipEntry2);
                byte[] bArr = new byte[16384];
                for (int read = inputStream.read(bArr); read != -1; read = inputStream.read(bArr)) {
                    zipOutputStream.write(bArr, 0, read);
                }
                zipOutputStream.closeEntry();
                zipOutputStream.close();
                if (createTempFile.setReadOnly()) {
                    Log.i("MultiDex", "Renaming to " + file.getPath());
                    if (!createTempFile.renameTo(file)) {
                        throw new IOException("Failed to rename \"" + createTempFile.getAbsolutePath() + "\" to \"" + file.getAbsolutePath() + "\"");
                    }
                    return;
                }
                throw new IOException("Failed to mark readonly \"" + createTempFile.getAbsolutePath() + "\" (tmp of \"" + file.getAbsolutePath() + "\")");
            } catch (Throwable th) {
                zipOutputStream.close();
                throw th;
            }
        } finally {
            b(inputStream);
            createTempFile.delete();
        }
    }

    @DexIgnore
    public static SharedPreferences f(Context context) {
        return context.getSharedPreferences("multidex.version", Build.VERSION.SDK_INT < 11 ? 0 : 4);
    }

    @DexIgnore
    public static long h(File file) {
        long lastModified = file.lastModified();
        return lastModified == -1 ? lastModified - 1 : lastModified;
    }

    @DexIgnore
    public static long j(File file) throws IOException {
        long c2 = Tt0.c(file);
        return c2 == -1 ? c2 - 1 : c2;
    }

    @DexIgnore
    public static boolean k(Context context, File file, long j, String str) {
        SharedPreferences f2 = f(context);
        if (f2.getLong(str + "timestamp", -1) == h(file)) {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("crc");
            return f2.getLong(sb.toString(), -1) != j;
        }
    }

    @DexIgnore
    public final void a() {
        File[] listFiles = this.d.listFiles(new Ai());
        if (listFiles == null) {
            Log.w("MultiDex", "Failed to list secondary dex dir content (" + this.d.getPath() + ").");
            return;
        }
        for (File file : listFiles) {
            Log.i("MultiDex", "Trying to delete old file " + file.getPath() + " of size " + file.length());
            if (!file.delete()) {
                Log.w("MultiDex", "Failed to delete old file " + file.getPath());
            } else {
                Log.i("MultiDex", "Deleted old file " + file.getPath());
            }
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.g.release();
        this.f.close();
        this.e.close();
    }

    @DexIgnore
    public List<? extends File> l(Context context, String str, boolean z) throws IOException {
        List<Bi> o;
        Log.i("MultiDex", "MultiDexExtractor.load(" + this.b.getPath() + ", " + z + ", " + str + ")");
        if (this.g.isValid()) {
            if (z || k(context, this.b, this.c, str)) {
                if (z) {
                    Log.i("MultiDex", "Forced extraction must be performed.");
                } else {
                    Log.i("MultiDex", "Detected that extraction must be performed.");
                }
                o = o();
                A(context, str, h(this.b), this.c, o);
            } else {
                try {
                    o = m(context, str);
                } catch (IOException e2) {
                    Log.w("MultiDex", "Failed to reload existing extracted secondary dex files, falling back to fresh extraction", e2);
                    o = o();
                    A(context, str, h(this.b), this.c, o);
                }
            }
            Log.i("MultiDex", "load found " + o.size() + " secondary dex files");
            return o;
        }
        throw new IllegalStateException("MultiDexExtractor was closed");
    }

    @DexIgnore
    public final List<Bi> m(Context context, String str) throws IOException {
        Log.i("MultiDex", "loading existing secondary dex files");
        String str2 = this.b.getName() + ".classes";
        SharedPreferences f2 = f(context);
        int i = f2.getInt(str + "dex.number", 1);
        ArrayList arrayList = new ArrayList(i + -1);
        for (int i2 = 2; i2 <= i; i2++) {
            Bi bi = new Bi(this.d, str2 + i2 + ".zip");
            if (bi.isFile()) {
                bi.crc = j(bi);
                long j = f2.getLong(str + "dex.crc." + i2, -1);
                long j2 = f2.getLong(str + "dex.time." + i2, -1);
                long lastModified = bi.lastModified();
                if (j2 == lastModified && j == bi.crc) {
                    arrayList.add(bi);
                } else {
                    throw new IOException("Invalid extracted dex: " + bi + " (key \"" + str + "\"), expected modification time: " + j2 + ", modification time: " + lastModified + ", expected crc: " + j + ", file crc: " + bi.crc);
                }
            } else {
                throw new IOException("Missing extracted secondary dex file '" + bi.getPath() + "'");
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<Bi> o() throws IOException {
        String str = this.b.getName() + ".classes";
        a();
        ArrayList arrayList = new ArrayList();
        ZipFile zipFile = new ZipFile(this.b);
        try {
            int i = 2;
            ZipEntry entry = zipFile.getEntry("classes2.dex");
            while (entry != null) {
                Bi bi = new Bi(this.d, str + i + ".zip");
                arrayList.add(bi);
                Log.i("MultiDex", "Extraction is needed for file " + bi);
                boolean z = false;
                for (int i2 = 0; i2 < 3 && !z; i2++) {
                    c(zipFile, entry, bi, str);
                    try {
                        bi.crc = j(bi);
                        z = true;
                    } catch (IOException e2) {
                        Log.w("MultiDex", "Failed to read crc from " + bi.getAbsolutePath(), e2);
                        z = false;
                    }
                    StringBuilder sb = new StringBuilder();
                    sb.append("Extraction ");
                    sb.append(z ? AnalyticsEvents.PARAMETER_SHARE_OUTCOME_SUCCEEDED : "failed");
                    sb.append(" '");
                    sb.append(bi.getAbsolutePath());
                    sb.append("': length ");
                    sb.append(bi.length());
                    sb.append(" - crc: ");
                    sb.append(bi.crc);
                    Log.i("MultiDex", sb.toString());
                    if (!z) {
                        bi.delete();
                        if (bi.exists()) {
                            Log.w("MultiDex", "Failed to delete corrupted secondary dex '" + bi.getPath() + "'");
                        }
                    }
                }
                if (z) {
                    int i3 = i + 1;
                    i = i3;
                    entry = zipFile.getEntry("classes" + i3 + ".dex");
                } else {
                    throw new IOException("Could not create zip file " + bi.getAbsolutePath() + " for secondary dex (" + i + ")");
                }
            }
            try {
            } catch (IOException e3) {
                Log.w("MultiDex", "Failed to close resource", e3);
            }
            return arrayList;
        } finally {
            try {
                zipFile.close();
            } catch (IOException e4) {
                Log.w("MultiDex", "Failed to close resource", e4);
            }
        }
    }
}
