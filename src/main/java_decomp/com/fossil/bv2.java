package com.fossil;

import com.fossil.E13;
import com.j256.ormlite.stmt.query.SimpleComparison;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bv2 extends E13<Bv2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Bv2 zzf;
    @DexIgnore
    public static volatile Z23<Bv2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd; // = 1;
    @DexIgnore
    public M13<Xu2> zze; // = E13.B();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Bv2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Bv2.zzf);
        }

        @DexIgnore
        public /* synthetic */ Ai(Tu2 tu2) {
            this();
        }
    }

    @DexIgnore
    public enum Bi implements G13 {
        b(1),
        c(2);
        
        @DexIgnore
        public /* final */ int zzd;

        @DexIgnore
        public Bi(int i) {
            this.zzd = i;
        }

        @DexIgnore
        public static Bi zza(int i) {
            if (i == 1) {
                return b;
            }
            if (i != 2) {
                return null;
            }
            return c;
        }

        @DexIgnore
        public static I13 zzb() {
            return Fv2.a;
        }

        @DexIgnore
        public final String toString() {
            return SimpleComparison.LESS_THAN_OPERATION + Bi.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzd + " name=" + name() + '>';
        }

        @DexIgnore
        @Override // com.fossil.G13
        public final int zza() {
            return this.zzd;
        }
    }

    /*
    static {
        Bv2 bv2 = new Bv2();
        zzf = bv2;
        E13.u(Bv2.class, bv2);
    }
    */

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Tu2.a[i - 1]) {
            case 1:
                return new Bv2();
            case 2:
                return new Ai(null);
            case 3:
                I13 zzb = Bi.zzb();
                return E13.s(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u100c\u0000\u0002\u001b", new Object[]{"zzc", "zzd", zzb, "zze", Xu2.class});
            case 4:
                return zzf;
            case 5:
                Z23<Bv2> z232 = zzg;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Bv2.class) {
                    try {
                        z23 = zzg;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzf);
                            zzg = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
