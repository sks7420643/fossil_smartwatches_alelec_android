package com.fossil;

import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class G74 implements Mg4 {
    @DexIgnore
    public /* final */ Set a;

    @DexIgnore
    public G74(Set set) {
        this.a = set;
    }

    @DexIgnore
    public static Mg4 a(Set set) {
        return new G74(set);
    }

    @DexIgnore
    @Override // com.fossil.Mg4
    public Object get() {
        return I74.f(this.a);
    }
}
