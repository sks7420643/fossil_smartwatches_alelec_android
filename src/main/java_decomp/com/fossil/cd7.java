package com.fossil;

import android.view.ViewTreeObserver;
import android.widget.ImageView;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cd7 implements ViewTreeObserver.OnPreDrawListener {
    @DexIgnore
    public /* final */ Qd7 b;
    @DexIgnore
    public /* final */ WeakReference<ImageView> c;
    @DexIgnore
    public Zc7 d;

    @DexIgnore
    public Cd7(Qd7 qd7, ImageView imageView, Zc7 zc7) {
        this.b = qd7;
        this.c = new WeakReference<>(imageView);
        this.d = zc7;
        imageView.getViewTreeObserver().addOnPreDrawListener(this);
    }

    @DexIgnore
    public void a() {
        this.d = null;
        ImageView imageView = this.c.get();
        if (imageView != null) {
            ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this);
            }
        }
    }

    @DexIgnore
    public boolean onPreDraw() {
        ImageView imageView = this.c.get();
        if (imageView != null) {
            ViewTreeObserver viewTreeObserver = imageView.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                int width = imageView.getWidth();
                int height = imageView.getHeight();
                if (width > 0 && height > 0) {
                    viewTreeObserver.removeOnPreDrawListener(this);
                    Qd7 qd7 = this.b;
                    qd7.k();
                    qd7.i(width, height);
                    qd7.e(imageView, this.d);
                }
            }
        }
        return true;
    }
}
