package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class D75 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ Bf5 r;
    @DexIgnore
    public /* final */ Bf5 s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ LinearLayout u;
    @DexIgnore
    public /* final */ LinearLayout v;
    @DexIgnore
    public /* final */ TodayHeartRateChart w;

    @DexIgnore
    public D75(Object obj, View view, int i, FlexibleTextView flexibleTextView, Bf5 bf5, Bf5 bf52, ImageView imageView, LinearLayout linearLayout, LinearLayout linearLayout2, TodayHeartRateChart todayHeartRateChart) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = bf5;
        x(bf5);
        this.s = bf52;
        x(bf52);
        this.t = imageView;
        this.u = linearLayout;
        this.v = linearLayout2;
        this.w = todayHeartRateChart;
    }
}
