package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Vm7 implements Iterator<Long>, Jr7 {
    @DexIgnore
    public final Long a() {
        return Long.valueOf(b());
    }

    @DexIgnore
    public abstract long b();

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // java.util.Iterator
    public /* bridge */ /* synthetic */ Long next() {
        return a();
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
