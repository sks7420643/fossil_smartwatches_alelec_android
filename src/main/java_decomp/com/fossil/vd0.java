package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.media.browse.MediaBrowser;
import android.os.Bundle;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vd0 {

    @DexIgnore
    public interface Ai {
        @DexIgnore
        Object c();  // void declaration

        @DexIgnore
        Object d();  // void declaration

        @DexIgnore
        Object e();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<T extends Ai> extends MediaBrowser.ConnectionCallback {
        @DexIgnore
        public /* final */ T a;

        @DexIgnore
        public Bi(T t) {
            this.a = t;
        }

        @DexIgnore
        public void onConnected() {
            this.a.d();
        }

        @DexIgnore
        public void onConnectionFailed() {
            this.a.e();
        }

        @DexIgnore
        public void onConnectionSuspended() {
            this.a.c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci {
        @DexIgnore
        public static Object a(Object obj) {
            return ((MediaBrowser.MediaItem) obj).getDescription();
        }

        @DexIgnore
        public static int b(Object obj) {
            return ((MediaBrowser.MediaItem) obj).getFlags();
        }
    }

    @DexIgnore
    public interface Di {
        @DexIgnore
        void c(String str, List<?> list);

        @DexIgnore
        void d(String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei<T extends Di> extends MediaBrowser.SubscriptionCallback {
        @DexIgnore
        public /* final */ T a;

        @DexIgnore
        public Ei(T t) {
            this.a = t;
        }

        @DexIgnore
        @Override // android.media.browse.MediaBrowser.SubscriptionCallback
        public void onChildrenLoaded(String str, List<MediaBrowser.MediaItem> list) {
            this.a.c(str, list);
        }

        @DexIgnore
        public void onError(String str) {
            this.a.d(str);
        }
    }

    @DexIgnore
    public static void a(Object obj) {
        ((MediaBrowser) obj).connect();
    }

    @DexIgnore
    public static Object b(Context context, ComponentName componentName, Object obj, Bundle bundle) {
        return new MediaBrowser(context, componentName, (MediaBrowser.ConnectionCallback) obj, bundle);
    }

    @DexIgnore
    public static Object c(Ai ai) {
        return new Bi(ai);
    }

    @DexIgnore
    public static Object d(Di di) {
        return new Ei(di);
    }

    @DexIgnore
    public static void e(Object obj) {
        ((MediaBrowser) obj).disconnect();
    }

    @DexIgnore
    public static Bundle f(Object obj) {
        return ((MediaBrowser) obj).getExtras();
    }

    @DexIgnore
    public static Object g(Object obj) {
        return ((MediaBrowser) obj).getSessionToken();
    }
}
