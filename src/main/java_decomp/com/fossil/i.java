package com.fossil;

import android.bluetooth.BluetoothDevice;
import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I extends Qq7 implements Hg6<Zk1, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ BluetoothDevice b;
    @DexIgnore
    public /* final */ /* synthetic */ E60 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public I(BluetoothDevice bluetoothDevice, E60 e60) {
        super(1);
        this.b = bluetoothDevice;
        this.c = e60;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Zk1 zk1) {
        W00 w00 = W00.c;
        String serialNumber = zk1.getSerialNumber();
        String address = this.b.getAddress();
        Wg6.b(address, "bluetoothDevice.address");
        w00.d(serialNumber, address);
        BluetoothLeAdapter bluetoothLeAdapter = BluetoothLeAdapter.k;
        BluetoothLeAdapter.d.remove(this.b.getAddress());
        BluetoothLeAdapter.l(BluetoothLeAdapter.k, this.c);
        return Cd6.a;
    }
}
