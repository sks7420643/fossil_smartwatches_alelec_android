package com.fossil;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.Process;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zf2 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public Zf2(Context context) {
        this.a = context;
    }

    @DexIgnore
    public int a(String str) {
        return this.a.checkCallingOrSelfPermission(str);
    }

    @DexIgnore
    public int b(String str, String str2) {
        return this.a.getPackageManager().checkPermission(str, str2);
    }

    @DexIgnore
    public ApplicationInfo c(String str, int i) throws PackageManager.NameNotFoundException {
        return this.a.getPackageManager().getApplicationInfo(str, i);
    }

    @DexIgnore
    public CharSequence d(String str) throws PackageManager.NameNotFoundException {
        return this.a.getPackageManager().getApplicationLabel(this.a.getPackageManager().getApplicationInfo(str, 0));
    }

    @DexIgnore
    public PackageInfo e(String str, int i) throws PackageManager.NameNotFoundException {
        return this.a.getPackageManager().getPackageInfo(str, i);
    }

    @DexIgnore
    public final String[] f(int i) {
        return this.a.getPackageManager().getPackagesForUid(i);
    }

    @DexIgnore
    public boolean g() {
        String nameForUid;
        if (Binder.getCallingUid() == Process.myUid()) {
            return Yf2.a(this.a);
        }
        if (!Mf2.j() || (nameForUid = this.a.getPackageManager().getNameForUid(Binder.getCallingUid())) == null) {
            return false;
        }
        return this.a.getPackageManager().isInstantApp(nameForUid);
    }

    @DexIgnore
    public final PackageInfo h(String str, int i, int i2) throws PackageManager.NameNotFoundException {
        return this.a.getPackageManager().getPackageInfo(str, 64);
    }

    @DexIgnore
    @TargetApi(19)
    public final boolean i(int i, String str) {
        if (Mf2.f()) {
            try {
                ((AppOpsManager) this.a.getSystemService("appops")).checkPackage(i, str);
                return true;
            } catch (SecurityException e) {
                return false;
            }
        } else {
            String[] packagesForUid = this.a.getPackageManager().getPackagesForUid(i);
            if (str == null || packagesForUid == null) {
                return false;
            }
            for (String str2 : packagesForUid) {
                if (str.equals(str2)) {
                    return true;
                }
            }
            return false;
        }
    }
}
