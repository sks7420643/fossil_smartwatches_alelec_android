package com.fossil;

import java.util.List;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ux2<E> extends Sx2<E> {
    @DexIgnore
    public /* final */ transient Sx2<E> d;

    @DexIgnore
    public Ux2(Sx2<E> sx2) {
        this.d = sx2;
    }

    @DexIgnore
    @Override // com.fossil.Sx2, com.fossil.Tx2
    public final boolean contains(@NullableDecl Object obj) {
        return this.d.contains(obj);
    }

    @DexIgnore
    @Override // java.util.List
    public final E get(int i) {
        Sw2.a(i, size());
        return this.d.get(zza(i));
    }

    @DexIgnore
    @Override // com.fossil.Sx2
    public final int indexOf(@NullableDecl Object obj) {
        int lastIndexOf = this.d.lastIndexOf(obj);
        if (lastIndexOf >= 0) {
            return zza(lastIndexOf);
        }
        return -1;
    }

    @DexIgnore
    @Override // com.fossil.Sx2
    public final int lastIndexOf(@NullableDecl Object obj) {
        int indexOf = this.d.indexOf(obj);
        if (indexOf >= 0) {
            return zza(indexOf);
        }
        return -1;
    }

    @DexIgnore
    public final int size() {
        return this.d.size();
    }

    @DexIgnore
    @Override // com.fossil.Sx2, java.util.List
    public final /* synthetic */ List subList(int i, int i2) {
        return zza(i, i2);
    }

    @DexIgnore
    public final int zza(int i) {
        return (size() - 1) - i;
    }

    @DexIgnore
    @Override // com.fossil.Sx2
    public final Sx2<E> zza(int i, int i2) {
        Sw2.e(i, i2, size());
        return ((Sx2) this.d.subList(size() - i2, size() - i)).zzd();
    }

    @DexIgnore
    @Override // com.fossil.Sx2
    public final Sx2<E> zzd() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final boolean zzh() {
        return this.d.zzh();
    }
}
