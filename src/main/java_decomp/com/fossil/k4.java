package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K4 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Ds b;
    @DexIgnore
    public /* final */ /* synthetic */ F5 c;
    @DexIgnore
    public /* final */ /* synthetic */ int d;

    @DexIgnore
    public K4(Ds ds, F5 f5, int i) {
        this.b = ds;
        this.c = f5;
        this.d = i;
    }

    @DexIgnore
    public final void run() {
        this.b.a(new P7(this.c, this.d));
    }
}
