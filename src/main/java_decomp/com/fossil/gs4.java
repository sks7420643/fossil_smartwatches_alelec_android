package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gs4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public Gs4(String str, Object obj, boolean z) {
        Wg6.c(str, "name");
        Wg6.c(obj, "data");
        this.a = str;
        this.b = obj;
        this.c = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Gs4(String str, Object obj, boolean z, int i, Qg6 qg6) {
        this(str, obj, (i & 4) != 0 ? false : z);
    }

    @DexIgnore
    public final Object a() {
        return this.b;
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final boolean c() {
        return this.c;
    }

    @DexIgnore
    public final void d(boolean z) {
        this.c = z;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Gs4) {
                Gs4 gs4 = (Gs4) obj;
                if (!Wg6.a(this.a, gs4.a) || !Wg6.a(this.b, gs4.b) || this.c != gs4.c) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        Object obj = this.b;
        if (obj != null) {
            i = obj.hashCode();
        }
        boolean z = this.c;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((hashCode * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "BottomDialogModel(name=" + this.a + ", data=" + this.b + ", isChecked=" + this.c + ")";
    }
}
