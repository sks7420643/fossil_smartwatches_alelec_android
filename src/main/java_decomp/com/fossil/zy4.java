package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zy4 {
    @DexIgnore
    public /* final */ int a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Ve5 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Ve5 ve5) {
            super(ve5.n());
            Wg6.c(ve5, "binding");
            this.a = ve5;
        }

        @DexIgnore
        public final void a(String str) {
            Wg6.c(str, "name");
            FlexibleTextView flexibleTextView = this.a.q;
            Wg6.b(flexibleTextView, "binding.ftvTitle");
            flexibleTextView.setText(str);
            String d = ThemeManager.l.a().d("primaryText");
            if (d != null) {
                this.a.q.setBackgroundColor(Color.parseColor(d));
            }
        }
    }

    @DexIgnore
    public Zy4(int i) {
        this.a = i;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public boolean b(List<? extends Object> list, int i) {
        Wg6.c(list, "items");
        return list.get(i) instanceof String;
    }

    @DexIgnore
    public void c(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        Object obj = null;
        Wg6.c(list, "items");
        Wg6.c(viewHolder, "holder");
        Ai ai = (Ai) (!(viewHolder instanceof Ai) ? null : viewHolder);
        Object obj2 = list.get(i);
        if (obj2 instanceof String) {
            obj = obj2;
        }
        String str = (String) obj;
        if (ai != null && str != null) {
            ai.a(str);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder d(ViewGroup viewGroup) {
        Wg6.c(viewGroup, "parent");
        Ve5 z = Ve5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemHeaderMemberBinding.\u2026(inflater, parent, false)");
        return new Ai(z);
    }
}
