package com.fossil;

import com.fossil.Be1;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ee1 implements Be1.Ai {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ Ai b;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        File a();
    }

    @DexIgnore
    public Ee1(Ai ai, long j) {
        this.a = j;
        this.b = ai;
    }

    @DexIgnore
    @Override // com.fossil.Be1.Ai
    public Be1 build() {
        File a2 = this.b.a();
        if (a2 == null) {
            return null;
        }
        if (a2.mkdirs() || (a2.exists() && a2.isDirectory())) {
            return Fe1.c(a2, this.a);
        }
        return null;
    }
}
