package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zt5 implements Factory<UpdateFirmwareUsecase> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<An4> b;

    @DexIgnore
    public Zt5(Provider<DeviceRepository> provider, Provider<An4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Zt5 a(Provider<DeviceRepository> provider, Provider<An4> provider2) {
        return new Zt5(provider, provider2);
    }

    @DexIgnore
    public static UpdateFirmwareUsecase c(DeviceRepository deviceRepository, An4 an4) {
        return new UpdateFirmwareUsecase(deviceRepository, an4);
    }

    @DexIgnore
    public UpdateFirmwareUsecase b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
