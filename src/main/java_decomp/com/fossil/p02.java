package com.fossil;

import com.fossil.U02;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P02 extends U02 {
    @DexIgnore
    public /* final */ Iterable<C02> a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends U02.Ai {
        @DexIgnore
        public Iterable<C02> a;
        @DexIgnore
        public byte[] b;

        @DexIgnore
        @Override // com.fossil.U02.Ai
        public U02 a() {
            String str = "";
            if (this.a == null) {
                str = " events";
            }
            if (str.isEmpty()) {
                return new P02(this.a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.U02.Ai
        public U02.Ai b(Iterable<C02> iterable) {
            if (iterable != null) {
                this.a = iterable;
                return this;
            }
            throw new NullPointerException("Null events");
        }

        @DexIgnore
        @Override // com.fossil.U02.Ai
        public U02.Ai c(byte[] bArr) {
            this.b = bArr;
            return this;
        }
    }

    @DexIgnore
    public P02(Iterable<C02> iterable, byte[] bArr) {
        this.a = iterable;
        this.b = bArr;
    }

    @DexIgnore
    @Override // com.fossil.U02
    public Iterable<C02> b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.U02
    public byte[] c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof U02)) {
            return false;
        }
        U02 u02 = (U02) obj;
        if (this.a.equals(u02.b())) {
            if (Arrays.equals(this.b, u02 instanceof P02 ? ((P02) u02).b : u02.c())) {
                z = true;
                return z;
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b);
    }

    @DexIgnore
    public String toString() {
        return "BackendRequest{events=" + this.a + ", extras=" + Arrays.toString(this.b) + "}";
    }
}
