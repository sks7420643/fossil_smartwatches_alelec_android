package com.fossil;

import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Lk6;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Nw7 extends Mw7 implements Tv7 {
    @DexIgnore
    public boolean c;

    @DexIgnore
    @Override // com.fossil.Tv7
    public Dw7 G(long j, Runnable runnable) {
        ScheduledFuture<?> V = this.c ? V(runnable, j, TimeUnit.MILLISECONDS) : null;
        return V != null ? new Cw7(V) : Pv7.i.G(j, runnable);
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public void M(Af6 af6, Runnable runnable) {
        Runnable runnable2;
        try {
            Executor S = S();
            Xx7 a2 = Yx7.a();
            if (a2 == null || (runnable2 = a2.b(runnable)) == null) {
                runnable2 = runnable;
            }
            S.execute(runnable2);
        } catch (RejectedExecutionException e) {
            Xx7 a3 = Yx7.a();
            if (a3 != null) {
                a3.d();
            }
            Pv7.i.z0(runnable);
        }
    }

    @DexIgnore
    public final void T() {
        this.c = Dz7.a(S());
    }

    @DexIgnore
    public final ScheduledFuture<?> V(Runnable runnable, long j, TimeUnit timeUnit) {
        try {
            Executor S = S();
            if (!(S instanceof ScheduledExecutorService)) {
                S = null;
            }
            ScheduledExecutorService scheduledExecutorService = (ScheduledExecutorService) S;
            if (scheduledExecutorService != null) {
                return scheduledExecutorService.schedule(runnable, j, timeUnit);
            }
            return null;
        } catch (RejectedExecutionException e) {
            return null;
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        Executor S = S();
        if (!(S instanceof ExecutorService)) {
            S = null;
        }
        ExecutorService executorService = (ExecutorService) S;
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof Nw7) && ((Nw7) obj).S() == S();
    }

    @DexIgnore
    @Override // com.fossil.Tv7
    public void f(long j, Lk6<? super Cd6> lk6) {
        ScheduledFuture<?> V = this.c ? V(new Qx7(this, lk6), j, TimeUnit.MILLISECONDS) : null;
        if (V != null) {
            Bx7.e(lk6, V);
        } else {
            Pv7.i.f(j, lk6);
        }
    }

    @DexIgnore
    public int hashCode() {
        return System.identityHashCode(S());
    }

    @DexIgnore
    @Override // com.fossil.Dv7
    public String toString() {
        return S().toString();
    }
}
