package com.fossil;

import com.fossil.Tj0;
import com.fossil.Uj0;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xj0 extends Uj0 {
    @DexIgnore
    public float k0; // = -1.0f;
    @DexIgnore
    public int l0; // = -1;
    @DexIgnore
    public int m0; // = -1;
    @DexIgnore
    public Tj0 n0; // = this.t;
    @DexIgnore
    public int o0; // = 0;
    @DexIgnore
    public boolean p0; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;

        /*
        static {
            int[] iArr = new int[Tj0.Di.values().length];
            a = iArr;
            try {
                iArr[Tj0.Di.LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Tj0.Di.RIGHT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[Tj0.Di.TOP.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[Tj0.Di.BOTTOM.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                a[Tj0.Di.BASELINE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                a[Tj0.Di.CENTER.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                a[Tj0.Di.CENTER_X.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                a[Tj0.Di.CENTER_Y.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                a[Tj0.Di.NONE.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
        }
        */
    }

    @DexIgnore
    public Xj0() {
        this.B.clear();
        this.B.add(this.n0);
        int length = this.A.length;
        for (int i = 0; i < length; i++) {
            this.A[i] = this.n0;
        }
    }

    @DexIgnore
    @Override // com.fossil.Uj0
    public void G0(Kj0 kj0) {
        if (u() != null) {
            int y = kj0.y(this.n0);
            if (this.o0 == 1) {
                C0(y);
                D0(0);
                b0(u().r());
                y0(0);
                return;
            }
            C0(0);
            D0(y);
            y0(u().D());
            b0(0);
        }
    }

    @DexIgnore
    public int I0() {
        return this.o0;
    }

    @DexIgnore
    public void J0(int i) {
        if (i > -1) {
            this.k0 = -1.0f;
            this.l0 = i;
            this.m0 = -1;
        }
    }

    @DexIgnore
    public void K0(int i) {
        if (i > -1) {
            this.k0 = -1.0f;
            this.l0 = -1;
            this.m0 = i;
        }
    }

    @DexIgnore
    public void L0(float f) {
        if (f > -1.0f) {
            this.k0 = f;
            this.l0 = -1;
            this.m0 = -1;
        }
    }

    @DexIgnore
    public void M0(int i) {
        if (this.o0 != i) {
            this.o0 = i;
            this.B.clear();
            if (this.o0 == 1) {
                this.n0 = this.s;
            } else {
                this.n0 = this.t;
            }
            this.B.add(this.n0);
            int length = this.A.length;
            for (int i2 = 0; i2 < length; i2++) {
                this.A[i2] = this.n0;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Uj0
    public void b(Kj0 kj0) {
        Tj0 tj0;
        boolean z = true;
        Vj0 vj0 = (Vj0) u();
        if (vj0 != null) {
            Tj0 h = vj0.h(Tj0.Di.LEFT);
            Tj0 h2 = vj0.h(Tj0.Di.RIGHT);
            Uj0 uj0 = this.D;
            boolean z2 = uj0 != null && uj0.C[0] == Uj0.Bi.WRAP_CONTENT;
            if (this.o0 == 0) {
                Tj0 h3 = vj0.h(Tj0.Di.TOP);
                tj0 = vj0.h(Tj0.Di.BOTTOM);
                Uj0 uj02 = this.D;
                if (uj02 == null || uj02.C[1] != Uj0.Bi.WRAP_CONTENT) {
                    z = false;
                    h = h3;
                } else {
                    h = h3;
                }
            } else {
                z = z2;
                tj0 = h2;
            }
            if (this.l0 != -1) {
                Oj0 r = kj0.r(this.n0);
                kj0.e(r, kj0.r(h), this.l0, 6);
                if (z) {
                    kj0.i(kj0.r(tj0), r, 0, 5);
                }
            } else if (this.m0 != -1) {
                Oj0 r2 = kj0.r(this.n0);
                Oj0 r3 = kj0.r(tj0);
                kj0.e(r2, r3, -this.m0, 6);
                if (z) {
                    kj0.i(r2, kj0.r(h), 0, 5);
                    kj0.i(r3, r2, 0, 5);
                }
            } else if (this.k0 != -1.0f) {
                kj0.d(Kj0.t(kj0, kj0.r(this.n0), kj0.r(h), kj0.r(tj0), this.k0, this.p0));
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Uj0
    public boolean c() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Uj0
    public void d(int i) {
        Uj0 u = u();
        if (u != null) {
            if (I0() == 1) {
                this.t.f().h(1, u.t.f(), 0);
                this.v.f().h(1, u.t.f(), 0);
                if (this.l0 != -1) {
                    this.s.f().h(1, u.s.f(), this.l0);
                    this.u.f().h(1, u.s.f(), this.l0);
                } else if (this.m0 != -1) {
                    this.s.f().h(1, u.u.f(), -this.m0);
                    this.u.f().h(1, u.u.f(), -this.m0);
                } else if (this.k0 != -1.0f && u.s() == Uj0.Bi.FIXED) {
                    int i2 = (int) (((float) u.E) * this.k0);
                    this.s.f().h(1, u.s.f(), i2);
                    this.u.f().h(1, u.s.f(), i2);
                }
            } else {
                this.s.f().h(1, u.s.f(), 0);
                this.u.f().h(1, u.s.f(), 0);
                if (this.l0 != -1) {
                    this.t.f().h(1, u.t.f(), this.l0);
                    this.v.f().h(1, u.t.f(), this.l0);
                } else if (this.m0 != -1) {
                    this.t.f().h(1, u.v.f(), -this.m0);
                    this.v.f().h(1, u.v.f(), -this.m0);
                } else if (this.k0 != -1.0f && u.B() == Uj0.Bi.FIXED) {
                    int i3 = (int) (((float) u.F) * this.k0);
                    this.t.f().h(1, u.t.f(), i3);
                    this.v.f().h(1, u.t.f(), i3);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Uj0
    public Tj0 h(Tj0.Di di) {
        switch (Ai.a[di.ordinal()]) {
            case 1:
            case 2:
                if (this.o0 == 1) {
                    return this.n0;
                }
                break;
            case 3:
            case 4:
                if (this.o0 == 0) {
                    return this.n0;
                }
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                return null;
        }
        throw new AssertionError(di.name());
    }

    @DexIgnore
    @Override // com.fossil.Uj0
    public ArrayList<Tj0> i() {
        return this.B;
    }
}
