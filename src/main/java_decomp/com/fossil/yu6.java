package com.fossil;

import com.portfolio.platform.ui.BaseActivity;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yu6 implements Factory<BaseActivity> {
    @DexIgnore
    public static BaseActivity a(Xu6 xu6) {
        BaseActivity a2 = xu6.a();
        Lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
