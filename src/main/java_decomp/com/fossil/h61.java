package com.fossil;

import android.net.Uri;
import com.facebook.internal.Utility;
import com.fossil.A18;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H61 extends G61<Uri> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public H61(A18.Ai ai) {
        super(ai);
        Wg6.c(ai, "callFactory");
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E61, com.fossil.G61
    public /* bridge */ /* synthetic */ boolean a(Uri uri) {
        return g(uri);
    }

    @DexIgnore
    @Override // com.fossil.E61
    public /* bridge */ /* synthetic */ String b(Object obj) {
        return h((Uri) obj);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.G61
    public /* bridge */ /* synthetic */ Q18 f(Uri uri) {
        return i(uri);
    }

    @DexIgnore
    public boolean g(Uri uri) {
        Wg6.c(uri, "data");
        return Wg6.a(uri.getScheme(), "http") || Wg6.a(uri.getScheme(), Utility.URL_SCHEME);
    }

    @DexIgnore
    public String h(Uri uri) {
        Wg6.c(uri, "data");
        String uri2 = uri.toString();
        Wg6.b(uri2, "data.toString()");
        return uri2;
    }

    @DexIgnore
    public Q18 i(Uri uri) {
        Wg6.c(uri, "$this$toHttpUrl");
        Q18 l = Q18.l(uri.toString());
        Wg6.b(l, "HttpUrl.get(toString())");
        return l;
    }
}
