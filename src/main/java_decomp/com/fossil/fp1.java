package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.H70;
import com.mapped.J70;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fp1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ H70 c;
    @DexIgnore
    public /* final */ float d;
    @DexIgnore
    public /* final */ J70 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Fp1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Fp1 createFromParcel(Parcel parcel) {
            long readLong = parcel.readLong();
            String readString = parcel.readString();
            if (readString != null) {
                Wg6.b(readString, "parcel.readString()!!");
                H70 valueOf = H70.valueOf(readString);
                float readFloat = parcel.readFloat();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    Wg6.b(readString2, "parcel.readString()!!");
                    return new Fp1(readLong, valueOf, readFloat, J70.valueOf(readString2));
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Fp1[] newArray(int i) {
            return new Fp1[i];
        }
    }

    @DexIgnore
    public Fp1(long j, H70 h70, float f, J70 j70) {
        this.b = j;
        this.c = h70;
        this.d = Hy1.e(f, 2);
        this.e = j70;
    }

    @DexIgnore
    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("alive", this.b);
            jSONObject.put(Constants.PROFILE_KEY_UNIT, Ey1.a(this.c));
            jSONObject.put("temp", Float.valueOf(this.d));
            jSONObject.put("cond_id", this.e.a());
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Fp1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Fp1 fp1 = (Fp1) obj;
            if (this.b != fp1.b) {
                return false;
            }
            if (this.c != fp1.c) {
                return false;
            }
            if (this.d != fp1.d) {
                return false;
            }
            return this.e == fp1.e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.CompactWeatherInfo");
    }

    @DexIgnore
    public final long getExpiredTimeStampInSecond() {
        return this.b;
    }

    @DexIgnore
    public final float getTemperature() {
        return this.d;
    }

    @DexIgnore
    public final H70 getTemperatureUnit() {
        return this.c;
    }

    @DexIgnore
    public final J70 getWeatherCondition() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        return (((((((int) this.b) * 31) + hashCode) * 31) + Float.valueOf(this.d).hashCode()) * 31) + this.e.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(G80.k(G80.k(new JSONObject(), Jd0.M2, Long.valueOf(this.b)), Jd0.u, Ey1.a(this.c)), Jd0.T1, Float.valueOf(this.d)), Jd0.t, Ey1.a(this.e));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeFloat(this.d);
        }
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
    }
}
