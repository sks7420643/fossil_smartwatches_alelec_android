package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.Z40;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.NoSuchElementException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class El1 extends Ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ O8 b;
    @DexIgnore
    public /* final */ Gl1[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<El1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* JADX DEBUG: Failed to insert an additional move for type inference into block B:62:0x004c */
        /* JADX DEBUG: Failed to insert an additional move for type inference into block B:64:0x005a */
        /* JADX DEBUG: Failed to insert an additional move for type inference into block B:65:0x0066 */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r6v8, types: [com.fossil.Jl1] */
        /* JADX WARN: Type inference failed for: r6v11, types: [com.fossil.Pl1] */
        /* JADX WARN: Type inference failed for: r6v12, types: [com.fossil.Ll1] */
        /* JADX WARNING: Unknown variable types count: 3 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final com.fossil.El1[] a(byte[] r14) throws java.lang.IllegalArgumentException {
            /*
            // Method dump skipped, instructions count: 236
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.El1.Ai.a(byte[]):com.fossil.El1[]");
        }

        @DexIgnore
        public El1 b(Parcel parcel) {
            Ql1 ql1;
            Il1 il1;
            O8 o8 = O8.values()[parcel.readInt()];
            Object[] createTypedArray = parcel.createTypedArray(Gl1.CREATOR);
            if (createTypedArray != null) {
                Wg6.b(createTypedArray, "parcel.createTypedArray(AlarmSubEntry.CREATOR)!!");
                Gl1[] gl1Arr = (Gl1[]) createTypedArray;
                for (Gl1 gl1 : gl1Arr) {
                    if (gl1 instanceof Hl1) {
                        if (gl1 != null) {
                            Hl1 hl1 = (Hl1) gl1;
                            int length = gl1Arr.length;
                            int i = 0;
                            while (true) {
                                if (i >= length) {
                                    ql1 = null;
                                    break;
                                }
                                ql1 = gl1Arr[i];
                                if (ql1 instanceof Ql1) {
                                    break;
                                }
                                i++;
                            }
                            Ql1 ql12 = ql1 != null ? ql1 : null;
                            int length2 = gl1Arr.length;
                            int i2 = 0;
                            while (true) {
                                if (i2 >= length2) {
                                    il1 = null;
                                    break;
                                }
                                Gl1 gl12 = gl1Arr[i2];
                                if (gl12 instanceof Il1) {
                                    il1 = gl12;
                                    break;
                                }
                                i2++;
                            }
                            Il1 il12 = il1 != null ? il1 : null;
                            int i3 = M8.b[o8.ordinal()];
                            if (i3 == 1) {
                                return hl1 instanceof Kl1 ? new Jl1((Kl1) hl1, ql12, il12) : new Nl1((Ol1) hl1, ql12, il12);
                            }
                            if (i3 == 2) {
                                return hl1 instanceof Kl1 ? new Ll1((Kl1) hl1, ql12, il12) : new Pl1((Ol1) hl1, ql12, il12);
                            }
                            throw new Kc6();
                        } else {
                            throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.FireTime");
                        }
                    }
                }
                throw new NoSuchElementException("Array contains no element matching the predicate.");
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ El1 createFromParcel(Parcel parcel) {
            return b(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public El1[] newArray(int i) {
            return new El1[i];
        }
    }

    @DexIgnore
    public El1(O8 o8, Gl1[] gl1Arr) {
        this.b = o8;
        this.c = gl1Arr;
    }

    @DexIgnore
    public final byte[] a() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (Gl1 gl1 : this.c) {
            byteArrayOutputStream.write(gl1.a());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        Wg6.b(byteArray, "byteArrayOutputStreamWriter.toByteArray()");
        byte[] array = ByteBuffer.allocate(byteArray.length + 3).order(ByteOrder.LITTLE_ENDIAN).put(this.b.b).putShort((short) byteArray.length).put(byteArray).array();
        Wg6.b(array, "ByteBuffer.allocate(ENTR\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final Gl1[] b() {
        return this.c;
    }

    @DexIgnore
    public final O8 c() {
        return this.b;
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Z40 z40 = (Z40) obj;
            return this.b == z40.c() && Cm7.c(this.c, z40.b());
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.Alarm");
    }

    @DexIgnore
    public Hl1 getFireTime() {
        Gl1[] gl1Arr = this.c;
        for (Gl1 gl1 : gl1Arr) {
            if (gl1 instanceof Ql1) {
                if (gl1 != null) {
                    return (Hl1) gl1;
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.FireTime");
                }
            }
        }
        throw new NoSuchElementException("Array contains no element matching the predicate.");
    }

    @DexIgnore
    public final Il1 getMessage() {
        Il1 il1;
        Gl1[] gl1Arr = this.c;
        int length = gl1Arr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                il1 = null;
                break;
            }
            il1 = gl1Arr[i];
            if (il1 instanceof Il1) {
                break;
            }
            i++;
        }
        return il1;
    }

    @DexIgnore
    public final Ql1 getTitle() {
        Ql1 ql1;
        Gl1[] gl1Arr = this.c;
        int length = gl1Arr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                ql1 = null;
                break;
            }
            ql1 = gl1Arr[i];
            if (ql1 instanceof Ql1) {
                break;
            }
            i++;
        }
        return ql1;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + Bm7.a(this.c);
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject k = G80.k(new JSONObject(), Jd0.e, Ey1.a(this.b));
        for (Gl1 gl1 : this.c) {
            Gy1.b(k, gl1.toJSONObject(), false, 2, null);
        }
        return k;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b.ordinal());
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.c, i);
        }
    }
}
