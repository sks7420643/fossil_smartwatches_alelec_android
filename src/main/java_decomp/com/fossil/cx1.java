package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import com.fossil.blesdk.adapter.BluetoothLeAdapter;
import com.fossil.common.cipher.TextEncryption;
import com.fossil.fitness.AlgorithmManager;
import com.fossil.fitness.DataCaching;
import com.mapped.H60;
import com.mapped.Kc6;
import com.mapped.Wg6;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cx1 {
    @DexIgnore
    public static long a; // = 1800000;
    @DexIgnore
    public static String b; // = "";
    @DexIgnore
    public static String c;
    @DexIgnore
    public static Ft1 d;
    @DexIgnore
    public static H60 e;
    @DexIgnore
    public static /* final */ Cx1 f; // = new Cx1();

    @DexIgnore
    public final H60 a() {
        return e;
    }

    @DexIgnore
    public final boolean b() {
        return Id0.i.a() != null;
    }

    @DexIgnore
    public final String c() {
        return c;
    }

    @DexIgnore
    public final long d(Lu1 lu1) {
        int i = Kd0.a[lu1.ordinal()];
        if (i == 1) {
            return D90.i.g();
        }
        if (i == 2) {
            return X80.i.g();
        }
        if (i == 3) {
            return Y80.i.g();
        }
        if (i == 4) {
            return W80.i.g();
        }
        throw new Kc6();
    }

    @DexIgnore
    public final String e() {
        return b;
    }

    @DexIgnore
    public final long f() {
        return a;
    }

    @DexIgnore
    public final String g() {
        return null;
    }

    @DexIgnore
    public final String h() {
        return null;
    }

    @DexIgnore
    public final Ft1 i() {
        return d;
    }

    @DexIgnore
    public final String j() {
        return Id0.i.l();
    }

    @DexIgnore
    public final void k(Context context) throws IllegalArgumentException, IllegalStateException {
        if (BluetoothAdapter.getDefaultAdapter() == null) {
            throw new IllegalStateException("Bluetooth is not supported on this hardware platform.");
        } else if (context == context.getApplicationContext()) {
            System.loadLibrary("FitnessAlgorithm");
            System.loadLibrary("EllipticCurveCrypto");
            System.loadLibrary("EInkImageFilter");
            Id0.i.c(1);
            Id0.i.b(context);
            M80.c.a("SDKManager", "init: sdkVersion=%s", "5.13.5-production-release");
            TextEncryption.j.s(context);
            L80.d.f(context);
            BluetoothLeAdapter.k.f(context);
            File file = new File(context.getFilesDir(), "msl");
            file.mkdirs();
            if (file.exists()) {
                AlgorithmManager.defaultManager().setDataCaching(new DataCaching(file.getAbsolutePath(), new byte[0]));
            }
            AlgorithmManager defaultManager = AlgorithmManager.defaultManager();
            Wg6.b(defaultManager, "AlgorithmManager.defaultManager()");
            defaultManager.setDebugEnabled(false);
        } else {
            throw new IllegalArgumentException("Invalid application context.");
        }
    }

    @DexIgnore
    public final void l(String str) {
        c = str;
    }

    @DexIgnore
    public final void m(String str) {
        b = str;
    }

    @DexIgnore
    public final void n(Zw1 zw1) {
        Id0.i.f(zw1);
    }

    @DexIgnore
    public final void o(Ft1 ft1) {
        d = ft1;
    }

    @DexIgnore
    public final void p(String str) {
        Id0.i.d(str);
        C90.e.a().c = str;
    }
}
