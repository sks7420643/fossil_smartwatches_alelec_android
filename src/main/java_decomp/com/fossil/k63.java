package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K63 implements Xw2<J63> {
    @DexIgnore
    public static K63 c; // = new K63();
    @DexIgnore
    public /* final */ Xw2<J63> b;

    @DexIgnore
    public K63() {
        this(Ww2.b(new M63()));
    }

    @DexIgnore
    public K63(Xw2<J63> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((J63) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((J63) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ J63 zza() {
        return this.b.zza();
    }
}
