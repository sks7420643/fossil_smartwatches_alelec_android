package com.fossil;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class V04 extends S04 {
    @DexIgnore
    public /* final */ TextWatcher d; // = new Ai();
    @DexIgnore
    public /* final */ TextInputLayout.f e; // = new Bi();
    @DexIgnore
    public /* final */ TextInputLayout.g f; // = new Ci(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements TextWatcher {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            V04 v04 = V04.this;
            v04.c.setChecked(!v04.f());
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements TextInputLayout.f {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        @Override // com.google.android.material.textfield.TextInputLayout.f
        public void a(TextInputLayout textInputLayout) {
            EditText editText = textInputLayout.getEditText();
            textInputLayout.setEndIconVisible(true);
            V04 v04 = V04.this;
            v04.c.setChecked(!v04.f());
            editText.removeTextChangedListener(V04.this.d);
            editText.addTextChangedListener(V04.this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements TextInputLayout.g {
        @DexIgnore
        public Ci(V04 v04) {
        }

        @DexIgnore
        @Override // com.google.android.material.textfield.TextInputLayout.g
        public void a(TextInputLayout textInputLayout, int i) {
            EditText editText = textInputLayout.getEditText();
            if (editText != null && i == 1) {
                editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di implements View.OnClickListener {
        @DexIgnore
        public Di() {
        }

        @DexIgnore
        public void onClick(View view) {
            EditText editText = V04.this.a.getEditText();
            if (editText != null) {
                int selectionEnd = editText.getSelectionEnd();
                if (V04.this.f()) {
                    editText.setTransformationMethod(null);
                } else {
                    editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
                if (selectionEnd >= 0) {
                    editText.setSelection(selectionEnd);
                }
            }
        }
    }

    @DexIgnore
    public V04(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    @Override // com.fossil.S04
    public void a() {
        this.a.setEndIconDrawable(Gf0.d(this.b, Mw3.design_password_eye));
        TextInputLayout textInputLayout = this.a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(Rw3.password_toggle_content_description));
        this.a.setEndIconOnClickListener(new Di());
        this.a.c(this.e);
        this.a.d(this.f);
    }

    @DexIgnore
    public final boolean f() {
        EditText editText = this.a.getEditText();
        return editText != null && (editText.getTransformationMethod() instanceof PasswordTransformationMethod);
    }
}
