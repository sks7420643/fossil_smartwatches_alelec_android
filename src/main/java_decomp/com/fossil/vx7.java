package com.fossil;

import com.mapped.Af6;
import com.mapped.Coroutine;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Vx7<S> extends Af6.Bi {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static <S, R> R a(Vx7<S> vx7, R r, Coroutine<? super R, ? super Af6.Bi, ? extends R> coroutine) {
            return (R) Af6.Bi.Aii.a(vx7, r, coroutine);
        }

        @DexIgnore
        public static <S, E extends Af6.Bi> E b(Vx7<S> vx7, Af6.Ci<E> ci) {
            return (E) Af6.Bi.Aii.b(vx7, ci);
        }

        @DexIgnore
        public static <S> Af6 c(Vx7<S> vx7, Af6.Ci<?> ci) {
            return Af6.Bi.Aii.c(vx7, ci);
        }

        @DexIgnore
        public static <S> Af6 d(Vx7<S> vx7, Af6 af6) {
            return Af6.Bi.Aii.d(vx7, af6);
        }
    }

    @DexIgnore
    void B(Af6 af6, S s);

    @DexIgnore
    S F(Af6 af6);
}
