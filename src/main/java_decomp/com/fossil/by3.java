package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Wx3;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class By3 implements Wx3.Ci {
    @DexIgnore
    public static /* final */ Parcelable.Creator<By3> CREATOR; // = new Ai();
    @DexIgnore
    public /* final */ long b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<By3> {
        @DexIgnore
        public By3 a(Parcel parcel) {
            return new By3(parcel.readLong(), null);
        }

        @DexIgnore
        public By3[] b(int i) {
            return new By3[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ By3 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ By3[] newArray(int i) {
            return b(i);
        }
    }

    @DexIgnore
    public By3(long j) {
        this.b = j;
    }

    @DexIgnore
    public /* synthetic */ By3(long j, Ai ai) {
        this(j);
    }

    @DexIgnore
    public static By3 a(long j) {
        return new By3(j);
    }

    @DexIgnore
    @Override // com.fossil.Wx3.Ci
    public boolean S(long j) {
        return j >= this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof By3)) {
            return false;
        }
        return this.b == ((By3) obj).b;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Long.valueOf(this.b)});
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.b);
    }
}
