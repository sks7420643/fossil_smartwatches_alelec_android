package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L73 implements M73 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a;
    @DexIgnore
    public static /* final */ Xv2<Boolean> b;
    @DexIgnore
    public static /* final */ Xv2<Boolean> c;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        a = hw2.d("measurement.client.global_params.dev", false);
        b = hw2.d("measurement.service.global_params_in_payload", true);
        c = hw2.d("measurement.service.global_params", false);
        hw2.b("measurement.id.service.global_params", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.M73
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.M73
    public final boolean zzb() {
        return a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.M73
    public final boolean zzc() {
        return b.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.M73
    public final boolean zzd() {
        return c.o().booleanValue();
    }
}
