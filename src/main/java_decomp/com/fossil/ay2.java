package com.fossil;

import java.util.Iterator;
import java.util.Set;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ay2<E> extends Tx2<E> implements Set<E> {
    @DexIgnore
    @NullableDecl
    public transient Sx2<E> c;

    @DexIgnore
    public static int zza(int i) {
        int max = Math.max(i, 2);
        boolean z = true;
        if (max < 751619276) {
            int highestOneBit = Integer.highestOneBit(max - 1);
            while (true) {
                highestOneBit <<= 1;
                if (((double) highestOneBit) * 0.7d >= ((double) max)) {
                    return highestOneBit;
                }
            }
        } else {
            if (max >= 1073741824) {
                z = false;
            }
            Sw2.f(z, "collection too large");
            return 1073741824;
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: LoopRegionVisitor
        jadx.core.utils.exceptions.JadxOverflowException: LoopRegionVisitor.assignOnlyInLoop endless recursion
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    @DexIgnore
    public static <E> com.fossil.Ay2<E> zza(java.util.Collection<? extends E> r15) {
        /*
            r14 = 1
            r7 = 0
            boolean r0 = r15 instanceof com.fossil.Ay2
            if (r0 == 0) goto L_0x0014
            boolean r0 = r15 instanceof java.util.SortedSet
            if (r0 != 0) goto L_0x0014
            r0 = r15
            com.fossil.Ay2 r0 = (com.fossil.Ay2) r0
            boolean r1 = r0.zzh()
            if (r1 != 0) goto L_0x0014
        L_0x0013:
            return r0
        L_0x0014:
            java.lang.Object[] r1 = r15.toArray()
            int r5 = r1.length
            r6 = r5
        L_0x001a:
            if (r6 == 0) goto L_0x0084
            if (r6 == r14) goto L_0x007c
            int r9 = zza(r6)
            java.lang.Object[] r3 = new java.lang.Object[r9]
            int r4 = r9 + -1
            r5 = r7
            r2 = r7
            r8 = r7
        L_0x0029:
            if (r8 >= r6) goto L_0x0052
            r10 = r1[r8]
            com.fossil.Oy2.a(r10, r8)
            int r11 = r10.hashCode()
            int r0 = com.fossil.Qx2.a(r11)
        L_0x0038:
            r12 = r0 & r4
            r13 = r3[r12]
            if (r13 != 0) goto L_0x0049
            r1[r5] = r10
            r3[r12] = r10
            int r2 = r2 + r11
            int r5 = r5 + 1
        L_0x0045:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x0029
        L_0x0049:
            boolean r12 = r13.equals(r10)
            if (r12 != 0) goto L_0x0045
            int r0 = r0 + 1
            goto L_0x0038
        L_0x0052:
            r0 = 0
            java.util.Arrays.fill(r1, r5, r6, r0)
            if (r5 != r14) goto L_0x0060
            com.fossil.Xy2 r0 = new com.fossil.Xy2
            r1 = r1[r7]
            r0.<init>(r1, r2)
            goto L_0x0013
        L_0x0060:
            int r0 = zza(r5)
            int r6 = r9 / 2
            if (r0 >= r6) goto L_0x006a
            r6 = r5
            goto L_0x001a
        L_0x006a:
            int r0 = r1.length
            int r6 = r0 >> 1
            int r0 = r0 >> 2
            int r0 = r0 + r6
            if (r5 >= r0) goto L_0x0076
            java.lang.Object[] r1 = java.util.Arrays.copyOf(r1, r5)
        L_0x0076:
            com.fossil.Wy2 r0 = new com.fossil.Wy2
            r0.<init>(r1, r2, r3, r4, r5)
            goto L_0x0013
        L_0x007c:
            com.fossil.Xy2 r0 = new com.fossil.Xy2
            r1 = r1[r7]
            r0.<init>(r1)
            goto L_0x0013
        L_0x0084:
            com.fossil.Wy2<java.lang.Object> r0 = com.fossil.Wy2.zza
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ay2.zza(java.util.Collection):com.fossil.Ay2");
    }

    @DexIgnore
    public boolean equals(@NullableDecl Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ay2) || !zza() || !((Ay2) obj).zza() || hashCode() == obj.hashCode()) {
            return Yy2.b(this, obj);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return Yy2.a(this);
    }

    @DexIgnore
    @Override // com.fossil.Tx2, java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public /* synthetic */ Iterator iterator() {
        return zzb();
    }

    @DexIgnore
    public boolean zza() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public Sx2<E> zzc() {
        Sx2<E> sx2 = this.c;
        if (sx2 != null) {
            return sx2;
        }
        Sx2<E> zzd = zzd();
        this.c = zzd;
        return zzd;
    }

    @DexIgnore
    public Sx2<E> zzd() {
        return Sx2.zza(toArray());
    }
}
