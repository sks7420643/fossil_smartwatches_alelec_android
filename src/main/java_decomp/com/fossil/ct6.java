package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Bn6;
import com.mapped.AlertDialogFragment;
import com.mapped.Qg6;
import com.mapped.UserCustomizeThemeFragment;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewSleepDayChart;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeSleepChartViewModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ct6 extends BaseFragment implements X47, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static String l;
    @DexIgnore
    public static String m;
    @DexIgnore
    public static String s;
    @DexIgnore
    public static /* final */ Ai t; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public CustomizeSleepChartViewModel h;
    @DexIgnore
    public G37<B55> i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Ct6.l;
        }

        @DexIgnore
        public final String b() {
            return Ct6.s;
        }

        @DexIgnore
        public final String c() {
            return Ct6.m;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<CustomizeSleepChartViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ Ct6 a;

        @DexIgnore
        public Bi(Ct6 ct6) {
            this.a = ct6;
        }

        @DexIgnore
        public final void a(CustomizeSleepChartViewModel.Ai ai) {
            if (ai != null) {
                Integer a2 = ai.a();
                if (a2 != null) {
                    this.a.O6(a2.intValue());
                }
                Integer b = ai.b();
                if (b != null) {
                    this.a.R6(b.intValue());
                }
                Integer e = ai.e();
                if (e != null) {
                    this.a.Q6(e.intValue());
                }
                Integer f = ai.f();
                if (f != null) {
                    this.a.T6(f.intValue());
                }
                Integer c = ai.c();
                if (c != null) {
                    this.a.P6(c.intValue());
                }
                Integer d = ai.d();
                if (d != null) {
                    this.a.S6(d.intValue());
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(CustomizeSleepChartViewModel.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ct6 b;

        @DexIgnore
        public Ci(Ct6 ct6) {
            this.b = ct6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, 701);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ct6 b;

        @DexIgnore
        public Di(Ct6 ct6) {
            this.b = ct6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, 702);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ct6 b;

        @DexIgnore
        public Ei(Ct6 ct6) {
            this.b = ct6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, 703);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ct6 b;

        @DexIgnore
        public Fi(Ct6 ct6) {
            this.b = ct6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.p(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = Ct6.class.getSimpleName();
        Wg6.b(simpleName, "CustomizeSleepChartFragment::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.X47
    public void C3(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        Hr7 hr7 = Hr7.a;
        String format = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(16777215 & i3)}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        CustomizeSleepChartViewModel customizeSleepChartViewModel = this.h;
        if (customizeSleepChartViewModel != null) {
            customizeSleepChartViewModel.h(i2, Color.parseColor(format));
            switch (i2) {
                case 701:
                    l = format;
                    return;
                case 702:
                    m = format;
                    return;
                case 703:
                    s = format;
                    return;
                default:
                    return;
            }
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void N6() {
        OverviewSleepDayChart overviewSleepDayChart;
        G37<B55> g37 = this.i;
        if (g37 != null) {
            B55 a2 = g37.a();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BarChart.b(0, BarChart.e.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(8, BarChart.e.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(56, BarChart.e.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(104, BarChart.e.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(111, BarChart.e.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(117, BarChart.e.LOWEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(122, BarChart.e.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(200, BarChart.e.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(211, BarChart.e.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(229, BarChart.e.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(247, BarChart.e.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(273, BarChart.e.LOWEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(286, BarChart.e.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(305, BarChart.e.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(316, BarChart.e.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(325, BarChart.e.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(337, BarChart.e.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(410, BarChart.e.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(474, BarChart.e.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(486, BarChart.e.HIGHEST, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            arrayList.add(new BarChart.b(495, BarChart.e.DEFAULT, Action.Apps.IF, 1571157180, new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)));
            ArrayList c = Hm7.c(arrayList);
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(new BarChart.a(390, c, 0, false, 12, null));
            Bn6.Bi bi = new Bn6.Bi(new BarChart.c(390, 390, arrayList2), 0.00996016f, 0.6055777f, 0.38446212f, 5, 304, 193, 25200);
            if (a2 != null && (overviewSleepDayChart = a2.x) != null) {
                overviewSleepDayChart.r(bi.g());
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void O6(int i2) {
        G37<B55> g37 = this.i;
        if (g37 != null) {
            B55 a2 = g37.a();
            if (a2 != null) {
                a2.x.J("awakeSleep", i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void P6(int i2) {
        G37<B55> g37 = this.i;
        if (g37 != null) {
            B55 a2 = g37.a();
            if (a2 != null) {
                a2.x.J("deepSleep", i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void Q6(int i2) {
        G37<B55> g37 = this.i;
        if (g37 != null) {
            B55 a2 = g37.a();
            if (a2 != null) {
                a2.x.J("lightSleep", i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        FLogger.INSTANCE.getLocal().d(k, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363373) {
            CustomizeSleepChartViewModel customizeSleepChartViewModel = this.h;
            if (customizeSleepChartViewModel != null) {
                customizeSleepChartViewModel.f(UserCustomizeThemeFragment.m.a(), l, m, s);
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void R6(int i2) {
        G37<B55> g37 = this.i;
        if (g37 != null) {
            B55 a2 = g37.a();
            if (a2 != null) {
                a2.B.setBackgroundColor(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void S6(int i2) {
        G37<B55> g37 = this.i;
        if (g37 != null) {
            B55 a2 = g37.a();
            if (a2 != null) {
                a2.C.setBackgroundColor(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void T6(int i2) {
        G37<B55> g37 = this.i;
        if (g37 != null) {
            B55 a2 = g37.a();
            if (a2 != null) {
                a2.D.setBackgroundColor(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        B55 b55 = (B55) Aq0.f(LayoutInflater.from(getContext()), 2131558538, null, false, A6());
        PortfolioApp.get.instance().getIface().D0(new Et6()).a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(CustomizeSleepChartViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026artViewModel::class.java)");
            CustomizeSleepChartViewModel customizeSleepChartViewModel = (CustomizeSleepChartViewModel) a2;
            this.h = customizeSleepChartViewModel;
            if (customizeSleepChartViewModel != null) {
                customizeSleepChartViewModel.e().h(getViewLifecycleOwner(), new Bi(this));
                CustomizeSleepChartViewModel customizeSleepChartViewModel2 = this.h;
                if (customizeSleepChartViewModel2 != null) {
                    customizeSleepChartViewModel2.g();
                    this.i = new G37<>(this, b55);
                    N6();
                    Wg6.b(b55, "binding");
                    return b55.n();
                }
                Wg6.n("mViewModel");
                throw null;
            }
            Wg6.n("mViewModel");
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(k, "onDestroy");
        l = null;
        m = null;
        s = null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(k, "onResume");
        CustomizeSleepChartViewModel customizeSleepChartViewModel = this.h;
        if (customizeSleepChartViewModel != null) {
            customizeSleepChartViewModel.g();
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<B55> g37 = this.i;
        if (g37 != null) {
            B55 a2 = g37.a();
            if (a2 != null) {
                a2.u.setOnClickListener(new Ci(this));
                a2.w.setOnClickListener(new Di(this));
                a2.v.setOnClickListener(new Ei(this));
                a2.t.setOnClickListener(new Fi(this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.X47
    public void q3(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
