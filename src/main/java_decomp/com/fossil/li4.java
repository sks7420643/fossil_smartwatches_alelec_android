package com.fossil;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Li4 implements Runnable {
    @DexIgnore
    public static /* final */ Object g; // = new Object();
    @DexIgnore
    public static Boolean h;
    @DexIgnore
    public static Boolean i;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Rf4 c;
    @DexIgnore
    public /* final */ PowerManager.WakeLock d;
    @DexIgnore
    public /* final */ Ki4 e;
    @DexIgnore
    public /* final */ long f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends BroadcastReceiver {
        @DexIgnore
        public Li4 a;

        @DexIgnore
        public Ai(Li4 li4) {
            this.a = li4;
        }

        @DexIgnore
        public void a() {
            if (Li4.j()) {
                Log.d("FirebaseMessaging", "Connectivity change received registered");
            }
            Li4.this.b.registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            synchronized (this) {
                if (this.a != null) {
                    if (this.a.i()) {
                        if (Li4.j()) {
                            Log.d("FirebaseMessaging", "Connectivity changed. Starting background sync.");
                        }
                        this.a.e.l(this.a, 0);
                        context.unregisterReceiver(this);
                        this.a = null;
                    }
                }
            }
        }
    }

    @DexIgnore
    public Li4(Ki4 ki4, Context context, Rf4 rf4, long j) {
        this.e = ki4;
        this.b = context;
        this.f = j;
        this.c = rf4;
        this.d = ((PowerManager) context.getSystemService("power")).newWakeLock(1, "wake:com.google.firebase.messaging");
    }

    @DexIgnore
    public static String e(String str) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 142);
        sb.append("Missing Permission: ");
        sb.append(str);
        sb.append(". This permission should normally be included by the manifest merger, but may needed to be manually added to your manifest");
        return sb.toString();
    }

    @DexIgnore
    public static boolean f(Context context) {
        boolean booleanValue;
        synchronized (g) {
            Boolean valueOf = Boolean.valueOf(i == null ? g(context, "android.permission.ACCESS_NETWORK_STATE", i) : i.booleanValue());
            i = valueOf;
            booleanValue = valueOf.booleanValue();
        }
        return booleanValue;
    }

    @DexIgnore
    public static boolean g(Context context, String str, Boolean bool) {
        if (bool != null) {
            return bool.booleanValue();
        }
        boolean z = context.checkCallingOrSelfPermission(str) == 0;
        if (z || !Log.isLoggable("FirebaseMessaging", 3)) {
            return z;
        }
        Log.d("FirebaseMessaging", e(str));
        return z;
    }

    @DexIgnore
    public static boolean h(Context context) {
        boolean booleanValue;
        synchronized (g) {
            Boolean valueOf = Boolean.valueOf(h == null ? g(context, "android.permission.WAKE_LOCK", h) : h.booleanValue());
            h = valueOf;
            booleanValue = valueOf.booleanValue();
        }
        return booleanValue;
    }

    @DexIgnore
    public static boolean j() {
        return Log.isLoggable("FirebaseMessaging", 3) || (Build.VERSION.SDK_INT == 23 && Log.isLoggable("FirebaseMessaging", 3));
    }

    @DexIgnore
    public final boolean i() {
        boolean z;
        synchronized (this) {
            ConnectivityManager connectivityManager = (ConnectivityManager) this.b.getSystemService("connectivity");
            NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
            z = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }
        return z;
    }

    @DexIgnore
    @SuppressLint({"Wakelock"})
    public void run() {
        if (h(this.b)) {
            this.d.acquire(Nh4.a);
        }
        try {
            this.e.m(true);
            if (!this.c.g()) {
                this.e.m(false);
                if (h(this.b)) {
                    try {
                        this.d.release();
                    } catch (RuntimeException e2) {
                        Log.i("FirebaseMessaging", "TopicsSyncTask's wakelock was already released due to timeout.");
                    }
                }
            } else if (!f(this.b) || i()) {
                if (this.e.p()) {
                    this.e.m(false);
                } else {
                    this.e.q(this.f);
                }
                if (h(this.b)) {
                    try {
                        this.d.release();
                    } catch (RuntimeException e3) {
                        Log.i("FirebaseMessaging", "TopicsSyncTask's wakelock was already released due to timeout.");
                    }
                }
            } else {
                new Ai(this).a();
                if (h(this.b)) {
                    try {
                        this.d.release();
                    } catch (RuntimeException e4) {
                        Log.i("FirebaseMessaging", "TopicsSyncTask's wakelock was already released due to timeout.");
                    }
                }
            }
        } catch (IOException e5) {
            String valueOf = String.valueOf(e5.getMessage());
            Log.e("FirebaseMessaging", valueOf.length() != 0 ? "Failed to sync topics. Won't retry sync. ".concat(valueOf) : new String("Failed to sync topics. Won't retry sync. "));
            this.e.m(false);
            if (h(this.b)) {
                try {
                    this.d.release();
                } catch (RuntimeException e6) {
                    Log.i("FirebaseMessaging", "TopicsSyncTask's wakelock was already released due to timeout.");
                }
            }
        } catch (Throwable th) {
            if (h(this.b)) {
                try {
                    this.d.release();
                } catch (RuntimeException e7) {
                    Log.i("FirebaseMessaging", "TopicsSyncTask's wakelock was already released due to timeout.");
                }
            }
            throw th;
        }
    }
}
