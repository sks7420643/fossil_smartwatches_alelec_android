package com.fossil;

import com.mapped.Bh6;
import com.mapped.Ch6;
import com.mapped.Hi6;
import com.mapped.Ni6;
import com.mapped.Yg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Er7 {
    @DexIgnore
    public static /* final */ Fr7 a;

    /*
    static {
        Fr7 fr7;
        try {
            fr7 = (Fr7) Class.forName("kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            fr7 = null;
        }
        if (fr7 == null) {
            fr7 = new Fr7();
        }
        a = fr7;
    }
    */

    @DexIgnore
    public static Gs7 a(Nq7 nq7) {
        a.a(nq7);
        return nq7;
    }

    @DexIgnore
    public static Es7 b(Class cls) {
        return a.b(cls);
    }

    @DexIgnore
    public static Hi6 c(Class cls, String str) {
        return a.c(cls, str);
    }

    @DexIgnore
    public static Is7 d(Yg6 yg6) {
        a.d(yg6);
        return yg6;
    }

    @DexIgnore
    public static Js7 e(Sq7 sq7) {
        a.e(sq7);
        return sq7;
    }

    @DexIgnore
    public static Ls7 f(Bh6 bh6) {
        a.f(bh6);
        return bh6;
    }

    @DexIgnore
    public static Ni6 g(Ch6 ch6) {
        a.g(ch6);
        return ch6;
    }

    @DexIgnore
    public static String h(Mq7 mq7) {
        return a.h(mq7);
    }

    @DexIgnore
    public static String i(Qq7 qq7) {
        return a.i(qq7);
    }
}
