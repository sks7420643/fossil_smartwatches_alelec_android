package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.mapped.Jh6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.NumberPicker;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I36 extends U47 implements H36 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ Ai u; // = new Ai(null);
    @DexIgnore
    public /* final */ Zp0 k; // = new Sr4(this);
    @DexIgnore
    public G37<Va5> l;
    @DexIgnore
    public G36 m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return I36.t;
        }

        @DexIgnore
        public final I36 b() {
            return new I36();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ I36 a;
        @DexIgnore
        public /* final */ /* synthetic */ Va5 b;

        @DexIgnore
        public Bi(I36 i36, Va5 va5) {
            this.a = i36;
            this.b = va5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            G36 A6 = I36.A6(this.a);
            NumberPicker numberPicker2 = this.b.u;
            Wg6.b(numberPicker2, "binding.numberPicker");
            A6.o(numberPicker2.getValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ I36 b;

        @DexIgnore
        public Ci(I36 i36) {
            this.b = i36;
        }

        @DexIgnore
        public final void onClick(View view) {
            I36.A6(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ Va5 b;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 c;

        @DexIgnore
        public Di(Va5 va5, Jh6 jh6) {
            this.b = va5;
            this.c = jh6;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.b.q;
            Wg6.b(constraintLayout, "it.clRoot");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                ViewGroup.LayoutParams layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) ((CoordinatorLayout.e) layoutParams).f();
                    if (bottomSheetBehavior != null) {
                        bottomSheetBehavior.E(3);
                        Va5 va5 = this.b;
                        Wg6.b(va5, "it");
                        View n = va5.n();
                        Wg6.b(n, "it.root");
                        n.getViewTreeObserver().removeOnGlobalLayoutListener(this.c.element);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                throw new Rc6("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new Rc6("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = I36.class.getSimpleName();
        Wg6.b(simpleName, "RemindTimeFragment::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G36 A6(I36 i36) {
        G36 g36 = i36.m;
        if (g36 != null) {
            return g36;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void C6(Va5 va5) {
        ArrayList arrayList = new ArrayList();
        Ur7 l2 = Bs7.l(new Wr7(20, 120), 20);
        int a2 = l2.a();
        int b = l2.b();
        int c = l2.c();
        if (c < 0 ? a2 >= b : a2 <= b) {
            while (true) {
                String g = Ll5.g(a2);
                Wg6.b(g, "timeString");
                arrayList.add(g);
                if (a2 == b) {
                    break;
                }
                a2 += c;
            }
        }
        NumberPicker numberPicker = va5.u;
        Wg6.b(numberPicker, "binding.numberPicker");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = va5.u;
        Wg6.b(numberPicker2, "binding.numberPicker");
        numberPicker2.setMaxValue(6);
        NumberPicker numberPicker3 = va5.u;
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            numberPicker3.setDisplayedValues((String[]) array);
            va5.u.setOnValueChangedListener(new Bi(this, va5));
            return;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void D6(G36 g36) {
        Wg6.c(g36, "presenter");
        this.m = g36;
    }

    @DexIgnore
    @Override // com.fossil.H36
    public void E(int i) {
        NumberPicker numberPicker;
        G37<Va5> g37 = this.l;
        if (g37 != null) {
            Va5 a2 = g37.a();
            if (a2 != null && (numberPicker = a2.u) != null) {
                Wg6.b(numberPicker, "numberPicker");
                numberPicker.setValue(i / 20);
                G36 g36 = this.m;
                if (g36 != null) {
                    g36.o(numberPicker.getValue());
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(G36 g36) {
        D6(g36);
    }

    @DexIgnore
    @Override // com.fossil.H36
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Va5 va5 = (Va5) Aq0.f(layoutInflater, 2131558615, viewGroup, false, this.k);
        String d = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(d)) {
            va5.q.setBackgroundColor(Color.parseColor(d));
        }
        va5.r.setOnClickListener(new Ci(this));
        Wg6.b(va5, "binding");
        C6(va5);
        this.l = new G37<>(this, va5);
        return va5.n();
    }

    @DexIgnore
    @Override // com.fossil.U47, androidx.fragment.app.Fragment, com.fossil.Kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        G36 g36 = this.m;
        if (g36 != null) {
            g36.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        G36 g36 = this.m;
        if (g36 != null) {
            g36.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<Va5> g37 = this.l;
        if (g37 != null) {
            Va5 a2 = g37.a();
            if (a2 != null) {
                Jh6 jh6 = new Jh6();
                jh6.element = null;
                jh6.element = (T) new Di(a2, jh6);
                Wg6.b(a2, "it");
                View n = a2.n();
                Wg6.b(n, "it.root");
                n.getViewTreeObserver().addOnGlobalLayoutListener(jh6.element);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.U47
    public void z6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
