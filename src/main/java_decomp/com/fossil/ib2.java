package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ib2 extends Fragment implements O72 {
    @DexIgnore
    public static WeakHashMap<FragmentActivity, WeakReference<Ib2>> e; // = new WeakHashMap<>();
    @DexIgnore
    public Map<String, LifecycleCallback> b; // = new Zi0();
    @DexIgnore
    public int c; // = 0;
    @DexIgnore
    public Bundle d;

    @DexIgnore
    public static Ib2 w6(FragmentActivity fragmentActivity) {
        Ib2 ib2;
        WeakReference<Ib2> weakReference = e.get(fragmentActivity);
        if (weakReference == null || (ib2 = weakReference.get()) == null) {
            try {
                ib2 = (Ib2) fragmentActivity.getSupportFragmentManager().Z("SupportLifecycleFragmentImpl");
                if (ib2 == null || ib2.isRemoving()) {
                    ib2 = new Ib2();
                    Xq0 j = fragmentActivity.getSupportFragmentManager().j();
                    j.d(ib2, "SupportLifecycleFragmentImpl");
                    j.i();
                }
                e.put(fragmentActivity, new WeakReference<>(ib2));
            } catch (ClassCastException e2) {
                throw new IllegalStateException("Fragment with tag SupportLifecycleFragmentImpl is not a SupportLifecycleFragmentImpl", e2);
            }
        }
        return ib2;
    }

    @DexIgnore
    @Override // com.fossil.O72
    public final <T extends LifecycleCallback> T S2(String str, Class<T> cls) {
        return cls.cast(this.b.get(str));
    }

    @DexIgnore
    @Override // com.fossil.O72
    public final /* synthetic */ Activity X2() {
        return getActivity();
    }

    @DexIgnore
    @Override // com.fossil.O72
    public final void b1(String str, LifecycleCallback lifecycleCallback) {
        if (!this.b.containsKey(str)) {
            this.b.put(str, lifecycleCallback);
            if (this.c > 0) {
                new Xl2(Looper.getMainLooper()).post(new Jb2(this, lifecycleCallback, str));
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 59);
        sb.append("LifecycleCallback with tag ");
        sb.append(str);
        sb.append(" already added to this fragment.");
        throw new IllegalArgumentException(sb.toString());
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (LifecycleCallback lifecycleCallback : this.b.values()) {
            lifecycleCallback.a(str, fileDescriptor, printWriter, strArr);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (LifecycleCallback lifecycleCallback : this.b.values()) {
            lifecycleCallback.e(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.c = 1;
        this.d = bundle;
        for (Map.Entry<String, LifecycleCallback> entry : this.b.entrySet()) {
            entry.getValue().f(bundle != null ? bundle.getBundle(entry.getKey()) : null);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void onDestroy() {
        super.onDestroy();
        this.c = 5;
        for (LifecycleCallback lifecycleCallback : this.b.values()) {
            lifecycleCallback.g();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void onResume() {
        super.onResume();
        this.c = 3;
        for (LifecycleCallback lifecycleCallback : this.b.values()) {
            lifecycleCallback.h();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Map.Entry<String, LifecycleCallback> entry : this.b.entrySet()) {
                Bundle bundle2 = new Bundle();
                entry.getValue().i(bundle2);
                bundle.putBundle(entry.getKey(), bundle2);
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void onStart() {
        super.onStart();
        this.c = 2;
        for (LifecycleCallback lifecycleCallback : this.b.values()) {
            lifecycleCallback.j();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public final void onStop() {
        super.onStop();
        this.c = 4;
        for (LifecycleCallback lifecycleCallback : this.b.values()) {
            lifecycleCallback.k();
        }
    }
}
