package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.Hz3;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hx3 extends Drawable implements Hz3.Bi {
    @DexIgnore
    public static /* final */ int w; // = Sw3.Widget_MaterialComponents_Badge;
    @DexIgnore
    public static /* final */ int x; // = Jw3.badgeStyle;
    @DexIgnore
    public /* final */ WeakReference<Context> b;
    @DexIgnore
    public /* final */ C04 c; // = new C04();
    @DexIgnore
    public /* final */ Hz3 d;
    @DexIgnore
    public /* final */ Rect e; // = new Rect();
    @DexIgnore
    public /* final */ float f;
    @DexIgnore
    public /* final */ float g;
    @DexIgnore
    public /* final */ float h;
    @DexIgnore
    public /* final */ Ai i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float k;
    @DexIgnore
    public int l;
    @DexIgnore
    public float m;
    @DexIgnore
    public float s;
    @DexIgnore
    public float t;
    @DexIgnore
    public WeakReference<View> u;
    @DexIgnore
    public WeakReference<ViewGroup> v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.Creator<Ai> CREATOR; // = new Aii();
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d; // = 255;
        @DexIgnore
        public int e; // = -1;
        @DexIgnore
        public int f;
        @DexIgnore
        public CharSequence g;
        @DexIgnore
        public int h;
        @DexIgnore
        public int i;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements Parcelable.Creator<Ai> {
            @DexIgnore
            public Ai a(Parcel parcel) {
                return new Ai(parcel);
            }

            @DexIgnore
            public Ai[] b(int i) {
                return new Ai[i];
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // android.os.Parcelable.Creator
            public /* bridge */ /* synthetic */ Ai createFromParcel(Parcel parcel) {
                return a(parcel);
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object[]' to match base method */
            @Override // android.os.Parcelable.Creator
            public /* bridge */ /* synthetic */ Ai[] newArray(int i) {
                return b(i);
            }
        }

        @DexIgnore
        public Ai(Context context) {
            this.c = new Pz3(context, Sw3.TextAppearance_MaterialComponents_Badge).b.getDefaultColor();
            this.g = context.getString(Rw3.mtrl_badge_numberless_content_description);
            this.h = Qw3.mtrl_badge_content_description;
        }

        @DexIgnore
        public Ai(Parcel parcel) {
            this.b = parcel.readInt();
            this.c = parcel.readInt();
            this.d = parcel.readInt();
            this.e = parcel.readInt();
            this.f = parcel.readInt();
            this.g = parcel.readString();
            this.h = parcel.readInt();
            this.i = parcel.readInt();
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i2) {
            parcel.writeInt(this.b);
            parcel.writeInt(this.c);
            parcel.writeInt(this.d);
            parcel.writeInt(this.e);
            parcel.writeInt(this.f);
            parcel.writeString(this.g.toString());
            parcel.writeInt(this.h);
            parcel.writeInt(this.i);
        }
    }

    @DexIgnore
    public Hx3(Context context) {
        this.b = new WeakReference<>(context);
        Jz3.c(context);
        Resources resources = context.getResources();
        this.f = (float) resources.getDimensionPixelSize(Lw3.mtrl_badge_radius);
        this.h = (float) resources.getDimensionPixelSize(Lw3.mtrl_badge_long_text_horizontal_padding);
        this.g = (float) resources.getDimensionPixelSize(Lw3.mtrl_badge_with_text_radius);
        Hz3 hz3 = new Hz3(this);
        this.d = hz3;
        hz3.e().setTextAlign(Paint.Align.CENTER);
        this.i = new Ai(context);
        v(Sw3.TextAppearance_MaterialComponents_Badge);
    }

    @DexIgnore
    public static Hx3 c(Context context) {
        return d(context, null, x, w);
    }

    @DexIgnore
    public static Hx3 d(Context context, AttributeSet attributeSet, int i2, int i3) {
        Hx3 hx3 = new Hx3(context);
        hx3.m(context, attributeSet, i2, i3);
        return hx3;
    }

    @DexIgnore
    public static Hx3 e(Context context, Ai ai) {
        Hx3 hx3 = new Hx3(context);
        hx3.o(ai);
        return hx3;
    }

    @DexIgnore
    public static int n(Context context, TypedArray typedArray, int i2) {
        return Oz3.a(context, typedArray, i2).getDefaultColor();
    }

    @DexIgnore
    @Override // com.fossil.Hz3.Bi
    public void a() {
        invalidateSelf();
    }

    @DexIgnore
    public final void b(Context context, Rect rect, View view) {
        int i2 = this.i.i;
        if (i2 == 8388691 || i2 == 8388693) {
            this.k = (float) rect.bottom;
        } else {
            this.k = (float) rect.top;
        }
        if (j() <= 9) {
            float f2 = !l() ? this.f : this.g;
            this.m = f2;
            this.t = f2;
            this.s = f2;
        } else {
            float f3 = this.g;
            this.m = f3;
            this.t = f3;
            this.s = (this.d.f(g()) / 2.0f) + this.h;
        }
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(l() ? Lw3.mtrl_badge_text_horizontal_edge_offset : Lw3.mtrl_badge_horizontal_edge_offset);
        int i3 = this.i.i;
        if (i3 == 8388659 || i3 == 8388691) {
            this.j = Mo0.z(view) == 0 ? ((float) dimensionPixelSize) + (((float) rect.left) - this.s) : (((float) rect.right) + this.s) - ((float) dimensionPixelSize);
        } else {
            this.j = Mo0.z(view) == 0 ? (((float) rect.right) + this.s) - ((float) dimensionPixelSize) : ((float) dimensionPixelSize) + (((float) rect.left) - this.s);
        }
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        if (!getBounds().isEmpty() && getAlpha() != 0 && isVisible()) {
            this.c.draw(canvas);
            if (l()) {
                f(canvas);
            }
        }
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        Rect rect = new Rect();
        String g2 = g();
        this.d.e().getTextBounds(g2, 0, g2.length(), rect);
        canvas.drawText(g2, this.j, ((float) (rect.height() / 2)) + this.k, this.d.e());
    }

    @DexIgnore
    public final String g() {
        if (j() <= this.l) {
            return Integer.toString(j());
        }
        Context context = this.b.get();
        if (context == null) {
            return "";
        }
        return context.getString(Rw3.mtrl_exceed_max_badge_number_suffix, Integer.valueOf(this.l), G78.ANY_NON_NULL_MARKER);
    }

    @DexIgnore
    public int getAlpha() {
        return this.i.d;
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.e.height();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.e.width();
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public CharSequence h() {
        if (!isVisible()) {
            return null;
        }
        if (!l()) {
            return this.i.g;
        }
        if (this.i.h <= 0) {
            return null;
        }
        Context context = this.b.get();
        if (context == null) {
            return null;
        }
        return context.getResources().getQuantityString(this.i.h, j(), Integer.valueOf(j()));
    }

    @DexIgnore
    public int i() {
        return this.i.f;
    }

    @DexIgnore
    public boolean isStateful() {
        return false;
    }

    @DexIgnore
    public int j() {
        if (!l()) {
            return 0;
        }
        return this.i.e;
    }

    @DexIgnore
    public Ai k() {
        return this.i;
    }

    @DexIgnore
    public boolean l() {
        return this.i.e != -1;
    }

    @DexIgnore
    public final void m(Context context, AttributeSet attributeSet, int i2, int i3) {
        TypedArray k2 = Jz3.k(context, attributeSet, Tw3.Badge, i2, i3, new int[0]);
        s(k2.getInt(Tw3.Badge_maxCharacterCount, 4));
        if (k2.hasValue(Tw3.Badge_number)) {
            t(k2.getInt(Tw3.Badge_number, 0));
        }
        p(n(context, k2, Tw3.Badge_backgroundColor));
        if (k2.hasValue(Tw3.Badge_badgeTextColor)) {
            r(n(context, k2, Tw3.Badge_badgeTextColor));
        }
        q(k2.getInt(Tw3.Badge_badgeGravity, 8388661));
        k2.recycle();
    }

    @DexIgnore
    public final void o(Ai ai) {
        s(ai.f);
        if (ai.e != -1) {
            t(ai.e);
        }
        p(ai.b);
        r(ai.c);
        q(ai.i);
    }

    @DexIgnore
    @Override // com.fossil.Hz3.Bi
    public boolean onStateChange(int[] iArr) {
        return super.onStateChange(iArr);
    }

    @DexIgnore
    public void p(int i2) {
        this.i.b = i2;
        ColorStateList valueOf = ColorStateList.valueOf(i2);
        if (this.c.w() != valueOf) {
            this.c.V(valueOf);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void q(int i2) {
        if (this.i.i != i2) {
            this.i.i = i2;
            WeakReference<View> weakReference = this.u;
            if (weakReference != null && weakReference.get() != null) {
                View view = this.u.get();
                WeakReference<ViewGroup> weakReference2 = this.v;
                w(view, weakReference2 != null ? weakReference2.get() : null);
            }
        }
    }

    @DexIgnore
    public void r(int i2) {
        this.i.c = i2;
        if (this.d.e().getColor() != i2) {
            this.d.e().setColor(i2);
            invalidateSelf();
        }
    }

    @DexIgnore
    public void s(int i2) {
        if (this.i.f != i2) {
            this.i.f = i2;
            y();
            this.d.i(true);
            x();
            invalidateSelf();
        }
    }

    @DexIgnore
    public void setAlpha(int i2) {
        this.i.d = i2;
        this.d.e().setAlpha(i2);
        invalidateSelf();
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
    }

    @DexIgnore
    public void t(int i2) {
        int max = Math.max(0, i2);
        if (this.i.e != max) {
            this.i.e = max;
            this.d.i(true);
            x();
            invalidateSelf();
        }
    }

    @DexIgnore
    public final void u(Pz3 pz3) {
        Context context;
        if (this.d.d() != pz3 && (context = this.b.get()) != null) {
            this.d.h(pz3, context);
            x();
        }
    }

    @DexIgnore
    public final void v(int i2) {
        Context context = this.b.get();
        if (context != null) {
            u(new Pz3(context, i2));
        }
    }

    @DexIgnore
    public void w(View view, ViewGroup viewGroup) {
        this.u = new WeakReference<>(view);
        this.v = new WeakReference<>(viewGroup);
        x();
        invalidateSelf();
    }

    @DexIgnore
    public final void x() {
        Context context = this.b.get();
        WeakReference<View> weakReference = this.u;
        View view = weakReference != null ? weakReference.get() : null;
        if (context != null && view != null) {
            Rect rect = new Rect();
            rect.set(this.e);
            Rect rect2 = new Rect();
            view.getDrawingRect(rect2);
            WeakReference<ViewGroup> weakReference2 = this.v;
            ViewGroup viewGroup = weakReference2 != null ? weakReference2.get() : null;
            if (viewGroup != null || Ix3.a) {
                if (viewGroup == null) {
                    viewGroup = (ViewGroup) view.getParent();
                }
                viewGroup.offsetDescendantRectToMyCoords(view, rect2);
            }
            b(context, rect2, view);
            Ix3.f(this.e, this.j, this.k, this.s, this.t);
            this.c.T(this.m);
            if (!rect.equals(this.e)) {
                this.c.setBounds(this.e);
            }
        }
    }

    @DexIgnore
    public final void y() {
        this.l = ((int) Math.pow(10.0d, ((double) i()) - 1.0d)) - 1;
    }
}
