package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.mapped.PagingRequestHelper;
import com.mapped.Wg6;
import com.portfolio.platform.data.NetworkState;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gl5 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements PagingRequestHelper.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ MutableLiveData a;

        @DexIgnore
        public Ai(MutableLiveData mutableLiveData) {
            this.a = mutableLiveData;
        }

        @DexIgnore
        @Override // com.mapped.PagingRequestHelper.Ai
        public final void e(PagingRequestHelper.Gi gi) {
            Wg6.c(gi, "report");
            if (gi.c()) {
                this.a.l(NetworkState.Companion.getLOADING());
            } else if (gi.b()) {
                this.a.l(NetworkState.Companion.error(Gl5.c(gi)));
            } else {
                this.a.l(NetworkState.Companion.getLOADED());
            }
        }
    }

    @DexIgnore
    public static final LiveData<NetworkState> b(PagingRequestHelper pagingRequestHelper) {
        Wg6.c(pagingRequestHelper, "$this$createStatusLiveData");
        MutableLiveData mutableLiveData = new MutableLiveData();
        pagingRequestHelper.a(new Ai(mutableLiveData));
        return mutableLiveData;
    }

    @DexIgnore
    public static final String c(PagingRequestHelper.Gi gi) {
        PagingRequestHelper.Di[] values = PagingRequestHelper.Di.values();
        ArrayList arrayList = new ArrayList();
        for (PagingRequestHelper.Di di : values) {
            Throwable a2 = gi.a(di);
            String message = a2 != null ? a2.getMessage() : null;
            if (message != null) {
                arrayList.add(message);
            }
        }
        return arrayList.isEmpty() ^ true ? (String) Pm7.F(arrayList) : "";
    }
}
