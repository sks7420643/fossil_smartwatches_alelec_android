package com.fossil;

import android.graphics.Bitmap;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L51 implements J51 {
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ N51<Integer, Bitmap> b; // = new N51<>();
    @DexIgnore
    public /* final */ TreeMap<Integer, Integer> c; // = new TreeMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [('[' char), (r0v2 int), (']' char)] */
    @Override // com.fossil.J51
    public String a(int i, int i2, Bitmap.Config config) {
        Wg6.c(config, "config");
        int a2 = Y81.a.a(i, i2, config);
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        sb.append(a2);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    @Override // com.fossil.J51
    public void b(Bitmap bitmap) {
        Wg6.c(bitmap, "bitmap");
        int b2 = W81.b(bitmap);
        this.b.f(Integer.valueOf(b2), bitmap);
        Integer num = this.c.get(Integer.valueOf(b2));
        this.c.put(Integer.valueOf(b2), Integer.valueOf(num == null ? 1 : num.intValue() + 1));
    }

    @DexIgnore
    @Override // com.fossil.J51
    public Bitmap c(int i, int i2, Bitmap.Config config) {
        Wg6.c(config, "config");
        int f = f(Y81.a.a(i, i2, config));
        Bitmap a2 = this.b.a(Integer.valueOf(f));
        if (a2 != null) {
            e(f, a2);
            a2.reconfigure(i, i2, config);
        }
        return a2;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [('[' char), (r0v1 int), (']' char)] */
    @Override // com.fossil.J51
    public String d(Bitmap bitmap) {
        Wg6.c(bitmap, "bitmap");
        int b2 = W81.b(bitmap);
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        sb.append(b2);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    public final void e(int i, Bitmap bitmap) {
        Integer num = this.c.get(Integer.valueOf(i));
        if (num != null) {
            Wg6.b(num, "sortedSizes[size] ?: run\u2026, this: $this\")\n        }");
            int intValue = num.intValue();
            if (intValue == 1) {
                this.c.remove(Integer.valueOf(i));
            } else {
                this.c.put(Integer.valueOf(i), Integer.valueOf(intValue - 1));
            }
        } else {
            throw new NullPointerException("Tried to decrement empty size, size: " + i + ", removed: " + d(bitmap) + ", this: " + this);
        }
    }

    @DexIgnore
    public final int f(int i) {
        Integer ceilingKey = this.c.ceilingKey(Integer.valueOf(i));
        return (ceilingKey == null || ceilingKey.intValue() > i * 8) ? i : ceilingKey.intValue();
    }

    @DexIgnore
    @Override // com.fossil.J51
    public Bitmap removeLast() {
        Bitmap e = this.b.e();
        if (e != null) {
            e(W81.b(e), e);
        }
        return e;
    }

    @DexIgnore
    public String toString() {
        return "SizeStrategy: groupedMap=" + this.b + ", sortedSizes=(" + this.c + ')';
    }
}
