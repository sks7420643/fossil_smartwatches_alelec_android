package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewPropertyAnimator;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mh0 extends HorizontalScrollView implements AdapterView.OnItemSelectedListener {
    @DexIgnore
    public Runnable b;
    @DexIgnore
    public Ci c;
    @DexIgnore
    public LinearLayoutCompat d;
    @DexIgnore
    public Spinner e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public ViewPropertyAnimator k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ View b;

        @DexIgnore
        public Ai(View view) {
            this.b = view;
        }

        @DexIgnore
        public void run() {
            Mh0.this.smoothScrollTo(this.b.getLeft() - ((Mh0.this.getWidth() - this.b.getWidth()) / 2), 0);
            Mh0.this.b = null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends BaseAdapter {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public int getCount() {
            return Mh0.this.d.getChildCount();
        }

        @DexIgnore
        public Object getItem(int i) {
            return ((Di) Mh0.this.d.getChildAt(i)).b();
        }

        @DexIgnore
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                return Mh0.this.d((ActionBar.b) getItem(i), true);
            }
            ((Di) view).a((ActionBar.b) getItem(i));
            return view;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci implements View.OnClickListener {
        @DexIgnore
        public Ci() {
        }

        @DexIgnore
        public void onClick(View view) {
            ((Di) view).b().e();
            int childCount = Mh0.this.d.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = Mh0.this.d.getChildAt(i);
                childAt.setSelected(childAt == view);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends LinearLayout {
        @DexIgnore
        public /* final */ int[] b;
        @DexIgnore
        public ActionBar.b c;
        @DexIgnore
        public TextView d;
        @DexIgnore
        public ImageView e;
        @DexIgnore
        public View f;

        @DexIgnore
        public Di(Context context, ActionBar.b bVar, boolean z) {
            super(context, null, Le0.actionBarTabStyle);
            int[] iArr = {16842964};
            this.b = iArr;
            this.c = bVar;
            Th0 v = Th0.v(context, null, iArr, Le0.actionBarTabStyle, 0);
            if (v.s(0)) {
                setBackgroundDrawable(v.g(0));
            }
            v.w();
            if (z) {
                setGravity(8388627);
            }
            c();
        }

        @DexIgnore
        public void a(ActionBar.b bVar) {
            this.c = bVar;
            c();
        }

        @DexIgnore
        public ActionBar.b b() {
            return this.c;
        }

        @DexIgnore
        public void c() {
            ActionBar.b bVar = this.c;
            View b2 = bVar.b();
            if (b2 != null) {
                ViewParent parent = b2.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(b2);
                    }
                    addView(b2);
                }
                this.f = b2;
                TextView textView = this.d;
                if (textView != null) {
                    textView.setVisibility(8);
                }
                ImageView imageView = this.e;
                if (imageView != null) {
                    imageView.setVisibility(8);
                    this.e.setImageDrawable(null);
                    return;
                }
                return;
            }
            View view = this.f;
            if (view != null) {
                removeView(view);
                this.f = null;
            }
            Drawable c2 = bVar.c();
            CharSequence d2 = bVar.d();
            if (c2 != null) {
                if (this.e == null) {
                    AppCompatImageView appCompatImageView = new AppCompatImageView(getContext());
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams.gravity = 16;
                    appCompatImageView.setLayoutParams(layoutParams);
                    addView(appCompatImageView, 0);
                    this.e = appCompatImageView;
                }
                this.e.setImageDrawable(c2);
                this.e.setVisibility(0);
            } else {
                ImageView imageView2 = this.e;
                if (imageView2 != null) {
                    imageView2.setVisibility(8);
                    this.e.setImageDrawable(null);
                }
            }
            boolean z = !TextUtils.isEmpty(d2);
            if (z) {
                if (this.d == null) {
                    AppCompatTextView appCompatTextView = new AppCompatTextView(getContext(), null, Le0.actionBarTabTextStyle);
                    appCompatTextView.setEllipsize(TextUtils.TruncateAt.END);
                    LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
                    layoutParams2.gravity = 16;
                    appCompatTextView.setLayoutParams(layoutParams2);
                    addView(appCompatTextView);
                    this.d = appCompatTextView;
                }
                this.d.setText(d2);
                this.d.setVisibility(0);
            } else {
                TextView textView2 = this.d;
                if (textView2 != null) {
                    textView2.setVisibility(8);
                    this.d.setText((CharSequence) null);
                }
            }
            ImageView imageView3 = this.e;
            if (imageView3 != null) {
                imageView3.setContentDescription(bVar.a());
            }
            Vh0.a(this, z ? null : bVar.a());
        }

        @DexIgnore
        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName("androidx.appcompat.app.ActionBar$Tab");
        }

        @DexIgnore
        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            accessibilityNodeInfo.setClassName("androidx.appcompat.app.ActionBar$Tab");
        }

        @DexIgnore
        public void onMeasure(int i, int i2) {
            int i3;
            super.onMeasure(i, i2);
            if (Mh0.this.g > 0 && getMeasuredWidth() > (i3 = Mh0.this.g)) {
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(i3, 1073741824), i2);
            }
        }

        @DexIgnore
        public void setSelected(boolean z) {
            boolean z2 = isSelected() != z;
            super.setSelected(z);
            if (z2 && z) {
                sendAccessibilityEvent(4);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei extends AnimatorListenerAdapter {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public int b;

        @DexIgnore
        public Ei() {
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            this.a = true;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            if (!this.a) {
                Mh0 mh0 = Mh0.this;
                mh0.k = null;
                mh0.setVisibility(this.b);
            }
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            Mh0.this.setVisibility(0);
            this.a = false;
        }
    }

    /*
    static {
        new DecelerateInterpolator();
    }
    */

    @DexIgnore
    public Mh0(Context context) {
        super(context);
        new Ei();
        setHorizontalScrollBarEnabled(false);
        Of0 b2 = Of0.b(context);
        setContentHeight(b2.f());
        this.h = b2.e();
        LinearLayoutCompat c2 = c();
        this.d = c2;
        addView(c2, new ViewGroup.LayoutParams(-2, -1));
    }

    @DexIgnore
    public void a(int i2) {
        View childAt = this.d.getChildAt(i2);
        Runnable runnable = this.b;
        if (runnable != null) {
            removeCallbacks(runnable);
        }
        Ai ai = new Ai(childAt);
        this.b = ai;
        post(ai);
    }

    @DexIgnore
    public final Spinner b() {
        AppCompatSpinner appCompatSpinner = new AppCompatSpinner(getContext(), null, Le0.actionDropDownStyle);
        appCompatSpinner.setLayoutParams(new LinearLayoutCompat.LayoutParams(-2, -1));
        appCompatSpinner.setOnItemSelectedListener(this);
        return appCompatSpinner;
    }

    @DexIgnore
    public final LinearLayoutCompat c() {
        LinearLayoutCompat linearLayoutCompat = new LinearLayoutCompat(getContext(), null, Le0.actionBarTabBarStyle);
        linearLayoutCompat.setMeasureWithLargestChildEnabled(true);
        linearLayoutCompat.setGravity(17);
        linearLayoutCompat.setLayoutParams(new LinearLayoutCompat.LayoutParams(-2, -1));
        return linearLayoutCompat;
    }

    @DexIgnore
    public Di d(ActionBar.b bVar, boolean z) {
        Di di = new Di(getContext(), bVar, z);
        if (z) {
            di.setBackgroundDrawable(null);
            di.setLayoutParams(new AbsListView.LayoutParams(-1, this.i));
        } else {
            di.setFocusable(true);
            if (this.c == null) {
                this.c = new Ci();
            }
            di.setOnClickListener(this.c);
        }
        return di;
    }

    @DexIgnore
    public final boolean e() {
        Spinner spinner = this.e;
        return spinner != null && spinner.getParent() == this;
    }

    @DexIgnore
    public final void f() {
        if (!e()) {
            if (this.e == null) {
                this.e = b();
            }
            removeView(this.d);
            addView(this.e, new ViewGroup.LayoutParams(-2, -1));
            if (this.e.getAdapter() == null) {
                this.e.setAdapter((SpinnerAdapter) new Bi());
            }
            Runnable runnable = this.b;
            if (runnable != null) {
                removeCallbacks(runnable);
                this.b = null;
            }
            this.e.setSelection(this.j);
        }
    }

    @DexIgnore
    public final boolean g() {
        if (e()) {
            removeView(this.e);
            addView(this.d, new ViewGroup.LayoutParams(-2, -1));
            setTabSelected(this.e.getSelectedItemPosition());
        }
        return false;
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Runnable runnable = this.b;
        if (runnable != null) {
            post(runnable);
        }
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        Of0 b2 = Of0.b(getContext());
        setContentHeight(b2.f());
        this.h = b2.e();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Runnable runnable = this.b;
        if (runnable != null) {
            removeCallbacks(runnable);
        }
    }

    @DexIgnore
    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onItemSelected(AdapterView<?> adapterView, View view, int i2, long j2) {
        ((Di) view).b().e();
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        boolean z = true;
        int mode = View.MeasureSpec.getMode(i2);
        boolean z2 = mode == 1073741824;
        setFillViewport(z2);
        int childCount = this.d.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.g = -1;
        } else {
            if (childCount > 2) {
                this.g = (int) (((float) View.MeasureSpec.getSize(i2)) * 0.4f);
            } else {
                this.g = View.MeasureSpec.getSize(i2) / 2;
            }
            this.g = Math.min(this.g, this.h);
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.i, 1073741824);
        if (z2 || !this.f) {
            z = false;
        }
        if (z) {
            this.d.measure(0, makeMeasureSpec);
            if (this.d.getMeasuredWidth() > View.MeasureSpec.getSize(i2)) {
                f();
            } else {
                g();
            }
        } else {
            g();
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i2, makeMeasureSpec);
        int measuredWidth2 = getMeasuredWidth();
        if (z2 && measuredWidth != measuredWidth2) {
            setTabSelected(this.j);
        }
    }

    @DexIgnore
    @Override // android.widget.AdapterView.OnItemSelectedListener
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    @DexIgnore
    public void setAllowCollapse(boolean z) {
        this.f = z;
    }

    @DexIgnore
    public void setContentHeight(int i2) {
        this.i = i2;
        requestLayout();
    }

    @DexIgnore
    public void setTabSelected(int i2) {
        this.j = i2;
        int childCount = this.d.getChildCount();
        int i3 = 0;
        while (i3 < childCount) {
            View childAt = this.d.getChildAt(i3);
            boolean z = i3 == i2;
            childAt.setSelected(z);
            if (z) {
                a(i2);
            }
            i3++;
        }
        Spinner spinner = this.e;
        if (spinner != null && i2 >= 0) {
            spinner.setSelection(i2);
        }
    }
}
