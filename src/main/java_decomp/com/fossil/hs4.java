package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Hs4 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    float getAnimationDuration();

    @DexIgnore
    Ds4 getAnimationState();

    @DexIgnore
    float getCurrentValue();

    @DexIgnore
    int getFrameDelay();

    @DexIgnore
    float getFrom();

    @DexIgnore
    float getTo();

    @DexIgnore
    void setAnimationState(Ds4 ds4);

    @DexIgnore
    void setCurrentValue(float f);

    @DexIgnore
    void setFrom(float f);

    @DexIgnore
    void setTo(float f);
}
