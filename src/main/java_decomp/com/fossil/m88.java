package com.fossil;

import com.fossil.S18;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Map;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class M88<T> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends M88<Iterable<T>> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.M88
        public /* bridge */ /* synthetic */ void a(O88 o88, Object obj) throws IOException {
            d(o88, (Iterable) obj);
        }

        @DexIgnore
        public void d(O88 o88, Iterable<T> iterable) throws IOException {
            if (iterable != null) {
                for (T t : iterable) {
                    M88.this.a(o88, t);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends M88<Object> {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.M88 */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.M88
        public void a(O88 o88, Object obj) throws IOException {
            if (obj != null) {
                int length = Array.getLength(obj);
                for (int i = 0; i < length; i++) {
                    M88.this.a(o88, Array.get(obj, i));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> extends M88<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ E88<T, RequestBody> c;

        @DexIgnore
        public Ci(Method method, int i, E88<T, RequestBody> e88) {
            this.a = method;
            this.b = i;
            this.c = e88;
        }

        @DexIgnore
        @Override // com.fossil.M88
        public void a(O88 o88, T t) {
            if (t != null) {
                try {
                    o88.j(this.c.a(t));
                } catch (IOException e) {
                    Method method = this.a;
                    int i = this.b;
                    throw U88.q(method, e, i, "Unable to convert " + ((Object) t) + " to RequestBody", new Object[0]);
                }
            } else {
                throw U88.p(this.a, this.b, "Body parameter value must not be null.", new Object[0]);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> extends M88<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ E88<T, String> b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public Di(String str, E88<T, String> e88, boolean z) {
            U88.b(str, "name == null");
            this.a = str;
            this.b = e88;
            this.c = z;
        }

        @DexIgnore
        @Override // com.fossil.M88
        public void a(O88 o88, T t) throws IOException {
            String a2;
            if (t != null && (a2 = this.b.a(t)) != null) {
                o88.a(this.a, a2, this.c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> extends M88<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ E88<T, String> c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public Ei(Method method, int i, E88<T, String> e88, boolean z) {
            this.a = method;
            this.b = i;
            this.c = e88;
            this.d = z;
        }

        @DexIgnore
        @Override // com.fossil.M88
        public /* bridge */ /* synthetic */ void a(O88 o88, Object obj) throws IOException {
            d(o88, (Map) obj);
        }

        @DexIgnore
        public void d(O88 o88, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry<String, T> entry : map.entrySet()) {
                    String key = entry.getKey();
                    if (key != null) {
                        T value = entry.getValue();
                        if (value != null) {
                            String a2 = this.c.a(value);
                            if (a2 != null) {
                                o88.a(key, a2, this.d);
                            } else {
                                Method method = this.a;
                                int i = this.b;
                                throw U88.p(method, i, "Field map value '" + ((Object) value) + "' converted to null by " + this.c.getClass().getName() + " for key '" + key + "'.", new Object[0]);
                            }
                        } else {
                            Method method2 = this.a;
                            int i2 = this.b;
                            throw U88.p(method2, i2, "Field map contained null value for key '" + key + "'.", new Object[0]);
                        }
                    } else {
                        throw U88.p(this.a, this.b, "Field map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw U88.p(this.a, this.b, "Field map was null.", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> extends M88<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ E88<T, String> b;

        @DexIgnore
        public Fi(String str, E88<T, String> e88) {
            U88.b(str, "name == null");
            this.a = str;
            this.b = e88;
        }

        @DexIgnore
        @Override // com.fossil.M88
        public void a(O88 o88, T t) throws IOException {
            String a2;
            if (t != null && (a2 = this.b.a(t)) != null) {
                o88.b(this.a, a2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi<T> extends M88<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ P18 c;
        @DexIgnore
        public /* final */ E88<T, RequestBody> d;

        @DexIgnore
        public Gi(Method method, int i, P18 p18, E88<T, RequestBody> e88) {
            this.a = method;
            this.b = i;
            this.c = p18;
            this.d = e88;
        }

        @DexIgnore
        @Override // com.fossil.M88
        public void a(O88 o88, T t) {
            if (t != null) {
                try {
                    o88.c(this.c, this.d.a(t));
                } catch (IOException e) {
                    Method method = this.a;
                    int i = this.b;
                    throw U88.p(method, i, "Unable to convert " + ((Object) t) + " to RequestBody", e);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<T> extends M88<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ E88<T, RequestBody> c;
        @DexIgnore
        public /* final */ String d;

        @DexIgnore
        public Hi(Method method, int i, E88<T, RequestBody> e88, String str) {
            this.a = method;
            this.b = i;
            this.c = e88;
            this.d = str;
        }

        @DexIgnore
        @Override // com.fossil.M88
        public /* bridge */ /* synthetic */ void a(O88 o88, Object obj) throws IOException {
            d(o88, (Map) obj);
        }

        @DexIgnore
        public void d(O88 o88, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry<String, T> entry : map.entrySet()) {
                    String key = entry.getKey();
                    if (key != null) {
                        T value = entry.getValue();
                        if (value != null) {
                            o88.c(P18.g("Content-Disposition", "form-data; name=\"" + key + "\"", "Content-Transfer-Encoding", this.d), this.c.a(value));
                        } else {
                            Method method = this.a;
                            int i = this.b;
                            throw U88.p(method, i, "Part map contained null value for key '" + key + "'.", new Object[0]);
                        }
                    } else {
                        throw U88.p(this.a, this.b, "Part map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw U88.p(this.a, this.b, "Part map was null.", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii<T> extends M88<T> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ E88<T, String> d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public Ii(Method method, int i, String str, E88<T, String> e88, boolean z) {
            this.a = method;
            this.b = i;
            U88.b(str, "name == null");
            this.c = str;
            this.d = e88;
            this.e = z;
        }

        @DexIgnore
        @Override // com.fossil.M88
        public void a(O88 o88, T t) throws IOException {
            if (t != null) {
                o88.e(this.c, this.d.a(t), this.e);
                return;
            }
            Method method = this.a;
            int i = this.b;
            throw U88.p(method, i, "Path parameter \"" + this.c + "\" value must not be null.", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji<T> extends M88<T> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ E88<T, String> b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public Ji(String str, E88<T, String> e88, boolean z) {
            U88.b(str, "name == null");
            this.a = str;
            this.b = e88;
            this.c = z;
        }

        @DexIgnore
        @Override // com.fossil.M88
        public void a(O88 o88, T t) throws IOException {
            String a2;
            if (t != null && (a2 = this.b.a(t)) != null) {
                o88.f(this.a, a2, this.c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki<T> extends M88<Map<String, T>> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ E88<T, String> c;
        @DexIgnore
        public /* final */ boolean d;

        @DexIgnore
        public Ki(Method method, int i, E88<T, String> e88, boolean z) {
            this.a = method;
            this.b = i;
            this.c = e88;
            this.d = z;
        }

        @DexIgnore
        @Override // com.fossil.M88
        public /* bridge */ /* synthetic */ void a(O88 o88, Object obj) throws IOException {
            d(o88, (Map) obj);
        }

        @DexIgnore
        public void d(O88 o88, Map<String, T> map) throws IOException {
            if (map != null) {
                for (Map.Entry<String, T> entry : map.entrySet()) {
                    String key = entry.getKey();
                    if (key != null) {
                        T value = entry.getValue();
                        if (value != null) {
                            String a2 = this.c.a(value);
                            if (a2 != null) {
                                o88.f(key, a2, this.d);
                            } else {
                                Method method = this.a;
                                int i = this.b;
                                throw U88.p(method, i, "Query map value '" + ((Object) value) + "' converted to null by " + this.c.getClass().getName() + " for key '" + key + "'.", new Object[0]);
                            }
                        } else {
                            Method method2 = this.a;
                            int i2 = this.b;
                            throw U88.p(method2, i2, "Query map contained null value for key '" + key + "'.", new Object[0]);
                        }
                    } else {
                        throw U88.p(this.a, this.b, "Query map contained null key.", new Object[0]);
                    }
                }
                return;
            }
            throw U88.p(this.a, this.b, "Query map was null", new Object[0]);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li<T> extends M88<T> {
        @DexIgnore
        public /* final */ E88<T, String> a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public Li(E88<T, String> e88, boolean z) {
            this.a = e88;
            this.b = z;
        }

        @DexIgnore
        @Override // com.fossil.M88
        public void a(O88 o88, T t) throws IOException {
            if (t != null) {
                o88.f(this.a.a(t), null, this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi extends M88<S18.Bi> {
        @DexIgnore
        public static /* final */ Mi a; // = new Mi();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.O88, java.lang.Object] */
        @Override // com.fossil.M88
        public /* bridge */ /* synthetic */ void a(O88 o88, S18.Bi bi) throws IOException {
            d(o88, bi);
        }

        @DexIgnore
        public void d(O88 o88, S18.Bi bi) {
            if (bi != null) {
                o88.d(bi);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni extends M88<Object> {
        @DexIgnore
        public /* final */ Method a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Ni(Method method, int i) {
            this.a = method;
            this.b = i;
        }

        @DexIgnore
        @Override // com.fossil.M88
        public void a(O88 o88, Object obj) {
            if (obj != null) {
                o88.k(obj);
                return;
            }
            throw U88.p(this.a, this.b, "@Url parameter is null.", new Object[0]);
        }
    }

    @DexIgnore
    public abstract void a(O88 o88, T t) throws IOException;

    @DexIgnore
    public final M88<Object> b() {
        return new Bi();
    }

    @DexIgnore
    public final M88<Iterable<T>> c() {
        return new Ai();
    }
}
