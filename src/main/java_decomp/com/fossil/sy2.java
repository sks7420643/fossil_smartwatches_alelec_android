package com.fossil;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sy2<K, V> extends Ay2<Map.Entry<K, V>> {
    @DexIgnore
    public /* final */ transient Wx2<K, V> d;
    @DexIgnore
    public /* final */ transient Object[] e;
    @DexIgnore
    public /* final */ transient int f;

    @DexIgnore
    public Sy2(Wx2<K, V> wx2, Object[] objArr, int i, int i2) {
        this.d = wx2;
        this.e = objArr;
        this.f = i2;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final boolean contains(Object obj) {
        if (obj instanceof Map.Entry) {
            Map.Entry entry = (Map.Entry) obj;
            Object key = entry.getKey();
            Object value = entry.getValue();
            return value != null && value.equals(this.d.get(key));
        }
    }

    @DexIgnore
    @Override // com.fossil.Tx2, com.fossil.Ay2, java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public final /* synthetic */ Iterator iterator() {
        return zzb();
    }

    @DexIgnore
    public final int size() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final int zzb(Object[] objArr, int i) {
        return zzc().zzb(objArr, i);
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final Cz2<Map.Entry<K, V>> zzb() {
        return (Cz2) zzc().iterator();
    }

    @DexIgnore
    @Override // com.fossil.Ay2
    public final Sx2<Map.Entry<K, V>> zzd() {
        return new Ry2(this);
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final boolean zzh() {
        return true;
    }
}
