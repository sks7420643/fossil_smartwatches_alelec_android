package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.Rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ds3 extends Y93 implements Cs3 {
    @DexIgnore
    public Ds3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.plus.internal.IPlusOneButtonCreator");
    }

    @DexIgnore
    @Override // com.fossil.Cs3
    public final Rg2 y(Rg2 rg2, int i, int i2, String str, int i3) throws RemoteException {
        Parcel d = d();
        Z93.a(d, rg2);
        d.writeInt(i);
        d.writeInt(i2);
        d.writeString(str);
        d.writeInt(i3);
        Parcel e = e(1, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }
}
