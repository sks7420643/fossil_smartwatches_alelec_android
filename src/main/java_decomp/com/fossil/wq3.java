package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Wq3 extends Jn3 implements Ln3 {
    @DexIgnore
    public /* final */ Yq3 b;

    @DexIgnore
    public Wq3(Yq3 yq3) {
        super(yq3.f0());
        Rc2.k(yq3);
        this.b = yq3;
    }

    @DexIgnore
    public Gr3 n() {
        return this.b.Y();
    }

    @DexIgnore
    public Kg3 o() {
        return this.b.U();
    }

    @DexIgnore
    public Jm3 p() {
        return this.b.Q();
    }
}
