package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wp4 implements Factory<Uq4> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Wp4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Wp4 a(Uo4 uo4) {
        return new Wp4(uo4);
    }

    @DexIgnore
    public static Uq4 c(Uo4 uo4) {
        Uq4 D = uo4.D();
        Lk7.c(D, "Cannot return null from a non-@Nullable @Provides method");
        return D;
    }

    @DexIgnore
    public Uq4 b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
