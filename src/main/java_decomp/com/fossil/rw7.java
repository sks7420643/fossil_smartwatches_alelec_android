package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rw7 implements Sw7 {
    @DexIgnore
    public /* final */ Kx7 b;

    @DexIgnore
    public Rw7(Kx7 kx7) {
        this.b = kx7;
    }

    @DexIgnore
    @Override // com.fossil.Sw7
    public Kx7 b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Sw7
    public boolean isActive() {
        return false;
    }

    @DexIgnore
    public String toString() {
        return Nv7.c() ? b().x("New") : super.toString();
    }
}
