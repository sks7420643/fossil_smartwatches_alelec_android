package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yr2 extends Nq2 {
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest s;
    @DexIgnore
    public /* final */ /* synthetic */ Ga3 t;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Yr2(Xr2 xr2, R62 r62, LocationRequest locationRequest, Ga3 ga3) {
        super(r62);
        this.s = locationRequest;
        this.t = ga3;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.M62$Bi] */
    @Override // com.fossil.I72
    public final /* synthetic */ void u(Fr2 fr2) throws RemoteException {
        fr2.x0(this.s, Q72.a(this.t, Pr2.b(), Ga3.class.getSimpleName()), new Oq2(this));
    }
}
