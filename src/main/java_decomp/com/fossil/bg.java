package com.fossil;

import com.fossil.blesdk.device.logic.phase.InstallThemePackagePhase$onStart$Anon1;
import com.fossil.blesdk.device.logic.phase.InstallThemePackagePhase$onStart$Anon2;
import com.fossil.blesdk.model.uiframework.packages.theme.ThemeEditor;
import com.mapped.Rm6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bg extends Lp {
    @DexIgnore
    public Lw1 C;
    @DexIgnore
    public /* final */ Lw1 D;

    @DexIgnore
    public Bg(K5 k5, I60 i60, Lw1 lw1) {
        super(k5, i60, Yp.M0, null, false, 24);
        this.D = lw1;
        this.C = lw1;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        if (this.x.a().getUiPackageOSVersion().isCompatible(this.C.h(), true)) {
            Rm6 unused = Gu7.d(Id0.i.e(), null, null, new InstallThemePackagePhase$onStart$Anon1(this, null), 3, null);
            return;
        }
        ThemeEditor edit = this.C.edit();
        if (edit != null) {
            Rm6 unused2 = Gu7.d(Id0.i.e(), null, null, new InstallThemePackagePhase$onStart$Anon2(this, edit, null), 3, null);
        } else {
            l(Nr.a(this.v, null, Zq.t, null, null, 13));
        }
    }

    @DexIgnore
    public final void J() {
        Lp.h(this, new Wk(this.w, this.x, this.C.getBundleId(), null, 8), new Ze(this), new Nf(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        return this.C;
    }
}
