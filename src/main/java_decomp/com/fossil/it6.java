package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.mapped.AlertDialogFragment;
import com.mapped.Qg6;
import com.mapped.UserCustomizeThemeFragment;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeTextViewModel;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class It6 extends BaseFragment implements X47, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static String l;
    @DexIgnore
    public static String m;
    @DexIgnore
    public static /* final */ Ai s; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public CustomizeTextViewModel h;
    @DexIgnore
    public G37<D55> i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return It6.l;
        }

        @DexIgnore
        public final String b() {
            return It6.m;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<CustomizeTextViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ It6 a;

        @DexIgnore
        public Bi(It6 it6) {
            this.a = it6;
        }

        @DexIgnore
        public final void a(CustomizeTextViewModel.Ai ai) {
            if (ai != null) {
                Integer a2 = ai.a();
                if (a2 != null) {
                    this.a.N6(a2.intValue());
                }
                Integer c = ai.c();
                if (c != null) {
                    this.a.P6(c.intValue());
                }
                Integer b = ai.b();
                if (b != null) {
                    this.a.M6(b.intValue());
                }
                Integer d = ai.d();
                if (d != null) {
                    this.a.O6(d.intValue());
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(CustomizeTextViewModel.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ It6 b;

        @DexIgnore
        public Ci(It6 it6) {
            this.b = it6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, 101);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ It6 b;

        @DexIgnore
        public Di(It6 it6) {
            this.b = it6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.l(childFragmentManager, 102);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ It6 b;

        @DexIgnore
        public Ei(It6 it6) {
            this.b = it6;
        }

        @DexIgnore
        public final void onClick(View view) {
            S37 s37 = S37.c;
            FragmentManager childFragmentManager = this.b.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.p(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = It6.class.getSimpleName();
        Wg6.b(simpleName, "CustomizeTextFragment::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.X47
    public void C3(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        Hr7 hr7 = Hr7.a;
        String format = String.format("#%06X", Arrays.copyOf(new Object[]{Integer.valueOf(16777215 & i3)}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        CustomizeTextViewModel customizeTextViewModel = this.h;
        if (customizeTextViewModel != null) {
            customizeTextViewModel.h(i2, Color.parseColor(format));
            if (i2 == 101) {
                l = format;
            } else if (i2 == 102) {
                m = format;
            }
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void M6(int i2) {
        G37<D55> g37 = this.i;
        if (g37 != null) {
            D55 a2 = g37.a();
            if (a2 != null) {
                a2.v.setTextColor(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void N6(int i2) {
        G37<D55> g37 = this.i;
        if (g37 != null) {
            D55 a2 = g37.a();
            if (a2 != null) {
                a2.z.setBackgroundColor(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void O6(int i2) {
        G37<D55> g37 = this.i;
        if (g37 != null) {
            D55 a2 = g37.a();
            if (a2 != null) {
                a2.w.setTextColor(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void P6(int i2) {
        G37<D55> g37 = this.i;
        if (g37 != null) {
            D55 a2 = g37.a();
            if (a2 != null) {
                a2.A.setBackgroundColor(i2);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i2, Intent intent) {
        Wg6.c(str, "tag");
        FLogger.INSTANCE.getLocal().d(k, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363373) {
            CustomizeTextViewModel customizeTextViewModel = this.h;
            if (customizeTextViewModel != null) {
                customizeTextViewModel.f(UserCustomizeThemeFragment.m.a(), l, m);
            } else {
                Wg6.n("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        D55 d55 = (D55) Aq0.f(LayoutInflater.from(getContext()), 2131558539, null, false, A6());
        PortfolioApp.get.instance().getIface().u0(new Kt6()).a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(CustomizeTextViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026extViewModel::class.java)");
            CustomizeTextViewModel customizeTextViewModel = (CustomizeTextViewModel) a2;
            this.h = customizeTextViewModel;
            if (customizeTextViewModel != null) {
                customizeTextViewModel.e().h(getViewLifecycleOwner(), new Bi(this));
                CustomizeTextViewModel customizeTextViewModel2 = this.h;
                if (customizeTextViewModel2 != null) {
                    customizeTextViewModel2.g();
                    this.i = new G37<>(this, d55);
                    Wg6.b(d55, "binding");
                    return d55.n();
                }
                Wg6.n("mViewModel");
                throw null;
            }
            Wg6.n("mViewModel");
            throw null;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        FLogger.INSTANCE.getLocal().d(k, "onDestroy");
        l = null;
        m = null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d(k, "onResume");
        CustomizeTextViewModel customizeTextViewModel = this.h;
        if (customizeTextViewModel != null) {
            customizeTextViewModel.g();
        } else {
            Wg6.n("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<D55> g37 = this.i;
        if (g37 != null) {
            D55 a2 = g37.a();
            if (a2 != null) {
                a2.t.setOnClickListener(new Ci(this));
                a2.u.setOnClickListener(new Di(this));
                a2.s.setOnClickListener(new Ei(this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.X47
    public void q3(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
