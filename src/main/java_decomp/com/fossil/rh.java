package com.fossil;

import com.fossil.fitness.WorkoutSessionManager;
import com.mapped.C90;
import com.mapped.Cd6;
import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rh extends Qq7 implements Hg6<Fs, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Fh b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Rh(Fh fh) {
        super(1);
        this.b = fh;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Fs fs) {
        C90 Q = ((Tu) fs).Q();
        WorkoutSessionManager workoutSessionManager = this.b.x.a.t;
        if (workoutSessionManager == null || workoutSessionManager.getCurrentSessionId() != Q.getSessionId()) {
            I60 i60 = this.b.x;
            i60.a.u0(WorkoutSessionManager.initialize(i60.a().getSerialNumber(), Q.getSessionId(), Q.getWorkoutState(), Q.getDurationInSecond()));
        }
        return Cd6.a;
    }
}
