package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B86 implements MembersInjector<CommuteTimeSettingsDefaultAddressPresenter> {
    @DexIgnore
    public static void a(CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter) {
        commuteTimeSettingsDefaultAddressPresenter.B();
    }
}
