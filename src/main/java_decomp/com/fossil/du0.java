package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.Ut0;
import com.mapped.Cf;
import com.mapped.Zf;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Du0<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> {
    @DexIgnore
    public /* final */ Ut0<T> a;
    @DexIgnore
    public /* final */ Ut0.Ci<T> b; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Ut0.Ci<T> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.Ut0.Ci
        public void a(Cf<T> cf, Cf<T> cf2) {
            Du0.this.g(cf2);
            Du0.this.h(cf, cf2);
        }
    }

    @DexIgnore
    public Du0(Zf.Di<T> di) {
        Ut0<T> ut0 = new Ut0<>(this, di);
        this.a = ut0;
        ut0.a(this.b);
    }

    @DexIgnore
    @Deprecated
    public void g(Cf<T> cf) {
    }

    @DexIgnore
    public T getItem(int i) {
        return this.a.b(i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.c();
    }

    @DexIgnore
    public void h(Cf<T> cf, Cf<T> cf2) {
    }

    @DexIgnore
    public void i(Cf<T> cf) {
        this.a.f(cf);
    }
}
