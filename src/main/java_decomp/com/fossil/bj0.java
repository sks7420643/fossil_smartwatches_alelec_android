package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bj0 {
    @DexIgnore
    public static final <T> Aj0<T> a(T... tArr) {
        Wg6.c(tArr, "values");
        Aj0<T> aj0 = new Aj0<>(tArr.length);
        for (T t : tArr) {
            aj0.add(t);
        }
        return aj0;
    }
}
