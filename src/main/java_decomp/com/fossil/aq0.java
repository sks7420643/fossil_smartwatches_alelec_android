package com.fossil;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Aq0 {
    @DexIgnore
    public static Xp0 a; // = new Yp0();
    @DexIgnore
    public static Zp0 b; // = null;

    @DexIgnore
    public static <T extends ViewDataBinding> T a(Zp0 zp0, View view, int i) {
        return (T) a.b(zp0, view, i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T b(Zp0 zp0, View[] viewArr, int i) {
        return (T) a.c(zp0, viewArr, i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T c(Zp0 zp0, ViewGroup viewGroup, int i, int i2) {
        int childCount = viewGroup.getChildCount();
        int i3 = childCount - i;
        if (i3 == 1) {
            return (T) a(zp0, viewGroup.getChildAt(childCount - 1), i2);
        }
        View[] viewArr = new View[i3];
        for (int i4 = 0; i4 < i3; i4++) {
            viewArr[i4] = viewGroup.getChildAt(i4 + i);
        }
        return (T) b(zp0, viewArr, i2);
    }

    @DexIgnore
    public static Zp0 d() {
        return b;
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T e(LayoutInflater layoutInflater, int i, ViewGroup viewGroup, boolean z) {
        return (T) f(layoutInflater, i, viewGroup, z, b);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T f(LayoutInflater layoutInflater, int i, ViewGroup viewGroup, boolean z, Zp0 zp0) {
        int i2 = 0;
        boolean z2 = viewGroup != null && z;
        if (z2) {
            i2 = viewGroup.getChildCount();
        }
        return z2 ? (T) c(zp0, viewGroup, i2, i) : (T) a(zp0, layoutInflater.inflate(i, viewGroup, z), i);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T g(Activity activity, int i) {
        return (T) h(activity, i, b);
    }

    @DexIgnore
    public static <T extends ViewDataBinding> T h(Activity activity, int i, Zp0 zp0) {
        activity.setContentView(i);
        return (T) c(zp0, (ViewGroup) activity.getWindow().getDecorView().findViewById(16908290), 0, i);
    }
}
