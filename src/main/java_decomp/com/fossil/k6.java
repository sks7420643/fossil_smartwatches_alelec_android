package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K6 extends BluetoothGattCallback {
    @DexIgnore
    public /* final */ /* synthetic */ K5 a;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public K6(K5 k5) {
        this.a = k5;
    }

    @DexIgnore
    public void onCharacteristicChanged(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic) {
        super.onCharacteristicChanged(bluetoothGatt, bluetoothGattCharacteristic);
        K5 k5 = this.a;
        O6 o6 = P6.b;
        if (bluetoothGattCharacteristic != null) {
            UUID uuid = bluetoothGattCharacteristic.getUuid();
            Wg6.b(uuid, "characteristic!!.uuid");
            N6 a2 = o6.a(uuid);
            byte[] value = bluetoothGattCharacteristic.getValue();
            if (value == null) {
                value = new byte[0];
            }
            k5.q(a2, value);
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public void onCharacteristicRead(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        super.onCharacteristicRead(bluetoothGatt, bluetoothGattCharacteristic, i);
        K5 k5 = this.a;
        G7 a2 = G7.d.a(i);
        O6 o6 = P6.b;
        if (bluetoothGattCharacteristic != null) {
            UUID uuid = bluetoothGattCharacteristic.getUuid();
            Wg6.b(uuid, "characteristic!!.uuid");
            N6 a3 = o6.a(uuid);
            byte[] value = bluetoothGattCharacteristic.getValue();
            if (value == null) {
                value = new byte[0];
            }
            k5.b.post(new Y4(k5, a2, a3, value));
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {
        super.onCharacteristicWrite(bluetoothGatt, bluetoothGattCharacteristic, i);
        K5 k5 = this.a;
        G7 a2 = G7.d.a(i);
        O6 o6 = P6.b;
        UUID uuid = bluetoothGattCharacteristic.getUuid();
        Wg6.b(uuid, "characteristic.uuid");
        N6 a3 = o6.a(uuid);
        byte[] value = bluetoothGattCharacteristic.getValue();
        if (value == null) {
            value = new byte[0];
        }
        k5.b.post(new G5(k5, a2, a3, value));
    }

    @DexIgnore
    public void onConnectionStateChange(BluetoothGatt bluetoothGatt, int i, int i2) {
        LinkedHashSet<Long> linkedHashSet;
        LinkedHashSet<Long> linkedHashSet2;
        BluetoothDevice device;
        super.onConnectionStateChange(bluetoothGatt, i, i2);
        BluetoothDevice device2 = bluetoothGatt.getDevice();
        if ((device2 != null ? device2.getAddress() : null) != null) {
            BluetoothDevice device3 = bluetoothGatt.getDevice();
            String address = device3 != null ? device3.getAddress() : null;
            BluetoothGatt bluetoothGatt2 = this.a.c;
            if (Wg6.a(address, (bluetoothGatt2 == null || (device = bluetoothGatt2.getDevice()) == null) ? null : device.getAddress())) {
                Ky1 ky1 = Ky1.DEBUG;
                if (i == 0) {
                    K5 k5 = this.a;
                    V4 v4 = k5.f;
                    if (v4 != null) {
                        k5.e(v4);
                    }
                    K5 k52 = this.a;
                    k52.f = null;
                    k52.e(new V4(i, i2, Dn7.c(Long.valueOf(System.currentTimeMillis()))));
                } else {
                    long currentTimeMillis = System.currentTimeMillis();
                    K5 k53 = this.a;
                    if (currentTimeMillis - k53.d < ((long) 3000)) {
                        k53.e = true;
                    }
                    V4 v42 = this.a.f;
                    if (v42 != null && v42.a == i && v42.b == i2) {
                        if (((v42 == null || (linkedHashSet2 = v42.c) == null) ? 0 : linkedHashSet2.size()) < 1000) {
                            V4 v43 = this.a.f;
                            if (!(v43 == null || (linkedHashSet = v43.c) == null)) {
                                linkedHashSet.add(Long.valueOf(System.currentTimeMillis()));
                            }
                        }
                    }
                    K5 k54 = this.a;
                    V4 v44 = k54.f;
                    if (v44 != null) {
                        k54.e(v44);
                    }
                    this.a.f = new V4(i, i2, Dn7.c(Long.valueOf(System.currentTimeMillis())));
                }
                this.a.c(i2, i);
            }
        }
    }

    @DexIgnore
    public void onDescriptorWrite(BluetoothGatt bluetoothGatt, BluetoothGattDescriptor bluetoothGattDescriptor, int i) {
        super.onDescriptorWrite(bluetoothGatt, bluetoothGattDescriptor, i);
        Ky1 ky1 = Ky1.DEBUG;
        BluetoothGattCharacteristic characteristic = bluetoothGattDescriptor.getCharacteristic();
        Wg6.b(characteristic, "descriptor.characteristic");
        characteristic.getUuid();
        BluetoothGattCharacteristic characteristic2 = bluetoothGattDescriptor.getCharacteristic();
        UUID uuid = characteristic2 != null ? characteristic2.getUuid() : null;
        UUID uuid2 = bluetoothGattDescriptor.getUuid();
        byte[] value = bluetoothGattDescriptor.getValue();
        if (value == null) {
            value = new byte[0];
        }
        if (uuid == null || uuid2 == null) {
            Ky1 ky12 = Ky1.DEBUG;
            return;
        }
        K5 k5 = this.a;
        k5.b.post(new I5(k5, G7.d.a(i), P6.b.a(uuid), V6.d.a(uuid2), value));
    }

    @DexIgnore
    public void onMtuChanged(BluetoothGatt bluetoothGatt, int i, int i2) {
        super.onMtuChanged(bluetoothGatt, i, i2);
        if (i2 == 0) {
            this.a.m = i - 3;
        }
        K5 k5 = this.a;
        k5.b.post(new E5(k5, G7.d.a(i2), i));
    }

    @DexIgnore
    public void onReadRemoteRssi(BluetoothGatt bluetoothGatt, int i, int i2) {
        super.onReadRemoteRssi(bluetoothGatt, i, i2);
        K5 k5 = this.a;
        k5.b.post(new A5(k5, G7.d.a(i2), i));
    }

    @DexIgnore
    public void onServicesDiscovered(BluetoothGatt bluetoothGatt, int i) {
        super.onServicesDiscovered(bluetoothGatt, i);
        List<BluetoothGattService> services = bluetoothGatt.getServices();
        K5 k5 = this.a;
        Wg6.b(services, "services");
        k5.s(services);
        Ky1 ky1 = Ky1.DEBUG;
        Pm7.N(services, "\n", null, null, 0, null, L5.b, 30, null);
        K5 k52 = this.a;
        G7 a2 = G7.d.a(i);
        ArrayList arrayList = new ArrayList(Im7.m(services, 10));
        for (T t : services) {
            Wg6.b(t, "it");
            arrayList.add(t.getUuid());
        }
        Object[] array = arrayList.toArray(new UUID[0]);
        if (array != null) {
            UUID[] uuidArr = (UUID[]) array;
            Set<N6> keySet = this.a.l.keySet();
            Wg6.b(keySet, "gattCharacteristicMap.keys");
            Object[] array2 = keySet.toArray(new N6[0]);
            if (array2 != null) {
                k52.b.post(new S4(k52, a2, uuidArr, (N6[]) array2));
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
