package com.fossil;

import com.fossil.Ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z94 extends Ta4 {
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ Ta4.Di h;
    @DexIgnore
    public /* final */ Ta4.Ci i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4.Ai {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public Ta4.Di g;
        @DexIgnore
        public Ta4.Ci h;

        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public Bi(Ta4 ta4) {
            this.a = ta4.i();
            this.b = ta4.e();
            this.c = Integer.valueOf(ta4.h());
            this.d = ta4.f();
            this.e = ta4.c();
            this.f = ta4.d();
            this.g = ta4.j();
            this.h = ta4.g();
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Ai
        public Ta4 a() {
            String str = "";
            if (this.a == null) {
                str = " sdkVersion";
            }
            if (this.b == null) {
                str = str + " gmpAppId";
            }
            if (this.c == null) {
                str = str + " platform";
            }
            if (this.d == null) {
                str = str + " installationUuid";
            }
            if (this.e == null) {
                str = str + " buildVersion";
            }
            if (this.f == null) {
                str = str + " displayVersion";
            }
            if (str.isEmpty()) {
                return new Z94(this.a, this.b, this.c.intValue(), this.d, this.e, this.f, this.g, this.h);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Ai
        public Ta4.Ai b(String str) {
            if (str != null) {
                this.e = str;
                return this;
            }
            throw new NullPointerException("Null buildVersion");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Ai
        public Ta4.Ai c(String str) {
            if (str != null) {
                this.f = str;
                return this;
            }
            throw new NullPointerException("Null displayVersion");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Ai
        public Ta4.Ai d(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null gmpAppId");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Ai
        public Ta4.Ai e(String str) {
            if (str != null) {
                this.d = str;
                return this;
            }
            throw new NullPointerException("Null installationUuid");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Ai
        public Ta4.Ai f(Ta4.Ci ci) {
            this.h = ci;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Ai
        public Ta4.Ai g(int i) {
            this.c = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Ai
        public Ta4.Ai h(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null sdkVersion");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Ai
        public Ta4.Ai i(Ta4.Di di) {
            this.g = di;
            return this;
        }
    }

    @DexIgnore
    public Z94(String str, String str2, int i2, String str3, String str4, String str5, Ta4.Di di, Ta4.Ci ci) {
        this.b = str;
        this.c = str2;
        this.d = i2;
        this.e = str3;
        this.f = str4;
        this.g = str5;
        this.h = di;
        this.i = ci;
    }

    @DexIgnore
    @Override // com.fossil.Ta4
    public String c() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.Ta4
    public String d() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.Ta4
    public String e() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Ta4.Di di;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4)) {
            return false;
        }
        Ta4 ta4 = (Ta4) obj;
        if (this.b.equals(ta4.i()) && this.c.equals(ta4.e()) && this.d == ta4.h() && this.e.equals(ta4.f()) && this.f.equals(ta4.c()) && this.g.equals(ta4.d()) && ((di = this.h) != null ? di.equals(ta4.j()) : ta4.j() == null)) {
            Ta4.Ci ci = this.i;
            if (ci == null) {
                if (ta4.g() == null) {
                    return true;
                }
            } else if (ci.equals(ta4.g())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Ta4
    public String f() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Ta4
    public Ta4.Ci g() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.Ta4
    public int h() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int i2 = 0;
        int hashCode = this.b.hashCode();
        int hashCode2 = this.c.hashCode();
        int i3 = this.d;
        int hashCode3 = this.e.hashCode();
        int hashCode4 = this.f.hashCode();
        int hashCode5 = this.g.hashCode();
        Ta4.Di di = this.h;
        int hashCode6 = di == null ? 0 : di.hashCode();
        Ta4.Ci ci = this.i;
        if (ci != null) {
            i2 = ci.hashCode();
        }
        return ((hashCode6 ^ ((((((((((((hashCode ^ 1000003) * 1000003) ^ hashCode2) * 1000003) ^ i3) * 1000003) ^ hashCode3) * 1000003) ^ hashCode4) * 1000003) ^ hashCode5) * 1000003)) * 1000003) ^ i2;
    }

    @DexIgnore
    @Override // com.fossil.Ta4
    public String i() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ta4
    public Ta4.Di j() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.Ta4
    public Ta4.Ai l() {
        return new Bi(this);
    }

    @DexIgnore
    public String toString() {
        return "CrashlyticsReport{sdkVersion=" + this.b + ", gmpAppId=" + this.c + ", platform=" + this.d + ", installationUuid=" + this.e + ", buildVersion=" + this.f + ", displayVersion=" + this.g + ", session=" + this.h + ", ndkPayload=" + this.i + "}";
    }
}
