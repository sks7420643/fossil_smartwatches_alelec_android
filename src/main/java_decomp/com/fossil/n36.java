package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class N36 {
    @DexIgnore
    public /* final */ Q26 a;
    @DexIgnore
    public /* final */ H36 b;

    @DexIgnore
    public N36(Q26 q26, H36 h36) {
        Wg6.c(q26, "mInActivityNudgeTimeContractView");
        Wg6.c(h36, "mRemindTimeContractView");
        this.a = q26;
        this.b = h36;
    }

    @DexIgnore
    public final Q26 a() {
        return this.a;
    }

    @DexIgnore
    public final H36 b() {
        return this.b;
    }
}
