package com.fossil;

import com.mapped.Wg6;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class N48 implements C58 {
    @DexIgnore
    public /* final */ C58 b;

    @DexIgnore
    public N48(C58 c58) {
        Wg6.c(c58, "delegate");
        this.b = c58;
    }

    @DexIgnore
    public final C58 a() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.C58, java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.b.close();
    }

    @DexIgnore
    @Override // com.fossil.C58
    public long d0(I48 i48, long j) throws IOException {
        Wg6.c(i48, "sink");
        return this.b.d0(i48, j);
    }

    @DexIgnore
    @Override // com.fossil.C58
    public D58 e() {
        return this.b.e();
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + '(' + this.b + ')';
    }
}
