package com.fossil;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewParent;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kz3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Io0 {
        @DexIgnore
        public /* final */ /* synthetic */ Ci a;
        @DexIgnore
        public /* final */ /* synthetic */ Di b;

        @DexIgnore
        public Ai(Ci ci, Di di) {
            this.a = ci;
            this.b = di;
        }

        @DexIgnore
        @Override // com.fossil.Io0
        public Vo0 a(View view, Vo0 vo0) {
            return this.a.a(view, vo0, new Di(this.b));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnAttachStateChangeListener {
        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            view.removeOnAttachStateChangeListener(this);
            Mo0.i0(view);
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        Vo0 a(View view, Vo0 vo0, Di di);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;

        @DexIgnore
        public Di(int i, int i2, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
        }

        @DexIgnore
        public Di(Di di) {
            this.a = di.a;
            this.b = di.b;
            this.c = di.c;
            this.d = di.d;
        }

        @DexIgnore
        public void a(View view) {
            Mo0.A0(view, this.a, this.b, this.c, this.d);
        }
    }

    @DexIgnore
    public static void a(View view, Ci ci) {
        Mo0.z0(view, new Ai(ci, new Di(Mo0.E(view), view.getPaddingTop(), Mo0.D(view), view.getPaddingBottom())));
        f(view);
    }

    @DexIgnore
    public static float b(Context context, int i) {
        return TypedValue.applyDimension(1, (float) i, context.getResources().getDisplayMetrics());
    }

    @DexIgnore
    public static float c(View view) {
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        for (ViewParent parent = view.getParent(); parent instanceof View; parent = parent.getParent()) {
            f = Mo0.u((View) parent) + f;
        }
        return f;
    }

    @DexIgnore
    public static boolean d(View view) {
        return Mo0.z(view) == 1;
    }

    @DexIgnore
    public static PorterDuff.Mode e(int i, PorterDuff.Mode mode) {
        if (i == 3) {
            return PorterDuff.Mode.SRC_OVER;
        }
        if (i == 5) {
            return PorterDuff.Mode.SRC_IN;
        }
        if (i == 9) {
            return PorterDuff.Mode.SRC_ATOP;
        }
        switch (i) {
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            case 16:
                return PorterDuff.Mode.ADD;
            default:
                return mode;
        }
    }

    @DexIgnore
    public static void f(View view) {
        if (Mo0.P(view)) {
            Mo0.i0(view);
        } else {
            view.addOnAttachStateChangeListener(new Bi());
        }
    }
}
