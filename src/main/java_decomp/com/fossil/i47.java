package com.fossil;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i47 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ a f1583a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void e(a aVar, Context context, Class cls, String str, int i, Object obj) {
            if ((i & 4) != 0) {
                str = "";
            }
            aVar.d(context, cls, str);
        }

        @DexIgnore
        public final void a(Context context, Intent intent, ServiceConnection serviceConnection, int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ServiceUtils", "Service Tracking - Service Utils bindService - context=" + context + ", intent=" + intent + ", serviceConnection=" + serviceConnection + ", flags=" + i);
            try {
                context.bindService(intent, serviceConnection, i);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("ServiceUtils", "bindService() - e=" + e);
            }
        }

        @DexIgnore
        public final <T extends Service> void b(Context context, Class<T> cls, ServiceConnection serviceConnection, int i) {
            pq7.c(context, "context");
            pq7.c(cls, Constants.SERVICE);
            pq7.c(serviceConnection, "serviceConnection");
            a(context, new Intent(context, (Class<?>) cls), serviceConnection, i);
        }

        @DexIgnore
        public final <T extends Service> void c(Context context, Class<T> cls, ServiceConnection serviceConnection, int i) {
            pq7.c(context, "context");
            pq7.c(cls, Constants.SERVICE);
            pq7.c(serviceConnection, "serviceConnection");
            String str = !TextUtils.isEmpty(PortfolioApp.h0.c().J()) ? com.misfit.frameworks.buttonservice.utils.Constants.START_FOREGROUND_ACTION : com.misfit.frameworks.buttonservice.utils.Constants.STOP_FOREGROUND_ACTION;
            Intent intent = new Intent(context, (Class<?>) cls);
            intent.setAction(str);
            FLogger.INSTANCE.getLocal().d("ServiceUtils", "startAndBindServiceConnection() - start foreground");
            f(context, intent);
            FLogger.INSTANCE.getLocal().d("ServiceUtils", "startAndBindServiceConnection() - start bind");
            a(context, intent, serviceConnection, i);
        }

        @DexIgnore
        public final <T extends Service> void d(Context context, Class<T> cls, String str) {
            pq7.c(context, "context");
            pq7.c(cls, Constants.SERVICE);
            pq7.c(str, "foregroundAction");
            String str2 = !TextUtils.isEmpty(PortfolioApp.h0.c().J()) ? com.misfit.frameworks.buttonservice.utils.Constants.START_FOREGROUND_ACTION : com.misfit.frameworks.buttonservice.utils.Constants.STOP_FOREGROUND_ACTION;
            if (TextUtils.isEmpty(str)) {
                str = str2;
            }
            Intent intent = new Intent(context, (Class<?>) cls);
            intent.setAction(str);
            f(context, intent);
        }

        @DexIgnore
        public final void f(Context context, Intent intent) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ServiceUtils", "Service Tracking - Service Utils startForegroundServiceByIntent - intent=" + intent);
            try {
                gl0.o(context, intent);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("ServiceUtils", "Service Tracking - startForegroundServiceByIntent() - e=" + e);
            }
        }

        @DexIgnore
        public final void g(Context context, ServiceConnection serviceConnection) {
            pq7.c(context, "context");
            pq7.c(serviceConnection, "serviceConnection");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ServiceUtils", "unbindService() - serviceConnection = " + serviceConnection);
            try {
                context.unbindService(serviceConnection);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("ServiceUtils", "unbindService() - e=" + e);
            }
        }
    }
}
