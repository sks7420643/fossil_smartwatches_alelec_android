package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gn3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Xr3 b;
    @DexIgnore
    public /* final */ /* synthetic */ Or3 c;
    @DexIgnore
    public /* final */ /* synthetic */ Qm3 d;

    @DexIgnore
    public Gn3(Qm3 qm3, Xr3 xr3, Or3 or3) {
        this.d = qm3;
        this.b = xr3;
        this.c = or3;
    }

    @DexIgnore
    public final void run() {
        this.d.b.d0();
        if (this.b.d.c() == null) {
            this.d.b.O(this.b, this.c);
        } else {
            this.d.b.x(this.b, this.c);
        }
    }
}
