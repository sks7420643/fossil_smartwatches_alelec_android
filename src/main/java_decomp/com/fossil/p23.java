package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface P23 extends O23, Cloneable {
    @DexIgnore
    P23 A(byte[] bArr) throws L13;

    @DexIgnore
    P23 F(M23 m23);

    @DexIgnore
    M23 b();

    @DexIgnore
    M23 h();

    @DexIgnore
    P23 k(byte[] bArr, Q03 q03) throws L13;
}
