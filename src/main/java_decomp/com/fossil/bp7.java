package com.fossil;

import com.mapped.Wg6;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bp7 extends Ap7 {
    @DexIgnore
    public static final Wo7 c(File file, Yo7 yo7) {
        Wg6.c(file, "$this$walk");
        Wg6.c(yo7, "direction");
        return new Wo7(file, yo7);
    }

    @DexIgnore
    public static final Wo7 d(File file) {
        Wg6.c(file, "$this$walkBottomUp");
        return c(file, Yo7.BOTTOM_UP);
    }
}
