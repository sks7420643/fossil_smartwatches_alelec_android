package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Us extends Rs {
    @DexIgnore
    public /* final */ Ho1 A;

    @DexIgnore
    public Us(Ho1 ho1, K5 k5) {
        super(Hs.c0, k5);
        this.A = ho1;
    }

    @DexIgnore
    @Override // com.fossil.Ns
    public U5 D() {
        N6 n6 = N6.l;
        Ho1 ho1 = this.A;
        ByteBuffer order = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
        Wg6.b(order, "ByteBuffer.allocate(8).o\u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(Ot.c.b);
        order.put(Lt.f.b);
        order.putInt(ho1.getNotificationUid());
        order.put(ho1.getAction().a());
        order.put(ho1.getActionStatus().a());
        byte[] array = order.array();
        Wg6.b(array, "appNotificationEventData.array()");
        return new J6(n6, array, this.y.z);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject z() {
        return G80.k(super.z(), Jd0.e4, this.A.toJSONObject());
    }
}
