package com.fossil;

import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hh6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pn7 implements Af6, Serializable {
    @DexIgnore
    public /* final */ Af6.Bi element;
    @DexIgnore
    public /* final */ Af6 left;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Serializable {
        @DexIgnore
        public static /* final */ Aii Companion; // = new Aii(null);
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Af6[] elements;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
                this();
            }
        }

        @DexIgnore
        public Ai(Af6[] af6Arr) {
            Wg6.c(af6Arr, MessengerShareContentUtility.ELEMENTS);
            this.elements = af6Arr;
        }

        @DexIgnore
        private final Object readResolve() {
            Af6[] af6Arr = this.elements;
            Af6 af6 = Un7.INSTANCE;
            for (Af6 af62 : af6Arr) {
                af6 = af6.plus(af62);
            }
            return af6;
        }

        @DexIgnore
        public final Af6[] getElements() {
            return this.elements;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Qq7 implements Coroutine<String, Af6.Bi, String> {
        @DexIgnore
        public static /* final */ Bi INSTANCE; // = new Bi();

        @DexIgnore
        public Bi() {
            super(2);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public /* bridge */ /* synthetic */ String invoke(String str, Af6.Bi bi) {
            return invoke(str, bi);
        }

        @DexIgnore
        public final String invoke(String str, Af6.Bi bi) {
            Wg6.c(str, "acc");
            Wg6.c(bi, "element");
            if (str.length() == 0) {
                return bi.toString();
            }
            return str + ", " + bi;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Qq7 implements Coroutine<Cd6, Af6.Bi, Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Af6[] $elements;
        @DexIgnore
        public /* final */ /* synthetic */ Hh6 $index;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Af6[] af6Arr, Hh6 hh6) {
            super(2);
            this.$elements = af6Arr;
            this.$index = hh6;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public /* bridge */ /* synthetic */ Cd6 invoke(Cd6 cd6, Af6.Bi bi) {
            invoke(cd6, bi);
            return Cd6.a;
        }

        @DexIgnore
        public final void invoke(Cd6 cd6, Af6.Bi bi) {
            Wg6.c(cd6, "<anonymous parameter 0>");
            Wg6.c(bi, "element");
            Af6[] af6Arr = this.$elements;
            Hh6 hh6 = this.$index;
            int i = hh6.element;
            hh6.element = i + 1;
            af6Arr[i] = bi;
        }
    }

    @DexIgnore
    public Pn7(Af6 af6, Af6.Bi bi) {
        Wg6.c(af6, ViewHierarchy.DIMENSION_LEFT_KEY);
        Wg6.c(bi, "element");
        this.left = af6;
        this.element = bi;
    }

    @DexIgnore
    private final Object writeReplace() {
        boolean z = false;
        int e = e();
        Af6[] af6Arr = new Af6[e];
        Hh6 hh6 = new Hh6();
        hh6.element = 0;
        fold(Cd6.a, new Ci(af6Arr, hh6));
        if (hh6.element == e) {
            z = true;
        }
        if (z) {
            return new Ai(af6Arr);
        }
        throw new IllegalStateException("Check failed.".toString());
    }

    @DexIgnore
    public final boolean b(Af6.Bi bi) {
        return Wg6.a(get(bi.getKey()), bi);
    }

    @DexIgnore
    public final boolean d(Pn7 pn7) {
        while (b(pn7.element)) {
            Af6 af6 = pn7.left;
            if (af6 instanceof Pn7) {
                pn7 = (Pn7) af6;
            } else if (af6 != null) {
                return b((Af6.Bi) af6);
            } else {
                throw new Rc6("null cannot be cast to non-null type kotlin.coroutines.CoroutineContext.Element");
            }
        }
        return false;
    }

    @DexIgnore
    public final int e() {
        int i = 2;
        while (true) {
            Af6 af6 = this.left;
            if (!(af6 instanceof Pn7)) {
                af6 = null;
            }
            Pn7 pn7 = (Pn7) af6;
            if (pn7 == null) {
                return i;
            }
            i++;
            this = pn7;
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Pn7) {
                Pn7 pn7 = (Pn7) obj;
                if (pn7.e() != e() || !pn7.d(this)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.mapped.Af6
    public <R> R fold(R r, Coroutine<? super R, ? super Af6.Bi, ? extends R> coroutine) {
        Wg6.c(coroutine, "operation");
        return (R) coroutine.invoke((Object) this.left.fold(r, coroutine), this.element);
    }

    @DexIgnore
    @Override // com.mapped.Af6
    public <E extends Af6.Bi> E get(Af6.Ci<E> ci) {
        Wg6.c(ci, "key");
        while (true) {
            E e = (E) this.element.get(ci);
            if (e != null) {
                return e;
            }
            Af6 af6 = this.left;
            if (!(af6 instanceof Pn7)) {
                return (E) af6.get(ci);
            }
            this = (Pn7) af6;
        }
    }

    @DexIgnore
    public int hashCode() {
        return this.left.hashCode() + this.element.hashCode();
    }

    @DexIgnore
    @Override // com.mapped.Af6
    public Af6 minusKey(Af6.Ci<?> ci) {
        Wg6.c(ci, "key");
        if (this.element.get(ci) != null) {
            return this.left;
        }
        Af6 minusKey = this.left.minusKey(ci);
        return minusKey != this.left ? minusKey == Un7.INSTANCE ? this.element : new Pn7(minusKey, this.element) : this;
    }

    @DexIgnore
    @Override // com.mapped.Af6
    public Af6 plus(Af6 af6) {
        Wg6.c(af6, "context");
        return Af6.Ai.a(this, af6);
    }

    @DexIgnore
    public String toString() {
        return "[" + ((String) fold("", Bi.INSTANCE)) + "]";
    }
}
