package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.RTLImageView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H26 extends RecyclerView.g<Bi> {
    @DexIgnore
    public ArrayList<QuickResponseMessage> a;
    @DexIgnore
    public Ai b;
    @DexIgnore
    public int c;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        Object T2();  // void declaration

        @DexIgnore
        void a1(QuickResponseMessage quickResponseMessage);

        @DexIgnore
        void x5(int i, String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public RTLImageView b;
        @DexIgnore
        public FlexibleEditText c;
        @DexIgnore
        public ConstraintLayout d;
        @DexIgnore
        public View e;
        @DexIgnore
        public QuickResponseMessage f;
        @DexIgnore
        public RTLImageView g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public String i;
        @DexIgnore
        public String j;
        @DexIgnore
        public /* final */ /* synthetic */ H26 k;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements TextWatcher {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public void afterTextChanged(Editable editable) {
                if (this.b.c.isEnabled()) {
                    int length = String.valueOf(editable).length();
                    if (length >= this.b.k.g()) {
                        this.b.h = true;
                        if (!TextUtils.isEmpty(this.b.i)) {
                            this.b.c.setBackgroundResource(2131230862);
                            this.b.c.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.b.i)));
                            return;
                        }
                        this.b.c.setBackgroundResource(2131230862);
                        this.b.c.setBackgroundTintList(ColorStateList.valueOf(-65536));
                    } else if (length != 0) {
                        this.b.g();
                    } else {
                        this.b.c.setHint(Um5.c(PortfolioApp.get.instance(), 2131886126));
                    }
                }
            }

            @DexIgnore
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @DexIgnore
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (this.b.c.hasFocus()) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("onTextChanged id ");
                    QuickResponseMessage quickResponseMessage = this.b.f;
                    sb.append(quickResponseMessage != null ? Integer.valueOf(quickResponseMessage.getId()) : null);
                    sb.append(" message ");
                    QuickResponseMessage quickResponseMessage2 = this.b.f;
                    sb.append(quickResponseMessage2 != null ? quickResponseMessage2.getResponse() : null);
                    local.d("QuickResponseAdapter", sb.toString());
                    Ai h = this.b.k.h();
                    QuickResponseMessage quickResponseMessage3 = this.b.f;
                    Integer valueOf = quickResponseMessage3 != null ? Integer.valueOf(quickResponseMessage3.getId()) : null;
                    if (valueOf != null) {
                        h.x5(valueOf.intValue(), String.valueOf(charSequence));
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements View.OnFocusChangeListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Bii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onFocusChange(View view, boolean z) {
                if (!z) {
                    this.b.g();
                    this.b.c.setEnabled(false);
                    this.b.k.h().T2();
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(H26 h26, View view) {
            super(view);
            Wg6.c(view, "view");
            this.k = h26;
            View findViewById = view.findViewById(2131362744);
            if (findViewById != null) {
                this.b = (RTLImageView) findViewById;
                View findViewById2 = view.findViewById(2131362302);
                if (findViewById2 != null) {
                    this.c = (FlexibleEditText) findViewById2;
                    View findViewById3 = view.findViewById(Nw3.container);
                    if (findViewById3 != null) {
                        this.d = (ConstraintLayout) findViewById3;
                        View findViewById4 = view.findViewById(2131362783);
                        if (findViewById4 != null) {
                            this.e = findViewById4;
                            View findViewById5 = view.findViewById(2131361943);
                            Wg6.b(findViewById5, "view.findViewById(R.id.bt_edit)");
                            this.g = (RTLImageView) findViewById5;
                            this.i = ThemeManager.l.a().d("error");
                            this.j = ThemeManager.l.a().d("nonBrandSurface");
                            String d2 = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
                            String d3 = ThemeManager.l.a().d("nonBrandSeparatorLine");
                            if (!TextUtils.isEmpty(d2)) {
                                this.d.setBackgroundColor(Color.parseColor(d2));
                            }
                            if (!TextUtils.isEmpty(d3)) {
                                this.e.setBackgroundColor(Color.parseColor(d3));
                            }
                            this.c.setEnabled(false);
                            this.b.setOnClickListener(this);
                            this.g.setOnClickListener(this);
                            this.c.setFilters(new InputFilter.LengthFilter[]{new InputFilter.LengthFilter(h26.g())});
                            this.c.addTextChangedListener(new Aii(this));
                            this.c.setOnFocusChangeListener(new Bii(this));
                            return;
                        }
                        throw new Rc6("null cannot be cast to non-null type android.view.View");
                    }
                    throw new Rc6("null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout");
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.view.FlexibleEditText");
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.view.RTLImageView");
        }

        @DexIgnore
        public final void f(QuickResponseMessage quickResponseMessage) {
            Wg6.c(quickResponseMessage, "response");
            this.f = quickResponseMessage;
            if (quickResponseMessage.getResponse().length() > 0) {
                this.c.setText(quickResponseMessage.getResponse());
                quickResponseMessage.getResponse();
                return;
            }
            this.c.setHint(Um5.c(PortfolioApp.get.instance(), 2131886126));
        }

        @DexIgnore
        public final void g() {
            if (this.h) {
                if (!TextUtils.isEmpty(this.j)) {
                    this.c.setBackground(null);
                    this.c.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.j)));
                } else {
                    this.c.setBackground(null);
                    this.c.setBackgroundTintList(ColorStateList.valueOf(-1));
                }
                this.h = false;
            }
        }

        @DexIgnore
        public void onClick(View view) {
            Integer valueOf = view != null ? Integer.valueOf(view.getId()) : null;
            if (valueOf != null && valueOf.intValue() == 2131362744) {
                QuickResponseMessage quickResponseMessage = this.f;
                if (quickResponseMessage != null) {
                    this.k.h().a1(quickResponseMessage);
                }
            } else if (valueOf != null && valueOf.intValue() == 2131361943) {
                this.c.setEnabled(true);
                this.c.requestFocus();
                this.c.e();
                FlexibleEditText flexibleEditText = this.c;
                Editable text = flexibleEditText.getText();
                Integer valueOf2 = text != null ? Integer.valueOf(text.length()) : null;
                if (valueOf2 != null) {
                    flexibleEditText.setSelection(valueOf2.intValue());
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public H26(ArrayList<QuickResponseMessage> arrayList, Ai ai, int i) {
        Wg6.c(arrayList, "mData");
        Wg6.c(ai, "mListener");
        this.a = arrayList;
        this.b = ai;
        this.c = i;
    }

    @DexIgnore
    public final int g() {
        return this.c;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public final Ai h() {
        return this.b;
    }

    @DexIgnore
    public void i(Bi bi, int i) {
        Wg6.c(bi, "holder");
        QuickResponseMessage quickResponseMessage = this.a.get(i);
        Wg6.b(quickResponseMessage, "mData[position]");
        bi.f(quickResponseMessage);
    }

    @DexIgnore
    public Bi j(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558703, viewGroup, false);
        Wg6.b(inflate, "LayoutInflater.from(pare\u2026_response, parent, false)");
        return new Bi(this, inflate);
    }

    @DexIgnore
    public final void k(List<QuickResponseMessage> list) {
        Wg6.c(list, "data");
        this.a.clear();
        for (T t : list) {
            QuickResponseMessage quickResponseMessage = new QuickResponseMessage(t.getResponse());
            quickResponseMessage.setId(t.getId());
            this.a.add(quickResponseMessage);
        }
        notifyDataSetChanged();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("QuickResponseFragment", "notifyDataSetChanged " + this.a);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        i(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return j(viewGroup, i);
    }
}
