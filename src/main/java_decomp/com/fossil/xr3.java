package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xr3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Xr3> CREATOR; // = new As3();
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public Fr3 d;
    @DexIgnore
    public long e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public String g;
    @DexIgnore
    public Vg3 h;
    @DexIgnore
    public long i;
    @DexIgnore
    public Vg3 j;
    @DexIgnore
    public long k;
    @DexIgnore
    public Vg3 l;

    @DexIgnore
    public Xr3(Xr3 xr3) {
        Rc2.k(xr3);
        this.b = xr3.b;
        this.c = xr3.c;
        this.d = xr3.d;
        this.e = xr3.e;
        this.f = xr3.f;
        this.g = xr3.g;
        this.h = xr3.h;
        this.i = xr3.i;
        this.j = xr3.j;
        this.k = xr3.k;
        this.l = xr3.l;
    }

    @DexIgnore
    public Xr3(String str, String str2, Fr3 fr3, long j2, boolean z, String str3, Vg3 vg3, long j3, Vg3 vg32, long j4, Vg3 vg33) {
        this.b = str;
        this.c = str2;
        this.d = fr3;
        this.e = j2;
        this.f = z;
        this.g = str3;
        this.h = vg3;
        this.i = j3;
        this.j = vg32;
        this.k = j4;
        this.l = vg33;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = Bd2.a(parcel);
        Bd2.u(parcel, 2, this.b, false);
        Bd2.u(parcel, 3, this.c, false);
        Bd2.t(parcel, 4, this.d, i2, false);
        Bd2.r(parcel, 5, this.e);
        Bd2.c(parcel, 6, this.f);
        Bd2.u(parcel, 7, this.g, false);
        Bd2.t(parcel, 8, this.h, i2, false);
        Bd2.r(parcel, 9, this.i);
        Bd2.t(parcel, 10, this.j, i2, false);
        Bd2.r(parcel, 11, this.k);
        Bd2.t(parcel, 12, this.l, i2, false);
        Bd2.b(parcel, a2);
    }
}
