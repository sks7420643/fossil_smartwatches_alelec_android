package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;
import android.content.Context;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uj7 {
    @DexIgnore
    public static void a(Activity activity) {
        Lk7.c(activity, Constants.ACTIVITY);
        Application application = activity.getApplication();
        if (application instanceof Dk7) {
            e(activity, (Dk7) application);
        } else {
            throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), Dk7.class.getCanonicalName()));
        }
    }

    @DexIgnore
    public static void b(Service service) {
        Lk7.c(service, Constants.SERVICE);
        Application application = service.getApplication();
        if (application instanceof Dk7) {
            e(service, (Dk7) application);
        } else {
            throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), Dk7.class.getCanonicalName()));
        }
    }

    @DexIgnore
    public static void c(BroadcastReceiver broadcastReceiver, Context context) {
        Lk7.c(broadcastReceiver, "broadcastReceiver");
        Lk7.c(context, "context");
        Application application = (Application) context.getApplicationContext();
        if (application instanceof Dk7) {
            e(broadcastReceiver, (Dk7) application);
        } else {
            throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), Dk7.class.getCanonicalName()));
        }
    }

    @DexIgnore
    public static void d(ContentProvider contentProvider) {
        Lk7.c(contentProvider, "contentProvider");
        Application application = (Application) contentProvider.getContext().getApplicationContext();
        if (application instanceof Dk7) {
            e(contentProvider, (Dk7) application);
        } else {
            throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), Dk7.class.getCanonicalName()));
        }
    }

    @DexIgnore
    public static void e(Object obj, Dk7 dk7) {
        Vj7<Object> c = dk7.c();
        Lk7.d(c, "%s.androidInjector() returned null", dk7.getClass());
        c.a(obj);
    }
}
