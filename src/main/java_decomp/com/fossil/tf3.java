package com.fossil;

import android.os.RemoteException;
import com.fossil.Qb3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tf3 extends Mc3 {
    @DexIgnore
    public /* final */ /* synthetic */ Qb3.Hi b;

    @DexIgnore
    public Tf3(Qb3 qb3, Qb3.Hi hi) {
        this.b = hi;
    }

    @DexIgnore
    @Override // com.fossil.Lc3
    public final void onMapLoaded() throws RemoteException {
        this.b.onMapLoaded();
    }
}
