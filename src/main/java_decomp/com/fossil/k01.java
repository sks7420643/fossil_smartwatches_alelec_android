package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K01 {
    @DexIgnore
    public /* final */ ViewPager2 a;
    @DexIgnore
    public /* final */ M01 b;
    @DexIgnore
    public /* final */ RecyclerView c;

    @DexIgnore
    public K01(ViewPager2 viewPager2, M01 m01, RecyclerView recyclerView) {
        this.a = viewPager2;
        this.b = m01;
        this.c = recyclerView;
    }

    @DexIgnore
    public boolean a() {
        return this.b.g();
    }
}
