package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum S43 {
    zza(0),
    zzb(0L),
    zzc(Float.valueOf((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)),
    zzd(Double.valueOf(0.0d)),
    zze(Boolean.FALSE),
    zzf(""),
    zzg(Xz2.zza),
    zzh(null),
    zzi(null);
    
    @DexIgnore
    public /* final */ Object zzj;

    @DexIgnore
    public S43(Object obj) {
        this.zzj = obj;
    }
}
