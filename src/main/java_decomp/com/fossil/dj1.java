package com.fossil;

import android.graphics.drawable.Drawable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dj1<R> implements Aj1<R>, Ej1<R> {
    @DexIgnore
    public static /* final */ Ai l; // = new Ai();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ Ai e;
    @DexIgnore
    public R f;
    @DexIgnore
    public Bj1 g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public Dd1 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public void a(Object obj) {
            obj.notifyAll();
        }

        @DexIgnore
        public void b(Object obj, long j) throws InterruptedException {
            obj.wait(j);
        }
    }

    @DexIgnore
    public Dj1(int i2, int i3) {
        this(i2, i3, true, l);
    }

    @DexIgnore
    public Dj1(int i2, int i3, boolean z, Ai ai) {
        this.b = i2;
        this.c = i3;
        this.d = z;
        this.e = ai;
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public void a(Pj1 pj1) {
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public void b(R r, Tj1<? super R> tj1) {
        synchronized (this) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r1 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        r1.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return true;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean cancel(boolean r4) {
        /*
            r3 = this;
            r1 = 0
            r0 = 1
            monitor-enter(r3)
            boolean r2 = r3.isDone()     // Catch:{ all -> 0x0022 }
            if (r2 == 0) goto L_0x000c
            monitor-exit(r3)     // Catch:{ all -> 0x0022 }
            r0 = 0
        L_0x000b:
            return r0
        L_0x000c:
            r2 = 1
            r3.h = r2     // Catch:{ all -> 0x0022 }
            com.fossil.Dj1$Ai r2 = r3.e     // Catch:{ all -> 0x0022 }
            r2.a(r3)     // Catch:{ all -> 0x0022 }
            if (r4 == 0) goto L_0x001b
            com.fossil.Bj1 r1 = r3.g     // Catch:{ all -> 0x0022 }
            r2 = 0
            r3.g = r2     // Catch:{ all -> 0x0022 }
        L_0x001b:
            monitor-exit(r3)     // Catch:{ all -> 0x0022 }
            if (r1 == 0) goto L_0x000b
            r1.clear()
            goto L_0x000b
        L_0x0022:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Dj1.cancel(boolean):boolean");
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public void d(Bj1 bj1) {
        synchronized (this) {
            this.g = bj1;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ej1
    public boolean e(Dd1 dd1, Object obj, Qj1<R> qj1, boolean z) {
        synchronized (this) {
            this.j = true;
            this.k = dd1;
            this.e.a(this);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public void f(Drawable drawable) {
        synchronized (this) {
        }
    }

    @DexIgnore
    @Override // com.fossil.Ej1
    public boolean g(R r, Object obj, Qj1<R> qj1, Gb1 gb1, boolean z) {
        synchronized (this) {
            this.i = true;
            this.f = r;
            this.e.a(this);
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.concurrent.Future
    public R get() throws InterruptedException, ExecutionException {
        try {
            return l(null);
        } catch (TimeoutException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    @Override // java.util.concurrent.Future
    public R get(long j2, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return l(Long.valueOf(timeUnit.toMillis(j2)));
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public void h(Drawable drawable) {
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public Bj1 i() {
        Bj1 bj1;
        synchronized (this) {
            bj1 = this.g;
        }
        return bj1;
    }

    @DexIgnore
    public boolean isCancelled() {
        boolean z;
        synchronized (this) {
            z = this.h;
        }
        return z;
    }

    @DexIgnore
    public boolean isDone() {
        boolean z;
        synchronized (this) {
            z = this.h || this.i || this.j;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public void j(Drawable drawable) {
    }

    @DexIgnore
    @Override // com.fossil.Qj1
    public void k(Pj1 pj1) {
        pj1.d(this.b, this.c);
    }

    @DexIgnore
    public final R l(Long l2) throws ExecutionException, InterruptedException, TimeoutException {
        R r;
        synchronized (this) {
            if (this.d && !isDone()) {
                Jk1.a();
            }
            if (this.h) {
                throw new CancellationException();
            } else if (this.j) {
                throw new ExecutionException(this.k);
            } else if (this.i) {
                r = this.f;
            } else {
                if (l2 == null) {
                    this.e.b(this, 0);
                } else if (l2.longValue() > 0) {
                    long currentTimeMillis = System.currentTimeMillis();
                    long longValue = l2.longValue() + currentTimeMillis;
                    while (!isDone() && currentTimeMillis < longValue) {
                        this.e.b(this, longValue - currentTimeMillis);
                        currentTimeMillis = System.currentTimeMillis();
                    }
                }
                if (Thread.interrupted()) {
                    throw new InterruptedException();
                } else if (this.j) {
                    throw new ExecutionException(this.k);
                } else if (this.h) {
                    throw new CancellationException();
                } else if (this.i) {
                    r = this.f;
                } else {
                    throw new TimeoutException();
                }
            }
        }
        return r;
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onDestroy() {
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onStart() {
    }

    @DexIgnore
    @Override // com.fossil.Ei1
    public void onStop() {
    }
}
