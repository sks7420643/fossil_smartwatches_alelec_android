package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class G98 implements E88<W18, String> {
    @DexIgnore
    public static /* final */ G98 a; // = new G98();

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.E88
    public /* bridge */ /* synthetic */ String a(W18 w18) throws IOException {
        return b(w18);
    }

    @DexIgnore
    public String b(W18 w18) throws IOException {
        return w18.string();
    }
}
