package com.fossil;

import android.os.Parcel;
import com.mapped.E90;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Vp1 extends Mp1 {
    @DexIgnore
    public Vp1(Parcel parcel) {
        super(parcel);
    }

    @DexIgnore
    public Vp1(E90 e90, byte b) {
        super(e90, b);
    }
}
