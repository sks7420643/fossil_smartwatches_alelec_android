package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface N61<T, V> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public static <T, V> boolean a(N61<T, V> n61, T t) {
            Wg6.c(t, "data");
            return true;
        }
    }

    @DexIgnore
    boolean a(T t);

    @DexIgnore
    V b(T t);
}
