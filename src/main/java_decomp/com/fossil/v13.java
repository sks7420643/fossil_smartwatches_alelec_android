package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class V13 {
    @DexIgnore
    public static /* final */ V13 a; // = new X13();
    @DexIgnore
    public static /* final */ V13 b; // = new A23();

    @DexIgnore
    public V13() {
    }

    @DexIgnore
    public static V13 a() {
        return a;
    }

    @DexIgnore
    public static V13 c() {
        return b;
    }

    @DexIgnore
    public abstract <L> void b(Object obj, Object obj2, long j);

    @DexIgnore
    public abstract void d(Object obj, long j);
}
