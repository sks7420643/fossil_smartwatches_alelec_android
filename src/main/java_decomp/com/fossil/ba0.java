package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Ba0 extends Enum<Ba0> {
    @DexIgnore
    public static /* final */ Ba0 c;
    @DexIgnore
    public static /* final */ /* synthetic */ Ba0[] d;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        Ba0 ba0 = new Ba0("EVENT_MAPPING", 0, (byte) 0);
        Ba0 ba02 = new Ba0("KEY_CODE_MAPPING", 1, (byte) 1);
        Ba0 ba03 = new Ba0("GOAL_TRACKING", 2, (byte) 2);
        c = ba03;
        d = new Ba0[]{ba0, ba02, ba03, new Ba0("UNDEFINED", 3, (byte) 255)};
    }
    */

    @DexIgnore
    public Ba0(String str, int i, byte b2) {
        this.b = (byte) b2;
    }

    @DexIgnore
    public static Ba0 valueOf(String str) {
        return (Ba0) Enum.valueOf(Ba0.class, str);
    }

    @DexIgnore
    public static Ba0[] values() {
        return (Ba0[]) d.clone();
    }
}
