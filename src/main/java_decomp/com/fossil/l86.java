package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Er4;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L86 extends BaseFragment implements K86, Er4.Ai {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public G37<E15> g;
    @DexIgnore
    public Er4 h;
    @DexIgnore
    public J86 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return L86.k;
        }

        @DexIgnore
        public final L86 b() {
            return new L86();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ L86 b;

        @DexIgnore
        public Bi(L86 l86) {
            this.b = l86;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.F6();
        }
    }

    /*
    static {
        String simpleName = L86.class.getSimpleName();
        Wg6.b(simpleName, "SearchRingPhoneFragment::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return k;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return false;
        }
        J86 j86 = this.i;
        if (j86 != null) {
            j86.p();
            Intent intent = new Intent();
            J86 j862 = this.i;
            if (j862 != null) {
                intent.putExtra("KEY_SELECTED_RINGPHONE", j862.n());
                activity.setResult(-1, intent);
                activity.finish();
                return false;
            }
            Wg6.n("mPresenter");
            throw null;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.K86
    public void J3(List<Ringtone> list) {
        Wg6.c(list, "data");
        Er4 er4 = this.h;
        if (er4 != null) {
            J86 j86 = this.i;
            if (j86 != null) {
                er4.k(list, j86.n());
            } else {
                Wg6.n("mPresenter");
                throw null;
            }
        } else {
            Wg6.n("mSearchRingPhoneAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void L6(J86 j86) {
        Wg6.c(j86, "presenter");
        this.i = j86;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(J86 j86) {
        L6(j86);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        G37<E15> g37 = new G37<>(this, (E15) Aq0.f(layoutInflater, 2131558434, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            E15 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        J86 j86 = this.i;
        if (j86 != null) {
            j86.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        J86 j86 = this.i;
        if (j86 != null) {
            j86.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<E15> g37 = this.g;
        if (g37 != null) {
            E15 a2 = g37.a();
            if (a2 != null) {
                this.h = new Er4(this);
                RecyclerView recyclerView = a2.s;
                Wg6.b(recyclerView, "it.rvRingphones");
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView2 = a2.s;
                Wg6.b(recyclerView2, "it.rvRingphones");
                Er4 er4 = this.h;
                if (er4 != null) {
                    recyclerView2.setAdapter(er4);
                    a2.q.setOnClickListener(new Bi(this));
                    return;
                }
                Wg6.n("mSearchRingPhoneAdapter");
                throw null;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Er4.Ai
    public void p2(Ringtone ringtone) {
        Wg6.c(ringtone, Constants.RINGTONE);
        J86 j86 = this.i;
        if (j86 != null) {
            j86.o(ringtone);
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
