package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@eo7(c = "com.fossil.blesdk.network.interceptor.AuthenticationInterceptor$intercept$accessToken$1", f = "AuthenticationInterceptor.kt", l = {18}, m = "invokeSuspend")
public final class vc0 extends ko7 implements vp7<iv7, qn7<? super String>, Object> {
    @DexIgnore
    public iv7 b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public int d;
    @DexIgnore
    public /* final */ /* synthetic */ wc0 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vc0(wc0 wc0, qn7 qn7) {
        super(2, qn7);
        this.e = wc0;
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final qn7<tl7> create(Object obj, qn7<?> qn7) {
        vc0 vc0 = new vc0(this.e, qn7);
        vc0.b = (iv7) obj;
        throw null;
        //return vc0;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.vp7
    public final Object invoke(iv7 iv7, qn7<? super String> qn7) {
        vc0 vc0 = new vc0(this.e, qn7);
        vc0.b = iv7;
        return vc0.invokeSuspend(tl7.f3441a);
    }

    @DexIgnore
    @Override // com.fossil.zn7
    public final Object invokeSuspend(Object obj) {
        Object d2 = yn7.d();
        int i = this.d;
        if (i == 0) {
            el7.b(obj);
            iv7 iv7 = this.b;
            wc0 wc0 = this.e;
            this.c = iv7;
            this.d = 1;
            Object a2 = wc0.a(this);
            return a2 == d2 ? d2 : a2;
        } else if (i == 1) {
            iv7 iv72 = (iv7) this.c;
            el7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
