package com.fossil;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lt0 {
    @DexIgnore
    public Mt0 a;

    @DexIgnore
    public Lt0(String str, int i, int i2) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.a = new Nt0(str, i, i2);
        } else {
            this.a = new Ot0(str, i, i2);
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Lt0)) {
            return false;
        }
        return this.a.equals(((Lt0) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }
}
