package com.fossil;

import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wa6 implements Factory<WeatherSettingPresenter> {
    @DexIgnore
    public static WeatherSettingPresenter a(Sa6 sa6, GoogleApiService googleApiService) {
        return new WeatherSettingPresenter(sa6, googleApiService);
    }
}
