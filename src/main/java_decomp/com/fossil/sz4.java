package com.fossil;

import com.mapped.TimeUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sz4 {
    @DexIgnore
    public final String a(DateTime dateTime) {
        return TimeUtils.t0(dateTime);
    }

    @DexIgnore
    public final DateTime b(String str) {
        try {
            return TimeUtils.S(str);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("DateTimeISOStringConverter", "toOffsetDateTime - e=" + e);
            return null;
        }
    }
}
