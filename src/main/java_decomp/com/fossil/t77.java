package com.fossil;

import com.portfolio.platform.watchface.data.WFAssetsLocal;
import com.portfolio.platform.watchface.data.source.WFAssetRemote;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T77 implements Factory<WFAssetRepository> {
    @DexIgnore
    public /* final */ Provider<WFAssetsLocal> a;
    @DexIgnore
    public /* final */ Provider<WFAssetRemote> b;

    @DexIgnore
    public T77(Provider<WFAssetsLocal> provider, Provider<WFAssetRemote> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static T77 a(Provider<WFAssetsLocal> provider, Provider<WFAssetRemote> provider2) {
        return new T77(provider, provider2);
    }

    @DexIgnore
    public static WFAssetRepository c(WFAssetsLocal wFAssetsLocal, WFAssetRemote wFAssetRemote) {
        return new WFAssetRepository(wFAssetsLocal, wFAssetRemote);
    }

    @DexIgnore
    public WFAssetRepository b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
