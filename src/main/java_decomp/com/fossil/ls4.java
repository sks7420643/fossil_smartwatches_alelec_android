package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ls4 implements Parcelable {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public int b;
    @DexIgnore
    @Vu3("id")
    public int c;
    @DexIgnore
    @Vu3("encryptMethod")
    public int d;
    @DexIgnore
    @Vu3("encryptedData")
    public String e;
    @DexIgnore
    @Vu3("keyType")
    public int f;
    @DexIgnore
    @Vu3("sequence")
    public String g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ls4> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public Ls4 a(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new Ls4(parcel);
        }

        @DexIgnore
        public Ls4[] b(int i) {
            return new Ls4[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Ls4 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Ls4[] newArray(int i) {
            return b(i);
        }
    }

    @DexIgnore
    public Ls4(int i, int i2, String str, int i3, String str2) {
        Wg6.c(str, "encryptedData");
        Wg6.c(str2, "sequence");
        this.c = i;
        this.d = i2;
        this.e = str;
        this.f = i3;
        this.g = str2;
        this.b = 1;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Ls4(android.os.Parcel r8) {
        /*
            r7 = this;
            r6 = 0
            java.lang.String r0 = "parcel"
            com.mapped.Wg6.c(r8, r0)
            int r1 = r8.readInt()
            int r2 = r8.readInt()
            java.lang.String r3 = r8.readString()
            if (r3 == 0) goto L_0x0031
            java.lang.String r0 = "parcel.readString()!!"
            com.mapped.Wg6.b(r3, r0)
            int r4 = r8.readInt()
            java.lang.String r5 = r8.readString()
            if (r5 == 0) goto L_0x002d
            java.lang.String r0 = "parcel.readString()!!"
            com.mapped.Wg6.b(r5, r0)
            r0 = r7
            r0.<init>(r1, r2, r3, r4, r5)
            return
        L_0x002d:
            com.mapped.Wg6.i()
            throw r6
        L_0x0031:
            com.mapped.Wg6.i()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ls4.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public final int a() {
        return this.d;
    }

    @DexIgnore
    public final String b() {
        return this.e;
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    public final int d() {
        return this.f;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Ls4) {
                Ls4 ls4 = (Ls4) obj;
                if (!(this.c == ls4.c && this.d == ls4.d && Wg6.a(this.e, ls4.e) && this.f == ls4.f && Wg6.a(this.g, ls4.g))) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String f() {
        return this.g;
    }

    @DexIgnore
    public final void g(int i) {
        this.b = i;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int i2 = this.c;
        int i3 = this.d;
        String str = this.e;
        int hashCode = str != null ? str.hashCode() : 0;
        int i4 = this.f;
        String str2 = this.g;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return ((((hashCode + (((i2 * 31) + i3) * 31)) * 31) + i4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "BCFitnessData(id=" + this.c + ", encryptMethod=" + this.d + ", encryptedData=" + this.e + ", keyType=" + this.f + ", sequence=" + this.g + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeInt(this.c);
        parcel.writeInt(this.d);
        parcel.writeString(this.e);
        parcel.writeInt(this.f);
        parcel.writeString(this.g);
    }
}
