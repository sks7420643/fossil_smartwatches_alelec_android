package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ef3 implements Parcelable.Creator<Me3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Me3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 0;
        Float f = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 2) {
                i = Ad2.v(parcel, t);
            } else if (l != 3) {
                Ad2.B(parcel, t);
            } else {
                f = Ad2.s(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Me3(i, f);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Me3[] newArray(int i) {
        return new Me3[i];
    }
}
