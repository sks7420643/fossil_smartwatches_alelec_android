package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nr3 implements Parcelable.Creator<Or3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Or3 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        long j = -2147483648L;
        boolean z = true;
        boolean z2 = false;
        int i = 0;
        boolean z3 = true;
        boolean z4 = true;
        boolean z5 = false;
        String str = null;
        ArrayList<String> arrayList = null;
        Boolean bool = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        String str8 = null;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        long j5 = 0;
        long j6 = 0;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            switch (Ad2.l(t)) {
                case 2:
                    str8 = Ad2.f(parcel, t);
                    break;
                case 3:
                    str7 = Ad2.f(parcel, t);
                    break;
                case 4:
                    str6 = Ad2.f(parcel, t);
                    break;
                case 5:
                    str5 = Ad2.f(parcel, t);
                    break;
                case 6:
                    j6 = Ad2.y(parcel, t);
                    break;
                case 7:
                    j5 = Ad2.y(parcel, t);
                    break;
                case 8:
                    str4 = Ad2.f(parcel, t);
                    break;
                case 9:
                    z = Ad2.m(parcel, t);
                    break;
                case 10:
                    z2 = Ad2.m(parcel, t);
                    break;
                case 11:
                    j = Ad2.y(parcel, t);
                    break;
                case 12:
                    str3 = Ad2.f(parcel, t);
                    break;
                case 13:
                    j4 = Ad2.y(parcel, t);
                    break;
                case 14:
                    j3 = Ad2.y(parcel, t);
                    break;
                case 15:
                    i = Ad2.v(parcel, t);
                    break;
                case 16:
                    z3 = Ad2.m(parcel, t);
                    break;
                case 17:
                    z4 = Ad2.m(parcel, t);
                    break;
                case 18:
                    z5 = Ad2.m(parcel, t);
                    break;
                case 19:
                    str2 = Ad2.f(parcel, t);
                    break;
                case 20:
                default:
                    Ad2.B(parcel, t);
                    break;
                case 21:
                    bool = Ad2.n(parcel, t);
                    break;
                case 22:
                    j2 = Ad2.y(parcel, t);
                    break;
                case 23:
                    arrayList = Ad2.h(parcel, t);
                    break;
                case 24:
                    str = Ad2.f(parcel, t);
                    break;
            }
        }
        Ad2.k(parcel, C);
        return new Or3(str8, str7, str6, str5, j6, j5, str4, z, z2, j, str3, j4, j3, i, z3, z4, z5, str2, bool, j2, arrayList, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Or3[] newArray(int i) {
        return new Or3[i];
    }
}
