package com.fossil;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import com.facebook.places.model.PlaceFields;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.service.FossilNotificationListenerService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g47 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static /* final */ a f1261a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String[] a() {
            HashMap<String, Boolean> b = b();
            Set<String> keySet = b.keySet();
            pq7.b(keySet, "permissionList.keys");
            ArrayList arrayList = new ArrayList();
            for (T t : keySet) {
                T t2 = t;
                pq7.b(t2, "it");
                if (((Boolean) zm7.h(b, t2)).booleanValue()) {
                    arrayList.add(t);
                }
            }
            Object[] array = arrayList.toArray(new String[0]);
            if (array != null) {
                return (String[]) array;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public final HashMap<String, Boolean> b() {
            PortfolioApp c = PortfolioApp.h0.c();
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            linkedHashMap.put(InAppPermission.ACCESS_BACKGROUND_LOCATION, Boolean.valueOf(c(c)));
            linkedHashMap.put(InAppPermission.ACCESS_FINE_LOCATION, Boolean.valueOf(d(c)));
            linkedHashMap.put(InAppPermission.LOCATION_SERVICE, Boolean.valueOf(g()));
            linkedHashMap.put(InAppPermission.BLUETOOTH, Boolean.valueOf(e()));
            linkedHashMap.put(InAppPermission.READ_CONTACTS, Boolean.valueOf(l(c)));
            linkedHashMap.put(InAppPermission.READ_PHONE_STATE, Boolean.valueOf(m(c)));
            linkedHashMap.put(InAppPermission.READ_SMS, Boolean.valueOf(n(c)));
            linkedHashMap.put(InAppPermission.NOTIFICATION_ACCESS, Boolean.valueOf(h()));
            linkedHashMap.put(InAppPermission.CAMERA, Boolean.valueOf(f(c)));
            linkedHashMap.put(InAppPermission.WRITE_EXTERNAL_STORAGE, Boolean.valueOf(o(c)));
            return linkedHashMap;
        }

        @DexIgnore
        public final boolean c(Context context) {
            pq7.c(context, "context");
            return i(context, "android.permission.ACCESS_BACKGROUND_LOCATION");
        }

        @DexIgnore
        public final boolean d(Context context) {
            pq7.c(context, "context");
            return i(context, "android.permission.ACCESS_FINE_LOCATION");
        }

        @DexIgnore
        public final boolean e() {
            BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
            return defaultAdapter != null && defaultAdapter.isEnabled();
        }

        @DexIgnore
        public final boolean f(Context context) {
            return i(context, "android.permission.CAMERA");
        }

        @DexIgnore
        public final boolean g() {
            LocationManager locationManager = (LocationManager) PortfolioApp.h0.c().getSystemService(PlaceFields.LOCATION);
            if (locationManager == null) {
                return false;
            }
            String bestProvider = locationManager.getBestProvider(new Criteria(), true);
            if (!(i68.a(bestProvider) || pq7.a("passive", bestProvider) || (vt7.j(LocationUtils.HUAWEI_MODEL, Build.MANUFACTURER, true) && vt7.j(LocationUtils.HUAWEI_LOCAL_PROVIDER, bestProvider, true)))) {
                return true;
            }
            try {
                if (Settings.Secure.getInt(PortfolioApp.h0.c().getContentResolver(), "location_mode") != 0) {
                    return true;
                }
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            return false;
        }

        @DexIgnore
        public final boolean h() {
            PortfolioApp c = PortfolioApp.h0.c();
            String string = Settings.Secure.getString(c.getContentResolver(), "enabled_notification_listeners");
            String str = c.getPackageName() + "/" + FossilNotificationListenerService.class.getCanonicalName();
            FLogger.INSTANCE.getLocal().d("PermissionUtils", "isNotificationListenerEnabled - notificationServicePath=" + str + ", enabledNotificationListeners=" + string);
            if (TextUtils.isEmpty(string)) {
                return false;
            }
            pq7.b(string, "enabledNotificationListeners");
            return wt7.v(string, str, false, 2, null);
        }

        @DexIgnore
        public final boolean i(Context context, String str) {
            return Build.VERSION.SDK_INT < 23 || gl0.a(context, str) == 0;
        }

        @DexIgnore
        public final boolean j(Context context, String... strArr) {
            pq7.c(context, "context");
            pq7.c(strArr, "perms");
            return v78.a(context, (String[]) Arrays.copyOf(strArr, strArr.length));
        }

        @DexIgnore
        public final boolean k(Context context) {
            pq7.c(context, "context");
            return i(context, "android.permission.READ_CALL_LOG");
        }

        @DexIgnore
        public final boolean l(Context context) {
            pq7.c(context, "context");
            return i(context, "android.permission.READ_CONTACTS");
        }

        @DexIgnore
        public final boolean m(Context context) {
            return i(context, "android.permission.READ_PHONE_STATE");
        }

        @DexIgnore
        public final boolean n(Context context) {
            return i(context, "android.permission.READ_SMS");
        }

        @DexIgnore
        public final boolean o(Context context) {
            pq7.c(context, "context");
            return i(context, "android.permission.WRITE_EXTERNAL_STORAGE");
        }

        @DexIgnore
        public final void p(Object obj, int i, String... strArr) {
            int[] iArr = new int[strArr.length];
            int length = strArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                iArr[i2] = 0;
            }
            v78.c(i, strArr, iArr, obj);
        }

        @DexIgnore
        public final boolean q(Activity activity, int i) {
            pq7.c(activity, Constants.ACTIVITY);
            if (c(activity)) {
                return true;
            }
            rk0.u(activity, new String[]{"android.permission.ACCESS_BACKGROUND_LOCATION"}, i);
            return false;
        }

        @DexIgnore
        public final boolean r(Activity activity, int i) {
            pq7.c(activity, Constants.ACTIVITY);
            if (d(activity)) {
                return true;
            }
            rk0.u(activity, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, i);
            return false;
        }

        @DexIgnore
        public final boolean s(Activity activity, int i) {
            pq7.c(activity, Constants.ACTIVITY);
            if (o(activity)) {
                return true;
            }
            rk0.u(activity, new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"}, i);
            return false;
        }

        @DexIgnore
        public final void t(Fragment fragment, int i, String... strArr) {
            pq7.c(fragment, "fragment");
            pq7.c(strArr, "perms");
            if (v78.a(fragment.requireContext(), (String[]) Arrays.copyOf(strArr, strArr.length))) {
                p(fragment, i, (String[]) Arrays.copyOf(strArr, strArr.length));
            } else {
                fragment.requestPermissions(strArr, i);
            }
        }
    }
}
