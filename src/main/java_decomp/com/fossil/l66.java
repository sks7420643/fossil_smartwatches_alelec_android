package com.fossil;

import com.fossil.Jn5;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L66 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<N66> c;
    @DexIgnore
    public List<? extends Jn5.Ai> d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public L66(String str, String str2, ArrayList<N66> arrayList, List<? extends Jn5.Ai> list, boolean z) {
        Wg6.c(str, "mPresetId");
        Wg6.c(str2, "mPresetName");
        Wg6.c(arrayList, "mWatchApps");
        Wg6.c(list, "mPermissionFeatures");
        this.a = str;
        this.b = str2;
        this.c = arrayList;
        this.d = list;
        this.e = z;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final ArrayList<N66> b() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof L66) {
                L66 l66 = (L66) obj;
                if (!Wg6.a(this.a, l66.a) || !Wg6.a(this.b, l66.b) || !Wg6.a(this.c, l66.c) || !Wg6.a(this.d, l66.d) || this.e != l66.e) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        ArrayList<N66> arrayList = this.c;
        int hashCode3 = arrayList != null ? arrayList.hashCode() : 0;
        List<? extends Jn5.Ai> list = this.d;
        if (list != null) {
            i = list.hashCode();
        }
        boolean z = this.e;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public String toString() {
        return "DianaPresetConfigWrapper(mPresetId=" + this.a + ", mPresetName=" + this.b + ", mWatchApps=" + this.c + ", mPermissionFeatures=" + this.d + ", mIsActive=" + this.e + ")";
    }
}
