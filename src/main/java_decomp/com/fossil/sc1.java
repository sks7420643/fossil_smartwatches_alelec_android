package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Sc1 {

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(Mb1 mb1, Exception exc, Wb1<?> wb1, Gb1 gb1);

        @DexIgnore
        Object b();  // void declaration

        @DexIgnore
        void e(Mb1 mb1, Object obj, Wb1<?> wb1, Gb1 gb1, Mb1 mb12);
    }

    @DexIgnore
    boolean c();

    @DexIgnore
    Object cancel();  // void declaration
}
