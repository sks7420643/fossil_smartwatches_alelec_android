package com.fossil;

import com.fossil.Qb3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wf3 extends Zc3 {
    @DexIgnore
    public /* final */ /* synthetic */ Qb3.Mi b;

    @DexIgnore
    public Wf3(Qb3 qb3, Qb3.Mi mi) {
        this.b = mi;
    }

    @DexIgnore
    @Override // com.fossil.Yc3
    public final void c0(Rs2 rs2) {
        this.b.onPolylineClick(new Pe3(rs2));
    }
}
