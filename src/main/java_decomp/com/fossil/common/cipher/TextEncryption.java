package com.fossil.common.cipher;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Base64;
import com.fossil.Bw7;
import com.fossil.Dx1;
import com.fossil.El7;
import com.fossil.Em7;
import com.fossil.Ex1;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Lx1;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.security.KeyPair;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TextEncryption {
    @DexIgnore
    public static /* final */ Lx1.Bi a; // = Lx1.Bi.CBC_PKCS5_PADDING;
    @DexIgnore
    public static /* final */ byte[] b; // = new byte[16];
    @DexIgnore
    public static /* final */ String c; // = Lx1.Bi.GCM_NO_PADDING.getValue();
    @DexIgnore
    public static Context d;
    @DexIgnore
    public static SharedPreferences e;
    @DexIgnore
    public static byte[] f;
    @DexIgnore
    public static /* final */ Object g; // = new Object();
    @DexIgnore
    @TargetApi(23)
    public static byte[] h;
    @DexIgnore
    public static /* final */ Object i; // = new Object();
    @DexIgnore
    public static /* final */ TextEncryption j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.fossil.common.cipher.TextEncryption$loadAsymmetricSecretKey$1", f = "TextEncryption.kt", l = {}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        public Ai(Xe6 xe6) {
            super(2, xe6);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            byte[] bArr = null;
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                TextEncryption textEncryption = TextEncryption.j;
                synchronized (TextEncryption.g) {
                    TextEncryption textEncryption2 = TextEncryption.j;
                    if (TextEncryption.f == null) {
                        KeyPair e = Ex1.b.e("a");
                        if (e == null) {
                            if (Build.VERSION.SDK_INT < 23) {
                                TextEncryption textEncryption3 = TextEncryption.j;
                                Context context = TextEncryption.d;
                                if (context != null) {
                                    e = Ex1.b.b(context, "a");
                                }
                            }
                            e = null;
                        }
                        if (e != null) {
                            TextEncryption textEncryption4 = TextEncryption.j;
                            SharedPreferences sharedPreferences = TextEncryption.e;
                            if (sharedPreferences != null) {
                                String string = sharedPreferences.getString("b", null);
                                if (string != null) {
                                    bArr = Base64.decode(string, 0);
                                }
                                if (bArr == null) {
                                    byte[] encoded = Lx1.a.c().getEncoded();
                                    Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                                    instance.init(1, e.getPublic());
                                    try {
                                        bArr = instance.doFinal(encoded);
                                        sharedPreferences.edit().putString("b", Base64.encodeToString(bArr, 0)).apply();
                                    } catch (Throwable th) {
                                        TextEncryption.j.l();
                                    }
                                }
                                Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                                instance2.init(2, e.getPrivate());
                                try {
                                    TextEncryption textEncryption5 = TextEncryption.j;
                                    TextEncryption.f = instance2.doFinal(bArr);
                                } catch (Throwable th2) {
                                    TextEncryption.j.l();
                                }
                            }
                        }
                    }
                    Cd6 cd6 = Cd6.a;
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.fossil.common.cipher.TextEncryption$loadSymmetricSecretKey$1", f = "TextEncryption.kt", l = {}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        public Bi(Xe6 xe6) {
            super(2, xe6);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            byte[] bArr = null;
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                TextEncryption textEncryption = TextEncryption.j;
                synchronized (TextEncryption.i) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        TextEncryption textEncryption2 = TextEncryption.j;
                        if (TextEncryption.h == null) {
                            SecretKey f = Ex1.b.f("c");
                            TextEncryption textEncryption3 = TextEncryption.j;
                            SharedPreferences sharedPreferences = TextEncryption.e;
                            if (sharedPreferences != null) {
                                String string = sharedPreferences.getString("d", null);
                                byte[] decode = string != null ? Base64.decode(string, 0) : null;
                                String string2 = sharedPreferences.getString("e", null);
                                if (string2 != null) {
                                    bArr = Base64.decode(string2, 0);
                                }
                                if (decode == null || bArr == null) {
                                    byte[] encoded = Lx1.a.c().getEncoded();
                                    TextEncryption textEncryption4 = TextEncryption.j;
                                    Cipher instance = Cipher.getInstance(TextEncryption.c);
                                    try {
                                        instance.init(1, f);
                                        decode = instance.doFinal(encoded);
                                        Wg6.b(instance, "cipher");
                                        bArr = instance.getIV();
                                        sharedPreferences.edit().putString("d", Base64.encodeToString(decode, 0)).putString("e", Base64.encodeToString(bArr, 0)).apply();
                                    } catch (Throwable th) {
                                        TextEncryption.j.m();
                                    }
                                }
                                TextEncryption textEncryption5 = TextEncryption.j;
                                Cipher instance2 = Cipher.getInstance(TextEncryption.c);
                                instance2.init(2, f, new GCMParameterSpec(128, bArr));
                                try {
                                    TextEncryption textEncryption6 = TextEncryption.j;
                                    TextEncryption.h = instance2.doFinal(decode);
                                } catch (Throwable th2) {
                                    TextEncryption.j.m();
                                }
                            }
                        }
                    }
                    Cd6 cd6 = Cd6.a;
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        TextEncryption textEncryption = new TextEncryption();
        j = textEncryption;
        textEncryption.q();
    }
    */

    @DexIgnore
    public final void l() {
        SharedPreferences.Editor edit;
        SharedPreferences.Editor remove;
        SharedPreferences sharedPreferences = e;
        if (!(sharedPreferences == null || (edit = sharedPreferences.edit()) == null || (remove = edit.remove("b")) == null)) {
            remove.apply();
        }
        Ex1.b.c("a");
    }

    @DexIgnore
    public final void m() {
        SharedPreferences.Editor edit;
        SharedPreferences.Editor remove;
        SharedPreferences.Editor remove2;
        SharedPreferences sharedPreferences = e;
        if (!(sharedPreferences == null || (edit = sharedPreferences.edit()) == null || (remove = edit.remove("d")) == null || (remove2 = remove.remove("e")) == null)) {
            remove2.apply();
        }
        Ex1.b.c("c");
    }

    @DexIgnore
    public final String n(String str) {
        Wg6.c(str, "encryptedText");
        for (byte[] bArr : Em7.E(new byte[][]{h, f})) {
            try {
                Lx1 lx1 = Lx1.a;
                Lx1.Bi bi = a;
                byte[] bArr2 = b;
                byte[] decode = Base64.decode(str, 2);
                Wg6.b(decode, "Base64.decode(encryptedText, Base64.NO_WRAP)");
                return new String(lx1.a(bi, bArr, bArr2, decode), Dx1.a());
            } catch (Exception e2) {
            }
        }
        return null;
    }

    @DexIgnore
    public final String o(String str) {
        byte[] bArr;
        Wg6.c(str, "text");
        if (Build.VERSION.SDK_INT >= 23) {
            r();
            bArr = h;
        } else {
            p();
            bArr = f;
        }
        if (bArr == null) {
            return null;
        }
        try {
            Lx1 lx1 = Lx1.a;
            Lx1.Bi bi = a;
            byte[] bArr2 = b;
            byte[] bytes = str.getBytes(Dx1.a());
            Wg6.b(bytes, "(this as java.lang.String).getBytes(charset)");
            return Base64.encodeToString(lx1.b(bi, bArr, bArr2, bytes), 2);
        } catch (Exception e2) {
            return null;
        }
    }

    @DexIgnore
    public final void p() {
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Ai(null), 3, null);
    }

    @DexIgnore
    public final void q() {
        p();
        r();
    }

    @DexIgnore
    public final void r() {
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Bi(null), 3, null);
    }

    @DexIgnore
    public final void s(Context context) {
        Wg6.c(context, "context");
        d = context;
        e = context.getSharedPreferences("com.fossil.common.cipher.TextEncryption", 0);
    }
}
