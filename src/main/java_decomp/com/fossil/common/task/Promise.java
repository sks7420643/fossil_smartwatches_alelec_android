package com.fossil.common.task;

import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Promise<V, E> {
    @DexIgnore
    public V a;
    @DexIgnore
    public E b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public CopyOnWriteArrayList<Hg6<V, Cd6>> d; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<Hg6<E, Cd6>> e; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<Hg6<Cd6, Cd6>> f; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<Hg6<E, Cd6>> g; // = new CopyOnWriteArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Hg6 $action;
        @DexIgnore
        public /* final */ /* synthetic */ Object $error$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Hg6 hg6, Xe6 xe6, Object obj) {
            super(2, xe6);
            this.$action = hg6;
            this.$error$inlined = obj;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.$action, xe6, this.$error$inlined);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.$action.invoke(this.$error$inlined);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.fossil.common.task.Promise$finally$1", f = "Promise.kt", l = {}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Hg6 $actionOnFinal;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ Promise this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Promise promise, Hg6 hg6, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = promise;
            this.$actionOnFinal = hg6;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$actionOnFinal, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: java.util.concurrent.CopyOnWriteArrayList */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.f.add(this.$actionOnFinal);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.fossil.common.task.Promise$onError$1", f = "Promise.kt", l = {}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Hg6 $actionOnError;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ Promise this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(Promise promise, Hg6 hg6, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = promise;
            this.$actionOnError = hg6;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$actionOnError, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: java.util.concurrent.CopyOnWriteArrayList */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.e.add(this.$actionOnError);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.fossil.common.task.Promise$onSuccess$1", f = "Promise.kt", l = {}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Hg6 $actionOnSuccess;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ Promise this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(Promise promise, Hg6 hg6, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = promise;
            this.$actionOnSuccess = hg6;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$actionOnSuccess, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: java.util.concurrent.CopyOnWriteArrayList */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.d.add(this.$actionOnSuccess);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Hg6 $action;
        @DexIgnore
        public /* final */ /* synthetic */ Object $error$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(Hg6 hg6, Xe6 xe6, Object obj) {
            super(2, xe6);
            this.$action = hg6;
            this.$error$inlined = obj;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.$action, xe6, this.$error$inlined);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.$action.invoke(this.$error$inlined);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Hg6 $action;
        @DexIgnore
        public /* final */ /* synthetic */ Object $result$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(Hg6 hg6, Xe6 xe6, Object obj) {
            super(2, xe6);
            this.$action = hg6;
            this.$result$inlined = obj;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.$action, xe6, this.$result$inlined);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.$action.invoke(this.$result$inlined);
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public final void d(E e2) {
        synchronized (this) {
            Wg6.c(e2, "error");
            if (!g() && !this.c) {
                this.c = true;
                if (!this.g.isEmpty()) {
                    Iterator<T> it = this.g.iterator();
                    while (it.hasNext()) {
                        try {
                            Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Ai(it.next(), null, e2), 3, null);
                        } catch (Exception e3) {
                        }
                    }
                    this.c = false;
                } else {
                    n(e2);
                }
            }
        }
    }

    @DexIgnore
    public Promise<V, E> e(Hg6<? super Cd6, Cd6> hg6) {
        Wg6.c(hg6, "actionOnFinal");
        if (g()) {
            try {
                hg6.invoke(Cd6.a);
            } catch (Exception e2) {
            }
        } else {
            Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Bi(this, hg6, null), 3, null);
        }
        return this;
    }

    @DexIgnore
    public final V f() {
        return this.a;
    }

    @DexIgnore
    public final boolean g() {
        return i() || h();
    }

    @DexIgnore
    public final boolean h() {
        return this.b != null;
    }

    @DexIgnore
    public final boolean i() {
        return this.a != null;
    }

    @DexIgnore
    public void j() {
        synchronized (this) {
            Iterator<T> it = this.f.iterator();
            while (it.hasNext()) {
                try {
                    it.next().invoke(Cd6.a);
                } catch (Exception e2) {
                }
            }
            this.d.clear();
            this.e.clear();
            this.f.clear();
            this.g.clear();
        }
    }

    @DexIgnore
    public Promise<V, E> k(Hg6<? super E, Cd6> hg6) {
        Wg6.c(hg6, "actionOnCancel");
        if (!g()) {
            this.g.add(hg6);
        }
        return this;
    }

    @DexIgnore
    public Promise<V, E> l(Hg6<? super E, Cd6> hg6) {
        Wg6.c(hg6, "actionOnError");
        if (!g()) {
            Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Ci(this, hg6, null), 3, null);
        } else if (h()) {
            try {
                E e2 = this.b;
                if (e2 != null) {
                    hg6.invoke(e2);
                } else {
                    Wg6.i();
                    throw null;
                }
            } catch (Exception e3) {
            }
        }
        return this;
    }

    @DexIgnore
    public Promise<V, E> m(Hg6<? super V, Cd6> hg6) {
        Wg6.c(hg6, "actionOnSuccess");
        if (!g()) {
            Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Di(this, hg6, null), 3, null);
        } else if (i()) {
            try {
                V v = this.a;
                if (v != null) {
                    hg6.invoke(v);
                } else {
                    Wg6.i();
                    throw null;
                }
            } catch (Exception e2) {
            }
        }
        return this;
    }

    @DexIgnore
    public final void n(E e2) {
        synchronized (this) {
            Wg6.c(e2, "error");
            if (!g()) {
                this.b = e2;
                this.a = this.a;
                Iterator<T> it = this.e.iterator();
                while (it.hasNext()) {
                    try {
                        Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Ei(it.next(), null, e2), 3, null);
                    } catch (Exception e3) {
                    }
                }
                j();
            }
        }
    }

    @DexIgnore
    public final void o(V v) {
        synchronized (this) {
            Wg6.c(v, Constants.RESULT);
            if (!g()) {
                this.a = v;
                this.b = null;
                Iterator<T> it = this.d.iterator();
                while (it.hasNext()) {
                    try {
                        Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Fi(it.next(), null, v), 3, null);
                    } catch (Exception e2) {
                    }
                }
                j();
            }
        }
    }
}
