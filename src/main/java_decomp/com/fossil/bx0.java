package com.fossil;

import android.database.Cursor;
import com.fossil.Gu0;
import com.fossil.Nw0;
import com.mapped.Oh;
import com.mapped.Rh;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Bx0<T> extends Gu0<T> {
    @DexIgnore
    public /* final */ String mCountQuery;
    @DexIgnore
    public /* final */ Oh mDb;
    @DexIgnore
    public /* final */ boolean mInTransaction;
    @DexIgnore
    public /* final */ String mLimitOffsetQuery;
    @DexIgnore
    public /* final */ Nw0.Ci mObserver;
    @DexIgnore
    public /* final */ Rh mSourceQuery;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Nw0.Ci {
        @DexIgnore
        public Ai(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        @Override // com.fossil.Nw0.Ci
        public void onInvalidated(Set<String> set) {
            Bx0.this.invalidate();
        }
    }

    @DexIgnore
    public Bx0(Oh oh, Ox0 ox0, boolean z, String... strArr) {
        this(oh, Rh.j(ox0), z, strArr);
    }

    @DexIgnore
    public Bx0(Oh oh, Rh rh, boolean z, String... strArr) {
        this.mDb = oh;
        this.mSourceQuery = rh;
        this.mInTransaction = z;
        this.mCountQuery = "SELECT COUNT(*) FROM ( " + this.mSourceQuery.b() + " )";
        this.mLimitOffsetQuery = "SELECT * FROM ( " + this.mSourceQuery.b() + " ) LIMIT ? OFFSET ?";
        this.mObserver = new Ai(strArr);
        oh.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private Rh getSQLiteQuery(int i, int i2) {
        Rh f = Rh.f(this.mLimitOffsetQuery, this.mSourceQuery.a() + 2);
        f.h(this.mSourceQuery);
        f.bindLong(f.a() - 1, (long) i2);
        f.bindLong(f.a(), (long) i);
        return f;
    }

    @DexIgnore
    public abstract List<T> convertRows(Cursor cursor);

    @DexIgnore
    public int countItems() {
        int i = 0;
        Rh f = Rh.f(this.mCountQuery, this.mSourceQuery.a());
        f.h(this.mSourceQuery);
        Cursor query = this.mDb.query(f);
        try {
            if (query.moveToFirst()) {
                i = query.getInt(0);
            } else {
                query.close();
                f.m();
            }
            return i;
        } finally {
            query.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public boolean isInvalid() {
        this.mDb.getInvalidationTracker().i();
        return super.isInvalid();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004d  */
    @Override // com.fossil.Gu0
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadInitial(com.fossil.Gu0.Di r7, com.fossil.Gu0.Bi<T> r8) {
        /*
            r6 = this;
            r1 = 0
            java.util.List r4 = java.util.Collections.emptyList()
            com.mapped.Oh r0 = r6.mDb
            r0.beginTransaction()
            int r5 = r6.countItems()     // Catch:{ all -> 0x0055 }
            if (r5 == 0) goto L_0x0051
            int r0 = com.fossil.Gu0.computeInitialLoadPosition(r7, r5)     // Catch:{ all -> 0x0055 }
            int r2 = com.fossil.Gu0.computeInitialLoadSize(r7, r0, r5)     // Catch:{ all -> 0x0055 }
            com.mapped.Rh r2 = r6.getSQLiteQuery(r0, r2)     // Catch:{ all -> 0x0055 }
            com.mapped.Oh r3 = r6.mDb     // Catch:{ all -> 0x003f }
            android.database.Cursor r1 = r3.query(r2)     // Catch:{ all -> 0x003f }
            java.util.List r4 = r6.convertRows(r1)     // Catch:{ all -> 0x003f }
            com.mapped.Oh r3 = r6.mDb     // Catch:{ all -> 0x003f }
            r3.setTransactionSuccessful()     // Catch:{ all -> 0x003f }
            r3 = r1
        L_0x002c:
            if (r3 == 0) goto L_0x0031
            r3.close()
        L_0x0031:
            com.mapped.Oh r1 = r6.mDb
            r1.endTransaction()
            if (r2 == 0) goto L_0x003b
            r2.m()
        L_0x003b:
            r8.a(r4, r0, r5)
            return
        L_0x003f:
            r0 = move-exception
            r3 = r1
        L_0x0041:
            if (r3 == 0) goto L_0x0046
            r3.close()
        L_0x0046:
            com.mapped.Oh r1 = r6.mDb
            r1.endTransaction()
            if (r2 == 0) goto L_0x0050
            r2.m()
        L_0x0050:
            throw r0
        L_0x0051:
            r0 = 0
            r2 = r1
            r3 = r1
            goto L_0x002c
        L_0x0055:
            r0 = move-exception
            r2 = r1
            r3 = r1
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Bx0.loadInitial(com.fossil.Gu0$Di, com.fossil.Gu0$Bi):void");
    }

    @DexIgnore
    public List<T> loadRange(int i, int i2) {
        List<T> convertRows;
        Rh sQLiteQuery = getSQLiteQuery(i, i2);
        if (this.mInTransaction) {
            this.mDb.beginTransaction();
            Cursor cursor = null;
            try {
                cursor = this.mDb.query(sQLiteQuery);
                convertRows = convertRows(cursor);
                this.mDb.setTransactionSuccessful();
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
                this.mDb.endTransaction();
                sQLiteQuery.m();
            }
        } else {
            Cursor query = this.mDb.query(sQLiteQuery);
            try {
                convertRows = convertRows(query);
            } finally {
                query.close();
                sQLiteQuery.m();
            }
        }
        return convertRows;
    }

    @DexIgnore
    @Override // com.fossil.Gu0
    public void loadRange(Gu0.Gi gi, Gu0.Ei<T> ei) {
        ei.a(loadRange(gi.a, gi.b));
    }
}
