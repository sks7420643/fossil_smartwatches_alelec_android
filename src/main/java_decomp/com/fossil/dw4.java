package com.fossil;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.F57;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dw4 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ List<Dt4> a; // = new ArrayList();
    @DexIgnore
    public /* final */ int b; // = W6.d(PortfolioApp.get.instance(), 2131099689);
    @DexIgnore
    public /* final */ int c; // = W6.d(PortfolioApp.get.instance(), 2131099677);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ F57.Bi a;
        @DexIgnore
        public /* final */ Uy4 b; // = Uy4.d.b();
        @DexIgnore
        public /* final */ Pd5 c;
        @DexIgnore
        public /* final */ /* synthetic */ Dw4 d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Dw4 dw4, Pd5 pd5) {
            super(pd5.n());
            Wg6.c(pd5, "binding");
            this.d = dw4;
            this.c = pd5;
            F57.Bi f = F57.a().f();
            Wg6.b(f, "TextDrawable.builder().round()");
            this.a = f;
        }

        @DexIgnore
        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /* JADX WARNING: Removed duplicated region for block: B:181:0x050c  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x008f  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0092  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00a6  */
        /* JADX WARNING: Removed duplicated region for block: B:49:0x0131  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(com.fossil.Dt4 r12) {
            /*
            // Method dump skipped, instructions count: 1330
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Dw4.Ai.a(com.fossil.Dt4):void");
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public final void i(List<Dt4> list) {
        Wg6.c(list, "data");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        Wg6.c(viewHolder, "holder");
        ((Ai) viewHolder).a(this.a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Pd5 z = Pd5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemBcNotificationChalle\u2026tInflater, parent, false)");
        return new Ai(this, z);
    }
}
