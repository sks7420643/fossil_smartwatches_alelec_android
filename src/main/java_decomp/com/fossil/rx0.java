package com.fossil;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import com.mapped.Ji;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rx0 implements Ji {
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ Ji.Ai d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ Object f; // = new Object();
    @DexIgnore
    public Ai g;
    @DexIgnore
    public boolean h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends SQLiteOpenHelper {
        @DexIgnore
        public /* final */ Qx0[] b;
        @DexIgnore
        public /* final */ Ji.Ai c;
        @DexIgnore
        public boolean d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements DatabaseErrorHandler {
            @DexIgnore
            public /* final */ /* synthetic */ Ji.Ai a;
            @DexIgnore
            public /* final */ /* synthetic */ Qx0[] b;

            @DexIgnore
            public Aii(Ji.Ai ai, Qx0[] qx0Arr) {
                this.a = ai;
                this.b = qx0Arr;
            }

            @DexIgnore
            public void onCorruption(SQLiteDatabase sQLiteDatabase) {
                this.a.c(Ai.b(this.b, sQLiteDatabase));
            }
        }

        @DexIgnore
        public Ai(Context context, String str, Qx0[] qx0Arr, Ji.Ai ai) {
            super(context, str, null, ai.a, new Aii(ai, qx0Arr));
            this.c = ai;
            this.b = qx0Arr;
        }

        @DexIgnore
        public static Qx0 b(Qx0[] qx0Arr, SQLiteDatabase sQLiteDatabase) {
            Qx0 qx0 = qx0Arr[0];
            if (qx0 == null || !qx0.a(sQLiteDatabase)) {
                qx0Arr[0] = new Qx0(sQLiteDatabase);
            }
            return qx0Arr[0];
        }

        @DexIgnore
        public Qx0 a(SQLiteDatabase sQLiteDatabase) {
            return b(this.b, sQLiteDatabase);
        }

        @DexIgnore
        public Lx0 c() {
            Lx0 a2;
            synchronized (this) {
                this.d = false;
                SQLiteDatabase writableDatabase = super.getWritableDatabase();
                if (this.d) {
                    close();
                    a2 = c();
                } else {
                    a2 = a(writableDatabase);
                }
            }
            return a2;
        }

        @DexIgnore
        @Override // java.lang.AutoCloseable
        public void close() {
            synchronized (this) {
                super.close();
                this.b[0] = null;
            }
        }

        @DexIgnore
        public void onConfigure(SQLiteDatabase sQLiteDatabase) {
            this.c.b(a(sQLiteDatabase));
        }

        @DexIgnore
        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            this.c.d(a(sQLiteDatabase));
        }

        @DexIgnore
        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.d = true;
            this.c.e(a(sQLiteDatabase), i, i2);
        }

        @DexIgnore
        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (!this.d) {
                this.c.f(a(sQLiteDatabase));
            }
        }

        @DexIgnore
        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.d = true;
            this.c.g(a(sQLiteDatabase), i, i2);
        }
    }

    @DexIgnore
    public Rx0(Context context, String str, Ji.Ai ai, boolean z) {
        this.b = context;
        this.c = str;
        this.d = ai;
        this.e = z;
    }

    @DexIgnore
    public final Ai a() {
        Ai ai;
        synchronized (this.f) {
            if (this.g == null) {
                Qx0[] qx0Arr = new Qx0[1];
                if (Build.VERSION.SDK_INT < 23 || this.c == null || !this.e) {
                    this.g = new Ai(this.b, this.c, qx0Arr, this.d);
                } else {
                    this.g = new Ai(this.b, new File(this.b.getNoBackupFilesDir(), this.c).getAbsolutePath(), qx0Arr, this.d);
                }
                if (Build.VERSION.SDK_INT >= 16) {
                    this.g.setWriteAheadLoggingEnabled(this.h);
                }
            }
            ai = this.g;
        }
        return ai;
    }

    @DexIgnore
    @Override // java.io.Closeable, com.mapped.Ji, java.lang.AutoCloseable
    public void close() {
        a().close();
    }

    @DexIgnore
    @Override // com.mapped.Ji
    public String getDatabaseName() {
        return this.c;
    }

    @DexIgnore
    @Override // com.mapped.Ji
    public Lx0 getWritableDatabase() {
        return a().c();
    }

    @DexIgnore
    @Override // com.mapped.Ji
    public void setWriteAheadLoggingEnabled(boolean z) {
        synchronized (this.f) {
            if (this.g != null) {
                this.g.setWriteAheadLoggingEnabled(z);
            }
            this.h = z;
        }
    }
}
