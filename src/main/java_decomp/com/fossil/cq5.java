package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cq5 implements MembersInjector<Aq5> {
    @DexIgnore
    public static void a(Aq5 aq5, DianaNotificationComponent dianaNotificationComponent) {
        aq5.g = dianaNotificationComponent;
    }

    @DexIgnore
    public static void b(Aq5 aq5, An4 an4) {
        aq5.h = an4;
    }
}
