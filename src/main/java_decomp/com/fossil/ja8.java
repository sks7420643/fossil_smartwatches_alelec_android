package com.fossil;

import com.mapped.Wg6;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ja8 {
    @DexIgnore
    public /* final */ HashMap<String, Ka8> a; // = new HashMap<>();

    @DexIgnore
    public final void a() {
        this.a.clear();
    }

    @DexIgnore
    public final Ka8 b(String str) {
        Wg6.c(str, "id");
        return this.a.get(str);
    }

    @DexIgnore
    public final void c(Ka8 ka8) {
        Wg6.c(ka8, "assetEntity");
        this.a.put(ka8.e(), ka8);
    }
}
