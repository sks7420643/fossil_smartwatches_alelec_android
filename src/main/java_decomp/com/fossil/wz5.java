package com.fossil;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import com.mapped.Qg6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wz5 extends Kq0 implements View.OnClickListener {
    @DexIgnore
    public static /* final */ Ai i; // = new Ai(null);
    @DexIgnore
    public Bi b;
    @DexIgnore
    public String c;
    @DexIgnore
    public FlexibleTextView d;
    @DexIgnore
    public FlexibleEditText e;
    @DexIgnore
    public ImageView f;
    @DexIgnore
    public String g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Wz5 a(String str, Bi bi) {
            Wg6.c(bi, "listener");
            Wz5 wz5 = new Wz5();
            wz5.b = bi;
            wz5.c = str;
            wz5.g = str;
            return wz5;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(String str);

        @DexIgnore
        Object onCancel();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Wz5 b;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ci(Wz5 wz5) {
            this.b = wz5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            Wg6.c(editable, "text");
            Wz5 wz5 = this.b;
            String obj = editable.toString();
            int length = obj.length() - 1;
            boolean z = false;
            int i = 0;
            while (i <= length) {
                boolean z2 = obj.charAt(!z ? i : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            wz5.c = obj.subSequence(i, length + 1).toString();
            boolean z3 = !TextUtils.isEmpty(this.b.c);
            Wz5.x6(this.b).setEnabled(z3);
            if (z3) {
                Wz5.y6(this.b).setVisibility(0);
            } else {
                Wz5.y6(this.b).setVisibility(8);
            }
            if (!z3 || !(!Wg6.a(this.b.g, this.b.c))) {
                this.b.F6(false);
            } else {
                this.b.F6(true);
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Wg6.c(charSequence, "text");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Wg6.c(charSequence, "text");
        }
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView x6(Wz5 wz5) {
        FlexibleTextView flexibleTextView = wz5.d;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        Wg6.n("ftvRename");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ImageView y6(Wz5 wz5) {
        ImageView imageView = wz5.f;
        if (imageView != null) {
            return imageView;
        }
        Wg6.n("ivClearName");
        throw null;
    }

    @DexIgnore
    public final void E6(boolean z) {
        Bi bi = this.b;
        if (bi != null) {
            if (z) {
                bi.onCancel();
            } else {
                String str = this.c;
                if (str != null) {
                    bi.a(str);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }
        dismiss();
    }

    @DexIgnore
    public final void F6(boolean z) {
        FlexibleTextView flexibleTextView = this.d;
        if (flexibleTextView != null) {
            flexibleTextView.setEnabled(z);
            if (z) {
                FlexibleTextView flexibleTextView2 = this.d;
                if (flexibleTextView2 != null) {
                    flexibleTextView2.setTextColor(W6.d(requireContext(), 2131099968));
                    FlexibleTextView flexibleTextView3 = this.d;
                    if (flexibleTextView3 != null) {
                        flexibleTextView3.setBackgroundResource(2131230824);
                    } else {
                        Wg6.n("ftvRename");
                        throw null;
                    }
                } else {
                    Wg6.n("ftvRename");
                    throw null;
                }
            } else {
                FlexibleTextView flexibleTextView4 = this.d;
                if (flexibleTextView4 != null) {
                    flexibleTextView4.setTextColor(W6.d(requireContext(), 2131099844));
                    FlexibleTextView flexibleTextView5 = this.d;
                    if (flexibleTextView5 != null) {
                        flexibleTextView5.setBackgroundResource(2131230825);
                    } else {
                        Wg6.n("ftvRename");
                        throw null;
                    }
                } else {
                    Wg6.n("ftvRename");
                    throw null;
                }
            }
        } else {
            Wg6.n("ftvRename");
            throw null;
        }
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131362378) {
                E6(true);
            } else if (id != 2131362513) {
                if (id == 2131362684) {
                    FlexibleEditText flexibleEditText = this.e;
                    if (flexibleEditText != null) {
                        flexibleEditText.setText("");
                        F6(false);
                        return;
                    }
                    Wg6.n("fetRename");
                    throw null;
                }
            } else if (!TextUtils.isEmpty(this.c)) {
                E6(false);
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(1, 2131951890);
    }

    @DexIgnore
    @Override // com.fossil.Kq0
    public Dialog onCreateDialog(Bundle bundle) {
        Dialog onCreateDialog = super.onCreateDialog(bundle);
        Wg6.b(onCreateDialog, "super.onCreateDialog(savedInstanceState)");
        onCreateDialog.requestWindowFeature(1);
        Window window = onCreateDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(0));
            window.setLayout(-1, -1);
        }
        return onCreateDialog;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(2131558616, viewGroup);
        ViewGroup viewGroup2 = (ViewGroup) inflate.findViewById(2131363010);
        String d2 = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
        if (d2 != null) {
            viewGroup2.setBackgroundColor(Color.parseColor(d2));
        }
        View findViewById = inflate.findViewById(2131362301);
        Wg6.b(findViewById, "view.findViewById(R.id.fet_rename)");
        this.e = (FlexibleEditText) findViewById;
        View findViewById2 = inflate.findViewById(2131362513);
        Wg6.b(findViewById2, "view.findViewById(R.id.ftv_rename)");
        this.d = (FlexibleTextView) findViewById2;
        View findViewById3 = inflate.findViewById(2131362684);
        Wg6.b(findViewById3, "view.findViewById(R.id.iv_clear_name)");
        this.f = (ImageView) findViewById3;
        String str = this.c;
        if (str != null) {
            FlexibleEditText flexibleEditText = this.e;
            if (flexibleEditText != null) {
                flexibleEditText.setText(str);
                FlexibleEditText flexibleEditText2 = this.e;
                if (flexibleEditText2 != null) {
                    flexibleEditText2.setSelection(str.length());
                    if (str.length() == 0) {
                        ImageView imageView = this.f;
                        if (imageView != null) {
                            imageView.setVisibility(8);
                        } else {
                            Wg6.n("ivClearName");
                            throw null;
                        }
                    } else {
                        ImageView imageView2 = this.f;
                        if (imageView2 != null) {
                            imageView2.setVisibility(0);
                        } else {
                            Wg6.n("ivClearName");
                            throw null;
                        }
                    }
                } else {
                    Wg6.n("fetRename");
                    throw null;
                }
            } else {
                Wg6.n("fetRename");
                throw null;
            }
        }
        F6(false);
        FlexibleEditText flexibleEditText3 = this.e;
        if (flexibleEditText3 != null) {
            flexibleEditText3.addTextChangedListener(new Ci(this));
            ImageView imageView3 = this.f;
            if (imageView3 != null) {
                imageView3.setOnClickListener(this);
                FlexibleTextView flexibleTextView = this.d;
                if (flexibleTextView != null) {
                    flexibleTextView.setOnClickListener(this);
                    inflate.findViewById(2131362378).setOnClickListener(this);
                    return inflate;
                }
                Wg6.n("ftvRename");
                throw null;
            }
            Wg6.n("ivClearName");
            throw null;
        }
        Wg6.n("fetRename");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    public void v6() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
