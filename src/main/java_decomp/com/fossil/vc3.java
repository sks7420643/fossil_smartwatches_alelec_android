package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.Rg2;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vc3 extends As2 implements Yb3 {
    @DexIgnore
    public Vc3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
    }

    @DexIgnore
    @Override // com.fossil.Yb3
    public final Rg2 B2(LatLng latLng, float f) throws RemoteException {
        Parcel d = d();
        Es2.d(d, latLng);
        d.writeFloat(f);
        Parcel e = e(9, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Yb3
    public final Rg2 C2(float f, float f2) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        d.writeFloat(f2);
        Parcel e = e(3, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Yb3
    public final Rg2 J1(CameraPosition cameraPosition) throws RemoteException {
        Parcel d = d();
        Es2.d(d, cameraPosition);
        Parcel e = e(7, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Yb3
    public final Rg2 Z0(float f, int i, int i2) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        d.writeInt(i);
        d.writeInt(i2);
        Parcel e = e(6, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Yb3
    public final Rg2 c2() throws RemoteException {
        Parcel e = e(2, d());
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Yb3
    public final Rg2 g0(LatLng latLng) throws RemoteException {
        Parcel d = d();
        Es2.d(d, latLng);
        Parcel e = e(8, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Yb3
    public final Rg2 u(LatLngBounds latLngBounds, int i) throws RemoteException {
        Parcel d = d();
        Es2.d(d, latLngBounds);
        d.writeInt(i);
        Parcel e = e(10, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Yb3
    public final Rg2 v2(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        Parcel e = e(4, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Yb3
    public final Rg2 x(float f) throws RemoteException {
        Parcel d = d();
        d.writeFloat(f);
        Parcel e = e(5, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }

    @DexIgnore
    @Override // com.fossil.Yb3
    public final Rg2 z0() throws RemoteException {
        Parcel e = e(1, d());
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }
}
