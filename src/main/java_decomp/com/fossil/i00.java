package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class I00 extends Enum<I00> {
    @DexIgnore
    public static /* final */ I00 b;
    @DexIgnore
    public static /* final */ I00 c;
    @DexIgnore
    public static /* final */ I00 d;
    @DexIgnore
    public static /* final */ I00 e;
    @DexIgnore
    public static /* final */ I00 f;
    @DexIgnore
    public static /* final */ I00 g;
    @DexIgnore
    public static /* final */ /* synthetic */ I00[] h;

    /*
    static {
        I00 i00 = new I00("ENABLE_MAINTAINING_CONNECTION", 0);
        b = i00;
        I00 i002 = new I00("ENABLE_MAINTAINING_HID_CONNECTION", 1);
        I00 i003 = new I00("APP_DISCONNECT", 2);
        c = i003;
        I00 i004 = new I00("APP_DISCONNECT_HID", 3);
        I00 i005 = new I00("SET_SECRET_KEY", 4);
        d = i005;
        I00 i006 = new I00("DEVICE_STATE_CHANGED", 5);
        e = i006;
        I00 i007 = new I00("CLEAR_CACHE", 6);
        f = i007;
        I00 i008 = new I00("ENABLE_BACKGROUND_SYNC", 7);
        g = i008;
        h = new I00[]{i00, i002, i003, i004, i005, i006, i007, i008};
    }
    */

    @DexIgnore
    public I00(String str, int i) {
    }

    @DexIgnore
    public static I00 valueOf(String str) {
        return (I00) Enum.valueOf(I00.class, str);
    }

    @DexIgnore
    public static I00[] values() {
        return (I00[]) h.clone();
    }
}
