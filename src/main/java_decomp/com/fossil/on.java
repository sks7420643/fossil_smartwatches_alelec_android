package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Ix1;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class On extends Zj {
    @DexIgnore
    public /* final */ byte[] T;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ On(K5 k5, I60 i60, byte[] bArr, String str, int i) {
        super(k5, i60, Yp.C0, true, Ke.b.b(k5.x, Ob.w), bArr, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 8) != 0 ? E.a("UUID.randomUUID().toString()") : str, 64);
        this.T = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro, com.fossil.Mj
    public JSONObject C() {
        return G80.k(G80.k(super.C(), Jd0.F0, Integer.valueOf(this.T.length)), Jd0.U0, Long.valueOf(Ix1.a.b(this.T, Ix1.Ai.CRC32)));
    }
}
