package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ui7 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context b;
    @DexIgnore
    public /* final */ /* synthetic */ Jg7 c;

    @DexIgnore
    public Ui7(Context context, Jg7 jg7) {
        this.b = context;
        this.c = jg7;
    }

    @DexIgnore
    public final void run() {
        Context context = this.b;
        if (context == null) {
            Ig7.m.f("The Context of StatService.onPause() can not be null!");
        } else {
            Ig7.J(context, Ei7.D(context), this.c);
        }
    }
}
