package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ie1 {

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(Id1<?> id1);
    }

    @DexIgnore
    void a(int i);

    @DexIgnore
    Id1<?> b(Mb1 mb1, Id1<?> id1);

    @DexIgnore
    Id1<?> c(Mb1 mb1);

    @DexIgnore
    Object d();  // void declaration

    @DexIgnore
    void e(Ai ai);
}
