package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Rh4 implements Ht3 {
    @DexIgnore
    public /* final */ Sh4 a;
    @DexIgnore
    public /* final */ Intent b;

    @DexIgnore
    public Rh4(Sh4 sh4, Intent intent) {
        this.a = sh4;
        this.b = intent;
    }

    @DexIgnore
    @Override // com.fossil.Ht3
    public final void onComplete(Nt3 nt3) {
        this.a.f(this.b, nt3);
    }
}
