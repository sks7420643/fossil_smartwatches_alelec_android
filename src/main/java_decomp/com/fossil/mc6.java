package com.fossil;

import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.microapp.MicroAppPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mc6 implements Factory<MicroAppPresenter> {
    @DexIgnore
    public static MicroAppPresenter a(Jc6 jc6, CategoryRepository categoryRepository) {
        return new MicroAppPresenter(jc6, categoryRepository);
    }
}
