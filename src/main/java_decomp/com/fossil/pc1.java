package com.fossil;

import com.fossil.Af1;
import com.fossil.Sc1;
import com.fossil.Wb1;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pc1 implements Sc1, Wb1.Ai<Object> {
    @DexIgnore
    public /* final */ List<Mb1> b;
    @DexIgnore
    public /* final */ Tc1<?> c;
    @DexIgnore
    public /* final */ Sc1.Ai d;
    @DexIgnore
    public int e;
    @DexIgnore
    public Mb1 f;
    @DexIgnore
    public List<Af1<File, ?>> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public volatile Af1.Ai<?> i;
    @DexIgnore
    public File j;

    @DexIgnore
    public Pc1(Tc1<?> tc1, Sc1.Ai ai) {
        this(tc1.c(), tc1, ai);
    }

    @DexIgnore
    public Pc1(List<Mb1> list, Tc1<?> tc1, Sc1.Ai ai) {
        this.e = -1;
        this.b = list;
        this.c = tc1;
        this.d = ai;
    }

    @DexIgnore
    public final boolean a() {
        return this.h < this.g.size();
    }

    @DexIgnore
    @Override // com.fossil.Wb1.Ai
    public void b(Exception exc) {
        this.d.a(this.f, exc, this.i.c, Gb1.DATA_DISK_CACHE);
    }

    @DexIgnore
    @Override // com.fossil.Sc1
    public boolean c() {
        boolean z = false;
        while (true) {
            if (this.g == null || !a()) {
                int i2 = this.e + 1;
                this.e = i2;
                if (i2 >= this.b.size()) {
                    return false;
                }
                Mb1 mb1 = this.b.get(this.e);
                File b2 = this.c.d().b(new Qc1(mb1, this.c.o()));
                this.j = b2;
                if (b2 != null) {
                    this.f = mb1;
                    this.g = this.c.j(b2);
                    this.h = 0;
                }
            } else {
                this.i = null;
                while (!z && a()) {
                    List<Af1<File, ?>> list = this.g;
                    int i3 = this.h;
                    this.h = i3 + 1;
                    this.i = list.get(i3).b(this.j, this.c.s(), this.c.f(), this.c.k());
                    if (this.i != null && this.c.t(this.i.c.getDataClass())) {
                        this.i.c.d(this.c.l(), this);
                        z = true;
                    }
                }
                return z;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Sc1
    public void cancel() {
        Af1.Ai<?> ai = this.i;
        if (ai != null) {
            ai.c.cancel();
        }
    }

    @DexIgnore
    @Override // com.fossil.Wb1.Ai
    public void e(Object obj) {
        this.d.e(this.f, obj, this.i.c, Gb1.DATA_DISK_CACHE, this.f);
    }
}
