package com.fossil;

import android.content.Context;
import com.fossil.A74;
import com.fossil.Je4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ie4 implements Je4 {
    @DexIgnore
    public Ke4 a;

    @DexIgnore
    public Ie4(Context context) {
        this.a = Ke4.a(context);
    }

    @DexIgnore
    public static A74<Je4> b() {
        A74.Bi a2 = A74.a(Je4.class);
        a2.b(K74.f(Context.class));
        a2.f(He4.b());
        return a2.d();
    }

    @DexIgnore
    public static /* synthetic */ Je4 c(B74 b74) {
        return new Ie4((Context) b74.get(Context.class));
    }

    @DexIgnore
    @Override // com.fossil.Je4
    public Je4.Ai a(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        boolean c = this.a.c(str, currentTimeMillis);
        boolean b = this.a.b(currentTimeMillis);
        return (!c || !b) ? b ? Je4.Ai.GLOBAL : c ? Je4.Ai.SDK : Je4.Ai.NONE : Je4.Ai.COMBINED;
    }
}
