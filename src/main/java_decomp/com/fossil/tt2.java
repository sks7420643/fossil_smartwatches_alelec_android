package com.fossil;

import android.os.RemoteException;
import com.fossil.Zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Tt2 extends Zs2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ R93 g;
    @DexIgnore
    public /* final */ /* synthetic */ Zs2 h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Tt2(Zs2 zs2, String str, R93 r93) {
        super(zs2);
        this.h = zs2;
        this.f = str;
        this.g = r93;
    }

    @DexIgnore
    @Override // com.fossil.Zs2.Ai
    public final void a() throws RemoteException {
        this.h.h.getMaxUserProperties(this.f, this.g);
    }

    @DexIgnore
    @Override // com.fossil.Zs2.Ai
    public final void b() {
        this.g.c(null);
    }
}
