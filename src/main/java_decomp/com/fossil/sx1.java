package com.fossil;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sx1 extends IllegalArgumentException {
    @DexIgnore
    public /* final */ Ai errorCode;

    @DexIgnore
    public enum Ai {
        UNSUPPORTED_VERSION(1),
        INVALID_FILE_DATA(2);
        
        @DexIgnore
        public /* final */ int id;

        @DexIgnore
        public Ai(int i) {
            this.id = i;
        }

        @DexIgnore
        public final int getId() {
            return this.id;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Sx1(Ai ai, String str, Throwable th) {
        super(str, th);
        Wg6.c(ai, "errorCode");
        this.errorCode = ai;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Sx1(Ai ai, String str, Throwable th, int i, Qg6 qg6) {
        this(ai, (i & 2) != 0 ? null : str, (i & 4) != 0 ? null : th);
    }

    @DexIgnore
    public final Ai getErrorCode() {
        return this.errorCode;
    }
}
