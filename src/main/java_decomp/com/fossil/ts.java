package com.fossil;

import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ts extends Fs {
    @DexIgnore
    public Hg6<? super C2, Cd6> A;
    @DexIgnore
    public Coroutine<? super byte[], ? super N6, Cd6> B;
    @DexIgnore
    public long C;

    @DexIgnore
    public Ts(K5 k5) {
        super(Hs.l, k5, 0, 4);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void f(long j) {
        this.C = j;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void i(P7 p7) {
        if (p7.a == F5.b) {
            Wc.b.c(this.y.x);
        }
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void s(O7 o7) {
        Hg6<? super C2, Cd6> hg6;
        Object jSONObject;
        byte[] bArr = o7.b;
        N6 n6 = o7.a;
        Coroutine<? super byte[], ? super N6, Cd6> coroutine = this.B;
        if (coroutine != null) {
            coroutine.invoke(bArr, n6);
        }
        if (n6 == N6.l) {
            C2 a2 = Wc.b.a(this.y.x, bArr);
            if (a2 == null || a2.d) {
                D90.i.d(new A90("streaming", V80.f, this.y.x, this.c, this.d, true, null, null, null, G80.k(G80.k(new JSONObject(), Jd0.S0, Dy1.e(bArr, null, 1, null)), Jd0.l1, (a2 == null || (jSONObject = a2.toJSONObject()) == null) ? JSONObject.NULL : jSONObject), 448));
            }
            if (a2 != null && (hg6 = this.A) != null) {
                hg6.invoke(a2);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public U5 w() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public long x() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public void y() {
    }
}
