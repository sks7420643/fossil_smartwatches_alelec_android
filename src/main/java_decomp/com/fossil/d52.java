package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class D52 extends Wk2 implements C52 {
    @DexIgnore
    public D52() {
        super("com.google.android.gms.auth.api.signin.internal.IRevocationService");
    }

    @DexIgnore
    @Override // com.fossil.Wk2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            a();
            return true;
        } else if (i != 2) {
            return false;
        } else {
            p();
            return true;
        }
    }
}
