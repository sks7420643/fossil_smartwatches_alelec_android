package com.fossil;

import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bu5 implements Factory<GetAddress> {
    @DexIgnore
    public /* final */ Provider<GoogleApiService> a;

    @DexIgnore
    public Bu5(Provider<GoogleApiService> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static Bu5 a(Provider<GoogleApiService> provider) {
        return new Bu5(provider);
    }

    @DexIgnore
    public static GetAddress c(GoogleApiService googleApiService) {
        return new GetAddress(googleApiService);
    }

    @DexIgnore
    public GetAddress b() {
        return c(this.a.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
