package com.fossil;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Be4 implements Xd4<Be4> {
    @DexIgnore
    public static /* final */ Sd4<Object> e; // = Yd4.b();
    @DexIgnore
    public static /* final */ Ud4<String> f; // = Zd4.b();
    @DexIgnore
    public static /* final */ Ud4<Boolean> g; // = Ae4.b();
    @DexIgnore
    public static /* final */ Bi h; // = new Bi(null);
    @DexIgnore
    public /* final */ Map<Class<?>, Sd4<?>> a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, Ud4<?>> b; // = new HashMap();
    @DexIgnore
    public Sd4<Object> c; // = e;
    @DexIgnore
    public boolean d; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Pd4 {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.Pd4
        public void a(Object obj, Writer writer) throws IOException {
            Ce4 ce4 = new Ce4(writer, Be4.this.a, Be4.this.b, Be4.this.c, Be4.this.d);
            ce4.i(obj, false);
            ce4.r();
        }

        @DexIgnore
        @Override // com.fossil.Pd4
        public String b(Object obj) {
            StringWriter stringWriter = new StringWriter();
            try {
                a(obj, stringWriter);
            } catch (IOException e) {
            }
            return stringWriter.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Ud4<Date> {
        @DexIgnore
        public static /* final */ DateFormat a;

        /*
        static {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            a = simpleDateFormat;
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        */

        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public /* synthetic */ Bi(Ai ai) {
            this();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public /* bridge */ /* synthetic */ void a(Object obj, Vd4 vd4) throws IOException {
            b((Date) obj, vd4);
        }

        @DexIgnore
        public void b(Date date, Vd4 vd4) throws IOException {
            vd4.d(a.format(date));
        }
    }

    @DexIgnore
    public Be4() {
        m(String.class, f);
        m(Boolean.class, g);
        m(Date.class, h);
    }

    @DexIgnore
    public static /* synthetic */ void i(Object obj, Td4 td4) throws IOException {
        throw new Rd4("Couldn't find encoder for type " + obj.getClass().getCanonicalName());
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Xd4' to match base method */
    @Override // com.fossil.Xd4
    public /* bridge */ /* synthetic */ Be4 a(Class cls, Sd4 sd4) {
        l(cls, sd4);
        return this;
    }

    @DexIgnore
    public Pd4 f() {
        return new Ai();
    }

    @DexIgnore
    public Be4 g(Wd4 wd4) {
        wd4.a(this);
        return this;
    }

    @DexIgnore
    public Be4 h(boolean z) {
        this.d = z;
        return this;
    }

    @DexIgnore
    public <T> Be4 l(Class<T> cls, Sd4<? super T> sd4) {
        this.a.put(cls, sd4);
        this.b.remove(cls);
        return this;
    }

    @DexIgnore
    public <T> Be4 m(Class<T> cls, Ud4<? super T> ud4) {
        this.b.put(cls, ud4);
        this.a.remove(cls);
        return this;
    }
}
