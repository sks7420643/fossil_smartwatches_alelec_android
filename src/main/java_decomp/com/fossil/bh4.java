package com.fossil;

import android.text.TextUtils;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bh4 {
    @DexIgnore
    public static /* final */ long a; // = TimeUnit.HOURS.toSeconds(1);
    @DexIgnore
    public static /* final */ Pattern b; // = Pattern.compile("\\AA[\\w-]{38}\\z");

    @DexIgnore
    public static boolean c(String str) {
        return b.matcher(str).matches();
    }

    @DexIgnore
    public static boolean d(String str) {
        return str.contains(":");
    }

    @DexIgnore
    public long a() {
        return TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
    }

    @DexIgnore
    public boolean b(Fh4 fh4) {
        return TextUtils.isEmpty(fh4.b()) || fh4.h() + fh4.c() < a() + a;
    }
}
