package com.fossil;

import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import androidx.collection.SparseArrayCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.loader.app.LoaderManager;
import com.fossil.At0;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.reflect.Modifier;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Xs0 extends LoaderManager {
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public /* final */ LifecycleOwner a;
    @DexIgnore
    public /* final */ Ci b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<D> extends MutableLiveData<D> implements At0.Ci<D> {
        @DexIgnore
        public /* final */ int k;
        @DexIgnore
        public /* final */ Bundle l;
        @DexIgnore
        public /* final */ At0<D> m;
        @DexIgnore
        public LifecycleOwner n;
        @DexIgnore
        public Bi<D> o;
        @DexIgnore
        public At0<D> p;

        @DexIgnore
        public Ai(int i, Bundle bundle, At0<D> at0, At0<D> at02) {
            this.k = i;
            this.l = bundle;
            this.m = at0;
            this.p = at02;
            at0.registerListener(i, this);
        }

        @DexIgnore
        @Override // com.fossil.At0.Ci
        public void a(At0<D> at0, D d) {
            if (Xs0.c) {
                Log.v("LoaderManager", "onLoadComplete: " + this);
            }
            if (Looper.myLooper() == Looper.getMainLooper()) {
                o(d);
                return;
            }
            if (Xs0.c) {
                Log.w("LoaderManager", "onLoadComplete was incorrectly called on a background thread");
            }
            l(d);
        }

        @DexIgnore
        @Override // androidx.lifecycle.LiveData
        public void j() {
            if (Xs0.c) {
                Log.v("LoaderManager", "  Starting: " + this);
            }
            this.m.startLoading();
        }

        @DexIgnore
        @Override // androidx.lifecycle.LiveData
        public void k() {
            if (Xs0.c) {
                Log.v("LoaderManager", "  Stopping: " + this);
            }
            this.m.stopLoading();
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.Ls0<? super D> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // androidx.lifecycle.LiveData
        public void m(Ls0<? super D> ls0) {
            super.m(ls0);
            this.n = null;
            this.o = null;
        }

        @DexIgnore
        @Override // androidx.lifecycle.MutableLiveData, androidx.lifecycle.LiveData
        public void o(D d) {
            super.o(d);
            At0<D> at0 = this.p;
            if (at0 != null) {
                at0.reset();
                this.p = null;
            }
        }

        @DexIgnore
        public At0<D> p(boolean z) {
            if (Xs0.c) {
                Log.v("LoaderManager", "  Destroying: " + this);
            }
            this.m.cancelLoad();
            this.m.abandon();
            Bi<D> bi = this.o;
            if (bi != null) {
                m(bi);
                if (z) {
                    bi.c();
                }
            }
            this.m.unregisterListener(this);
            if ((bi == null || bi.b()) && !z) {
                return this.m;
            }
            this.m.reset();
            return this.p;
        }

        @DexIgnore
        public void q(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            printWriter.print(str);
            printWriter.print("mId=");
            printWriter.print(this.k);
            printWriter.print(" mArgs=");
            printWriter.println(this.l);
            printWriter.print(str);
            printWriter.print("mLoader=");
            printWriter.println(this.m);
            At0<D> at0 = this.m;
            at0.dump(str + "  ", fileDescriptor, printWriter, strArr);
            if (this.o != null) {
                printWriter.print(str);
                printWriter.print("mCallbacks=");
                printWriter.println(this.o);
                Bi<D> bi = this.o;
                bi.a(str + "  ", printWriter);
            }
            printWriter.print(str);
            printWriter.print("mData=");
            printWriter.println(r().dataToString((D) e()));
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.println(g());
        }

        @DexIgnore
        public At0<D> r() {
            return this.m;
        }

        @DexIgnore
        public void s() {
            LifecycleOwner lifecycleOwner = this.n;
            Bi<D> bi = this.o;
            if (lifecycleOwner != null && bi != null) {
                super.m(bi);
                h(lifecycleOwner, bi);
            }
        }

        @DexIgnore
        public At0<D> t(LifecycleOwner lifecycleOwner, LoaderManager.a<D> aVar) {
            Bi<D> bi = new Bi<>(this.m, aVar);
            h(lifecycleOwner, bi);
            Bi<D> bi2 = this.o;
            if (bi2 != null) {
                m(bi2);
            }
            this.n = lifecycleOwner;
            this.o = bi;
            return this.m;
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder(64);
            sb.append("LoaderInfo{");
            sb.append(Integer.toHexString(System.identityHashCode(this)));
            sb.append(" #");
            sb.append(this.k);
            sb.append(" : ");
            In0.a(this.m, sb);
            sb.append("}}");
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<D> implements Ls0<D> {
        @DexIgnore
        public /* final */ At0<D> a;
        @DexIgnore
        public /* final */ LoaderManager.a<D> b;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public Bi(At0<D> at0, LoaderManager.a<D> aVar) {
            this.a = at0;
            this.b = aVar;
        }

        @DexIgnore
        public void a(String str, PrintWriter printWriter) {
            printWriter.print(str);
            printWriter.print("mDeliveredData=");
            printWriter.println(this.c);
        }

        @DexIgnore
        public boolean b() {
            return this.c;
        }

        @DexIgnore
        public void c() {
            if (this.c) {
                if (Xs0.c) {
                    Log.v("LoaderManager", "  Resetting: " + this.a);
                }
                this.b.g(this.a);
            }
        }

        @DexIgnore
        @Override // com.fossil.Ls0
        public void onChanged(D d) {
            if (Xs0.c) {
                Log.v("LoaderManager", "  onLoadFinished in " + this.a + ": " + this.a.dataToString(d));
            }
            this.b.a(this.a, d);
            this.c = true;
        }

        @DexIgnore
        public String toString() {
            return this.b.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci extends Ts0 {
        @DexIgnore
        public static /* final */ ViewModelProvider.Factory c; // = new Aii();
        @DexIgnore
        public SparseArrayCompat<Ai> a; // = new SparseArrayCompat<>();
        @DexIgnore
        public boolean b; // = false;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements ViewModelProvider.Factory {
            @DexIgnore
            @Override // androidx.lifecycle.ViewModelProvider.Factory
            public <T extends Ts0> T create(Class<T> cls) {
                return new Ci();
            }
        }

        @DexIgnore
        public static Ci c(ViewModelStore viewModelStore) {
            return (Ci) new ViewModelProvider(viewModelStore, c).a(Ci.class);
        }

        @DexIgnore
        public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
            if (this.a.s() > 0) {
                printWriter.print(str);
                printWriter.println("Loaders:");
                String str2 = str + "    ";
                for (int i = 0; i < this.a.s(); i++) {
                    Ai t = this.a.t(i);
                    printWriter.print(str);
                    printWriter.print("  #");
                    printWriter.print(this.a.p(i));
                    printWriter.print(": ");
                    printWriter.println(t.toString());
                    t.q(str2, fileDescriptor, printWriter, strArr);
                }
            }
        }

        @DexIgnore
        public void b() {
            this.b = false;
        }

        @DexIgnore
        public <D> Ai<D> d(int i) {
            return this.a.j(i);
        }

        @DexIgnore
        public boolean e() {
            return this.b;
        }

        @DexIgnore
        public void f() {
            int s = this.a.s();
            for (int i = 0; i < s; i++) {
                this.a.t(i).s();
            }
        }

        @DexIgnore
        public void g(int i, Ai ai) {
            this.a.q(i, ai);
        }

        @DexIgnore
        public void h(int i) {
            this.a.r(i);
        }

        @DexIgnore
        public void i() {
            this.b = true;
        }

        @DexIgnore
        @Override // com.fossil.Ts0
        public void onCleared() {
            super.onCleared();
            int s = this.a.s();
            for (int i = 0; i < s; i++) {
                this.a.t(i).p(true);
            }
            this.a.e();
        }
    }

    @DexIgnore
    public Xs0(LifecycleOwner lifecycleOwner, ViewModelStore viewModelStore) {
        this.a = lifecycleOwner;
        this.b = Ci.c(viewModelStore);
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    public void a(int i) {
        if (this.b.e()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            if (c) {
                Log.v("LoaderManager", "destroyLoader in " + this + " of " + i);
            }
            Ai d = this.b.d(i);
            if (d != null) {
                d.p(true);
                this.b.h(i);
            }
        } else {
            throw new IllegalStateException("destroyLoader must be called on the main thread");
        }
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    @Deprecated
    public void b(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        this.b.a(str, fileDescriptor, printWriter, strArr);
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    public <D> At0<D> d(int i, Bundle bundle, LoaderManager.a<D> aVar) {
        if (this.b.e()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            Ai<D> d = this.b.d(i);
            if (c) {
                Log.v("LoaderManager", "initLoader in " + this + ": args=" + bundle);
            }
            if (d == null) {
                return g(i, bundle, aVar, null);
            }
            if (c) {
                Log.v("LoaderManager", "  Re-using existing loader " + d);
            }
            return d.t(this.a, aVar);
        } else {
            throw new IllegalStateException("initLoader must be called on the main thread");
        }
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    public void e() {
        this.b.f();
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager
    public <D> At0<D> f(int i, Bundle bundle, LoaderManager.a<D> aVar) {
        if (this.b.e()) {
            throw new IllegalStateException("Called while creating a loader");
        } else if (Looper.getMainLooper() == Looper.myLooper()) {
            if (c) {
                Log.v("LoaderManager", "restartLoader in " + this + ": args=" + bundle);
            }
            Ai<D> d = this.b.d(i);
            At0<D> at0 = null;
            if (d != null) {
                at0 = d.p(false);
            }
            return g(i, bundle, aVar, at0);
        } else {
            throw new IllegalStateException("restartLoader must be called on the main thread");
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final <D> At0<D> g(int i, Bundle bundle, LoaderManager.a<D> aVar, At0<D> at0) {
        try {
            this.b.i();
            At0<D> d = aVar.d(i, bundle);
            if (d == null) {
                throw new IllegalArgumentException("Object returned from onCreateLoader must not be null");
            } else if (!d.getClass().isMemberClass() || Modifier.isStatic(d.getClass().getModifiers())) {
                Ai ai = new Ai(i, bundle, d, at0);
                if (c) {
                    Log.v("LoaderManager", "  Created new loader " + ai);
                }
                this.b.g(i, ai);
                this.b.b();
                return ai.t(this.a, aVar);
            } else {
                throw new IllegalArgumentException("Object returned from onCreateLoader must not be a non-static inner member class: " + d);
            }
        } catch (Throwable th) {
            this.b.b();
            throw th;
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(128);
        sb.append("LoaderManager{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append(" in ");
        In0.a(this.a, sb);
        sb.append("}}");
        return sb.toString();
    }
}
