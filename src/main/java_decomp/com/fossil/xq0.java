package com.fossil;

import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import java.lang.reflect.Modifier;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Xq0 {
    @DexIgnore
    public ArrayList<Ai> a; // = new ArrayList<>();
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public String i;
    @DexIgnore
    public int j;
    @DexIgnore
    public CharSequence k;
    @DexIgnore
    public int l;
    @DexIgnore
    public CharSequence m;
    @DexIgnore
    public ArrayList<String> n;
    @DexIgnore
    public ArrayList<String> o;
    @DexIgnore
    public boolean p; // = false;
    @DexIgnore
    public ArrayList<Runnable> q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public int a;
        @DexIgnore
        public Fragment b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;
        @DexIgnore
        public Lifecycle.State g;
        @DexIgnore
        public Lifecycle.State h;

        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public Ai(int i, Fragment fragment) {
            this.a = i;
            this.b = fragment;
            Lifecycle.State state = Lifecycle.State.RESUMED;
            this.g = state;
            this.h = state;
        }

        @DexIgnore
        public Ai(int i, Fragment fragment, Lifecycle.State state) {
            this.a = i;
            this.b = fragment;
            this.g = fragment.mMaxState;
            this.h = state;
        }
    }

    @DexIgnore
    public Xq0(Nq0 nq0, ClassLoader classLoader) {
    }

    @DexIgnore
    public Xq0 b(int i2, Fragment fragment, String str) {
        n(i2, fragment, str, 1);
        return this;
    }

    @DexIgnore
    public Xq0 c(ViewGroup viewGroup, Fragment fragment, String str) {
        fragment.mContainer = viewGroup;
        b(viewGroup.getId(), fragment, str);
        return this;
    }

    @DexIgnore
    public Xq0 d(Fragment fragment, String str) {
        n(0, fragment, str, 1);
        return this;
    }

    @DexIgnore
    public void e(Ai ai) {
        this.a.add(ai);
        ai.c = this.b;
        ai.d = this.c;
        ai.e = this.d;
        ai.f = this.e;
    }

    @DexIgnore
    public Xq0 f(String str) {
        if (this.h) {
            this.g = true;
            this.i = str;
            return this;
        }
        throw new IllegalStateException("This FragmentTransaction is not allowed to be added to the back stack.");
    }

    @DexIgnore
    public Xq0 g(Fragment fragment) {
        e(new Ai(7, fragment));
        return this;
    }

    @DexIgnore
    public abstract int h();

    @DexIgnore
    public abstract int i();

    @DexIgnore
    public abstract void j();

    @DexIgnore
    public abstract void k();

    @DexIgnore
    public Xq0 l(Fragment fragment) {
        e(new Ai(6, fragment));
        return this;
    }

    @DexIgnore
    public Xq0 m() {
        if (!this.g) {
            this.h = false;
            return this;
        }
        throw new IllegalStateException("This transaction is already being added to the back stack");
    }

    @DexIgnore
    public void n(int i2, Fragment fragment, String str, int i3) {
        Class<?> cls = fragment.getClass();
        int modifiers = cls.getModifiers();
        if (cls.isAnonymousClass() || !Modifier.isPublic(modifiers) || (cls.isMemberClass() && !Modifier.isStatic(modifiers))) {
            throw new IllegalStateException("Fragment " + cls.getCanonicalName() + " must be a public static class to be  properly recreated from instance state.");
        }
        if (str != null) {
            String str2 = fragment.mTag;
            if (str2 == null || str.equals(str2)) {
                fragment.mTag = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.mTag + " now " + str);
            }
        }
        if (i2 != 0) {
            if (i2 != -1) {
                int i4 = fragment.mFragmentId;
                if (i4 == 0 || i4 == i2) {
                    fragment.mFragmentId = i2;
                    fragment.mContainerId = i2;
                } else {
                    throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.mFragmentId + " now " + i2);
                }
            } else {
                throw new IllegalArgumentException("Can't add fragment " + fragment + " with tag " + str + " to container view with no id");
            }
        }
        e(new Ai(i3, fragment));
    }

    @DexIgnore
    public Xq0 o(Fragment fragment) {
        e(new Ai(4, fragment));
        return this;
    }

    @DexIgnore
    public abstract boolean p();

    @DexIgnore
    public Xq0 q(Fragment fragment) {
        e(new Ai(3, fragment));
        return this;
    }

    @DexIgnore
    public Xq0 r(int i2, Fragment fragment) {
        s(i2, fragment, null);
        return this;
    }

    @DexIgnore
    public Xq0 s(int i2, Fragment fragment, String str) {
        if (i2 != 0) {
            n(i2, fragment, str, 2);
            return this;
        }
        throw new IllegalArgumentException("Must use non-zero containerViewId");
    }

    @DexIgnore
    public Xq0 t(Fragment fragment, Lifecycle.State state) {
        e(new Ai(10, fragment, state));
        return this;
    }

    @DexIgnore
    public Xq0 u(boolean z) {
        this.p = z;
        return this;
    }

    @DexIgnore
    public Xq0 v(Fragment fragment) {
        e(new Ai(5, fragment));
        return this;
    }
}
