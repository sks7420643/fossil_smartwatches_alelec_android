package com.fossil;

import com.fossil.P28;
import java.lang.ref.Reference;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F18 {
    @DexIgnore
    public static /* final */ Executor g; // = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), B28.G("OkHttp ConnectionPool", true));
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ Runnable c;
    @DexIgnore
    public /* final */ Deque<L28> d;
    @DexIgnore
    public /* final */ M28 e;
    @DexIgnore
    public boolean f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void run() {
            while (true) {
                long a2 = F18.this.a(System.nanoTime());
                if (a2 != -1) {
                    if (a2 > 0) {
                        long j = a2 / 1000000;
                        synchronized (F18.this) {
                            try {
                                F18.this.wait(j, (int) (a2 - (1000000 * j)));
                            } catch (InterruptedException e) {
                            }
                        }
                    }
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public F18() {
        this(5, 5, TimeUnit.MINUTES);
    }

    @DexIgnore
    public F18(int i, long j, TimeUnit timeUnit) {
        this.c = new Ai();
        this.d = new ArrayDeque();
        this.e = new M28();
        this.a = i;
        this.b = timeUnit.toNanos(j);
        if (j <= 0) {
            throw new IllegalArgumentException("keepAliveDuration <= 0: " + j);
        }
    }

    @DexIgnore
    public long a(long j) {
        synchronized (this) {
            L28 l28 = null;
            long j2 = Long.MIN_VALUE;
            int i = 0;
            int i2 = 0;
            for (L28 l282 : this.d) {
                if (e(l282, j) > 0) {
                    i++;
                } else {
                    i2++;
                    long j3 = j - l282.o;
                    if (j3 > j2) {
                        j2 = j3;
                        l28 = l282;
                    }
                }
            }
            if (j2 >= this.b || i2 > this.a) {
                this.d.remove(l28);
                B28.h(l28.r());
                return 0;
            } else if (i2 > 0) {
                return this.b - j2;
            } else if (i > 0) {
                return this.b;
            } else {
                this.f = false;
                return -1;
            }
        }
    }

    @DexIgnore
    public boolean b(L28 l28) {
        if (l28.k || this.a == 0) {
            this.d.remove(l28);
            return true;
        }
        notifyAll();
        return false;
    }

    @DexIgnore
    public Socket c(X08 x08, P28 p28) {
        for (L28 l28 : this.d) {
            if (l28.m(x08, null) && l28.o() && l28 != p28.d()) {
                return p28.m(l28);
            }
        }
        return null;
    }

    @DexIgnore
    public L28 d(X08 x08, P28 p28, X18 x18) {
        for (L28 l28 : this.d) {
            if (l28.m(x08, x18)) {
                p28.a(l28, true);
                return l28;
            }
        }
        return null;
    }

    @DexIgnore
    public final int e(L28 l28, long j) {
        List<Reference<P28>> list = l28.n;
        int i = 0;
        while (i < list.size()) {
            Reference<P28> reference = list.get(i);
            if (reference.get() != null) {
                i++;
            } else {
                W38.j().r("A connection to " + l28.q().a().l() + " was leaked. Did you forget to close a response body?", ((P28.Ai) reference).a);
                list.remove(i);
                l28.k = true;
                if (list.isEmpty()) {
                    l28.o = j - this.b;
                    return 0;
                }
            }
        }
        return list.size();
    }

    @DexIgnore
    public void f(L28 l28) {
        if (!this.f) {
            this.f = true;
            g.execute(this.c);
        }
        this.d.add(l28);
    }
}
