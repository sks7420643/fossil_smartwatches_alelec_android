package com.fossil;

import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qz4 {
    @DexIgnore
    public final String a(Date date) {
        try {
            SimpleDateFormat simpleDateFormat = TimeUtils.a.get();
            if (simpleDateFormat != null) {
                return simpleDateFormat.format(date);
            }
            Wg6.i();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("DateShortStringConverter", "fromOffsetDateTime - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public final Date b(String str) {
        try {
            SimpleDateFormat simpleDateFormat = TimeUtils.a.get();
            if (simpleDateFormat != null) {
                return simpleDateFormat.parse(str);
            }
            Wg6.i();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("DateShortStringConverter", "toOffsetDateTime - e=" + e);
            return null;
        }
    }
}
