package com.fossil;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Qj1<R> extends Ei1 {
    @DexIgnore
    void a(Pj1 pj1);

    @DexIgnore
    void b(R r, Tj1<? super R> tj1);

    @DexIgnore
    void d(Bj1 bj1);

    @DexIgnore
    void f(Drawable drawable);

    @DexIgnore
    void h(Drawable drawable);

    @DexIgnore
    Bj1 i();

    @DexIgnore
    void j(Drawable drawable);

    @DexIgnore
    void k(Pj1 pj1);
}
