package com.fossil;

import android.annotation.SuppressLint;
import android.os.Build;
import android.text.PrecomputedText;
import android.text.Spannable;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.MetricAffectingSpan;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class En0 implements Spannable {
    @DexIgnore
    public /* final */ Spannable b;
    @DexIgnore
    public /* final */ Ai c;
    @DexIgnore
    public /* final */ PrecomputedText d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ TextPaint a;
        @DexIgnore
        public /* final */ TextDirectionHeuristic b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ int d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static class Aii {
            @DexIgnore
            public /* final */ TextPaint a;
            @DexIgnore
            public TextDirectionHeuristic b;
            @DexIgnore
            public int c;
            @DexIgnore
            public int d;

            @DexIgnore
            public Aii(TextPaint textPaint) {
                this.a = textPaint;
                if (Build.VERSION.SDK_INT >= 23) {
                    this.c = 1;
                    this.d = 1;
                } else {
                    this.d = 0;
                    this.c = 0;
                }
                if (Build.VERSION.SDK_INT >= 18) {
                    this.b = TextDirectionHeuristics.FIRSTSTRONG_LTR;
                } else {
                    this.b = null;
                }
            }

            @DexIgnore
            public Ai a() {
                return new Ai(this.a, this.b, this.c, this.d);
            }

            @DexIgnore
            public Aii b(int i) {
                this.c = i;
                return this;
            }

            @DexIgnore
            public Aii c(int i) {
                this.d = i;
                return this;
            }

            @DexIgnore
            public Aii d(TextDirectionHeuristic textDirectionHeuristic) {
                this.b = textDirectionHeuristic;
                return this;
            }
        }

        @DexIgnore
        public Ai(PrecomputedText.Params params) {
            this.a = params.getTextPaint();
            this.b = params.getTextDirection();
            this.c = params.getBreakStrategy();
            this.d = params.getHyphenationFrequency();
            int i = Build.VERSION.SDK_INT;
        }

        @DexIgnore
        @SuppressLint({"NewApi"})
        public Ai(TextPaint textPaint, TextDirectionHeuristic textDirectionHeuristic, int i, int i2) {
            if (Build.VERSION.SDK_INT >= 29) {
                new PrecomputedText.Params.Builder(textPaint).setBreakStrategy(i).setHyphenationFrequency(i2).setTextDirection(textDirectionHeuristic).build();
            }
            this.a = textPaint;
            this.b = textDirectionHeuristic;
            this.c = i;
            this.d = i2;
        }

        @DexIgnore
        public boolean a(Ai ai) {
            if ((Build.VERSION.SDK_INT >= 23 && (this.c != ai.b() || this.d != ai.c())) || this.a.getTextSize() != ai.e().getTextSize() || this.a.getTextScaleX() != ai.e().getTextScaleX() || this.a.getTextSkewX() != ai.e().getTextSkewX()) {
                return false;
            }
            if ((Build.VERSION.SDK_INT >= 21 && (this.a.getLetterSpacing() != ai.e().getLetterSpacing() || !TextUtils.equals(this.a.getFontFeatureSettings(), ai.e().getFontFeatureSettings()))) || this.a.getFlags() != ai.e().getFlags()) {
                return false;
            }
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                if (!this.a.getTextLocales().equals(ai.e().getTextLocales())) {
                    return false;
                }
            } else if (i >= 17 && !this.a.getTextLocale().equals(ai.e().getTextLocale())) {
                return false;
            }
            if (this.a.getTypeface() == null) {
                return ai.e().getTypeface() == null;
            }
            if (!this.a.getTypeface().equals(ai.e().getTypeface())) {
                return false;
            }
        }

        @DexIgnore
        public int b() {
            return this.c;
        }

        @DexIgnore
        public int c() {
            return this.d;
        }

        @DexIgnore
        public TextDirectionHeuristic d() {
            return this.b;
        }

        @DexIgnore
        public TextPaint e() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof Ai)) {
                return false;
            }
            Ai ai = (Ai) obj;
            if (!a(ai)) {
                return false;
            }
            return Build.VERSION.SDK_INT < 18 || this.b == ai.d();
        }

        @DexIgnore
        public int hashCode() {
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                return Kn0.b(Float.valueOf(this.a.getTextSize()), Float.valueOf(this.a.getTextScaleX()), Float.valueOf(this.a.getTextSkewX()), Float.valueOf(this.a.getLetterSpacing()), Integer.valueOf(this.a.getFlags()), this.a.getTextLocales(), this.a.getTypeface(), Boolean.valueOf(this.a.isElegantTextHeight()), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else if (i >= 21) {
                return Kn0.b(Float.valueOf(this.a.getTextSize()), Float.valueOf(this.a.getTextScaleX()), Float.valueOf(this.a.getTextSkewX()), Float.valueOf(this.a.getLetterSpacing()), Integer.valueOf(this.a.getFlags()), this.a.getTextLocale(), this.a.getTypeface(), Boolean.valueOf(this.a.isElegantTextHeight()), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else if (i >= 18) {
                return Kn0.b(Float.valueOf(this.a.getTextSize()), Float.valueOf(this.a.getTextScaleX()), Float.valueOf(this.a.getTextSkewX()), Integer.valueOf(this.a.getFlags()), this.a.getTextLocale(), this.a.getTypeface(), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else if (i >= 17) {
                return Kn0.b(Float.valueOf(this.a.getTextSize()), Float.valueOf(this.a.getTextScaleX()), Float.valueOf(this.a.getTextSkewX()), Integer.valueOf(this.a.getFlags()), this.a.getTextLocale(), this.a.getTypeface(), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            } else {
                return Kn0.b(Float.valueOf(this.a.getTextSize()), Float.valueOf(this.a.getTextScaleX()), Float.valueOf(this.a.getTextSkewX()), Integer.valueOf(this.a.getFlags()), this.a.getTypeface(), this.b, Integer.valueOf(this.c), Integer.valueOf(this.d));
            }
        }

        @DexIgnore
        public String toString() {
            StringBuilder sb = new StringBuilder("{");
            sb.append("textSize=" + this.a.getTextSize());
            sb.append(", textScaleX=" + this.a.getTextScaleX());
            sb.append(", textSkewX=" + this.a.getTextSkewX());
            if (Build.VERSION.SDK_INT >= 21) {
                sb.append(", letterSpacing=" + this.a.getLetterSpacing());
                sb.append(", elegantTextHeight=" + this.a.isElegantTextHeight());
            }
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                sb.append(", textLocale=" + this.a.getTextLocales());
            } else if (i >= 17) {
                sb.append(", textLocale=" + this.a.getTextLocale());
            }
            sb.append(", typeface=" + this.a.getTypeface());
            if (Build.VERSION.SDK_INT >= 26) {
                sb.append(", variationSettings=" + this.a.getFontVariationSettings());
            }
            sb.append(", textDir=" + this.b);
            sb.append(", breakStrategy=" + this.c);
            sb.append(", hyphenationFrequency=" + this.d);
            sb.append("}");
            return sb.toString();
        }
    }

    @DexIgnore
    public Ai a() {
        return this.c;
    }

    @DexIgnore
    public PrecomputedText b() {
        Spannable spannable = this.b;
        if (spannable instanceof PrecomputedText) {
            return (PrecomputedText) spannable;
        }
        return null;
    }

    @DexIgnore
    public char charAt(int i) {
        return this.b.charAt(i);
    }

    @DexIgnore
    public int getSpanEnd(Object obj) {
        return this.b.getSpanEnd(obj);
    }

    @DexIgnore
    public int getSpanFlags(Object obj) {
        return this.b.getSpanFlags(obj);
    }

    @DexIgnore
    public int getSpanStart(Object obj) {
        return this.b.getSpanStart(obj);
    }

    @DexIgnore
    @Override // android.text.Spanned
    @SuppressLint({"NewApi"})
    public <T> T[] getSpans(int i, int i2, Class<T> cls) {
        return Build.VERSION.SDK_INT >= 29 ? (T[]) this.d.getSpans(i, i2, cls) : (T[]) this.b.getSpans(i, i2, cls);
    }

    @DexIgnore
    public int length() {
        return this.b.length();
    }

    @DexIgnore
    public int nextSpanTransition(int i, int i2, Class cls) {
        return this.b.nextSpanTransition(i, i2, cls);
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public void removeSpan(Object obj) {
        if (obj instanceof MetricAffectingSpan) {
            throw new IllegalArgumentException("MetricAffectingSpan can not be removed from PrecomputedText.");
        } else if (Build.VERSION.SDK_INT >= 29) {
            this.d.removeSpan(obj);
        } else {
            this.b.removeSpan(obj);
        }
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public void setSpan(Object obj, int i, int i2, int i3) {
        if (obj instanceof MetricAffectingSpan) {
            throw new IllegalArgumentException("MetricAffectingSpan can not be set to PrecomputedText.");
        } else if (Build.VERSION.SDK_INT >= 29) {
            this.d.setSpan(obj, i, i2, i3);
        } else {
            this.b.setSpan(obj, i, i2, i3);
        }
    }

    @DexIgnore
    public CharSequence subSequence(int i, int i2) {
        return this.b.subSequence(i, i2);
    }

    @DexIgnore
    public String toString() {
        return this.b.toString();
    }
}
