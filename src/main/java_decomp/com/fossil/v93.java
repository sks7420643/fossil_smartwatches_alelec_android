package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V93 extends Ss2 implements T93 {
    @DexIgnore
    public V93(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void beginAdUnitExposure(String str, long j) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeLong(j);
        i(23, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void clearConditionalUserProperty(String str, String str2, Bundle bundle) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        Qt2.c(d, bundle);
        i(9, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void endAdUnitExposure(String str, long j) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeLong(j);
        i(24, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void generateEventId(U93 u93) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, u93);
        i(22, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void getAppInstanceId(U93 u93) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, u93);
        i(20, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void getCachedAppInstanceId(U93 u93) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, u93);
        i(19, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void getConditionalUserProperties(String str, String str2, U93 u93) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        Qt2.b(d, u93);
        i(10, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void getCurrentScreenClass(U93 u93) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, u93);
        i(17, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void getCurrentScreenName(U93 u93) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, u93);
        i(16, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void getGmpAppId(U93 u93) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, u93);
        i(21, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void getMaxUserProperties(String str, U93 u93) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        Qt2.b(d, u93);
        i(6, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void getTestFlag(U93 u93, int i) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, u93);
        d.writeInt(i);
        i(38, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void getUserProperties(String str, String str2, boolean z, U93 u93) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        Qt2.d(d, z);
        Qt2.b(d, u93);
        i(5, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void initForTests(Map map) throws RemoteException {
        Parcel d = d();
        d.writeMap(map);
        i(37, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void initialize(Rg2 rg2, Xs2 xs2, long j) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, rg2);
        Qt2.c(d, xs2);
        d.writeLong(j);
        i(1, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void isDataCollectionEnabled(U93 u93) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, u93);
        i(40, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        Qt2.c(d, bundle);
        Qt2.d(d, z);
        Qt2.d(d, z2);
        d.writeLong(j);
        i(2, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void logEventAndBundle(String str, String str2, Bundle bundle, U93 u93, long j) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        Qt2.c(d, bundle);
        Qt2.b(d, u93);
        d.writeLong(j);
        i(3, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void logHealthData(int i, String str, Rg2 rg2, Rg2 rg22, Rg2 rg23) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        d.writeString(str);
        Qt2.b(d, rg2);
        Qt2.b(d, rg22);
        Qt2.b(d, rg23);
        i(33, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void onActivityCreated(Rg2 rg2, Bundle bundle, long j) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, rg2);
        Qt2.c(d, bundle);
        d.writeLong(j);
        i(27, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void onActivityDestroyed(Rg2 rg2, long j) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, rg2);
        d.writeLong(j);
        i(28, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void onActivityPaused(Rg2 rg2, long j) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, rg2);
        d.writeLong(j);
        i(29, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void onActivityResumed(Rg2 rg2, long j) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, rg2);
        d.writeLong(j);
        i(30, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void onActivitySaveInstanceState(Rg2 rg2, U93 u93, long j) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, rg2);
        Qt2.b(d, u93);
        d.writeLong(j);
        i(31, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void onActivityStarted(Rg2 rg2, long j) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, rg2);
        d.writeLong(j);
        i(25, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void onActivityStopped(Rg2 rg2, long j) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, rg2);
        d.writeLong(j);
        i(26, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void performAction(Bundle bundle, U93 u93, long j) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, bundle);
        Qt2.b(d, u93);
        d.writeLong(j);
        i(32, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void registerOnMeasurementEventListener(Us2 us2) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, us2);
        i(35, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void resetAnalyticsData(long j) throws RemoteException {
        Parcel d = d();
        d.writeLong(j);
        i(12, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void setConditionalUserProperty(Bundle bundle, long j) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, bundle);
        d.writeLong(j);
        i(8, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void setCurrentScreen(Rg2 rg2, String str, String str2, long j) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, rg2);
        d.writeString(str);
        d.writeString(str2);
        d.writeLong(j);
        i(15, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void setDataCollectionEnabled(boolean z) throws RemoteException {
        Parcel d = d();
        Qt2.d(d, z);
        i(39, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void setDefaultEventParameters(Bundle bundle) throws RemoteException {
        Parcel d = d();
        Qt2.c(d, bundle);
        i(42, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void setEventInterceptor(Us2 us2) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, us2);
        i(34, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void setInstanceIdProvider(Vs2 vs2) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, vs2);
        i(18, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void setMeasurementEnabled(boolean z, long j) throws RemoteException {
        Parcel d = d();
        Qt2.d(d, z);
        d.writeLong(j);
        i(11, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void setMinimumSessionDuration(long j) throws RemoteException {
        Parcel d = d();
        d.writeLong(j);
        i(13, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void setSessionTimeoutDuration(long j) throws RemoteException {
        Parcel d = d();
        d.writeLong(j);
        i(14, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void setUserId(String str, long j) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeLong(j);
        i(7, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void setUserProperty(String str, String str2, Rg2 rg2, boolean z, long j) throws RemoteException {
        Parcel d = d();
        d.writeString(str);
        d.writeString(str2);
        Qt2.b(d, rg2);
        Qt2.d(d, z);
        d.writeLong(j);
        i(4, d);
    }

    @DexIgnore
    @Override // com.fossil.T93
    public final void unregisterOnMeasurementEventListener(Us2 us2) throws RemoteException {
        Parcel d = d();
        Qt2.b(d, us2);
        i(36, d);
    }
}
