package com.fossil;

import com.mapped.An4;
import com.mapped.Cj4;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fo5 implements MembersInjector<NotificationReceiver> {
    @DexIgnore
    public static void a(NotificationReceiver notificationReceiver, Cj4 cj4) {
        notificationReceiver.b = cj4;
    }

    @DexIgnore
    public static void b(NotificationReceiver notificationReceiver, An4 an4) {
        notificationReceiver.a = an4;
    }

    @DexIgnore
    public static void c(NotificationReceiver notificationReceiver, UserRepository userRepository) {
        notificationReceiver.c = userRepository;
    }
}
