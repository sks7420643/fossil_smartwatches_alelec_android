package com.fossil;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vb2 implements Parcelable.Creator<DataHolder> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataHolder createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        int i = 0;
        int i2 = 0;
        Bundle bundle = null;
        CursorWindow[] cursorWindowArr = null;
        String[] strArr = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            int l = Ad2.l(t);
            if (l == 1) {
                strArr = Ad2.g(parcel, t);
            } else if (l == 2) {
                cursorWindowArr = (CursorWindow[]) Ad2.i(parcel, t, CursorWindow.CREATOR);
            } else if (l == 3) {
                i = Ad2.v(parcel, t);
            } else if (l == 4) {
                bundle = Ad2.a(parcel, t);
            } else if (l != 1000) {
                Ad2.B(parcel, t);
            } else {
                i2 = Ad2.v(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        DataHolder dataHolder = new DataHolder(i2, strArr, cursorWindowArr, i, bundle);
        dataHolder.L();
        return dataHolder;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ DataHolder[] newArray(int i) {
        return new DataHolder[i];
    }
}
