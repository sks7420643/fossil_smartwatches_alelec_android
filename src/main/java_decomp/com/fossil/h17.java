package com.fossil;

import com.mapped.An4;
import com.mapped.Cj4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase;
import com.portfolio.platform.ui.device.domain.usecase.SwitchActiveDeviceUseCase;
import com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class H17 implements Factory<WatchSettingViewModel> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<SetVibrationStrengthUseCase> b;
    @DexIgnore
    public /* final */ Provider<UnlinkDeviceUseCase> c;
    @DexIgnore
    public /* final */ Provider<Cj4> d;
    @DexIgnore
    public /* final */ Provider<An4> e;
    @DexIgnore
    public /* final */ Provider<SwitchActiveDeviceUseCase> f;
    @DexIgnore
    public /* final */ Provider<ReconnectDeviceUseCase> g;
    @DexIgnore
    public /* final */ Provider<Tt4> h;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> i;

    @DexIgnore
    public H17(Provider<DeviceRepository> provider, Provider<SetVibrationStrengthUseCase> provider2, Provider<UnlinkDeviceUseCase> provider3, Provider<Cj4> provider4, Provider<An4> provider5, Provider<SwitchActiveDeviceUseCase> provider6, Provider<ReconnectDeviceUseCase> provider7, Provider<Tt4> provider8, Provider<PortfolioApp> provider9) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
        this.i = provider9;
    }

    @DexIgnore
    public static H17 a(Provider<DeviceRepository> provider, Provider<SetVibrationStrengthUseCase> provider2, Provider<UnlinkDeviceUseCase> provider3, Provider<Cj4> provider4, Provider<An4> provider5, Provider<SwitchActiveDeviceUseCase> provider6, Provider<ReconnectDeviceUseCase> provider7, Provider<Tt4> provider8, Provider<PortfolioApp> provider9) {
        return new H17(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9);
    }

    @DexIgnore
    public static WatchSettingViewModel c(DeviceRepository deviceRepository, SetVibrationStrengthUseCase setVibrationStrengthUseCase, UnlinkDeviceUseCase unlinkDeviceUseCase, Cj4 cj4, An4 an4, SwitchActiveDeviceUseCase switchActiveDeviceUseCase, ReconnectDeviceUseCase reconnectDeviceUseCase, Tt4 tt4, PortfolioApp portfolioApp) {
        return new WatchSettingViewModel(deviceRepository, setVibrationStrengthUseCase, unlinkDeviceUseCase, cj4, an4, switchActiveDeviceUseCase, reconnectDeviceUseCase, tt4, portfolioApp);
    }

    @DexIgnore
    public WatchSettingViewModel b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
