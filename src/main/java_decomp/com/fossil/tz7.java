package com.fossil;

import com.mapped.Af6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Tz7<T> extends Au7<T> implements Do7 {
    @DexIgnore
    public /* final */ Xe6<T> e;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.mapped.Xe6<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public Tz7(Af6 af6, Xe6<? super T> xe6) {
        super(af6, true);
        this.e = xe6;
    }

    @DexIgnore
    @Override // com.fossil.Fx7
    public final boolean V() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Do7
    public final Do7 getCallerFrame() {
        return (Do7) this.e;
    }

    @DexIgnore
    @Override // com.fossil.Do7
    public final StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Fx7
    public void p(Object obj) {
        Wv7.b(Xn7.c(this.e), Wu7.a(obj, this.e));
    }

    @DexIgnore
    @Override // com.fossil.Au7
    public void u0(Object obj) {
        Xe6<T> xe6 = this.e;
        xe6.resumeWith(Wu7.a(obj, xe6));
    }
}
