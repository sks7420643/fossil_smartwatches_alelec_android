package com.fossil;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D46 extends U47 {
    @DexIgnore
    public static /* final */ String y;
    @DexIgnore
    public static /* final */ Ai z; // = new Ai(null);
    @DexIgnore
    public int k; // = -1;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public /* final */ Zp0 u; // = new Sr4(this);
    @DexIgnore
    public G37<T85> v;
    @DexIgnore
    public Bi w;
    @DexIgnore
    public HashMap x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return D46.y;
        }

        @DexIgnore
        public final D46 b() {
            return new D46();
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(int i, boolean z, boolean z2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ D46 b;

        @DexIgnore
        public Ci(D46 d46) {
            this.b = d46;
        }

        @DexIgnore
        public final void onClick(View view) {
            Dialog dialog = this.b.getDialog();
            if (dialog != null) {
                D46 d46 = this.b;
                Wg6.b(dialog, "it");
                d46.onDismiss(dialog);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ D46 b;
        @DexIgnore
        public /* final */ /* synthetic */ T85 c;

        @DexIgnore
        public Di(D46 d46, T85 t85) {
            this.b = d46;
            this.c = t85;
        }

        @DexIgnore
        public final void onClick(View view) {
            RTLImageView rTLImageView = this.c.v;
            Wg6.b(rTLImageView, "binding.ivCallsMessageCheck");
            if (rTLImageView.getVisibility() == 4) {
                RTLImageView rTLImageView2 = this.c.v;
                Wg6.b(rTLImageView2, "binding.ivCallsMessageCheck");
                rTLImageView2.setVisibility(0);
                RTLImageView rTLImageView3 = this.c.u;
                Wg6.b(rTLImageView3, "binding.ivCallsCheck");
                rTLImageView3.setVisibility(4);
                RTLImageView rTLImageView4 = this.c.w;
                Wg6.b(rTLImageView4, "binding.ivMessagesCheck");
                rTLImageView4.setVisibility(4);
                this.b.m = true;
                this.b.l = true;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ D46 b;
        @DexIgnore
        public /* final */ /* synthetic */ T85 c;

        @DexIgnore
        public Ei(D46 d46, T85 t85) {
            this.b = d46;
            this.c = t85;
        }

        @DexIgnore
        public final void onClick(View view) {
            RTLImageView rTLImageView = this.c.u;
            Wg6.b(rTLImageView, "binding.ivCallsCheck");
            if (rTLImageView.getVisibility() == 4) {
                RTLImageView rTLImageView2 = this.c.u;
                Wg6.b(rTLImageView2, "binding.ivCallsCheck");
                rTLImageView2.setVisibility(0);
                RTLImageView rTLImageView3 = this.c.v;
                Wg6.b(rTLImageView3, "binding.ivCallsMessageCheck");
                rTLImageView3.setVisibility(4);
                RTLImageView rTLImageView4 = this.c.w;
                Wg6.b(rTLImageView4, "binding.ivMessagesCheck");
                rTLImageView4.setVisibility(4);
                this.b.l = true;
                this.b.m = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ D46 b;
        @DexIgnore
        public /* final */ /* synthetic */ T85 c;

        @DexIgnore
        public Fi(D46 d46, T85 t85) {
            this.b = d46;
            this.c = t85;
        }

        @DexIgnore
        public final void onClick(View view) {
            RTLImageView rTLImageView = this.c.w;
            Wg6.b(rTLImageView, "binding.ivMessagesCheck");
            if (rTLImageView.getVisibility() == 4) {
                RTLImageView rTLImageView2 = this.c.w;
                Wg6.b(rTLImageView2, "binding.ivMessagesCheck");
                rTLImageView2.setVisibility(0);
                RTLImageView rTLImageView3 = this.c.v;
                Wg6.b(rTLImageView3, "binding.ivCallsMessageCheck");
                rTLImageView3.setVisibility(4);
                RTLImageView rTLImageView4 = this.c.u;
                Wg6.b(rTLImageView4, "binding.ivCallsCheck");
                rTLImageView4.setVisibility(4);
                this.b.m = true;
                this.b.l = false;
            }
        }
    }

    /*
    static {
        String simpleName = D46.class.getSimpleName();
        Wg6.b(simpleName, "NotificationAllowCallsAn\u2026nt::class.java.simpleName");
        y = simpleName;
    }
    */

    @DexIgnore
    public final void D6(Bi bi) {
        Wg6.c(bi, "onDismissListener");
        this.w = bi;
    }

    @DexIgnore
    public final void E6(int i, boolean z2, boolean z3) {
        this.k = i;
        this.l = z2;
        this.m = z3;
        this.s = z2;
        this.t = z3;
    }

    @DexIgnore
    public final void F6() {
        if (this.m && this.l) {
            G37<T85> g37 = this.v;
            if (g37 != null) {
                T85 a2 = g37.a();
                if (a2 != null) {
                    RTLImageView rTLImageView = a2.v;
                    Wg6.b(rTLImageView, "it.ivCallsMessageCheck");
                    rTLImageView.setVisibility(0);
                    RTLImageView rTLImageView2 = a2.u;
                    Wg6.b(rTLImageView2, "it.ivCallsCheck");
                    rTLImageView2.setVisibility(4);
                    RTLImageView rTLImageView3 = a2.w;
                    Wg6.b(rTLImageView3, "it.ivMessagesCheck");
                    rTLImageView3.setVisibility(4);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        } else if (this.l) {
            G37<T85> g372 = this.v;
            if (g372 != null) {
                T85 a3 = g372.a();
                if (a3 != null) {
                    RTLImageView rTLImageView4 = a3.v;
                    Wg6.b(rTLImageView4, "it.ivCallsMessageCheck");
                    rTLImageView4.setVisibility(4);
                    RTLImageView rTLImageView5 = a3.u;
                    Wg6.b(rTLImageView5, "it.ivCallsCheck");
                    rTLImageView5.setVisibility(0);
                    RTLImageView rTLImageView6 = a3.w;
                    Wg6.b(rTLImageView6, "it.ivMessagesCheck");
                    rTLImageView6.setVisibility(4);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        } else {
            G37<T85> g373 = this.v;
            if (g373 != null) {
                T85 a4 = g373.a();
                if (a4 != null) {
                    RTLImageView rTLImageView7 = a4.v;
                    Wg6.b(rTLImageView7, "it.ivCallsMessageCheck");
                    rTLImageView7.setVisibility(4);
                    RTLImageView rTLImageView8 = a4.u;
                    Wg6.b(rTLImageView8, "it.ivCallsCheck");
                    rTLImageView8.setVisibility(4);
                    RTLImageView rTLImageView9 = a4.w;
                    Wg6.b(rTLImageView9, "it.ivMessagesCheck");
                    rTLImageView9.setVisibility(0);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        T85 t85 = (T85) Aq0.f(layoutInflater, 2131558587, viewGroup, false, this.u);
        t85.x.setOnClickListener(new Ci(this));
        this.v = new G37<>(this, t85);
        F6();
        t85.r.setOnClickListener(new Di(this, t85));
        t85.q.setOnClickListener(new Ei(this, t85));
        t85.s.setOnClickListener(new Fi(this, t85));
        Wg6.b(t85, "binding");
        return t85.n();
    }

    @DexIgnore
    @Override // com.fossil.U47, androidx.fragment.app.Fragment, com.fossil.Kq0
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        z6();
    }

    @DexIgnore
    @Override // com.fossil.U47, com.fossil.Kq0
    public void onDismiss(DialogInterface dialogInterface) {
        Bi bi;
        Wg6.c(dialogInterface, "dialog");
        if (!((this.s == this.l && this.t == this.m) || (bi = this.w) == null)) {
            bi.a(this.k, this.l, this.m);
        }
        super.onDismiss(dialogInterface);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        F6();
    }

    @DexIgnore
    @Override // com.fossil.U47
    public void z6() {
        HashMap hashMap = this.x;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
