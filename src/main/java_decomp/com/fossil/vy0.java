package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.TypeEvaluator;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vy0 {
    @DexIgnore
    public static /* final */ boolean a; // = (Build.VERSION.SDK_INT >= 19);
    @DexIgnore
    public static /* final */ boolean b; // = (Build.VERSION.SDK_INT >= 18);
    @DexIgnore
    public static /* final */ boolean c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements TypeEvaluator<Matrix> {
        @DexIgnore
        public /* final */ float[] a; // = new float[9];
        @DexIgnore
        public /* final */ float[] b; // = new float[9];
        @DexIgnore
        public /* final */ Matrix c; // = new Matrix();

        @DexIgnore
        public Matrix a(float f, Matrix matrix, Matrix matrix2) {
            matrix.getValues(this.a);
            matrix2.getValues(this.b);
            for (int i = 0; i < 9; i++) {
                float[] fArr = this.b;
                float f2 = fArr[i];
                float[] fArr2 = this.a;
                fArr[i] = ((f2 - fArr2[i]) * f) + fArr2[i];
            }
            this.c.setValues(this.b);
            return this.c;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [float, java.lang.Object, java.lang.Object] */
        @Override // android.animation.TypeEvaluator
        public /* bridge */ /* synthetic */ Matrix evaluate(float f, Matrix matrix, Matrix matrix2) {
            return a(f, matrix, matrix2);
        }
    }

    /*
    static {
        boolean z = true;
        if (Build.VERSION.SDK_INT < 28) {
            z = false;
        }
        c = z;
    }
    */

    @DexIgnore
    public static View a(ViewGroup viewGroup, View view, View view2) {
        Matrix matrix = new Matrix();
        matrix.setTranslate((float) (-view2.getScrollX()), (float) (-view2.getScrollY()));
        Hz0.j(view, matrix);
        Hz0.k(viewGroup, matrix);
        RectF rectF = new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) view.getWidth(), (float) view.getHeight());
        matrix.mapRect(rectF);
        int round = Math.round(rectF.left);
        int round2 = Math.round(rectF.top);
        int round3 = Math.round(rectF.right);
        int round4 = Math.round(rectF.bottom);
        ImageView imageView = new ImageView(view.getContext());
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Bitmap b2 = b(view, matrix, rectF, viewGroup);
        if (b2 != null) {
            imageView.setImageBitmap(b2);
        }
        imageView.measure(View.MeasureSpec.makeMeasureSpec(round3 - round, 1073741824), View.MeasureSpec.makeMeasureSpec(round4 - round2, 1073741824));
        imageView.layout(round, round2, round3, round4);
        return imageView;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0097  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap b(android.view.View r8, android.graphics.Matrix r9, android.graphics.RectF r10, android.view.ViewGroup r11) {
        /*
            r4 = 0
            r1 = 0
            boolean r0 = com.fossil.Vy0.a
            if (r0 == 0) goto L_0x001f
            boolean r0 = r8.isAttachedToWindow()
            r0 = r0 ^ 1
            if (r11 != 0) goto L_0x0019
        L_0x000e:
            r2 = r1
            r3 = r0
        L_0x0010:
            boolean r0 = com.fossil.Vy0.b
            if (r0 == 0) goto L_0x0095
            if (r3 == 0) goto L_0x0095
            if (r2 != 0) goto L_0x0021
        L_0x0018:
            return r4
        L_0x0019:
            boolean r2 = r11.isAttachedToWindow()
            r3 = r0
            goto L_0x0010
        L_0x001f:
            r0 = r1
            goto L_0x000e
        L_0x0021:
            android.view.ViewParent r0 = r8.getParent()
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            int r1 = r0.indexOfChild(r8)
            android.view.ViewGroupOverlay r2 = r11.getOverlay()
            r2.add(r8)
        L_0x0032:
            float r2 = r10.width()
            int r2 = java.lang.Math.round(r2)
            float r5 = r10.height()
            int r5 = java.lang.Math.round(r5)
            if (r2 <= 0) goto L_0x0084
            if (r5 <= 0) goto L_0x0084
            r4 = 1065353216(0x3f800000, float:1.0)
            r6 = 1233125376(0x49800000, float:1048576.0)
            int r7 = r2 * r5
            float r7 = (float) r7
            float r6 = r6 / r7
            float r4 = java.lang.Math.min(r4, r6)
            float r2 = (float) r2
            float r2 = r2 * r4
            int r2 = java.lang.Math.round(r2)
            float r5 = (float) r5
            float r5 = r5 * r4
            int r5 = java.lang.Math.round(r5)
            float r6 = r10.left
            float r6 = -r6
            float r7 = r10.top
            float r7 = -r7
            r9.postTranslate(r6, r7)
            r9.postScale(r4, r4)
            boolean r4 = com.fossil.Vy0.c
            if (r4 == 0) goto L_0x0097
            android.graphics.Picture r4 = new android.graphics.Picture
            r4.<init>()
            android.graphics.Canvas r2 = r4.beginRecording(r2, r5)
            r2.concat(r9)
            r8.draw(r2)
            r4.endRecording()
            android.graphics.Bitmap r4 = android.graphics.Bitmap.createBitmap(r4)
        L_0x0084:
            boolean r2 = com.fossil.Vy0.b
            if (r2 == 0) goto L_0x0018
            if (r3 == 0) goto L_0x0018
            android.view.ViewGroupOverlay r2 = r11.getOverlay()
            r2.remove(r8)
            r0.addView(r8, r1)
            goto L_0x0018
        L_0x0095:
            r0 = r4
            goto L_0x0032
        L_0x0097:
            android.graphics.Bitmap$Config r4 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r4 = android.graphics.Bitmap.createBitmap(r2, r5, r4)
            android.graphics.Canvas r2 = new android.graphics.Canvas
            r2.<init>(r4)
            r2.concat(r9)
            r8.draw(r2)
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Vy0.b(android.view.View, android.graphics.Matrix, android.graphics.RectF, android.view.ViewGroup):android.graphics.Bitmap");
    }

    @DexIgnore
    public static Animator c(Animator animator, Animator animator2) {
        if (animator == null) {
            return animator2;
        }
        if (animator2 == null) {
            return animator;
        }
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(animator, animator2);
        return animatorSet;
    }
}
