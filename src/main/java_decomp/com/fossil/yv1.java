package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yv1 extends Sv1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public Bw1 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Yv1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Yv1 createFromParcel(Parcel parcel) {
            return new Yv1(parcel, (Qg6) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Yv1[] newArray(int i) {
            return new Yv1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Yv1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(Bw1.class.getClassLoader());
        if (readParcelable != null) {
            this.k = (Bw1) readParcelable;
        } else {
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    public Yv1(Jv1 jv1, Kv1 kv1, Bw1 bw1) {
        super(Vv1.SECOND_TIMEZONE, jv1, kv1, false, null, null, 56);
        this.k = bw1;
    }

    @DexIgnore
    public Yv1(JSONObject jSONObject, Cc0[] cc0Arr) throws IllegalArgumentException {
        super(jSONObject, cc0Arr, null, 4);
        JSONObject optJSONObject = jSONObject.optJSONObject("data");
        this.k = new Bw1(optJSONObject == null ? new JSONObject() : optJSONObject);
    }

    @DexIgnore
    @Override // com.fossil.Sv1, java.lang.Object, com.fossil.Mv1, com.fossil.Mv1
    public Yv1 clone() {
        return new Yv1(b().clone(), c().clone(), this.k.clone());
    }

    @DexIgnore
    @Override // com.fossil.Sv1
    public JSONObject e() {
        return this.k.a();
    }

    @DexIgnore
    public final Bw1 getTimeZoneDataConfig() {
        return this.k;
    }

    @DexIgnore
    @Override // com.fossil.Sv1
    public Yv1 setBackgroundImage(Tv1 tv1) {
        return (Yv1) super.setBackgroundImage(tv1);
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Yv1 setScaledHeight(float f) {
        Mv1 scaledHeight = super.setScaledHeight(f);
        if (scaledHeight != null) {
            return (Yv1) scaledHeight;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.SecondTimeZoneComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Yv1 setScaledPosition(Jv1 jv1) {
        Mv1 scaledPosition = super.setScaledPosition(jv1);
        if (scaledPosition != null) {
            return (Yv1) scaledPosition;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.SecondTimeZoneComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Yv1 setScaledSize(Kv1 kv1) {
        Mv1 scaledSize = super.setScaledSize(kv1);
        if (scaledSize != null) {
            return (Yv1) scaledSize;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.SecondTimeZoneComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Yv1 setScaledWidth(float f) {
        Mv1 scaledWidth = super.setScaledWidth(f);
        if (scaledWidth != null) {
            return (Yv1) scaledWidth;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.SecondTimeZoneComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Yv1 setScaledX(float f) {
        Mv1 scaledX = super.setScaledX(f);
        if (scaledX != null) {
            return (Yv1) scaledX;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.SecondTimeZoneComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Yv1 setScaledY(float f) {
        Mv1 scaledY = super.setScaledY(f);
        if (scaledY != null) {
            return (Yv1) scaledY;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.SecondTimeZoneComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.Sv1
    public Yv1 setTheme(Uv1 uv1) {
        Sv1 theme = super.setTheme(uv1);
        if (theme != null) {
            return (Yv1) theme;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.uiframework.element.classic.complication.SecondTimeZoneComplicationElement");
    }

    @DexIgnore
    @Override // com.fossil.Sv1, com.fossil.Mv1
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.k, i);
        }
    }
}
