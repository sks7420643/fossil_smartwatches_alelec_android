package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sv4 extends BaseFragment {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ Ai t; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public G37<N85> h;
    @DexIgnore
    public BCMemberInChallengeViewModel i;
    @DexIgnore
    public String j;
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public Xv4 l;
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Sv4.s;
        }

        @DexIgnore
        public final Sv4 b(String str, boolean z) {
            Sv4 sv4 = new Sv4();
            Bundle bundle = new Bundle();
            bundle.putString("challenge_id_extra", str);
            bundle.putBoolean("challenge_status_extra", z);
            sv4.setArguments(bundle);
            return sv4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Sv4 b;

        @DexIgnore
        public Bi(Sv4 sv4) {
            this.b = sv4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ Sv4 a;

        @DexIgnore
        public Ci(Sv4 sv4) {
            this.a = sv4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView;
            N85 n85 = (N85) Sv4.K6(this.a).a();
            if (!(n85 == null || (flexibleTextView = n85.q) == null)) {
                flexibleTextView.setVisibility(8);
            }
            Sv4.O6(this.a).j(this.a.j, this.a.k);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Sv4 a;

        @DexIgnore
        public Di(Sv4 sv4) {
            this.a = sv4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            N85 n85 = (N85) Sv4.K6(this.a).a();
            if (n85 != null) {
                SwipeRefreshLayout swipeRefreshLayout = n85.u;
                Wg6.b(swipeRefreshLayout, "swipe");
                Wg6.b(bool, "it");
                swipeRefreshLayout.setRefreshing(bool.booleanValue());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<List<? extends Object>> {
        @DexIgnore
        public /* final */ /* synthetic */ Sv4 a;

        @DexIgnore
        public Ei(Sv4 sv4) {
            this.a = sv4;
        }

        @DexIgnore
        public final void a(List<? extends Object> list) {
            Xv4 M6 = Sv4.M6(this.a);
            Wg6.b(list, "it");
            M6.g(list);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends Object> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi<T> implements Ls0<Lc6<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ Sv4 a;

        @DexIgnore
        public Fi(Sv4 sv4) {
            this.a = sv4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, ? extends ServerError> lc6) {
            if (lc6.getFirst().booleanValue()) {
                N85 n85 = (N85) Sv4.K6(this.a).a();
                if (n85 != null) {
                    FlexibleTextView flexibleTextView = n85.q;
                    Wg6.b(flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                return;
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                Toast.makeText(activity, Um5.c(activity, 2131886231), 1).show();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends ServerError> lc6) {
            a(lc6);
        }
    }

    /*
    static {
        String simpleName = Sv4.class.getSimpleName();
        Wg6.b(simpleName, "BCMemberInChallengeFragment::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 K6(Sv4 sv4) {
        G37<N85> g37 = sv4.h;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Xv4 M6(Sv4 sv4) {
        Xv4 xv4 = sv4.l;
        if (xv4 != null) {
            return xv4;
        }
        Wg6.n("memberAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ BCMemberInChallengeViewModel O6(Sv4 sv4) {
        BCMemberInChallengeViewModel bCMemberInChallengeViewModel = sv4.i;
        if (bCMemberInChallengeViewModel != null) {
            return bCMemberInChallengeViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void Q6() {
        Xv4 xv4 = new Xv4();
        this.l = xv4;
        if (xv4 != null) {
            xv4.setHasStableIds(true);
            G37<N85> g37 = this.h;
            if (g37 != null) {
                N85 a2 = g37.a();
                if (a2 != null) {
                    RecyclerView recyclerView = a2.t;
                    recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                    recyclerView.setHasFixedSize(true);
                    Xv4 xv42 = this.l;
                    if (xv42 != null) {
                        recyclerView.setAdapter(xv42);
                        a2.r.setOnClickListener(new Bi(this));
                        a2.u.setOnRefreshListener(new Ci(this));
                        return;
                    }
                    Wg6.n("memberAdapter");
                    throw null;
                }
                return;
            }
            Wg6.n("binding");
            throw null;
        }
        Wg6.n("memberAdapter");
        throw null;
    }

    @DexIgnore
    public final void R6() {
        BCMemberInChallengeViewModel bCMemberInChallengeViewModel = this.i;
        if (bCMemberInChallengeViewModel != null) {
            bCMemberInChallengeViewModel.g().h(getViewLifecycleOwner(), new Di(this));
            BCMemberInChallengeViewModel bCMemberInChallengeViewModel2 = this.i;
            if (bCMemberInChallengeViewModel2 != null) {
                bCMemberInChallengeViewModel2.h().h(getViewLifecycleOwner(), new Ei(this));
                BCMemberInChallengeViewModel bCMemberInChallengeViewModel3 = this.i;
                if (bCMemberInChallengeViewModel3 != null) {
                    bCMemberInChallengeViewModel3.f().h(getViewLifecycleOwner(), new Fi(this));
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().D1().a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCMemberInChallengeViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026ngeViewModel::class.java)");
            this.i = (BCMemberInChallengeViewModel) a2;
            Bundle arguments = getArguments();
            this.j = arguments != null ? arguments.getString("challenge_id_extra") : null;
            Bundle arguments2 = getArguments();
            this.k = arguments2 != null ? arguments2.getBoolean("challenge_status_extra") : true;
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        N85 n85 = (N85) Aq0.f(layoutInflater, 2131558584, viewGroup, false, A6());
        this.h = new G37<>(this, n85);
        Wg6.b(n85, "binding");
        return n85.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        BCMemberInChallengeViewModel bCMemberInChallengeViewModel = this.i;
        if (bCMemberInChallengeViewModel != null) {
            bCMemberInChallengeViewModel.j(this.j, this.k);
            Q6();
            R6();
            return;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
