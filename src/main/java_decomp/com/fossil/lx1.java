package com.fossil;

import com.mapped.Wg6;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lx1 {
    @DexIgnore
    public static /* final */ Lx1 a; // = new Lx1();

    @DexIgnore
    public enum Ai {
        ENCRYPT(1),
        DECRYPT(2);
        
        @DexIgnore
        public /* final */ int cipherOptMode;

        @DexIgnore
        public Ai(int i) {
            this.cipherOptMode = i;
        }

        @DexIgnore
        public final int getCipherOptMode() {
            return this.cipherOptMode;
        }
    }

    @DexIgnore
    public enum Bi {
        CBC_PKCS5_PADDING("AES/CBC/PKCS5PADDING"),
        CBC_NO_PADDING("AES/CBC/NoPadding"),
        CTR_NO_PADDING("AES/CTR/NoPadding"),
        GCM_NO_PADDING("AES/GCM/NoPadding");
        
        @DexIgnore
        public /* final */ String value;

        @DexIgnore
        public Bi(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public final byte[] a(Bi bi, byte[] bArr, byte[] bArr2, byte[] bArr3) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        Wg6.c(bi, "transformation");
        Wg6.c(bArr, "rawKey");
        Wg6.c(bArr2, "iv");
        Wg6.c(bArr3, "data");
        return d(Ai.DECRYPT, bi, bArr, bArr2, bArr3);
    }

    @DexIgnore
    public final byte[] b(Bi bi, byte[] bArr, byte[] bArr2, byte[] bArr3) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        Wg6.c(bi, "transformation");
        Wg6.c(bArr, "rawKey");
        Wg6.c(bArr2, "iv");
        Wg6.c(bArr3, "data");
        return d(Ai.ENCRYPT, bi, bArr, bArr2, bArr3);
    }

    @DexIgnore
    public final SecretKey c() throws NoSuchAlgorithmException {
        SecureRandom secureRandom = new SecureRandom();
        KeyGenerator instance = KeyGenerator.getInstance("AES");
        instance.init(256, secureRandom);
        SecretKey generateKey = instance.generateKey();
        Wg6.b(generateKey, "keyGenerator.generateKey()");
        return generateKey;
    }

    @DexIgnore
    public final byte[] d(Ai ai, Bi bi, byte[] bArr, byte[] bArr2, byte[] bArr3) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchProviderException {
        Wg6.c(ai, "operation");
        Wg6.c(bi, "transformation");
        Wg6.c(bArr, "rawKey");
        Wg6.c(bArr2, "iv");
        Wg6.c(bArr3, "data");
        SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
        Cipher instance = Cipher.getInstance(bi.getValue());
        instance.init(ai.getCipherOptMode(), secretKeySpec, new IvParameterSpec(bArr2));
        byte[] doFinal = instance.doFinal(bArr3);
        Wg6.b(doFinal, "cipher.doFinal(data)");
        return doFinal;
    }
}
