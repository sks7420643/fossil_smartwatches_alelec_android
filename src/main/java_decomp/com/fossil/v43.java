package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class V43 implements Xw2<Y43> {
    @DexIgnore
    public static V43 c; // = new V43();
    @DexIgnore
    public /* final */ Xw2<Y43> b;

    @DexIgnore
    public V43() {
        this(Ww2.b(new X43()));
    }

    @DexIgnore
    public V43(Xw2<Y43> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((Y43) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ Y43 zza() {
        return this.b.zza();
    }
}
