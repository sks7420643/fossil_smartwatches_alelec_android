package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cj5 extends Nc7 {
    @DexIgnore
    public /* final */ Handler i; // = new Handler(Looper.getMainLooper());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Object b;

        @DexIgnore
        public Ai(Object obj) {
            this.b = obj;
        }

        @DexIgnore
        public void run() {
            Cj5.super.i(this.b);
        }
    }

    @DexIgnore
    public Cj5(Uc7 uc7) {
        super(uc7);
    }

    @DexIgnore
    @Override // com.fossil.Nc7
    public void i(Object obj) {
        if (Objects.equals(Looper.myLooper(), Looper.getMainLooper())) {
            super.i(obj);
        } else {
            this.i.post(new Ai(obj));
        }
    }
}
