package com.fossil;

import android.os.Bundle;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W64 implements So3 {
    @DexIgnore
    public /* final */ /* synthetic */ Zs2 a;

    @DexIgnore
    public W64(Zs2 zs2) {
        this.a = zs2;
    }

    @DexIgnore
    @Override // com.fossil.So3
    public final List<Bundle> a(String str, String str2) {
        return this.a.B(str, str2);
    }

    @DexIgnore
    @Override // com.fossil.So3
    public final int b(String str) {
        return this.a.M(str);
    }

    @DexIgnore
    @Override // com.fossil.So3
    public final void c(Bundle bundle) {
        this.a.j(bundle);
    }

    @DexIgnore
    @Override // com.fossil.So3
    public final void d(String str) {
        this.a.J(str);
    }

    @DexIgnore
    @Override // com.fossil.So3
    public final Map<String, Object> e(String str, String str2, boolean z) {
        return this.a.g(str, str2, z);
    }

    @DexIgnore
    @Override // com.fossil.So3
    public final void f(String str, String str2, Bundle bundle) {
        this.a.E(str, str2, bundle);
    }

    @DexIgnore
    @Override // com.fossil.So3
    public final void g(String str, String str2, Bundle bundle) {
        this.a.s(str, str2, bundle);
    }

    @DexIgnore
    @Override // com.fossil.So3
    public final String zza() {
        return this.a.S();
    }

    @DexIgnore
    @Override // com.fossil.So3
    public final void zza(String str) {
        this.a.D(str);
    }

    @DexIgnore
    @Override // com.fossil.So3
    public final String zzb() {
        return this.a.U();
    }

    @DexIgnore
    @Override // com.fossil.So3
    public final String zzc() {
        return this.a.N();
    }

    @DexIgnore
    @Override // com.fossil.So3
    public final String zzd() {
        return this.a.I();
    }

    @DexIgnore
    @Override // com.fossil.So3
    public final long zze() {
        return this.a.P();
    }
}
