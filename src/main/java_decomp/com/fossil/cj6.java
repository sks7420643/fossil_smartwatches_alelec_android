package com.fossil;

import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cj6 implements Factory<HeartRateOverviewMonthPresenter> {
    @DexIgnore
    public static HeartRateOverviewMonthPresenter a(Zi6 zi6, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        return new HeartRateOverviewMonthPresenter(zi6, userRepository, heartRateSummaryRepository);
    }
}
