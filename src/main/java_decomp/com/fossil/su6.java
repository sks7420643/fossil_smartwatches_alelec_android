package com.fossil;

import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.uirenew.login.LoginActivity;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Su6 implements MembersInjector<LoginActivity> {
    @DexIgnore
    public static void a(LoginActivity loginActivity, Un5 un5) {
        loginActivity.A = un5;
    }

    @DexIgnore
    public static void b(LoginActivity loginActivity, Vn5 vn5) {
        loginActivity.B = vn5;
    }

    @DexIgnore
    public static void c(LoginActivity loginActivity, MFLoginWechatManager mFLoginWechatManager) {
        loginActivity.D = mFLoginWechatManager;
    }

    @DexIgnore
    public static void d(LoginActivity loginActivity, Xn5 xn5) {
        loginActivity.C = xn5;
    }

    @DexIgnore
    public static void e(LoginActivity loginActivity, LoginPresenter loginPresenter) {
        loginActivity.E = loginPresenter;
    }
}
