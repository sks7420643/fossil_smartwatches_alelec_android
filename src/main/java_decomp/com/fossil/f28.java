package com.fossil;

import com.fossil.P18;
import com.fossil.V18;
import com.fossil.wearables.fsl.dial.ConfigItem;
import com.misfit.frameworks.common.enums.Action;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import net.sqlcipher.database.SQLiteDatabase;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class F28 {
    @DexIgnore
    public /* final */ V18 a;
    @DexIgnore
    public /* final */ Response b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ V18 b;
        @DexIgnore
        public /* final */ Response c;
        @DexIgnore
        public Date d;
        @DexIgnore
        public String e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public String g;
        @DexIgnore
        public Date h;
        @DexIgnore
        public long i;
        @DexIgnore
        public long j;
        @DexIgnore
        public String k;
        @DexIgnore
        public int l; // = -1;

        @DexIgnore
        public Ai(long j2, V18 v18, Response response) {
            this.a = j2;
            this.b = v18;
            this.c = response;
            if (response != null) {
                this.i = response.L();
                this.j = response.F();
                P18 l2 = response.l();
                int h2 = l2.h();
                for (int i2 = 0; i2 < h2; i2++) {
                    String e2 = l2.e(i2);
                    String i3 = l2.i(i2);
                    if (ConfigItem.KEY_DATE.equalsIgnoreCase(e2)) {
                        this.d = T28.b(i3);
                        this.e = i3;
                    } else if ("Expires".equalsIgnoreCase(e2)) {
                        this.h = T28.b(i3);
                    } else if ("Last-Modified".equalsIgnoreCase(e2)) {
                        this.f = T28.b(i3);
                        this.g = i3;
                    } else if ("ETag".equalsIgnoreCase(e2)) {
                        this.k = i3;
                    } else if ("Age".equalsIgnoreCase(e2)) {
                        this.l = U28.f(i3, -1);
                    }
                }
            }
        }

        @DexIgnore
        public static boolean e(V18 v18) {
            return (v18.c("If-Modified-Since") == null && v18.c("If-None-Match") == null) ? false : true;
        }

        @DexIgnore
        public final long a() {
            long j2 = 0;
            Date date = this.d;
            if (date != null) {
                j2 = Math.max(0L, this.j - date.getTime());
            }
            int i2 = this.l;
            if (i2 != -1) {
                j2 = Math.max(j2, TimeUnit.SECONDS.toMillis((long) i2));
            }
            long j3 = this.j;
            return j2 + (j3 - this.i) + (this.a - j3);
        }

        @DexIgnore
        public final long b() {
            long j2 = 0;
            Z08 b2 = this.c.b();
            if (b2.d() != -1) {
                return TimeUnit.SECONDS.toMillis((long) b2.d());
            }
            if (this.h != null) {
                Date date = this.d;
                long time = this.h.getTime() - (date != null ? date.getTime() : this.j);
                if (time <= 0) {
                    return 0;
                }
                return time;
            }
            if (this.f != null && this.c.G().j().A() == null) {
                Date date2 = this.d;
                long time2 = (date2 != null ? date2.getTime() : this.i) - this.f.getTime();
                if (time2 > 0) {
                    j2 = time2 / 10;
                }
            }
            return j2;
        }

        @DexIgnore
        public F28 c() {
            F28 d2 = d();
            return (d2.a == null || !this.b.b().j()) ? d2 : new F28(null, null);
        }

        @DexIgnore
        public final F28 d() {
            long j2 = 0;
            if (this.c == null) {
                return new F28(this.b, null);
            }
            if (this.b.f() && this.c.h() == null) {
                return new F28(this.b, null);
            }
            if (!F28.a(this.c, this.b)) {
                return new F28(this.b, null);
            }
            Z08 b2 = this.b.b();
            if (b2.h() || e(this.b)) {
                return new F28(this.b, null);
            }
            Z08 b3 = this.c.b();
            long a2 = a();
            long b4 = b();
            if (b2.d() != -1) {
                b4 = Math.min(b4, TimeUnit.SECONDS.toMillis((long) b2.d()));
            }
            long millis = b2.f() != -1 ? TimeUnit.SECONDS.toMillis((long) b2.f()) : 0;
            if (!b3.g() && b2.e() != -1) {
                j2 = TimeUnit.SECONDS.toMillis((long) b2.e());
            }
            if (!b3.h()) {
                long j3 = millis + a2;
                if (j3 < j2 + b4) {
                    Response.a B = this.c.B();
                    if (j3 >= b4) {
                        B.a("Warning", "110 HttpURLConnection \"Response is stale\"");
                    }
                    if (a2 > LogBuilder.MAX_INTERVAL && f()) {
                        B.a("Warning", "113 HttpURLConnection \"Heuristic expiration\"");
                    }
                    return new F28(null, B.c());
                }
            }
            String str = this.k;
            String str2 = "If-Modified-Since";
            if (str != null) {
                str2 = "If-None-Match";
            } else if (this.f != null) {
                str = this.g;
            } else if (this.d == null) {
                return new F28(this.b, null);
            } else {
                str = this.e;
            }
            P18.Ai f2 = this.b.e().f();
            Z18.a.b(f2, str2, str);
            V18.Ai h2 = this.b.h();
            h2.f(f2.e());
            return new F28(h2.b(), this.c);
        }

        @DexIgnore
        public final boolean f() {
            return this.c.b().d() == -1 && this.h == null;
        }
    }

    @DexIgnore
    public F28(V18 v18, Response response) {
        this.a = v18;
        this.b = response;
    }

    @DexIgnore
    public static boolean a(Response response, V18 v18) {
        int f = response.f();
        if (!(f == 200 || f == 410 || f == 414 || f == 501 || f == 203 || f == 204)) {
            if (f != 307) {
                if (!(f == 308 || f == 404 || f == 405)) {
                    switch (f) {
                        case SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS /* 300 */:
                        case Action.Presenter.NEXT /* 301 */:
                            break;
                        case Action.Presenter.PREVIOUS /* 302 */:
                            break;
                        default:
                            return false;
                    }
                }
            }
            if (response.j("Expires") == null && response.b().d() == -1 && !response.b().c() && !response.b().b()) {
                return false;
            }
        }
        return !response.b().i() && !v18.b().i();
    }
}
