package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A12 implements Factory<Z02> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<X02> b;

    @DexIgnore
    public A12(Provider<Context> provider, Provider<X02> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static A12 a(Provider<Context> provider, Provider<X02> provider2) {
        return new A12(provider, provider2);
    }

    @DexIgnore
    public Z02 b() {
        return new Z02(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
