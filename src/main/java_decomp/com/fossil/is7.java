package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Is7<R> extends Ls7<R>, Object<R> {

    @DexIgnore
    public interface Ai<R> extends Hs7<R>, Hg6<R, Cd6> {
    }

    @DexIgnore
    Ai<R> getSetter();
}
