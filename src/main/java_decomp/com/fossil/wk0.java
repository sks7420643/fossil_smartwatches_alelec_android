package com.fossil;

import android.app.Service;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobServiceEngine;
import android.app.job.JobWorkItem;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Wk0 extends Service {
    @DexIgnore
    public static /* final */ HashMap<ComponentName, Hi> h; // = new HashMap<>();
    @DexIgnore
    public Bi b;
    @DexIgnore
    public Hi c;
    @DexIgnore
    public Ai d;
    @DexIgnore
    public boolean e; // = false;
    @DexIgnore
    public boolean f; // = false;
    @DexIgnore
    public /* final */ ArrayList<Di> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Ai extends AsyncTask<Void, Void, Void> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public Void a(Void... voidArr) {
            while (true) {
                Ei a2 = Wk0.this.a();
                if (a2 == null) {
                    return null;
                }
                Wk0.this.e(a2.getIntent());
                a2.a();
            }
        }

        @DexIgnore
        public void b(Void r2) {
            Wk0.this.g();
        }

        @DexIgnore
        public void c(Void r2) {
            Wk0.this.g();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object[]] */
        @Override // android.os.AsyncTask
        public /* bridge */ /* synthetic */ Void doInBackground(Void[] voidArr) {
            return a(voidArr);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // android.os.AsyncTask
        public /* bridge */ /* synthetic */ void onCancelled(Void r1) {
            b(r1);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // android.os.AsyncTask
        public /* bridge */ /* synthetic */ void onPostExecute(Void r1) {
            c(r1);
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        IBinder a();

        @DexIgnore
        Ei b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Hi {
        @DexIgnore
        public /* final */ PowerManager.WakeLock d;
        @DexIgnore
        public /* final */ PowerManager.WakeLock e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;

        @DexIgnore
        public Ci(Context context, ComponentName componentName) {
            super(componentName);
            context.getApplicationContext();
            PowerManager powerManager = (PowerManager) context.getSystemService("power");
            PowerManager.WakeLock newWakeLock = powerManager.newWakeLock(1, componentName.getClassName() + ":launch");
            this.d = newWakeLock;
            newWakeLock.setReferenceCounted(false);
            PowerManager.WakeLock newWakeLock2 = powerManager.newWakeLock(1, componentName.getClassName() + ":run");
            this.e = newWakeLock2;
            newWakeLock2.setReferenceCounted(false);
        }

        @DexIgnore
        @Override // com.fossil.Wk0.Hi
        public void b() {
            synchronized (this) {
                if (this.g) {
                    if (this.f) {
                        this.d.acquire(60000);
                    }
                    this.g = false;
                    this.e.release();
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.Wk0.Hi
        public void c() {
            synchronized (this) {
                if (!this.g) {
                    this.g = true;
                    this.e.acquire(600000);
                    this.d.release();
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.Wk0.Hi
        public void d() {
            synchronized (this) {
                this.f = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di implements Ei {
        @DexIgnore
        public /* final */ Intent a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public Di(Intent intent, int i) {
            this.a = intent;
            this.b = i;
        }

        @DexIgnore
        @Override // com.fossil.Wk0.Ei
        public void a() {
            Wk0.this.stopSelf(this.b);
        }

        @DexIgnore
        @Override // com.fossil.Wk0.Ei
        public Intent getIntent() {
            return this.a;
        }
    }

    @DexIgnore
    public interface Ei {
        @DexIgnore
        Object a();  // void declaration

        @DexIgnore
        Intent getIntent();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi extends JobServiceEngine implements Bi {
        @DexIgnore
        public /* final */ Wk0 a;
        @DexIgnore
        public /* final */ Object b; // = new Object();
        @DexIgnore
        public JobParameters c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class Aii implements Ei {
            @DexIgnore
            public /* final */ JobWorkItem a;

            @DexIgnore
            public Aii(JobWorkItem jobWorkItem) {
                this.a = jobWorkItem;
            }

            @DexIgnore
            @Override // com.fossil.Wk0.Ei
            public void a() {
                synchronized (Fi.this.b) {
                    if (Fi.this.c != null) {
                        Fi.this.c.completeWork(this.a);
                    }
                }
            }

            @DexIgnore
            @Override // com.fossil.Wk0.Ei
            public Intent getIntent() {
                return this.a.getIntent();
            }
        }

        @DexIgnore
        public Fi(Wk0 wk0) {
            super(wk0);
            this.a = wk0;
        }

        @DexIgnore
        @Override // com.fossil.Wk0.Bi
        public IBinder a() {
            return getBinder();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
            return new com.fossil.Wk0.Fi.Aii(r3, r2);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0011, code lost:
            if (r2 == null) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0013, code lost:
            r2.getIntent().setExtrasClassLoader(r3.a.getClassLoader());
         */
        @DexIgnore
        @Override // com.fossil.Wk0.Bi
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.fossil.Wk0.Ei b() {
            /*
                r3 = this;
                r0 = 0
                java.lang.Object r1 = r3.b
                monitor-enter(r1)
                android.app.job.JobParameters r2 = r3.c     // Catch:{ all -> 0x0026 }
                if (r2 != 0) goto L_0x000a
                monitor-exit(r1)     // Catch:{ all -> 0x0026 }
            L_0x0009:
                return r0
            L_0x000a:
                android.app.job.JobParameters r2 = r3.c     // Catch:{ all -> 0x0026 }
                android.app.job.JobWorkItem r2 = r2.dequeueWork()     // Catch:{ all -> 0x0026 }
                monitor-exit(r1)     // Catch:{ all -> 0x0026 }
                if (r2 == 0) goto L_0x0009
                android.content.Intent r0 = r2.getIntent()
                com.fossil.Wk0 r1 = r3.a
                java.lang.ClassLoader r1 = r1.getClassLoader()
                r0.setExtrasClassLoader(r1)
                com.fossil.Wk0$Fi$Aii r0 = new com.fossil.Wk0$Fi$Aii
                r0.<init>(r2)
                goto L_0x0009
            L_0x0026:
                r0 = move-exception
                monitor-exit(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Wk0.Fi.b():com.fossil.Wk0$Ei");
        }

        @DexIgnore
        public boolean onStartJob(JobParameters jobParameters) {
            this.c = jobParameters;
            this.a.c(false);
            return true;
        }

        @DexIgnore
        public boolean onStopJob(JobParameters jobParameters) {
            boolean b2 = this.a.b();
            synchronized (this.b) {
                this.c = null;
            }
            return b2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi extends Hi {
        @DexIgnore
        public /* final */ JobInfo d;

        @DexIgnore
        public Gi(Context context, ComponentName componentName, int i) {
            super(componentName);
            a(i);
            this.d = new JobInfo.Builder(i, this.a).setOverrideDeadline(0).build();
            JobScheduler jobScheduler = (JobScheduler) context.getApplicationContext().getSystemService("jobscheduler");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Hi {
        @DexIgnore
        public /* final */ ComponentName a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public int c;

        @DexIgnore
        public Hi(ComponentName componentName) {
            this.a = componentName;
        }

        @DexIgnore
        public void a(int i) {
            if (!this.b) {
                this.b = true;
                this.c = i;
            } else if (this.c != i) {
                throw new IllegalArgumentException("Given job ID " + i + " is different than previous " + this.c);
            }
        }

        @DexIgnore
        public void b() {
        }

        @DexIgnore
        public void c() {
        }

        @DexIgnore
        public void d() {
        }
    }

    @DexIgnore
    public Wk0() {
        if (Build.VERSION.SDK_INT >= 26) {
            this.g = null;
        } else {
            this.g = new ArrayList<>();
        }
    }

    @DexIgnore
    public static Hi d(Context context, ComponentName componentName, boolean z, int i) {
        Hi hi = h.get(componentName);
        if (hi == null) {
            if (Build.VERSION.SDK_INT < 26) {
                hi = new Ci(context, componentName);
            } else if (z) {
                hi = new Gi(context, componentName, i);
            } else {
                throw new IllegalArgumentException("Can't be here without a job id");
            }
            h.put(componentName, hi);
        }
        return hi;
    }

    @DexIgnore
    public Ei a() {
        Bi bi = this.b;
        if (bi != null) {
            return bi.b();
        }
        synchronized (this.g) {
            if (this.g.size() <= 0) {
                return null;
            }
            return this.g.remove(0);
        }
    }

    @DexIgnore
    public boolean b() {
        Ai ai = this.d;
        if (ai != null) {
            ai.cancel(this.e);
        }
        return f();
    }

    @DexIgnore
    public void c(boolean z) {
        if (this.d == null) {
            this.d = new Ai();
            Hi hi = this.c;
            if (hi != null && z) {
                hi.c();
            }
            this.d.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }
    }

    @DexIgnore
    public abstract void e(Intent intent);

    @DexIgnore
    public boolean f() {
        return true;
    }

    @DexIgnore
    public void g() {
        ArrayList<Di> arrayList = this.g;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.d = null;
                if (this.g != null && this.g.size() > 0) {
                    c(false);
                } else if (!this.f) {
                    this.c.b();
                }
            }
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        Bi bi = this.b;
        if (bi != null) {
            return bi.a();
        }
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 26) {
            this.b = new Fi(this);
            this.c = null;
            return;
        }
        this.b = null;
        this.c = d(this, new ComponentName(this, Wk0.class), false, 0);
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        ArrayList<Di> arrayList = this.g;
        if (arrayList != null) {
            synchronized (arrayList) {
                this.f = true;
                this.c.b();
            }
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i, int i2) {
        if (this.g == null) {
            return 2;
        }
        this.c.d();
        synchronized (this.g) {
            ArrayList<Di> arrayList = this.g;
            if (intent == null) {
                intent = new Intent();
            }
            arrayList.add(new Di(intent, i2));
            c(true);
        }
        return 3;
    }
}
