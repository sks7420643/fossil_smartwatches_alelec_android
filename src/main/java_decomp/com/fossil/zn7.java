package com.fossil;

import com.fossil.Dl7;
import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Zn7 implements Xe6<Object>, Do7, Serializable {
    @DexIgnore
    public /* final */ Xe6<Object> completion;

    @DexIgnore
    public Zn7(Xe6<Object> xe6) {
        this.completion = xe6;
    }

    @DexIgnore
    public Xe6<Cd6> create(Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        throw new UnsupportedOperationException("create(Continuation) has not been overridden");
    }

    @DexIgnore
    public Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        throw new UnsupportedOperationException("create(Any?;Continuation) has not been overridden");
    }

    @DexIgnore
    @Override // com.fossil.Do7
    public Do7 getCallerFrame() {
        Xe6<Object> xe6 = this.completion;
        if (!(xe6 instanceof Do7)) {
            xe6 = null;
        }
        return (Do7) xe6;
    }

    @DexIgnore
    public final Xe6<Object> getCompletion() {
        return this.completion;
    }

    @DexIgnore
    @Override // com.mapped.Xe6
    public abstract /* synthetic */ Af6 getContext();

    @DexIgnore
    @Override // com.fossil.Do7
    public StackTraceElement getStackTraceElement() {
        return Fo7.d(this);
    }

    @DexIgnore
    public abstract Object invokeSuspend(Object obj);

    @DexIgnore
    public void releaseIntercepted() {
    }

    @DexIgnore
    @Override // com.mapped.Xe6
    public final void resumeWith(Object obj) {
        while (true) {
            Go7.b(this);
            Xe6<Object> xe6 = this.completion;
            if (xe6 != null) {
                try {
                    Object invokeSuspend = this.invokeSuspend(obj);
                    if (invokeSuspend != Yn7.d()) {
                        Dl7.Ai ai = Dl7.Companion;
                        obj = Dl7.constructor-impl(invokeSuspend);
                        this.releaseIntercepted();
                        if (xe6 instanceof Zn7) {
                            this = (Zn7) xe6;
                        } else {
                            xe6.resumeWith(obj);
                            return;
                        }
                    } else {
                        return;
                    }
                } catch (Throwable th) {
                    Dl7.Ai ai2 = Dl7.Companion;
                    obj = Dl7.constructor-impl(El7.a(th));
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Continuation at ");
        Object stackTraceElement = getStackTraceElement();
        if (stackTraceElement == null) {
            stackTraceElement = getClass().getName();
        }
        sb.append(stackTraceElement);
        return sb.toString();
    }
}
