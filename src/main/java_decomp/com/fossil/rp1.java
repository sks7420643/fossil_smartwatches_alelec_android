package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.E90;
import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rp1 extends Vp1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Rp1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Rp1 createFromParcel(Parcel parcel) {
            return new Rp1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Rp1[] newArray(int i) {
            return new Rp1[i];
        }
    }

    @DexIgnore
    public Rp1(byte b) {
        super(E90.AUTHENTICATION_REQUEST, b);
    }

    @DexIgnore
    public /* synthetic */ Rp1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }
}
