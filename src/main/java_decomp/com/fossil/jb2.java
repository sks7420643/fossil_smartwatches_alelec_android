package com.fossil;

import com.google.android.gms.common.api.internal.LifecycleCallback;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jb2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ LifecycleCallback b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ Ib2 d;

    @DexIgnore
    public Jb2(Ib2 ib2, LifecycleCallback lifecycleCallback, String str) {
        this.d = ib2;
        this.b = lifecycleCallback;
        this.c = str;
    }

    @DexIgnore
    public final void run() {
        if (Ib2.v6(this.d) > 0) {
            this.b.f(Ib2.x6(this.d) != null ? Ib2.x6(this.d).getBundle(this.c) : null);
        }
        if (Ib2.v6(this.d) >= 2) {
            this.b.j();
        }
        if (Ib2.v6(this.d) >= 3) {
            this.b.h();
        }
        if (Ib2.v6(this.d) >= 4) {
            this.b.k();
        }
        if (Ib2.v6(this.d) >= 5) {
            this.b.g();
        }
    }
}
