package com.fossil;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cx3 {
    @DexIgnore
    public long a; // = 0;
    @DexIgnore
    public long b; // = 300;
    @DexIgnore
    public TimeInterpolator c; // = null;
    @DexIgnore
    public int d; // = 0;
    @DexIgnore
    public int e; // = 1;

    @DexIgnore
    public Cx3(long j, long j2) {
        this.a = j;
        this.b = j2;
    }

    @DexIgnore
    public Cx3(long j, long j2, TimeInterpolator timeInterpolator) {
        this.a = j;
        this.b = j2;
        this.c = timeInterpolator;
    }

    @DexIgnore
    public static Cx3 b(ValueAnimator valueAnimator) {
        Cx3 cx3 = new Cx3(valueAnimator.getStartDelay(), valueAnimator.getDuration(), f(valueAnimator));
        cx3.d = valueAnimator.getRepeatCount();
        cx3.e = valueAnimator.getRepeatMode();
        return cx3;
    }

    @DexIgnore
    public static TimeInterpolator f(ValueAnimator valueAnimator) {
        TimeInterpolator interpolator = valueAnimator.getInterpolator();
        return ((interpolator instanceof AccelerateDecelerateInterpolator) || interpolator == null) ? Uw3.b : interpolator instanceof AccelerateInterpolator ? Uw3.c : interpolator instanceof DecelerateInterpolator ? Uw3.d : interpolator;
    }

    @DexIgnore
    public void a(Animator animator) {
        animator.setStartDelay(c());
        animator.setDuration(d());
        animator.setInterpolator(e());
        if (animator instanceof ValueAnimator) {
            ValueAnimator valueAnimator = (ValueAnimator) animator;
            valueAnimator.setRepeatCount(g());
            valueAnimator.setRepeatMode(h());
        }
    }

    @DexIgnore
    public long c() {
        return this.a;
    }

    @DexIgnore
    public long d() {
        return this.b;
    }

    @DexIgnore
    public TimeInterpolator e() {
        TimeInterpolator timeInterpolator = this.c;
        return timeInterpolator != null ? timeInterpolator : Uw3.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Cx3)) {
            return false;
        }
        Cx3 cx3 = (Cx3) obj;
        if (c() == cx3.c() && d() == cx3.d() && g() == cx3.g() && h() == cx3.h()) {
            return e().getClass().equals(cx3.e().getClass());
        }
        return false;
    }

    @DexIgnore
    public int g() {
        return this.d;
    }

    @DexIgnore
    public int h() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((int) (c() ^ (c() >>> 32))) * 31) + ((int) (d() ^ (d() >>> 32)))) * 31) + e().getClass().hashCode()) * 31) + g()) * 31) + h();
    }

    @DexIgnore
    public String toString() {
        return '\n' + Cx3.class.getName() + '{' + Integer.toHexString(System.identityHashCode(this)) + " delay: " + c() + " duration: " + d() + " interpolator: " + e().getClass() + " repeatCount: " + g() + " repeatMode: " + h() + "}\n";
    }
}
