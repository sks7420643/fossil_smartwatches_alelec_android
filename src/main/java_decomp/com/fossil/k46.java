package com.fossil;

import androidx.loader.app.LoaderManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K46 implements Factory<LoaderManager> {
    @DexIgnore
    public static LoaderManager a(J46 j46) {
        LoaderManager b = j46.b();
        Lk7.c(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
