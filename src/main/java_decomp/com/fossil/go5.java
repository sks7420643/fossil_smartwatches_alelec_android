package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.fossil.eo5;
import com.fossil.iq4;
import com.fossil.p27;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Calendar;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class go5 extends BroadcastReceiver {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public on5 f1339a;
    @DexIgnore
    public p27 b;
    @DexIgnore
    public bk5 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.e<p27.c, p27.b> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ go5 f1340a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(go5 go5) {
            this.f1340a = go5;
        }

        @DexIgnore
        /* renamed from: b */
        public void a(p27.b bVar) {
            pq7.c(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("TimeTickReceiver", bVar.toString());
        }

        @DexIgnore
        /* renamed from: c */
        public void onSuccess(p27.c cVar) {
            pq7.c(cVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("TimeTickReceiver", "Re-schedule SyncDataReminder");
            bk5 a2 = this.f1340a.a();
            Context applicationContext = PortfolioApp.h0.c().getApplicationContext();
            pq7.b(applicationContext, "PortfolioApp.instance.applicationContext");
            a2.h(applicationContext);
        }
    }

    @DexIgnore
    public go5() {
        PortfolioApp.h0.c().M().w0(this);
    }

    @DexIgnore
    public final bk5 a() {
        bk5 bk5 = this.c;
        if (bk5 != null) {
            return bk5;
        }
        pq7.n("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("TimeTickReceiver", "onReceive() - action = " + action);
        on5 on5 = this.f1339a;
        if (on5 != null) {
            long C = on5.C(PortfolioApp.h0.c().J());
            if (!TextUtils.isEmpty(action) && pq7.a(action, "android.intent.action.TIME_TICK") && System.currentTimeMillis() - C <= 120000) {
                eo5.a aVar = eo5.c;
                if (context != null) {
                    aVar.d(context);
                } else {
                    pq7.i();
                    throw null;
                }
            }
            if (pq7.a("android.intent.action.TIME_TICK", action)) {
                TimeZone timeZone = TimeZone.getDefault();
                try {
                    Calendar instance = Calendar.getInstance();
                    pq7.b(instance, "calendar");
                    Calendar instance2 = Calendar.getInstance();
                    pq7.b(instance2, "Calendar.getInstance()");
                    instance.setTimeInMillis(instance2.getTimeInMillis() - ((long) 60000));
                    Calendar instance3 = Calendar.getInstance();
                    pq7.b(instance3, "Calendar.getInstance()");
                    if (timeZone.getOffset(instance3.getTimeInMillis()) != timeZone.getOffset(instance.getTimeInMillis())) {
                        FLogger.INSTANCE.getLocal().d("TimeTickReceiver", "DST Changed.");
                        p27 p27 = this.b;
                        if (p27 != null) {
                            p27.e(new p27.a(), new a(this));
                        } else {
                            pq7.n("mDstChangeUseCase");
                            throw null;
                        }
                    }
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.e("TimeTickReceiver", ".timeZoneChangeReceiver - ex=" + e);
                }
            }
        } else {
            pq7.n("mSharedPreferencesManager");
            throw null;
        }
    }
}
