package com.fossil;

import java.io.File;
import java.io.FilenameFilter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class T84 implements FilenameFilter {
    @DexIgnore
    public static /* final */ T84 a; // = new T84();

    @DexIgnore
    public static FilenameFilter a() {
        return a;
    }

    @DexIgnore
    public boolean accept(File file, String str) {
        return str.startsWith(".ae");
    }
}
