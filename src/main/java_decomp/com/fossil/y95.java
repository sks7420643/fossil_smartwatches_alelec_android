package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Y95 extends X95 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d y; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray z;
    @DexIgnore
    public long x;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        z = sparseIntArray;
        sparseIntArray.put(2131362686, 1);
        z.put(2131362968, 2);
        z.put(2131362546, 3);
        z.put(2131363043, 4);
        z.put(2131362417, 5);
        z.put(2131362289, 6);
    }
    */

    @DexIgnore
    public Y95(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 7, y, z));
    }

    @DexIgnore
    public Y95(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleButton) objArr[6], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[3], (RTLImageView) objArr[1], (DashBar) objArr[2], (ConstraintLayout) objArr[0], (RecyclerView) objArr[4]);
        this.x = -1;
        this.v.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.x = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.x != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.x = 1;
        }
        w();
    }
}
