package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cf5 extends Bf5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B;
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public long z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        B = sparseIntArray;
        sparseIntArray.put(2131362773, 1);
        B.put(2131362562, 2);
        B.put(2131362517, 3);
        B.put(2131362516, 4);
        B.put(2131363111, 5);
        B.put(2131362482, 6);
        B.put(2131362481, 7);
        B.put(2131362561, 8);
    }
    */

    @DexIgnore
    public Cf5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 9, A, B));
    }

    @DexIgnore
    public Cf5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[2], (AppCompatImageView) objArr[1], (View) objArr[5]);
        this.z = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.y = constraintLayout;
        constraintLayout.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.z != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.z = 1;
        }
        w();
    }
}
