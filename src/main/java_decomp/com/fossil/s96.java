package com.fossil;

import android.graphics.Bitmap;
import androidx.lifecycle.MutableLiveData;
import com.fossil.imagefilters.FilterType;
import com.mapped.Wg6;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S96 extends Ts0 {
    @DexIgnore
    public Bitmap a;

    /*
    static {
        System.loadLibrary("EInkImageFilter");
    }
    */

    @DexIgnore
    public S96(WFBackgroundPhotoRepository wFBackgroundPhotoRepository, RingStyleRepository ringStyleRepository) {
        Wg6.c(wFBackgroundPhotoRepository, "wfBackgroundPhotoRepository");
        Wg6.c(ringStyleRepository, "ringStyleRepository");
        new MutableLiveData();
        new MutableLiveData();
        FilterType filterType = FilterType.ORDERED_DITHERING;
    }

    @DexIgnore
    @Override // com.fossil.Ts0
    public void onCleared() {
        Jv7.d(Us0.a(this), null, 1, null);
        Bitmap bitmap = this.a;
        if (bitmap != null) {
            bitmap.recycle();
        }
        super.onCleared();
    }
}
