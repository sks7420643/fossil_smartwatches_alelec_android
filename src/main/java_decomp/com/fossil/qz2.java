package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qz2 {
    @DexIgnore
    public static /* final */ Class<?> a; // = a("libcore.io.Memory");
    @DexIgnore
    public static /* final */ boolean b; // = (a("org.robolectric.Robolectric") != null);

    @DexIgnore
    public static <T> Class<T> a(String str) {
        try {
            return (Class<T>) Class.forName(str);
        } catch (Throwable th) {
            return null;
        }
    }

    @DexIgnore
    public static boolean b() {
        return a != null && !b;
    }

    @DexIgnore
    public static Class<?> c() {
        return a;
    }
}
