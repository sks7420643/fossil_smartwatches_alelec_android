package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.watchface.WatchFaceEditorView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Rg5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ View A;
    @DexIgnore
    public /* final */ RTLImageView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ View D;
    @DexIgnore
    public /* final */ View E;
    @DexIgnore
    public /* final */ View F;
    @DexIgnore
    public /* final */ View G;
    @DexIgnore
    public /* final */ View H;
    @DexIgnore
    public /* final */ ViewPager2 I;
    @DexIgnore
    public /* final */ ImageView J;
    @DexIgnore
    public /* final */ View K;
    @DexIgnore
    public /* final */ WatchFaceEditorView L;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ RTLImageView s;
    @DexIgnore
    public /* final */ FrameLayout t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ RTLImageView v;
    @DexIgnore
    public /* final */ LinearLayout w;
    @DexIgnore
    public /* final */ LinearLayout x;
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public /* final */ BottomNavigationView z;

    @DexIgnore
    public Rg5(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleButton flexibleButton, RTLImageView rTLImageView, FrameLayout frameLayout, FlexibleTextView flexibleTextView, RTLImageView rTLImageView2, LinearLayout linearLayout, LinearLayout linearLayout2, ConstraintLayout constraintLayout2, BottomNavigationView bottomNavigationView, View view2, RTLImageView rTLImageView3, FlexibleTextView flexibleTextView2, View view3, View view4, View view5, View view6, View view7, ViewPager2 viewPager2, ImageView imageView, View view8, WatchFaceEditorView watchFaceEditorView) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleButton;
        this.s = rTLImageView;
        this.t = frameLayout;
        this.u = flexibleTextView;
        this.v = rTLImageView2;
        this.w = linearLayout;
        this.x = linearLayout2;
        this.y = constraintLayout2;
        this.z = bottomNavigationView;
        this.A = view2;
        this.B = rTLImageView3;
        this.C = flexibleTextView2;
        this.D = view3;
        this.E = view4;
        this.F = view5;
        this.G = view6;
        this.H = view7;
        this.I = viewPager2;
        this.J = imageView;
        this.K = view8;
        this.L = watchFaceEditorView;
    }

    @DexIgnore
    @Deprecated
    public static Rg5 A(LayoutInflater layoutInflater, Object obj) {
        return (Rg5) ViewDataBinding.p(layoutInflater, 2131558851, null, false, obj);
    }

    @DexIgnore
    public static Rg5 z(LayoutInflater layoutInflater) {
        return A(layoutInflater, Aq0.d());
    }
}
