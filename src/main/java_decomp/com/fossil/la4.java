package com.fossil;

import com.fossil.Ta4$d$d$a$b$c;
import com.fossil.Ta4$d$d$a$b$e;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class La4 extends Ta4$d$d$a$b$c {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Ua4<Ta4$d$d$a$b$e.b> c;
    @DexIgnore
    public /* final */ Ta4$d$d$a$b$c d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4$d$d$a$b$c.a {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Ua4<Ta4$d$d$a$b$e.b> c;
        @DexIgnore
        public Ta4$d$d$a$b$c d;
        @DexIgnore
        public Integer e;

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$c.a
        public Ta4$d$d$a$b$c a() {
            String str = "";
            if (this.a == null) {
                str = " type";
            }
            if (this.c == null) {
                str = str + " frames";
            }
            if (this.e == null) {
                str = str + " overflowCount";
            }
            if (str.isEmpty()) {
                return new La4(this.a, this.b, this.c, this.d, this.e.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$c.a
        public Ta4$d$d$a$b$c.a b(Ta4$d$d$a$b$c ta4$d$d$a$b$c) {
            this.d = ta4$d$d$a$b$c;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$c.a
        public Ta4$d$d$a$b$c.a c(Ua4<Ta4$d$d$a$b$e.b> ua4) {
            if (ua4 != null) {
                this.c = ua4;
                return this;
            }
            throw new NullPointerException("Null frames");
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$c.a
        public Ta4$d$d$a$b$c.a d(int i) {
            this.e = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$c.a
        public Ta4$d$d$a$b$c.a e(String str) {
            this.b = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Ta4$d$d$a$b$c.a
        public Ta4$d$d$a$b$c.a f(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null type");
        }
    }

    @DexIgnore
    public La4(String str, String str2, Ua4<Ta4$d$d$a$b$e.b> ua4, Ta4$d$d$a$b$c ta4$d$d$a$b$c, int i) {
        this.a = str;
        this.b = str2;
        this.c = ua4;
        this.d = ta4$d$d$a$b$c;
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$c
    public Ta4$d$d$a$b$c b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$c
    public Ua4<Ta4$d$d$a$b$e.b> c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$c
    public int d() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$c
    public String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        String str;
        Ta4$d$d$a$b$c ta4$d$d$a$b$c;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4$d$d$a$b$c)) {
            return false;
        }
        Ta4$d$d$a$b$c ta4$d$d$a$b$c2 = (Ta4$d$d$a$b$c) obj;
        return this.a.equals(ta4$d$d$a$b$c2.f()) && ((str = this.b) != null ? str.equals(ta4$d$d$a$b$c2.e()) : ta4$d$d$a$b$c2.e() == null) && this.c.equals(ta4$d$d$a$b$c2.c()) && ((ta4$d$d$a$b$c = this.d) != null ? ta4$d$d$a$b$c.equals(ta4$d$d$a$b$c2.b()) : ta4$d$d$a$b$c2.b() == null) && this.e == ta4$d$d$a$b$c2.d();
    }

    @DexIgnore
    @Override // com.fossil.Ta4$d$d$a$b$c
    public String f() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int hashCode = this.a.hashCode();
        String str = this.b;
        int hashCode2 = str == null ? 0 : str.hashCode();
        int hashCode3 = this.c.hashCode();
        Ta4$d$d$a$b$c ta4$d$d$a$b$c = this.d;
        if (ta4$d$d$a$b$c != null) {
            i = ta4$d$d$a$b$c.hashCode();
        }
        return ((((((hashCode2 ^ ((hashCode ^ 1000003) * 1000003)) * 1000003) ^ hashCode3) * 1000003) ^ i) * 1000003) ^ this.e;
    }

    @DexIgnore
    public String toString() {
        return "Exception{type=" + this.a + ", reason=" + this.b + ", frames=" + this.c + ", causedBy=" + this.d + ", overflowCount=" + this.e + "}";
    }
}
