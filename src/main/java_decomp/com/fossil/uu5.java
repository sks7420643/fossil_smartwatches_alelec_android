package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uu5 extends iq4<a, iq4.d, iq4.a> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ SleepSummariesRepository d;
    @DexIgnore
    public /* final */ UserRepository e;
    @DexIgnore
    public /* final */ SleepSessionsRepository f;
    @DexIgnore
    public /* final */ FitnessDataRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Date f3647a;

        @DexIgnore
        public a(Date date) {
            pq7.c(date, "date");
            this.f3647a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.f3647a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries", f = "FetchSleepSummaries.kt", l = {29, 57, 58, 61}, m = "run")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uu5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(uu5 uu5, qn7 qn7) {
            super(qn7);
            this.this$0 = uu5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    /*
    static {
        String simpleName = uu5.class.getSimpleName();
        pq7.b(simpleName, "FetchSleepSummaries::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public uu5(SleepSummariesRepository sleepSummariesRepository, UserRepository userRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository) {
        pq7.c(sleepSummariesRepository, "mSleepSummariesRepository");
        pq7.c(userRepository, "mUserRepository");
        pq7.c(sleepSessionsRepository, "mSleepSessionsRepository");
        pq7.c(fitnessDataRepository, "mFitnessDataRepository");
        this.d = sleepSummariesRepository;
        this.e = userRepository;
        this.f = sleepSessionsRepository;
        this.g = fitnessDataRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return h;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x012a  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x016e  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x025f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.uu5.a r23, com.fossil.qn7<? super com.fossil.tl7> r24) {
        /*
        // Method dump skipped, instructions count: 627
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uu5.k(com.fossil.uu5$a, com.fossil.qn7):java.lang.Object");
    }
}
