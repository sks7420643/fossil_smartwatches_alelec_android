package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W80 extends Z80 {
    @DexIgnore
    public static long h; // = 100;
    @DexIgnore
    public static /* final */ W80 i; // = new W80();

    @DexIgnore
    public W80() {
        super("spg", 102400, 10485760, "spg", "spg", new Zw1("", "", ""), 1800, new B90(), Ld0.f, false);
    }

    @DexIgnore
    @Override // com.fossil.Z80
    public long e() {
        return h;
    }
}
