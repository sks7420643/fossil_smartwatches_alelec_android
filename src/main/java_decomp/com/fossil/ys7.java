package com.fossil;

import com.mapped.Gg6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ys7 extends Xs7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Ts7<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator a;

        @DexIgnore
        public Ai(Iterator it) {
            this.a = it;
        }

        @DexIgnore
        @Override // com.fossil.Ts7
        public Iterator<T> iterator() {
            return this.a;
        }
    }

    @DexIgnore
    public static final <T> Ts7<T> b(Iterator<? extends T> it) {
        Wg6.c(it, "$this$asSequence");
        return c(new Ai(it));
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.Ts7<? extends T> */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> Ts7<T> c(Ts7<? extends T> ts7) {
        Wg6.c(ts7, "$this$constrainOnce");
        return ts7 instanceof Ps7 ? ts7 : new Ps7(ts7);
    }

    @DexIgnore
    public static final <T> Ts7<T> d(Gg6<? extends T> gg6, Hg6<? super T, ? extends T> hg6) {
        Wg6.c(gg6, "seedFunction");
        Wg6.c(hg6, "nextFunction");
        return new Rs7(gg6, hg6);
    }
}
