package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uv5 extends BaseFragment implements Zv6 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public G37<L65> g;
    @DexIgnore
    public Yv6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Uv5.j;
        }

        @DexIgnore
        public final Uv5 b() {
            return new Uv5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Uv5 b;

        @DexIgnore
        public Bi(Uv5 uv5) {
            this.b = uv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ L65 b;
        @DexIgnore
        public /* final */ /* synthetic */ Uv5 c;

        @DexIgnore
        public Ci(L65 l65, Uv5 uv5) {
            this.b = l65;
            this.c = uv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Yv6 K6 = Uv5.K6(this.c);
            FlexibleTextInputEditText flexibleTextInputEditText = this.b.t;
            Wg6.b(flexibleTextInputEditText, "binding.etEmail");
            K6.o(String.valueOf(flexibleTextInputEditText.getText()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ L65 b;
        @DexIgnore
        public /* final */ /* synthetic */ Uv5 c;

        @DexIgnore
        public Di(L65 l65, Uv5 uv5) {
            this.b = l65;
            this.c = uv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            Yv6 K6 = Uv5.K6(this.c);
            FlexibleTextInputEditText flexibleTextInputEditText = this.b.t;
            Wg6.b(flexibleTextInputEditText, "binding.etEmail");
            K6.o(String.valueOf(flexibleTextInputEditText.getText()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Uv5 b;

        @DexIgnore
        public Ei(Uv5 uv5) {
            this.b = uv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Uv5 b;

        @DexIgnore
        public Fi(Uv5 uv5) {
            this.b = uv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpActivity.a aVar = SignUpActivity.F;
            Wg6.b(view, "it");
            Context context = view.getContext();
            Wg6.b(context, "it.context");
            aVar.a(context);
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Uv5 b;

        @DexIgnore
        public Gi(Uv5 uv5) {
            this.b = uv5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Uv5 b;

        @DexIgnore
        public Hi(Uv5 uv5) {
            this.b = uv5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String str;
            Yv6 K6 = Uv5.K6(this.b);
            if (charSequence == null || (str = charSequence.toString()) == null) {
                str = "";
            }
            K6.n(str);
        }
    }

    /*
    static {
        String simpleName = Uv5.class.getSimpleName();
        if (simpleName != null) {
            Wg6.b(simpleName, "ForgotPasswordFragment::class.java.simpleName!!");
            j = simpleName;
            return;
        }
        Wg6.i();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ Yv6 K6(Uv5 uv5) {
        Yv6 yv6 = uv5.h;
        if (yv6 != null) {
            return yv6;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public String D6() {
        return j;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Zv6
    public void H2() {
        G37<L65> g37 = this.g;
        if (g37 != null) {
            L65 a2 = g37.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.v;
                Wg6.b(flexibleButton, "it.fbSendLink");
                flexibleButton.setEnabled(false);
                FlexibleButton flexibleButton2 = a2.v;
                Wg6.b(flexibleButton2, "it.fbSendLink");
                flexibleButton2.setClickable(false);
                FlexibleButton flexibleButton3 = a2.v;
                Wg6.b(flexibleButton3, "it.fbSendLink");
                flexibleButton3.setFocusable(false);
                a2.v.d("flexible_button_disabled");
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Yv6 yv6) {
        M6(yv6);
    }

    @DexIgnore
    public void M6(Yv6 yv6) {
        Wg6.c(yv6, "presenter");
        this.h = yv6;
    }

    @DexIgnore
    @Override // com.fossil.Zv6
    public void P4(boolean z) {
        FlexibleButton flexibleButton;
        G37<L65> g37 = this.g;
        if (g37 != null) {
            L65 a2 = g37.a();
            if (a2 != null && (flexibleButton = a2.z) != null) {
                if (z) {
                    Wg6.b(flexibleButton, "it");
                    flexibleButton.setVisibility(0);
                    return;
                }
                Wg6.b(flexibleButton, "it");
                flexibleButton.setVisibility(8);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zv6
    public void S0() {
        G37<L65> g37 = this.g;
        if (g37 != null) {
            L65 a2 = g37.a();
            if (a2 != null) {
                FlexibleButton flexibleButton = a2.v;
                Wg6.b(flexibleButton, "it.fbSendLink");
                flexibleButton.setEnabled(true);
                FlexibleButton flexibleButton2 = a2.v;
                Wg6.b(flexibleButton2, "it.fbSendLink");
                flexibleButton2.setClickable(true);
                FlexibleButton flexibleButton3 = a2.v;
                Wg6.b(flexibleButton3, "it.fbSendLink");
                flexibleButton3.setFocusable(true);
                a2.v.d("flexible_button_primary");
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zv6
    public void h() {
        a();
    }

    @DexIgnore
    @Override // com.fossil.Zv6
    public void i() {
        b();
    }

    @DexIgnore
    @Override // com.fossil.Zv6
    public void m3() {
        G37<L65> g37 = this.g;
        if (g37 != null) {
            L65 a2 = g37.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                Wg6.b(constraintLayout, "it.clResetPwSuccess");
                if (constraintLayout.getVisibility() == 0) {
                    S37 s37 = S37.c;
                    FragmentManager childFragmentManager = getChildFragmentManager();
                    Wg6.b(childFragmentManager, "childFragmentManager");
                    s37.m0(childFragmentManager);
                    return;
                }
                ConstraintLayout constraintLayout2 = a2.q;
                Wg6.b(constraintLayout2, "it.clResetPw");
                constraintLayout2.setVisibility(8);
                ConstraintLayout constraintLayout3 = a2.r;
                Wg6.b(constraintLayout3, "it.clResetPwSuccess");
                constraintLayout3.setVisibility(0);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zv6
    public void n6(String str) {
        FlexibleTextInputEditText flexibleTextInputEditText;
        Wg6.c(str, Constants.EMAIL);
        G37<L65> g37 = this.g;
        if (g37 != null) {
            L65 a2 = g37.a();
            if (a2 != null && (flexibleTextInputEditText = a2.t) != null) {
                flexibleTextInputEditText.setText(str);
                flexibleTextInputEditText.requestFocus();
                Xk5 xk5 = Xk5.a;
                Wg6.b(flexibleTextInputEditText, "it");
                Context context = flexibleTextInputEditText.getContext();
                Wg6.b(context, "it.context");
                xk5.b(flexibleTextInputEditText, context);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zv6
    public void o(int i2, String str) {
        Wg6.c(str, "errorMessage");
        S37 s37 = S37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        s37.n0(i2, str, childFragmentManager);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        G37<L65> g37 = new G37<>(this, (L65) Aq0.f(layoutInflater, 2131558556, viewGroup, false, A6()));
        this.g = g37;
        if (g37 != null) {
            L65 a2 = g37.a();
            if (a2 != null) {
                Wg6.b(a2, "mBinding.get()!!");
                return a2.n();
            }
            Wg6.i();
            throw null;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Yv6 yv6 = this.h;
        if (yv6 != null) {
            yv6.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        G37<L65> g37 = this.g;
        if (g37 != null) {
            L65 a2 = g37.a();
            if (a2 != null) {
                a2.u.setOnClickListener(new Bi(this));
                a2.v.setOnClickListener(new Ci(a2, this));
                a2.x.setOnClickListener(new Di(a2, this));
                a2.C.setOnClickListener(new Ei(this));
                a2.z.setOnClickListener(new Fi(this));
                a2.D.setOnClickListener(new Gi(this));
                a2.t.addTextChangedListener(new Hi(this));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Zv6
    public void r5(String str) {
        Wg6.c(str, "message");
        G37<L65> g37 = this.g;
        if (g37 != null) {
            L65 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextInputLayout flexibleTextInputLayout = a2.B;
                Wg6.b(flexibleTextInputLayout, "it.inputEmail");
                flexibleTextInputLayout.setErrorEnabled(false);
                FlexibleTextInputLayout flexibleTextInputLayout2 = a2.B;
                Wg6.b(flexibleTextInputLayout2, "it.inputEmail");
                flexibleTextInputLayout2.setError(str);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
