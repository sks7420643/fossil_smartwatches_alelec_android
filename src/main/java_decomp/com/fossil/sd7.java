package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.fossil.Rd7;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sd7 extends Rd7 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public Sd7(Context context) {
        this.a = context;
    }

    @DexIgnore
    public static Bitmap j(Resources resources, int i, Pd7 pd7) {
        BitmapFactory.Options d = Rd7.d(pd7);
        if (Rd7.g(d)) {
            BitmapFactory.decodeResource(resources, i, d);
            Rd7.b(pd7.h, pd7.i, d, pd7);
        }
        return BitmapFactory.decodeResource(resources, i, d);
    }

    @DexIgnore
    @Override // com.fossil.Rd7
    public boolean c(Pd7 pd7) {
        if (pd7.e != 0) {
            return true;
        }
        return "android.resource".equals(pd7.d.getScheme());
    }

    @DexIgnore
    @Override // com.fossil.Rd7
    public Rd7.Ai f(Pd7 pd7, int i) throws IOException {
        Resources o = Xd7.o(this.a, pd7);
        return new Rd7.Ai(j(o, Xd7.n(o, pd7), pd7), Picasso.LoadedFrom.DISK);
    }
}
