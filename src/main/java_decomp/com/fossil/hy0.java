package com.fossil;

import android.graphics.Matrix;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hy0 {
    @DexIgnore
    public static Dy0 a(View view, ViewGroup viewGroup, Matrix matrix) {
        return Build.VERSION.SDK_INT == 28 ? Fy0.b(view, viewGroup, matrix) : Gy0.b(view, viewGroup, matrix);
    }

    @DexIgnore
    public static void b(View view) {
        if (Build.VERSION.SDK_INT == 28) {
            Fy0.f(view);
        } else {
            Gy0.f(view);
        }
    }
}
