package com.fossil;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class L65 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout B;
    @DexIgnore
    public /* final */ RTLImageView C;
    @DexIgnore
    public /* final */ RTLImageView D;
    @DexIgnore
    public /* final */ RTLImageView E;
    @DexIgnore
    public /* final */ FrameLayout F;
    @DexIgnore
    public /* final */ ScrollView G;
    @DexIgnore
    public /* final */ FlexibleTextView H;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ View s;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText t;
    @DexIgnore
    public /* final */ FlexibleButton u;
    @DexIgnore
    public /* final */ FlexibleButton v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleButton x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleButton z;

    @DexIgnore
    public L65(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, View view2, FlexibleTextInputEditText flexibleTextInputEditText, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleTextView flexibleTextView, FlexibleButton flexibleButton3, FlexibleTextView flexibleTextView2, FlexibleButton flexibleButton4, FlexibleTextView flexibleTextView3, FlexibleTextInputLayout flexibleTextInputLayout, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, FrameLayout frameLayout, ScrollView scrollView, FlexibleTextView flexibleTextView4) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = view2;
        this.t = flexibleTextInputEditText;
        this.u = flexibleButton;
        this.v = flexibleButton2;
        this.w = flexibleTextView;
        this.x = flexibleButton3;
        this.y = flexibleTextView2;
        this.z = flexibleButton4;
        this.A = flexibleTextView3;
        this.B = flexibleTextInputLayout;
        this.C = rTLImageView;
        this.D = rTLImageView2;
        this.E = rTLImageView3;
        this.F = frameLayout;
        this.G = scrollView;
        this.H = flexibleTextView4;
    }
}
