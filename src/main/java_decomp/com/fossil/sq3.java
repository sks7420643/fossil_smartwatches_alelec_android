package com.fossil;

import android.app.ActivityManager;
import android.os.Bundle;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sq3 {
    @DexIgnore
    public /* final */ /* synthetic */ Jq3 a;

    @DexIgnore
    public Sq3(Jq3 jq3) {
        this.a = jq3;
    }

    @DexIgnore
    public final void a() {
        this.a.h();
        if (this.a.l().v(this.a.zzm().b())) {
            this.a.l().r.a(true);
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
            if (runningAppProcessInfo.importance == 100) {
                this.a.d().N().a("Detected application was in foreground");
                c(this.a.zzm().b(), false);
            }
        }
    }

    @DexIgnore
    public final void b(long j, boolean z) {
        this.a.h();
        this.a.F();
        if (this.a.l().v(j)) {
            this.a.l().r.a(true);
        }
        this.a.l().u.b(j);
        if (this.a.l().r.b()) {
            c(j, z);
        }
    }

    @DexIgnore
    public final void c(long j, boolean z) {
        this.a.h();
        if (this.a.a.o()) {
            this.a.l().u.b(j);
            this.a.d().N().b("Session started, time", Long.valueOf(this.a.zzm().c()));
            Long valueOf = Long.valueOf(j / 1000);
            this.a.p().S("auto", "_sid", valueOf, j);
            this.a.l().r.a(false);
            Bundle bundle = new Bundle();
            bundle.putLong("_sid", valueOf.longValue());
            if (this.a.m().s(Xg3.q0) && z) {
                bundle.putLong("_aib", 1);
            }
            this.a.p().N("auto", "_s", j, bundle);
            if (Z53.a() && this.a.m().s(Xg3.v0)) {
                String a2 = this.a.l().z.a();
                if (!TextUtils.isEmpty(a2)) {
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("_ffr", a2);
                    this.a.p().N("auto", "_ssr", j, bundle2);
                }
            }
        }
    }
}
