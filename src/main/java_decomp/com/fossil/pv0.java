package com.fossil;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Pv0 extends RecyclerView.j {
    @DexIgnore
    public static /* final */ boolean DEBUG; // = false;
    @DexIgnore
    public static /* final */ String TAG; // = "SimpleItemAnimator";
    @DexIgnore
    public boolean mSupportsChangeAnimations; // = true;

    @DexIgnore
    public abstract boolean animateAdd(RecyclerView.ViewHolder viewHolder);

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.j
    public boolean animateAppearance(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar, RecyclerView.j.c cVar2) {
        return (cVar == null || (cVar.a == cVar2.a && cVar.b == cVar2.b)) ? animateAdd(viewHolder) : animateMove(viewHolder, cVar.a, cVar.b, cVar2.a, cVar2.b);
    }

    @DexIgnore
    public abstract boolean animateChange(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2, int i, int i2, int i3, int i4);

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.j
    public boolean animateChange(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2, RecyclerView.j.c cVar, RecyclerView.j.c cVar2) {
        int i;
        int i2;
        int i3 = cVar.a;
        int i4 = cVar.b;
        if (viewHolder2.shouldIgnore()) {
            i = cVar.a;
            i2 = cVar.b;
        } else {
            i = cVar2.a;
            i2 = cVar2.b;
        }
        return animateChange(viewHolder, viewHolder2, i3, i4, i, i2);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.j
    public boolean animateDisappearance(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar, RecyclerView.j.c cVar2) {
        int i = cVar.a;
        int i2 = cVar.b;
        View view = viewHolder.itemView;
        int left = cVar2 == null ? view.getLeft() : cVar2.a;
        int top = cVar2 == null ? view.getTop() : cVar2.b;
        if (viewHolder.isRemoved() || (i == left && i2 == top)) {
            return animateRemove(viewHolder);
        }
        view.layout(left, top, view.getWidth() + left, view.getHeight() + top);
        return animateMove(viewHolder, i, i2, left, top);
    }

    @DexIgnore
    public abstract boolean animateMove(RecyclerView.ViewHolder viewHolder, int i, int i2, int i3, int i4);

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.j
    public boolean animatePersistence(RecyclerView.ViewHolder viewHolder, RecyclerView.j.c cVar, RecyclerView.j.c cVar2) {
        if (cVar.a != cVar2.a || cVar.b != cVar2.b) {
            return animateMove(viewHolder, cVar.a, cVar.b, cVar2.a, cVar2.b);
        }
        dispatchMoveFinished(viewHolder);
        return false;
    }

    @DexIgnore
    public abstract boolean animateRemove(RecyclerView.ViewHolder viewHolder);

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.j
    public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
        return !this.mSupportsChangeAnimations || viewHolder.isInvalid();
    }

    @DexIgnore
    public final void dispatchAddFinished(RecyclerView.ViewHolder viewHolder) {
        onAddFinished(viewHolder);
        dispatchAnimationFinished(viewHolder);
    }

    @DexIgnore
    public final void dispatchAddStarting(RecyclerView.ViewHolder viewHolder) {
        onAddStarting(viewHolder);
    }

    @DexIgnore
    public final void dispatchChangeFinished(RecyclerView.ViewHolder viewHolder, boolean z) {
        onChangeFinished(viewHolder, z);
        dispatchAnimationFinished(viewHolder);
    }

    @DexIgnore
    public final void dispatchChangeStarting(RecyclerView.ViewHolder viewHolder, boolean z) {
        onChangeStarting(viewHolder, z);
    }

    @DexIgnore
    public final void dispatchMoveFinished(RecyclerView.ViewHolder viewHolder) {
        onMoveFinished(viewHolder);
        dispatchAnimationFinished(viewHolder);
    }

    @DexIgnore
    public final void dispatchMoveStarting(RecyclerView.ViewHolder viewHolder) {
        onMoveStarting(viewHolder);
    }

    @DexIgnore
    public final void dispatchRemoveFinished(RecyclerView.ViewHolder viewHolder) {
        onRemoveFinished(viewHolder);
        dispatchAnimationFinished(viewHolder);
    }

    @DexIgnore
    public final void dispatchRemoveStarting(RecyclerView.ViewHolder viewHolder) {
        onRemoveStarting(viewHolder);
    }

    @DexIgnore
    public boolean getSupportsChangeAnimations() {
        return this.mSupportsChangeAnimations;
    }

    @DexIgnore
    public void onAddFinished(RecyclerView.ViewHolder viewHolder) {
    }

    @DexIgnore
    public void onAddStarting(RecyclerView.ViewHolder viewHolder) {
    }

    @DexIgnore
    public void onChangeFinished(RecyclerView.ViewHolder viewHolder, boolean z) {
    }

    @DexIgnore
    public void onChangeStarting(RecyclerView.ViewHolder viewHolder, boolean z) {
    }

    @DexIgnore
    public void onMoveFinished(RecyclerView.ViewHolder viewHolder) {
    }

    @DexIgnore
    public void onMoveStarting(RecyclerView.ViewHolder viewHolder) {
    }

    @DexIgnore
    public void onRemoveFinished(RecyclerView.ViewHolder viewHolder) {
    }

    @DexIgnore
    public void onRemoveStarting(RecyclerView.ViewHolder viewHolder) {
    }

    @DexIgnore
    public void setSupportsChangeAnimations(boolean z) {
        this.mSupportsChangeAnimations = z;
    }
}
