package com.fossil;

import java.lang.reflect.Array;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qn4 {
    @DexIgnore
    public /* final */ Rn4[] a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public Qn4(int i, int i2) {
        Rn4[] rn4Arr = new Rn4[i];
        this.a = rn4Arr;
        int length = rn4Arr.length;
        for (int i3 = 0; i3 < length; i3++) {
            this.a[i3] = new Rn4(((i2 + 4) * 17) + 1);
        }
        this.d = i2 * 17;
        this.c = i;
        this.b = -1;
    }

    @DexIgnore
    public Rn4 a() {
        return this.a[this.b];
    }

    @DexIgnore
    public byte[][] b(int i, int i2) {
        byte[][] bArr = (byte[][]) Array.newInstance(Byte.TYPE, this.c * i2, this.d * i);
        int i3 = this.c * i2;
        for (int i4 = 0; i4 < i3; i4++) {
            bArr[(i3 - i4) - 1] = this.a[i4 / i2].b(i);
        }
        return bArr;
    }

    @DexIgnore
    public void c() {
        this.b++;
    }
}
