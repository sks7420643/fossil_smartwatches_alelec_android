package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mw6 implements Factory<OnboardingHeightWeightPresenter> {
    @DexIgnore
    public static OnboardingHeightWeightPresenter a(Iw6 iw6, UserRepository userRepository, GetRecommendedGoalUseCase getRecommendedGoalUseCase) {
        return new OnboardingHeightWeightPresenter(iw6, userRepository, getRecommendedGoalUseCase);
    }
}
