package com.fossil;

import dagger.internal.Factory;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E02 implements Factory<Executor> {
    @DexIgnore
    public static /* final */ E02 a; // = new E02();

    @DexIgnore
    public static E02 a() {
        return a;
    }

    @DexIgnore
    public static Executor b() {
        Executor a2 = D02.a();
        Lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public Executor c() {
        return b();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return c();
    }
}
