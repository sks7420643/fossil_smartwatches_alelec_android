package com.fossil;

import android.os.Parcel;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ib0 extends Mb0 {
    @DexIgnore
    public static /* final */ Hb0 CREATOR; // = new Hb0(null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;

    @DexIgnore
    public /* synthetic */ Ib0(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.d = parcel.readInt() != 0;
        this.e = parcel.readInt();
    }

    @DexIgnore
    public Ib0(Bv1 bv1, Ry1 ry1, int i, int i2) {
        super(bv1, ry1);
        this.d = false;
        this.e = 0;
        this.f = i;
        this.g = i2;
    }

    @DexIgnore
    public Ib0(Bv1 bv1, Ry1 ry1, boolean z, int i) {
        super(bv1, ry1);
        this.d = z;
        this.e = i;
    }

    @DexIgnore
    @Override // com.fossil.Mb0
    public List<Va0> b() {
        short s;
        int i;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Za0());
        if (this.d) {
            arrayList.add(new Ia0(new R90[]{new R90(Ca0.c, W90.d, Da0.d, Ea0.c, 0), new R90(Ca0.d, W90.d, Da0.d, Ea0.c, 0)}));
        }
        arrayList.add(new Ma0(0.5d));
        arrayList.add(new Ga0(Fa0.c));
        Ca0 ca0 = Ca0.c;
        W90 w90 = W90.c;
        Da0 c = c();
        Ea0 ea0 = Ea0.c;
        if (d()) {
            s = (short) ((this.f * 30) + ((this.g * 30) / 60));
        } else {
            int i2 = this.e;
            s = (short) (i2 >= 60 ? (i2 / 60) * 30 : i2 * 6);
        }
        R90 r90 = new R90(ca0, w90, c, ea0, s);
        Ca0 ca02 = Ca0.d;
        W90 w902 = W90.c;
        Da0 c2 = c();
        Ea0 ea02 = Ea0.c;
        if (d()) {
            i = this.g;
        } else {
            i = this.e;
            if (i >= 60) {
                i %= 60;
            }
        }
        arrayList.add(new Ia0(new R90[]{r90, new R90(ca02, w902, c2, ea02, (short) (i * 6))}));
        arrayList.add(new Ma0(8.0d));
        arrayList.add(new Ka0());
        return arrayList;
    }

    @DexIgnore
    public final Da0 c() {
        return d() ? Da0.d : this.e >= 60 ? Da0.d : Da0.c;
    }

    @DexIgnore
    public final boolean d() {
        return this.e == 0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.Mb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Ib0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Ib0 ib0 = (Ib0) obj;
            if (this.d != ib0.d) {
                return false;
            }
            return this.e == ib0.e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppCommuteTimeResponse");
    }

    @DexIgnore
    @Override // com.fossil.Mb0
    public int hashCode() {
        return (Boolean.valueOf(this.d).hashCode() * 31) + this.e;
    }

    @DexIgnore
    @Override // com.fossil.Ox1, com.fossil.Mb0
    public JSONObject toJSONObject() {
        return G80.k(G80.k(super.toJSONObject(), Jd0.w3, Integer.valueOf(this.d ? 1 : 0)), Jd0.x3, Integer.valueOf(this.e));
    }

    @DexIgnore
    @Override // com.fossil.Mb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }
}
