package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Vi5 extends Ri5 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Bundle c;

    @DexIgnore
    public Vi5(String str, int i, Bundle bundle) {
        this.a = i;
        this.b = str;
        this.c = bundle;
    }

    @DexIgnore
    public int a() {
        return this.a;
    }

    @DexIgnore
    public Bundle b() {
        return this.c;
    }

    @DexIgnore
    public String c() {
        return this.b;
    }
}
