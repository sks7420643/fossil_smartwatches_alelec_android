package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.SupportMapFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pf3 extends Qc3 {
    @DexIgnore
    public /* final */ /* synthetic */ Sb3 b;

    @DexIgnore
    public Pf3(SupportMapFragment.a aVar, Sb3 sb3) {
        this.b = sb3;
    }

    @DexIgnore
    @Override // com.fossil.Pc3
    public final void l2(Zb3 zb3) throws RemoteException {
        this.b.onMapReady(new Qb3(zb3));
    }
}
