package com.fossil;

import com.facebook.share.internal.ShareConstants;
import com.mapped.Lc6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Zm7 extends Ym7 {
    @DexIgnore
    public static final <K, V> Map<K, V> g() {
        Sm7 sm7 = Sm7.INSTANCE;
        if (sm7 != null) {
            return sm7;
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.collections.Map<K, V>");
    }

    @DexIgnore
    public static final <K, V> V h(Map<K, ? extends V> map, K k) {
        Wg6.c(map, "$this$getValue");
        return (V) Xm7.a(map, k);
    }

    @DexIgnore
    public static final <K, V> HashMap<K, V> i(Lc6<? extends K, ? extends V>... lc6Arr) {
        Wg6.c(lc6Arr, "pairs");
        HashMap<K, V> hashMap = new HashMap<>(Ym7.b(lc6Arr.length));
        m(hashMap, lc6Arr);
        return hashMap;
    }

    @DexIgnore
    public static final <K, V> Map<K, V> j(Lc6<? extends K, ? extends V>... lc6Arr) {
        Wg6.c(lc6Arr, "pairs");
        if (lc6Arr.length <= 0) {
            return g();
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap(Ym7.b(lc6Arr.length));
        p(lc6Arr, linkedHashMap);
        return linkedHashMap;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Map<K, ? extends V> */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <K, V> Map<K, V> k(Map<K, ? extends V> map) {
        Wg6.c(map, "$this$optimizeReadOnlyMap");
        int size = map.size();
        return size != 0 ? size != 1 ? map : Ym7.e(map) : g();
    }

    @DexIgnore
    public static final <K, V> void l(Map<? super K, ? super V> map, Iterable<? extends Lc6<? extends K, ? extends V>> iterable) {
        Wg6.c(map, "$this$putAll");
        Wg6.c(iterable, "pairs");
        Iterator<? extends Lc6<? extends K, ? extends V>> it = iterable.iterator();
        while (it.hasNext()) {
            Lc6 lc6 = (Lc6) it.next();
            map.put((Object) lc6.component1(), (Object) lc6.component2());
        }
    }

    @DexIgnore
    public static final <K, V> void m(Map<? super K, ? super V> map, Lc6<? extends K, ? extends V>[] lc6Arr) {
        Wg6.c(map, "$this$putAll");
        Wg6.c(lc6Arr, "pairs");
        for (Lc6<? extends K, ? extends V> lc6 : lc6Arr) {
            map.put((Object) lc6.component1(), (Object) lc6.component2());
        }
    }

    @DexIgnore
    public static final <K, V> Map<K, V> n(Iterable<? extends Lc6<? extends K, ? extends V>> iterable) {
        Wg6.c(iterable, "$this$toMap");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            int size = collection.size();
            if (size == 0) {
                return g();
            }
            if (size != 1) {
                LinkedHashMap linkedHashMap = new LinkedHashMap(Ym7.b(collection.size()));
                o(iterable, linkedHashMap);
                return linkedHashMap;
            }
            return Ym7.c((Lc6) (iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next()));
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        o(iterable, linkedHashMap2);
        return k(linkedHashMap2);
    }

    @DexIgnore
    public static final <K, V, M extends Map<? super K, ? super V>> M o(Iterable<? extends Lc6<? extends K, ? extends V>> iterable, M m) {
        Wg6.c(iterable, "$this$toMap");
        Wg6.c(m, ShareConstants.DESTINATION);
        l(m, iterable);
        return m;
    }

    @DexIgnore
    public static final <K, V, M extends Map<? super K, ? super V>> M p(Lc6<? extends K, ? extends V>[] lc6Arr, M m) {
        Wg6.c(lc6Arr, "$this$toMap");
        Wg6.c(m, ShareConstants.DESTINATION);
        m(m, lc6Arr);
        return m;
    }
}
