package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.R62;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U42 extends Ec2<G52> {
    @DexIgnore
    public /* final */ GoogleSignInOptions E;

    @DexIgnore
    public U42(Context context, Looper looper, Ac2 ac2, GoogleSignInOptions googleSignInOptions, R62.Bi bi, R62.Ci ci) {
        super(context, looper, 91, ac2, bi, ci);
        googleSignInOptions = googleSignInOptions == null ? new GoogleSignInOptions.a().a() : googleSignInOptions;
        if (!ac2.d().isEmpty()) {
            GoogleSignInOptions.a aVar = new GoogleSignInOptions.a(googleSignInOptions);
            for (Scope scope : ac2.d()) {
                aVar.f(scope, new Scope[0]);
            }
            googleSignInOptions = aVar.a();
        }
        this.E = googleSignInOptions;
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi, com.fossil.Yb2
    public final boolean g() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final String p() {
        return "com.google.android.gms.auth.api.signin.internal.ISignInService";
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final /* synthetic */ IInterface q(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.signin.internal.ISignInService");
        return queryLocalInterface instanceof G52 ? (G52) queryLocalInterface : new H52(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi, com.fossil.Ec2, com.fossil.Yb2
    public final int s() {
        return H62.a;
    }

    @DexIgnore
    public final GoogleSignInOptions t0() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi, com.fossil.Yb2
    public final Intent u() {
        return V42.b(E(), this.E);
    }

    @DexIgnore
    @Override // com.fossil.Yb2
    public final String x() {
        return "com.google.android.gms.auth.api.signin.service.START";
    }
}
