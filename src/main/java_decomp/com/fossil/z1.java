package com.fossil;

import android.os.Parcel;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z1 extends C2 {
    @DexIgnore
    public static /* final */ X2 CREATOR; // = new X2(null);
    @DexIgnore
    public X1 e;

    @DexIgnore
    public Z1(byte b) {
        super(Lt.l, b, false, 4);
    }

    @DexIgnore
    public /* synthetic */ Z1(Parcel parcel, Qg6 qg6) {
        super(parcel);
    }

    @DexIgnore
    @Override // com.fossil.C2
    public byte[] a() {
        this.e = new X1(System.currentTimeMillis());
        ByteBuffer order = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
        X1 x1 = this.e;
        if (x1 != null) {
            ByteBuffer putInt = order.putInt((int) x1.b);
            X1 x12 = this.e;
            if (x12 != null) {
                ByteBuffer putShort = putInt.putShort((short) ((int) x12.c));
                X1 x13 = this.e;
                if (x13 != null) {
                    byte[] array = putShort.putShort((short) x13.d).array();
                    Wg6.b(array, "ByteBuffer.allocate(DATA\u2026\n                .array()");
                    return array;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.C2, com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = super.toJSONObject();
        Jd0 jd0 = Jd0.z0;
        X1 x1 = this.e;
        return G80.k(jSONObject, jd0, x1 != null ? x1.toJSONObject() : null);
    }
}
