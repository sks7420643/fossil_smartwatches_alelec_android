package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ia3 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Ia3> CREATOR; // = new Ua3();
    @DexIgnore
    public /* final */ List<LocationRequest> b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public Sa3 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ ArrayList<LocationRequest> a; // = new ArrayList<>();
        @DexIgnore
        public boolean b; // = false;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public final Ai a(LocationRequest locationRequest) {
            if (locationRequest != null) {
                this.a.add(locationRequest);
            }
            return this;
        }

        @DexIgnore
        public final Ia3 b() {
            return new Ia3(this.a, this.b, this.c, null);
        }
    }

    @DexIgnore
    public Ia3(List<LocationRequest> list, boolean z, boolean z2, Sa3 sa3) {
        this.b = list;
        this.c = z;
        this.d = z2;
        this.e = sa3;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.y(parcel, 1, Collections.unmodifiableList(this.b), false);
        Bd2.c(parcel, 2, this.c);
        Bd2.c(parcel, 3, this.d);
        Bd2.t(parcel, 5, this.e, i, false);
        Bd2.b(parcel, a2);
    }
}
