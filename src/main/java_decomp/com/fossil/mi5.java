package com.fossil;

import com.fossil.fitness.WorkoutType;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Mi5 {
    UNKNOWN("unknown"),
    RUNNING("running"),
    CYCLING("cycling"),
    TREADMILL("treadmill"),
    ELLIPTICAL("elliptical"),
    WEIGHTS("weights"),
    WORKOUT("workout"),
    WALKING("walking"),
    ROWING("rowing"),
    HIKING("hiking"),
    YOGA("yoga"),
    SWIMMING("swimming"),
    AEROBIC("aerobic"),
    SPINNING("spinning");
    
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Mi5 a(String str) {
            Mi5 mi5;
            Wg6.c(str, "value");
            Mi5[] values = Mi5.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    mi5 = null;
                    break;
                }
                mi5 = values[i];
                String mValue = mi5.getMValue();
                String lowerCase = str.toLowerCase();
                Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                if (Wg6.a(mValue, lowerCase)) {
                    break;
                }
                i++;
            }
            return mi5 != null ? mi5 : Mi5.UNKNOWN;
        }

        @DexIgnore
        public final Mi5 b(int i) {
            return i == WorkoutType.RUNNING.ordinal() ? Mi5.RUNNING : i == WorkoutType.CYCLING.ordinal() ? Mi5.CYCLING : i == WorkoutType.TREADMILL.ordinal() ? Mi5.TREADMILL : i == WorkoutType.ELLIPTICAL.ordinal() ? Mi5.ELLIPTICAL : i == WorkoutType.WEIGHTS.ordinal() ? Mi5.WEIGHTS : i == WorkoutType.WORKOUT.ordinal() ? Mi5.WORKOUT : i == WorkoutType.WALKING.ordinal() ? Mi5.WALKING : i == WorkoutType.ROWING.ordinal() ? Mi5.ROWING : i == WorkoutType.HIKING.ordinal() ? Mi5.HIKING : i == WorkoutType.YOGA.ordinal() ? Mi5.YOGA : i == WorkoutType.SWIMMING.ordinal() ? Mi5.SWIMMING : i == WorkoutType.AEROBIC.ordinal() ? Mi5.AEROBIC : i == WorkoutType.SPINNING.ordinal() ? Mi5.SPINNING : Mi5.UNKNOWN;
        }

        @DexIgnore
        public final WorkoutType c(Mi5 mi5) {
            if (mi5 != null) {
                switch (Li5.a[mi5.ordinal()]) {
                    case 1:
                        return WorkoutType.RUNNING;
                    case 2:
                        return WorkoutType.CYCLING;
                    case 3:
                        return WorkoutType.TREADMILL;
                    case 4:
                        return WorkoutType.ELLIPTICAL;
                    case 5:
                        return WorkoutType.WEIGHTS;
                    case 6:
                        return WorkoutType.WORKOUT;
                    case 7:
                        return WorkoutType.WALKING;
                    case 8:
                        return WorkoutType.ROWING;
                    case 9:
                        return WorkoutType.HIKING;
                    case 10:
                        return WorkoutType.YOGA;
                    case 11:
                        return WorkoutType.SWIMMING;
                    case 12:
                        return WorkoutType.AEROBIC;
                    case 13:
                        return WorkoutType.SPINNING;
                }
            }
            return WorkoutType.UNKNOWN;
        }
    }

    @DexIgnore
    public Mi5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        Wg6.c(str, "<set-?>");
        this.mValue = str;
    }
}
