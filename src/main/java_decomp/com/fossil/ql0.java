package com.fossil;

import android.graphics.Insets;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ql0 {
    @DexIgnore
    public static /* final */ Ql0 e; // = new Ql0(0, 0, 0, 0);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public Ql0(int i, int i2, int i3, int i4) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
    }

    @DexIgnore
    public static Ql0 a(int i, int i2, int i3, int i4) {
        return (i == 0 && i2 == 0 && i3 == 0 && i4 == 0) ? e : new Ql0(i, i2, i3, i4);
    }

    @DexIgnore
    public Insets b() {
        return Insets.of(this.a, this.b, this.c, this.d);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Ql0.class != obj.getClass()) {
            return false;
        }
        Ql0 ql0 = (Ql0) obj;
        if (this.d != ql0.d) {
            return false;
        }
        if (this.a != ql0.a) {
            return false;
        }
        if (this.c != ql0.c) {
            return false;
        }
        return this.b == ql0.b;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.a * 31) + this.b) * 31) + this.c) * 31) + this.d;
    }

    @DexIgnore
    public String toString() {
        return "Insets{left=" + this.a + ", top=" + this.b + ", right=" + this.c + ", bottom=" + this.d + '}';
    }
}
