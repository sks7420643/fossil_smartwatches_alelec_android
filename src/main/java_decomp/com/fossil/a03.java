package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A03 implements C03 {
    @DexIgnore
    public A03() {
    }

    @DexIgnore
    public /* synthetic */ A03(Wz2 wz2) {
        this();
    }

    @DexIgnore
    @Override // com.fossil.C03
    public final byte[] a(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }
}
