package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Nh7 implements Pi7 {
    @DexIgnore
    public /* final */ /* synthetic */ List a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ Gh7 c;

    @DexIgnore
    public Nh7(Gh7 gh7, List list, boolean z) {
        this.c = gh7;
        this.a = list;
        this.b = z;
    }

    @DexIgnore
    @Override // com.fossil.Pi7
    public void a() {
        Ig7.n();
        this.c.r(this.a, this.b, true);
    }

    @DexIgnore
    @Override // com.fossil.Pi7
    public void b() {
        Ig7.p();
        this.c.p(this.a, 1, this.b, true);
    }
}
