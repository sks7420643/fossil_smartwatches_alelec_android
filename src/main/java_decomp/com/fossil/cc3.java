package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Cc3 extends IInterface {
    @DexIgnore
    We3 H0() throws RemoteException;

    @DexIgnore
    Rg2 k0(LatLng latLng) throws RemoteException;

    @DexIgnore
    LatLng m2(Rg2 rg2) throws RemoteException;
}
