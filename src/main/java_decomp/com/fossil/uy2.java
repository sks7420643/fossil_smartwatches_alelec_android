package com.fossil;

import java.util.Iterator;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uy2<K> extends Ay2<K> {
    @DexIgnore
    public /* final */ transient Wx2<K, ?> d;
    @DexIgnore
    public /* final */ transient Sx2<K> e;

    @DexIgnore
    public Uy2(Wx2<K, ?> wx2, Sx2<K> sx2) {
        this.d = wx2;
        this.e = sx2;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final boolean contains(@NullableDecl Object obj) {
        return this.d.get(obj) != null;
    }

    @DexIgnore
    @Override // com.fossil.Tx2, com.fossil.Ay2, java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public final /* synthetic */ Iterator iterator() {
        return zzb();
    }

    @DexIgnore
    public final int size() {
        return this.d.size();
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final int zzb(Object[] objArr, int i) {
        return zzc().zzb(objArr, i);
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final Cz2<K> zzb() {
        return (Cz2) zzc().iterator();
    }

    @DexIgnore
    @Override // com.fossil.Tx2, com.fossil.Ay2
    public final Sx2<K> zzc() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Tx2
    public final boolean zzh() {
        return true;
    }
}
