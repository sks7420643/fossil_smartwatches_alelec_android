package com.fossil;

import com.google.android.gms.common.data.DataHolder;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Tb2<T> extends Pb2<T> {
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public ArrayList<Integer> d;

    @DexIgnore
    public Tb2(DataHolder dataHolder) {
        super(dataHolder);
    }

    @DexIgnore
    public String b() {
        return null;
    }

    @DexIgnore
    public abstract T c(int i, int i2);

    @DexIgnore
    public abstract String f();

    @DexIgnore
    @Override // com.fossil.Qb2
    public final T get(int i) {
        int i2;
        int intValue;
        int intValue2;
        h();
        int j = j(i);
        if (i < 0) {
            i2 = 0;
        } else if (i == this.d.size()) {
            i2 = 0;
        } else {
            if (i == this.d.size() - 1) {
                intValue = this.b.getCount();
                intValue2 = this.d.get(i).intValue();
            } else {
                intValue = this.d.get(i + 1).intValue();
                intValue2 = this.d.get(i).intValue();
            }
            i2 = intValue - intValue2;
            if (i2 == 1) {
                int j2 = j(i);
                int D = this.b.D(j2);
                String b = b();
                if (b != null && this.b.A(b, j2, D) == null) {
                    i2 = 0;
                }
            }
        }
        return c(j, i2);
    }

    @DexIgnore
    @Override // com.fossil.Qb2
    public int getCount() {
        h();
        return this.d.size();
    }

    @DexIgnore
    public final void h() {
        synchronized (this) {
            if (!this.c) {
                int count = this.b.getCount();
                ArrayList<Integer> arrayList = new ArrayList<>();
                this.d = arrayList;
                if (count > 0) {
                    arrayList.add(0);
                    String f = f();
                    String A = this.b.A(f, 0, this.b.D(0));
                    int i = 1;
                    while (i < count) {
                        int D = this.b.D(i);
                        String A2 = this.b.A(f, i, D);
                        if (A2 != null) {
                            if (!A2.equals(A)) {
                                this.d.add(Integer.valueOf(i));
                            } else {
                                A2 = A;
                            }
                            i++;
                            A = A2;
                        } else {
                            StringBuilder sb = new StringBuilder(String.valueOf(f).length() + 78);
                            sb.append("Missing value for markerColumn: ");
                            sb.append(f);
                            sb.append(", at row: ");
                            sb.append(i);
                            sb.append(", for window: ");
                            sb.append(D);
                            throw new NullPointerException(sb.toString());
                        }
                    }
                }
                this.c = true;
            }
        }
    }

    @DexIgnore
    public final int j(int i) {
        if (i >= 0 && i < this.d.size()) {
            return this.d.get(i).intValue();
        }
        StringBuilder sb = new StringBuilder(53);
        sb.append("Position ");
        sb.append(i);
        sb.append(" is out of bounds for this buffer");
        throw new IllegalArgumentException(sb.toString());
    }
}
