package com.fossil;

import android.content.ContentResolver;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.ContactsContract;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Hc1 extends Ec1<InputStream> {
    public static final UriMatcher e;

    /*
    static {
        UriMatcher uriMatcher = new UriMatcher(-1);
        e = uriMatcher;
        uriMatcher.addURI("com.android.contacts", "contacts/lookup/*/#", 1);
        e.addURI("com.android.contacts", "contacts/lookup/*", 1);
        e.addURI("com.android.contacts", "contacts/#/photo", 2);
        e.addURI("com.android.contacts", "contacts/#", 3);
        e.addURI("com.android.contacts", "contacts/#/display_photo", 4);
        e.addURI("com.android.contacts", "phone_lookup/*", 5);
    }
    */

    public Hc1(ContentResolver contentResolver, Uri uri) {
        super(contentResolver, uri);
    }

    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Ec1
    public /* bridge */ /* synthetic */ void b(InputStream inputStream) throws IOException {
        f(inputStream);
    }

    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Ec1
    public /* bridge */ /* synthetic */ InputStream e(Uri uri, ContentResolver contentResolver) throws FileNotFoundException {
        return g(uri, contentResolver);
    }

    public void f(InputStream inputStream) throws IOException {
        inputStream.close();
    }

    public InputStream g(Uri uri, ContentResolver contentResolver) throws FileNotFoundException {
        InputStream h = h(uri, contentResolver);
        if (h != null) {
            return h;
        }
        throw new FileNotFoundException("InputStream is null for " + uri);
    }

    @Override // com.fossil.Wb1
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    public final InputStream h(Uri uri, ContentResolver contentResolver) throws FileNotFoundException {
        int match = e.match(uri);
        if (match != 1) {
            if (match == 3) {
                return i(contentResolver, uri);
            }
            if (match != 5) {
                return contentResolver.openInputStream(uri);
            }
        }
        Uri lookupContact = ContactsContract.Contacts.lookupContact(contentResolver, uri);
        if (lookupContact != null) {
            return i(contentResolver, lookupContact);
        }
        throw new FileNotFoundException("Contact cannot be found");
    }

    public final InputStream i(ContentResolver contentResolver, Uri uri) {
        return ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri, true);
    }
}
