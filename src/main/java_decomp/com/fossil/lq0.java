package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import androidx.fragment.app.Fragment;
import com.fossil.Om0;
import com.fossil.Yq0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Lq0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Om0.Ai {
        @DexIgnore
        public /* final */ /* synthetic */ Fragment a;

        @DexIgnore
        public Ai(Fragment fragment) {
            this.a = fragment;
        }

        @DexIgnore
        @Override // com.fossil.Om0.Ai
        public void onCancel() {
            if (this.a.getAnimatingAway() != null) {
                View animatingAway = this.a.getAnimatingAway();
                this.a.setAnimatingAway(null);
                animatingAway.clearAnimation();
            }
            this.a.setAnimator(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ ViewGroup b;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment c;
        @DexIgnore
        public /* final */ /* synthetic */ Yq0.Gi d;
        @DexIgnore
        public /* final */ /* synthetic */ Om0 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Runnable {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            public void run() {
                if (Bi.this.c.getAnimatingAway() != null) {
                    Bi.this.c.setAnimatingAway(null);
                    Bi bi = Bi.this;
                    bi.d.a(bi.c, bi.e);
                }
            }
        }

        @DexIgnore
        public Bi(ViewGroup viewGroup, Fragment fragment, Yq0.Gi gi, Om0 om0) {
            this.b = viewGroup;
            this.c = fragment;
            this.d = gi;
            this.e = om0;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            this.b.post(new Aii());
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ ViewGroup a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment c;
        @DexIgnore
        public /* final */ /* synthetic */ Yq0.Gi d;
        @DexIgnore
        public /* final */ /* synthetic */ Om0 e;

        @DexIgnore
        public Ci(ViewGroup viewGroup, View view, Fragment fragment, Yq0.Gi gi, Om0 om0) {
            this.a = viewGroup;
            this.b = view;
            this.c = fragment;
            this.d = gi;
            this.e = om0;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.a.endViewTransition(this.b);
            Animator animator2 = this.c.getAnimator();
            this.c.setAnimator(null);
            if (animator2 != null && this.a.indexOfChild(this.b) < 0) {
                this.d.a(this.c, this.e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di {
        @DexIgnore
        public /* final */ Animation a;
        @DexIgnore
        public /* final */ Animator b;

        @DexIgnore
        public Di(Animator animator) {
            this.a = null;
            this.b = animator;
            if (animator == null) {
                throw new IllegalStateException("Animator cannot be null");
            }
        }

        @DexIgnore
        public Di(Animation animation) {
            this.a = animation;
            this.b = null;
            if (animation == null) {
                throw new IllegalStateException("Animation cannot be null");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ei extends AnimationSet implements Runnable {
        @DexIgnore
        public /* final */ ViewGroup b;
        @DexIgnore
        public /* final */ View c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public boolean f; // = true;

        @DexIgnore
        public Ei(Animation animation, ViewGroup viewGroup, View view) {
            super(false);
            this.b = viewGroup;
            this.c = view;
            addAnimation(animation);
            this.b.post(this);
        }

        @DexIgnore
        public boolean getTransformation(long j, Transformation transformation) {
            this.f = true;
            if (this.d) {
                return !this.e;
            }
            if (super.getTransformation(j, transformation)) {
                return true;
            }
            this.d = true;
            Jo0.a(this.b, this);
            return true;
        }

        @DexIgnore
        public boolean getTransformation(long j, Transformation transformation, float f2) {
            this.f = true;
            if (this.d) {
                return !this.e;
            }
            if (super.getTransformation(j, transformation, f2)) {
                return true;
            }
            this.d = true;
            Jo0.a(this.b, this);
            return true;
        }

        @DexIgnore
        public void run() {
            if (this.d || !this.f) {
                this.b.endViewTransition(this.c);
                this.e = true;
                return;
            }
            this.f = false;
            this.b.post(this);
        }
    }

    @DexIgnore
    public static void a(Fragment fragment, Di di, Yq0.Gi gi) {
        View view = fragment.mView;
        ViewGroup viewGroup = fragment.mContainer;
        viewGroup.startViewTransition(view);
        Om0 om0 = new Om0();
        om0.d(new Ai(fragment));
        gi.b(fragment, om0);
        if (di.a != null) {
            Ei ei = new Ei(di.a, viewGroup, view);
            fragment.setAnimatingAway(fragment.mView);
            ei.setAnimationListener(new Bi(viewGroup, fragment, gi, om0));
            fragment.mView.startAnimation(ei);
            return;
        }
        Animator animator = di.b;
        fragment.setAnimator(animator);
        animator.addListener(new Ci(viewGroup, view, fragment, gi, om0));
        animator.setTarget(fragment.mView);
        animator.start();
    }

    @DexIgnore
    public static Di b(Context context, Mq0 mq0, Fragment fragment, boolean z) {
        int c;
        boolean z2;
        int nextTransition = fragment.getNextTransition();
        int nextAnim = fragment.getNextAnim();
        fragment.setNextAnim(0);
        View b = mq0.b(fragment.mContainerId);
        if (!(b == null || b.getTag(Gq0.visible_removing_fragment_view_tag) == null)) {
            b.setTag(Gq0.visible_removing_fragment_view_tag, null);
        }
        ViewGroup viewGroup = fragment.mContainer;
        if (viewGroup != null && viewGroup.getLayoutTransition() != null) {
            return null;
        }
        Animation onCreateAnimation = fragment.onCreateAnimation(nextTransition, z, nextAnim);
        if (onCreateAnimation != null) {
            return new Di(onCreateAnimation);
        }
        Animator onCreateAnimator = fragment.onCreateAnimator(nextTransition, z, nextAnim);
        if (onCreateAnimator != null) {
            return new Di(onCreateAnimator);
        }
        if (nextAnim != 0) {
            boolean equals = "anim".equals(context.getResources().getResourceTypeName(nextAnim));
            if (equals) {
                try {
                    Animation loadAnimation = AnimationUtils.loadAnimation(context, nextAnim);
                    if (loadAnimation != null) {
                        return new Di(loadAnimation);
                    }
                    z2 = true;
                } catch (Resources.NotFoundException e) {
                    throw e;
                } catch (RuntimeException e2) {
                    z2 = false;
                }
            } else {
                z2 = false;
            }
            if (!z2) {
                try {
                    Animator loadAnimator = AnimatorInflater.loadAnimator(context, nextAnim);
                    if (loadAnimator != null) {
                        return new Di(loadAnimator);
                    }
                } catch (RuntimeException e3) {
                    if (!equals) {
                        Animation loadAnimation2 = AnimationUtils.loadAnimation(context, nextAnim);
                        if (loadAnimation2 != null) {
                            return new Di(loadAnimation2);
                        }
                    } else {
                        throw e3;
                    }
                }
            }
        }
        if (nextTransition == 0 || (c = c(nextTransition, z)) < 0) {
            return null;
        }
        return new Di(AnimationUtils.loadAnimation(context, c));
    }

    @DexIgnore
    public static int c(int i, boolean z) {
        if (i == 4097) {
            return z ? Fq0.fragment_open_enter : Fq0.fragment_open_exit;
        }
        if (i == 4099) {
            return z ? Fq0.fragment_fade_enter : Fq0.fragment_fade_exit;
        }
        if (i != 8194) {
            return -1;
        }
        return z ? Fq0.fragment_close_enter : Fq0.fragment_close_exit;
    }
}
