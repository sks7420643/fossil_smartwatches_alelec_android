package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Bg5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView q;

    @DexIgnore
    public Bg5(Object obj, View view, int i, ImageView imageView) {
        super(obj, view, i);
        this.q = imageView;
    }

    @DexIgnore
    @Deprecated
    public static Bg5 A(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (Bg5) ViewDataBinding.p(layoutInflater, 2131558722, viewGroup, z, obj);
    }

    @DexIgnore
    public static Bg5 z(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return A(layoutInflater, viewGroup, z, Aq0.d());
    }
}
