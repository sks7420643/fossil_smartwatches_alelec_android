package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class U31 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Ai {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b;
        @DexIgnore
        public static /* final */ /* synthetic */ int[] c;

        /*
        static {
            int[] iArr = new int[Y01.values().length];
            c = iArr;
            try {
                iArr[Y01.NOT_REQUIRED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                c[Y01.CONNECTED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                c[Y01.UNMETERED.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                c[Y01.NOT_ROAMING.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                c[Y01.METERED.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            int[] iArr2 = new int[N01.values().length];
            b = iArr2;
            try {
                iArr2[N01.EXPONENTIAL.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            try {
                b[N01.LINEAR.ordinal()] = 2;
            } catch (NoSuchFieldError e7) {
            }
            int[] iArr3 = new int[F11.values().length];
            a = iArr3;
            try {
                iArr3[F11.ENQUEUED.ordinal()] = 1;
            } catch (NoSuchFieldError e8) {
            }
            try {
                a[F11.RUNNING.ordinal()] = 2;
            } catch (NoSuchFieldError e9) {
            }
            try {
                a[F11.SUCCEEDED.ordinal()] = 3;
            } catch (NoSuchFieldError e10) {
            }
            try {
                a[F11.FAILED.ordinal()] = 4;
            } catch (NoSuchFieldError e11) {
            }
            try {
                a[F11.BLOCKED.ordinal()] = 5;
            } catch (NoSuchFieldError e12) {
            }
            try {
                a[F11.CANCELLED.ordinal()] = 6;
            } catch (NoSuchFieldError e13) {
            }
        }
        */
    }

    @DexIgnore
    public static int a(N01 n01) {
        int i = Ai.b[n01.ordinal()];
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        throw new IllegalArgumentException("Could not convert " + n01 + " to int");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0040 A[SYNTHETIC, Splitter:B:23:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0055 A[SYNTHETIC, Splitter:B:33:0x0055] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.Q01 b(byte[] r6) {
        /*
            r2 = 0
            com.fossil.Q01 r3 = new com.fossil.Q01
            r3.<init>()
            if (r6 != 0) goto L_0x000a
            r0 = r3
        L_0x0009:
            return r0
        L_0x000a:
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream
            r4.<init>(r6)
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x0039, all -> 0x0066 }
            r1.<init>(r4)     // Catch:{ IOException -> 0x0039, all -> 0x0066 }
            int r0 = r1.readInt()     // Catch:{ IOException -> 0x0068 }
        L_0x0018:
            if (r0 <= 0) goto L_0x002c
            java.lang.String r2 = r1.readUTF()     // Catch:{ IOException -> 0x0068 }
            android.net.Uri r2 = android.net.Uri.parse(r2)     // Catch:{ IOException -> 0x0068 }
            boolean r5 = r1.readBoolean()     // Catch:{ IOException -> 0x0068 }
            r3.a(r2, r5)     // Catch:{ IOException -> 0x0068 }
            int r0 = r0 + -1
            goto L_0x0018
        L_0x002c:
            r1.close()     // Catch:{ IOException -> 0x0034 }
        L_0x002f:
            r4.close()     // Catch:{ IOException -> 0x0047 }
        L_0x0032:
            r0 = r3
            goto L_0x0009
        L_0x0034:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x002f
        L_0x0039:
            r0 = move-exception
            r1 = r2
        L_0x003b:
            r0.printStackTrace()     // Catch:{ all -> 0x0051 }
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ IOException -> 0x004c }
        L_0x0043:
            r4.close()
            goto L_0x0032
        L_0x0047:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0032
        L_0x004c:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0043
        L_0x0051:
            r0 = move-exception
            r2 = r1
        L_0x0053:
            if (r2 == 0) goto L_0x0058
            r2.close()     // Catch:{ IOException -> 0x005c }
        L_0x0058:
            r4.close()     // Catch:{ IOException -> 0x0061 }
        L_0x005b:
            throw r0
        L_0x005c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0058
        L_0x0061:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005b
        L_0x0066:
            r0 = move-exception
            goto L_0x0053
        L_0x0068:
            r0 = move-exception
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.U31.b(byte[]):com.fossil.Q01");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0046 A[SYNTHETIC, Splitter:B:14:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0066 A[SYNTHETIC, Splitter:B:29:0x0066] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] c(com.fossil.Q01 r5) {
        /*
            r2 = 0
            int r0 = r5.c()
            if (r0 != 0) goto L_0x0008
        L_0x0007:
            return r2
        L_0x0008:
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream
            r3.<init>()
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x006d, all -> 0x0062 }
            r1.<init>(r3)     // Catch:{ IOException -> 0x006d, all -> 0x0062 }
            int r0 = r5.c()     // Catch:{ IOException -> 0x0040, all -> 0x0082 }
            r1.writeInt(r0)     // Catch:{ IOException -> 0x0040, all -> 0x0082 }
            java.util.Set r0 = r5.b()     // Catch:{ IOException -> 0x0040, all -> 0x0082 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ IOException -> 0x0040, all -> 0x0082 }
        L_0x0021:
            boolean r0 = r2.hasNext()     // Catch:{ IOException -> 0x0040, all -> 0x0082 }
            if (r0 == 0) goto L_0x0051
            java.lang.Object r0 = r2.next()     // Catch:{ IOException -> 0x0040, all -> 0x0082 }
            com.fossil.Q01$Ai r0 = (com.fossil.Q01.Ai) r0     // Catch:{ IOException -> 0x0040, all -> 0x0082 }
            android.net.Uri r4 = r0.a()     // Catch:{ IOException -> 0x0040, all -> 0x0082 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0040, all -> 0x0082 }
            r1.writeUTF(r4)     // Catch:{ IOException -> 0x0040, all -> 0x0082 }
            boolean r0 = r0.b()     // Catch:{ IOException -> 0x0040, all -> 0x0082 }
            r1.writeBoolean(r0)     // Catch:{ IOException -> 0x0040, all -> 0x0082 }
            goto L_0x0021
        L_0x0040:
            r0 = move-exception
        L_0x0041:
            r0.printStackTrace()     // Catch:{ all -> 0x007f }
            if (r1 == 0) goto L_0x0049
            r1.close()     // Catch:{ IOException -> 0x0070 }
        L_0x0049:
            r3.close()     // Catch:{ IOException -> 0x0058 }
        L_0x004c:
            byte[] r2 = r3.toByteArray()
            goto L_0x0007
        L_0x0051:
            r1.close()     // Catch:{ IOException -> 0x005d }
        L_0x0054:
            r3.close()
            goto L_0x004c
        L_0x0058:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004c
        L_0x005d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0054
        L_0x0062:
            r0 = move-exception
        L_0x0063:
            r1 = r2
        L_0x0064:
            if (r1 == 0) goto L_0x0069
            r1.close()     // Catch:{ IOException -> 0x0075 }
        L_0x0069:
            r3.close()     // Catch:{ IOException -> 0x007a }
        L_0x006c:
            throw r0
        L_0x006d:
            r0 = move-exception
            r1 = r2
            goto L_0x0041
        L_0x0070:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0049
        L_0x0075:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0069
        L_0x007a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x006c
        L_0x007f:
            r0 = move-exception
            r2 = r1
            goto L_0x0063
        L_0x0082:
            r0 = move-exception
            goto L_0x0064
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.U31.c(com.fossil.Q01):byte[]");
    }

    @DexIgnore
    public static N01 d(int i) {
        if (i == 0) {
            return N01.EXPONENTIAL;
        }
        if (i == 1) {
            return N01.LINEAR;
        }
        throw new IllegalArgumentException("Could not convert " + i + " to BackoffPolicy");
    }

    @DexIgnore
    public static Y01 e(int i) {
        if (i == 0) {
            return Y01.NOT_REQUIRED;
        }
        if (i == 1) {
            return Y01.CONNECTED;
        }
        if (i == 2) {
            return Y01.UNMETERED;
        }
        if (i == 3) {
            return Y01.NOT_ROAMING;
        }
        if (i == 4) {
            return Y01.METERED;
        }
        throw new IllegalArgumentException("Could not convert " + i + " to NetworkType");
    }

    @DexIgnore
    public static F11 f(int i) {
        if (i == 0) {
            return F11.ENQUEUED;
        }
        if (i == 1) {
            return F11.RUNNING;
        }
        if (i == 2) {
            return F11.SUCCEEDED;
        }
        if (i == 3) {
            return F11.FAILED;
        }
        if (i == 4) {
            return F11.BLOCKED;
        }
        if (i == 5) {
            return F11.CANCELLED;
        }
        throw new IllegalArgumentException("Could not convert " + i + " to State");
    }

    @DexIgnore
    public static int g(Y01 y01) {
        int i = Ai.c[y01.ordinal()];
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        if (i == 3) {
            return 2;
        }
        if (i == 4) {
            return 3;
        }
        if (i == 5) {
            return 4;
        }
        throw new IllegalArgumentException("Could not convert " + y01 + " to int");
    }

    @DexIgnore
    public static int h(F11 f11) {
        switch (Ai.a[f11.ordinal()]) {
            case 1:
                return 0;
            case 2:
                return 1;
            case 3:
                return 2;
            case 4:
                return 3;
            case 5:
                return 4;
            case 6:
                return 5;
            default:
                throw new IllegalArgumentException("Could not convert " + f11 + " to int");
        }
    }
}
