package com.fossil;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Er4 extends RecyclerView.g<Bi> {
    @DexIgnore
    public /* final */ ArrayList<Ringtone> a; // = new ArrayList<>();
    @DexIgnore
    public Ringtone b;
    @DexIgnore
    public /* final */ Ai c;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void p2(Ringtone ringtone);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public ImageView a;
        @DexIgnore
        public View b;
        @DexIgnore
        public /* final */ TextView c;
        @DexIgnore
        public /* final */ ConstraintLayout d;
        @DexIgnore
        public /* final */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ Er4 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Er4 er4 = this.b.f;
                Object obj = er4.a.get(this.b.getAdapterPosition());
                Wg6.b(obj, "mRingPhones[adapterPosition]");
                er4.b = (Ringtone) obj;
                this.b.f.c.p2(Er4.i(this.b.f));
                this.b.f.notifyDataSetChanged();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Er4 er4, View view) {
            super(view);
            Wg6.c(view, "view");
            this.f = er4;
            View findViewById = view.findViewById(2131362747);
            Wg6.b(findViewById, "view.findViewById(R.id.iv_ringphone)");
            this.a = (ImageView) findViewById;
            View findViewById2 = view.findViewById(2131362748);
            Wg6.b(findViewById2, "view.findViewById(R.id.iv_ringphone_selected)");
            this.b = findViewById2;
            View findViewById3 = view.findViewById(2131363386);
            Wg6.b(findViewById3, "view.findViewById(R.id.tv_ring_phone)");
            this.c = (TextView) findViewById3;
            View findViewById4 = view.findViewById(2131363010);
            Wg6.b(findViewById4, "view.findViewById(R.id.root_background)");
            this.d = (ConstraintLayout) findViewById4;
            View findViewById5 = view.findViewById(2131362632);
            Wg6.b(findViewById5, "view.findViewById(R.id.indicator)");
            this.e = findViewById5;
            String d2 = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d2)) {
                this.d.setBackgroundColor(Color.parseColor(d2));
            }
            String d3 = ThemeManager.l.a().d("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(d3)) {
                this.e.setBackgroundColor(Color.parseColor(d3));
            }
            this.a.setOnClickListener(new Aii(this));
        }

        @DexIgnore
        public final void a(Ringtone ringtone) {
            Wg6.c(ringtone, "ringTone");
            this.c.setText(ringtone.getRingtoneName());
            this.b.setVisibility(0);
            if (Wg6.a(ringtone.getRingtoneName(), Er4.i(this.f).getRingtoneName())) {
                this.b.setVisibility(0);
            } else {
                this.b.setVisibility(8);
            }
        }
    }

    @DexIgnore
    public Er4(Ai ai) {
        Wg6.c(ai, "mListener");
        this.c = ai;
    }

    @DexIgnore
    public static final /* synthetic */ Ringtone i(Er4 er4) {
        Ringtone ringtone = er4.b;
        if (ringtone != null) {
            return ringtone;
        }
        Wg6.n("mSelectedRingPhone");
        throw null;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public final void k(List<Ringtone> list, Ringtone ringtone) {
        Wg6.c(list, "data");
        Wg6.c(ringtone, "selectedRingtone");
        this.a.clear();
        this.a.addAll(list);
        this.b = ringtone;
        notifyDataSetChanged();
    }

    @DexIgnore
    public void l(Bi bi, int i) {
        Wg6.c(bi, "holder");
        int adapterPosition = bi.getAdapterPosition();
        if (getItemCount() > adapterPosition && adapterPosition != -1) {
            Ringtone ringtone = this.a.get(adapterPosition);
            Wg6.b(ringtone, "mRingPhones[adapterPos]");
            bi.a(ringtone);
        }
    }

    @DexIgnore
    public Bi m(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558712, viewGroup, false);
        Wg6.b(inflate, "LayoutInflater.from(pare\u2026ing_phone, parent, false)");
        return new Bi(this, inflate);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        l(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return m(viewGroup, i);
    }
}
