package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ml5 {
    @DexIgnore
    public static /* final */ Ml5 a; // = new Ml5();

    @DexIgnore
    public final String a(Integer num) {
        String c = Dl5.c(num != null ? (float) num.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
        Wg6.b(c, "NumberHelper.decimalForm\u2026Time?.toFloat() ?: 0F, 0)");
        return c;
    }

    @DexIgnore
    public final String b(Float f) {
        String c = Dl5.c(f != null ? f.floatValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0);
        Wg6.b(c, "NumberHelper.decimalForm\u2026Number(calories ?: 0F, 0)");
        return c;
    }

    @DexIgnore
    public final String c(Float f, Ai5 ai5) {
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        Wg6.c(ai5, Constants.PROFILE_KEY_UNIT);
        if (ai5 == Ai5.IMPERIAL) {
            if (f != null) {
                f2 = f.floatValue();
            }
            String c = Dl5.c(Jk5.l(f2), 1);
            Wg6.b(c, "NumberHelper.decimalForm\u2026Miles(distance ?: 0F), 1)");
            return c;
        }
        if (f != null) {
            f2 = f.floatValue();
        }
        String c2 = Dl5.c(Jk5.k(f2), 1);
        Wg6.b(c2, "NumberHelper.decimalForm\u2026eters(distance ?: 0F), 1)");
        return c2;
    }

    @DexIgnore
    public final String d(Integer num) {
        String c = Dl5.c(num != null ? (float) num.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 2);
        Wg6.b(c, "NumberHelper.decimalForm\u2026teps?.toFloat() ?: 0F, 2)");
        return c;
    }
}
