package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Q54 {
    @DexIgnore
    public StringBuilder a; // = new StringBuilder();
    @DexIgnore
    public boolean b;

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x001c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(char[] r9, int r10, int r11) throws java.io.IOException {
        /*
            r8 = this;
            r2 = 0
            r7 = 10
            r1 = 1
            boolean r0 = r8.b
            if (r0 == 0) goto L_0x0029
            if (r11 <= 0) goto L_0x0029
            char r0 = r9[r10]
            if (r0 != r7) goto L_0x0027
            r0 = r1
        L_0x000f:
            r8.c(r0)
            if (r0 == 0) goto L_0x0029
            int r0 = r10 + 1
        L_0x0016:
            int r5 = r10 + r11
            r3 = r0
            r4 = r0
        L_0x001a:
            if (r4 >= r5) goto L_0x0055
            char r0 = r9[r4]
            if (r0 == r7) goto L_0x0049
            r6 = 13
            if (r0 == r6) goto L_0x002b
        L_0x0024:
            int r4 = r4 + 1
            goto L_0x001a
        L_0x0027:
            r0 = r2
            goto L_0x000f
        L_0x0029:
            r0 = r10
            goto L_0x0016
        L_0x002b:
            java.lang.StringBuilder r0 = r8.a
            int r6 = r4 - r3
            r0.append(r9, r3, r6)
            r8.b = r1
            int r3 = r4 + 1
            if (r3 >= r5) goto L_0x005d
            char r0 = r9[r3]
            if (r0 != r7) goto L_0x0047
            r0 = r1
        L_0x003d:
            r8.c(r0)
            if (r0 == 0) goto L_0x005d
            r0 = r3
        L_0x0043:
            int r3 = r0 + 1
            r4 = r0
            goto L_0x0024
        L_0x0047:
            r0 = r2
            goto L_0x003d
        L_0x0049:
            java.lang.StringBuilder r0 = r8.a
            int r6 = r4 - r3
            r0.append(r9, r3, r6)
            r8.c(r1)
            r0 = r4
            goto L_0x0043
        L_0x0055:
            java.lang.StringBuilder r0 = r8.a
            int r1 = r5 - r3
            r0.append(r9, r3, r1)
            return
        L_0x005d:
            r0 = r4
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Q54.a(char[], int, int):void");
    }

    @DexIgnore
    public void b() throws IOException {
        if (this.b || this.a.length() > 0) {
            c(false);
        }
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public final boolean c(boolean z) throws IOException {
        d(this.a.toString(), this.b ? z ? "\r\n" : "\r" : z ? "\n" : "");
        this.a = new StringBuilder();
        this.b = false;
        return z;
    }

    @DexIgnore
    public abstract void d(String str, String str2) throws IOException;
}
