package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ke7 {
    @DexIgnore
    public static Map<String, Object> a(Ie7 ie7) {
        Be7 b = ie7.b();
        if (b == null) {
            return null;
        }
        HashMap hashMap = new HashMap();
        hashMap.put("sql", b.e());
        hashMap.put("arguments", b.d());
        return hashMap;
    }
}
