package com.fossil;

import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase;
import com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nu6 implements Factory<WorkoutSettingViewModel> {
    @DexIgnore
    public /* final */ Provider<SetWorkoutSettingUseCase> a;
    @DexIgnore
    public /* final */ Provider<WorkoutSettingRepository> b;

    @DexIgnore
    public Nu6(Provider<SetWorkoutSettingUseCase> provider, Provider<WorkoutSettingRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Nu6 a(Provider<SetWorkoutSettingUseCase> provider, Provider<WorkoutSettingRepository> provider2) {
        return new Nu6(provider, provider2);
    }

    @DexIgnore
    public static WorkoutSettingViewModel c(SetWorkoutSettingUseCase setWorkoutSettingUseCase, WorkoutSettingRepository workoutSettingRepository) {
        return new WorkoutSettingViewModel(setWorkoutSettingUseCase, workoutSettingRepository);
    }

    @DexIgnore
    public WorkoutSettingViewModel b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
