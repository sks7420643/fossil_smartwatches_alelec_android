package com.fossil;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.iq4;
import com.fossil.wq5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import java.io.File;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wb7 extends iq4<b, c, a> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ d e; // = new d();
    @DexIgnore
    public /* final */ DianaWatchFaceRepository f;
    @DexIgnore
    public /* final */ FileRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements iq4.a {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ int f3912a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public a(int i, ArrayList<Integer> arrayList) {
            pq7.c(arrayList, "mBLEErrorCodes");
            this.f3912a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.f3912a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iq4.b {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ String f3913a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public b(String str, String str2, String str3) {
            pq7.c(str, "watchFaceId");
            this.f3913a = str;
            this.b = str2;
            this.c = str3;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(String str, String str2, String str3, int i, kq7 kq7) {
            this(str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? null : str3);
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final String c() {
            return this.f3913a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements iq4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements wq5.b {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.wq5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            pq7.c(communicateMode, "communicateMode");
            pq7.c(intent, "intent");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PreviewWatchFaceUseCase", "onReceive - isExecuted " + wb7.this.o() + " communicateMode=" + communicateMode);
            if (wb7.this.o() && communicateMode == CommunicateMode.PREVIEW_THEME_SESSION) {
                wb7.this.r(false);
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    FLogger.INSTANCE.getLocal().d("PreviewWatchFaceUseCase", "onReceive - success");
                    wb7.this.j(new c());
                    return;
                }
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("PreviewWatchFaceUseCase", "onReceive - failed erorCode " + intExtra + " permissionErrorCodes " + integerArrayListExtra);
                wb7.this.i(new a(intExtra, integerArrayListExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends ko7 implements vp7<iv7, qn7<? super File>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ qn7 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ b $requestValues$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wb7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(qn7 qn7, wb7 wb7, b bVar, qn7 qn72) {
            super(2, qn7);
            this.this$0 = wb7;
            this.$requestValues$inlined = bVar;
            this.$continuation$inlined = qn72;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(qn7, this.this$0, this.$requestValues$inlined, this.$continuation$inlined);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super File> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return this.this$0.g.getFileByRemoteUrl(this.$requestValues$inlined.b());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends ko7 implements vp7<iv7, qn7<? super DianaWatchFaceUser>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ qn7 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ b $requestValues$inlined;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wb7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(qn7 qn7, wb7 wb7, b bVar, qn7 qn72) {
            super(2, qn7);
            this.this$0 = wb7;
            this.$requestValues$inlined = bVar;
            this.$continuation$inlined = qn72;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            f fVar = new f(qn7, this.this$0, this.$requestValues$inlined, this.$continuation$inlined);
            fVar.p$ = (iv7) obj;
            throw null;
            //return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super DianaWatchFaceUser> qn7) {
            throw null;
            //return ((f) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                DianaWatchFaceRepository dianaWatchFaceRepository = this.this$0.f;
                String c = this.$requestValues$inlined.c();
                this.L$0 = iv7;
                this.label = 1;
                Object dianaWatchFaceUserById = dianaWatchFaceRepository.getDianaWatchFaceUserById(c, this);
                return dianaWatchFaceUserById == d ? d : dianaWatchFaceUserById;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends ko7 implements vp7<iv7, qn7<? super File>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ qn7 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ b $requestValues$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $watchFace;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wb7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(dr7 dr7, qn7 qn7, wb7 wb7, b bVar, qn7 qn72) {
            super(2, qn7);
            this.$watchFace = dr7;
            this.this$0 = wb7;
            this.$requestValues$inlined = bVar;
            this.$continuation$inlined = qn72;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.$watchFace, qn7, this.this$0, this.$requestValues$inlined, this.$continuation$inlined);
            gVar.p$ = (iv7) obj;
            throw null;
            //return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super File> qn7) {
            throw null;
            //return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return this.this$0.g.getFileByFileName(this.$watchFace.element.getId());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends ko7 implements vp7<iv7, qn7<? super Boolean>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ qn7 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ b $requestValues$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $watchFace;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wb7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(dr7 dr7, qn7 qn7, wb7 wb7, b bVar, qn7 qn72) {
            super(2, qn7);
            this.$watchFace = dr7;
            this.this$0 = wb7;
            this.$requestValues$inlined = bVar;
            this.$continuation$inlined = qn72;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.$watchFace, qn7, this.this$0, this.$requestValues$inlined, this.$continuation$inlined);
            hVar.p$ = (iv7) obj;
            throw null;
            //return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Boolean> qn7) {
            throw null;
            //return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                FileRepository fileRepository = this.this$0.g;
                String downloadURL = this.$watchFace.element.getDownloadURL();
                String id = this.$watchFace.element.getId();
                FileType fileType = FileType.WATCH_FACE;
                String checksum = this.$watchFace.element.getChecksum();
                this.L$0 = iv7;
                this.label = 1;
                Object downloadAndSaveWithFileName = fileRepository.downloadAndSaveWithFileName(downloadURL, id, fileType, checksum, this);
                return downloadAndSaveWithFileName == d ? d : downloadAndSaveWithFileName;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends ko7 implements vp7<iv7, qn7<? super File>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ qn7 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ b $requestValues$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ dr7 $watchFace;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wb7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(dr7 dr7, qn7 qn7, wb7 wb7, b bVar, qn7 qn72) {
            super(2, qn7);
            this.$watchFace = dr7;
            this.this$0 = wb7;
            this.$requestValues$inlined = bVar;
            this.$continuation$inlined = qn72;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            i iVar = new i(this.$watchFace, qn7, this.this$0, this.$requestValues$inlined, this.$continuation$inlined);
            iVar.p$ = (iv7) obj;
            throw null;
            //return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super File> qn7) {
            throw null;
            //return ((i) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            yn7.d();
            if (this.label == 0) {
                el7.b(obj);
                return this.this$0.g.getFileByFileName(this.$watchFace.element.getId());
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.watchface.usecase.PreviewWatchFaceUseCase", f = "PreviewWatchFaceUseCase.kt", l = {54, 55, 58, 63, 66, 69, 72, 81}, m = "run")
    public static final class j extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ wb7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(wb7 wb7, qn7 qn7) {
            super(qn7);
            this.this$0 = wb7;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.k(null, this);
        }
    }

    @DexIgnore
    public wb7(DianaWatchFaceRepository dianaWatchFaceRepository, FileRepository fileRepository) {
        pq7.c(dianaWatchFaceRepository, "mWatchFaceRepository");
        pq7.c(fileRepository, "mFileRepository");
        this.f = dianaWatchFaceRepository;
        this.g = fileRepository;
    }

    @DexIgnore
    @Override // com.fossil.iq4
    public String h() {
        return "PreviewWatchFaceUseCase";
    }

    @DexIgnore
    public final boolean o() {
        return this.d;
    }

    @DexIgnore
    public final void p() {
        wq5.d.e(this.e, CommunicateMode.PREVIEW_THEME_SESSION);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0047, code lost:
        if (r1 != null) goto L_0x0049;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0130  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x015c  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x017c  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x01c4  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01e2  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01f9  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0221  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0224  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x029b  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x02d2  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x02e0  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* renamed from: q */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object k(com.fossil.wb7.b r13, com.fossil.qn7<java.lang.Object> r14) {
        /*
        // Method dump skipped, instructions count: 796
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wb7.k(com.fossil.wb7$b, com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void r(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final void s() {
        wq5.d.j(this.e, CommunicateMode.PREVIEW_THEME_SESSION);
    }
}
