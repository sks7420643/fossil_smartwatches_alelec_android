package com.fossil;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Nh5 {
    WALKING("walking"),
    RUNNING("running"),
    BIKING("biking");
    
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    public Nh5(String str) {
        this.value = str;
    }

    @DexIgnore
    public static Nh5 fromString(String str) {
        if (!TextUtils.isEmpty(str)) {
            Nh5[] values = values();
            for (Nh5 nh5 : values) {
                if (str.equalsIgnoreCase(nh5.value)) {
                    return nh5;
                }
            }
        }
        return WALKING;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }
}
