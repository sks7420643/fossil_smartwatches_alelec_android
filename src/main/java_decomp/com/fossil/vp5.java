package com.fossil;

import com.mapped.An4;
import com.mapped.Cj4;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.receiver.AppPackageInstallReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vp5 implements MembersInjector<AppPackageInstallReceiver> {
    @DexIgnore
    public static void a(AppPackageInstallReceiver appPackageInstallReceiver, Cj4 cj4) {
        appPackageInstallReceiver.c = cj4;
    }

    @DexIgnore
    public static void b(AppPackageInstallReceiver appPackageInstallReceiver, D26 d26) {
        appPackageInstallReceiver.e = d26;
    }

    @DexIgnore
    public static void c(AppPackageInstallReceiver appPackageInstallReceiver, V36 v36) {
        appPackageInstallReceiver.d = v36;
    }

    @DexIgnore
    public static void d(AppPackageInstallReceiver appPackageInstallReceiver, NotificationSettingsDatabase notificationSettingsDatabase) {
        appPackageInstallReceiver.f = notificationSettingsDatabase;
    }

    @DexIgnore
    public static void e(AppPackageInstallReceiver appPackageInstallReceiver, NotificationsRepository notificationsRepository) {
        appPackageInstallReceiver.a = notificationsRepository;
    }

    @DexIgnore
    public static void f(AppPackageInstallReceiver appPackageInstallReceiver, An4 an4) {
        appPackageInstallReceiver.b = an4;
    }
}
