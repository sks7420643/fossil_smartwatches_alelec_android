package com.fossil;

import android.os.RemoteException;
import com.fossil.P72;
import com.mapped.Wv2;
import com.mapped.Yv2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Db3 extends Y72<Fr2, Yv2> {
    @DexIgnore
    public /* final */ /* synthetic */ Wv2 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Db3(Wv2 wv2, P72.Ai ai) {
        super(ai);
        this.b = wv2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.M62$Bi, com.fossil.Ot3] */
    @Override // com.fossil.Y72
    public final /* synthetic */ void b(Fr2 fr2, Ot3 ot3) throws RemoteException {
        try {
            fr2.z0(a(), this.b.w(ot3));
        } catch (RuntimeException e) {
            ot3.d(e);
        }
    }
}
