package com.fossil;

import android.content.res.AssetManager;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Gc1 extends Ub1<InputStream> {
    @DexIgnore
    public Gc1(AssetManager assetManager, String str) {
        super(assetManager, str);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Ub1
    public /* bridge */ /* synthetic */ void b(InputStream inputStream) throws IOException {
        f(inputStream);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Ub1
    public /* bridge */ /* synthetic */ InputStream e(AssetManager assetManager, String str) throws IOException {
        return g(assetManager, str);
    }

    @DexIgnore
    public void f(InputStream inputStream) throws IOException {
        inputStream.close();
    }

    @DexIgnore
    public InputStream g(AssetManager assetManager, String str) throws IOException {
        return assetManager.open(str);
    }

    @DexIgnore
    @Override // com.fossil.Wb1
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }
}
