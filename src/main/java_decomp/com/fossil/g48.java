package com.fossil;

import com.mapped.Cd6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class G48 extends D58 {
    @DexIgnore
    public static /* final */ long h; // = TimeUnit.SECONDS.toMillis(60);
    @DexIgnore
    public static /* final */ long i; // = TimeUnit.MILLISECONDS.toNanos(h);
    @DexIgnore
    public static G48 j;
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public boolean e;
    @DexIgnore
    public G48 f;
    @DexIgnore
    public long g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final G48 c() throws InterruptedException {
            G48 g48 = G48.j;
            if (g48 != null) {
                G48 g482 = g48.f;
                if (g482 == null) {
                    long nanoTime = System.nanoTime();
                    G48.class.wait(G48.h);
                    G48 g483 = G48.j;
                    if (g483 == null) {
                        Wg6.i();
                        throw null;
                    } else if (g483.f != null || System.nanoTime() - nanoTime < G48.i) {
                        return null;
                    } else {
                        return G48.j;
                    }
                } else {
                    long u = g482.u(System.nanoTime());
                    if (u > 0) {
                        long j = u / 1000000;
                        G48.class.wait(j, (int) (u - (1000000 * j)));
                        return null;
                    }
                    G48 g484 = G48.j;
                    if (g484 != null) {
                        g484.f = g482.f;
                        g482.f = null;
                        return g482;
                    }
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }

        @DexIgnore
        public final boolean d(G48 g48) {
            synchronized (G48.class) {
                try {
                    for (G48 g482 = G48.j; g482 != null; g482 = g482.f) {
                        if (g482.f == g48) {
                            g482.f = g48.f;
                            g48.f = null;
                            return false;
                        }
                    }
                    return true;
                } catch (Throwable th) {
                    throw th;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public final void e(G48 g48, long j, boolean z) {
            synchronized (G48.class) {
                try {
                    if (G48.j == null) {
                        G48.j = new G48();
                        new Bi().start();
                    }
                    long nanoTime = System.nanoTime();
                    int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
                    if (i != 0 && z) {
                        g48.g = Math.min(j, g48.c() - nanoTime) + nanoTime;
                    } else if (i != 0) {
                        g48.g = j + nanoTime;
                    } else if (z) {
                        g48.g = g48.c();
                    } else {
                        throw new AssertionError();
                    }
                    long u = g48.u(nanoTime);
                    G48 g482 = G48.j;
                    if (g482 != null) {
                        while (g482.f != null) {
                            G48 g483 = g482.f;
                            if (g483 == null) {
                                Wg6.i();
                                throw null;
                            } else if (u < g483.u(nanoTime)) {
                                break;
                            } else {
                                g482 = g482.f;
                                if (g482 == null) {
                                    Wg6.i();
                                    throw null;
                                }
                            }
                        }
                        g48.f = g482.f;
                        g482.f = g48;
                        if (g482 == G48.j) {
                            G48.class.notify();
                        }
                        Cd6 cd6 = Cd6.a;
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Thread {
        @DexIgnore
        public Bi() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
            if (r0 == null) goto L_0x0000;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x001e, code lost:
            r0.x();
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r2 = this;
            L_0x0000:
                java.lang.Class<com.fossil.G48> r0 = com.fossil.G48.class
                monitor-enter(r0)     // Catch:{ InterruptedException -> 0x0022 }
                com.fossil.G48$Ai r0 = com.fossil.G48.k     // Catch:{ all -> 0x0024 }
                com.fossil.G48 r0 = r0.c()     // Catch:{ all -> 0x0024 }
                com.fossil.G48 r1 = com.fossil.G48.i()     // Catch:{ all -> 0x0024 }
                if (r0 != r1) goto L_0x0017
                r0 = 0
                com.fossil.G48.o(r0)     // Catch:{ all -> 0x0024 }
                java.lang.Class<com.fossil.G48> r0 = com.fossil.G48.class
                monitor-exit(r0)
                return
            L_0x0017:
                com.mapped.Cd6 r1 = com.mapped.Cd6.a
                java.lang.Class<com.fossil.G48> r1 = com.fossil.G48.class
                monitor-exit(r1)
                if (r0 == 0) goto L_0x0000
                r0.x()
                goto L_0x0000
            L_0x0022:
                r0 = move-exception
                goto L_0x0000
            L_0x0024:
                r0 = move-exception
                java.lang.Class<com.fossil.G48> r1 = com.fossil.G48.class
                monitor-exit(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.G48.Bi.run():void");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements A58 {
        @DexIgnore
        public /* final */ /* synthetic */ G48 b;
        @DexIgnore
        public /* final */ /* synthetic */ A58 c;

        @DexIgnore
        public Ci(G48 g48, A58 a58) {
            this.b = g48;
            this.c = a58;
        }

        @DexIgnore
        @Override // com.fossil.A58
        public void K(I48 i48, long j) {
            long j2;
            Wg6.c(i48, "source");
            F48.b(i48.p0(), 0, j);
            for (long j3 = j; j3 > 0; j3 -= j2) {
                X48 x48 = i48.b;
                if (x48 != null) {
                    j2 = 0;
                    while (true) {
                        if (j2 >= ((long) 65536)) {
                            break;
                        }
                        j2 += (long) (x48.c - x48.b);
                        if (j2 >= j3) {
                            j2 = j3;
                            break;
                        }
                        x48 = x48.f;
                        if (x48 == null) {
                            Wg6.i();
                            throw null;
                        }
                    }
                    G48 g48 = this.b;
                    g48.r();
                    try {
                        this.c.K(i48, j2);
                        Cd6 cd6 = Cd6.a;
                        if (g48.s()) {
                            throw g48.m(null);
                        }
                    } catch (IOException e) {
                        if (!g48.s()) {
                            throw e;
                        }
                        throw g48.m(e);
                    } finally {
                        g48.s();
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }

        @DexIgnore
        public G48 a() {
            return this.b;
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.A58, java.lang.AutoCloseable
        public void close() {
            G48 g48 = this.b;
            g48.r();
            try {
                this.c.close();
                Cd6 cd6 = Cd6.a;
                if (g48.s()) {
                    throw g48.m(null);
                }
            } catch (IOException e) {
                if (!g48.s()) {
                    throw e;
                }
                throw g48.m(e);
            } finally {
                g48.s();
            }
        }

        @DexIgnore
        @Override // com.fossil.A58
        public /* bridge */ /* synthetic */ D58 e() {
            return a();
        }

        @DexIgnore
        @Override // com.fossil.A58, java.io.Flushable
        public void flush() {
            G48 g48 = this.b;
            g48.r();
            try {
                this.c.flush();
                Cd6 cd6 = Cd6.a;
                if (g48.s()) {
                    throw g48.m(null);
                }
            } catch (IOException e) {
                if (!g48.s()) {
                    throw e;
                }
                throw g48.m(e);
            } finally {
                g48.s();
            }
        }

        @DexIgnore
        public String toString() {
            return "AsyncTimeout.sink(" + this.c + ')';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements C58 {
        @DexIgnore
        public /* final */ /* synthetic */ G48 b;
        @DexIgnore
        public /* final */ /* synthetic */ C58 c;

        @DexIgnore
        public Di(G48 g48, C58 c58) {
            this.b = g48;
            this.c = c58;
        }

        @DexIgnore
        public G48 a() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.C58, java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            G48 g48 = this.b;
            g48.r();
            try {
                this.c.close();
                Cd6 cd6 = Cd6.a;
                if (g48.s()) {
                    throw g48.m(null);
                }
            } catch (IOException e) {
                if (!g48.s()) {
                    throw e;
                }
                throw g48.m(e);
            } finally {
                g48.s();
            }
        }

        @DexIgnore
        @Override // com.fossil.C58
        public long d0(I48 i48, long j) {
            Wg6.c(i48, "sink");
            G48 g48 = this.b;
            g48.r();
            try {
                long d0 = this.c.d0(i48, j);
                if (!g48.s()) {
                    return d0;
                }
                throw g48.m(null);
            } catch (IOException e) {
                if (!g48.s()) {
                    throw e;
                }
                throw g48.m(e);
            } finally {
                g48.s();
            }
        }

        @DexIgnore
        @Override // com.fossil.C58
        public /* bridge */ /* synthetic */ D58 e() {
            return a();
        }

        @DexIgnore
        public String toString() {
            return "AsyncTimeout.source(" + this.c + ')';
        }
    }

    @DexIgnore
    public final IOException m(IOException iOException) {
        return t(iOException);
    }

    @DexIgnore
    public final void r() {
        if (!this.e) {
            long h2 = h();
            boolean e2 = e();
            if (h2 != 0 || e2) {
                this.e = true;
                k.e(this, h2, e2);
                return;
            }
            return;
        }
        throw new IllegalStateException("Unbalanced enter/exit".toString());
    }

    @DexIgnore
    public final boolean s() {
        if (!this.e) {
            return false;
        }
        this.e = false;
        return k.d(this);
    }

    @DexIgnore
    public IOException t(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    @DexIgnore
    public final long u(long j2) {
        return this.g - j2;
    }

    @DexIgnore
    public final A58 v(A58 a58) {
        Wg6.c(a58, "sink");
        return new Ci(this, a58);
    }

    @DexIgnore
    public final C58 w(C58 c58) {
        Wg6.c(c58, "source");
        return new Di(this, c58);
    }

    @DexIgnore
    public void x() {
    }
}
