package com.fossil;

import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import com.fossil.Zs2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vt2 extends Zs2.Ai {
    @DexIgnore
    public /* final */ /* synthetic */ Sn3 f;
    @DexIgnore
    public /* final */ /* synthetic */ Zs2 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Vt2(Zs2 zs2, Sn3 sn3) {
        super(zs2);
        this.g = zs2;
        this.f = sn3;
    }

    @DexIgnore
    @Override // com.fossil.Zs2.Ai
    public final void a() throws RemoteException {
        for (int i = 0; i < this.g.e.size(); i++) {
            if (this.f.equals(((Pair) this.g.e.get(i)).first)) {
                Log.w(this.g.a, "OnEventListener already registered.");
                return;
            }
        }
        Zs2.Ci ci = new Zs2.Ci(this.f);
        this.g.e.add(new Pair(this.f, ci));
        this.g.h.registerOnMeasurementEventListener(ci);
    }
}
