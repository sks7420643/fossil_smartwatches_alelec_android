package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Kg0 extends Yf0 implements Menu {
    @DexIgnore
    public /* final */ Gm0 d;

    @DexIgnore
    public Kg0(Context context, Gm0 gm0) {
        super(context);
        if (gm0 != null) {
            this.d = gm0;
            return;
        }
        throw new IllegalArgumentException("Wrapped Object can not be null.");
    }

    @DexIgnore
    @Override // android.view.Menu
    public MenuItem add(int i) {
        return c(this.d.add(i));
    }

    @DexIgnore
    @Override // android.view.Menu
    public MenuItem add(int i, int i2, int i3, int i4) {
        return c(this.d.add(i, i2, i3, i4));
    }

    @DexIgnore
    @Override // android.view.Menu
    public MenuItem add(int i, int i2, int i3, CharSequence charSequence) {
        return c(this.d.add(i, i2, i3, charSequence));
    }

    @DexIgnore
    @Override // android.view.Menu
    public MenuItem add(CharSequence charSequence) {
        return c(this.d.add(charSequence));
    }

    @DexIgnore
    public int addIntentOptions(int i, int i2, int i3, ComponentName componentName, Intent[] intentArr, Intent intent, int i4, MenuItem[] menuItemArr) {
        MenuItem[] menuItemArr2 = menuItemArr != null ? new MenuItem[menuItemArr.length] : null;
        int addIntentOptions = this.d.addIntentOptions(i, i2, i3, componentName, intentArr, intent, i4, menuItemArr2);
        if (menuItemArr2 != null) {
            int length = menuItemArr2.length;
            for (int i5 = 0; i5 < length; i5++) {
                menuItemArr[i5] = c(menuItemArr2[i5]);
            }
        }
        return addIntentOptions;
    }

    @DexIgnore
    @Override // android.view.Menu
    public SubMenu addSubMenu(int i) {
        return d(this.d.addSubMenu(i));
    }

    @DexIgnore
    @Override // android.view.Menu
    public SubMenu addSubMenu(int i, int i2, int i3, int i4) {
        return d(this.d.addSubMenu(i, i2, i3, i4));
    }

    @DexIgnore
    @Override // android.view.Menu
    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        return d(this.d.addSubMenu(i, i2, i3, charSequence));
    }

    @DexIgnore
    @Override // android.view.Menu
    public SubMenu addSubMenu(CharSequence charSequence) {
        return d(this.d.addSubMenu(charSequence));
    }

    @DexIgnore
    public void clear() {
        e();
        this.d.clear();
    }

    @DexIgnore
    public void close() {
        this.d.close();
    }

    @DexIgnore
    public MenuItem findItem(int i) {
        return c(this.d.findItem(i));
    }

    @DexIgnore
    public MenuItem getItem(int i) {
        return c(this.d.getItem(i));
    }

    @DexIgnore
    public boolean hasVisibleItems() {
        return this.d.hasVisibleItems();
    }

    @DexIgnore
    public boolean isShortcutKey(int i, KeyEvent keyEvent) {
        return this.d.isShortcutKey(i, keyEvent);
    }

    @DexIgnore
    public boolean performIdentifierAction(int i, int i2) {
        return this.d.performIdentifierAction(i, i2);
    }

    @DexIgnore
    public boolean performShortcut(int i, KeyEvent keyEvent, int i2) {
        return this.d.performShortcut(i, keyEvent, i2);
    }

    @DexIgnore
    public void removeGroup(int i) {
        f(i);
        this.d.removeGroup(i);
    }

    @DexIgnore
    public void removeItem(int i) {
        g(i);
        this.d.removeItem(i);
    }

    @DexIgnore
    public void setGroupCheckable(int i, boolean z, boolean z2) {
        this.d.setGroupCheckable(i, z, z2);
    }

    @DexIgnore
    public void setGroupEnabled(int i, boolean z) {
        this.d.setGroupEnabled(i, z);
    }

    @DexIgnore
    public void setGroupVisible(int i, boolean z) {
        this.d.setGroupVisible(i, z);
    }

    @DexIgnore
    public void setQwertyMode(boolean z) {
        this.d.setQwertyMode(z);
    }

    @DexIgnore
    public int size() {
        return this.d.size();
    }
}
