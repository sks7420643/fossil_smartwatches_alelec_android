package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yn3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ long b;
    @DexIgnore
    public /* final */ /* synthetic */ Un3 c;

    @DexIgnore
    public Yn3(Un3 un3, long j) {
        this.c = un3;
        this.b = j;
    }

    @DexIgnore
    public final void run() {
        this.c.l().q.b(this.b);
        this.c.d().M().b("Session timeout duration set", Long.valueOf(this.b));
    }
}
