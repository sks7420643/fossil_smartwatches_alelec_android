package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ry1 extends Ox1 implements Parcelable, Comparable<Ry1>, Nx1 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ int major;
    @DexIgnore
    public /* final */ int minor;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Ry1> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public Ry1 a(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new Ry1(parcel, (Qg6) null);
        }

        @DexIgnore
        public final Ry1 b(byte[] bArr) {
            Wg6.c(bArr, "data");
            if (bArr.length < 2) {
                return null;
            }
            return new Ry1(bArr[0], bArr[1]);
        }

        @DexIgnore
        public final Ry1 c(String str) {
            Wg6.c(str, "versionString");
            List Y = Wt7.Y(str, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0, 6, null);
            if (Y.size() >= 2) {
                Integer c = Ut7.c((String) Y.get(0));
                Integer c2 = Ut7.c((String) Y.get(1));
                if (!(c == null || c2 == null)) {
                    return new Ry1(c.intValue(), c2.intValue());
                }
            }
            return new Ry1(0, 0);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Ry1 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        public Ry1[] d(int i) {
            return new Ry1[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Ry1[] newArray(int i) {
            return d(i);
        }
    }

    @DexIgnore
    public Ry1(byte b, byte b2) {
        this.major = Hy1.p(b);
        this.minor = Hy1.p(b2);
    }

    @DexIgnore
    public Ry1(int i, int i2) {
        this.major = i;
        this.minor = i2;
    }

    @DexIgnore
    public Ry1(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt());
    }

    @DexIgnore
    public /* synthetic */ Ry1(Parcel parcel, Qg6 qg6) {
        this(parcel);
    }

    @DexIgnore
    public Ry1(short s, short s2) {
        this.major = Hy1.n(s);
        this.minor = Hy1.n(s2);
    }

    @DexIgnore
    @Override // java.lang.Object
    public Ry1 clone() {
        return new Ry1(this.major, this.minor);
    }

    @DexIgnore
    public int compareTo(Ry1 ry1) {
        Wg6.c(ry1, FacebookRequestErrorClassification.KEY_OTHER);
        int i = this.major;
        int i2 = ry1.major;
        if (i <= i2) {
            if (i < i2) {
                return -1;
            }
            int i3 = this.minor;
            int i4 = ry1.minor;
            if (i3 <= i4) {
                return i3 >= i4 ? 0 : -1;
            }
        }
        return 1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(Ry1 ry1) {
        return compareTo(ry1);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Ry1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Ry1 ry1 = (Ry1) obj;
            if (this.major != ry1.major) {
                return false;
            }
            return this.minor == ry1.minor;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.common.version.Version");
    }

    @DexIgnore
    public final int getMajor() {
        return this.major;
    }

    @DexIgnore
    public final int getMinor() {
        return this.minor;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0005: IGET  (r1v0 int) = (r2v0 'this' com.fossil.Ry1 A[IMMUTABLE_TYPE, THIS]) com.fossil.Ry1.major int), ('.' char), (wrap: int : 0x000f: IGET  (r1v2 int) = (r2v0 'this' com.fossil.Ry1 A[IMMUTABLE_TYPE, THIS]) com.fossil.Ry1.minor int)] */
    public final String getShortDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.major);
        sb.append('.');
        sb.append(this.minor);
        return sb.toString();
    }

    @DexIgnore
    public int hashCode() {
        return (this.major * 31) + this.minor;
    }

    @DexIgnore
    public final boolean isCompatible(Ry1 ry1, boolean z) {
        Wg6.c(ry1, FacebookRequestErrorClassification.KEY_OTHER);
        return this.major == ry1.major && (!z || this.minor >= ry1.minor);
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject put = new JSONObject().put("major", this.major).put("minor", this.minor);
        Wg6.b(put, "JSONObject()\n           \u2026     .put(\"minor\", minor)");
        return put;
    }

    @DexIgnore
    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: int : 0x0005: IGET  (r1v0 int) = (r2v0 'this' com.fossil.Ry1 A[IMMUTABLE_TYPE, THIS]) com.fossil.Ry1.major int), ('.' char), (wrap: int : 0x000f: IGET  (r1v2 int) = (r2v0 'this' com.fossil.Ry1 A[IMMUTABLE_TYPE, THIS]) com.fossil.Ry1.minor int)] */
    @Override // com.fossil.Ox1
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.major);
        sb.append('.');
        sb.append(this.minor);
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.major);
        }
        if (parcel != null) {
            parcel.writeInt(this.minor);
        }
    }
}
