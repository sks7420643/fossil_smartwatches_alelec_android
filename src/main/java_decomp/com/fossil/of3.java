package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.StreetViewPanoramaView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Of3 extends Bd3 {
    @DexIgnore
    public /* final */ /* synthetic */ Tb3 b;

    @DexIgnore
    public Of3(StreetViewPanoramaView.a aVar, Tb3 tb3) {
        this.b = tb3;
    }

    @DexIgnore
    @Override // com.fossil.Ad3
    public final void b1(Dc3 dc3) throws RemoteException {
        this.b.a(new Wb3(dc3));
    }
}
