package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A83 implements Xw2<Z73> {
    @DexIgnore
    public static A83 c; // = new A83();
    @DexIgnore
    public /* final */ Xw2<Z73> b;

    @DexIgnore
    public A83() {
        this(Ww2.b(new C83()));
    }

    @DexIgnore
    public A83(Xw2<Z73> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static long a() {
        return ((Z73) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ Z73 zza() {
        return this.b.zza();
    }
}
