package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D53 implements E53 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a;
    @DexIgnore
    public static /* final */ Xv2<Long> b;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        a = hw2.d("measurement.sdk.attribution.cache", true);
        b = hw2.b("measurement.sdk.attribution.cache.ttl", 604800000);
    }
    */

    @DexIgnore
    @Override // com.fossil.E53
    public final boolean zza() {
        return a.o().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.E53
    public final long zzb() {
        return b.o().longValue();
    }
}
