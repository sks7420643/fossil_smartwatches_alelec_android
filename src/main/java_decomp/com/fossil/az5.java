package com.fossil;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.nk5;
import com.fossil.t47;
import com.google.android.material.appbar.AppBarLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.FlexibleFitnessTab;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RingProgressBar;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class az5 extends qv5 implements ld6, aw5, t47.g {
    @DexIgnore
    public static /* final */ a Y; // = new a(null);
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public we6 G;
    @DexIgnore
    public sd6 H;
    @DexIgnore
    public bg6 I;
    @DexIgnore
    public ji6 J;
    @DexIgnore
    public nj6 K;
    @DexIgnore
    public fh6 L;
    @DexIgnore
    public g37<v75> M;
    @DexIgnore
    public kd6 N;
    @DexIgnore
    public /* final */ ArrayList<Fragment> O; // = new ArrayList<>();
    @DexIgnore
    public int P;
    @DexIgnore
    public l67 Q;
    @DexIgnore
    public ObjectAnimator R;
    @DexIgnore
    public int S; // = -1;
    @DexIgnore
    public wa1 T;
    @DexIgnore
    public /* final */ int U;
    @DexIgnore
    public /* final */ int V;
    @DexIgnore
    public /* final */ int W;
    @DexIgnore
    public HashMap X;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public int l;
    @DexIgnore
    public int m;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final az5 a() {
            return new az5();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Animation.AnimationListener {
        @DexIgnore
        public /* final */ /* synthetic */ az5 b;
        @DexIgnore
        public /* final */ /* synthetic */ ProgressBar c;

        @DexIgnore
        public b(az5 az5, ProgressBar progressBar) {
            this.b = az5;
            this.c = progressBar;
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            this.c.setProgress(0);
            this.c.setVisibility(4);
            this.b.S = -1;
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ls0<String> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ v75 f367a;
        @DexIgnore
        public /* final */ /* synthetic */ Animation b;
        @DexIgnore
        public /* final */ /* synthetic */ az5 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements CloudImageHelper.OnImageCallbackListener {

            @DexIgnore
            /* renamed from: a  reason: collision with root package name */
            public /* final */ /* synthetic */ c f368a;

            @DexIgnore
            public a(c cVar) {
                this.f368a = cVar;
            }

            @DexIgnore
            @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
            public void onImageCallback(String str, String str2) {
                pq7.c(str, "serial");
                pq7.c(str2, "filePath");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDashboardFragment", "onImageCallback, filePath=" + str2);
                if (!TextUtils.isEmpty(str2)) {
                    FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "onImageCallback, new logo");
                    az5.N6(this.f368a.c).t(str2).F0(this.f368a.f367a.O);
                    c cVar = this.f368a;
                    cVar.f367a.O.startAnimation(cVar.b);
                }
            }
        }

        @DexIgnore
        public c(v75 v75, Animation animation, az5 az5) {
            this.f367a = v75;
            this.b = animation;
            this.c = az5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardFragment", "activeDeviceSerialLiveData onChange " + str);
            if (!TextUtils.isEmpty(str)) {
                CloudImageHelper.ItemImage with = CloudImageHelper.Companion.getInstance().with();
                pq7.b(str, "serial");
                with.setSerialNumber(str).setSerialPrefix(nk5.o.m(str)).setType(Constants.DeviceType.TYPE_BRAND_LOGO).setImageCallback(new a(this)).download();
                return;
            }
            this.f367a.O.setImageResource(2131231246);
            this.f367a.O.startAnimation(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ az5 b;

        @DexIgnore
        public d(az5 az5) {
            this.b = az5;
        }

        @DexIgnore
        public final void onClick(View view) {
            az5.M6(this.b).s(4);
            az5.M6(this.b).q(4);
            this.b.b7(4);
            this.b.S6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CustomSwipeRefreshLayout.c {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ v75 f369a;
        @DexIgnore
        public /* final */ /* synthetic */ az5 b;

        @DexIgnore
        public e(v75 v75, az5 az5) {
            this.f369a = v75;
            this.b = az5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.c
        public void a() {
            FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "onRefresh");
            az5.M6(this.b).r();
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.c
        public void b() {
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "onStartSwipe");
            FlexibleProgressBar flexibleProgressBar = this.f369a.Y;
            pq7.b(flexibleProgressBar, "binding.syncProgress");
            flexibleProgressBar.setVisibility(4);
            View view = this.f369a.d0;
            pq7.b(view, "binding.vBorderBottom");
            view.setVisibility(0);
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout.c
        public void c(boolean z) {
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "onEndSwipe");
            this.b.U6(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ az5 b;

        @DexIgnore
        public f(az5 az5) {
            this.b = az5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.B;
                pq7.b(activity, "it");
                PairingInstructionsActivity.a.b(aVar, activity, false, false, 4, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnLongClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ az5 b;

        @DexIgnore
        public g(az5 az5) {
            this.b = az5;
        }

        @DexIgnore
        public final boolean onLongClick(View view) {
            DebugActivity.a aVar = DebugActivity.O;
            FragmentActivity requireActivity = this.b.requireActivity();
            pq7.b(requireActivity, "requireActivity()");
            aVar.a(requireActivity);
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ az5 b;

        @DexIgnore
        public h(az5 az5) {
            this.b = az5;
        }

        @DexIgnore
        public final void onClick(View view) {
            az5.M6(this.b).s(0);
            az5.M6(this.b).q(0);
            this.b.b7(0);
            this.b.S6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ az5 b;

        @DexIgnore
        public i(az5 az5) {
            this.b = az5;
        }

        @DexIgnore
        public final void onClick(View view) {
            az5.M6(this.b).s(1);
            az5.M6(this.b).q(1);
            this.b.b7(1);
            this.b.S6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ az5 b;

        @DexIgnore
        public j(az5 az5) {
            this.b = az5;
        }

        @DexIgnore
        public final void onClick(View view) {
            az5.M6(this.b).s(2);
            az5.M6(this.b).q(2);
            this.b.b7(2);
            this.b.S6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ az5 b;

        @DexIgnore
        public k(az5 az5) {
            this.b = az5;
        }

        @DexIgnore
        public final void onClick(View view) {
            az5.M6(this.b).s(3);
            az5.M6(this.b).q(3);
            this.b.b7(3);
            this.b.S6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ az5 b;

        @DexIgnore
        public l(az5 az5) {
            this.b = az5;
        }

        @DexIgnore
        public final void onClick(View view) {
            az5.M6(this.b).s(5);
            az5.M6(this.b).q(5);
            this.b.b7(5);
            this.b.S6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements AppBarLayout.d {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ v75 f370a;

        @DexIgnore
        public m(v75 v75) {
            this.f370a = v75;
        }

        @DexIgnore
        @Override // com.google.android.material.appbar.AppBarLayout.c
        public void a(AppBarLayout appBarLayout, int i) {
            CustomSwipeRefreshLayout customSwipeRefreshLayout = this.f370a.X;
            pq7.b(customSwipeRefreshLayout, "binding.srlPullToSync");
            customSwipeRefreshLayout.setEnabled(Math.abs(i) == 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements ValueAnimator.AnimatorUpdateListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ FlexibleProgressBar f371a;
        @DexIgnore
        public /* final */ /* synthetic */ br7 b;

        @DexIgnore
        public n(br7 br7, FlexibleProgressBar flexibleProgressBar, br7 br72, az5 az5, int i) {
            this.f371a = flexibleProgressBar;
            this.b = br72;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            FlexibleProgressBar flexibleProgressBar = this.f371a;
            pq7.b(flexibleProgressBar, "it");
            int i = this.b.element;
            pq7.b(valueAnimator, "animation");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                flexibleProgressBar.setProgress(((Integer) animatedValue).intValue() + i);
                return;
            }
            throw new il7("null cannot be cast to non-null type kotlin.Int");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements Animator.AnimatorListener {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ /* synthetic */ FlexibleProgressBar f372a;
        @DexIgnore
        public /* final */ /* synthetic */ az5 b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public o(br7 br7, FlexibleProgressBar flexibleProgressBar, br7 br72, az5 az5, int i) {
            this.f372a = flexibleProgressBar;
            this.b = az5;
            this.c = i;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            pq7.c(animator, "animation");
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            pq7.c(animator, "animation");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardFragment", "onAnimationEnd " + this.c);
            az5 az5 = this.b;
            FlexibleProgressBar flexibleProgressBar = this.f372a;
            pq7.b(flexibleProgressBar, "it");
            az5.R6(flexibleProgressBar, this.c);
        }

        @DexIgnore
        public void onAnimationRepeat(Animator animator) {
            pq7.c(animator, "animation");
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            pq7.c(animator, "animation");
        }
    }

    @DexIgnore
    public az5() {
        String d2 = qn5.l.a().d("nonBrandSurface");
        this.U = Color.parseColor(d2 == null ? "#FFFFFF" : d2);
        String d3 = qn5.l.a().d("hybridInactiveTab");
        this.V = Color.parseColor(d3 == null ? "#FFFFFF" : d3);
        String d4 = qn5.l.a().d(Explore.COLUMN_BACKGROUND);
        this.W = Color.parseColor(d4 == null ? "#FFFFFF" : d4);
    }

    @DexIgnore
    public static final /* synthetic */ kd6 M6(az5 az5) {
        kd6 kd6 = az5.N;
        if (kd6 != null) {
            return kd6;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ wa1 N6(az5 az5) {
        wa1 wa1 = az5.T;
        if (wa1 != null) {
            return wa1;
        }
        pq7.n("mRequestManager");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void A(boolean z2) {
        if (isActive()) {
            g37<v75> g37 = this.M;
            if (g37 != null) {
                v75 a2 = g37.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.A;
                    pq7.b(constraintLayout, "it.clUpdateFw");
                    constraintLayout.setVisibility(8);
                    a2.X.setDisableSwipe(false);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public String D6() {
        return "HomeDashboardFragment";
    }

    @DexIgnore
    @Override // com.fossil.pv5
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void G0() {
        AppBarLayout appBarLayout;
        if (isActive()) {
            g37<v75> g37 = this.M;
            if (g37 != null) {
                v75 a2 = g37.a();
                if (a2 != null && (appBarLayout = a2.q) != null) {
                    appBarLayout.r(true, true);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void J1(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary, Integer num, Integer num2, boolean z2) {
        double d2;
        double d3;
        int i2;
        int i3;
        int i4;
        String sb;
        String format;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("setDataSummaryForTabs - latestGoalTrackingTarget=");
        sb2.append(num);
        sb2.append(", ");
        sb2.append("heartRateResting=");
        sb2.append(num2);
        sb2.append(", isNewSession=");
        sb2.append(z2);
        sb2.append(", mBinding.get()=");
        g37<v75> g37 = this.M;
        if (g37 != null) {
            sb2.append(g37.a());
            sb2.append(", hashCode=");
            sb2.append(hashCode());
            local.d("HomeDashboardFragment", sb2.toString());
            g37<v75> g372 = this.M;
            if (g372 != null) {
                v75 a2 = g372.a();
                if (a2 != null) {
                    PortfolioApp c2 = PortfolioApp.h0.c();
                    int sleepMinutes = mFSleepDay != null ? mFSleepDay.getSleepMinutes() : 0;
                    int intValue = num2 != null ? num2.intValue() : 0;
                    if (activitySummary != null) {
                        d2 = activitySummary.getSteps();
                        int activeTime = activitySummary.getActiveTime();
                        d3 = activitySummary.getCalories();
                        i2 = activeTime;
                    } else {
                        d2 = 0.0d;
                        d3 = 0.0d;
                        i2 = 0;
                    }
                    if (goalTrackingSummary != null) {
                        i3 = goalTrackingSummary.getGoalTarget();
                        i4 = goalTrackingSummary.getTotalTracked();
                    } else {
                        i3 = 0;
                        i4 = 0;
                    }
                    if (i3 == 0 && num != null) {
                        i3 = num.intValue();
                    }
                    String c3 = um5.c(c2, 2131887327);
                    FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "setDataSummaryForTabs - steps=" + d2 + ", activeTime=" + i2 + ", calories=" + d3 + ", goalTarget=" + i3 + ", goalTotalTracked=" + i4 + ", sleepMinutes=" + sleepMinutes + ", resting=" + intValue);
                    FlexibleFitnessTab flexibleFitnessTab = a2.F;
                    int i5 = (d2 > 0.0d ? 1 : (d2 == 0.0d ? 0 : -1));
                    String d4 = (i5 != 0 || !z2) ? ml5.f2399a.d(Integer.valueOf(lr7.a(d2))) : c3;
                    pq7.b(d4, "if (steps == 0.0 && isNe\u2026ormat(steps.roundToInt())");
                    flexibleFitnessTab.K(d4);
                    FlexibleFitnessTab flexibleFitnessTab2 = a2.E;
                    String a3 = (i2 != 0 || !z2) ? ml5.f2399a.a(Integer.valueOf(i2)) : c3;
                    pq7.b(a3, "if (activeTime == 0 && i\u2026iveTimeFormat(activeTime)");
                    flexibleFitnessTab2.K(a3);
                    FlexibleFitnessTab flexibleFitnessTab3 = a2.G;
                    String b2 = (d3 != 0.0d || !z2) ? ml5.f2399a.b(Float.valueOf((float) d3)) : c3;
                    pq7.b(b2, "if (calories == 0.0 && i\u2026ormat(calories.toFloat())");
                    flexibleFitnessTab3.K(b2);
                    FlexibleFitnessTab flexibleFitnessTab4 = a2.J;
                    if (sleepMinutes != 0 || !z2) {
                        StringBuilder sb3 = new StringBuilder();
                        hr7 hr7 = hr7.f1520a;
                        Locale locale = Locale.US;
                        pq7.b(locale, "Locale.US");
                        String format2 = String.format(locale, "%d", Arrays.copyOf(new Object[]{Integer.valueOf(sleepMinutes / 60)}, 1));
                        pq7.b(format2, "java.lang.String.format(locale, format, *args)");
                        sb3.append(format2);
                        sb3.append(":");
                        hr7 hr72 = hr7.f1520a;
                        Locale locale2 = Locale.US;
                        pq7.b(locale2, "Locale.US");
                        String format3 = String.format(locale2, "%02d", Arrays.copyOf(new Object[]{Integer.valueOf(sleepMinutes % 60)}, 1));
                        pq7.b(format3, "java.lang.String.format(locale, format, *args)");
                        sb3.append(format3);
                        sb = sb3.toString();
                    } else {
                        sb = um5.c(c2, 2131887329);
                    }
                    pq7.b(sb, "if (sleepMinutes == 0 &&\u2026              .toString()");
                    flexibleFitnessTab4.K(sb);
                    FlexibleFitnessTab flexibleFitnessTab5 = a2.H;
                    if (i3 != 0 || !z2) {
                        hr7 hr73 = hr7.f1520a;
                        String c4 = um5.c(c2, 2131886756);
                        pq7.b(c4, "LanguageHelper.getString\u2026ingToday_Label__OfNumber)");
                        format = String.format(c4, Arrays.copyOf(new Object[]{Integer.valueOf(i3)}, 1));
                        pq7.b(format, "java.lang.String.format(format, *args)");
                    } else {
                        format = c3;
                    }
                    pq7.b(format, "if (goalTarget == 0 && i\u2026l__OfNumber), goalTarget)");
                    flexibleFitnessTab5.J(format);
                    FlexibleFitnessTab flexibleFitnessTab6 = a2.H;
                    String valueOf = (i4 != 0 || !z2) ? String.valueOf(i4) : c3;
                    pq7.b(valueOf, "if (goalTotalTracked == \u2026alTotalTracked.toString()");
                    flexibleFitnessTab6.K(valueOf);
                    FlexibleFitnessTab flexibleFitnessTab7 = a2.I;
                    if (i5 != 0 || !z2) {
                        c3 = String.valueOf(intValue);
                    }
                    pq7.b(c3, "if (steps == 0.0 && isNe\u2026e else resting.toString()");
                    flexibleFitnessTab7.K(c3);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void Q5() {
        if (isActive()) {
            g37<v75> g37 = this.M;
            if (g37 != null) {
                v75 a2 = g37.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.A;
                    pq7.b(constraintLayout, "it.clUpdateFw");
                    constraintLayout.setVisibility(0);
                    FlexibleProgressBar flexibleProgressBar = a2.Q;
                    pq7.b(flexibleProgressBar, "it.pbProgress");
                    flexibleProgressBar.setMax(100);
                    a2.X.setDisableSwipe(true);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void R1(String str, String str2) {
        l67 l67 = this.Q;
        if (l67 != null) {
            l67.c(str, str2);
        }
    }

    @DexIgnore
    @Override // com.fossil.t47.g, com.fossil.qv5
    public void R5(String str, int i2, Intent intent) {
        pq7.c(str, "tag");
        if ((str.length() > 0) && pq7.a(str, "ASK_TO_CANCEL_WORKOUT") && getActivity() != null) {
            if (i2 == 2131363291) {
                FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "don't quit");
                kd6 kd6 = this.N;
                if (kd6 != null) {
                    kd6.p(PortfolioApp.h0.c().J(), false);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            } else if (i2 == 2131363373) {
                FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "quit and sync");
                kd6 kd62 = this.N;
                if (kd62 != null) {
                    kd62.p(PortfolioApp.h0.c().J(), true);
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public final void R6(ProgressBar progressBar, int i2) {
        CustomSwipeRefreshLayout customSwipeRefreshLayout;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "animateSyncProgressEnd " + progressBar);
        if (i2 == 0) {
            progressBar.setVisibility(0);
        } else if (i2 == 1) {
            X6(2);
        } else if (i2 == 2) {
            TranslateAnimation translateAnimation = new TranslateAnimation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, progressBar.getY(), progressBar.getY() - ((float) progressBar.getHeight()));
            translateAnimation.setDuration((long) SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS);
            translateAnimation.setAnimationListener(new b(this, progressBar));
            progressBar.startAnimation(translateAnimation);
            g37<v75> g37 = this.M;
            if (g37 != null) {
                v75 a2 = g37.a();
                if (a2 != null && (customSwipeRefreshLayout = a2.X) != null) {
                    customSwipeRefreshLayout.k();
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void S6() {
        kd6 kd6 = this.N;
        if (kd6 == null) {
            return;
        }
        if (kd6 != null) {
            int o2 = kd6.o();
            if (!this.O.isEmpty()) {
                int size = this.O.size();
                int i2 = 0;
                while (i2 < size) {
                    if (this.O.get(i2) instanceof aw5) {
                        Fragment fragment = this.O.get(i2);
                        if (fragment != null) {
                            ((aw5) fragment).b2(i2 == o2);
                        } else {
                            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.VisibleChangeListener");
                        }
                    }
                    i2++;
                }
            }
            if (o2 == 0) {
                E6("steps_view");
            } else if (o2 == 1) {
                E6("active_minutes_view");
            } else if (o2 == 2) {
                E6("calories_view");
            } else if (o2 == 3) {
                E6("heart_rate_view");
            } else if (o2 == 5) {
                E6("sleep_view");
            }
        } else {
            pq7.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void T6() {
        FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "cancelSyncProgress");
        ObjectAnimator objectAnimator = this.R;
        if (objectAnimator != null) {
            objectAnimator.cancel();
        }
        g37<v75> g37 = this.M;
        if (g37 != null) {
            v75 a2 = g37.a();
            if (a2 != null) {
                FlexibleProgressBar flexibleProgressBar = a2.Y;
                pq7.b(flexibleProgressBar, "it.syncProgress");
                flexibleProgressBar.setVisibility(4);
                FlexibleProgressBar flexibleProgressBar2 = a2.Y;
                pq7.b(flexibleProgressBar2, "it.syncProgress");
                flexibleProgressBar2.setProgress(0);
                a2.X.k();
            }
            kd6 kd6 = this.N;
            if (kd6 != null) {
                kd6.t(false);
            } else {
                pq7.n("mPresenter");
                throw null;
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void U0() {
        s37 s37 = s37.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        pq7.b(childFragmentManager, "childFragmentManager");
        s37.i(childFragmentManager);
    }

    @DexIgnore
    public final void U6(boolean z2) {
        g37<v75> g37 = this.M;
        if (g37 != null) {
            v75 a2 = g37.a();
            if (a2 != null) {
                if (z2) {
                    FlexibleProgressBar flexibleProgressBar = a2.Y;
                    pq7.b(flexibleProgressBar, "syncProgress");
                    flexibleProgressBar.setVisibility(0);
                } else {
                    FlexibleProgressBar flexibleProgressBar2 = a2.Y;
                    pq7.b(flexibleProgressBar2, "syncProgress");
                    flexibleProgressBar2.setVisibility(4);
                }
                View view = a2.d0;
                pq7.b(view, "vBorderBottom");
                view.setVisibility(4);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void V6() {
        Fragment Z = getChildFragmentManager().Z(ve6.s.a());
        Fragment Z2 = getChildFragmentManager().Z("DashboardActiveTimeFragment");
        Fragment Z3 = getChildFragmentManager().Z("DashboardCaloriesFragment");
        Fragment Z4 = getChildFragmentManager().Z(ii6.s.a());
        Fragment Z5 = getChildFragmentManager().Z(mj6.s.a());
        Fragment Z6 = getChildFragmentManager().Z(eh6.s.a());
        if (Z == null) {
            Z = ve6.s.b();
        }
        if (Z2 == null) {
            Z2 = rd6.m.a();
        }
        if (Z3 == null) {
            Z3 = new ag6();
        }
        if (Z4 == null) {
            Z4 = ii6.s.b();
        }
        if (Z5 == null) {
            Z5 = mj6.s.b();
        }
        if (Z6 == null) {
            Z6 = eh6.s.b();
        }
        this.O.clear();
        this.O.add(Z);
        this.O.add(Z2);
        this.O.add(Z3);
        this.O.add(Z4);
        this.O.add(Z6);
        this.O.add(Z5);
        g37<v75> g37 = this.M;
        if (g37 != null) {
            v75 a2 = g37.a();
            if (a2 != null) {
                kd6 kd6 = this.N;
                if (kd6 != null) {
                    nk5.a aVar = nk5.o;
                    if (kd6 == null) {
                        pq7.n("mPresenter");
                        throw null;
                    } else if (aVar.w(kd6.n())) {
                        FlexibleFitnessTab flexibleFitnessTab = a2.E;
                        pq7.b(flexibleFitnessTab, "flexibleTabActiveTime");
                        flexibleFitnessTab.setVisibility(0);
                        FlexibleFitnessTab flexibleFitnessTab2 = a2.I;
                        pq7.b(flexibleFitnessTab2, "flexibleTabHeartRate");
                        flexibleFitnessTab2.setVisibility(0);
                        FlexibleFitnessTab flexibleFitnessTab3 = a2.H;
                        pq7.b(flexibleFitnessTab3, "flexibleTabGoalTracking");
                        flexibleFitnessTab3.setVisibility(8);
                    } else {
                        FlexibleFitnessTab flexibleFitnessTab4 = a2.E;
                        pq7.b(flexibleFitnessTab4, "flexibleTabActiveTime");
                        flexibleFitnessTab4.setVisibility(8);
                        FlexibleFitnessTab flexibleFitnessTab5 = a2.I;
                        pq7.b(flexibleFitnessTab5, "flexibleTabHeartRate");
                        flexibleFitnessTab5.setVisibility(8);
                        FlexibleFitnessTab flexibleFitnessTab6 = a2.H;
                        pq7.b(flexibleFitnessTab6, "flexibleTabGoalTracking");
                        flexibleFitnessTab6.setVisibility(0);
                    }
                }
                ViewPager2 viewPager2 = a2.W;
                pq7.b(viewPager2, "rvTabs");
                viewPager2.setAdapter(new g67(getChildFragmentManager(), this.O));
                if (a2.W.getChildAt(0) != null) {
                    View childAt = a2.W.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setItemViewCacheSize(4);
                    } else {
                        throw new il7("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                }
                ViewPager2 viewPager22 = a2.W;
                pq7.b(viewPager22, "rvTabs");
                viewPager22.setUserInputEnabled(false);
            }
            ro4 M2 = PortfolioApp.h0.c().M();
            if (Z != null) {
                ve6 ve6 = (ve6) Z;
                if (Z2 != null) {
                    rd6 rd6 = (rd6) Z2;
                    ag6 ag6 = (ag6) Z3;
                    if (Z4 != null) {
                        ii6 ii6 = (ii6) Z4;
                        if (Z5 != null) {
                            mj6 mj6 = (mj6) Z5;
                            if (Z6 != null) {
                                M2.t0(new dd6(ve6, rd6, ag6, ii6, mj6, (eh6) Z6)).a(this);
                                return;
                            }
                            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment");
                        }
                        throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.DashboardSleepFragment");
                    }
                    throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRateFragment");
                }
                throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment");
            }
            throw new il7("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activity.DashboardActivityFragment");
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void W6() {
        String d2 = qn5.l.a().d("onDianaStepsTab");
        if (d2 != null) {
            this.h = Color.parseColor(d2);
            tl7 tl7 = tl7.f3441a;
        } else {
            this.h = gl0.d(requireContext(), 2131099943);
            tl7 tl72 = tl7.f3441a;
        }
        String d3 = qn5.l.a().d("onDianaActiveMinutesTab");
        if (d3 != null) {
            this.i = Color.parseColor(d3);
            tl7 tl73 = tl7.f3441a;
        } else {
            this.i = gl0.d(requireContext(), 2131099943);
            tl7 tl74 = tl7.f3441a;
        }
        String d4 = qn5.l.a().d("onDianaActiveCaloriesTab");
        if (d4 != null) {
            this.j = Color.parseColor(d4);
            tl7 tl75 = tl7.f3441a;
        } else {
            this.j = gl0.d(requireContext(), 2131099943);
            tl7 tl76 = tl7.f3441a;
        }
        String d5 = qn5.l.a().d("onDianaHeartRateTab");
        if (d5 != null) {
            this.k = Color.parseColor(d5);
            tl7 tl77 = tl7.f3441a;
        } else {
            this.k = gl0.d(requireContext(), 2131099943);
            tl7 tl78 = tl7.f3441a;
        }
        String d6 = qn5.l.a().d("onDianaSleepTab");
        if (d6 != null) {
            this.l = Color.parseColor(d6);
            tl7 tl79 = tl7.f3441a;
        } else {
            this.l = gl0.d(requireContext(), 2131099943);
            tl7 tl710 = tl7.f3441a;
        }
        String d7 = qn5.l.a().d("onDianaInactiveTab");
        if (d7 != null) {
            this.m = Color.parseColor(d7);
            tl7 tl711 = tl7.f3441a;
        } else {
            this.m = gl0.d(requireContext(), 2131099947);
            tl7 tl712 = tl7.f3441a;
        }
        String d8 = qn5.l.a().d("onHybridStepsTab");
        if (d8 != null) {
            this.s = Color.parseColor(d8);
            tl7 tl713 = tl7.f3441a;
        } else {
            this.s = gl0.d(requireContext(), 2131099943);
            tl7 tl714 = tl7.f3441a;
        }
        String d9 = qn5.l.a().d("onHybridActiveCaloriesTab");
        if (d9 != null) {
            this.t = Color.parseColor(d9);
            tl7 tl715 = tl7.f3441a;
        } else {
            this.t = gl0.d(requireContext(), 2131099943);
            tl7 tl716 = tl7.f3441a;
        }
        String d10 = qn5.l.a().d("onHybridSleepTab");
        if (d10 != null) {
            this.u = Color.parseColor(d10);
            tl7 tl717 = tl7.f3441a;
        } else {
            this.u = gl0.d(requireContext(), 2131099943);
            tl7 tl718 = tl7.f3441a;
        }
        String d11 = qn5.l.a().d("onHybridInactiveTab");
        if (d11 != null) {
            this.v = Color.parseColor(d11);
            tl7 tl719 = tl7.f3441a;
        } else {
            this.v = gl0.d(requireContext(), 2131099948);
            tl7 tl720 = tl7.f3441a;
        }
        String d12 = qn5.l.a().d("onHybridGoalTrackingTab");
        if (d12 != null) {
            this.w = Color.parseColor(d12);
            tl7 tl721 = tl7.f3441a;
        } else {
            this.w = gl0.d(requireContext(), 2131099943);
            tl7 tl722 = tl7.f3441a;
        }
        String d13 = qn5.l.a().d("dianaStepsTab");
        if (d13 != null) {
            this.x = Color.parseColor(d13);
            tl7 tl723 = tl7.f3441a;
        } else {
            this.x = gl0.d(requireContext(), 2131099812);
            tl7 tl724 = tl7.f3441a;
        }
        String d14 = qn5.l.a().d("dianaActiveMinutesTab");
        if (d14 != null) {
            this.y = Color.parseColor(d14);
            tl7 tl725 = tl7.f3441a;
        } else {
            this.y = gl0.d(requireContext(), 2131099807);
            tl7 tl726 = tl7.f3441a;
        }
        String d15 = qn5.l.a().d("dianaActiveCaloriesTab");
        if (d15 != null) {
            this.z = Color.parseColor(d15);
            tl7 tl727 = tl7.f3441a;
        } else {
            this.z = gl0.d(requireContext(), 2131099806);
            tl7 tl728 = tl7.f3441a;
        }
        String d16 = qn5.l.a().d("dianaHeartRateTab");
        if (d16 != null) {
            this.A = Color.parseColor(d16);
            tl7 tl729 = tl7.f3441a;
        } else {
            this.A = gl0.d(requireContext(), 2131099808);
            tl7 tl730 = tl7.f3441a;
        }
        String d17 = qn5.l.a().d("dianaSleepTab");
        if (d17 != null) {
            this.B = Color.parseColor(d17);
            tl7 tl731 = tl7.f3441a;
        } else {
            this.B = gl0.d(requireContext(), 2131099810);
            tl7 tl732 = tl7.f3441a;
        }
        String d18 = qn5.l.a().d("dianaInactiveTab");
        if (d18 != null) {
            Color.parseColor(d18);
            tl7 tl733 = tl7.f3441a;
        } else {
            gl0.d(requireContext(), 2131099809);
            tl7 tl734 = tl7.f3441a;
        }
        String d19 = qn5.l.a().d("hybridStepsTab");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "ABCD=" + d19);
        String d20 = qn5.l.a().d("hybridStepsTab");
        if (d20 != null) {
            this.C = Color.parseColor(d20);
            tl7 tl735 = tl7.f3441a;
        } else {
            this.C = gl0.d(requireContext(), 2131099943);
            tl7 tl736 = tl7.f3441a;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("HomeDashboardFragment", "hybridStepsColor=" + this.C);
        String d21 = qn5.l.a().d("hybridActiveCaloriesTab");
        if (d21 != null) {
            this.D = Color.parseColor(d21);
            tl7 tl737 = tl7.f3441a;
        } else {
            this.D = gl0.d(requireContext(), 2131099943);
            tl7 tl738 = tl7.f3441a;
        }
        String d22 = qn5.l.a().d("hybridSleepTab");
        if (d22 != null) {
            this.E = Color.parseColor(d22);
            tl7 tl739 = tl7.f3441a;
        } else {
            this.E = gl0.d(requireContext(), 2131099943);
            tl7 tl740 = tl7.f3441a;
        }
        String d23 = qn5.l.a().d("hybridInactiveTab");
        if (d23 != null) {
            Color.parseColor(d23);
            tl7 tl741 = tl7.f3441a;
        } else {
            gl0.d(requireContext(), 2131099948);
            tl7 tl742 = tl7.f3441a;
        }
        String d24 = qn5.l.a().d("hybridGoalTrackingTab");
        if (d24 != null) {
            this.F = Color.parseColor(d24);
            tl7 tl743 = tl7.f3441a;
        } else {
            this.F = gl0.d(requireContext(), 2131099853);
            tl7 tl744 = tl7.f3441a;
        }
        g37<v75> g37 = this.M;
        if (g37 != null) {
            v75 a2 = g37.a();
            if (a2 != null) {
                String d25 = qn5.l.a().d("primaryText");
                if (d25 != null) {
                    a2.O.setColorFilter(Color.parseColor(d25), PorterDuff.Mode.SRC_ATOP);
                    tl7 tl745 = tl7.f3441a;
                }
                a2.w.setBackgroundColor(this.U);
                a2.f0.setBackgroundColor(this.W);
                a2.t.setBackgroundColor(this.W);
                a2.A.setBackgroundColor(this.W);
                a2.u.setBackgroundColor(this.W);
                a2.z.setBackgroundColor(this.W);
                tl7 tl746 = tl7.f3441a;
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @SuppressLint({"ObjectAnimatorBinding"})
    public final void X6(int i2) {
        FlexibleProgressBar flexibleProgressBar;
        int i3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("runProgress state ");
        sb.append(i2);
        sb.append(" on thread ");
        Thread currentThread = Thread.currentThread();
        pq7.b(currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        local.d("HomeDashboardFragment", sb.toString());
        g37<v75> g37 = this.M;
        if (g37 != null) {
            v75 a2 = g37.a();
            if (a2 != null && (flexibleProgressBar = a2.Y) != null) {
                br7 br7 = new br7();
                br7.element = 0;
                br7 br72 = new br7();
                br72.element = 0;
                this.S = i2;
                if (i2 == 0) {
                    br7.element = 0;
                    br72.element = 2000;
                    i3 = 2000;
                } else if (i2 == 1) {
                    br7.element = 2000;
                    i3 = 7000;
                    br72.element = SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS;
                } else if (i2 != 2) {
                    i3 = 0;
                } else {
                    br7.element = 9000;
                    br72.element = 1000;
                    i3 = 1000;
                }
                pq7.b(flexibleProgressBar, "it");
                flexibleProgressBar.setProgress(br7.element);
                ObjectAnimator ofInt = ObjectAnimator.ofInt(this, "", i3);
                this.R = ofInt;
                if (ofInt != null) {
                    ofInt.setDuration((long) br72.element);
                    ofInt.addUpdateListener(new n(br72, flexibleProgressBar, br7, this, i2));
                    ofInt.addListener(new o(br72, flexibleProgressBar, br7, this, i2));
                    ofInt.start();
                    return;
                }
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    /* renamed from: Y6 */
    public void M5(kd6 kd6) {
        pq7.c(kd6, "presenter");
        this.N = kd6;
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void Z1(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("HomeDashboardFragment", "syncCompleted - success: " + z2);
        if (!z2) {
            T6();
        } else {
            X6(1);
        }
    }

    @DexIgnore
    public final void Z6() {
        g37<v75> g37 = this.M;
        if (g37 != null) {
            v75 a2 = g37.a();
            if (a2 != null) {
                RingProgressBar ringProgressBar = a2.T;
                pq7.b(ringProgressBar, "it.rpbBiggest");
                a7(ringProgressBar, RingProgressBar.a.STEPS);
                nk5.a aVar = nk5.o;
                kd6 kd6 = this.N;
                if (kd6 == null) {
                    pq7.n("mPresenter");
                    throw null;
                } else if (aVar.w(kd6.n())) {
                    RingProgressBar ringProgressBar2 = a2.V;
                    pq7.b(ringProgressBar2, "it.rpbSmallest");
                    ringProgressBar2.setVisibility(0);
                    RingProgressBar ringProgressBar3 = a2.S;
                    pq7.b(ringProgressBar3, "it.rpbBig");
                    a7(ringProgressBar3, RingProgressBar.a.ACTIVE_TIME);
                    RingProgressBar ringProgressBar4 = a2.U;
                    pq7.b(ringProgressBar4, "it.rpbMedium");
                    a7(ringProgressBar4, RingProgressBar.a.CALORIES);
                    RingProgressBar ringProgressBar5 = a2.V;
                    pq7.b(ringProgressBar5, "it.rpbSmallest");
                    a7(ringProgressBar5, RingProgressBar.a.SLEEP);
                    FlexibleFitnessTab flexibleFitnessTab = a2.E;
                    String c2 = um5.c(PortfolioApp.h0.c(), 2131886676);
                    pq7.b(c2, "LanguageHelper.getString\u2026n_StepsToday_Label__Mins)");
                    flexibleFitnessTab.J(c2);
                    FlexibleFitnessTab flexibleFitnessTab2 = a2.F;
                    String c3 = um5.c(PortfolioApp.h0.c(), 2131886678);
                    pq7.b(c3, "LanguageHelper.getString\u2026_StepsToday_Label__Steps)");
                    flexibleFitnessTab2.J(c3);
                    FlexibleFitnessTab flexibleFitnessTab3 = a2.G;
                    String c4 = um5.c(PortfolioApp.h0.c(), 2131886672);
                    pq7.b(c4, "LanguageHelper.getString\u2026n_StepsToday_Label__Cals)");
                    flexibleFitnessTab3.J(c4);
                    FlexibleFitnessTab flexibleFitnessTab4 = a2.I;
                    String c5 = um5.c(PortfolioApp.h0.c(), 2131886677);
                    pq7.b(c5, "LanguageHelper.getString\u2026tepsToday_Label__Resting)");
                    flexibleFitnessTab4.J(c5);
                    FlexibleFitnessTab flexibleFitnessTab5 = a2.J;
                    String c6 = um5.c(PortfolioApp.h0.c(), 2131886673);
                    pq7.b(c6, "LanguageHelper.getString\u2026tepsToday_Label__HrsMins)");
                    flexibleFitnessTab5.J(c6);
                } else {
                    RingProgressBar ringProgressBar6 = a2.V;
                    pq7.b(ringProgressBar6, "it.rpbSmallest");
                    ringProgressBar6.setVisibility(0);
                    RingProgressBar ringProgressBar7 = a2.S;
                    pq7.b(ringProgressBar7, "it.rpbBig");
                    a7(ringProgressBar7, RingProgressBar.a.CALORIES);
                    RingProgressBar ringProgressBar8 = a2.U;
                    pq7.b(ringProgressBar8, "it.rpbMedium");
                    a7(ringProgressBar8, RingProgressBar.a.SLEEP);
                    RingProgressBar ringProgressBar9 = a2.V;
                    pq7.b(ringProgressBar9, "it.rpbSmallest");
                    a7(ringProgressBar9, RingProgressBar.a.GOAL);
                    FlexibleFitnessTab flexibleFitnessTab6 = a2.F;
                    String c7 = um5.c(PortfolioApp.h0.c(), 2131886778);
                    pq7.b(c7, "LanguageHelper.getString\u2026_StepsToday_Label__Steps)");
                    flexibleFitnessTab6.J(c7);
                    FlexibleFitnessTab flexibleFitnessTab7 = a2.G;
                    String c8 = um5.c(PortfolioApp.h0.c(), 2131886775);
                    pq7.b(c8, "LanguageHelper.getString\u2026n_StepsToday_Label__Cals)");
                    flexibleFitnessTab7.J(c8);
                    FlexibleFitnessTab flexibleFitnessTab8 = a2.H;
                    String c9 = um5.c(PortfolioApp.h0.c(), 2131886777);
                    pq7.b(c9, "LanguageHelper.getString\u2026epsToday_Label__OfNumber)");
                    flexibleFitnessTab8.J(c9);
                    FlexibleFitnessTab flexibleFitnessTab9 = a2.J;
                    String c10 = um5.c(PortfolioApp.h0.c(), 2131886776);
                    pq7.b(c10, "LanguageHelper.getString\u2026tepsToday_Label__HrsMins)");
                    flexibleFitnessTab9.J(c10);
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a7(RingProgressBar ringProgressBar, RingProgressBar.a aVar) {
        int i2 = bz5.f535a[aVar.ordinal()];
        if (i2 == 1) {
            ringProgressBar.setIconSource(2131231189);
            ringProgressBar.i(this.y, this.W);
            ringProgressBar.j("dianaActiveMinuteRing", "dianaUnfilledRing");
        } else if (i2 == 2) {
            ringProgressBar.setIconSource(2131231193);
            ringProgressBar.i(this.x, this.W);
            nk5.a aVar2 = nk5.o;
            kd6 kd6 = this.N;
            if (kd6 == null) {
                pq7.n("mPresenter");
                throw null;
            } else if (aVar2.w(kd6.n())) {
                ringProgressBar.j("dianaStepsRing", "dianaUnfilledRing");
            } else {
                ringProgressBar.j("hybridStepsRing", "hybridUnfilledRing");
            }
        } else if (i2 == 3) {
            ringProgressBar.setIconSource(2131231190);
            ringProgressBar.i(this.z, this.W);
            nk5.a aVar3 = nk5.o;
            kd6 kd62 = this.N;
            if (kd62 == null) {
                pq7.n("mPresenter");
                throw null;
            } else if (aVar3.w(kd62.n())) {
                ringProgressBar.j("dianaActiveCaloriesRing", "dianaUnfilledRing");
            } else {
                ringProgressBar.j("hybridActiveCaloriesRing", "hybridUnfilledRing");
            }
        } else if (i2 == 4) {
            ringProgressBar.setIconSource(2131231192);
            ringProgressBar.i(this.B, this.W);
            nk5.a aVar4 = nk5.o;
            kd6 kd63 = this.N;
            if (kd63 == null) {
                pq7.n("mPresenter");
                throw null;
            } else if (aVar4.w(kd63.n())) {
                ringProgressBar.j("dianaSleepRing", "dianaUnfilledRing");
            } else {
                ringProgressBar.j("hybridSleepRing", "hybridUnfilledRing");
            }
        } else if (i2 == 5) {
            ringProgressBar.setIconSource(2131231191);
            ringProgressBar.i(this.F, this.W);
            ringProgressBar.j("hybridGoalTrackingRing", "hybridUnfilledRing");
        }
    }

    @DexIgnore
    @Override // com.fossil.aw5
    public void b2(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("HomeDashboardFragment visible=");
        sb.append(z2);
        sb.append(", tracer=");
        sb.append(C6());
        sb.append(", isRunning=");
        vl5 C6 = C6();
        sb.append(C6 != null ? Boolean.valueOf(C6.f()) : null);
        local.d("onVisibleChanged", sb.toString());
        if (z2) {
            vl5 C62 = C6();
            if (C62 != null) {
                C62.i();
            }
            if (this.M != null) {
                int i2 = this.S;
                if (i2 == 2 || i2 == -1) {
                    T6();
                    return;
                }
                return;
            }
            return;
        }
        vl5 C63 = C6();
        if (C63 != null) {
            C63.c("");
        }
    }

    @DexIgnore
    public final void b7(int i2) {
        ViewPager2 viewPager2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "scroll to position=" + i2);
        g37<v75> g37 = this.M;
        if (g37 != null) {
            v75 a2 = g37.a();
            if (a2 != null) {
                a2.F.I(this.V);
                a2.J.I(this.V);
                a2.G.I(this.V);
                a2.E.I(this.V);
                a2.I.I(this.V);
                a2.H.I(this.V);
                nk5.a aVar = nk5.o;
                kd6 kd6 = this.N;
                if (kd6 != null) {
                    if (aVar.w(kd6.n())) {
                        a2.F.H(this.m);
                        a2.E.H(this.m);
                        a2.G.H(this.m);
                        a2.I.H(this.m);
                        a2.J.H(this.m);
                        if (i2 == 0) {
                            a2.f0.setBackgroundColor(this.x);
                            a2.F.H(this.h);
                            a2.F.I(this.x);
                        } else if (i2 == 1) {
                            a2.f0.setBackgroundColor(this.y);
                            a2.E.I(this.y);
                            a2.E.H(this.i);
                        } else if (i2 == 2) {
                            a2.f0.setBackgroundColor(this.z);
                            a2.G.I(this.z);
                            a2.G.H(this.j);
                        } else if (i2 == 3) {
                            a2.f0.setBackgroundColor(this.A);
                            a2.I.I(this.A);
                            a2.I.H(this.k);
                        } else if (i2 == 5) {
                            a2.f0.setBackgroundColor(this.B);
                            a2.J.I(this.B);
                            a2.J.H(this.l);
                        }
                    } else {
                        a2.F.H(this.v);
                        a2.H.H(this.v);
                        a2.G.H(this.v);
                        a2.J.H(this.v);
                        if (i2 == 0) {
                            a2.f0.setBackgroundColor(this.C);
                            a2.F.H(this.s);
                            a2.F.I(this.C);
                        } else if (i2 == 2) {
                            a2.f0.setBackgroundColor(this.D);
                            a2.G.I(this.D);
                            a2.G.H(this.t);
                        } else if (i2 == 4) {
                            a2.f0.setBackgroundColor(this.F);
                            a2.H.I(this.F);
                            a2.H.H(this.w);
                        } else if (i2 == 5) {
                            a2.f0.setBackgroundColor(this.E);
                            a2.J.I(this.E);
                            a2.J.H(this.u);
                        }
                    }
                    g37<v75> g372 = this.M;
                    if (g372 != null) {
                        v75 a3 = g372.a();
                        if (!(a3 == null || (viewPager2 = a3.W) == null)) {
                            viewPager2.j(i2, false);
                        }
                        this.P = i2;
                        kd6 kd62 = this.N;
                        if (kd62 != null) {
                            kd62.q(i2);
                        } else {
                            pq7.n("mPresenter");
                            throw null;
                        }
                    } else {
                        pq7.n("mBinding");
                        throw null;
                    }
                } else {
                    pq7.n("mPresenter");
                    throw null;
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void g5(ActivitySummary activitySummary, MFSleepDay mFSleepDay, GoalTrackingSummary goalTrackingSummary) {
        float f2;
        float f3;
        float f4;
        float f5;
        float f6;
        g37<v75> g37 = this.M;
        if (g37 != null) {
            v75 a2 = g37.a();
            if (a2 != null) {
                int d2 = sk5.c.d(activitySummary, rh5.ACTIVE_TIME);
                float f7 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                if (d2 > 0) {
                    f2 = (activitySummary != null ? (float) activitySummary.getActiveTime() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) d2);
                } else {
                    f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int d3 = sk5.c.d(activitySummary, rh5.TOTAL_STEPS);
                if (d3 > 0) {
                    f3 = (activitySummary != null ? (float) activitySummary.getSteps() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) d3);
                } else {
                    f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int d4 = sk5.c.d(activitySummary, rh5.CALORIES);
                if (d4 > 0) {
                    f4 = (activitySummary != null ? (float) activitySummary.getCalories() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) d4);
                } else {
                    f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int e2 = sk5.c.e(mFSleepDay);
                if (e2 > 0) {
                    f5 = (mFSleepDay != null ? (float) mFSleepDay.getSleepMinutes() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) / ((float) e2);
                } else {
                    f5 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                }
                int c2 = sk5.c.c(goalTrackingSummary);
                if (c2 > 0) {
                    if (goalTrackingSummary != null) {
                        f7 = (float) goalTrackingSummary.getTotalTracked();
                    }
                    f6 = f7 / ((float) c2);
                } else {
                    f6 = 0.0f;
                }
                boolean z2 = f3 >= 1.0f && f4 >= 1.0f && f5 >= 1.0f;
                FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "updateVisualization steps: " + f3 + ", time: " + f2 + ", calories: " + f4 + ", sleep: " + f5);
                nk5.a aVar = nk5.o;
                kd6 kd6 = this.N;
                if (kd6 == null) {
                    pq7.n("mPresenter");
                    throw null;
                } else if (aVar.w(kd6.n())) {
                    boolean z3 = z2 && f2 >= 1.0f;
                    a2.T.l(f3, z3);
                    a2.S.l(f2, z3);
                    a2.U.l(f4, z3);
                    a2.V.l(f5, z3);
                } else {
                    boolean z4 = z2 && f6 >= 1.0f;
                    a2.T.l(f3, z4);
                    a2.S.l(f4, z4);
                    a2.U.l(f5, z4);
                    a2.V.l(f6, z4);
                }
            }
        } else {
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void o0(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardFragment", "updateOtaProgress " + i2 + " isActive " + isActive());
        if (isActive()) {
            g37<v75> g37 = this.M;
            if (g37 != null) {
                v75 a2 = g37.a();
                if (a2 != null) {
                    ConstraintLayout constraintLayout = a2.A;
                    pq7.b(constraintLayout, "it.clUpdateFw");
                    if (constraintLayout.getVisibility() != 0) {
                        ConstraintLayout constraintLayout2 = a2.A;
                        pq7.b(constraintLayout2, "it.clUpdateFw");
                        constraintLayout2.setVisibility(0);
                        FlexibleProgressBar flexibleProgressBar = a2.Q;
                        pq7.b(flexibleProgressBar, "it.pbProgress");
                        flexibleProgressBar.setMax(100);
                    }
                    FlexibleProgressBar flexibleProgressBar2 = a2.Y;
                    pq7.b(flexibleProgressBar2, "it.syncProgress");
                    if (flexibleProgressBar2.getVisibility() == 0) {
                        T6();
                    }
                    a2.X.setDisableSwipe(true);
                    FlexibleProgressBar flexibleProgressBar3 = a2.Q;
                    pq7.b(flexibleProgressBar3, "it.pbProgress");
                    flexibleProgressBar3.setProgress(i2);
                    return;
                }
                return;
            }
            pq7.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FLogger.INSTANCE.getLocal().d("HomeDashboardFragment", "onActivityCreated");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        pq7.c(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        this.M = new g37<>(this, (v75) aq0.f(layoutInflater, 2131558574, viewGroup, false, A6()));
        W6();
        g37<v75> g37 = this.M;
        if (g37 != null) {
            v75 a2 = g37.a();
            if (a2 != null) {
                return a2.n();
            }
            return null;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        kd6 kd6 = this.N;
        if (kd6 == null) {
            return;
        }
        if (kd6 != null) {
            kd6.m();
            vl5 C6 = C6();
            if (C6 != null) {
                C6.c("");
                return;
            }
            return;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        kd6 kd6 = this.N;
        if (kd6 == null) {
            return;
        }
        if (kd6 != null) {
            b7(kd6.o());
            Z6();
            kd6 kd62 = this.N;
            if (kd62 != null) {
                kd62.l();
                vl5 C6 = C6();
                if (C6 != null) {
                    C6.i();
                    return;
                }
                return;
            }
            pq7.n("mPresenter");
            throw null;
        }
        pq7.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        pq7.c(view, "view");
        super.onViewCreated(view, bundle);
        V6();
        wa1 v2 = oa1.v(this);
        pq7.b(v2, "Glide.with(this)");
        this.T = v2;
        g37<v75> g37 = this.M;
        if (g37 != null) {
            v75 a2 = g37.a();
            if (a2 != null) {
                PortfolioApp.h0.c().K().h(getViewLifecycleOwner(), new c(a2, AnimationUtils.loadAnimation(requireContext(), 2130771996), this));
                if (wr4.f3989a.a().e()) {
                    String Q2 = PortfolioApp.h0.c().Q();
                    if (Q2 != null) {
                        String upperCase = Q2.toUpperCase();
                        pq7.b(upperCase, "(this as java.lang.String).toUpperCase()");
                        FlexibleTextView flexibleTextView = a2.K;
                        pq7.b(flexibleTextView, "binding.ftvDescription");
                        hr7 hr7 = hr7.f1520a;
                        String c2 = um5.c(getContext(), 2131887069);
                        pq7.b(c2, "LanguageHelper.getString\u2026rABrandSmartwatchToStart)");
                        String format = String.format(c2, Arrays.copyOf(new Object[]{upperCase}, 1));
                        pq7.b(format, "java.lang.String.format(format, *args)");
                        flexibleTextView.setText(format);
                    } else {
                        throw new il7("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    String c3 = um5.c(getContext(), 2131887069);
                    pq7.b(c3, "withoutDeviceText");
                    String q = vt7.q(c3, "%s", "", false, 4, null);
                    FlexibleTextView flexibleTextView2 = a2.K;
                    pq7.b(flexibleTextView2, "binding.ftvDescription");
                    flexibleTextView2.setText(q);
                }
                FlexibleProgressBar flexibleProgressBar = a2.Y;
                pq7.b(flexibleProgressBar, "binding.syncProgress");
                flexibleProgressBar.setMax(10000);
                a2.X.setOnRefreshListener(new e(a2, this));
                View headView = a2.X.getHeadView();
                if (!(headView instanceof l67)) {
                    headView = null;
                }
                this.Q = (l67) headView;
                a2.D.setOnClickListener(new f(this));
                if (!PortfolioApp.h0.c().z0()) {
                    a2.O.setOnLongClickListener(new g(this));
                }
                a2.q.b(new m(a2));
                a2.F.setOnClickListener(new h(this));
                a2.E.setOnClickListener(new i(this));
                a2.G.setOnClickListener(new j(this));
                a2.I.setOnClickListener(new k(this));
                a2.J.setOnClickListener(new l(this));
                a2.H.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void q1(boolean z2, boolean z3, boolean z4) {
        g37<v75> g37 = this.M;
        if (g37 != null) {
            v75 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z2 || !z4) {
                ConstraintLayout constraintLayout = a2.B;
                pq7.b(constraintLayout, "binding.clVisualization");
                constraintLayout.setVisibility(0);
                ConstraintLayout constraintLayout2 = a2.y;
                pq7.b(constraintLayout2, "binding.clNoDevice");
                constraintLayout2.setVisibility(8);
                a2.X.setByPass(!z3);
                if (!z4 && !z3) {
                    ImageView imageView = a2.N;
                    pq7.b(imageView, "binding.ivNoWatchFound");
                    imageView.setVisibility(0);
                    return;
                }
                return;
            }
            ConstraintLayout constraintLayout3 = a2.B;
            pq7.b(constraintLayout3, "binding.clVisualization");
            constraintLayout3.setVisibility(8);
            ConstraintLayout constraintLayout4 = a2.y;
            pq7.b(constraintLayout4, "binding.clNoDevice");
            constraintLayout4.setVisibility(0);
            a2.X.setByPass(true);
            ImageView imageView2 = a2.N;
            pq7.b(imageView2, "binding.ivNoWatchFound");
            imageView2.setVisibility(8);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void q6() {
        if (isActive()) {
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.x0(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void r2(Date date) {
        pq7.c(date, "date");
        g37<v75> g37 = this.M;
        if (g37 != null) {
            v75 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.a0;
                pq7.b(flexibleTextView, "tvToday");
                hr7 hr7 = hr7.f1520a;
                String string = PortfolioApp.h0.c().getString(2131886691);
                pq7.b(string, "PortfolioApp.instance.ge\u2026ay_Title__TodayMonthDate)");
                String format = String.format(string, Arrays.copyOf(new Object[]{ll5.c(date)}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void t1() {
        if (isActive()) {
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "confirmCancelingWorkout");
            s37 s37 = s37.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            pq7.b(childFragmentManager, "childFragmentManager");
            s37.c(childFragmentManager);
            FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "confirmCancelingWorkout");
        }
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void t6() {
        CustomSwipeRefreshLayout customSwipeRefreshLayout;
        FLogger.INSTANCE.getLocal().e("HomeDashboardFragment", "showAutoSync");
        X6(0);
        g37<v75> g37 = this.M;
        if (g37 != null) {
            v75 a2 = g37.a();
            if (a2 != null && (customSwipeRefreshLayout = a2.X) != null) {
                customSwipeRefreshLayout.r();
                return;
            }
            return;
        }
        pq7.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.pv5, com.fossil.qv5
    public void v6() {
        HashMap hashMap = this.X;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.ld6
    public void x3(boolean z2) {
        g37<v75> g37 = this.M;
        if (g37 != null) {
            v75 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z2) {
                FlexibleTextView flexibleTextView = a2.M;
                pq7.b(flexibleTextView, "it.ftvLowBattery");
                hr7 hr7 = hr7.f1520a;
                String c2 = um5.c(PortfolioApp.h0.c(), 2131886847);
                pq7.b(c2, "LanguageHelper.getString\u2026YourBatteryIsBelowNumber)");
                String format = String.format(c2, Arrays.copyOf(new Object[]{"25%"}, 1));
                pq7.b(format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                NestedScrollView nestedScrollView = a2.P;
                pq7.b(nestedScrollView, "it.nsvLowBattery");
                nestedScrollView.setVisibility(0);
                return;
            }
            NestedScrollView nestedScrollView2 = a2.P;
            pq7.b(nestedScrollView2, "it.nsvLowBattery");
            nestedScrollView2.setVisibility(8);
            return;
        }
        pq7.n("mBinding");
        throw null;
    }
}
