package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.mapped.W6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Yy3 extends Xy3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai extends C04 {
        @DexIgnore
        public Ai(G04 g04) {
            super(g04);
        }

        @DexIgnore
        @Override // com.fossil.C04
        public boolean isStateful() {
            return true;
        }
    }

    @DexIgnore
    public Yy3(FloatingActionButton floatingActionButton, Vz3 vz3) {
        super(floatingActionButton, vz3);
    }

    @DexIgnore
    @Override // com.fossil.Xy3
    public void A() {
    }

    @DexIgnore
    @Override // com.fossil.Xy3
    public void C() {
        f0();
    }

    @DexIgnore
    @Override // com.fossil.Xy3
    public void E(int[] iArr) {
        if (Build.VERSION.SDK_INT != 21) {
            return;
        }
        if (this.y.isEnabled()) {
            this.y.setElevation(this.h);
            if (this.y.isPressed()) {
                this.y.setTranslationZ(this.j);
            } else if (this.y.isFocused() || this.y.isHovered()) {
                this.y.setTranslationZ(this.i);
            } else {
                this.y.setTranslationZ(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        } else {
            this.y.setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.y.setTranslationZ(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    @Override // com.fossil.Xy3
    public void F(float f, float f2, float f3) {
        if (Build.VERSION.SDK_INT == 21) {
            this.y.refreshDrawableState();
        } else {
            StateListAnimator stateListAnimator = new StateListAnimator();
            stateListAnimator.addState(Xy3.G, j0(f, f3));
            stateListAnimator.addState(Xy3.H, j0(f, f2));
            stateListAnimator.addState(Xy3.I, j0(f, f2));
            stateListAnimator.addState(Xy3.J, j0(f, f2));
            AnimatorSet animatorSet = new AnimatorSet();
            ArrayList arrayList = new ArrayList();
            arrayList.add(ObjectAnimator.ofFloat(this.y, "elevation", f).setDuration(0L));
            int i = Build.VERSION.SDK_INT;
            if (i >= 22 && i <= 24) {
                FloatingActionButton floatingActionButton = this.y;
                arrayList.add(ObjectAnimator.ofFloat(floatingActionButton, View.TRANSLATION_Z, floatingActionButton.getTranslationZ()).setDuration(100L));
            }
            arrayList.add(ObjectAnimator.ofFloat(this.y, View.TRANSLATION_Z, 0.0f).setDuration(100L));
            animatorSet.playSequentially((Animator[]) arrayList.toArray(new Animator[0]));
            animatorSet.setInterpolator(Xy3.F);
            stateListAnimator.addState(Xy3.K, animatorSet);
            stateListAnimator.addState(Xy3.L, j0(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            this.y.setStateListAnimator(stateListAnimator);
        }
        if (Z()) {
            f0();
        }
    }

    @DexIgnore
    @Override // com.fossil.Xy3
    public boolean K() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.Xy3
    public void V(ColorStateList colorStateList) {
        Drawable drawable = this.c;
        if (drawable instanceof RippleDrawable) {
            ((RippleDrawable) drawable).setColor(Tz3.d(colorStateList));
        } else {
            super.V(colorStateList);
        }
    }

    @DexIgnore
    @Override // com.fossil.Xy3
    public boolean Z() {
        return this.z.c() || !b0();
    }

    @DexIgnore
    @Override // com.fossil.Xy3
    public void d0() {
    }

    @DexIgnore
    public Wy3 i0(int i, ColorStateList colorStateList) {
        Context context = this.y.getContext();
        G04 g04 = this.a;
        Pn0.d(g04);
        Wy3 wy3 = new Wy3(g04);
        wy3.e(W6.d(context, Kw3.design_fab_stroke_top_outer_color), W6.d(context, Kw3.design_fab_stroke_top_inner_color), W6.d(context, Kw3.design_fab_stroke_end_inner_color), W6.d(context, Kw3.design_fab_stroke_end_outer_color));
        wy3.d((float) i);
        wy3.c(colorStateList);
        return wy3;
    }

    @DexIgnore
    @Override // com.fossil.Xy3
    public C04 j() {
        G04 g04 = this.a;
        Pn0.d(g04);
        return new Ai(g04);
    }

    @DexIgnore
    public final Animator j0(float f, float f2) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(ObjectAnimator.ofFloat(this.y, "elevation", f).setDuration(0L)).with(ObjectAnimator.ofFloat(this.y, View.TRANSLATION_Z, f2).setDuration(100L));
        animatorSet.setInterpolator(Xy3.F);
        return animatorSet;
    }

    @DexIgnore
    @Override // com.fossil.Xy3
    public float n() {
        return this.y.getElevation();
    }

    @DexIgnore
    @Override // com.fossil.Xy3
    public void s(Rect rect) {
        if (this.z.c()) {
            super.s(rect);
        } else if (!b0()) {
            int sizeDimension = (this.k - this.y.getSizeDimension()) / 2;
            rect.set(sizeDimension, sizeDimension, sizeDimension, sizeDimension);
        } else {
            rect.set(0, 0, 0, 0);
        }
    }

    @DexIgnore
    @Override // com.fossil.Xy3
    public void x(ColorStateList colorStateList, PorterDuff.Mode mode, ColorStateList colorStateList2, int i) {
        Drawable drawable;
        C04 j = j();
        this.b = j;
        j.setTintList(colorStateList);
        if (mode != null) {
            this.b.setTintMode(mode);
        }
        this.b.M(this.y.getContext());
        if (i > 0) {
            this.d = i0(i, colorStateList);
            Wy3 wy3 = this.d;
            Pn0.d(wy3);
            C04 c04 = this.b;
            Pn0.d(c04);
            drawable = new LayerDrawable(new Drawable[]{wy3, c04});
        } else {
            this.d = null;
            drawable = this.b;
        }
        RippleDrawable rippleDrawable = new RippleDrawable(Tz3.d(colorStateList2), drawable, null);
        this.c = rippleDrawable;
        this.e = rippleDrawable;
    }
}
