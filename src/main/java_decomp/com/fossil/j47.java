package com.fossil;

import com.fossil.Xx0;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class J47 implements Xx0 {
    @DexIgnore
    public /* final */ AtomicInteger a; // = new AtomicInteger(0);
    @DexIgnore
    public volatile Xx0.Ai b;

    @DexIgnore
    public J47(String str) {
    }

    @DexIgnore
    @Override // com.fossil.Xx0
    public boolean a() {
        return this.a.get() == 0;
    }

    @DexIgnore
    public void b() {
        int decrementAndGet = this.a.decrementAndGet();
        if (decrementAndGet == 0 && this.b != null) {
            this.b.a();
        }
        if (decrementAndGet < 0) {
            throw new IllegalArgumentException("Counter has been corrupted!");
        }
    }

    @DexIgnore
    public void c() {
        this.a.getAndIncrement();
    }
}
