package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Eb5 extends Db5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d Q; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray R;
    @DexIgnore
    public long P;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        R = sparseIntArray;
        sparseIntArray.put(2131362810, 1);
        R.put(2131362686, 2);
        R.put(2131362968, 3);
        R.put(2131363410, 4);
        R.put(2131363316, 5);
        R.put(2131363157, 6);
        R.put(2131362638, 7);
        R.put(2131362233, 8);
        R.put(2131362679, 9);
        R.put(2131362644, 10);
        R.put(2131362241, 11);
        R.put(2131363329, 12);
        R.put(2131363328, 13);
        R.put(2131361924, 14);
        R.put(2131361926, 15);
        R.put(2131363398, 16);
        R.put(2131362714, 17);
        R.put(2131362719, 18);
        R.put(2131362661, 19);
        R.put(2131362772, 20);
        R.put(2131362771, 21);
        R.put(2131363340, 22);
        R.put(2131363358, 23);
        R.put(2131361941, 24);
    }
    */

    @DexIgnore
    public Eb5(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 25, Q, R));
    }

    @DexIgnore
    public Eb5(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (Barrier) objArr[14], (Barrier) objArr[15], (FlexibleButton) objArr[24], (FlexibleTextInputEditText) objArr[8], (FlexibleTextInputEditText) objArr[11], (FlexibleTextInputLayout) objArr[7], (FlexibleTextInputLayout) objArr[10], (FloatingActionButton) objArr[19], (RTLImageView) objArr[9], (RTLImageView) objArr[2], (ImageView) objArr[17], (ImageView) objArr[18], (ConstraintLayout) objArr[21], (ConstraintLayout) objArr[20], (ConstraintLayout) objArr[1], (DashBar) objArr[3], (ConstraintLayout) objArr[0], (ScrollView) objArr[6], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[22], (FlexibleButton) objArr[23], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[4]);
        this.P = -1;
        this.G.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.P = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.P != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.P = 1;
        }
        w();
    }
}
