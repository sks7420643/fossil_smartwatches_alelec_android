package com.fossil;

import androidx.loader.app.LoaderManager;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L16 {
    @DexIgnore
    public /* final */ J16 a;
    @DexIgnore
    public /* final */ LoaderManager b;

    @DexIgnore
    public L16(J16 j16, LoaderManager loaderManager) {
        Wg6.c(j16, "mView");
        Wg6.c(loaderManager, "mLoaderManager");
        this.a = j16;
        this.b = loaderManager;
    }

    @DexIgnore
    public final LoaderManager a() {
        return this.b;
    }

    @DexIgnore
    public final J16 b() {
        return this.a;
    }
}
