package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Pi7 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Object b();  // void declaration
}
