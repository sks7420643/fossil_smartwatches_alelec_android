package com.fossil;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Vv1;
import com.fossil.imagefilters.EInkImageFactory;
import com.fossil.imagefilters.Format;
import com.mapped.Kc6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Sv1 extends Mv1 {
    @DexIgnore
    public static /* final */ Ai j; // = new Ai(null);
    @DexIgnore
    public /* final */ Vb0 e;
    @DexIgnore
    public /* final */ Vv1 f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public Uv1 h;
    @DexIgnore
    public Tv1 i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Sv1 a(JSONObject jSONObject, Cc0[] cc0Arr) {
            try {
                Vv1.Ai ai = Vv1.d;
                String string = jSONObject.getString("name");
                Wg6.b(string, "jsonObject.getString(UIScriptConstant.NAME)");
                Vv1 a2 = ai.a(string);
                if (a2 != null) {
                    switch (Wb0.a[a2.ordinal()]) {
                        case 1:
                            return new Aw1(jSONObject, cc0Arr);
                        case 2:
                            return new Zv1(jSONObject, cc0Arr);
                        case 3:
                            return new Wv1(jSONObject, cc0Arr);
                        case 4:
                            return new Rv1(jSONObject, cc0Arr);
                        case 5:
                            return new Yv1(jSONObject, cc0Arr);
                        case 6:
                            return new Ov1(jSONObject, cc0Arr);
                        case 7:
                            return new Qv1(jSONObject, cc0Arr);
                        case 8:
                            return new Pv1(jSONObject, cc0Arr);
                        case 9:
                            return new Xv1(jSONObject, cc0Arr);
                        default:
                            throw new Kc6();
                    }
                }
            } catch (Exception e) {
            }
            return null;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Sv1(Parcel parcel) {
        super(parcel);
        boolean z = true;
        this.e = Vb0.c;
        this.f = Vv1.values()[parcel.readInt()];
        this.g = parcel.readByte() != ((byte) 1) ? false : z;
        Parcelable readParcelable = parcel.readParcelable(Uv1.class.getClassLoader());
        if (readParcelable != null) {
            this.h = (Uv1) readParcelable;
            this.i = (Tv1) parcel.readParcelable(Gw1.class.getClassLoader());
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Sv1(Vv1 vv1, Jv1 jv1, Kv1 kv1, boolean z, Uv1 uv1, Tv1 tv1, int i2) {
        super(G80.e(0, 1), jv1, kv1);
        z = (i2 & 8) != 0 ? false : z;
        uv1 = (i2 & 16) != 0 ? new Uv1(Cw1.DEFAULT) : uv1;
        tv1 = (i2 & 32) != 0 ? null : tv1;
        this.e = Vb0.c;
        this.f = vv1;
        this.g = z;
        this.h = uv1;
        if (!z) {
            this.i = tv1;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Sv1(JSONObject jSONObject, Cc0[] cc0Arr, String str, int i2) throws IllegalArgumentException {
        super(jSONObject, (i2 & 4) != 0 ? null : str);
        Cc0 cc0;
        byte[] bArr;
        Tv1 tv1 = null;
        this.e = Vb0.c;
        try {
            Vv1.Ai ai = Vv1.d;
            String string = jSONObject.getString("name");
            Wg6.b(string, "jsonObject.getString(UIScriptConstant.NAME)");
            Vv1 a2 = ai.a(string);
            if (a2 != null) {
                this.f = a2;
                this.g = jSONObject.optBoolean("goal_ring", false);
                Uv1 a3 = Uv1.CREATOR.a(jSONObject);
                this.h = a3 == null ? new Uv1(Cw1.DEFAULT) : a3;
                Object opt = jSONObject.opt("bg");
                if (opt instanceof String) {
                    int length = cc0Arr.length;
                    int i3 = 0;
                    while (true) {
                        if (i3 >= length) {
                            cc0 = null;
                            break;
                        }
                        cc0 = cc0Arr[i3];
                        if (Wg6.a(opt, cc0.b)) {
                            break;
                        }
                        i3++;
                    }
                    if (!(cc0 == null || (bArr = cc0.c) == null)) {
                        Bitmap decode = EInkImageFactory.decode(bArr, Format.RLE);
                        Wg6.b(decode, "EInkImageFactory.decode(imageData, Format.RLE)");
                        tv1 = new Tv1((String) opt, Cy1.b(decode, null, 1, null));
                        decode.recycle();
                    }
                }
                this.i = tv1;
                return;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        } catch (Exception e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    @DexIgnore
    public final Cc0 a(String str) {
        Tv1 tv1 = this.i;
        if (tv1 == null) {
            return null;
        }
        Lv1 actualSize = c().toActualSize(240, 240);
        if (str == null) {
            str = tv1.getName();
        }
        return tv1.a(actualSize, str);
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public Vb0 a() {
        return this.e;
    }

    @DexIgnore
    public final JSONObject b(String str) {
        JSONObject put = super.d().put("name", this.f.a()).put("goal_ring", this.g).put("data", e());
        Wg6.b(put, "super.getUIScript()\n    \u2026nt.DATA, getDataScript())");
        JSONObject c = Gy1.c(put, this.h.a());
        Tv1 tv1 = this.i;
        if (tv1 != null) {
            if (str == null) {
                str = tv1.getName();
            }
            c.put("bg", str);
        }
        return c;
    }

    @DexIgnore
    @Override // java.lang.Object, com.fossil.Mv1, com.fossil.Mv1
    public abstract /* synthetic */ Nx1 clone();

    @DexIgnore
    @Override // com.fossil.Mv1
    public JSONObject d() {
        Tv1 tv1 = this.i;
        return b(tv1 != null ? tv1.getName() : null);
    }

    @DexIgnore
    public Object e() {
        return null;
    }

    @DexIgnore
    public final Tv1 getBackgroundImage() {
        return this.i;
    }

    @DexIgnore
    public final Vv1 getComplicationType() {
        return this.f;
    }

    @DexIgnore
    public final Uv1 getTheme() {
        return this.h;
    }

    @DexIgnore
    public final boolean isPercentageCircleEnable() {
        return this.g;
    }

    @DexIgnore
    public Sv1 setBackgroundImage(Tv1 tv1) {
        if (tv1 == null) {
            tv1 = null;
        } else {
            this.g = false;
        }
        this.i = tv1;
        return this;
    }

    @DexIgnore
    public Sv1 setPercentageCircleEnable(boolean z) {
        this.g = z;
        if (z) {
            this.i = null;
        }
        return this;
    }

    @DexIgnore
    public Sv1 setTheme(Uv1 uv1) {
        this.h = uv1;
        return this;
    }

    @DexIgnore
    @Override // com.fossil.Mv1
    public void writeToParcel(Parcel parcel, int i2) {
        super.writeToParcel(parcel, i2);
        if (parcel != null) {
            parcel.writeInt(this.f.ordinal());
        }
        if (parcel != null) {
            parcel.writeByte(this.g ? (byte) 1 : 0);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.h, i2);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.i, i2);
        }
    }
}
