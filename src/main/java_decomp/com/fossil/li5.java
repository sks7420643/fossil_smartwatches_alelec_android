package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Li5 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[Mi5.values().length];
        a = iArr;
        iArr[Mi5.RUNNING.ordinal()] = 1;
        a[Mi5.CYCLING.ordinal()] = 2;
        a[Mi5.TREADMILL.ordinal()] = 3;
        a[Mi5.ELLIPTICAL.ordinal()] = 4;
        a[Mi5.WEIGHTS.ordinal()] = 5;
        a[Mi5.WORKOUT.ordinal()] = 6;
        a[Mi5.WALKING.ordinal()] = 7;
        a[Mi5.ROWING.ordinal()] = 8;
        a[Mi5.HIKING.ordinal()] = 9;
        a[Mi5.YOGA.ordinal()] = 10;
        a[Mi5.SWIMMING.ordinal()] = 11;
        a[Mi5.AEROBIC.ordinal()] = 12;
        a[Mi5.SPINNING.ordinal()] = 13;
    }
    */
}
