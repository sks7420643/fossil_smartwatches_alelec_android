package com.fossil;

import com.fossil.Ix1;
import com.fossil.Sx1;
import com.mapped.Qg6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Wx1<T> extends Rx1<T> {
    @DexIgnore
    public /* final */ Ix1.Ai b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Wx1(byte b2, byte b3, Ry1 ry1) {
        super(ry1);
        Wg6.c(ry1, "baseVersion");
        this.c = (byte) b2;
        this.d = (byte) b3;
        this.b = Ix1.Ai.CRC32;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Wx1(byte b2, byte b3, Ry1 ry1, int i, Qg6 qg6) {
        this(b2, (i & 2) != 0 ? (byte) 255 : b3, (i & 4) != 0 ? new Ry1(1, 0) : ry1);
    }

    @DexIgnore
    @Override // com.fossil.Rx1
    public T b(byte[] bArr) throws Sx1 {
        Wg6.c(bArr, "data");
        if (e(bArr)) {
            return d(bArr);
        }
        throw new Sx1(Sx1.Ai.INVALID_FILE_DATA, "Invalid file.", null, 4, null);
    }

    @DexIgnore
    public Ix1.Ai c() {
        return this.b;
    }

    @DexIgnore
    public abstract T d(byte[] bArr) throws Sx1;

    @DexIgnore
    public boolean e(byte[] bArr) {
        Wg6.c(bArr, "fileData");
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        if (this.c != order.get(1)) {
            return false;
        }
        byte b2 = order.get(0);
        byte b3 = this.d;
        if ((b3 != ((byte) 255) && b3 != b2) || new Ry1(bArr[2], bArr[3]).getMajor() != a().getMajor() || Hy1.o(order.getInt(8)) != Hy1.o((bArr.length - 12) - 4)) {
            return false;
        }
        return Hy1.o(order.getInt(bArr.length + -4)) == Ix1.a.b(Dm7.k(bArr, 12, bArr.length + -4), c());
    }
}
