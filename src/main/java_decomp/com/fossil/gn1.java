package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gn1 extends R60 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public static /* final */ long f; // = Hy1.b(Oq7.a);
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ short d;
    @DexIgnore
    public /* final */ short e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Gn1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Gn1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 8) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new Gn1(Hy1.o(order.getInt(0)), order.getShort(4), order.getShort(6));
            }
            throw new IllegalArgumentException(E.b(E.e("Invalid data size: "), bArr.length, ", require: 8"));
        }

        @DexIgnore
        public Gn1 b(Parcel parcel) {
            return new Gn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Gn1 createFromParcel(Parcel parcel) {
            return new Gn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Gn1[] newArray(int i) {
            return new Gn1[i];
        }
    }

    @DexIgnore
    public Gn1(long j, short s, short s2) throws IllegalArgumentException {
        super(Zm1.TIME);
        this.c = j;
        this.d = (short) s;
        this.e = (short) s2;
        d();
    }

    @DexIgnore
    public /* synthetic */ Gn1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = parcel.readLong();
        this.d = (short) ((short) parcel.readInt());
        this.e = (short) ((short) parcel.readInt());
        d();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.c).putShort(this.d).putShort(this.e).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("second", this.c);
            jSONObject.put("millisecond", Short.valueOf(this.d));
            jSONObject.put("timezone_offset_in_minute", Short.valueOf(this.e));
        } catch (JSONException e2) {
            D90.i.i(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        boolean z = true;
        long j = f;
        long j2 = this.c;
        if (0 <= j2 && j >= j2) {
            short s = this.d;
            if (s >= 0 && 1000 >= s) {
                short s2 = this.e;
                if (Short.MIN_VALUE > s2 || Short.MAX_VALUE < s2) {
                    z = false;
                }
                if (!z) {
                    throw new IllegalArgumentException(E.c(E.e("timezoneOffsetInMinute("), this.e, ") is out of range ", "[-32768, 32767]."));
                }
                return;
            }
            throw new IllegalArgumentException(E.c(E.e("millisecond("), this.d, ") is out of range ", "[0, 1000]."));
        }
        StringBuilder e2 = E.e("second(");
        e2.append(this.c);
        e2.append(") is out of range ");
        e2.append("[0, ");
        e2.append(f);
        e2.append("].");
        throw new IllegalArgumentException(e2.toString());
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Gn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Gn1 gn1 = (Gn1) obj;
            if (this.c != gn1.c) {
                return false;
            }
            if (this.d != gn1.d) {
                return false;
            }
            return this.e == gn1.e;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.TimeConfig");
    }

    @DexIgnore
    public final short getMillisecond() {
        return this.d;
    }

    @DexIgnore
    public final long getSecond() {
        return this.c;
    }

    @DexIgnore
    public final short getTimezoneOffsetInMinute() {
        return this.e;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        return (((((super.hashCode() * 31) + Long.valueOf(this.c).hashCode()) * 31) + this.d) * 31) + this.e;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(Hy1.n(this.d));
        }
        if (parcel != null) {
            parcel.writeInt(Hy1.n(this.e));
        }
    }
}
