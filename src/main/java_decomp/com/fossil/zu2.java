package com.fossil;

import com.fossil.Av2;
import com.fossil.E13;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zu2 extends E13<Zu2, Ai> implements O23 {
    @DexIgnore
    public static /* final */ Zu2 zzd;
    @DexIgnore
    public static volatile Z23<Zu2> zze;
    @DexIgnore
    public M13<Av2> zzc; // = E13.B();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends E13.Ai<Zu2, Ai> implements O23 {
        @DexIgnore
        public Ai() {
            super(Zu2.zzd);
        }

        @DexIgnore
        public /* synthetic */ Ai(Tu2 tu2) {
            this();
        }

        @DexIgnore
        public final Ai x(Av2.Ai ai) {
            if (this.d) {
                u();
                this.d = false;
            }
            ((Zu2) this.c).G((Av2) ((E13) ai.h()));
            return this;
        }

        @DexIgnore
        public final Av2 y(int i) {
            return ((Zu2) this.c).C(0);
        }
    }

    /*
    static {
        Zu2 zu2 = new Zu2();
        zzd = zu2;
        E13.u(Zu2.class, zu2);
    }
    */

    @DexIgnore
    public static Ai H() {
        return (Ai) zzd.w();
    }

    @DexIgnore
    public final Av2 C(int i) {
        return this.zzc.get(0);
    }

    @DexIgnore
    public final List<Av2> D() {
        return this.zzc;
    }

    @DexIgnore
    public final void G(Av2 av2) {
        av2.getClass();
        M13<Av2> m13 = this.zzc;
        if (!m13.zza()) {
            this.zzc = E13.q(m13);
        }
        this.zzc.add(av2);
    }

    @DexIgnore
    @Override // com.fossil.E13
    public final Object r(int i, Object obj, Object obj2) {
        Z23 z23;
        switch (Tu2.a[i - 1]) {
            case 1:
                return new Zu2();
            case 2:
                return new Ai(null);
            case 3:
                return E13.s(zzd, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzc", Av2.class});
            case 4:
                return zzd;
            case 5:
                Z23<Zu2> z232 = zze;
                if (z232 != null) {
                    return z232;
                }
                synchronized (Zu2.class) {
                    try {
                        z23 = zze;
                        if (z23 == null) {
                            z23 = new E13.Ci(zzd);
                            zze = z23;
                        }
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                return z23;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
