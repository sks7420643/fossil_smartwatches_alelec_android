package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P53 implements Q53 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        a = hw2.d("measurement.sdk.dynamite.allow_remote_dynamite2", false);
        hw2.d("measurement.collection.init_params_control_enabled", true);
        hw2.d("measurement.sdk.dynamite.use_dynamite3", true);
        hw2.b("measurement.id.sdk.dynamite.use_dynamite", 0);
    }
    */

    @DexIgnore
    @Override // com.fossil.Q53
    public final boolean zza() {
        return a.o().booleanValue();
    }
}
