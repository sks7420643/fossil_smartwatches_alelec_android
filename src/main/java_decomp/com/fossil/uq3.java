package com.fossil;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PersistableBundle;
import com.misfit.frameworks.buttonservice.model.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uq3 extends Zq3 {
    @DexIgnore
    public /* final */ AlarmManager d; // = ((AlarmManager) e().getSystemService(Alarm.TABLE_NAME));
    @DexIgnore
    public /* final */ Ng3 e;
    @DexIgnore
    public Integer f;

    @DexIgnore
    public Uq3(Yq3 yq3) {
        super(yq3);
        this.e = new Xq3(this, yq3.f0(), yq3);
    }

    @DexIgnore
    @Override // com.fossil.Zq3
    public final boolean t() {
        this.d.cancel(y());
        if (Build.VERSION.SDK_INT < 24) {
            return false;
        }
        w();
        return false;
    }

    @DexIgnore
    public final void u(long j) {
        r();
        b();
        Context e2 = e();
        if (!Hm3.b(e2)) {
            d().M().a("Receiver not registered/enabled");
        }
        if (!Kr3.X(e2, false)) {
            d().M().a("Service not registered/enabled");
        }
        v();
        d().N().b("Scheduling upload, millis", Long.valueOf(j));
        long c = zzm().c();
        if (j < Math.max(0L, Xg3.x.a(null).longValue()) && !this.e.d()) {
            this.e.c(j);
        }
        b();
        if (Build.VERSION.SDK_INT >= 24) {
            Context e3 = e();
            ComponentName componentName = new ComponentName(e3, "com.google.android.gms.measurement.AppMeasurementJobService");
            int x = x();
            PersistableBundle persistableBundle = new PersistableBundle();
            persistableBundle.putString("action", "com.google.android.gms.measurement.UPLOAD");
            D03.b(e3, new JobInfo.Builder(x, componentName).setMinimumLatency(j).setOverrideDeadline(j << 1).setExtras(persistableBundle).build(), "com.google.android.gms", "UploadAlarm");
            return;
        }
        this.d.setInexactRepeating(2, c + j, Math.max(Xg3.s.a(null).longValue(), j), y());
    }

    @DexIgnore
    public final void v() {
        r();
        d().N().a("Unscheduling upload");
        this.d.cancel(y());
        this.e.e();
        if (Build.VERSION.SDK_INT >= 24) {
            w();
        }
    }

    @DexIgnore
    @TargetApi(24)
    public final void w() {
        ((JobScheduler) e().getSystemService("jobscheduler")).cancel(x());
    }

    @DexIgnore
    public final int x() {
        if (this.f == null) {
            String valueOf = String.valueOf(e().getPackageName());
            this.f = Integer.valueOf((valueOf.length() != 0 ? "measurement".concat(valueOf) : new String("measurement")).hashCode());
        }
        return this.f.intValue();
    }

    @DexIgnore
    public final PendingIntent y() {
        Context e2 = e();
        return PendingIntent.getBroadcast(e2, 0, new Intent().setClassName(e2, "com.google.android.gms.measurement.AppMeasurementReceiver").setAction("com.google.android.gms.measurement.UPLOAD"), 0);
    }
}
