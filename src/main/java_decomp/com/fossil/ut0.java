package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Uu0;
import com.mapped.Cf;
import com.mapped.Zf;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ut0<T> {
    @DexIgnore
    public /* final */ Jv0 a;
    @DexIgnore
    public /* final */ Uu0<T> b;
    @DexIgnore
    public Executor c; // = Bi0.g();
    @DexIgnore
    public /* final */ List<Ci<T>> d; // = new CopyOnWriteArrayList();
    @DexIgnore
    public boolean e;
    @DexIgnore
    public Cf<T> f;
    @DexIgnore
    public Cf<T> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public Cf.Ei i; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Cf.Ei {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.mapped.Cf.Ei
        public void a(int i, int i2) {
            Ut0.this.a.d(i, i2, null);
        }

        @DexIgnore
        @Override // com.mapped.Cf.Ei
        public void b(int i, int i2) {
            Ut0.this.a.b(i, i2);
        }

        @DexIgnore
        @Override // com.mapped.Cf.Ei
        public void c(int i, int i2) {
            Ut0.this.a.c(i, i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Cf b;
        @DexIgnore
        public /* final */ /* synthetic */ Cf c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;
        @DexIgnore
        public /* final */ /* synthetic */ Cf e;
        @DexIgnore
        public /* final */ /* synthetic */ Runnable f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Zf.Ci b;

            @DexIgnore
            public Aii(Zf.Ci ci) {
                this.b = ci;
            }

            @DexIgnore
            public void run() {
                Bi bi = Bi.this;
                Ut0 ut0 = Ut0.this;
                if (ut0.h == bi.d) {
                    ut0.d(bi.e, bi.c, this.b, bi.b.g, bi.f);
                }
            }
        }

        @DexIgnore
        public Bi(Cf cf, Cf cf2, int i, Cf cf3, Runnable runnable) {
            this.b = cf;
            this.c = cf2;
            this.d = i;
            this.e = cf3;
            this.f = runnable;
        }

        @DexIgnore
        public void run() {
            Ut0.this.c.execute(new Aii(Fu0.a(this.b.f, this.c.f, Ut0.this.b.b())));
        }
    }

    @DexIgnore
    public interface Ci<T> {
        @DexIgnore
        void a(Cf<T> cf, Cf<T> cf2);
    }

    @DexIgnore
    public Ut0(RecyclerView.g gVar, Zf.Di<T> di) {
        this.a = new Tu0(gVar);
        this.b = new Uu0.Ai(di).a();
    }

    @DexIgnore
    public void a(Ci<T> ci) {
        this.d.add(ci);
    }

    @DexIgnore
    public T b(int i2) {
        Cf<T> cf = this.f;
        if (cf == null) {
            Cf<T> cf2 = this.g;
            if (cf2 != null) {
                return cf2.get(i2);
            }
            throw new IndexOutOfBoundsException("Item count is zero, getItem() call is invalid");
        }
        cf.v(i2);
        return this.f.get(i2);
    }

    @DexIgnore
    public int c() {
        Cf<T> cf = this.f;
        if (cf != null) {
            return cf.size();
        }
        Cf<T> cf2 = this.g;
        if (cf2 == null) {
            return 0;
        }
        return cf2.size();
    }

    @DexIgnore
    public void d(Cf<T> cf, Cf<T> cf2, Zf.Ci ci, int i2, Runnable runnable) {
        Cf<T> cf3 = this.g;
        if (cf3 == null || this.f != null) {
            throw new IllegalStateException("must be in snapshot state to apply diff");
        }
        this.f = cf;
        this.g = null;
        Fu0.b(this.a, cf3.f, cf.f, ci);
        cf.j(cf2, this.i);
        if (!this.f.isEmpty()) {
            int c2 = Fu0.c(ci, cf3.f, cf2.f, i2);
            Cf<T> cf4 = this.f;
            cf4.v(Math.max(0, Math.min(cf4.size() - 1, c2)));
        }
        e(cf3, this.f, runnable);
    }

    @DexIgnore
    public final void e(Cf<T> cf, Cf<T> cf2, Runnable runnable) {
        for (Ci<T> ci : this.d) {
            ci.a(cf, cf2);
        }
        if (runnable != null) {
            runnable.run();
        }
    }

    @DexIgnore
    public void f(Cf<T> cf) {
        g(cf, null);
    }

    @DexIgnore
    public void g(Cf<T> cf, Runnable runnable) {
        if (cf != null) {
            if (this.f == null && this.g == null) {
                this.e = cf.s();
            } else if (cf.s() != this.e) {
                throw new IllegalArgumentException("AsyncPagedListDiffer cannot handle both contiguous and non-contiguous lists.");
            }
        }
        int i2 = this.h + 1;
        this.h = i2;
        Cf<T> cf2 = this.f;
        if (cf != cf2) {
            Cf<T> cf3 = this.g;
            if (cf3 == null) {
                cf3 = cf2;
            }
            if (cf == null) {
                int c2 = c();
                Cf<T> cf4 = this.f;
                if (cf4 != null) {
                    cf4.B(this.i);
                    this.f = null;
                } else if (this.g != null) {
                    this.g = null;
                }
                this.a.c(0, c2);
                e(cf3, null, runnable);
            } else if (this.f == null && this.g == null) {
                this.f = cf;
                cf.j(null, this.i);
                this.a.b(0, cf.size());
                e(null, cf, runnable);
            } else {
                Cf<T> cf5 = this.f;
                if (cf5 != null) {
                    cf5.B(this.i);
                    this.g = (Cf) this.f.D();
                    this.f = null;
                }
                Cf<T> cf6 = this.g;
                if (cf6 == null || this.f != null) {
                    throw new IllegalStateException("must be in snapshot state to diff");
                }
                this.b.a().execute(new Bi(cf6, (Cf) cf.D(), i2, cf, runnable));
            }
        } else if (runnable != null) {
            runnable.run();
        }
    }
}
