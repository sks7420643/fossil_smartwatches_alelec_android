package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I73 implements Xw2<H73> {
    @DexIgnore
    public static I73 c; // = new I73();
    @DexIgnore
    public /* final */ Xw2<H73> b;

    @DexIgnore
    public I73() {
        this(Ww2.b(new K73()));
    }

    @DexIgnore
    public I73(Xw2<H73> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((H73) c.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((H73) c.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ H73 zza() {
        return this.b.zza();
    }
}
