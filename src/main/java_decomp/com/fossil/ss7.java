package com.fossil;

import com.mapped.Coroutine;
import com.mapped.Wg6;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ss7<T1, T2, V> implements Ts7<V> {
    @DexIgnore
    public /* final */ Ts7<T1> a;
    @DexIgnore
    public /* final */ Ts7<T2> b;
    @DexIgnore
    public /* final */ Coroutine<T1, T2, V> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Iterator<V>, Jr7 {
        @DexIgnore
        public /* final */ Iterator<T1> b;
        @DexIgnore
        public /* final */ Iterator<T2> c;
        @DexIgnore
        public /* final */ /* synthetic */ Ss7 d;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(Ss7 ss7) {
            this.d = ss7;
            this.b = ss7.a.iterator();
            this.c = ss7.b.iterator();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext() && this.c.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public V next() {
            return (V) this.d.c.invoke(this.b.next(), this.c.next());
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.Ts7<? extends T1> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.Ts7<? extends T2> */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.mapped.Coroutine<? super T1, ? super T2, ? extends V> */
    /* JADX WARN: Multi-variable type inference failed */
    public Ss7(Ts7<? extends T1> ts7, Ts7<? extends T2> ts72, Coroutine<? super T1, ? super T2, ? extends V> coroutine) {
        Wg6.c(ts7, "sequence1");
        Wg6.c(ts72, "sequence2");
        Wg6.c(coroutine, "transform");
        this.a = ts7;
        this.b = ts72;
        this.c = coroutine;
    }

    @DexIgnore
    @Override // com.fossil.Ts7
    public Iterator<V> iterator() {
        return new Ai(this);
    }
}
