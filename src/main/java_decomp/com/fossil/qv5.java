package com.fossil;

import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import com.fossil.Jn5;
import com.mapped.AlertDialogFragment;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Qv5 extends BaseFragment implements AlertDialogFragment.Gi {
    @DexIgnore
    public HashMap g;

    /*
    static {
        Wg6.b(Qv5.class.getSimpleName(), "BasePermissionFragment::class.java.simpleName");
    }
    */

    @DexIgnore
    public final void M(Uh5... uh5Arr) {
        Wg6.c(uh5Arr, "permissionCodes");
        ArrayList<Uh5> arrayList = new ArrayList<>();
        for (Uh5 uh5 : uh5Arr) {
            arrayList.add(uh5);
        }
        Jn5.b.a(getContext(), arrayList, Hm7.i(Jn5.Ai.PAIR_DEVICE));
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i, Intent intent) {
        Wg6.c(str, "tag");
        if (getActivity() instanceof BaseActivity) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).R5(str, i, intent);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
