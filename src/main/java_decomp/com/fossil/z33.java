package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z33 extends X33<W33, W33> {
    @DexIgnore
    public static void m(Object obj, W33 w33) {
        ((E13) obj).zzb = w33;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.X33
    public final /* synthetic */ W33 a() {
        return W33.g();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, long] */
    @Override // com.fossil.X33
    public final /* synthetic */ void b(W33 w33, int i, long j) {
        w33.c(i << 3, Long.valueOf(j));
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, com.fossil.Xz2] */
    @Override // com.fossil.X33
    public final /* synthetic */ void c(W33 w33, int i, Xz2 xz2) {
        w33.c((i << 3) | 2, xz2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.R43] */
    @Override // com.fossil.X33
    public final /* synthetic */ void d(W33 w33, R43 r43) throws IOException {
        w33.h(r43);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.X33
    public final /* bridge */ /* synthetic */ void e(Object obj, W33 w33) {
        m(obj, w33);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.X33
    public final /* synthetic */ W33 f(Object obj) {
        return ((E13) obj).zzb;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.R43] */
    @Override // com.fossil.X33
    public final /* synthetic */ void g(W33 w33, R43 r43) throws IOException {
        w33.e(r43);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.X33
    public final /* synthetic */ void h(Object obj, W33 w33) {
        m(obj, w33);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.X33
    public final /* synthetic */ W33 i(W33 w33, W33 w332) {
        W33 w333 = w33;
        W33 w334 = w332;
        return w334.equals(W33.a()) ? w333 : W33.b(w333, w334);
    }

    @DexIgnore
    @Override // com.fossil.X33
    public final void j(Object obj) {
        ((E13) obj).zzb.i();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.X33
    public final /* synthetic */ int k(W33 w33) {
        return w33.j();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.X33
    public final /* synthetic */ int l(W33 w33) {
        return w33.k();
    }
}
