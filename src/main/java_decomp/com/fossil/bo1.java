package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Bo1 extends Ox1 implements Parcelable {
    @DexIgnore
    public /* final */ Do1 b;

    @DexIgnore
    public Bo1(Parcel parcel) {
        this(Do1.values()[parcel.readInt()]);
    }

    @DexIgnore
    public Bo1(Do1 do1) {
        this.b = do1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final Do1 getActionType() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(new JSONObject(), Jd0.e, this.b);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b.ordinal());
    }
}
