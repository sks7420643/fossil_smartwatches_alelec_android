package com.fossil;

import com.fossil.bu0;
import com.fossil.xt0;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yt0<Key, Value> extends vt0<Key, Value> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<Value> {
        @DexIgnore
        public abstract void a(List<Value> list);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Value> extends a<Value> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ xt0.d<Value> f4364a;

        @DexIgnore
        public b(yt0 yt0, int i, Executor executor, bu0.a<Value> aVar) {
            this.f4364a = new xt0.d<>(yt0, i, executor, aVar);
        }

        @DexIgnore
        @Override // com.fossil.yt0.a
        public void a(List<Value> list) {
            if (!this.f4364a.a()) {
                this.f4364a.b(new bu0<>(list, 0, 0, 0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<Value> extends a<Value> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<Value> extends c<Value> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ xt0.d<Value> f4365a;

        @DexIgnore
        public d(yt0 yt0, boolean z, bu0.a<Value> aVar) {
            this.f4365a = new xt0.d<>(yt0, 0, null, aVar);
        }

        @DexIgnore
        @Override // com.fossil.yt0.a
        public void a(List<Value> list) {
            if (!this.f4365a.a()) {
                this.f4365a.b(new bu0<>(list, 0, 0, 0));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<Key> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Key f4366a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public e(Key key, int i, boolean z) {
            this.f4366a = key;
            this.b = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f<Key> {

        @DexIgnore
        /* renamed from: a  reason: collision with root package name */
        public /* final */ Key f4367a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public f(Key key, int i) {
            this.f4367a = key;
            this.b = i;
        }
    }

    @DexIgnore
    @Override // com.fossil.vt0
    public final void dispatchLoadAfter(int i, Value value, int i2, Executor executor, bu0.a<Value> aVar) {
        loadAfter(new f<>(getKey(value), i2), new b(this, 1, executor, aVar));
    }

    @DexIgnore
    @Override // com.fossil.vt0
    public final void dispatchLoadBefore(int i, Value value, int i2, Executor executor, bu0.a<Value> aVar) {
        loadBefore(new f<>(getKey(value), i2), new b(this, 2, executor, aVar));
    }

    @DexIgnore
    @Override // com.fossil.vt0
    public final void dispatchLoadInitial(Key key, int i, int i2, boolean z, Executor executor, bu0.a<Value> aVar) {
        d dVar = new d(this, z, aVar);
        loadInitial(new e<>(key, i, z), dVar);
        dVar.f4365a.c(executor);
    }

    @DexIgnore
    @Override // com.fossil.vt0
    public final Key getKey(int i, Value value) {
        if (value == null) {
            return null;
        }
        return getKey(value);
    }

    @DexIgnore
    public abstract Key getKey(Value value);

    @DexIgnore
    public abstract void loadAfter(f<Key> fVar, a<Value> aVar);

    @DexIgnore
    public abstract void loadBefore(f<Key> fVar, a<Value> aVar);

    @DexIgnore
    public abstract void loadInitial(e<Key> eVar, c<Value> cVar);

    @DexIgnore
    @Override // com.fossil.xt0
    public final <ToValue> yt0<Key, ToValue> map(gi0<Value, ToValue> gi0) {
        return mapByPage((gi0) xt0.createListFunction(gi0));
    }

    @DexIgnore
    @Override // com.fossil.xt0
    public final <ToValue> yt0<Key, ToValue> mapByPage(gi0<List<Value>, List<ToValue>> gi0) {
        return new ju0(this, gi0);
    }
}
