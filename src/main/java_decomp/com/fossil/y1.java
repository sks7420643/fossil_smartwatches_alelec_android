package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Wg6;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Y1 extends C2 {
    @DexIgnore
    public static /* final */ W1 CREATOR; // = new W1(null);
    @DexIgnore
    public /* final */ Bo1 e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore
    public Y1(byte b, int i, Bo1 bo1) {
        super(Lt.f, b, false, 4);
        this.e = bo1;
        this.f = i;
    }

    @DexIgnore
    public /* synthetic */ Y1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        Parcelable readParcelable = parcel.readParcelable(Bo1.class.getClassLoader());
        if (readParcelable != null) {
            this.e = (Bo1) readParcelable;
            this.f = parcel.readInt();
            return;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.C2, com.fossil.Ox1
    public JSONObject toJSONObject() {
        return G80.k(G80.k(super.toJSONObject(), Jd0.v0, this.e.toJSONObject()), Jd0.d4, Integer.valueOf(this.f));
    }

    @DexIgnore
    @Override // com.fossil.C2
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.f);
        }
    }
}
