package com.fossil;

import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oy4 extends RecyclerView.g<Ai> {
    @DexIgnore
    public /* final */ List<Qt4> a; // = new ArrayList();
    @DexIgnore
    public /* final */ SparseArray<Ai> b; // = new SparseArray<>();
    @DexIgnore
    public /* final */ Ny5 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Df5 a;
        @DexIgnore
        public /* final */ Ny5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Qq7 implements Hg6<View, Cd6> {
            @DexIgnore
            public /* final */ /* synthetic */ Ps4 $challenge;
            @DexIgnore
            public /* final */ /* synthetic */ Qt4 $history$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ps4 ps4, Ai ai, Qt4 qt4) {
                super(1);
                this.$challenge = ps4;
                this.this$0 = ai;
                this.$history$inlined = qt4;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.mapped.Hg6
            public /* bridge */ /* synthetic */ Cd6 invoke(View view) {
                invoke(view);
                return Cd6.a;
            }

            @DexIgnore
            public final void invoke(View view) {
                this.this$0.a().e6(this.$challenge);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Df5 df5, Ny5 ny5) {
            super(df5.n());
            Wg6.c(df5, "binding");
            Wg6.c(ny5, "listener");
            this.a = df5;
            this.b = ny5;
        }

        @DexIgnore
        public final Ny5 a() {
            return this.b;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0090  */
        /* JADX WARNING: Removed duplicated region for block: B:76:0x0295  */
        /* JADX WARNING: Removed duplicated region for block: B:78:0x029e  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void b(com.fossil.Qt4 r15) {
            /*
            // Method dump skipped, instructions count: 700
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Oy4.Ai.b(com.fossil.Qt4):void");
        }
    }

    @DexIgnore
    public Oy4(Ny5 ny5) {
        Wg6.c(ny5, "listener");
        this.c = ny5;
    }

    @DexIgnore
    public void g(Ai ai, int i) {
        Wg6.c(ai, "holder");
        this.b.put(i, ai);
        ai.b(this.a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public Ai h(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        Df5 z = Df5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemHistoryChallengeLayo\u2026e(inflate, parent, false)");
        return new Ai(z, this.c);
    }

    @DexIgnore
    public final void i(List<Qt4> list) {
        Wg6.c(list, "newData");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Ai ai, int i) {
        g(ai, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Ai onCreateViewHolder(ViewGroup viewGroup, int i) {
        return h(viewGroup, i);
    }
}
