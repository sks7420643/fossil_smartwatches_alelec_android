package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Sg4 implements Runnable {
    @DexIgnore
    public /* final */ Tg4 b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public Sg4(Tg4 tg4, boolean z) {
        this.b = tg4;
        this.c = z;
    }

    @DexIgnore
    public static Runnable a(Tg4 tg4, boolean z) {
        return new Sg4(tg4, z);
    }

    @DexIgnore
    public void run() {
        this.b.g(this.c);
    }
}
