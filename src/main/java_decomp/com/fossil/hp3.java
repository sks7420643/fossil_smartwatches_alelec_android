package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hp3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ Fr3 c;
    @DexIgnore
    public /* final */ /* synthetic */ Or3 d;
    @DexIgnore
    public /* final */ /* synthetic */ Fp3 e;

    @DexIgnore
    public Hp3(Fp3 fp3, boolean z, Fr3 fr3, Or3 or3) {
        this.e = fp3;
        this.b = z;
        this.c = fr3;
        this.d = or3;
    }

    @DexIgnore
    public final void run() {
        Cl3 cl3 = this.e.d;
        if (cl3 == null) {
            this.e.d().F().a("Discarding data. Failed to set user property");
            return;
        }
        this.e.M(cl3, this.b ? null : this.c, this.d);
        this.e.e0();
    }
}
