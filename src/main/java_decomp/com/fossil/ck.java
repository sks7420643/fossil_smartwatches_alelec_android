package com.fossil;

import com.mapped.Cd6;
import com.mapped.Coroutine;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ck extends Qq7 implements Coroutine<Fs, Float, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Nm b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Ck(Nm nm) {
        super(2);
        this.b = nm;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public Cd6 invoke(Fs fs, Float f) {
        float floatValue = f.floatValue();
        Iterator<T> it = this.b.G.iterator();
        int i = 0;
        while (it.hasNext()) {
            i = ((int) it.next().g) + i;
        }
        float f2 = (((float) i) + (((float) this.b.E) * floatValue)) / ((float) this.b.E);
        Nm nm = this.b;
        if (f2 - nm.J > nm.N || f2 == 1.0f) {
            Nm nm2 = this.b;
            nm2.J = f2;
            nm2.d(f2);
        }
        return Cd6.a;
    }
}
