package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Ft3<TResult, TContinuationResult> {
    @DexIgnore
    TContinuationResult then(Nt3<TResult> nt3) throws Exception;
}
