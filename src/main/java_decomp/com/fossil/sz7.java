package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sz7 {
    @DexIgnore
    public /* final */ Lz7 a;

    @DexIgnore
    public Sz7(Lz7 lz7) {
        this.a = lz7;
    }

    @DexIgnore
    public String toString() {
        return "Removed[" + this.a + ']';
    }
}
