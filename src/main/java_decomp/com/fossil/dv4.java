package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.buddy_challenge.domain.ProfileRepository;
import com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dv4 implements Factory<BCCreateSocialProfileViewModel> {
    @DexIgnore
    public /* final */ Provider<ProfileRepository> a;
    @DexIgnore
    public /* final */ Provider<FCMRepository> b;
    @DexIgnore
    public /* final */ Provider<An4> c;

    @DexIgnore
    public Dv4(Provider<ProfileRepository> provider, Provider<FCMRepository> provider2, Provider<An4> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static Dv4 a(Provider<ProfileRepository> provider, Provider<FCMRepository> provider2, Provider<An4> provider3) {
        return new Dv4(provider, provider2, provider3);
    }

    @DexIgnore
    public static BCCreateSocialProfileViewModel c(ProfileRepository profileRepository, FCMRepository fCMRepository, An4 an4) {
        return new BCCreateSocialProfileViewModel(profileRepository, fCMRepository, an4);
    }

    @DexIgnore
    public BCCreateSocialProfileViewModel b() {
        return c(this.a.get(), this.b.get(), this.c.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
