package com.fossil;

import androidx.lifecycle.Lifecycle;
import com.mapped.Fd;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L61 extends Lifecycle {
    @DexIgnore
    public static /* final */ L61 b; // = new L61();

    @DexIgnore
    @Override // androidx.lifecycle.Lifecycle
    public void a(Fd fd) {
        Wg6.c(fd, "observer");
    }

    @DexIgnore
    @Override // androidx.lifecycle.Lifecycle
    public Lifecycle.State b() {
        return Lifecycle.State.RESUMED;
    }

    @DexIgnore
    @Override // androidx.lifecycle.Lifecycle
    public void c(Fd fd) {
        Wg6.c(fd, "observer");
    }
}
