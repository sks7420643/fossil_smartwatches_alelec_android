package com.fossil;

import androidx.lifecycle.LiveData;
import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Wg6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Or0 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Sr0>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ LiveData $source;
        @DexIgnore
        public /* final */ /* synthetic */ Js0 $this_addDisposableSource;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii<T> implements Ls0<S> {
            @DexIgnore
            public /* final */ /* synthetic */ Ai a;

            @DexIgnore
            public Aii(Ai ai) {
                this.a = ai;
            }

            @DexIgnore
            @Override // com.fossil.Ls0
            public final void onChanged(T t) {
                this.a.$this_addDisposableSource.o(t);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Js0 js0, LiveData liveData, Xe6 xe6) {
            super(2, xe6);
            this.$this_addDisposableSource = js0;
            this.$source = liveData;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.$this_addDisposableSource, this.$source, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Sr0> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.$this_addDisposableSource.p(this.$source, new Aii(this));
                return new Sr0(this.$source, this.$this_addDisposableSource);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public static final <T> Object a(Js0<T> js0, LiveData<T> liveData, Xe6<? super Sr0> xe6) {
        return Eu7.g(Bw7.c().S(), new Ai(js0, liveData, null), xe6);
    }

    @DexIgnore
    public static final <T> LiveData<T> b(Af6 af6, long j, Coroutine<? super Hs0<T>, ? super Xe6<? super Cd6>, ? extends Object> coroutine) {
        Wg6.c(af6, "context");
        Wg6.c(coroutine, "block");
        return new Nr0(af6, j, coroutine);
    }

    @DexIgnore
    public static /* synthetic */ LiveData c(Af6 af6, long j, Coroutine coroutine, int i, Object obj) {
        if ((i & 1) != 0) {
            af6 = Un7.INSTANCE;
        }
        if ((i & 2) != 0) {
            j = 5000;
        }
        return b(af6, j, coroutine);
    }
}
