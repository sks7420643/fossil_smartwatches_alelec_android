package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.fossil.A11;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class L11 implements A11 {
    @DexIgnore
    public /* final */ MutableLiveData<A11.Bi> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ J41<A11.Bi.Cii> d; // = J41.t();

    @DexIgnore
    public L11() {
        a(A11.b);
    }

    @DexIgnore
    public void a(A11.Bi bi) {
        this.c.l(bi);
        if (bi instanceof A11.Bi.Cii) {
            this.d.p((A11.Bi.Cii) bi);
        } else if (bi instanceof A11.Bi.Aii) {
            this.d.q(((A11.Bi.Aii) bi).a());
        }
    }
}
