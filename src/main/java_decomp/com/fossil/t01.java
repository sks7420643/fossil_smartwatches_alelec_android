package com.fossil;

import android.app.Notification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T01 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Notification c;

    @DexIgnore
    public T01(int i, Notification notification, int i2) {
        this.a = i;
        this.c = notification;
        this.b = i2;
    }

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public Notification b() {
        return this.c;
    }

    @DexIgnore
    public int c() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || T01.class != obj.getClass()) {
            return false;
        }
        T01 t01 = (T01) obj;
        if (this.a == t01.a && this.b == t01.b) {
            return this.c.equals(t01.c);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.a * 31) + this.b) * 31) + this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ForegroundInfo{mNotificationId=" + this.a + ", mForegroundServiceType=" + this.b + ", mNotification=" + this.c + '}';
    }
}
