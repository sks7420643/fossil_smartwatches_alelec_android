package com.fossil;

import android.app.Activity;
import com.google.android.gms.common.api.internal.LifecycleCallback;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class B82 extends Ra2 {
    @DexIgnore
    public /* final */ Aj0<G72<?>> g; // = new Aj0<>();
    @DexIgnore
    public L72 h;

    @DexIgnore
    public B82(O72 o72) {
        super(o72);
        this.b.b1("ConnectionlessLifecycleHelper", this);
    }

    @DexIgnore
    public static void q(Activity activity, L72 l72, G72<?> g72) {
        O72 c = LifecycleCallback.c(activity);
        B82 b82 = (B82) c.S2("ConnectionlessLifecycleHelper", B82.class);
        if (b82 == null) {
            b82 = new B82(c);
        }
        b82.h = l72;
        Rc2.l(g72, "ApiKey cannot be null");
        b82.g.add(g72);
        l72.l(b82);
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.LifecycleCallback
    public void h() {
        super.h();
        s();
    }

    @DexIgnore
    @Override // com.fossil.Ra2, com.google.android.gms.common.api.internal.LifecycleCallback
    public void j() {
        super.j();
        s();
    }

    @DexIgnore
    @Override // com.fossil.Ra2, com.google.android.gms.common.api.internal.LifecycleCallback
    public void k() {
        super.k();
        this.h.q(this);
    }

    @DexIgnore
    @Override // com.fossil.Ra2
    public final void m(Z52 z52, int i) {
        this.h.h(z52, i);
    }

    @DexIgnore
    @Override // com.fossil.Ra2
    public final void o() {
        this.h.D();
    }

    @DexIgnore
    public final Aj0<G72<?>> r() {
        return this.g;
    }

    @DexIgnore
    public final void s() {
        if (!this.g.isEmpty()) {
            this.h.l(this);
        }
    }
}
