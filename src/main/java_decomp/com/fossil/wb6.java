package com.fossil;

import com.mapped.HybridCustomizeEditPresenter;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wb6 implements MembersInjector<HybridCustomizeEditActivity> {
    @DexIgnore
    public static void a(HybridCustomizeEditActivity hybridCustomizeEditActivity, HybridCustomizeEditPresenter hybridCustomizeEditPresenter) {
        hybridCustomizeEditActivity.A = hybridCustomizeEditPresenter;
    }

    @DexIgnore
    public static void b(HybridCustomizeEditActivity hybridCustomizeEditActivity, Po4 po4) {
        hybridCustomizeEditActivity.B = po4;
    }
}
