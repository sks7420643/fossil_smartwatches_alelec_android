package com.fossil;

import android.os.Bundle;
import com.fossil.R62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wa2 implements R62.Bi, R62.Ci {
    @DexIgnore
    public /* final */ M62<?> b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public Va2 d;

    @DexIgnore
    public Wa2(M62<?> m62, boolean z) {
        this.b = m62;
        this.c = z;
    }

    @DexIgnore
    public final void a(Va2 va2) {
        this.d = va2;
    }

    @DexIgnore
    public final void b() {
        Rc2.l(this.d, "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
    }

    @DexIgnore
    @Override // com.fossil.K72
    public final void d(int i) {
        b();
        this.d.d(i);
    }

    @DexIgnore
    @Override // com.fossil.K72
    public final void e(Bundle bundle) {
        b();
        this.d.e(bundle);
    }

    @DexIgnore
    @Override // com.fossil.R72
    public final void n(Z52 z52) {
        b();
        this.d.i(z52, this.b, this.c);
    }
}
