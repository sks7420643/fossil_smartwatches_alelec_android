package com.fossil;

import com.fossil.G02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wz1 extends G02 {
    @DexIgnore
    public /* final */ H02 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Uy1<?> c;
    @DexIgnore
    public /* final */ Wy1<?, byte[]> d;
    @DexIgnore
    public /* final */ Ty1 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends G02.Ai {
        @DexIgnore
        public H02 a;
        @DexIgnore
        public String b;
        @DexIgnore
        public Uy1<?> c;
        @DexIgnore
        public Wy1<?, byte[]> d;
        @DexIgnore
        public Ty1 e;

        @DexIgnore
        @Override // com.fossil.G02.Ai
        public G02 a() {
            String str = "";
            if (this.a == null) {
                str = " transportContext";
            }
            if (this.b == null) {
                str = str + " transportName";
            }
            if (this.c == null) {
                str = str + " event";
            }
            if (this.d == null) {
                str = str + " transformer";
            }
            if (this.e == null) {
                str = str + " encoding";
            }
            if (str.isEmpty()) {
                return new Wz1(this.a, this.b, this.c, this.d, this.e);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.G02.Ai
        public G02.Ai b(Ty1 ty1) {
            if (ty1 != null) {
                this.e = ty1;
                return this;
            }
            throw new NullPointerException("Null encoding");
        }

        @DexIgnore
        @Override // com.fossil.G02.Ai
        public G02.Ai c(Uy1<?> uy1) {
            if (uy1 != null) {
                this.c = uy1;
                return this;
            }
            throw new NullPointerException("Null event");
        }

        @DexIgnore
        @Override // com.fossil.G02.Ai
        public G02.Ai d(Wy1<?, byte[]> wy1) {
            if (wy1 != null) {
                this.d = wy1;
                return this;
            }
            throw new NullPointerException("Null transformer");
        }

        @DexIgnore
        @Override // com.fossil.G02.Ai
        public G02.Ai e(H02 h02) {
            if (h02 != null) {
                this.a = h02;
                return this;
            }
            throw new NullPointerException("Null transportContext");
        }

        @DexIgnore
        @Override // com.fossil.G02.Ai
        public G02.Ai f(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null transportName");
        }
    }

    @DexIgnore
    public Wz1(H02 h02, String str, Uy1<?> uy1, Wy1<?, byte[]> wy1, Ty1 ty1) {
        this.a = h02;
        this.b = str;
        this.c = uy1;
        this.d = wy1;
        this.e = ty1;
    }

    @DexIgnore
    @Override // com.fossil.G02
    public Ty1 b() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.G02
    public Uy1<?> c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.G02
    public Wy1<?, byte[]> e() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof G02)) {
            return false;
        }
        G02 g02 = (G02) obj;
        return this.a.equals(g02.f()) && this.b.equals(g02.g()) && this.c.equals(g02.c()) && this.d.equals(g02.e()) && this.e.equals(g02.b());
    }

    @DexIgnore
    @Override // com.fossil.G02
    public H02 f() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.G02
    public String g() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return ((((((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode()) * 1000003) ^ this.e.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "SendRequest{transportContext=" + this.a + ", transportName=" + this.b + ", event=" + this.c + ", transformer=" + this.d + ", encoding=" + this.e + "}";
    }
}
