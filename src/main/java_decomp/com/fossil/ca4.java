package com.fossil;

import com.fossil.Ta4;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ca4 extends Ta4.Ci.Bii {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ta4.Ci.Bii.Aiii {
        @DexIgnore
        public String a;
        @DexIgnore
        public byte[] b;

        @DexIgnore
        @Override // com.fossil.Ta4.Ci.Bii.Aiii
        public Ta4.Ci.Bii a() {
            String str = "";
            if (this.a == null) {
                str = " filename";
            }
            if (this.b == null) {
                str = str + " contents";
            }
            if (str.isEmpty()) {
                return new Ca4(this.a, this.b);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Ci.Bii.Aiii
        public Ta4.Ci.Bii.Aiii b(byte[] bArr) {
            if (bArr != null) {
                this.b = bArr;
                return this;
            }
            throw new NullPointerException("Null contents");
        }

        @DexIgnore
        @Override // com.fossil.Ta4.Ci.Bii.Aiii
        public Ta4.Ci.Bii.Aiii c(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null filename");
        }
    }

    @DexIgnore
    public Ca4(String str, byte[] bArr) {
        this.a = str;
        this.b = bArr;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Ci.Bii
    public byte[] b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ta4.Ci.Bii
    public String c() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Ta4.Ci.Bii)) {
            return false;
        }
        Ta4.Ci.Bii bii = (Ta4.Ci.Bii) obj;
        if (this.a.equals(bii.c())) {
            if (Arrays.equals(this.b, bii instanceof Ca4 ? ((Ca4) bii).b : bii.b())) {
                z = true;
                return z;
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.b);
    }

    @DexIgnore
    public String toString() {
        return "File{filename=" + this.a + ", contents=" + Arrays.toString(this.b) + "}";
    }
}
