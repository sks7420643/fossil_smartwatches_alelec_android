package com.fossil;

import android.os.Bundle;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class D84 implements C84, B84 {
    @DexIgnore
    public /* final */ F84 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ TimeUnit c;
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public CountDownLatch e;

    @DexIgnore
    public D84(F84 f84, int i, TimeUnit timeUnit) {
        this.a = f84;
        this.b = i;
        this.c = timeUnit;
    }

    @DexIgnore
    @Override // com.fossil.B84
    public void a(String str, Bundle bundle) {
        synchronized (this.d) {
            X74.f().b("Logging Crashlytics event to Firebase");
            this.e = new CountDownLatch(1);
            this.a.a(str, bundle);
            X74.f().b("Awaiting app exception callback from FA...");
            try {
                if (this.e.await((long) this.b, this.c)) {
                    X74.f().b("App exception callback received from FA listener.");
                } else {
                    X74.f().b("Timeout exceeded while awaiting app exception callback from FA listener.");
                }
            } catch (InterruptedException e2) {
                X74.f().b("Interrupted while awaiting app exception callback from FA listener.");
            }
            this.e = null;
        }
    }

    @DexIgnore
    @Override // com.fossil.C84
    public void q(String str, Bundle bundle) {
        CountDownLatch countDownLatch = this.e;
        if (countDownLatch != null && "_ae".equals(str)) {
            countDownLatch.countDown();
        }
    }
}
