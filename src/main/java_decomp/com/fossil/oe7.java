package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oe7 extends Qe7 {
    @DexIgnore
    public Oe7(Context context) {
        super(context);
    }

    @DexIgnore
    @Override // com.fossil.Qe7
    public final void b(String str) {
        synchronized (this) {
            Log.i("MID", "write mid to sharedPreferences");
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.a).edit();
            edit.putString(Se7.h("4kU71lN96TJUomD1vOU9lgj9Tw=="), str);
            edit.commit();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qe7
    public final boolean c() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Qe7
    public final String d() {
        String string;
        synchronized (this) {
            Log.i("MID", "read mid from sharedPreferences");
            string = PreferenceManager.getDefaultSharedPreferences(this.a).getString(Se7.h("4kU71lN96TJUomD1vOU9lgj9Tw=="), null);
        }
        return string;
    }
}
