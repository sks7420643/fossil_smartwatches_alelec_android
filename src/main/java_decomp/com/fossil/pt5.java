package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pt5 implements Factory<SetVibrationStrengthUseCase> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<An4> b;

    @DexIgnore
    public Pt5(Provider<DeviceRepository> provider, Provider<An4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static Pt5 a(Provider<DeviceRepository> provider, Provider<An4> provider2) {
        return new Pt5(provider, provider2);
    }

    @DexIgnore
    public static SetVibrationStrengthUseCase c(DeviceRepository deviceRepository, An4 an4) {
        return new SetVibrationStrengthUseCase(deviceRepository, an4);
    }

    @DexIgnore
    public SetVibrationStrengthUseCase b() {
        return c(this.a.get(), this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
