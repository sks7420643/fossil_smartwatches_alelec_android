package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dg extends Qq7 implements Hg6<Fs, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Eh b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Dg(Eh eh) {
        super(1);
        this.b = eh;
    }

    @DexIgnore
    public final void a(Fs fs) {
        Qs qs = (Qs) fs;
        List d0 = Em7.d0(qs.A);
        List d02 = Em7.d0(qs.B);
        Pm7.N(d0, null, null, null, 0, null, Pf.b, 31, null);
        Pm7.N(d02, null, null, null, 0, null, Bf.b, 31, null);
        Fh fh = this.b.b;
        K5 k5 = fh.w;
        Ky1 ky1 = Ky1.DEBUG;
        String str = fh.a;
        fh.H.clear();
        CopyOnWriteArrayList<N6> copyOnWriteArrayList = fh.H;
        ArrayList arrayList = new ArrayList();
        for (Object obj : d02) {
            if (Fh.Q.contains((N6) obj)) {
                arrayList.add(obj);
            }
        }
        copyOnWriteArrayList.addAll(arrayList);
        this.b.b.U();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Cd6 invoke(Fs fs) {
        a(fs);
        return Cd6.a;
    }
}
