package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Su extends Mu {
    @DexIgnore
    public Se L; // = new Se(0, 0, 0);

    @DexIgnore
    public Su(K5 k5) {
        super(Fu.h, Hs.s, k5, 0, 8);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return Gy1.c(super.A(), this.L.toJSONObject());
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public JSONObject F(byte[] bArr) {
        this.E = true;
        JSONObject jSONObject = new JSONObject();
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        Se se = new Se(Hy1.n(order.getShort(0)), Hy1.n(order.getShort(2)), Hy1.n(order.getShort(4)));
        this.L = se;
        return Gy1.c(jSONObject, se.toJSONObject());
    }
}
