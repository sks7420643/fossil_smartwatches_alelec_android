package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.Uu0;
import com.fossil.Vu0;
import com.mapped.Zf;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Iv0<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> {
    @DexIgnore
    public /* final */ Vu0<T> mDiffer;
    @DexIgnore
    public /* final */ Vu0.Bi<T> mListener; // = new Ai();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Vu0.Bi<T> {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        @Override // com.fossil.Vu0.Bi
        public void a(List<T> list, List<T> list2) {
            Iv0.this.onCurrentListChanged(list, list2);
        }
    }

    @DexIgnore
    public Iv0(Uu0<T> uu0) {
        Vu0<T> vu0 = new Vu0<>(new Tu0(this), uu0);
        this.mDiffer = vu0;
        vu0.a(this.mListener);
    }

    @DexIgnore
    public Iv0(Zf.Di<T> di) {
        Vu0<T> vu0 = new Vu0<>(new Tu0(this), new Uu0.Ai(di).a());
        this.mDiffer = vu0;
        vu0.a(this.mListener);
    }

    @DexIgnore
    public List<T> getCurrentList() {
        return this.mDiffer.b();
    }

    @DexIgnore
    public T getItem(int i) {
        return this.mDiffer.b().get(i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.mDiffer.b().size();
    }

    @DexIgnore
    public void onCurrentListChanged(List<T> list, List<T> list2) {
    }

    @DexIgnore
    public void submitList(List<T> list) {
        this.mDiffer.e(list);
    }

    @DexIgnore
    public void submitList(List<T> list, Runnable runnable) {
        this.mDiffer.f(list, runnable);
    }
}
