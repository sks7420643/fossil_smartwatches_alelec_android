package com.fossil;

import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class S24<E> extends Y24<E> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ U24<?> collection;

        @DexIgnore
        public Ai(U24<?> u24) {
            this.collection = u24;
        }

        @DexIgnore
        public Object readResolve() {
            return this.collection.asList();
        }
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    @DexIgnore
    @Override // com.fossil.U24, com.fossil.Y24
    public boolean contains(Object obj) {
        return delegateCollection().contains(obj);
    }

    @DexIgnore
    public abstract U24<E> delegateCollection();

    @DexIgnore
    public boolean isEmpty() {
        return delegateCollection().isEmpty();
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean isPartialView() {
        return delegateCollection().isPartialView();
    }

    @DexIgnore
    public int size() {
        return delegateCollection().size();
    }

    @DexIgnore
    @Override // com.fossil.U24, com.fossil.Y24
    public Object writeReplace() {
        return new Ai(delegateCollection());
    }
}
