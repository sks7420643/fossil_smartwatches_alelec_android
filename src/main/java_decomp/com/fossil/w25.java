package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class W25 extends V25 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d Q; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray R;
    @DexIgnore
    public long P;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        R = sparseIntArray;
        sparseIntArray.put(2131362117, 1);
        R.put(2131362666, 2);
        R.put(2131363410, 3);
        R.put(2131362089, 4);
        R.put(2131362667, 5);
        R.put(2131362402, 6);
        R.put(2131362401, 7);
        R.put(2131362783, 8);
        R.put(2131362399, 9);
        R.put(2131362397, 10);
        R.put(2131362429, 11);
        R.put(2131362735, 12);
        R.put(2131362161, 13);
        R.put(2131361890, 14);
        R.put(2131362051, 15);
        R.put(2131362091, 16);
        R.put(2131362925, 17);
        R.put(2131362505, 18);
        R.put(2131362445, 19);
        R.put(2131362189, 20);
        R.put(2131362796, 21);
        R.put(2131363455, 22);
        R.put(2131362494, 23);
        R.put(2131363023, 24);
    }
    */

    @DexIgnore
    public W25(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 25, Q, R));
    }

    @DexIgnore
    public W25(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (AppBarLayout) objArr[14], (ConstraintLayout) objArr[15], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[16], (ConstraintLayout) objArr[1], (CoordinatorLayout) objArr[13], (OverviewDayChart) objArr[20], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[19], (FlexibleTextView) objArr[23], (FlexibleTextView) objArr[18], (RTLImageView) objArr[2], (RTLImageView) objArr[5], (RTLImageView) objArr[12], (View) objArr[8], (LinearLayout) objArr[21], (FlexibleProgressBar) objArr[17], (ConstraintLayout) objArr[0], (RecyclerView) objArr[24], (FlexibleTextView) objArr[3], (View) objArr[22]);
        this.P = -1;
        this.L.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.P = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.P != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.P = 1;
        }
        w();
    }
}
