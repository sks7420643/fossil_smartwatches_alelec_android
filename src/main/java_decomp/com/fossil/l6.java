package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L6 {
    @DexIgnore
    public /* synthetic */ L6(Qg6 qg6) {
    }

    @DexIgnore
    public final N6 a(byte b) {
        N6 n6;
        N6[] values = N6.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                n6 = null;
                break;
            }
            n6 = values[i];
            if (n6.c == b) {
                break;
            }
            i++;
        }
        return n6 != null ? n6 : N6.d;
    }
}
