package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O54 implements Closeable {
    @DexIgnore
    public static /* final */ Ci e; // = (Bi.c() ? Bi.a : Ai.a);
    @DexIgnore
    public /* final */ Ci b;
    @DexIgnore
    public /* final */ Deque<Closeable> c; // = new ArrayDeque(4);
    @DexIgnore
    public Throwable d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Ci {
        @DexIgnore
        public static /* final */ Ai a; // = new Ai();

        @DexIgnore
        @Override // com.fossil.O54.Ci
        public void a(Closeable closeable, Throwable th, Throwable th2) {
            Logger logger = N54.a;
            Level level = Level.WARNING;
            logger.log(level, "Suppressing exception thrown when closing " + closeable, th2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Ci {
        @DexIgnore
        public static /* final */ Bi a; // = new Bi();
        @DexIgnore
        public static /* final */ Method b; // = b();

        @DexIgnore
        public static Method b() {
            try {
                return Throwable.class.getMethod("addSuppressed", Throwable.class);
            } catch (Throwable th) {
                return null;
            }
        }

        @DexIgnore
        public static boolean c() {
            return b != null;
        }

        @DexIgnore
        @Override // com.fossil.O54.Ci
        public void a(Closeable closeable, Throwable th, Throwable th2) {
            if (th != th2) {
                try {
                    b.invoke(th, th2);
                } catch (Throwable th3) {
                    Ai.a.a(closeable, th, th2);
                }
            }
        }
    }

    @DexIgnore
    public interface Ci {
        @DexIgnore
        void a(Closeable closeable, Throwable th, Throwable th2);
    }

    @DexIgnore
    public O54(Ci ci) {
        I14.l(ci);
        this.b = ci;
    }

    @DexIgnore
    public static O54 a() {
        return new O54(e);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public <C extends Closeable> C b(C c2) {
        if (c2 != null) {
            this.c.addFirst(c2);
        }
        return c2;
    }

    @DexIgnore
    public RuntimeException c(Throwable th) throws IOException {
        I14.l(th);
        this.d = th;
        N14.g(th, IOException.class);
        throw new RuntimeException(th);
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        Throwable th = this.d;
        while (!this.c.isEmpty()) {
            Closeable removeFirst = this.c.removeFirst();
            try {
                removeFirst.close();
            } catch (Throwable th2) {
                if (th == null) {
                    th = th2;
                } else {
                    this.b.a(removeFirst, th, th2);
                }
            }
        }
        if (this.d == null && th != null) {
            N14.g(th, IOException.class);
            throw new AssertionError(th);
        }
    }
}
