package com.fossil;

import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M28 {
    @DexIgnore
    public /* final */ Set<X18> a; // = new LinkedHashSet();

    @DexIgnore
    public void a(X18 x18) {
        synchronized (this) {
            this.a.remove(x18);
        }
    }

    @DexIgnore
    public void b(X18 x18) {
        synchronized (this) {
            this.a.add(x18);
        }
    }

    @DexIgnore
    public boolean c(X18 x18) {
        boolean contains;
        synchronized (this) {
            contains = this.a.contains(x18);
        }
        return contains;
    }
}
