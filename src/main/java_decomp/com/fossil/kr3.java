package com.fossil;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.internal.Utility;
import com.j256.ormlite.field.FieldType;
import com.misfit.frameworks.common.constants.Constants;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;
import javax.security.auth.x500.X500Principal;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kr3 extends In3 {
    @DexIgnore
    public static /* final */ String[] g; // = {"firebase_", "google_", "ga_"};
    @DexIgnore
    public static /* final */ String[] h; // = {"_err"};
    @DexIgnore
    public SecureRandom c;
    @DexIgnore
    public /* final */ AtomicLong d; // = new AtomicLong(0);
    @DexIgnore
    public int e;
    @DexIgnore
    public Integer f; // = null;

    @DexIgnore
    public Kr3(Pm3 pm3) {
        super(pm3);
    }

    @DexIgnore
    public static boolean B0(String str) {
        return !TextUtils.isEmpty(str) && str.startsWith(LocaleConverter.LOCALE_DELIMITER);
    }

    @DexIgnore
    public static Bundle C(List<Fr3> list) {
        Bundle bundle = new Bundle();
        if (list == null) {
            return bundle;
        }
        for (Fr3 fr3 : list) {
            String str = fr3.f;
            if (str != null) {
                bundle.putString(fr3.c, str);
            } else {
                Long l = fr3.e;
                if (l != null) {
                    bundle.putLong(fr3.c, l.longValue());
                } else {
                    Double d2 = fr3.h;
                    if (d2 != null) {
                        bundle.putDouble(fr3.c, d2.doubleValue());
                    }
                }
            }
        }
        return bundle;
    }

    @DexIgnore
    public static boolean E0(String str) {
        for (String str2 : h) {
            if (str2.equals(str)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static String G(String str, int i, boolean z) {
        if (str == null) {
            return null;
        }
        if (str.codePointCount(0, str.length()) <= i) {
            return str;
        }
        if (z) {
            return String.valueOf(str.substring(0, str.offsetByCodePoints(0, i))).concat("...");
        }
        return null;
    }

    @DexIgnore
    public static MessageDigest I0() {
        for (int i = 0; i < 2; i++) {
            try {
                MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_MD5);
                if (instance != null) {
                    return instance;
                }
            } catch (NoSuchAlgorithmException e2) {
            }
        }
        return null;
    }

    @DexIgnore
    public static void J(Bundle bundle, int i, String str, String str2, Object obj) {
        if (s0(bundle, i)) {
            bundle.putString("_ev", G(str, 40, true));
            if (obj != null) {
                Rc2.k(bundle);
                if (obj == null) {
                    return;
                }
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    bundle.putLong("_el", (long) String.valueOf(obj).length());
                }
            }
        }
    }

    @DexIgnore
    public static boolean K0(String str) {
        Rc2.k(str);
        return str.matches("^(1:\\d+:android:[a-f0-9]+|ca-app-pub-.*)$");
    }

    @DexIgnore
    public static boolean X(Context context, boolean z) {
        Rc2.k(context);
        return Build.VERSION.SDK_INT >= 24 ? r0(context, "com.google.android.gms.measurement.AppMeasurementJobService") : r0(context, "com.google.android.gms.measurement.AppMeasurementService");
    }

    @DexIgnore
    public static boolean Y(Intent intent) {
        String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        return "android-app://com.google.android.googlequicksearchbox/https/www.google.com".equals(stringExtra) || "https://www.google.com".equals(stringExtra) || "android-app://com.google.appcrawler".equals(stringExtra);
    }

    @DexIgnore
    public static boolean Z(Bundle bundle, int i) {
        if (bundle.size() <= i) {
            return false;
        }
        int i2 = 0;
        for (String str : new TreeSet(bundle.keySet())) {
            i2++;
            if (i2 > i) {
                bundle.remove(str);
            }
        }
        return true;
    }

    @DexIgnore
    public static boolean a0(Boolean bool, Boolean bool2) {
        if (bool == null && bool2 == null) {
            return true;
        }
        if (bool == null) {
            return false;
        }
        return bool.equals(bool2);
    }

    @DexIgnore
    public static boolean b0(Object obj) {
        return (obj instanceof Parcelable[]) || (obj instanceof ArrayList) || (obj instanceof Bundle);
    }

    @DexIgnore
    public static boolean c0(String str) {
        Rc2.g(str);
        return str.charAt(0) != '_' || str.equals("_ep");
    }

    @DexIgnore
    public static boolean i0(String str, String str2, String str3, String str4) {
        boolean isEmpty = TextUtils.isEmpty(str);
        boolean isEmpty2 = TextUtils.isEmpty(str2);
        if (!isEmpty && !isEmpty2) {
            return !str.equals(str2);
        }
        if (isEmpty && isEmpty2) {
            return (TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) ? !TextUtils.isEmpty(str4) : !str3.equals(str4);
        }
        if (isEmpty || !isEmpty2) {
            return TextUtils.isEmpty(str3) || !str3.equals(str4);
        }
        if (TextUtils.isEmpty(str4)) {
            return false;
        }
        return TextUtils.isEmpty(str3) || !str3.equals(str4);
    }

    @DexIgnore
    public static boolean j0(String str, String[] strArr) {
        Rc2.k(strArr);
        for (String str2 : strArr) {
            if (z0(str, str2)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static boolean l0(List<String> list, List<String> list2) {
        if (list == null && list2 == null) {
            return true;
        }
        if (list == null) {
            return false;
        }
        return list.equals(list2);
    }

    @DexIgnore
    public static byte[] m0(Parcelable parcelable) {
        if (parcelable == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            parcelable.writeToParcel(obtain, 0);
            return obtain.marshall();
        } finally {
            obtain.recycle();
        }
    }

    @DexIgnore
    public static Bundle p0(Bundle bundle) {
        if (bundle == null) {
            return new Bundle();
        }
        Bundle bundle2 = new Bundle(bundle);
        for (String str : bundle2.keySet()) {
            Object obj = bundle2.get(str);
            if (obj instanceof Bundle) {
                bundle2.putBundle(str, new Bundle((Bundle) obj));
            } else if (obj instanceof Parcelable[]) {
                Parcelable[] parcelableArr = (Parcelable[]) obj;
                for (int i = 0; i < parcelableArr.length; i++) {
                    if (parcelableArr[i] instanceof Bundle) {
                        parcelableArr[i] = new Bundle((Bundle) parcelableArr[i]);
                    }
                }
            } else if (obj instanceof List) {
                List list = (List) obj;
                for (int i2 = 0; i2 < list.size(); i2++) {
                    Object obj2 = list.get(i2);
                    if (obj2 instanceof Bundle) {
                        list.set(i2, new Bundle((Bundle) obj2));
                    }
                }
            }
        }
        return bundle2;
    }

    @DexIgnore
    public static ArrayList<Bundle> q0(List<Xr3> list) {
        if (list == null) {
            return new ArrayList<>(0);
        }
        ArrayList<Bundle> arrayList = new ArrayList<>(list.size());
        for (Xr3 xr3 : list) {
            Bundle bundle = new Bundle();
            bundle.putString("app_id", xr3.b);
            bundle.putString("origin", xr3.c);
            bundle.putLong("creation_timestamp", xr3.e);
            bundle.putString("name", xr3.d.c);
            Kn3.b(bundle, xr3.d.c());
            bundle.putBoolean("active", xr3.f);
            String str = xr3.g;
            if (str != null) {
                bundle.putString("trigger_event_name", str);
            }
            Vg3 vg3 = xr3.h;
            if (vg3 != null) {
                bundle.putString("timed_out_event_name", vg3.b);
                Ug3 ug3 = xr3.h.c;
                if (ug3 != null) {
                    bundle.putBundle("timed_out_event_params", ug3.k());
                }
            }
            bundle.putLong("trigger_timeout", xr3.i);
            Vg3 vg32 = xr3.j;
            if (vg32 != null) {
                bundle.putString("triggered_event_name", vg32.b);
                Ug3 ug32 = xr3.j.c;
                if (ug32 != null) {
                    bundle.putBundle("triggered_event_params", ug32.k());
                }
            }
            bundle.putLong("triggered_timestamp", xr3.d.d);
            bundle.putLong("time_to_live", xr3.k);
            Vg3 vg33 = xr3.l;
            if (vg33 != null) {
                bundle.putString("expired_event_name", vg33.b);
                Ug3 ug33 = xr3.l.c;
                if (ug33 != null) {
                    bundle.putBundle("expired_event_params", ug33.k());
                }
            }
            arrayList.add(bundle);
        }
        return arrayList;
    }

    @DexIgnore
    public static boolean r0(Context context, String str) {
        ServiceInfo serviceInfo;
        try {
            PackageManager packageManager = context.getPackageManager();
            return (packageManager == null || (serviceInfo = packageManager.getServiceInfo(new ComponentName(context, str), 0)) == null || !serviceInfo.enabled) ? false : true;
        } catch (PackageManager.NameNotFoundException e2) {
            return false;
        }
    }

    @DexIgnore
    public static boolean s0(Bundle bundle, int i) {
        if (bundle == null || bundle.getLong("_err") != 0) {
            return false;
        }
        bundle.putLong("_err", (long) i);
        return true;
    }

    @DexIgnore
    public static long v(long j, long j2) {
        return ((60000 * j2) + j) / LogBuilder.MAX_INTERVAL;
    }

    @DexIgnore
    public static Bundle[] v0(Object obj) {
        if (obj instanceof Bundle) {
            return new Bundle[]{(Bundle) obj};
        } else if (obj instanceof Parcelable[]) {
            Parcelable[] parcelableArr = (Parcelable[]) obj;
            return (Bundle[]) Arrays.copyOf(parcelableArr, parcelableArr.length, Bundle[].class);
        } else if (!(obj instanceof ArrayList)) {
            return null;
        } else {
            ArrayList arrayList = (ArrayList) obj;
            return (Bundle[]) arrayList.toArray(new Bundle[arrayList.size()]);
        }
    }

    @DexIgnore
    public static long x(Ug3 ug3) {
        if (ug3 == null) {
            return 0;
        }
        Iterator<String> it = ug3.iterator();
        long j = 0;
        while (it.hasNext()) {
            Object h2 = ug3.h(it.next());
            if (h2 instanceof Parcelable[]) {
                j = ((long) ((Parcelable[]) h2).length) + j;
            }
        }
        return j;
    }

    @DexIgnore
    public static long y(byte[] bArr) {
        Rc2.k(bArr);
        Rc2.n(bArr.length > 0);
        long j = 0;
        int i = 0;
        int length = bArr.length - 1;
        while (length >= 0 && length >= bArr.length - 8) {
            i += 8;
            length--;
            j = ((((long) bArr[length]) & 255) << i) + j;
        }
        return j;
    }

    @DexIgnore
    public static boolean z0(String str, String str2) {
        if (str == null && str2 == null) {
            return true;
        }
        if (str == null) {
            return false;
        }
        return str.equals(str2);
    }

    @DexIgnore
    public final Bundle A(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        if (bundle != null) {
            for (String str : bundle.keySet()) {
                Object F = F(str, bundle.get(str));
                if (F == null) {
                    d().K().b("Param value can't be null", j().y(str));
                } else {
                    M(bundle2, str, F);
                }
            }
        }
        return bundle2;
    }

    @DexIgnore
    public final boolean A0(String str) {
        h();
        if (Ag2.a(e()).a(str) == 0) {
            return true;
        }
        d().M().b("Permission not granted", str);
        return false;
    }

    @DexIgnore
    public final Bundle B(String str, String str2, Bundle bundle, List<String> list, boolean z, boolean z2) {
        int i;
        boolean z3 = S53.a() && m().s(Xg3.I0);
        boolean j0 = z3 ? j0(str2, On3.c) : z2;
        Bundle bundle2 = null;
        if (bundle != null) {
            bundle2 = new Bundle(bundle);
            int z4 = m().z();
            Iterator<String> it = (m().B(str, Xg3.b0) ? new TreeSet<>(bundle.keySet()) : bundle.keySet()).iterator();
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (!it.hasNext()) {
                    break;
                }
                String next = it.next();
                if (list == null || !list.contains(next)) {
                    int F0 = z ? F0(next) : 0;
                    i = F0 == 0 ? H0(next) : F0;
                } else {
                    i = 0;
                }
                if (i != 0) {
                    J(bundle2, i, next, next, i == 3 ? next : null);
                    bundle2.remove(next);
                } else {
                    int u = u(str, str2, next, bundle.get(next), bundle2, list, z, j0);
                    if (z3 && u == 17) {
                        J(bundle2, u, next, next, Boolean.FALSE);
                    } else if (u != 0 && !"_ev".equals(next)) {
                        J(bundle2, u, u == 21 ? str2 : next, next, bundle.get(next));
                        bundle2.remove(next);
                    }
                    if (c0(next) && (i3 = i3 + 1) > z4) {
                        StringBuilder sb = new StringBuilder(48);
                        sb.append("Event can't contain more than ");
                        sb.append(z4);
                        sb.append(" params");
                        d().H().c(sb.toString(), j().v(str2), j().t(bundle));
                        s0(bundle2, 5);
                        bundle2.remove(next);
                    }
                }
                i2 = i3;
            }
        }
        return bundle2;
    }

    @DexIgnore
    public final boolean C0(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String O = m().O();
        b();
        return O.equals(str);
    }

    @DexIgnore
    public final Vg3 D(String str, String str2, Bundle bundle, String str3, long j, boolean z, boolean z2) {
        Bundle bundle2;
        if (TextUtils.isEmpty(str2)) {
            return null;
        }
        if (n0(str2) == 0) {
            if (bundle == null) {
                bundle2 = new Bundle();
            }
            bundle2.putString("_o", str3);
            return new Vg3(str2, new Ug3(A(B(str, str2, bundle2, Ff2.c("_o"), false, false))), str3, j);
        }
        d().F().b("Invalid conditional property event name", j().z(str2));
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final long D0() {
        long andIncrement;
        long nextLong;
        long j;
        if (this.d.get() == 0) {
            synchronized (this.d) {
                nextLong = new Random(System.nanoTime() ^ zzm().b()).nextLong();
                int i = this.e + 1;
                this.e = i;
                j = (long) i;
            }
            return nextLong + j;
        }
        synchronized (this.d) {
            this.d.compareAndSet(-1, 1);
            andIncrement = this.d.getAndIncrement();
        }
        return andIncrement;
    }

    @DexIgnore
    public final Object E(int i, Object obj, boolean z, boolean z2) {
        Bundle A;
        if (obj == null) {
            return null;
        }
        if ((obj instanceof Long) || (obj instanceof Double)) {
            return obj;
        }
        if (obj instanceof Integer) {
            return Long.valueOf((long) ((Integer) obj).intValue());
        }
        if (obj instanceof Byte) {
            return Long.valueOf((long) ((Byte) obj).byteValue());
        }
        if (obj instanceof Short) {
            return Long.valueOf((long) ((Short) obj).shortValue());
        }
        if (obj instanceof Boolean) {
            return Long.valueOf(((Boolean) obj).booleanValue() ? 1 : 0);
        } else if (obj instanceof Float) {
            return Double.valueOf(((Float) obj).doubleValue());
        } else {
            if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
                return G(String.valueOf(obj), i, z);
            }
            if (!S53.a() || !m().s(Xg3.H0) || !m().s(Xg3.G0) || !z2 || (!(obj instanceof Bundle[]) && !(obj instanceof Parcelable[]))) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            Parcelable[] parcelableArr = (Parcelable[]) obj;
            for (Parcelable parcelable : parcelableArr) {
                if ((parcelable instanceof Bundle) && (A = A((Bundle) parcelable)) != null && !A.isEmpty()) {
                    arrayList.add(A);
                }
            }
            return arrayList.toArray(new Bundle[arrayList.size()]);
        }
    }

    @DexIgnore
    public final Object F(String str, Object obj) {
        int i = 256;
        if ("_ev".equals(str)) {
            return E(256, obj, true, true);
        }
        if (!B0(str)) {
            i = 100;
        }
        return E(i, obj, false, true);
    }

    @DexIgnore
    public final int F0(String str) {
        if (!f0("event param", str)) {
            return 3;
        }
        if (!k0("event param", null, str)) {
            return 14;
        }
        return e0("event param", 40, str) ? 0 : 3;
    }

    @DexIgnore
    public final SecureRandom G0() {
        h();
        if (this.c == null) {
            this.c = new SecureRandom();
        }
        return this.c;
    }

    @DexIgnore
    public final URL H(long j, String str, String str2, long j2) {
        try {
            Rc2.g(str2);
            Rc2.g(str);
            String format = String.format("https://www.googleadservices.com/pagead/conversion/app/deeplink?id_type=adid&sdk_version=%s&rdid=%s&bundleid=%s&retry=%s", String.format("v%s.%s", Long.valueOf(j), Integer.valueOf(J0())), str2, str, Long.valueOf(j2));
            return new URL(str.equals(m().P()) ? format.concat("&ddl_test=1") : format);
        } catch (IllegalArgumentException | MalformedURLException e2) {
            d().F().b("Failed to create BOW URL for Deferred Deep Link. exception", e2.getMessage());
            return null;
        }
    }

    @DexIgnore
    public final int H0(String str) {
        if (!t0("event param", str)) {
            return 3;
        }
        if (!k0("event param", null, str)) {
            return 14;
        }
        return e0("event param", 40, str) ? 0 : 3;
    }

    @DexIgnore
    public final void I(int i, String str, String str2, int i2) {
        V(null, i, str, str2, i2);
    }

    @DexIgnore
    public final int J0() {
        if (this.f == null) {
            this.f = Integer.valueOf(D62.h().b(e()) / 1000);
        }
        return this.f.intValue();
    }

    @DexIgnore
    public final void K(Bundle bundle, long j) {
        long j2 = bundle.getLong("_et");
        if (j2 != 0) {
            d().I().b("Params already contained engagement", Long.valueOf(j2));
        }
        bundle.putLong("_et", j2 + j);
    }

    @DexIgnore
    public final void L(Bundle bundle, Bundle bundle2) {
        if (bundle2 != null) {
            for (String str : bundle2.keySet()) {
                if (!bundle.containsKey(str)) {
                    k().M(bundle, str, bundle2.get(str));
                }
            }
        }
    }

    @DexIgnore
    public final int L0(String str) {
        if ("_ldl".equals(str)) {
            return 2048;
        }
        if (FieldType.FOREIGN_ID_FIELD_SUFFIX.equals(str)) {
            return 256;
        }
        return (!m().s(Xg3.l0) || !"_lgclid".equals(str)) ? 36 : 100;
    }

    @DexIgnore
    public final void M(Bundle bundle, String str, Object obj) {
        if (bundle != null) {
            if (obj instanceof Long) {
                bundle.putLong(str, ((Long) obj).longValue());
            } else if (obj instanceof String) {
                bundle.putString(str, String.valueOf(obj));
            } else if (obj instanceof Double) {
                bundle.putDouble(str, ((Double) obj).doubleValue());
            } else if (S53.a() && m().s(Xg3.H0) && m().s(Xg3.G0) && (obj instanceof Bundle[])) {
                bundle.putParcelableArray(str, (Bundle[]) obj);
            } else if (str != null) {
                d().K().c("Not putting event parameter. Invalid value type. name, type", j().y(str), obj != null ? obj.getClass().getSimpleName() : null);
            }
        }
    }

    @DexIgnore
    public final String M0() {
        byte[] bArr = new byte[16];
        G0().nextBytes(bArr);
        return String.format(Locale.US, "%032x", new BigInteger(1, bArr));
    }

    @DexIgnore
    public final void N(U93 u93, int i) {
        Bundle bundle = new Bundle();
        bundle.putInt("r", i);
        try {
            u93.c(bundle);
        } catch (RemoteException e2) {
            this.a.d().I().b("Error returning int value to wrapper", e2);
        }
    }

    @DexIgnore
    public final boolean N0() {
        try {
            e().getClassLoader().loadClass("com.google.firebase.remoteconfig.FirebaseRemoteConfig");
            return true;
        } catch (ClassNotFoundException e2) {
            return false;
        }
    }

    @DexIgnore
    public final void O(U93 u93, long j) {
        Bundle bundle = new Bundle();
        bundle.putLong("r", j);
        try {
            u93.c(bundle);
        } catch (RemoteException e2) {
            this.a.d().I().b("Error returning long value to wrapper", e2);
        }
    }

    @DexIgnore
    public final void P(U93 u93, Bundle bundle) {
        try {
            u93.c(bundle);
        } catch (RemoteException e2) {
            this.a.d().I().b("Error returning bundle value to wrapper", e2);
        }
    }

    @DexIgnore
    public final void Q(U93 u93, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("r", str);
        try {
            u93.c(bundle);
        } catch (RemoteException e2) {
            this.a.d().I().b("Error returning string value to wrapper", e2);
        }
    }

    @DexIgnore
    public final void R(U93 u93, ArrayList<Bundle> arrayList) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("r", arrayList);
        try {
            u93.c(bundle);
        } catch (RemoteException e2) {
            this.a.d().I().b("Error returning bundle list to wrapper", e2);
        }
    }

    @DexIgnore
    public final void S(U93 u93, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("r", z);
        try {
            u93.c(bundle);
        } catch (RemoteException e2) {
            this.a.d().I().b("Error returning boolean value to wrapper", e2);
        }
    }

    @DexIgnore
    public final void T(U93 u93, byte[] bArr) {
        Bundle bundle = new Bundle();
        bundle.putByteArray("r", bArr);
        try {
            u93.c(bundle);
        } catch (RemoteException e2) {
            this.a.d().I().b("Error returning byte array to wrapper", e2);
        }
    }

    @DexIgnore
    public final void U(Pl3 pl3, int i) {
        int i2 = 0;
        for (String str : new TreeSet(pl3.d.keySet())) {
            if (c0(str) && (i2 = i2 + 1) > i) {
                StringBuilder sb = new StringBuilder(48);
                sb.append("Event can't contain more than ");
                sb.append(i);
                sb.append(" params");
                d().H().c(sb.toString(), j().v(pl3.a), j().t(pl3.d));
                s0(pl3.d, 5);
                pl3.d.remove(str);
            }
        }
    }

    @DexIgnore
    public final void V(String str, int i, String str2, String str3, int i2) {
        Bundle bundle = new Bundle();
        s0(bundle, i);
        if (!TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str3)) {
            bundle.putString(str2, str3);
        }
        if (i == 6 || i == 7 || i == 2) {
            bundle.putLong("_el", (long) i2);
        }
        this.a.b();
        this.a.E().Q("auto", "_err", bundle);
    }

    @DexIgnore
    public final void W(String str, String str2, String str3, Bundle bundle, List<String> list, boolean z) {
        int i;
        int u;
        String sb;
        if (bundle != null) {
            boolean s = m().s(Xg3.I0);
            int z2 = s ? 0 : m().z();
            int i2 = 0;
            for (String str4 : new TreeSet(bundle.keySet())) {
                if (list == null || !list.contains(str4)) {
                    int F0 = z ? F0(str4) : 0;
                    i = F0 == 0 ? H0(str4) : F0;
                } else {
                    i = 0;
                }
                if (i != 0) {
                    J(bundle, i, str4, str4, i == 3 ? str4 : null);
                    bundle.remove(str4);
                } else {
                    if (b0(bundle.get(str4))) {
                        d().K().d("Nested Bundle parameters are not allowed; discarded. event name, param name, child param name", str2, str3, str4);
                        u = 22;
                    } else {
                        u = u(str, str2, str4, bundle.get(str4), bundle, list, z, false);
                    }
                    if (u != 0 && !"_ev".equals(str4)) {
                        J(bundle, u, str4, str4, bundle.get(str4));
                        bundle.remove(str4);
                    } else if (c0(str4) && (!s || !j0(str4, Nn3.d))) {
                        int i3 = i2 + 1;
                        if (i3 > z2) {
                            if (s) {
                                sb = "Item cannot contain custom parameters";
                            } else {
                                StringBuilder sb2 = new StringBuilder(63);
                                sb2.append("Child bundles can't contain more than ");
                                sb2.append(z2);
                                sb2.append(" custom params");
                                sb = sb2.toString();
                            }
                            d().H().c(sb, j().v(str2), j().t(bundle));
                            s0(bundle, s ? 23 : 5);
                            bundle.remove(str4);
                        }
                        i2 = i3;
                    }
                }
            }
        }
    }

    @DexIgnore
    @SuppressLint({"ApplySharedPref"})
    public final boolean d0(String str, double d2) {
        try {
            SharedPreferences.Editor edit = e().getSharedPreferences("google.analytics.deferred.deeplink.prefs", 0).edit();
            edit.putString("deeplink", str);
            edit.putLong("timestamp", Double.doubleToRawLongBits(d2));
            return edit.commit();
        } catch (Exception e2) {
            d().F().b("Failed to persist Deferred Deep Link. exception", e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean e0(String str, int i, String str2) {
        if (str2 == null) {
            d().H().b("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.codePointCount(0, str2.length()) <= i) {
            return true;
        } else {
            d().H().d("Name is too long. Type, maximum supported length, name", str, Integer.valueOf(i), str2);
            return false;
        }
    }

    @DexIgnore
    public final boolean f0(String str, String str2) {
        if (str2 == null) {
            d().H().b("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            d().H().b("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (!Character.isLetter(codePointAt)) {
                d().H().c("Name must start with a letter. Type, name", str, str2);
                return false;
            }
            int length = str2.length();
            int charCount = Character.charCount(codePointAt);
            while (charCount < length) {
                int codePointAt2 = str2.codePointAt(charCount);
                if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                    charCount += Character.charCount(codePointAt2);
                } else {
                    d().H().c("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                    return false;
                }
            }
            return true;
        }
    }

    @DexIgnore
    public final boolean g0(String str, String str2, int i, Object obj) {
        if (obj == null || (obj instanceof Long) || (obj instanceof Float) || (obj instanceof Integer) || (obj instanceof Byte) || (obj instanceof Short) || (obj instanceof Boolean) || (obj instanceof Double)) {
            return true;
        }
        if (!(obj instanceof String) && !(obj instanceof Character) && !(obj instanceof CharSequence)) {
            return false;
        }
        String valueOf = String.valueOf(obj);
        if (valueOf.codePointCount(0, valueOf.length()) <= i) {
            return true;
        }
        d().K().d("Value is too long; discarded. Value kind, name, value length", str, str2, Integer.valueOf(valueOf.length()));
        return false;
    }

    @DexIgnore
    public final boolean h0(String str, String str2, String str3) {
        if (!TextUtils.isEmpty(str)) {
            if (!K0(str)) {
                if (!this.a.I()) {
                    return false;
                }
                d().H().b("Invalid google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI. provided id", Kl3.w(str));
                return false;
            }
        } else if (!I73.a() || !m().s(Xg3.o0) || TextUtils.isEmpty(str3)) {
            if (!TextUtils.isEmpty(str2)) {
                if (!K0(str2)) {
                    d().H().b("Invalid admob_app_id. Analytics disabled.", Kl3.w(str2));
                    return false;
                }
            } else if (!this.a.I()) {
                return false;
            } else {
                d().H().a("Missing google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI");
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final boolean k0(String str, String[] strArr, String str2) {
        boolean z;
        if (str2 == null) {
            d().H().b("Name is required and can't be null. Type", str);
            return false;
        }
        Rc2.k(str2);
        String[] strArr2 = g;
        int length = strArr2.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (str2.startsWith(strArr2[i])) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            d().H().c("Name starts with reserved prefix. Type, name", str, str2);
            return false;
        } else if (strArr == null || !j0(str2, strArr)) {
            return true;
        } else {
            d().H().c("Name is reserved. Type, name", str, str2);
            return false;
        }
    }

    @DexIgnore
    @Override // com.fossil.In3
    public final void n() {
        h();
        SecureRandom secureRandom = new SecureRandom();
        long nextLong = secureRandom.nextLong();
        if (nextLong == 0) {
            nextLong = secureRandom.nextLong();
            if (nextLong == 0) {
                d().I().a("Utils falling back to Random for random id");
            }
        }
        this.d.set(nextLong);
    }

    @DexIgnore
    public final int n0(String str) {
        if (!t0(Constants.EVENT, str)) {
            return 2;
        }
        if (!k0(Constants.EVENT, On3.a, str)) {
            return 13;
        }
        return e0(Constants.EVENT, 40, str) ? 0 : 2;
    }

    @DexIgnore
    public final int o0(String str, Object obj) {
        return "_ldl".equals(str) ? g0("user property referrer", str, L0(str), obj) : g0("user property", str, L0(str), obj) ? 0 : 7;
    }

    @DexIgnore
    @Override // com.fossil.In3
    public final boolean r() {
        return true;
    }

    @DexIgnore
    public final int t(int i) {
        return D62.h().j(e(), H62.a);
    }

    @DexIgnore
    public final boolean t0(String str, String str2) {
        if (str2 == null) {
            d().H().b("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            d().H().b("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (Character.isLetter(codePointAt) || codePointAt == 95) {
                int length = str2.length();
                int charCount = Character.charCount(codePointAt);
                while (charCount < length) {
                    int codePointAt2 = str2.codePointAt(charCount);
                    if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                        charCount += Character.charCount(codePointAt2);
                    } else {
                        d().H().c("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                        return false;
                    }
                }
                return true;
            }
            d().H().c("Name must start with a letter or _ (underscore). Type, name", str, str2);
            return false;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:86:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int u(java.lang.String r15, java.lang.String r16, java.lang.String r17, java.lang.Object r18, android.os.Bundle r19, java.util.List<java.lang.String> r20, boolean r21, boolean r22) {
        /*
        // Method dump skipped, instructions count: 405
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Kr3.u(java.lang.String, java.lang.String, java.lang.String, java.lang.Object, android.os.Bundle, java.util.List, boolean, boolean):int");
    }

    @DexIgnore
    public final boolean u0(String str, String str2, int i, Object obj) {
        int size;
        if (obj instanceof Parcelable[]) {
            size = ((Parcelable[]) obj).length;
        } else {
            if (obj instanceof ArrayList) {
                size = ((ArrayList) obj).size();
            }
            return true;
        }
        if (size > i) {
            d().K().d("Parameter array is too long; discarded. Value kind, name, array length", str, str2, Integer.valueOf(size));
            return false;
        }
        return true;
    }

    @DexIgnore
    public final long w(Context context, String str) {
        h();
        Rc2.k(context);
        Rc2.g(str);
        PackageManager packageManager = context.getPackageManager();
        MessageDigest I0 = I0();
        if (I0 == null) {
            d().F().a("Could not get MD5 instance");
            return -1;
        }
        if (packageManager != null) {
            try {
                if (!y0(context, str)) {
                    PackageInfo e2 = Ag2.a(context).e(e().getPackageName(), 64);
                    if (e2.signatures != null && e2.signatures.length > 0) {
                        return y(I0.digest(e2.signatures[0].toByteArray()));
                    }
                    d().I().a("Could not get signatures");
                    return -1;
                }
            } catch (PackageManager.NameNotFoundException e3) {
                d().F().b("Package name not found", e3);
            }
        }
        return 0;
    }

    @DexIgnore
    public final int w0(String str) {
        if (!t0("user property", str)) {
            return 6;
        }
        if (!k0("user property", Qn3.a, str)) {
            return 15;
        }
        return e0("user property", 24, str) ? 0 : 6;
    }

    @DexIgnore
    public final Object x0(String str, Object obj) {
        return "_ldl".equals(str) ? E(L0(str), obj, true, false) : E(L0(str), obj, false, false);
    }

    @DexIgnore
    public final boolean y0(Context context, String str) {
        X500Principal x500Principal = new X500Principal("CN=Android Debug,O=Android,C=US");
        try {
            PackageInfo e2 = Ag2.a(context).e(str, 64);
            if (!(e2 == null || e2.signatures == null || e2.signatures.length <= 0)) {
                return ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(e2.signatures[0].toByteArray()))).getSubjectX500Principal().equals(x500Principal);
            }
        } catch (CertificateException e3) {
            d().F().b("Error obtaining certificate", e3);
        } catch (PackageManager.NameNotFoundException e4) {
            d().F().b("Package name not found", e4);
        }
        return true;
    }

    @DexIgnore
    public final Bundle z(Uri uri) {
        String str;
        String str2;
        String str3;
        String str4;
        Bundle bundle = null;
        if (uri != null) {
            try {
                if (uri.isHierarchical()) {
                    str4 = uri.getQueryParameter("utm_campaign");
                    str3 = uri.getQueryParameter("utm_source");
                    str2 = uri.getQueryParameter("utm_medium");
                    str = uri.getQueryParameter("gclid");
                } else {
                    str = null;
                    str2 = null;
                    str3 = null;
                    str4 = null;
                }
                if (!TextUtils.isEmpty(str4) || !TextUtils.isEmpty(str3) || !TextUtils.isEmpty(str2) || !TextUtils.isEmpty(str)) {
                    bundle = new Bundle();
                    if (!TextUtils.isEmpty(str4)) {
                        bundle.putString(AppEventsLogger.PUSH_PAYLOAD_CAMPAIGN_KEY, str4);
                    }
                    if (!TextUtils.isEmpty(str3)) {
                        bundle.putString("source", str3);
                    }
                    if (!TextUtils.isEmpty(str2)) {
                        bundle.putString("medium", str2);
                    }
                    if (!TextUtils.isEmpty(str)) {
                        bundle.putString("gclid", str);
                    }
                    String queryParameter = uri.getQueryParameter("utm_term");
                    if (!TextUtils.isEmpty(queryParameter)) {
                        bundle.putString("term", queryParameter);
                    }
                    String queryParameter2 = uri.getQueryParameter("utm_content");
                    if (!TextUtils.isEmpty(queryParameter2)) {
                        bundle.putString("content", queryParameter2);
                    }
                    String queryParameter3 = uri.getQueryParameter("aclid");
                    if (!TextUtils.isEmpty(queryParameter3)) {
                        bundle.putString("aclid", queryParameter3);
                    }
                    String queryParameter4 = uri.getQueryParameter("cp1");
                    if (!TextUtils.isEmpty(queryParameter4)) {
                        bundle.putString("cp1", queryParameter4);
                    }
                    String queryParameter5 = uri.getQueryParameter("anid");
                    if (!TextUtils.isEmpty(queryParameter5)) {
                        bundle.putString("anid", queryParameter5);
                    }
                }
            } catch (UnsupportedOperationException e2) {
                d().I().b("Install referrer url isn't a hierarchical URI", e2);
            }
        }
        return bundle;
    }
}
