package com.fossil;

import android.graphics.Bitmap;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mh1 implements Qb1<Bb1, Bitmap> {
    @DexIgnore
    public /* final */ Rd1 a;

    @DexIgnore
    public Mh1(Rd1 rd1) {
        this.a = rd1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ boolean a(Bb1 bb1, Ob1 ob1) throws IOException {
        return d(bb1, ob1);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Id1' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Qb1
    public /* bridge */ /* synthetic */ Id1<Bitmap> b(Bb1 bb1, int i, int i2, Ob1 ob1) throws IOException {
        return c(bb1, i, i2, ob1);
    }

    @DexIgnore
    public Id1<Bitmap> c(Bb1 bb1, int i, int i2, Ob1 ob1) {
        return Yf1.f(bb1.a(), this.a);
    }

    @DexIgnore
    public boolean d(Bb1 bb1, Ob1 ob1) {
        return true;
    }
}
