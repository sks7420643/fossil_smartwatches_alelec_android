package com.fossil;

import android.graphics.drawable.Drawable;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class U04 extends S04 {
    @DexIgnore
    public U04(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    @Override // com.fossil.S04
    public void a() {
        this.a.setEndIconOnClickListener(null);
        this.a.setEndIconDrawable((Drawable) null);
        this.a.setEndIconContentDescription((CharSequence) null);
    }
}
