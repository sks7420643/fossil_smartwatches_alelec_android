package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iz1 extends Oz1 {
    @DexIgnore
    public /* final */ long a;

    @DexIgnore
    public Iz1(long j) {
        this.a = j;
    }

    @DexIgnore
    @Override // com.fossil.Oz1
    public long a() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Oz1) {
            return this.a == ((Oz1) obj).a();
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        return ((int) (j ^ (j >>> 32))) ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "LogResponse{nextRequestWaitMillis=" + this.a + "}";
    }
}
