package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Wg6;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Az4 {
    @DexIgnore
    public /* final */ int a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ Nf5 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(Nf5 nf5, View view) {
            super(view);
            Wg6.c(nf5, "binding");
            Wg6.c(view, "root");
            this.a = nf5;
        }

        @DexIgnore
        public final void a(String str) {
            Wg6.c(str, "name");
            FlexibleTextView flexibleTextView = this.a.q;
            Wg6.b(flexibleTextView, "binding.ftvTitle");
            flexibleTextView.setText(str);
        }
    }

    @DexIgnore
    public Az4(int i) {
        this.a = i;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public boolean b(List<? extends Object> list, int i) {
        Wg6.c(list, "items");
        return list.get(i) instanceof String;
    }

    @DexIgnore
    public void c(List<? extends Object> list, int i, RecyclerView.ViewHolder viewHolder) {
        Object obj = null;
        Wg6.c(list, "items");
        Wg6.c(viewHolder, "holder");
        Ai ai = (Ai) (!(viewHolder instanceof Ai) ? null : viewHolder);
        Object obj2 = list.get(i);
        if (obj2 instanceof String) {
            obj = obj2;
        }
        String str = (String) obj;
        if (ai != null && str != null) {
            ai.a(str);
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder d(ViewGroup viewGroup) {
        Wg6.c(viewGroup, "parent");
        Nf5 z = Nf5.z(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        Wg6.b(z, "ItemRecommendedChallenge\u2026(inflater, parent, false)");
        View n = z.n();
        Wg6.b(n, "binding.root");
        return new Ai(z, n);
    }
}
