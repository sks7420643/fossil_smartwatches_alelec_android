package com.fossil;

import android.content.ContentResolver;
import com.mapped.An4;
import com.mapped.U04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class M37 implements Factory<L37> {
    @DexIgnore
    public static L37 a(PortfolioApp portfolioApp, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, NotificationSettingsDatabase notificationSettingsDatabase, U04 u04, ContentResolver contentResolver, Lu5 lu5, An4 an4) {
        return new L37(portfolioApp, notificationsRepository, deviceRepository, notificationSettingsDatabase, u04, contentResolver, lu5, an4);
    }
}
