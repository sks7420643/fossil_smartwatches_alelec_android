package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Hi2 implements Parcelable.Creator<Ii2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ii2 createFromParcel(Parcel parcel) {
        int C = Ad2.C(parcel);
        String str = null;
        while (parcel.dataPosition() < C) {
            int t = Ad2.t(parcel);
            if (Ad2.l(t) != 1) {
                Ad2.B(parcel, t);
            } else {
                str = Ad2.f(parcel, t);
            }
        }
        Ad2.k(parcel, C);
        return new Ii2(str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ Ii2[] newArray(int i) {
        return new Ii2[i];
    }
}
