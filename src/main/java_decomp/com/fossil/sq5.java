package com.fossil;

import com.google.gson.Gson;
import com.mapped.Dx6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.ServerError;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Sq5<T> implements Dx6<T> {
    @DexIgnore
    public abstract void a(Call<T> call, ServerError serverError);

    @DexIgnore
    public abstract void b(Call<T> call, Throwable th);

    @DexIgnore
    public abstract void c(Call<T> call, Q88<T> q88);

    @DexIgnore
    @Override // com.mapped.Dx6
    public void onFailure(Call<T> call, Throwable th) {
        Wg6.c(call, "call");
        Wg6.c(th, "t");
        b(call, th);
    }

    @DexIgnore
    @Override // com.mapped.Dx6
    public void onResponse(Call<T> call, Q88<T> q88) {
        String f;
        String f2;
        String f3;
        Wg6.c(call, "call");
        Wg6.c(q88, "response");
        if (q88.e()) {
            c(call, q88);
            return;
        }
        int b = q88.b();
        if (b == 504 || b == 503 || b == 500 || b == 401 || b == 429) {
            ServerError serverError = new ServerError();
            serverError.setCode(Integer.valueOf(b));
            W18 d = q88.d();
            if (d == null || (f = d.string()) == null) {
                f = q88.f();
            }
            serverError.setMessage(f);
            a(call, serverError);
            return;
        }
        try {
            Gson gson = new Gson();
            W18 d2 = q88.d();
            if (d2 == null || (f3 = d2.string()) == null) {
                f3 = q88.f();
            }
            ServerError serverError2 = (ServerError) gson.k(f3, ServerError.class);
            Wg6.b(serverError2, "serverError");
            a(call, serverError2);
        } catch (Exception e) {
            ServerError serverError3 = new ServerError();
            serverError3.setCode(Integer.valueOf(b));
            W18 d3 = q88.d();
            if (d3 == null || (f2 = d3.string()) == null) {
                f2 = q88.f();
            }
            serverError3.setMessage(f2);
            a(call, serverError3);
        }
    }
}
