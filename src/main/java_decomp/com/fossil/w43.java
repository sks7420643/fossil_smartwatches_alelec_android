package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class W43 implements T43 {
    @DexIgnore
    public static /* final */ Xv2<Boolean> a; // = new Hw2(Yv2.a("com.google.android.gms.measurement")).d("measurement.sdk.collection.validate_param_names_alphabetical", true);

    @DexIgnore
    @Override // com.fossil.T43
    public final boolean zza() {
        return a.o().booleanValue();
    }
}
