package com.fossil;

import android.content.Context;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Dl3 extends Lm3 {
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f;
    @DexIgnore
    public long g;
    @DexIgnore
    public long h;
    @DexIgnore
    public List<String> i;
    @DexIgnore
    public int j;
    @DexIgnore
    public String k;
    @DexIgnore
    public String l;
    @DexIgnore
    public String m;

    @DexIgnore
    public Dl3(Pm3 pm3, long j2) {
        super(pm3);
        this.h = j2;
    }

    @DexIgnore
    @Override // com.fossil.Lm3
    public final boolean A() {
        return true;
    }

    @DexIgnore
    public final Or3 B(String str) {
        h();
        f();
        String C = C();
        String D = D();
        x();
        String str2 = this.d;
        long G = (long) G();
        x();
        String str3 = this.f;
        long C2 = m().C();
        x();
        h();
        if (this.g == 0) {
            this.g = this.a.F().w(e(), e().getPackageName());
        }
        long j2 = this.g;
        boolean o = this.a.o();
        boolean z = l().v;
        h();
        f();
        String J = !this.a.o() ? null : J();
        Pm3 pm3 = this.a;
        Long valueOf = Long.valueOf(pm3.z().j.a());
        long min = valueOf.longValue() == 0 ? pm3.F : Math.min(pm3.F, valueOf.longValue());
        int H = H();
        boolean booleanValue = m().I().booleanValue();
        Zr3 m2 = m();
        m2.f();
        Boolean A = m2.A("google_analytics_ssaid_collection_enabled");
        boolean booleanValue2 = Boolean.valueOf(A == null || A.booleanValue()).booleanValue();
        Xl3 l2 = l();
        l2.h();
        boolean z2 = l2.B().getBoolean("deferred_analytics_collection", false);
        String E = E();
        Boolean A2 = m().A("google_analytics_default_allow_ad_personalization_signals");
        return new Or3(C, D, str2, G, str3, C2, j2, str, o, !z, J, 0, min, H, booleanValue, booleanValue2, z2, E, A2 == null ? null : Boolean.valueOf(!A2.booleanValue()), this.h, m().s(Xg3.c0) ? this.i : null, (!I73.a() || !m().s(Xg3.o0)) ? null : F());
    }

    @DexIgnore
    public final String C() {
        x();
        return this.c;
    }

    @DexIgnore
    public final String D() {
        x();
        return this.k;
    }

    @DexIgnore
    public final String E() {
        x();
        return this.l;
    }

    @DexIgnore
    public final String F() {
        x();
        return this.m;
    }

    @DexIgnore
    public final int G() {
        x();
        return this.e;
    }

    @DexIgnore
    public final int H() {
        x();
        return this.j;
    }

    @DexIgnore
    public final List<String> I() {
        return this.i;
    }

    @DexIgnore
    public final String J() {
        if (!F93.a() || !m().s(Xg3.r0)) {
            try {
                Class<?> loadClass = e().getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics");
                if (loadClass == null) {
                    return null;
                }
                try {
                    Object invoke = loadClass.getDeclaredMethod("getInstance", Context.class).invoke(null, e());
                    if (invoke == null) {
                        return null;
                    }
                    try {
                        return (String) loadClass.getDeclaredMethod("getFirebaseInstanceId", new Class[0]).invoke(invoke, new Object[0]);
                    } catch (Exception e2) {
                        d().K().a("Failed to retrieve Firebase Instance Id");
                        return null;
                    }
                } catch (Exception e3) {
                    d().J().a("Failed to obtain Firebase Analytics instance");
                    return null;
                }
            } catch (ClassNotFoundException e4) {
                return null;
            }
        } else {
            d().N().a("Disabled IID for tests.");
            return null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:101:0x0290  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x029c  */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x02e9  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c3 A[Catch:{ IllegalStateException -> 0x02af }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00e9  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0123  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x014b  */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0178  */
    @Override // com.fossil.Lm3
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void v() {
        /*
        // Method dump skipped, instructions count: 776
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Dl3.v():void");
    }
}
