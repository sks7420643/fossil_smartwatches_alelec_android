package com.fossil;

import java.lang.Thread;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class A94 implements Thread.UncaughtExceptionHandler {
    @DexIgnore
    public /* final */ Ai a;
    @DexIgnore
    public /* final */ Rc4 b;
    @DexIgnore
    public /* final */ Thread.UncaughtExceptionHandler c;
    @DexIgnore
    public /* final */ AtomicBoolean d; // = new AtomicBoolean(false);

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(Rc4 rc4, Thread thread, Throwable th);
    }

    @DexIgnore
    public A94(Ai ai, Rc4 rc4, Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.a = ai;
        this.b = rc4;
        this.c = uncaughtExceptionHandler;
    }

    @DexIgnore
    public boolean a() {
        return this.d.get();
    }

    @DexIgnore
    public void uncaughtException(Thread thread, Throwable th) {
        this.d.set(true);
        if (thread == null) {
            try {
                X74.f().d("Could not handle uncaught exception; null thread");
            } catch (Exception e) {
                X74.f().e("An error occurred in the uncaught exception handler", e);
            } catch (Throwable th2) {
                X74.f().b("Crashlytics completed exception processing. Invoking default exception handler.");
                this.c.uncaughtException(thread, th);
                this.d.set(false);
                throw th2;
            }
        } else if (th == null) {
            X74.f().d("Could not handle uncaught exception; null throwable");
        } else {
            this.a.a(this.b, thread, th);
        }
        X74.f().b("Crashlytics completed exception processing. Invoking default exception handler.");
        this.c.uncaughtException(thread, th);
        this.d.set(false);
    }
}
