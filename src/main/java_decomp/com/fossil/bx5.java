package com.fossil;

import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Zf;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bx5 extends Zf.Di<DailyHeartRateSummary> {
    @DexIgnore
    public boolean a(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        Wg6.c(dailyHeartRateSummary, "oldItem");
        Wg6.c(dailyHeartRateSummary2, "newItem");
        return Wg6.a(dailyHeartRateSummary, dailyHeartRateSummary2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Zf.Di
    public /* bridge */ /* synthetic */ boolean areContentsTheSame(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        return a(dailyHeartRateSummary, dailyHeartRateSummary2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Zf.Di
    public /* bridge */ /* synthetic */ boolean areItemsTheSame(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        return b(dailyHeartRateSummary, dailyHeartRateSummary2);
    }

    @DexIgnore
    public boolean b(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        Wg6.c(dailyHeartRateSummary, "oldItem");
        Wg6.c(dailyHeartRateSummary2, "newItem");
        return TimeUtils.m0(dailyHeartRateSummary.getDate(), dailyHeartRateSummary2.getDate());
    }
}
