package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.internal.ServerProtocol;
import com.fossil.Xh4;
import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ai4 {
    @DexIgnore
    public static /* final */ Pd4 a;

    /*
    static {
        Be4 be4 = new Be4();
        be4.l(Xh4.Bi.class, new Xh4.Ci());
        be4.l(Xh4.class, new Xh4.Ai());
        a = be4.f();
    }
    */

    @DexIgnore
    public static boolean A(Intent intent) {
        if (intent == null || s(intent)) {
            return false;
        }
        return a();
    }

    @DexIgnore
    public static boolean B(Intent intent) {
        if (intent == null || s(intent)) {
            return false;
        }
        return "1".equals(intent.getStringExtra("google.c.a.e"));
    }

    @DexIgnore
    public static boolean a() {
        ApplicationInfo applicationInfo;
        try {
            J64.h();
            Context g = J64.h().g();
            SharedPreferences sharedPreferences = g.getSharedPreferences("com.google.firebase.messaging", 0);
            if (sharedPreferences.contains("export_to_big_query")) {
                return sharedPreferences.getBoolean("export_to_big_query", false);
            }
            try {
                PackageManager packageManager = g.getPackageManager();
                if (packageManager == null || (applicationInfo = packageManager.getApplicationInfo(g.getPackageName(), 128)) == null || applicationInfo.metaData == null || !applicationInfo.metaData.containsKey("delivery_metrics_exported_to_big_query_enabled")) {
                    return false;
                }
                return applicationInfo.metaData.getBoolean("delivery_metrics_exported_to_big_query_enabled", false);
            } catch (PackageManager.NameNotFoundException e) {
                return false;
            }
        } catch (IllegalStateException e2) {
            Log.i("FirebaseMessaging", "FirebaseApp has not being initialized. Device might be in direct boot mode. Skip exporting delivery metrics to Big Query");
            return false;
        }
    }

    @DexIgnore
    public static String b(Intent intent) {
        return intent.getStringExtra("collapse_key");
    }

    @DexIgnore
    public static String c(Intent intent) {
        return intent.getStringExtra("google.c.a.c_id");
    }

    @DexIgnore
    public static String d(Intent intent) {
        return intent.getStringExtra("google.c.a.c_l");
    }

    @DexIgnore
    public static String e() {
        return FirebaseInstanceId.getInstance(J64.h()).k();
    }

    @DexIgnore
    public static String f(Intent intent) {
        return intent.getStringExtra("google.c.a.m_c");
    }

    @DexIgnore
    public static String g(Intent intent) {
        String stringExtra = intent.getStringExtra("google.message_id");
        return stringExtra == null ? intent.getStringExtra("message_id") : stringExtra;
    }

    @DexIgnore
    public static String h(Intent intent) {
        return intent.getStringExtra("google.c.a.m_l");
    }

    @DexIgnore
    public static int i(String str) {
        if ("high".equals(str)) {
            return 1;
        }
        return "normal".equals(str) ? 2 : 0;
    }

    @DexIgnore
    public static String j(Intent intent) {
        return intent.getStringExtra("google.c.a.ts");
    }

    @DexIgnore
    public static String k(Intent intent) {
        return (intent.getExtras() == null || !Bi4.t(intent.getExtras())) ? "DATA_MESSAGE" : "DISPLAY_NOTIFICATION";
    }

    @DexIgnore
    public static String l(Intent intent) {
        return (intent.getExtras() == null || !Bi4.t(intent.getExtras())) ? "data" : ServerProtocol.DIALOG_PARAM_DISPLAY;
    }

    @DexIgnore
    public static String m() {
        return J64.h().g().getPackageName();
    }

    @DexIgnore
    public static int n(Intent intent) {
        String stringExtra = intent.getStringExtra("google.delivered_priority");
        if (stringExtra == null) {
            if ("1".equals(intent.getStringExtra("google.priority_reduced"))) {
                return 2;
            }
            stringExtra = intent.getStringExtra("google.priority");
        }
        return i(stringExtra);
    }

    @DexIgnore
    public static String o() {
        J64 h = J64.h();
        String d = h.j().d();
        if (d != null) {
            return d;
        }
        String c = h.j().c();
        if (!c.startsWith("1:")) {
            return c;
        }
        String[] split = c.split(":");
        if (split.length < 2) {
            return null;
        }
        String str = split[1];
        if (str.isEmpty()) {
            return null;
        }
        return str;
    }

    @DexIgnore
    public static String p(Intent intent) {
        String stringExtra = intent.getStringExtra("from");
        if (stringExtra == null || !stringExtra.startsWith("/topics/")) {
            return null;
        }
        return stringExtra;
    }

    @DexIgnore
    public static int q(Intent intent) {
        Object obj = intent.getExtras().get("google.ttl");
        if (obj instanceof Integer) {
            return ((Integer) obj).intValue();
        }
        if (obj instanceof String) {
            try {
                return Integer.parseInt((String) obj);
            } catch (NumberFormatException e) {
                String valueOf = String.valueOf(obj);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 13);
                sb.append("Invalid TTL: ");
                sb.append(valueOf);
                Log.w("FirebaseMessaging", sb.toString());
            }
        }
        return 0;
    }

    @DexIgnore
    public static String r(Intent intent) {
        if (intent.hasExtra("google.c.a.udt")) {
            return intent.getStringExtra("google.c.a.udt");
        }
        return null;
    }

    @DexIgnore
    public static boolean s(Intent intent) {
        return "com.google.firebase.messaging.RECEIVE_DIRECT_BOOT".equals(intent.getAction());
    }

    @DexIgnore
    public static void t(Intent intent) {
        y("_nd", intent);
    }

    @DexIgnore
    public static void u(Intent intent) {
        y("_nf", intent);
    }

    @DexIgnore
    public static void v(Intent intent) {
        z(intent);
        y("_no", intent);
    }

    @DexIgnore
    public static void w(Intent intent, Xy1<String> xy1) {
        y("_nr", intent);
        if (xy1 != null) {
            x("MESSAGE_DELIVERED", intent, xy1);
        }
    }

    @DexIgnore
    public static void x(String str, Intent intent, Xy1<String> xy1) {
        try {
            xy1.a(Uy1.e(a.b(new Xh4.Bi(new Xh4(str, intent)))));
        } catch (Rd4 e) {
            Log.d("FirebaseMessaging", "Failed to encode big query analytics payload. Skip sending");
        }
    }

    @DexIgnore
    public static void y(String str, Intent intent) {
        Bundle bundle = new Bundle();
        String c = c(intent);
        if (c != null) {
            bundle.putString("_nmid", c);
        }
        String d = d(intent);
        if (d != null) {
            bundle.putString("_nmn", d);
        }
        String h = h(intent);
        if (!TextUtils.isEmpty(h)) {
            bundle.putString("label", h);
        }
        String f = f(intent);
        if (!TextUtils.isEmpty(f)) {
            bundle.putString("message_channel", f);
        }
        String p = p(intent);
        if (p != null) {
            bundle.putString("_nt", p);
        }
        String j = j(intent);
        if (j != null) {
            try {
                bundle.putInt("_nmt", Integer.parseInt(j));
            } catch (NumberFormatException e) {
                Log.w("FirebaseMessaging", "Error while parsing timestamp in GCM event", e);
            }
        }
        String r = r(intent);
        if (r != null) {
            try {
                bundle.putInt("_ndt", Integer.parseInt(r));
            } catch (NumberFormatException e2) {
                Log.w("FirebaseMessaging", "Error while parsing use_device_time in GCM event", e2);
            }
        }
        String l = l(intent);
        if ("_nr".equals(str) || "_nf".equals(str)) {
            bundle.putString("_nmc", l);
        }
        if (Log.isLoggable("FirebaseMessaging", 3)) {
            String valueOf = String.valueOf(bundle);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 37 + String.valueOf(valueOf).length());
            sb.append("Logging to scion event=");
            sb.append(str);
            sb.append(" scionPayload=");
            sb.append(valueOf);
            Log.d("FirebaseMessaging", sb.toString());
        }
        M64 m64 = (M64) J64.h().f(M64.class);
        if (m64 != null) {
            m64.a("fcm", str, bundle);
        } else {
            Log.w("FirebaseMessaging", "Unable to log event: analytics library is missing");
        }
    }

    @DexIgnore
    public static void z(Intent intent) {
        if (intent != null) {
            if ("1".equals(intent.getStringExtra("google.c.a.tc"))) {
                M64 m64 = (M64) J64.h().f(M64.class);
                if (Log.isLoggable("FirebaseMessaging", 3)) {
                    Log.d("FirebaseMessaging", "Received event with track-conversion=true. Setting user property and reengagement event");
                }
                if (m64 != null) {
                    String stringExtra = intent.getStringExtra("google.c.a.c_id");
                    m64.b("fcm", "_ln", stringExtra);
                    Bundle bundle = new Bundle();
                    bundle.putString("source", "Firebase");
                    bundle.putString("medium", "notification");
                    bundle.putString(AppEventsLogger.PUSH_PAYLOAD_CAMPAIGN_KEY, stringExtra);
                    m64.a("fcm", "_cmp", bundle);
                    return;
                }
                Log.w("FirebaseMessaging", "Unable to set user property for conversion tracking:  analytics library is missing");
            } else if (Log.isLoggable("FirebaseMessaging", 3)) {
                Log.d("FirebaseMessaging", "Received event with track-conversion=false. Do not set user property");
            }
        }
    }
}
