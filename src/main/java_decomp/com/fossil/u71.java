package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import androidx.lifecycle.Lifecycle;
import coil.target.ImageViewTarget;
import com.fossil.P18;
import com.fossil.W71;
import com.fossil.X71;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U71 extends Y71<U71> {
    @DexIgnore
    public Drawable A;
    @DexIgnore
    public Drawable B;
    @DexIgnore
    public Drawable C;
    @DexIgnore
    public /* final */ Context t;
    @DexIgnore
    public J81 u; // = null;
    @DexIgnore
    public Lifecycle v; // = null;
    @DexIgnore
    public N81 w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public U71(Context context, Z41 z41) {
        super(z41, null);
        Wg6.c(context, "context");
        Wg6.c(z41, Constants.DEFAULTS);
        this.t = context;
        this.w = z41.h();
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.A = z41.f();
        this.B = z41.d();
        this.C = z41.e();
    }

    @DexIgnore
    public final U71 A(J81 j81) {
        this.u = j81;
        return this;
    }

    @DexIgnore
    public final T71 w() {
        Context context = this.t;
        Object f = f();
        J81 j81 = this.u;
        Lifecycle lifecycle = this.v;
        N81 n81 = this.w;
        String k = k();
        List<String> a2 = a();
        X71.Ai l = l();
        G81 r = r();
        E81 q = q();
        D81 p = p();
        U51 g = g();
        Dv7 i = i();
        List<M81> s = s();
        Bitmap.Config d = d();
        ColorSpace e = e();
        P18.Ai j = j();
        W71 w71 = null;
        P18 s2 = W81.s(j != null ? j.e() : null);
        Wg6.b(s2, "headers?.build().orEmpty()");
        W71.Ai o = o();
        if (o != null) {
            w71 = o.a();
        }
        return new T71(context, f, j81, lifecycle, n81, k, a2, l, r, q, p, g, i, s, d, e, s2, W81.r(w71), n(), h(), m(), b(), c(), this.x, this.y, this.z, this.A, this.B, this.C);
    }

    @DexIgnore
    public final U71 x(Object obj) {
        u(obj);
        return this;
    }

    @DexIgnore
    public final U71 y(Drawable drawable) {
        this.A = drawable;
        this.x = 0;
        return this;
    }

    @DexIgnore
    public final U71 z(ImageView imageView) {
        Wg6.c(imageView, "imageView");
        A(new ImageViewTarget(imageView));
        return this;
    }
}
