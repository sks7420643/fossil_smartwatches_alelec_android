package com.fossil;

import com.fossil.D54;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S44<E> extends M34<E> {
    @DexIgnore
    public static /* final */ S44<Comparable> NATURAL_EMPTY_SET; // = new S44<>(Y24.of(), I44.natural());
    @DexIgnore
    public /* final */ transient Y24<E> c;

    @DexIgnore
    public S44(Y24<E> y24, Comparator<? super E> comparator) {
        super(comparator);
        this.c = y24;
    }

    @DexIgnore
    public final int c(Object obj) throws ClassCastException {
        return Collections.binarySearch(this.c, obj, unsafeComparator());
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.M34
    public E ceiling(E e) {
        int tailIndex = tailIndex(e, true);
        if (tailIndex == size()) {
            return null;
        }
        return this.c.get(tailIndex);
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean contains(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            return c(obj) >= 0;
        } catch (ClassCastException e) {
            return false;
        }
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set, java.util.AbstractCollection
    public boolean containsAll(Collection<?> collection) {
        if (collection instanceof C44) {
            collection = ((C44) collection).elementSet();
        }
        if (!C54.b(comparator(), collection) || collection.size() <= 1) {
            return super.containsAll(collection);
        }
        J44 q = P34.q(iterator());
        Iterator<?> it = collection.iterator();
        Object next = it.next();
        while (q.hasNext()) {
            try {
                int unsafeCompare = unsafeCompare(q.peek(), next);
                if (unsafeCompare < 0) {
                    q.next();
                } else if (unsafeCompare == 0) {
                    if (!it.hasNext()) {
                        return true;
                    }
                    next = it.next();
                } else if (unsafeCompare > 0) {
                    break;
                }
            } catch (ClassCastException | NullPointerException e) {
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.U24
    public int copyIntoArray(Object[] objArr, int i) {
        return this.c.copyIntoArray(objArr, i);
    }

    @DexIgnore
    @Override // com.fossil.H34
    public Y24<E> createAsList() {
        return size() <= 1 ? this.c : new J34(this, this.c);
    }

    @DexIgnore
    @Override // com.fossil.M34
    public M34<E> createDescendingSet() {
        I44 reverse = I44.from(this.comparator).reverse();
        return isEmpty() ? M34.emptySet(reverse) : new S44(this.c.reverse(), reverse);
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.M34, com.fossil.M34
    public H54<E> descendingIterator() {
        return this.c.reverse().iterator();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0035 A[Catch:{ ClassCastException | NoSuchElementException -> 0x0047 }] */
    @Override // com.fossil.H34
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r7) {
        /*
            r6 = this;
            r0 = 1
            r1 = 0
            if (r7 != r6) goto L_0x0005
        L_0x0004:
            return r0
        L_0x0005:
            boolean r2 = r7 instanceof java.util.Set
            if (r2 != 0) goto L_0x000b
            r0 = r1
            goto L_0x0004
        L_0x000b:
            java.util.Set r7 = (java.util.Set) r7
            int r2 = r6.size()
            int r3 = r7.size()
            if (r2 == r3) goto L_0x0019
            r0 = r1
            goto L_0x0004
        L_0x0019:
            boolean r2 = r6.isEmpty()
            if (r2 != 0) goto L_0x0004
            java.util.Comparator<? super E> r2 = r6.comparator
            boolean r2 = com.fossil.C54.b(r2, r7)
            if (r2 == 0) goto L_0x004a
            java.util.Iterator r2 = r7.iterator()
            com.fossil.H54 r3 = r6.iterator()     // Catch:{ ClassCastException -> 0x0047, NoSuchElementException -> 0x004f }
        L_0x002f:
            boolean r4 = r3.hasNext()     // Catch:{ ClassCastException -> 0x0047, NoSuchElementException -> 0x004f }
            if (r4 == 0) goto L_0x0004
            java.lang.Object r4 = r3.next()     // Catch:{ ClassCastException -> 0x0047, NoSuchElementException -> 0x004f }
            java.lang.Object r5 = r2.next()     // Catch:{ ClassCastException -> 0x0047, NoSuchElementException -> 0x004f }
            if (r5 == 0) goto L_0x0045
            int r4 = r6.unsafeCompare(r4, r5)     // Catch:{ ClassCastException -> 0x0047, NoSuchElementException -> 0x004f }
            if (r4 == 0) goto L_0x002f
        L_0x0045:
            r0 = r1
            goto L_0x0004
        L_0x0047:
            r0 = move-exception
        L_0x0048:
            r0 = r1
            goto L_0x0004
        L_0x004a:
            boolean r0 = r6.containsAll(r7)
            goto L_0x0004
        L_0x004f:
            r0 = move-exception
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.S44.equals(java.lang.Object):boolean");
    }

    @DexIgnore
    @Override // java.util.SortedSet, com.fossil.M34
    public E first() {
        if (!isEmpty()) {
            return this.c.get(0);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.M34
    public E floor(E e) {
        int headIndex = headIndex(e, true) - 1;
        if (headIndex == -1) {
            return null;
        }
        return this.c.get(headIndex);
    }

    @DexIgnore
    public S44<E> getSubSet(int i, int i2) {
        return (i == 0 && i2 == size()) ? this : i < i2 ? new S44<>(this.c.subList(i, i2), this.comparator) : M34.emptySet(this.comparator);
    }

    @DexIgnore
    public int headIndex(E e, boolean z) {
        Y24<E> y24 = this.c;
        I14.l(e);
        return D54.a(y24, e, comparator(), z ? D54.Ci.FIRST_AFTER : D54.Ci.FIRST_PRESENT, D54.Bi.NEXT_HIGHER);
    }

    @DexIgnore
    @Override // com.fossil.M34
    public M34<E> headSetImpl(E e, boolean z) {
        return getSubSet(0, headIndex(e, z));
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.M34
    public E higher(E e) {
        int tailIndex = tailIndex(e, false);
        if (tailIndex == size()) {
            return null;
        }
        return this.c.get(tailIndex);
    }

    @DexIgnore
    @Override // com.fossil.M34
    public int indexOf(Object obj) {
        int i;
        if (obj == null) {
            return -1;
        }
        try {
            i = D54.a(this.c, obj, unsafeComparator(), D54.Ci.ANY_PRESENT, D54.Bi.INVERTED_INSERTION_INDEX);
            if (i < 0) {
                i = -1;
            }
        } catch (ClassCastException e) {
            i = -1;
        }
        return i;
    }

    @DexIgnore
    @Override // com.fossil.U24
    public boolean isPartialView() {
        return this.c.isPartialView();
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set, com.fossil.U24, com.fossil.U24, java.util.NavigableSet, java.lang.Iterable, com.fossil.M34, com.fossil.M34, java.util.AbstractCollection, com.fossil.H34, com.fossil.H34
    public H54<E> iterator() {
        return this.c.iterator();
    }

    @DexIgnore
    @Override // java.util.SortedSet, com.fossil.M34
    public E last() {
        if (!isEmpty()) {
            return this.c.get(size() - 1);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    @Override // java.util.NavigableSet, com.fossil.M34
    public E lower(E e) {
        int headIndex = headIndex(e, false) - 1;
        if (headIndex == -1) {
            return null;
        }
        return this.c.get(headIndex);
    }

    @DexIgnore
    public int size() {
        return this.c.size();
    }

    @DexIgnore
    @Override // com.fossil.M34
    public M34<E> subSetImpl(E e, boolean z, E e2, boolean z2) {
        return tailSetImpl(e, z).headSetImpl(e2, z2);
    }

    @DexIgnore
    public int tailIndex(E e, boolean z) {
        Y24<E> y24 = this.c;
        I14.l(e);
        return D54.a(y24, e, comparator(), z ? D54.Ci.FIRST_PRESENT : D54.Ci.FIRST_AFTER, D54.Bi.NEXT_HIGHER);
    }

    @DexIgnore
    @Override // com.fossil.M34
    public M34<E> tailSetImpl(E e, boolean z) {
        return getSubSet(tailIndex(e, z), size());
    }

    @DexIgnore
    public Comparator<Object> unsafeComparator() {
        return this.comparator;
    }
}
