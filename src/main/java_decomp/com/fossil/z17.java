package com.fossil;

import com.mapped.An4;
import com.mapped.GetLocation;
import com.mapped.LoadLocation;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.locate.map.usecase.GetAddress;
import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Z17 implements Factory<FindDevicePresenter> {
    @DexIgnore
    public static FindDevicePresenter a(Ct0 ct0, DeviceRepository deviceRepository, An4 an4, V17 v17, GetLocation getLocation, LoadLocation loadLocation, GetAddress getAddress, Gu5 gu5, PortfolioApp portfolioApp) {
        return new FindDevicePresenter(ct0, deviceRepository, an4, v17, getLocation, loadLocation, getAddress, gu5, portfolioApp);
    }
}
