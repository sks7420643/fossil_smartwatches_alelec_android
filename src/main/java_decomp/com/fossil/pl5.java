package com.fossil;

import com.mapped.An4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;
import com.misfit.frameworks.buttonservice.model.calibration.HandCalibrationObj;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Pl5 {
    @DexIgnore
    public static /* final */ String g; // = "pl5";
    @DexIgnore
    public static Pl5 h;
    @DexIgnore
    public int a;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ AtomicBoolean c; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicBoolean d; // = new AtomicBoolean(false);
    @DexIgnore
    public CalibrationEnums.HandId e;
    @DexIgnore
    public An4 f;

    @DexIgnore
    public Pl5() {
        PortfolioApp.d0.getIface().g0(this);
    }

    @DexIgnore
    public static Pl5 h() {
        Pl5 pl5;
        synchronized (Pl5.class) {
            try {
                if (h == null) {
                    h = new Pl5();
                }
                pl5 = h;
            } catch (Throwable th) {
                throw th;
            }
        }
        return pl5;
    }

    @DexIgnore
    public boolean a(String str) {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = g;
            local.d(str2, "Cancel Calibration - serial=" + str);
            this.c.set(false);
            PortfolioApp.O().deviceCancelCalibration(str);
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    @DexIgnore
    public boolean b(String str, String str2) {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = g;
            local.d(str3, "Complete Calibration - serial=" + str);
            PortfolioApp.O().deviceCompleteCalibration(str);
            this.c.set(false);
            this.f.Y0(str, str2, true);
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    @DexIgnore
    public boolean c(String str) {
        try {
            this.a = 0;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = g;
            local.d(str2, "Enter Calibration - serial=" + str);
            PortfolioApp.O().deviceStartCalibration(str);
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    @DexIgnore
    public final boolean d(String str, int i, CalibrationEnums.HandId handId, CalibrationEnums.MovingType movingType, CalibrationEnums.Direction direction, CalibrationEnums.Speed speed) {
        try {
            PortfolioApp.O().deviceMovingHand(str, new HandCalibrationObj(handId, movingType, direction, speed, i));
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    @DexIgnore
    public boolean e(boolean z, String str, int i, CalibrationEnums.HandId handId) {
        return d(str, i, handId, CalibrationEnums.MovingType.DISTANCE, z ? CalibrationEnums.Direction.CLOCKWISE : CalibrationEnums.Direction.COUNTER_CLOCKWISE, CalibrationEnums.Speed.FULL);
    }

    @DexIgnore
    public void f(boolean z, String str, int i, CalibrationEnums.HandId handId) {
        synchronized (this.c) {
            if (!this.c.get()) {
                FLogger.INSTANCE.getLocal().d(g, "Start smart movement");
                try {
                    PortfolioApp.get.h(h());
                } catch (Exception e2) {
                    FLogger.INSTANCE.getLocal().e(g, "Exception when register bus events");
                }
                this.c.set(true);
                this.b = System.currentTimeMillis();
                this.a = 0;
                this.d.set(z);
                this.e = handId;
                e(z, str, i, handId);
            }
        }
    }

    @DexIgnore
    public void g() {
        synchronized (this.c) {
            this.c.set(false);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.d(str, "Stop smart movement isInSmartMovementMode=" + this.c.get() + ", fromThread=" + Thread.currentThread().getName());
            try {
                PortfolioApp.get.l(h());
            } catch (Exception e2) {
                FLogger.INSTANCE.getLocal().e(g, "Exception when unregister receiver");
                e2.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void i(String str) {
        try {
            PortfolioApp.O().resetHandsToZeroDegree(str);
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = g;
            local.d(str2, "Error reset hands second timezone of serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    @Tc7
    public void onMovingHandCompleted(Ui5 ui5) {
        synchronized (this.c) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            local.e(str, "onMovingHandCompleted isInSmartMovementMode=" + this.c.get() + ", fromThread=" + Thread.currentThread().getName());
            if (this.c.get() && ui5.b()) {
                int currentTimeMillis = ((int) (((System.currentTimeMillis() - this.b) / 10) * 2)) % 360;
                int i = currentTimeMillis >= this.a ? currentTimeMillis - this.a : (currentTimeMillis + 360) - this.a;
                this.a = currentTimeMillis;
                e(this.d.get(), ui5.a(), i, this.e);
            }
        }
    }
}
