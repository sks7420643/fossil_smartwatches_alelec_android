package com.fossil;

import com.mapped.Cd6;
import com.mapped.Hg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wq extends Qq7 implements Hg6<Fs, Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ Af b;
    @DexIgnore
    public /* final */ /* synthetic */ long c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Wq(Af af, long j) {
        super(1);
        this.b = af;
        this.c = j;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public Cd6 invoke(Fs fs) {
        if (this.c == ((Fw) fs).L) {
            Af af = this.b;
            Af.K(af, af.H);
            Af af2 = this.b;
            long j = af2.G;
            long j2 = af2.I;
            if (j != j2) {
                Af.J(af2);
            } else if (j2 == af2.D) {
                Af.R(af2);
            } else {
                af2.T();
            }
        } else {
            Af af3 = this.b;
            Af.M(af3, Math.max(0L, af3.H - 6144));
            Af af4 = this.b;
            if (af4.H == 0) {
                af4.T();
            } else {
                Af.S(af4);
            }
        }
        return Cd6.a;
    }
}
