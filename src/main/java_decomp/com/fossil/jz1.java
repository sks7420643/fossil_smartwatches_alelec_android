package com.fossil;

import com.fossil.Pz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jz1 extends Pz1 {
    @DexIgnore
    public /* final */ Pz1.Ci a;
    @DexIgnore
    public /* final */ Pz1.Bi b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Pz1.Ai {
        @DexIgnore
        public Pz1.Ci a;
        @DexIgnore
        public Pz1.Bi b;

        @DexIgnore
        @Override // com.fossil.Pz1.Ai
        public Pz1.Ai a(Pz1.Bi bi) {
            this.b = bi;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Pz1.Ai
        public Pz1.Ai b(Pz1.Ci ci) {
            this.a = ci;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.Pz1.Ai
        public Pz1 c() {
            return new Jz1(this.a, this.b, null);
        }
    }

    @DexIgnore
    public /* synthetic */ Jz1(Pz1.Ci ci, Pz1.Bi bi, Ai ai) {
        this.a = ci;
        this.b = bi;
    }

    @DexIgnore
    @Override // com.fossil.Pz1
    public Pz1.Bi b() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Pz1
    public Pz1.Ci c() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        boolean z;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Pz1)) {
            return false;
        }
        Pz1.Ci ci = this.a;
        if (ci != null ? ci.equals(((Jz1) obj).a) : ((Jz1) obj).a == null) {
            Pz1.Bi bi = this.b;
            if (bi == null) {
                if (((Jz1) obj).b == null) {
                    z = true;
                    return z;
                }
            } else if (bi.equals(((Jz1) obj).b)) {
                z = true;
                return z;
            }
        }
        z = false;
        return z;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Pz1.Ci ci = this.a;
        int hashCode = ci == null ? 0 : ci.hashCode();
        Pz1.Bi bi = this.b;
        if (bi != null) {
            i = bi.hashCode();
        }
        return ((hashCode ^ 1000003) * 1000003) ^ i;
    }

    @DexIgnore
    public String toString() {
        return "NetworkConnectionInfo{networkType=" + this.a + ", mobileSubtype=" + this.b + "}";
    }
}
