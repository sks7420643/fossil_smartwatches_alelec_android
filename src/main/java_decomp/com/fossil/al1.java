package com.fossil;

import com.mapped.Qg6;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Al1 {
    UNKNOWN(false, Q3.f.m()),
    DIANA(false, Q3.f.g()),
    SE1(true, Q3.f.k()),
    SLIM(true, Q3.f.l()),
    MINI(true, Q3.f.j()),
    HELLAS(false, Q3.f.h()),
    SE0(true, Q3.f.k()),
    WEAR_OS(false, new Type[0]),
    IVY(false, Q3.f.g());
    
    @DexIgnore
    public static /* final */ Ai Companion; // = new Ai(null);
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ Type[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Al1 a(String str, String str2) {
            if (Vt7.s(str, "DN", false, 2, null)) {
                return Al1.DIANA;
            }
            if (!Vt7.s(str, "HW", false, 2, null)) {
                return Vt7.s(str, "HL", false, 2, null) ? Al1.SLIM : Vt7.s(str, "HM", false, 2, null) ? Al1.MINI : Vt7.s(str, "AW", false, 2, null) ? Al1.HELLAS : Vt7.s(str, "IV", false, 2, null) ? Al1.IVY : b(str2);
            }
            Al1 b = b(str2);
            return b == Al1.UNKNOWN ? Al1.SE1 : b;
        }

        @DexIgnore
        public final Al1 b(String str) {
            return !Zk1.u.d(str) ? Al1.UNKNOWN : Vt7.s(str, "D", false, 2, null) ? Al1.DIANA : Vt7.s(str, "W", false, 2, null) ? Al1.SE1 : Vt7.s(str, "L", false, 2, null) ? Al1.SLIM : Vt7.s(str, "M", false, 2, null) ? Al1.MINI : Vt7.s(str, "Y", false, 2, null) ? Al1.HELLAS : Vt7.s(str, "Z", false, 2, null) ? Al1.SE0 : Vt7.s(str, "V", false, 2, null) ? Al1.IVY : Al1.UNKNOWN;
        }
    }

    @DexIgnore
    public Al1(boolean z, Type[] typeArr) {
        this.b = z;
        this.c = typeArr;
    }

    @DexIgnore
    public final Type[] a() {
        return this.c;
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }
}
