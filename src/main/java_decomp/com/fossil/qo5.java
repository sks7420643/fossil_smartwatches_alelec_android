package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface Qo5 {
    @DexIgnore
    Object a();  // void declaration

    @DexIgnore
    Mo5 b(String str);

    @DexIgnore
    void c(Mo5 mo5);

    @DexIgnore
    List<Mo5> d(String str);

    @DexIgnore
    void e(String str);

    @DexIgnore
    List<Mo5> f(String str);

    @DexIgnore
    void g(String str);

    @DexIgnore
    Mo5 getPresetById(String str);

    @DexIgnore
    int h(String str);

    @DexIgnore
    void i(String str, int i);

    @DexIgnore
    Long[] insert(List<Mo5> list);

    @DexIgnore
    void j(List<Mo5> list);

    @DexIgnore
    Mo5 k();

    @DexIgnore
    long l(Mo5 mo5);

    @DexIgnore
    LiveData<List<Mo5>> m(String str);
}
