package com.fossil;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.ShareConstants;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.mapped.BCPlayerConverter;
import com.mapped.Gh;
import com.mapped.Hh;
import com.mapped.HistoryChallengeConverter;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rs4 implements Qs4 {
    @DexIgnore
    public /* final */ Oh a;
    @DexIgnore
    public /* final */ Hh<Ps4> b;
    @DexIgnore
    public /* final */ Oz4 c; // = new Oz4();
    @DexIgnore
    public /* final */ Hh<Ls4> d;
    @DexIgnore
    public /* final */ Hh<Ks4> e;
    @DexIgnore
    public /* final */ BCPlayerConverter f; // = new BCPlayerConverter();
    @DexIgnore
    public /* final */ Hh<Bt4> g;
    @DexIgnore
    public /* final */ HistoryChallengeConverter h; // = new HistoryChallengeConverter();
    @DexIgnore
    public /* final */ Hh<Ms4> i;
    @DexIgnore
    public /* final */ Gh<Bt4> j;
    @DexIgnore
    public /* final */ Vh k;
    @DexIgnore
    public /* final */ Vh l;
    @DexIgnore
    public /* final */ Vh m;
    @DexIgnore
    public /* final */ Vh n;
    @DexIgnore
    public /* final */ Vh o;
    @DexIgnore
    public /* final */ Vh p;
    @DexIgnore
    public /* final */ Vh q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai extends Vh {
        @DexIgnore
        public Ai(Rs4 rs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM fitness";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Bi extends Vh {
        @DexIgnore
        public Bi(Rs4 rs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM display_player";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ci extends Vh {
        @DexIgnore
        public Ci(Rs4 rs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM challenge_player";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Di extends Vh {
        @DexIgnore
        public Di(Rs4 rs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM history_challenge";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ei extends Vh {
        @DexIgnore
        public Ei(Rs4 rs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM players";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Fi implements Callable<Ps4> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh a;

        @DexIgnore
        public Fi(Rh rh) {
            this.a = rh;
        }

        @DexIgnore
        public Ps4 a() throws Exception {
            Ps4 ps4;
            Cursor b2 = Ex0.b(Rs4.this.a, this.a, false, null);
            try {
                int c = Dx0.c(b2, "id");
                int c2 = Dx0.c(b2, "type");
                int c3 = Dx0.c(b2, "name");
                int c4 = Dx0.c(b2, "des");
                int c5 = Dx0.c(b2, "owner");
                int c6 = Dx0.c(b2, "numberOfPlayers");
                int c7 = Dx0.c(b2, SampleRaw.COLUMN_START_TIME);
                int c8 = Dx0.c(b2, SampleRaw.COLUMN_END_TIME);
                int c9 = Dx0.c(b2, "target");
                int c10 = Dx0.c(b2, "duration");
                int c11 = Dx0.c(b2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
                int c12 = Dx0.c(b2, "version");
                int c13 = Dx0.c(b2, "status");
                int c14 = Dx0.c(b2, "syncData");
                int c15 = Dx0.c(b2, "createdAt");
                int c16 = Dx0.c(b2, "updatedAt");
                int c17 = Dx0.c(b2, "category");
                if (b2.moveToFirst()) {
                    ps4 = new Ps4(b2.getString(c), b2.getString(c2), b2.getString(c3), b2.getString(c4), Rs4.this.c.c(b2.getString(c5)), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), Rs4.this.c.d(b2.getString(c7)), Rs4.this.c.d(b2.getString(c8)), b2.isNull(c9) ? null : Integer.valueOf(b2.getInt(c9)), b2.isNull(c10) ? null : Integer.valueOf(b2.getInt(c10)), b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getString(c14), Rs4.this.c.d(b2.getString(c15)), Rs4.this.c.d(b2.getString(c16)), b2.getString(c17));
                } else {
                    ps4 = null;
                }
                return ps4;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Ps4 call() throws Exception {
            return a();
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Gi implements Callable<Ps4> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh a;

        @DexIgnore
        public Gi(Rh rh) {
            this.a = rh;
        }

        @DexIgnore
        public Ps4 a() throws Exception {
            Ps4 ps4;
            Cursor b2 = Ex0.b(Rs4.this.a, this.a, false, null);
            try {
                int c = Dx0.c(b2, "id");
                int c2 = Dx0.c(b2, "type");
                int c3 = Dx0.c(b2, "name");
                int c4 = Dx0.c(b2, "des");
                int c5 = Dx0.c(b2, "owner");
                int c6 = Dx0.c(b2, "numberOfPlayers");
                int c7 = Dx0.c(b2, SampleRaw.COLUMN_START_TIME);
                int c8 = Dx0.c(b2, SampleRaw.COLUMN_END_TIME);
                int c9 = Dx0.c(b2, "target");
                int c10 = Dx0.c(b2, "duration");
                int c11 = Dx0.c(b2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
                int c12 = Dx0.c(b2, "version");
                int c13 = Dx0.c(b2, "status");
                int c14 = Dx0.c(b2, "syncData");
                int c15 = Dx0.c(b2, "createdAt");
                int c16 = Dx0.c(b2, "updatedAt");
                int c17 = Dx0.c(b2, "category");
                if (b2.moveToFirst()) {
                    ps4 = new Ps4(b2.getString(c), b2.getString(c2), b2.getString(c3), b2.getString(c4), Rs4.this.c.c(b2.getString(c5)), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), Rs4.this.c.d(b2.getString(c7)), Rs4.this.c.d(b2.getString(c8)), b2.isNull(c9) ? null : Integer.valueOf(b2.getInt(c9)), b2.isNull(c10) ? null : Integer.valueOf(b2.getInt(c10)), b2.getString(c11), b2.getString(c12), b2.getString(c13), b2.getString(c14), Rs4.this.c.d(b2.getString(c15)), Rs4.this.c.d(b2.getString(c16)), b2.getString(c17));
                } else {
                    ps4 = null;
                }
                return ps4;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Ps4 call() throws Exception {
            return a();
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Hi extends Hh<Ps4> {
        @DexIgnore
        public Hi(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(com.mapped.Mi mi, Ps4 ps4) {
            if (ps4.f() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, ps4.f());
            }
            if (ps4.r() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, ps4.r());
            }
            if (ps4.g() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, ps4.g());
            }
            if (ps4.c() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, ps4.c());
            }
            String b = Rs4.this.c.b(ps4.i());
            if (b == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, b);
            }
            if (ps4.h() == null) {
                mi.bindNull(6);
            } else {
                mi.bindLong(6, (long) ps4.h().intValue());
            }
            String a2 = Rs4.this.c.a(ps4.m());
            if (a2 == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, a2);
            }
            String a3 = Rs4.this.c.a(ps4.e());
            if (a3 == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, a3);
            }
            if (ps4.q() == null) {
                mi.bindNull(9);
            } else {
                mi.bindLong(9, (long) ps4.q().intValue());
            }
            if (ps4.d() == null) {
                mi.bindNull(10);
            } else {
                mi.bindLong(10, (long) ps4.d().intValue());
            }
            if (ps4.k() == null) {
                mi.bindNull(11);
            } else {
                mi.bindString(11, ps4.k());
            }
            if (ps4.t() == null) {
                mi.bindNull(12);
            } else {
                mi.bindString(12, ps4.t());
            }
            if (ps4.n() == null) {
                mi.bindNull(13);
            } else {
                mi.bindString(13, ps4.n());
            }
            if (ps4.p() == null) {
                mi.bindNull(14);
            } else {
                mi.bindString(14, ps4.p());
            }
            String a4 = Rs4.this.c.a(ps4.b());
            if (a4 == null) {
                mi.bindNull(15);
            } else {
                mi.bindString(15, a4);
            }
            String a5 = Rs4.this.c.a(ps4.s());
            if (a5 == null) {
                mi.bindNull(16);
            } else {
                mi.bindString(16, a5);
            }
            if (ps4.a() == null) {
                mi.bindNull(17);
            } else {
                mi.bindString(17, ps4.a());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(com.mapped.Mi mi, Ps4 ps4) {
            a(mi, ps4);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `challenge` (`id`,`type`,`name`,`des`,`owner`,`numberOfPlayers`,`startTime`,`endTime`,`target`,`duration`,`privacy`,`version`,`status`,`syncData`,`createdAt`,`updatedAt`,`category`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ii implements Callable<List<Bt4>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh a;

        @DexIgnore
        public Ii(Rh rh) {
            this.a = rh;
        }

        @DexIgnore
        public List<Bt4> a() throws Exception {
            Cursor b2 = Ex0.b(Rs4.this.a, this.a, false, null);
            try {
                int c = Dx0.c(b2, "id");
                int c2 = Dx0.c(b2, "type");
                int c3 = Dx0.c(b2, "name");
                int c4 = Dx0.c(b2, "des");
                int c5 = Dx0.c(b2, "owner");
                int c6 = Dx0.c(b2, "numberOfPlayers");
                int c7 = Dx0.c(b2, SampleRaw.COLUMN_START_TIME);
                int c8 = Dx0.c(b2, SampleRaw.COLUMN_END_TIME);
                int c9 = Dx0.c(b2, "target");
                int c10 = Dx0.c(b2, "duration");
                int c11 = Dx0.c(b2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
                int c12 = Dx0.c(b2, "version");
                int c13 = Dx0.c(b2, "status");
                int c14 = Dx0.c(b2, "players");
                int c15 = Dx0.c(b2, "topPlayer");
                int c16 = Dx0.c(b2, "currentPlayer");
                int c17 = Dx0.c(b2, "createdAt");
                int c18 = Dx0.c(b2, "updatedAt");
                int c19 = Dx0.c(b2, "completedAt");
                int c20 = Dx0.c(b2, "isFirst");
                ArrayList arrayList = new ArrayList(b2.getCount());
                while (b2.moveToNext()) {
                    arrayList.add(new Bt4(b2.getString(c), b2.getString(c2), b2.getString(c3), b2.getString(c4), Rs4.this.h.d(b2.getString(c5)), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), Rs4.this.c.d(b2.getString(c7)), Rs4.this.c.d(b2.getString(c8)), b2.isNull(c9) ? null : Integer.valueOf(b2.getInt(c9)), b2.isNull(c10) ? null : Integer.valueOf(b2.getInt(c10)), b2.getString(c11), b2.getString(c12), b2.getString(c13), Rs4.this.h.f(b2.getString(c14)), Rs4.this.h.e(b2.getString(c15)), Rs4.this.h.e(b2.getString(c16)), Rs4.this.c.d(b2.getString(c17)), Rs4.this.c.d(b2.getString(c18)), Rs4.this.c.d(b2.getString(c19)), b2.getInt(c20) != 0));
                }
                return arrayList;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ List<Bt4> call() throws Exception {
            return a();
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ji implements Callable<Bt4> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh a;

        @DexIgnore
        public Ji(Rh rh) {
            this.a = rh;
        }

        @DexIgnore
        public Bt4 a() throws Exception {
            Bt4 bt4;
            Cursor b2 = Ex0.b(Rs4.this.a, this.a, false, null);
            try {
                int c = Dx0.c(b2, "id");
                int c2 = Dx0.c(b2, "type");
                int c3 = Dx0.c(b2, "name");
                int c4 = Dx0.c(b2, "des");
                int c5 = Dx0.c(b2, "owner");
                int c6 = Dx0.c(b2, "numberOfPlayers");
                int c7 = Dx0.c(b2, SampleRaw.COLUMN_START_TIME);
                int c8 = Dx0.c(b2, SampleRaw.COLUMN_END_TIME);
                int c9 = Dx0.c(b2, "target");
                int c10 = Dx0.c(b2, "duration");
                int c11 = Dx0.c(b2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
                int c12 = Dx0.c(b2, "version");
                int c13 = Dx0.c(b2, "status");
                int c14 = Dx0.c(b2, "players");
                int c15 = Dx0.c(b2, "topPlayer");
                int c16 = Dx0.c(b2, "currentPlayer");
                int c17 = Dx0.c(b2, "createdAt");
                int c18 = Dx0.c(b2, "updatedAt");
                int c19 = Dx0.c(b2, "completedAt");
                int c20 = Dx0.c(b2, "isFirst");
                if (b2.moveToFirst()) {
                    bt4 = new Bt4(b2.getString(c), b2.getString(c2), b2.getString(c3), b2.getString(c4), Rs4.this.h.d(b2.getString(c5)), b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6)), Rs4.this.c.d(b2.getString(c7)), Rs4.this.c.d(b2.getString(c8)), b2.isNull(c9) ? null : Integer.valueOf(b2.getInt(c9)), b2.isNull(c10) ? null : Integer.valueOf(b2.getInt(c10)), b2.getString(c11), b2.getString(c12), b2.getString(c13), Rs4.this.h.f(b2.getString(c14)), Rs4.this.h.e(b2.getString(c15)), Rs4.this.h.e(b2.getString(c16)), Rs4.this.c.d(b2.getString(c17)), Rs4.this.c.d(b2.getString(c18)), Rs4.this.c.d(b2.getString(c19)), b2.getInt(c20) != 0);
                } else {
                    bt4 = null;
                }
                return bt4;
            } finally {
                b2.close();
            }
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // java.util.concurrent.Callable
        public /* bridge */ /* synthetic */ Bt4 call() throws Exception {
            return a();
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ki extends Hh<Ls4> {
        @DexIgnore
        public Ki(Rs4 rs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(com.mapped.Mi mi, Ls4 ls4) {
            mi.bindLong(1, (long) ls4.e());
            mi.bindLong(2, (long) ls4.c());
            mi.bindLong(3, (long) ls4.a());
            if (ls4.b() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, ls4.b());
            }
            mi.bindLong(5, (long) ls4.d());
            if (ls4.f() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, ls4.f());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(com.mapped.Mi mi, Ls4 ls4) {
            a(mi, ls4);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `fitness` (`pinType`,`id`,`encryptMethod`,`encryptedData`,`keyType`,`sequence`) VALUES (?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Li extends Hh<Ks4> {
        @DexIgnore
        public Li(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(com.mapped.Mi mi, Ks4 ks4) {
            if (ks4.a() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, ks4.a());
            }
            if (ks4.c() == null) {
                mi.bindNull(2);
            } else {
                mi.bindLong(2, (long) ks4.c().intValue());
            }
            String a2 = Rs4.this.f.a(ks4.d());
            if (a2 == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, a2);
            }
            String a3 = Rs4.this.f.a(ks4.b());
            if (a3 == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, a3);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(com.mapped.Mi mi, Ks4 ks4) {
            a(mi, ks4);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `display_player` (`challengeId`,`numberOfPlayers`,`topPlayers`,`nearPlayers`) VALUES (?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Mi extends Hh<Js4> {
        @DexIgnore
        public Mi(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(com.mapped.Mi mi, Js4 js4) {
            if (js4.a() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, js4.a());
            }
            if (js4.b() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, js4.b());
            }
            String a2 = Rs4.this.f.a(js4.c());
            if (a2 == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, a2);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(com.mapped.Mi mi, Js4 js4) {
            a(mi, js4);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `challenge_player` (`challengeId`,`challengeName`,`players`) VALUES (?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ni extends Hh<Bt4> {
        @DexIgnore
        public Ni(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(com.mapped.Mi mi, Bt4 bt4) {
            if (bt4.g() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, bt4.g());
            }
            if (bt4.q() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, bt4.q());
            }
            if (bt4.h() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, bt4.h());
            }
            if (bt4.d() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, bt4.d());
            }
            String a2 = Rs4.this.h.a(bt4.j());
            if (a2 == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, a2);
            }
            if (bt4.i() == null) {
                mi.bindNull(6);
            } else {
                mi.bindLong(6, (long) bt4.i().intValue());
            }
            String a3 = Rs4.this.c.a(bt4.m());
            if (a3 == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, a3);
            }
            String a4 = Rs4.this.c.a(bt4.f());
            if (a4 == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, a4);
            }
            if (bt4.o() == null) {
                mi.bindNull(9);
            } else {
                mi.bindLong(9, (long) bt4.o().intValue());
            }
            if (bt4.e() == null) {
                mi.bindNull(10);
            } else {
                mi.bindLong(10, (long) bt4.e().intValue());
            }
            if (bt4.l() == null) {
                mi.bindNull(11);
            } else {
                mi.bindString(11, bt4.l());
            }
            if (bt4.s() == null) {
                mi.bindNull(12);
            } else {
                mi.bindString(12, bt4.s());
            }
            if (bt4.n() == null) {
                mi.bindNull(13);
            } else {
                mi.bindString(13, bt4.n());
            }
            String c = Rs4.this.h.c(bt4.k());
            if (c == null) {
                mi.bindNull(14);
            } else {
                mi.bindString(14, c);
            }
            String b = Rs4.this.h.b(bt4.p());
            if (b == null) {
                mi.bindNull(15);
            } else {
                mi.bindString(15, b);
            }
            String b2 = Rs4.this.h.b(bt4.c());
            if (b2 == null) {
                mi.bindNull(16);
            } else {
                mi.bindString(16, b2);
            }
            String a5 = Rs4.this.c.a(bt4.b());
            if (a5 == null) {
                mi.bindNull(17);
            } else {
                mi.bindString(17, a5);
            }
            String a6 = Rs4.this.c.a(bt4.r());
            if (a6 == null) {
                mi.bindNull(18);
            } else {
                mi.bindString(18, a6);
            }
            String a7 = Rs4.this.c.a(bt4.a());
            if (a7 == null) {
                mi.bindNull(19);
            } else {
                mi.bindString(19, a7);
            }
            mi.bindLong(20, bt4.t() ? 1 : 0);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(com.mapped.Mi mi, Bt4 bt4) {
            a(mi, bt4);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `history_challenge` (`id`,`type`,`name`,`des`,`owner`,`numberOfPlayers`,`startTime`,`endTime`,`target`,`duration`,`privacy`,`version`,`status`,`players`,`topPlayer`,`currentPlayer`,`createdAt`,`updatedAt`,`completedAt`,`isFirst`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Oi extends Hh<Ms4> {
        @DexIgnore
        public Oi(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(com.mapped.Mi mi, Ms4 ms4) {
            if (ms4.d() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, ms4.d());
            }
            if (ms4.i() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, ms4.i());
            }
            if (ms4.c() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, ms4.c());
            }
            if (ms4.e() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, ms4.e());
            }
            Integer valueOf = ms4.p() == null ? null : Integer.valueOf(ms4.p().booleanValue() ? 1 : 0);
            if (valueOf == null) {
                mi.bindNull(5);
            } else {
                mi.bindLong(5, (long) valueOf.intValue());
            }
            if (ms4.h() == null) {
                mi.bindNull(6);
            } else {
                mi.bindLong(6, (long) ms4.h().intValue());
            }
            if (ms4.k() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, ms4.k());
            }
            if (ms4.m() == null) {
                mi.bindNull(8);
            } else {
                mi.bindLong(8, (long) ms4.m().intValue());
            }
            if (ms4.n() == null) {
                mi.bindNull(9);
            } else {
                mi.bindLong(9, (long) ms4.n().intValue());
            }
            if (ms4.a() == null) {
                mi.bindNull(10);
            } else {
                mi.bindLong(10, (long) ms4.a().intValue());
            }
            if (ms4.b() == null) {
                mi.bindNull(11);
            } else {
                mi.bindLong(11, (long) ms4.b().intValue());
            }
            if (ms4.g() == null) {
                mi.bindNull(12);
            } else {
                mi.bindString(12, ms4.g());
            }
            String a2 = Rs4.this.c.a(ms4.f());
            if (a2 == null) {
                mi.bindNull(13);
            } else {
                mi.bindString(13, a2);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(com.mapped.Mi mi, Ms4 ms4) {
            a(mi, ms4);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `players` (`id`,`socialId`,`firstName`,`lastName`,`isDeleted`,`ranking`,`status`,`stepsBase`,`stepsOffset`,`caloriesBase`,`caloriesOffset`,`profilePicture`,`lastSync`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Pi extends Gh<Bt4> {
        @DexIgnore
        public Pi(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void a(com.mapped.Mi mi, Bt4 bt4) {
            if (bt4.g() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, bt4.g());
            }
            if (bt4.q() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, bt4.q());
            }
            if (bt4.h() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, bt4.h());
            }
            if (bt4.d() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, bt4.d());
            }
            String a2 = Rs4.this.h.a(bt4.j());
            if (a2 == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, a2);
            }
            if (bt4.i() == null) {
                mi.bindNull(6);
            } else {
                mi.bindLong(6, (long) bt4.i().intValue());
            }
            String a3 = Rs4.this.c.a(bt4.m());
            if (a3 == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, a3);
            }
            String a4 = Rs4.this.c.a(bt4.f());
            if (a4 == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, a4);
            }
            if (bt4.o() == null) {
                mi.bindNull(9);
            } else {
                mi.bindLong(9, (long) bt4.o().intValue());
            }
            if (bt4.e() == null) {
                mi.bindNull(10);
            } else {
                mi.bindLong(10, (long) bt4.e().intValue());
            }
            if (bt4.l() == null) {
                mi.bindNull(11);
            } else {
                mi.bindString(11, bt4.l());
            }
            if (bt4.s() == null) {
                mi.bindNull(12);
            } else {
                mi.bindString(12, bt4.s());
            }
            if (bt4.n() == null) {
                mi.bindNull(13);
            } else {
                mi.bindString(13, bt4.n());
            }
            String c = Rs4.this.h.c(bt4.k());
            if (c == null) {
                mi.bindNull(14);
            } else {
                mi.bindString(14, c);
            }
            String b = Rs4.this.h.b(bt4.p());
            if (b == null) {
                mi.bindNull(15);
            } else {
                mi.bindString(15, b);
            }
            String b2 = Rs4.this.h.b(bt4.c());
            if (b2 == null) {
                mi.bindNull(16);
            } else {
                mi.bindString(16, b2);
            }
            String a5 = Rs4.this.c.a(bt4.b());
            if (a5 == null) {
                mi.bindNull(17);
            } else {
                mi.bindString(17, a5);
            }
            String a6 = Rs4.this.c.a(bt4.r());
            if (a6 == null) {
                mi.bindNull(18);
            } else {
                mi.bindString(18, a6);
            }
            String a7 = Rs4.this.c.a(bt4.a());
            if (a7 == null) {
                mi.bindNull(19);
            } else {
                mi.bindString(19, a7);
            }
            mi.bindLong(20, bt4.t() ? 1 : 0);
            if (bt4.g() == null) {
                mi.bindNull(21);
            } else {
                mi.bindString(21, bt4.g());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Gh
        public /* bridge */ /* synthetic */ void bind(com.mapped.Mi mi, Bt4 bt4) {
            a(mi, bt4);
        }

        @DexIgnore
        @Override // com.mapped.Vh, com.mapped.Gh
        public String createQuery() {
            return "UPDATE OR ABORT `history_challenge` SET `id` = ?,`type` = ?,`name` = ?,`des` = ?,`owner` = ?,`numberOfPlayers` = ?,`startTime` = ?,`endTime` = ?,`target` = ?,`duration` = ?,`privacy` = ?,`version` = ?,`status` = ?,`players` = ?,`topPlayer` = ?,`currentPlayer` = ?,`createdAt` = ?,`updatedAt` = ?,`completedAt` = ?,`isFirst` = ? WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Qi extends Vh {
        @DexIgnore
        public Qi(Rs4 rs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM challenge WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ri extends Vh {
        @DexIgnore
        public Ri(Rs4 rs4, Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM challenge";
        }
    }

    @DexIgnore
    public Rs4(Oh oh) {
        this.a = oh;
        this.b = new Hi(oh);
        this.d = new Ki(this, oh);
        this.e = new Li(oh);
        new Mi(oh);
        this.g = new Ni(oh);
        this.i = new Oi(oh);
        this.j = new Pi(oh);
        this.k = new Qi(this, oh);
        this.l = new Ri(this, oh);
        this.m = new Ai(this, oh);
        this.n = new Bi(this, oh);
        this.o = new Ci(this, oh);
        this.p = new Di(this, oh);
        this.q = new Ei(this, oh);
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public List<Ms4> a() {
        Boolean valueOf;
        Rh f2 = Rh.f("SELECT * FROM players", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, "socialId");
            int c4 = Dx0.c(b2, Constants.PROFILE_KEY_FIRST_NAME);
            int c5 = Dx0.c(b2, Constants.PROFILE_KEY_LAST_NAME);
            int c6 = Dx0.c(b2, "isDeleted");
            int c7 = Dx0.c(b2, "ranking");
            int c8 = Dx0.c(b2, "status");
            int c9 = Dx0.c(b2, "stepsBase");
            int c10 = Dx0.c(b2, "stepsOffset");
            int c11 = Dx0.c(b2, "caloriesBase");
            int c12 = Dx0.c(b2, "caloriesOffset");
            int c13 = Dx0.c(b2, Constants.PROFILE_KEY_PROFILE_PIC);
            int c14 = Dx0.c(b2, "lastSync");
            try {
                try {
                    ArrayList arrayList = new ArrayList(b2.getCount());
                    while (b2.moveToNext()) {
                        String string = b2.getString(c2);
                        String string2 = b2.getString(c3);
                        String string3 = b2.getString(c4);
                        String string4 = b2.getString(c5);
                        Integer valueOf2 = b2.isNull(c6) ? null : Integer.valueOf(b2.getInt(c6));
                        if (valueOf2 == null) {
                            valueOf = null;
                        } else {
                            valueOf = Boolean.valueOf(valueOf2.intValue() != 0);
                        }
                        try {
                            arrayList.add(new Ms4(string, string2, string3, string4, valueOf, b2.isNull(c7) ? null : Integer.valueOf(b2.getInt(c7)), b2.getString(c8), b2.isNull(c9) ? null : Integer.valueOf(b2.getInt(c9)), b2.isNull(c10) ? null : Integer.valueOf(b2.getInt(c10)), b2.isNull(c11) ? null : Integer.valueOf(b2.getInt(c11)), b2.isNull(c12) ? null : Integer.valueOf(b2.getInt(c12)), b2.getString(c13), this.c.d(b2.getString(c14))));
                        } catch (Throwable th) {
                            th = th;
                        }
                    }
                    b2.close();
                    f2.m();
                    return arrayList;
                } catch (Throwable th2) {
                    th = th2;
                    b2.close();
                    f2.m();
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public int b(String str) {
        this.a.assertNotSuspendingTransaction();
        com.mapped.Mi acquire = this.k.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
            this.k.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public LiveData<Ps4> c(String[] strArr, Date date) {
        StringBuilder b2 = Hx0.b();
        b2.append("SELECT ");
        b2.append(G78.ANY_MARKER);
        b2.append(" FROM challenge WHERE status IN (");
        int length = strArr.length;
        Hx0.a(b2, length);
        b2.append(") AND endTime > ");
        b2.append("?");
        b2.append(" LIMIT 1");
        int i2 = length + 1;
        Rh f2 = Rh.f(b2.toString(), i2);
        int i3 = 1;
        for (String str : strArr) {
            if (str == null) {
                f2.bindNull(i3);
            } else {
                f2.bindString(i3, str);
            }
            i3++;
        }
        String a2 = this.c.a(date);
        if (a2 == null) {
            f2.bindNull(i2);
        } else {
            f2.bindString(i2, a2);
        }
        return this.a.getInvalidationTracker().d(new String[]{"challenge"}, false, new Gi(f2));
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public Ks4 d(String str) {
        Ks4 ks4 = null;
        Rh f2 = Rh.f("SELECT * FROM display_player WHERE challengeId = ?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "challengeId");
            int c3 = Dx0.c(b2, "numberOfPlayers");
            int c4 = Dx0.c(b2, "topPlayers");
            int c5 = Dx0.c(b2, "nearPlayers");
            if (b2.moveToFirst()) {
                ks4 = new Ks4(b2.getString(c2), b2.isNull(c3) ? null : Integer.valueOf(b2.getInt(c3)), this.f.b(b2.getString(c4)), this.f.b(b2.getString(c5)));
            }
            return ks4;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public Bt4 e(String str) {
        Throwable th;
        Bt4 bt4;
        Rh f2 = Rh.f("SELECT * FROM history_challenge WHERE id = ?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, "type");
            int c4 = Dx0.c(b2, "name");
            int c5 = Dx0.c(b2, "des");
            int c6 = Dx0.c(b2, "owner");
            int c7 = Dx0.c(b2, "numberOfPlayers");
            int c8 = Dx0.c(b2, SampleRaw.COLUMN_START_TIME);
            int c9 = Dx0.c(b2, SampleRaw.COLUMN_END_TIME);
            int c10 = Dx0.c(b2, "target");
            int c11 = Dx0.c(b2, "duration");
            int c12 = Dx0.c(b2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
            int c13 = Dx0.c(b2, "version");
            int c14 = Dx0.c(b2, "status");
            try {
                int c15 = Dx0.c(b2, "players");
                int c16 = Dx0.c(b2, "topPlayer");
                int c17 = Dx0.c(b2, "currentPlayer");
                int c18 = Dx0.c(b2, "createdAt");
                int c19 = Dx0.c(b2, "updatedAt");
                int c20 = Dx0.c(b2, "completedAt");
                int c21 = Dx0.c(b2, "isFirst");
                if (b2.moveToFirst()) {
                    bt4 = new Bt4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), this.h.d(b2.getString(c6)), b2.isNull(c7) ? null : Integer.valueOf(b2.getInt(c7)), this.c.d(b2.getString(c8)), this.c.d(b2.getString(c9)), b2.isNull(c10) ? null : Integer.valueOf(b2.getInt(c10)), b2.isNull(c11) ? null : Integer.valueOf(b2.getInt(c11)), b2.getString(c12), b2.getString(c13), b2.getString(c14), this.h.f(b2.getString(c15)), this.h.e(b2.getString(c16)), this.h.e(b2.getString(c17)), this.c.d(b2.getString(c18)), this.c.d(b2.getString(c19)), this.c.d(b2.getString(c20)), b2.getInt(c21) != 0);
                } else {
                    bt4 = null;
                }
                b2.close();
                f2.m();
                return bt4;
            } catch (Throwable th2) {
                th = th2;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public Ps4 f(String[] strArr, Date date) {
        Throwable th;
        StringBuilder b2 = Hx0.b();
        b2.append("SELECT ");
        b2.append(G78.ANY_MARKER);
        b2.append(" FROM challenge WHERE status IN (");
        int length = strArr.length;
        Hx0.a(b2, length);
        b2.append(") AND endTime > ");
        b2.append("?");
        b2.append(" LIMIT 1");
        String sb = b2.toString();
        int i2 = 1;
        int i3 = length + 1;
        Rh f2 = Rh.f(sb, i3);
        for (String str : strArr) {
            if (str == null) {
                f2.bindNull(i2);
            } else {
                f2.bindString(i2, str);
            }
            i2++;
        }
        String a2 = this.c.a(date);
        if (a2 == null) {
            f2.bindNull(i3);
        } else {
            f2.bindString(i3, a2);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b3 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b3, "id");
            int c3 = Dx0.c(b3, "type");
            int c4 = Dx0.c(b3, "name");
            int c5 = Dx0.c(b3, "des");
            int c6 = Dx0.c(b3, "owner");
            int c7 = Dx0.c(b3, "numberOfPlayers");
            int c8 = Dx0.c(b3, SampleRaw.COLUMN_START_TIME);
            int c9 = Dx0.c(b3, SampleRaw.COLUMN_END_TIME);
            int c10 = Dx0.c(b3, "target");
            int c11 = Dx0.c(b3, "duration");
            try {
                Ps4 ps4 = b3.moveToFirst() ? new Ps4(b3.getString(c2), b3.getString(c3), b3.getString(c4), b3.getString(c5), this.c.c(b3.getString(c6)), b3.isNull(c7) ? null : Integer.valueOf(b3.getInt(c7)), this.c.d(b3.getString(c8)), this.c.d(b3.getString(c9)), b3.isNull(c10) ? null : Integer.valueOf(b3.getInt(c10)), b3.isNull(c11) ? null : Integer.valueOf(b3.getInt(c11)), b3.getString(Dx0.c(b3, ShareConstants.WEB_DIALOG_PARAM_PRIVACY)), b3.getString(Dx0.c(b3, "version")), b3.getString(Dx0.c(b3, "status")), b3.getString(Dx0.c(b3, "syncData")), this.c.d(b3.getString(Dx0.c(b3, "createdAt"))), this.c.d(b3.getString(Dx0.c(b3, "updatedAt"))), b3.getString(Dx0.c(b3, "category"))) : null;
                b3.close();
                f2.m();
                return ps4;
            } catch (Throwable th2) {
                th = th2;
                b3.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b3.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public void g() {
        this.a.assertNotSuspendingTransaction();
        com.mapped.Mi acquire = this.l.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.l.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public long h(Ls4 ls4) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            long insertAndReturnId = this.d.insertAndReturnId(ls4);
            this.a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public LiveData<List<Bt4>> i() {
        Rh f2 = Rh.f("SELECT * FROM history_challenge ORDER BY completedAt DESC LIMIT 15", 0);
        Nw0 invalidationTracker = this.a.getInvalidationTracker();
        Ii ii = new Ii(f2);
        return invalidationTracker.d(new String[]{"history_challenge"}, false, ii);
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public Long[] insert(List<Ps4> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public List<Ls4> j() {
        Rh f2 = Rh.f("SELECT * FROM fitness", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "pinType");
            int c3 = Dx0.c(b2, "id");
            int c4 = Dx0.c(b2, "encryptMethod");
            int c5 = Dx0.c(b2, "encryptedData");
            int c6 = Dx0.c(b2, "keyType");
            int c7 = Dx0.c(b2, "sequence");
            ArrayList arrayList = new ArrayList(b2.getCount());
            while (b2.moveToNext()) {
                Ls4 ls4 = new Ls4(b2.getInt(c3), b2.getInt(c4), b2.getString(c5), b2.getInt(c6), b2.getString(c7));
                ls4.g(b2.getInt(c2));
                arrayList.add(ls4);
            }
            return arrayList;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public long k(Ps4 ps4) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(ps4);
            this.a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public LiveData<Bt4> l(String str) {
        Rh f2 = Rh.f("SELECT * FROM history_challenge WHERE id = ? limit 1", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        Nw0 invalidationTracker = this.a.getInvalidationTracker();
        Ji ji = new Ji(f2);
        return invalidationTracker.d(new String[]{"history_challenge"}, false, ji);
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public Ps4 m(String str) {
        Throwable th;
        Ps4 ps4;
        Rh f2 = Rh.f("SELECT * FROM challenge WHERE id = ?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            int c2 = Dx0.c(b2, "id");
            int c3 = Dx0.c(b2, "type");
            int c4 = Dx0.c(b2, "name");
            int c5 = Dx0.c(b2, "des");
            int c6 = Dx0.c(b2, "owner");
            int c7 = Dx0.c(b2, "numberOfPlayers");
            int c8 = Dx0.c(b2, SampleRaw.COLUMN_START_TIME);
            int c9 = Dx0.c(b2, SampleRaw.COLUMN_END_TIME);
            int c10 = Dx0.c(b2, "target");
            int c11 = Dx0.c(b2, "duration");
            int c12 = Dx0.c(b2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
            int c13 = Dx0.c(b2, "version");
            int c14 = Dx0.c(b2, "status");
            try {
                int c15 = Dx0.c(b2, "syncData");
                int c16 = Dx0.c(b2, "createdAt");
                int c17 = Dx0.c(b2, "updatedAt");
                int c18 = Dx0.c(b2, "category");
                if (b2.moveToFirst()) {
                    ps4 = new Ps4(b2.getString(c2), b2.getString(c3), b2.getString(c4), b2.getString(c5), this.c.c(b2.getString(c6)), b2.isNull(c7) ? null : Integer.valueOf(b2.getInt(c7)), this.c.d(b2.getString(c8)), this.c.d(b2.getString(c9)), b2.isNull(c10) ? null : Integer.valueOf(b2.getInt(c10)), b2.isNull(c11) ? null : Integer.valueOf(b2.getInt(c11)), b2.getString(c12), b2.getString(c13), b2.getString(c14), b2.getString(c15), this.c.d(b2.getString(c16)), this.c.d(b2.getString(c17)), b2.getString(c18));
                } else {
                    ps4 = null;
                }
                b2.close();
                f2.m();
                return ps4;
            } catch (Throwable th2) {
                th = th2;
                b2.close();
                f2.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b2.close();
            f2.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public int n(String str) {
        int i2 = 0;
        Rh f2 = Rh.f("SELECT COUNT(*) FROM challenge WHERE id =?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor b2 = Ex0.b(this.a, f2, false, null);
        try {
            if (b2.moveToFirst()) {
                i2 = b2.getInt(0);
            }
            return i2;
        } finally {
            b2.close();
            f2.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public void o() {
        this.a.assertNotSuspendingTransaction();
        com.mapped.Mi acquire = this.p.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.p.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public void p() {
        this.a.assertNotSuspendingTransaction();
        com.mapped.Mi acquire = this.q.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.q.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public void q() {
        this.a.assertNotSuspendingTransaction();
        com.mapped.Mi acquire = this.o.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.o.release(acquire);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    @Override // com.fossil.Qs4
    public int r(Bt4 bt4) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            int handle = this.j.handle(bt4);
            this.a.setTransactionSuccessful();
            this.a.endTransaction();
            return handle + 0;
        } catch (Throwable th) {
            this.a.endTransaction();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public LiveData<Ps4> s(String str) {
        Rh f2 = Rh.f("SELECT * FROM challenge WHERE id = ?", 1);
        if (str == null) {
            f2.bindNull(1);
        } else {
            f2.bindString(1, str);
        }
        Nw0 invalidationTracker = this.a.getInvalidationTracker();
        Fi fi = new Fi(f2);
        return invalidationTracker.d(new String[]{"challenge"}, false, fi);
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public Long[] t(List<Bt4> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.g.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public void u(String[] strArr) {
        this.a.assertNotSuspendingTransaction();
        StringBuilder b2 = Hx0.b();
        b2.append("DELETE FROM challenge WHERE status IN (");
        Hx0.a(b2, strArr.length);
        b2.append(")");
        com.mapped.Mi compileStatement = this.a.compileStatement(b2.toString());
        int i2 = 1;
        for (String str : strArr) {
            if (str == null) {
                compileStatement.bindNull(i2);
            } else {
                compileStatement.bindString(i2, str);
            }
            i2++;
        }
        this.a.beginTransaction();
        try {
            compileStatement.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public Long[] v(List<Ms4> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.i.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public void w() {
        this.a.assertNotSuspendingTransaction();
        com.mapped.Mi acquire = this.m.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.m.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public void x() {
        this.a.assertNotSuspendingTransaction();
        com.mapped.Mi acquire = this.n.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.n.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.Qs4
    public Long[] y(List<Ks4> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.e.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }
}
