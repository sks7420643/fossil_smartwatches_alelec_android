package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class I77 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ Ai b; // = new Ai();
    @DexIgnore
    public static /* final */ I77 c; // = new I77();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements BleCommandResultManager.Bi {
        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            boolean z = true;
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            ServiceActionResult serviceActionResult = ServiceActionResult.values()[intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), ServiceActionResult.UNALLOWED_ACTION.ordinal())];
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
            if (integerArrayListExtra == null) {
                integerArrayListExtra = new ArrayList<>(intExtra);
            }
            Bundle extras = intent.getExtras();
            byte[] byteArray = extras != null ? extras.getByteArray(ButtonService.THEME_BINARY_EXTRA) : null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            I77 i77 = I77.c;
            local.e(I77.a, "errorCode " + intExtra + " permissionErrorCodes " + integerArrayListExtra + " resultCode: " + serviceActionResult + " - themeBinary: " + byteArray);
            int i = H77.a[communicateMode.ordinal()];
            if (i == 1) {
                I77 i772 = I77.c;
                if (serviceActionResult != ServiceActionResult.SUCCEEDED) {
                    z = false;
                }
                I77.e(i772, "preview_theme_action", z, intExtra, null, 8, null);
            } else if (i == 2) {
                I77 i773 = I77.c;
                if (serviceActionResult != ServiceActionResult.SUCCEEDED) {
                    z = false;
                }
                i773.d("apply_theme_action", z, intExtra, byteArray);
            } else if (i == 3) {
                I77.c.d("parse_theme_data_to_bin_action", serviceActionResult == ServiceActionResult.SUCCEEDED, intExtra, byteArray);
            }
        }
    }

    /*
    static {
        String simpleName = I77.class.getSimpleName();
        Wg6.b(simpleName, "WFBleConnection::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public static /* synthetic */ void e(I77 i77, String str, boolean z, int i, byte[] bArr, int i2, Object obj) {
        if ((i2 & 8) != 0) {
            bArr = null;
        }
        i77.d(str, z, i, bArr);
    }

    @DexIgnore
    public final void c() {
        BleCommandResultManager.d.e(b, CommunicateMode.PREVIEW_THEME_SESSION, CommunicateMode.APPLY_THEME_SESSION, CommunicateMode.PARSE_THEME_DATA_TO_BINARY);
    }

    @DexIgnore
    public final void d(String str, boolean z, int i, byte[] bArr) {
        Intent intent = new Intent();
        intent.setAction(str);
        intent.putExtra("theme_result_extra", z);
        if (bArr != null) {
            intent.putExtra(ButtonService.THEME_BINARY_EXTRA, bArr);
        }
        intent.putExtra("error_code_extra", i);
        Ct0.b(PortfolioApp.get.instance()).d(intent);
    }

    @DexIgnore
    public final void f() {
        BleCommandResultManager.d.j(b, CommunicateMode.PREVIEW_THEME_SESSION, CommunicateMode.APPLY_THEME_SESSION, CommunicateMode.PARSE_THEME_DATA_TO_BINARY);
    }
}
