package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ny0 {
    @DexIgnore
    public static /* final */ int action_container; // = 2131361864;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361866;
    @DexIgnore
    public static /* final */ int action_image; // = 2131361867;
    @DexIgnore
    public static /* final */ int action_text; // = 2131361873;
    @DexIgnore
    public static /* final */ int actions; // = 2131361874;
    @DexIgnore
    public static /* final */ int async; // = 2131361895;
    @DexIgnore
    public static /* final */ int blocking; // = 2131361918;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131362026;
    @DexIgnore
    public static /* final */ int forever; // = 2131362328;
    @DexIgnore
    public static /* final */ int ghost_view; // = 2131362565;
    @DexIgnore
    public static /* final */ int ghost_view_holder; // = 2131362566;
    @DexIgnore
    public static /* final */ int icon; // = 2131362608;
    @DexIgnore
    public static /* final */ int icon_group; // = 2131362609;
    @DexIgnore
    public static /* final */ int info; // = 2131362633;
    @DexIgnore
    public static /* final */ int italic; // = 2131362649;
    @DexIgnore
    public static /* final */ int line1; // = 2131362784;
    @DexIgnore
    public static /* final */ int line3; // = 2131362786;
    @DexIgnore
    public static /* final */ int normal; // = 2131362883;
    @DexIgnore
    public static /* final */ int notification_background; // = 2131362884;
    @DexIgnore
    public static /* final */ int notification_main_column; // = 2131362885;
    @DexIgnore
    public static /* final */ int notification_main_column_container; // = 2131362886;
    @DexIgnore
    public static /* final */ int parent_matrix; // = 2131362920;
    @DexIgnore
    public static /* final */ int right_icon; // = 2131362997;
    @DexIgnore
    public static /* final */ int right_side; // = 2131362998;
    @DexIgnore
    public static /* final */ int save_non_transition_alpha; // = 2131363070;
    @DexIgnore
    public static /* final */ int save_overlay_view; // = 2131363071;
    @DexIgnore
    public static /* final */ int tag_transition_group; // = 2131363181;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_event_manager; // = 2131363182;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_listeners; // = 2131363183;
    @DexIgnore
    public static /* final */ int text; // = 2131363190;
    @DexIgnore
    public static /* final */ int text2; // = 2131363191;
    @DexIgnore
    public static /* final */ int time; // = 2131363213;
    @DexIgnore
    public static /* final */ int title; // = 2131363215;
    @DexIgnore
    public static /* final */ int transition_current_scene; // = 2131363229;
    @DexIgnore
    public static /* final */ int transition_layout_save; // = 2131363230;
    @DexIgnore
    public static /* final */ int transition_position; // = 2131363231;
    @DexIgnore
    public static /* final */ int transition_scene_layoutid_cache; // = 2131363232;
    @DexIgnore
    public static /* final */ int transition_transform; // = 2131363233;
}
