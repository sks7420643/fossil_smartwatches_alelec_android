package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kb3 extends Mq2 implements Ib3 {
    @DexIgnore
    public Kb3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.ILocationCallback");
    }

    @DexIgnore
    @Override // com.fossil.Ib3
    public final void S1(LocationResult locationResult) throws RemoteException {
        Parcel d = d();
        Qr2.c(d, locationResult);
        n(1, d);
    }

    @DexIgnore
    @Override // com.fossil.Ib3
    public final void j1(LocationAvailability locationAvailability) throws RemoteException {
        Parcel d = d();
        Qr2.c(d, locationAvailability);
        n(2, d);
    }
}
