package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mb {
    @DexIgnore
    public /* synthetic */ Mb(Qg6 qg6) {
    }

    @DexIgnore
    public final Ob a(byte b) {
        Ob[] values = Ob.values();
        for (Ob ob : values) {
            if (ob.c == b) {
                return ob;
            }
        }
        return null;
    }

    @DexIgnore
    public final Ob b(short s) {
        Ob[] values = Ob.values();
        for (Ob ob : values) {
            short s2 = ob.b;
            if (((short) (s | s2)) == s2) {
                return ob;
            }
        }
        return null;
    }
}
