package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class E44 extends I44<Comparable> implements Serializable {
    @DexIgnore
    public static /* final */ E44 INSTANCE; // = new E44();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public transient I44<Comparable> b;
    @DexIgnore
    public transient I44<Comparable> c;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    public int compare(Comparable comparable, Comparable comparable2) {
        I14.l(comparable);
        I14.l(comparable2);
        return comparable.compareTo(comparable2);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.I44, java.util.Comparator
    public /* bridge */ /* synthetic */ int compare(Comparable comparable, Comparable comparable2) {
        return compare(comparable, comparable2);
    }

    @DexIgnore
    @Override // com.fossil.I44
    public <S extends Comparable> I44<S> nullsFirst() {
        I44<S> i44 = (I44<S>) this.b;
        if (i44 != null) {
            return i44;
        }
        I44<S> nullsFirst = super.nullsFirst();
        this.b = nullsFirst;
        return nullsFirst;
    }

    @DexIgnore
    @Override // com.fossil.I44
    public <S extends Comparable> I44<S> nullsLast() {
        I44<S> i44 = (I44<S>) this.c;
        if (i44 != null) {
            return i44;
        }
        I44<S> nullsLast = super.nullsLast();
        this.c = nullsLast;
        return nullsLast;
    }

    @DexIgnore
    @Override // com.fossil.I44
    public <S extends Comparable> I44<S> reverse() {
        return T44.INSTANCE;
    }

    @DexIgnore
    public String toString() {
        return "Ordering.natural()";
    }
}
