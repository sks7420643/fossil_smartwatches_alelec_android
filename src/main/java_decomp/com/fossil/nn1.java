package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Nn1 {
    DC(N6.i),
    FTC(N6.j),
    FTD(N6.k),
    AUT(N6.n),
    ASYNC(N6.l),
    FTD_1(N6.m),
    HEART_RATE(N6.o);
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ N6 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }
    }

    @DexIgnore
    public Nn1(N6 n6) {
        this.b = n6;
    }

    @DexIgnore
    public final N6 a() {
        return this.b;
    }
}
