package com.fossil;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ot0 implements Mt0 {
    @DexIgnore
    public String a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public Ot0(String str, int i, int i2) {
        this.a = str;
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Ot0)) {
            return false;
        }
        Ot0 ot0 = (Ot0) obj;
        return TextUtils.equals(this.a, ot0.a) && this.b == ot0.b && this.c == ot0.c;
    }

    @DexIgnore
    public int hashCode() {
        return Kn0.b(this.a, Integer.valueOf(this.b), Integer.valueOf(this.c));
    }
}
