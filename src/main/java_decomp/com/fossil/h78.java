package com.fossil;

import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class H78 implements E78 {
    @DexIgnore
    public String b;
    @DexIgnore
    public P78 c;
    @DexIgnore
    public Queue<K78> d;

    @DexIgnore
    public H78(P78 p78, Queue<K78> queue) {
        this.c = p78;
        this.b = p78.c();
        this.d = queue;
    }

    @DexIgnore
    public final void a(I78 i78, String str, Object[] objArr, Throwable th) {
        b(i78, null, str, objArr, th);
    }

    @DexIgnore
    public final void b(I78 i78, G78 g78, String str, Object[] objArr, Throwable th) {
        K78 k78 = new K78();
        k78.j(System.currentTimeMillis());
        k78.c(i78);
        k78.d(this.c);
        k78.e(this.b);
        k78.f(g78);
        k78.g(str);
        k78.b(objArr);
        k78.i(th);
        k78.h(Thread.currentThread().getName());
        this.d.add(k78);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void debug(String str) {
        a(I78.TRACE, str, null, null);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void debug(String str, Throwable th) {
        a(I78.DEBUG, str, null, th);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void error(String str) {
        a(I78.ERROR, str, null, null);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void error(String str, Throwable th) {
        a(I78.ERROR, str, null, th);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void error(String str, Object... objArr) {
        a(I78.ERROR, str, objArr, null);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void info(String str) {
        a(I78.INFO, str, null, null);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void info(String str, Object obj) {
        a(I78.INFO, str, new Object[]{obj}, null);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void info(String str, Throwable th) {
        a(I78.INFO, str, null, th);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public boolean isDebugEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.E78
    public boolean isErrorEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.E78
    public boolean isInfoEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.E78
    public boolean isTraceEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.E78
    public boolean isWarnEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void trace(String str) {
        a(I78.TRACE, str, null, null);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void trace(String str, Throwable th) {
        a(I78.TRACE, str, null, th);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void warn(String str) {
        a(I78.WARN, str, null, null);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void warn(String str, Object obj, Object obj2) {
        a(I78.WARN, str, new Object[]{obj, obj2}, null);
    }

    @DexIgnore
    @Override // com.fossil.E78
    public void warn(String str, Throwable th) {
        a(I78.WARN, str, null, th);
    }
}
