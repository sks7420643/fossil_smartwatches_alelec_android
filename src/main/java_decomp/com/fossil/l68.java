package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class L68 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ M68 b;
    @DexIgnore
    public /* final */ T68 c;

    @DexIgnore
    public L68(String str, T68 t68) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        } else if (t68 != null) {
            this.a = str;
            this.c = t68;
            this.b = new M68();
            b(t68);
            c(t68);
            d(t68);
        } else {
            throw new IllegalArgumentException("Body may not be null");
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        if (str != null) {
            this.b.a(new Q68(str, str2));
            return;
        }
        throw new IllegalArgumentException("Field name may not be null");
    }

    @DexIgnore
    public void b(T68 t68) {
        StringBuilder sb = new StringBuilder();
        sb.append("form-data; name=\"");
        sb.append(g());
        sb.append("\"");
        if (t68.d() != null) {
            sb.append("; filename=\"");
            sb.append(t68.d());
            sb.append("\"");
        }
        a("Content-Disposition", sb.toString());
    }

    @DexIgnore
    public void c(T68 t68) {
        StringBuilder sb = new StringBuilder();
        sb.append(t68.c());
        if (t68.b() != null) {
            sb.append("; charset=");
            sb.append(t68.b());
        }
        a("Content-Type", sb.toString());
    }

    @DexIgnore
    public void d(T68 t68) {
        a("Content-Transfer-Encoding", t68.a());
    }

    @DexIgnore
    public T68 e() {
        return this.c;
    }

    @DexIgnore
    public M68 f() {
        return this.b;
    }

    @DexIgnore
    public String g() {
        return this.a;
    }
}
