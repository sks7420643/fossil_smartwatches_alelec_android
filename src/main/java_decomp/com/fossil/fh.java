package com.fossil;

import com.mapped.Q40;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fh extends Lp {
    @DexIgnore
    public static /* final */ ArrayList<N6> Q; // = Hm7.c(N6.i, N6.j, N6.k, N6.n, N6.l, N6.m);
    @DexIgnore
    public Zk1 C;
    @DexIgnore
    public /* final */ ArrayList<Ow> D; // = new ArrayList<>();
    @DexIgnore
    public long E; // = System.currentTimeMillis();
    @DexIgnore
    public long F;
    @DexIgnore
    public int G;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<N6> H;
    @DexIgnore
    public int I;
    @DexIgnore
    public long J;
    @DexIgnore
    public long K;
    @DexIgnore
    public boolean L;
    @DexIgnore
    public boolean M;
    @DexIgnore
    public int N;
    @DexIgnore
    public int O;
    @DexIgnore
    public /* final */ HashMap<Ym, Object> P;

    @DexIgnore
    public Fh(K5 k5, I60 i60, HashMap<Ym, Object> hashMap, String str) {
        super(k5, i60, Yp.c, str, false, 16);
        this.P = hashMap;
        this.C = new Zk1(k5.C(), k5.x, "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
        Gm7.b(Hd0.y.u());
        Hm7.h(N6.e, N6.f, N6.g, N6.i, N6.j, N6.k, N6.n, N6.l);
        this.H = new CopyOnWriteArrayList<>();
    }

    @DexIgnore
    public static final /* synthetic */ void H(Fh fh, long j) {
        if (fh.G < 2) {
            fh.b = null;
            fh.o.postDelayed(new Eh(fh), j);
            return;
        }
        Nr nr = fh.v;
        fh.K(Nr.a(nr, null, Zq.I.a(nr.d), null, null, 13));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        I60 i60 = this.x;
        if (i60.a.v == Q40.Ci.CONNECTED) {
            this.C = i60.a();
            l(Nr.a(this.v, null, Zq.b, null, null, 13));
            return;
        }
        this.E = System.currentTimeMillis();
        this.M = true;
        V();
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return G80.k(G80.k(super.C(), Jd0.o1, Ey1.a(this.w.H())), Jd0.f5, Integer.valueOf(this.w.A.getType()));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        return G80.k(G80.k(G80.k(super.E(), Jd0.k2, Long.valueOf(this.F)), Jd0.l2, Integer.valueOf(this.N)), Jd0.m2, Integer.valueOf(this.O));
    }

    @DexIgnore
    public final void K(Nr nr) {
        if (nr.c == Zq.b) {
            l(nr);
            return;
        }
        this.v = nr;
        this.O++;
        Lp.i(this, new Os(this.w), Jq.b, Xq.b, null, new Lr(this), null, 40, null);
    }

    @DexIgnore
    public final void R() {
        Lp.i(this, new Bv(Ob.y.b, this.w, 0, 4), Wn.b, Io.b, null, new Vo(this), null, 40, null);
    }

    @DexIgnore
    public final void S() {
        Lp.i(this, new Uu(this.w), new Fj(this), Rj.b, null, new Dk(this), null, 40, null);
    }

    @DexIgnore
    public final void T() {
        Lp.i(this, new Yu(Cx1.f.f(), this.w), Om.b, Zm.b, null, new Ln(this), null, 40, null);
    }

    @DexIgnore
    public final void U() {
        int i;
        Ky1 ky1 = Ky1.DEBUG;
        if (this.I >= this.H.size()) {
            Hashtable hashtable = new Hashtable();
            hashtable.put(Ow.b, Integer.MAX_VALUE);
            for (T t : this.H) {
                Ow a2 = t.a();
                switch (M6.b[t.ordinal()]) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        i = Integer.MAX_VALUE;
                        break;
                    case 8:
                    case 9:
                    case 10:
                        i = 1;
                        break;
                    default:
                        i = 0;
                        break;
                }
                Integer num = (Integer) hashtable.get(a2);
                if (num == null) {
                    num = 0;
                }
                Wg6.b(num, "resourceQuotas[resourceType] ?: 0");
                hashtable.put(a2, Integer.valueOf(num.intValue() + i));
            }
            this.x.b().d(new Nw(hashtable, null));
            Lp.h(this, new Ah(this.w, this.x, this.z), new Pk(this), new Cl(this), null, null, null, 56, null);
            return;
        }
        N6 n6 = this.H.get(this.I);
        Wg6.b(n6, "characteristicsToSubscri\u2026ibingCharacteristicIndex]");
        Lp.i(this, new Vs(n6, true, this.w), new Xn(this), new Jo(this), null, null, null, 56, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void V() {
        /*
            r14 = this;
            r12 = 0
            r4 = 0
            r3 = 0
            boolean r0 = r14.L
            if (r0 != 0) goto L_0x001a
            java.util.HashMap<com.fossil.Ym, java.lang.Object> r0 = r14.P
            com.fossil.Ym r1 = com.fossil.Ym.c
            java.lang.Object r0 = r0.get(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            if (r0 == 0) goto L_0x0042
            boolean r0 = r0.booleanValue()
        L_0x0018:
            if (r0 == 0) goto L_0x0044
        L_0x001a:
            r0 = 1
            r2 = r0
        L_0x001c:
            r14.L = r3
            java.util.HashMap<com.fossil.Ym, java.lang.Object> r0 = r14.P
            com.fossil.Ym r1 = com.fossil.Ym.b
            java.lang.Object r0 = r0.get(r1)
            java.lang.Long r0 = (java.lang.Long) r0
            if (r0 == 0) goto L_0x0046
            long r0 = r0.longValue()
        L_0x002e:
            com.fossil.Q3 r5 = com.fossil.Q3.f
            long r6 = r5.e(r2)
            int r5 = (r0 > r12 ? 1 : (r0 == r12 ? 0 : -1))
            if (r5 != 0) goto L_0x0049
        L_0x0038:
            int r0 = (r6 > r12 ? 1 : (r6 == r12 ? 0 : -1))
            if (r0 >= 0) goto L_0x0057
            com.fossil.Nr r0 = r14.v
            r14.l(r0)
        L_0x0041:
            return
        L_0x0042:
            r0 = r3
            goto L_0x0018
        L_0x0044:
            r2 = r3
            goto L_0x001c
        L_0x0046:
            r0 = 30000(0x7530, double:1.4822E-319)
            goto L_0x002e
        L_0x0049:
            long r8 = java.lang.System.currentTimeMillis()
            long r10 = r14.E
            long r8 = r8 - r10
            long r0 = r0 - r8
            long r0 = java.lang.Math.min(r0, r6)
            r6 = r0
            goto L_0x0038
        L_0x0057:
            long r0 = java.lang.System.currentTimeMillis()
            r14.J = r0
            com.fossil.As r1 = new com.fossil.As
            com.fossil.K5 r0 = r14.w
            r1.<init>(r0, r2, r6)
            boolean r0 = r14.M
            r1.w = r0
            r14.M = r3
            int r0 = r14.N
            int r0 = r0 + 1
            r14.N = r0
            com.fossil.Wo r2 = new com.fossil.Wo
            r2.<init>(r14)
            com.fossil.Yq r3 = new com.fossil.Yq
            r3.<init>(r14)
            com.fossil.Mr r5 = new com.fossil.Mr
            r5.<init>(r14)
            com.fossil.Cf r6 = com.fossil.Cf.b
            r7 = 8
            r0 = r14
            r8 = r4
            com.fossil.Lp.i(r0, r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Fh.V():void");
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void e(F5 f5) {
        if (Kn.a[f5.ordinal()] == 1) {
            Fs fs = this.b;
            if (fs == null || fs.t) {
                Lp lp = this.n;
                if (lp == null || lp.t) {
                    k(Zq.k);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public boolean q(Lp lp) {
        return u(lp) || (lp.z().isEmpty() && this.y != lp.y);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public boolean r(Fs fs) {
        Hs hs = fs != null ? fs.x : null;
        return hs != null && Kn.c[hs.ordinal()] == 1;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.D;
    }
}
