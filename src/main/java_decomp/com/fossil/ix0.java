package com.fossil;

import android.database.Cursor;
import android.os.Build;
import com.j256.ormlite.field.DatabaseFieldConfigLoader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ix0 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Map<String, Ai> b;
    @DexIgnore
    public /* final */ Set<Bi> c;
    @DexIgnore
    public /* final */ Set<Di> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ String f;
        @DexIgnore
        public /* final */ int g;

        @DexIgnore
        public Ai(String str, String str2, boolean z, int i, String str3, int i2) {
            this.a = str;
            this.b = str2;
            this.d = z;
            this.e = i;
            this.c = a(str2);
            this.f = str3;
            this.g = i2;
        }

        @DexIgnore
        public static int a(String str) {
            if (str == null) {
                return 5;
            }
            String upperCase = str.toUpperCase(Locale.US);
            if (upperCase.contains("INT")) {
                return 3;
            }
            if (upperCase.contains("CHAR") || upperCase.contains("CLOB") || upperCase.contains("TEXT")) {
                return 2;
            }
            if (!upperCase.contains("BLOB")) {
                return (upperCase.contains("REAL") || upperCase.contains("FLOA") || upperCase.contains("DOUB")) ? 4 : 1;
            }
            return 5;
        }

        @DexIgnore
        public boolean b() {
            return this.e > 0;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            String str;
            String str2;
            boolean z = true;
            if (this == obj) {
                return true;
            }
            if (obj == null || Ai.class != obj.getClass()) {
                return false;
            }
            Ai ai = (Ai) obj;
            if (Build.VERSION.SDK_INT >= 20) {
                if (this.e != ai.e) {
                    return false;
                }
            } else if (b() != ai.b()) {
                return false;
            }
            if (!this.a.equals(ai.a) || this.d != ai.d) {
                return false;
            }
            if (this.g == 1 && ai.g == 2 && (str2 = this.f) != null && !str2.equals(ai.f)) {
                return false;
            }
            if (this.g == 2 && ai.g == 1 && (str = ai.f) != null && !str.equals(this.f)) {
                return false;
            }
            int i = this.g;
            if (i != 0 && i == ai.g) {
                String str3 = this.f;
                if (str3 != null) {
                    if (!str3.equals(ai.f)) {
                        return false;
                    }
                } else if (ai.f != null) {
                    return false;
                }
            }
            if (this.c != ai.c) {
                z = false;
            }
            return z;
        }

        @DexIgnore
        public int hashCode() {
            int hashCode = this.a.hashCode();
            return (((this.d ? 1231 : 1237) + (((hashCode * 31) + this.c) * 31)) * 31) + this.e;
        }

        @DexIgnore
        public String toString() {
            return "Column{name='" + this.a + "', type='" + this.b + "', affinity='" + this.c + "', notNull=" + this.d + ", primaryKeyPosition=" + this.e + ", defaultValue='" + this.f + "'}";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ List<String> d;
        @DexIgnore
        public /* final */ List<String> e;

        @DexIgnore
        public Bi(String str, String str2, String str3, List<String> list, List<String> list2) {
            this.a = str;
            this.b = str2;
            this.c = str3;
            this.d = Collections.unmodifiableList(list);
            this.e = Collections.unmodifiableList(list2);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Bi.class != obj.getClass()) {
                return false;
            }
            Bi bi = (Bi) obj;
            if (!this.a.equals(bi.a) || !this.b.equals(bi.b) || !this.c.equals(bi.c) || !this.d.equals(bi.d)) {
                return false;
            }
            return this.e.equals(bi.e);
        }

        @DexIgnore
        public int hashCode() {
            return (((((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "ForeignKey{referenceTable='" + this.a + "', onDelete='" + this.b + "', onUpdate='" + this.c + "', columnNames=" + this.d + ", referenceColumnNames=" + this.e + '}';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci implements Comparable<Ci> {
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ String d;
        @DexIgnore
        public /* final */ String e;

        @DexIgnore
        public Ci(int i, int i2, String str, String str2) {
            this.b = i;
            this.c = i2;
            this.d = str;
            this.e = str2;
        }

        @DexIgnore
        public int a(Ci ci) {
            int i = this.b - ci.b;
            return i == 0 ? this.c - ci.c : i;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // java.lang.Comparable
        public /* bridge */ /* synthetic */ int compareTo(Ci ci) {
            return a(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ List<String> c;

        @DexIgnore
        public Di(String str, boolean z, List<String> list) {
            this.a = str;
            this.b = z;
            this.c = list;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Di.class != obj.getClass()) {
                return false;
            }
            Di di = (Di) obj;
            if (this.b != di.b || !this.c.equals(di.c)) {
                return false;
            }
            return this.a.startsWith("index_") ? di.a.startsWith("index_") : this.a.equals(di.a);
        }

        @DexIgnore
        public int hashCode() {
            return ((((this.a.startsWith("index_") ? -1184239155 : this.a.hashCode()) * 31) + (this.b ? 1 : 0)) * 31) + this.c.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Index{name='" + this.a + "', unique=" + this.b + ", columns=" + this.c + '}';
        }
    }

    @DexIgnore
    public Ix0(String str, Map<String, Ai> map, Set<Bi> set, Set<Di> set2) {
        this.a = str;
        this.b = Collections.unmodifiableMap(map);
        this.c = Collections.unmodifiableSet(set);
        this.d = set2 == null ? null : Collections.unmodifiableSet(set2);
    }

    @DexIgnore
    public static Ix0 a(Lx0 lx0, String str) {
        return new Ix0(str, b(lx0, str), d(lx0, str), f(lx0, str));
    }

    @DexIgnore
    public static Map<String, Ai> b(Lx0 lx0, String str) {
        Cursor query = lx0.query("PRAGMA table_info(`" + str + "`)");
        HashMap hashMap = new HashMap();
        try {
            if (query.getColumnCount() > 0) {
                int columnIndex = query.getColumnIndex("name");
                int columnIndex2 = query.getColumnIndex("type");
                int columnIndex3 = query.getColumnIndex("notnull");
                int columnIndex4 = query.getColumnIndex("pk");
                int columnIndex5 = query.getColumnIndex("dflt_value");
                while (query.moveToNext()) {
                    String string = query.getString(columnIndex);
                    hashMap.put(string, new Ai(string, query.getString(columnIndex2), query.getInt(columnIndex3) != 0, query.getInt(columnIndex4), query.getString(columnIndex5), 2));
                }
            }
            return hashMap;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    public static List<Ci> c(Cursor cursor) {
        int columnIndex = cursor.getColumnIndex("id");
        int columnIndex2 = cursor.getColumnIndex("seq");
        int columnIndex3 = cursor.getColumnIndex("from");
        int columnIndex4 = cursor.getColumnIndex("to");
        int count = cursor.getCount();
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < count; i++) {
            cursor.moveToPosition(i);
            arrayList.add(new Ci(cursor.getInt(columnIndex), cursor.getInt(columnIndex2), cursor.getString(columnIndex3), cursor.getString(columnIndex4)));
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    @DexIgnore
    public static Set<Bi> d(Lx0 lx0, String str) {
        HashSet hashSet = new HashSet();
        Cursor query = lx0.query("PRAGMA foreign_key_list(`" + str + "`)");
        try {
            int columnIndex = query.getColumnIndex("id");
            int columnIndex2 = query.getColumnIndex("seq");
            int columnIndex3 = query.getColumnIndex("table");
            int columnIndex4 = query.getColumnIndex("on_delete");
            int columnIndex5 = query.getColumnIndex("on_update");
            List<Ci> c2 = c(query);
            int count = query.getCount();
            for (int i = 0; i < count; i++) {
                query.moveToPosition(i);
                if (query.getInt(columnIndex2) == 0) {
                    int i2 = query.getInt(columnIndex);
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    for (Ci ci : c2) {
                        if (ci.b == i2) {
                            arrayList.add(ci.d);
                            arrayList2.add(ci.e);
                        }
                    }
                    hashSet.add(new Bi(query.getString(columnIndex3), query.getString(columnIndex4), query.getString(columnIndex5), arrayList, arrayList2));
                }
            }
            return hashSet;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static Di e(Lx0 lx0, String str, boolean z) {
        Cursor query = lx0.query("PRAGMA index_xinfo(`" + str + "`)");
        try {
            int columnIndex = query.getColumnIndex("seqno");
            int columnIndex2 = query.getColumnIndex("cid");
            int columnIndex3 = query.getColumnIndex("name");
            if (columnIndex == -1 || columnIndex2 == -1 || columnIndex3 == -1) {
                query.close();
                return null;
            }
            TreeMap treeMap = new TreeMap();
            while (query.moveToNext()) {
                if (query.getInt(columnIndex2) >= 0) {
                    treeMap.put(Integer.valueOf(query.getInt(columnIndex)), query.getString(columnIndex3));
                }
            }
            ArrayList arrayList = new ArrayList(treeMap.size());
            arrayList.addAll(treeMap.values());
            Di di = new Di(str, z, arrayList);
            query.close();
            return di;
        } catch (Throwable th) {
            query.close();
            throw th;
        }
    }

    @DexIgnore
    public static Set<Di> f(Lx0 lx0, String str) {
        Cursor query = lx0.query("PRAGMA index_list(`" + str + "`)");
        try {
            int columnIndex = query.getColumnIndex("name");
            int columnIndex2 = query.getColumnIndex("origin");
            int columnIndex3 = query.getColumnIndex(DatabaseFieldConfigLoader.FIELD_NAME_UNIQUE);
            if (columnIndex == -1 || columnIndex2 == -1 || columnIndex3 == -1) {
                return null;
            }
            HashSet hashSet = new HashSet();
            while (query.moveToNext()) {
                if ("c".equals(query.getString(columnIndex2))) {
                    Di e = e(lx0, query.getString(columnIndex), query.getInt(columnIndex3) == 1);
                    if (e == null) {
                        query.close();
                        return null;
                    }
                    hashSet.add(e);
                }
            }
            query.close();
            return hashSet;
        } finally {
            query.close();
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Set<Di> set;
        if (this == obj) {
            return true;
        }
        if (obj == null || Ix0.class != obj.getClass()) {
            return false;
        }
        Ix0 ix0 = (Ix0) obj;
        String str = this.a;
        if (str == null ? ix0.a != null : !str.equals(ix0.a)) {
            return false;
        }
        Map<String, Ai> map = this.b;
        if (map == null ? ix0.b != null : !map.equals(ix0.b)) {
            return false;
        }
        Set<Bi> set2 = this.c;
        if (set2 == null ? ix0.c != null : !set2.equals(ix0.c)) {
            return false;
        }
        Set<Di> set3 = this.d;
        if (set3 == null || (set = ix0.d) == null) {
            return true;
        }
        return set3.equals(set);
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        Map<String, Ai> map = this.b;
        int hashCode2 = map != null ? map.hashCode() : 0;
        Set<Bi> set = this.c;
        if (set != null) {
            i = set.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "TableInfo{name='" + this.a + "', columns=" + this.b + ", foreignKeys=" + this.c + ", indices=" + this.d + '}';
    }
}
