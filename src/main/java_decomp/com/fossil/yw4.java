package com.fossil;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Yw4 extends FragmentStateAdapter {
    @DexIgnore
    public /* final */ List<Fragment> j;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Yw4(List<Fragment> list, Fragment fragment) {
        super(fragment);
        Wg6.c(list, "frags");
        Wg6.c(fragment, "fragment");
        this.j = list;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.j.size();
    }

    @DexIgnore
    @Override // androidx.viewpager2.adapter.FragmentStateAdapter
    public Fragment i(int i) {
        return this.j.get(i);
    }
}
