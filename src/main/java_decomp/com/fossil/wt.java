package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Wt extends Enum<Wt> implements Mt {
    @DexIgnore
    public static /* final */ Wt d;
    @DexIgnore
    public static /* final */ Wt e;
    @DexIgnore
    public static /* final */ /* synthetic */ Wt[] f;
    @DexIgnore
    public static /* final */ Vt g; // = new Vt(null);
    @DexIgnore
    public /* final */ String b; // = Ey1.a(this);
    @DexIgnore
    public /* final */ byte c;

    /*
    static {
        Wt wt = new Wt("SUCCESS", 0, (byte) 0);
        d = wt;
        Wt wt2 = new Wt("WRONG_LENGTH", 1, (byte) 1);
        Wt wt3 = new Wt("FAIL_TO_GENERATE_RANDOM_NUMBER", 2, (byte) 2);
        Wt wt4 = new Wt("FAIL_TO_GET_KEY", 3, (byte) 3);
        Wt wt5 = new Wt("FAIL_TO_ENCRYPT_FRAME", 4, (byte) 4);
        Wt wt6 = new Wt("FAIL_TO_DECRYPT_FRAME", 5, (byte) 5);
        Wt wt7 = new Wt("INVALID_KEY_TYPE", 6, (byte) 6);
        Wt wt8 = new Wt("WRONG_CONFIRM_RANDOM_NUMBER", 7, (byte) 7);
        Wt wt9 = new Wt("WRONG_STATE", 8, (byte) 8);
        Wt wt10 = new Wt("FAIL_TO_STORE_KEY", 9, (byte) 9);
        Wt wt11 = new Wt("WRONG_KEY_OPERATION", 10, (byte) 10);
        Wt wt12 = new Wt("NOT_SUPPORTED", 11, (byte) 11);
        Wt wt13 = new Wt("FAIL_TO_GENERATE_PUBLIC_KEY", 12, (byte) 12);
        Wt wt14 = new Wt("FAIL_TO_GENERATE_SECRET_KEY", 13, (byte) 13);
        Wt wt15 = new Wt("FAIL_BECAUSE_OF_INVALID_INPUT", 14, DateTimeFieldType.HOUR_OF_HALFDAY);
        Wt wt16 = new Wt("FAIL_BECAUSE_OF_NOT_SET_KEY", 15, DateTimeFieldType.CLOCKHOUR_OF_HALFDAY);
        Wt wt17 = new Wt("FAIL_TO_SEND_REQUEST_AUTHENTICATE_THROUGH_ASYNC", 16, DateTimeFieldType.CLOCKHOUR_OF_DAY);
        Wt wt18 = new Wt("UNKNOWN", 17, (byte) 255);
        e = wt18;
        f = new Wt[]{wt, wt2, wt3, wt4, wt5, wt6, wt7, wt8, wt9, wt10, wt11, wt12, wt13, wt14, wt15, wt16, wt17, wt18};
    }
    */

    @DexIgnore
    public Wt(String str, int i, byte b2) {
        this.c = (byte) b2;
    }

    @DexIgnore
    public static Wt valueOf(String str) {
        return (Wt) Enum.valueOf(Wt.class, str);
    }

    @DexIgnore
    public static Wt[] values() {
        return (Wt[]) f.clone();
    }

    @DexIgnore
    @Override // com.fossil.Mt
    public boolean a() {
        return this == d;
    }

    @DexIgnore
    @Override // com.fossil.Mt
    public String getLogName() {
        return this.b;
    }
}
