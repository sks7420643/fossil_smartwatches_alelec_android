package com.fossil;

import android.os.Process;
import com.fossil.A91;
import com.fossil.M91;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class B91 extends Thread {
    @DexIgnore
    public static /* final */ boolean h; // = U91.b;
    @DexIgnore
    public /* final */ BlockingQueue<M91<?>> b;
    @DexIgnore
    public /* final */ BlockingQueue<M91<?>> c;
    @DexIgnore
    public /* final */ A91 d;
    @DexIgnore
    public /* final */ P91 e;
    @DexIgnore
    public volatile boolean f; // = false;
    @DexIgnore
    public /* final */ Bi g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ M91 b;

        @DexIgnore
        public Ai(M91 m91) {
            this.b = m91;
        }

        @DexIgnore
        public void run() {
            try {
                B91.this.c.put(this.b);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi implements M91.Bi {
        @DexIgnore
        public /* final */ Map<String, List<M91<?>>> a; // = new HashMap();
        @DexIgnore
        public /* final */ B91 b;

        @DexIgnore
        public Bi(B91 b91) {
            this.b = b91;
        }

        @DexIgnore
        @Override // com.fossil.M91.Bi
        public void a(M91<?> m91) {
            synchronized (this) {
                String cacheKey = m91.getCacheKey();
                List<M91<?>> remove = this.a.remove(cacheKey);
                if (remove != null && !remove.isEmpty()) {
                    if (U91.b) {
                        U91.e("%d waiting requests for cacheKey=%s; resend to network", Integer.valueOf(remove.size()), cacheKey);
                    }
                    M91<?> remove2 = remove.remove(0);
                    this.a.put(cacheKey, remove);
                    remove2.setNetworkRequestCompleteListener(this);
                    try {
                        this.b.c.put(remove2);
                    } catch (InterruptedException e) {
                        U91.c("Couldn't add request to queue. %s", e.toString());
                        Thread.currentThread().interrupt();
                        this.b.e();
                    }
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.M91.Bi
        public void b(M91<?> m91, O91<?> o91) {
            List<M91<?>> remove;
            A91.Ai ai = o91.b;
            if (ai == null || ai.a()) {
                a(m91);
                return;
            }
            String cacheKey = m91.getCacheKey();
            synchronized (this) {
                remove = this.a.remove(cacheKey);
            }
            if (remove != null) {
                if (U91.b) {
                    U91.e("Releasing %d waiting requests for cacheKey=%s.", Integer.valueOf(remove.size()), cacheKey);
                }
                for (M91<?> m912 : remove) {
                    this.b.e.a(m912, o91);
                }
            }
        }

        @DexIgnore
        public final boolean d(M91<?> m91) {
            synchronized (this) {
                String cacheKey = m91.getCacheKey();
                if (this.a.containsKey(cacheKey)) {
                    List<M91<?>> list = this.a.get(cacheKey);
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    m91.addMarker("waiting-for-response");
                    list.add(m91);
                    this.a.put(cacheKey, list);
                    if (U91.b) {
                        U91.b("Request for cacheKey=%s is in flight, putting on hold.", cacheKey);
                    }
                    return true;
                }
                this.a.put(cacheKey, null);
                m91.setNetworkRequestCompleteListener(this);
                if (U91.b) {
                    U91.b("new request, sending to network %s", cacheKey);
                }
                return false;
            }
        }
    }

    @DexIgnore
    public B91(BlockingQueue<M91<?>> blockingQueue, BlockingQueue<M91<?>> blockingQueue2, A91 a91, P91 p91) {
        this.b = blockingQueue;
        this.c = blockingQueue2;
        this.d = a91;
        this.e = p91;
        this.g = new Bi(this);
    }

    @DexIgnore
    public final void c() throws InterruptedException {
        d(this.b.take());
    }

    @DexIgnore
    public void d(M91<?> m91) throws InterruptedException {
        m91.addMarker("cache-queue-take");
        if (m91.isCanceled()) {
            m91.finish("cache-discard-canceled");
            return;
        }
        A91.Ai b2 = this.d.b(m91.getCacheKey());
        if (b2 == null) {
            m91.addMarker("cache-miss");
            if (!this.g.d(m91)) {
                this.c.put(m91);
            }
        } else if (b2.a()) {
            m91.addMarker("cache-hit-expired");
            m91.setCacheEntry(b2);
            if (!this.g.d(m91)) {
                this.c.put(m91);
            }
        } else {
            m91.addMarker("cache-hit");
            O91<?> parseNetworkResponse = m91.parseNetworkResponse(new J91(b2.a, b2.g));
            m91.addMarker("cache-hit-parsed");
            if (!b2.b()) {
                this.e.a(m91, parseNetworkResponse);
                return;
            }
            m91.addMarker("cache-hit-refresh-needed");
            m91.setCacheEntry(b2);
            parseNetworkResponse.d = true;
            if (!this.g.d(m91)) {
                this.e.b(m91, parseNetworkResponse, new Ai(m91));
            } else {
                this.e.a(m91, parseNetworkResponse);
            }
        }
    }

    @DexIgnore
    public void e() {
        this.f = true;
        interrupt();
    }

    @DexIgnore
    public void run() {
        if (h) {
            U91.e("start new dispatcher", new Object[0]);
        }
        Process.setThreadPriority(10);
        this.d.a();
        while (true) {
            try {
                c();
            } catch (InterruptedException e2) {
                if (this.f) {
                    Thread.currentThread().interrupt();
                    return;
                }
                U91.c("Ignoring spurious interrupt of CacheDispatcher thread; use quit() to terminate it", new Object[0]);
            }
        }
    }
}
