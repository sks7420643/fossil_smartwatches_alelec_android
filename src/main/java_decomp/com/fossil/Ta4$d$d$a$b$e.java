package com.fossil;

import com.fossil.Na4;
import com.fossil.Oa4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Ta4$d$d$a$b$e {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract Ta4$d$d$a$b$e a();

        @DexIgnore
        public abstract a b(Ua4<b> ua4);

        @DexIgnore
        public abstract a c(int i);

        @DexIgnore
        public abstract a d(String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class a {
            @DexIgnore
            public abstract b a();

            @DexIgnore
            public abstract a b(String str);

            @DexIgnore
            public abstract a c(int i);

            @DexIgnore
            public abstract a d(long j);

            @DexIgnore
            public abstract a e(long j);

            @DexIgnore
            public abstract a f(String str);
        }

        @DexIgnore
        public static a a() {
            return new Oa4.Bi();
        }

        @DexIgnore
        public abstract String b();

        @DexIgnore
        public abstract int c();

        @DexIgnore
        public abstract long d();

        @DexIgnore
        public abstract long e();

        @DexIgnore
        public abstract String f();
    }

    @DexIgnore
    public static a a() {
        return new Na4.Bi();
    }

    @DexIgnore
    public abstract Ua4<b> b();

    @DexIgnore
    public abstract int c();

    @DexIgnore
    public abstract String d();
}
