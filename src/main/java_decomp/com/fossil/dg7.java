package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Dg7 extends Exception {
    @DexIgnore
    public Dg7() {
    }

    @DexIgnore
    public Dg7(String str) {
        super(str);
    }

    @DexIgnore
    public Dg7(String str, Throwable th) {
        super(str, th);
    }

    @DexIgnore
    public Dg7(Throwable th) {
        super(th);
    }
}
