package com.fossil;

import android.content.Context;
import android.graphics.PointF;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Gv0 extends RecyclerView.v {
    @DexIgnore
    public /* final */ LinearInterpolator i; // = new LinearInterpolator();
    @DexIgnore
    public /* final */ DecelerateInterpolator j; // = new DecelerateInterpolator();
    @DexIgnore
    public PointF k;
    @DexIgnore
    public /* final */ DisplayMetrics l;
    @DexIgnore
    public boolean m; // = false;
    @DexIgnore
    public float n;
    @DexIgnore
    public int o; // = 0;
    @DexIgnore
    public int p; // = 0;

    @DexIgnore
    public Gv0(Context context) {
        this.l = context.getResources().getDisplayMetrics();
    }

    @DexIgnore
    public final float A() {
        if (!this.m) {
            this.n = v(this.l);
            this.m = true;
        }
        return this.n;
    }

    @DexIgnore
    public int B() {
        PointF pointF = this.k;
        if (pointF != null) {
            float f = pointF.y;
            if (f != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                return f > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : -1;
            }
        }
        return 0;
    }

    @DexIgnore
    public void C(RecyclerView.v.a aVar) {
        PointF a2 = a(f());
        if (a2 == null || (a2.x == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && a2.y == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
            aVar.b(f());
            r();
            return;
        }
        i(a2);
        this.k = a2;
        this.o = (int) (a2.x * 10000.0f);
        this.p = (int) (a2.y * 10000.0f);
        aVar.d((int) (((float) this.o) * 1.2f), (int) (((float) this.p) * 1.2f), (int) (((float) x(10000)) * 1.2f), this.i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.v
    public void l(int i2, int i3, RecyclerView.State state, RecyclerView.v.a aVar) {
        if (c() == 0) {
            r();
            return;
        }
        this.o = y(this.o, i2);
        int y = y(this.p, i3);
        this.p = y;
        if (this.o == 0 && y == 0) {
            C(aVar);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.v
    public void m() {
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.v
    public void n() {
        this.p = 0;
        this.o = 0;
        this.k = null;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.v
    public void o(View view, RecyclerView.State state, RecyclerView.v.a aVar) {
        int t = t(view, z());
        int u = u(view, B());
        int w = w((int) Math.sqrt((double) ((t * t) + (u * u))));
        if (w > 0) {
            aVar.d(-t, -u, w, this.j);
        }
    }

    @DexIgnore
    public int s(int i2, int i3, int i4, int i5, int i6) {
        if (i6 == -1) {
            return i4 - i2;
        }
        if (i6 == 0) {
            int i7 = i4 - i2;
            if (i7 > 0) {
                return i7;
            }
            int i8 = i5 - i3;
            if (i8 >= 0) {
                return 0;
            }
            return i8;
        } else if (i6 == 1) {
            return i5 - i3;
        } else {
            throw new IllegalArgumentException("snap preference should be one of the constants defined in SmoothScroller, starting with SNAP_");
        }
    }

    @DexIgnore
    public int t(View view, int i2) {
        RecyclerView.m e = e();
        if (e == null || !e.l()) {
            return 0;
        }
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        return s(e.R(view) - ((ViewGroup.MarginLayoutParams) layoutParams).leftMargin, e.U(view) + ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin, e.f0(), e.p0() - e.g0(), i2);
    }

    @DexIgnore
    public int u(View view, int i2) {
        RecyclerView.m e = e();
        if (e == null || !e.m()) {
            return 0;
        }
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
        return s(e.V(view) - ((ViewGroup.MarginLayoutParams) layoutParams).topMargin, e.P(view) + ((ViewGroup.MarginLayoutParams) layoutParams).bottomMargin, e.h0(), e.X() - e.e0(), i2);
    }

    @DexIgnore
    public float v(DisplayMetrics displayMetrics) {
        return 25.0f / ((float) displayMetrics.densityDpi);
    }

    @DexIgnore
    public int w(int i2) {
        return (int) Math.ceil(((double) x(i2)) / 0.3356d);
    }

    @DexIgnore
    public int x(int i2) {
        return (int) Math.ceil((double) (((float) Math.abs(i2)) * A()));
    }

    @DexIgnore
    public final int y(int i2, int i3) {
        int i4 = i2 - i3;
        if (i2 * i4 <= 0) {
            return 0;
        }
        return i4;
    }

    @DexIgnore
    public int z() {
        PointF pointF = this.k;
        if (pointF != null) {
            float f = pointF.x;
            if (f != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                return f > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES ? 1 : -1;
            }
        }
        return 0;
    }
}
