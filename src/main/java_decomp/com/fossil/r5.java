package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class R5 extends Enum<R5> {
    @DexIgnore
    public static /* final */ R5 b;
    @DexIgnore
    public static /* final */ R5 c;
    @DexIgnore
    public static /* final */ R5 d;
    @DexIgnore
    public static /* final */ R5 e;
    @DexIgnore
    public static /* final */ R5 f;
    @DexIgnore
    public static /* final */ R5 g;
    @DexIgnore
    public static /* final */ R5 h;
    @DexIgnore
    public static /* final */ R5 i;
    @DexIgnore
    public static /* final */ R5 j;
    @DexIgnore
    public static /* final */ R5 k;
    @DexIgnore
    public static /* final */ R5 l;
    @DexIgnore
    public static /* final */ R5 m;
    @DexIgnore
    public static /* final */ R5 n;
    @DexIgnore
    public static /* final */ /* synthetic */ R5[] o;
    @DexIgnore
    public static /* final */ Q5 p; // = new Q5(null);

    /*
    static {
        R5 r5 = new R5("SUCCESS", 0, 0);
        b = r5;
        R5 r52 = new R5("NOT_START", 1, 1);
        c = r52;
        R5 r53 = new R5("WRONG_STATE", 2, 6);
        R5 r54 = new R5("GATT_ERROR", 3, 7);
        d = r54;
        R5 r55 = new R5("UNEXPECTED_RESULT", 4, 8);
        e = r55;
        R5 r56 = new R5("INTERRUPTED_BECAUSE_OF_REQUEST_TIME_OUT", 5, 9);
        f = r56;
        R5 r57 = new R5("BLUETOOTH_OFF", 6, 10);
        g = r57;
        R5 r58 = new R5("UNSUPPORTED", 7, 11);
        h = r58;
        R5 r59 = new R5("HID_PROXY_NOT_CONNECTED", 8, 256);
        i = r59;
        R5 r510 = new R5("HID_FAIL_TO_INVOKE_PRIVATE_METHOD", 9, 257);
        j = r510;
        R5 r511 = new R5("HID_INPUT_DEVICE_DISABLED", 10, 258);
        k = r511;
        R5 r512 = new R5("HID_UNKNOWN_ERROR", 11, 511);
        l = r512;
        R5 r513 = new R5("INTERRUPTED", 12, 254);
        m = r513;
        R5 r514 = new R5("CONNECTION_DROPPED", 13, 255);
        n = r514;
        o = new R5[]{r5, r52, r53, r54, r55, r56, r57, r58, r59, r510, r511, r512, r513, r514};
    }
    */

    @DexIgnore
    public R5(String str, int i2, int i3) {
    }

    @DexIgnore
    public static R5 valueOf(String str) {
        return (R5) Enum.valueOf(R5.class, str);
    }

    @DexIgnore
    public static R5[] values() {
        return (R5[]) o.clone();
    }
}
