package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Un6 extends Rn6 {
    @DexIgnore
    public String e; // = PortfolioApp.get.instance().J();
    @DexIgnore
    public /* final */ Ai f; // = new Ai(this);
    @DexIgnore
    public /* final */ Sn6 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ Un6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ai(Un6 un6) {
            this.a = un6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            if (otaEvent != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeUpdateFirmwarePresenter", "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
                if (!TextUtils.isEmpty(otaEvent.getSerial()) && Vt7.j(otaEvent.getSerial(), PortfolioApp.get.instance().J(), true)) {
                    this.a.o().Z((int) (otaEvent.getProcess() * ((float) 10)));
                }
            }
        }
    }

    @DexIgnore
    public Un6(Sn6 sn6) {
        Wg6.c(sn6, "mView");
        this.g = sn6;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        PortfolioApp instance = PortfolioApp.get.instance();
        Ai ai = this.f;
        instance.registerReceiver(ai, new IntentFilter(PortfolioApp.get.instance().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        n();
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        try {
            PortfolioApp.get.instance().unregisterReceiver(this.f);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("HomeUpdateFirmwarePresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public final void n() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeUpdateFirmwarePresenter", "serial=" + this.e + ", mCurrentDeviceType=" + deviceBySerial);
        if (DeviceHelper.o.w(deviceBySerial)) {
            explore.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886916));
            explore.setBackground(2131231353);
            explore2.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886917));
            explore2.setBackground(2131231351);
            explore3.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886914));
            explore3.setBackground(2131231354);
            explore4.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886915));
            explore4.setBackground(2131231350);
        } else {
            explore.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886923));
            explore.setBackground(2131231353);
            explore2.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886922));
            explore2.setBackground(2131231355);
            explore3.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886920));
            explore3.setBackground(2131231354);
            explore4.setDescription(Um5.c(PortfolioApp.get.instance(), 2131886921));
            explore4.setBackground(2131231352);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.g.X(arrayList);
    }

    @DexIgnore
    public final Sn6 o() {
        return this.g;
    }

    @DexIgnore
    public void p() {
        this.g.M5(this);
    }
}
