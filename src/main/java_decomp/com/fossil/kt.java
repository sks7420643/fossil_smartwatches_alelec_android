package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class Kt extends Enum<Kt> implements Mt {
    @DexIgnore
    public static /* final */ Kt d;
    @DexIgnore
    public static /* final */ Kt e;
    @DexIgnore
    public static /* final */ Kt f;
    @DexIgnore
    public static /* final */ Kt g;
    @DexIgnore
    public static /* final */ Kt h;
    @DexIgnore
    public static /* final */ Kt i;
    @DexIgnore
    public static /* final */ /* synthetic */ Kt[] j;
    @DexIgnore
    public static /* final */ It k; // = new It(null);
    @DexIgnore
    public /* final */ String b; // = Ey1.a(this);
    @DexIgnore
    public /* final */ byte c;

    /*
    static {
        Kt kt = new Kt("SUCCESS", 0, (byte) 0);
        d = kt;
        Kt kt2 = new Kt("INVALID_OPERATION", 1, (byte) 1);
        e = kt2;
        Kt kt3 = new Kt("INVALID_FILE_HANDLE", 2, (byte) 2);
        f = kt3;
        Kt kt4 = new Kt("INVALID_OPERATION_DATA", 3, (byte) 3);
        Kt kt5 = new Kt("OPERATION_IN_PROGRESS", 4, (byte) 4);
        g = kt5;
        Kt kt6 = new Kt("VERIFICATION_FAIL", 5, (byte) 5);
        h = kt6;
        Kt kt7 = new Kt("UNKNOWN", 6, (byte) 255);
        i = kt7;
        j = new Kt[]{kt, kt2, kt3, kt4, kt5, kt6, kt7};
    }
    */

    @DexIgnore
    public Kt(String str, int i2, byte b2) {
        this.c = (byte) b2;
    }

    @DexIgnore
    public static Kt valueOf(String str) {
        return (Kt) Enum.valueOf(Kt.class, str);
    }

    @DexIgnore
    public static Kt[] values() {
        return (Kt[]) j.clone();
    }

    @DexIgnore
    @Override // com.fossil.Mt
    public boolean a() {
        return this == d;
    }

    @DexIgnore
    @Override // com.fossil.Mt
    public String getLogName() {
        return this.b;
    }
}
