package com.fossil;

import android.content.Context;
import android.location.Location;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.fossil.P72;
import com.fossil.R62;
import com.google.android.gms.location.LocationRequest;
import com.mapped.Yv2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fr2 extends Rr2 {
    @DexIgnore
    public /* final */ Yq2 G;

    @DexIgnore
    public Fr2(Context context, Looper looper, R62.Bi bi, R62.Ci ci, String str, Ac2 ac2) {
        super(context, looper, bi, ci, str, ac2);
        this.G = new Yq2(context, this.F);
    }

    @DexIgnore
    @Override // com.fossil.M62.Fi, com.fossil.Yb2
    public final void a() {
        synchronized (this.G) {
            if (c()) {
                try {
                    this.G.b();
                    this.G.i();
                } catch (Exception e) {
                    Log.e("LocationClientImpl", "Client disconnected before listeners could be cleaned up", e);
                }
            }
            super.a();
        }
    }

    @DexIgnore
    public final Location u0() throws RemoteException {
        return this.G.a();
    }

    @DexIgnore
    public final void v0(P72.Ai<Ga3> ai, Rq2 rq2) throws RemoteException {
        this.G.d(ai, rq2);
    }

    @DexIgnore
    public final void w0(Ir2 ir2, P72<Yv2> p72, Rq2 rq2) throws RemoteException {
        synchronized (this.G) {
            this.G.e(ir2, p72, rq2);
        }
    }

    @DexIgnore
    public final void x0(LocationRequest locationRequest, P72<Ga3> p72, Rq2 rq2) throws RemoteException {
        synchronized (this.G) {
            this.G.f(locationRequest, p72, rq2);
        }
    }

    @DexIgnore
    public final void y0(Ia3 ia3, J72<Ka3> j72, String str) throws RemoteException {
        boolean z = true;
        A();
        Rc2.b(ia3 != null, "locationSettingsRequest can't be null nor empty.");
        if (j72 == null) {
            z = false;
        }
        Rc2.b(z, "listener can't be null.");
        ((Uq2) I()).O2(ia3, new Hr2(j72), str);
    }

    @DexIgnore
    public final void z0(P72.Ai<Yv2> ai, Rq2 rq2) throws RemoteException {
        this.G.j(ai, rq2);
    }
}
