package com.fossil;

import com.baseflow.geolocator.utils.LocaleConverter;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.helper.AnalyticsHelper;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ul5 {
    @DexIgnore
    public String a; // = "";
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public AnalyticsHelper c;
    @DexIgnore
    public Map<String, String> d; // = new HashMap();
    @DexIgnore
    public Map<String, Object> e; // = new HashMap();
    @DexIgnore
    public volatile boolean f;
    @DexIgnore
    public long g; // = -1;
    @DexIgnore
    public Ai h;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        Object T4();  // void declaration

        @DexIgnore
        Object f2();  // void declaration
    }

    @DexIgnore
    public Ul5(AnalyticsHelper analyticsHelper, String str, String str2) {
        Wg6.c(analyticsHelper, "analyticsHelper");
        Wg6.c(str, "traceName");
        this.c = analyticsHelper;
        String valueOf = String.valueOf(System.currentTimeMillis());
        if (str2 != null) {
            valueOf = str2 + LocaleConverter.LOCALE_DELIMITER + valueOf;
        }
        this.a = valueOf;
        this.b = str;
    }

    @DexIgnore
    public final void a(Sl5 sl5) {
        Wg6.c(sl5, "analyticsEvent");
        sl5.a("trace_id", this.a);
        sl5.b();
    }

    @DexIgnore
    public final Ul5 b(String str, String str2) {
        Wg6.c(str, "paramName");
        Wg6.c(str2, "paramValue");
        Map<String, String> map = this.d;
        if (map != null) {
            map.put(str, str2);
        }
        return this;
    }

    @DexIgnore
    public final void c(String str) {
        Wg6.c(str, "errorCode");
        if (!this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is not started");
            return;
        }
        this.f = false;
        if (this.g > 0) {
            long currentTimeMillis = System.currentTimeMillis();
            long j = this.g;
            Map<String, ? extends Object> map = this.e;
            if (map != null) {
                map.put("trace_id", this.a);
                map.put("duration", Integer.valueOf((int) (currentTimeMillis - j)));
                map.put(Constants.RESULT, str);
                AnalyticsHelper analyticsHelper = this.c;
                if (analyticsHelper != null) {
                    analyticsHelper.l(this.b + "_end", map);
                }
            }
        }
        Ai ai = this.h;
        if (ai != null) {
            ai.T4();
        }
    }

    @DexIgnore
    public final void d(String str, boolean z, String str2) {
        Wg6.c(str, "serial");
        Wg6.c(str2, "errorCode");
        if (!this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is not started");
            return;
        }
        this.f = false;
        if (this.g > 0) {
            long currentTimeMillis = System.currentTimeMillis();
            long j = this.g;
            Map<String, ? extends Object> map = this.e;
            if (map != null) {
                map.put("trace_id", this.a);
                map.put("duration", Integer.valueOf((int) (currentTimeMillis - j)));
                map.put("cache_hit", Integer.valueOf(z ? 1 : 0));
                map.put("Serial_Number", str);
                map.put(Constants.RESULT, str2);
                AnalyticsHelper analyticsHelper = this.c;
                if (analyticsHelper != null) {
                    analyticsHelper.l(this.b + "_end", map);
                }
            }
        }
        Ai ai = this.h;
        if (ai != null) {
            ai.T4();
        }
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public final boolean f() {
        return this.f;
    }

    @DexIgnore
    public final void g() {
        this.h = null;
    }

    @DexIgnore
    public final Ul5 h(Ai ai) {
        Wg6.c(ai, "tracingCallback");
        this.h = ai;
        return this;
    }

    @DexIgnore
    public final void i() {
        if (this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is already started");
            return;
        }
        this.f = true;
        Map<String, String> map = this.d;
        if (map != null) {
            map.put("trace_id", this.a);
        }
        AnalyticsHelper analyticsHelper = this.c;
        if (analyticsHelper != null) {
            analyticsHelper.l(this.b + "_start", this.d);
        }
        Map<String, String> map2 = this.d;
        if (map2 != null) {
            map2.clear();
        }
        this.g = System.currentTimeMillis();
        Ai ai = this.h;
        if (ai != null) {
            ai.f2();
        }
    }

    @DexIgnore
    public String toString() {
        return "Tracer name: " + this.b + ", id: " + this.a + ", running: " + this.f;
    }
}
