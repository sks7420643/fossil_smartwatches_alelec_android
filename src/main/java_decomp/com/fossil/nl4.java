package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nl4 extends Pl4 {
    @DexIgnore
    public static /* final */ Nl4 b;

    /*
    static {
        Nl4 nl4 = new Nl4();
        b = nl4;
        nl4.setStackTrace(Pl4.NO_TRACE);
    }
    */

    @DexIgnore
    public Nl4() {
    }

    @DexIgnore
    public Nl4(Throwable th) {
        super(th);
    }

    @DexIgnore
    public static Nl4 getFormatInstance() {
        return Pl4.isStackTrace ? new Nl4() : b;
    }

    @DexIgnore
    public static Nl4 getFormatInstance(Throwable th) {
        return Pl4.isStackTrace ? new Nl4(th) : b;
    }
}
