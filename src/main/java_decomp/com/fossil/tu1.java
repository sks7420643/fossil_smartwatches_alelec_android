package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tu1 extends ox1 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ xu1 b;
    @DexIgnore
    public /* final */ wu1[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<tu1> {
        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public tu1 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                pq7.b(readString, "parcel.readString()!!");
                xu1 valueOf = xu1.valueOf(readString);
                Object[] createTypedArray = parcel.createTypedArray(wu1.CREATOR);
                if (createTypedArray != null) {
                    pq7.b(createTypedArray, "parcel.createTypedArray(\u2026AppDeclaration.CREATOR)!!");
                    return new tu1(valueOf, (wu1[]) createTypedArray);
                }
                pq7.i();
                throw null;
            }
            pq7.i();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public tu1[] newArray(int i) {
            return new tu1[i];
        }
    }

    @DexIgnore
    public tu1(xu1 xu1, wu1[] wu1Arr) {
        this.b = xu1;
        this.c = wu1Arr;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!pq7.a(tu1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            tu1 tu1 = (tu1) obj;
            xu1 xu1 = this.b;
            xu1 xu12 = tu1.b;
            if (xu1 != xu12) {
                return false;
            }
            return xu1 == xu12 && Arrays.equals(this.c, tu1.c);
        }
        throw new il7("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.MicroAppMapping");
    }

    @DexIgnore
    public final xu1 getMicroAppButton() {
        return this.b;
    }

    @DexIgnore
    public final wu1[] getMicroAppDeclarations() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ox1
    public JSONObject toJSONObject() {
        JSONArray jSONArray = new JSONArray();
        for (wu1 wu1 : this.c) {
            jSONArray.put(wu1.toJSONObject());
        }
        return g80.k(g80.k(g80.k(new JSONObject(), jd0.C3, ey1.a(this.b)), jd0.D3, jSONArray), jd0.E3, Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.c, i);
        }
    }
}
