package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Uu extends Mu {
    @DexIgnore
    public int L; // = 20;
    @DexIgnore
    public /* final */ boolean M;

    @DexIgnore
    public Uu(K5 k5) {
        super(Fu.k, Hs.h, k5, 0, 8);
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public JSONObject A() {
        return G80.k(super.A(), Jd0.K0, Integer.valueOf(this.L));
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public JSONObject F(byte[] bArr) {
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 2) {
            int n = Hy1.n(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0));
            this.L = n;
            G80.k(jSONObject, Jd0.K0, Integer.valueOf(n));
            this.v = Mw.a(this.v, null, null, Lw.b, null, null, 27);
        } else {
            this.v = Mw.a(this.v, null, null, Lw.k, null, null, 27);
        }
        this.E = true;
        return jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public boolean O() {
        return this.M;
    }
}
