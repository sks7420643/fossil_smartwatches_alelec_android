package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Serializable;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K14 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<T> implements J14<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Collection<?> target;

        @DexIgnore
        public Bi(Collection<?> collection) {
            I14.l(collection);
            this.target = collection;
        }

        @DexIgnore
        @Override // com.fossil.J14
        public boolean apply(T t) {
            try {
                return this.target.contains(t);
            } catch (ClassCastException | NullPointerException e) {
                return false;
            }
        }

        @DexIgnore
        @Override // com.fossil.J14
        public boolean equals(Object obj) {
            if (obj instanceof Bi) {
                return this.target.equals(((Bi) obj).target);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.target.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Predicates.in(" + this.target + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci<T> implements J14<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ T target;

        @DexIgnore
        public Ci(T t) {
            this.target = t;
        }

        @DexIgnore
        @Override // com.fossil.J14
        public boolean apply(T t) {
            return this.target.equals(t);
        }

        @DexIgnore
        @Override // com.fossil.J14
        public boolean equals(Object obj) {
            if (obj instanceof Ci) {
                return this.target.equals(((Ci) obj).target);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.target.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Predicates.equalTo(" + ((Object) this.target) + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di<T> implements J14<T>, Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ J14<T> predicate;

        @DexIgnore
        public Di(J14<T> j14) {
            I14.l(j14);
            this.predicate = j14;
        }

        @DexIgnore
        @Override // com.fossil.J14
        public boolean apply(T t) {
            return !this.predicate.apply(t);
        }

        @DexIgnore
        @Override // com.fossil.J14
        public boolean equals(Object obj) {
            if (obj instanceof Di) {
                return this.predicate.equals(((Di) obj).predicate);
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.predicate.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "Predicates.not(" + this.predicate + ")";
        }
    }

    @DexIgnore
    public enum Ei implements J14<Object> {
        ALWAYS_TRUE {
            @DexIgnore
            @Override // com.fossil.J14, com.fossil.K14.Ei
            public boolean apply(Object obj) {
                return true;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.alwaysTrue()";
            }
        },
        ALWAYS_FALSE {
            @DexIgnore
            @Override // com.fossil.J14, com.fossil.K14.Ei
            public boolean apply(Object obj) {
                return false;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.alwaysFalse()";
            }
        },
        IS_NULL {
            @DexIgnore
            @Override // com.fossil.J14, com.fossil.K14.Ei
            public boolean apply(Object obj) {
                return obj == null;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.isNull()";
            }
        },
        NOT_NULL {
            @DexIgnore
            @Override // com.fossil.J14, com.fossil.K14.Ei
            public boolean apply(Object obj) {
                return obj != null;
            }

            @DexIgnore
            public String toString() {
                return "Predicates.notNull()";
            }
        };

        @DexIgnore
        @Override // com.fossil.J14
        @CanIgnoreReturnValue
        public abstract /* synthetic */ boolean apply(T t);

        @DexIgnore
        public <T> J14<T> withNarrowedType() {
            return this;
        }
    }

    /*
    static {
        D14.h(',');
    }
    */

    @DexIgnore
    public static <T> J14<T> a(T t) {
        return t == null ? c() : new Ci(t);
    }

    @DexIgnore
    public static <T> J14<T> b(Collection<? extends T> collection) {
        return new Bi(collection);
    }

    @DexIgnore
    public static <T> J14<T> c() {
        return Ei.IS_NULL.withNarrowedType();
    }

    @DexIgnore
    public static <T> J14<T> d(J14<T> j14) {
        return new Di(j14);
    }
}
