package com.fossil;

import com.mapped.Wg6;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Mu extends Ps {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ N6 I;
    @DexIgnore
    public /* final */ N6 J;
    @DexIgnore
    public /* final */ byte[] K;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Mu(Fu fu, Hs hs, K5 k5, int i, int i2) {
        super(hs, k5, (i2 & 8) != 0 ? 3 : i);
        byte[] array = ByteBuffer.allocate(fu.d.length + 2).put(fu.b.a()).put(fu.c.a()).put(fu.d).array();
        Wg6.b(array, "ByteBuffer.allocate(2 + \u2026\n                .array()");
        this.G = array;
        byte[] array2 = ByteBuffer.allocate(fu.g.length + 2).put(fu.e.a()).put(fu.f.a()).put(fu.g).array();
        Wg6.b(array2, "ByteBuffer.allocate(2 + \u2026\n                .array()");
        this.H = array2;
        N6 n6 = N6.i;
        this.I = n6;
        this.J = n6;
        this.K = new byte[0];
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final Mt E(byte b) {
        return Hu.g.a(b);
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final N6 K() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public byte[] L() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final byte[] M() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final N6 N() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.Ps
    public final byte[] P() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.Fs
    public final long a(O7 o7) {
        return 0;
    }
}
