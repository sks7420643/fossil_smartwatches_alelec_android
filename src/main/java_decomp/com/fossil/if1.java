package com.fossil;

import com.fossil.Af1;
import com.fossil.Wb1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class If1<Model> implements Af1<Model, Model> {
    @DexIgnore
    public static /* final */ If1<?> a; // = new If1<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<Model> implements Bf1<Model, Model> {
        @DexIgnore
        public static /* final */ Ai<?> a; // = new Ai<>();

        @DexIgnore
        public static <T> Ai<T> a() {
            return (Ai<T>) a;
        }

        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Model, Model> b(Ef1 ef1) {
            return If1.c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi<Model> implements Wb1<Model> {
        @DexIgnore
        public /* final */ Model b;

        @DexIgnore
        public Bi(Model model) {
            this.b = model;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Gb1 c() {
            return Gb1.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public void d(Sa1 sa1, Wb1.Ai<? super Model> ai) {
            ai.e(this.b);
        }

        @DexIgnore
        @Override // com.fossil.Wb1
        public Class<Model> getDataClass() {
            return (Class<Model>) this.b.getClass();
        }
    }

    @DexIgnore
    public static <T> If1<T> c() {
        return (If1<T>) a;
    }

    @DexIgnore
    @Override // com.fossil.Af1
    public boolean a(Model model) {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.Af1
    public Af1.Ai<Model> b(Model model, int i, int i2, Ob1 ob1) {
        return new Af1.Ai<>(new Yj1(model), new Bi(model));
    }
}
