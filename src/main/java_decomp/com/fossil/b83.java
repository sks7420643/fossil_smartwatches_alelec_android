package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class B83 implements Xw2<E83> {
    @DexIgnore
    public static B83 c; // = new B83();
    @DexIgnore
    public /* final */ Xw2<E83> b;

    @DexIgnore
    public B83() {
        this(Ww2.b(new D83()));
    }

    @DexIgnore
    public B83(Xw2<E83> xw2) {
        this.b = Ww2.a(xw2);
    }

    @DexIgnore
    public static boolean a() {
        return ((E83) c.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.Xw2
    public final /* synthetic */ E83 zza() {
        return this.b.zza();
    }
}
