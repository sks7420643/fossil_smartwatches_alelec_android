package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class On0<T> extends Nn0<T> {
    @DexIgnore
    public /* final */ Object c; // = new Object();

    @DexIgnore
    public On0(int i) {
        super(i);
    }

    @DexIgnore
    @Override // com.fossil.Mn0, com.fossil.Nn0
    public boolean a(T t) {
        boolean a2;
        synchronized (this.c) {
            a2 = super.a(t);
        }
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.Mn0, com.fossil.Nn0
    public T b() {
        T t;
        synchronized (this.c) {
            t = (T) super.b();
        }
        return t;
    }
}
