package com.fossil;

import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cz1 implements Wd4 {
    @DexIgnore
    public static /* final */ Wd4 a; // = new Cz1();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Sd4<Bz1> {
        @DexIgnore
        public static /* final */ Ai a; // = new Ai();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public void a(Object obj, Td4 td4) throws IOException {
            Bz1 bz1 = (Bz1) obj;
            Td4 td42 = td4;
            td42.f("sdkVersion", bz1.i());
            td42.f(DeviceRequestsHelper.DEVICE_INFO_MODEL, bz1.f());
            td42.f("hardware", bz1.d());
            td42.f("device", bz1.b());
            td42.f("product", bz1.h());
            td42.f("osBuild", bz1.g());
            td42.f("manufacturer", bz1.e());
            td42.f("fingerprint", bz1.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Sd4<Kz1> {
        @DexIgnore
        public static /* final */ Bi a; // = new Bi();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public void a(Object obj, Td4 td4) throws IOException {
            td4.f("logRequest", ((Kz1) obj).b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Sd4<Lz1> {
        @DexIgnore
        public static /* final */ Ci a; // = new Ci();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public void a(Object obj, Td4 td4) throws IOException {
            Lz1 lz1 = (Lz1) obj;
            Td4 td42 = td4;
            td42.f("clientType", lz1.c());
            td42.f("androidClientInfo", lz1.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Sd4<Mz1> {
        @DexIgnore
        public static /* final */ Di a; // = new Di();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public void a(Object obj, Td4 td4) throws IOException {
            Mz1 mz1 = (Mz1) obj;
            Td4 td42 = td4;
            td42.b("eventTimeMs", mz1.d());
            td42.f("eventCode", mz1.c());
            td42.b("eventUptimeMs", mz1.e());
            td42.f("sourceExtension", mz1.g());
            td42.f("sourceExtensionJsonProto3", mz1.h());
            td42.b("timezoneOffsetSeconds", mz1.i());
            td42.f("networkConnectionInfo", mz1.f());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements Sd4<Nz1> {
        @DexIgnore
        public static /* final */ Ei a; // = new Ei();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public void a(Object obj, Td4 td4) throws IOException {
            Nz1 nz1 = (Nz1) obj;
            Td4 td42 = td4;
            td42.b("requestTimeMs", nz1.g());
            td42.b("requestUptimeMs", nz1.h());
            td42.f("clientInfo", nz1.b());
            td42.f("logSource", nz1.d());
            td42.f("logSourceName", nz1.e());
            td42.f("logEvent", nz1.c());
            td42.f("qosTier", nz1.f());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements Sd4<Pz1> {
        @DexIgnore
        public static /* final */ Fi a; // = new Fi();

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.Qd4
        public void a(Object obj, Td4 td4) throws IOException {
            Pz1 pz1 = (Pz1) obj;
            Td4 td42 = td4;
            td42.f("networkType", pz1.c());
            td42.f("mobileSubtype", pz1.b());
        }
    }

    @DexIgnore
    @Override // com.fossil.Wd4
    public void a(Xd4<?> xd4) {
        xd4.a(Kz1.class, Bi.a);
        xd4.a(Ez1.class, Bi.a);
        xd4.a(Nz1.class, Ei.a);
        xd4.a(Hz1.class, Ei.a);
        xd4.a(Lz1.class, Ci.a);
        xd4.a(Fz1.class, Ci.a);
        xd4.a(Bz1.class, Ai.a);
        xd4.a(Dz1.class, Ai.a);
        xd4.a(Mz1.class, Di.a);
        xd4.a(Gz1.class, Di.a);
        xd4.a(Pz1.class, Fi.a);
        xd4.a(Jz1.class, Fi.a);
    }
}
