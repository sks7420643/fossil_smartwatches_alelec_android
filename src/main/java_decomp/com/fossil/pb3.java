package com.fossil;

import android.graphics.Point;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pb3 {
    @DexIgnore
    public static Yb3 a;

    @DexIgnore
    public static Ob3 a(CameraPosition cameraPosition) {
        try {
            return new Ob3(l().J1(cameraPosition));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public static Ob3 b(LatLng latLng) {
        try {
            return new Ob3(l().g0(latLng));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public static Ob3 c(LatLngBounds latLngBounds, int i) {
        try {
            return new Ob3(l().u(latLngBounds, i));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public static Ob3 d(LatLng latLng, float f) {
        try {
            return new Ob3(l().B2(latLng, f));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public static Ob3 e(float f, float f2) {
        try {
            return new Ob3(l().C2(f, f2));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public static Ob3 f(float f) {
        try {
            return new Ob3(l().x(f));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public static Ob3 g(float f, Point point) {
        try {
            return new Ob3(l().Z0(f, point.x, point.y));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public static Ob3 h() {
        try {
            return new Ob3(l().z0());
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public static Ob3 i() {
        try {
            return new Ob3(l().c2());
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public static Ob3 j(float f) {
        try {
            return new Ob3(l().v2(f));
        } catch (RemoteException e) {
            throw new Se3(e);
        }
    }

    @DexIgnore
    public static void k(Yb3 yb3) {
        Rc2.k(yb3);
        a = yb3;
    }

    @DexIgnore
    public static Yb3 l() {
        Yb3 yb3 = a;
        Rc2.l(yb3, "CameraUpdateFactory is not initialized");
        return yb3;
    }
}
