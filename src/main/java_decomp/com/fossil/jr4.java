package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Jr4 extends RecyclerView.g<Bi> {
    @DexIgnore
    public Oi5 a;
    @DexIgnore
    public ArrayList<Oi5> b;
    @DexIgnore
    public Ai c;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a(Oi5 oi5);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi extends RecyclerView.ViewHolder {
        @DexIgnore
        public CustomizeWidget a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public Oi5 d;
        @DexIgnore
        public /* final */ /* synthetic */ Jr4 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ Bi b;

            @DexIgnore
            public Aii(Bi bi) {
                this.b = bi;
            }

            @DexIgnore
            public final void onClick(View view) {
                Ai j;
                if (this.b.e.getItemCount() > this.b.getAdapterPosition() && this.b.getAdapterPosition() != -1 && (j = this.b.e.j()) != null) {
                    Object obj = this.b.e.b.get(this.b.getAdapterPosition());
                    Wg6.b(obj, "mData[adapterPosition]");
                    j.a((Oi5) obj);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(Jr4 jr4, View view) {
            super(view);
            Wg6.c(view, "view");
            this.e = jr4;
            View findViewById = view.findViewById(2131363552);
            Wg6.b(findViewById, "view.findViewById(R.id.wc_workout_type)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363429);
            Wg6.b(findViewById2, "view.findViewById(R.id.tv_workout_type_name)");
            this.b = (TextView) findViewById2;
            this.c = view.findViewById(2131362728);
            this.a.setOnClickListener(new Aii(this));
        }

        @DexIgnore
        public final void a(Oi5 oi5) {
            Wg6.c(oi5, "workoutWrapperType");
            this.d = oi5;
            if (oi5 != null) {
                Lc6<Integer, Integer> a2 = Oi5.Companion.a(oi5);
                this.a.c0(a2.getFirst().intValue());
                this.b.setText(Um5.c(PortfolioApp.get.instance(), a2.getSecond().intValue()));
            }
            this.a.setSelectedWc(oi5.ordinal() == this.e.a.ordinal());
            if (oi5.ordinal() == this.e.a.ordinal()) {
                View view = this.c;
                Wg6.b(view, "ivIndicator");
                view.setBackground(W6.f(PortfolioApp.get.instance(), 2131230956));
                return;
            }
            View view2 = this.c;
            Wg6.b(view2, "ivIndicator");
            view2.setBackground(W6.f(PortfolioApp.get.instance(), 2131230957));
        }
    }

    @DexIgnore
    public Jr4(ArrayList<Oi5> arrayList, Ai ai) {
        Wg6.c(arrayList, "mData");
        this.b = arrayList;
        this.c = ai;
        this.a = Oi5.WORKOUT;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Jr4(ArrayList arrayList, Ai ai, int i, Qg6 qg6) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : ai);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    public final int i(Oi5 oi5) {
        T t;
        boolean z;
        Wg6.c(oi5, "workoutWrapperType");
        Iterator<T> it = this.b.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (next == oi5) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            return this.b.indexOf(t2);
        }
        return -1;
    }

    @DexIgnore
    public final Ai j() {
        return this.c;
    }

    @DexIgnore
    public void k(Bi bi, int i) {
        Wg6.c(bi, "holder");
        if (getItemCount() > i && i != -1) {
            Oi5 oi5 = this.b.get(i);
            Wg6.b(oi5, "mData[position]");
            bi.a(oi5);
        }
    }

    @DexIgnore
    public Bi l(ViewGroup viewGroup, int i) {
        Wg6.c(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558730, viewGroup, false);
        Wg6.b(inflate, "LayoutInflater.from(pare\u2026kout_type, parent, false)");
        return new Bi(this, inflate);
    }

    @DexIgnore
    public final void m(List<? extends Oi5> list) {
        Wg6.c(list, "data");
        this.b.clear();
        this.b.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void n(Ai ai) {
        Wg6.c(ai, "listener");
        this.c = ai;
    }

    @DexIgnore
    public final void o(Oi5 oi5) {
        Wg6.c(oi5, "workoutWrapperType");
        this.a = oi5;
        notifyDataSetChanged();
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [androidx.recyclerview.widget.RecyclerView$ViewHolder, int] */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ void onBindViewHolder(Bi bi, int i) {
        k(bi, i);
    }

    @DexIgnore
    /* Return type fixed from 'androidx.recyclerview.widget.RecyclerView$ViewHolder' to match base method */
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public /* bridge */ /* synthetic */ Bi onCreateViewHolder(ViewGroup viewGroup, int i) {
        return l(viewGroup, i);
    }
}
