package com.fossil;

import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class T36 implements Factory<DoNotDisturbScheduledTimePresenter> {
    @DexIgnore
    public static DoNotDisturbScheduledTimePresenter a(R36 r36, DNDSettingsDatabase dNDSettingsDatabase) {
        return new DoNotDisturbScheduledTimePresenter(r36, dNDSettingsDatabase);
    }
}
