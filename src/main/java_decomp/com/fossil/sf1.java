package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Sf1<T> implements Id1<T> {
    @DexIgnore
    public /* final */ T b;

    @DexIgnore
    public Sf1(T t) {
        Ik1.d(t);
        this.b = t;
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public void b() {
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public final int c() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public Class<T> d() {
        return (Class<T>) this.b.getClass();
    }

    @DexIgnore
    @Override // com.fossil.Id1
    public final T get() {
        return this.b;
    }
}
