package com.fossil;

import com.fossil.U24;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class H34<E> extends U24<E> implements Set<E> {
    @DexIgnore
    public static /* final */ int MAX_TABLE_SIZE; // = 1073741824;
    @DexIgnore
    @LazyInit
    public transient Y24<E> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai<E> extends U24.Ai<E> {
        @DexIgnore
        public Ai() {
            this(4);
        }

        @DexIgnore
        public Ai(int i) {
            super(i);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.U24.Bi
        public /* bridge */ /* synthetic */ U24.Bi a(Object obj) {
            return g(obj);
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<E> g(E e) {
            super.e(e);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<E> h(E... eArr) {
            super.b(eArr);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<E> i(Iterator<? extends E> it) {
            super.c(it);
            return this;
        }

        @DexIgnore
        public H34<E> j() {
            H34<E> a2 = H34.a(this.b, this.a);
            this.b = a2.size();
            return a2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Bi<E> extends H34<E> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Aii extends S24<E> {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            @Override // com.fossil.S24
            public Bi<E> delegateCollection() {
                return Bi.this;
            }

            @DexIgnore
            @Override // java.util.List
            public E get(int i) {
                return (E) Bi.this.get(i);
            }
        }

        @DexIgnore
        @Override // com.fossil.H34
        public Y24<E> createAsList() {
            return new Aii();
        }

        @DexIgnore
        public abstract E get(int i);

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.U24, com.fossil.U24, com.fossil.H34, com.fossil.H34, java.lang.Iterable
        public H54<E> iterator() {
            return asList().iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ci implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Object[] elements;

        @DexIgnore
        public Ci(Object[] objArr) {
            this.elements = objArr;
        }

        @DexIgnore
        public Object readResolve() {
            return H34.copyOf(this.elements);
        }
    }

    @DexIgnore
    public static <E> H34<E> a(int i, Object... objArr) {
        int i2;
        if (i == 0) {
            return of();
        }
        if (i == 1) {
            return of(objArr[0]);
        }
        int chooseTableSize = chooseTableSize(i);
        Object[] objArr2 = new Object[chooseTableSize];
        int i3 = chooseTableSize - 1;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        while (i6 < i) {
            Object obj = objArr[i6];
            H44.b(obj, i6);
            int hashCode = obj.hashCode();
            int b2 = R24.b(hashCode);
            while (true) {
                int i7 = b2 & i3;
                Object obj2 = objArr2[i7];
                if (obj2 == null) {
                    objArr[i5] = obj;
                    objArr2[i7] = obj;
                    i2 = i4 + hashCode;
                    i5++;
                    break;
                } else if (obj2.equals(obj)) {
                    i2 = i4;
                    break;
                } else {
                    b2++;
                }
            }
            i6++;
            i4 = i2;
        }
        Arrays.fill(objArr, i5, i, (Object) null);
        if (i5 == 1) {
            return new A54(objArr[0], i4);
        }
        if (chooseTableSize != chooseTableSize(i5)) {
            return a(i5, objArr);
        }
        if (i5 < objArr.length) {
            objArr = H44.a(objArr, i5);
        }
        return new R44(objArr, i4, objArr2, i3);
    }

    @DexIgnore
    public static H34 b(EnumSet enumSet) {
        return X24.asImmutable(EnumSet.copyOf(enumSet));
    }

    @DexIgnore
    public static <E> Ai<E> builder() {
        return new Ai<>();
    }

    @DexIgnore
    public static int chooseTableSize(int i) {
        boolean z = true;
        if (i < 751619276) {
            int highestOneBit = Integer.highestOneBit(i - 1);
            while (true) {
                highestOneBit <<= 1;
                if (((double) highestOneBit) * 0.7d >= ((double) i)) {
                    return highestOneBit;
                }
            }
        } else {
            if (i >= 1073741824) {
                z = false;
            }
            I14.e(z, "collection too large");
            return 1073741824;
        }
    }

    @DexIgnore
    public static <E> H34<E> copyOf(Iterable<? extends E> iterable) {
        return iterable instanceof Collection ? copyOf((Collection) iterable) : copyOf(iterable.iterator());
    }

    @DexIgnore
    public static <E> H34<E> copyOf(Collection<? extends E> collection) {
        if ((collection instanceof H34) && !(collection instanceof M34)) {
            H34<E> h34 = (H34) collection;
            if (!h34.isPartialView()) {
                return h34;
            }
        } else if (collection instanceof EnumSet) {
            return b((EnumSet) collection);
        }
        Object[] array = collection.toArray();
        return a(array.length, array);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: com.fossil.H34$Ai */
    /* JADX WARN: Multi-variable type inference failed */
    public static <E> H34<E> copyOf(Iterator<? extends E> it) {
        if (!it.hasNext()) {
            return of();
        }
        Object next = it.next();
        if (!it.hasNext()) {
            return of(next);
        }
        Ai ai = new Ai();
        ai.g(next);
        ai.i(it);
        return ai.j();
    }

    @DexIgnore
    public static <E> H34<E> copyOf(E[] eArr) {
        int length = eArr.length;
        return length != 0 ? length != 1 ? a(eArr.length, (Object[]) eArr.clone()) : of((Object) eArr[0]) : of();
    }

    @DexIgnore
    public static <E> H34<E> of() {
        return R44.EMPTY;
    }

    @DexIgnore
    public static <E> H34<E> of(E e) {
        return new A54(e);
    }

    @DexIgnore
    public static <E> H34<E> of(E e, E e2) {
        return a(2, e, e2);
    }

    @DexIgnore
    public static <E> H34<E> of(E e, E e2, E e3) {
        return a(3, e, e2, e3);
    }

    @DexIgnore
    public static <E> H34<E> of(E e, E e2, E e3, E e4) {
        return a(4, e, e2, e3, e4);
    }

    @DexIgnore
    public static <E> H34<E> of(E e, E e2, E e3, E e4, E e5) {
        return a(5, e, e2, e3, e4, e5);
    }

    @DexIgnore
    @SafeVarargs
    public static <E> H34<E> of(E e, E e2, E e3, E e4, E e5, E e6, E... eArr) {
        int length = eArr.length + 6;
        Object[] objArr = new Object[length];
        objArr[0] = e;
        objArr[1] = e2;
        objArr[2] = e3;
        objArr[3] = e4;
        objArr[4] = e5;
        objArr[5] = e6;
        System.arraycopy(eArr, 0, objArr, 6, eArr.length);
        return a(length, objArr);
    }

    @DexIgnore
    @Override // com.fossil.U24
    public Y24<E> asList() {
        Y24<E> y24 = this.b;
        if (y24 != null) {
            return y24;
        }
        Y24<E> createAsList = createAsList();
        this.b = createAsList;
        return createAsList;
    }

    @DexIgnore
    public Y24<E> createAsList() {
        return new M44(this, toArray());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof H34) || !isHashCodeFast() || !((H34) obj).isHashCodeFast() || hashCode() == obj.hashCode()) {
            return X44.a(this, obj);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return X44.b(this);
    }

    @DexIgnore
    public boolean isHashCodeFast() {
        return false;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, com.fossil.U24, com.fossil.U24, java.lang.Iterable
    public abstract H54<E> iterator();

    @DexIgnore
    @Override // com.fossil.U24
    public Object writeReplace() {
        return new Ci(toArray());
    }
}
