package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.Rg2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vd2 extends Hl2 implements Oc2 {
    @DexIgnore
    public Vd2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ISignInButtonCreator");
    }

    @DexIgnore
    @Override // com.fossil.Oc2
    public final Rg2 P0(Rg2 rg2, Uc2 uc2) throws RemoteException {
        Parcel d = d();
        Il2.c(d, rg2);
        Il2.d(d, uc2);
        Parcel e = e(2, d);
        Rg2 e2 = Rg2.Ai.e(e.readStrongBinder());
        e.recycle();
        return e2;
    }
}
