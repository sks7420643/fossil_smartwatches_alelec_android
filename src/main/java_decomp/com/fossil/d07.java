package com.fossil;

import com.portfolio.platform.usecase.RequestEmailOtp;
import com.portfolio.platform.usecase.VerifyEmailOtp;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class D07 implements MembersInjector<B07> {
    @DexIgnore
    public static void a(B07 b07, RequestEmailOtp requestEmailOtp) {
        b07.e = requestEmailOtp;
    }

    @DexIgnore
    public static void b(B07 b07, VerifyEmailOtp verifyEmailOtp) {
        b07.f = verifyEmailOtp;
    }

    @DexIgnore
    public static void c(B07 b07) {
        b07.w();
    }
}
