package com.fossil;

import com.mapped.Lk6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Nu7 {
    @DexIgnore
    public static final void a(Lk6<?> lk6, Dw7 dw7) {
        lk6.e(new Ew7(dw7));
    }

    @DexIgnore
    public static final <T> Lu7<T> b(Xe6<? super T> xe6) {
        if (!(xe6 instanceof Vv7)) {
            return new Lu7<>(xe6, 0);
        }
        Lu7<T> l = ((Vv7) xe6).l();
        if (l != null) {
            if (!l.B()) {
                l = null;
            }
            if (l != null) {
                return l;
            }
        }
        return new Lu7<>(xe6, 0);
    }

    @DexIgnore
    public static final void c(Lk6<?> lk6, Lz7 lz7) {
        lk6.e(new Ox7(lz7));
    }
}
