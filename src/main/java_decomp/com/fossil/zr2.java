package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Zr2 extends Nq2 {
    @DexIgnore
    public /* final */ /* synthetic */ Ga3 s;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Zr2(Xr2 xr2, R62 r62, Ga3 ga3) {
        super(r62);
        this.s = ga3;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.M62$Bi] */
    @Override // com.fossil.I72
    public final /* synthetic */ void u(Fr2 fr2) throws RemoteException {
        fr2.v0(Q72.b(this.s, Ga3.class.getSimpleName()), new Oq2(this));
    }
}
