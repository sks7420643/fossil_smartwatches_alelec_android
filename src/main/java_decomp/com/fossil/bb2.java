package com.fossil;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.L72;
import com.fossil.M62;
import com.fossil.M62.Di;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bb2<O extends M62.Di> extends Q62<O> {
    @DexIgnore
    public /* final */ M62.Fi j;
    @DexIgnore
    public /* final */ Wa2 k;
    @DexIgnore
    public /* final */ Ac2 l;
    @DexIgnore
    public /* final */ M62.Ai<? extends Ys3, Gs3> m;

    @DexIgnore
    public Bb2(Context context, M62<O> m62, Looper looper, M62.Fi fi, Wa2 wa2, Ac2 ac2, M62.Ai<? extends Ys3, Gs3> ai) {
        super(context, m62, looper);
        this.j = fi;
        this.k = wa2;
        this.l = ac2;
        this.m = ai;
        this.i.i(this);
    }

    @DexIgnore
    @Override // com.fossil.Q62
    public final M62.Fi n(Looper looper, L72.Ai<O> ai) {
        this.k.a(ai);
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.Q62
    public final X92 p(Context context, Handler handler) {
        return new X92(context, handler, this.l, this.m);
    }

    @DexIgnore
    public final M62.Fi s() {
        return this.j;
    }
}
