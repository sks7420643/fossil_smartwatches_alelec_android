package com.fossil;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Z68 implements X68 {
    @DexIgnore
    public String b;
    @DexIgnore
    public Y68 c;

    @DexIgnore
    public Z68(String str) {
        c(str);
    }

    @DexIgnore
    public static Integer b(StringTokenizer stringTokenizer) {
        try {
            String nextToken = stringTokenizer.nextToken();
            if (nextToken.length() <= 1 || !nextToken.startsWith("0")) {
                return Integer.valueOf(nextToken);
            }
            throw new NumberFormatException("Number part has a leading 0: '" + nextToken + "'");
        } catch (NoSuchElementException e) {
            throw new NumberFormatException("Number is invalid");
        }
    }

    @DexIgnore
    public int a(X68 x68) {
        return x68 instanceof Z68 ? this.c.a(((Z68) x68).c) : a(new Z68(x68.toString()));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c(java.lang.String r6) {
        /*
            r5 = this;
            r1 = 1
            r0 = 0
            com.fossil.Y68 r2 = new com.fossil.Y68
            r2.<init>(r6)
            r5.c = r2
            java.lang.String r2 = "-"
            int r3 = r6.indexOf(r2)
            if (r3 >= 0) goto L_0x003a
            r3 = 0
            r2 = r6
        L_0x0013:
            if (r3 == 0) goto L_0x0026
            int r4 = r3.length()     // Catch:{ NumberFormatException -> 0x0048 }
            if (r4 == r1) goto L_0x0023
            java.lang.String r4 = "0"
            boolean r4 = r3.startsWith(r4)     // Catch:{ NumberFormatException -> 0x0048 }
            if (r4 != 0) goto L_0x0045
        L_0x0023:
            java.lang.Integer.valueOf(r3)     // Catch:{ NumberFormatException -> 0x0048 }
        L_0x0026:
            java.lang.String r3 = "."
            boolean r3 = r2.contains(r3)
            if (r3 != 0) goto L_0x0050
            java.lang.String r3 = "0"
            boolean r3 = r2.startsWith(r3)
            if (r3 != 0) goto L_0x0050
            java.lang.Integer.valueOf(r2)     // Catch:{ NumberFormatException -> 0x004c }
        L_0x0039:
            return
        L_0x003a:
            java.lang.String r2 = r6.substring(r0, r3)
            int r3 = r3 + 1
            java.lang.String r3 = r6.substring(r3)
            goto L_0x0013
        L_0x0045:
            r5.b = r3
            goto L_0x0026
        L_0x0048:
            r4 = move-exception
            r5.b = r3
            goto L_0x0026
        L_0x004c:
            r0 = move-exception
            r5.b = r6
            goto L_0x0039
        L_0x0050:
            java.util.StringTokenizer r3 = new java.util.StringTokenizer
            java.lang.String r4 = "."
            r3.<init>(r2, r4)
            b(r3)     // Catch:{ NumberFormatException -> 0x00a6 }
            boolean r4 = r3.hasMoreTokens()     // Catch:{ NumberFormatException -> 0x00a6 }
            if (r4 == 0) goto L_0x0063
            b(r3)     // Catch:{ NumberFormatException -> 0x00a6 }
        L_0x0063:
            boolean r4 = r3.hasMoreTokens()     // Catch:{ NumberFormatException -> 0x00a6 }
            if (r4 == 0) goto L_0x006c
            b(r3)     // Catch:{ NumberFormatException -> 0x00a6 }
        L_0x006c:
            boolean r4 = r3.hasMoreTokens()     // Catch:{ NumberFormatException -> 0x00a6 }
            if (r4 == 0) goto L_0x0088
            java.lang.String r0 = r3.nextToken()     // Catch:{ NumberFormatException -> 0x00a6 }
            r5.b = r0     // Catch:{ NumberFormatException -> 0x00a6 }
            java.lang.String r0 = "\\d+"
            java.util.regex.Pattern r0 = java.util.regex.Pattern.compile(r0)     // Catch:{ NumberFormatException -> 0x00a6 }
            java.lang.String r3 = r5.b     // Catch:{ NumberFormatException -> 0x00a6 }
            java.util.regex.Matcher r0 = r0.matcher(r3)     // Catch:{ NumberFormatException -> 0x00a6 }
            boolean r0 = r0.matches()     // Catch:{ NumberFormatException -> 0x00a6 }
        L_0x0088:
            java.lang.String r3 = ".."
            boolean r3 = r2.contains(r3)     // Catch:{ NumberFormatException -> 0x00a6 }
            if (r3 != 0) goto L_0x00a9
            java.lang.String r3 = "."
            boolean r3 = r2.startsWith(r3)     // Catch:{ NumberFormatException -> 0x00a6 }
            if (r3 != 0) goto L_0x00a9
            java.lang.String r3 = "."
            boolean r2 = r2.endsWith(r3)     // Catch:{ NumberFormatException -> 0x00a6 }
            if (r2 == 0) goto L_0x00a1
            r0 = r1
        L_0x00a1:
            if (r0 == 0) goto L_0x0039
            r5.b = r6
            goto L_0x0039
        L_0x00a6:
            r0 = move-exception
            r0 = r1
            goto L_0x00a1
        L_0x00a9:
            r0 = r1
            goto L_0x00a1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Z68.c(java.lang.String):void");
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(X68 x68) {
        return a(x68);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof X68)) {
            return false;
        }
        return a((X68) obj) == 0;
    }

    @DexIgnore
    public int hashCode() {
        return this.c.hashCode() + 11;
    }

    @DexIgnore
    public String toString() {
        return this.c.toString();
    }
}
