package com.fossil;

import com.fossil.A34;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Arrays;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class T24<K, V> extends A34<K, V> implements Y14<K, V> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<K, V> extends A34.Bi<K, V> {
        @DexIgnore
        @Override // com.fossil.A34.Bi
        public /* bridge */ /* synthetic */ A34 a() {
            return g();
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.A34.Bi
        public /* bridge */ /* synthetic */ A34.Bi c(Object obj, Object obj2) {
            h(obj, obj2);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.A34.Bi
        public /* bridge */ /* synthetic */ A34.Bi d(Map.Entry entry) {
            i(entry);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.A34.Bi
        public /* bridge */ /* synthetic */ A34.Bi e(Iterable iterable) {
            j(iterable);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.A34.Bi
        public /* bridge */ /* synthetic */ A34.Bi f(Map map) {
            k(map);
            return this;
        }

        @DexIgnore
        public T24<K, V> g() {
            int i = this.c;
            if (i == 0) {
                return T24.of();
            }
            if (i == 1) {
                return T24.of((Object) this.b[0].getKey(), (Object) this.b[0].getValue());
            }
            if (this.a != null) {
                if (this.d) {
                    this.b = (B34[]) H44.a(this.b, i);
                }
                Arrays.sort(this.b, 0, this.c, I44.from(this.a).onResultOf(X34.s()));
            }
            this.d = this.c == this.b.length;
            return N44.fromEntryArray(this.c, this.b);
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<K, V> h(K k, V v) {
            super.c(k, v);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<K, V> i(Map.Entry<? extends K, ? extends V> entry) {
            super.d(entry);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<K, V> j(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.e(iterable);
            return this;
        }

        @DexIgnore
        @CanIgnoreReturnValue
        public Ai<K, V> k(Map<? extends K, ? extends V> map) {
            super.f(map);
            return this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi extends A34.Ei {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;

        @DexIgnore
        public Bi(T24<?, ?> t24) {
            super(t24);
        }

        @DexIgnore
        @Override // com.fossil.A34.Ei
        public Object readResolve() {
            return createMap(new Ai());
        }
    }

    @DexIgnore
    public static <K, V> Ai<K, V> builder() {
        return new Ai<>();
    }

    @DexIgnore
    public static <K, V> T24<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        Map.Entry[] entryArr = (Map.Entry[]) O34.i(iterable, A34.EMPTY_ENTRY_ARRAY);
        int length = entryArr.length;
        if (length == 0) {
            return of();
        }
        if (length != 1) {
            return N44.fromEntries(entryArr);
        }
        Map.Entry entry = entryArr[0];
        return of(entry.getKey(), entry.getValue());
    }

    @DexIgnore
    public static <K, V> T24<K, V> copyOf(Map<? extends K, ? extends V> map) {
        if (map instanceof T24) {
            T24<K, V> t24 = (T24) map;
            if (!t24.isPartialView()) {
                return t24;
            }
        }
        return copyOf((Iterable) map.entrySet());
    }

    @DexIgnore
    public static <K, V> T24<K, V> of() {
        return N44.EMPTY;
    }

    @DexIgnore
    public static <K, V> T24<K, V> of(K k, V v) {
        return new Y44(k, v);
    }

    @DexIgnore
    public static <K, V> T24<K, V> of(K k, V v, K k2, V v2) {
        return N44.fromEntries(A34.entryOf(k, v), A34.entryOf(k2, v2));
    }

    @DexIgnore
    public static <K, V> T24<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return N44.fromEntries(A34.entryOf(k, v), A34.entryOf(k2, v2), A34.entryOf(k3, v3));
    }

    @DexIgnore
    public static <K, V> T24<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return N44.fromEntries(A34.entryOf(k, v), A34.entryOf(k2, v2), A34.entryOf(k3, v3), A34.entryOf(k4, v4));
    }

    @DexIgnore
    public static <K, V> T24<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return N44.fromEntries(A34.entryOf(k, v), A34.entryOf(k2, v2), A34.entryOf(k3, v3), A34.entryOf(k4, v4), A34.entryOf(k5, v5));
    }

    @DexIgnore
    @CanIgnoreReturnValue
    @Deprecated
    public V forcePut(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract T24<V, K> inverse();

    @DexIgnore
    @Override // java.util.Map, com.fossil.A34, com.fossil.A34
    public H34<V> values() {
        return inverse().keySet();
    }

    @DexIgnore
    @Override // com.fossil.A34
    public Object writeReplace() {
        return new Bi(this);
    }
}
