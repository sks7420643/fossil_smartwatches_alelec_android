package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class By7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "kotlinx.coroutines.TimeoutKt", f = "Timeout.kt", l = {54}, m = "withTimeoutOrNull")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;

        @DexIgnore
        public Ai(Xe6 xe6) {
            super(xe6);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return By7.c(0, null, this);
        }
    }

    @DexIgnore
    public static final Zx7 a(long j, Rm6 rm6) {
        return new Zx7("Timed out waiting for " + j + " ms", rm6);
    }

    @DexIgnore
    public static final <U, T extends U> Object b(Ay7<U, ? super T> ay7, Coroutine<? super Il6, ? super Xe6<? super T>, ? extends Object> coroutine) {
        Bx7.f(ay7, Uv7.b(ay7.e.getContext()).G(ay7.f, ay7));
        return E08.d(ay7, ay7, coroutine);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final <T> java.lang.Object c(long r8, com.mapped.Coroutine<? super com.mapped.Il6, ? super com.mapped.Xe6<? super T>, ? extends java.lang.Object> r10, com.mapped.Xe6<? super T> r11) {
        /*
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r11 instanceof com.fossil.By7.Ai
            if (r0 == 0) goto L_0x0030
            r0 = r11
            com.fossil.By7$Ai r0 = (com.fossil.By7.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0030
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0015:
            java.lang.Object r3 = r2.result
            java.lang.Object r5 = com.fossil.Yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x004b
            if (r0 != r6) goto L_0x0043
            java.lang.Object r0 = r2.L$1
            com.mapped.Jh6 r0 = (com.mapped.Jh6) r0
            java.lang.Object r1 = r2.L$0
            com.mapped.Coroutine r1 = (com.mapped.Coroutine) r1
            long r6 = r2.J$0
            com.fossil.El7.b(r3)     // Catch:{ Zx7 -> 0x0037 }
            r0 = r3
        L_0x002f:
            return r0
        L_0x0030:
            com.fossil.By7$Ai r0 = new com.fossil.By7$Ai
            r0.<init>(r11)
            r2 = r0
            goto L_0x0015
        L_0x0037:
            r1 = move-exception
            r2 = r1
        L_0x0039:
            com.mapped.Rm6 r1 = r2.coroutine
            T r0 = r0.element
            com.fossil.Ay7 r0 = (com.fossil.Ay7) r0
            if (r1 != r0) goto L_0x0081
            r0 = r4
            goto L_0x002f
        L_0x0043:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x004b:
            com.fossil.El7.b(r3)
            r0 = 0
            int r0 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
            if (r0 > 0) goto L_0x0056
            r0 = r4
            goto L_0x002f
        L_0x0056:
            com.mapped.Jh6 r1 = new com.mapped.Jh6
            r1.<init>()
            r1.element = r4
            r2.J$0 = r8     // Catch:{ Zx7 -> 0x007e }
            r2.L$0 = r10     // Catch:{ Zx7 -> 0x007e }
            r2.L$1 = r1     // Catch:{ Zx7 -> 0x007e }
            r0 = 1
            r2.label = r0     // Catch:{ Zx7 -> 0x007e }
            com.fossil.Ay7 r0 = new com.fossil.Ay7     // Catch:{ Zx7 -> 0x007e }
            r0.<init>(r8, r2)     // Catch:{ Zx7 -> 0x007e }
            r1.element = r0     // Catch:{ Zx7 -> 0x007e }
            java.lang.Object r0 = b(r0, r10)     // Catch:{ Zx7 -> 0x007e }
            java.lang.Object r3 = com.fossil.Yn7.d()     // Catch:{ Zx7 -> 0x007e }
            if (r0 != r3) goto L_0x007a
            com.fossil.Go7.c(r2)     // Catch:{ Zx7 -> 0x007e }
        L_0x007a:
            if (r0 != r5) goto L_0x002f
            r0 = r5
            goto L_0x002f
        L_0x007e:
            r2 = move-exception
            r0 = r1
            goto L_0x0039
        L_0x0081:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.By7.c(long, com.mapped.Coroutine, com.mapped.Xe6):java.lang.Object");
    }
}
