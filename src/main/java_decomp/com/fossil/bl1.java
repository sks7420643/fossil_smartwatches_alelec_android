package com.fossil;

import com.mapped.Qg6;
import com.mapped.U40;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bl1 extends Yx1 {
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ U40 b;
    @DexIgnore
    public /* final */ Nr c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Bl1 a(Nr nr, HashMap<Bi, Object> hashMap) {
            return new Bl1(U40.e.a(nr, hashMap), nr);
        }
    }

    @DexIgnore
    public enum Bi {
        b
    }

    @DexIgnore
    public Bl1(U40 u40, Nr nr) {
        super(u40);
        this.b = u40;
        this.c = nr;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Bl1(U40 u40, Nr nr, int i) {
        this(u40, (i & 2) != 0 ? new Nr(null, Zq.b, null, null, 13) : nr);
    }

    @DexIgnore
    public final Nr a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.Yx1
    public U40 getErrorCode() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Yx1, com.fossil.Ox1
    public JSONObject toJSONObject() {
        JSONObject jSONObject = super.toJSONObject();
        Nr nr = this.c;
        if (nr.c != Zq.b) {
            G80.k(jSONObject, Jd0.B1, nr.toJSONObject());
        }
        return jSONObject;
    }
}
