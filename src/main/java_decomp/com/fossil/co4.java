package com.fossil;

import java.lang.reflect.Array;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Co4 {
    @DexIgnore
    public /* final */ byte[][] a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public Co4(int i, int i2) {
        this.a = (byte[][]) Array.newInstance(Byte.TYPE, i2, i);
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public void a(byte b2) {
        for (int i = 0; i < this.c; i++) {
            for (int i2 = 0; i2 < this.b; i2++) {
                this.a[i][i2] = (byte) b2;
            }
        }
    }

    @DexIgnore
    public byte b(int i, int i2) {
        return this.a[i2][i];
    }

    @DexIgnore
    public byte[][] c() {
        return this.a;
    }

    @DexIgnore
    public int d() {
        return this.c;
    }

    @DexIgnore
    public int e() {
        return this.b;
    }

    @DexIgnore
    public void f(int i, int i2, int i3) {
        this.a[i2][i] = (byte) ((byte) i3);
    }

    @DexIgnore
    public void g(int i, int i2, boolean z) {
        this.a[i2][i] = (byte) (z ? (byte) 1 : 0);
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder((this.b * 2 * this.c) + 2);
        for (int i = 0; i < this.c; i++) {
            for (int i2 = 0; i2 < this.b; i2++) {
                byte b2 = this.a[i][i2];
                if (b2 == 0) {
                    sb.append(" 0");
                } else if (b2 != 1) {
                    sb.append("  ");
                } else {
                    sb.append(" 1");
                }
            }
            sb.append('\n');
        }
        return sb.toString();
    }
}
