package com.fossil;

import android.util.Log;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.nn5;
import com.fossil.qw0;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.UserDao;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase;
import java.io.File;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SupportFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bn5 {

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public static u08 f458a; // = w08.b(false, 1, null);
    @DexIgnore
    public static volatile UserDatabase b;
    @DexIgnore
    public static volatile DianaCustomizeDatabase c;
    @DexIgnore
    public static volatile FitnessDatabase d;
    @DexIgnore
    public static volatile SleepDatabase e;
    @DexIgnore
    public static volatile GoalTrackingDatabase f;
    @DexIgnore
    public static volatile ThirdPartyDatabase g;
    @DexIgnore
    public static volatile AlarmDatabase h;
    @DexIgnore
    public static volatile WorkoutSettingDatabase i;
    @DexIgnore
    public static /* final */ bn5 j; // = new bn5();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {340}, m = "getAlarmDatabase")
    public static final class a extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(bn5 bn5, qn7 qn7) {
            super(qn7);
            this.this$0 = bn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.u(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {362}, m = "getDianaCustomizeDatabase")
    public static final class b extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(bn5 bn5, qn7 qn7) {
            super(qn7);
            this.this$0 = bn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.v(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {329}, m = "getFitnessDatabase")
    public static final class c extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(bn5 bn5, qn7 qn7) {
            super(qn7);
            this.this$0 = bn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.y(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {307}, m = "getGoalTrackingDatabase")
    public static final class d extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(bn5 bn5, qn7 qn7) {
            super(qn7);
            this.this$0 = bn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.A(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {318}, m = "getSleepDatabase")
    public static final class e extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(bn5 bn5, qn7 qn7) {
            super(qn7);
            this.this$0 = bn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.D(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {351}, m = "getThirdPartyDatabase")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(bn5 bn5, qn7 qn7) {
            super(qn7);
            this.this$0 = bn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.F(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {296}, m = "getUserDatabase")
    public static final class g extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(bn5 bn5, qn7 qn7) {
            super(qn7);
            this.this$0 = bn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.H(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager", f = "EncryptionDatabaseManager.kt", l = {380}, m = "getWorkoutSettingDatabase")
    public static final class h extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ bn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(bn5 bn5, qn7 qn7) {
            super(qn7);
            this.this$0 = bn5;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.J(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.manager.EncryptedDatabaseManager$provideEncryptedDatabase$2", f = "EncryptionDatabaseManager.kt", l = {390, 56}, m = "invokeSuspend")
    public static final class i extends ko7 implements vp7<iv7, qn7<? super Integer>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;

        @DexIgnore
        public i(qn7 qn7) {
            super(2, qn7);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            i iVar = new i(qn7);
            iVar.p$ = (iv7) obj;
            throw null;
            //return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super Integer> qn7) {
            throw null;
            //return ((i) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00dd A[Catch:{ all -> 0x0129 }] */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0120  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
            // Method dump skipped, instructions count: 316
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.bn5.i.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object A(com.fossil.qn7<? super com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.bn5.d
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.fossil.bn5$d r0 = (com.fossil.bn5.d) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.fossil.bn5 r0 = (com.fossil.bn5) r0
            com.fossil.el7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getGoalTrackingDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = com.fossil.bn5.f
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.fossil.bn5$d r0 = new com.fossil.bn5$d
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r2)
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r2 = com.fossil.bn5.f
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getGoalTrackingDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.fossil.pq7.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = com.fossil.bn5.f
            if (r0 != 0) goto L_0x0032
            com.fossil.pq7.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bn5.A(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final UserDatabase B() {
        return b;
    }

    @DexIgnore
    public final SleepDatabase C(char[] cArr) {
        String str;
        UserDao userDao;
        MFUser currentUser;
        UserDatabase userDatabase = b;
        if (userDatabase == null || (userDao = userDatabase.userDao()) == null || (currentUser = userDao.getCurrentUser()) == null || (str = currentUser.getUserId()) == null) {
            str = "Anonymous";
        }
        String str2 = str + "_sleep.db";
        nn5.a c2 = nn5.f2550a.c(PortfolioApp.h0.c(), str2);
        Log.d("EncryptionDM", "getSleepDatabase isEncrypted " + c2 + " dbName " + str2);
        if (c2 == nn5.a.UNENCRYPTED) {
            Log.d("EncryptionDM", "getSleepDatabase encrypt fitness db with passphrase " + ((Object) cArr));
            nn5 nn5 = nn5.f2550a;
            PortfolioApp c3 = PortfolioApp.h0.c();
            File databasePath = PortfolioApp.h0.c().getDatabasePath(str2);
            pq7.b(databasePath, "PortfolioApp.instance.getDatabasePath(dbName)");
            nn5.b(c3, databasePath, cArr != null ? (char[]) cArr.clone() : null);
        }
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            if (!nn5.f2550a.e(PortfolioApp.h0.c(), str2, bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.h0.c().getDatabasePath(str2).delete();
            }
            qw0.a a2 = pw0.a(PortfolioApp.h0.c(), SleepDatabase.class, str2);
            a2.g(supportFactory);
            a2.b(SleepDatabase.Companion.getMIGRATION_FROM_3_TO_9());
            a2.f();
            a2.e();
            qw0 d2 = a2.d();
            pq7.b(d2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (SleepDatabase) d2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object D(com.fossil.qn7<? super com.portfolio.platform.data.source.local.sleep.SleepDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.bn5.e
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.fossil.bn5$e r0 = (com.fossil.bn5.e) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.fossil.bn5 r0 = (com.fossil.bn5) r0
            com.fossil.el7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getSleepDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = com.fossil.bn5.e
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.fossil.bn5$e r0 = new com.fossil.bn5$e
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r2)
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r2 = com.fossil.bn5.e
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getSleepDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.fossil.pq7.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = com.fossil.bn5.e
            if (r0 != 0) goto L_0x0032
            com.fossil.pq7.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bn5.D(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final ThirdPartyDatabase E(char[] cArr) {
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            nn5.a c2 = nn5.f2550a.c(PortfolioApp.h0.c(), "thirdParty.db");
            Log.d("EncryptionDM", "getThirdPartyDatabase isEncrypted " + c2 + " dbName thirdParty.db");
            if (c2 == nn5.a.UNENCRYPTED) {
                Log.d("EncryptionDM", "getThirdPartyDatabase encrypt fitness db with passphrase " + ((Object) cArr));
                nn5 nn5 = nn5.f2550a;
                PortfolioApp c3 = PortfolioApp.h0.c();
                File databasePath = PortfolioApp.h0.c().getDatabasePath("thirdParty.db");
                pq7.b(databasePath, "PortfolioApp.instance.ge\u2026HIRD_PARTY_DATABASE_NAME)");
                nn5.b(c3, databasePath, (char[]) cArr.clone());
            }
            if (!nn5.f2550a.e(PortfolioApp.h0.c(), "thirdParty.db", bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.h0.c().getDatabasePath("thirdParty.db").delete();
            }
            qw0.a a2 = pw0.a(PortfolioApp.h0.c(), ThirdPartyDatabase.class, "thirdParty.db");
            a2.g(supportFactory);
            a2.f();
            qw0 d2 = a2.d();
            pq7.b(d2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (ThirdPartyDatabase) d2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object F(com.fossil.qn7<? super com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.bn5.f
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.fossil.bn5$f r0 = (com.fossil.bn5.f) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.fossil.bn5 r0 = (com.fossil.bn5) r0
            com.fossil.el7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getThirdPartyDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r0 = com.fossil.bn5.g
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.fossil.bn5$f r0 = new com.fossil.bn5$f
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r2)
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r2 = com.fossil.bn5.g
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getThirdPartyDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.fossil.pq7.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase r0 = com.fossil.bn5.g
            if (r0 != 0) goto L_0x0032
            com.fossil.pq7.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bn5.F(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final UserDatabase G(char[] cArr) {
        if (nn5.f2550a.c(PortfolioApp.h0.c(), "user.db") == nn5.a.UNENCRYPTED) {
            nn5 nn5 = nn5.f2550a;
            PortfolioApp c2 = PortfolioApp.h0.c();
            File databasePath = PortfolioApp.h0.c().getDatabasePath("user.db");
            pq7.b(databasePath, "PortfolioApp.instance.ge\u2026tants.USER_DATABASE_NAME)");
            nn5.b(c2, databasePath, cArr != null ? (char[]) cArr.clone() : null);
        }
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            if (!nn5.f2550a.e(PortfolioApp.h0.c(), "user.db", bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.h0.c().getDatabasePath("user.db").delete();
            }
            qw0.a a2 = pw0.a(PortfolioApp.h0.c(), UserDatabase.class, "user.db");
            a2.b(UserDatabase.Companion.getMIGRATION_FROM_3_TO_4(), UserDatabase.Companion.getMIGRATION_FROM_4_TO_5());
            a2.f();
            a2.e();
            a2.g(supportFactory);
            qw0 d2 = a2.d();
            pq7.b(d2, "Room\n                .da\u2026\n                .build()");
            return (UserDatabase) d2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object H(com.fossil.qn7<? super com.portfolio.platform.data.model.room.UserDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.bn5.g
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.fossil.bn5$g r0 = (com.fossil.bn5.g) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.fossil.bn5 r0 = (com.fossil.bn5) r0
            com.fossil.el7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getUserDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.model.room.UserDatabase r0 = com.fossil.bn5.b
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.fossil.bn5$g r0 = new com.fossil.bn5$g
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r2)
            com.portfolio.platform.data.model.room.UserDatabase r2 = com.fossil.bn5.b
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getUserDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.fossil.pq7.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.model.room.UserDatabase r0 = com.fossil.bn5.b
            if (r0 != 0) goto L_0x0032
            com.fossil.pq7.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bn5.H(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final WorkoutSettingDatabase I(char[] cArr) {
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            nn5.a c2 = nn5.f2550a.c(PortfolioApp.h0.c(), "workoutSetting.db");
            Log.d("EncryptionDM", "getWorkoutSetting isEncrypted " + c2 + " dbName workoutSetting.db");
            if (c2 == nn5.a.UNENCRYPTED) {
                Log.d("EncryptionDM", "getWorkoutSetting encrypt fitness db with passphrase " + ((Object) cArr));
                nn5 nn5 = nn5.f2550a;
                PortfolioApp c3 = PortfolioApp.h0.c();
                File databasePath = PortfolioApp.h0.c().getDatabasePath("workoutSetting.db");
                pq7.b(databasePath, "PortfolioApp.instance.ge\u2026UT_SETTING_DATABASE_NAME)");
                nn5.b(c3, databasePath, (char[]) cArr.clone());
            }
            if (!nn5.f2550a.e(PortfolioApp.h0.c(), "workoutSetting.db", bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.h0.c().getDatabasePath("workoutSetting.db").delete();
            }
            qw0.a a2 = pw0.a(PortfolioApp.h0.c(), WorkoutSettingDatabase.class, "workoutSetting.db");
            a2.g(supportFactory);
            a2.b(WorkoutSettingDatabase.Companion.getMIGRATION_FROM_1_TO_2());
            a2.f();
            qw0 d2 = a2.d();
            pq7.b(d2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (WorkoutSettingDatabase) d2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object J(com.fossil.qn7<? super com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.bn5.h
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.fossil.bn5$h r0 = (com.fossil.bn5.h) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.fossil.bn5 r0 = (com.fossil.bn5) r0
            com.fossil.el7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getWorkoutSettingDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase r0 = com.fossil.bn5.i
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.fossil.bn5$h r0 = new com.fossil.bn5$h
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r2)
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase r2 = com.fossil.bn5.i
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getWorkoutSettingDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.fossil.pq7.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.source.local.workoutsetting.WorkoutSettingDatabase r0 = com.fossil.bn5.i
            if (r0 != 0) goto L_0x0032
            com.fossil.pq7.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bn5.J(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final boolean K() {
        return b == null || d == null || e == null || f == null || g == null || h == null || i == null || c == null;
    }

    @DexIgnore
    public final /* synthetic */ Object L(qn7<? super Integer> qn7) {
        return eu7.g(bw7.b(), new i(null), qn7);
    }

    @DexIgnore
    public final void M() {
        Log.d("EncryptionDM", "resetDatabasePrefix");
        d = null;
        e = null;
        h = null;
        b = null;
        i = null;
        f = null;
        g = null;
    }

    @DexIgnore
    public final AlarmDatabase t(char[] cArr) {
        UserDao userDao;
        MFUser currentUser;
        String userId;
        UserDatabase userDatabase = b;
        String str = (userDatabase == null || (userDao = userDatabase.userDao()) == null || (currentUser = userDao.getCurrentUser()) == null || (userId = currentUser.getUserId()) == null) ? "Anonymous" : userId;
        String str2 = str + "_alarm.db";
        nn5.a c2 = nn5.f2550a.c(PortfolioApp.h0.c(), str2);
        Log.d("EncryptionDM", "getAlarmDatabase isEncrypted " + c2 + " dbName " + str2);
        if (c2 == nn5.a.UNENCRYPTED) {
            Log.d("EncryptionDM", "getAlarmDatabase encrypt fitness db with passphrase " + ((Object) cArr));
            nn5 nn5 = nn5.f2550a;
            PortfolioApp c3 = PortfolioApp.h0.c();
            File databasePath = PortfolioApp.h0.c().getDatabasePath(str2);
            pq7.b(databasePath, "PortfolioApp.instance.getDatabasePath(dbName)");
            nn5.b(c3, databasePath, cArr != null ? (char[]) cArr.clone() : null);
        }
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            if (!nn5.f2550a.e(PortfolioApp.h0.c(), str2, bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.h0.c().getDatabasePath(str2).delete();
            }
            qw0.a a2 = pw0.a(PortfolioApp.h0.c(), AlarmDatabase.class, str2);
            a2.g(supportFactory);
            a2.b(AlarmDatabase.Companion.migrating3Or4To5(str, 3), AlarmDatabase.Companion.migrating3Or4To5(str, 4), AlarmDatabase.Companion.getMIGRATION_FROM_5_TO_6());
            a2.f();
            qw0 d2 = a2.d();
            pq7.b(d2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (AlarmDatabase) d2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object u(com.fossil.qn7<? super com.portfolio.platform.data.source.local.alarm.AlarmDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.bn5.a
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.fossil.bn5$a r0 = (com.fossil.bn5.a) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.fossil.bn5 r0 = (com.fossil.bn5) r0
            com.fossil.el7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getAlarmDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.source.local.alarm.AlarmDatabase r0 = com.fossil.bn5.h
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.fossil.bn5$a r0 = new com.fossil.bn5$a
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r2)
            com.portfolio.platform.data.source.local.alarm.AlarmDatabase r2 = com.fossil.bn5.h
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getAlarmDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.fossil.pq7.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.source.local.alarm.AlarmDatabase r0 = com.fossil.bn5.h
            if (r0 != 0) goto L_0x0032
            com.fossil.pq7.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bn5.u(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object v(com.fossil.qn7<? super com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.bn5.b
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.fossil.bn5$b r0 = (com.fossil.bn5.b) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.fossil.bn5 r0 = (com.fossil.bn5) r0
            com.fossil.el7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getDianaCustomizeDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = com.fossil.bn5.c
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.fossil.bn5$b r0 = new com.fossil.bn5$b
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r2)
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r2 = com.fossil.bn5.c
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getDianaCustomizeDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.fossil.pq7.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = com.fossil.bn5.c
            if (r0 != 0) goto L_0x0032
            com.fossil.pq7.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bn5.v(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final DianaCustomizeDatabase w(char[] cArr) {
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            nn5.a c2 = nn5.f2550a.c(PortfolioApp.h0.c(), "dianaCustomize.db");
            Log.d("EncryptionDM", "getDianaCustomzieDatabase isEncrypted " + c2 + " dbName dianaCustomize.db");
            if (c2 == nn5.a.UNENCRYPTED) {
                Log.d("EncryptionDM", "getDianaCustomzieDatabase encrypt diana customize db with passphrase " + ((Object) cArr));
                nn5 nn5 = nn5.f2550a;
                PortfolioApp c3 = PortfolioApp.h0.c();
                File databasePath = PortfolioApp.h0.c().getDatabasePath("dianaCustomize.db");
                pq7.b(databasePath, "PortfolioApp.instance.ge\u2026_CUSTOMIZE_DATABASE_NAME)");
                nn5.b(c3, databasePath, (char[]) cArr.clone());
            }
            if (!nn5.f2550a.e(PortfolioApp.h0.c(), "dianaCustomize.db", bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.h0.c().getDatabasePath("dianaCustomize.db").delete();
            }
            qw0.a a2 = pw0.a(PortfolioApp.h0.c(), DianaCustomizeDatabase.class, "dianaCustomize.db");
            a2.g(supportFactory);
            a2.b(DianaCustomizeDatabase.Companion.getMIGRATION_FROM_13_TO_14(), DianaCustomizeDatabase.Companion.getMIGRATION_FROM_14_TO_15(), DianaCustomizeDatabase.Companion.getMIGRATION_FROM_15_TO_16());
            qw0 d2 = a2.d();
            pq7.b(d2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (DianaCustomizeDatabase) d2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    public final FitnessDatabase x(char[] cArr) {
        String str;
        UserDao userDao;
        MFUser currentUser;
        UserDatabase userDatabase = b;
        if (userDatabase == null || (userDao = userDatabase.userDao()) == null || (currentUser = userDao.getCurrentUser()) == null || (str = currentUser.getUserId()) == null) {
            str = "Anonymous";
        }
        String str2 = str + "_fitness.db";
        nn5.a c2 = nn5.f2550a.c(PortfolioApp.h0.c(), str2);
        Log.d("EncryptionDM", "getFitnessDatabase isEncrypted " + c2 + " dbName " + str2);
        if (c2 == nn5.a.UNENCRYPTED) {
            Log.d("EncryptionDM", "getFitnessDatabase encrypt fitness db with passphrase " + ((Object) cArr));
            nn5 nn5 = nn5.f2550a;
            PortfolioApp c3 = PortfolioApp.h0.c();
            File databasePath = PortfolioApp.h0.c().getDatabasePath(str2);
            pq7.b(databasePath, "PortfolioApp.instance.getDatabasePath(dbName)");
            nn5.b(c3, databasePath, cArr != null ? (char[]) cArr.clone() : null);
        }
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            if (!nn5.f2550a.e(PortfolioApp.h0.c(), str2, bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.h0.c().getDatabasePath(str2).delete();
            }
            qw0.a a2 = pw0.a(PortfolioApp.h0.c(), FitnessDatabase.class, str2);
            a2.g(supportFactory);
            a2.b(FitnessDatabase.Companion.getMIGRATION_FROM_4_TO_21());
            a2.b(FitnessDatabase.Companion.getMIGRATION_FROM_21_TO_22());
            a2.f();
            a2.e();
            qw0 d2 = a2.d();
            pq7.b(d2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (FitnessDatabase) d2;
        }
        pq7.i();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object y(com.fossil.qn7<? super com.portfolio.platform.data.source.local.fitness.FitnessDatabase> r7) {
        /*
            r6 = this;
            r5 = 0
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.fossil.bn5.c
            if (r0 == 0) goto L_0x0033
            r0 = r7
            com.fossil.bn5$c r0 = (com.fossil.bn5.c) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.fossil.bn5 r0 = (com.fossil.bn5) r0
            com.fossil.el7.b(r2)
        L_0x0027:
            java.lang.String r0 = "EncryptionDM"
            java.lang.String r1 = "getFitnessDatabase done"
            android.util.Log.d(r0, r1)
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = com.fossil.bn5.d
            if (r0 == 0) goto L_0x005b
        L_0x0032:
            return r0
        L_0x0033:
            com.fossil.bn5$c r0 = new com.fossil.bn5$c
            r0.<init>(r6, r7)
            goto L_0x0014
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.el7.b(r2)
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r2 = com.fossil.bn5.d
            if (r2 != 0) goto L_0x005f
            java.lang.String r2 = "EncryptionDM"
            java.lang.String r3 = "getFitnessDatabase start"
            android.util.Log.d(r2, r3)
            r0.L$0 = r6
            r0.label = r4
            java.lang.Object r0 = r6.L(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0032
        L_0x005b:
            com.fossil.pq7.i()
            throw r5
        L_0x005f:
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = com.fossil.bn5.d
            if (r0 != 0) goto L_0x0032
            com.fossil.pq7.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bn5.y(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final GoalTrackingDatabase z(char[] cArr) {
        if (cArr != null) {
            byte[] bytes = SQLiteDatabase.getBytes((char[]) cArr.clone());
            SupportFactory supportFactory = new SupportFactory(bytes);
            nn5.a c2 = nn5.f2550a.c(PortfolioApp.h0.c(), "goalTracking.db");
            Log.d("EncryptionDM", "getGoalTrackingDatabase isEncrypted " + c2 + " dbName goalTracking.db");
            if (c2 == nn5.a.UNENCRYPTED) {
                Log.d("EncryptionDM", "getGoalTrackingDatabase encrypt fitness db with passphrase " + ((Object) cArr));
                nn5 nn5 = nn5.f2550a;
                PortfolioApp c3 = PortfolioApp.h0.c();
                File databasePath = PortfolioApp.h0.c().getDatabasePath("goalTracking.db");
                pq7.b(databasePath, "PortfolioApp.instance.ge\u2026L_TRACKING_DATABASE_NAME)");
                nn5.b(c3, databasePath, (char[]) cArr.clone());
            }
            if (!nn5.f2550a.e(PortfolioApp.h0.c(), "goalTracking.db", bytes)) {
                Log.d("EncryptionDM", "getAlarmDatabase try to open fail");
                PortfolioApp.h0.c().getDatabasePath("goalTracking.db").delete();
            }
            qw0.a a2 = pw0.a(PortfolioApp.h0.c(), GoalTrackingDatabase.class, "goalTracking.db");
            a2.g(supportFactory);
            a2.f();
            qw0 d2 = a2.d();
            pq7.b(d2, "Room.databaseBuilder(Por\u2026\n                .build()");
            return (GoalTrackingDatabase) d2;
        }
        pq7.i();
        throw null;
    }
}
