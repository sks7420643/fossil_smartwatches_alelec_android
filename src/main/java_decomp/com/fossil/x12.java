package com.fossil;

import com.fossil.S32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class X12 implements S32.Ai {
    @DexIgnore
    public /* final */ B22 a;
    @DexIgnore
    public /* final */ H02 b;

    @DexIgnore
    public X12(B22 b22, H02 h02) {
        this.a = b22;
        this.b = h02;
    }

    @DexIgnore
    public static S32.Ai b(B22 b22, H02 h02) {
        return new X12(b22, h02);
    }

    @DexIgnore
    @Override // com.fossil.S32.Ai
    public Object a() {
        return this.a.c.q(this.b);
    }
}
