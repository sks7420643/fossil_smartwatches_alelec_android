package com.fossil;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"BanParcelableUsage"})
public final class Rq0 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Rq0> CREATOR; // = new Ai();
    @DexIgnore
    public ArrayList<Uq0> b;
    @DexIgnore
    public ArrayList<String> c;
    @DexIgnore
    public Jq0[] d;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Rq0> {
        @DexIgnore
        public Rq0 a(Parcel parcel) {
            return new Rq0(parcel);
        }

        @DexIgnore
        public Rq0[] b(int i) {
            return new Rq0[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Rq0 createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Rq0[] newArray(int i) {
            return b(i);
        }
    }

    @DexIgnore
    public Rq0() {
    }

    @DexIgnore
    public Rq0(Parcel parcel) {
        this.b = parcel.createTypedArrayList(Uq0.CREATOR);
        this.c = parcel.createStringArrayList();
        this.d = (Jq0[]) parcel.createTypedArray(Jq0.CREATOR);
        this.e = parcel.readInt();
        this.f = parcel.readString();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.b);
        parcel.writeStringList(this.c);
        parcel.writeTypedArray(this.d, i);
        parcel.writeInt(this.e);
        parcel.writeString(this.f);
    }
}
