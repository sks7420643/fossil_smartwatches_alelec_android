package com.fossil;

import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.Zu4;
import com.mapped.Cd6;
import com.mapped.Lc6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendViewModel;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vu4 extends BaseFragment implements Zu4.Ai {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ Ai u; // = new Ai(null);
    @DexIgnore
    public G37<J35> g;
    @DexIgnore
    public BCInviteFriendViewModel h;
    @DexIgnore
    public Ts4 i;
    @DexIgnore
    public Po4 j;
    @DexIgnore
    public Zu4 k;
    @DexIgnore
    public Zu4 l;
    @DexIgnore
    public Parcelable m;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Vu4.t;
        }

        @DexIgnore
        public final Vu4 b(Ts4 ts4) {
            Vu4 vu4 = new Vu4();
            Bundle bundle = new Bundle();
            bundle.putParcelable("challenge_draft_extra", ts4);
            vu4.setArguments(bundle);
            return vu4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 b;

        @DexIgnore
        public Bi(Vu4 vu4, boolean z) {
            this.b = vu4;
        }

        @DexIgnore
        public final void onClick(View view) {
            Vu4.R6(this.b).C();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 b;

        @DexIgnore
        public Ci(Vu4 vu4, boolean z) {
            this.b = vu4;
        }

        @DexIgnore
        public final void onClick(View view) {
            AnalyticsHelper g = AnalyticsHelper.f.g();
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                g.m("bc_start_type_now", activity);
                Vu4.R6(this.b).I();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type android.app.Activity");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 b;

        @DexIgnore
        public Di(Vu4 vu4, boolean z) {
            this.b = vu4;
        }

        @DexIgnore
        public final void onClick(View view) {
            AnalyticsHelper g = AnalyticsHelper.f.g();
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                g.m("bc_start_type_later", activity);
                Vu4.R6(this.b).H();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type android.app.Activity");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 b;

        @DexIgnore
        public Ei(Vu4 vu4, boolean z) {
            this.b = vu4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.B0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 a;

        @DexIgnore
        public Fi(Vu4 vu4, boolean z) {
            this.a = vu4;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            Vu4.R6(this.a).D(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ J35 a;
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 b;

        @DexIgnore
        public Gi(J35 j35, Vu4 vu4, boolean z) {
            this.a = j35;
            this.b = vu4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.a.x;
            Wg6.b(flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            Vu4.R6(this.b).F();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ J35 b;
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 c;

        @DexIgnore
        public Hi(J35 j35, Vu4 vu4, boolean z) {
            this.b = j35;
            this.c = vu4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (charSequence == null || charSequence.length() == 0) {
                Vu4.R6(this.c).B();
                FlexibleTextView flexibleTextView = this.b.D;
                Wg6.b(flexibleTextView, "tvEmpty");
                flexibleTextView.setVisibility(8);
                ImageView imageView = this.b.r;
                Wg6.b(imageView, "btnSearchClear");
                imageView.setVisibility(8);
                return;
            }
            ImageView imageView2 = this.b.r;
            Wg6.b(imageView2, "btnSearchClear");
            imageView2.setVisibility(0);
            FlexibleCheckBox flexibleCheckBox = this.b.v;
            Wg6.b(flexibleCheckBox, "cbInviteAll");
            flexibleCheckBox.setVisibility(8);
            FlexibleTextView flexibleTextView2 = this.b.y;
            Wg6.b(flexibleTextView2, "ftvInviteAll");
            flexibleTextView2.setVisibility(8);
            if (this.c.m == null) {
                Vu4 vu4 = this.c;
                RecyclerView recyclerView = this.b.B;
                Wg6.b(recyclerView, "rvFriends");
                RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                vu4.m = layoutManager != null ? layoutManager.e1() : null;
                Cd6 cd6 = Cd6.a;
            }
            Vu4.R6(this.c).G(charSequence.toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ J35 b;

        @DexIgnore
        public Ii(J35 j35) {
            this.b = j35;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.w.setText("");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements View.OnFocusChangeListener {
        @DexIgnore
        public static /* final */ Ji b; // = new Ji();

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                Xr4.a.g(PortfolioApp.get.instance());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki<T> implements Ls0<Lc6<? extends Boolean, ? extends Boolean>> {
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 a;

        @DexIgnore
        public Ki(Vu4 vu4) {
            this.a = vu4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, Boolean> lc6) {
            J35 j35 = (J35) Vu4.M6(this.a).a();
            if (j35 != null) {
                Boolean first = lc6.getFirst();
                Boolean second = lc6.getSecond();
                if (first != null) {
                    if (first.booleanValue()) {
                        SwipeRefreshLayout swipeRefreshLayout = j35.C;
                        Wg6.b(swipeRefreshLayout, "swipe");
                        swipeRefreshLayout.setEnabled(true);
                        SwipeRefreshLayout swipeRefreshLayout2 = j35.C;
                        Wg6.b(swipeRefreshLayout2, "swipe");
                        swipeRefreshLayout2.setRefreshing(true);
                        FlexibleCheckBox flexibleCheckBox = j35.v;
                        Wg6.b(flexibleCheckBox, "cbInviteAll");
                        flexibleCheckBox.setVisibility(8);
                        FlexibleTextView flexibleTextView = j35.y;
                        Wg6.b(flexibleTextView, "ftvInviteAll");
                        flexibleTextView.setVisibility(8);
                    } else {
                        SwipeRefreshLayout swipeRefreshLayout3 = j35.C;
                        Wg6.b(swipeRefreshLayout3, "swipe");
                        swipeRefreshLayout3.setRefreshing(false);
                        SwipeRefreshLayout swipeRefreshLayout4 = j35.C;
                        Wg6.b(swipeRefreshLayout4, "swipe");
                        swipeRefreshLayout4.setEnabled(false);
                    }
                }
                if (second == null) {
                    return;
                }
                if (second.booleanValue()) {
                    this.a.b();
                } else {
                    this.a.a();
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends Boolean> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li<T> implements Ls0<Lc6<? extends List<? extends Ot4>, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 a;

        @DexIgnore
        public Li(Vu4 vu4) {
            this.a = vu4;
        }

        @DexIgnore
        public final void a(Lc6<? extends List<Ot4>, ? extends ServerError> lc6) {
            List<Ot4> list = (List) lc6.getFirst();
            if (list != null) {
                J35 j35 = (J35) Vu4.M6(this.a).a();
                if (j35 == null) {
                    return;
                }
                if (list.isEmpty()) {
                    FlexibleTextView flexibleTextView = j35.D;
                    Wg6.b(flexibleTextView, "tvEmpty");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                FlexibleCheckBox flexibleCheckBox = j35.v;
                Wg6.b(flexibleCheckBox, "cbInviteAll");
                flexibleCheckBox.setVisibility(0);
                FlexibleTextView flexibleTextView2 = j35.y;
                Wg6.b(flexibleTextView2, "ftvInviteAll");
                flexibleTextView2.setVisibility(0);
                RecyclerView recyclerView = j35.B;
                Wg6.b(recyclerView, "rvFriends");
                recyclerView.setAdapter(Vu4.P6(this.a));
                Vu4.P6(this.a).j(list);
                Parcelable parcelable = this.a.m;
                if (parcelable != null) {
                    RecyclerView recyclerView2 = j35.B;
                    Wg6.b(recyclerView2, "rvFriends");
                    RecyclerView.m layoutManager = recyclerView2.getLayoutManager();
                    if (layoutManager != null) {
                        layoutManager.d1(parcelable);
                        return;
                    }
                    return;
                }
                return;
            }
            J35 j352 = (J35) Vu4.M6(this.a).a();
            if (j352 != null) {
                SwipeRefreshLayout swipeRefreshLayout = j352.C;
                Wg6.b(swipeRefreshLayout, "swipe");
                swipeRefreshLayout.setEnabled(true);
                SwipeRefreshLayout swipeRefreshLayout2 = j352.C;
                Wg6.b(swipeRefreshLayout2, "swipe");
                swipeRefreshLayout2.setRefreshing(false);
                FlexibleTextView flexibleTextView3 = j352.x;
                Wg6.b(flexibleTextView3, "ftvError");
                flexibleTextView3.setVisibility(0);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends List<? extends Ot4>, ? extends ServerError> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 a;

        @DexIgnore
        public Mi(Vu4 vu4) {
            this.a = vu4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            FlexibleCheckBox flexibleCheckBox;
            J35 j35 = (J35) Vu4.M6(this.a).a();
            if (j35 != null && (flexibleCheckBox = j35.v) != null) {
                Wg6.b(bool, "it");
                flexibleCheckBox.setChecked(bool.booleanValue());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni<T> implements Ls0<Lc6<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 a;

        @DexIgnore
        public Ni(Vu4 vu4) {
            this.a = vu4;
        }

        @DexIgnore
        public final void a(Lc6<Boolean, ? extends ServerError> lc6) {
            if (lc6.getFirst().booleanValue()) {
                this.a.W6();
                return;
            }
            ServerError serverError = (ServerError) lc6.getSecond();
            S37 s37 = S37.c;
            Integer code = serverError != null ? serverError.getCode() : null;
            String message = serverError != null ? serverError.getMessage() : null;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.g(code, message, childFragmentManager);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends Boolean, ? extends ServerError> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi<T> implements Ls0<List<? extends Date>> {
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 a;

        @DexIgnore
        public Oi(Vu4 vu4) {
            this.a = vu4;
        }

        @DexIgnore
        public final void a(List<? extends Date> list) {
            Vu4 vu4 = this.a;
            Wg6.b(list, "it");
            vu4.Z6(list);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends Date> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi<T> implements Ls0<List<? extends Gs4>> {
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 a;

        @DexIgnore
        public Pi(Vu4 vu4) {
            this.a = vu4;
        }

        @DexIgnore
        public final void a(List<Gs4> list) {
            Vu4 vu4 = this.a;
            Wg6.b(list, "it");
            vu4.a7(list);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends Gs4> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Qi<T> implements Ls0<Lc6<? extends List<? extends Ot4>, ? extends String>> {
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 a;

        @DexIgnore
        public Qi(Vu4 vu4) {
            this.a = vu4;
        }

        @DexIgnore
        public final void a(Lc6<? extends List<Ot4>, String> lc6) {
            List<Ot4> list = (List) lc6.getFirst();
            String second = lc6.getSecond();
            J35 j35 = (J35) Vu4.M6(this.a).a();
            if (j35 != null) {
                RecyclerView recyclerView = j35.B;
                Wg6.b(recyclerView, "rvFriends");
                recyclerView.setAdapter(Vu4.O6(this.a));
                Vu4.O6(this.a).j(list);
                if (list.isEmpty()) {
                    FlexibleTextView flexibleTextView = j35.D;
                    Wg6.b(flexibleTextView, "tvEmpty");
                    Hr7 hr7 = Hr7.a;
                    FlexibleTextView flexibleTextView2 = j35.D;
                    Wg6.b(flexibleTextView2, "tvEmpty");
                    String c = Um5.c(flexibleTextView2.getContext(), 2131886286);
                    Wg6.b(c, "LanguageHelper.getString\u2026or__NothingFoundForInput)");
                    String format = String.format(c, Arrays.copyOf(new Object[]{second}, 1));
                    Wg6.b(format, "java.lang.String.format(format, *args)");
                    flexibleTextView.setText(format);
                    FlexibleTextView flexibleTextView3 = j35.D;
                    Wg6.b(flexibleTextView3, "tvEmpty");
                    flexibleTextView3.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView4 = j35.D;
                Wg6.b(flexibleTextView4, "tvEmpty");
                flexibleTextView4.setVisibility(8);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Lc6<? extends List<? extends Ot4>, ? extends String> lc6) {
            a(lc6);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ri<T> implements Ls0<Gl7<? extends Ps4, ? extends Long, ? extends String>> {
        @DexIgnore
        public static /* final */ Ri a; // = new Ri();

        @DexIgnore
        public final void a(Gl7<Ps4, Long, String> gl7) {
            Xr4.a.d(gl7.getFirst(), gl7.getSecond().longValue(), gl7.getThird(), PortfolioApp.get.instance());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Gl7<? extends Ps4, ? extends Long, ? extends String> gl7) {
            a(gl7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Si implements Ly5 {
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Si(Vu4 vu4) {
            this.a = vu4;
        }

        @DexIgnore
        @Override // com.fossil.Ly5
        public void a(Date date) {
            Wg6.c(date, "date");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Vu4.u.a();
            local.e(a2, "showDateTimePicker - updateValue - date: " + date);
            Vu4.R6(this.a).J(date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ti implements My5 {
        @DexIgnore
        public /* final */ /* synthetic */ Vu4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ti(Vu4 vu4) {
            this.a = vu4;
        }

        @DexIgnore
        @Override // com.fossil.My5
        public void a(Gs4 gs4) {
            Wg6.c(gs4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = Vu4.u.a();
            local.e(a2, "showSoonTime - updateValue - model: " + gs4);
            Object a3 = gs4.a();
            if (!(a3 instanceof Date)) {
                a3 = null;
            }
            Date date = (Date) a3;
            if (date != null) {
                Vu4.R6(this.a).J(date);
            }
        }
    }

    /*
    static {
        String simpleName = Vu4.class.getSimpleName();
        Wg6.b(simpleName, "BCInviteFriendFragment::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 M6(Vu4 vu4) {
        G37<J35> g37 = vu4.g;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Zu4 O6(Vu4 vu4) {
        Zu4 zu4 = vu4.l;
        if (zu4 != null) {
            return zu4;
        }
        Wg6.n("searchingFriendsAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Zu4 P6(Vu4 vu4) {
        Zu4 zu4 = vu4.k;
        if (zu4 != null) {
            return zu4;
        }
        Wg6.n("suggestedFriendsAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ BCInviteFriendViewModel R6(Vu4 vu4) {
        BCInviteFriendViewModel bCInviteFriendViewModel = vu4.h;
        if (bCInviteFriendViewModel != null) {
            return bCInviteFriendViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void B0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.Zu4.Ai
    public void G5(Ot4 ot4) {
        Wg6.c(ot4, "friend");
        BCInviteFriendViewModel bCInviteFriendViewModel = this.h;
        if (bCInviteFriendViewModel != null) {
            bCInviteFriendViewModel.E(ot4);
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void V6() {
        G37<J35> g37 = this.g;
        if (g37 != null) {
            J35 a2 = g37.a();
            if (a2 != null) {
                Fk0 fk0 = new Fk0();
                fk0.c(a2.A);
                RecyclerView recyclerView = a2.B;
                Wg6.b(recyclerView, "rvFriends");
                int id = recyclerView.getId();
                FlexibleButton flexibleButton = a2.q;
                Wg6.b(flexibleButton, "btnDone");
                fk0.e(id, 4, flexibleButton.getId(), 3);
                fk0.a(a2.A);
                return;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final void W6() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public final void X6(boolean z) {
        G37<J35> g37 = this.g;
        if (g37 != null) {
            J35 a2 = g37.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.E;
                Wg6.b(flexibleTextView, "tvTitle");
                Ts4 ts4 = this.i;
                flexibleTextView.setText(ts4 != null ? ts4.e() : null);
                if (z) {
                    FlexibleButton flexibleButton = a2.t;
                    Wg6.b(flexibleButton, "btnStartNow");
                    flexibleButton.setVisibility(8);
                    FlexibleButton flexibleButton2 = a2.s;
                    Wg6.b(flexibleButton2, "btnStartLater");
                    flexibleButton2.setVisibility(8);
                    FlexibleButton flexibleButton3 = a2.q;
                    Wg6.b(flexibleButton3, "btnDone");
                    flexibleButton3.setVisibility(0);
                    V6();
                    FlexibleTextView flexibleTextView2 = a2.y;
                    Wg6.b(flexibleTextView2, "ftvInviteAll");
                    flexibleTextView2.setVisibility(8);
                    FlexibleCheckBox flexibleCheckBox = a2.v;
                    Wg6.b(flexibleCheckBox, "cbInviteAll");
                    flexibleCheckBox.setVisibility(8);
                    a2.q.setOnClickListener(new Bi(this, z));
                } else {
                    a2.t.setOnClickListener(new Ci(this, z));
                    a2.s.setOnClickListener(new Di(this, z));
                }
                a2.z.setOnClickListener(new Ei(this, z));
                this.k = new Zu4(this);
                this.l = new Zu4(this);
                RecyclerView recyclerView = a2.B;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                Zu4 zu4 = this.k;
                if (zu4 != null) {
                    recyclerView.setAdapter(zu4);
                    recyclerView.setHasFixedSize(true);
                    a2.v.setOnCheckedChangeListener(new Fi(this, z));
                    a2.C.setOnRefreshListener(new Gi(a2, this, z));
                    a2.r.setOnClickListener(new Ii(a2));
                    a2.w.setOnFocusChangeListener(Ji.b);
                    a2.w.addTextChangedListener(new Hi(a2, this, z));
                    return;
                }
                Wg6.n("suggestedFriendsAdapter");
                throw null;
            }
            return;
        }
        Wg6.n("binding");
        throw null;
    }

    @DexIgnore
    public final void Y6() {
        BCInviteFriendViewModel bCInviteFriendViewModel = this.h;
        if (bCInviteFriendViewModel != null) {
            bCInviteFriendViewModel.t().h(getViewLifecycleOwner(), new Ki(this));
            BCInviteFriendViewModel bCInviteFriendViewModel2 = this.h;
            if (bCInviteFriendViewModel2 != null) {
                bCInviteFriendViewModel2.y().h(getViewLifecycleOwner(), new Li(this));
                BCInviteFriendViewModel bCInviteFriendViewModel3 = this.h;
                if (bCInviteFriendViewModel3 != null) {
                    bCInviteFriendViewModel3.r().h(getViewLifecycleOwner(), new Mi(this));
                    BCInviteFriendViewModel bCInviteFriendViewModel4 = this.h;
                    if (bCInviteFriendViewModel4 != null) {
                        bCInviteFriendViewModel4.x().h(getViewLifecycleOwner(), new Ni(this));
                        BCInviteFriendViewModel bCInviteFriendViewModel5 = this.h;
                        if (bCInviteFriendViewModel5 != null) {
                            bCInviteFriendViewModel5.s().h(getViewLifecycleOwner(), new Oi(this));
                            BCInviteFriendViewModel bCInviteFriendViewModel6 = this.h;
                            if (bCInviteFriendViewModel6 != null) {
                                bCInviteFriendViewModel6.w().h(getViewLifecycleOwner(), new Pi(this));
                                BCInviteFriendViewModel bCInviteFriendViewModel7 = this.h;
                                if (bCInviteFriendViewModel7 != null) {
                                    bCInviteFriendViewModel7.u().h(getViewLifecycleOwner(), new Qi(this));
                                    BCInviteFriendViewModel bCInviteFriendViewModel8 = this.h;
                                    if (bCInviteFriendViewModel8 != null) {
                                        bCInviteFriendViewModel8.q().h(getViewLifecycleOwner(), Ri.a);
                                    } else {
                                        Wg6.n("viewModel");
                                        throw null;
                                    }
                                } else {
                                    Wg6.n("viewModel");
                                    throw null;
                                }
                            } else {
                                Wg6.n("viewModel");
                                throw null;
                            }
                        } else {
                            Wg6.n("viewModel");
                            throw null;
                        }
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void Z6(List<? extends Date> list) {
        Ky5 ky5 = new Ky5();
        String c = Um5.c(requireContext(), 2131886341);
        Wg6.b(c, "LanguageHelper.getString\u2026e_Invite_CTA__StartLater)");
        ky5.setTitle(c);
        ky5.O6(list);
        ky5.Q6(new Si(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        ky5.show(childFragmentManager, Ky5.D.a());
    }

    @DexIgnore
    public final void a7(List<Gs4> list) {
        Es4 es4 = new Es4();
        String c = Um5.c(requireContext(), 2131886347);
        Wg6.b(c, "LanguageHelper.getString\u2026artNow_Title__QuickStart)");
        es4.setTitle(c);
        es4.E6(list);
        es4.F6(true);
        es4.G6(new Ti(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        es4.show(childFragmentManager, Es4.A.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().j0().a(this);
        Po4 po4 = this.j;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCInviteFriendViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026endViewModel::class.java)");
            this.h = (BCInviteFriendViewModel) a2;
            Bundle arguments = getArguments();
            this.i = arguments != null ? (Ts4) arguments.getParcelable("challenge_draft_extra") : null;
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        J35 j35 = (J35) Aq0.f(layoutInflater, 2131558514, viewGroup, false, A6());
        this.g = new G37<>(this, j35);
        Wg6.b(j35, "bd");
        return j35.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        String str = null;
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        BCInviteFriendViewModel bCInviteFriendViewModel = this.h;
        if (bCInviteFriendViewModel != null) {
            bCInviteFriendViewModel.z(this.i);
            Ts4 ts4 = this.i;
            if (ts4 != null) {
                str = ts4.c();
            }
            X6(str != null);
            Y6();
            return;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
