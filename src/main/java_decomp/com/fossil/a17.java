package com.fossil;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Sm5;
import com.mapped.AlertDialogFragment;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A17 extends Os5 implements L17, AlertDialogFragment.Gi, Sm5.Bi, AlertDialogFragment.Hi {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ Ai u; // = new Ai(null);
    @DexIgnore
    public K17 j;
    @DexIgnore
    public G37<T25> k;
    @DexIgnore
    public Wj5 l;
    @DexIgnore
    public List<DeviceHelper.Bi> m; // = new ArrayList();
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final A17 a() {
            return new A17();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ A17 b;

        @DexIgnore
        public Bi(A17 a17) {
            this.b = a17;
        }

        @DexIgnore
        public final void onClick(View view) {
            A17.c7(this.b).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ A17 b;

        @DexIgnore
        public Ci(A17 a17) {
            this.b = a17;
        }

        @DexIgnore
        public final void onClick(View view) {
            A17.c7(this.b).p();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ T25 a;
        @DexIgnore
        public /* final */ /* synthetic */ A17 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements Ej1<Drawable> {
            @DexIgnore
            public boolean a(Drawable drawable, Object obj, Qj1<Drawable> qj1, Gb1 gb1, boolean z) {
                Wg6.c(drawable, "resource");
                Wg6.c(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                Wg6.c(qj1, "target");
                Wg6.c(gb1, "dataSource");
                FLogger.INSTANCE.getLocal().d(A17.t, "showHour onResourceReady");
                return false;
            }

            @DexIgnore
            @Override // com.fossil.Ej1
            public boolean e(Dd1 dd1, Object obj, Qj1<Drawable> qj1, boolean z) {
                Wg6.c(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                Wg6.c(qj1, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = A17.t;
                local.d(str, "showHour onLoadFail e=" + dd1);
                return true;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object, com.fossil.Qj1, com.fossil.Gb1, boolean] */
            @Override // com.fossil.Ej1
            public /* bridge */ /* synthetic */ boolean g(Drawable drawable, Object obj, Qj1<Drawable> qj1, Gb1 gb1, boolean z) {
                return a(drawable, obj, qj1, gb1, z);
            }
        }

        @DexIgnore
        public Di(T25 t25, A17 a17, String str) {
            this.a = t25;
            this.b = a17;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            Wj5 wj5;
            Vj5<Drawable> J;
            Vj5<Drawable> b1;
            Wg6.c(str, "serial");
            Wg6.c(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = A17.t;
            local.d(str3, "showHour onImageCallback serial=" + str + " filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.l);
            if (this.b.isActive() && (wj5 = this.b.l) != null && (J = wj5.J(str2)) != null && (b1 = J.b1(new Aii())) != null) {
                b1.F0(this.a.r);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ T25 a;
        @DexIgnore
        public /* final */ /* synthetic */ A17 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements Ej1<Drawable> {
            @DexIgnore
            public boolean a(Drawable drawable, Object obj, Qj1<Drawable> qj1, Gb1 gb1, boolean z) {
                Wg6.c(drawable, "resource");
                Wg6.c(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                Wg6.c(qj1, "target");
                Wg6.c(gb1, "dataSource");
                FLogger.INSTANCE.getLocal().d(A17.t, "showMinute onResourceReady");
                return false;
            }

            @DexIgnore
            @Override // com.fossil.Ej1
            public boolean e(Dd1 dd1, Object obj, Qj1<Drawable> qj1, boolean z) {
                Wg6.c(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                Wg6.c(qj1, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = A17.t;
                local.d(str, "showMinute onLoadFail e=" + dd1);
                return true;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object, com.fossil.Qj1, com.fossil.Gb1, boolean] */
            @Override // com.fossil.Ej1
            public /* bridge */ /* synthetic */ boolean g(Drawable drawable, Object obj, Qj1<Drawable> qj1, Gb1 gb1, boolean z) {
                return a(drawable, obj, qj1, gb1, z);
            }
        }

        @DexIgnore
        public Ei(T25 t25, A17 a17, String str) {
            this.a = t25;
            this.b = a17;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            Wj5 wj5;
            Vj5<Drawable> J;
            Vj5<Drawable> b1;
            Wg6.c(str, "serial");
            Wg6.c(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = A17.t;
            local.d(str3, "showMinute onImageCallback serial=" + str + " filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.l);
            if (this.b.isActive() && (wj5 = this.b.l) != null && (J = wj5.J(str2)) != null && (b1 = J.b1(new Aii())) != null) {
                b1.F0(this.a.r);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ T25 a;
        @DexIgnore
        public /* final */ /* synthetic */ A17 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements Ej1<Drawable> {
            @DexIgnore
            public boolean a(Drawable drawable, Object obj, Qj1<Drawable> qj1, Gb1 gb1, boolean z) {
                Wg6.c(drawable, "resource");
                Wg6.c(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                Wg6.c(qj1, "target");
                Wg6.c(gb1, "dataSource");
                FLogger.INSTANCE.getLocal().d(A17.t, "showSubeye onResourceReady");
                return false;
            }

            @DexIgnore
            @Override // com.fossil.Ej1
            public boolean e(Dd1 dd1, Object obj, Qj1<Drawable> qj1, boolean z) {
                Wg6.c(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                Wg6.c(qj1, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = A17.t;
                local.d(str, "showSubeye onLoadFail e=" + dd1);
                return true;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object, com.fossil.Qj1, com.fossil.Gb1, boolean] */
            @Override // com.fossil.Ej1
            public /* bridge */ /* synthetic */ boolean g(Drawable drawable, Object obj, Qj1<Drawable> qj1, Gb1 gb1, boolean z) {
                return a(drawable, obj, qj1, gb1, z);
            }
        }

        @DexIgnore
        public Fi(T25 t25, A17 a17, String str) {
            this.a = t25;
            this.b = a17;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            Wj5 wj5;
            Vj5<Drawable> J;
            Vj5<Drawable> b1;
            Wg6.c(str, "serial");
            Wg6.c(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = A17.t;
            local.d(str3, "showSubeye onImageCallback serial=" + str + " filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.l);
            if (this.b.isActive() && (wj5 = this.b.l) != null && (J = wj5.J(str2)) != null && (b1 = J.b1(new Aii())) != null) {
                b1.F0(this.a.r);
            }
        }
    }

    /*
    static {
        String simpleName = A17.class.getSimpleName();
        Wg6.b(simpleName, "CalibrationFragment::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ K17 c7(A17 a17) {
        K17 k17 = a17.j;
        if (k17 != null) {
            return k17;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Sm5.Bi
    public void A0(View view) {
        Wg6.c(view, "view");
        FLogger.INSTANCE.getLocal().d(t, "GestureDetectorCallback onLongPressEvent");
        K17 k17 = this.j;
        if (k17 != null) {
            k17.s(view.getId() == 2131361855);
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.L17
    public void F1(String str) {
        Wg6.c(str, "deviceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = t;
        local.d(str2, "showHour: deviceId = " + str);
        if (isActive()) {
            G37<T25> g37 = this.k;
            if (g37 != null) {
                T25 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.z;
                    Wg6.b(flexibleTextView, "binding.ftvTitle");
                    flexibleTextView.setText(Um5.c(PortfolioApp.get.instance(), 2131887143));
                    FlexibleTextView flexibleTextView2 = a2.x;
                    Wg6.b(flexibleTextView2, "binding.ftvDescription");
                    flexibleTextView2.setText(Um5.c(PortfolioApp.get.instance(), 2131887142));
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(DeviceHelper.o.m(str)).setType(Constants.CalibrationType.TYPE_HOUR);
                    AppCompatImageView appCompatImageView = a2.r;
                    Wg6.b(appCompatImageView, "binding.acivDevice");
                    type.setPlaceHolder(appCompatImageView, DeviceHelper.o.i(str, this.m.get(0))).setImageCallback(new Di(a2, this, str)).downloadForCalibration();
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        K17 k17 = this.j;
        if (k17 != null) {
            k17.n();
            return true;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.L17
    public void I4(String str) {
        Wg6.c(str, "deviceId");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = t;
            local.d(str2, "showMinute: deviceId = " + str);
            G37<T25> g37 = this.k;
            if (g37 != null) {
                T25 a2 = g37.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.z;
                    Wg6.b(flexibleTextView, "binding.ftvTitle");
                    flexibleTextView.setText(Um5.c(PortfolioApp.get.instance(), 2131887146));
                    FlexibleTextView flexibleTextView2 = a2.x;
                    Wg6.b(flexibleTextView2, "binding.ftvDescription");
                    flexibleTextView2.setText(Um5.c(PortfolioApp.get.instance(), 2131887145));
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(DeviceHelper.o.m(str)).setType(Constants.CalibrationType.TYPE_MINUTE);
                    AppCompatImageView appCompatImageView = a2.r;
                    Wg6.b(appCompatImageView, "binding.acivDevice");
                    type.setPlaceHolder(appCompatImageView, DeviceHelper.o.i(str, this.m.get(1))).setImageCallback(new Ei(a2, this, str)).downloadForCalibration();
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.L17
    public void L5(int i, int i2) {
        if (isActive()) {
            G37<T25> g37 = this.k;
            if (g37 != null) {
                T25 a2 = g37.a();
                if (a2 != null) {
                    a2.A.setLength(i2);
                    int i3 = i + 1;
                    a2.A.setProgress((int) ((((float) i3) / ((float) i2)) * ((float) 100)));
                    String c = i3 < i2 ? Um5.c(PortfolioApp.get.instance(), 2131887141) : Um5.c(PortfolioApp.get.instance(), 2131887144);
                    FlexibleButton flexibleButton = a2.y;
                    Wg6.b(flexibleButton, "it.ftvNext");
                    flexibleButton.setText(c);
                    return;
                }
                return;
            }
            Wg6.n("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(Object obj) {
        e7((K17) obj);
    }

    @DexIgnore
    @Override // com.fossil.Sm5.Bi
    public void N5(View view, Boolean bool) {
    }

    @DexIgnore
    @Override // com.fossil.Os5, com.mapped.AlertDialogFragment.Gi
    public void R5(String str, int i, Intent intent) {
        Wg6.c(str, "tag");
        super.R5(str, i, intent);
        int hashCode = str.hashCode();
        if (hashCode == -1391436191 ? !str.equals("SYNC_FAILED") : hashCode != 1178575340 || !str.equals("SERVER_ERROR")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).R5(str, i, intent);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
        e0();
    }

    @DexIgnore
    @Override // com.fossil.Os5
    public void S6() {
        e0();
    }

    @DexIgnore
    @Override // com.fossil.Os5
    public void T6() {
        e0();
    }

    @DexIgnore
    @Override // com.fossil.Os5
    public void U6() {
        e0();
    }

    @DexIgnore
    @Override // com.fossil.Sm5.Bi
    public void V0(View view) {
        Wg6.c(view, "view");
        FLogger.INSTANCE.getLocal().d(t, "GestureDetectorCallback onLongPressEnded");
        K17 k17 = this.j;
        if (k17 != null) {
            k17.t();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Os5
    public void V6() {
    }

    @DexIgnore
    @Override // com.fossil.Os5
    public void W6() {
    }

    @DexIgnore
    @Override // com.fossil.L17
    public void e0() {
        FLogger.INSTANCE.getLocal().d(t, VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void e7(K17 k17) {
        Wg6.c(k17, "presenter");
        this.j = k17;
    }

    @DexIgnore
    @Override // com.fossil.Sm5.Bi
    public void h5(View view) {
        Wg6.c(view, "view");
        FLogger.INSTANCE.getLocal().d(t, "GestureDetectorCallback onRelease");
        view.setPressed(false);
    }

    @DexIgnore
    @Override // com.mapped.AlertDialogFragment.Hi
    public void i1(String str) {
        if (!TextUtils.isEmpty(str) && str != null && str.hashCode() == 1925385819 && str.equals("DEVICE_CONNECT_FAILED")) {
            K17 k17 = this.j;
            if (k17 != null) {
                k17.q(false);
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.L17
    public void i4() {
        FLogger.INSTANCE.getLocal().d(t, "showGeneralError");
        K17 k17 = this.j;
        if (k17 != null) {
            k17.q(true);
            if (isActive()) {
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                s37.V(childFragmentManager);
                return;
            }
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Sm5.Bi
    public void onClick(View view) {
        Wg6.c(view, "view");
        FLogger.INSTANCE.getLocal().d(t, "GestureDetectorCallback onClick");
        K17 k17 = this.j;
        if (k17 != null) {
            k17.r(view.getId() == 2131361855);
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        T25 t25 = (T25) Aq0.f(layoutInflater, 2131558506, viewGroup, false, A6());
        t25.q.setOnClickListener(new Bi(this));
        t25.y.setOnClickListener(new Ci(this));
        new Sm5().a(t25.s, this);
        new Sm5().a(t25.t, this);
        this.k = new G37<>(this, t25);
        this.l = Tj5.c(this);
        K17 k17 = this.j;
        if (k17 != null) {
            this.m = k17.o();
            Wg6.b(t25, "binding");
            return t25.n();
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Os5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        K17 k17 = this.j;
        if (k17 != null) {
            k17.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        K17 k17 = this.j;
        if (k17 != null) {
            k17.m();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.L17
    public void r1() {
        FLogger.INSTANCE.getLocal().d(t, "showBluetoothError");
        if (isActive()) {
            K17 k17 = this.j;
            if (k17 != null) {
                k17.q(true);
                S37 s37 = S37.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                Wg6.b(childFragmentManager, "childFragmentManager");
                s37.k(childFragmentManager);
                return;
            }
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Sm5.Bi
    public void v4(View view) {
        Wg6.c(view, "view");
        FLogger.INSTANCE.getLocal().d(t, "GestureDetectorCallback onPress");
        view.setPressed(true);
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Os5
    public void v6() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.L17
    public void x1(String str) {
        Wg6.c(str, "deviceId");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = t;
            local.d(str2, "showSubeye: deviceId = " + str);
            if (this.m.size() > 2) {
                G37<T25> g37 = this.k;
                if (g37 != null) {
                    T25 a2 = g37.a();
                    if (a2 != null) {
                        FlexibleTextView flexibleTextView = a2.z;
                        Wg6.b(flexibleTextView, "binding.ftvTitle");
                        flexibleTextView.setText(Um5.c(PortfolioApp.get.instance(), 2131887148));
                        FlexibleTextView flexibleTextView2 = a2.x;
                        Wg6.b(flexibleTextView2, "binding.ftvDescription");
                        flexibleTextView2.setText(Um5.c(PortfolioApp.get.instance(), 2131887147));
                        CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(DeviceHelper.o.m(str)).setType(Constants.CalibrationType.TYPE_SUB_EYE);
                        AppCompatImageView appCompatImageView = a2.r;
                        Wg6.b(appCompatImageView, "binding.acivDevice");
                        type.setPlaceHolder(appCompatImageView, DeviceHelper.o.i(str, this.m.get(2))).setImageCallback(new Fi(a2, this, str)).downloadForCalibration();
                        return;
                    }
                    return;
                }
                Wg6.n("mBinding");
                throw null;
            }
        }
    }
}
