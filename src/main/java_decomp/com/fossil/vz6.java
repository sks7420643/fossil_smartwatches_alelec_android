package com.fossil;

import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Vz6 implements Factory<SignUpPresenter> {
    @DexIgnore
    public static SignUpPresenter a(Pz6 pz6, BaseActivity baseActivity) {
        return new SignUpPresenter(pz6, baseActivity);
    }
}
