package com.fossil;

import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wo0 extends ClickableSpan {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Yo0 c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public Wo0(int i, Yo0 yo0, int i2) {
        this.b = i;
        this.c = yo0;
        this.d = i2;
    }

    @DexIgnore
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", this.b);
        this.c.R(this.d, bundle);
    }
}
