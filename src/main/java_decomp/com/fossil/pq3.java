package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Pq3 {
    @DexIgnore
    public Oq3 a;
    @DexIgnore
    public /* final */ /* synthetic */ Jq3 b;

    @DexIgnore
    public Pq3(Jq3 jq3) {
        this.b = jq3;
    }

    @DexIgnore
    public final void a() {
        this.b.h();
        if (this.b.m().s(Xg3.p0) && this.a != null) {
            this.b.c.removeCallbacks(this.a);
        }
        if (this.b.m().s(Xg3.D0)) {
            this.b.l().w.a(false);
        }
    }

    @DexIgnore
    public final void b(long j) {
        if (this.b.m().s(Xg3.p0)) {
            this.a = new Oq3(this, this.b.zzm().b(), j);
            this.b.c.postDelayed(this.a, 2000);
        }
    }
}
