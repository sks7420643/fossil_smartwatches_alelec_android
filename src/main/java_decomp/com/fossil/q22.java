package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Q22 {
    @DexIgnore
    public static Q22 a(long j, H02 h02, C02 c02) {
        return new J22(j, h02, c02);
    }

    @DexIgnore
    public abstract C02 b();

    @DexIgnore
    public abstract long c();

    @DexIgnore
    public abstract H02 d();
}
