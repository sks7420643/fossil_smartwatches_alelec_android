package com.fossil;

import com.portfolio.platform.ui.BaseActivity;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sz6 implements Factory<BaseActivity> {
    @DexIgnore
    public static BaseActivity a(Rz6 rz6) {
        BaseActivity a2 = rz6.a();
        Lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
