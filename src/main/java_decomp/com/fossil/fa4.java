package com.fossil;

import com.fossil.Ta4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fa4 extends Ta4.Di.Aii.Biii {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    @Override // com.fossil.Ta4.Di.Aii.Biii
    public String a() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof Ta4.Di.Aii.Biii) {
            return this.a.equals(((Ta4.Di.Aii.Biii) obj).a());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "Organization{clsId=" + this.a + "}";
    }
}
