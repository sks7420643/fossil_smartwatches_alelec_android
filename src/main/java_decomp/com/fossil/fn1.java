package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.R60;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fn1 extends R60 {
    @DexIgnore
    public static /* final */ Ai CREATOR; // = new Ai(null);
    @DexIgnore
    public /* final */ short c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Parcelable.Creator<Fn1> {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Fn1 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 2) {
                return new Fn1(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0));
            }
            throw new IllegalArgumentException(E.c(E.e("Invalid data size: "), bArr.length, ", ", "require: 2"));
        }

        @DexIgnore
        public Fn1 b(Parcel parcel) {
            return new Fn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public Fn1 createFromParcel(Parcel parcel) {
            return new Fn1(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public Fn1[] newArray(int i) {
            return new Fn1[i];
        }
    }

    @DexIgnore
    public /* synthetic */ Fn1(Parcel parcel, Qg6 qg6) {
        super(parcel);
        this.c = (short) ((short) parcel.readInt());
        d();
    }

    @DexIgnore
    public Fn1(short s) {
        super(Zm1.SECOND_TIMEZONE_OFFSET);
        this.c = (short) s;
        d();
    }

    @DexIgnore
    @Override // com.mapped.R60
    public byte[] b() {
        byte[] array = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(this.c).array();
        Wg6.b(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        try {
            G80.k(jSONObject, Jd0.w2, Short.valueOf(this.c));
        } catch (JSONException e) {
            D90.i.i(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public final void d() throws IllegalArgumentException {
        short s = this.c;
        if (!(s == 1024 || (-720 <= s && 840 >= s))) {
            StringBuilder e = E.e("secondTimezoneOffsetInMinute (");
            e.append((int) this.c);
            e.append(") ");
            e.append(" must be equal to 1024 ");
            e.append(" or in range ");
            e.append("[-720, ");
            e.append("840].");
            throw new IllegalArgumentException(e.toString());
        }
    }

    @DexIgnore
    @Override // com.mapped.R60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!Wg6.a(Fn1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((Fn1) obj).c;
        }
        throw new Rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.SecondTimezoneOffsetConfig");
    }

    @DexIgnore
    public final short getSecondTimezoneOffsetInMinute() {
        return this.c;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public int hashCode() {
        return (super.hashCode() * 31) + this.c;
    }

    @DexIgnore
    @Override // com.mapped.R60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(Hy1.n(this.c));
        }
    }
}
