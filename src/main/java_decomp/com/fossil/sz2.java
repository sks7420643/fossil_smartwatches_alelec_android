package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Sz2 {
    @DexIgnore
    public int a;
    @DexIgnore
    public long b;
    @DexIgnore
    public Object c;
    @DexIgnore
    public /* final */ Q03 d;

    @DexIgnore
    public Sz2(Q03 q03) {
        if (q03 != null) {
            this.d = q03;
            return;
        }
        throw null;
    }
}
