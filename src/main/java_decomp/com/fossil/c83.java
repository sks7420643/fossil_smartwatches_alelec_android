package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class C83 implements Z73 {
    @DexIgnore
    public static /* final */ Xv2<Long> a;

    /*
    static {
        Hw2 hw2 = new Hw2(Yv2.a("com.google.android.gms.measurement"));
        hw2.b("measurement.id.max_bundles_per_iteration", 0);
        a = hw2.b("measurement.max_bundles_per_iteration", 2);
    }
    */

    @DexIgnore
    @Override // com.fossil.Z73
    public final long zza() {
        return a.o().longValue();
    }
}
