package com.fossil;

import android.os.Build;
import android.os.LocaleList;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rm0 {
    @DexIgnore
    public Tm0 a;

    /*
    static {
        a(new Locale[0]);
    }
    */

    @DexIgnore
    public Rm0(Tm0 tm0) {
        this.a = tm0;
    }

    @DexIgnore
    public static Rm0 a(Locale... localeArr) {
        return Build.VERSION.SDK_INT >= 24 ? d(new LocaleList(localeArr)) : new Rm0(new Sm0(localeArr));
    }

    @DexIgnore
    public static Locale b(String str) {
        if (str.contains(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR)) {
            String[] split = str.split(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR, -1);
            if (split.length > 2) {
                return new Locale(split[0], split[1], split[2]);
            }
            if (split.length > 1) {
                return new Locale(split[0], split[1]);
            }
            if (split.length == 1) {
                return new Locale(split[0]);
            }
        } else if (!str.contains(LocaleConverter.LOCALE_DELIMITER)) {
            return new Locale(str);
        } else {
            String[] split2 = str.split(LocaleConverter.LOCALE_DELIMITER, -1);
            if (split2.length > 2) {
                return new Locale(split2[0], split2[1], split2[2]);
            }
            if (split2.length > 1) {
                return new Locale(split2[0], split2[1]);
            }
            if (split2.length == 1) {
                return new Locale(split2[0]);
            }
        }
        throw new IllegalArgumentException("Can not parse language tag: [" + str + "]");
    }

    @DexIgnore
    public static Rm0 d(LocaleList localeList) {
        return new Rm0(new Um0(localeList));
    }

    @DexIgnore
    public Locale c(int i) {
        return this.a.get(i);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof Rm0) && this.a.equals(((Rm0) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }
}
