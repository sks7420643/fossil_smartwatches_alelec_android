package com.fossil;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface A58 extends Closeable, Flushable {
    @DexIgnore
    void K(I48 i48, long j) throws IOException;

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    void close() throws IOException;

    @DexIgnore
    D58 e();

    @DexIgnore
    @Override // java.io.Flushable
    void flush() throws IOException;
}
