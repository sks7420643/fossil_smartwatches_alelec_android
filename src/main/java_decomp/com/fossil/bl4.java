package com.fossil;

import com.misfit.frameworks.common.enums.Action;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Bl4 extends Hl4 {
    @DexIgnore
    public static volatile Bl4[] x;
    @DexIgnore
    public Dl4 a;
    @DexIgnore
    public Dl4 b;
    @DexIgnore
    public Dl4 c;
    @DexIgnore
    public Dl4 d;
    @DexIgnore
    public Dl4 e;
    @DexIgnore
    public Dl4 f;
    @DexIgnore
    public Dl4 g;
    @DexIgnore
    public Dl4 h;
    @DexIgnore
    public Dl4 i;
    @DexIgnore
    public Dl4 j;
    @DexIgnore
    public Dl4 k;
    @DexIgnore
    public Dl4 l;
    @DexIgnore
    public Dl4 m;
    @DexIgnore
    public Dl4 n;
    @DexIgnore
    public Dl4 o;
    @DexIgnore
    public Dl4 p;
    @DexIgnore
    public int q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public Al4[] v;
    @DexIgnore
    public Al4[] w;

    @DexIgnore
    public Bl4() {
        c();
    }

    @DexIgnore
    public static Bl4[] d() {
        if (x == null) {
            synchronized (Fl4.a) {
                if (x == null) {
                    x = new Bl4[0];
                }
            }
        }
        return x;
    }

    @DexIgnore
    @Override // com.fossil.Hl4
    public /* bridge */ /* synthetic */ Hl4 b(El4 el4) throws IOException {
        e(el4);
        return this;
    }

    @DexIgnore
    public Bl4 c() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = 0;
        this.r = "";
        this.s = "";
        this.t = "";
        this.u = "";
        this.v = Al4.d();
        this.w = Al4.d();
        return this;
    }

    @DexIgnore
    public Bl4 e(El4 el4) throws IOException {
        while (true) {
            int q2 = el4.q();
            switch (q2) {
                case 0:
                    break;
                case 10:
                    if (this.a == null) {
                        this.a = new Dl4();
                    }
                    el4.j(this.a);
                    break;
                case 18:
                    if (this.b == null) {
                        this.b = new Dl4();
                    }
                    el4.j(this.b);
                    break;
                case 26:
                    if (this.c == null) {
                        this.c = new Dl4();
                    }
                    el4.j(this.c);
                    break;
                case 34:
                    if (this.d == null) {
                        this.d = new Dl4();
                    }
                    el4.j(this.d);
                    break;
                case 42:
                    if (this.e == null) {
                        this.e = new Dl4();
                    }
                    el4.j(this.e);
                    break;
                case 50:
                    if (this.f == null) {
                        this.f = new Dl4();
                    }
                    el4.j(this.f);
                    break;
                case 58:
                    if (this.g == null) {
                        this.g = new Dl4();
                    }
                    el4.j(this.g);
                    break;
                case 66:
                    if (this.h == null) {
                        this.h = new Dl4();
                    }
                    el4.j(this.h);
                    break;
                case 74:
                    el4.p();
                    break;
                case 80:
                    this.q = el4.i();
                    break;
                case 90:
                    this.r = el4.p();
                    break;
                case 98:
                    el4.p();
                    break;
                case 106:
                    this.s = el4.p();
                    break;
                case 122:
                    this.t = el4.p();
                    break;
                case 130:
                    this.u = el4.p();
                    break;
                case 138:
                    el4.p();
                    break;
                case 144:
                    el4.h();
                    break;
                case 154:
                    int a2 = Jl4.a(el4, 154);
                    Al4[] al4Arr = this.v;
                    int length = al4Arr == null ? 0 : al4Arr.length;
                    int i2 = a2 + length;
                    Al4[] al4Arr2 = new Al4[i2];
                    if (length != 0) {
                        System.arraycopy(this.v, 0, al4Arr2, 0, length);
                    }
                    while (length < i2 - 1) {
                        al4Arr2[length] = new Al4();
                        el4.j(al4Arr2[length]);
                        el4.q();
                        length++;
                    }
                    al4Arr2[length] = new Al4();
                    el4.j(al4Arr2[length]);
                    this.v = al4Arr2;
                    break;
                case 162:
                    int a3 = Jl4.a(el4, 162);
                    Al4[] al4Arr3 = this.w;
                    int length2 = al4Arr3 == null ? 0 : al4Arr3.length;
                    int i3 = a3 + length2;
                    Al4[] al4Arr4 = new Al4[i3];
                    if (length2 != 0) {
                        System.arraycopy(this.w, 0, al4Arr4, 0, length2);
                    }
                    while (length2 < i3 - 1) {
                        al4Arr4[length2] = new Al4();
                        el4.j(al4Arr4[length2]);
                        el4.q();
                        length2++;
                    }
                    al4Arr4[length2] = new Al4();
                    el4.j(al4Arr4[length2]);
                    this.w = al4Arr4;
                    break;
                case 170:
                    if (this.i == null) {
                        this.i = new Dl4();
                    }
                    el4.j(this.i);
                    break;
                case 176:
                    el4.h();
                    break;
                case 186:
                    el4.p();
                    break;
                case 194:
                    if (this.p == null) {
                        this.p = new Dl4();
                    }
                    el4.j(this.p);
                    break;
                case Action.Selfie.TAKE_BURST /* 202 */:
                    if (this.j == null) {
                        this.j = new Dl4();
                    }
                    el4.j(this.j);
                    break;
                case 208:
                    el4.h();
                    break;
                case 218:
                    if (this.k == null) {
                        this.k = new Dl4();
                    }
                    el4.j(this.k);
                    break;
                case 226:
                    if (this.l == null) {
                        this.l = new Dl4();
                    }
                    el4.j(this.l);
                    break;
                case 234:
                    if (this.m == null) {
                        this.m = new Dl4();
                    }
                    el4.j(this.m);
                    break;
                case 242:
                    if (this.n == null) {
                        this.n = new Dl4();
                    }
                    el4.j(this.n);
                    break;
                case 250:
                    if (this.o == null) {
                        this.o = new Dl4();
                    }
                    el4.j(this.o);
                    break;
                case 256:
                    el4.h();
                    break;
                default:
                    if (Jl4.e(el4, q2)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }
}
