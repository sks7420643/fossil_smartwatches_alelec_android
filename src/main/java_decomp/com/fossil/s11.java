package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Build;
import androidx.work.WorkerParameters;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.utils.ForceStopRunnable;
import com.fossil.O01;
import com.fossil.X01;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class S11 extends G11 {
    @DexIgnore
    public static S11 j;
    @DexIgnore
    public static S11 k;
    @DexIgnore
    public static /* final */ Object l; // = new Object();
    @DexIgnore
    public Context a;
    @DexIgnore
    public O01 b;
    @DexIgnore
    public WorkDatabase c;
    @DexIgnore
    public K41 d;
    @DexIgnore
    public List<N11> e;
    @DexIgnore
    public M11 f;
    @DexIgnore
    public Z31 g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public BroadcastReceiver.PendingResult i;

    @DexIgnore
    public S11(Context context, O01 o01, K41 k41) {
        this(context, o01, k41, context.getResources().getBoolean(C11.workmanager_test_configuration));
    }

    @DexIgnore
    public S11(Context context, O01 o01, K41 k41, WorkDatabase workDatabase) {
        Context applicationContext = context.getApplicationContext();
        X01.e(new X01.Ai(o01.g()));
        List<N11> h2 = h(applicationContext, o01, k41);
        r(context, o01, k41, workDatabase, h2, new M11(context, o01, k41, workDatabase, h2));
    }

    @DexIgnore
    public S11(Context context, O01 o01, K41 k41, boolean z) {
        this(context, o01, k41, WorkDatabase.a(context.getApplicationContext(), k41.c(), z));
    }

    @DexIgnore
    public static void f(Context context, O01 o01) {
        synchronized (l) {
            if (j != null && k != null) {
                throw new IllegalStateException("WorkManager is already initialized.  Did you try to initialize it manually without disabling WorkManagerInitializer? See WorkManager#initialize(Context, Configuration) or the class level Javadoc for more information.");
            } else if (j == null) {
                Context applicationContext = context.getApplicationContext();
                if (k == null) {
                    k = new S11(applicationContext, o01, new L41(o01.i()));
                }
                j = k;
            }
        }
    }

    @DexIgnore
    @Deprecated
    public static S11 k() {
        S11 s11;
        synchronized (l) {
            s11 = j != null ? j : k;
        }
        return s11;
    }

    @DexIgnore
    public static S11 l(Context context) {
        S11 k2;
        synchronized (l) {
            k2 = k();
            if (k2 == null) {
                Context applicationContext = context.getApplicationContext();
                if (applicationContext instanceof O01.Bi) {
                    f(applicationContext, ((O01.Bi) applicationContext).a());
                    k2 = l(applicationContext);
                } else {
                    throw new IllegalStateException("WorkManager is not initialized properly.  You have explicitly disabled WorkManagerInitializer in your manifest, have not manually called WorkManager#initialize at this point, and your Application does not implement Configuration.Provider.");
                }
            }
        }
        return k2;
    }

    @DexIgnore
    @Override // com.fossil.G11
    public A11 b(List<? extends H11> list) {
        if (!list.isEmpty()) {
            return new P11(this, list).a();
        }
        throw new IllegalArgumentException("enqueue needs at least one WorkRequest.");
    }

    @DexIgnore
    @Override // com.fossil.G11
    public A11 d(String str, S01 s01, List<Z01> list) {
        return new P11(this, str, s01, list).a();
    }

    @DexIgnore
    public A11 g(UUID uuid) {
        V31 b2 = V31.b(uuid, this);
        this.d.b(b2);
        return b2.d();
    }

    @DexIgnore
    public List<N11> h(Context context, O01 o01, K41 k41) {
        return Arrays.asList(O11.a(context, this), new V11(context, o01, k41, this));
    }

    @DexIgnore
    public Context i() {
        return this.a;
    }

    @DexIgnore
    public O01 j() {
        return this.b;
    }

    @DexIgnore
    public Z31 m() {
        return this.g;
    }

    @DexIgnore
    public M11 n() {
        return this.f;
    }

    @DexIgnore
    public List<N11> o() {
        return this.e;
    }

    @DexIgnore
    public WorkDatabase p() {
        return this.c;
    }

    @DexIgnore
    public K41 q() {
        return this.d;
    }

    @DexIgnore
    public final void r(Context context, O01 o01, K41 k41, WorkDatabase workDatabase, List<N11> list, M11 m11) {
        Context applicationContext = context.getApplicationContext();
        this.a = applicationContext;
        this.b = o01;
        this.d = k41;
        this.c = workDatabase;
        this.e = list;
        this.f = m11;
        this.g = new Z31(workDatabase);
        this.h = false;
        if (Build.VERSION.SDK_INT < 24 || !applicationContext.isDeviceProtectedStorage()) {
            this.d.b(new ForceStopRunnable(applicationContext, this));
            return;
        }
        throw new IllegalStateException("Cannot initialize WorkManager in direct boot mode");
    }

    @DexIgnore
    public void s() {
        synchronized (l) {
            this.h = true;
            if (this.i != null) {
                this.i.finish();
                this.i = null;
            }
        }
    }

    @DexIgnore
    public void t() {
        if (Build.VERSION.SDK_INT >= 23) {
            D21.b(i());
        }
        p().j().t();
        O11.b(j(), p(), o());
    }

    @DexIgnore
    public void u(BroadcastReceiver.PendingResult pendingResult) {
        synchronized (l) {
            this.i = pendingResult;
            if (this.h) {
                pendingResult.finish();
                this.i = null;
            }
        }
    }

    @DexIgnore
    public void v(String str) {
        w(str, null);
    }

    @DexIgnore
    public void w(String str, WorkerParameters.a aVar) {
        this.d.b(new B41(this, str, aVar));
    }

    @DexIgnore
    public void x(String str) {
        this.d.b(new C41(this, str, true));
    }

    @DexIgnore
    public void y(String str) {
        this.d.b(new C41(this, str, false));
    }
}
