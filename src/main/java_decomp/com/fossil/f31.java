package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class F31 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public F31(String str, int i) {
        this.a = str;
        this.b = i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof F31)) {
            return false;
        }
        F31 f31 = (F31) obj;
        if (this.b == f31.b) {
            return this.a.equals(f31.a);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b;
    }
}
