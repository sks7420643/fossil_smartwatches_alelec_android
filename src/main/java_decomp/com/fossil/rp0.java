package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Rp0 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Rp0> CREATOR; // = new Bi();
    @DexIgnore
    public static /* final */ Rp0 c; // = new Ai();
    @DexIgnore
    public /* final */ Parcelable b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Rp0 {
        @DexIgnore
        public Ai() {
            super((Ai) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Parcelable.ClassLoaderCreator<Rp0> {
        @DexIgnore
        public Rp0 a(Parcel parcel) {
            return b(parcel, null);
        }

        @DexIgnore
        public Rp0 b(Parcel parcel, ClassLoader classLoader) {
            if (parcel.readParcelable(classLoader) == null) {
                return Rp0.c;
            }
            throw new IllegalStateException("superState must be null");
        }

        @DexIgnore
        public Rp0[] c(int i) {
            return new Rp0[i];
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
            return a(parcel);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.ClassLoaderCreator
        public /* bridge */ /* synthetic */ Rp0 createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return b(parcel, classLoader);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return c(i);
        }
    }

    @DexIgnore
    public Rp0() {
        this.b = null;
    }

    @DexIgnore
    public Rp0(Parcel parcel, ClassLoader classLoader) {
        Parcelable readParcelable = parcel.readParcelable(classLoader);
        this.b = readParcelable == null ? c : readParcelable;
    }

    @DexIgnore
    public Rp0(Parcelable parcelable) {
        if (parcelable != null) {
            this.b = parcelable == c ? null : parcelable;
            return;
        }
        throw new IllegalArgumentException("superState must not be null");
    }

    @DexIgnore
    public /* synthetic */ Rp0(Ai ai) {
        this();
    }

    @DexIgnore
    public final Parcelable a() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.b, i);
    }
}
