package com.fossil;

import android.location.Location;
import com.fossil.P72;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Er2 implements P72.Bi<Ga3> {
    @DexIgnore
    public /* final */ /* synthetic */ Location a;

    @DexIgnore
    public Er2(Dr2 dr2, Location location) {
        this.a = location;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.P72.Bi
    public final /* synthetic */ void a(Ga3 ga3) {
        ga3.onLocationChanged(this.a);
    }

    @DexIgnore
    @Override // com.fossil.P72.Bi
    public final void b() {
    }
}
