package com.fossil;

import com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class A27 implements MembersInjector<FindDevicePresenter> {
    @DexIgnore
    public static void a(FindDevicePresenter findDevicePresenter) {
        findDevicePresenter.I();
    }
}
