package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.wearables.fsl.appfilter.AppFilterProvider;
import com.fossil.wearables.fsl.appfilter.AppFilterProviderImpl;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.ContactProviderImpl;
import com.fossil.wearables.fsl.location.LocationProvider;
import com.fossil.wearables.fsl.location.LocationProviderImpl;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProvider;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProviderImp;
import com.mapped.Cd6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProvider;
import com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProviderImp;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProvider;
import com.portfolio.platform.data.legacy.threedotzero.DeviceProviderImp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Mn5 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static Mn5 o;
    @DexIgnore
    public static /* final */ Ai p; // = new Ai(null);
    @DexIgnore
    public String a; // = "Anonymous";
    @DexIgnore
    public ContactProvider b;
    @DexIgnore
    public LocationProvider c;
    @DexIgnore
    public AppFilterProvider d;
    @DexIgnore
    public DeviceProvider e;
    @DexIgnore
    public Lp5 f;
    @DexIgnore
    public MFSleepSessionProvider g;
    @DexIgnore
    public Hp5 h;
    @DexIgnore
    public Pp5 i;
    @DexIgnore
    public Jp5 j;
    @DexIgnore
    public SecondTimezoneProvider k;
    @DexIgnore
    public Np5 l;
    @DexIgnore
    public Rp5 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Mn5 a() {
            if (Mn5.o == null) {
                Mn5.o = new Mn5();
            }
            Mn5 mn5 = Mn5.o;
            if (mn5 != null) {
                return mn5;
            }
            Wg6.i();
            throw null;
        }
    }

    /*
    static {
        String simpleName = Mn5.class.getSimpleName();
        Wg6.b(simpleName, "ProviderManager::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public Mn5() {
        PortfolioApp.get.instance().getIface().x0(this);
    }

    @DexIgnore
    public final AppFilterProvider c() {
        AppFilterProvider appFilterProvider;
        synchronized (this) {
            String str = this.a;
            if (this.d == null) {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + AppFilterProviderImpl.DB_NAME;
                this.d = new Ep5(PortfolioApp.get.instance().getApplicationContext(), str2);
                FLogger.INSTANCE.getLocal().d(n, "Inside .getAppFilterProvider dbPath " + str2);
            } else {
                String str3 = str + LocaleConverter.LOCALE_DELIMITER + AppFilterProviderImpl.DB_NAME;
                AppFilterProvider appFilterProvider2 = this.d;
                if (appFilterProvider2 != null) {
                    String dbPath = appFilterProvider2.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getAppFilterProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                    if (!TextUtils.isEmpty(dbPath) && (!Wg6.a(str3, dbPath))) {
                        this.d = new Ep5(PortfolioApp.get.instance().getApplicationContext(), str3);
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            appFilterProvider = this.d;
            if (appFilterProvider == null) {
                Wg6.i();
                throw null;
            }
        }
        return appFilterProvider;
    }

    @DexIgnore
    public final ContactProvider d() {
        ContactProvider contactProvider;
        synchronized (this) {
            String str = this.a;
            if (this.b == null) {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + ContactProviderImpl.DB_NAME;
                FLogger.INSTANCE.getLocal().d(n, "Inside .getContactProvider newPath=" + str2);
                this.b = new Fp5(PortfolioApp.get.instance().getApplicationContext(), str2);
            } else {
                String str3 = str + LocaleConverter.LOCALE_DELIMITER + ContactProviderImpl.DB_NAME;
                ContactProvider contactProvider2 = this.b;
                if (contactProvider2 != null) {
                    String dbPath = contactProvider2.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getContactProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                    if (!TextUtils.isEmpty(dbPath) && (!Wg6.a(str3, dbPath))) {
                        this.b = new Fp5(PortfolioApp.get.instance().getApplicationContext(), str3);
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            contactProvider = this.b;
            if (contactProvider == null) {
                Wg6.i();
                throw null;
            }
        }
        return contactProvider;
    }

    @DexIgnore
    public final DeviceProvider e() {
        DeviceProviderImp deviceProviderImp;
        DeviceProvider deviceProvider;
        synchronized (this) {
            String str = this.a;
            FLogger.INSTANCE.getLocal().d(n, "Inside .getDeviceProvider with userId=" + str);
            if (this.e == null) {
                this.e = new DeviceProviderImp(PortfolioApp.get.instance().getApplicationContext(), str + LocaleConverter.LOCALE_DELIMITER + DeviceProviderImp.DB_NAME);
            } else {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + DeviceProviderImp.DB_NAME;
                try {
                    DeviceProvider deviceProvider2 = this.e;
                    if (deviceProvider2 != null) {
                        String dbPath = deviceProvider2.getDbPath();
                        if (TextUtils.isEmpty(dbPath) || (!Wg6.a(str2, dbPath))) {
                            deviceProviderImp = new DeviceProviderImp(PortfolioApp.get.instance().getApplicationContext(), str2);
                            this.e = deviceProviderImp;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } catch (Exception e2) {
                    FLogger.INSTANCE.getLocal().e(n, "getDeviceProvider - ex=" + e2);
                    if (TextUtils.isEmpty(null) || (!Wg6.a(str2, null))) {
                        deviceProviderImp = new DeviceProviderImp(PortfolioApp.get.instance().getApplicationContext(), str2);
                    }
                } catch (Throwable th) {
                    if (TextUtils.isEmpty(null) || (!Wg6.a(str2, null))) {
                        this.e = new DeviceProviderImp(PortfolioApp.get.instance().getApplicationContext(), str2);
                    }
                    throw th;
                }
            }
            deviceProvider = this.e;
            if (deviceProvider == null) {
                Wg6.i();
                throw null;
            }
        }
        return deviceProvider;
    }

    @DexIgnore
    public final DeviceProvider f(String str) {
        DeviceProvider deviceProvider;
        synchronized (this) {
            Wg6.c(str, ButtonService.USER_ID);
            if (this.e == null) {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + DeviceProviderImp.DB_NAME;
                FLogger.INSTANCE.getLocal().e(n, "Get device provider, current path " + str2);
                this.e = new DeviceProviderImp(PortfolioApp.get.instance().getApplicationContext(), str2);
            }
            deviceProvider = this.e;
            if (deviceProvider == null) {
                Wg6.i();
                throw null;
            }
        }
        return deviceProvider;
    }

    @DexIgnore
    public final Jp5 g() {
        Jp5 jp5;
        synchronized (this) {
            String str = this.a;
            if (this.j == null) {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + "firmwares.db";
                this.j = new Kp5(PortfolioApp.get.instance().getApplicationContext(), str2);
                FLogger.INSTANCE.getLocal().d(n, "Inside .getFirmwareProvider dbPath=" + str2);
            } else {
                String str3 = str + LocaleConverter.LOCALE_DELIMITER + "firmwares.db";
                Jp5 jp52 = this.j;
                if (jp52 != null) {
                    String dbPath = jp52.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getFirmwareProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                    if (!TextUtils.isEmpty(dbPath) && (!Wg6.a(str3, dbPath))) {
                        this.j = new Kp5(PortfolioApp.get.instance().getApplicationContext(), str3);
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            jp5 = this.j;
            if (jp5 == null) {
                Wg6.i();
                throw null;
            }
        }
        return jp5;
    }

    @DexIgnore
    public final Hp5 h() {
        Hp5 hp5;
        synchronized (this) {
            if (this.h == null) {
                String str = this.a;
                this.h = new Ip5(PortfolioApp.get.instance().getApplicationContext(), str + LocaleConverter.LOCALE_DELIMITER + "hourNotification.db");
            }
            hp5 = this.h;
            if (hp5 == null) {
                Wg6.i();
                throw null;
            }
        }
        return hp5;
    }

    @DexIgnore
    public final LocationProvider i() {
        LocationProvider locationProvider;
        synchronized (this) {
            String str = this.a;
            if (this.c == null) {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + LocationProviderImpl.DB_NAME;
                this.c = new LocationProviderImpl(PortfolioApp.get.instance().getApplicationContext(), str2);
                FLogger.INSTANCE.getLocal().d(n, "Inside .getLocationProvider path " + str2);
            } else {
                String str3 = str + LocaleConverter.LOCALE_DELIMITER + LocationProviderImpl.DB_NAME;
                LocationProvider locationProvider2 = this.c;
                if (locationProvider2 != null) {
                    String dbPath = locationProvider2.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getLocationProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                    if (!TextUtils.isEmpty(dbPath) && (!Wg6.a(str3, dbPath))) {
                        this.c = new LocationProviderImpl(PortfolioApp.get.instance().getApplicationContext(), str3);
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            locationProvider = this.c;
            if (locationProvider == null) {
                Wg6.i();
                throw null;
            }
        }
        return locationProvider;
    }

    @DexIgnore
    public final Lp5 j() {
        Lp5 lp5;
        synchronized (this) {
            String str = this.a;
            if (this.f == null) {
                this.f = new Mp5(PortfolioApp.get.instance().getApplicationContext(), str + LocaleConverter.LOCALE_DELIMITER + "microAppSetting.db");
            } else {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + "microAppSetting.db";
                Lp5 lp52 = this.f;
                if (lp52 != null) {
                    String dbPath = lp52.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getMicroAppSettingProvider previousDbPath=" + dbPath + ", newPath=" + str2);
                    if (!TextUtils.isEmpty(dbPath) && (!Wg6.a(str2, dbPath))) {
                        this.f = new Mp5(PortfolioApp.get.instance().getApplicationContext(), str2);
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            lp5 = this.f;
            if (lp5 == null) {
                Wg6.i();
                throw null;
            }
        }
        return lp5;
    }

    @DexIgnore
    public final Np5 k() {
        if (this.l == null) {
            this.l = new Op5(PortfolioApp.get.instance().getApplicationContext(), "phone_favorites_contact.db");
        }
        Np5 np5 = this.l;
        if (np5 != null) {
            return np5;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Pp5 l() {
        Qp5 qp5;
        Pp5 pp5;
        synchronized (this) {
            String str = this.a;
            if (this.i == null) {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + "pin.db";
                this.i = new Qp5(PortfolioApp.get.instance().getApplicationContext(), str2);
                FLogger.INSTANCE.getLocal().d(n, "Inside .getPinProvider dbPath=" + str2);
            } else {
                String str3 = str + LocaleConverter.LOCALE_DELIMITER + "pin.db";
                try {
                    Pp5 pp52 = this.i;
                    if (pp52 != null) {
                        String dbPath = pp52.getDbPath();
                        FLogger.INSTANCE.getLocal().d(n, "Inside .getPinProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                        if (C78.a(dbPath) || (!Wg6.a(str3, dbPath))) {
                            qp5 = new Qp5(PortfolioApp.get.instance().getApplicationContext(), str3);
                            this.i = qp5;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } catch (Exception e2) {
                    FLogger.INSTANCE.getLocal().e(n, "getPinProvider - ex=" + e2);
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getPinProvider previousDbPath=" + ((String) null) + ", newPath=" + str3);
                    if (C78.a(null) || (!Wg6.a(str3, null))) {
                        qp5 = new Qp5(PortfolioApp.get.instance().getApplicationContext(), str3);
                    }
                } catch (Throwable th) {
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getPinProvider previousDbPath=" + ((String) null) + ", newPath=" + str3);
                    if (C78.a(null) || (!Wg6.a(str3, null))) {
                        this.i = new Qp5(PortfolioApp.get.instance().getApplicationContext(), str3);
                    }
                    throw th;
                }
            }
            pp5 = this.i;
            if (pp5 == null) {
                Wg6.i();
                throw null;
            }
        }
        return pp5;
    }

    @DexIgnore
    public final SecondTimezoneProvider m() {
        SecondTimezoneProvider secondTimezoneProvider;
        synchronized (this) {
            String str = this.a;
            if (this.k == null) {
                this.k = new SecondTimezoneProviderImp(PortfolioApp.get.instance().getApplicationContext(), str + LocaleConverter.LOCALE_DELIMITER + SecondTimezoneProviderImp.DB_NAME);
            } else {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + SecondTimezoneProviderImp.DB_NAME;
                SecondTimezoneProvider secondTimezoneProvider2 = this.k;
                if (secondTimezoneProvider2 != null) {
                    String dbPath = secondTimezoneProvider2.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getSecondTimezoneProvider previousDbPath=" + dbPath + ", newPath=" + str2);
                    if (!TextUtils.isEmpty(dbPath) && (!Wg6.a(str2, dbPath))) {
                        this.k = new SecondTimezoneProviderImp(PortfolioApp.get.instance().getApplicationContext(), str2);
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            secondTimezoneProvider = this.k;
            if (secondTimezoneProvider == null) {
                Wg6.i();
                throw null;
            }
        }
        return secondTimezoneProvider;
    }

    @DexIgnore
    public final Rp5 n() {
        if (this.m == null) {
            synchronized (Mn5.class) {
                try {
                    if (this.m == null) {
                        String str = this.a + LocaleConverter.LOCALE_DELIMITER + "serverSetting.db";
                        FLogger.INSTANCE.getLocal().d(n, "getServerSettingProvider dbPath: " + str);
                        Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                        Wg6.b(applicationContext, "PortfolioApp.instance.applicationContext");
                        this.m = new Sp5(applicationContext, str);
                    }
                    Cd6 cd6 = Cd6.a;
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        Rp5 rp5 = this.m;
        if (rp5 != null) {
            return rp5;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final MFSleepSessionProvider o() {
        MFSleepSessionProvider mFSleepSessionProvider;
        synchronized (this) {
            String str = this.a;
            if (this.g == null) {
                String str2 = str + LocaleConverter.LOCALE_DELIMITER + MFSleepSessionProviderImp.DB_NAME;
                this.g = new MFSleepSessionProviderImp(PortfolioApp.get.instance().getApplicationContext(), str2);
                FLogger.INSTANCE.getLocal().d(n, "Inside .getSleepProvider dbPath " + str2);
            } else {
                String str3 = str + LocaleConverter.LOCALE_DELIMITER + MFSleepSessionProviderImp.DB_NAME;
                MFSleepSessionProvider mFSleepSessionProvider2 = this.g;
                if (mFSleepSessionProvider2 != null) {
                    String dbPath = mFSleepSessionProvider2.getDbPath();
                    FLogger.INSTANCE.getLocal().d(n, "Inside .getSleepProvider previousDbPath=" + dbPath + ", newPath=" + str3);
                    if (!TextUtils.isEmpty(dbPath) && (!Wg6.a(str3, dbPath))) {
                        this.g = new MFSleepSessionProviderImp(PortfolioApp.get.instance().getApplicationContext(), str3);
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            mFSleepSessionProvider = this.g;
            if (mFSleepSessionProvider == null) {
                Wg6.i();
                throw null;
            }
        }
        return mFSleepSessionProvider;
    }

    @DexIgnore
    public final void p() {
        FLogger.INSTANCE.getLocal().d(n, "reset database");
        this.b = null;
        this.c = null;
        this.d = null;
        this.g = null;
        this.i = null;
        this.e = null;
        this.h = null;
        this.f = null;
        this.m = null;
    }

    @DexIgnore
    public final void q(String str) {
        Wg6.c(str, ButtonService.USER_ID);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = n;
        local.d(str2, "updateUserId current " + str + " updated " + str);
        if (TextUtils.isEmpty(str)) {
            str = "Anonymous";
        }
        this.a = str;
    }
}
