package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Oa7 extends Hq4 {
    @DexIgnore
    public /* final */ MutableLiveData<String> h;
    @DexIgnore
    public /* final */ LiveData<String> i;
    @DexIgnore
    public /* final */ MutableLiveData<U37<Boolean>> j;
    @DexIgnore
    public /* final */ LiveData<U37<Boolean>> k;
    @DexIgnore
    public /* final */ MutableLiveData<U37<String>> l;
    @DexIgnore
    public /* final */ LiveData<U37<String>> m;
    @DexIgnore
    public /* final */ MutableLiveData<U37<String>> n;
    @DexIgnore
    public /* final */ LiveData<U37<String>> o;

    @DexIgnore
    public Oa7() {
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        this.h = mutableLiveData;
        this.i = mutableLiveData;
        MutableLiveData<U37<Boolean>> mutableLiveData2 = new MutableLiveData<>(new U37(Boolean.FALSE));
        this.j = mutableLiveData2;
        this.k = mutableLiveData2;
        MutableLiveData<U37<String>> mutableLiveData3 = new MutableLiveData<>();
        this.l = mutableLiveData3;
        this.m = mutableLiveData3;
        MutableLiveData<U37<String>> mutableLiveData4 = new MutableLiveData<>();
        this.n = mutableLiveData4;
        this.o = mutableLiveData4;
    }

    @DexIgnore
    public final void n(String str) {
        Wg6.c(str, "watchFaceId");
        this.l.l(new U37<>(str));
    }

    @DexIgnore
    public final void o(String str) {
        this.n.l(new U37<>(str));
    }

    @DexIgnore
    public final LiveData<U37<String>> p() {
        return this.m;
    }

    @DexIgnore
    public final LiveData<String> q() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<U37<String>> r() {
        return this.o;
    }

    @DexIgnore
    public final LiveData<U37<Boolean>> s() {
        return this.k;
    }

    @DexIgnore
    public final void t(String str) {
        Wg6.c(str, "watchFaceId");
        this.h.l(str);
    }

    @DexIgnore
    public final void u(boolean z) {
        this.j.l(new U37<>(Boolean.valueOf(z)));
    }
}
