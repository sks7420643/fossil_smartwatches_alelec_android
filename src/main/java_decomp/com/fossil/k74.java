package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K74 {
    @DexIgnore
    public /* final */ Class<?> a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public K74(Class<?> cls, int i, int i2) {
        R74.c(cls, "Null dependency anInterface.");
        this.a = cls;
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public static K74 e(Class<?> cls) {
        return new K74(cls, 0, 0);
    }

    @DexIgnore
    public static K74 f(Class<?> cls) {
        return new K74(cls, 1, 0);
    }

    @DexIgnore
    public static K74 g(Class<?> cls) {
        return new K74(cls, 1, 1);
    }

    @DexIgnore
    public static K74 h(Class<?> cls) {
        return new K74(cls, 2, 0);
    }

    @DexIgnore
    public Class<?> a() {
        return this.a;
    }

    @DexIgnore
    public boolean b() {
        return this.c == 0;
    }

    @DexIgnore
    public boolean c() {
        return this.b == 1;
    }

    @DexIgnore
    public boolean d() {
        return this.b == 2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof K74)) {
            return false;
        }
        K74 k74 = (K74) obj;
        return this.a == k74.a && this.b == k74.b && this.c == k74.c;
    }

    @DexIgnore
    public int hashCode() {
        return ((((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b) * 1000003) ^ this.c;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("Dependency{anInterface=");
        sb.append(this.a);
        sb.append(", type=");
        int i = this.b;
        sb.append(i == 1 ? "required" : i == 0 ? "optional" : "set");
        sb.append(", direct=");
        sb.append(this.c == 0);
        sb.append("}");
        return sb.toString();
    }
}
