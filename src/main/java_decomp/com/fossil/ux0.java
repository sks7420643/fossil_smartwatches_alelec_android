package com.fossil;

import android.database.sqlite.SQLiteStatement;
import com.mapped.Mi;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ux0 extends Tx0 implements Mi {
    @DexIgnore
    public /* final */ SQLiteStatement c;

    @DexIgnore
    public Ux0(SQLiteStatement sQLiteStatement) {
        super(sQLiteStatement);
        this.c = sQLiteStatement;
    }

    @DexIgnore
    @Override // com.mapped.Mi
    public long executeInsert() {
        return this.c.executeInsert();
    }

    @DexIgnore
    @Override // com.mapped.Mi
    public int executeUpdateDelete() {
        return this.c.executeUpdateDelete();
    }
}
