package com.fossil;

import com.fossil.S32;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Y12 implements S32.Ai {
    @DexIgnore
    public /* final */ B22 a;
    @DexIgnore
    public /* final */ V02 b;
    @DexIgnore
    public /* final */ Iterable c;
    @DexIgnore
    public /* final */ H02 d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore
    public Y12(B22 b22, V02 v02, Iterable iterable, H02 h02, int i) {
        this.a = b22;
        this.b = v02;
        this.c = iterable;
        this.d = h02;
        this.e = i;
    }

    @DexIgnore
    public static S32.Ai b(B22 b22, V02 v02, Iterable iterable, H02 h02, int i) {
        return new Y12(b22, v02, iterable, h02, i);
    }

    @DexIgnore
    @Override // com.fossil.S32.Ai
    public Object a() {
        return B22.c(this.a, this.b, this.c, this.d, this.e);
    }
}
