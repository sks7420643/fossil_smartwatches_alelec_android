package com.fossil;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import com.fossil.Tq4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.mapped.AppWrapper;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class K66 extends Tq4<Ai, Bi, Tq4.Ai> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ NotificationsRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements Tq4.Bi {
        @DexIgnore
        public /* final */ List<J06> a;
        @DexIgnore
        public /* final */ List<AppWrapper> b;

        @DexIgnore
        public Ai(List<J06> list, List<AppWrapper> list2) {
            Wg6.c(list, "contactWrapperList");
            Wg6.c(list2, "appWrapperList");
            this.a = list;
            this.b = list2;
        }

        @DexIgnore
        public final List<AppWrapper> a() {
            return this.b;
        }

        @DexIgnore
        public final List<J06> b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Tq4.Ci {
        @DexIgnore
        public Bi(boolean z) {
        }
    }

    /*
    static {
        String simpleName = K66.class.getSimpleName();
        Wg6.b(simpleName, "SaveAllHybridNotification::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public K66(NotificationsRepository notificationsRepository) {
        Wg6.c(notificationsRepository, "mNotificationsRepository");
        this.d = notificationsRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.Tq4$Bi] */
    @Override // com.fossil.Tq4
    public /* bridge */ /* synthetic */ void a(Ai ai) {
        f(ai);
    }

    @DexIgnore
    public void f(Ai ai) {
        Wg6.c(ai, "requestValues");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        for (J06 j06 : ai.b()) {
            if (!j06.isAdded()) {
                arrayList2.add(j06);
            } else {
                arrayList.add(j06);
            }
        }
        h(arrayList2);
        k(arrayList);
        for (AppWrapper appWrapper : ai.a()) {
            InstalledApp installedApp = appWrapper.getInstalledApp();
            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
            if (isSelected == null) {
                Wg6.i();
                throw null;
            } else if (isSelected.booleanValue()) {
                arrayList3.add(appWrapper);
            } else {
                arrayList4.add(appWrapper);
            }
        }
        g(arrayList4);
        j(arrayList3);
        FLogger.INSTANCE.getLocal().d(e, "Inside .SaveAllHybridNotification done");
        b().onSuccess(new Bi(true));
    }

    @DexIgnore
    public final void g(List<AppWrapper> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (AppWrapper appWrapper : list) {
                AppFilter appFilter = new AppFilter();
                InstalledApp installedApp = appWrapper.getInstalledApp();
                Integer valueOf = installedApp != null ? Integer.valueOf(installedApp.getDbRowId()) : null;
                if (valueOf != null) {
                    appFilter.setDbRowId(valueOf.intValue());
                    appFilter.setHour(appWrapper.getCurrentHandGroup());
                    InstalledApp installedApp2 = appWrapper.getInstalledApp();
                    appFilter.setType(installedApp2 != null ? installedApp2.getIdentifier() : null);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = e;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Removed App: name=");
                    InstalledApp installedApp3 = appWrapper.getInstalledApp();
                    sb.append(installedApp3 != null ? installedApp3.getIdentifier() : null);
                    sb.append(", rowId=");
                    InstalledApp installedApp4 = appWrapper.getInstalledApp();
                    sb.append(installedApp4 != null ? Integer.valueOf(installedApp4.getDbRowId()) : null);
                    sb.append(", hour=");
                    sb.append(appWrapper.getCurrentHandGroup());
                    local.d(str, sb.toString());
                    arrayList.add(appFilter);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            this.d.removeListAppFilter(arrayList);
        }
    }

    @DexIgnore
    public final void h(List<J06> list) {
        if (!list.isEmpty()) {
            ContactProvider d2 = Mn5.p.a().d();
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            for (J06 j06 : list) {
                Contact contact = j06.getContact();
                if (contact != null) {
                    Contact contact2 = d2.getContact(contact.getDbRowId());
                    if (contact2 != null) {
                        arrayList2.add(contact2);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = e;
                        local.d(str, "Removed contact=" + contact2.getFirstName() + ", rowId=" + contact2.getDbRowId());
                        List<ContactGroup> allContactGroups = d2.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
                        ArrayList arrayList4 = new ArrayList();
                        for (ContactGroup contactGroup : allContactGroups) {
                            Wg6.b(contactGroup, "contactGroupItem");
                            Iterator<Contact> it = contactGroup.getContacts().iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                Contact next = it.next();
                                int contactId = contact2.getContactId();
                                Wg6.b(next, "contactItem");
                                if (contactId == next.getContactId()) {
                                    contact2.setDbRowId(next.getDbRowId());
                                    arrayList.add(contactGroup);
                                    arrayList4.addAll(next.getPhoneNumbers());
                                    break;
                                }
                            }
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = e;
                        local2.d(str2, "Save phone numbers " + arrayList4 + " of contact " + contact2.getDisplayName());
                        Iterator it2 = arrayList4.iterator();
                        while (it2.hasNext()) {
                            PhoneNumber phoneNumber = (PhoneNumber) it2.next();
                            Wg6.b(phoneNumber, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
                            arrayList3.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
                        }
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            this.d.removeListContact(arrayList2);
            this.d.removeContactGroupList(arrayList);
            i(arrayList3);
        }
    }

    @DexIgnore
    public final void i(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact phoneFavoritesContact : list) {
            this.d.removePhoneFavoritesContact(phoneFavoritesContact);
        }
    }

    @DexIgnore
    public final void j(List<AppWrapper> list) {
        if (!list.isEmpty()) {
            ArrayList arrayList = new ArrayList();
            for (AppWrapper appWrapper : list) {
                AppFilter appFilter = new AppFilter();
                appFilter.setHour(appWrapper.getCurrentHandGroup());
                InstalledApp installedApp = appWrapper.getInstalledApp();
                appFilter.setType(installedApp != null ? installedApp.getIdentifier() : null);
                appFilter.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = e;
                StringBuilder sb = new StringBuilder();
                sb.append("Saved App: name=");
                InstalledApp installedApp2 = appWrapper.getInstalledApp();
                sb.append(installedApp2 != null ? installedApp2.getIdentifier() : null);
                sb.append(", hour=");
                sb.append(appWrapper.getCurrentHandGroup());
                local.d(str, sb.toString());
                arrayList.add(appFilter);
            }
            this.d.saveListAppFilters(arrayList);
        }
    }

    @DexIgnore
    public final void k(List<J06> list) {
        Iterator<J06> it;
        if (!list.isEmpty()) {
            List<ContactGroup> allContactGroups = this.d.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            ContactProvider d2 = Mn5.p.a().d();
            Iterator<J06> it2 = list.iterator();
            while (it2.hasNext()) {
                J06 next = it2.next();
                if (allContactGroups != null) {
                    Iterator<ContactGroup> it3 = allContactGroups.iterator();
                    boolean z = false;
                    while (true) {
                        if (!it3.hasNext()) {
                            break;
                        }
                        ContactGroup next2 = it3.next();
                        Wg6.b(next2, "contactGroup");
                        Iterator<Contact> it4 = next2.getContacts().iterator();
                        while (true) {
                            if (!it4.hasNext()) {
                                z = z;
                                break;
                            }
                            Contact next3 = it4.next();
                            if (!(next.getContact() == null || next3 == null)) {
                                int contactId = next3.getContactId();
                                Contact contact = next.getContact();
                                if (contact != null && contactId == contact.getContactId()) {
                                    Contact contact2 = next.getContact();
                                    Boolean valueOf = contact2 != null ? Boolean.valueOf(contact2.isUseCall()) : null;
                                    if (valueOf != null) {
                                        next3.setUseCall(valueOf.booleanValue());
                                        Contact contact3 = next.getContact();
                                        Boolean valueOf2 = contact3 != null ? Boolean.valueOf(contact3.isUseSms()) : null;
                                        if (valueOf2 != null) {
                                            next3.setUseSms(valueOf2.booleanValue());
                                            Contact contact4 = next.getContact();
                                            next3.setFirstName(contact4 != null ? contact4.getFirstName() : null);
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String str = e;
                                            StringBuilder sb = new StringBuilder();
                                            sb.append("Contact Id = ");
                                            Contact contact5 = next.getContact();
                                            sb.append(contact5 != null ? Integer.valueOf(contact5.getContactId()) : null);
                                            sb.append(", ");
                                            sb.append("Contact name = ");
                                            Contact contact6 = next.getContact();
                                            sb.append(contact6 != null ? contact6.getFirstName() : null);
                                            sb.append(", ");
                                            sb.append("Contact db row = ");
                                            Contact contact7 = next.getContact();
                                            sb.append(contact7 != null ? Integer.valueOf(contact7.getDbRowId()) : null);
                                            sb.append(", ");
                                            sb.append("Contact phone = ");
                                            sb.append(next.getPhoneNumber());
                                            local.d(str, sb.toString());
                                            next.setContact(next3);
                                            d2.removeContactGroup(next2);
                                            allContactGroups.remove(next2);
                                            z = true;
                                        } else {
                                            Wg6.i();
                                            throw null;
                                        }
                                    } else {
                                        Wg6.i();
                                        throw null;
                                    }
                                }
                            }
                        }
                        if (z) {
                            it = it2;
                            break;
                        }
                    }
                }
                it = it2;
                ContactGroup contactGroup = new ContactGroup();
                contactGroup.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
                contactGroup.setHour(next.getCurrentHandGroup());
                arrayList2.add(contactGroup);
                Contact contact8 = next.getContact();
                if (contact8 != null) {
                    contact8.setContactGroup(contactGroup);
                    Contact contact9 = next.getContact();
                    Boolean valueOf3 = contact9 != null ? Boolean.valueOf(contact9.isUseCall()) : null;
                    if (valueOf3 != null) {
                        contact8.setUseCall(valueOf3.booleanValue());
                        Contact contact10 = next.getContact();
                        Boolean valueOf4 = contact10 != null ? Boolean.valueOf(contact10.isUseSms()) : null;
                        if (valueOf4 != null) {
                            contact8.setUseSms(valueOf4.booleanValue());
                            contact8.setUseEmail(false);
                            arrayList.add(contact8);
                            ContentResolver contentResolver = PortfolioApp.get.instance().getContentResolver();
                            if (next.hasPhoneNumber()) {
                                Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                                Cursor query = contentResolver.query(uri, new String[]{"data1"}, "contact_id=" + contact8.getContactId(), null, null);
                                if (query != null) {
                                    while (query.moveToNext()) {
                                        try {
                                            PhoneNumber phoneNumber = new PhoneNumber();
                                            phoneNumber.setNumber(query.getString(query.getColumnIndex("data1")));
                                            phoneNumber.setContact(contact8);
                                            arrayList3.add(phoneNumber);
                                            if (next.isFavorites()) {
                                                arrayList4.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
                                            }
                                        } catch (Exception e2) {
                                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                            String str2 = e;
                                            local2.e(str2, "Error Inside " + e + ".saveContactToFSL - ex=" + e2);
                                        } catch (Throwable th) {
                                            query.close();
                                            throw th;
                                        }
                                    }
                                    query.close();
                                }
                                if (contact8.getContactId() == -100) {
                                    PhoneNumber phoneNumber2 = new PhoneNumber();
                                    phoneNumber2.setNumber("-1234");
                                    phoneNumber2.setContact(contact8);
                                    arrayList3.add(phoneNumber2);
                                }
                                if (contact8.getContactId() == -200) {
                                    PhoneNumber phoneNumber3 = new PhoneNumber();
                                    phoneNumber3.setNumber("-5678");
                                    phoneNumber3.setContact(contact8);
                                    arrayList3.add(phoneNumber3);
                                }
                            }
                            it2 = it;
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            this.d.saveContactGroupList(arrayList2);
            this.d.saveListContact(arrayList);
            this.d.saveListPhoneNumber(arrayList3);
            l(arrayList4);
        }
    }

    @DexIgnore
    public final void l(List<? extends PhoneFavoritesContact> list) {
        for (PhoneFavoritesContact phoneFavoritesContact : list) {
            this.d.savePhoneFavoritesContact(phoneFavoritesContact);
        }
    }
}
