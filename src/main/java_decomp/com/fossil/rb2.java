package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Rb2<T> implements Iterator<T> {
    @DexIgnore
    public /* final */ Qb2<T> b;
    @DexIgnore
    public int c; // = -1;

    @DexIgnore
    public Rb2(Qb2<T> qb2) {
        Rc2.k(qb2);
        this.b = qb2;
    }

    @DexIgnore
    public boolean hasNext() {
        return this.c < this.b.getCount() + -1;
    }

    @DexIgnore
    @Override // java.util.Iterator
    public T next() {
        if (hasNext()) {
            Qb2<T> qb2 = this.b;
            int i = this.c + 1;
            this.c = i;
            return qb2.get(i);
        }
        int i2 = this.c;
        StringBuilder sb = new StringBuilder(46);
        sb.append("Cannot advance the iterator beyond ");
        sb.append(i2);
        throw new NoSuchElementException(sb.toString());
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Cannot remove elements from a DataBufferIterator");
    }
}
