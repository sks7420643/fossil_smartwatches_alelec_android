package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ow4 extends ts0 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);

    @DexIgnore
    /* renamed from: a  reason: collision with root package name */
    public /* final */ MutableLiveData<cl7<Boolean, ServerError>> f2732a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Boolean, Boolean>> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<ps4> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<gl7<List<at4>, ServerError, Integer>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<gs4>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<vy4, ps4>> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<iz4> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<cl7<Boolean, ServerError>> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<gl7<String, String, String>> i; // = new MutableLiveData<>();
    @DexIgnore
    public LiveData<ps4> j;
    @DexIgnore
    public String k;
    @DexIgnore
    public ps4 l;
    @DexIgnore
    public Timer m;
    @DexIgnore
    public /* final */ tt4 n;
    @DexIgnore
    public /* final */ zt4 o;
    @DexIgnore
    public /* final */ on5 p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(kq7 kq7) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ow4.q;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$acceptOrJoin$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {121, 136, 155}, m = "invokeSuspend")
    public static final class b extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isAccepted;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ow4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$acceptOrJoin$1$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super Object>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ps4 $challenge;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, ps4 ps4, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$challenge = ps4;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, this.$challenge, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super Object> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Long f;
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    this.this$0.this$0.n.e();
                    String p = this.$challenge.p();
                    if (p != null && (f = ao7.f(PortfolioApp.h0.c().m1(p))) != null) {
                        return f;
                    }
                    this.this$0.this$0.p.m0(ao7.a(true));
                    return tl7.f3441a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ow4$b$b")
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$acceptOrJoin$1$currentChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.ow4$b$b  reason: collision with other inner class name */
        public static final class C0183b extends ko7 implements vp7<iv7, qn7<? super ps4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0183b(b bVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0183b bVar = new C0183b(this.this$0, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super ps4> qn7) {
                throw null;
                //return ((C0183b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.n.g(new String[]{"running", "waiting"}, xy4.f4212a.a());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$acceptOrJoin$1$result$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {137}, m = "invokeSuspend")
        public static final class c extends ko7 implements vp7<iv7, qn7<? super kz4<ps4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $activeSerialNumber;
            @DexIgnore
            public /* final */ /* synthetic */ int $encryptedMethod;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(b bVar, int i, String str, qn7 qn7) {
                super(2, qn7);
                this.this$0 = bVar;
                this.$encryptedMethod = i;
                this.$activeSerialNumber = str;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                c cVar = new c(this.this$0, this.$encryptedMethod, this.$activeSerialNumber, qn7);
                cVar.p$ = (iv7) obj;
                throw null;
                //return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super kz4<ps4>> qn7) {
                throw null;
                //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    tt4 tt4 = this.this$0.this$0.n;
                    String b = ow4.b(this.this$0.this$0);
                    int i2 = this.$encryptedMethod;
                    String str = this.$activeSerialNumber;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object D = tt4.D(b, i2, str, this);
                    return D == d ? d : D;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ow4 ow4, boolean z, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ow4;
            this.$isAccepted = z;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            b bVar = new b(this.this$0, this.$isAccepted, qn7);
            bVar.p$ = (iv7) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x007b  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00b4  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0129  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x01f9  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 540
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ow4.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {331, 332, 336, 342}, m = "invokeSuspend")
    public static final class c extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ow4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super List<xs4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<xs4>> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return this.this$0.this$0.o.l();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1$friendsIn$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class b extends ko7 implements vp7<iv7, qn7<? super List<? extends at4>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ dr7 $friends;
            @DexIgnore
            public /* final */ /* synthetic */ List $players;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(dr7 dr7, List list, qn7 qn7) {
                super(2, qn7);
                this.$friends = dr7;
                this.$players = list;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                b bVar = new b(this.$friends, this.$players, qn7);
                bVar.p$ = (iv7) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super List<? extends at4>> qn7) {
                throw null;
                //return ((b) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                yn7.d();
                if (this.label == 0) {
                    el7.b(obj);
                    return py4.i(this.$friends.element, this.$players);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ow4$c$c")
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1$friendsWrapper$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {331}, m = "invokeSuspend")
        /* renamed from: com.fossil.ow4$c$c  reason: collision with other inner class name */
        public static final class C0184c extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends xs4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0184c(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                C0184c cVar = new C0184c(this.this$0, qn7);
                cVar.p$ = (iv7) obj;
                throw null;
                //return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends xs4>>> qn7) {
                throw null;
                //return ((C0184c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    zt4 zt4 = this.this$0.this$0.o;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object g = zt4.g(this);
                    return g == d ? d : g;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$friendsInChallenge$1$playersWrapper$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {332}, m = "invokeSuspend")
        public static final class d extends ko7 implements vp7<iv7, qn7<? super kz4<List<? extends ms4>>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(c cVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                d dVar = new d(this.this$0, qn7);
                dVar.p$ = (iv7) obj;
                throw null;
                //return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super kz4<List<? extends ms4>>> qn7) {
                throw null;
                //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    tt4 tt4 = this.this$0.this$0.n;
                    String str = this.this$0.$challengeId;
                    this.L$0 = iv7;
                    this.label = 1;
                    Object t = tt4.t(str, new String[]{"waiting", "running", "completed", "left_after_start"}, this);
                    return t == d ? d : t;
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ow4 ow4, String str, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ow4;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            c cVar = new c(this.this$0, this.$challengeId, qn7);
            cVar.p$ = (iv7) obj;
            throw null;
            //return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((c) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x00a6  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00f8  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x0139  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0157  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0160  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0174  */
        @Override // com.fossil.zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 378
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ow4.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$leaveChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {198}, m = "invokeSuspend")
    public static final class d extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ow4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$leaveChallenge$1$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {Action.Music.MUSIC_END_ACTION, 210}, m = "invokeSuspend")
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ow4$d$a$a")
            @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$leaveChallenge$1$1$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.ow4$d$a$a  reason: collision with other inner class name */
            public static final class C0185a extends ko7 implements vp7<iv7, qn7<? super Long>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $synDataStr;
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0185a(String str, qn7 qn7) {
                    super(2, qn7);
                    this.$synDataStr = str;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0185a aVar = new C0185a(this.$synDataStr, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super Long> qn7) {
                    throw null;
                    //return ((C0185a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        return ao7.f(PortfolioApp.h0.c().m1(this.$synDataStr));
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, qn7 qn7) {
                super(2, qn7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(this.this$0, qn7);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x006e  */
            /* JADX WARNING: Removed duplicated region for block: B:23:0x0132  */
            @Override // com.fossil.zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r15) {
                /*
                // Method dump skipped, instructions count: 335
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.ow4.d.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ow4 ow4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ow4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            d dVar = new d(this.this$0, qn7);
            dVar.p$ = (iv7) obj;
            throw null;
            //return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((d) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.b.l(hl7.a(null, ao7.a(true)));
                dv7 b = bw7.b();
                a aVar = new a(this, null);
                this.L$0 = iv7;
                this.label = 1;
                if (eu7.g(b, aVar, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$loadChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS}, m = "invokeSuspend")
    public static final class e extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ow4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ow4 ow4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ow4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            e eVar = new e(this.this$0, qn7);
            eVar.p$ = (iv7) obj;
            throw null;
            //return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((e) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                ow4 ow4 = this.this$0;
                this.L$0 = iv7;
                this.label = 1;
                if (ow4.B(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.l(hl7.a(ao7.a(false), null));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel", f = "BCWaitingChallengeDetailViewModel.kt", l = {306}, m = "loadChallengeFromServer")
    public static final class f extends co7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ow4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ow4 ow4, qn7 qn7) {
            super(qn7);
            this.this$0 = ow4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.B(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$loadChallengeFromServer$result$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {306}, m = "invokeSuspend")
    public static final class g extends ko7 implements vp7<iv7, qn7<? super kz4<ps4>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ow4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(ow4 ow4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ow4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            g gVar = new g(this.this$0, qn7);
            gVar.p$ = (iv7) obj;
            throw null;
            //return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super kz4<ps4>> qn7) {
            throw null;
            //return ((g) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                tt4 tt4 = this.this$0.n;
                String b = ow4.b(this.this$0);
                this.L$0 = iv7;
                this.label = 1;
                Object n = tt4.n(tt4, b, null, this, 2, null);
                return n == d ? d : n;
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @eo7(c = "com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailViewModel$loadForVisitChallenge$1", f = "BCWaitingChallengeDetailViewModel.kt", l = {105}, m = "invokeSuspend")
    public static final class h extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public iv7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ow4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(ow4 ow4, qn7 qn7) {
            super(2, qn7);
            this.this$0 = ow4;
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final qn7<tl7> create(Object obj, qn7<?> qn7) {
            pq7.c(qn7, "completion");
            h hVar = new h(this.this$0, qn7);
            hVar.p$ = (iv7) obj;
            throw null;
            //return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.vp7
        public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
            throw null;
            //return ((h) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
        }

        @DexIgnore
        @Override // com.fossil.zn7
        public final Object invokeSuspend(Object obj) {
            Object d = yn7.d();
            int i = this.label;
            if (i == 0) {
                el7.b(obj);
                iv7 iv7 = this.p$;
                this.this$0.b.l(hl7.a(null, ao7.a(true)));
                ow4 ow4 = this.this$0;
                this.L$0 = iv7;
                this.label = 1;
                if (ow4.B(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                iv7 iv72 = (iv7) this.L$0;
                el7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.l(hl7.a(null, ao7.a(false)));
            return tl7.f3441a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends TimerTask {
        @DexIgnore
        public /* final */ /* synthetic */ ow4 b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;
        @DexIgnore
        public /* final */ /* synthetic */ ps4 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends ko7 implements vp7<iv7, qn7<? super tl7>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public iv7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ow4$i$a$a")
            /* renamed from: com.fossil.ow4$i$a$a  reason: collision with other inner class name */
            public static final class C0186a extends ko7 implements vp7<iv7, qn7<? super Object>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public iv7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0186a(a aVar, qn7 qn7) {
                    super(2, qn7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                    pq7.c(qn7, "completion");
                    C0186a aVar = new C0186a(this.this$0, qn7);
                    aVar.p$ = (iv7) obj;
                    throw null;
                    //return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.vp7
                public final Object invoke(iv7 iv7, qn7<? super Object> qn7) {
                    throw null;
                    //return ((C0186a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
                }

                @DexIgnore
                @Override // com.fossil.zn7
                public final Object invokeSuspend(Object obj) {
                    yn7.d();
                    if (this.label == 0) {
                        el7.b(obj);
                        i iVar = this.this$0.this$0;
                        if (iVar.c) {
                            iVar.d.v("running");
                        } else {
                            iVar.d.v("completed");
                        }
                        int c = this.this$0.this$0.b.n.c(this.this$0.this$0.d.f());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = ow4.r.a();
                        local.e(a2, "starAndStopChallenge - count: " + c);
                        if (c > 0) {
                            return ao7.f(this.this$0.this$0.b.n.B(this.this$0.this$0.d));
                        }
                        this.this$0.this$0.b.c.l(this.this$0.this$0.d);
                        return tl7.f3441a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(qn7 qn7, i iVar) {
                super(2, qn7);
                this.this$0 = iVar;
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final qn7<tl7> create(Object obj, qn7<?> qn7) {
                pq7.c(qn7, "completion");
                a aVar = new a(qn7, this.this$0);
                aVar.p$ = (iv7) obj;
                throw null;
                //return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.vp7
            public final Object invoke(iv7 iv7, qn7<? super tl7> qn7) {
                throw null;
                //return ((a) create(iv7, qn7)).invokeSuspend(tl7.f3441a);
            }

            @DexIgnore
            @Override // com.fossil.zn7
            public final Object invokeSuspend(Object obj) {
                Object d = yn7.d();
                int i = this.label;
                if (i == 0) {
                    el7.b(obj);
                    iv7 iv7 = this.p$;
                    dv7 b = bw7.b();
                    C0186a aVar = new C0186a(this, null);
                    this.L$0 = iv7;
                    this.label = 1;
                    if (eu7.g(b, aVar, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    iv7 iv72 = (iv7) this.L$0;
                    el7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return tl7.f3441a;
            }
        }

        @DexIgnore
        public i(ow4 ow4, boolean z, ps4 ps4) {
            this.b = ow4;
            this.c = z;
            this.d = ps4;
        }

        @DexIgnore
        public void run() {
            xw7 unused = gu7.d(us0.a(this.b), null, null, new a(null, this), 3, null);
        }
    }

    /*
    static {
        String simpleName = ow4.class.getSimpleName();
        pq7.b(simpleName, "BCWaitingChallengeDetail\u2026el::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public ow4(tt4 tt4, zt4 zt4, on5 on5) {
        pq7.c(tt4, "challengeRepository");
        pq7.c(zt4, "friendRepository");
        pq7.c(on5, "shared");
        this.n = tt4;
        this.o = zt4;
        this.p = on5;
    }

    @DexIgnore
    public static final /* synthetic */ String b(ow4 ow4) {
        String str = ow4.k;
        if (str != null) {
            return str;
        }
        pq7.n("challengeId");
        throw null;
    }

    @DexIgnore
    public final void A() {
        xw7 unused = gu7.d(us0.a(this), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object B(com.fossil.qn7<? super com.fossil.tl7> r8) {
        /*
            r7 = this;
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = 1
            boolean r0 = r8 instanceof com.fossil.ow4.f
            if (r0 == 0) goto L_0x005f
            r0 = r8
            com.fossil.ow4$f r0 = (com.fossil.ow4.f) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x005f
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x006d
            if (r3 != r6) goto L_0x0065
            java.lang.Object r0 = r0.L$0
            com.fossil.ow4 r0 = (com.fossil.ow4) r0
            com.fossil.el7.b(r1)
            r7 = r0
        L_0x0027:
            r0 = r1
            com.fossil.kz4 r0 = (com.fossil.kz4) r0
            java.lang.Object r1 = r0.c()
            com.fossil.ps4 r1 = (com.fossil.ps4) r1
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.fossil.ow4.q
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "loadChallengeFromServer - challenge: "
            r4.append(r5)
            r4.append(r1)
            java.lang.String r4 = r4.toString()
            r2.e(r3, r4)
            if (r1 == 0) goto L_0x0086
            r7.l = r1
            androidx.lifecycle.LiveData<com.fossil.ps4> r0 = r7.j
            if (r0 != 0) goto L_0x0059
            androidx.lifecycle.MutableLiveData<com.fossil.ps4> r0 = r7.c
            r0.l(r1)
        L_0x0059:
            r7.I(r1)
        L_0x005c:
            com.fossil.tl7 r0 = com.fossil.tl7.f3441a
        L_0x005e:
            return r0
        L_0x005f:
            com.fossil.ow4$f r0 = new com.fossil.ow4$f
            r0.<init>(r7, r8)
            goto L_0x0013
        L_0x0065:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x006d:
            com.fossil.el7.b(r1)
            com.fossil.dv7 r1 = com.fossil.bw7.b()
            com.fossil.ow4$g r3 = new com.fossil.ow4$g
            r4 = 0
            r3.<init>(r7, r4)
            r0.L$0 = r7
            r0.label = r6
            java.lang.Object r1 = com.fossil.eu7.g(r1, r3, r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
            goto L_0x005e
        L_0x0086:
            com.portfolio.platform.data.model.ServerError r0 = r0.a()
            androidx.lifecycle.LiveData<com.fossil.ps4> r1 = r7.j
            if (r1 != 0) goto L_0x009c
            androidx.lifecycle.MutableLiveData<com.fossil.cl7<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r1 = r7.h
            java.lang.Boolean r2 = com.fossil.ao7.a(r6)
            com.fossil.cl7 r0 = com.fossil.hl7.a(r2, r0)
            r1.l(r0)
            goto L_0x005c
        L_0x009c:
            androidx.lifecycle.MutableLiveData<com.fossil.cl7<java.lang.Boolean, com.portfolio.platform.data.model.ServerError>> r1 = r7.h
            r2 = 0
            java.lang.Boolean r2 = com.fossil.ao7.a(r2)
            com.fossil.cl7 r0 = com.fossil.hl7.a(r2, r0)
            r1.l(r0)
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ow4.B(com.fossil.qn7):java.lang.Object");
    }

    @DexIgnore
    public final void C(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = q;
        local.e(str2, "loadForVisitChallenge - visitId: " + str);
        xw7 unused = gu7.d(us0.a(this), null, null, new h(this, null), 3, null);
    }

    @DexIgnore
    public final void D() {
        LiveData<ps4> liveData = this.j;
        ps4 e2 = liveData != null ? liveData.e() : null;
        if (e2 != null) {
            String l0 = PortfolioApp.h0.c().l0();
            ht4 i2 = e2.i();
            if (pq7.a(l0, i2 != null ? i2.b() : null) && pq7.a("waiting", e2.n())) {
                long b2 = xy4.f4212a.b();
                Date m2 = e2.m();
                if (b2 < (m2 != null ? m2.getTime() : 0)) {
                    this.e.l(qy4.f3046a.f());
                    return;
                }
            }
            this.e.l(qy4.f3046a.e());
        }
    }

    @DexIgnore
    public final void E(vy4 vy4) {
        pq7.c(vy4, "option");
        LiveData<ps4> liveData = this.j;
        ps4 e2 = liveData != null ? liveData.e() : null;
        if (e2 != null) {
            int i2 = pw4.f2878a[vy4.ordinal()];
            if (i2 == 1) {
                m(vy4.EDIT, e2);
            } else if (i2 == 2) {
                m(vy4.ADD_FRIENDS, e2);
            } else if (i2 == 3) {
                m(vy4.LEAVE, e2);
            }
        }
    }

    @DexIgnore
    public final void F() {
        Timer timer = this.m;
        if (timer != null) {
            timer.cancel();
        }
    }

    @DexIgnore
    public final void G() {
        A();
        String str = this.k;
        if (str != null) {
            n(str);
        } else {
            pq7.n("challengeId");
            throw null;
        }
    }

    @DexIgnore
    public final void H() {
        ps4 e2 = this.c.e();
        if (e2 == null) {
            LiveData<ps4> liveData = this.j;
            e2 = liveData != null ? liveData.e() : null;
        }
        I(e2 != null ? py4.a(e2) : null);
    }

    @DexIgnore
    public final void I(ps4 ps4) {
        long b2;
        long j2 = 0;
        FLogger.INSTANCE.getLocal().e(q, "starAndStopChallenge - challenge: " + ps4);
        if (ps4 != null) {
            boolean z = true;
            if (!pq7.a("completed", ps4.n())) {
                Date m2 = ps4.m();
                long time = m2 != null ? m2.getTime() : 0;
                Date e2 = ps4.e();
                long time2 = e2 != null ? e2.getTime() : 0;
                if (!pq7.a("waiting", ps4.n())) {
                    b2 = time2 - xy4.f4212a.b();
                    z = false;
                } else if (xy4.f4212a.b() < time2) {
                    b2 = time - xy4.f4212a.b();
                } else {
                    b2 = 0;
                    z = false;
                }
                FLogger.INSTANCE.getLocal().e(qx4.n.a(), "starAndStopChallenge - startTime: " + time + " - endTime: " + time2 + " - exactTime: " + xy4.f4212a.b() + "- timeLeft: " + b2 + " - isStart: " + z);
                if (b2 >= 0) {
                    j2 = b2;
                }
                Timer timer = this.m;
                if (timer != null) {
                    timer.cancel();
                }
                Timer timer2 = new Timer();
                this.m = timer2;
                if (timer2 != null) {
                    timer2.schedule(new i(this, z, ps4), j2);
                }
            }
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.e(str, "acceptOrJoin - isAccepted: " + z);
        xw7 unused = gu7.d(us0.a(this), null, null, new b(this, z, null), 3, null);
    }

    @DexIgnore
    public final void m(vy4 vy4, ps4 ps4) {
        this.f.l(new cl7<>(vy4, ps4));
    }

    @DexIgnore
    public final void n(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = q;
        local.e(str2, "friendsInChallenge - challengeId: " + str);
        xw7 unused = gu7.d(us0.a(this), null, null, new c(this, str, null), 3, null);
    }

    @DexIgnore
    public final LiveData<gl7<String, String, String>> o() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<ps4> p() {
        return cz4.a(this.c);
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, ServerError>> q() {
        return this.h;
    }

    @DexIgnore
    public final LiveData<gl7<List<at4>, ServerError, Integer>> r() {
        return cz4.a(this.d);
    }

    @DexIgnore
    public final LiveData<ps4> s() {
        return this.j;
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, Boolean>> t() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<cl7<Boolean, ServerError>> u() {
        return this.f2732a;
    }

    @DexIgnore
    public final LiveData<cl7<vy4, ps4>> v() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<List<gs4>> w() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<iz4> x() {
        return this.g;
    }

    @DexIgnore
    public final void y(ps4 ps4, String str) {
        String str2;
        Date m2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = q;
        local.e(str3, "init - challenge: " + ps4 + " - visitId: " + str);
        if (str == null) {
            this.l = ps4;
            if (ps4 == null || (str2 = ps4.f()) == null) {
                str2 = "";
            }
            this.k = str2;
            tt4 tt4 = this.n;
            if (str2 != null) {
                this.j = cz4.a(tt4.b(str2));
                I(ps4);
                if (xy4.f4212a.b() < ((ps4 == null || (m2 = ps4.m()) == null) ? 0 : m2.getTime())) {
                    A();
                }
            } else {
                pq7.n("challengeId");
                throw null;
            }
        } else {
            this.k = str;
            C(str);
        }
        tt4 tt42 = this.n;
        String str4 = this.k;
        if (str4 != null) {
            tt42.J(str4, false);
            String str5 = this.k;
            if (str5 != null) {
                n(str5);
            } else {
                pq7.n("challengeId");
                throw null;
            }
        } else {
            pq7.n("challengeId");
            throw null;
        }
    }

    @DexIgnore
    public final void z() {
        xw7 unused = gu7.d(us0.a(this), null, null, new d(this, null), 3, null);
    }
}
