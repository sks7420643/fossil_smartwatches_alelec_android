package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Z84 {
    @DexIgnore
    public static Z84 a(Ta4 ta4, String str) {
        return new M84(ta4, str);
    }

    @DexIgnore
    public abstract Ta4 b();

    @DexIgnore
    public abstract String c();
}
