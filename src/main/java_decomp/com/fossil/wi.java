package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Wi extends Rp {
    @DexIgnore
    public static /* final */ Ii V; // = new Ii(null);
    @DexIgnore
    public /* final */ long U;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Wi(K5 k5, I60 i60, long j, String str, int i) {
        super(k5, i60, Yp.G0, V.a(j), false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 8) != 0 ? E.a("UUID.randomUUID().toString()") : str, 48);
        this.U = j;
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro, com.fossil.Mj
    public JSONObject C() {
        return G80.k(super.C(), Jd0.y5, Long.valueOf(this.U));
    }
}
