package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.W6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputViewModel;
import com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendActivity;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ku4 extends BaseFragment {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ Ai t; // = new Ai(null);
    @DexIgnore
    public Po4 g;
    @DexIgnore
    public G37<B45> h;
    @DexIgnore
    public BCCreateChallengeInputViewModel i;
    @DexIgnore
    public Vs4 j;
    @DexIgnore
    public Ts4 k;
    @DexIgnore
    public List<Gs4> l; // = new ArrayList();
    @DexIgnore
    public HashMap m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return Ku4.s;
        }

        @DexIgnore
        public final Ku4 b(Vs4 vs4, Ts4 ts4) {
            Ku4 ku4 = new Ku4();
            Bundle bundle = new Bundle();
            bundle.putParcelable("challenge_template_extra", vs4);
            bundle.putParcelable("challenge_draft_extra", ts4);
            ku4.setArguments(bundle);
            return ku4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 b;

        @DexIgnore
        public Bi(Ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.onBackPressed();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 b;

        @DexIgnore
        public Ci(Ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public final void onClick(View view) {
            Ku4.Q6(this.b).J();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 b;

        @DexIgnore
        public Di(Ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public final void onClick(View view) {
            Ku4.Q6(this.b).F();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 b;

        @DexIgnore
        public Ei(Ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.Z6();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 b;

        @DexIgnore
        public Fi(Ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            BCCreateChallengeInputViewModel Q6 = Ku4.Q6(this.b);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                Q6.I(Wt7.u0(valueOf).toString());
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 b;

        @DexIgnore
        public Gi(Ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            BCCreateChallengeInputViewModel Q6 = Ku4.Q6(this.b);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                Q6.E(Wt7.u0(valueOf).toString());
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 b;

        @DexIgnore
        public Hi(Ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Ku4.Q6(this.b).G(charSequence);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 b;

        @DexIgnore
        public Ii(Ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Ku4.Q6(this.b).H(charSequence);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 b;

        @DexIgnore
        public Ji(Ku4 ku4) {
            this.b = ku4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            Ku4.Q6(this.b).L(charSequence);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki<T> implements Ls0<BCCreateChallengeInputViewModel.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 a;

        @DexIgnore
        public Ki(Ku4 ku4) {
            this.a = ku4;
        }

        @DexIgnore
        public final void a(BCCreateChallengeInputViewModel.Bi bi) {
            this.a.W6(bi.a());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(BCCreateChallengeInputViewModel.Bi bi) {
            a(bi);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li<T> implements Ls0<Gl7<? extends Boolean, ? extends ServerError, ? extends Ts4>> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 a;

        @DexIgnore
        public Li(Ku4 ku4) {
            this.a = ku4;
        }

        @DexIgnore
        public final void a(Gl7<Boolean, ? extends ServerError, Ts4> gl7) {
            if (gl7.getFirst().booleanValue()) {
                Ts4 third = gl7.getThird();
                if (third == null) {
                    FragmentActivity activity = this.a.getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                }
                Ku4 ku4 = this.a;
                ku4.U6(ku4.V6(third));
                return;
            }
            ServerError serverError = (ServerError) gl7.getSecond();
            S37 s37 = S37.c;
            Integer code = serverError != null ? serverError.getCode() : null;
            String message = serverError != null ? serverError.getMessage() : null;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            Wg6.b(childFragmentManager, "childFragmentManager");
            s37.g(code, message, childFragmentManager);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Gl7<? extends Boolean, ? extends ServerError, ? extends Ts4> gl7) {
            a(gl7);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 a;

        @DexIgnore
        public Mi(Ku4 ku4) {
            this.a = ku4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            Wg6.b(bool, "it");
            if (bool.booleanValue()) {
                this.a.b();
            } else {
                this.a.a();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 a;

        @DexIgnore
        public Ni(Ku4 ku4) {
            this.a = ku4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            B45 b45 = (B45) Ku4.N6(this.a).a();
            if (b45 != null) {
                FlexibleTextInputLayout flexibleTextInputLayout = b45.B;
                Wg6.b(flexibleTextInputLayout, "inputDes");
                flexibleTextInputLayout.setErrorEnabled(!bool.booleanValue());
                if (!bool.booleanValue()) {
                    FlexibleTextInputLayout flexibleTextInputLayout2 = b45.B;
                    Wg6.b(flexibleTextInputLayout2, "inputDes");
                    FlexibleTextInputLayout flexibleTextInputLayout3 = b45.B;
                    Wg6.b(flexibleTextInputLayout3, "inputDes");
                    flexibleTextInputLayout2.setError(Um5.c(flexibleTextInputLayout3.getContext(), 2131886219));
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Oi<T> implements Ls0<Ts4> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 a;

        @DexIgnore
        public Oi(Ku4 ku4) {
            this.a = ku4;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0049  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0067  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x013c  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x013f  */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x0046  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a(com.fossil.Ts4 r9) {
            /*
            // Method dump skipped, instructions count: 440
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Ku4.Oi.a(com.fossil.Ts4):void");
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Ts4 ts4) {
            a(ts4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Pi<T> implements Ls0<List<Gs4>> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 a;

        @DexIgnore
        public Pi(Ku4 ku4) {
            this.a = ku4;
        }

        @DexIgnore
        public final void a(List<Gs4> list) {
            T t;
            FlexibleTextInputEditText flexibleTextInputEditText;
            this.a.l.clear();
            List list2 = this.a.l;
            Wg6.b(list, "models");
            list2.addAll(list);
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (next.c()) {
                    t = next;
                    break;
                }
            }
            T t2 = t;
            if (t2 != null) {
                Object a2 = t2.a();
                String c = Wg6.a(a2, "private") ? Um5.c(PortfolioApp.get.instance(), 2131886279) : Wg6.a(a2, "public_with_friend") ? Um5.c(PortfolioApp.get.instance(), 2131886278) : Um5.c(PortfolioApp.get.instance(), 2131886278);
                B45 b45 = (B45) Ku4.N6(this.a).a();
                if (b45 != null && (flexibleTextInputEditText = b45.y) != null) {
                    flexibleTextInputEditText.setText(c);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<Gs4> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Qi<T> implements Ls0<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 a;

        @DexIgnore
        public Qi(Ku4 ku4) {
            this.a = ku4;
        }

        @DexIgnore
        public final void a(Integer num) {
            FlexibleEditText flexibleEditText;
            B45 b45 = (B45) Ku4.N6(this.a).a();
            if (b45 != null && (flexibleEditText = b45.v) != null) {
                flexibleEditText.setText(String.valueOf(num.intValue()));
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Integer num) {
            a(num);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ri<T> implements Ls0<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 a;

        @DexIgnore
        public Ri(Ku4 ku4) {
            this.a = ku4;
        }

        @DexIgnore
        public final void a(Integer num) {
            FlexibleEditText flexibleEditText;
            B45 b45 = (B45) Ku4.N6(this.a).a();
            if (b45 != null && (flexibleEditText = b45.w) != null) {
                flexibleEditText.setText(String.valueOf(num.intValue()));
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Integer num) {
            a(num);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Si<T> implements Ls0<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 a;

        @DexIgnore
        public Si(Ku4 ku4) {
            this.a = ku4;
        }

        @DexIgnore
        public final void a(Integer num) {
            FlexibleEditText flexibleEditText;
            B45 b45 = (B45) Ku4.N6(this.a).a();
            if (b45 != null && (flexibleEditText = b45.z) != null) {
                flexibleEditText.setText(String.valueOf(num.intValue()));
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Integer num) {
            a(num);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ti<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 a;

        @DexIgnore
        public Ti(Ku4 ku4) {
            this.a = ku4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            B45 b45 = (B45) Ku4.N6(this.a).a();
            if (b45 != null) {
                FlexibleButton flexibleButton = b45.r;
                Wg6.b(flexibleButton, "btnNext");
                Wg6.b(bool, "it");
                flexibleButton.setEnabled(bool.booleanValue());
                FlexibleButton flexibleButton2 = b45.q;
                Wg6.b(flexibleButton2, "btnDone");
                flexibleButton2.setEnabled(bool.booleanValue());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ui<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 a;

        @DexIgnore
        public Ui(Ku4 ku4) {
            this.a = ku4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            int i;
            B45 b45 = (B45) Ku4.N6(this.a).a();
            if (b45 != null) {
                FlexibleTextView flexibleTextView = b45.A;
                Wg6.b(flexibleTextView, "ftvInputErrorDurationStep");
                Wg6.b(bool, "isValid");
                if (bool.booleanValue()) {
                    i = 8;
                } else {
                    FlexibleTextView flexibleTextView2 = b45.A;
                    Wg6.b(flexibleTextView2, "ftvInputErrorDurationStep");
                    FlexibleTextView flexibleTextView3 = b45.A;
                    Wg6.b(flexibleTextView3, "ftvInputErrorDurationStep");
                    flexibleTextView2.setText(Um5.c(flexibleTextView3.getContext(), 2131886268));
                    i = 0;
                }
                flexibleTextView.setVisibility(i);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Vi<T> implements Ls0<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 a;

        @DexIgnore
        public Vi(Ku4 ku4) {
            this.a = ku4;
        }

        @DexIgnore
        public final void a(Boolean bool) {
            int i;
            B45 b45 = (B45) Ku4.N6(this.a).a();
            if (b45 != null) {
                FlexibleTextView flexibleTextView = b45.A;
                Wg6.b(flexibleTextView, "ftvInputErrorDurationStep");
                Wg6.b(bool, "isValid");
                if (bool.booleanValue()) {
                    i = 8;
                } else {
                    FlexibleTextView flexibleTextView2 = b45.A;
                    Wg6.b(flexibleTextView2, "ftvInputErrorDurationStep");
                    FlexibleTextView flexibleTextView3 = b45.A;
                    Wg6.b(flexibleTextView3, "ftvInputErrorDurationStep");
                    flexibleTextView2.setText(Um5.c(flexibleTextView3.getContext(), 2131886275));
                    i = 0;
                }
                flexibleTextView.setVisibility(i);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(Boolean bool) {
            a(bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Wi<T> implements Ls0<BCCreateChallengeInputViewModel.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 a;

        @DexIgnore
        public Wi(Ku4 ku4) {
            this.a = ku4;
        }

        @DexIgnore
        public final void a(BCCreateChallengeInputViewModel.Ai ai) {
            B45 b45 = (B45) Ku4.N6(this.a).a();
            if (b45 == null) {
                return;
            }
            if (ai.b()) {
                FlexibleTextInputLayout flexibleTextInputLayout = b45.C;
                Wg6.b(flexibleTextInputLayout, "inputName");
                flexibleTextInputLayout.setErrorEnabled(false);
                return;
            }
            FlexibleTextInputLayout flexibleTextInputLayout2 = b45.C;
            Wg6.b(flexibleTextInputLayout2, "inputName");
            flexibleTextInputLayout2.setErrorEnabled(true);
            if (ai.a() < 1) {
                FlexibleTextInputLayout flexibleTextInputLayout3 = b45.C;
                Wg6.b(flexibleTextInputLayout3, "inputName");
                FlexibleTextView flexibleTextView = b45.A;
                Wg6.b(flexibleTextView, "ftvInputErrorDurationStep");
                flexibleTextInputLayout3.setError(Um5.c(flexibleTextView.getContext(), 2131886247));
            } else if (ai.a() > 32) {
                FlexibleTextInputLayout flexibleTextInputLayout4 = b45.C;
                Wg6.b(flexibleTextInputLayout4, "inputName");
                FlexibleTextView flexibleTextView2 = b45.A;
                Wg6.b(flexibleTextView2, "ftvInputErrorDurationStep");
                flexibleTextInputLayout4.setError(Um5.c(flexibleTextView2.getContext(), 2131886258));
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(BCCreateChallengeInputViewModel.Ai ai) {
            a(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Xi implements My5 {
        @DexIgnore
        public /* final */ /* synthetic */ Ku4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Xi(Ku4 ku4) {
            this.a = ku4;
        }

        @DexIgnore
        @Override // com.fossil.My5
        public void a(Gs4 gs4) {
            Wg6.c(gs4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            Ku4.Q6(this.a).K(gs4);
        }
    }

    /*
    static {
        String simpleName = Ku4.class.getSimpleName();
        Wg6.b(simpleName, "BCCreateChallengeInputFr\u2026nt::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ G37 N6(Ku4 ku4) {
        G37<B45> g37 = ku4.h;
        if (g37 != null) {
            return g37;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ BCCreateChallengeInputViewModel Q6(Ku4 ku4) {
        BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel = ku4.i;
        if (bCCreateChallengeInputViewModel != null) {
            return bCCreateChallengeInputViewModel;
        }
        Wg6.n("viewModel");
        throw null;
    }

    @DexIgnore
    public final void T6() {
        String k2;
        Vs4 vs4 = this.j;
        if (vs4 == null || (k2 = vs4.g()) == null) {
            Ts4 ts4 = this.k;
            k2 = ts4 != null ? ts4.k() : null;
        }
        String str = Wg6.a(k2, "activity_best_result") ? "bc_abr_input" : "bc_arg_input";
        AnalyticsHelper g2 = AnalyticsHelper.f.g();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            g2.m(str, activity);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    public final void U6(Intent intent) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public final Intent V6(Ts4 ts4) {
        Intent intent = new Intent();
        intent.putExtra("challenge_name_extra", ts4.e());
        intent.putExtra("challenge_des_extra", ts4.a());
        intent.putExtra("challenge_target_extra", ts4.i());
        intent.putExtra("challenge_duration_extra", ts4.b());
        return intent;
    }

    @DexIgnore
    public final void W6(Ts4 ts4) {
        BCInviteFriendActivity.A.b(this, ts4);
    }

    @DexIgnore
    public final void X6() {
        G37<B45> g37 = this.h;
        if (g37 != null) {
            B45 a2 = g37.a();
            if (a2 != null) {
                a2.E.setOnClickListener(new Bi(this));
                a2.r.setOnClickListener(new Ci(this));
                a2.q.setOnClickListener(new Di(this));
                a2.y.setOnClickListener(new Ei(this));
                a2.x.addTextChangedListener(new Fi(this));
                a2.u.addTextChangedListener(new Gi(this));
                a2.v.addTextChangedListener(new Hi(this));
                a2.w.addTextChangedListener(new Ii(this));
                a2.z.addTextChangedListener(new Ji(this));
                ImageView imageView = a2.F;
                Wg6.b(imageView, "ivThumbnail");
                Context context = imageView.getContext();
                Vs4 vs4 = this.j;
                imageView.setImageDrawable(W6.f(context, vs4 != null ? vs4.f() : 2131231014));
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Y6() {
        BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel = this.i;
        if (bCCreateChallengeInputViewModel != null) {
            bCCreateChallengeInputViewModel.p().h(getViewLifecycleOwner(), new Oi(this));
            BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel2 = this.i;
            if (bCCreateChallengeInputViewModel2 != null) {
                bCCreateChallengeInputViewModel2.y().h(getViewLifecycleOwner(), new Pi(this));
                BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel3 = this.i;
                if (bCCreateChallengeInputViewModel3 != null) {
                    bCCreateChallengeInputViewModel3.r().h(getViewLifecycleOwner(), new Qi(this));
                    BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel4 = this.i;
                    if (bCCreateChallengeInputViewModel4 != null) {
                        bCCreateChallengeInputViewModel4.u().h(getViewLifecycleOwner(), new Ri(this));
                        BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel5 = this.i;
                        if (bCCreateChallengeInputViewModel5 != null) {
                            bCCreateChallengeInputViewModel5.z().h(getViewLifecycleOwner(), new Si(this));
                            BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel6 = this.i;
                            if (bCCreateChallengeInputViewModel6 != null) {
                                bCCreateChallengeInputViewModel6.x().h(getViewLifecycleOwner(), new Ti(this));
                                BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel7 = this.i;
                                if (bCCreateChallengeInputViewModel7 != null) {
                                    bCCreateChallengeInputViewModel7.B().h(getViewLifecycleOwner(), new Ui(this));
                                    BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel8 = this.i;
                                    if (bCCreateChallengeInputViewModel8 != null) {
                                        bCCreateChallengeInputViewModel8.A().h(getViewLifecycleOwner(), new Vi(this));
                                        BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel9 = this.i;
                                        if (bCCreateChallengeInputViewModel9 != null) {
                                            bCCreateChallengeInputViewModel9.v().h(getViewLifecycleOwner(), new Wi(this));
                                            BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel10 = this.i;
                                            if (bCCreateChallengeInputViewModel10 != null) {
                                                bCCreateChallengeInputViewModel10.w().h(getViewLifecycleOwner(), new Ki(this));
                                                BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel11 = this.i;
                                                if (bCCreateChallengeInputViewModel11 != null) {
                                                    bCCreateChallengeInputViewModel11.q().h(getViewLifecycleOwner(), new Li(this));
                                                    BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel12 = this.i;
                                                    if (bCCreateChallengeInputViewModel12 != null) {
                                                        bCCreateChallengeInputViewModel12.s().h(getViewLifecycleOwner(), new Mi(this));
                                                        BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel13 = this.i;
                                                        if (bCCreateChallengeInputViewModel13 != null) {
                                                            bCCreateChallengeInputViewModel13.o().h(getViewLifecycleOwner(), new Ni(this));
                                                        } else {
                                                            Wg6.n("viewModel");
                                                            throw null;
                                                        }
                                                    } else {
                                                        Wg6.n("viewModel");
                                                        throw null;
                                                    }
                                                } else {
                                                    Wg6.n("viewModel");
                                                    throw null;
                                                }
                                            } else {
                                                Wg6.n("viewModel");
                                                throw null;
                                            }
                                        } else {
                                            Wg6.n("viewModel");
                                            throw null;
                                        }
                                    } else {
                                        Wg6.n("viewModel");
                                        throw null;
                                    }
                                } else {
                                    Wg6.n("viewModel");
                                    throw null;
                                }
                            } else {
                                Wg6.n("viewModel");
                                throw null;
                            }
                        } else {
                            Wg6.n("viewModel");
                            throw null;
                        }
                    } else {
                        Wg6.n("viewModel");
                        throw null;
                    }
                } else {
                    Wg6.n("viewModel");
                    throw null;
                }
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        } else {
            Wg6.n("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void Z6() {
        Es4 b = Es4.A.b();
        String c = Um5.c(requireContext(), 2131886213);
        Wg6.b(c, "LanguageHelper.getString\u2026nge_List__PrivacySetting)");
        b.setTitle(c);
        b.E6(this.l);
        b.G6(new Xi(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        b.show(childFragmentManager, Es4.A.a());
    }

    @DexIgnore
    public final void a7(boolean z) {
        G37<B45> g37 = this.h;
        if (g37 != null) {
            B45 a2 = g37.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                FlexibleButton flexibleButton = a2.r;
                Wg6.b(flexibleButton, "btnNext");
                flexibleButton.setVisibility(0);
                FlexibleButton flexibleButton2 = a2.q;
                Wg6.b(flexibleButton2, "btnDone");
                flexibleButton2.setVisibility(8);
                FlexibleTextInputLayout flexibleTextInputLayout = a2.D;
                Wg6.b(flexibleTextInputLayout, "inputPrivacy");
                flexibleTextInputLayout.setEnabled(true);
                return;
            }
            FlexibleButton flexibleButton3 = a2.r;
            Wg6.b(flexibleButton3, "btnNext");
            flexibleButton3.setVisibility(8);
            FlexibleButton flexibleButton4 = a2.q;
            Wg6.b(flexibleButton4, "btnDone");
            flexibleButton4.setVisibility(0);
            FlexibleTextInputLayout flexibleTextInputLayout2 = a2.D;
            Wg6.b(flexibleTextInputLayout2, "inputPrivacy");
            flexibleTextInputLayout2.setEnabled(false);
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 11 && i3 == -1) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.setResult(i3);
            }
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                activity2.finish();
            }
        }
        FragmentActivity activity3 = getActivity();
        if (activity3 != null) {
            activity3.setResult(i3, intent);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().getIface().d0().a(this);
        Po4 po4 = this.g;
        if (po4 != null) {
            Ts0 a2 = Vs0.d(this, po4).a(BCCreateChallengeInputViewModel.class);
            Wg6.b(a2, "ViewModelProviders.of(th\u2026putViewModel::class.java)");
            this.i = (BCCreateChallengeInputViewModel) a2;
            Bundle arguments = getArguments();
            this.j = arguments != null ? (Vs4) arguments.getParcelable("challenge_template_extra") : null;
            Bundle arguments2 = getArguments();
            this.k = arguments2 != null ? (Ts4) arguments2.getParcelable("challenge_draft_extra") : null;
            return;
        }
        Wg6.n("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        B45 b45 = (B45) Aq0.f(layoutInflater, 2131558525, viewGroup, false, A6());
        this.h = new G37<>(this, b45);
        Wg6.b(b45, "binding");
        return b45.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        T6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        Ts4 ts4 = this.k;
        if (ts4 != null) {
            BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel = this.i;
            if (bCCreateChallengeInputViewModel == null) {
                Wg6.n("viewModel");
                throw null;
            } else if (ts4 != null) {
                bCCreateChallengeInputViewModel.D(ts4);
                a7(false);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            a7(true);
            BCCreateChallengeInputViewModel bCCreateChallengeInputViewModel2 = this.i;
            if (bCCreateChallengeInputViewModel2 != null) {
                bCCreateChallengeInputViewModel2.C(this.j);
            } else {
                Wg6.n("viewModel");
                throw null;
            }
        }
        X6();
        Y6();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.m;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
