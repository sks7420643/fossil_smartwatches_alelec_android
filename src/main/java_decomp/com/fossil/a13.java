package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum A13 {
    zza(false),
    zzb(true),
    zzc(true),
    zzd(false);
    
    @DexIgnore
    public /* final */ boolean zze;

    @DexIgnore
    public A13(boolean z) {
        this.zze = z;
    }
}
