package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class S06 implements Factory<NotificationAppsPresenter> {
    @DexIgnore
    public static NotificationAppsPresenter a(N06 n06, Uq4 uq4, V36 v36, D26 d26, U06 u06, An4 an4, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new NotificationAppsPresenter(n06, uq4, v36, d26, u06, an4, notificationSettingsDatabase);
    }
}
