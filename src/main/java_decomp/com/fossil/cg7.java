package com.fossil;

import android.app.ListActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Cg7 extends ListActivity {
    @DexIgnore
    public void onPause() {
        super.onPause();
        Hg7.a(this);
    }

    @DexIgnore
    public void onResume() {
        super.onResume();
        Hg7.b(this);
    }
}
