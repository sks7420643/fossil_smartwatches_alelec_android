package com.fossil;

import android.util.Base64;
import com.fossil.Xz1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class H02 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class Ai {
        @DexIgnore
        public abstract H02 a();

        @DexIgnore
        public abstract Ai b(String str);

        @DexIgnore
        public abstract Ai c(byte[] bArr);

        @DexIgnore
        public abstract Ai d(Vy1 vy1);
    }

    @DexIgnore
    public static Ai a() {
        Xz1.Bi bi = new Xz1.Bi();
        bi.d(Vy1.DEFAULT);
        return bi;
    }

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public abstract byte[] c();

    @DexIgnore
    public abstract Vy1 d();

    @DexIgnore
    public H02 e(Vy1 vy1) {
        Ai a2 = a();
        a2.b(b());
        a2.d(vy1);
        a2.c(c());
        return a2.a();
    }

    @DexIgnore
    public final String toString() {
        return String.format("TransportContext(%s, %s, %s)", b(), d(), c() == null ? "" : Base64.encodeToString(c(), 2));
    }
}
