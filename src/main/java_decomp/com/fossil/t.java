package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.fossil.blesdk.database.SdkDatabaseWrapper", f = "SdkDatabaseWrapper.kt", l = {264}, m = "getThemeTemplate")
public final class T extends Jf6 {
    @DexIgnore
    public /* synthetic */ Object b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ /* synthetic */ U d;
    @DexIgnore
    public Object e;
    @DexIgnore
    public Object f;
    @DexIgnore
    public Object g;
    @DexIgnore
    public Object h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public T(U u, Xe6 xe6) {
        super(xe6);
        this.d = u;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        this.b = obj;
        this.c |= RecyclerView.UNDEFINED_DURATION;
        return this.d.f(null, null, this);
    }
}
