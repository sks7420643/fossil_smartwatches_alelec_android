package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O12 implements Factory<H22> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<K22> b;
    @DexIgnore
    public /* final */ Provider<V12> c;
    @DexIgnore
    public /* final */ Provider<T32> d;

    @DexIgnore
    public O12(Provider<Context> provider, Provider<K22> provider2, Provider<V12> provider3, Provider<T32> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static O12 a(Provider<Context> provider, Provider<K22> provider2, Provider<V12> provider3, Provider<T32> provider4) {
        return new O12(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static H22 c(Context context, K22 k22, V12 v12, T32 t32) {
        H22 a2 = N12.a(context, k22, v12, t32);
        Lk7.c(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public H22 b() {
        return c(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
