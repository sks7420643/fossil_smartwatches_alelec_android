package com.fossil;

import android.os.SystemClock;
import android.text.TextUtils;
import com.fossil.A91;
import com.misfit.frameworks.buttonservice.log.FileLogWriter;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Z91 implements A91 {
    @DexIgnore
    public /* final */ Map<String, Ai> a;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai {
        @DexIgnore
        public long a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ long d;
        @DexIgnore
        public /* final */ long e;
        @DexIgnore
        public /* final */ long f;
        @DexIgnore
        public /* final */ long g;
        @DexIgnore
        public /* final */ List<F91> h;

        @DexIgnore
        public Ai(String str, A91.Ai ai) {
            this(str, ai.b, ai.c, ai.d, ai.e, ai.f, a(ai));
            this.a = (long) ai.a.length;
        }

        @DexIgnore
        public Ai(String str, String str2, long j, long j2, long j3, long j4, List<F91> list) {
            this.b = str;
            this.c = "".equals(str2) ? null : str2;
            this.d = j;
            this.e = j2;
            this.f = j3;
            this.g = j4;
            this.h = list;
        }

        @DexIgnore
        public static List<F91> a(A91.Ai ai) {
            List<F91> list = ai.h;
            return list != null ? list : Ba1.f(ai.g);
        }

        @DexIgnore
        public static Ai b(Bi bi) throws IOException {
            if (Z91.l(bi) == 538247942) {
                return new Ai(Z91.n(bi), Z91.n(bi), Z91.m(bi), Z91.m(bi), Z91.m(bi), Z91.m(bi), Z91.k(bi));
            }
            throw new IOException();
        }

        @DexIgnore
        public A91.Ai c(byte[] bArr) {
            A91.Ai ai = new A91.Ai();
            ai.a = bArr;
            ai.b = this.c;
            ai.c = this.d;
            ai.d = this.e;
            ai.e = this.f;
            ai.f = this.g;
            ai.g = Ba1.g(this.h);
            ai.h = Collections.unmodifiableList(this.h);
            return ai;
        }

        @DexIgnore
        public boolean d(OutputStream outputStream) {
            try {
                Z91.s(outputStream, 538247942);
                Z91.u(outputStream, this.b);
                Z91.u(outputStream, this.c == null ? "" : this.c);
                Z91.t(outputStream, this.d);
                Z91.t(outputStream, this.e);
                Z91.t(outputStream, this.f);
                Z91.t(outputStream, this.g);
                Z91.r(this.h, outputStream);
                outputStream.flush();
                return true;
            } catch (IOException e2) {
                U91.b("%s", e2.toString());
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bi extends FilterInputStream {
        @DexIgnore
        public /* final */ long b;
        @DexIgnore
        public long c;

        @DexIgnore
        public Bi(InputStream inputStream, long j) {
            super(inputStream);
            this.b = j;
        }

        @DexIgnore
        public long a() {
            return this.b - this.c;
        }

        @DexIgnore
        @Override // java.io.FilterInputStream, java.io.InputStream
        public int read() throws IOException {
            int read = super.read();
            if (read != -1) {
                this.c++;
            }
            return read;
        }

        @DexIgnore
        @Override // java.io.FilterInputStream, java.io.InputStream
        public int read(byte[] bArr, int i, int i2) throws IOException {
            int read = super.read(bArr, i, i2);
            if (read != -1) {
                this.c += (long) read;
            }
            return read;
        }
    }

    @DexIgnore
    public Z91(File file) {
        this(file, FileLogWriter.FILE_LOG_SIZE_THRESHOLD);
    }

    @DexIgnore
    public Z91(File file, int i) {
        this.a = new LinkedHashMap(16, 0.75f, true);
        this.b = 0;
        this.c = file;
        this.d = i;
    }

    @DexIgnore
    public static int j(InputStream inputStream) throws IOException {
        int read = inputStream.read();
        if (read != -1) {
            return read;
        }
        throw new EOFException();
    }

    @DexIgnore
    public static List<F91> k(Bi bi) throws IOException {
        int l = l(bi);
        if (l >= 0) {
            List<F91> emptyList = l == 0 ? Collections.emptyList() : new ArrayList<>();
            for (int i = 0; i < l; i++) {
                emptyList.add(new F91(n(bi).intern(), n(bi).intern()));
            }
            return emptyList;
        }
        throw new IOException("readHeaderList size=" + l);
    }

    @DexIgnore
    public static int l(InputStream inputStream) throws IOException {
        return (j(inputStream) << 0) | 0 | (j(inputStream) << 8) | (j(inputStream) << 16) | (j(inputStream) << 24);
    }

    @DexIgnore
    public static long m(InputStream inputStream) throws IOException {
        return ((((long) j(inputStream)) & 255) << 0) | 0 | ((((long) j(inputStream)) & 255) << 8) | ((((long) j(inputStream)) & 255) << 16) | ((((long) j(inputStream)) & 255) << 24) | ((((long) j(inputStream)) & 255) << 32) | ((((long) j(inputStream)) & 255) << 40) | ((((long) j(inputStream)) & 255) << 48) | ((((long) j(inputStream)) & 255) << 56);
    }

    @DexIgnore
    public static String n(Bi bi) throws IOException {
        return new String(q(bi, m(bi)), "UTF-8");
    }

    @DexIgnore
    public static byte[] q(Bi bi, long j) throws IOException {
        long a2 = bi.a();
        if (j >= 0 && j <= a2) {
            int i = (int) j;
            if (((long) i) == j) {
                byte[] bArr = new byte[i];
                new DataInputStream(bi).readFully(bArr);
                return bArr;
            }
        }
        throw new IOException("streamToBytes length=" + j + ", maxLength=" + a2);
    }

    @DexIgnore
    public static void r(List<F91> list, OutputStream outputStream) throws IOException {
        if (list != null) {
            s(outputStream, list.size());
            for (F91 f91 : list) {
                u(outputStream, f91.a());
                u(outputStream, f91.b());
            }
            return;
        }
        s(outputStream, 0);
    }

    @DexIgnore
    public static void s(OutputStream outputStream, int i) throws IOException {
        outputStream.write((i >> 0) & 255);
        outputStream.write((i >> 8) & 255);
        outputStream.write((i >> 16) & 255);
        outputStream.write((i >> 24) & 255);
    }

    @DexIgnore
    public static void t(OutputStream outputStream, long j) throws IOException {
        outputStream.write((byte) ((int) (j >>> 0)));
        outputStream.write((byte) ((int) (j >>> 8)));
        outputStream.write((byte) ((int) (j >>> 16)));
        outputStream.write((byte) ((int) (j >>> 24)));
        outputStream.write((byte) ((int) (j >>> 32)));
        outputStream.write((byte) ((int) (j >>> 40)));
        outputStream.write((byte) ((int) (j >>> 48)));
        outputStream.write((byte) ((int) (j >>> 56)));
    }

    @DexIgnore
    public static void u(OutputStream outputStream, String str) throws IOException {
        byte[] bytes = str.getBytes("UTF-8");
        t(outputStream, (long) bytes.length);
        outputStream.write(bytes, 0, bytes.length);
    }

    @DexIgnore
    @Override // com.fossil.A91
    public void a() {
        synchronized (this) {
            if (!this.c.exists()) {
                if (!this.c.mkdirs()) {
                    U91.c("Unable to create cache dir %s", this.c.getAbsolutePath());
                }
                return;
            }
            File[] listFiles = this.c.listFiles();
            if (listFiles != null) {
                for (File file : listFiles) {
                    try {
                        long length = file.length();
                        Bi bi = new Bi(new BufferedInputStream(d(file)), length);
                        try {
                            Ai b2 = Ai.b(bi);
                            b2.a = length;
                            i(b2.b, b2);
                        } finally {
                            bi.close();
                        }
                    } catch (IOException e) {
                        file.delete();
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.A91
    public A91.Ai b(String str) {
        synchronized (this) {
            Ai ai = this.a.get(str);
            if (ai == null) {
                return null;
            }
            File f = f(str);
            try {
                Bi bi = new Bi(new BufferedInputStream(d(f)), f.length());
                try {
                    Ai b2 = Ai.b(bi);
                    if (!TextUtils.equals(str, b2.b)) {
                        U91.b("%s: key=%s, found=%s", f.getAbsolutePath(), str, b2.b);
                        p(str);
                        return null;
                    }
                    A91.Ai c2 = ai.c(q(bi, bi.a()));
                    bi.close();
                    return c2;
                } finally {
                    bi.close();
                }
            } catch (IOException e) {
                U91.b("%s: %s", f.getAbsolutePath(), e.toString());
                o(str);
                return null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.A91
    public void c(String str, A91.Ai ai) {
        synchronized (this) {
            h(ai.a.length);
            File f = f(str);
            try {
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(e(f));
                Ai ai2 = new Ai(str, ai);
                if (ai2.d(bufferedOutputStream)) {
                    bufferedOutputStream.write(ai.a);
                    bufferedOutputStream.close();
                    i(str, ai2);
                } else {
                    bufferedOutputStream.close();
                    U91.b("Failed to write header for %s", f.getAbsolutePath());
                    throw new IOException();
                }
            } catch (IOException e) {
                if (!f.delete()) {
                    U91.b("Could not clean up file %s", f.getAbsolutePath());
                }
            }
        }
    }

    @DexIgnore
    public InputStream d(File file) throws FileNotFoundException {
        return new FileInputStream(file);
    }

    @DexIgnore
    public OutputStream e(File file) throws FileNotFoundException {
        return new FileOutputStream(file);
    }

    @DexIgnore
    public File f(String str) {
        return new File(this.c, g(str));
    }

    @DexIgnore
    public final String g(String str) {
        int length = str.length() / 2;
        int hashCode = str.substring(0, length).hashCode();
        return String.valueOf(hashCode) + String.valueOf(str.substring(length).hashCode());
    }

    @DexIgnore
    public final void h(int i) {
        long j = (long) i;
        if (this.b + j >= ((long) this.d)) {
            if (U91.b) {
                U91.e("Pruning old cache entries.", new Object[0]);
            }
            long j2 = this.b;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            Iterator<Map.Entry<String, Ai>> it = this.a.entrySet().iterator();
            int i2 = 0;
            while (true) {
                if (!it.hasNext()) {
                    i2 = i2;
                    break;
                }
                Ai value = it.next().getValue();
                if (f(value.b).delete()) {
                    this.b -= value.a;
                } else {
                    String str = value.b;
                    U91.b("Could not delete cache entry for key=%s, filename=%s", str, g(str));
                }
                it.remove();
                i2++;
                if (((float) (this.b + j)) < ((float) this.d) * 0.9f) {
                    break;
                }
            }
            if (U91.b) {
                U91.e("pruned %d files, %d bytes, %d ms", Integer.valueOf(i2), Long.valueOf(this.b - j2), Long.valueOf(SystemClock.elapsedRealtime() - elapsedRealtime));
            }
        }
    }

    @DexIgnore
    public final void i(String str, Ai ai) {
        if (!this.a.containsKey(str)) {
            this.b += ai.a;
        } else {
            this.b = (ai.a - this.a.get(str).a) + this.b;
        }
        this.a.put(str, ai);
    }

    @DexIgnore
    public void o(String str) {
        synchronized (this) {
            boolean delete = f(str).delete();
            p(str);
            if (!delete) {
                U91.b("Could not delete cache entry for key=%s, filename=%s", str, g(str));
            }
        }
    }

    @DexIgnore
    public final void p(String str) {
        Ai remove = this.a.remove(str);
        if (remove != null) {
            this.b -= remove.a;
        }
    }
}
