package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class U36 implements MembersInjector<DoNotDisturbScheduledTimePresenter> {
    @DexIgnore
    public static void a(DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter) {
        doNotDisturbScheduledTimePresenter.B();
    }
}
