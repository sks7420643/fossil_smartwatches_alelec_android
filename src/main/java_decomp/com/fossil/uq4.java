package com.fossil;

import com.fossil.Tq4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Uq4 {
    @DexIgnore
    public /* final */ Vq4 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<R extends Tq4.Ci, E extends Tq4.Ai> implements Tq4.Di<R, E> {
        @DexIgnore
        public /* final */ Tq4.Di<R, E> a;
        @DexIgnore
        public /* final */ Uq4 b;

        @DexIgnore
        public Ai(Tq4.Di<R, E> di, Uq4 uq4) {
            this.a = di;
            this.b = uq4;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.Uq4$Ai<R extends com.fossil.Tq4$Ci, E extends com.fossil.Tq4$Ai> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void a(Object obj) {
            b((Tq4.Ai) obj);
        }

        @DexIgnore
        public void b(E e) {
            this.b.c(e, this.a);
        }

        @DexIgnore
        public void c(R r) {
            this.b.d(r, this.a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.Uq4$Ai<R extends com.fossil.Tq4$Ci, E extends com.fossil.Tq4$Ai> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void onSuccess(Object obj) {
            c((Tq4.Ci) obj);
        }
    }

    @DexIgnore
    public Uq4(Vq4 vq4) {
        this.a = vq4;
    }

    @DexIgnore
    public static /* synthetic */ void b(Tq4 tq4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("UseCaseHandler", "execute: getIdlingResource = " + T37.b());
        tq4.c();
        if (!T37.b().a()) {
            T37.a();
        }
    }

    @DexIgnore
    public <Q extends Tq4.Bi, R extends Tq4.Ci, E extends Tq4.Ai> void a(Tq4<Q, R, E> tq4, Q q, Tq4.Di<R, E> di) {
        tq4.d(q);
        tq4.e(new Ai(di, this));
        T37.c();
        this.a.execute(new Mo4(tq4));
    }

    @DexIgnore
    public <R extends Tq4.Ci, E extends Tq4.Ai> void c(E e, Tq4.Di<R, E> di) {
        this.a.b(e, di);
    }

    @DexIgnore
    public <R extends Tq4.Ci, E extends Tq4.Ai> void d(R r, Tq4.Di<R, E> di) {
        this.a.a(r, di);
    }
}
