package com.fossil;

import com.fossil.Ix1;
import com.fossil.crypto.EllipticCurveKeyPair;
import com.mapped.Wg6;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Fq extends Lp {
    @DexIgnore
    public /* final */ ArrayList<Ow> C; // = By1.a(this.i, Hm7.c(Ow.f));
    @DexIgnore
    public byte[] D;
    @DexIgnore
    public /* final */ EllipticCurveKeyPair E;
    @DexIgnore
    public /* final */ byte[] F;

    @DexIgnore
    public Fq(K5 k5, I60 i60, byte[] bArr) {
        super(k5, i60, Yp.R, null, false, 24);
        this.F = bArr;
        EllipticCurveKeyPair create = EllipticCurveKeyPair.create();
        Wg6.b(create, "EllipticCurveKeyPair.create()");
        this.E = create;
    }

    @DexIgnore
    public static final /* synthetic */ void G(Fq fq) {
        K5 k5 = fq.w;
        byte[] publicKey = fq.E.publicKey();
        Wg6.b(publicKey, "keyPair.publicKey()");
        Lp.i(fq, new Dt(k5, publicKey), new Go(fq), new So(fq), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        byte[] bArr = this.F;
        if (bArr.length != 16) {
            k(Zq.y);
        } else {
            Lp.i(this, new Et(this.w, Rt.c, bArr), new Fp(this), new Sp(this), null, null, null, 56, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return G80.k(super.C(), Jd0.q2, Dy1.e(this.F, null, 1, null));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        JSONObject E2 = super.E();
        byte[] bArr = this.D;
        if (bArr != null) {
            G80.k(E2, Jd0.t2, Long.valueOf(Ix1.a.b(bArr, Ix1.Ai.CRC32)));
        }
        return E2;
    }

    @DexIgnore
    public final void H() {
        byte[] bArr = this.D;
        if (bArr != null) {
            Lp.h(this, new Ji(this.w, this.x, Hd0.y.b(), Dm7.k(bArr, 0, 16), this.z), In.b, Un.b, null, new Wm(this), null, 40, null);
        } else {
            l(Nr.a(this.v, null, Zq.i, null, null, 13));
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        byte[] bArr = this.D;
        return bArr != null ? bArr : new byte[0];
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.C;
    }
}
