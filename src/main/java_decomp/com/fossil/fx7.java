package com.fossil;

import com.facebook.internal.AnalyticsEvents;
import com.fossil.Lz7;
import com.mapped.Af6;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import java.util.ArrayList;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Fx7 implements Rm6, Su7, Nx7 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater b; // = AtomicReferenceFieldUpdater.newUpdater(Fx7.class, Object.class, "_state");
    @DexIgnore
    public volatile Object _parentHandle;
    @DexIgnore
    public volatile Object _state;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<T> extends Lu7<T> {
        @DexIgnore
        public /* final */ Fx7 i;

        @DexIgnore
        public Ai(Xe6<? super T> xe6, Fx7 fx7) {
            super(xe6, 1);
            this.i = fx7;
        }

        @DexIgnore
        @Override // com.fossil.Lu7
        public Throwable r(Rm6 rm6) {
            Throwable e;
            Object Q = this.i.Q();
            return (!(Q instanceof Ci) || (e = ((Ci) Q).e()) == null) ? Q instanceof Vu7 ? ((Vu7) Q).a : rm6.k() : e;
        }

        @DexIgnore
        @Override // com.fossil.Lu7
        public String z() {
            return "AwaitContinuation";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends Ex7<Rm6> {
        @DexIgnore
        public /* final */ Fx7 f;
        @DexIgnore
        public /* final */ Ci g;
        @DexIgnore
        public /* final */ Ru7 h;
        @DexIgnore
        public /* final */ Object i;

        @DexIgnore
        public Bi(Fx7 fx7, Ci ci, Ru7 ru7, Object obj) {
            super(ru7.f);
            this.f = fx7;
            this.g = ci;
            this.h = ru7;
            this.i = obj;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Cd6 invoke(Throwable th) {
            w(th);
            return Cd6.a;
        }

        @DexIgnore
        @Override // com.fossil.Lz7
        public String toString() {
            return "ChildCompletion[" + this.h + ", " + this.i + ']';
        }

        @DexIgnore
        @Override // com.fossil.Zu7
        public void w(Throwable th) {
            this.f.E(this.g, this.h, this.i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Sw7 {
        @DexIgnore
        public volatile Object _exceptionsHolder; // = null;
        @DexIgnore
        public volatile int _isCompleting;
        @DexIgnore
        public volatile Object _rootCause;
        @DexIgnore
        public /* final */ Kx7 b;

        @DexIgnore
        public Ci(Kx7 kx7, boolean z, Throwable th) {
            this.b = kx7;
            this._isCompleting = z ? 1 : 0;
            this._rootCause = th;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v5, resolved type: java.util.ArrayList<java.lang.Throwable> */
        /* JADX WARN: Multi-variable type inference failed */
        public final void a(Throwable th) {
            Throwable e = e();
            if (e == null) {
                l(th);
            } else if (th != e) {
                Object d = d();
                if (d == null) {
                    k(th);
                } else if (d instanceof Throwable) {
                    if (th != d) {
                        ArrayList<Throwable> c = c();
                        c.add(d);
                        c.add(th);
                        k(c);
                    }
                } else if (d instanceof ArrayList) {
                    ((ArrayList) d).add(th);
                } else {
                    throw new IllegalStateException(("State is " + d).toString());
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.Sw7
        public Kx7 b() {
            return this.b;
        }

        @DexIgnore
        public final ArrayList<Throwable> c() {
            return new ArrayList<>(4);
        }

        @DexIgnore
        public final Object d() {
            return this._exceptionsHolder;
        }

        @DexIgnore
        public final Throwable e() {
            return (Throwable) this._rootCause;
        }

        @DexIgnore
        public final boolean f() {
            return e() != null;
        }

        @DexIgnore
        /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean g() {
            /*
                r1 = this;
                int r0 = r1._isCompleting
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Fx7.Ci.g():boolean");
        }

        @DexIgnore
        public final boolean h() {
            return d() == Gx7.e();
        }

        @DexIgnore
        public final List<Throwable> i(Throwable th) {
            ArrayList<Throwable> arrayList;
            Object d = d();
            if (d == null) {
                arrayList = c();
            } else if (d instanceof Throwable) {
                ArrayList<Throwable> c = c();
                c.add(d);
                arrayList = c;
            } else if (d instanceof ArrayList) {
                arrayList = (ArrayList) d;
            } else {
                throw new IllegalStateException(("State is " + d).toString());
            }
            Throwable e = e();
            if (e != null) {
                arrayList.add(0, e);
            }
            if (th != null && (!Wg6.a(th, e))) {
                arrayList.add(th);
            }
            k(Gx7.e());
            return arrayList;
        }

        @DexIgnore
        @Override // com.fossil.Sw7
        public boolean isActive() {
            return e() == null;
        }

        @DexIgnore
        public final void j(boolean z) {
            this._isCompleting = z ? 1 : 0;
        }

        @DexIgnore
        public final void k(Object obj) {
            this._exceptionsHolder = obj;
        }

        @DexIgnore
        public final void l(Throwable th) {
            this._rootCause = th;
        }

        @DexIgnore
        public String toString() {
            return "Finishing[cancelling=" + f() + ", completing=" + g() + ", rootCause=" + e() + ", exceptions=" + d() + ", list=" + b() + ']';
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Lz7.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ Fx7 d;
        @DexIgnore
        public /* final */ /* synthetic */ Object e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(Lz7 lz7, Lz7 lz72, Fx7 fx7, Object obj) {
            super(lz72);
            this.d = fx7;
            this.e = obj;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Cz7
        public /* bridge */ /* synthetic */ Object g(Lz7 lz7) {
            return i(lz7);
        }

        @DexIgnore
        public Object i(Lz7 lz7) {
            if (this.d.Q() == this.e) {
                return null;
            }
            return Kz7.a();
        }
    }

    @DexIgnore
    public Fx7(boolean z) {
        this._state = z ? Gx7.c() : Gx7.d();
        this._parentHandle = null;
    }

    @DexIgnore
    public static /* synthetic */ CancellationException n0(Fx7 fx7, Throwable th, String str, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                str = null;
            }
            return fx7.m0(th, str);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: toCancellationException");
    }

    @DexIgnore
    @Override // com.mapped.Rm6
    public final Dw7 A(Hg6<? super Throwable, Cd6> hg6) {
        return j(false, true, hg6);
    }

    @DexIgnore
    @Override // com.fossil.Nx7
    public CancellationException C() {
        CancellationException cancellationException;
        Object Q = Q();
        if (Q instanceof Ci) {
            cancellationException = ((Ci) Q).e();
        } else if (Q instanceof Vu7) {
            cancellationException = ((Vu7) Q).a;
        } else if (!(Q instanceof Sw7)) {
            cancellationException = null;
        } else {
            throw new IllegalStateException(("Cannot be cancelling child in this state: " + Q).toString());
        }
        CancellationException cancellationException2 = !(cancellationException instanceof CancellationException) ? null : cancellationException;
        if (cancellationException2 != null) {
            return cancellationException2;
        }
        return new Yw7("Parent job is " + l0(Q), cancellationException, this);
    }

    @DexIgnore
    @Override // com.mapped.Rm6
    public void D(CancellationException cancellationException) {
        if (cancellationException == null) {
            cancellationException = new Yw7(x(), null, this);
        }
        u(cancellationException);
    }

    @DexIgnore
    public final void E(Ci ci, Ru7 ru7, Object obj) {
        if (Nv7.a()) {
            if (!(Q() == ci)) {
                throw new AssertionError();
            }
        }
        Ru7 a0 = a0(ru7);
        if (a0 == null || !t0(ci, a0, obj)) {
            p(H(ci, obj));
        }
    }

    @DexIgnore
    public final Throwable G(Object obj) {
        if (obj != null ? obj instanceof Throwable : true) {
            return obj != null ? (Throwable) obj : new Yw7(x(), null, this);
        }
        if (obj != null) {
            return ((Nx7) obj).C();
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.ParentJob");
    }

    @DexIgnore
    public final Object H(Ci ci, Object obj) {
        boolean f;
        Throwable K;
        if (Nv7.a()) {
            if (!(Q() == ci)) {
                throw new AssertionError();
            }
        }
        if (Nv7.a() && !(!ci.h())) {
            throw new AssertionError();
        } else if (!Nv7.a() || ci.g()) {
            Vu7 vu7 = (Vu7) (!(obj instanceof Vu7) ? null : obj);
            Throwable th = vu7 != null ? vu7.a : null;
            synchronized (ci) {
                f = ci.f();
                List<Throwable> i = ci.i(th);
                K = K(ci, i);
                if (K != null) {
                    o(K, i);
                }
            }
            Vu7 vu72 = K == null ? obj : K == th ? obj : new Vu7(K, false, 2, null);
            if (K != null) {
                if (!w(K) ? R(K) : true) {
                    if (vu72 != null) {
                        ((Vu7) vu72).b();
                    } else {
                        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.CompletedExceptionally");
                    }
                }
            }
            if (!f) {
                d0(K);
            }
            e0(vu72);
            boolean compareAndSet = b.compareAndSet(this, ci, Gx7.g(vu72));
            if (!Nv7.a() || compareAndSet) {
                z(ci, vu72);
                return vu72;
            }
            throw new AssertionError();
        } else {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public final Ru7 I(Sw7 sw7) {
        Ru7 ru7 = (Ru7) (!(sw7 instanceof Ru7) ? null : sw7);
        if (ru7 != null) {
            return ru7;
        }
        Kx7 b2 = sw7.b();
        if (b2 != null) {
            return a0(b2);
        }
        return null;
    }

    @DexIgnore
    public final Throwable J(Object obj) {
        Vu7 vu7 = (Vu7) (!(obj instanceof Vu7) ? null : obj);
        if (vu7 != null) {
            return vu7.a;
        }
        return null;
    }

    @DexIgnore
    public final Throwable K(Ci ci, List<? extends Throwable> list) {
        T t;
        T t2;
        boolean z;
        if (!list.isEmpty()) {
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                T next = it.next();
                if (!(next instanceof CancellationException)) {
                    t = next;
                    break;
                }
            }
            T t3 = t;
            if (t3 != null) {
                return t3;
            }
            Throwable th = (Throwable) list.get(0);
            if (!(th instanceof Zx7)) {
                return th;
            }
            Iterator<T> it2 = list.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t2 = null;
                    break;
                }
                T next2 = it2.next();
                T t4 = next2;
                if (t4 == th || !(t4 instanceof Zx7)) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    t2 = next2;
                    break;
                }
            }
            T t5 = t2;
            return t5 != null ? t5 : th;
        } else if (ci.f()) {
            return new Yw7(x(), null, this);
        } else {
            return null;
        }
    }

    @DexIgnore
    @Override // com.mapped.Rm6
    public final Qu7 L(Su7 su7) {
        Dw7 d = Rm6.Ai.d(this, true, false, new Ru7(this, su7), 2, null);
        if (d != null) {
            return (Qu7) d;
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.ChildHandle");
    }

    @DexIgnore
    public boolean M() {
        return true;
    }

    @DexIgnore
    public boolean N() {
        return false;
    }

    @DexIgnore
    public final Kx7 O(Sw7 sw7) {
        Kx7 b2 = sw7.b();
        if (b2 != null) {
            return b2;
        }
        if (sw7 instanceof Gw7) {
            return new Kx7();
        }
        if (sw7 instanceof Ex7) {
            h0((Ex7) sw7);
            return null;
        }
        throw new IllegalStateException(("State should have list: " + sw7).toString());
    }

    @DexIgnore
    public final Qu7 P() {
        return (Qu7) this._parentHandle;
    }

    @DexIgnore
    public final Object Q() {
        while (true) {
            Object obj = this._state;
            if (!(obj instanceof Rz7)) {
                return obj;
            }
            ((Rz7) obj).c(this);
        }
    }

    @DexIgnore
    public boolean R(Throwable th) {
        return false;
    }

    @DexIgnore
    public void S(Throwable th) {
        throw th;
    }

    @DexIgnore
    public final void T(Rm6 rm6) {
        if (Nv7.a()) {
            if (!(P() == null)) {
                throw new AssertionError();
            }
        }
        if (rm6 == null) {
            j0(Lx7.b);
            return;
        }
        rm6.start();
        Qu7 L = rm6.L(this);
        j0(L);
        if (U()) {
            L.dispose();
            j0(Lx7.b);
        }
    }

    @DexIgnore
    public final boolean U() {
        return !(Q() instanceof Sw7);
    }

    @DexIgnore
    public boolean V() {
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003d, code lost:
        if (r1 == null) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003f, code lost:
        b0(((com.fossil.Fx7.Ci) r2).b(), r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        return com.fossil.Gx7.a();
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object W(java.lang.Object r8) {
        /*
            r7 = this;
            r3 = 0
            r4 = r3
        L_0x0002:
            java.lang.Object r2 = r7.Q()
            boolean r1 = r2 instanceof com.fossil.Fx7.Ci
            if (r1 == 0) goto L_0x0055
            monitor-enter(r2)
            r0 = r2
            com.fossil.Fx7$Ci r0 = (com.fossil.Fx7.Ci) r0     // Catch:{ all -> 0x0052 }
            r1 = r0
            boolean r1 = r1.h()     // Catch:{ all -> 0x0052 }
            if (r1 == 0) goto L_0x001b
            com.fossil.Vz7 r1 = com.fossil.Gx7.f()     // Catch:{ all -> 0x0052 }
            monitor-exit(r2)
        L_0x001a:
            return r1
        L_0x001b:
            r0 = r2
            com.fossil.Fx7$Ci r0 = (com.fossil.Fx7.Ci) r0
            r1 = r0
            boolean r5 = r1.f()
            if (r8 != 0) goto L_0x0027
            if (r5 != 0) goto L_0x0030
        L_0x0027:
            if (r4 == 0) goto L_0x004d
        L_0x0029:
            r0 = r2
            com.fossil.Fx7$Ci r0 = (com.fossil.Fx7.Ci) r0
            r1 = r0
            r1.a(r4)
        L_0x0030:
            r0 = r2
            com.fossil.Fx7$Ci r0 = (com.fossil.Fx7.Ci) r0
            r1 = r0
            java.lang.Throwable r1 = r1.e()
            r4 = r5 ^ 1
            if (r4 == 0) goto L_0x00ae
        L_0x003c:
            monitor-exit(r2)
            if (r1 == 0) goto L_0x0048
            com.fossil.Fx7$Ci r2 = (com.fossil.Fx7.Ci) r2
            com.fossil.Kx7 r2 = r2.b()
            r7.b0(r2, r1)
        L_0x0048:
            com.fossil.Vz7 r1 = com.fossil.Gx7.a()
            goto L_0x001a
        L_0x004d:
            java.lang.Throwable r4 = r7.G(r8)
            goto L_0x0029
        L_0x0052:
            r1 = move-exception
            monitor-exit(r2)
            throw r1
        L_0x0055:
            boolean r1 = r2 instanceof com.fossil.Sw7
            if (r1 == 0) goto L_0x00a8
            if (r4 == 0) goto L_0x006f
        L_0x005b:
            r1 = r2
            com.fossil.Sw7 r1 = (com.fossil.Sw7) r1
            boolean r5 = r1.isActive()
            if (r5 == 0) goto L_0x0074
            boolean r1 = r7.q0(r1, r4)
            if (r1 == 0) goto L_0x0002
            com.fossil.Vz7 r1 = com.fossil.Gx7.a()
            goto L_0x001a
        L_0x006f:
            java.lang.Throwable r4 = r7.G(r8)
            goto L_0x005b
        L_0x0074:
            com.fossil.Vu7 r1 = new com.fossil.Vu7
            r5 = 0
            r6 = 2
            r1.<init>(r4, r5, r6, r3)
            java.lang.Object r1 = r7.r0(r2, r1)
            com.fossil.Vz7 r5 = com.fossil.Gx7.a()
            if (r1 == r5) goto L_0x008d
            com.fossil.Vz7 r2 = com.fossil.Gx7.b()
            if (r1 != r2) goto L_0x001a
            goto L_0x0002
        L_0x008d:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "Cannot happen in "
            r1.append(r3)
            r1.append(r2)
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = r1.toString()
            java.lang.String r1 = r1.toString()
            r2.<init>(r1)
            throw r2
        L_0x00a8:
            com.fossil.Vz7 r1 = com.fossil.Gx7.f()
            goto L_0x001a
        L_0x00ae:
            r1 = r3
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Fx7.W(java.lang.Object):java.lang.Object");
    }

    @DexIgnore
    public final Object X(Object obj) {
        Object r0;
        do {
            r0 = r0(Q(), obj);
            if (r0 == Gx7.a()) {
                throw new IllegalStateException("Job " + this + " is already complete or completing, but is being completed with " + obj, J(obj));
            }
        } while (r0 == Gx7.b());
        return r0;
    }

    @DexIgnore
    public final Ex7<?> Y(Hg6<? super Throwable, Cd6> hg6, boolean z) {
        Zw7 zw7 = null;
        boolean z2 = true;
        if (z) {
            if (hg6 instanceof Zw7) {
                zw7 = hg6;
            }
            Zw7 zw72 = zw7;
            if (zw72 != null) {
                if (Nv7.a()) {
                    if (zw72.e != this) {
                        z2 = false;
                    }
                    if (!z2) {
                        throw new AssertionError();
                    }
                }
                if (zw72 != null) {
                    return zw72;
                }
            }
            return new Vw7(this, hg6);
        }
        if (hg6 instanceof Ex7) {
            zw7 = hg6;
        }
        Ex7<?> ex7 = zw7;
        if (ex7 != null) {
            if (Nv7.a()) {
                if (ex7.e != this || (ex7 instanceof Zw7)) {
                    z2 = false;
                }
                if (!z2) {
                    throw new AssertionError();
                }
            }
            if (ex7 != null) {
                return ex7;
            }
        }
        return new Ww7(this, hg6);
    }

    @DexIgnore
    public String Z() {
        return Ov7.a(this);
    }

    @DexIgnore
    public final Ru7 a0(Lz7 lz7) {
        while (lz7.q()) {
            lz7 = lz7.n();
        }
        Lz7 lz72 = lz7;
        while (true) {
            lz72 = lz72.m();
            if (!lz72.q()) {
                if (lz72 instanceof Ru7) {
                    return (Ru7) lz72;
                }
                if (lz72 instanceof Kx7) {
                    return null;
                }
            }
        }
    }

    @DexIgnore
    public final void b0(Kx7 kx7, Throwable th) {
        d0(th);
        Object l = kx7.l();
        if (l != null) {
            Av7 av7 = null;
            Lz7 lz7 = (Lz7) l;
            while (!Wg6.a(lz7, kx7)) {
                if (lz7 instanceof Zw7) {
                    Ex7 ex7 = (Ex7) lz7;
                    try {
                        ex7.w(th);
                    } catch (Throwable th2) {
                        if (av7 != null) {
                            Tk7.a(av7, th2);
                            if (av7 != null) {
                            }
                        }
                        av7 = new Av7("Exception in completion handler " + ex7 + " for " + this, th2);
                        Cd6 cd6 = Cd6.a;
                    }
                }
                lz7 = lz7.m();
                av7 = av7;
            }
            if (av7 != null) {
                S(av7);
            }
            w(th);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public final void c0(Kx7 kx7, Throwable th) {
        Object l = kx7.l();
        if (l != null) {
            Av7 av7 = null;
            Lz7 lz7 = (Lz7) l;
            while (!Wg6.a(lz7, kx7)) {
                if (lz7 instanceof Ex7) {
                    Ex7 ex7 = (Ex7) lz7;
                    try {
                        ex7.w(th);
                    } catch (Throwable th2) {
                        if (av7 != null) {
                            Tk7.a(av7, th2);
                            if (av7 != null) {
                            }
                        }
                        av7 = new Av7("Exception in completion handler " + ex7 + " for " + this, th2);
                        Cd6 cd6 = Cd6.a;
                    }
                }
                lz7 = lz7.m();
                av7 = av7;
            }
            if (av7 != null) {
                S(av7);
                return;
            }
            return;
        }
        throw new Rc6("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    public void d0(Throwable th) {
    }

    @DexIgnore
    public void e0(Object obj) {
    }

    @DexIgnore
    public void f0() {
    }

    @DexIgnore
    @Override // com.mapped.Af6
    public <R> R fold(R r, Coroutine<? super R, ? super Af6.Bi, ? extends R> coroutine) {
        return (R) Rm6.Ai.b(this, r, coroutine);
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v1, types: [com.fossil.Rw7] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void g0(com.fossil.Gw7 r3) {
        /*
            r2 = this;
            com.fossil.Kx7 r0 = new com.fossil.Kx7
            r0.<init>()
            boolean r1 = r3.isActive()
            if (r1 == 0) goto L_0x0011
        L_0x000b:
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r1 = com.fossil.Fx7.b
            r1.compareAndSet(r2, r3, r0)
            return
        L_0x0011:
            com.fossil.Rw7 r1 = new com.fossil.Rw7
            r1.<init>(r0)
            r0 = r1
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Fx7.g0(com.fossil.Gw7):void");
    }

    @DexIgnore
    @Override // com.mapped.Af6, com.mapped.Af6.Bi
    public <E extends Af6.Bi> E get(Af6.Ci<E> ci) {
        return (E) Rm6.Ai.c(this, ci);
    }

    @DexIgnore
    @Override // com.mapped.Af6.Bi
    public final Af6.Ci<?> getKey() {
        return Rm6.r;
    }

    @DexIgnore
    public final void h0(Ex7<?> ex7) {
        ex7.h(new Kx7());
        b.compareAndSet(this, ex7, ex7.m());
    }

    @DexIgnore
    public final void i0(Ex7<?> ex7) {
        Object Q;
        do {
            Q = Q();
            if (Q instanceof Ex7) {
                if (Q != ex7) {
                    return;
                }
            } else if ((Q instanceof Sw7) && ((Sw7) Q).b() != null) {
                ex7.r();
                return;
            } else {
                return;
            }
        } while (!b.compareAndSet(this, Q, Gx7.c()));
    }

    @DexIgnore
    @Override // com.mapped.Rm6
    public boolean isActive() {
        Object Q = Q();
        return (Q instanceof Sw7) && ((Sw7) Q).isActive();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0070, code lost:
        r3 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0080, code lost:
        r1 = r3;
     */
    @DexIgnore
    @Override // com.mapped.Rm6
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.Dw7 j(boolean r9, boolean r10, com.mapped.Hg6<? super java.lang.Throwable, com.mapped.Cd6> r11) {
        /*
            r8 = this;
            r4 = 0
            r3 = r4
        L_0x0002:
            java.lang.Object r2 = r8.Q()
            boolean r1 = r2 instanceof com.fossil.Gw7
            if (r1 == 0) goto L_0x0028
            r1 = r2
            com.fossil.Gw7 r1 = (com.fossil.Gw7) r1
            boolean r5 = r1.isActive()
            if (r5 == 0) goto L_0x0024
            if (r3 == 0) goto L_0x001f
            r1 = r3
        L_0x0016:
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r3 = com.fossil.Fx7.b
            boolean r2 = r3.compareAndSet(r8, r2, r1)
            if (r2 == 0) goto L_0x0098
        L_0x001e:
            return r1
        L_0x001f:
            com.fossil.Ex7 r1 = r8.Y(r11, r9)
            goto L_0x0016
        L_0x0024:
            r8.g0(r1)
            goto L_0x0002
        L_0x0028:
            boolean r1 = r2 instanceof com.fossil.Sw7
            if (r1 == 0) goto L_0x00a0
            r1 = r2
            com.fossil.Sw7 r1 = (com.fossil.Sw7) r1
            com.fossil.Kx7 r7 = r1.b()
            if (r7 != 0) goto L_0x0045
            if (r2 == 0) goto L_0x003d
            com.fossil.Ex7 r2 = (com.fossil.Ex7) r2
            r8.h0(r2)
            goto L_0x0002
        L_0x003d:
            com.mapped.Rc6 r1 = new com.mapped.Rc6
            java.lang.String r2 = "null cannot be cast to non-null type kotlinx.coroutines.JobNode<*>"
            r1.<init>(r2)
            throw r1
        L_0x0045:
            com.fossil.Lx7 r5 = com.fossil.Lx7.b
            if (r9 == 0) goto L_0x008d
            boolean r1 = r2 instanceof com.fossil.Fx7.Ci
            if (r1 == 0) goto L_0x008d
            monitor-enter(r2)
            r0 = r2
            com.fossil.Fx7$Ci r0 = (com.fossil.Fx7.Ci) r0     // Catch:{ all -> 0x008a }
            r1 = r0
            java.lang.Throwable r6 = r1.e()     // Catch:{ all -> 0x008a }
            if (r6 == 0) goto L_0x0066
            boolean r1 = r11 instanceof com.fossil.Ru7     // Catch:{ all -> 0x008a }
            if (r1 == 0) goto L_0x007d
            r0 = r2
            com.fossil.Fx7$Ci r0 = (com.fossil.Fx7.Ci) r0     // Catch:{ all -> 0x008a }
            r1 = r0
            boolean r1 = r1.g()     // Catch:{ all -> 0x008a }
            if (r1 != 0) goto L_0x007d
        L_0x0066:
            if (r3 == 0) goto L_0x0072
            r1 = r3
        L_0x0069:
            boolean r3 = r8.n(r2, r7, r1)     // Catch:{ all -> 0x008a }
            if (r3 != 0) goto L_0x0077
            monitor-exit(r2)
            r3 = r1
            goto L_0x0002
        L_0x0072:
            com.fossil.Ex7 r1 = r8.Y(r11, r9)
            goto L_0x0069
        L_0x0077:
            if (r6 != 0) goto L_0x007b
            monitor-exit(r2)
            goto L_0x001e
        L_0x007b:
            r3 = r1
            r5 = r1
        L_0x007d:
            com.mapped.Cd6 r1 = com.mapped.Cd6.a
            monitor-exit(r2)
            r1 = r3
        L_0x0081:
            if (r6 == 0) goto L_0x0090
            if (r10 == 0) goto L_0x0088
            r11.invoke(r6)
        L_0x0088:
            r1 = r5
            goto L_0x001e
        L_0x008a:
            r1 = move-exception
            monitor-exit(r2)
            throw r1
        L_0x008d:
            r6 = r4
            r1 = r3
            goto L_0x0081
        L_0x0090:
            if (r1 == 0) goto L_0x009b
        L_0x0092:
            boolean r2 = r8.n(r2, r7, r1)
            if (r2 != 0) goto L_0x001e
        L_0x0098:
            r3 = r1
            goto L_0x0002
        L_0x009b:
            com.fossil.Ex7 r1 = r8.Y(r11, r9)
            goto L_0x0092
        L_0x00a0:
            if (r10 == 0) goto L_0x00b0
            boolean r1 = r2 instanceof com.fossil.Vu7
            if (r1 != 0) goto L_0x00b4
            r1 = r4
        L_0x00a7:
            com.fossil.Vu7 r1 = (com.fossil.Vu7) r1
            if (r1 == 0) goto L_0x00ad
            java.lang.Throwable r4 = r1.a
        L_0x00ad:
            r11.invoke(r4)
        L_0x00b0:
            com.fossil.Lx7 r1 = com.fossil.Lx7.b
            goto L_0x001e
        L_0x00b4:
            r1 = r2
            goto L_0x00a7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Fx7.j(boolean, boolean, com.mapped.Hg6):com.fossil.Dw7");
    }

    @DexIgnore
    public final void j0(Qu7 qu7) {
        this._parentHandle = qu7;
    }

    @DexIgnore
    @Override // com.mapped.Rm6
    public final CancellationException k() {
        Object Q = Q();
        if (Q instanceof Ci) {
            Throwable e = ((Ci) Q).e();
            if (e != null) {
                CancellationException m0 = m0(e, Ov7.a(this) + " is cancelling");
                if (m0 != null) {
                    return m0;
                }
            }
            throw new IllegalStateException(("Job is still new or active: " + this).toString());
        } else if (Q instanceof Sw7) {
            throw new IllegalStateException(("Job is still new or active: " + this).toString());
        } else if (Q instanceof Vu7) {
            return n0(this, ((Vu7) Q).a, null, 1, null);
        } else {
            return new Yw7(Ov7.a(this) + " has completed normally", null, this);
        }
    }

    @DexIgnore
    public final int k0(Object obj) {
        if (obj instanceof Gw7) {
            if (((Gw7) obj).isActive()) {
                return 0;
            }
            if (!b.compareAndSet(this, obj, Gx7.c())) {
                return -1;
            }
            f0();
            return 1;
        } else if (!(obj instanceof Rw7)) {
            return 0;
        } else {
            if (!b.compareAndSet(this, obj, ((Rw7) obj).b())) {
                return -1;
            }
            f0();
            return 1;
        }
    }

    @DexIgnore
    public final String l0(Object obj) {
        if (!(obj instanceof Ci)) {
            return obj instanceof Sw7 ? !((Sw7) obj).isActive() ? "New" : "Active" : obj instanceof Vu7 ? AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_CANCELLED : AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_COMPLETED;
        }
        Ci ci = (Ci) obj;
        return ci.f() ? "Cancelling" : ci.g() ? "Completing" : "Active";
    }

    @DexIgnore
    @Override // com.fossil.Su7
    public final void m(Nx7 nx7) {
        t(nx7);
    }

    @DexIgnore
    public final CancellationException m0(Throwable th, String str) {
        CancellationException cancellationException = (CancellationException) (!(th instanceof CancellationException) ? null : th);
        if (cancellationException != null) {
            return cancellationException;
        }
        if (str == null) {
            str = x();
        }
        return new Yw7(str, th, this);
    }

    @DexIgnore
    @Override // com.mapped.Af6
    public Af6 minusKey(Af6.Ci<?> ci) {
        return Rm6.Ai.e(this, ci);
    }

    @DexIgnore
    public final boolean n(Object obj, Kx7 kx7, Ex7<?> ex7) {
        int v;
        Di di = new Di(ex7, ex7, this, obj);
        do {
            v = kx7.n().v(ex7, kx7, di);
            if (v == 1) {
                return true;
            }
        } while (v != 2);
        return false;
    }

    @DexIgnore
    public final void o(Throwable th, List<? extends Throwable> list) {
        if (list.size() > 1) {
            Set newSetFromMap = Collections.newSetFromMap(new IdentityHashMap(list.size()));
            Throwable k = !Nv7.d() ? th : Uz7.k(th);
            for (Throwable th2 : list) {
                if (Nv7.d()) {
                    th2 = Uz7.k(th2);
                }
                if (th2 != th && th2 != k && !(th2 instanceof CancellationException) && newSetFromMap.add(th2)) {
                    Tk7.a(th, th2);
                }
            }
        }
    }

    @DexIgnore
    public final String o0() {
        return Z() + '{' + l0(Q()) + '}';
    }

    @DexIgnore
    public void p(Object obj) {
    }

    @DexIgnore
    public final boolean p0(Sw7 sw7, Object obj) {
        if (Nv7.a()) {
            if (!((sw7 instanceof Gw7) || (sw7 instanceof Ex7))) {
                throw new AssertionError();
            }
        }
        if (Nv7.a() && !(!(obj instanceof Vu7))) {
            throw new AssertionError();
        } else if (!b.compareAndSet(this, sw7, Gx7.g(obj))) {
            return false;
        } else {
            d0(null);
            e0(obj);
            z(sw7, obj);
            return true;
        }
    }

    @DexIgnore
    @Override // com.mapped.Af6
    public Af6 plus(Af6 af6) {
        return Rm6.Ai.f(this, af6);
    }

    @DexIgnore
    public final Object q(Xe6<Object> xe6) {
        Object Q;
        do {
            Q = Q();
            if (!(Q instanceof Sw7)) {
                if (!(Q instanceof Vu7)) {
                    return Gx7.h(Q);
                }
                Throwable th = ((Vu7) Q).a;
                if (!Nv7.d()) {
                    throw th;
                } else if (!(xe6 instanceof Do7)) {
                    throw th;
                } else {
                    throw Uz7.a(th, (Do7) xe6);
                }
            }
        } while (k0(Q) < 0);
        return r(xe6);
    }

    @DexIgnore
    public final boolean q0(Sw7 sw7, Throwable th) {
        if (Nv7.a() && !(!(sw7 instanceof Ci))) {
            throw new AssertionError();
        } else if (!Nv7.a() || sw7.isActive()) {
            Kx7 O = O(sw7);
            if (O == null) {
                return false;
            }
            if (!b.compareAndSet(this, sw7, new Ci(O, false, th))) {
                return false;
            }
            b0(O, th);
            return true;
        } else {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public final /* synthetic */ Object r(Xe6<Object> xe6) {
        Ai ai = new Ai(Xn7.c(xe6), this);
        Nu7.a(ai, A(new Px7(this, ai)));
        Object t = ai.t();
        if (t == Yn7.d()) {
            Go7.c(xe6);
        }
        return t;
    }

    @DexIgnore
    public final Object r0(Object obj, Object obj2) {
        return !(obj instanceof Sw7) ? Gx7.a() : (((obj instanceof Gw7) || (obj instanceof Ex7)) && !(obj instanceof Ru7) && !(obj2 instanceof Vu7)) ? !p0((Sw7) obj, obj2) ? Gx7.b() : obj2 : s0((Sw7) obj, obj2);
    }

    @DexIgnore
    public final boolean s(Throwable th) {
        return t(th);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0068, code lost:
        if (r1 == null) goto L_0x006d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x006a, code lost:
        b0(r3, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x006d, code lost:
        r0 = I(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0071, code lost:
        if (r0 == null) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0077, code lost:
        if (t0(r2, r0, r7) == false) goto L_0x0087;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return com.fossil.Gx7.b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return H(r2, r7);
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object s0(com.fossil.Sw7 r6, java.lang.Object r7) {
        /*
            r5 = this;
            r1 = 0
            com.fossil.Kx7 r3 = r5.O(r6)
            if (r3 == 0) goto L_0x008c
            boolean r0 = r6 instanceof com.fossil.Fx7.Ci
            if (r0 != 0) goto L_0x001e
            r0 = r1
        L_0x000c:
            com.fossil.Fx7$Ci r0 = (com.fossil.Fx7.Ci) r0
            if (r0 == 0) goto L_0x0020
            r2 = r0
        L_0x0011:
            monitor-enter(r2)
            boolean r0 = r2.g()     // Catch:{ all -> 0x0082 }
            if (r0 == 0) goto L_0x0028
            com.fossil.Vz7 r0 = com.fossil.Gx7.a()     // Catch:{ all -> 0x0082 }
            monitor-exit(r2)
        L_0x001d:
            return r0
        L_0x001e:
            r0 = r6
            goto L_0x000c
        L_0x0020:
            com.fossil.Fx7$Ci r0 = new com.fossil.Fx7$Ci
            r2 = 0
            r0.<init>(r3, r2, r1)
            r2 = r0
            goto L_0x0011
        L_0x0028:
            r0 = 1
            r2.j(r0)
            if (r2 == r6) goto L_0x003c
            java.util.concurrent.atomic.AtomicReferenceFieldUpdater r0 = com.fossil.Fx7.b
            boolean r0 = r0.compareAndSet(r5, r6, r2)
            if (r0 != 0) goto L_0x003c
            com.fossil.Vz7 r0 = com.fossil.Gx7.b()
            monitor-exit(r2)
            goto L_0x001d
        L_0x003c:
            boolean r0 = com.fossil.Nv7.a()
            if (r0 == 0) goto L_0x004a
            boolean r0 = r2.h()
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x007c
        L_0x004a:
            boolean r4 = r2.f()
            boolean r0 = r7 instanceof com.fossil.Vu7
            if (r0 != 0) goto L_0x0085
            r0 = r1
        L_0x0053:
            com.fossil.Vu7 r0 = (com.fossil.Vu7) r0
            if (r0 == 0) goto L_0x005c
            java.lang.Throwable r0 = r0.a
            r2.a(r0)
        L_0x005c:
            java.lang.Throwable r0 = r2.e()
            r4 = r4 ^ 1
            if (r4 == 0) goto L_0x0065
            r1 = r0
        L_0x0065:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
            monitor-exit(r2)
            if (r1 == 0) goto L_0x006d
            r5.b0(r3, r1)
        L_0x006d:
            com.fossil.Ru7 r0 = r5.I(r6)
            if (r0 == 0) goto L_0x0087
            boolean r0 = r5.t0(r2, r0, r7)
            if (r0 == 0) goto L_0x0087
            com.fossil.Vz7 r0 = com.fossil.Gx7.b
            goto L_0x001d
        L_0x007c:
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x0082:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0085:
            r0 = r7
            goto L_0x0053
        L_0x0087:
            java.lang.Object r0 = r5.H(r2, r7)
            goto L_0x001d
        L_0x008c:
            com.fossil.Vz7 r0 = com.fossil.Gx7.b()
            goto L_0x001d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Fx7.s0(com.fossil.Sw7, java.lang.Object):java.lang.Object");
    }

    @DexIgnore
    @Override // com.mapped.Rm6
    public final boolean start() {
        int k0;
        do {
            k0 = k0(Q());
            if (k0 == 0) {
                return false;
            }
        } while (k0 != 1);
        return true;
    }

    @DexIgnore
    public final boolean t(Object obj) {
        boolean z;
        Object a2 = Gx7.a();
        if (N() && (a2 = v(obj)) == Gx7.b) {
            return true;
        }
        if (a2 == Gx7.a()) {
            a2 = W(obj);
        }
        if (a2 == Gx7.a()) {
            z = true;
        } else if (a2 == Gx7.b) {
            z = true;
        } else if (a2 == Gx7.f()) {
            z = false;
        } else {
            p(a2);
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final boolean t0(Ci ci, Ru7 ru7, Object obj) {
        while (Rm6.Ai.d(ru7.f, false, false, new Bi(this, ci, ru7, obj), 1, null) == Lx7.b) {
            ru7 = a0(ru7);
            if (ru7 == null) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public String toString() {
        return o0() + '@' + Ov7.b(this);
    }

    @DexIgnore
    public void u(Throwable th) {
        t(th);
    }

    @DexIgnore
    public final Object v(Object obj) {
        Object r0;
        do {
            Object Q = Q();
            if (!(Q instanceof Sw7) || ((Q instanceof Ci) && ((Ci) Q).g())) {
                return Gx7.a();
            }
            r0 = r0(Q, new Vu7(G(obj), false, 2, null));
        } while (r0 == Gx7.b());
        return r0;
    }

    @DexIgnore
    public final boolean w(Throwable th) {
        if (V()) {
            return true;
        }
        boolean z = th instanceof CancellationException;
        Qu7 P = P();
        return (P == null || P == Lx7.b) ? z : P.c(th) || z;
    }

    @DexIgnore
    public String x() {
        return "Job was cancelled";
    }

    @DexIgnore
    public boolean y(Throwable th) {
        if (th instanceof CancellationException) {
            return true;
        }
        return t(th) && M();
    }

    @DexIgnore
    public final void z(Sw7 sw7, Object obj) {
        Throwable th = null;
        Qu7 P = P();
        if (P != null) {
            P.dispose();
            j0(Lx7.b);
        }
        Vu7 vu7 = (Vu7) (!(obj instanceof Vu7) ? null : obj);
        if (vu7 != null) {
            th = vu7.a;
        }
        if (sw7 instanceof Ex7) {
            try {
                ((Ex7) sw7).w(th);
            } catch (Throwable th2) {
                S(new Av7("Exception in completion handler " + sw7 + " for " + this, th2));
            }
        } else {
            Kx7 b2 = sw7.b();
            if (b2 != null) {
                c0(b2, th);
            }
        }
    }
}
