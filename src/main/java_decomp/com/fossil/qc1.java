package com.fossil;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qc1 implements Mb1 {
    @DexIgnore
    public /* final */ Mb1 b;
    @DexIgnore
    public /* final */ Mb1 c;

    @DexIgnore
    public Qc1(Mb1 mb1, Mb1 mb12) {
        this.b = mb1;
        this.c = mb12;
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
        this.c.a(messageDigest);
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public boolean equals(Object obj) {
        if (!(obj instanceof Qc1)) {
            return false;
        }
        Qc1 qc1 = (Qc1) obj;
        return this.b.equals(qc1.b) && this.c.equals(qc1.c);
    }

    @DexIgnore
    @Override // com.fossil.Mb1
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "DataCacheKey{sourceKey=" + this.b + ", signature=" + this.c + '}';
    }
}
