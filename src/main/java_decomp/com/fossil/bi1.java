package com.fossil;

import android.content.Context;
import android.util.Log;
import com.fossil.Yh1;
import com.mapped.W6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Bi1 implements Zh1 {
    @DexIgnore
    @Override // com.fossil.Zh1
    public Yh1 a(Context context, Yh1.Ai ai) {
        boolean z = W6.a(context, "android.permission.ACCESS_NETWORK_STATE") == 0;
        if (Log.isLoggable("ConnectivityMonitor", 3)) {
            Log.d("ConnectivityMonitor", z ? "ACCESS_NETWORK_STATE permission granted, registering connectivity monitor" : "ACCESS_NETWORK_STATE permission missing, cannot register connectivity monitor");
        }
        return z ? new Ai1(context, ai) : new Fi1();
    }
}
