package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class Fg2 implements Callable {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Eg2 c;

    @DexIgnore
    public Fg2(boolean z, String str, Eg2 eg2) {
        this.a = z;
        this.b = str;
        this.c = eg2;
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public final Object call() {
        return Dg2.b(this.a, this.b, this.c);
    }
}
