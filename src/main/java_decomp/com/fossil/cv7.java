package com.fossil;

import com.mapped.Af6;
import com.mapped.Il6;

public final class Cv7 {
    public static final boolean a;

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        if (r0.equals("on") == false) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0053, code lost:
        if (r0.equals("") != false) goto L_0x0008;
     */
    /*
    static {
        /*
            java.lang.String r0 = "kotlinx.coroutines.scheduler"
            java.lang.String r0 = com.fossil.Wz7.d(r0)
            if (r0 != 0) goto L_0x000c
        L_0x0008:
            r0 = 1
        L_0x0009:
            com.fossil.Cv7.a = r0
            return
        L_0x000c:
            int r1 = r0.hashCode()
            if (r1 == 0) goto L_0x004d
            r2 = 3551(0xddf, float:4.976E-42)
            if (r1 == r2) goto L_0x0025
            r2 = 109935(0x1ad6f, float:1.54052E-40)
            if (r1 != r2) goto L_0x002d
            java.lang.String r1 = "off"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x002d
            r0 = 0
            goto L_0x0009
        L_0x0025:
            java.lang.String r1 = "on"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x0008
        L_0x002d:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "System property 'kotlinx.coroutines.scheduler' has unrecognized value '"
            r1.append(r2)
            r1.append(r0)
            r0 = 39
            r1.append(r0)
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = r1.toString()
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x004d:
            java.lang.String r1 = ""
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x002d
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Cv7.<clinit>():void");
    }
    */

    public static final Dv7 a() {
        return a ? H08.i : Tu7.e;
    }

    public static final String b(Af6 af6) {
        String str;
        if (!Nv7.c()) {
            return null;
        }
        Gv7 gv7 = (Gv7) af6.get(Gv7.c);
        if (gv7 == null) {
            return null;
        }
        Hv7 hv7 = (Hv7) af6.get(Hv7.c);
        if (hv7 == null || (str = hv7.M()) == null) {
            str = "coroutine";
        }
        return str + '#' + gv7.M();
    }

    public static final Af6 c(Il6 il6, Af6 af6) {
        Af6 plus = il6.h().plus(af6);
        Af6 plus2 = Nv7.c() ? plus.plus(new Gv7(Nv7.b().incrementAndGet())) : plus;
        return (plus == Bw7.a() || plus.get(Rn7.p) != null) ? plus2 : plus2.plus(Bw7.a());
    }
}
