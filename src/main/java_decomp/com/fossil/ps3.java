package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ps3 extends Hl2 implements Ms3 {
    @DexIgnore
    public Ps3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.signin.internal.ISignInService");
    }

    @DexIgnore
    @Override // com.fossil.Ms3
    public final void F0(Jc2 jc2, int i, boolean z) throws RemoteException {
        Parcel d = d();
        Il2.c(d, jc2);
        d.writeInt(i);
        Il2.a(d, z);
        i(9, d);
    }

    @DexIgnore
    @Override // com.fossil.Ms3
    public final void K(int i) throws RemoteException {
        Parcel d = d();
        d.writeInt(i);
        i(7, d);
    }

    @DexIgnore
    @Override // com.fossil.Ms3
    public final void Q1(Ss3 ss3, Ks3 ks3) throws RemoteException {
        Parcel d = d();
        Il2.d(d, ss3);
        Il2.c(d, ks3);
        i(12, d);
    }
}
