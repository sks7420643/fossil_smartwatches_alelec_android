package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Pw5;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O76 extends BaseFragment implements U86 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public G37<Za5> g;
    @DexIgnore
    public T86 h;
    @DexIgnore
    public Pw5 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final O76 a() {
            return new O76();
        }

        @DexIgnore
        public final String b() {
            return O76.k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Pw5.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ O76 a;

        @DexIgnore
        public Bi(O76 o76) {
            this.a = o76;
        }

        @DexIgnore
        @Override // com.fossil.Pw5.Bi
        public void a(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b = O76.l.b();
            local.d(b, "onItemClick position=" + i);
            Pw5 pw5 = this.a.i;
            if (pw5 != null) {
                Pw5.Ci ci = pw5.h().get(i);
                if (ci.b() == Pw5.Ci.Aii.TYPE_VALUE) {
                    SecondTimezoneSetting c = ci.c();
                    if (c != null) {
                        c.setTimezoneOffset(ConversionUtils.INSTANCE.getTimezoneRawOffsetById(c.getTimeZoneId()));
                        Intent intent = new Intent();
                        intent.putExtra("SECOND_TIMEZONE", c);
                        FragmentActivity activity = this.a.getActivity();
                        if (activity != null) {
                            activity.setResult(-1, intent);
                        }
                        FragmentActivity activity2 = this.a.getActivity();
                        if (activity2 != null) {
                            activity2.finish();
                            return;
                        }
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements RecyclerViewAlphabetIndex.b {
        @DexIgnore
        public /* final */ /* synthetic */ O76 a;
        @DexIgnore
        public /* final */ /* synthetic */ Za5 b;

        @DexIgnore
        public Ci(O76 o76, Za5 za5) {
            this.a = o76;
            this.b = za5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex.b
        public void a(View view, int i, String str) {
            Wg6.c(view, "view");
            Wg6.c(str, "character");
            Pw5 pw5 = this.a.i;
            Integer valueOf = pw5 != null ? Integer.valueOf(pw5.i(str)) : null;
            if (valueOf != null && valueOf.intValue() != -1) {
                RecyclerView recyclerView = this.b.z;
                Wg6.b(recyclerView, "binding.timezoneRecyclerView");
                RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                if (layoutManager != null) {
                    ((LinearLayoutManager) layoutManager).D2(valueOf.intValue(), 0);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ O76 b;
        @DexIgnore
        public /* final */ /* synthetic */ Za5 c;

        @DexIgnore
        public Di(O76 o76, Za5 za5) {
            this.b = o76;
            this.c = za5;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            FLogger.INSTANCE.getLocal().d(O76.l.b(), "afterTextChanged s=" + ((Object) editable));
            Pw5 pw5 = this.b.i;
            if (pw5 != null) {
                String valueOf = String.valueOf(editable);
                int length = valueOf.length() - 1;
                boolean z = false;
                int i = 0;
                while (i <= length) {
                    boolean z2 = valueOf.charAt(!z ? i : length) <= ' ';
                    if (!z) {
                        if (!z2) {
                            z = true;
                        } else {
                            i++;
                        }
                    } else if (!z2) {
                        break;
                    } else {
                        length--;
                    }
                }
                pw5.g(valueOf.subSequence(i, length + 1).toString());
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b2 = O76.l.b();
            local.d(b2, "beforeTextChanged s=" + charSequence + " start=" + i + " count=" + i2 + " after=" + i3);
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String b2 = O76.l.b();
            local.d(b2, "onTextChanged s=" + charSequence + " start=" + i + " before=" + i2 + " count=" + i3);
            ImageView imageView = this.c.r;
            Wg6.b(imageView, "binding.clearIv");
            imageView.setVisibility(!TextUtils.isEmpty(charSequence) ? 0 : 4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ O76 b;

        @DexIgnore
        public Ei(O76 o76) {
            this.b = o76;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Za5 b;

        @DexIgnore
        public Fi(Za5 za5) {
            this.b = za5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.b.y.setText("");
        }
    }

    /*
    static {
        String simpleName = O76.class.getSimpleName();
        Wg6.b(simpleName, "SearchSecondTimezoneFrag\u2026nt::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean F6() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.U86
    public void G2(List<SecondTimezoneSetting> list) {
        Wg6.c(list, "secondTimezones");
        Pw5 pw5 = this.i;
        if (pw5 != null) {
            pw5.k(list);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Gq4
    public /* bridge */ /* synthetic */ void M5(T86 t86) {
        M6(t86);
    }

    @DexIgnore
    public void M6(T86 t86) {
        Wg6.c(t86, "presenter");
        this.h = t86;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.c(layoutInflater, "inflater");
        Za5 za5 = (Za5) Aq0.f(layoutInflater, 2131558620, viewGroup, false, A6());
        Pw5 pw5 = new Pw5();
        pw5.j(new Bi(this));
        this.i = pw5;
        RecyclerView recyclerView = za5.z;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.i);
        RecyclerViewAlphabetIndex recyclerViewAlphabetIndex = za5.x;
        recyclerViewAlphabetIndex.a();
        recyclerViewAlphabetIndex.setOnSectionIndexClickListener(new Ci(this, za5));
        za5.y.addTextChangedListener(new Di(this, za5));
        za5.q.setOnClickListener(new Ei(this));
        za5.r.setOnClickListener(new Fi(za5));
        this.g = new G37<>(this, za5);
        Wg6.b(za5, "binding");
        return za5.n();
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        T86 t86 = this.h;
        if (t86 != null) {
            t86.m();
            super.onPause();
            return;
        }
        Wg6.n("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        T86 t86 = this.h;
        if (t86 != null) {
            t86.l();
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.U86
    public void v5(String str) {
        FlexibleTextView flexibleTextView;
        Wg6.c(str, "cityName");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = k;
        local.d(str2, "cityName=" + str);
        G37<Za5> g37 = this.g;
        if (g37 != null) {
            Za5 a2 = g37.a();
            if (a2 != null && (flexibleTextView = a2.t) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        Wg6.n("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public void v6() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }
}
