package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.HashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ko extends Zj {
    @DexIgnore
    public long T;
    @DexIgnore
    public long U;
    @DexIgnore
    public String V;
    @DexIgnore
    public String W;
    @DexIgnore
    public Zk1 X;
    @DexIgnore
    public int Y;
    @DexIgnore
    public boolean Z;
    @DexIgnore
    public /* final */ String a0;

    @DexIgnore
    public Ko(K5 k5, I60 i60, byte[] bArr, String str) {
        super(k5, i60, Yp.h, false, Ke.b.b(k5.x, Ob.d), bArr, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.a0 = str;
        String firmwareVersion = i60.a().getFirmwareVersion();
        this.V = firmwareVersion.length() == 0 ? "unknown" : firmwareVersion;
        this.W = "unknown";
        this.X = new Zk1(k5.C(), k5.x, "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
        this.Z = true;
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro
    public void B() {
        String firmwareVersion = this.x.a().getFirmwareVersion();
        this.V = firmwareVersion;
        String str = this.a0;
        if (str == null || !Vt7.j(firmwareVersion, str, true)) {
            if (Q3.f.a()) {
                this.w.y = true;
                a(new Qk(this));
            }
            if (Q3.f.f(this.x.a())) {
                Lp.h(this, new Af(this.w, this.x, this.S, false, 23131, 0.001f, this.z), new An(this), new Mn(this), new Yn(this), null, null, 48, null);
            } else {
                super.B();
            }
        } else {
            this.W = this.V;
            this.X = this.x.a();
            this.Z = false;
            l(Nr.a(this.v, null, Zq.b, null, null, 13));
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro, com.fossil.Mj
    public JSONObject C() {
        JSONObject C = super.C();
        Jd0 jd0 = Jd0.j5;
        Object obj = this.a0;
        if (obj == null) {
            obj = JSONObject.NULL;
        }
        return G80.k(C, jd0, obj);
    }

    @DexIgnore
    @Override // com.fossil.Lp, com.fossil.Ro
    public JSONObject E() {
        return G80.k(G80.k(G80.k(super.E(), Jd0.q4, Long.valueOf(Math.max(this.U - this.T, 0L))), Jd0.r4, this.V), Jd0.s4, this.W);
    }

    @DexIgnore
    @Override // com.fossil.Ro
    public Mv O() {
        return new Jv(this.D, this.w);
    }

    @DexIgnore
    @Override // com.fossil.Ro
    public void P() {
        if (Q3.f.a()) {
            Lp.i(this, new Nv(this.w), Dl.b, Ql.b, null, new Cm(this), Pm.b, 8, null);
        } else {
            U();
        }
    }

    @DexIgnore
    public final Zk1 T() {
        return this.X;
    }

    @DexIgnore
    public final void U() {
        int i = this.Y;
        if (i < 2) {
            this.Y = i + 1;
            HashMap i2 = Zm7.i(Hl7.a(Ym.c, Boolean.valueOf(Q3.f.c(true))), Hl7.a(Ym.b, 55000L));
            if (this.T == 0) {
                this.T = System.currentTimeMillis();
            }
            Lp.h(this, new Fh(this.w, this.x, i2, this.z), new Gj(this), new Sj(this), null, null, Ek.b, 24, null);
            return;
        }
        this.Z = true;
        l(this.v);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void e(F5 f5) {
        if (Ti.a[f5.ordinal()] == 1) {
            Fs fs = this.b;
            if (fs == null || fs.t) {
                Lp lp = this.n;
                if (lp == null || lp.t) {
                    k(Zq.k);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        return this.X.getFirmwareVersion();
    }
}
