package com.fossil;

import com.fossil.Ix1;
import com.mapped.Wg6;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Lg extends Lp {
    @DexIgnore
    public /* final */ ArrayList<Ow> C; // = By1.a(this.i, Hm7.c(Ow.f));
    @DexIgnore
    public Boolean D;
    @DexIgnore
    public /* final */ byte[] E; // = new byte[8];
    @DexIgnore
    public /* final */ byte[] F;
    @DexIgnore
    public /* final */ byte[] G;

    @DexIgnore
    public Lg(K5 k5, I60 i60, byte[] bArr) {
        super(k5, i60, Yp.U, null, false, 24);
        this.G = bArr;
        byte[] copyOf = Arrays.copyOf(this.G, 16);
        Wg6.b(copyOf, "java.util.Arrays.copyOf(this, newSize)");
        this.F = copyOf;
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public void B() {
        if (this.G.length < 16) {
            k(Zq.y);
            return;
        }
        new SecureRandom().nextBytes(this.E);
        Lp.i(this, new Ft(this.w, Hd0.y.b(), this.E), new Jf(this), new Xf(this), null, null, null, 56, null);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject C() {
        return G80.k(super.C(), Jd0.t2, Long.valueOf(Ix1.a.b(this.G, Ix1.Ai.CRC32)));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public JSONObject E() {
        return G80.k(super.E(), Jd0.B3, this.D);
    }

    @DexIgnore
    public final void H(byte[] bArr) {
        Zq zq;
        if (bArr.length != 16) {
            zq = Zq.p;
        } else {
            byte[] a2 = Rt.g.a(Hd0.y.b(), Lx1.a.a(Ji.K.b(), this.F, Ji.K.a(), bArr));
            if (a2.length != 16) {
                zq = Zq.p;
            } else {
                if (Arrays.equals(this.E, Dm7.k(a2, 8, 16))) {
                    this.D = Boolean.TRUE;
                }
                zq = Zq.b;
            }
        }
        l(Nr.a(this.v, null, zq, null, null, 13));
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public Object x() {
        Boolean bool = this.D;
        return Boolean.valueOf(bool != null ? bool.booleanValue() : false);
    }

    @DexIgnore
    @Override // com.fossil.Lp
    public ArrayList<Ow> z() {
        return this.C;
    }
}
