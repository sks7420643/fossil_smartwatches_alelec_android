package com.fossil;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.widget.RemoteViews;
import com.fossil.Zk0;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Cl5 {
    @DexIgnore
    public static /* final */ NotificationManager a;
    @DexIgnore
    public static NotificationChannel b;
    @DexIgnore
    public static /* final */ Cl5 c; // = new Cl5();

    /*
    static {
        Object systemService = PortfolioApp.get.instance().getSystemService("notification");
        if (systemService != null) {
            a = (NotificationManager) systemService;
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.app.NotificationManager");
    }
    */

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0161  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.app.Notification a(com.fossil.Ws4 r12, android.content.Context r13) {
        /*
        // Method dump skipped, instructions count: 359
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.Cl5.a(com.fossil.Ws4, android.content.Context):android.app.Notification");
    }

    @DexIgnore
    public final Notification b(Ws4 ws4) {
        String str;
        Xs4 xs4;
        Hz4 hz4 = Hz4.a;
        Lt4 d = ws4.d();
        String b2 = d != null ? d.b() : null;
        Lt4 d2 = ws4.d();
        String d3 = d2 != null ? d2.d() : null;
        Lt4 d4 = ws4.d();
        if (d4 == null || (str = d4.e()) == null) {
            str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
        }
        String a2 = hz4.a(b2, d3, str);
        String c2 = Um5.c(PortfolioApp.get.instance(), 2131887229);
        Hr7 hr7 = Hr7.a;
        String c3 = Um5.c(PortfolioApp.get.instance(), 2131886195);
        Wg6.b(c3, "LanguageHelper.getString\u2026Body_Send_Friend_Request)");
        String format = String.format(c3, Arrays.copyOf(new Object[]{a2}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        Lt4 d5 = ws4.d();
        if (d5 != null) {
            String c4 = d5.c();
            String e = d5.e();
            if (e == null) {
                e = "";
            }
            String b3 = d5.b();
            if (b3 == null) {
                b3 = "";
            }
            String d6 = d5.d();
            if (d6 == null) {
                d6 = "";
            }
            String a3 = d5.a();
            if (a3 == null) {
                a3 = "";
            }
            xs4 = new Xs4(c4, e, b3, d6, null, a3, false, 0, 2);
        } else {
            xs4 = null;
        }
        Intent intent = new Intent(PortfolioApp.get.instance(), BCNotificationActionReceiver.class);
        intent.setAction("com.buddy_challenge.friend.accept");
        intent.putExtra("friend_extra", xs4);
        PortfolioApp instance = PortfolioApp.get.instance();
        String c5 = ws4.c();
        PendingIntent broadcast = PendingIntent.getBroadcast(instance, c5 != null ? c5.hashCode() : 0, intent, SQLiteDatabase.CREATE_IF_NECESSARY);
        Intent intent2 = new Intent(PortfolioApp.get.instance(), BCNotificationActionReceiver.class);
        intent2.setAction("com.buddy_challenge.friend.decline");
        intent2.putExtra("friend_extra", xs4);
        PortfolioApp instance2 = PortfolioApp.get.instance();
        String c6 = ws4.c();
        PendingIntent broadcast2 = PendingIntent.getBroadcast(instance2, c6 != null ? c6.hashCode() : 0, intent2, SQLiteDatabase.CREATE_IF_NECESSARY);
        String string = PortfolioApp.get.instance().getString(2131886294);
        Wg6.b(string, "PortfolioApp.instance.ge\u2026Friends_List_CTA__Accept)");
        String string2 = PortfolioApp.get.instance().getString(2131886295);
        Wg6.b(string2, "PortfolioApp.instance.ge\u2026riends_List_CTA__Decline)");
        BitmapFactory.decodeResource(PortfolioApp.get.instance().getResources(), 2131230909);
        Zk0.Ei ei = new Zk0.Ei(PortfolioApp.get.instance(), "FOSSIL_NOTIFICATION_CHANNEL_ID");
        ei.n(c2);
        ei.m(format);
        ei.y(R.drawable.ic_launcher_transparent);
        ei.a(0, string, broadcast);
        ei.a(0, string2, broadcast2);
        ei.h(0);
        ei.A(null);
        ei.v(false);
        ei.w(2);
        Notification c7 = ei.c();
        Wg6.b(c7, "NotificationCompat.Build\u2026\n                .build()");
        return c7;
    }

    @DexIgnore
    public final void c() {
        Object systemService = PortfolioApp.get.instance().getSystemService("notification");
        if (systemService != null) {
            ((NotificationManager) systemService).cancelAll();
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.app.NotificationManager");
    }

    @DexIgnore
    public final void d(Context context, int i) {
        Wg6.c(context, "context");
        Object systemService = context.getSystemService("notification");
        if (systemService != null) {
            ((NotificationManager) systemService).cancel(i);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.app.NotificationManager");
    }

    @DexIgnore
    public final void e() {
        d(PortfolioApp.get.instance(), 999);
    }

    @DexIgnore
    public final void f() {
        if (b == null) {
            NotificationChannel notificationChannel = new NotificationChannel("FOSSIL_NOTIFICATION_CHANNEL_ID", "Fossil Smartwatches Notification", 2);
            b = notificationChannel;
            if (notificationChannel != null) {
                notificationChannel.enableLights(true);
                NotificationChannel notificationChannel2 = b;
                if (notificationChannel2 != null) {
                    notificationChannel2.enableVibration(true);
                    NotificationChannel notificationChannel3 = b;
                    if (notificationChannel3 != null) {
                        notificationChannel3.setImportance(4);
                        NotificationChannel notificationChannel4 = b;
                        if (notificationChannel4 != null) {
                            notificationChannel4.setShowBadge(true);
                            NotificationManager notificationManager = a;
                            NotificationChannel notificationChannel5 = b;
                            if (notificationChannel5 != null) {
                                notificationManager.createNotificationChannel(notificationChannel5);
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void g(Context context, int i, String str, String str2, PendingIntent pendingIntent, List<? extends Zk0.Ai> list) {
        Wg6.c(context, "context");
        Wg6.c(str, "title");
        Wg6.c(str2, "text");
        Zk0.Ci ci = new Zk0.Ci();
        ci.g(str2);
        Uri defaultUri = RingtoneManager.getDefaultUri(2);
        if (Build.VERSION.SDK_INT >= 26) {
            f();
        }
        Zk0.Ei ei = new Zk0.Ei(context);
        ei.j("FOSSIL_NOTIFICATION_CHANNEL_ID");
        ei.n(str);
        ei.m(str2);
        ei.y(R.drawable.ic_launcher_transparent);
        ei.A(ci);
        ei.w(2);
        ei.C(new long[]{1000, 1000, 1000});
        ei.z(defaultUri);
        ei.l(pendingIntent);
        ei.g(true);
        Notification c2 = ei.c();
        c2.flags |= 16;
        if (Build.VERSION.SDK_INT >= 21) {
            Resources resources = PortfolioApp.get.instance().getResources();
            Package r3 = android.R.class.getPackage();
            if (r3 != null) {
                Wg6.b(r3, "android.R::class.java.`package`!!");
                int identifier = resources.getIdentifier("right_icon", "id", r3.getName());
                if (identifier != 0) {
                    RemoteViews remoteViews = c2.contentView;
                    if (remoteViews != null) {
                        remoteViews.setViewVisibility(identifier, 4);
                    }
                    RemoteViews remoteViews2 = c2.headsUpContentView;
                    if (remoteViews2 != null) {
                        remoteViews2.setViewVisibility(identifier, 4);
                    }
                    RemoteViews remoteViews3 = c2.bigContentView;
                    if (remoteViews3 != null) {
                        remoteViews3.setViewVisibility(identifier, 4);
                    }
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
        if (list != null) {
            for (Zk0.Ai ai : list) {
                ei.b(ai);
            }
        }
        a.notify(i, c2);
    }

    @DexIgnore
    public final void h(Context context, int i, String str, String str2, PendingIntent pendingIntent, List<? extends Zk0.Ai> list) {
        Wg6.c(context, "context");
        Wg6.c(str, "title");
        Wg6.c(str2, "text");
        Wg6.c(pendingIntent, "pendingIntent");
        Zk0.Ci ci = new Zk0.Ci();
        ci.g(str2);
        Uri defaultUri = RingtoneManager.getDefaultUri(2);
        Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), 2131689472);
        Zk0.Ei ei = new Zk0.Ei(context);
        ei.n(str);
        ei.m(str2);
        ei.y(2131689472);
        ei.r(decodeResource);
        ei.A(ci);
        ei.C(new long[]{1000, 1000, 1000});
        ei.z(defaultUri);
        ei.l(pendingIntent);
        Notification c2 = ei.c();
        c2.flags |= 16;
        if (Build.VERSION.SDK_INT >= 21) {
            Resources resources = PortfolioApp.get.instance().getResources();
            Package r2 = android.R.class.getPackage();
            if (r2 != null) {
                Wg6.b(r2, "android.R::class.java.`package`!!");
                int identifier = resources.getIdentifier("right_icon", "id", r2.getName());
                if (identifier != 0) {
                    RemoteViews remoteViews = c2.contentView;
                    if (remoteViews != null) {
                        remoteViews.setViewVisibility(identifier, 4);
                    }
                    RemoteViews remoteViews2 = c2.headsUpContentView;
                    if (remoteViews2 != null) {
                        remoteViews2.setViewVisibility(identifier, 4);
                    }
                    RemoteViews remoteViews3 = c2.bigContentView;
                    if (remoteViews3 != null) {
                        remoteViews3.setViewVisibility(identifier, 4);
                    }
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
        if (list != null) {
            for (Zk0.Ai ai : list) {
                ei.b(ai);
            }
        }
        Object systemService = context.getSystemService("notification");
        if (systemService != null) {
            ((NotificationManager) systemService).notify(i, c2);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.app.NotificationManager");
    }

    @DexIgnore
    public final void i(Ws4 ws4, Context context) {
        int hashCode;
        Notification b2;
        String a2;
        Wg6.c(ws4, "notification");
        Wg6.c(context, "context");
        Ss4 b3 = ws4.b();
        if (b3 == null || (a2 = b3.a()) == null) {
            Lt4 d = ws4.d();
            String c2 = d != null ? d.c() : null;
            hashCode = c2 != null ? c2.hashCode() : 0;
        } else {
            hashCode = a2.hashCode();
        }
        if (Build.VERSION.SDK_INT >= 26) {
            f();
        }
        String e = ws4.e();
        int hashCode2 = e.hashCode();
        if (hashCode2 != 1433363166) {
            if (hashCode2 == 1898363949 && e.equals("Title_Send_Invitation_Challenge")) {
                b2 = a(ws4, context);
            }
            throw new Exception("Wrong type");
        }
        if (e.equals("Title_Send_Friend_Request")) {
            b2 = b(ws4);
        }
        throw new Exception("Wrong type");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("Helper", "notificationID: " + hashCode);
        a.notify(hashCode, b2);
    }

    @DexIgnore
    public final void j() {
        Intent intent = new Intent(PortfolioApp.get.instance(), BCNotificationActionReceiver.class);
        intent.setAction("com.buddy_challenge.set_sync_data");
        PendingIntent broadcast = PendingIntent.getBroadcast(PortfolioApp.get.instance(), 9, intent, SQLiteDatabase.CREATE_IF_NECESSARY);
        Zk0.Ei ei = new Zk0.Ei(PortfolioApp.get.instance(), "FOSSIL_NOTIFICATION_CHANNEL_ID");
        ei.n(Um5.c(PortfolioApp.get.instance(), 2131886223));
        ei.y(R.drawable.ic_launcher_transparent);
        ei.a(0, Um5.c(PortfolioApp.get.instance(), 2131886130), broadcast);
        ei.h(0);
        Zk0.Ci ci = new Zk0.Ci();
        ci.g(Um5.c(PortfolioApp.get.instance(), 2131886225));
        ei.A(ci);
        ei.v(false);
        ei.w(2);
        Notification c2 = ei.c();
        Object systemService = PortfolioApp.get.instance().getSystemService("notification");
        if (systemService != null) {
            ((NotificationManager) systemService).notify(999, c2);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type android.app.NotificationManager");
    }
}
