package com.fossil;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Rr5 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public Rr5(String str, String str2, String str3, String str4, String str5) {
        Wg6.c(str, "packageName");
        Wg6.c(str2, "appName");
        Wg6.c(str3, "title");
        Wg6.c(str4, "artist");
        Wg6.c(str5, "album");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str5;
    }

    @DexIgnore
    public final String a() {
        return this.e;
    }

    @DexIgnore
    public final String b() {
        return this.d;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final String d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Rr5) {
                Rr5 rr5 = (Rr5) obj;
                if (!Wg6.a(this.a, rr5.a) || !Wg6.a(this.b, rr5.b) || !Wg6.a(this.c, rr5.c) || !Wg6.a(this.d, rr5.d) || !Wg6.a(this.e, rr5.e)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.a;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.b;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.c;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.d;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.e;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return (((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "MusicMetadata[packageName=" + this.a + ", appName=" + this.b + ", title=" + this.c + ", artist=" + this.d + ", album=" + this.e + ']';
    }
}
