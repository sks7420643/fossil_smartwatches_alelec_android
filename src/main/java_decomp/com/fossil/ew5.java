package com.fossil;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ew5 extends Hv0 {
    @DexIgnore
    public RecyclerView f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public /* final */ Ai i;

    @DexIgnore
    public interface Ai {
        @DexIgnore
        void a4(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends RecyclerView.q {
        @DexIgnore
        public /* final */ /* synthetic */ Ew5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(Ew5 ew5) {
            this.a = ew5;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            Wg6.c(recyclerView, "recyclerView");
            super.onScrollStateChanged(recyclerView, i);
            if (i == 0) {
                try {
                    View h = this.a.h(recyclerView.getLayoutManager());
                    Ew5 ew5 = this.a;
                    RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                    if (layoutManager == null) {
                        Wg6.i();
                        throw null;
                    } else if (h != null) {
                        ew5.x(layoutManager.i0(h));
                        if (this.a.h != this.a.w()) {
                            this.a.h = this.a.w();
                            Ai ai = this.a.i;
                            if (ai != null) {
                                ai.a4(this.a.w());
                            }
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.e("BlurLinearSnapHelper", "attachToRecyclerView - e=" + e);
                }
            }
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            Wg6.c(recyclerView, "recyclerView");
            super.onScrolled(recyclerView, i, i2);
            this.a.h(recyclerView.getLayoutManager());
        }
    }

    @DexIgnore
    public Ew5(Ai ai) {
        this.i = ai;
        this.h = -1;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Ew5(Ai ai, int i2, Qg6 qg6) {
        this((i2 & 1) != 0 ? null : ai);
    }

    @DexIgnore
    @Override // com.fossil.Qv0
    public void b(RecyclerView recyclerView) {
        super.b(recyclerView);
        this.f = recyclerView;
        if (recyclerView != null) {
            recyclerView.addOnScrollListener(new Bi(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.Hv0, com.fossil.Qv0
    public View h(RecyclerView.m mVar) {
        View h2 = super.h(mVar);
        v(mVar, h2);
        return h2;
    }

    @DexIgnore
    public final void v(RecyclerView.m mVar, View view) {
        View view2;
        View view3;
        if (view != null && mVar != null) {
            view.setAlpha(1.0f);
            int i2 = -1;
            int i3 = 0;
            int K = mVar.K() - 1;
            if (K >= 0) {
                while (true) {
                    if (!Wg6.a(mVar.J(i3), view)) {
                        if (i3 == K) {
                            break;
                        }
                        i3++;
                    } else {
                        i2 = i3;
                        break;
                    }
                }
            }
            if (i2 >= 0) {
                if (i2 > 0 && i2 < mVar.K() - 1) {
                    view2 = mVar.J(i2 - 1);
                    view3 = mVar.J(i2 + 1);
                } else if (i2 == 0) {
                    view3 = mVar.J(i2 + 1);
                    view2 = null;
                } else if (i2 == mVar.K() - 1) {
                    view2 = mVar.J(i2 - 1);
                    view3 = null;
                } else {
                    view2 = null;
                    view3 = null;
                }
                if (view2 != null) {
                    view2.setAlpha(0.8f);
                }
                if (view3 != null) {
                    view3.setAlpha(0.8f);
                }
            }
        }
    }

    @DexIgnore
    public final int w() {
        return this.g;
    }

    @DexIgnore
    public final void x(int i2) {
        this.g = i2;
    }
}
