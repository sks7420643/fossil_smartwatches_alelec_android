package com.fossil;

import com.mapped.An4;
import com.mapped.HybridMessageNotificationComponent;
import com.mapped.U04;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.service.notification.DianaNotificationComponent;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xq5 implements MembersInjector<FossilNotificationListenerService> {
    @DexIgnore
    public static void a(FossilNotificationListenerService fossilNotificationListenerService, U04 u04) {
        fossilNotificationListenerService.l = u04;
    }

    @DexIgnore
    public static void b(FossilNotificationListenerService fossilNotificationListenerService, DianaNotificationComponent dianaNotificationComponent) {
        fossilNotificationListenerService.j = dianaNotificationComponent;
    }

    @DexIgnore
    public static void c(FossilNotificationListenerService fossilNotificationListenerService, HybridMessageNotificationComponent hybridMessageNotificationComponent) {
        fossilNotificationListenerService.k = hybridMessageNotificationComponent;
    }

    @DexIgnore
    public static void d(FossilNotificationListenerService fossilNotificationListenerService, MusicControlComponent musicControlComponent) {
        fossilNotificationListenerService.i = musicControlComponent;
    }

    @DexIgnore
    public static void e(FossilNotificationListenerService fossilNotificationListenerService, An4 an4) {
        fossilNotificationListenerService.m = an4;
    }

    @DexIgnore
    public static void f(FossilNotificationListenerService fossilNotificationListenerService, UserRepository userRepository) {
        fossilNotificationListenerService.s = userRepository;
    }
}
