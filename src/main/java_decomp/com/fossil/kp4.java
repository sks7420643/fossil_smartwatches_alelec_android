package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.data.source.remote.GuestApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Kp4 implements Factory<GuestApiService> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<An4> b;

    @DexIgnore
    public Kp4(Uo4 uo4, Provider<An4> provider) {
        this.a = uo4;
        this.b = provider;
    }

    @DexIgnore
    public static Kp4 a(Uo4 uo4, Provider<An4> provider) {
        return new Kp4(uo4, provider);
    }

    @DexIgnore
    public static GuestApiService c(Uo4 uo4, An4 an4) {
        GuestApiService r = uo4.r(an4);
        Lk7.c(r, "Cannot return null from a non-@Nullable @Provides method");
        return r;
    }

    @DexIgnore
    public GuestApiService b() {
        return c(this.a, this.b.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
