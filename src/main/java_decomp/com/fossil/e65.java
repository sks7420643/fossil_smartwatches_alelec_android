package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class E65 extends D65 {
    @DexIgnore
    public static /* final */ ViewDataBinding.d B; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray C;
    @DexIgnore
    public long A;
    @DexIgnore
    public /* final */ ScrollView z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        C = sparseIntArray;
        sparseIntArray.put(2131363010, 1);
        C.put(2131362300, 2);
        C.put(2131362702, 3);
        C.put(2131362422, 4);
        C.put(2131362421, 5);
        C.put(2131362420, 6);
        C.put(2131363002, 7);
        C.put(2131362378, 8);
        C.put(2131362518, 9);
    }
    */

    @DexIgnore
    public E65(Zp0 zp0, View view) {
        this(zp0, view, ViewDataBinding.t(zp0, view, 10, B, C));
    }

    @DexIgnore
    public E65(Zp0 zp0, View view, Object[] objArr) {
        super(zp0, view, 0, (FlexibleEditText) objArr[2], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[9], (ImageView) objArr[3], (ConstraintLayout) objArr[7], (ConstraintLayout) objArr[1]);
        this.A = -1;
        ScrollView scrollView = (ScrollView) objArr[0];
        this.z = scrollView;
        scrollView.setTag(null);
        y(view);
        q();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void g() {
        synchronized (this) {
            this.A = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean o() {
        synchronized (this) {
            return this.A != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void q() {
        synchronized (this) {
            this.A = 1;
        }
        w();
    }
}
