package com.fossil;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import com.facebook.applinks.AppLinkData;
import com.google.android.gms.gcm.PendingCallback;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Pj2 extends Service {
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public int c;
    @DexIgnore
    public ExecutorService d;
    @DexIgnore
    public Messenger e;
    @DexIgnore
    public ComponentName f;
    @DexIgnore
    public Nj2 g;
    @DexIgnore
    public Bq2 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @TargetApi(21)
    public final class Ai extends Aq2 {
        @DexIgnore
        public Ai(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public final void handleMessage(Message message) {
            Messenger messenger;
            if (!Pf2.b(Pj2.this, message.sendingUid, "com.google.android.gms")) {
                Log.e("GcmTaskService", "unable to verify presence of Google Play Services");
                return;
            }
            int i = message.what;
            if (i == 1) {
                Bundle data = message.getData();
                if (!data.isEmpty() && (messenger = message.replyTo) != null) {
                    String string = data.getString("tag");
                    ArrayList parcelableArrayList = data.getParcelableArrayList("triggered_uris");
                    long j = data.getLong("max_exec_duration", 180);
                    if (!Pj2.this.k(string)) {
                        Pj2.this.e(new Bi(string, messenger, data.getBundle(AppLinkData.ARGUMENTS_EXTRAS_KEY), j, parcelableArrayList));
                    }
                }
            } else if (i != 2) {
                if (i != 4) {
                    String valueOf = String.valueOf(message);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 31);
                    sb.append("Unrecognized message received: ");
                    sb.append(valueOf);
                    Log.e("GcmTaskService", sb.toString());
                    return;
                }
                Pj2.this.a();
            } else if (Log.isLoggable("GcmTaskService", 3)) {
                String valueOf2 = String.valueOf(message);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 45);
                sb2.append("ignoring unimplemented stop message for now: ");
                sb2.append(valueOf2);
                Log.d("GcmTaskService", sb2.toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi implements Runnable {
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ Bundle c;
        @DexIgnore
        public /* final */ List<Uri> d;
        @DexIgnore
        public /* final */ long e;
        @DexIgnore
        public /* final */ Uj2 f;
        @DexIgnore
        public /* final */ Messenger g;

        @DexIgnore
        public Bi(String str, IBinder iBinder, Bundle bundle, long j, List<Uri> list) {
            Uj2 vj2;
            this.b = str;
            if (iBinder == null) {
                vj2 = null;
            } else {
                IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.gcm.INetworkTaskCallback");
                vj2 = queryLocalInterface instanceof Uj2 ? (Uj2) queryLocalInterface : new Vj2(iBinder);
            }
            this.f = vj2;
            this.c = bundle;
            this.e = j;
            this.d = list;
            this.g = null;
        }

        @DexIgnore
        public Bi(String str, Messenger messenger, Bundle bundle, long j, List<Uri> list) {
            this.b = str;
            this.g = messenger;
            this.c = bundle;
            this.e = j;
            this.d = list;
            this.f = null;
        }

        @DexIgnore
        public static /* synthetic */ void b(Throwable th, Xj2 xj2) {
            if (th != null) {
                try {
                    xj2.close();
                } catch (Throwable th2) {
                    Gq2.b(th, th2);
                }
            } else {
                xj2.close();
            }
        }

        @DexIgnore
        public final void c(int i) {
            synchronized (Pj2.this.b) {
                try {
                    if (Pj2.this.g.e(this.b, Pj2.this.f.getClassName())) {
                        Pj2.this.g.c(this.b, Pj2.this.f.getClassName());
                        if (!d() && !Pj2.this.g.d(Pj2.this.f.getClassName())) {
                            Pj2.this.stopSelf(Pj2.this.c);
                        }
                        return;
                    }
                    if (d()) {
                        Messenger messenger = this.g;
                        Message obtain = Message.obtain();
                        obtain.what = 3;
                        obtain.arg1 = i;
                        Bundle bundle = new Bundle();
                        bundle.putParcelable("component", Pj2.this.f);
                        bundle.putString("tag", this.b);
                        obtain.setData(bundle);
                        messenger.send(obtain);
                    } else {
                        this.f.d0(i);
                    }
                    Pj2.this.g.c(this.b, Pj2.this.f.getClassName());
                    if (!d() && !Pj2.this.g.d(Pj2.this.f.getClassName())) {
                        Pj2.this.stopSelf(Pj2.this.c);
                    }
                } catch (RemoteException e2) {
                    String valueOf = String.valueOf(this.b);
                    Log.e("GcmTaskService", valueOf.length() != 0 ? "Error reporting result of operation to scheduler for ".concat(valueOf) : new String("Error reporting result of operation to scheduler for "));
                    Pj2.this.g.c(this.b, Pj2.this.f.getClassName());
                    if (!d() && !Pj2.this.g.d(Pj2.this.f.getClassName())) {
                        Pj2.this.stopSelf(Pj2.this.c);
                    }
                } catch (Throwable th) {
                    Pj2.this.g.c(this.b, Pj2.this.f.getClassName());
                    if (!d() && !Pj2.this.g.d(Pj2.this.f.getClassName())) {
                        Pj2.this.stopSelf(Pj2.this.c);
                    }
                    throw th;
                }
            }
        }

        @DexIgnore
        public final boolean d() {
            return this.g != null;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x004d, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x004e, code lost:
            b(r0, r7);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0051, code lost:
            throw r1;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void run() {
            /*
                r8 = this;
                java.lang.String r0 = r8.b
                java.lang.String r0 = java.lang.String.valueOf(r0)
                int r1 = r0.length()
                if (r1 == 0) goto L_0x003f
                java.lang.String r1 = "nts:client:onRunTask:"
                java.lang.String r0 = r1.concat(r0)
            L_0x0012:
                com.fossil.Xj2 r7 = new com.fossil.Xj2
                r7.<init>(r0)
                com.fossil.Rj2 r1 = new com.fossil.Rj2     // Catch:{ all -> 0x004b }
                java.lang.String r2 = r8.b     // Catch:{ all -> 0x004b }
                android.os.Bundle r3 = r8.c     // Catch:{ all -> 0x004b }
                long r4 = r8.e     // Catch:{ all -> 0x004b }
                java.util.List<android.net.Uri> r6 = r8.d     // Catch:{ all -> 0x004b }
                r1.<init>(r2, r3, r4, r6)     // Catch:{ all -> 0x004b }
                com.fossil.Pj2 r0 = com.fossil.Pj2.this     // Catch:{ all -> 0x004b }
                com.fossil.Bq2 r0 = com.fossil.Pj2.c(r0)     // Catch:{ all -> 0x004b }
                java.lang.String r2 = "onRunTask"
                int r3 = com.fossil.Fq2.a     // Catch:{ all -> 0x004b }
                r0.b(r2, r3)     // Catch:{ all -> 0x004b }
                com.fossil.Pj2 r0 = com.fossil.Pj2.this     // Catch:{ all -> 0x0047 }
                int r0 = r0.b(r1)     // Catch:{ all -> 0x0047 }
                r8.c(r0)
                r0 = 0
                b(r0, r7)
                return
            L_0x003f:
                java.lang.String r0 = new java.lang.String
                java.lang.String r1 = "nts:client:onRunTask:"
                r0.<init>(r1)
                goto L_0x0012
            L_0x0047:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0049 }
            L_0x0049:
                r0 = move-exception
                throw r0
            L_0x004b:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x004d }
            L_0x004d:
                r1 = move-exception
                b(r0, r7)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.Pj2.Bi.run():void");
        }
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public abstract int b(Rj2 rj2);

    @DexIgnore
    public final void d(int i) {
        synchronized (this.b) {
            this.c = i;
            if (!this.g.d(this.f.getClassName())) {
                stopSelf(this.c);
            }
        }
    }

    @DexIgnore
    public final void e(Bi bi) {
        try {
            this.d.execute(bi);
        } catch (RejectedExecutionException e2) {
            Log.e("GcmTaskService", "Executor is shutdown. onDestroy was called but main looper had an unprocessed start task message. The task will be retried with backoff delay.", e2);
            bi.c(1);
        }
    }

    @DexIgnore
    public final boolean k(String str) {
        boolean z;
        synchronized (this.b) {
            z = !this.g.b(str, this.f.getClassName());
            if (z) {
                String packageName = getPackageName();
                StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 44 + String.valueOf(str).length());
                sb.append(packageName);
                sb.append(" ");
                sb.append(str);
                sb.append(": Task already running, won't start another");
                Log.w("GcmTaskService", sb.toString());
            }
        }
        return z;
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        if (intent == null || !Mf2.h() || !"com.google.android.gms.gcm.ACTION_TASK_READY".equals(intent.getAction())) {
            return null;
        }
        return this.e.getBinder();
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.g = Nj2.a(this);
        this.d = Xp2.a().a(10, new Tj2(this), 10);
        this.e = new Messenger(new Ai(Looper.getMainLooper()));
        this.f = new ComponentName(this, Pj2.class);
        Cq2.a();
        getClass();
        this.h = Cq2.a;
    }

    @DexIgnore
    public void onDestroy() {
        super.onDestroy();
        List<Runnable> shutdownNow = this.d.shutdownNow();
        if (!shutdownNow.isEmpty()) {
            int size = shutdownNow.size();
            StringBuilder sb = new StringBuilder(79);
            sb.append("Shutting down, but not all tasks are finished executing. Remaining: ");
            sb.append(size);
            Log.e("GcmTaskService", sb.toString());
        }
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent == null) {
            d(i2);
        } else {
            try {
                intent.setExtrasClassLoader(PendingCallback.class.getClassLoader());
                String action = intent.getAction();
                if ("com.google.android.gms.gcm.ACTION_TASK_READY".equals(action)) {
                    String stringExtra = intent.getStringExtra("tag");
                    Parcelable parcelableExtra = intent.getParcelableExtra(Constants.CALLBACK);
                    Bundle bundleExtra = intent.getBundleExtra(AppLinkData.ARGUMENTS_EXTRAS_KEY);
                    ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("triggered_uris");
                    long longExtra = intent.getLongExtra("max_exec_duration", 180);
                    if (!(parcelableExtra instanceof PendingCallback)) {
                        String packageName = getPackageName();
                        StringBuilder sb = new StringBuilder(String.valueOf(packageName).length() + 47 + String.valueOf(stringExtra).length());
                        sb.append(packageName);
                        sb.append(" ");
                        sb.append(stringExtra);
                        sb.append(": Could not process request, invalid callback.");
                        Log.e("GcmTaskService", sb.toString());
                    } else if (k(stringExtra)) {
                        d(i2);
                    } else {
                        e(new Bi(stringExtra, ((PendingCallback) parcelableExtra).b, bundleExtra, longExtra, parcelableArrayListExtra));
                    }
                } else if ("com.google.android.gms.gcm.SERVICE_ACTION_INITIALIZE".equals(action)) {
                    a();
                } else {
                    StringBuilder sb2 = new StringBuilder(String.valueOf(action).length() + 37);
                    sb2.append("Unknown action received ");
                    sb2.append(action);
                    sb2.append(", terminating");
                    Log.e("GcmTaskService", sb2.toString());
                }
                d(i2);
            } finally {
                d(i2);
            }
        }
        return 2;
    }
}
