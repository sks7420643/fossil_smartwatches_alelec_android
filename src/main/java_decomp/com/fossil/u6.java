package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum U6 {
    b,
    c;

    @DexIgnore
    public final UUID a() {
        if (T6.a[ordinal()] != 1) {
            return null;
        }
        return Hd0.y.a();
    }
}
