package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class L8 extends Enum<L8> {
    @DexIgnore
    public static /* final */ L8 c;
    @DexIgnore
    public static /* final */ /* synthetic */ L8[] d;
    @DexIgnore
    public /* final */ short b;

    /*
    static {
        L8 l8 = new L8("ACTIVITY_FILE", 0, Ob.e.b);
        c = l8;
        d = new L8[]{l8, new L8("HARDWARE_LOG", 1, Ob.f.b)};
    }
    */

    @DexIgnore
    public L8(String str, int i, short s) {
        this.b = (short) s;
    }

    @DexIgnore
    public static L8 valueOf(String str) {
        return (L8) Enum.valueOf(L8.class, str);
    }

    @DexIgnore
    public static L8[] values() {
        return (L8[]) d.clone();
    }
}
