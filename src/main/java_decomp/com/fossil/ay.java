package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Ay implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Sy b;
    @DexIgnore
    public /* final */ /* synthetic */ Lp c;
    @DexIgnore
    public /* final */ /* synthetic */ float d;

    @DexIgnore
    public Ay(Sy sy, Lp lp, float f) {
        this.b = sy;
        this.c = lp;
        this.d = f;
    }

    @DexIgnore
    public final void run() {
        E60.p0(this.b.c, Ky1.DEBUG, Ey1.a(this.c.y), "Progress: %.4f.", Float.valueOf(this.d));
        this.b.b.y(this.d);
    }
}
