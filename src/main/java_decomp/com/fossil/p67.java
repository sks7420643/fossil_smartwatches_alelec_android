package com.fossil;

import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class P67 {
    @DexIgnore
    public static final boolean a(Rect rect, MotionEvent motionEvent) {
        Wg6.c(rect, "$this$contains");
        Wg6.c(motionEvent, Constants.EVENT);
        return rect.contains((int) motionEvent.getRawX(), (int) motionEvent.getRawY());
    }

    @DexIgnore
    public static final Rect b(View view) {
        Wg6.c(view, "$this$getDrawingRectOnScreen");
        int[] iArr = new int[2];
        Rect rect = new Rect();
        view.getDrawingRect(rect);
        view.getLocationOnScreen(iArr);
        rect.offset(iArr[0], iArr[1]);
        rect.right = (int) (((float) rect.left) + (((float) rect.width()) * view.getScaleX()));
        rect.bottom = (int) (((float) rect.top) + (((float) rect.height()) * view.getScaleX()));
        return rect;
    }

    @DexIgnore
    public static final int[] c(View view) {
        Wg6.c(view, "$this$getLocationOnScreen");
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        return iArr;
    }
}
