package com.fossil;

import android.net.Uri;
import com.facebook.internal.Utility;
import com.fossil.Af1;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Mf1 implements Af1<Uri, InputStream> {
    @DexIgnore
    public static /* final */ Set<String> b; // = Collections.unmodifiableSet(new HashSet(Arrays.asList("http", Utility.URL_SCHEME)));
    @DexIgnore
    public /* final */ Af1<Te1, InputStream> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Ai implements Bf1<Uri, InputStream> {
        @DexIgnore
        @Override // com.fossil.Bf1
        public Af1<Uri, InputStream> b(Ef1 ef1) {
            return new Mf1(ef1.d(Te1.class, InputStream.class));
        }
    }

    @DexIgnore
    public Mf1(Af1<Te1, InputStream> af1) {
        this.a = af1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ boolean a(Uri uri) {
        return d(uri);
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.Af1$Ai' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, int, int, com.fossil.Ob1] */
    @Override // com.fossil.Af1
    public /* bridge */ /* synthetic */ Af1.Ai<InputStream> b(Uri uri, int i, int i2, Ob1 ob1) {
        return c(uri, i, i2, ob1);
    }

    @DexIgnore
    public Af1.Ai<InputStream> c(Uri uri, int i, int i2, Ob1 ob1) {
        return this.a.b(new Te1(uri.toString()), i, i2, ob1);
    }

    @DexIgnore
    public boolean d(Uri uri) {
        return b.contains(uri.getScheme());
    }
}
