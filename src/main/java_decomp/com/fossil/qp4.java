package com.fossil;

import com.mapped.An4;
import com.portfolio.platform.MigrationManager;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.migration.MigrationHelper;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Qp4 implements Factory<MigrationHelper> {
    @DexIgnore
    public /* final */ Uo4 a;
    @DexIgnore
    public /* final */ Provider<An4> b;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> c;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> d;
    @DexIgnore
    public /* final */ Provider<MigrationManager> e;

    @DexIgnore
    public Qp4(Uo4 uo4, Provider<An4> provider, Provider<DeviceRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<MigrationManager> provider4) {
        this.a = uo4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
    }

    @DexIgnore
    public static Qp4 a(Uo4 uo4, Provider<An4> provider, Provider<DeviceRepository> provider2, Provider<DianaPresetRepository> provider3, Provider<MigrationManager> provider4) {
        return new Qp4(uo4, provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static MigrationHelper c(Uo4 uo4, An4 an4, DeviceRepository deviceRepository, DianaPresetRepository dianaPresetRepository, MigrationManager migrationManager) {
        MigrationHelper x = uo4.x(an4, deviceRepository, dianaPresetRepository, migrationManager);
        Lk7.c(x, "Cannot return null from a non-@Nullable @Provides method");
        return x;
    }

    @DexIgnore
    public MigrationHelper b() {
        return c(this.a, this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
