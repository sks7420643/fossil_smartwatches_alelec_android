package com.fossil;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.fossil.H72;
import com.fossil.Pc2;
import com.google.firebase.components.ComponentDiscoveryService;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class J64 {
    @DexIgnore
    public static /* final */ Object i; // = new Object();
    @DexIgnore
    public static /* final */ Executor j; // = new Di();
    @DexIgnore
    public static /* final */ Map<String, J64> k; // = new Zi0();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ L64 c;
    @DexIgnore
    public /* final */ I74 d;
    @DexIgnore
    public /* final */ AtomicBoolean e; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ AtomicBoolean f; // = new AtomicBoolean();
    @DexIgnore
    public /* final */ P74<Lh4> g;
    @DexIgnore
    public /* final */ List<Bi> h; // = new CopyOnWriteArrayList();

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @TargetApi(14)
    public static class Ci implements H72.Ai {
        @DexIgnore
        public static AtomicReference<Ci> a; // = new AtomicReference<>();

        @DexIgnore
        public static void c(Context context) {
            if (Mf2.a() && (context.getApplicationContext() instanceof Application)) {
                Application application = (Application) context.getApplicationContext();
                if (a.get() == null) {
                    Ci ci = new Ci();
                    if (a.compareAndSet(null, ci)) {
                        H72.c(application);
                        H72.b().a(ci);
                    }
                }
            }
        }

        @DexIgnore
        @Override // com.fossil.H72.Ai
        public void a(boolean z) {
            synchronized (J64.i) {
                Iterator it = new ArrayList(J64.k.values()).iterator();
                while (it.hasNext()) {
                    J64 j64 = (J64) it.next();
                    if (j64.e.get()) {
                        j64.t(z);
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Di implements Executor {
        @DexIgnore
        public static /* final */ Handler b; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public Di() {
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            b.post(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @TargetApi(24)
    public static class Ei extends BroadcastReceiver {
        @DexIgnore
        public static AtomicReference<Ei> b; // = new AtomicReference<>();
        @DexIgnore
        public /* final */ Context a;

        @DexIgnore
        public Ei(Context context) {
            this.a = context;
        }

        @DexIgnore
        public static void b(Context context) {
            if (b.get() == null) {
                Ei ei = new Ei(context);
                if (b.compareAndSet(null, ei)) {
                    context.registerReceiver(ei, new IntentFilter("android.intent.action.USER_UNLOCKED"));
                }
            }
        }

        @DexIgnore
        public void c() {
            this.a.unregisterReceiver(this);
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            synchronized (J64.i) {
                for (J64 j64 : J64.k.values()) {
                    j64.l();
                }
            }
            c();
        }
    }

    @DexIgnore
    public J64(Context context, String str, L64 l64) {
        new CopyOnWriteArrayList();
        Rc2.k(context);
        this.a = context;
        Rc2.g(str);
        this.b = str;
        Rc2.k(l64);
        this.c = l64;
        List<E74> a2 = C74.b(context, ComponentDiscoveryService.class).a();
        String a3 = Qi4.a();
        this.d = new I74(j, a2, A74.n(context, Context.class, new Class[0]), A74.n(this, J64.class, new Class[0]), A74.n(l64, L64.class, new Class[0]), Si4.a("fire-android", ""), Si4.a("fire-core", "19.3.0"), a3 != null ? Si4.a("kotlin", a3) : null, Oi4.b(), Ie4.b());
        this.g = new P74<>(I64.a(this, context));
    }

    @DexIgnore
    public static J64 h() {
        J64 j64;
        synchronized (i) {
            j64 = k.get("[DEFAULT]");
            if (j64 == null) {
                throw new IllegalStateException("Default FirebaseApp is not initialized in this process " + Nf2.a() + ". Make sure to call FirebaseApp.initializeApp(Context) first.");
            }
        }
        return j64;
    }

    @DexIgnore
    public static J64 m(Context context) {
        synchronized (i) {
            if (k.containsKey("[DEFAULT]")) {
                return h();
            }
            L64 a2 = L64.a(context);
            if (a2 == null) {
                Log.w("FirebaseApp", "Default FirebaseApp failed to initialize because no default options were found. This usually means that com.google.gms:google-services was not applied to your gradle project.");
                return null;
            }
            return n(context, a2);
        }
    }

    @DexIgnore
    public static J64 n(Context context, L64 l64) {
        return o(context, l64, "[DEFAULT]");
    }

    @DexIgnore
    public static J64 o(Context context, L64 l64, String str) {
        J64 j64;
        Ci.c(context);
        String s = s(str);
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        synchronized (i) {
            boolean z = !k.containsKey(s);
            Rc2.o(z, "FirebaseApp name " + s + " already exists!");
            Rc2.l(context, "Application context cannot be null.");
            j64 = new J64(context, s, l64);
            k.put(s, j64);
        }
        j64.l();
        return j64;
    }

    @DexIgnore
    public static /* synthetic */ Lh4 r(J64 j64, Context context) {
        return new Lh4(context, j64.k(), (Fe4) j64.d.get(Fe4.class));
    }

    @DexIgnore
    public static String s(String str) {
        return str.trim();
    }

    @DexIgnore
    public final void e() {
        Rc2.o(!this.f.get(), "FirebaseApp was deleted");
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof J64)) {
            return false;
        }
        return this.b.equals(((J64) obj).i());
    }

    @DexIgnore
    public <T> T f(Class<T> cls) {
        e();
        return (T) this.d.get(cls);
    }

    @DexIgnore
    public Context g() {
        e();
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String i() {
        e();
        return this.b;
    }

    @DexIgnore
    public L64 j() {
        e();
        return this.c;
    }

    @DexIgnore
    public String k() {
        return Cf2.a(i().getBytes(Charset.defaultCharset())) + G78.ANY_NON_NULL_MARKER + Cf2.a(j().c().getBytes(Charset.defaultCharset()));
    }

    @DexIgnore
    public final void l() {
        if (!Xm0.a(this.a)) {
            Ei.b(this.a);
        } else {
            this.d.d(q());
        }
    }

    @DexIgnore
    public boolean p() {
        e();
        return this.g.get().b();
    }

    @DexIgnore
    public boolean q() {
        return "[DEFAULT]".equals(i());
    }

    @DexIgnore
    public final void t(boolean z) {
        Log.d("FirebaseApp", "Notifying background state change listeners.");
        for (Bi bi : this.h) {
            bi.a(z);
        }
    }

    @DexIgnore
    public String toString() {
        Pc2.Ai c2 = Pc2.c(this);
        c2.a("name", this.b);
        c2.a("options", this.c);
        return c2.toString();
    }
}
