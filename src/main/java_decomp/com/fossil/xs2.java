package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Xs2 extends Zc2 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Xs2> CREATOR; // = new At2();
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ Bundle h;

    @DexIgnore
    public Xs2(long j, long j2, boolean z, String str, String str2, String str3, Bundle bundle) {
        this.b = j;
        this.c = j2;
        this.d = z;
        this.e = str;
        this.f = str2;
        this.g = str3;
        this.h = bundle;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = Bd2.a(parcel);
        Bd2.r(parcel, 1, this.b);
        Bd2.r(parcel, 2, this.c);
        Bd2.c(parcel, 3, this.d);
        Bd2.u(parcel, 4, this.e, false);
        Bd2.u(parcel, 5, this.f, false);
        Bd2.u(parcel, 6, this.g, false);
        Bd2.e(parcel, 7, this.h, false);
        Bd2.b(parcel, a2);
    }
}
