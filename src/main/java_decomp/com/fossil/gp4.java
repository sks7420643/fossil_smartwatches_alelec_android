package com.fossil;

import com.portfolio.platform.manager.FileDownloadManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Gp4 implements Factory<FileDownloadManager> {
    @DexIgnore
    public /* final */ Uo4 a;

    @DexIgnore
    public Gp4(Uo4 uo4) {
        this.a = uo4;
    }

    @DexIgnore
    public static Gp4 a(Uo4 uo4) {
        return new Gp4(uo4);
    }

    @DexIgnore
    public static FileDownloadManager c(Uo4 uo4) {
        FileDownloadManager n = uo4.n();
        Lk7.c(n, "Cannot return null from a non-@Nullable @Provides method");
        return n;
    }

    @DexIgnore
    public FileDownloadManager b() {
        return c(this.a);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public /* bridge */ /* synthetic */ Object get() {
        return b();
    }
}
