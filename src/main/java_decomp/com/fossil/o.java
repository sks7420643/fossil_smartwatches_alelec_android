package com.fossil;

import com.fossil.Yx1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class O<V, E extends Yx1> {
    @DexIgnore
    public /* final */ V a;
    @DexIgnore
    public /* final */ E b;

    @DexIgnore
    public O(E e) {
        this.a = null;
        this.b = e;
    }

    @DexIgnore
    public O(V v) {
        this.a = v;
        this.b = null;
    }
}
