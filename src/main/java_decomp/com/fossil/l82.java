package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class L82 extends F92 {
    @DexIgnore
    public /* final */ /* synthetic */ Z52 b;
    @DexIgnore
    public /* final */ /* synthetic */ M82 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public L82(M82 m82, D92 d92, Z52 z52) {
        super(d92);
        this.c = m82;
        this.b = z52;
    }

    @DexIgnore
    @Override // com.fossil.F92
    public final void a() {
        this.c.d.D(this.b);
    }
}
