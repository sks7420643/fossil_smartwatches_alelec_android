package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Iz6 implements Factory<Dz6> {
    @DexIgnore
    public static Dz6 a(Gz6 gz6) {
        Dz6 b = gz6.b();
        Lk7.c(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
