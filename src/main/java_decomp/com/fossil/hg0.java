package com.fossil;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;
import com.fossil.Ig0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hg0 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Cg0 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public View f;
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public Ig0.Ai i;
    @DexIgnore
    public Gg0 j;
    @DexIgnore
    public PopupWindow.OnDismissListener k;
    @DexIgnore
    public /* final */ PopupWindow.OnDismissListener l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Ai implements PopupWindow.OnDismissListener {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public void onDismiss() {
            Hg0.this.e();
        }
    }

    @DexIgnore
    public Hg0(Context context, Cg0 cg0, View view, boolean z, int i2) {
        this(context, cg0, view, z, i2, 0);
    }

    @DexIgnore
    public Hg0(Context context, Cg0 cg0, View view, boolean z, int i2, int i3) {
        this.g = 8388611;
        this.l = new Ai();
        this.a = context;
        this.b = cg0;
        this.f = view;
        this.c = z;
        this.d = i2;
        this.e = i3;
    }

    @DexIgnore
    public final Gg0 a() {
        Display defaultDisplay = ((WindowManager) this.a.getSystemService("window")).getDefaultDisplay();
        Point point = new Point();
        if (Build.VERSION.SDK_INT >= 17) {
            defaultDisplay.getRealSize(point);
        } else {
            defaultDisplay.getSize(point);
        }
        Gg0 zf0 = Math.min(point.x, point.y) >= this.a.getResources().getDimensionPixelSize(Oe0.abc_cascading_menus_min_smallest_width) ? new Zf0(this.a, this.f, this.d, this.e, this.c) : new Mg0(this.a, this.b, this.f, this.d, this.e, this.c);
        zf0.m(this.b);
        zf0.v(this.l);
        zf0.q(this.f);
        zf0.g(this.i);
        zf0.s(this.h);
        zf0.t(this.g);
        return zf0;
    }

    @DexIgnore
    public void b() {
        if (d()) {
            this.j.dismiss();
        }
    }

    @DexIgnore
    public Gg0 c() {
        if (this.j == null) {
            this.j = a();
        }
        return this.j;
    }

    @DexIgnore
    public boolean d() {
        Gg0 gg0 = this.j;
        return gg0 != null && gg0.a();
    }

    @DexIgnore
    public void e() {
        this.j = null;
        PopupWindow.OnDismissListener onDismissListener = this.k;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    @DexIgnore
    public void f(View view) {
        this.f = view;
    }

    @DexIgnore
    public void g(boolean z) {
        this.h = z;
        Gg0 gg0 = this.j;
        if (gg0 != null) {
            gg0.s(z);
        }
    }

    @DexIgnore
    public void h(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public void i(PopupWindow.OnDismissListener onDismissListener) {
        this.k = onDismissListener;
    }

    @DexIgnore
    public void j(Ig0.Ai ai) {
        this.i = ai;
        Gg0 gg0 = this.j;
        if (gg0 != null) {
            gg0.g(ai);
        }
    }

    @DexIgnore
    public void k() {
        if (!m()) {
            throw new IllegalStateException("MenuPopupHelper cannot be used without an anchor");
        }
    }

    @DexIgnore
    public final void l(int i2, int i3, boolean z, boolean z2) {
        Gg0 c2 = c();
        c2.w(z2);
        if (z) {
            if ((Wn0.b(this.g, Mo0.z(this.f)) & 7) == 5) {
                i2 -= this.f.getWidth();
            }
            c2.u(i2);
            c2.x(i3);
            int i4 = (int) ((this.a.getResources().getDisplayMetrics().density * 48.0f) / 2.0f);
            c2.r(new Rect(i2 - i4, i3 - i4, i2 + i4, i4 + i3));
        }
        c2.show();
    }

    @DexIgnore
    public boolean m() {
        if (d()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        l(0, 0, false, false);
        return true;
    }

    @DexIgnore
    public boolean n(int i2, int i3) {
        if (d()) {
            return true;
        }
        if (this.f == null) {
            return false;
        }
        l(i2, i3, true, true);
        return true;
    }
}
