package com.fossil;

import com.mapped.Qg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum Mn1 {
    KCAL(0),
    CAL(1);
    
    @DexIgnore
    public static /* final */ Ai d; // = new Ai(null);
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
        }

        @DexIgnore
        public final Mn1 a(int i) {
            Mn1[] values = Mn1.values();
            for (Mn1 mn1 : values) {
                if (mn1.a() == i) {
                    return mn1;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public Mn1(int i) {
        this.b = i;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }
}
