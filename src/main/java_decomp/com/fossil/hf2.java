package com.fossil;

import android.os.SystemClock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Hf2 implements Ef2 {
    @DexIgnore
    public static /* final */ Hf2 a; // = new Hf2();

    @DexIgnore
    public static Ef2 d() {
        return a;
    }

    @DexIgnore
    @Override // com.fossil.Ef2
    public long a() {
        return System.nanoTime();
    }

    @DexIgnore
    @Override // com.fossil.Ef2
    public long b() {
        return System.currentTimeMillis();
    }

    @DexIgnore
    @Override // com.fossil.Ef2
    public long c() {
        return SystemClock.elapsedRealtime();
    }
}
