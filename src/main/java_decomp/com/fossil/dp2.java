package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class Dp2 extends Xm2 implements Bp2 {
    @DexIgnore
    public Dp2() {
        super("com.google.android.gms.fitness.internal.service.IFitnessSensorService");
    }

    @DexIgnore
    @Override // com.fossil.Xm2
    public final boolean d(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            j0((Yo2) Qo2.a(parcel, Yo2.CREATOR), Xn2.e(parcel.readStrongBinder()));
        } else if (i == 2) {
            h1((Gj2) Qo2.a(parcel, Gj2.CREATOR), Oo2.e(parcel.readStrongBinder()));
        } else if (i != 3) {
            return false;
        } else {
            R1((Zo2) Qo2.a(parcel, Zo2.CREATOR), Oo2.e(parcel.readStrongBinder()));
        }
        return true;
    }
}
